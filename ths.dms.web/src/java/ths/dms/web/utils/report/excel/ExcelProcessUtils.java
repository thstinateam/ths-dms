/**
 * 
 *//*
package ths.dms.web.utils.report.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;

*//**
 * The Class ExcelProcessUtils.
 *
 * @author hungtx
 * @since Aug 19, 2013
 *//*
public class ExcelProcessUtils {
	//writableworkbook jxl
	public final static String HEADER = "header";
	public final static String MENU = "menu";
	public final static String MENU_GRAY = "menu_gray";
	public final static String MENU_RED = "menu_red";
	public final static String BOLD = "bold";
	public final static String BOLD_CENTER = "bold_center";
	public final static String NORMAL_CENTER = "normal_center";
	public final static String NORMAL = "normal";
	public final static String DETAIL_NORMAL = "detail_normal";
	public final static String NORMAL_MEDIUM_TOP_BORDER = "detail_medium_top_border";
	public final static String NORMAL_MEDIUM_LEFT_BORDER = "detail_medium_left_border";
	public final static String NORMAL_MEDIUM_RIGHT_BORDER = "detail_medium_right_border";
	public final static String NORMAL_MEDIUM_BOTTOM_BORDER = "detail_medium_bottom_border";
	
	public final static String DETAIL_NORMAL_DOTTED_CENTER = "detail_normal_dotted_center";
	public final static String DETAIL_NORMAL_DOTTED_CENTER_RED = "detail_normal_dotted_center_red";
	public final static String DETAIL_NORMAL_DOTTED_LEFT = "detail_normal_dotted_left";
	public final static String DETAIL_NORMAL_DOTTED_RIGHT = "detail_normal_dotted_right";
	public final static String DETAIL_NORMAL_CENTER = "detail_normal_center";
	public final static String DETAIL_NORMAL_LEFT = "detail_normal_left";
	public final static String DETAIL_NORMAL_RIGHT = "detail_normal_right";
	public final static String DETAIL_ORANGE_ACCENT6_DOTTED_LEFT = "detail_orange_accent6_dotted_left";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_LEFT = "detail_orange_light80_dotted_left";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_LEFT = "detail_orange_light60_dotted_left";
	public final static String DETAIL_ORANGE_LIGHT40_DOTTED_LEFT = "detail_orange_light40_dotted_left";
	public final static String DETAIL_ORANGE_DARK25_DOTTED_LEFT = "detail_orange_dark25_dotted_left";
	public final static String DETAIL_ORANGE_DARK50_DOTTED_LEFT = "detail_orange_dark50_dotted_left";
	public final static String DETAIL_ORANGE_ACCENT6_DOTTED_RIGHT = "detail_orange_accent6_dotted_right";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT = "detail_orange_light80_dotted_right";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT = "detail_orange_light60_dotted_right";
	public final static String DETAIL_ORANGE_LIGHT40_DOTTED_RIGHT = "detail_orange_light40_dotted_right";
	public final static String DETAIL_ORANGE_DARK25_DOTTED_RIGHT = "detail_orange_dark25_dotted_right";
	public final static String DETAIL_ORANGE_DARK50_DOTTED_RIGHT = "detail_orange_dark50_dotted_right";
	public final static String DETAIL_ORANGE_ACCENT6_DOTTED_CENTER = "detail_orange_accent6_dotted_center";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_CENTER = "detail_orange_light80_dotted_center";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_CENTER = "detail_orange_light60_dotted_center";
	public final static String DETAIL_ORANGE_LIGHT40_DOTTED_CENTER = "detail_orange_light40_dotted_center";
	public final static String DETAIL_ORANGE_DARK25_DOTTED_CENTER = "detail_orange_dark25_dotted_center";
	public final static String DETAIL_ORANGE_DARK50_DOTTED_CENTER = "detail_orange_dark50_dotted_center";
	
	public final static String DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_LEFT = "detail_normal_orange_accent6_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_LEFT = "detail_normal_orange_light80_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_LEFT = "detail_normal_orange_light60_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_LEFT = "detail_normal_orange_light40_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_DARK25_DOTTED_LEFT = "detail_normal_orange_dark25_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_DARK50_DOTTED_LEFT = "detail_normal_orange_dark50_dotted_left";
	
	public final static String DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_CENTER = "detail_normal_orange_accent6_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_CENTER = "detail_normal_orange_light80_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_CENTER = "detail_normal_orange_light60_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_CENTER = "detail_normal_orange_light40_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_DARK25_DOTTED_CENTER = "detail_normal_orange_dark25_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_DARK50_DOTTED_CENTER = "detail_normal_orange_dark50_dotted_center";
	
	public final static String DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_RIGHT = "detail_normal_orange_accent6_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_RIGHT = "detail_normal_orange_light80_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_RIGHT = "detail_normal_orange_light60_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_RIGHT = "detail_normal_orange_light40_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_DARK25_DOTTED_RIGHT = "detail_normal_orange_dark25_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_DARK50_DOTTED_RIGHT = "detail_normal_orange_dark50_dotted_right";
	
	//detail with red color font
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_LEFT_RED = "detail_orange_light80_dotted_left_red";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT_RED = "detail_orange_light80_dotted_right_red";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_CENTER_RED = "detail_orange_light80_dotted_center_red";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_LEFT_RED = "detail_orange_light60_dotted_left_red";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT_RED = "detail_orange_light60_dotted_right_red";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_CENTER_RED = "detail_orange_light60_dotted_center_red";
	
	
	//JXL color, index must be in 8-64
	public final static int JXL_BLUE = 8;
	public final static int JXL_WHITE = 9;
	public final static int JXL_BLACK = 10;
	public final static int JXL_GRAY = 11;
	public final static int JXL_RED = 12;
	
	public final static int JXL_ORANGE_ACCENT6 = 19;
	public final static int JXL_ORANGE_LIGHT80 = 20;
	public final static int JXL_ORANGE_LIGHT60 = 21;
	public final static int JXL_ORANGE_LIGHT40 = 22;
	public final static int JXL_ORANGE_DARK25 = 23;
	public final static int JXL_ORANGE_DARK50 = 24;
	
	//XSSF color
	public final static XSSFColor poiBlue = new XSSFColor(new java.awt.Color(83, 142, 213));
	public final static XSSFColor poiWhite = new XSSFColor(new java.awt.Color(255, 255, 255));
	public final static XSSFColor poiBlack = new XSSFColor(new java.awt.Color(0, 0, 0));
	public final static XSSFColor poiGreen = new XSSFColor(new java.awt.Color(0, 176, 80));
	public final static XSSFColor poiOrange_Accent6 = new XSSFColor(new java.awt.Color(247, 150,70));
	public final static XSSFColor poiOrange_light80 = new XSSFColor(new java.awt.Color(253, 233, 217));
	public final static XSSFColor poiOrange_light60 = new XSSFColor(new java.awt.Color(252, 213, 180));
	public final static XSSFColor poiOrange_light40 = new XSSFColor(new java.awt.Color(250, 192, 144));
	public final static XSSFColor poiOrange_dark25 = new XSSFColor(new java.awt.Color(228, 109, 10));
	public final static XSSFColor poiOrange_dark50 = new XSSFColor(new java.awt.Color(151, 72, 7));
	public final static XSSFColor poiGray = new XSSFColor(new java.awt.Color(216, 216, 216));
	public final static XSSFColor poiRed = new XSSFColor(new java.awt.Color(255, 0, 0));
	
	
	*//** -------------------BEGIN createStyles(XSSFWorkbook wb) -----------------------------**//*
	public final static String STYLE_TITLE = "title";
	public final static String STYLE_PERCENT = "percent";
	public final static String STYLE_COEFF = "coeff";
	public final static String STYLE_CURRENCY = "currency";
	public final static String STYLE_DATE = "date";	
	public final static String STYLE_ACCOUNTING = "accounting";	
	public final static String STYLE_HEADER = "header";
	public final static String STYLE_TEXT = "text";
	public final static String STYLE_TEXT_ALIGN_LEFT = "text_left";
	public final static String STYLE_TEXT_ALIGN_CENTER = "text_center";
	public final static String STYLE_TEXT_ALIGN_RIGHT = "text_right";
	*//** -------------------END createStyles (XSSFWorkbook wb)-----------------------------**//*
	
	*//**
	 * Substitute.
	 *
	 * @param zipfile the zipfile
	 * @param tmpfile the tmpfile
	 * @param entry the entry
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public static void substitute(File zipfile, File tmpfile, String entry,
			OutputStream out) throws IOException {
		ZipFile zip = new ZipFile(zipfile);

		ZipOutputStream zos = new ZipOutputStream(out);

		@SuppressWarnings("unchecked")
		Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
		while (en.hasMoreElements()) {
			ZipEntry ze = en.nextElement();
			if (!ze.getName().equals(entry)) {
				zos.putNextEntry(new ZipEntry(ze.getName()));
				InputStream is = zip.getInputStream(ze);
				copyStream(is, zos);
				is.close();
			}
		}
		zos.putNextEntry(new ZipEntry(entry));
		InputStream is = new FileInputStream(tmpfile);
		copyStream(is, zos);
		is.close();

		zos.close();
		zip.close();
		zipfile.delete();
	}
	
	*//**
	 * Copy stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	private static void copyStream(InputStream in, OutputStream out)
			throws IOException {
		byte[] chunk = new byte[1024];
		int count;
		while ((count = in.read(chunk)) >= 0) {
			out.write(chunk, 0, count);
		}
	}	
	
	
	
	public static Map<String, WritableCellFormat> createStylesWritableWorkbook(WritableWorkbook wb) throws WriteException {
		Map<String, WritableCellFormat> styles = new HashMap<String, WritableCellFormat>();
//		XSSFDataFormat fmt = wb.createDataFormat();
		wb.setColourRGB(Colour.getInternalColour(JXL_BLUE), 83, 142, 213);
		wb.setColourRGB(Colour.getInternalColour(JXL_WHITE), 255, 255, 255);
		wb.setColourRGB(Colour.getInternalColour(JXL_BLACK), 0, 0, 0);
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_LIGHT80), 253,	233, 217);//01
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_LIGHT60), 252, 213, 180);//02
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_LIGHT40), 250, 191, 143);//03
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_ACCENT6), 247, 150,	40); //04
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_DARK25),  226, 107, 10);  //05
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_DARK50), 151, 71, 6); //06
		wb.setColourRGB(Colour.getInternalColour(JXL_GRAY), 216, 216, 216);
		wb.setColourRGB(Colour.getInternalColour(JXL_RED), 255, 0, 0);
		WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD);
		headerFont.setColour(Colour.getInternalColour(JXL_BLUE));
	    WritableCellFormat headerCellFormat = new WritableCellFormat(headerFont);
	    headerCellFormat.setAlignment(Alignment.CENTRE);
	    headerCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	    styles.put(HEADER, headerCellFormat);
	    WritableFont menuFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
	    WritableCellFormat menuCellFormat = new WritableCellFormat(menuFont);
	    menuFont.setColour(Colour.getInternalColour(JXL_WHITE));
	    menuCellFormat.setAlignment(Alignment.CENTRE);
	    menuCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	    menuCellFormat.setWrap(true);
	    menuCellFormat.setBorder(Border.ALL, BorderLineStyle.MEDIUM,Colour.getInternalColour(JXL_BLACK));
	    menuCellFormat.setBackground(Colour.getInternalColour(JXL_BLUE));
	    styles.put(MENU, menuCellFormat);
	    
	    WritableFont menuFontGray = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
	    WritableCellFormat menuGrayCellFormat = new WritableCellFormat(menuFontGray);
	    menuFontGray.setColour(Colour.getInternalColour(JXL_GRAY));
	    menuGrayCellFormat.setAlignment(Alignment.CENTRE);
	    menuGrayCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	    menuGrayCellFormat.setWrap(true);
	    menuGrayCellFormat.setBorder(Border.ALL, BorderLineStyle.MEDIUM,Colour.getInternalColour(JXL_BLACK));
	    menuGrayCellFormat.setBackground(Colour.getInternalColour(JXL_GRAY));
	    styles.put(MENU_GRAY, menuGrayCellFormat);
	    
	    
	    WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
	    WritableCellFormat boldNoBorderCellFormat = new WritableCellFormat(boldFont);
	    styles.put(BOLD, boldNoBorderCellFormat);
	    
	    WritableCellFormat boldCenterNoBorderCellFormat = new WritableCellFormat(boldFont);
	    boldCenterNoBorderCellFormat.setAlignment(Alignment.CENTRE);
	    styles.put(BOLD_CENTER, boldCenterNoBorderCellFormat);
	    
	   
	    
	    WritableFont normalFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
	    WritableCellFormat normalCellFormat = new WritableCellFormat(normalFont);
	    styles.put(NORMAL, normalCellFormat);
	    
	    WritableCellFormat normalCenterNoBorderCellFormat = new WritableCellFormat(normalFont);
	    normalCenterNoBorderCellFormat.setAlignment(Alignment.CENTRE);
	    styles.put(NORMAL_CENTER, normalCenterNoBorderCellFormat);
	    
	    WritableFont detailNormalFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
	    WritableCellFormat detailNormalCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    styles.put(DETAIL_NORMAL, detailNormalCellFormat);
	    
	    WritableCellFormat detailNormalDottedCenterCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalDottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER, detailNormalDottedCenterCellFormat);
	    
	    WritableCellFormat detailNormalDottedLeftCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalDottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    styles.put(DETAIL_NORMAL_DOTTED_LEFT, detailNormalDottedLeftCellFormat);
	    
	    NumberFormat currencyFormat = new NumberFormat("###,##0"); 
	    WritableCellFormat detailNormalDottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailNormalDottedRightCellFormat.setFont(detailNormalFont);
	    detailNormalDottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    styles.put(DETAIL_NORMAL_DOTTED_RIGHT, detailNormalDottedRightCellFormat);
	    
	    WritableCellFormat detailNormalCenterCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    detailNormalCenterCellFormat.setAlignment(Alignment.CENTRE);
	    styles.put(DETAIL_NORMAL_CENTER, detailNormalCenterCellFormat);
	    
	    WritableCellFormat detailNormalLeftCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    detailNormalLeftCellFormat.setAlignment(Alignment.LEFT);
	    styles.put(DETAIL_NORMAL_LEFT, detailNormalLeftCellFormat);
	    
	    WritableCellFormat detailNormalRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailNormalRightCellFormat.setFont(detailNormalFont);
	    detailNormalRightCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    detailNormalRightCellFormat.setAlignment(Alignment.RIGHT);
	    styles.put(DETAIL_NORMAL_RIGHT, detailNormalRightCellFormat);
	    

	    WritableFont detailBoldFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
	    WritableCellFormat detailOrangeAccent6DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeAccent6DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeAccent6DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeAccent6DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_ACCENT6));
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_LEFT, detailOrangeAccent6DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeLight80DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeLight80DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_LEFT, detailOrangeLight80DottedLeftCellFormat);
	    
	    
	    WritableFont detailBoldFontRed = new WritableFont(WritableFont.ARIAL);
	    detailBoldFontRed.setColour(Colour.getInternalColour(JXL_RED));
	    detailBoldFontRed.setPointSize(10);
	    detailBoldFontRed.setBoldStyle(WritableFont.BOLD);
	    WritableCellFormat detailOrangeLight80DottedLeftCellFormatRed = new WritableCellFormat(detailBoldFontRed);
	    detailOrangeLight80DottedLeftCellFormatRed.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedLeftCellFormatRed.setAlignment(Alignment.LEFT);
	    detailOrangeLight80DottedLeftCellFormatRed.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_LEFT_RED, detailOrangeLight80DottedLeftCellFormatRed);
	    
	    WritableCellFormat detailOrangeLight60DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeLight60DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight60DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeLight60DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT60));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_LEFT, detailOrangeLight60DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeLight40DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeLight40DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight40DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeLight40DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT40));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_LEFT, detailOrangeLight40DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeDark25DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeDark25DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark25DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeDark25DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK25));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_LEFT, detailOrangeDark25DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeDark50DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeDark50DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark50DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeDark50DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK50));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_LEFT, detailOrangeDark50DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeAccent6DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeAccent6DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeAccent6DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeAccent6DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeAccent6DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_ACCENT6));
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_RIGHT, detailOrangeAccent6DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight80DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight80DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeLight80DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT, detailOrangeLight80DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedRightCellFormatRed = new WritableCellFormat(currencyFormat);
	    detailOrangeLight80DottedRightCellFormatRed.setFont(detailBoldFontRed);
	    detailOrangeLight80DottedRightCellFormatRed.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedRightCellFormatRed.setAlignment(Alignment.RIGHT);
	    detailOrangeLight80DottedRightCellFormatRed.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT_RED, detailOrangeLight80DottedRightCellFormatRed);
	    
	    WritableCellFormat detailOrangeLight60DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight60DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight60DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight60DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeLight60DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT60));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT, detailOrangeLight60DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeLight40DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight40DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight40DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight40DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeLight40DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT40));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_RIGHT, detailOrangeLight40DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeDark25DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeDark25DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeDark25DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark25DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeDark25DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK25));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_RIGHT, detailOrangeDark25DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeDark50DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeDark50DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeDark50DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark50DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeDark50DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK50));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_RIGHT, detailOrangeDark50DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeAccent6DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeAccent6DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeAccent6DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeAccent6DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeAccent6DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_ACCENT6));
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_CENTER, detailOrangeAccent6DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight80DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight80DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeLight80DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_CENTER, detailOrangeLight80DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedCenterCellFormatRed = new WritableCellFormat(currencyFormat);
	    detailOrangeLight80DottedCenterCellFormatRed.setFont(detailBoldFontRed);
	    detailOrangeLight80DottedCenterCellFormatRed.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedCenterCellFormatRed.setAlignment(Alignment.CENTRE);
	    detailOrangeLight80DottedCenterCellFormatRed.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_CENTER_RED, detailOrangeLight80DottedCenterCellFormatRed);
	    
	    WritableCellFormat detailOrangeLight60DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight60DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight60DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight60DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeLight60DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT60));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_CENTER, detailOrangeLight60DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeLight40DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight40DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight40DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight40DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeLight40DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT40));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_CENTER, detailOrangeLight40DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeDark25DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeDark25DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeDark25DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark25DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeDark25DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK25));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_CENTER, detailOrangeDark25DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeDark50DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeDark50DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeDark50DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark50DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeDark50DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK50));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_CENTER, detailOrangeDark50DottedCenterCellFormat);
	    //Currency
//	    WritableFont curFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
//	    WritableCellFormat curCellFormat = new WritableCellFormat(currencyFormat);
//	    curCellFormat.setFont(curFont);
//	    detailNormalCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
//	    styles.put(DETAIL_NORMAL, detailNormalCellFormat);
//	    
//	    WritableCellFormat detailNormalDottedCenterCellFormat = new WritableCellFormat(detailNormalFont);
//	    detailNormalDottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.DOTTED);
//	    detailNormalDottedCenterCellFormat.setAlignment(Alignment.CENTRE);
//	    styles.put(DETAIL_NORMAL_DOTTED_CENTER, detailNormalDottedCenterCellFormat);
//	    
//	    WritableCellFormat detailNormalDottedLeftCellFormat = new WritableCellFormat(detailNormalFont);
//	    detailNormalDottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.DOTTED);
//	    detailNormalDottedLeftCellFormat.setAlignment(Alignment.LEFT);
//	    styles.put(DETAIL_NORMAL_DOTTED_LEFT, detailNormalDottedLeftCellFormat);
//	    
//	    WritableCellFormat detailNormalDottedRightCellFormat = new WritableCellFormat(detailNormalFont);
//	    detailNormalDottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.DOTTED);
//	    detailNormalDottedRightCellFormat.setAlignment(Alignment.RIGHT);
//	    styles.put(DETAIL_NORMAL_DOTTED_RIGHT, detailNormalDottedRightCellFormat);
	    
	    
	    
		return styles;
	}
	
	
	
	
	
	public static Map<String, XSSFCellStyle> createStylesPOI(Workbook wb) throws WriteException {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		XSSFDataFormat fmt = (XSSFDataFormat) wb.createDataFormat();
		
		//Init Font
	    XSSFFont headerFont = (XSSFFont) wb.createFont();
	    XSSFFont detailFont = (XSSFFont) wb.createFont();
	    XSSFFont detailRedFont = (XSSFFont) wb.createFont();
	    XSSFFont menuFont = (XSSFFont) wb.createFont();
	    XSSFFont menuFontGray = (XSSFFont) wb.createFont();
	    XSSFFont boldFont = (XSSFFont) wb.createFont();
	    XSSFFont normalFont = (XSSFFont) wb.createFont();
	    XSSFFont detailNormalFont = (XSSFFont) wb.createFont();
	    XSSFFont detailNormalRedFont = (XSSFFont) wb.createFont();
	    XSSFFont detailBoldFont = (XSSFFont) wb.createFont();
	    XSSFFont detailBoldRedFont = (XSSFFont) wb.createFont();
	    
	    
	    //set font
	    setFontPOI(headerFont,"Arial",20,true,poiBlue);
	    setFontPOI(detailFont,"Arial",9,false,poiBlack);
	    setFontPOI(detailRedFont,"Arial",9,false,poiRed);
	    setFontPOI(menuFont,"Arial",9,true,poiWhite);
	    setFontPOI(menuFontGray,"Arial",9,true,poiBlack);
	    setFontPOI(boldFont,"Arial",10,true,poiBlack);
	    setFontPOI(normalFont,"Arial",9,false,poiBlack);
	    setFontPOI(detailBoldFont,"Arial",9,true,poiBlack);
	    setFontPOI(detailBoldRedFont,"Arial",9,true,poiRed);
	    setFontPOI(detailNormalFont,"Arial",9,false,poiBlack);
	    setFontPOI(detailNormalRedFont,"Arial",9,false,poiRed);
	    
	    
	    //Set Cell Styles
	    XSSFCellStyle headerCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    headerCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    headerCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    headerCellFormat.setFont(headerFont);
	    styles.put(HEADER, headerCellFormat);
	    
	    XSSFCellStyle menuCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    menuCellFormat.setFillForegroundColor(poiBlue);
	    menuCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    //menuCellFormat.setWrapText(true);
	    menuCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    menuCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    setBorderForCell(menuCellFormat,BorderStyle.MEDIUM,poiBlack);
	    menuCellFormat.setFont(menuFont);
	    styles.put(MENU, menuCellFormat);
	    
	    XSSFCellStyle menuGrayCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    menuGrayCellFormat.setFillForegroundColor(poiGray);
	    menuGrayCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    //menuGrayCellFormat.setWrapText(true);
	    menuGrayCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    menuGrayCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    setBorderForCell(menuGrayCellFormat,BorderStyle.MEDIUM,poiBlack);
	    menuGrayCellFormat.setFont(menuFontGray);
	    styles.put(MENU_GRAY, menuGrayCellFormat);
	    
	    XSSFCellStyle menuRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    menuRedCellFormat.setFillForegroundColor(poiRed);
	    menuRedCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    //menuRedCellFormat.setWrapText(true);
	    menuRedCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    menuRedCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    setBorderForCell(menuRedCellFormat,BorderStyle.MEDIUM,poiBlack);
	    menuRedCellFormat.setFont(menuFont);
	    styles.put(MENU_RED, menuRedCellFormat);
	    
	    XSSFCellStyle boldNoBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    boldNoBorderCellFormat.setFont(boldFont);
	    styles.put(BOLD, boldNoBorderCellFormat);
	    
	    XSSFCellStyle boldCenterNoBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    boldCenterNoBorderCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    boldCenterNoBorderCellFormat.setFont(boldFont);
	    styles.put(BOLD_CENTER, boldCenterNoBorderCellFormat);
	    
	    XSSFCellStyle normalCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalCellFormat.setFont(normalFont);
	    styles.put(NORMAL, normalCellFormat);
	    
	    XSSFCellStyle normalCenterNoBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalCenterNoBorderCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    normalCenterNoBorderCellFormat.setFont(normalFont);
	    styles.put(NORMAL_CENTER, normalCenterNoBorderCellFormat);
	    
	    XSSFCellStyle normalMediumTopBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalMediumTopBorderCellFormat.setFont(normalFont);
	    normalMediumTopBorderCellFormat.setBorderTop(BorderStyle.MEDIUM);
	    normalMediumTopBorderCellFormat.setBorderColor(BorderSide.TOP, poiBlack);
	    styles.put(NORMAL_MEDIUM_TOP_BORDER, normalMediumTopBorderCellFormat);
	    
	    XSSFCellStyle normalMediumLeftBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalMediumLeftBorderCellFormat.setFont(normalFont);
	    normalMediumLeftBorderCellFormat.setBorderTop(BorderStyle.MEDIUM);
	    normalMediumLeftBorderCellFormat.setBorderColor(BorderSide.LEFT, poiBlack);
	    styles.put(NORMAL_MEDIUM_LEFT_BORDER, normalMediumLeftBorderCellFormat);
	    
	    XSSFCellStyle normalMediumRightBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalMediumRightBorderCellFormat.setFont(normalFont);
	    normalMediumRightBorderCellFormat.setBorderTop(BorderStyle.MEDIUM);
	    normalMediumRightBorderCellFormat.setBorderColor(BorderSide.RIGHT, poiBlack);
	    styles.put(NORMAL_MEDIUM_RIGHT_BORDER, normalMediumRightBorderCellFormat);
	    
	    XSSFCellStyle normalMediumBottomBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalMediumBottomBorderCellFormat.setFont(normalFont);
	    normalMediumBottomBorderCellFormat.setBorderTop(BorderStyle.MEDIUM);
	    normalMediumBottomBorderCellFormat.setBorderColor(BorderSide.BOTTOM, poiBlack);
	    styles.put(NORMAL_MEDIUM_BOTTOM_BORDER, normalMediumRightBorderCellFormat);
	    
	    XSSFCellStyle detailNormalCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalCellFormat.setFont(detailNormalFont);
	    styles.put(DETAIL_NORMAL, detailNormalCellFormat);
	    
	    XSSFCellStyle detailNormalDottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedCenterCellFormat.setFont(detailNormalFont);
	    detailNormalDottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER, detailNormalDottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalDottedCenterRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedCenterRedCellFormat, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedCenterRedCellFormat.setFont(detailNormalRedFont);
	    detailNormalDottedCenterRedCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER_RED, detailNormalDottedCenterRedCellFormat);
	    
	    XSSFCellStyle detailNormalDottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailNormalDottedLeftCellFormat.setFont(detailNormalFont);
	    styles.put(DETAIL_NORMAL_DOTTED_LEFT, detailNormalDottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalDottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
//	    detailNormalDottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0.00;-* #,##0.00;_-* \"\"??;_-@_-"));
	    detailNormalDottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    detailNormalDottedRightCellFormat.setFont(detailNormalFont);
	    styles.put(DETAIL_NORMAL_DOTTED_RIGHT, detailNormalDottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalCenterCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailNormalCenterCellFormat.setFont(detailNormalFont);
	    styles.put(DETAIL_NORMAL_CENTER, detailNormalCenterCellFormat);
	    
	    XSSFCellStyle detailNormalLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalLeftCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailNormalLeftCellFormat.setFont(detailNormalFont);
	    styles.put(DETAIL_NORMAL_LEFT, detailNormalLeftCellFormat);
	    
	    XSSFCellStyle detailNormalRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalRightCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailNormalRightCellFormat.setFont(detailNormalFont);
	    detailNormalRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_NORMAL_RIGHT, detailNormalRightCellFormat);
	    
	    XSSFCellStyle detailOrangeAccent6DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeAccent6DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeAccent6DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeAccent6DottedLeftCellFormat.setFillForegroundColor(poiOrange_Accent6);
	    detailOrangeAccent6DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeAccent6DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_LEFT, detailOrangeAccent6DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeLight80DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight80DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight80DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeLight80DottedLeftCellFormat.setFillForegroundColor(poiOrange_light80);
	    detailOrangeLight80DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight80DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_LEFT, detailOrangeLight80DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeLight60DottedLeftCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_LEFT, detailOrangeLight60DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedLeftRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedLeftRedCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedLeftRedCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeLight60DottedLeftRedCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedLeftRedCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedLeftRedCellFormat.setFont(detailBoldRedFont);
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_LEFT_RED, detailOrangeLight60DottedLeftRedCellFormat);
	    
	    XSSFCellStyle detailOrangeLight40DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight40DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight40DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeLight40DottedLeftCellFormat.setFillForegroundColor(poiOrange_light40);
	    detailOrangeLight40DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight40DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_LEFT, detailOrangeLight40DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeDark25DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark25DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark25DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeDark25DottedLeftCellFormat.setFillForegroundColor(poiOrange_dark25);
	    detailOrangeDark25DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark25DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_LEFT, detailOrangeDark25DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeDark50DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark50DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark50DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeDark50DottedLeftCellFormat.setFillForegroundColor(poiOrange_dark50);
	    detailOrangeDark50DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark50DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_LEFT, detailOrangeDark50DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeLight80DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight80DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight80DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeLight80DottedRightCellFormat.setFillForegroundColor(poiOrange_light80);
	    detailOrangeLight80DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight80DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight80DottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT, detailOrangeLight80DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeLight60DottedRightCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight60DottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT, detailOrangeLight60DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedRightRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedRightRedCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedRightRedCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeLight60DottedRightRedCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedRightRedCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedRightRedCellFormat.setFont(detailBoldRedFont);
	    detailOrangeLight60DottedRightRedCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT_RED, detailOrangeLight60DottedRightRedCellFormat);
	    
	    XSSFCellStyle detailOrangeLight40DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight40DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight40DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeLight40DottedRightCellFormat.setFillForegroundColor(poiOrange_light40);
	    detailOrangeLight40DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight40DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight40DottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_RIGHT, detailOrangeLight40DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeDark25DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark25DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark25DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeDark25DottedRightCellFormat.setFillForegroundColor(poiOrange_dark25);
	    detailOrangeDark25DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark25DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeDark25DottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_RIGHT, detailOrangeDark25DottedRightCellFormat);
	    
	    
	    XSSFCellStyle detailOrangeDark50DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark50DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark50DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeDark50DottedRightCellFormat.setFillForegroundColor(poiOrange_dark50);
	    detailOrangeDark50DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark50DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeDark50DottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_RIGHT, detailOrangeDark50DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeLight80DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight80DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight80DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeLight80DottedCenterCellFormat.setFillForegroundColor(poiOrange_light80);
	    detailOrangeLight80DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight80DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight80DottedCenterCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_CENTER, detailOrangeLight80DottedCenterCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeLight60DottedCenterCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight60DottedCenterCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_CENTER, detailOrangeLight60DottedCenterCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedCenterRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedCenterRedCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedCenterRedCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeLight60DottedCenterRedCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedCenterRedCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedCenterRedCellFormat.setFont(detailBoldRedFont);
	    detailOrangeLight60DottedCenterRedCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_CENTER_RED, detailOrangeLight60DottedCenterRedCellFormat);
	    
	    XSSFCellStyle detailOrangeLight40DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight40DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight40DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeLight40DottedCenterCellFormat.setFillForegroundColor(poiOrange_light40);
	    detailOrangeLight40DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight40DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight40DottedCenterCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_CENTER, detailOrangeLight40DottedCenterCellFormat);
	    
	    XSSFCellStyle detailOrangeDark25DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark25DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark25DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeDark25DottedCenterCellFormat.setFillForegroundColor(poiOrange_dark25);
	    detailOrangeDark25DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark25DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeDark25DottedCenterCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_CENTER, detailOrangeDark25DottedCenterCellFormat);
	    
	    
	    XSSFCellStyle detailOrangeDark50DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark50DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark50DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeDark50DottedCenterCellFormat.setFillForegroundColor(poiOrange_dark50);
	    detailOrangeDark50DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark50DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeDark50DottedCenterCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_CENTER, detailOrangeDark50DottedCenterCellFormat);
	    
//	    XSSFCellStyle detailOrangeLight60DottedCenterRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
//	    detailOrangeLight60DottedCenterRedCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT_RED));
//	    detailOrangeLight60DottedCenterRedCellFormat.setAlignment(HorizontalAlignment.CENTER);
//	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_CENTER_RED, detailOrangeLight60DottedCenterRedCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeAccent6DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeAccent6DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_ACCENT6_DOTTED_LEFT));
	    detailNormalOrangeAccent6DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_LEFT, detailNormalOrangeAccent6DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight80DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight80DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT80_DOTTED_LEFT));
	    detailNormalOrangeLight80DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_LEFT, detailNormalOrangeLight80DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight60DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight60DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT60_DOTTED_LEFT));
	    detailNormalOrangeLight60DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_LEFT, detailNormalOrangeLight60DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight40DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight40DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT40_DOTTED_LEFT));
	    detailNormalOrangeLight40DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_LEFT, detailNormalOrangeLight40DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark25DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark25DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_DARK25_DOTTED_LEFT));
	    detailNormalOrangeDark25DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK25_DOTTED_LEFT, detailNormalOrangeDark25DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark50DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark50DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_DARK50_DOTTED_LEFT));
	    detailNormalOrangeDark50DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK50_DOTTED_LEFT, detailNormalOrangeDark50DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeAccent6DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeAccent6DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_LEFT));
	    detailNormalOrangeAccent6DottedRightCellFormat.setAlignment(HorizontalAlignment.RIGHT);
	    styles.put(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_RIGHT, detailNormalOrangeAccent6DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight80DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight80DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT));
	    detailNormalOrangeLight80DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_RIGHT, detailNormalOrangeLight80DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight60DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight60DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT));
	    detailNormalOrangeLight60DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_RIGHT, detailNormalOrangeLight60DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight40DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight40DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT40_DOTTED_RIGHT));
	    detailNormalOrangeLight40DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_RIGHT, detailNormalOrangeLight40DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark25DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark25DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_DARK25_DOTTED_RIGHT));
	    detailNormalOrangeDark25DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK25_DOTTED_RIGHT, detailNormalOrangeDark25DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark50DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark50DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_DARK50_DOTTED_RIGHT));
	    detailNormalOrangeDark50DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK50_DOTTED_RIGHT, detailNormalOrangeDark50DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeAccent6DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeAccent6DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_LEFT));
	    detailNormalOrangeAccent6DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_CENTER, detailNormalOrangeAccent6DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight80DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight80DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_LEFT));
	    detailNormalOrangeLight80DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_CENTER, detailNormalOrangeLight80DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight60DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight60DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_LEFT));
	    detailNormalOrangeLight60DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_CENTER, detailNormalOrangeLight60DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight40DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight40DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_LEFT));
	    detailNormalOrangeLight40DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_CENTER, detailNormalOrangeLight40DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark25DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark25DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_DARK25_DOTTED_LEFT));
	    detailNormalOrangeDark25DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK25_DOTTED_CENTER, detailNormalOrangeDark25DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark50DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark50DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_DARK50_DOTTED_LEFT));
	    detailNormalOrangeDark50DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK50_DOTTED_CENTER, detailNormalOrangeDark50DottedCenterCellFormat);
	    
	    
		return styles;
	}
	
	
	
	//params: border's Color tren, trai, duoi, phai cua cell	
//	public static XSSFCellStyle setBorderForCell(XSSFCellStyle cellStyle, BorderStyle borderStyle,XSSFColor borderColor,XSSFColor... params){
//		BorderStyle b = borderStyle == null?BorderStyle.THIN:borderStyle;
//		cellStyle.setBorderLeft(b);
//		cellStyle.setBorderTop(b);
//		cellStyle.setBorderRight(b);
//		cellStyle.setBorderBottom(b);
//		//set color
//		cellStyle.setBorderColor(BorderSide.TOP, borderColor);
//		cellStyle.setBorderColor(BorderSide.LEFT, borderColor);
//		cellStyle.setBorderColor(BorderSide.BOTTOM, borderColor);
//		cellStyle.setBorderColor(BorderSide.RIGHT, borderColor);
//		if(params.length <= 4){
//			int i = 0;
//			for(XSSFColor sideColor:params){
//				BorderSide bSide = null;
//				switch (i++){
//					case (0): bSide = BorderSide.TOP; break;
//					case (1): bSide = BorderSide.LEFT; break;
//					case (2): bSide = BorderSide.BOTTOM; break;
//					case (3): bSide = BorderSide.RIGHT; break;
//					default:bSide = BorderSide.TOP;
//				};
//				cellStyle.setBorderColor(bSide, sideColor);
//			}
//		}
//		return cellStyle;
//	}
	
	*//**
	 * Set Border Style for a CellStyle
	 * Dat Border Style cho mot CellStyle
	 * @author Datnt43
	 * @since 09/01/2014
	 * @param cellStyle
	 * @param borderStyle : border style cho tat ca cac canh cua cell (neu chi set cho mot so canh thi de null)
	 * @param borderColor: mau canh
	 * @param params: border's Style tren, trai, duoi, phai cua cell	
	 *//*
	public static CellStyle setBorderForCell(CellStyle cellStyle, BorderStyle borderStyle,XSSFColor borderColor,BorderStyle... params){
//		BorderStyle a = borderStyle == null?BorderStyle.THIN:borderStyle;
		Short b = borderStyle == null?-1:Short.valueOf(String.valueOf(borderStyle.ordinal()));
		Short color = borderColor == null?-1:borderColor.getIndexed();
		
		if(b != -1){
			cellStyle.setBorderLeft(b);
			cellStyle.setBorderTop(b);
			cellStyle.setBorderRight(b);
			cellStyle.setBorderBottom(b);
		}
		//set color
		if(color != -1){
			cellStyle.setTopBorderColor(color);
			cellStyle.setBottomBorderColor(color);
			cellStyle.setLeftBorderColor(color);
			cellStyle.setRightBorderColor(color);
		}
		if(params.length <= 4){
			int i = 0;
			for(BorderStyle sideBorder:params){
				Short c = sideBorder == null?-1:Short.valueOf(String.valueOf(sideBorder.ordinal()));
				switch (i++){
					case (0): if(c!=-1){cellStyle.setBorderTop(c);};
							break;
					case (1): if(c!=-1){cellStyle.setBorderLeft(c);};
							break;
					case (2): if(c!=-1){cellStyle.setBorderBottom(c);};
							break;
					case (3): if(c!=-1){cellStyle.setBorderRight(c);};
						break;
					default:	break;
				};
			}
		}
		return cellStyle;
	}
	
	public static XSSFFont setFontPOI(XSSFFont fontStyle,String fontName,Integer fontHeight,Boolean isBold,XSSFColor fontColor){
		String fName = fontName == null?"Arial":fontName;
		Integer fHeight = fontHeight == null?9:fontHeight;
		Boolean fIsBold = isBold == null?false:isBold;
		XSSFColor fColor = fontColor == null?new XSSFColor(new java.awt.Color(0, 0, 0)):fontColor;
		fontStyle.setBold(fIsBold);
		fontStyle.setFontName(fName);
		fontStyle.setFontHeight(fHeight);
		if(fColor.getRgb()[0] == 0 && fColor.getRgb()[1] == 0 && fColor.getRgb()[2] == 0){
			fontStyle.setColor(HSSFColor.WHITE.index);
		} else if(fColor.getRgb()[0] == -1 && fColor.getRgb()[1] == -1 && fColor.getRgb()[2] == -1){
			fontStyle.setColor(HSSFColor.BLACK.index);
		} else{
			fontStyle.setColor(fColor);
		}
		return fontStyle;
	}
	
		
	public static Map<String, XSSFCellStyle> createStyles(XSSFWorkbook wb) {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		XSSFDataFormat fmt = wb.createDataFormat();
		
		XSSFCellStyle titleStyle = wb.createCellStyle();
		XSSFFont titleFont = wb.createFont();
		titleFont.setBold(true);
		titleFont.setFontHeight(18);
		titleStyle.setFont(titleFont);				
		styles.put(STYLE_TITLE, titleStyle);
		
		XSSFCellStyle headerStyle = wb.createCellStyle();
		XSSFFont headerFont = wb.createFont();
		headerFont.setBold(true);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerStyle.setFont(headerFont);
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		setBorderForCell(headerStyle);		
		styles.put(STYLE_HEADER, headerStyle);

		XSSFCellStyle percentStyle = wb.createCellStyle();
		percentStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentStyle.setDataFormat(fmt.getFormat("0.0%"));
		setBorderForCell(percentStyle);
		styles.put(STYLE_PERCENT, percentStyle);
		
		
		XSSFCellStyle dateStyle = wb.createCellStyle();
		dateStyle.setAlignment(HorizontalAlignment.RIGHT);
		dateStyle.setDataFormat(fmt.getFormat("d/m/yy"));
		setBorderForCell(dateStyle);
		styles.put(STYLE_DATE, dateStyle);

		XSSFCellStyle currencyStyle = wb.createCellStyle();
		currencyStyle.setAlignment(HorizontalAlignment.RIGHT);
		currencyStyle.setDataFormat(fmt.getFormat("#,###"));
		setBorderForCell(currencyStyle);
		styles.put(STYLE_CURRENCY, currencyStyle);
		
		
		XSSFCellStyle accountingStyle = wb.createCellStyle();
		accountingStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingStyle.setDataFormat(fmt.getFormat("#,###0.00"));
		setBorderForCell(accountingStyle);
		styles.put(STYLE_ACCOUNTING, accountingStyle);
			
		
		
		XSSFCellStyle textLeftStyle = wb.createCellStyle();
		textLeftStyle.setAlignment(HorizontalAlignment.LEFT);
		textLeftStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textLeftStyle);
		styles.put(STYLE_TEXT_ALIGN_LEFT, textLeftStyle);
		
		XSSFCellStyle textCenterStyle = wb.createCellStyle();
		textCenterStyle.setAlignment(HorizontalAlignment.CENTER);
		textCenterStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textCenterStyle);
		styles.put(STYLE_TEXT_ALIGN_CENTER, textCenterStyle);
		
		XSSFCellStyle textRightStyle = wb.createCellStyle();
		textRightStyle.setAlignment(HorizontalAlignment.RIGHT);
		textRightStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textRightStyle);
		styles.put(STYLE_TEXT_ALIGN_RIGHT, textRightStyle);

		return styles;
	}
	private static void setBorderForCell(XSSFCellStyle style){
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderBottom(BorderStyle.THIN);
	}
}
*/

/**
 * trungtm6 merge source from AFC on 21/07/2015
 */
package ths.dms.web.utils.report.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;

import ths.dms.web.utils.StringUtil;

/**
 * The Class ExcelProcessUtils.
 *
 * @author hungtx
 * @since Aug 19, 2013
 */
public class ExcelProcessUtils {
	public final static String STYLE_TITLE = "title";
	public final static String STYLE_PERCENT = "percent";
	public final static String STYLE_PERCENT_B = "percent_b";
	public final static String STYLE_COEFF = "coeff";
	public final static String STYLE_CURRENCY = "currency";
	public final static String STYLE_CURRENCY_B = "b_currency";
	public final static String STYLE_BOLD_CURRENCY = "bold_currency";
	public final static String STYLE_BOLD_CURRENCY_ZERO = "bold_currency_zero";
	public final static String STYLE_CURRENCY_NONE_ZERO = "currency_none_zero";
	public final static String STYLE_DATE = "date";	
	public final static String STYLE_ACCOUNTING = "accounting";	
	public final static String STYLE_GREEN_CURRENCY = "green_currency";
	public final static String STYLE_YELLOW_CURRENCY = "yellow_currency";
	public final static String STYLE_GREY_CURRENCY = "grey_currency";
	public final static String STYLE_BLUE_CURRENCY = "blue_currency";
	public final static String STYLE_GREEN_CURRENCY_BOLD = "green_currency_bold";
	public final static String STYLE_GREEN_CURRENCY_BOLD_NORMAL = "green_currency_bold_normal";
	public final static String STYLE_YELLOW_CURRENCY_BOLD = "yellow_currency_bold";
	public final static String STYLE_GREY_CURRENCY_BOLD = "grey_currency_bold";
	public final static String STYLE_BLUE_CURRENCY_BOLD = "blue_currency_bold";
	public final static String STYLE_BLUE_CURRENCY_BOLD_NORMAL = "blue_currency_bold_normal";
	public final static String STYLE_BOLD_ACCOUNTING = "bold_currency";
	public final static String STYLE_ORANGE_CURRENCY = "orange_currency";
	public final static String STYLE_ORANGE_CURRENCY_BOLD = "orange_currency_bold";
	public final static String STYLE_ORANGE_CURRENCY_BOLD_NORMAL = "orange_currency_bold_normal";
	
	public final static String STYLE_TOTAL_CURRENCY = "total_currency";
	public final static String STYLE_HEADER = "header";
	public final static String STYLE_TEXT_DETAIL = "STYLE_TEXT_DETAIL";
	public final static String STYLE_HEADER_ORANGE = "header_orange";
	public final static String STYLE_HEADER_ORANGE_HO_2_3 = "header_orange_2_3";
	public final static String STYLE_HEADER_GREEN = "header_green";
	public final static String STYLE_HEADER_GREEN_HO_2_3 = "header_green_ho_2_3";
	public final static String STYLE_HEADER_RED_HO_2_3 = "header_red_ho_2_3";
	public final static String STYLE_HEADER_PINK = "header_pink";
	public final static String STYLE_HEADER_PINK_HO_2_3 = "header_pink_ho_2_3";
	public final static String STYLE_HEADER_PINK_Orange_2_3 = "header_orange_2_3";
	public final static String STYLE_HEADER_BLUE = "header_blue";
	public final static String STYLE_HEADER_BLUE_HO_2_3 = "header_blue_ho_2_3";
	public final static String STYLE_HEADER_BLUE1 = "header_blue1";
	public final static String STYLE_HEADER_DARK_ORANGE = "header_dark_orange";
	public final static String STYLE_DARK_ORANGE_CURRENCY_BOLD = "dark_orange_currency_bold";
	
	public final static String STYLE_HEADER_DARK_ORANGE_HO_2_3 = "header_dark_orange_ho_2_3";
	public final static String STYLE_HEADER_ORANGE_NO_BORDER = "header_orange_no_border";
	public final static String STYLE_HEADER_ORANGE_NO_BORDER_HO_2_3 = "header_orange_no_border_ho_2_3";
	public final static String STYLE_TEXT = "text";
	public final static String STYLE_BOLD_TEXT = "bold_text";
	public final static String STYLE_TEXT_ALIGN_LEFT = "text_left";
	public final static String STYLE_TEXT_BORDER_DASH_ALIGN_LEFT = "text_left_border_dash";
	public final static String STYLE_TEXT_BORDER_DASH_ALIGN_RIGHT = "text_right_border_dash";
	public final static String STYLE_PERCENT_BORDER_DASH_ALIGN_RIGHT = "percent_right_border_dash";
	public final static String STYLE_TEXT_BORDER_DASH_ALIGN_LEFT_BOLD = "text_left_border_dash_bold";
	public final static String STYLE_TEXT_BORDER_DASH_ALIGN_RIGHT_BOLD = "text_right_border_dash_bold";
	public final static String STYLE_PERCENT_BORDER_DASH_ALIGN_RIGHT_BOLD = "percent_right_border_dash_bold";
	public final static String STYLE_TEXT_ALIGN_BOLD_LEFT = "text_bold_left";
	public final static String STYLE_TEXT_ALIGN_CENTER = "text_center";
	public final static String STYLE_TEXT_ALIGN_BOLD_CENTER = "text_bold_center";
	public final static String STYLE_TEXT_ALIGN_RIGHT = "text_right";
	public final static String STYLE_TEXT_ALIGN_BOLD_RIGHT = "text_bold_right";
	public final static String STYLE_BOLD = "text_bold";
	public final static String STYLE_TEXT_ALIGN_BOLD_RIGHT_CURRENCY = "text_bold_rigth_currency";
	public final static String STYLE_TEXT_ALIGN_LEFT_NO_BORDER = "STYLE_TEXT_ALIGN_LEFT_NO_BORDER";
	public final static String STYLE_TEXT_ALIGN_LEFT_NO_BORDER_BOLD = "STYLE_TEXT_ALIGN_LEFT_NO_BORDER_BOLD";
	public final static String STYLE_TEXT_RED_LEFT = "STYLE_TEXT_RED_LEFT";
	
	//group color
	public final static String STYLE_TEXT_GROUP_LEVEL_1 = "style_text_group_level_1";
	public final static String STYLE_TEXT_GROUP_LEVEL_2 = "style_text_group_level_2";
	public final static String STYLE_TEXT_GROUP_LEVEL_3 = "style_text_group_level_3";
	public final static String STYLE_TEXT_GROUP_LEVEL_4 = "style_text_group_level_4";
	public final static String STYLE_TEXT_GROUP_LEVEL_5 = "style_text_group_level_5";
	
	public final static String STYLE_TEXT_NORMAL_GROUP_LEVEL_1 = "style_text_NORMAL_GROUP_level_1";
	public final static String STYLE_TEXT_NORMAL_GROUP_LEVEL_2 = "style_text_NORMAL_GROUP_level_2";
	public final static String STYLE_TEXT_NORMAL_GROUP_LEVEL_3 = "style_text_NORMAL_GROUP_level_3";
	public final static String STYLE_TEXT_NORMAL_GROUP_LEVEL_4 = "style_text_NORMAL_GROUP_level_4";
	public final static String STYLE_TEXT_NORMAL_GROUP_LEVEL_5 = "style_text_NORMAL_GROUP_level_5";
	
	public final static String STYLE_TEXT_CENTER_GROUP_LEVEL_1 = "style_text_center_group_level_1";
	public final static String STYLE_TEXT_CENTER_GROUP_LEVEL_2 = "style_text_center_group_level_2";
	public final static String STYLE_TEXT_CENTER_GROUP_LEVEL_3 = "style_text_center_group_level_3";
	public final static String STYLE_TEXT_CENTER_GROUP_LEVEL_4 = "style_text_center_group_level_4";
	public final static String STYLE_TEXT_CENTER_GROUP_LEVEL_5 = "style_text_center_group_level_5";
	
	public final static String STYLE_CURRENCY_GROUP_LEVEL_1 = "style_currency_group_level_1";
	public final static String STYLE_CURRENCY_GROUP_LEVEL_2 = "style_currency_group_level_2";
	public final static String STYLE_CURRENCY_GROUP_LEVEL_3 = "style_currency_group_level_3";
	public final static String STYLE_CURRENCY_GROUP_LEVEL_4 = "style_currency_group_level_4";
	public final static String STYLE_CURRENCY_GROUP_LEVEL_5 = "style_currency_group_level_5";
	
	public final static String STYLE_CURRENCY_NORMAL_GROUP_LEVEL_1 = "style_currency_normal_group_level_1";
	public final static String STYLE_CURRENCY_NORMAL_GROUP_LEVEL_2 = "style_currency_normal_group_level_2";
	public final static String STYLE_CURRENCY_NORMAL_GROUP_LEVEL_3 = "style_currency_normal_group_level_3";
	public final static String STYLE_CURRENCY_NORMAL_GROUP_LEVEL_4 = "style_currency_normal_group_level_4";
	public final static String STYLE_CURRENCY_NORMAL_GROUP_LEVEL_5 = "style_currency_normal_group_level_5";
	
	public final static String STYLE_ACCOUNTING_GROUP_LEVEL_1 = "style_accounting_group_level_1";
	public final static String STYLE_ACCOUNTING_GROUP_LEVEL_2 = "style_accounting_group_level_2";
	public final static String STYLE_ACCOUNTING_GROUP_LEVEL_3 = "style_accounting_group_level_3";
	public final static String STYLE_ACCOUNTING_GROUP_LEVEL_4 = "style_accounting_group_level_4";
	public final static String STYLE_ACCOUNTING_GROUP_LEVEL_5 = "style_accounting_group_level_5";
	
	public final static String STYLE_PERCENT_GROUP_LEVEL_1 = "style_percent_group_level_1";
	public final static String STYLE_PERCENT_GROUP_LEVEL_2 = "style_percent_group_level_2";
	public final static String STYLE_PERCENT_GROUP_LEVEL_3 = "style_percent_group_level_3";
	public final static String STYLE_PERCENT_GROUP_LEVEL_4 = "style_percent_group_level_4";
	public final static String STYLE_PERCENT_GROUP_LEVEL_5 = "style_percent_group_level_5";
	
	public final static String STYLE_DETAIL_NORMAL_DOTTED_RIGHT = "detail_normal_dotted_right";
	
	/**
	 * Creates the styles.
	 *
	 * @param wb the wb
	 * @return the map
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public static Map<String, XSSFCellStyle> createStyles(XSSFWorkbook wb) {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		XSSFDataFormat fmt = wb.createDataFormat();
		XSSFCellStyle cellStyle = wb.createCellStyle();
		
		XSSFCellStyle titleStyle = wb.createCellStyle();
		XSSFFont titleFont = wb.createFont();
		titleFont.setBold(true);
		titleFont.setFontHeight(18);
		titleStyle.setFont(titleFont);	
		titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		styles.put(STYLE_TITLE, titleStyle);
		
		XSSFCellStyle titleSubStyle = wb.createCellStyle();
		XSSFFont titleSubFont = wb.createFont();
		titleSubFont.setBold(true);
		titleSubFont.setFontHeight(12);
		titleSubStyle.setFont(titleSubFont);				
		styles.put(STYLE_TEXT, titleSubStyle);
		
		XSSFCellStyle headerStyle = wb.createCellStyle();
		XSSFFont headerFont = wb.createFont();
		headerFont.setBold(true);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerStyle.setFont(headerFont);
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerStyle.setWrapText(true);
		setBorderForCell(headerStyle);		
		styles.put(STYLE_HEADER, headerStyle);
		
		XSSFCellStyle detailStyle = wb.createCellStyle();
		XSSFFont detailFont = wb.createFont();
		detailStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		detailStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		detailStyle.setFont(detailFont);
		detailStyle.setAlignment(HorizontalAlignment.CENTER);
		detailStyle.setWrapText(true);
		setBorderForCell(detailStyle);		
		styles.put(STYLE_TEXT_DETAIL, detailStyle);
		
		XSSFCellStyle headerOrangeStyle = wb.createCellStyle();
		XSSFFont headerOrangeFont = wb.createFont();
		headerOrangeFont.setBold(true);
		headerOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeStyle.setFont(headerOrangeFont);
		headerOrangeStyle.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerOrangeStyle.setWrapText(true);
		setBorderForCell(headerOrangeStyle);		
		styles.put(STYLE_HEADER_ORANGE, headerOrangeStyle);
		
		XSSFCellStyle headerOrangeHO2_3Style = wb.createCellStyle();
		XSSFFont headerOrangeHO2_3Font = wb.createFont();
		headerOrangeHO2_3Font.setBold(true);
		headerOrangeHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeHO2_3Style.setFont(headerOrangeHO2_3Font);
		headerOrangeHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerOrangeHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerOrangeHO2_3Style);		
		styles.put(STYLE_HEADER_ORANGE_HO_2_3, headerOrangeHO2_3Style);
		
		XSSFCellStyle headerOrangeNoBorderStyle = wb.createCellStyle();
		XSSFFont headerOrangeNoBorderFont = wb.createFont();
		headerOrangeFont.setBold(true);
		headerOrangeNoBorderStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeNoBorderStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeNoBorderStyle.setFont(headerOrangeNoBorderFont);
		headerOrangeNoBorderStyle.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeNoBorderStyle.setWrapText(true);
		styles.put(STYLE_HEADER_ORANGE_NO_BORDER, headerOrangeNoBorderStyle);
		
		
		XSSFCellStyle headerOrangeNoBorderHO2_3Style = wb.createCellStyle();
		XSSFFont headerOrangeNoBorderHO2_3Font = wb.createFont();
		headerOrangeNoBorderHO2_3Font.setBold(true);
		headerOrangeNoBorderHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeNoBorderHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeNoBorderHO2_3Style.setFont(headerOrangeNoBorderHO2_3Font);
		headerOrangeNoBorderHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeNoBorderHO2_3Style.setWrapText(true);
		setBorderForCellNote(headerOrangeNoBorderHO2_3Style);
		styles.put(STYLE_HEADER_ORANGE_NO_BORDER_HO_2_3, headerOrangeNoBorderHO2_3Style);
		
		XSSFCellStyle headerGreenStyle = wb.createCellStyle();
		XSSFFont headerGreenFont = wb.createFont();
		headerGreenFont.setBold(true);
		headerGreenStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 208, 80)));
		headerGreenStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerGreenStyle.setFont(headerGreenFont);
		headerGreenStyle.setAlignment(HorizontalAlignment.CENTER);
		headerGreenStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerGreenStyle.setWrapText(true);
		setBorderForCell(headerGreenStyle);		
		styles.put(STYLE_HEADER_GREEN, headerGreenStyle);
		
		XSSFCellStyle headerGreenHO2_3Style = wb.createCellStyle();
		XSSFFont headerGreenHO2_3StyleFont = wb.createFont();
		headerGreenHO2_3StyleFont.setBold(true);
		headerGreenHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 208, 80)));
		headerGreenHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerGreenHO2_3Style.setFont(headerGreenHO2_3StyleFont);
		headerGreenHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerGreenHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerGreenHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerGreenHO2_3Style);		
		styles.put(STYLE_HEADER_GREEN_HO_2_3, headerGreenHO2_3Style);
		
		XSSFCellStyle headerRedHO2_3Style = wb.createCellStyle();
		XSSFFont headerRedHO2_3StyleFont = wb.createFont();
		headerRedHO2_3StyleFont.setBold(true);
		headerRedHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
		headerRedHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerRedHO2_3Style.setFont(headerGreenHO2_3StyleFont);
		headerRedHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerRedHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerRedHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerRedHO2_3Style);		
		styles.put(STYLE_HEADER_RED_HO_2_3, headerRedHO2_3Style);
		
		
		XSSFCellStyle headerPinkStyle = wb.createCellStyle();
		XSSFFont headerPinkFont = wb.createFont();
		headerPinkFont.setBold(true);
		headerPinkStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(217, 151, 149)));
		headerPinkStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerPinkStyle.setFont(headerPinkFont);
		headerPinkStyle.setAlignment(HorizontalAlignment.CENTER);
		headerPinkStyle.setWrapText(true);
		setBorderForCell(headerPinkStyle);		
		styles.put(STYLE_HEADER_PINK, headerPinkStyle);
		
		
		XSSFCellStyle headerPinkHO2_3Style = wb.createCellStyle();
		XSSFFont headerPinkHO2_3Font = wb.createFont();
		headerPinkHO2_3Font.setBold(true);
		headerPinkHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(217, 151, 149)));
		headerPinkHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerPinkHO2_3Style.setFont(headerPinkHO2_3Font);
		headerPinkHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerPinkHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerPinkHO2_3Style);		
		styles.put(STYLE_HEADER_PINK_HO_2_3, headerPinkHO2_3Style);

		XSSFCellStyle headerBlueStyle = wb.createCellStyle();
		XSSFFont headerBlueFont = wb.createFont();
		headerBlueFont.setBold(true);
		headerBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(92, 180, 204)));
//		headerBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		headerBlueStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerBlueStyle.setFont(headerBlueFont);
		headerBlueStyle.setAlignment(HorizontalAlignment.CENTER);
		headerBlueStyle.setWrapText(true);
		setBorderForCell(headerBlueStyle);		
		styles.put(STYLE_HEADER_BLUE, headerBlueStyle);
		
		XSSFCellStyle headerBlueHO2_3Style = wb.createCellStyle();
		XSSFFont headerBlueHO2_3Font = wb.createFont();
		headerBlueHO2_3Font.setBold(true);
		headerBlueHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(49, 132, 155)));
		headerBlueHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerBlueHO2_3Style.setFont(headerBlueHO2_3Font);
		headerBlueHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerBlueHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerBlueHO2_3Style);		
		styles.put(STYLE_HEADER_BLUE_HO_2_3, headerBlueHO2_3Style);
		
		XSSFCellStyle headerDarkOrangeStyle = wb.createCellStyle();
		XSSFFont headerDarkOrangeFont = wb.createFont();
		headerDarkOrangeFont.setBold(true);
		headerDarkOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		headerDarkOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerDarkOrangeStyle.setFont(headerDarkOrangeFont);
		headerDarkOrangeStyle.setAlignment(HorizontalAlignment.CENTER);
		headerDarkOrangeStyle.setWrapText(true);
		setBorderForCell(headerDarkOrangeStyle);		
		styles.put(STYLE_HEADER_DARK_ORANGE, headerDarkOrangeStyle);
		
		XSSFCellStyle headerDarkOrangeHO2_3Style = wb.createCellStyle();
		XSSFFont headerDarkOrangeHO2_3Font = wb.createFont();
		headerDarkOrangeHO2_3Font.setBold(true);
		headerDarkOrangeHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		headerDarkOrangeHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerDarkOrangeHO2_3Style.setFont(headerDarkOrangeHO2_3Font);
		headerDarkOrangeHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerDarkOrangeHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerDarkOrangeHO2_3Style);		
		styles.put(STYLE_HEADER_DARK_ORANGE_HO_2_3, headerDarkOrangeHO2_3Style);
		
		
		XSSFCellStyle percentStyle = wb.createCellStyle();
		percentStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentStyle.setDataFormat(fmt.getFormat("0.0%"));
		setBorderForCell(percentStyle);
		styles.put(STYLE_PERCENT, percentStyle);
		
		XSSFCellStyle percentBoldStyle = wb.createCellStyle();
		percentBoldStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentBoldStyle.setDataFormat(fmt.getFormat("0.0%"));
		setBorderForCell(percentBoldStyle);
		styles.put(STYLE_PERCENT_B, percentBoldStyle);
		
		
		XSSFCellStyle dateStyle = wb.createCellStyle();
		dateStyle.setAlignment(HorizontalAlignment.RIGHT);
		dateStyle.setDataFormat(fmt.getFormat("d/m/yy"));
		setBorderForCell(dateStyle);
		styles.put(STYLE_DATE, dateStyle);

		XSSFCellStyle currencyStyle = wb.createCellStyle();
		currencyStyle.setAlignment(HorizontalAlignment.RIGHT);
		currencyStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(currencyStyle);
		styles.put(STYLE_CURRENCY, currencyStyle);
		
		
		XSSFCellStyle currencyBStyle = wb.createCellStyle();
		currencyBStyle.setAlignment(HorizontalAlignment.RIGHT);
		currencyBStyle.setDataFormat(fmt.getFormat("#,###0"));
		XSSFFont titleBFont = wb.createFont();
		titleBFont.setBold(true);
		currencyBStyle.setFont(titleBFont);
		setBorderForCell(currencyBStyle);
		styles.put(STYLE_CURRENCY_B, currencyBStyle);
		
		/**
		 * Vuongdl
		 */
		XSSFCellStyle currencyZeroBoldStyle = wb.createCellStyle();
		currencyZeroBoldStyle.setAlignment(HorizontalAlignment.RIGHT);
		currencyZeroBoldStyle.setDataFormat(fmt.getFormat("#,###0"));
		XSSFFont titleCurrencyZeroFont = wb.createFont();
		titleCurrencyZeroFont.setBold(true);
		currencyZeroBoldStyle.setFont(titleCurrencyZeroFont);
		setBorderThickForCell(currencyZeroBoldStyle);
		styles.put(STYLE_BOLD_CURRENCY_ZERO, currencyZeroBoldStyle);
				
		XSSFCellStyle accountingStyle = wb.createCellStyle();
		accountingStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingStyle.setDataFormat(fmt.getFormat("#,###0.00"));
		setBorderForCell(accountingStyle);
		styles.put(STYLE_ACCOUNTING, accountingStyle);
		
		XSSFCellStyle accountingGreenStyle = wb.createCellStyle();
		accountingGreenStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreenStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(139, 231, 150)));
		accountingGreenStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreenStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingGreenStyle);
		styles.put(STYLE_GREEN_CURRENCY, accountingGreenStyle);
		
		XSSFCellStyle accountingGreenStyleBold = wb.createCellStyle();
		accountingGreenStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreenStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(139, 231, 150)));
		accountingGreenStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreenStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, null,11, accountingGreenStyleBold);
		setBorderDashForCell(accountingGreenStyleBold);
		styles.put(STYLE_GREEN_CURRENCY_BOLD, accountingGreenStyleBold);
		
		XSSFCellStyle accountingYellowStyle = wb.createCellStyle();
		accountingYellowStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingYellowStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 153)));
		accountingYellowStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingYellowStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingYellowStyle);
		styles.put(STYLE_YELLOW_CURRENCY, accountingYellowStyle);
		
		XSSFCellStyle accountingYellowStyleBold = wb.createCellStyle();
		accountingYellowStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingYellowStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 153)));
		accountingYellowStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingYellowStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, null,11, accountingYellowStyleBold);
		setBorderDashForCell(accountingYellowStyleBold);
		styles.put(STYLE_YELLOW_CURRENCY_BOLD, accountingYellowStyleBold);
		
		XSSFCellStyle accountingGreyStyle = wb.createCellStyle();
		accountingGreyStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreyStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(227, 230, 182)));
		accountingGreyStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreyStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingGreyStyle);
		styles.put(STYLE_GREY_CURRENCY, accountingGreyStyle);
		
		XSSFCellStyle accountingGreyStyleBold = wb.createCellStyle();
		accountingGreyStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreyStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(227, 230, 182)));
		accountingGreyStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreyStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, null,11, accountingGreyStyleBold);
		setBorderDashForCell(accountingGreyStyleBold);
		styles.put(STYLE_GREY_CURRENCY_BOLD, accountingGreyStyleBold);
		
		XSSFCellStyle accountingBlueStyle = wb.createCellStyle();
		accountingBlueStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		accountingBlueStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingBlueStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingBlueStyle);
		styles.put(STYLE_BLUE_CURRENCY, accountingBlueStyle);
		
		XSSFCellStyle accountingBlueStyleBold = wb.createCellStyle();
		accountingBlueStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingBlueStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		accountingBlueStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingBlueStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, null,11, accountingBlueStyleBold);
		setBorderDashForCell(accountingBlueStyleBold);
		styles.put(STYLE_BLUE_CURRENCY_BOLD, accountingBlueStyleBold);
		
		XSSFCellStyle accountingOrangeStyle = wb.createCellStyle();
		accountingOrangeStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		accountingOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingOrangeStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingOrangeStyle);
		styles.put(STYLE_ORANGE_CURRENCY, accountingOrangeStyle);
		
		XSSFCellStyle accountingTotalStyle = wb.createCellStyle();
		accountingTotalStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingTotalStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(147, 205, 221)));
		accountingTotalStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		XSSFFont accountingTotalFont = wb.createFont();
		accountingTotalFont.setBold(true);
		accountingTotalStyle.setFont(accountingTotalFont);
		accountingTotalStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCellTotal(accountingTotalStyle);
		styles.put(STYLE_TOTAL_CURRENCY, accountingTotalStyle);
		
		
		XSSFCellStyle accountingBoldStyle = wb.createCellStyle();
		accountingBoldStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingBoldStyle.setDataFormat(fmt.getFormat("#,###0.00"));
		XSSFFont titleAccountingFont = wb.createFont();
		titleAccountingFont.setBold(true);
		accountingBoldStyle.setFont(titleAccountingFont);
		setBorderThickForCell(accountingBoldStyle);
		styles.put(STYLE_BOLD_ACCOUNTING, accountingBoldStyle);
			
		
		XSSFCellStyle textLeftStyle = wb.createCellStyle();
		textLeftStyle.setAlignment(HorizontalAlignment.LEFT);
		textLeftStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textLeftStyle);
		styles.put(STYLE_TEXT_ALIGN_LEFT, textLeftStyle);
		
		XSSFCellStyle textLeftBorderDashStyle = wb.createCellStyle();
		textLeftBorderDashStyle.setAlignment(HorizontalAlignment.LEFT);
		textLeftBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		setBorderDashForCell(textLeftBorderDashStyle);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_LEFT, textLeftBorderDashStyle);
		
		XSSFCellStyle textRightBorderDashStyle = wb.createCellStyle();
		textRightBorderDashStyle.setAlignment(HorizontalAlignment.RIGHT);
		textRightBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		setBorderDashForCell(textRightBorderDashStyle);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_RIGHT, textRightBorderDashStyle);
		
		XSSFCellStyle percentRightBorderDashStyle = wb.createCellStyle();
		percentRightBorderDashStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentRightBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		setBorderDashForCell(percentRightBorderDashStyle);
		percentRightBorderDashStyle.setDataFormat(fmt.getFormat("0.0%"));
		styles.put(STYLE_PERCENT_BORDER_DASH_ALIGN_RIGHT, percentRightBorderDashStyle);
		
		XSSFCellStyle textLeftBorderDashStyleBold = wb.createCellStyle();
		textLeftBorderDashStyleBold.setAlignment(HorizontalAlignment.LEFT);
		textLeftBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, null,11, textLeftBorderDashStyleBold);
		setBorderDashForCell(textLeftBorderDashStyleBold);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_LEFT_BOLD, textLeftBorderDashStyleBold);
		
		XSSFCellStyle textRightBorderDashStyleBold = wb.createCellStyle();
		textRightBorderDashStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		textRightBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, null,11, textRightBorderDashStyleBold);
		setBorderDashForCell(textRightBorderDashStyleBold);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_RIGHT_BOLD, textRightBorderDashStyleBold);
		
		XSSFCellStyle percentRightBorderDashStyleBold = wb.createCellStyle();
		percentRightBorderDashStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		percentRightBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, null,11, percentRightBorderDashStyleBold);
		setBorderDashForCell(percentRightBorderDashStyleBold);
		percentRightBorderDashStyleBold.setDataFormat(fmt.getFormat("0.0%"));
		styles.put(STYLE_PERCENT_BORDER_DASH_ALIGN_RIGHT_BOLD, percentRightBorderDashStyleBold);
		
		XSSFCellStyle textBoldLeftStyle = wb.createCellStyle();
		textBoldLeftStyle.setAlignment(HorizontalAlignment.LEFT);
		textBoldLeftStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignLeftFont = wb.createFont();
		titleAlignLeftFont.setBold(true);
		textBoldLeftStyle.setFont(titleAlignLeftFont);
//		setBorderThickForCell(textBoldLeftStyle);
		setBorderForCell(textBoldLeftStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_LEFT, textBoldLeftStyle);
		
		XSSFCellStyle textCenterStyle = wb.createCellStyle();
		textCenterStyle.setAlignment(HorizontalAlignment.CENTER);
		textCenterStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textCenterStyle);
		styles.put(STYLE_TEXT_ALIGN_CENTER, textCenterStyle);
		
		XSSFCellStyle textBoldCenterStyle = wb.createCellStyle();
		textBoldCenterStyle.setAlignment(HorizontalAlignment.CENTER);
		textBoldCenterStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignCenterFont = wb.createFont();
		titleAlignCenterFont.setBold(true);
		textBoldCenterStyle.setFont(titleAlignCenterFont);
//		setBorderThickForCell(textBoldCenterStyle);
		setBorderForCell(textBoldCenterStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_CENTER, textBoldCenterStyle);
		
		XSSFCellStyle textRightStyle = wb.createCellStyle();
		textRightStyle.setAlignment(HorizontalAlignment.RIGHT);
		textRightStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textRightStyle);
		styles.put(STYLE_TEXT_ALIGN_RIGHT, textRightStyle);
		
		XSSFCellStyle boldStyle = wb.createCellStyle();
		XSSFFont font = wb.createFont();
		font.setBold(true);
		//titleFont.setFontHeight(18);
		//titleStyle.setFont(titleFont);	
		boldStyle.setFont(font);
		setBorderForCell(boldStyle);
		styles.put(STYLE_BOLD, boldStyle);
		
		XSSFCellStyle textBoldRightStyle = wb.createCellStyle();
		textBoldRightStyle.setAlignment(HorizontalAlignment.RIGHT);
		textBoldRightStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignRightFont = wb.createFont();
		titleAlignRightFont.setBold(true);
		textBoldRightStyle.setFont(titleAlignRightFont);
		setBorderThickForCell(textBoldRightStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_RIGHT, textBoldRightStyle);
		
		//XSSFCellStyle textBoldRightStyle = wb.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.RIGHT);
		cellStyle.setDataFormat(fmt.getFormat("@"));
		//XSSFFont titleAlignRightFont = wb.createFont();
		titleAlignRightFont.setBold(true);
		cellStyle.setFont(titleAlignRightFont);
		cellStyle.setAlignment(HorizontalAlignment.RIGHT);
		cellStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(cellStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_RIGHT_CURRENCY, cellStyle);

		return styles;
	}
	
	/**
	 * @author tuannd20
	 * @param wb
	 * @param fontName
	 * @return style map
	 * @since 11/10/2013
	 */
	public static Map<String, XSSFCellStyle> createStylesWithFont(XSSFWorkbook wb, String fontName) {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		XSSFDataFormat fmt = wb.createDataFormat();
		XSSFCellStyle cellStyle = wb.createCellStyle();
		
		XSSFFont font = wb.createFont();
		font.setFontName(fontName);
		
		XSSFFont boldFont = wb.createFont();
		boldFont.setFontName(fontName);
		boldFont.setBold(true);
		
		XSSFCellStyle titleStyle = wb.createCellStyle();
		XSSFFont titleFont = wb.createFont();
		titleFont.setFontName(fontName);
		titleFont.setBold(true);
		titleFont.setFontHeight(18);
		titleStyle.setFont(titleFont);			
		titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		styles.put(STYLE_TITLE, titleStyle);
		
		XSSFCellStyle titleSubStyle = wb.createCellStyle();
		XSSFFont titleSubFont = wb.createFont();
		titleSubFont.setFontName(fontName);
		titleSubFont.setBold(true);
		titleSubFont.setFontHeight(12);
		titleSubStyle.setFont(titleSubFont);				
		styles.put(STYLE_TEXT, titleSubStyle);
		
		XSSFCellStyle headerStyle = wb.createCellStyle();
		XSSFFont headerFont = wb.createFont();
		headerFont.setFontName(fontName);
		headerFont.setBold(true);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerStyle.setFont(headerFont);
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerStyle.setWrapText(true);
		setBorderForCell(headerStyle);		
		styles.put(STYLE_HEADER, headerStyle);

		XSSFCellStyle percentStyle = wb.createCellStyle();
		XSSFFont percentStyleFont = wb.createFont();
		percentStyleFont.setFontName(fontName);
		percentStyle.setFont(percentStyleFont);
		percentStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentStyle.setDataFormat(fmt.getFormat("0.0%"));
		setBorderForCell(percentStyle);
		styles.put(STYLE_PERCENT, percentStyle);
		
		XSSFCellStyle percentBoldStyle = wb.createCellStyle();
		XSSFFont percentBoldStyleFont = wb.createFont();
		percentBoldStyleFont.setBold(true);
		percentBoldStyleFont.setFontName(fontName);
		percentBoldStyle.setFont(percentBoldStyleFont);
		percentBoldStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentBoldStyle.setDataFormat(fmt.getFormat("0.0%"));
		setBorderForCell(percentBoldStyle);
		styles.put(STYLE_PERCENT_B, percentBoldStyle);
		
		XSSFCellStyle dateStyle = wb.createCellStyle();
		XSSFFont dateStyleFont = wb.createFont();
		dateStyleFont.setFontName(fontName);
		dateStyle.setFont(dateStyleFont);
		dateStyle.setAlignment(HorizontalAlignment.RIGHT);
		dateStyle.setDataFormat(fmt.getFormat("d/m/yy"));
		setBorderForCell(dateStyle);
		styles.put(STYLE_DATE, dateStyle);

		XSSFCellStyle currencyStyle = wb.createCellStyle();
		XSSFFont currencyStyleFont = wb.createFont();
		currencyStyleFont.setFontName(fontName);
		currencyStyle.setFont(currencyStyleFont);
		currencyStyle.setAlignment(HorizontalAlignment.RIGHT);
		currencyStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(currencyStyle);
		styles.put(STYLE_CURRENCY, currencyStyle);
		
		XSSFCellStyle currencyBStyle = wb.createCellStyle();
		currencyBStyle.setAlignment(HorizontalAlignment.RIGHT);
		currencyBStyle.setDataFormat(fmt.getFormat("#,###0"));
		XSSFFont titleBFont = wb.createFont();
		titleBFont.setFontName(fontName);
		titleBFont.setBold(true);
		currencyBStyle.setFont(titleBFont);
		setBorderForCell(currencyBStyle);
		styles.put(STYLE_CURRENCY_B, currencyBStyle);
		
		XSSFCellStyle currencyStyleNoneZero = wb.createCellStyle();
		XSSFFont currencyStyleFontNoneZero = wb.createFont();
		currencyStyleNoneZero.setFont(currencyStyleFontNoneZero);
		currencyStyleNoneZero.setAlignment(HorizontalAlignment.RIGHT);
		currencyStyleNoneZero.setDataFormat(fmt.getFormat("#,###"));
		setBorderForCell(currencyStyleNoneZero);
		styles.put(STYLE_CURRENCY_NONE_ZERO, currencyStyleNoneZero);
		
		
		XSSFCellStyle accountingStyle = wb.createCellStyle();
		XSSFFont accountingStyleFont = wb.createFont();
		accountingStyleFont.setFontName(fontName);
		accountingStyle.setFont(accountingStyleFont);
		accountingStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingStyle.setDataFormat(fmt.getFormat("#,###0.00"));
		setBorderForCell(accountingStyle);
		styles.put(STYLE_ACCOUNTING, accountingStyle);
		
		XSSFCellStyle accountingBoldStyle = wb.createCellStyle();
		accountingBoldStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingBoldStyle.setDataFormat(fmt.getFormat("#,###0.00"));
		XSSFFont titleAccountingFont = wb.createFont();
		titleAccountingFont.setFontName(fontName);
		titleAccountingFont.setBold(true);
		accountingBoldStyle.setFont(titleAccountingFont);
		//setBorderThickForCell(accountingBoldStyle);
		setBorderForCell(accountingBoldStyle);
		styles.put(STYLE_BOLD_ACCOUNTING, accountingBoldStyle);
			
		
		XSSFCellStyle textLeftStyle = wb.createCellStyle();
		textLeftStyle.setAlignment(HorizontalAlignment.LEFT);
		textLeftStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont textLeftStyleFont = wb.createFont();
		textLeftStyleFont.setFontName(fontName);
		textLeftStyle.setFont(textLeftStyleFont);
		setBorderForCell(textLeftStyle);
		styles.put(STYLE_TEXT_ALIGN_LEFT, textLeftStyle);
		
		XSSFCellStyle textBoldLeftStyle = wb.createCellStyle();
		textBoldLeftStyle.setAlignment(HorizontalAlignment.LEFT);
		textBoldLeftStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignLeftFont = wb.createFont();
		titleAlignLeftFont.setFontName(fontName);
		titleAlignLeftFont.setBold(true);
		textBoldLeftStyle.setFont(titleAlignLeftFont);
		//setBorderThickForCell(textBoldLeftStyle);
		setBorderForCell(textBoldLeftStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_LEFT, textBoldLeftStyle);
		
		XSSFCellStyle textLeftNoBorder = wb.createCellStyle();
		textLeftNoBorder.setAlignment(HorizontalAlignment.LEFT);
		textLeftNoBorder.setDataFormat(fmt.getFormat("@"));
		textLeftNoBorder.setFont(font);
		styles.put(STYLE_TEXT_ALIGN_LEFT_NO_BORDER, textLeftNoBorder);
		
		XSSFCellStyle textBoldLeftNoBorder = wb.createCellStyle();
		textBoldLeftNoBorder.setAlignment(HorizontalAlignment.LEFT);
		textBoldLeftNoBorder.setDataFormat(fmt.getFormat("@"));
		textBoldLeftNoBorder.setFont(boldFont);
		styles.put(STYLE_TEXT_ALIGN_LEFT_NO_BORDER_BOLD, textBoldLeftNoBorder);
		
		XSSFCellStyle textCenterStyle = wb.createCellStyle();
		XSSFFont textCenterStyleFont = wb.createFont();
		textCenterStyleFont.setFontName(fontName);
		textCenterStyle.setFont(textCenterStyleFont);
		textCenterStyle.setAlignment(HorizontalAlignment.CENTER);
		textCenterStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textCenterStyle);
		styles.put(STYLE_TEXT_ALIGN_CENTER, textCenterStyle);
		
		XSSFCellStyle textBoldCenterStyle = wb.createCellStyle();
		textBoldCenterStyle.setAlignment(HorizontalAlignment.CENTER);
		textBoldCenterStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignCenterFont = wb.createFont();
		titleAlignCenterFont.setFontName(fontName);
		titleAlignCenterFont.setBold(true);
		textBoldCenterStyle.setFont(titleAlignCenterFont);
		//setBorderThickForCell(textBoldCenterStyle);
		setBorderForCell(textBoldCenterStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_CENTER, textBoldCenterStyle);
		
		XSSFCellStyle textRightStyle = wb.createCellStyle();
		XSSFFont textRightStyleFont = wb.createFont();
		textRightStyleFont.setFontName(fontName);
		textRightStyle.setFont(textRightStyleFont);
		textRightStyle.setAlignment(HorizontalAlignment.RIGHT);
		textRightStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textRightStyle);
		styles.put(STYLE_TEXT_ALIGN_RIGHT, textRightStyle);
		
		XSSFCellStyle boldStyle = wb.createCellStyle();
//		XSSFFont font = wb.createFont();
//		font.setFontName(fontName);
//		font.setBold(true);
		//titleFont.setFontHeight(18);
		//titleStyle.setFont(titleFont);	
		boldStyle.setFont(boldFont);
		setBorderForCell(boldStyle);
		styles.put(STYLE_BOLD, boldStyle);
		
		XSSFCellStyle textBoldRightStyle = wb.createCellStyle();
		textBoldRightStyle.setAlignment(HorizontalAlignment.RIGHT);
		textBoldRightStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignRightFont = wb.createFont();
		titleAlignRightFont.setFontName(fontName);
		titleAlignRightFont.setBold(true);
		textBoldRightStyle.setFont(titleAlignRightFont);
		//setBorderThickForCell(textBoldRightStyle);
		setBorderForCell(textBoldRightStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_RIGHT, textBoldRightStyle);
		
		//XSSFCellStyle textBoldRightStyle = wb.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.RIGHT);
		cellStyle.setDataFormat(fmt.getFormat("@"));
		//XSSFFont titleAlignRightFont = wb.createFont();
		titleAlignRightFont.setBold(true);
		cellStyle.setFont(titleAlignRightFont);
		cellStyle.setAlignment(HorizontalAlignment.RIGHT);
		cellStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(cellStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_RIGHT_CURRENCY, cellStyle);

		
		XSSFCellStyle headerGreenStyle = wb.createCellStyle();
		XSSFFont headerGreenFont = wb.createFont();
		headerGreenFont.setFontName(fontName);
		headerGreenFont.setBold(true);
		headerGreenStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 208, 80)));
		headerGreenStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerGreenStyle.setFont(headerGreenFont);
		headerGreenStyle.setAlignment(HorizontalAlignment.CENTER);
		headerGreenStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerGreenStyle.setWrapText(true);
		setBorderForCell(headerGreenStyle);		
		styles.put(STYLE_HEADER_GREEN, headerGreenStyle);
		
		XSSFCellStyle headerGreenHO2_3Style = wb.createCellStyle();
		XSSFFont headerGreenHO2_3StyleFont = wb.createFont();
		headerGreenHO2_3StyleFont.setFontName(fontName);
		headerGreenHO2_3StyleFont.setBold(true);
		headerGreenHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 208, 80)));
		headerGreenHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerGreenHO2_3Style.setFont(headerGreenHO2_3StyleFont);
		headerGreenHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerGreenHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerGreenHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerGreenHO2_3Style);		
		styles.put(STYLE_HEADER_GREEN_HO_2_3, headerGreenHO2_3Style);
		
		XSSFCellStyle headerRedHO2_3Style = wb.createCellStyle();
		XSSFFont headerRedHO2_3StyleFont = wb.createFont();
		headerRedHO2_3StyleFont.setBold(true);
		headerRedHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
		headerRedHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerRedHO2_3Style.setFont(headerGreenHO2_3StyleFont);
		headerRedHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerRedHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerRedHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerRedHO2_3Style);		
		styles.put(STYLE_HEADER_RED_HO_2_3, headerRedHO2_3Style);
		
		XSSFCellStyle headerPinkStyle = wb.createCellStyle();
		XSSFFont headerPinkFont = wb.createFont();
		headerPinkFont.setFontName(fontName);
		headerPinkFont.setBold(true);
		headerPinkStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(217, 151, 149)));
		headerPinkStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerPinkStyle.setFont(headerPinkFont);
		headerPinkStyle.setAlignment(HorizontalAlignment.CENTER);
		headerPinkStyle.setWrapText(true);
		setBorderForCell(headerPinkStyle);		
		styles.put(STYLE_HEADER_PINK, headerPinkStyle);
		
		
		XSSFCellStyle headerPinkHO2_3Style = wb.createCellStyle();
		XSSFFont headerPinkHO2_3Font = wb.createFont();
		headerPinkHO2_3Font.setFontName(fontName);
		headerPinkHO2_3Font.setBold(true);
		headerPinkHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(217, 151, 149)));
		headerPinkHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerPinkHO2_3Style.setFont(headerPinkHO2_3Font);
		headerPinkHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerPinkHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerPinkHO2_3Style);		
		styles.put(STYLE_HEADER_PINK_HO_2_3, headerPinkHO2_3Style);
		
		XSSFCellStyle headerOrange2_3Style = wb.createCellStyle();
		XSSFFont headerOrange2_3Font = wb.createFont();
		headerOrange2_3Font.setFontName(fontName);
		headerOrange2_3Font.setBold(true);
		headerOrange2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		headerOrange2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrange2_3Style.setFont(headerPinkHO2_3Font);
		headerOrange2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerOrange2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerOrange2_3Style);		
		styles.put(STYLE_HEADER_PINK_Orange_2_3, headerPinkHO2_3Style);

		XSSFCellStyle headerBlueStyle = wb.createCellStyle();
		XSSFFont headerBlueFont = wb.createFont();
		headerBlueFont.setFontName(fontName);
		headerBlueFont.setBold(true);
		headerBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(49, 132, 155)));
//		headerBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		headerBlueStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerBlueStyle.setFont(headerBlueFont);
		headerBlueStyle.setAlignment(HorizontalAlignment.CENTER);
		headerBlueStyle.setWrapText(true);
		setBorderForCell(headerBlueStyle);		
		styles.put(STYLE_HEADER_BLUE, headerBlueStyle);
		
		XSSFCellStyle headerBlueHO2_3Style = wb.createCellStyle();
		XSSFFont headerBlueHO2_3Font = wb.createFont();
		headerBlueHO2_3Font.setFontName(fontName);
		headerBlueHO2_3Font.setBold(true);
		headerBlueHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(49, 132, 155)));
		headerBlueHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerBlueHO2_3Style.setFont(headerBlueHO2_3Font);
		headerBlueHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerBlueHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerBlueHO2_3Style);		
		styles.put(STYLE_HEADER_BLUE_HO_2_3, headerBlueHO2_3Style);
		
		XSSFCellStyle headerBlue1 = wb.createCellStyle();
		XSSFFont headerBlue1Style = wb.createFont();
		headerBlue1Style.setFontName(fontName);
		headerBlue1Style.setBold(true);
		headerBlue1.setFillForegroundColor(new XSSFColor(new java.awt.Color(173,240,243)));
		headerBlue1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerBlue1.setFont(headerBlue1Style);
		headerBlue1.setAlignment(HorizontalAlignment.CENTER);
		headerBlue1.setVerticalAlignment(VerticalAlignment.CENTER);		
		headerBlue1.setWrapText(true);
		setBorderForCell(headerBlue1);
		styles.put(STYLE_HEADER_BLUE1, headerBlue1);
		
		XSSFCellStyle headerDarkOrangeStyle = wb.createCellStyle();
		XSSFFont headerDarkOrangeFont = wb.createFont();
		headerDarkOrangeFont.setFontName(fontName);
		headerDarkOrangeFont.setBold(true);
		headerDarkOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		headerDarkOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerDarkOrangeStyle.setFont(headerDarkOrangeFont);
		headerDarkOrangeStyle.setAlignment(HorizontalAlignment.CENTER);
		headerDarkOrangeStyle.setWrapText(true);
		setBorderForCell(headerDarkOrangeStyle);		
		styles.put(STYLE_HEADER_DARK_ORANGE, headerDarkOrangeStyle);
		
		XSSFCellStyle headerDarkOrangeHO2_3Style = wb.createCellStyle();
		XSSFFont headerDarkOrangeHO2_3Font = wb.createFont();
		headerDarkOrangeHO2_3Font.setFontName(fontName);
		headerDarkOrangeHO2_3Font.setBold(true);
		headerDarkOrangeHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		headerDarkOrangeHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerDarkOrangeHO2_3Style.setFont(headerDarkOrangeHO2_3Font);
		headerDarkOrangeHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerDarkOrangeHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerDarkOrangeHO2_3Style);		
		styles.put(STYLE_HEADER_DARK_ORANGE_HO_2_3, headerDarkOrangeHO2_3Style);
		
		XSSFCellStyle headerOrangeNoBorderStyle = wb.createCellStyle();
		XSSFFont headerOrangeNoBorderFont = wb.createFont();
		headerOrangeNoBorderFont.setFontName(fontName);
		headerOrangeNoBorderFont.setBold(true);
		headerOrangeNoBorderStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeNoBorderStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeNoBorderStyle.setFont(headerOrangeNoBorderFont);
		headerOrangeNoBorderStyle.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeNoBorderStyle.setWrapText(true);
		styles.put(STYLE_HEADER_ORANGE_NO_BORDER, headerOrangeNoBorderStyle);
		
		
		XSSFCellStyle headerOrangeNoBorderHO2_3Style = wb.createCellStyle();
		XSSFFont headerOrangeNoBorderHO2_3Font = wb.createFont();
		headerOrangeNoBorderHO2_3Font.setFontName(fontName);
		headerOrangeNoBorderHO2_3Font.setBold(true);
		headerOrangeNoBorderHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeNoBorderHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeNoBorderHO2_3Style.setFont(headerOrangeNoBorderHO2_3Font);
		headerOrangeNoBorderHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeNoBorderHO2_3Style.setWrapText(true);
		setBorderForCellNote(headerOrangeNoBorderHO2_3Style);
		styles.put(STYLE_HEADER_ORANGE_NO_BORDER_HO_2_3, headerOrangeNoBorderHO2_3Style);
		
		XSSFCellStyle textLeftBorderDashStyle = wb.createCellStyle();
		XSSFFont textLeftBorderDashFont = wb.createFont();
		textLeftBorderDashFont.setFontName(fontName);
		textLeftBorderDashStyle.setAlignment(HorizontalAlignment.LEFT);
		textLeftBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		textLeftBorderDashStyle.setFont(textLeftBorderDashFont);
		setBorderDashForCell(textLeftBorderDashStyle);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_LEFT, textLeftBorderDashStyle);
		
		XSSFCellStyle textRightBorderDashStyle = wb.createCellStyle();
		XSSFFont textRightBorderDashFont = wb.createFont();
		textRightBorderDashFont.setFontName(fontName);
		textRightBorderDashStyle.setAlignment(HorizontalAlignment.RIGHT);
		textRightBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		textRightBorderDashStyle.setFont(textRightBorderDashFont);
		setBorderDashForCell(textRightBorderDashStyle);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_RIGHT, textRightBorderDashStyle);
		
		XSSFCellStyle percentRightBorderDashStyle = wb.createCellStyle();
		XSSFFont percentRightBorderDashFont = wb.createFont();
		percentRightBorderDashFont.setFontName(fontName);
		percentRightBorderDashStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentRightBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		percentRightBorderDashStyle.setFont(percentRightBorderDashFont);
		setBorderDashForCell(percentRightBorderDashStyle);
		percentRightBorderDashStyle.setDataFormat(fmt.getFormat("0.0%"));
		styles.put(STYLE_PERCENT_BORDER_DASH_ALIGN_RIGHT, percentRightBorderDashStyle);
		
		XSSFCellStyle accountingGreenStyle = wb.createCellStyle();
		XSSFFont accountingGreenFont = wb.createFont();
		accountingGreenFont.setFontName(fontName);
		accountingGreenStyle.setFont(accountingGreenFont);
		accountingGreenStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreenStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(139, 231, 150)));
		accountingGreenStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreenStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingGreenStyle);
		styles.put(STYLE_GREEN_CURRENCY, accountingGreenStyle);
		
		XSSFCellStyle accountingYellowStyle = wb.createCellStyle();
		XSSFFont accountingYellowFont = wb.createFont();
		accountingYellowFont.setFontName(fontName);
		accountingYellowStyle.setFont(accountingYellowFont);
		accountingYellowStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingYellowStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 153)));
		accountingYellowStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingYellowStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingYellowStyle);
		styles.put(STYLE_YELLOW_CURRENCY, accountingYellowStyle);
		
		XSSFCellStyle accountingGreyStyle = wb.createCellStyle();
		XSSFFont accountingGreyFont = wb.createFont();
		accountingGreyFont.setFontName(fontName);
		accountingGreyStyle.setFont(accountingGreyFont);
		accountingGreyStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreyStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(227, 230, 182)));
		accountingGreyStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreyStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingGreyStyle);
		styles.put(STYLE_GREY_CURRENCY, accountingGreyStyle);
		
		XSSFCellStyle accountingBlueStyle = wb.createCellStyle();
		XSSFFont accountingBlueFont = wb.createFont();
		accountingBlueFont.setFontName(fontName);
		accountingBlueStyle.setFont(accountingBlueFont);
		accountingBlueStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		accountingBlueStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingBlueStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingBlueStyle);
		styles.put(STYLE_BLUE_CURRENCY, accountingBlueStyle);
		
		XSSFCellStyle accountingOrangeStyle = wb.createCellStyle();
		XSSFFont accountingOrangeFont = wb.createFont();
		accountingOrangeFont.setFontName(fontName);
		accountingOrangeStyle.setFont(accountingOrangeFont);
		accountingOrangeStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		accountingOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingOrangeStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingOrangeStyle);
		styles.put(STYLE_ORANGE_CURRENCY, accountingOrangeStyle);
		
		XSSFCellStyle accountingOrangeStyleBold = wb.createCellStyle();
		XSSFFont fontOrangeStyleBold = wb.createFont();
		fontOrangeStyleBold.setBold(true);
		fontOrangeStyleBold.setFontName(fontName);
		accountingOrangeStyleBold.setFont(accountingOrangeFont);
		accountingOrangeStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingOrangeStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		accountingOrangeStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingOrangeStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingOrangeStyleBold);
		styles.put(STYLE_ORANGE_CURRENCY_BOLD, accountingOrangeStyleBold);
		
		XSSFCellStyle accountingOrangeStyleBold1 = wb.createCellStyle();
		XSSFFont fontOrangeStyleBold1 = wb.createFont();
		fontOrangeStyleBold1.setBold(true);
		fontOrangeStyleBold1.setFontName(fontName);
		accountingOrangeStyleBold1.setFont(fontOrangeStyleBold1);
		accountingOrangeStyleBold1.setAlignment(HorizontalAlignment.RIGHT);
		accountingOrangeStyleBold1.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		accountingOrangeStyleBold1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingOrangeStyleBold1.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(accountingOrangeStyleBold1);
		styles.put(STYLE_ORANGE_CURRENCY_BOLD_NORMAL, accountingOrangeStyleBold1);
		
		XSSFCellStyle accountingTotalStyle = wb.createCellStyle();
		accountingTotalStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingTotalStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(147, 205, 221)));
		accountingTotalStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		XSSFFont accountingTotalFont = wb.createFont();
		accountingTotalFont.setFontName(fontName);
		accountingTotalFont.setBold(true);
		accountingTotalStyle.setFont(accountingTotalFont);
		accountingTotalStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCellTotal(accountingTotalStyle);
		styles.put(STYLE_TOTAL_CURRENCY, accountingTotalStyle);
		
		
		XSSFCellStyle headerOrangeStyle = wb.createCellStyle();
		XSSFFont headerOrangeFont = wb.createFont();
		headerOrangeFont.setFontName(fontName);
		headerOrangeFont.setBold(true);
		headerOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeStyle.setFont(headerOrangeFont);
		headerOrangeStyle.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeStyle.setWrapText(true);
		setBorderForCell(headerOrangeStyle);		
		styles.put(STYLE_HEADER_ORANGE, headerOrangeStyle);
		
		XSSFCellStyle headerOrangeHO2_3Style = wb.createCellStyle();
		XSSFFont headerOrangeHO2_3Font = wb.createFont();
		headerOrangeHO2_3Font.setFontName(fontName);
		headerOrangeHO2_3Font.setBold(true);
		headerOrangeHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeHO2_3Style.setFont(headerOrangeHO2_3Font);
		headerOrangeHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerOrangeHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerOrangeHO2_3Style);		
		styles.put(STYLE_HEADER_ORANGE_HO_2_3, headerOrangeHO2_3Style);
		
		XSSFCellStyle textLeftBorderDashStyleBold = wb.createCellStyle();
		textLeftBorderDashStyleBold.setAlignment(HorizontalAlignment.LEFT);
		textLeftBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, fontName,11, textLeftBorderDashStyleBold);
		setBorderDashForCell(textLeftBorderDashStyleBold);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_LEFT_BOLD, textLeftBorderDashStyleBold);
		
		XSSFCellStyle textRightBorderDashStyleBold = wb.createCellStyle();
		textRightBorderDashStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		textRightBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, fontName,11, textRightBorderDashStyleBold);
		setBorderDashForCell(textRightBorderDashStyleBold);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_RIGHT_BOLD, textRightBorderDashStyleBold);

		XSSFCellStyle percentRightBorderDashStyleBold = wb.createCellStyle();
		percentRightBorderDashStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		percentRightBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, fontName,11, percentRightBorderDashStyleBold);
		setBorderDashForCell(percentRightBorderDashStyleBold);
		percentRightBorderDashStyleBold.setDataFormat(fmt.getFormat("0.0%"));
		styles.put(STYLE_PERCENT_BORDER_DASH_ALIGN_RIGHT_BOLD, percentRightBorderDashStyleBold);
		
		XSSFCellStyle accountingGreenStyleBold = wb.createCellStyle();
		accountingGreenStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreenStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(139, 231, 150)));
		accountingGreenStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreenStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingGreenStyleBold);
		setBorderDashForCell(accountingGreenStyleBold);
		styles.put(STYLE_GREEN_CURRENCY_BOLD, accountingGreenStyleBold);
		
		XSSFCellStyle accountingGreenStyleBold1 = wb.createCellStyle();
		accountingGreenStyleBold1.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreenStyleBold1.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 208, 80)));
		accountingGreenStyleBold1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreenStyleBold1.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingGreenStyleBold1);
		setBorderForCell(accountingGreenStyleBold1);
		styles.put(STYLE_GREEN_CURRENCY_BOLD_NORMAL, accountingGreenStyleBold1);
		
		XSSFCellStyle accountingDarkOrganeBold = wb.createCellStyle();
		accountingDarkOrganeBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingDarkOrganeBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		accountingDarkOrganeBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingDarkOrganeBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingDarkOrganeBold);
		setBorderForCell(accountingDarkOrganeBold);
		styles.put(STYLE_DARK_ORANGE_CURRENCY_BOLD, accountingDarkOrganeBold);
		
		XSSFCellStyle accountingYellowStyleBold = wb.createCellStyle();
		accountingYellowStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingYellowStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 153)));
		accountingYellowStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingYellowStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, null,11, accountingYellowStyleBold);
		setBorderDashForCell(accountingYellowStyleBold);
		styles.put(STYLE_YELLOW_CURRENCY_BOLD, accountingYellowStyleBold);
		
		XSSFCellStyle accountingGreyStyleBold = wb.createCellStyle();
		accountingGreyStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreyStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(227, 230, 182)));
		accountingGreyStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreyStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingGreyStyleBold);
		setBorderDashForCell(accountingGreyStyleBold);
		styles.put(STYLE_GREY_CURRENCY_BOLD, accountingGreyStyleBold);
		
		XSSFCellStyle accountingBlueStyleBold = wb.createCellStyle();
		accountingBlueStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingBlueStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		accountingBlueStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingBlueStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingBlueStyleBold);
		setBorderDashForCell(accountingBlueStyleBold);
		styles.put(STYLE_BLUE_CURRENCY_BOLD, accountingBlueStyleBold);
		
		XSSFCellStyle accountingBlueStyleBold1 = wb.createCellStyle();
		accountingBlueStyleBold1.setAlignment(HorizontalAlignment.RIGHT);
		accountingBlueStyleBold1.setFillForegroundColor(new XSSFColor(new java.awt.Color(49, 132, 155)));
		accountingBlueStyleBold1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingBlueStyleBold1.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingBlueStyleBold1);
		setBorderForCell(accountingBlueStyleBold1);
		styles.put(STYLE_BLUE_CURRENCY_BOLD_NORMAL, accountingBlueStyleBold1);
		
		//TEXT_ALIGN_LEFT
		XSSFCellStyle group1 = wb.createCellStyle();
		group1.setAlignment(HorizontalAlignment.LEFT);
		group1.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group1.setDataFormat(fmt.getFormat("@"));
		group1.setFont(boldFont);
		setBorderForCell(group1);
		styles.put(STYLE_TEXT_GROUP_LEVEL_1, group1);
		
		XSSFCellStyle normalGroup1 = wb.createCellStyle();
		normalGroup1.setAlignment(HorizontalAlignment.LEFT);
		normalGroup1.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		normalGroup1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup1.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(normalGroup1);
		normalGroup1.setFont(font);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_1, normalGroup1);
		
		XSSFCellStyle group2 = wb.createCellStyle();
		group2.setAlignment(HorizontalAlignment.LEFT);
		group2.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group2.setDataFormat(fmt.getFormat("@"));
		group2.setFont(boldFont);
		setBorderForCell(group2);
		styles.put(STYLE_TEXT_GROUP_LEVEL_2, group2);
		
		XSSFCellStyle normalGroup2 = wb.createCellStyle();
		normalGroup2.setAlignment(HorizontalAlignment.LEFT);
		normalGroup2.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		normalGroup2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup2.setDataFormat(fmt.getFormat("@"));
		normalGroup2.setFont(font);
		setBorderForCell(normalGroup2);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_2, normalGroup2);
		
		XSSFCellStyle group3 = wb.createCellStyle();
		group3.setAlignment(HorizontalAlignment.LEFT);
		group3.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group3.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group3.setDataFormat(fmt.getFormat("@"));
		group3.setFont(boldFont);
		setBorderForCell(group3);
		styles.put(STYLE_TEXT_GROUP_LEVEL_3, group3);
		
		XSSFCellStyle normalGroup3 = wb.createCellStyle();
		normalGroup3.setAlignment(HorizontalAlignment.LEFT);
		normalGroup3.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		normalGroup3.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup3.setDataFormat(fmt.getFormat("@"));
		normalGroup3.setFont(font);
		setBorderForCell(normalGroup3);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_3, normalGroup3);
		
		XSSFCellStyle group4 = wb.createCellStyle();
		group4.setAlignment(HorizontalAlignment.LEFT);
		group4.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group4.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group4.setDataFormat(fmt.getFormat("@"));
		group4.setFont(boldFont);
		setBorderForCell(group4);
		styles.put(STYLE_TEXT_GROUP_LEVEL_4, group4);
		
		XSSFCellStyle normalGroup4 = wb.createCellStyle();
		normalGroup4.setAlignment(HorizontalAlignment.LEFT);
		normalGroup4.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		normalGroup4.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup4.setDataFormat(fmt.getFormat("@"));
		normalGroup4.setFont(font);
		setBorderForCell(normalGroup4);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_4, normalGroup4);
		
		XSSFCellStyle group5 = wb.createCellStyle();
		group5.setAlignment(HorizontalAlignment.LEFT);
		group5.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group5.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group5.setDataFormat(fmt.getFormat("@"));
		group5.setFont(boldFont);
		setBorderForCell(group5);
		styles.put(STYLE_TEXT_GROUP_LEVEL_5, group5);
		
		XSSFCellStyle normalGroup5 = wb.createCellStyle();
		normalGroup5.setAlignment(HorizontalAlignment.LEFT);
		normalGroup5.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		normalGroup5.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup5.setDataFormat(fmt.getFormat("@"));
		normalGroup5.setFont(font);
		setBorderForCell(normalGroup5);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_5, normalGroup5);
		
		//TEXT_ALIGN_CENTER
		XSSFCellStyle group6 = wb.createCellStyle();
		group6.setAlignment(HorizontalAlignment.CENTER);
		group6.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group6.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group6.setDataFormat(fmt.getFormat("@"));
		group6.setFont(boldFont);
		setBorderForCell(group6);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_1, group6);
		
		XSSFCellStyle group7 = wb.createCellStyle();
		group7.setAlignment(HorizontalAlignment.CENTER);
		group7.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group7.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group7.setDataFormat(fmt.getFormat("@"));
		group7.setFont(boldFont);
		setBorderForCell(group7);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_2, group7);
		
		XSSFCellStyle group8 = wb.createCellStyle();
		group8.setAlignment(HorizontalAlignment.CENTER);
		group8.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group8.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group8.setDataFormat(fmt.getFormat("@"));
		group8.setFont(boldFont);
		setBorderForCell(group8);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_3, group8);
		
		XSSFCellStyle group9 = wb.createCellStyle();
		group9.setAlignment(HorizontalAlignment.CENTER);
		group9.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group9.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group9.setDataFormat(fmt.getFormat("@"));
		group9.setFont(boldFont);
		setBorderForCell(group9);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_4, group9);
		
		XSSFCellStyle group10 = wb.createCellStyle();
		group10.setAlignment(HorizontalAlignment.CENTER);
		group10.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group10.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group10.setDataFormat(fmt.getFormat("@"));
		group10.setFont(boldFont);
		setBorderForCell(group10);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_5, group10);
		
		//GROUP_PERCENT
		XSSFCellStyle group11 = wb.createCellStyle();
		group11.setAlignment(HorizontalAlignment.RIGHT);
		group11.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group11.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group11.setDataFormat(fmt.getFormat("0.0%"));
		group11.setFont(boldFont);
		setBorderForCell(group11);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_1, group11);
		
		XSSFCellStyle group12 = wb.createCellStyle();
		group12.setAlignment(HorizontalAlignment.RIGHT);
		group12.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group12.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group12.setDataFormat(fmt.getFormat("0.0%"));
		group12.setFont(boldFont);
		setBorderForCell(group12);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_2, group12);
		
		XSSFCellStyle group13 = wb.createCellStyle();
		group13.setAlignment(HorizontalAlignment.RIGHT);
		group13.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group13.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group13.setDataFormat(fmt.getFormat("0.0%"));
		group13.setFont(boldFont);
		setBorderForCell(group13);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_3, group13);
		
		XSSFCellStyle group14 = wb.createCellStyle();
		group14.setAlignment(HorizontalAlignment.RIGHT);
		group14.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group14.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group14.setDataFormat(fmt.getFormat("0.0%"));
		group14.setFont(boldFont);
		setBorderForCell(group14);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_4, group14);
		
		XSSFCellStyle group15 = wb.createCellStyle();
		group15.setAlignment(HorizontalAlignment.RIGHT);
		group15.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group15.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group15.setDataFormat(fmt.getFormat("0.0%"));
		group15.setFont(boldFont);
		setBorderForCell(group15);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_5, group15);
		
		//GROUP_ACCOUNTING
		XSSFCellStyle group16 = wb.createCellStyle();
		group16.setAlignment(HorizontalAlignment.RIGHT);
		group16.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group16.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group16.setDataFormat(fmt.getFormat("#,###0.00"));
		group16.setFont(boldFont);
		setBorderForCell(group16);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_1, group16);
		
		XSSFCellStyle group17 = wb.createCellStyle();
		group17.setAlignment(HorizontalAlignment.RIGHT);
		group17.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group17.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group17.setDataFormat(fmt.getFormat("#,###0.00"));
		group17.setFont(boldFont);
		setBorderForCell(group17);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_2, group17);
		
		XSSFCellStyle group18 = wb.createCellStyle();
		group18.setAlignment(HorizontalAlignment.RIGHT);
		group18.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group18.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group18.setDataFormat(fmt.getFormat("#,###0.00"));
		group18.setFont(boldFont);
		setBorderForCell(group18);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_3, group18);
		
		XSSFCellStyle group19 = wb.createCellStyle();
		group19.setAlignment(HorizontalAlignment.RIGHT);
		group19.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group19.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group19.setDataFormat(fmt.getFormat("#,###0.00"));
		group19.setFont(boldFont);
		setBorderForCell(group19);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_4, group19);
		
		XSSFCellStyle group20 = wb.createCellStyle();
		group20.setAlignment(HorizontalAlignment.RIGHT);
		group20.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group20.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group20.setDataFormat(fmt.getFormat("#,###0.00"));
		group20.setFont(boldFont);
		setBorderForCell(group20);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_5, group20);
		
		//GROUP_CURRENCY
		XSSFCellStyle group21 = wb.createCellStyle();
		group21.setAlignment(HorizontalAlignment.RIGHT);
		group21.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group21.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group21.setDataFormat(fmt.getFormat("#,###0"));
		group21.setFont(boldFont);
		setBorderForCell(group21);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_1, group21);
		
		XSSFCellStyle group22 = wb.createCellStyle();
		group22.setAlignment(HorizontalAlignment.RIGHT);
		group22.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group22.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group22.setDataFormat(fmt.getFormat("#,###0"));
		group22.setFont(boldFont);
		setBorderForCell(group22);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_2, group22);
		
		XSSFCellStyle group23 = wb.createCellStyle();
		group23.setAlignment(HorizontalAlignment.RIGHT);
		group23.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group23.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group23.setDataFormat(fmt.getFormat("#,###0"));
		group23.setFont(boldFont);
		setBorderForCell(group23);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_3, group23);
		
		XSSFCellStyle group24 = wb.createCellStyle();
		group24.setAlignment(HorizontalAlignment.RIGHT);
		group24.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group24.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group24.setDataFormat(fmt.getFormat("#,###0"));
		group24.setFont(boldFont);
		setBorderForCell(group24);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_4, group24);
		
		XSSFCellStyle group25 = wb.createCellStyle();
		group25.setAlignment(HorizontalAlignment.RIGHT);
		group25.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group25.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group25.setDataFormat(fmt.getFormat("#,###0"));
		group25.setFont(boldFont);
		setBorderForCell(group25);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_5, group25);
		
		XSSFCellStyle normalGroup21 = wb.createCellStyle();
		normalGroup21.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup21.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		normalGroup21.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup21.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup21);
		normalGroup21.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_1, normalGroup21);

		XSSFCellStyle normalGroup22 = wb.createCellStyle();
		normalGroup22.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup22.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		normalGroup22.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup22.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup22);
		normalGroup22.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_2, normalGroup22);

		XSSFCellStyle normalGroup23 = wb.createCellStyle();
		normalGroup23.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup23.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		normalGroup23.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup23.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup23);
		normalGroup23.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_3, normalGroup23);

		XSSFCellStyle normalGroup24 = wb.createCellStyle();
		normalGroup24.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup24.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		normalGroup24.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup24.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup24);
		normalGroup24.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_4, normalGroup24);

		XSSFCellStyle normalGroup25 = wb.createCellStyle();
		normalGroup25.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup25.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		normalGroup25.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup25.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup25);
		normalGroup25.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_5, normalGroup25);
		
		XSSFCellStyle textRed = wb.createCellStyle();
		XSSFFont textRedFont = wb.createFont();
		textRedFont.setFontName(fontName);
		textRedFont.setFontHeight(12);
		textRedFont.setColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
		textRed.setAlignment(HorizontalAlignment.LEFT);
		textRed.setFont(textRedFont);				
		setBorderForCell(textRed);
		styles.put(STYLE_TEXT_RED_LEFT, textRed);
		
		//trietptm them style cho project khoi loi
		XSSFFont detailNormalFont = wb.createFont();
		detailNormalFont.setBold(false);
		detailNormalFont.setFontName("Arial");
		detailNormalFont.setFontHeight(10);
	    XSSFCellStyle detailNormalDottedRightCellStyle = wb.createCellStyle();
	    detailNormalDottedRightCellStyle.setDataFormat(fmt.getFormat("#,###0"));
	    detailNormalDottedRightCellStyle.setFont(detailNormalFont);
	    detailNormalDottedRightCellStyle.setBorderColor(BorderSide.TOP, new XSSFColor(new java.awt.Color(10)));
	    detailNormalDottedRightCellStyle.setBorderColor(BorderSide.BOTTOM, new XSSFColor(new java.awt.Color(10)));
	    detailNormalDottedRightCellStyle.setBorderColor(BorderSide.RIGHT, new XSSFColor(new java.awt.Color(10)));
	    detailNormalDottedRightCellStyle.setBorderColor(BorderSide.LEFT, new XSSFColor(new java.awt.Color(10)));
	    detailNormalDottedRightCellStyle.setAlignment(HorizontalAlignment.RIGHT);
	    styles.put(STYLE_DETAIL_NORMAL_DOTTED_RIGHT, detailNormalDottedRightCellStyle);
	    //end
	    
		return styles;
	}
	
	/**
	 * Create styles for SXSSFWorkbook
	 * @author trungtm6
	 * @param wb
	 * @param fontName
	 * @return
	 */
	public static Map<String, XSSFCellStyle> createStylesWithFont(SXSSFWorkbook wb, String fontName) {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		XSSFDataFormat fmt = (XSSFDataFormat)wb.createDataFormat();
		XSSFCellStyle cellStyle = (XSSFCellStyle)wb.createCellStyle();
		
		XSSFFont font = (XSSFFont)wb.createFont();
		font.setFontName(fontName);
		
		XSSFFont boldFont = (XSSFFont)wb.createFont();
		boldFont.setFontName(fontName);
		boldFont.setBold(true);
		
		XSSFCellStyle titleStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont titleFont = (XSSFFont)wb.createFont();
		titleFont.setFontName(fontName);
		titleFont.setBold(true);
		titleFont.setFontHeight(18);
		titleStyle.setFont(titleFont);			
		titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		styles.put(STYLE_TITLE, titleStyle);
		
		XSSFCellStyle titleSubStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont titleSubFont = (XSSFFont)wb.createFont();
		titleSubFont.setFontName(fontName);
		titleSubFont.setBold(true);
		titleSubFont.setFontHeight(12);
		titleSubStyle.setFont(titleSubFont);				
		styles.put(STYLE_TEXT, titleSubStyle);
		
		XSSFCellStyle headerStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerFont = (XSSFFont)wb.createFont();
		headerFont.setFontName(fontName);
		headerFont.setBold(true);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerStyle.setFont(headerFont);
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerStyle.setWrapText(true);
		setBorderForCell(headerStyle);		
		styles.put(STYLE_HEADER, headerStyle);

		XSSFCellStyle percentStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont percentStyleFont = (XSSFFont)wb.createFont();
		percentStyleFont.setFontName(fontName);
		percentStyle.setFont(percentStyleFont);
		percentStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentStyle.setDataFormat(fmt.getFormat("0.0%"));
		setBorderForCell(percentStyle);
		styles.put(STYLE_PERCENT, percentStyle);
		
		XSSFCellStyle percentBoldStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont percentBoldStyleFont = (XSSFFont)wb.createFont();
		percentBoldStyleFont.setBold(true);
		percentBoldStyleFont.setFontName(fontName);
		percentBoldStyle.setFont(percentBoldStyleFont);
		percentBoldStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentBoldStyle.setDataFormat(fmt.getFormat("0.0%"));
		setBorderForCell(percentBoldStyle);
		styles.put(STYLE_PERCENT_B, percentBoldStyle);
		
		XSSFCellStyle dateStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont dateStyleFont = (XSSFFont)wb.createFont();
		dateStyleFont.setFontName(fontName);
		dateStyle.setFont(dateStyleFont);
		dateStyle.setAlignment(HorizontalAlignment.RIGHT);
		dateStyle.setDataFormat(fmt.getFormat("d/m/yy"));
		setBorderForCell(dateStyle);
		styles.put(STYLE_DATE, dateStyle);

		XSSFCellStyle currencyStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont currencyStyleFont = (XSSFFont)wb.createFont();
		currencyStyleFont.setFontName(fontName);
		currencyStyle.setFont(currencyStyleFont);
		currencyStyle.setAlignment(HorizontalAlignment.RIGHT);
		currencyStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(currencyStyle);
		styles.put(STYLE_CURRENCY, currencyStyle);
		
		XSSFCellStyle currencyBStyle = (XSSFCellStyle)wb.createCellStyle();
		currencyBStyle.setAlignment(HorizontalAlignment.RIGHT);
		currencyBStyle.setDataFormat(fmt.getFormat("#,###0"));
		XSSFFont titleBFont = (XSSFFont)wb.createFont();
		titleBFont.setFontName(fontName);
		titleBFont.setBold(true);
		currencyBStyle.setFont(titleBFont);
		setBorderForCell(currencyBStyle);
		styles.put(STYLE_CURRENCY_B, currencyBStyle);
		
		XSSFCellStyle currencyStyleNoneZero = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont currencyStyleFontNoneZero = (XSSFFont)wb.createFont();
		currencyStyleNoneZero.setFont(currencyStyleFontNoneZero);
		currencyStyleNoneZero.setAlignment(HorizontalAlignment.RIGHT);
		currencyStyleNoneZero.setDataFormat(fmt.getFormat("#,###"));
		setBorderForCell(currencyStyleNoneZero);
		styles.put(STYLE_CURRENCY_NONE_ZERO, currencyStyleNoneZero);
		
		
		XSSFCellStyle accountingStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont accountingStyleFont = (XSSFFont)wb.createFont();
		accountingStyleFont.setFontName(fontName);
		accountingStyle.setFont(accountingStyleFont);
		accountingStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingStyle.setDataFormat(fmt.getFormat("#,###0.00"));
		setBorderForCell(accountingStyle);
		styles.put(STYLE_ACCOUNTING, accountingStyle);
		
		XSSFCellStyle accountingBoldStyle = (XSSFCellStyle)wb.createCellStyle();
		accountingBoldStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingBoldStyle.setDataFormat(fmt.getFormat("#,###0.00"));
		XSSFFont titleAccountingFont = (XSSFFont)wb.createFont();
		titleAccountingFont.setFontName(fontName);
		titleAccountingFont.setBold(true);
		accountingBoldStyle.setFont(titleAccountingFont);
		//setBorderThickForCell(accountingBoldStyle);
		setBorderForCell(accountingBoldStyle);
		styles.put(STYLE_BOLD_ACCOUNTING, accountingBoldStyle);
			
		
		XSSFCellStyle textLeftStyle = (XSSFCellStyle)wb.createCellStyle();
		textLeftStyle.setAlignment(HorizontalAlignment.LEFT);
		textLeftStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont textLeftStyleFont = (XSSFFont)wb.createFont();
		textLeftStyleFont.setFontName(fontName);
		textLeftStyle.setFont(textLeftStyleFont);
		setBorderForCell(textLeftStyle);
		styles.put(STYLE_TEXT_ALIGN_LEFT, textLeftStyle);
		
		XSSFCellStyle textBoldLeftStyle = (XSSFCellStyle)wb.createCellStyle();
		textBoldLeftStyle.setAlignment(HorizontalAlignment.LEFT);
		textBoldLeftStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignLeftFont = (XSSFFont)wb.createFont();
		titleAlignLeftFont.setFontName(fontName);
		titleAlignLeftFont.setBold(true);
		textBoldLeftStyle.setFont(titleAlignLeftFont);
		//setBorderThickForCell(textBoldLeftStyle);
		setBorderForCell(textBoldLeftStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_LEFT, textBoldLeftStyle);
		
		XSSFCellStyle textLeftNoBorder = (XSSFCellStyle)wb.createCellStyle();
		textLeftNoBorder.setAlignment(HorizontalAlignment.LEFT);
		textLeftNoBorder.setDataFormat(fmt.getFormat("@"));
		textLeftNoBorder.setFont(font);
		styles.put(STYLE_TEXT_ALIGN_LEFT_NO_BORDER, textLeftNoBorder);
		
		XSSFCellStyle textBoldLeftNoBorder = (XSSFCellStyle)wb.createCellStyle();
		textBoldLeftNoBorder.setAlignment(HorizontalAlignment.LEFT);
		textBoldLeftNoBorder.setDataFormat(fmt.getFormat("@"));
		textBoldLeftNoBorder.setFont(boldFont);
		styles.put(STYLE_TEXT_ALIGN_LEFT_NO_BORDER_BOLD, textBoldLeftNoBorder);
		
		XSSFCellStyle textCenterStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont textCenterStyleFont = (XSSFFont)wb.createFont();
		textCenterStyleFont.setFontName(fontName);
		textCenterStyle.setFont(textCenterStyleFont);
		textCenterStyle.setAlignment(HorizontalAlignment.CENTER);
		textCenterStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textCenterStyle);
		styles.put(STYLE_TEXT_ALIGN_CENTER, textCenterStyle);
		
		XSSFCellStyle textBoldCenterStyle = (XSSFCellStyle)wb.createCellStyle();
		textBoldCenterStyle.setAlignment(HorizontalAlignment.CENTER);
		textBoldCenterStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignCenterFont = (XSSFFont)wb.createFont();
		titleAlignCenterFont.setFontName(fontName);
		titleAlignCenterFont.setBold(true);
		textBoldCenterStyle.setFont(titleAlignCenterFont);
		//setBorderThickForCell(textBoldCenterStyle);
		setBorderForCell(textBoldCenterStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_CENTER, textBoldCenterStyle);
		
		XSSFCellStyle textRightStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont textRightStyleFont = (XSSFFont)wb.createFont();
		textRightStyleFont.setFontName(fontName);
		textRightStyle.setFont(textRightStyleFont);
		textRightStyle.setAlignment(HorizontalAlignment.RIGHT);
		textRightStyle.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(textRightStyle);
		styles.put(STYLE_TEXT_ALIGN_RIGHT, textRightStyle);
		
		XSSFCellStyle boldStyle = (XSSFCellStyle)wb.createCellStyle();
//		XSSFFont font = (XSSFFont)wb.createFont();
//		font.setFontName(fontName);
//		font.setBold(true);
		//titleFont.setFontHeight(18);
		//titleStyle.setFont(titleFont);	
		boldStyle.setFont(boldFont);
		setBorderForCell(boldStyle);
		styles.put(STYLE_BOLD, boldStyle);
		
		XSSFCellStyle textBoldRightStyle = (XSSFCellStyle)wb.createCellStyle();
		textBoldRightStyle.setAlignment(HorizontalAlignment.RIGHT);
		textBoldRightStyle.setDataFormat(fmt.getFormat("@"));
		XSSFFont titleAlignRightFont = (XSSFFont)wb.createFont();
		titleAlignRightFont.setFontName(fontName);
		titleAlignRightFont.setBold(true);
		textBoldRightStyle.setFont(titleAlignRightFont);
		//setBorderThickForCell(textBoldRightStyle);
		setBorderForCell(textBoldRightStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_RIGHT, textBoldRightStyle);
		
		//XSSFCellStyle textBoldRightStyle = (XSSFCellStyle)wb.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.RIGHT);
		cellStyle.setDataFormat(fmt.getFormat("@"));
		//XSSFFont titleAlignRightFont = (XSSFFont)wb.createFont();
		titleAlignRightFont.setBold(true);
		cellStyle.setFont(titleAlignRightFont);
		cellStyle.setAlignment(HorizontalAlignment.RIGHT);
		cellStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(cellStyle);
		styles.put(STYLE_TEXT_ALIGN_BOLD_RIGHT_CURRENCY, cellStyle);

		
		XSSFCellStyle headerGreenStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerGreenFont = (XSSFFont)wb.createFont();
		headerGreenFont.setFontName(fontName);
		headerGreenFont.setBold(true);
		headerGreenStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 208, 80)));
		headerGreenStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerGreenStyle.setFont(headerGreenFont);
		headerGreenStyle.setAlignment(HorizontalAlignment.CENTER);
		headerGreenStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerGreenStyle.setWrapText(true);
		setBorderForCell(headerGreenStyle);		
		styles.put(STYLE_HEADER_GREEN, headerGreenStyle);
		
		XSSFCellStyle headerGreenHO2_3Style = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerGreenHO2_3StyleFont = (XSSFFont)wb.createFont();
		headerGreenHO2_3StyleFont.setFontName(fontName);
		headerGreenHO2_3StyleFont.setBold(true);
		headerGreenHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 208, 80)));
		headerGreenHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerGreenHO2_3Style.setFont(headerGreenHO2_3StyleFont);
		headerGreenHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerGreenHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerGreenHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerGreenHO2_3Style);		
		styles.put(STYLE_HEADER_GREEN_HO_2_3, headerGreenHO2_3Style);
		
		XSSFCellStyle headerRedHO2_3Style = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerRedHO2_3StyleFont = (XSSFFont)wb.createFont();
		headerRedHO2_3StyleFont.setBold(true);
		headerRedHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
		headerRedHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerRedHO2_3Style.setFont(headerGreenHO2_3StyleFont);
		headerRedHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerRedHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerRedHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerRedHO2_3Style);		
		styles.put(STYLE_HEADER_RED_HO_2_3, headerRedHO2_3Style);
		
		XSSFCellStyle headerPinkStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerPinkFont = (XSSFFont)wb.createFont();
		headerPinkFont.setFontName(fontName);
		headerPinkFont.setBold(true);
		headerPinkStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(217, 151, 149)));
		headerPinkStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerPinkStyle.setFont(headerPinkFont);
		headerPinkStyle.setAlignment(HorizontalAlignment.CENTER);
		headerPinkStyle.setWrapText(true);
		setBorderForCell(headerPinkStyle);		
		styles.put(STYLE_HEADER_PINK, headerPinkStyle);
		
		
		XSSFCellStyle headerPinkHO2_3Style = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerPinkHO2_3Font = (XSSFFont)wb.createFont();
		headerPinkHO2_3Font.setFontName(fontName);
		headerPinkHO2_3Font.setBold(true);
		headerPinkHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(217, 151, 149)));
		headerPinkHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerPinkHO2_3Style.setFont(headerPinkHO2_3Font);
		headerPinkHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerPinkHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerPinkHO2_3Style);		
		styles.put(STYLE_HEADER_PINK_HO_2_3, headerPinkHO2_3Style);
		
		XSSFCellStyle headerOrange2_3Style = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerOrange2_3Font = (XSSFFont)wb.createFont();
		headerOrange2_3Font.setFontName(fontName);
		headerOrange2_3Font.setBold(true);
		headerOrange2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		headerOrange2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrange2_3Style.setFont(headerPinkHO2_3Font);
		headerOrange2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerOrange2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerOrange2_3Style);		
		styles.put(STYLE_HEADER_PINK_Orange_2_3, headerPinkHO2_3Style);

		XSSFCellStyle headerBlueStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerBlueFont = (XSSFFont)wb.createFont();
		headerBlueFont.setFontName(fontName);
		headerBlueFont.setBold(true);
		headerBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(49, 132, 155)));
//		headerBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		headerBlueStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerBlueStyle.setFont(headerBlueFont);
		headerBlueStyle.setAlignment(HorizontalAlignment.CENTER);
		headerBlueStyle.setWrapText(true);
		setBorderForCell(headerBlueStyle);		
		styles.put(STYLE_HEADER_BLUE, headerBlueStyle);
		
		XSSFCellStyle headerBlueHO2_3Style = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerBlueHO2_3Font = (XSSFFont)wb.createFont();
		headerBlueHO2_3Font.setFontName(fontName);
		headerBlueHO2_3Font.setBold(true);
		headerBlueHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(49, 132, 155)));
		headerBlueHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerBlueHO2_3Style.setFont(headerBlueHO2_3Font);
		headerBlueHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerBlueHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerBlueHO2_3Style);		
		styles.put(STYLE_HEADER_BLUE_HO_2_3, headerBlueHO2_3Style);
		
		XSSFCellStyle headerBlue1 = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerBlue1Style = (XSSFFont)wb.createFont();
		headerBlue1Style.setFontName(fontName);
		headerBlue1Style.setBold(true);
		headerBlue1.setFillForegroundColor(new XSSFColor(new java.awt.Color(173,240,243)));
		headerBlue1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerBlue1.setFont(headerBlue1Style);
		headerBlue1.setAlignment(HorizontalAlignment.CENTER);
		headerBlue1.setVerticalAlignment(VerticalAlignment.CENTER);		
		headerBlue1.setWrapText(true);
		setBorderForCell(headerBlue1);
		styles.put(STYLE_HEADER_BLUE1, headerBlue1);
		
		XSSFCellStyle headerDarkOrangeStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerDarkOrangeFont = (XSSFFont)wb.createFont();
		headerDarkOrangeFont.setFontName(fontName);
		headerDarkOrangeFont.setBold(true);
		headerDarkOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		headerDarkOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerDarkOrangeStyle.setFont(headerDarkOrangeFont);
		headerDarkOrangeStyle.setAlignment(HorizontalAlignment.CENTER);
		headerDarkOrangeStyle.setWrapText(true);
		setBorderForCell(headerDarkOrangeStyle);		
		styles.put(STYLE_HEADER_DARK_ORANGE, headerDarkOrangeStyle);
		
		XSSFCellStyle headerDarkOrangeHO2_3Style = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerDarkOrangeHO2_3Font = (XSSFFont)wb.createFont();
		headerDarkOrangeHO2_3Font.setFontName(fontName);
		headerDarkOrangeHO2_3Font.setBold(true);
		headerDarkOrangeHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		headerDarkOrangeHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerDarkOrangeHO2_3Style.setFont(headerDarkOrangeHO2_3Font);
		headerDarkOrangeHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerDarkOrangeHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerDarkOrangeHO2_3Style);		
		styles.put(STYLE_HEADER_DARK_ORANGE_HO_2_3, headerDarkOrangeHO2_3Style);
		
		XSSFCellStyle headerOrangeNoBorderStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerOrangeNoBorderFont = (XSSFFont)wb.createFont();
		headerOrangeNoBorderFont.setFontName(fontName);
		headerOrangeNoBorderFont.setBold(true);
		headerOrangeNoBorderStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeNoBorderStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeNoBorderStyle.setFont(headerOrangeNoBorderFont);
		headerOrangeNoBorderStyle.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeNoBorderStyle.setWrapText(true);
		styles.put(STYLE_HEADER_ORANGE_NO_BORDER, headerOrangeNoBorderStyle);
		
		
		XSSFCellStyle headerOrangeNoBorderHO2_3Style = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerOrangeNoBorderHO2_3Font = (XSSFFont)wb.createFont();
		headerOrangeNoBorderHO2_3Font.setFontName(fontName);
		headerOrangeNoBorderHO2_3Font.setBold(true);
		headerOrangeNoBorderHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeNoBorderHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeNoBorderHO2_3Style.setFont(headerOrangeNoBorderHO2_3Font);
		headerOrangeNoBorderHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeNoBorderHO2_3Style.setWrapText(true);
		setBorderForCellNote(headerOrangeNoBorderHO2_3Style);
		styles.put(STYLE_HEADER_ORANGE_NO_BORDER_HO_2_3, headerOrangeNoBorderHO2_3Style);
		
		XSSFCellStyle textLeftBorderDashStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont textLeftBorderDashFont = (XSSFFont)wb.createFont();
		textLeftBorderDashFont.setFontName(fontName);
		textLeftBorderDashStyle.setAlignment(HorizontalAlignment.LEFT);
		textLeftBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		textLeftBorderDashStyle.setFont(textLeftBorderDashFont);
		setBorderDashForCell(textLeftBorderDashStyle);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_LEFT, textLeftBorderDashStyle);
		
		XSSFCellStyle textRightBorderDashStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont textRightBorderDashFont = (XSSFFont)wb.createFont();
		textRightBorderDashFont.setFontName(fontName);
		textRightBorderDashStyle.setAlignment(HorizontalAlignment.RIGHT);
		textRightBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		textRightBorderDashStyle.setFont(textRightBorderDashFont);
		setBorderDashForCell(textRightBorderDashStyle);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_RIGHT, textRightBorderDashStyle);
		
		XSSFCellStyle percentRightBorderDashStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont percentRightBorderDashFont = (XSSFFont)wb.createFont();
		percentRightBorderDashFont.setFontName(fontName);
		percentRightBorderDashStyle.setAlignment(HorizontalAlignment.RIGHT);
		percentRightBorderDashStyle.setDataFormat(fmt.getFormat("@"));
		percentRightBorderDashStyle.setFont(percentRightBorderDashFont);
		setBorderDashForCell(percentRightBorderDashStyle);
		percentRightBorderDashStyle.setDataFormat(fmt.getFormat("0.0%"));
		styles.put(STYLE_PERCENT_BORDER_DASH_ALIGN_RIGHT, percentRightBorderDashStyle);
		
		XSSFCellStyle accountingGreenStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont accountingGreenFont = (XSSFFont)wb.createFont();
		accountingGreenFont.setFontName(fontName);
		accountingGreenStyle.setFont(accountingGreenFont);
		accountingGreenStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreenStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(139, 231, 150)));
		accountingGreenStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreenStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingGreenStyle);
		styles.put(STYLE_GREEN_CURRENCY, accountingGreenStyle);
		
		XSSFCellStyle accountingYellowStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont accountingYellowFont = (XSSFFont)wb.createFont();
		accountingYellowFont.setFontName(fontName);
		accountingYellowStyle.setFont(accountingYellowFont);
		accountingYellowStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingYellowStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 153)));
		accountingYellowStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingYellowStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingYellowStyle);
		styles.put(STYLE_YELLOW_CURRENCY, accountingYellowStyle);
		
		XSSFCellStyle accountingGreyStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont accountingGreyFont = (XSSFFont)wb.createFont();
		accountingGreyFont.setFontName(fontName);
		accountingGreyStyle.setFont(accountingGreyFont);
		accountingGreyStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreyStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(227, 230, 182)));
		accountingGreyStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreyStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingGreyStyle);
		styles.put(STYLE_GREY_CURRENCY, accountingGreyStyle);
		
		XSSFCellStyle accountingBlueStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont accountingBlueFont = (XSSFFont)wb.createFont();
		accountingBlueFont.setFontName(fontName);
		accountingBlueStyle.setFont(accountingBlueFont);
		accountingBlueStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingBlueStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		accountingBlueStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingBlueStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingBlueStyle);
		styles.put(STYLE_BLUE_CURRENCY, accountingBlueStyle);
		
		XSSFCellStyle accountingOrangeStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont accountingOrangeFont = (XSSFFont)wb.createFont();
		accountingOrangeFont.setFontName(fontName);
		accountingOrangeStyle.setFont(accountingOrangeFont);
		accountingOrangeStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		accountingOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingOrangeStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingOrangeStyle);
		styles.put(STYLE_ORANGE_CURRENCY, accountingOrangeStyle);
		
		XSSFCellStyle accountingOrangeStyleBold = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont fontOrangeStyleBold = (XSSFFont)wb.createFont();
		fontOrangeStyleBold.setBold(true);
		fontOrangeStyleBold.setFontName(fontName);
		accountingOrangeStyleBold.setFont(accountingOrangeFont);
		accountingOrangeStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingOrangeStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		accountingOrangeStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingOrangeStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBorderDashForCell(accountingOrangeStyleBold);
		styles.put(STYLE_ORANGE_CURRENCY_BOLD, accountingOrangeStyleBold);
		
		XSSFCellStyle accountingOrangeStyleBold1 = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont fontOrangeStyleBold1 = (XSSFFont)wb.createFont();
		fontOrangeStyleBold1.setBold(true);
		fontOrangeStyleBold1.setFontName(fontName);
		accountingOrangeStyleBold1.setFont(fontOrangeStyleBold1);
		accountingOrangeStyleBold1.setAlignment(HorizontalAlignment.RIGHT);
		accountingOrangeStyleBold1.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		accountingOrangeStyleBold1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingOrangeStyleBold1.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(accountingOrangeStyleBold1);
		styles.put(STYLE_ORANGE_CURRENCY_BOLD_NORMAL, accountingOrangeStyleBold1);
		
		XSSFCellStyle accountingTotalStyle = (XSSFCellStyle)wb.createCellStyle();
		accountingTotalStyle.setAlignment(HorizontalAlignment.RIGHT);
		accountingTotalStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(147, 205, 221)));
		accountingTotalStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		XSSFFont accountingTotalFont = (XSSFFont)wb.createFont();
		accountingTotalFont.setFontName(fontName);
		accountingTotalFont.setBold(true);
		accountingTotalStyle.setFont(accountingTotalFont);
		accountingTotalStyle.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCellTotal(accountingTotalStyle);
		styles.put(STYLE_TOTAL_CURRENCY, accountingTotalStyle);
		
		
		XSSFCellStyle headerOrangeStyle = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerOrangeFont = (XSSFFont)wb.createFont();
		headerOrangeFont.setFontName(fontName);
		headerOrangeFont.setBold(true);
		headerOrangeStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeStyle.setFont(headerOrangeFont);
		headerOrangeStyle.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeStyle.setWrapText(true);
		setBorderForCell(headerOrangeStyle);		
		styles.put(STYLE_HEADER_ORANGE, headerOrangeStyle);
		
		XSSFCellStyle headerOrangeHO2_3Style = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont headerOrangeHO2_3Font = (XSSFFont)wb.createFont();
		headerOrangeHO2_3Font.setFontName(fontName);
		headerOrangeHO2_3Font.setBold(true);
		headerOrangeHO2_3Style.setFillForegroundColor(new XSSFColor(new java.awt.Color(252, 213, 180)));
		headerOrangeHO2_3Style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		headerOrangeHO2_3Style.setFont(headerOrangeHO2_3Font);
		headerOrangeHO2_3Style.setAlignment(HorizontalAlignment.CENTER);
		headerOrangeHO2_3Style.setVerticalAlignment(VerticalAlignment.CENTER);
		headerOrangeHO2_3Style.setWrapText(true);
		setBorderForCell_HO2_3(headerOrangeHO2_3Style);		
		styles.put(STYLE_HEADER_ORANGE_HO_2_3, headerOrangeHO2_3Style);
		
		XSSFCellStyle textLeftBorderDashStyleBold = (XSSFCellStyle)wb.createCellStyle();
		textLeftBorderDashStyleBold.setAlignment(HorizontalAlignment.LEFT);
		textLeftBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, fontName,11, textLeftBorderDashStyleBold);
		setBorderDashForCell(textLeftBorderDashStyleBold);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_LEFT_BOLD, textLeftBorderDashStyleBold);
		
		XSSFCellStyle textRightBorderDashStyleBold = (XSSFCellStyle)wb.createCellStyle();
		textRightBorderDashStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		textRightBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, fontName,11, textRightBorderDashStyleBold);
		setBorderDashForCell(textRightBorderDashStyleBold);
		styles.put(STYLE_TEXT_BORDER_DASH_ALIGN_RIGHT_BOLD, textRightBorderDashStyleBold);

		XSSFCellStyle percentRightBorderDashStyleBold = (XSSFCellStyle)wb.createCellStyle();
		percentRightBorderDashStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		percentRightBorderDashStyleBold.setDataFormat(fmt.getFormat("@"));
		setBoldFont(wb, fontName,11, percentRightBorderDashStyleBold);
		setBorderDashForCell(percentRightBorderDashStyleBold);
		percentRightBorderDashStyleBold.setDataFormat(fmt.getFormat("0.0%"));
		styles.put(STYLE_PERCENT_BORDER_DASH_ALIGN_RIGHT_BOLD, percentRightBorderDashStyleBold);
		
		XSSFCellStyle accountingGreenStyleBold = (XSSFCellStyle)wb.createCellStyle();
		accountingGreenStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreenStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(139, 231, 150)));
		accountingGreenStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreenStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingGreenStyleBold);
		setBorderDashForCell(accountingGreenStyleBold);
		styles.put(STYLE_GREEN_CURRENCY_BOLD, accountingGreenStyleBold);
		
		XSSFCellStyle accountingGreenStyleBold1 = (XSSFCellStyle)wb.createCellStyle();
		accountingGreenStyleBold1.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreenStyleBold1.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 208, 80)));
		accountingGreenStyleBold1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreenStyleBold1.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingGreenStyleBold1);
		setBorderForCell(accountingGreenStyleBold1);
		styles.put(STYLE_GREEN_CURRENCY_BOLD_NORMAL, accountingGreenStyleBold1);
		
		XSSFCellStyle accountingDarkOrganeBold = (XSSFCellStyle)wb.createCellStyle();
		accountingDarkOrganeBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingDarkOrganeBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(247, 150, 70)));
		accountingDarkOrganeBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingDarkOrganeBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingDarkOrganeBold);
		setBorderForCell(accountingDarkOrganeBold);
		styles.put(STYLE_DARK_ORANGE_CURRENCY_BOLD, accountingDarkOrganeBold);
		
		XSSFCellStyle accountingYellowStyleBold = (XSSFCellStyle)wb.createCellStyle();
		accountingYellowStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingYellowStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 153)));
		accountingYellowStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingYellowStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, null,11, accountingYellowStyleBold);
		setBorderDashForCell(accountingYellowStyleBold);
		styles.put(STYLE_YELLOW_CURRENCY_BOLD, accountingYellowStyleBold);
		
		XSSFCellStyle accountingGreyStyleBold = (XSSFCellStyle)wb.createCellStyle();
		accountingGreyStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingGreyStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(227, 230, 182)));
		accountingGreyStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingGreyStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingGreyStyleBold);
		setBorderDashForCell(accountingGreyStyleBold);
		styles.put(STYLE_GREY_CURRENCY_BOLD, accountingGreyStyleBold);
		
		XSSFCellStyle accountingBlueStyleBold = (XSSFCellStyle)wb.createCellStyle();
		accountingBlueStyleBold.setAlignment(HorizontalAlignment.RIGHT);
		accountingBlueStyleBold.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 255, 255)));
		accountingBlueStyleBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingBlueStyleBold.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingBlueStyleBold);
		setBorderDashForCell(accountingBlueStyleBold);
		styles.put(STYLE_BLUE_CURRENCY_BOLD, accountingBlueStyleBold);
		
		XSSFCellStyle accountingBlueStyleBold1 = (XSSFCellStyle)wb.createCellStyle();
		accountingBlueStyleBold1.setAlignment(HorizontalAlignment.RIGHT);
		accountingBlueStyleBold1.setFillForegroundColor(new XSSFColor(new java.awt.Color(49, 132, 155)));
		accountingBlueStyleBold1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		accountingBlueStyleBold1.setDataFormat(fmt.getFormat("#,###0"));
		setBoldFont(wb, fontName,11, accountingBlueStyleBold1);
		setBorderForCell(accountingBlueStyleBold1);
		styles.put(STYLE_BLUE_CURRENCY_BOLD_NORMAL, accountingBlueStyleBold1);
		
		//TEXT_ALIGN_LEFT
		XSSFCellStyle group1 = (XSSFCellStyle)wb.createCellStyle();
		group1.setAlignment(HorizontalAlignment.LEFT);
		group1.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group1.setDataFormat(fmt.getFormat("@"));
		group1.setFont(boldFont);
		setBorderForCell(group1);
		styles.put(STYLE_TEXT_GROUP_LEVEL_1, group1);
		
		XSSFCellStyle normalGroup1 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup1.setAlignment(HorizontalAlignment.LEFT);
		normalGroup1.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		normalGroup1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup1.setDataFormat(fmt.getFormat("@"));
		setBorderForCell(normalGroup1);
		normalGroup1.setFont(font);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_1, normalGroup1);
		
		XSSFCellStyle group2 = (XSSFCellStyle)wb.createCellStyle();
		group2.setAlignment(HorizontalAlignment.LEFT);
		group2.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group2.setDataFormat(fmt.getFormat("@"));
		group2.setFont(boldFont);
		setBorderForCell(group2);
		styles.put(STYLE_TEXT_GROUP_LEVEL_2, group2);
		
		XSSFCellStyle normalGroup2 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup2.setAlignment(HorizontalAlignment.LEFT);
		normalGroup2.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		normalGroup2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup2.setDataFormat(fmt.getFormat("@"));
		normalGroup2.setFont(font);
		setBorderForCell(normalGroup2);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_2, normalGroup2);
		
		XSSFCellStyle group3 = (XSSFCellStyle)wb.createCellStyle();
		group3.setAlignment(HorizontalAlignment.LEFT);
		group3.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group3.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group3.setDataFormat(fmt.getFormat("@"));
		group3.setFont(boldFont);
		setBorderForCell(group3);
		styles.put(STYLE_TEXT_GROUP_LEVEL_3, group3);
		
		XSSFCellStyle normalGroup3 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup3.setAlignment(HorizontalAlignment.LEFT);
		normalGroup3.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		normalGroup3.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup3.setDataFormat(fmt.getFormat("@"));
		normalGroup3.setFont(font);
		setBorderForCell(normalGroup3);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_3, normalGroup3);
		
		XSSFCellStyle group4 = (XSSFCellStyle)wb.createCellStyle();
		group4.setAlignment(HorizontalAlignment.LEFT);
		group4.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group4.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group4.setDataFormat(fmt.getFormat("@"));
		group4.setFont(boldFont);
		setBorderForCell(group4);
		styles.put(STYLE_TEXT_GROUP_LEVEL_4, group4);
		
		XSSFCellStyle normalGroup4 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup4.setAlignment(HorizontalAlignment.LEFT);
		normalGroup4.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		normalGroup4.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup4.setDataFormat(fmt.getFormat("@"));
		normalGroup4.setFont(font);
		setBorderForCell(normalGroup4);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_4, normalGroup4);
		
		XSSFCellStyle group5 = (XSSFCellStyle)wb.createCellStyle();
		group5.setAlignment(HorizontalAlignment.LEFT);
		group5.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group5.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group5.setDataFormat(fmt.getFormat("@"));
		group5.setFont(boldFont);
		setBorderForCell(group5);
		styles.put(STYLE_TEXT_GROUP_LEVEL_5, group5);
		
		XSSFCellStyle normalGroup5 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup5.setAlignment(HorizontalAlignment.LEFT);
		normalGroup5.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		normalGroup5.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup5.setDataFormat(fmt.getFormat("@"));
		normalGroup5.setFont(font);
		setBorderForCell(normalGroup5);
		styles.put(STYLE_TEXT_NORMAL_GROUP_LEVEL_5, normalGroup5);
		
		//TEXT_ALIGN_CENTER
		XSSFCellStyle group6 = (XSSFCellStyle)wb.createCellStyle();
		group6.setAlignment(HorizontalAlignment.CENTER);
		group6.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group6.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group6.setDataFormat(fmt.getFormat("@"));
		group6.setFont(boldFont);
		setBorderForCell(group6);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_1, group6);
		
		XSSFCellStyle group7 = (XSSFCellStyle)wb.createCellStyle();
		group7.setAlignment(HorizontalAlignment.CENTER);
		group7.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group7.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group7.setDataFormat(fmt.getFormat("@"));
		group7.setFont(boldFont);
		setBorderForCell(group7);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_2, group7);
		
		XSSFCellStyle group8 = (XSSFCellStyle)wb.createCellStyle();
		group8.setAlignment(HorizontalAlignment.CENTER);
		group8.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group8.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group8.setDataFormat(fmt.getFormat("@"));
		group8.setFont(boldFont);
		setBorderForCell(group8);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_3, group8);
		
		XSSFCellStyle group9 = (XSSFCellStyle)wb.createCellStyle();
		group9.setAlignment(HorizontalAlignment.CENTER);
		group9.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group9.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group9.setDataFormat(fmt.getFormat("@"));
		group9.setFont(boldFont);
		setBorderForCell(group9);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_4, group9);
		
		XSSFCellStyle group10 = (XSSFCellStyle)wb.createCellStyle();
		group10.setAlignment(HorizontalAlignment.CENTER);
		group10.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group10.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group10.setDataFormat(fmt.getFormat("@"));
		group10.setFont(boldFont);
		setBorderForCell(group10);
		styles.put(STYLE_TEXT_CENTER_GROUP_LEVEL_5, group10);
		
		//GROUP_PERCENT
		XSSFCellStyle group11 = (XSSFCellStyle)wb.createCellStyle();
		group11.setAlignment(HorizontalAlignment.RIGHT);
		group11.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group11.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group11.setDataFormat(fmt.getFormat("0.0%"));
		group11.setFont(boldFont);
		setBorderForCell(group11);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_1, group11);
		
		XSSFCellStyle group12 = (XSSFCellStyle)wb.createCellStyle();
		group12.setAlignment(HorizontalAlignment.RIGHT);
		group12.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group12.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group12.setDataFormat(fmt.getFormat("0.0%"));
		group12.setFont(boldFont);
		setBorderForCell(group12);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_2, group12);
		
		XSSFCellStyle group13 = (XSSFCellStyle)wb.createCellStyle();
		group13.setAlignment(HorizontalAlignment.RIGHT);
		group13.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group13.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group13.setDataFormat(fmt.getFormat("0.0%"));
		group13.setFont(boldFont);
		setBorderForCell(group13);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_3, group13);
		
		XSSFCellStyle group14 = (XSSFCellStyle)wb.createCellStyle();
		group14.setAlignment(HorizontalAlignment.RIGHT);
		group14.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group14.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group14.setDataFormat(fmt.getFormat("0.0%"));
		group14.setFont(boldFont);
		setBorderForCell(group14);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_4, group14);
		
		XSSFCellStyle group15 = (XSSFCellStyle)wb.createCellStyle();
		group15.setAlignment(HorizontalAlignment.RIGHT);
		group15.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group15.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group15.setDataFormat(fmt.getFormat("0.0%"));
		group15.setFont(boldFont);
		setBorderForCell(group15);
		styles.put(STYLE_PERCENT_GROUP_LEVEL_5, group15);
		
		//GROUP_ACCOUNTING
		XSSFCellStyle group16 = (XSSFCellStyle)wb.createCellStyle();
		group16.setAlignment(HorizontalAlignment.RIGHT);
		group16.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group16.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group16.setDataFormat(fmt.getFormat("#,###0.00"));
		group16.setFont(boldFont);
		setBorderForCell(group16);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_1, group16);
		
		XSSFCellStyle group17 = (XSSFCellStyle)wb.createCellStyle();
		group17.setAlignment(HorizontalAlignment.RIGHT);
		group17.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group17.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group17.setDataFormat(fmt.getFormat("#,###0.00"));
		group17.setFont(boldFont);
		setBorderForCell(group17);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_2, group17);
		
		XSSFCellStyle group18 = (XSSFCellStyle)wb.createCellStyle();
		group18.setAlignment(HorizontalAlignment.RIGHT);
		group18.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group18.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group18.setDataFormat(fmt.getFormat("#,###0.00"));
		group18.setFont(boldFont);
		setBorderForCell(group18);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_3, group18);
		
		XSSFCellStyle group19 = (XSSFCellStyle)wb.createCellStyle();
		group19.setAlignment(HorizontalAlignment.RIGHT);
		group19.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group19.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group19.setDataFormat(fmt.getFormat("#,###0.00"));
		group19.setFont(boldFont);
		setBorderForCell(group19);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_4, group19);
		
		XSSFCellStyle group20 = (XSSFCellStyle)wb.createCellStyle();
		group20.setAlignment(HorizontalAlignment.RIGHT);
		group20.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group20.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group20.setDataFormat(fmt.getFormat("#,###0.00"));
		group20.setFont(boldFont);
		setBorderForCell(group20);
		styles.put(STYLE_ACCOUNTING_GROUP_LEVEL_5, group20);
		
		//GROUP_CURRENCY
		XSSFCellStyle group21 = (XSSFCellStyle)wb.createCellStyle();
		group21.setAlignment(HorizontalAlignment.RIGHT);
		group21.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		group21.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group21.setDataFormat(fmt.getFormat("#,###0"));
		group21.setFont(boldFont);
		setBorderForCell(group21);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_1, group21);
		
		XSSFCellStyle group22 = (XSSFCellStyle)wb.createCellStyle();
		group22.setAlignment(HorizontalAlignment.RIGHT);
		group22.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		group22.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group22.setDataFormat(fmt.getFormat("#,###0"));
		group22.setFont(boldFont);
		setBorderForCell(group22);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_2, group22);
		
		XSSFCellStyle group23 = (XSSFCellStyle)wb.createCellStyle();
		group23.setAlignment(HorizontalAlignment.RIGHT);
		group23.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		group23.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group23.setDataFormat(fmt.getFormat("#,###0"));
		group23.setFont(boldFont);
		setBorderForCell(group23);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_3, group23);
		
		XSSFCellStyle group24 = (XSSFCellStyle)wb.createCellStyle();
		group24.setAlignment(HorizontalAlignment.RIGHT);
		group24.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		group24.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group24.setDataFormat(fmt.getFormat("#,###0"));
		group24.setFont(boldFont);
		setBorderForCell(group24);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_4, group24);
		
		XSSFCellStyle group25 = (XSSFCellStyle)wb.createCellStyle();
		group25.setAlignment(HorizontalAlignment.RIGHT);
		group25.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		group25.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		group25.setDataFormat(fmt.getFormat("#,###0"));
		group25.setFont(boldFont);
		setBorderForCell(group25);
		styles.put(STYLE_CURRENCY_GROUP_LEVEL_5, group25);
		
		XSSFCellStyle normalGroup21 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup21.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup21.setFillForegroundColor(new XSSFColor(new java.awt.Color(197, 217, 241)));
		normalGroup21.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup21.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup21);
		normalGroup21.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_1, normalGroup21);

		XSSFCellStyle normalGroup22 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup22.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup22.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
		normalGroup22.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup22.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup22);
		normalGroup22.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_2, normalGroup22);

		XSSFCellStyle normalGroup23 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup23.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup23.setFillForegroundColor(new XSSFColor(new java.awt.Color(83, 141, 213)));
		normalGroup23.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup23.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup23);
		normalGroup23.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_3, normalGroup23);

		XSSFCellStyle normalGroup24 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup24.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup24.setFillForegroundColor(new XSSFColor(new java.awt.Color(146, 205, 220)));
		normalGroup24.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup24.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup24);
		normalGroup24.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_4, normalGroup24);

		XSSFCellStyle normalGroup25 = (XSSFCellStyle)wb.createCellStyle();
		normalGroup25.setAlignment(HorizontalAlignment.RIGHT);
		normalGroup25.setFillForegroundColor(new XSSFColor(new java.awt.Color(221, 217, 196)));
		normalGroup25.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		normalGroup25.setDataFormat(fmt.getFormat("#,###0"));
		setBorderForCell(normalGroup25);
		normalGroup25.setFont(font);
		styles.put(STYLE_CURRENCY_NORMAL_GROUP_LEVEL_5, normalGroup25);
		
		XSSFCellStyle textRed = (XSSFCellStyle)wb.createCellStyle();
		XSSFFont textRedFont = (XSSFFont)wb.createFont();
		textRedFont.setFontName(fontName);
		textRedFont.setFontHeight(12);
		textRedFont.setColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
		textRed.setAlignment(HorizontalAlignment.LEFT);
		textRed.setFont(textRedFont);	
		setBorderForCell(textRed);
		styles.put(STYLE_TEXT_RED_LEFT, textRed);
		
		return styles;
	}

	
	public static void setBorderForCell(XSSFCellStyle style){
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderBottom(BorderStyle.THIN);
	}
	
	public static void setBorderForCellNote(XSSFCellStyle style){
		style.setBorderBottom(BorderStyle.DASHED);
		style.setBorderColor(BorderSide.BOTTOM, new XSSFColor(new java.awt.Color(0, 176, 240)));
	}
	
	public static void setBorderForCellTotal(XSSFCellStyle style){
		style.setBorderTop(BorderStyle.DASHED);
		style.setBorderColor(BorderSide.TOP, new XSSFColor(new java.awt.Color(0, 176, 240)));
	}
	
	public static void setBorderForCell_HO2_3(XSSFCellStyle style){
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderBottom(BorderStyle.DASHED);
		style.setBorderColor(BorderSide.BOTTOM, new XSSFColor(new java.awt.Color(0, 176, 240)));
	}
	
	public static void setBorderDashForCell(XSSFCellStyle style){
		style.setBorderLeft(BorderStyle.DASHED);
		style.setBorderTop(BorderStyle.DASHED);
		style.setBorderRight(BorderStyle.DASHED);
		style.setBorderBottom(BorderStyle.DASHED);
		style.setBorderColor(BorderSide.BOTTOM, new XSSFColor(new java.awt.Color(0, 176, 240)));
		style.setBorderColor(BorderSide.LEFT, new XSSFColor(new java.awt.Color(0, 176, 240)));
		style.setBorderColor(BorderSide.RIGHT, new XSSFColor(new java.awt.Color(0, 176, 240)));
		style.setBorderColor(BorderSide.TOP, new XSSFColor(new java.awt.Color(0, 176, 240)));
	}
	
	public static void setBoldFont(XSSFWorkbook wb, String fontName, double fontHeight, XSSFCellStyle style){
		XSSFFont font = wb.createFont();
		if(StringUtil.isNullOrEmpty(fontName)){
			font.setFontName("Arial");
		}else{
			font.setFontName(fontName);
		}
		font.setFontHeight(fontHeight);
		font.setBold(true);
		style.setFont(font);
	}
	
	public static void setBoldFont(SXSSFWorkbook wb, String fontName, double fontHeight, XSSFCellStyle style){
		XSSFFont font = (XSSFFont)wb.createFont();
		if(StringUtil.isNullOrEmpty(fontName)){
			font.setFontName("Arial");
		}else{
			font.setFontName(fontName);
		}
		font.setFontHeight(fontHeight);
		font.setBold(true);
		style.setFont(font);
	}
	
	public static void setBorderThickForCell(XSSFCellStyle style){
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THICK);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderBottom(BorderStyle.THICK);
	}
	/**
	 * Substitute.
	 *
	 * @param zipfile the zipfile
	 * @param tmpfile the tmpfile
	 * @param entry the entry
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public static void substitute(File zipfile, File tmpfile, String entry,
			OutputStream out) throws IOException {
		ZipFile zip = new ZipFile(zipfile);

		ZipOutputStream zos = new ZipOutputStream(out);

		@SuppressWarnings("unchecked")
		Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
		while (en.hasMoreElements()) {
			ZipEntry ze = en.nextElement();
			if (!ze.getName().equals(entry)) {
				zos.putNextEntry(new ZipEntry(ze.getName()));
				InputStream is = zip.getInputStream(ze);
				copyStream(is, zos);
				is.close();
				zos.closeEntry();
			}
		}
		zos.putNextEntry(new ZipEntry(entry));
		InputStream is = new FileInputStream(tmpfile);
		copyStream(is, zos);
		is.close();
		zos.closeEntry();

		zos.close();
		zip.close();
		zipfile.delete();
	}

	
	/**
	 * Copy stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	private static void copyStream(InputStream in, OutputStream out)
			throws IOException {
		byte[] chunk = new byte[1024];
		int count;
		while ((count = in.read(chunk)) >= 0) {
			out.write(chunk, 0, count);
		}
	}	
}

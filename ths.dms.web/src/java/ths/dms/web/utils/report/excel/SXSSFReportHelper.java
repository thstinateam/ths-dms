package ths.dms.web.utils.report.excel;

import java.io.FileOutputStream;
import java.math.BigDecimal;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.StringUtil;

/**
 * ho tro thao tac excel bang apache poi - sxssfworkbook
 * 
 * @author lacnv1
 * @since Feb 25, 2014
 */
public class SXSSFReportHelper {
	/**
	 * Tao cell du lieu
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void addCell(SXSSFSheet sheet, int colIdx, int rowIdx, String text, XSSFCellStyle format)
			throws Exception {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			row = (SXSSFRow)sheet.createRow(rowIdx);
		}
		SXSSFCell cell = (SXSSFCell)row.getCell(colIdx);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(colIdx);
		}
		cell.setCellStyle(format);
		if (text == null) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(text);
		}
	}
	
	/**
	 * Tao cell du lieu
	 * 
	 * @author lacnv1
	 * @since Mar 03, 2014
	 */
	public static void addFormulaCell(SXSSFSheet sheet, int colIdx, int rowIdx, String textFormula, XSSFCellStyle format)
			throws Exception {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			row = (SXSSFRow)sheet.createRow(rowIdx);
		}
		SXSSFCell cell = (SXSSFCell)row.getCell(colIdx);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(colIdx);
		}
		cell.setCellStyle(format);
		if (textFormula == null) {
			cell.setCellValue("");
		} else {
			cell.setCellFormula(textFormula);
		}
	}
	
	/**
	 * Tao cell du lieu
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void addNumberCell(SXSSFSheet sheet, int colIdx, int rowIdx, Double num, XSSFCellStyle format)
			throws Exception {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			row = (SXSSFRow)sheet.createRow(rowIdx);
		}
		SXSSFCell cell = (SXSSFCell)row.getCell(colIdx);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(colIdx);
		}
		cell.setCellStyle(format);
		if (num == null) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(num);
		}
	}
	
	/**
	 * Tao cell du lieu
	 * 
	 * @author lacnv1
	 * @since Mar 12, 2014
	 */
	public static void addNumberCell(SXSSFSheet sheet, int colIdx, int rowIdx, BigDecimal num, XSSFCellStyle format)
			throws Exception {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			row = (SXSSFRow)sheet.createRow(rowIdx);
		}
		SXSSFCell cell = (SXSSFCell)row.getCell(colIdx);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(colIdx);
		}
		cell.setCellStyle(format);
		if (num == null) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(num.doubleValue());
		}
	}
	
	/**
	 * Tao cell du lieu, neu null -> 0
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void addNumberOrZeroCell(SXSSFSheet sheet, int colIdx, int rowIdx, Double num, XSSFCellStyle format)
			throws Exception {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			row = (SXSSFRow)sheet.createRow(rowIdx);
		}
		SXSSFCell cell = (SXSSFCell)row.getCell(colIdx);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(colIdx);
		}
		cell.setCellStyle(format);
		if (num == null) {
			cell.setCellValue(0);
		} else {
			cell.setCellValue(num);
		}
	}
	
	/**
	 * Tao cell du lieu, neu 0 -> null
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void addNumberOrNullCell(SXSSFSheet sheet, int colIdx, int rowIdx, Double num, XSSFCellStyle format)
			throws Exception {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			row = (SXSSFRow)sheet.createRow(rowIdx);
		}
		SXSSFCell cell = (SXSSFCell)row.getCell(colIdx);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(colIdx);
		}
		cell.setCellStyle(format);
		if (num == null || num == 0) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(num);
		}
	}
	
	/**
	 * Tao cell du lieu
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void addIntegerCell(SXSSFSheet sheet, int colIdx, int rowIdx, Integer num, XSSFCellStyle format)
			throws Exception {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			row = (SXSSFRow)sheet.createRow(rowIdx);
		}
		SXSSFCell cell = (SXSSFCell)row.getCell(colIdx);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(colIdx);
		}
		cell.setCellStyle(format);
		if (num == null) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(num);
		}
	}
	
	/**
	 * Tao cell du lieu
	 * 
	 * @author lacnv1
	 * @since Apr 17, 2014
	 */
	public static void addIntegerCell(SXSSFSheet sheet, int colIdx, int rowIdx, Long num, XSSFCellStyle format)
			throws Exception {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			row = (SXSSFRow)sheet.createRow(rowIdx);
		}
		SXSSFCell cell = (SXSSFCell)row.getCell(colIdx);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(colIdx);
		}
		cell.setCellStyle(format);
		if (num == null) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(num);
		}
	}
	
	/**
	 * Kiem tra xem so thuc num co phan le thap phan khong
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 * @return true neu le, false neu chan
	 */
	public static boolean checkDecimal(Double num)
			throws Exception {
		if (num == null || num == 0) {
			return false;
		}
		return (num - Math.floor(num) != 0);
	}
	
	/**
	 * Tao cell du lieu va merge tren nhieu dong c1r1->c2r2
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void addMergeCells(SXSSFSheet sheet, int c1, int r1,
			int c2, int r2, String text, XSSFCellStyle format) {
		SXSSFRow row = getRow(sheet, r1);
		SXSSFCell cell = (SXSSFCell)row.getCell(c1);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(c1);
		}
		cell.setCellStyle(format);
		SXSSFCell cell1 = null;
		for (int i = c1 + 1; i <= c2; i++) {
			cell1 = (SXSSFCell)row.getCell(i);
			if (cell1 == null) {
				cell1 = (SXSSFCell)row.createCell(i);
			}
			cell1.setCellStyle(format);
		}
		for (int j = r1 + 1; j <= r2; j++) {
			row = getRow(sheet, j);
			for (int i = c1; i <= c2; i++) {
				cell1 = (SXSSFCell)row.getCell(i);
				if (cell1 == null) {
					cell1 = (SXSSFCell)row.createCell(i);
				}
				cell1.setCellStyle(format);
			}
		}
		if (text == null) {
			cell.setCellValue("");
		} else {
			cell.setCellValue(text);
		}
		sheet.addMergedRegion(new CellRangeAddress(r1, r2, c1, c2));
	}
	
	/**
	 * Tao cell du lieu va merge tren nhieu dong c1r1->c2r2
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void addNumberMergeCells(SXSSFSheet sheet, int c1, int r1,
			int c2, int r2, BigDecimal num, XSSFCellStyle format) {
		SXSSFRow row = getRow(sheet, r1);
		SXSSFCell cell = (SXSSFCell)row.getCell(c1);
		if (cell == null) {
			cell = (SXSSFCell)row.createCell(c1);
		}
		cell.setCellStyle(format);
		SXSSFCell cell1 = null;
		for (int i = c1 + 1; i <= c2; i++) {
			cell1 = (SXSSFCell)row.getCell(i);
			if (cell1 == null) {
				cell1 = (SXSSFCell)row.createCell(i);
			}
			cell1.setCellStyle(format);
		}
		for (int j = r1 + 1; j <= r2; j++) {
			row = getRow(sheet, j);
			for (int i = c1; i <= c2; i++) {
				cell1 = (SXSSFCell)row.getCell(i);
				if (cell1 == null) {
					cell1 = (SXSSFCell)row.createCell(i);
				}
				cell1.setCellStyle(format);
			}
		}
		if (num != null) {
			cell.setCellValue(num.doubleValue());
		}
		sheet.addMergedRegion(new CellRangeAddress(r1, r2, c1, c2));
	}
	
	/**
	 * Lay dong i tren sheet
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static SXSSFRow getRow(SXSSFSheet sheet, int rowIdx) {
		SXSSFRow row = (SXSSFRow)sheet.getRow(rowIdx);
		if (row == null) {
			return (SXSSFRow)sheet.createRow(rowIdx);
		}
		return row;
	}
	
	/**
	 * Lay ten file bao cao full
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static String getReportFileName(String rptName, String fileExtension) throws Exception {
		StringBuilder sbFileName = new StringBuilder(rptName)
			.append("_")
			.append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE))
			.append(fileExtension);
		StringUtil.replaceSeparatorChar(sbFileName.toString());
		return sbFileName.toString();
	}
	
	/**
	 * dat chieu rong cho cot
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void setColumnWidths(SXSSFSheet sheet, int startCol, short... widths)
				throws Exception {
		for (int i = 0, n = widths.length; i < n; i++) {
			sheet.setColumnWidth(i + startCol, widths[i] * 256);
		}
	}
	
	/**
	 * dat chieu rong cho cot
	 * 
	 * @author lacnv1
	 * @since Feb 25, 2014
	 */
	public static void setColumnWidthEx(SXSSFSheet sheet, int startCol, int num, short width)
				throws Exception {
		for (int i = 0; i < num; i++) {
			sheet.setColumnWidth(i + startCol, width * 256);
		}
	}
	
	/**
	 * dat chieu cao cho dong
	 * 
	 * @author lacnv1
	 * @since Feb 28, 2014
	 */
	public static void setRowHeights(SXSSFSheet sheet, int startRow, short... heights)
				throws Exception {
		SXSSFRow row = null;
		for (int i = 0, n = heights.length; i < n; i++) {
			row = (SXSSFRow)sheet.getRow(i + startRow);
			if (row == null) {
				row = (SXSSFRow)sheet.createRow(i + startRow);
			}
			row.setHeight((short)(heights[i] * 20));
		}
	}
	
	/**
	 * dat chieu cao cho dong
	 * 
	 * @author lacnv1
	 * @since Feb 28, 2014
	 */
	public static void setRowHeightEx(SXSSFSheet sheet, int startRow, int num, short height)
				throws Exception {
		SXSSFRow row = null;
		for (int i = 0; i < num; i++) {
			row = (SXSSFRow)sheet.getRow(i + startRow);
			if (row == null) {
				row = (SXSSFRow)sheet.createRow(i + startRow);
			}
			row.setHeight((short)(height * 20));
		}
	}
	
	/**
	 * Tao file bao cao xlsx tu sxssfworkbook
	 * 
	 * @param workbook
	 * @param fileName - ten file bao cao (khi xuat ra se gan them thoi gian vao phia sau + .xlsx)
	 * @param fileNameRes - chuoi key trong file resource, neu fileName null thi su dung de lay ten file bao cao trong resource
	 * 
	 * @author lacnv1
	 * @since Apr 07, 2015
	 * 
	 * @see exportXLSX(SXSSFWorkbook, String, String, String)
	 */
	public static String exportXLSX(SXSSFWorkbook workbook, String fileName, String fileNameRes)
			throws Exception {
		return exportXLSX(workbook, fileName, fileNameRes, null); // khong sua ham ma goi 1 ham khac de han che chinh sua nhung noi da su dung
	}
	
	/**
	 * Tao file bao cao xlsx tu sxssfworkbook
	 * 
	 * @param workbook
	 * @param fileName - ten file bao cao (khi xuat ra se gan them thoi gian vao phia sau + .xlsx) (uu tien 2)
	 * @param fileNameRes - chuoi key trong file resource, neu fileName null thi su dung de lay ten file bao cao trong resource
	 * @param exactFileName - ten file bao cao (ten chinh xac, khong cong them bat cu ki tu nao) (uu tien 1)
	 * 
	 * @author lacnv1
	 * @since Apr 08, 2015
	 */
	public static String exportXLSX(SXSSFWorkbook workbook, String fileName, String fileNameRes, String exactFileName)
			throws Exception {
		FileOutputStream out = null;
		
		try {
			if (workbook == null) {
				throw new IllegalArgumentException("workbook: null");
			}
			if (StringUtil.isNullOrEmpty(exactFileName)
					&& StringUtil.isNullOrEmpty(fileName) && StringUtil.isNullOrEmpty(fileNameRes)) {
				throw new IllegalArgumentException("exactFileName + fileName + fileNameRes: null");
			}
			if (StringUtil.isNullOrEmpty(exactFileName)) {
				if (StringUtil.isNullOrEmpty(fileName)) {
					fileName = R.getResource(fileNameRes);
				}
				fileName = SXSSFReportHelper.getReportFileName(fileName, FileExtension.XLSX.getValue());
			} else {
				fileName = exactFileName;
			}
			String realPath = Configuration.getStoreRealPath() + fileName;
			out = new FileOutputStream(realPath);
			//storedFilePath = realPath;
			workbook.write(out);
			return Configuration.getExportExcelPath() + fileName;
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (out != null) {
				out.close();
				out = null;
			}
			if (workbook != null) {
				workbook.dispose();
				workbook = null;
			}
		}
	}
}
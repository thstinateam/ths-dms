/**
 * 
 */
package ths.dms.web.utils.report.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Writer;

import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ths.dms.helper.Configuration;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;

/**
 * The Class ExcelReporter.
 *
 * @author hungtx
 * @since Aug 19, 2013
 */
public class ExcelReporter {
	
	/**
	 * Export.
	 *
	 * @param reportFileName the report file name
	 * @param wb the wb
	 * @param fw the fw
	 * @param tmp the tmp
	 * @return the string
	 * @author tientv11
	 * @since Aug 19, 2013
	 */
	/*public static String exportXLSX(String reportFileName, XSSFWorkbook wb, Writer fw,File tmp){		
		StringBuilder sbFileName = new StringBuilder("/").append(DateUtil.toDateString(
				DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_")
				.append(reportFileName).append(FileExtension.XLSX.getValue());
		StringUtil.replaceSeparatorChar(sbFileName.toString());
		String outputPath = Configuration.getStoreRealPath() + sbFileName.toString();
		String outputDownload = Configuration.getExportExcelPath() + sbFileName.toString();
		XSSFSheet sheet = wb.createSheet(reportFileName);
		
		// name of the zip entry holding sheet data, e.g.
		// /xl/worksheets/sheet1.xml
		String sheetRef = sheet.getPackagePart().getPartName().getName();
		String tmpFileName = "template_" + sbFileName.toString() + ".xlsx";
		try{
			fw.close();
			// save the template
			FileOutputStream os = new FileOutputStream(tmpFileName);
			wb.write(os);
			os.close();
			
			// Substitute the template entry with the generated data
			FileOutputStream out = new FileOutputStream(outputPath);
			ExcelProcessUtils.substitute(new File(tmpFileName), tmp, sheetRef.substring(1), out);			
			out.close();			
			tmp.delete();			
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		} 
		
		return outputDownload;
	}*/
	
	public static String exportXLSX(String reportFileName, XSSFWorkbook wb, Writer fw,File tmp){		
		StringBuilder sbFileName = new StringBuilder("/").append(DateUtil.toDateString(
				DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_")
				.append(reportFileName).append(FileExtension.XLSX.getValue());
		StringUtil.replaceSeparatorChar(sbFileName.toString());
		String outputPath = Configuration.getStoreRealPath() + sbFileName.toString();
		String outputDownload = Configuration.getExportExcelPath() + sbFileName.toString();
		XSSFSheet sheet = wb.createSheet(reportFileName);
		
		// name of the zip entry holding sheet data, e.g.
		// /xl/worksheets/sheet1.xml
		String sheetRef = sheet.getPackagePart().getPartName().getName();
		String tempFileName = "template_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + reportFileName + FileExtension.XLSX.getValue();
		try{
			fw.close();
			// save the template
			FileOutputStream os = new FileOutputStream(tempFileName);
			wb.write(os);
			os.close();
			
			// Substitute the template entry with the generated data
			FileOutputStream out = new FileOutputStream(outputPath);
			ExcelProcessUtils.substitute(new File(tempFileName), tmp, sheetRef.substring(1), out);			
			out.close();			
			tmp.delete();
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		} 
		
		return outputDownload;
	}
	
	/**
	 * Export Excel khong can qua chung thuc
	 * @author hunglm16
	 * @param reportFileName
	 * @param wb
	 * @param fw
	 * @param tmp
	 * @return
	 * @since 15/09/2015
	 */
	public static String exportXLSXNotAuthentication(String reportFileName, XSSFWorkbook wb, Writer fw, File tmp) {
		StringBuilder sbFileName = new StringBuilder("/").append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(reportFileName).append(FileExtension.XLSX.getValue());
		StringUtil.replaceSeparatorChar(sbFileName.toString());
		String outputPath = Configuration.getStoreImportDownloadPath() + sbFileName.toString();
		String outputDownload = Configuration.getStoreImportFailDownloadPath() + sbFileName.toString();
		XSSFSheet sheet = wb.createSheet(reportFileName);

		// name of the zip entry holding sheet data, e.g.
		// /xl/worksheets/sheet1.xml
		String sheetRef = sheet.getPackagePart().getPartName().getName();
		String tempFileName = "template_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + reportFileName + FileExtension.XLSX.getValue();
		try {
			fw.close();
			// save the template
			FileOutputStream os = new FileOutputStream(tempFileName);
			wb.write(os);
			os.close();

			// Substitute the template entry with the generated data
			FileOutputStream out = new FileOutputStream(outputPath);
			ExcelProcessUtils.substitute(new File(tempFileName), tmp, sheetRef.substring(1), out);
			out.close();
			tmp.delete();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}

		return outputDownload;
	}
	
	/**
	 * Export.
	 *
	 * @param reportFileName the report file name
	 * @param wb the wb
	 * @param fw the fw
	 * @param tmp the tmp
	 * @return the string
	 * @author tientv11	 
	 */
	@SuppressWarnings("deprecation")
	public static String exportXLSXMerging(String reportFileName, XSSFWorkbook wb,Writer fw,File tmp){		
		StringBuilder sbFileName = new StringBuilder("/").append(DateUtil.toDateString(
				DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_")
				.append(reportFileName).append(FileExtension.XLSX.getValue());
		StringUtil.replaceSeparatorChar(sbFileName.toString());
		String outputPath = Configuration.getStoreRealPath() + sbFileName.toString();
		String outputDownload = Configuration.getExportExcelPath() + sbFileName.toString();
		XSSFSheet sheet = wb.createSheet(reportFileName);		
		sheet.addMergedRegion(new CellRangeAddress(
				5, //first row (0-based)
	            6, //last row  (0-based)
	            0, //first column (0-based)
	             0//last column  (0-based)
	    ));
		String sheetRef = sheet.getPackagePart().getPartName().getName();
		try{
			fw.close();			
			FileOutputStream os = new FileOutputStream("template.xlsx");
			wb.write(os);
			os.close();
			
			// Substitute the template entry with the generated data
			FileOutputStream out = new FileOutputStream(outputPath);
			ExcelProcessUtils.substitute(new File("template.xlsx"), tmp, sheetRef.substring(1), out);			
			out.close();			
			tmp.delete();
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		} 
		
		return outputDownload;
	}

}

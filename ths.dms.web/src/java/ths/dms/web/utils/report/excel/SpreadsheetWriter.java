/**
 * 
 */
package ths.dms.web.utils.report.excel;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellReference;

/**
 * Writes spreadsheet data in a Writer.
 *
 * @author hungtx
 * @since Aug 19, 2013
 */
/*public class SpreadsheetWriter {	
	private final Writer _out;
	private int _rownum;

	public SpreadsheetWriter(Writer out) {
		_out = out;
	}

	*//**
	 * Begin sheet.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void beginSheet(double... columnWidth) throws IOException {
		_out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">" +
				"<sheetViews><sheetView showGridLines=\"0\" /></sheetViews>");
		if(columnWidth!= null && columnWidth.length > 0){
			_out.write("<cols>\n");
			for(int i=0;i<columnWidth.length;i++){
				if(columnWidth[i] > 0){
					_out.write("<col min=\""+ (i+1) +"\" max=\""+ (i+1) +"\" width=\""+ String.valueOf(columnWidth[i]) +"\" customWidth=\"1\"/>");
				}
			}
			_out.write("</cols>\n");
		}
		_out.write("<sheetData>\n");
	}
	
	*//**
	 * @author lacnv1
	 * @throws IOException
	 * begin sheet and set not show gridline
	 *//*
	public void beginSheetEx(int sheetId, double... columnWidths) throws IOException {
		StringBuilder str = new StringBuilder();
		str.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		str.append("<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\"");
		str.append(" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">");
		str.append("<sheetViews><sheetView showGridLines=\"0\" workbookViewId=\"");
		str.append(Integer.toString(sheetId));
		str.append("\"/></sheetViews>");
		str.append("<sheetFormatPr defaultRowHeight=\"13\"/>");
		
		if(columnWidths != null && columnWidths.length > 0){
			str.append("<cols>");
			for(int i=0; i<columnWidths.length; i++){
				if(columnWidths[i] > 0){
					str.append("<col min=\"" + (i+1) + "\" max=\"" + (i+1) + "\" width=\"" +
							String.valueOf(columnWidths[i]) +"\" customWidth=\"1\"/>");
				}
			}
			str.append("</cols>");
		}
		
		str.append("<sheetData>");
		_out.write(str.toString());
	}

	*//**
	 * End sheet.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void endSheet() throws IOException {
		_out.write("</sheetData>");
		_out.write("</worksheet>");
	}

	*//**
	 * Insert a new row.
	 *
	 * @param rownum 0-based row number
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void insertRow(int rownum) throws IOException {
		_out.write("<row r=\"" + (rownum + 1) + "\">\n");
		this._rownum = rownum;
	}

	
	
	*//**
	 * Insert row with outline.
	 *
	 * @param rownum the rownum
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author tientv
	 * @since Aug 19, 2013
	 *//*
	public void mergeRowSpans (int rownum) throws IOException {
		_out.write("<row r=\"" + (rownum + 1) + "\"  spans=\""+ (rownum + 1) +":8\" >\n");
		this._rownum = rownum;		
	}
	
	*//**
	 * Insert row with outline.
	 *
	 * @param rownum the rownum
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void insertRowWithOutline(int rownum, int level) throws IOException {
		_out.write("<row r=\"" + (rownum + 1) + "\" outlineLevel=\"" + level + "\">\n");
		this._rownum = rownum;
	}

	*//**
	 * Insert row end marker.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void endRow() throws IOException {
		_out.write("</row>\n");
	}

	*//**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @param styleIndex the style index
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void createCell(int columnIndex, String value, int styleIndex, XLSXCellType cellType)
			throws IOException {
		String ref = new CellReference(_rownum, columnIndex).formatAsString();
		
		if(cellType!= null){
			_out.write("<c r=\"" + ref + "\" t=\""+ cellType.getValue() +"\"");
		} else {
			_out.write("<c r=\"" + ref + "\" t=\"inlineStr\"");
		}		
		if (styleIndex != -1){
			_out.write(" s=\"" + styleIndex + "\"");
		}
		_out.write(">");
		if(cellType == null || (cellType!= null && XLSXCellType.INLINESTR.equals(cellType))){
			_out.write("<is><t>" + value + "</t></is>");
		} else{
			_out.write("<v>" + value + "</v>");
		}		
		_out.write("</c>");
	}
	
	*//**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @param styleIndex the style index
	 * @author hungtx
	 * @since Aug 20, 2013
	 *//*
	public void createCell(int columnIndex, String value, int styleIndex) throws IOException{
		createCell(columnIndex,value,styleIndex,null);
	}

	*//**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void createCell(int columnIndex, String value)
			throws IOException {
		createCell(columnIndex, value, -1);
	}

	*//**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @param styleIndex the style index
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void createCell(int columnIndex, double value, int styleIndex)
			throws IOException {
		String ref = new CellReference(_rownum, columnIndex)
				.formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"n\"");
		if (styleIndex != -1){
			_out.write(" s=\"" + styleIndex + "\"");
		}
		_out.write(">");
		_out.write("<v>" + value + "</v>");
		_out.write("</c>");
	}

	*//**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void createCell(int columnIndex, double value)
			throws IOException {
		createCell(columnIndex, value, -1);
	}

	*//**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @param styleIndex the style index
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void createCell(int columnIndex, Calendar value, int styleIndex)
			throws IOException {
		createCell(columnIndex, DateUtil.getExcelDate(value, false),
				styleIndex);
	}
	
	*//**
	 * Creates the empty row.
	 *
	 * @author hungtx
	 * @since Aug 19, 2013
	 *//*
	public void createEmptyRow(int rownum) throws IOException{
		insertRow(rownum);
		endRow();
	}
	
	
	*//**
	 * return cells that shall be merged as string
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author nhanlt
	 * @since Oct 22, 2013
	 *//*
	public String returnMergeCell(int row1, int column1,int row2, int column2) throws IOException {
		String refFrom = new CellReference(row1, column1).formatAsString();
		String refTo = new CellReference(row2, column2).formatAsString();
		return "<mergeCell ref=\"" + refFrom+":"+ refTo+"\" />\n";
	}
	
	
	
	*//**
	 * End sheet with merge cells.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author nhanlt
	 * @since Oct 22, 2013
	 *//*
	public void endSheetWithMergeCells(List<String> lstCellMerged) throws IOException {
		_out.write("</sheetData>");
		if (lstCellMerged.size() > 0) {
			_out.write("<mergeCells count=\"" + lstCellMerged.size() +"\">\n");
			for (int i = 0;i < lstCellMerged.size();i++) {
				_out.write(lstCellMerged.get(i));
			}
			_out.write(" </mergeCells>\n");
		}
		_out.write("</worksheet>");
	}

}
*/

/**
 * trungtm6 merge source from AFC on 21/07/2015
 *
 */
public class SpreadsheetWriter {	
	
	/** The _out. */
	private final Writer _out;
	
	/** The _rownum. */
	private int _rownum;

	/**
	 * Instantiates a new spreadsheet writer.
	 *
	 * @param out the out
	 */
	public SpreadsheetWriter(Writer out) {
		_out = out;
	}

	/**
	 * Begin sheet.
	 *
	 * @param columnWidth the column width
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void beginSheet(double... columnWidth) throws IOException {
		_out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">");
		if(columnWidth!= null && columnWidth.length > 0){
			_out.write("<cols>\n");
			for(int i=0;i<columnWidth.length;i++){
				if(columnWidth[i] > 0){
					_out.write("<col min=\""+ (i+1) +"\" max=\""+ (i+1) +"\" width=\""+ String.valueOf(columnWidth[i]) +"\" customWidth=\"1\"/>");
				}
			}
			_out.write("</cols>\n");
		}
		_out.write("<sheetData>\n");
	}
	
	/**
	 * Begin sheet with group column.
	 *
	 * @param size the size
	 * @param columnWidth the column width
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author lamnh6
	 * @since May 12, 2014
	 */
	public void beginSheetWithGroupColumn(int size,double... columnWidth) throws IOException {
		_out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">");
		if(columnWidth!= null && columnWidth.length > 0){
			_out.write("<cols>\n");
			int length = columnWidth.length;
			for(int i=0;i<length;i++){
				if(columnWidth[i] >= 0){
					_out.write("<col min=\""+ (i+1) +"\" max=\""+ (i+1) +"\" width=\""+ String.valueOf(columnWidth[i]) +"\" customWidth=\"1\"/>");
				}
			}
			_out.write("<col outlineLevel=\"1\" min=\""+ (length) +"\" max=\""+ (length + size) +"\" width=\"25\" customWidth=\"1\"/>");
			_out.write("<col collapsed=\"1\" min=\""+ (length+size+1) +"\" max=\""+ (length+size+1) +"\" width=\"25\" customWidth=\"1\"/>");
			length = length+size+2;
			_out.write("<col outlineLevel=\"1\" min=\""+ (length) +"\" max=\""+ (length + size) +"\" width=\"25\" customWidth=\"1\"/>");
			_out.write("<col collapsed=\"1\" min=\""+ (length+size+1) +"\" max=\""+ (length+size+1) +"\" width=\"25\" customWidth=\"1\"/>");
			length = length+size+2;
			_out.write("<col outlineLevel=\"1\" min=\""+ (length) +"\" max=\""+ (length + size) +"\" width=\"25\" customWidth=\"1\"/>");
			_out.write("<col collapsed=\"1\" min=\""+ (length+size+1) +"\" max=\""+ (length+size+1) +"\" width=\"25\" customWidth=\"1\"/>");
			_out.write("</cols>\n");
		}
		_out.write("<sheetData>\n");
	}

	/**
	 * End sheet.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void endSheet() throws IOException {
		_out.write("</sheetData>");
		_out.write("</worksheet>");
	}
	
	

	/**
	 * return cells that shall be merged as string.
	 *
	 * @param row1 the row1
	 * @param column1 the column1
	 * @param row2 the row2
	 * @param column2 the column2
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author nhanlt
	 * @since Oct 22, 2013
	 */
	public String returnMergeCell(int row1, int column1,int row2, int column2) throws IOException {
		String refFrom = new CellReference(row1, column1).formatAsString();
		String refTo = new CellReference(row2, column2).formatAsString();
		return "<mergeCell ref=\"" + refFrom+":"+ refTo+"\" />\n";
	}
	
	
	
	/**
	 * End sheet with merge cells.
	 *
	 * @param lstCellMerged the lst cell merged
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author nhanlt
	 * @since Oct 22, 2013
	 */
	public void endSheetWithMergeCells(List<String> lstCellMerged) throws IOException {
		_out.write("</sheetData>");
		if (lstCellMerged.size() > 0) {
			_out.write("<mergeCells count=\"" + lstCellMerged.size() +"\">\n");
			for (int i = 0;i < lstCellMerged.size();i++) {
				_out.write(lstCellMerged.get(i));
			}
			_out.write(" </mergeCells>\n");
		}
		_out.write("</worksheet>");
	}

	/**
	 * Insert a new row.
	 *
	 * @param rownum 0-based row number
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void insertRow(int rownum) throws IOException {
		_out.write("<row r=\"" + (rownum + 1) + "\">\n");
		this._rownum = rownum;
	}

	
	
	/**
	 * Insert row with outline.
	 *
	 * @param rownum the rownum
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author tientv
	 * @since Aug 19, 2013
	 */
	public void mergeRowSpans (int rownum) throws IOException {
		_out.write("<row r=\"" + (rownum + 1) + "\"  spans=\""+ (rownum + 1) +":8\" >\n");
		this._rownum = rownum;		
	}
	
	/**
	 * Insert row with outline.
	 *
	 * @param rownum the rownum
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void insertRowWithOutline(int rownum, int level) throws IOException {
		_out.write("<row r=\"" + (rownum + 1) + "\" outlineLevel=\"" + level + "\">\n");
		this._rownum = rownum;
	}

	/**
	 * Insert row end marker.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void endRow() throws IOException {
		_out.write("</row>\n");
	}

	/**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @param styleIndex the style index
	 * @param cellType the cell type
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void createCell(int columnIndex, String value, int styleIndex, XLSXCellType cellType)
			throws IOException {
		String ref = new CellReference(_rownum, columnIndex).formatAsString();
		
		if(cellType!= null){
			_out.write("<c r=\"" + ref + "\" t=\""+ cellType.getValue() +"\"");
		} else {
			_out.write("<c r=\"" + ref + "\" t=\"inlineStr\"");
		}		
		if (styleIndex != -1){
			_out.write(" s=\"" + styleIndex + "\"");
		}
		_out.write(">");
		if(cellType == null || (cellType!= null && XLSXCellType.INLINESTR.equals(cellType))){
			_out.write("<is><t>" + value + "</t></is>");
		} else{
			_out.write("<v>" + value + "</v>");
		}		
		_out.write("</c>");
	}
	

/**
 * Creates the cell.
 *
 * @param columnIndex the column index
 * @param value the value
 * @param styleIndex the style index
 * @param cellType the cell type
 * @throws IOException Signals that an I/O exception has occurred.
 */
public void createCell(int columnIndex, Object value, int styleIndex, XLSXCellType cellType)
	throws IOException {
String ref = new CellReference(_rownum, columnIndex).formatAsString();

if(cellType!= null){
	_out.write("<c r=\"" + ref + "\" t=\""+ cellType.getValue() +"\"");
} else {
	_out.write("<c r=\"" + ref + "\" t=\"inlineStr\"");
}		
if (styleIndex != -1){
	_out.write(" s=\"" + styleIndex + "\"");
}
_out.write(">");
if(cellType == null || (cellType!= null && XLSXCellType.INLINESTR.equals(cellType))){
	_out.write("<is><t>" + value + "</t></is>");
} else{
	_out.write("<v>" + value + "</v>");
}		
_out.write("</c>");
}
	
	/**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @param styleIndex the style index
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 20, 2013
	 */
	public void createCell(int columnIndex, String value, int styleIndex) throws IOException{
		createCell(columnIndex,value,styleIndex,null);
	}

	/**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void createCell(int columnIndex, String value)
			throws IOException {
		createCell(columnIndex, value, -1);
	}

	/**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @param styleIndex the style index
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void createCell(int columnIndex, double value, int styleIndex)
			throws IOException {
		String ref = new CellReference(_rownum, columnIndex)
				.formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"n\"");
		if (styleIndex != -1){
			_out.write(" s=\"" + styleIndex + "\"");
		}
		_out.write(">");
		_out.write("<v>" + value + "</v>");
		_out.write("</c>");
	}
	
	public void createCell(int columnIndex, BigDecimal value, int styleIndex)
	throws IOException {
		String ref = new CellReference(_rownum, columnIndex)
				.formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"n\"");
		if (styleIndex != -1){
			_out.write(" s=\"" + styleIndex + "\"");
		}
		_out.write(">");
		_out.write("<v>" + value + "</v>");
		_out.write("</c>");
		}
	
	public void createCell(int columnIndex, Float value, int styleIndex)
	throws IOException {
		String ref = new CellReference(_rownum, columnIndex)
				.formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"n\"");
		if (styleIndex != -1){
			_out.write(" s=\"" + styleIndex + "\"");
		}
		_out.write(">");
		_out.write("<v>" + value + "</v>");
		_out.write("</c>");
		}

	/**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void createCell(int columnIndex, double value)
			throws IOException {
		createCell(columnIndex, value, -1);
	}

	/**
	 * Creates the cell.
	 *
	 * @param columnIndex the column index
	 * @param value the value
	 * @param styleIndex the style index
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void createCell(int columnIndex, Calendar value, int styleIndex)
			throws IOException {
		createCell(columnIndex, DateUtil.getExcelDate(value, false),
				styleIndex);
	}
	
	/**
	 * Creates the empty row.
	 *
	 * @param rownum the rownum
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public void createEmptyRow(int rownum) throws IOException{
		insertRow(rownum);
		endRow();
	}
}

package ths.dms.web.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 
 * @author tuannd20
 *
 */
public class NumberUtil {
	/**
	 * Try parse an integer without throw exception
	 * @author tuannd20
	 * @param intStr
	 * @return null if exception happen, otherwise an Integer value
	 */
	public static Integer tryParseInteger(String intStr) {
		try{
			Integer val = Integer.parseInt(intStr);
			return val;
		} catch (NumberFormatException e){
			return null;
		}
	}
	
	/**
	 * Try parse an Long without throw exception
	 * @author trietptm
	 * @param longStr
	 * @return
	 * @since Apr 5, 2016
	 */
	public static Long tryParseLong(String longStr) {
		try{
			Long val = Long.parseLong(longStr);
			return val;
		} catch (NumberFormatException e){
			return null;
		}
	}
	
	/**
	 * Try parse an double without throw exception
	 * @author tuannd20
	 * @param intStr
	 * @return null if exception happen, otherwise an Double value
	 */
	public static Double tryParseDouble(String doubleStr) {
		try{
			Double val = Double.parseDouble(doubleStr);
			return val;
		} catch (NumberFormatException e){
			return null;
		}
	}
	
	
	public static Float tryParseFloat(String floatStr) {
		try{
			Float val = Float.parseFloat(floatStr);
			return val;
		} catch (NumberFormatException e){
			return null;
		}
	}
	
	/**
	 * Ap dung cho ca so luong am
	 * Xu ly convfactQuantityPackage
	 * @author vuongmq
	 * @param quantity
	 * @param convfact
	 * @return BigDecimal
	 * @since Dec 24, 2015
	 */
	public static BigDecimal convfactQuantityPackage(BigDecimal quantity, BigDecimal convfact) {
		BigDecimal result = BigDecimal.ZERO;
		if (quantity != null && convfact != null) {
			if (quantity.compareTo(BigDecimal.ZERO) < 0) {
				quantity = quantity.negate();
				result = quantity.divide(convfact, RoundingMode.FLOOR);
				result = result.negate();
			} else {
				result = quantity.divide(convfact, RoundingMode.FLOOR);
			}
		}
		return result;
	}
	
	/**
	 *  Ap dung cho ca so luong am
	 * Xu ly convfactQuantityRetail
	 * @author vuongmq
	 * @param quantity
	 * @param convfact
	 * @return BigDecimal
	 * @since Dec 24, 2015
	 */
	public static BigDecimal convfactQuantityRetail(BigDecimal quantity, BigDecimal convfact) {
		BigDecimal result = BigDecimal.ZERO;
		if (quantity != null && convfact != null) {
			if (quantity.compareTo(BigDecimal.ZERO) < 0) {
				quantity = quantity.negate();
				result = quantity.remainder(convfact);
				result = result.negate();
			} else {
				result = quantity.remainder(convfact);
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		BigDecimal quantity = new BigDecimal(-1606);
		BigDecimal convfact = new BigDecimal(48);
		BigDecimal thung = null;
		BigDecimal hop = null;
		thung = convfactQuantityPackage(quantity, convfact);
		hop = convfactQuantityRetail(quantity, convfact);
		System.out.println("thung:" + thung);
		System.out.println("hop:" + hop);
		Long a = -1606L;
		Long b = 48L;
		Long c = a.longValue() / b.longValue();
		Long d = a.longValue() % b.longValue();
		System.out.println("thung:" + c);
		System.out.println("hop:" + d);
	}
}

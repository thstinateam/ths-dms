/**
 * 
 */
package ths.dms.web.enumtype;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.StringUtil;


/**
 * @author hungtx
 *
 */
public enum ShopReportTemplate {
	/** MODULE : SALE PRODUCT */
	/** @see : Phieu xuat hang theo nhan vien ban hang: In phieu giao hang */
	SALE_PRODUCT_INPHIEUGH_NVGH("PXHTNVGH","sale-product/","PhieuGopGNvaTT"),	
	SALE_PRODUCT_INPHIEUGH_SR_LOT("PXHTLOT","sale-product/","pgnvtttdh_subreport_lot"),
	SALE_PRODUCT_INPHIEUGH_SR_CONVFACT("PXHTCONVFACT","sale-product/","pgnvtttdh_subreport_convfact"),
	SALE_PRODUCT_INPHIEUGH_SR_UOM1("PXHTUOM1","sale-product/","pgnvtttdh_subreport_uom1"),
	SALE_PRODUCT_INPHIEUGH_SR_UOM2("PXHTUOM2","sale-product/","pgnvtttdh_subreport_uom2"),
	SALE_PRODUCT_INPHIEUGH_SR_PRICE("PXHTPRICE","sale-product/","pgnvtttdh_subreport_price"),
	SALE_PRODUCT_INPHIEUGH_SR_AMOUNT("PXHTAMOUNT","sale-product/","pgnvtttdh_subreport_amount"),
	SALE_PRODUCT_INPHIEUGH_SR_PROMOTION("PXHTPROMOTION","sale-product/","pgnvtttdh_subreport_promotion"),	
	STOCK_CATEGORY_INFOR_EXPORT_INVENTORY("INVENTORYEXPORT","stock/","StockInfoInventory"),
	QLTB_KHMT("QLTB_KHMT","toolcustomer/","template_customer_tool"),
	QLHL("QLHL","trainingplan/","template_training_plan"),
	
	/**
	 * Thiet bi
	 */
	QLTB_QLKHOTHIETBI("QLKHOTHIETBI","equipment/","Xuat_quan_ly_kho_thiet_bi"),
	QLTB_QLQUYENKHOTHIETBI("QLQUYENKHOTHIETBI","equipment/","Xuat_quan_ly_quyen_kho_thiet_bi"),
	QLTB_GANQUYENNGUOIDUNG("GANQUYENNGUOIDUNG","equipment/","Xuat_gan_quyen_nguoi_dung"),
	
	/** 
	@author tientv
	MODULE BAO CAO  */
	/** BAO CAO KHO: KHO */
	KHO_XNT_F1("XNTF1","stock/","rpt_stock_f1"),	//Bao cao xuat nhap ton F1
	DEMO_KHO_XNT_F1("XNTF1","stock/","demo_rpt_stock_f1"),	//Bao cao xuat nhap ton F1 demo
	STOCK_XNTCT ("STOCK_XNTCT","stock/","rpt_stock_xntct"),	//Bao cao xuat nhap ton chi tiet
	DEMO_DS2("DS2", "supervise-customer/","BCDS2_Demo"),   //Bao cao DS2 demo
	KIEMKEKHO_BCDKTT_9_1("BCDKTT_9_1", "stock/",""),   //Bao cao dem kho thuc te
	//Cac bao cao bo sung moi trien khai 10/3
	//Bao cao 7.2.7
	RPT_7_2_7("RPT_7_2_7","stock/","rpt_7_2_7"),	
	RPT_7_2_8("RPT_7_2_8","stock/","rpt_7_2_8"),
	RPT_7_3_9("RPT_7_3_9","stock/","rpt_7_3_9"),
	DS_3_TGBHHQNVBH("DS_3_TGBHHQNVBH", "supervise-customer/","rpt_tgbhhqnvbh"),//(hunglm16) BC thoi gian hieu qua cua NVBH
	
	/** BAO CAO THEO DOI HANG NGAY:	TDHN */
	TDHN_GNTTG("GNTTG","daily-follow/","rpt_tdhn_gnttg"), // phieu giao nhan va thanh toan gop	
	TDHN_PXHTNVGH("PXHTNVGH","daily-follow/","rpt_tdhn_pxhtnvgh"),//7.1.1 Phiếu xuất hàng theo NVGH	
	TDHN_PXHTNVGH_LOT("PXHTNVGH_LOT","daily-follow/","rpt_tdhn_pxhtnvgh_lot"), // subreport lot
	TDHN_PXHTNVGH_SALE_QUANTITY("PXHTNVGH_SALE_QUANTITY","daily-follow/","rpt_tdhn_pxhtnvgh_sale_quantity"), // subreport sales quantity
	TDHN_PXHTNVGH_PRICE("TDHN_PXHTNVGH_PRICE","daily-follow/","rpt_tdhn_pxhtnvgh_price"), // subreport prices
	TDHN_PXHTNVGH_PROM_QUANTITY("PXHTNVGH_PROM_QUANTITY","daily-follow/","rpt_tdhn_pxhtnvgh_prom_quantity"), // subreport prom quantity
	TDHN_PXHTNVGH_A("PXHTNVGH_A","daily-follow/","rpt_tdhn_pxhtnvgh_a"),//Phiếu xuất hàng theo NVGH
	TDHN_PXHTNVBH("PXHTNVBH","daily-follow/","rpt_tdhn_pxhtnvbh"),	
	TDHN_PXHTNVBH_LOT("PXHTNVBH_LOT","daily-follow/","rpt_tdhn_pxhtnvbh_lot"), // subreport lot
	TDHN_PXHTNVBH_SALE_QUANTITY("PXHTNVBH_SALE_QUANTITY","daily-follow/","rpt_tdhn_pxhtnvbh_sale_quantity"), // subreport sales quantity	
	TDHN_PXHTNVBH_PROM_QUANTITY("PXHTNVBH_PROM_QUANTITY","daily-follow/","rpt_tdhn_pxhtnvbh_prom_quantity"), // subreport prom quantity
	TDHN_PXHTNVBH_PRICE("TDHN_PXHTNVBH_PRICE","daily-follow/","rpt_tdhn_pxhtnvbh_price"), // subreport price
	TDHN_BKDHNVGH("BKDHNVGH","daily-follow/","rpt_tdhn_bkdhnvgh"),//7.1.2 bang ke don giao hang theo NVGH
	TDHN_BKDHNVGH_A("BKDHNVGH_A","daily-follow/","rpt_tdhn_bkdhnvgh_a"),//bang ke don giao hang theo NVGH
	TDHN_PDTH("PDTH","daily-follow/","rpt_bchn_pdth"),//thay doi tra hang
	DAILY_FOLLOW_DELIVERY_TICKET("PGNTN","daily-follow/","rpt_bchn"), //7.1.3 bao cao theo doi hang ngay , phieu giao nhan va thanh toan
	DAILY_FOLLOW_DELIVERY_TICKET_A("PGNTN_A","daily-follow/","rpt_bchn_a"), // bao cao theo doi hang ngay , phieu giao nhan va thanh toan
	DAILY_FOLLOW_DELIVERY_TICKET_ADD("daily_follow_delivery_ticket_add","daily-follow/","rpt_daily_follow_delivery_ticket_add"),
	DAILY_FOLLOW_PTHNVGH("PTHNVGH","daily-follow/","rpt_daily_pthnvgh"), // phieu tra hang theo nhan vien giao hang
	TDHN_PTHNVGH_LOT("PTHNVGH_LOT","daily-follow/","rpt_tdhn_pthnvgh_lot"), // subreport lot
	TDHN_PTHNVGH_QUANTITY("PTHNVGH_QUANTITY","daily-follow/","rpt_tdhn_pthnvgh_quantity"), // subreport sales quantity	
	TDHN_PTHNVGH_PROMOTION_QUANTITY("PTHNVGH_PROMOTION_QUANTITY","daily-follow/","rpt_tdhn_pthnvgh_prom_quantity"), // subreport prom quantity
	PO_CUSTOMER_SERVICE("po_customer_service","po/","rpt_manage_customer_service"),
	PRODUCT_BUY("product_buy","product-buy/","rpt_product_buy"),
	DAILY_FOLLOW_BKPTHNVGH("BKPTHNVGH","daily-follow/","rpt_bchn_bkpthnvgh"),// bao cao theo doi hang ngay ,bang ke phieu tra hang NVGH
	TDHN_PTHCKH("PTHCKH","daily-follow/","rpt_tdhn_pthckh"), // phieu tra hang cua khach hang  //HuyNp4 - NamLB
	TDHN_PDHCG("PDHCG","daily-follow/","rpt_bchn_pdhcg"), //phieu dat hang chua giao //Vuonghn
	BCVNM_THDHHH("BCVNM_THDHHH","daily-follow/","rpt_bchn_thdhhh"),// tong hop doi hang hu hong 10.1.18
	
	//	BAO CAO THUE
//	CTHD_GTGT("CTHDGTGT","tax/","rpt_tax_cthd_gtgt"),//chi tiet hoa don GTGT
	BCT_BKHDGTGT("BKHDGTGT","tax/","rpt7_7_6"),//bang ke hoa don GTGT
	BCT_DCDHHTHDGTGT("DCDHHTHDGTGT","tax/","rpt7_7_8"),
	CTHD_GTGT("CTHDGTGT","tax/","cthd_again"),//chi tiet hoa don GTGT
	DCDHHT_HDGTGT("DCDHHTHDGTGT","tax/","cthd_again"),
	KD16("KD16","crm-report/kd16/","kd16"),//chi tiet hoa don GTGT
	KD16_1("KD16_1","crm-report/kd16.1/","KD16_1"),//chi tiet hoa don GTGT
	BCT_HDGTGT("HDGTGT","tax/","rpt_bct_hdgtgt"), // hoa don gia tri gia tang	
	
	/* BAO CAO DOANH THU BAN HANG : DTBH_  ACTION : SalesRevenueReportAction*/
	DTBH_DAY_REPORT("DTBH_BCNGAY","sales-revenue/","rpt_day_report"),						//Bao cao ngay
	DTBH_10_1_2("DTBH_10_1_2","sales-revenue/","rpt_10_1_2"),						//Bao cao ngay
	DTBH_DAY_REPORT_TARA("DTBH_BCNGAY","sales-revenue/","rpt_day_report_tara"),					//Bao cao ngay - tara
	DTBH_PO_REPORT("DTBH_PO","sales-revenue/","try"),
	DTBH_TTCTTB("TTCTTB","sales-revenue/","rpt_dtbh_thcthtb"), //TUNGTT: tra thuong cttb
	DTBH_THCTHTB("THCTHTB","sales-revenue/","rpt_dtbh_thcthtb"), //(Sontt): Bao cao: Tong hop chi tra hang trung bay.
	DTBH_DSKHTMH("DSKHTMH","sales-revenue/","rpt_dtbh_dskhtmh"), //(Sontt): Bao cao: Danh sach khach hang theo mat hang
	DTBH_BHTSP("BHTSP","sales-revenue/","rpt_dtbh_bhtsp"), //(CangND) : Bao cao ban hang theo san pham
	DTBH_DSDHTNTNVBH("DTBH_DSDHTNTNVBH","sales-revenue/","rpt_dtbh_dsdhtntnvbh"),// Báo cáo danh sách đơn hàng trong ngày theo NVBH
	DTBH_CTTHTB("DTBH_CTTHTB","sales-revenue/","rpt_ctcthtb"),//(hunglm16) Báo cáo chi tiết trả hàng trưng bày
	DTBH_DTTHNV("DTTHNV","sales-revenue/","rpt_dtbh_dtthnv"), //(TungTT) Bao cao: Doanh thu tong hop nhan vien
	DTBH_CTKMTCTNew("CTKMTCT","sales-revenue/","rpt_7_2_6"), //(HungLM16) Bao cao: Chi tiet khuyen mai the chuong trinh
	DTBH_TDBHTMH("TDBHTMH","sales-revenue/","rpt_dtbh_tdbhtmh"), //(SangTN): Bao cao: Theo doi ban hang theo mat hang
	DTBH_DTBHTNTNVBH("DTBHTNTNVBH","sales-revenue/","rpt_dtbh_dtbhtntnvbh"), //namlb: Bao cao doanh thu khach hang trong ngay theo nhan vien ban han
	DTBH_DSTHNVBHTN("DSTHNVBHTN","sales-revenue/","rpt_7_2_2"), //phut: Bao cao doanh so tong hop NVBH theo ngay
	DTBH_DSBHTNSP("DSBHTNSP","sales-revenue/","rpt_7_2_2_sp"), //phuongvm: Bao cao doanh thu ban hang theo san pham
	DTBH_DSTHNVBHTN_CT("DTBHTNTNVBHCT","sales-revenue/","rpt_7_2_2_nvbh_chitiet"), // bao cao 7_2_2 vuongmq: them bao cao DTBH trong ngay theo NVBH Chi tiet, type =2
	DTBH_BCCTKM("BCCTKM","sales-revenue/","rpt_dtbh_ctkm"), //vuonghn: Bao cao chi tiet khuyen mai
	DTBH_TDDSTKHNHMH("TDDSTKHNHMH", "sales-revenue/","rpt_dtbh_tddstkhnhmh"),//SangTN: Theo do doanh so theo khach hang - nganh hang - ma hang
	DTBH_DSTMCTTB("DSTMCTTB", "sales-revenue/", "rpt_dtbh_dstmcttb"), // vuongmq: Doanh so theo muc chuong trinh trung bay
	DTBH_TDCTDS("TDCTDS", "sales-revenue/","rpt_dtbh_tddstkhnhmh"),//TUNGTT;: Bao cao theo doi chi tieu doanh so
	DTBH_BTHDH("BTHDH", "sales-revenue/","rpt_dtbh_tddstkhnhmh"),//TUNGTT;: Bao cao doi hang
	DTBH_CTKMTCT("CTKMTCT", "sales-revenue/","rpt_dtbh_tddstkhnhmh"),//TUNGTT;: Bao cao chi tiết khuyến mãi theo chương trình
	DTBH_CTKMTCTNV("CTKMTCTNV", "sales-revenue/","rpt_dtbh_tddstkhnhmh"),//TUNGTT;: Bao cao chi tiết khuyến mãi theo chương trình nhân viên
	DTBH_CTCTHTB("CTCTHTB","sales-revenue/","rpt_ctcthtb"), //BÃ¡o cÃ¡o DS31 - Chi tiet chi tra hang trung bay
	TDBH_7_2_5("TDBH_7_2_5","sales-revenue/",""),
	TDBH_7_2_7("TDBH_7_2_7","sales-revenue/","rpt_7.2.7-ctkmck"),
	TDBH_7_2_8("TDBH_7_2_8","sales-revenue/","rpt_7.2.7-dsttdl"),
	TDBH_7_2_12("TDBH_7_2_12","sales-revenue/","rpt_7.2.12"),
	TDBH_7_2_13("TDBH_7_2_13","sales-revenue/",""),
	TDBH_7_2_14("TDBH_7_2_14","sales-revenue/",""),
	TDBH_PTDTBHSKU("PTDTBHSKU","sales-revenue/","rpt_7.2.15"),// vuongmq phan tich doanh thu ban hang sku 7.2.15
	TDBH_BCDHT_THEOGIATRI("BCDHT_THEOGIATRI","sales-revenue/","rpt_7.2.16"),// vuongmq 7-2-16. Báo cáo đơn hàng trả theo giá trị
	TDBH_BCDHT_THEOTONGDONHANG("BCDHT_THEOTONGDONHANG","sales-revenue/","rpt_7.2.17"),// vuongmq 7-2-17. Báo cáo đơn hàng trả theo tổng số đơn hàng
	TDMH_7_3_9("TDMH_7_3_9","po/","rpt_7.2.3-ddhvnm"),
	
	// BAO CAO CONG NO
	PAYMENT_RPT("PAYMENT_RPT", "debit-pay/", "phieu_thanh_toan"),
	
	PHIEU_SUA_CHUA_THIET_BI("PHIEU_SUA_CHUA_THIET_BI","equipment/","phieu_sua_chua_thiet_bi"),// vuongmq, 22/04/2015; phieu sua chua thiet bi
	// BAO CAO THANH TOAN VA CONG NO KHACH HANG: TTCNKH
	DEBIT_TCN("TCN","debit-pay/",""), // bao cao tong cong no
	DEBIT_TCN_KH("TCNKH","debit-pay/","rpt_debit_tcn_kh"), // bao cao tong cong no
	DEBIT_TCN_NVBH("TCNNVBH","debit-pay/","rpt_debit_tcn_nvbh"), // bao cao tong cong no
	PAY_TTKH("TTKH","debit-pay/","rpt_pay_ttkh"), // bao cao thanh toan cua khach hang
	PAY_PT("PT","debit-pay/","rpt_pay_pt"), // phieu thu
	
	
	/* BAO CAO GIAM SAT KHACH HANG  GSKH_ */
	GSKH_BCNVKBM("GSKH_NVKBM","supervise-customer/","bao_cao_nhan_vien_khong_bat_may"),	//Bao cao nhan vien khong bat may
	GSKH_BCHA("GSKH_BCHA","supervise-customer/","bao_cao_hinh_anh"),  // Bao cao hinh anh VT9	
	GSKH_BCNVDMVS("GSKHBCNVDMVS","supervise-customer/","bcdmvsnvbh"),//bao cao di muon ve som cua NVBH
	GSKH_BCNVDMVS_TARA("GSKHBCNVDMVS","supervise-customer/","rpt_supervise_nvbh_dmvs_tara"),//bao cao di muon ve som cua NVBH
	GSKH_BCTGTM("BCTGTM","supervise-customer/","rpt_gskh_bctgtm"),	// bao cao thoi gian tat may
	GSKH_BCNVKGTKHTT("BCNVKGTKHTT","supervise-customer/","rpt_gskh_bcnvkgtkhtt"),   //bao cao nhan vien khong ghe tham khach hang trong tuyen
	GSKH_BCGTKH("BCGTKH","supervise-customer/","rpt_gskh_bcgtkh"),		//bao cao ghe tham khach hang
	GSKH_BCTGGTNVBH("BCTGGTNVBH","supervise-customer/","rpt_gskh_bctggtnvbh"),// bao cao thoi gian ghe tham nhan vien ban hang
	GSKH_BCTGGTNVBH_TARA("BCTGGTNVBH","supervise-customer/","rpt_gskh_bctggtnvbh_tara"),// bao cao thoi gian ghe tham nhan vien ban hang
	GSKH_BCTGGTNVBH_1("BCTGGTNVBH_1","supervise-customer/","rpt_nvbh_1"),// bao cao thoi gian ghe tham NVBH_1
	GSKH_BCDSBHSKUCTTKH("BCDSBHSKUCTTKH", "superviser-customer/", "bao_cao_doanh_so_ban_hang_sku_chi_tiet_theo_khach_hang"),//Bao cao doanh so ban hang sku chi tiet theo khach hang SangTN
	GSKH_BCDSBHSKUCTTNVBH("BCDSBHSKUCTTNVBH", "superviser-customer/", "bao_cao_doanh_so_ban_hang_sku_chi_tiet_theo_nhan_vien_ban_hang"),//Bao cao doanh so ban hang sku chi tiet theo nhan vien ban hang SangTN
	GSKH_VT6("VT6","supervise-customer/",""), // vt6
	GSKH_BCGTKHVT7("BCGTKHVT7","supervise-customer/",""), // vt7 bao cao ghe tham khach hang
	GSKH_VT7_1 ("VT7_1","supervise-customer/","template_BCVT7.1"),
	GSKH_BCKQCTBVT10("BCKQCTBVT10","supervise-customer/",""), // vt10 bao cao ket qua cham trung bay
	GSKH_BCNVMCLIPVT11("BCNVMCLIPVT11","supervise-customer/","bao_cao_nhan_vien_mo_clip"), // vt11 bao cao nhan vien mo clip
	GSKH_BCXKHTGCTTBVT12("BCXKHTGCTTBVT12","supervise-customer/",""), // vt12 bao cao xoa khach hang tham gia cttb
	
	/* BAO CAO VANSALE */
	VANSALES_PXKKVCNB("PXKKVCNB","van-sales/","rpt_vs_pxkkvcnb"),    // phieu xuat kho kiem van chuyen noi bo
	VANSALES_XBTNV("XBTNV","van-sales/","rpt_vansale_xbtnv"),      	//xuat ban ton nhan vien
	//VANSALES_BCXNKNVBH("BCXNKNVBH","van-sales/","rpt_vs_xnknvbh"),// bao cao xuat nhap kho nhan vien ban hang
	VANSALES_BCXNKNVBH("BCXNKNVBH","van-sales/","rpt_vs_xnknvbh2"),// bao cao xuat nhap kho nhan vien ban hang
	
	/* BAO CAO MUA HANG */
	PO_DCPOCTDVKH("DCPOCTDVKH","po/","rpt_po_dcpoctdvkh"),////doi chieu po comfirm tu dvkh
	PO_BTDTTDH("BTDTTDH", "po/", "rpt_po_tdttdh"),
	PO_BKCTCTMH_A4("BKCTCTMH_A4", "po/", "rpt_po_bkctctmh_a4_portrait"),
	PO_BKCTCTMH_A5("BKCTCTMH_A5", "po/", "rpt_po_bkctctmh_a5"),
	PO_BCPOAUTONPP("BCPOAUTONPP", "po/", "rpt_po_bcpoautonpp"),
	PO_BKCTHHMV("BKCTHHMV", "po/", "rpt_po_bkcthhmv"), // bao cao bang ke chung tu hang hoa mua hang
	PDHVNM_5_1("PDHVNM_5_1", "po/", "rpt_po_pdhvnm_5_1"), // phieu dat hang vinamilk 5.1
	/* BAO CAO KHACH HANG  KH_ */
	KH_DSKHTTBH("DSKHTTBH","customer/","rpt_customer_dskhttbh"),		//Danh sach khach hang theo tuyen ban hang
	KH7_5_1_TH("KH7_5_1_TH","customer/","rpt-7.5.1-th"), // tuoi no phai thu - tong hop
	KH7_5_1_CT("KH7_5_1_CT","customer/","rpt-7.5.1-ct"), // tuoi no phai thu - chi tiet
	//2 bao cao nay dat cung name de hien len cung 1 man hinh:
	KH_DSKH("DSKH","customer/","rpt_kh_dskh" ),		//Danh sach khach hang
	KH_DSKH_NVBH("DSKH","customer/","rpt_kh_dskhtnvbh" ),		//Danh sach khach hang theo nhan vien ban hang
	//subreport areanameAndlistcustomer of report dskhtnvbh:
	KH_DSKH_NVBH_AREANAME_AND_LISTCUSTOMER("DSKH_NVBH_AREANAME_AND_LISTCUSTOMER","customer/","rpt_kh_dskhtnvbh_areaname_and_listcustomer" ),
	/* BAO CAO CRM */
	CRM_DS_PPTNH("DSPPTNH","crm-report/kd1.3/","rpt_crm_bcds_pptnh"),		//Doanh số phân phối theo nhóm hàng
	CRM_KD191("KD19_1","crm-report/kd19_1/","rpt_crm_kd19_1"),//Báo cáo NVGS bị điểm kém
	CRM_TKTHDSPP_TMTG("TKTHDSPPTMTG","crm-report/kd10.3/","rpt_crm_tkthdspp_tmtg"),		//Tổng kết thực hiện DS, PP theo mức tham gias
	CRM_DS_SKU30("DSSKU30","crm-report/kd9/","rpt_crm_bcds_sku30"),	//SKU ban cham 30 ngay
	CRM_DS_SKU30_TARA("DSSKU30","crm-report/kd9/","rpt_kd9"),	//SKU ban cham 30 ngay
	CRM_BPPX_TCDDS("BPPXTCDDS","crm-report/kd17/","rpt_crm_bcbppx_tcdds"),		//Báo cáo bao phủ phường xã theo cấp độ doanh số
	CRM_DS_SDLCPSDS("SDLCPSDS","crm-report/kd15/","rpt_crm_bcds_sdlcpsds"),		//SỐ ĐIỂM LẺ CHƯA PSDS
	CRM_KD10("KD10","crm-report/kd10/","rpt_crm_kd10"),		//KD10 - Thực hiện Doanh số, PP SKUs đề nghị theo NPP
	//CRM_KD14_SKU_GROUP("SKUGROUP","crm-report/kd14/","rpt_crm_kd14"), // Báo cáo KD4 -  Doanh so ban hang SKUs theo mien
	CRM_KD14("KD14","crm-report/kd14/","rpt_crm_kd14"), //KD4 - Bao cao nhom SKUs tren diem ban hang thang - dynamic columns
	CRM_DSNBNH("DSNBNH","crm-report/kd6/","rpt_kd6_importbonus"),					//Báo cáo KD6 - Doanh số nhập - Bán nhóm hàng
	CRM_KD10_1("KD10_1","crm-report/kd10_1/","rpt_crm_kd10_1"),       //KD10.1- thuc hien doanh so phan phoi SKU- GOLDSOY 
	CRM_KD16_1("KD16_1_X","crm-report/kd16_1/","rpt_crm_kd16_1"),       //KD16.1-THONG KÊ SỐ XÃ CHƯA PSDS-GOLDSOY 
	
	CRM_DSBH_NVBH("DSBHNVBH","crm-report/kd3/","rpt_crm_kd3"),			//Báo cáo KD3 - Doanh số bán hàng nhân viên bán hàng
//	CRM_KD4_SKU_AREA("SKUAREA","crm-report/kd4/","rpt_crm_kd4"), // Báo cáo KD4 -  Doanh so ban hang SKUs theo mien
	CRM_KD4_SKU_AREA("SKUAREA","crm-report/kd4/","demo_kd4"), // DEMO-KD4	
	//me
//	CRM_KD5("KD5","crm-report/kd5/","rpt_crm_kd5"),//Luong ban hang SKU theo mien
	CRM_KD5("KD5","crm-report/kd5/","kd5"),//Luong ban hang SKU theo mien
//	CRM_KD17("KD17","crm-report/kd17/","kd17"),//KD17- BAO PHỦ PHƯỜNG XÃ THEO CẤP ĐỘ DOANH SỐ
	CRM_KD17("KD17","crm-report/kd17/","demo_kd17"),//DEMO-KD17
	CRM_KD11("KD11","crm-report/kd11/","kd11"),//Bao cao KD11- Theo doi diem le chua dat doanh so Alpha Shop 
	CRM_KD11_2("KD11_2","crm-report/kd11/","kd11_2"),//KD11.2- Chấm điểm PA chương trình trưng bày
	//end-me
//	CRM_KD21("KD21","crm-report/kd21/","report_crm_kd21"),//thuc hien CT HTTM
	CRM_KD21("KD21","crm-report/kd21/","demo_kd21"),//DEMO-KD21
	CRM_KD2("KD2","crm-report/kd2/","report_crm_kd2"),//Doanh so ban hang cua NVBH theo nhan
	CRM_KD81("KD81","crm-report/kd81/","rpt_crm_kd81"),//Bao cao NVBH bi diem kem
	CRM_KD15("KD15","crm-report/kd15/","rpt_crm_kd15"),
	DEMO_CRM_KD15("KD15","crm-report/kd15/","demo_rpt_crm_kd15"),
	CRM_KD16("CRMKD16","crm-report/kd16/","rpt_crm_kd16"),//Thong ke xa chua PSDS so voi thang truoc
	
//	CRM_DSBH_NVBH_SKU("DSBHNVBHSKU","crm-report/kd1/","rpt_crm_kd1"),       //Báo cáo KD1 - Doanh số bán hàng nhân viên bán hàng theo SKU
	CRM_DSBH_NVBH_SKU("DSBHNVBHSKU","crm-report/kd1/","rpt_crm_kd1_tara"),       
//	CRM_KD1_1("KD1_1","crm-report/kd1.1/","rpt_crm_bcds_pptskus"),//Bao cao doanh so phan phoi theo skus
	
	
	CRM_KD10_2("KD10_2","crm-report/kd10.2/","rpt_crm_kd10_2"),//Bao cao Diem le can phan phoi theo skus de nghi
	CRM_KD18("KD18","crm-report/kd18/","rpt_crm_kd18"), //Bao cao so sanh so luong diem le PSDS thang
	CRM_KD1_1("KD1_1","crm-report/kd1.1/","rpt_kd1_1tara"),
	CRM_KD1_2("KD1_2","crm-report/kd1_2/","rpt_crm_kd1_2"),//Bao cao doanh so phan phoi theo nhan.
	CRM_KD1_3("KD1_3","crm-report/kd1_3/","rpt_kd1_3"),//KD1.3 - Doanh số phân phối theo nhóm hàng
	CRM_KD13("KD13","crm-report/kd13/","rpt_crm_kd13"),//Bao cao tu khong phat sinh doanh so
	CRM_KD7("KD7","crm-report/kd7/","rpt_report_kd7"),//Bao cao doanh so phan phoi theo skus
	CRM_KD8("KD8","crm-report/kd8/","BCDNVBHTTVTDS"),//Bao cao diem nvbh theo tuyen va theo doanh so
	CRM_KD12("KD12","crm-report/kd12/","rpt_crm_kd12"), //Bao cao ket qua danh gia Tu.
	CRM_KD11_1("KD11_1","crm-report/kd11_1/","rpt_kd11_1"),//Bao cao KD11_1 - Theo doi diem le chua dat doanh so Alpha Shop
	CRM_KD19("KD19","crm-report/kd19/","rpt_crm_kd19"), //ĐIeM PA HUaN LUYeN TReN TUYeN CuA GS NPP
	CRM_VT4("VT4","crm-report/vt4/","rpt_vt4"),//Bao cao khong ghe tham khach hang trong tuyen
	CRM_DS1("DS1","crm-report/ds1/","rpt_ds1"),//Bao cao doanh số bán hàng SKUs ngày theo mien
	CRM_VT2("VT2","crm-report/vt2/","rpt_vt2"),
	CRM_KD9("KD9","crm-report/kd9/","rpt_kd9"),
	
	
	DEMO_DS1_3("DEMO_DS1_3","demo/","rpt_1.3"),//DS1.3 - Báo cáo theo dõi xuất hàng theo mặt hàng
	DEMO_DS1_5("DEMO_DS1_5","demo/","rpt_1.5"),// DS1.5 - Báo cáo chi tiết bán hàng của NV
	DEMO_DS1_6("DEMO_DS1_6","demo/","rpt_1.6"),// DS1.6 - Báo cáo doanh thu bán hàng trong ngày theo NVBH
	DEMO_DS1_7("DEMO_DS1_7","demo/","rpt_1.7"),//DS1.7 - Báo cáo danh sách khách hàng theo mặt hàng
	DEMO_DS1_8("DEMO_DS1_8","demo/","rpt_1.8"),//DS1.8 - Báo cáo theo dõi chi tiêu DS
	DEMO_DS1_9("DEMO_DS1_9","demo/","rpt_1.9"),//DS1.9 - Báo cáo theo dõi DS theo KH-NH-MH
	DEMO_DS2_0("DEMO_DS2_0","demo/","rpt_2.0"),//DS2.0 - Báo cáo doanh số theo chương trình trưng bày
	
	DEMO_DS3_2("DEMO_DS3_2","demo/","rpt_3.1"),// DS3.2 - Báo cáo tổng hợp chi trả hàng trưng bày
	DEMO_DS4_1("DEMO_DS4_1","demo/","rpt_4.1"),//DS4.1 - Báo cáo chi tiết khuyến mãi theo CT
	DEMO_DS4_2("DEMO_DS4_2","demo/","rpt_4.2"),//DS4.2 - Báo cáo chi tiết khuyến mãi theo CT-NV-MH
	DEMO_DS4_3("DEMO_DS4_3","demo/","rpt_4.3"),//DS4.3 - Báo cáo chi tiết khuyến mãi theo CT-NV
	DEMO_DS4_5("DEMO_DS4_5","demo/","rpt_4.5"),//DS4.5 - Báo cáo chi tiết khuyến mãi
	DEMO_DS7_0("DEMO_DS7_0","demo/","rpt_7.0"),//DS7.0 - Báo cáo kết quả phân phối theo mặt hàng
	
	DEMO_KD1_2("DEMO_KD1_2","demo/","rpt_kd1_2"),//KD1.2 - Doanh số phân phối theo nhãn hàng
	DEMO_KD1_3("DEMO_KD1_3","demo/","rpt_kd1_3"),//KD1.3 - Doanh số phân phối theo nhóm hàng 
	DEMO_KD6("DEMO_KD6","demo/","rpt_kd11"),//    KD6 - Doanh số nhập-bán 
	DEMO_KD7("DEMO_KD7","demo/","rpt_kd7"),//    KD6 - Doanh số nhập-bán 
	DEMO_KD8("DEMO_KD8","demo/","rpt_kd8"),//    KD6 - Doanh số nhập-bán 
	DEMO_KD11("DEMO_KD11","demo/","rpt_kd11"),//KD11-Doanh số điểm lẻ tham gia chương trình trưng bày 
	DEMO_KD11_1("DEMO_KD11_1","demo/","rpt_kd11.1"),//KD11.1 - Theo dõi điểm lẻ chưa đạt doanh số trưng bày
	DEMO_KD12("DEMO_KD12","demo/","rpt_kd12"),//KD12-Kết quả đánh giá hiệu quả sử dụng thiết bị
	DEMO_KD16("DEMO_KD16","demo/","rpt_kd16"),//KD16 - Thống kê số xã chưa PSDS so với tháng trước 
	DEMO_KD16_1("DEMO_KD16_1","demo/","rpt_kd16.1"),//KD16.1 - Thống kê số xã chưa PSDS 
	DEMO_KD19("DEMO_KD19","demo/","rpt_kd19"),//KD19 - Điểm huấn luyện trên tuyến của GS NPP
	
	XKNV("Export_XKNV","stock/","XKNV"),
	XKNV_SUB_LOT("Export_XKNV_LOT","stock/","XKNV_sub_lot"),
	XKNV_SUB_QTY("Export_XKNV_QTY","stock/","XKNV_sub_qty"),
	XKNV_SUB_PRICE("Export_XKNV_PRICE","stock/","XKNV_sub_price"),
	XKNV_SUB_TOTAL("Export_XKNV_TOTAL","stock/","XKNV_sub_total"),
	NKKHO("NKKho","stock/","NKKho"),
	NKKHO_PROCESS("NKKho","stock/","NKKho_process"), // trang thai: dang thuc hien or tu choi
	NKKHO_SUB_LOT("NKKho_lot","stock/","NKKho_sub_lot"),
	NKKHO_SUB_CYCLE("NKKho_cycle","stock/","NKKho_sub_cycle"),
	NKKHO_SUB_COUNTED("NKKho_counted","stock/","NKKho_sub_counted"),
	NKKHO_SUB_BEFORE("NKKho_before","stock/","NKKho_sub_before"),
	NKKHO_SUB_QTYDIFF("NKKho_qtydiff","stock/","NKKho_sub_qtydiff"),
	NKKHO_SUB_PRICE("NKKho_price","stock/","NKKho_sub_price"),
	NKKHO_SUB_TOTAL("NKKho_total","stock/","NKKho_sub_total"),
	NKKHO_SUB_DATE("NKKho_date","stock/","NKKho_sub_date"),
	NKKHO_SUB_CODE("NKKho_code","stock/","NKKho_sub_code"),
	NVTH("Export_NVTH","stock/","NVTH"),
	XKDC("Export_XKDC","stock/","XKDC"),
	NKDC("Export_NKDC","stock/","NKDC"),
	CTTB_STAFF("CTTB","display-program/","cttb_nvbh"),
	
	DAILY_FOLLOW_DELIVERY_TICKET_A4("PGNTN","daily-follow/","rpt_bchn_size_a4"), //7.1.3 bao cao theo doi hang ngay , phieu giao nhan va thanh toan
	
	// - bao cao HO	
	KM1_1("KM1_1","ho/",""),
	KM1_2("KM1_2","ho/",""),
	DM1_1TTKH("DM1_1TTKH", "ho", ""),
	DM1_2TTNV("DM1_2TTNV", "ho", ""),
	GS1_1TTT("GS1_1TTT", "ho", ""),
	KM1_3TDKM3("KM1_3TDKM3", "ho", ""),
	// - bao cao HO san luong danh so
	SLDS1_1("SLDS1_1","ho/", ""),
	SLDS1_2("SLDS1_2","ho/", ""),
	SLDS1_3("SLDS1_3","ho/", ""),
	HO_SLDS1_4("HO_SLDS1_4","ho/", ""),
	SLDS1_5("SLDS1_5","ho/", ""),
	SLDS1_6("SLDS1_6","ho/", ""),
	
	HO_XNTCT("HO_XNTCT","ho/", ""),
	HO_XNTF1("HO_XNTF1","ho/", ""),
	HO_XNTF1TT("HO_XNTF1TT","ho/", ""),
	
	// Thiet bi
	EQUIPMENT_STOCK_TRANS_REPORT("EQUIPMENT_STOCK_TRANS_REPORT", "equipment/", "bb_chuyen_kho_thiet_bi"),
	BC_DS_MTD_TM_TK("BAO_CAO_DANH_SACH_MUON_TU_DONG_TU_MAT_THEO_KY","equipment/","bc_danh_sach_muon_tu_dong_tu_mat_theo_ky"),// datpv4, 13/05/2015;report:3.1.1.6
	;
	private String name;
	private String folder;
	private String inputFileName;	
	
	public String getFolder() {
		return folder;
	}

	private ShopReportTemplate(String name,String folder,String inputFileName) {
		this.name = name;
		this.folder = folder;
		this.inputFileName = inputFileName;	
	}
	
	private static Map<String, ShopReportTemplate> values = null;

	public String getInputFileName() {
		return inputFileName;
	}
	
	public String getName() {
		return name;
	}

	/**
	 * Parses the value.
	 *
	 * @param value the value
	 * @return the shop report template
	 */
	public static ShopReportTemplate parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, ShopReportTemplate>(
					ShopReportTemplate.values().length);

			for (ShopReportTemplate e : ShopReportTemplate.values()) {
				values.put(e.getName(), e);
			}
		}

		return values.get(value);
	}
	
	/**
	 * Gets the template path.
	 *
	 * @param withStruts the with struts
	 * @return the template path
	 */
	public String getTemplatePath(boolean withStruts, FileExtension ext) {
		if (!withStruts) {
			return StringUtil.replaceSeparatorChar(new StringBuilder(
					ServletActionContext.getServletContext().getRealPath("/"))					
					.append(ConstantManager.TEMPLATE_REPORT_FOLDER)					
					.append("shop-report/").append(folder).append(inputFileName)
					.append(ext.getValue()).toString());
		}
		return StringUtil.replaceSeparatorChar(new StringBuilder()
				.append(ConstantManager.TEMPLATE_REPORT_FOLDER)
				.append("shop-report/").append(folder).append(inputFileName)
				.append(ext.getValue()).toString());
	}
	
	
	/**
	 * Gets the output path.
	 *
	 * @param ext the ext
	 * @return the output path
	 */
	public String getOutputPath(FileExtension ext){
		StringBuilder sbFileName = new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE))
				.append("_").append(inputFileName)
				.append(ext.getValue());		
		return StringUtil.replaceSeparatorChar(sbFileName.toString());
	}
	
	/**
	 * Gets the template path. Xuat excel
	 * @author vuongmq
	 * @date Mar 20,2015
	 * withStruts: true  la lay getServletContext-> lay thu muc tu ngoai Excel, 
	 * withStruts: false  la lay getServletContext-> lay thu muc den tiep shop-report/, 
	 */
	public String getTemplatePathExcel(boolean withStruts, FileExtension ext) {
		if (!withStruts) {
			return StringUtil.replaceSeparatorChar(new StringBuilder(
					ServletActionContext.getServletContext().getRealPath("/"))					
					.append(ConstantManager.TEMPLATE_REPORT_FOLDER)					
					.append("shop-report/").append(folder).append(inputFileName)
					.append(ext.getValue()).toString());
		}
		return StringUtil.replaceSeparatorChar(new StringBuilder(
				ServletActionContext.getServletContext().getRealPath("/"))
				.append(ConstantManager.TEMPLATE_REPORT_FOLDER)
				.append(folder).append(inputFileName)
				.append(ext.getValue()).toString());
	}
}

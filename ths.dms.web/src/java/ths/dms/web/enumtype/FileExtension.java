/**
 * 
 */
package ths.dms.web.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum FileExtension {	
	XLS ("XLS",".xls"),
	XLSX ("XLSX",".xlsx"),
	XLSM ("XLSM",".xlsm"),
	PDF ("PDF",".pdf"),
	DOC ("RTF",".doc"),
	CSV ("CSV",".csv"),
	XML ("XML",".xml"),
	JASPER ("JASPER",".jasper"),
	JRXML ("JRXML",".jrxml"),
	HTML ("HTML",".html"),
	ZIP("ZIP",".zip"),
	DOCX("DOCX",".docx");

	/** The value. */
	private String name;
	
	/** The value. */
	private String value;

	/** The values. */
	private static Map<String, FileExtension> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	FileExtension(String name,String value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static FileExtension parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, FileExtension>(FileExtension.values().length);
			for (FileExtension e : FileExtension.values())
				values.put(e.getName(), e);
		}
		return values.get(value);
	}
}

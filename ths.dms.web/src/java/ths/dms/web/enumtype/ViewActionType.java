/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.enumtype;

import java.util.HashMap;
import java.util.Map;


/**
 * cac hanh dong tren trang view
 * @author tuannd20
 */
public enum ViewActionType {
	CREATE(1),
	EDIT(2),
	READONLY(3)
    ;
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, ViewActionType> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    ViewActionType(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author tuannd20
     * @param value the value
     * @return the gender type
     */
    public static ViewActionType parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, ViewActionType>(ViewActionType.values().length);
            for (ViewActionType e : ViewActionType.values()) {
            	values.put(e.getValue(), e);            	
            }
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.web.enumtype;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.StringUtil;


/**
 * @author hungtx
 *
 */
public enum SaleMTReportTemplate {
	
	
	BCBH("BCBH","salemt-report/","rpt_bcbh"),//bao cao ban hang
	BCKM("BCKM","salemt-report/","rpt_bckm"),//bao cao khuyen mai
	BCDSTHD("BCDSTHD","salemt-report/","rpt_bcdsthd"),//bao cao doanh so theo hoa don
	BCDSTCAT("BCDSTCAT","salemt-report/","rpt_bcdstcat"),//bao cao doanh so theo cat
	BCDSTCAT1("BCDSTCAT1","salemt-report/","rpt_bcdstcat1"),//bao cao doanh so theo cat theo thang
	BCKHKGD("BCKHKGD","salemt-report/","rpt_bckhkgd"),//bao cao khach hang khong giao dich
	BCNXDCH("BCNXDCH","salemt-report/","rpt_bcnxdch"),//bao cao nhap xuat dieu chinh HO
	BCCHNH("BCCHNH","salemt-report/","rpt_bcchnh"),//bao cao cua hang nhap hang
	BCCHXH("BCCHXH","salemt-report/","rpt_bcchxh"),//bao cao cua hang xuat hang
	BCTHH("BCTHH","salemt-report/","rpt_bcthh"),//bao cao tra hang hong
	BCTKCH("BCTKCH","salemt-report/","rpt_bctkch"),//bao cao ton kho cua hang
	BCXNT("BCXNT","salemt-report/","rpt_bcxnt"),//bao cao xuat nhap ton
	BCDH("BCDH","salemt-report/","rpt_bcdh"),//bao cao dat hang
	BCDSPNH("BCDSPNH","salemt-report/","rpt_bcdspnh"),//danh sach phieu nhap hang
	BCTTPT("BCTTPT","salemt-report/","rpt_bcttpt"),//bao cao tinh trang phieu tang
	BCDSKH("BCDSKH","salemt-report/","rpt_bcdskh"),//bao cao danh sach khach hang
	BCSN("BCSN","salemt-report/","rpt_bcsn"),//bao cao sinh nhat khach hang
	BCHTCPSP("BCHTCPSP","salemt-report/","rpt_bccpsp"),//bao cao chi phi san pham
	BCHTCPSPCT("BCHTCPSPCT","salemt-report/","rpt_bccpspct"),//bao cao chi phi san pham chi tiet
	BCQLDTTB("BCQLDTTB","salemt-report/","rpt_bcqldttb"),//bao cao quan ly doanh thu thiet bi
	BCDLBHT("BCDLBHT","salemt-report/","rpt_bcdlbht"),//bao cao du lieu ban hang tho
	BCCTTTHTCTSP("BCCTTTHTCTSP", "salemt-report/", "rpt_bccthtttctsp"), //bao cao chuong trinh thanh toan ho tro chi tieu sp
	;
	private String name;
	private String folder;
	private String inputFileName;	
	
	public String getFolder() {
		return folder;
	}

	private SaleMTReportTemplate(String name,String folder,String inputFileName) {
		this.name = name;
		this.folder = folder;
		this.inputFileName = inputFileName;	
	}
	
	private static Map<String, SaleMTReportTemplate> values = null;

	public String getInputFileName() {
		return inputFileName;
	}
	
	public String getName() {
		return name;
	}

	/**
	 * Parses the value.
	 *
	 * @param value the value
	 * @return the shop report template
	 */
	public static SaleMTReportTemplate parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, SaleMTReportTemplate>(
					SaleMTReportTemplate.values().length);

			for (SaleMTReportTemplate e : SaleMTReportTemplate.values()) {
				values.put(e.getName(), e);
			}
		}

		return values.get(value);
	}
	
	/**
	 * Gets the template path.
	 *
	 * @param withStruts the with struts
	 * @return the template path
	 */
	public String getTemplatePath(boolean withStruts, FileExtension ext) {
		if (!withStruts) {
			return StringUtil.replaceSeparatorChar(new StringBuilder(
					ServletActionContext.getServletContext().getRealPath("/"))					
					.append(ConstantManager.TEMPLATE_REPORT_FOLDER)					
					.append("shop-report/").append(folder).append(inputFileName)
					.append(ext.getValue()).toString());
		}
		return StringUtil.replaceSeparatorChar(new StringBuilder()
				.append(ConstantManager.TEMPLATE_REPORT_FOLDER)
				.append("shop-report/").append(folder).append(inputFileName)
				.append(ext.getValue()).toString());
	}
	
	
	/**
	 * Gets the output path.
	 *
	 * @param ext the ext
	 * @return the output path
	 */
	public String getOutputPath(FileExtension ext){
		StringBuilder sbFileName = new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE))
				.append("_").append(inputFileName)
				.append(ext.getValue());		
		return StringUtil.replaceSeparatorChar(sbFileName.toString());
	}
}

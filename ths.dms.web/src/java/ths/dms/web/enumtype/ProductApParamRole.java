package ths.dms.web.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ProductApParamRole {
	
	USER1("USER1"),
	USER2("USER2"),
	USER3("USER3"),
	USER4("USER4"),
	USER5("USER5"),
	USER6("USER6"),
	USER7("USER7"),
	USER8("USER8"),
	USER9("USER9"),
	USER10("USER10");
	
	private String value;
	
	private static Map<String, ProductApParamRole> values = null;
	
	
	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	ProductApParamRole(String value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static ProductApParamRole parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, ProductApParamRole>(ProductApParamRole.values().length);
			for (ProductApParamRole e : ProductApParamRole.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

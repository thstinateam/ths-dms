/**
 * 
 */
package ths.dms.web.enumtype;

import java.util.HashMap;
import java.util.Map;

import ths.dms.helper.Configuration;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.StringUtil;

public enum BIReportTemplate {
	DEMO_TEMPLATE ("demo.jasper"),
	DEMO_TEMPLATE_TMP ("demo.jrxml"),
	DEMO_OUTPUT("demo");
	

	/** The value. */
	private String value;

	/** The values. */
	private static Map<String, BIReportTemplate> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the template path.
	 *
	 * @return the template path
	 */
	public String getTemplatePath(){
		return StringUtil.replaceSeparatorChar(new StringBuilder(/*ServletActionContext.getServletContext().getRealPath("/")*/).append(ConstantManager.TEMPLATE_REPORT_FOLDER).append("bi-report/").append(value).toString());
	}
	
	/**
	 * Gets the output path.
	 *
	 * @param ext the ext
	 * @return the output path
	 */
	public String getOutputPath(String ext){
		StringBuilder sbFileName = new StringBuilder(Configuration.getStoreRealPath()).append(value);
		if(!StringUtil.isNullOrEmpty(ext)){
			sbFileName.append(ext);
		}
		return StringUtil.replaceSeparatorChar(sbFileName.toString());
	}
	
	public String getURLPath(String ext) {
		StringBuilder urlPath = new StringBuilder(Configuration.getExportExcelPath()).append(value);
		if(!StringUtil.isNullOrEmpty(ext)) {
			urlPath.append(ext);
		}
		return StringUtil.replaceSeparatorChar(urlPath.toString());
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	BIReportTemplate(String value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static BIReportTemplate parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, BIReportTemplate>(BIReportTemplate.values().length);
			for (BIReportTemplate e : BIReportTemplate.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

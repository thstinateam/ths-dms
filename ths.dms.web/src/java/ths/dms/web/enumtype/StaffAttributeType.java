/**
 * 
 */
package ths.dms.web.enumtype;

import java.util.HashMap;
import java.util.Map;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 * @author liemtpt
 * @since 21/01/2015
 */
public enum StaffAttributeType {
	CHON_MOT(1),
	CHON_NHIEU(2),
	CHON_CHU(3),
	CHON_SO(4),
	CHON_SO_THAP_PHAN(5),
	NGAY_THANG(6),
	VI_TRI(7);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ActiveType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    StaffAttributeType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ActiveType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, ActiveType>(
                    ActiveType.values().length);
            for (ActiveType e : ActiveType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

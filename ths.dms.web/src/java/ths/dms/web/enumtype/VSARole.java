/**
 * 
 */
package ths.dms.web.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum VSARole {
//	VNM_ADMIN("VNM_ADMIN"),
//	// SO TEST
//	VNM_SO ("VNM_SO"),	
//	// dich vu khach hang
//	VNM_DVKH ("VNM_DVKH"),
//	// giam sat npp
//	VNM_GSNPP ("VNM_GSNPP"),
//	// truong ban hang vung
//	VNM_TBHV("VNM_TBHV"),
//	// ho tro thuong mai
//	VNM_HTTM ("VNM_HTTM"),
//	// ke toan vnm
//	VNM_KTVNM ("VNM_KTVNM"),	
//	// theo doi dat hang
//	VNM_TDDH ("VNM_TDDH"),
//	// tac nghiep kinh doanh
//	VNM_TNKD ("VNM_TNKD"),
//	// kich hoat chuong trinh httm
//	VNM_HTTM_ACTIVE ("VNM_HTTM_ACTIVE"),
//	//KH su dung tu:quyen 1	
//	VNM_SHOP_ENABLE ("VNM_SHOP_ENABLE"),
//	//KH su dung tu:ca 2 quyen
//	VNM_SHOP_ENABLE_BOTH ("VNM_SHOP_ENABLE_BOTH"),
//	//NV phat trien khach hang
//	VNM_NVPTKH ("VNM_NVPTKH"),
//	// tac nghiep kinh doanh, lay all cac nha pp la cap duoi cua user dang nhap
//	VNM_TNKD_SELECT_ALL ("VNM_TNKD_SELECT_ALL"),
	
	//SALE MT PERMISSION//
	SALEMT_PERMISSION ("SALEMT_ADMIN"),
	
	SALEMT_QLKM ("SALEMT_QLKM"),

	SALEMT_REPORT_ADMIN ("SALEMT_REPORT_ADMIN"),
	
	SALEMT_REPORT_MB ("SALEMT_REPORT_MB"),
	
	SALEMT_REPORT_MD ("SALEMT_REPORT_MD"),
	
	SALEMT_REPORT_MH ("SALEMT_REPORT_MH"),
	
	SALEMT_REPORT_ML ("SALEMT_REPORT_ML"),
	
	SALEMT_REPORT_MN ("SALEMT_REPORT_MN"),
	
	SALEMT_REPORT_MR ("SALEMT_REPORT_MR"),
	
	SALEMT_REPORT_MT ("SALEMT_REPORT_MT"),
	
	SALEMT_REPORT_MY ("SALEMT_REPORT_MY"),
	//SALE MT PERMISSION//
	
	VNM_TABLET_PORTAL_CTB("VNM_TABLET_PORTAL_CTB"),  /** Cham trung bay */
	
	
//	[[VINAMILK SALESONLINE DEMO 27/05
	VNM_SALESONLINE_HO ("VNM_SALESONLINE_HO"),
	
	VNM_SALESONLINE_GS ("VNM_SALESONLINE_GS"),
	
	VNM_SALESONLINE_DISTRIBUTOR ("VNM_SALESONLINE_DISTRIBUTOR");
//	VINAMILK SALESONLINE DEMO 27/05]]

	

	
	
	/** The value. */
	private String value;

	/** The values. */
	private static Map<String, VSARole> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	VSARole(String value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static VSARole parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, VSARole>(VSARole.values().length);
			for (VSARole e : VSARole.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

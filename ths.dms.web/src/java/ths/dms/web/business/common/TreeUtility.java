/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.business.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.TreeNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;

/**
 * Utility de build tree: unit-tree, report-tree, ...
 * @author tuannd20
 * @param <T>
 * @since 02/03/2015
 */
public class TreeUtility extends AbstractAction {

	/** field serialVersionUID  field long */
	private static final long serialVersionUID = 1L;

	@Autowired
	private ShopMgr shopMgr;

	@Autowired
	private ProductMgr productMgr;

	private Map<Long, Shop> unputOnTreeShops;//cac shop chua put tren cay - unputOnTreeShops

	private ITreeDataFilter treeShopFilter;

	public TreeUtility(ApplicationContext context) {
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
	}

	public TreeUtility(ApplicationContext context, ITreeDataFilter treeShopFilter) {
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		this.treeShopFilter = treeShopFilter;
	}

	public TreeUtility(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public interface ITreeDataFilter {
		List<Shop> filterTreeData(List<Shop> treeDatas);
	}

	/**
	 * tao cay don vi tim kiem
	 * @author tuannd20
	 * @param startFromShops Danh sach cac shop dung lam node goc cua cay. Neu truyen vao Null, mac dinh lay shop cao nhat (shop co parent = null) lam node goc
	 * @return cac node tren cay don vi
	 * @since 02/03/2015
	 */
	public List<TreeNode> buildUnitTree(List<Long> startFromShops) {
		try {
			List<TreeNode> treeRootNodes = new ArrayList<TreeNode>();
			List<Shop> shopsOnTree = shopMgr.getShopsOnTree(startFromShops, false);
			if (treeShopFilter != null) {
				shopsOnTree = treeShopFilter.filterTreeData(shopsOnTree);
			}
			if (shopsOnTree != null && shopsOnTree.size() > 0) {
				for (int i = 0, size = shopsOnTree.size(); i < size; i++) {
					Shop shop = shopsOnTree.get(i);
					TreeNode shopTreeNode = convertShopToShopTreeNode(shop);
					Boolean putable = putShopNodeToTree(treeRootNodes, shopTreeNode);
					if (!putable) {
						treeRootNodes.add(shopTreeNode);
					}
				}
			}

			changeLeafTreeNodeState(treeRootNodes);

			return treeRootNodes;
		} catch (BusinessException e) {
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.TreeUtility.buildUnitTree()"), createLogErrorStandard(startLogDate));
		}
		return null;
	}

	/**
	 * tao cay nganh hang
	 *
	 * @author hoanv25
	 * @return cac node tren cay don vi
	 * @since Auggust 19,2015
	 */
	public List<TreeNode> buildSubCatTree() {
		try {
			List<TreeNode> treeRootNodes = new ArrayList<TreeNode>();
			List<ProductInfo> productTree = productMgr.getSubCatOnTree(false);
			if (productTree != null && productTree.size() > 0) {
				for (int i = 0, size = productTree.size(); i < size; i++) {
					ProductInfo product = productTree.get(i);
					TreeNode shopTreeNode = convertProductCatToShopTreeNode(product);
					Boolean putable = putSubCatNodeToTree(treeRootNodes, shopTreeNode);
					if (!putable) {
						treeRootNodes.add(shopTreeNode);
					}
				}
			}
			changeLeafTreeNodeState(treeRootNodes);
			return treeRootNodes;
		} catch (BusinessException e) {
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.TreeUtility.buildSubCatTree()"), createLogErrorStandard(startLogDate));
		}
		return null;
	}

	/**
	 * tao cay don vi theo phan quyen tu cap hien tai tro xuong
	 * @author trieptm
	 * @return cac node tren cay don vi
	 * @since 02/03/2015
	 */
	public List<TreeNode> buildUnitTree(Long shopId, boolean labelChoose, Long staffRootId, Long roleId, Long shopRootId) {
		try {
			List<TreeNode> treeRootNodes = new ArrayList<TreeNode>();
			if (labelChoose) {
				TreeNode treeNode = new TreeNode();
				treeNode.setId(-2);
				treeNode.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "shop.common.tree.choose"));
				treeNode.setState("closed");
				Map<String, Object> treeNodeAttribute = new HashMap<String, Object>();
				treeNodeAttribute.put("shop", new Shop());
				treeNode.setAttributes(treeNodeAttribute);
				treeRootNodes.add(treeNode);
			}
			if (staffRootId != null && roleId != null && shopRootId != null && shopId != null) {
				ShopFilter filter = new ShopFilter();
				filter.setStatus(ActiveType.RUNNING);
				filter.setShopId(shopId);
				filter.setStaffRootId(staffRootId);
				filter.setRoleId(roleId);
				filter.setShopRootId(shopRootId);
				ObjectVO<Shop> vo = shopMgr.getListChildShop(filter);
				if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {

					List<Shop> shopsOnTree = vo.getLstObject();
					for (int i = 0, size = shopsOnTree.size(); i < size; i++) {
						Shop shop = shopsOnTree.get(i);
						TreeNode shopTreeNode = convertShopToShopTreeNode(shop);
						Boolean putable = putShopNodeToTree(treeRootNodes, shopTreeNode);
						if (!putable) {
							treeRootNodes.add(shopTreeNode);
						}
					}
					changeLeafTreeNodeState(treeRootNodes);
				}
			}
			return treeRootNodes;
		} catch (BusinessException e) {
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.TreeUtility.buildUnitTree()"), createLogErrorStandard(startLogDate));
		}
		return null;
	}
	/**
	 * tao cay don vi tim kiem
	 * @author longnh15
	 * @return cac node tren cay don vi
	 * @since 02/03/2015
	 */
	public List<TreeNode> buildUnitTreeForDataPermission(Long keyshopId, Boolean isShopsInOrgAccessOnly, ShopFilter shopFilter, Long rootShopId) {
		try {
			List<TreeNode> treeRootNodes = new ArrayList<TreeNode>();
			if (shopFilter == null) {
				shopFilter = new ShopFilter();
				shopFilter.setStatus(ActiveType.RUNNING);
			}
			List<Shop> shopsOnTree = null;
			//Danh sach shop
			if (isShopsInOrgAccessOnly) {
				shopsOnTree = shopMgr.getShopsOnTreeFilter(shopFilter, keyshopId);
			}
			else {
				shopsOnTree = shopMgr.getShopsInAccessOnTree(shopFilter, keyshopId);
			}
			//Danh sach shop can check
			List<Shop> grantedShopsOnPermission = shopMgr.retriveShopsForKeyShop(keyshopId);
			if (shopsOnTree != null && shopsOnTree.size() > 0) {
				//for (Shop shop : shopsOnTree) {
				unputOnTreeShops = constructShopLookupTable(shopsOnTree);
				for (int i = 0, size = shopsOnTree.size(); i < size; i++) {
					Shop shop = shopsOnTree.get(i);
					if (!unputOnTreeShops.containsKey(shop.getId())) {
						continue;
					}
					putShopNodeToTree(treeRootNodes, shop, grantedShopsOnPermission,rootShopId);
				}
			}
			changeLeafTreeNodeState(treeRootNodes);
			return treeRootNodes;
		} catch (BusinessException e) {
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.TreeUtility.buildUnitTreeForDataPermission()"), createLogErrorStandard(startLogDate));
		}
		return null;
	}
	/**
	 * dat node vao cay
	 * @author tuannd20
	 * @param treeRootNodes Danh sach cac node goc
	 * @param shop Thong tin don vi se dua vao cay
	 * @param grantedShopsOnPermission Danh sach cac don vi duoc phan cho quyen du lieu. Dung de danh dau tren cay
	 * @return true: dat thanh cong vao cay (co 1 node parent); false: ko tim thay node cha nao de dat vao cay
	 * @since 13/03/2015
	 */
	private Boolean putShopNodeToTree(List<TreeNode> treeRootNodes, Shop shop, List<Shop> grantedShopsOnPermission, Long rootShopId) {
		TreeNode shopTreeNode = convertShopToShopTreeNode(shop);
		markTreeNodeAsSelected(shopTreeNode, grantedShopsOnPermission);

		if (isRootNodeOnTree(shopTreeNode,rootShopId)) {
			treeRootNodes.add(shopTreeNode);
			unputOnTreeShops.remove(shop.getId());
			return true;
		}

		Boolean putable = putChildShopNodeToTree(treeRootNodes, shopTreeNode, grantedShopsOnPermission, rootShopId);
		unputOnTreeShops.remove(shop.getId());
		return putable;

	}
	/**
	 * dat 1 node vao cay theo thu tu cha-con
	 * @author tuannd20
	 * @param treeRootNodes danh sach node
	 * @param shopTreeNode node can dat vao cay
	 * @param grantedShopsOnPermission danh sach cac shop duoc phan quyen cho quyen du lieu
	 * @param grantedExceptionShopsOnPermission Danh sach cac shop loai tru duoc phan quyen cho quyen du lieu
	 * @return ket qua dat node vao cay thanh cong (true) hay that bai (false)
	 * @since 02/03/2015
	 */
	private Boolean putChildShopNodeToTree(List<TreeNode> treeRootNodes, TreeNode shopTreeNode, List<Shop> grantedShopsOnPermission, Long rootShopId) {
		if (treeRootNodes != null && shopTreeNode != null) {
			Map<String, Object> nodeAttributes = shopTreeNode.getAttributes();
			if (nodeAttributes != null && nodeAttributes.containsKey("shop")) {
				Object shopObj = nodeAttributes.get("shop");
				if (shopObj instanceof Shop) {
					Shop shop = (Shop)shopObj;
					Long parentShopId = shop.getParentShop() != null ? shop.getParentShop().getId() : null;

					if (parentShopId != null && unputOnTreeShops.containsKey(parentShopId)) {	// parent shop haven't add to tree yet
						putShopNodeToTree(treeRootNodes, shop.getParentShop(), grantedShopsOnPermission, rootShopId);
					}

					//for (TreeNode treeNode : treeRootNodes) {
					for (int i = 0, size = treeRootNodes.size(); i < size; i++) {
						TreeNode treeNode = treeRootNodes.get(i);
						if (treeNode.getId() instanceof Long) {
							Long treeNodeId = (Long) treeNode.getId();
							if (treeNodeId.compareTo(parentShopId) == 0) {
								List<TreeNode> children = treeNode.getChildren();
								if (children == null) {
									children = new ArrayList<TreeNode>();
									treeNode.setChildren(children);
								}
								children.add(shopTreeNode);
								return true;
							} else {
								Boolean putable = putChildShopNodeToTree(treeNode.getChildren(), shopTreeNode, grantedShopsOnPermission, rootShopId);
								if (putable) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	/**
	 * kiem tra node co phai la node goc (root) tren cay hay ko
	 * @author tuannd20
	 * @param treeNode node dang kiem tra
	 * @return true: node dang kiem tra la node goc hay ko; false: node dang kiem tra ko fai la node goc
	 * @since 12/03/2015
	 */
	private Boolean isRootNodeOnTree(TreeNode treeNode, Long rootShopId) {
		if (treeNode != null) {
			Map<String, Object> attributes = treeNode.getAttributes();
			if (attributes != null && attributes.containsKey("shop")) {
				Object shopObj = attributes.get("shop");
//				if (shopObj instanceof Shop && ((Shop)shopObj).getParentShop() == null) {
//					return true;
//				}
				if (shopObj instanceof Shop && rootShopId.equals(((Shop)shopObj).getId())) {
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * danh dau node check hay ko check dua tren du lieu phan quyen
	 * @author tuannd20
	 * @param treeNode node kiem tra
	 * @param grantedShopsOnPermission danh sach cac don vi duoc phan tren quyen
	 * @param grantedExceptionShopsOnPermission Danh sach cac don vi loai tru
	 * @since 10/03/2015
	 */
	private void markTreeNodeAsSelected(TreeNode treeNode, List<Shop> grantedShopsOnPermission) {
		if (grantedShopsOnPermission != null && grantedShopsOnPermission.size() > 0) {
			Map<String, Object> attributes = treeNode.getAttributes();
			if (attributes != null && attributes.containsKey("shopId") && attributes.get("shopId") instanceof Long) {
				Long shopId = (Long) attributes.get("shopId");
				// danh dau don vi phan quyen
				for (Shop shop: grantedShopsOnPermission) {
					if (shopId.compareTo(shop.getId()) == 0) {
						attributes.put("selected", true);
						treeNode.setState("open");
						break;
					}
				}

			}
		}
	}
	/**
	 * tao bang tra (lookup) danh sach don vi chua dua vao cay
	 * @author tuannd20
	 * @param shopsOnTree danh sach shops
	 * @return bang tra don vi chua dua vao cay
	 * @since 13/03/2015
	 */
	private Map<Long, Shop> constructShopLookupTable(List<Shop> shopsOnTree) {
		Map<Long, Shop> shopLookupTable = new HashMap<Long, Shop>();
		for (Shop shop: shopsOnTree) {
			shopLookupTable.put(shop.getId(), shop);
		}
		return shopLookupTable;
	}
	/**
	 * convert du lieu don vi sang dang node tren cay
	 * @author tuannd20
	 * @param shop
	 * @return du lieu don vi dang node tren cay
	 * @since 02/03/2015
	 */
	private TreeNode convertShopToShopTreeNode(Shop shop) {
		final String DELIMITER = " - ";
		if (shop == null) {
			return null;
		}
		TreeNode treeNode = new TreeNode();
		treeNode.setId(shop.getId());
		treeNode.setText(shop.getShopCode() + DELIMITER + shop.getShopName());
		treeNode.setState("closed");
		Map<String, Object> treeNodeAttribute = new HashMap<String, Object>();
		treeNodeAttribute.put("shop", shop);
		treeNodeAttribute.put("shopId", shop.getId());
		treeNode.setAttributes(treeNodeAttribute);

		return treeNode;
	}

	/**
	 * convert du lieu sang dang node tren cay
	 * @author hoanv25
	 * @param product
	 * @return du lieu dang node tren cay
	 * @since 19/08/2015
	 */
	private TreeNode convertProductCatToShopTreeNode(ProductInfo product) {
		if (product == null) {
			return null;
		}
		TreeNode treeNode = new TreeNode();
		treeNode.setId(product.getId());
		treeNode.setText(product.getProductInfoCode());
		treeNode.setState("closed");
		Map<String, Object> treeNodeAttribute = new HashMap<String, Object>();
		treeNodeAttribute.put("product", product);
		treeNodeAttribute.put("productId", product.getId());
		treeNode.setAttributes(treeNodeAttribute);

		return treeNode;
	}

	/**
	 * dat 1 node vao cay theo thu tu cha-con
	 * @author tuannd20
	 * @param treeRootNodes danh sach node
	 * @param shopTreeNode node can dat vao cay
	 * @return ket qua dat node vao cay thanh cong (true) hay that bai (false)
	 * @since 02/03/2015
	 */
	private Boolean putShopNodeToTree(List<TreeNode> treeRootNodes, TreeNode shopTreeNode) {
		if (treeRootNodes != null && shopTreeNode != null) {
			/*if (treeRootNodes.size() == 0) {
				treeRootNodes.add(shopTreeNode);
				return true;
			}*/
			Map<String, Object> nodeAttributes = shopTreeNode.getAttributes();
			if (nodeAttributes != null && nodeAttributes.containsKey("shop")) {
				Object shopObj = nodeAttributes.get("shop");
				if (shopObj instanceof Shop) {
					Shop shop = (Shop)shopObj;
					Long parentShopId = shop.getParentShop() != null ? shop.getParentShop().getId() : null;
					//for (TreeNode treeNode : treeRootNodes) {
					for (int i = 0, size = treeRootNodes.size(); i < size; i++) {
						TreeNode treeNode = treeRootNodes.get(i);
						if (treeNode.getId() instanceof Long) {
							Long treeNodeId = (Long) treeNode.getId();
							if (treeNodeId.compareTo(parentShopId) == 0) {
								List<TreeNode> children = treeNode.getChildren();
								if (children == null) {
									children = new ArrayList<TreeNode>();
									treeNode.setChildren(children);
								}
								children.add(shopTreeNode);
								return true;
							} else {
								Boolean putable = putShopNodeToTree(treeNode.getChildren(), shopTreeNode);
								if (putable) {
									return true;
								}
							}
						}
					}
					/*treeRootNodes.add(shopTreeNode);
					return true;*/
				}
			}
		}
		return false;
	}

	/**
	 * dat 1 node vao cay theo thu tu cha-con
	 * @author hoanv25
	 * @param treeRootNodes danh sach node
	 * @param shopTreeNode node can dat vao cay
	 * @return ket qua dat node vao cay thanh cong (true) hay that bai (false)
	 * @since 19/08/2015
	 */
	private Boolean putSubCatNodeToTree(List<TreeNode> treeRootNodes, TreeNode shopTreeNode) {
		if (treeRootNodes != null && shopTreeNode != null) {
			Map<String, Object> nodeAttributes = shopTreeNode.getAttributes();
			if (nodeAttributes != null && nodeAttributes.containsKey("product")) {
				Object shopObj = nodeAttributes.get("product");
				if (shopObj instanceof ProductInfo) {
					ProductInfo productInfo = (ProductInfo) shopObj;
					for (int i = 0, size = treeRootNodes.size(); i < size; i++) {
						TreeNode treeNode = treeRootNodes.get(i);
						if (treeNode.getId() instanceof Long) {
							/*Long treeNodeId = (Long) treeNode.getId();
							if (treeNodeId.compareTo(parentShopId) == 0) {
								List<TreeNode> children = treeNode.getChildren();
								if (children == null) {
									children = new ArrayList<TreeNode>();
									treeNode.setChildren(children);
								}
								children.add(shopTreeNode);
								return true;
							} else {*/
								Boolean putable = putSubCatNodeToTree(treeNode.getChildren(), shopTreeNode);
								if (putable) {
									return true;
								}
						//	}
						}
					}
					/*
					 * treeRootNodes.add(shopTreeNode); return true;
					 */
				}
			}
		}
		return false;
	}

	/**
	 * doi trang thai cac node ko co children tren cay sang leaf
	 * @author tuannd20
	 * @param treeNodes danh sach node tren cay
	 * @since 02/03/2015
	 */
	private void changeLeafTreeNodeState(List<TreeNode> treeNodes) {
		if (treeNodes != null) {
			for (TreeNode treeNode : treeNodes) {
				if (treeNode.getChildren() == null || treeNode.getChildren().size() == 0) {
					treeNode.setState("open");
					treeNode.setChildren(new LinkedList<TreeNode>());
				} else {
					changeLeafTreeNodeState(treeNode.getChildren());
				}
			}
		}
	}
}

/**
 * Copyright (c) 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.business.common;

import java.util.List;

import org.springframework.stereotype.Component;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.exceptions.BusinessException;

/**
 * cac ham xu ly thong tin thuoc tinh dong cua khach hang, nhan vien, san pham
 * @author tuannd20
 * @since 11/09/2015
 */
@Component
public interface DynamicAttributeHelper {
	/**
	 * Lay du lieu thuoc tinh dong cua khach hang
	 * @author tuannd20
	 * @param attributeStatus trang thai thuoc tinh dong can lay
	 * @return danh sach thuoc tinh dong cua khach hang
	 * @throws BusinessException
	 * @since 11/09/2015
	 */
	List<Object> getCustomerDynamicAttributeVO(ActiveType attributeStatus) throws BusinessException;
	
	/**
	 * Lay du lieu thuoc tinh dong cua nhan vien
	 * @author tuannd20
	 * @param attributeStatus trang thai thuoc tinh dong can lay
	 * @return danh sach thuoc tinh dong cua nhan vien
	 * @throws BusinessException
	 * @since 12/09/2015
	 */
	List<Object> getStaffDynamicAttributeVO(ActiveType attributeStatus) throws BusinessException;
	
	/**
	 * Lay du lieu thuoc tinh dong cua san pham
	 * @author tuannd20
	 * @param attributeStatus trang thai thuoc tinh dong can lay
	 * @return danh sach thuoc tinh dong cua san pham
	 * @throws BusinessException
	 * @since 16/09/2015
	 */
	List<Object> getProductDynamicAttributeVO(ActiveType attributeStatus) throws BusinessException;
}

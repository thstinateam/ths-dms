/**
 * Copyright (c) 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.business.common;

import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;

import ths.dms.helper.R;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * doi tuong validate thuoc tinh dong
 * @author tuannd20
 * @since 13/09/2015
 */
public class DynamicAttributeValidator {

	/**
	 * validate du lieu thuoc tinh dong trong file import
	 * @author tuannd20
	 * @param rowData dong du lieu trong file import
	 * @param dataPosisiton vi tri cell chua du lieu can validate
	 * @param dynamicAttributeVO doi tuong mo ta thong tin thuoc tinh dong
	 * @return Chuoi gia tri loi neu co loi. Nguoc lai, null hoac chuoi rong
	 * @since 13/09/2015
	 */
	public static String validateDynamicAttributeColumnData(List<String> rowData, int dataPosisiton, AttributeDynamicVO dynamicAttributeVO) {
		String cellData = rowData.get(dataPosisiton);
		if (dynamicAttributeVO == null || dynamicAttributeVO.getType() == null) {
			return null;
		}

		String errorMsg = null;
		if (AttributeColumnType.CHARACTER.getValue().equals(dynamicAttributeVO.getType())) {
			errorMsg = validateTextDynamicAttribute(cellData, dynamicAttributeVO);
		} else if (AttributeColumnType.NUMBER.getValue().equals(dynamicAttributeVO.getType())) {
			errorMsg = validateNumberDynamicAttribute(cellData, dynamicAttributeVO);
		} else if (AttributeColumnType.DATE_TIME.getValue().equals(dynamicAttributeVO.getType())) {
			errorMsg = validateDateTimeDynamicAttribute(cellData, dynamicAttributeVO);
		} else if (AttributeColumnType.CHOICE.getValue().equals(dynamicAttributeVO.getType())) {
			errorMsg = validateChoiceDynamicAttribute(cellData, dynamicAttributeVO);
		} else if (AttributeColumnType.MULTI_CHOICE.getValue().equals(dynamicAttributeVO.getType())) {
			errorMsg = validateMultiChoiceDynamicAttribute(cellData, dynamicAttributeVO);
		} else if (AttributeColumnType.LOCATION.getValue().equals(dynamicAttributeVO.getType())) {
			errorMsg = validateLocationDynamicAttribute(cellData, dynamicAttributeVO);
		}

		return errorMsg;
	}

	/**
	 * validate du lieu thuoc tinh dong dang text
	 * @author tuannd20
	 * @param validateValue du lieu can validate
	 * @param dynamicAttributeVO doi tuong mo ta thong tin thuoc tinh dong
	 * @return Chuoi gia tri loi neu co loi. Nguoc lai, null hoac chuoi rong
	 * @since 13/09/2015
	 */
	public static String validateTextDynamicAttribute(String validateValue, AttributeDynamicVO dynamicAttributeVO) {
		String errorMsg = validateForRequiredDynamicAttribute(validateValue, dynamicAttributeVO);
		if (errorMsg == null && !StringUtil.isNullOrEmpty(validateValue)) {
			validateValue = validateValue.trim();
			if (dynamicAttributeVO.getDataLength() != null && validateValue.length() > dynamicAttributeVO.getDataLength()) {
				errorMsg = R.getResource("dynamic.attribute.text.exceed.maxlength", dynamicAttributeVO.getAttributeName(), dynamicAttributeVO.getDataLength()) + ConstantManager.NEW_LINE_CHARACTER;
			}
		}
		return errorMsg;
	}

	/**
	 * validate du lieu thuoc tinh dong dang so
	 * @author tuannd20
	 * @param validateValue du lieu can validate
	 * @param dynamicAttributeVO doi tuong mo ta thong tin thuoc tinh dong
	 * @return Chuoi gia tri loi neu co loi. Nguoc lai, null hoac chuoi rong
	 * @since 13/09/2015
	 */
	public static String validateNumberDynamicAttribute(String validateValue, AttributeDynamicVO dynamicAttributeVO) {
		String errorMsg = validateForRequiredDynamicAttribute(validateValue, dynamicAttributeVO);
		if (errorMsg == null && !StringUtil.isNullOrEmpty(validateValue)) {
			errorMsg = "";
			if (dynamicAttributeVO.getDataLength() != null && validateValue.length() > dynamicAttributeVO.getDataLength()) {
				errorMsg += R.getResource("dynamic.attribute.text.exceed.maxlength", dynamicAttributeVO.getAttributeName(), dynamicAttributeVO.getDataLength()) + ConstantManager.NEW_LINE_CHARACTER;
			}
			BigDecimal cellValue = null;
			try {
				cellValue = new BigDecimal(validateValue);
			} catch (NumberFormatException e) {
				errorMsg += R.getResource("dynamic.attribute.number.invalid.format", dynamicAttributeVO.getAttributeName()) + ConstantManager.NEW_LINE_CHARACTER;
			}
			if (cellValue != null) {
				if (dynamicAttributeVO.getMinValue() != null && cellValue.doubleValue() < dynamicAttributeVO.getMinValue()) {
					errorMsg += R.getResource("dynamic.attribute.number.less.than.min.value", dynamicAttributeVO.getAttributeName(), dynamicAttributeVO.getMinValue()) + ConstantManager.NEW_LINE_CHARACTER;
				}
				if (dynamicAttributeVO.getMaxValue() != null && cellValue.doubleValue() > dynamicAttributeVO.getMaxValue()) {
					errorMsg += R.getResource("dynamic.attribute.number.greater.than.max.value", dynamicAttributeVO.getAttributeName(), dynamicAttributeVO.getMaxValue()) + ConstantManager.NEW_LINE_CHARACTER;
				}
			}
		}
		return errorMsg;
	}

	/**
	 * validate du lieu thuoc tinh dong dang ngay thang
	 * @author tuannd20
	 * @param validateValue du lieu can validate
	 * @param dynamicAttributeVO doi tuong mo ta thong tin thuoc tinh dong
	 * @return Chuoi gia tri loi neu co loi. Nguoc lai, null hoac chuoi rong
	 * @since 13/09/2015
	 */
	public static String validateDateTimeDynamicAttribute(String validateValue, AttributeDynamicVO dynamicAttributeVO) {
		String errorMsg = validateForRequiredDynamicAttribute(validateValue, dynamicAttributeVO);
		if (errorMsg == null && !StringUtil.isNullOrEmpty(validateValue) && !ValidateUtil.isValidDate(validateValue.trim(), DateUtil.DATE_FORMAT_DDMMYYYY)) {
			errorMsg = R.getResource("dynamic.attribute.date.invalid.format.date", validateValue, dynamicAttributeVO.getAttributeName()) + ConstantManager.NEW_LINE_CHARACTER;
		}
		return errorMsg;
	}

	/**
	 * validate du lieu thuoc tinh dong dang single select
	 * @author tuannd20
	 * @param validateValue du lieu can validate
	 * @param dynamicAttributeVO doi tuong mo ta thong tin thuoc tinh dong
	 * @return Chuoi gia tri loi neu co loi. Nguoc lai, null hoac chuoi rong
	 * @since 13/09/2015
	 */
	public static String validateChoiceDynamicAttribute(String validateValue, AttributeDynamicVO dynamicAttributeVO) {
		String errorMsg = validateForRequiredDynamicAttribute(validateValue, dynamicAttributeVO);
		if (errorMsg == null && !StringUtil.isNullOrEmpty(validateValue)) {
			validateValue = validateValue.trim();
			List<AttributeDetailVO> attributeDetailVOs = dynamicAttributeVO.getAttributeDetailVOs();
			if (attributeDetailVOs == null || !isSelectedDynamicAttributeItemExistInDeclaredValue(validateValue, attributeDetailVOs)) {
				errorMsg = R.getResource("dynamic.attribute.single.select.value.invalid", dynamicAttributeVO.getAttributeName()) + ConstantManager.NEW_LINE_CHARACTER;
			}
		}
		return errorMsg;
	}

	/**
	 * validate du lieu thuoc tinh dong dang multi select
	 * @author tuannd20
	 * @param validateValue du lieu can validate
	 * @param dynamicAttributeVO doi tuong mo ta thong tin thuoc tinh dong
	 * @return Chuoi gia tri loi neu co loi. Nguoc lai, null hoac chuoi rong
	 * @since 13/09/2015
	 */
	public static String validateMultiChoiceDynamicAttribute(String validateValue, AttributeDynamicVO dynamicAttributeVO) {
		String errorMsg = validateForRequiredDynamicAttribute(validateValue, dynamicAttributeVO);
		if (errorMsg == null && !StringUtil.isNullOrEmpty(validateValue)) {
			final String DELIMITER = ",";
			validateValue = validateValue.trim();
			List<AttributeDetailVO> attributeDetailVOs = dynamicAttributeVO.getAttributeDetailVOs();
			Boolean isSelectedDynamicAttributeItemExistInDeclaredValue = true;
			if (attributeDetailVOs != null) {
				String[] selectedItems = validateValue.split(DELIMITER);
				for (String selectedItem : selectedItems) {
					isSelectedDynamicAttributeItemExistInDeclaredValue = isSelectedDynamicAttributeItemExistInDeclaredValue(selectedItem.trim(), attributeDetailVOs);
					if (!isSelectedDynamicAttributeItemExistInDeclaredValue) {
						break;
					}
				}
			} else {
				isSelectedDynamicAttributeItemExistInDeclaredValue = false;
			}
			if (!isSelectedDynamicAttributeItemExistInDeclaredValue) {
				errorMsg = R.getResource("dynamic.attribute.single.select.value.invalid", dynamicAttributeVO.getAttributeName()) + ConstantManager.NEW_LINE_CHARACTER;
			}
		}
		return errorMsg;
	}

	/*
	 * kiem tra gia tri thuoc tinh dong da chon co ton tai trong danh sach thuoc tinh da khai bao hay khong
	 */
	private static Boolean isSelectedDynamicAttributeItemExistInDeclaredValue(String selectedItem, List<AttributeDetailVO> attributeDetailVOs) {
		for (AttributeDetailVO attributeDetailVO : attributeDetailVOs) {
			if (attributeDetailVO != null && !StringUtil.isNullOrEmpty(attributeDetailVO.getName())
					&& selectedItem.equals(attributeDetailVO.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * validate du lieu thuoc tinh dong loai vi tri
	 * @author tuannd20
	 * @param validateValue du lieu can validate
	 * @param dynamicAttributeVO doi tuong mo ta thong tin thuoc tinh dong
	 * @return Chuoi gia tri loi neu co loi. Nguoc lai, null hoac chuoi rong
	 * @since 13/09/2015
	 */
	public static String validateLocationDynamicAttribute(String validateValue, AttributeDynamicVO dynamicAttributeVO) {
		String errorMsg = validateForRequiredDynamicAttribute(validateValue, dynamicAttributeVO);
		if (errorMsg == null && !StringUtil.isNullOrEmpty(validateValue)) {
			validateValue = validateValue.trim();
			final String latLngDelimiter = ";";
			String[] gpsCoordinate = validateValue.split(latLngDelimiter);
			if (gpsCoordinate.length != 2) {
				errorMsg = R.getResource("dynamic.attribute.location.gps.coordinate.invalid.format", dynamicAttributeVO.getAttributeName()) + ConstantManager.NEW_LINE_CHARACTER;
			} else {
				String lat = gpsCoordinate[0], lng = gpsCoordinate[1];
				try {
					new BigDecimal(lat);
					new BigDecimal(lng);
				} catch (NumberFormatException e) {
					errorMsg = R.getResource("dynamic.attribute.location.gps.coordinate.invalid.format", dynamicAttributeVO.getAttributeName()) + ConstantManager.NEW_LINE_CHARACTER;
				}
			}
		}
		return errorMsg;
	}

	/*
	 * validate thuoc tinh co bat buoc nhap hay khong
	 */
	private static String validateForRequiredDynamicAttribute(String cellData, AttributeDynamicVO dynamicAttributeVO) {
		if (dynamicAttributeVO.getMandatory() != null && dynamicAttributeVO.getMandatory() > 0 && StringUtil.isNullOrEmpty(cellData)) {
			return R.getResource("dynamic.attribute.is.required", dynamicAttributeVO.getAttributeName()) + ConstantManager.NEW_LINE_CHARACTER;
		}
		return null;
	}

	/**
	 * Lay gia tri thuoc tinh dong da nhap tren file import.
	 * Doi voi loai thuoc tinh chon 1 & chon nhieu thi tra ve chuoi id cua cac option, cach nhau boi dau ";"
	 *
	 * @author tuannd20
	 * @param rowData du lieu 1 row tren file import
	 * @param dataPosisiton vi tri du lieu can lay gia tri tren file import
	 * @param dynamicAttributeVO doi tuong mo ta thong tin thuoc tinh dong
	 * @return gia tri thuoc tinh dong da nhap.
	 * @since 13/09/2015
	 */
	public static String getInputDynamicAttributeValue(List<String> rowData, int dataPosisiton, AttributeDynamicVO dynamicAttributeVO) {
		String cellData = rowData.get(dataPosisiton);
		if (dynamicAttributeVO == null || dynamicAttributeVO.getType() == null) {
			return null;
		}

		if (AttributeColumnType.CHOICE.getValue().equals(dynamicAttributeVO.getType()) ||
			AttributeColumnType.MULTI_CHOICE.getValue().equals(dynamicAttributeVO.getType())) {
			final String cellDataDelimiter = ",";
			String multiSelectCellData = "";
			final String dynamicAttributeValueDelimiter = ";";
			List<AttributeDetailVO> attributeDetailVOs = dynamicAttributeVO.getAttributeDetailVOs();
			if (attributeDetailVOs != null) {
				String[] selectedItems = cellData.split(cellDataDelimiter);

				for (String selectedItem : selectedItems) {
					selectedItem = selectedItem.trim();
					for (AttributeDetailVO attributeDetailVO : attributeDetailVOs) {
						if (attributeDetailVO != null && !StringUtil.isNullOrEmpty(attributeDetailVO.getName())
								&& selectedItem.equals(attributeDetailVO.getName())) {
							multiSelectCellData += attributeDetailVO.getId() + dynamicAttributeValueDelimiter;
							break;
						}
					}
				}
			}
			if (multiSelectCellData.endsWith(dynamicAttributeValueDelimiter)) {
				multiSelectCellData = multiSelectCellData.substring(0, multiSelectCellData.length() - 1);
			}
			return multiSelectCellData;
		}
		return cellData;
	}
}

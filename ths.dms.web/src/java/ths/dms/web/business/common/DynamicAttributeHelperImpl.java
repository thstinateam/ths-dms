/**
 * Copyright (c) 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.business.common;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.AttributeMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.vo.DynamicAttributeVO;
import ths.dms.core.exceptions.BusinessException;

/**
 * lay thong tin thuoc tinh dong cua khach hang, nhan vien, san pham 
 * @author tuannd20
 * @since 11/09/2015
 */
public class DynamicAttributeHelperImpl implements DynamicAttributeHelper {
	private AttributeMgr attributeMgr;

	@Override
	public List<Object> getCustomerDynamicAttributeVO(ActiveType attributeStatus) throws BusinessException {
		if (attributeStatus == null) {
			throw new IllegalArgumentException("attribute status must not null.");
		}
		List<DynamicAttributeVO> customerDynamicAttributes = attributeMgr.getDynamicAttribute(Customer.class, attributeStatus);
		return groupDynamicAttributeVO(customerDynamicAttributes);
	}
	
	@Override
	public List<Object> getStaffDynamicAttributeVO(ActiveType attributeStatus) throws BusinessException {
		if (attributeStatus == null) {
			throw new IllegalArgumentException("attribute status must not null.");
		}
		List<DynamicAttributeVO> customerDynamicAttributes = attributeMgr.getDynamicAttribute(Staff.class, attributeStatus);
		return groupDynamicAttributeVO(customerDynamicAttributes);
	}
	
	private List<Object> groupDynamicAttributeVO(List<DynamicAttributeVO> dynamicAttributes) {
		List<Object> dynamicAttributeVOs = new ArrayList<Object>();
		if (dynamicAttributes != null) {
			for (int i = 0, size = dynamicAttributes.size(); i < size;) {
				DynamicAttributeVO dynamicAttributeVO = dynamicAttributes.get(i);
				
				AttributeDynamicVO attributeVO = new AttributeDynamicVO();
				attributeVO.setAttributeId(dynamicAttributeVO.getAttributeId());
				attributeVO.setAttributeCode(dynamicAttributeVO.getAttributeCode());
				attributeVO.setAttributeName(dynamicAttributeVO.getAttributeName());
				attributeVO.setType(dynamicAttributeVO.getAttributeType());
				attributeVO.setDataLength(dynamicAttributeVO.getAttibuteValueMaxLength());
				attributeVO.setMinValue(dynamicAttributeVO.getAttributeMinValue() != null ? dynamicAttributeVO.getAttributeMinValue().intValue() : null);
				attributeVO.setMaxValue(dynamicAttributeVO.getAttributeMaxValue() != null ? dynamicAttributeVO.getAttributeMaxValue().intValue() : null);
				attributeVO.setMandatory(dynamicAttributeVO.getAttributeMandatoy());
				
				List<AttributeDetailVO> attributeDetails = new ArrayList<AttributeDetailVO>();
				if (AttributeColumnType.CHOICE.getValue().equals(dynamicAttributeVO.getAttributeType())
						|| AttributeColumnType.MULTI_CHOICE.getValue().equals(dynamicAttributeVO.getAttributeType())) {
					int j = i;
					for (; j < size; j++) {
						DynamicAttributeVO nextDynamicAttributeVO = dynamicAttributes.get(j);
						if (!nextDynamicAttributeVO.getAttributeCode().equals(dynamicAttributeVO.getAttributeCode())) {
							break;
						}
						AttributeDetailVO attributeDetailVO = new AttributeDetailVO();
						attributeDetailVO.setId(nextDynamicAttributeVO.getAttributeDetailId());
						attributeDetailVO.setCode(nextDynamicAttributeVO.getAttributeDetailCode());
						attributeDetailVO.setName(nextDynamicAttributeVO.getAttributeDetailName());
						attributeDetails.add(attributeDetailVO);
					}
					i = j;
				} else {
					i++;
				}
				attributeVO.setAttributeDetailVOs(attributeDetails);
				
				dynamicAttributeVOs.add(attributeVO);
			}
		}
		return dynamicAttributeVOs;
	}

	@Override
	public List<Object> getProductDynamicAttributeVO(ActiveType attributeStatus) throws BusinessException {
		if (attributeStatus == null) {
			throw new IllegalArgumentException("attribute status must not null.");
		}
		List<DynamicAttributeVO> productDynamicAttributes = attributeMgr.getDynamicAttribute(Product.class, attributeStatus);
		return groupDynamicAttributeVO(productDynamicAttributes);
	}
	
	public void setAttributeMgr(AttributeMgr attributeMgr) {
		this.attributeMgr = attributeMgr;
	}
}

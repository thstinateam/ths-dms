package ths.dms.web.action.workingplan;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.WorkingDateConfig;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.WorkingDateConfigFilter;
import ths.dms.core.entities.enumtype.WorkingDateProcedureType;
import ths.dms.core.entities.enumtype.WorkingDateType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.WorkDateTempVO;
import ths.dms.core.entities.vo.WorkingDateConfigVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import viettel.passport.client.ShopToken;

public class WorkingDatePlanAction extends AbstractAction {
	private static final long serialVersionUID = -2128120975032582873L;

	/** The ap param mgr. */
	private ApParamMgr apParamMgr;

	/** The exception day mgr. */
	private ExceptionDayMgr exceptionDayMgr;

	private ShopMgr shopMgr;

	/** The lst reasons. */
	private List<ApParam> lstReasons;

	/** The date type. */
	private int dateType;

	/** The date. */
	private String dateValue;

	/** The reason id. */
	private Long reasonId;

	private String code;

	/** The lst year. */
	private List<StatusBean> lstYears;

	private List<CellBean> lstBeanMonth;

	private List<CellBean> lstBeanDateType;

	private List<ShopVO> lstChildShop;

	/** The current date. */
	private int currentYear;

	/** The current month. */
	private int currentMonth;

	/** The excel file. */
	private File excelFile;

	/** The excel file content type. */
	private String excelFileContentType;

	private int month;
	private int year;

	private String reasonCode;
	private String shopCodeStr;

	private String fromMonth;
	private String toMonth;
	private Integer type;
	private Long shopId;
	private List<Integer> lstDefaultWork;
	private List<WorkDateTempVO> lstWorkDate;
	private List<WorkingDateConfig> lstWorkingDateConfig;

	public String getShopCodeStr() {
		return shopCodeStr;
	}

	public void setShopCodeStr(String shopCodeStr) {
		this.shopCodeStr = shopCodeStr;
	}

	/**
	 * @return the lstReasons
	 */
	public List<ApParam> getLstReasons() {
		return lstReasons;
	}

	/**
	 * @param lstReasons
	 *            the lstReasons to set
	 */
	public void setLstReasons(List<ApParam> lstReasons) {
		this.lstReasons = lstReasons;
	}

	/**
	 * @return the dateType
	 */
	public int getDateType() {
		return dateType;
	}

	/**
	 * @param dateType
	 *            the dateType to set
	 */
	public void setDateType(int dateType) {
		this.dateType = dateType;
	}

	/**
	 * @return the reasonId
	 */
	public Long getReasonId() {
		return reasonId;
	}

	/**
	 * @param reasonId
	 *            the reasonId to set
	 */
	public void setReasonId(Long reasonId) {
		this.reasonId = reasonId;
	}

	/**
	 * @return the dateValue
	 */
	public String getDateValue() {
		return dateValue;
	}

	/**
	 * @param dateValue
	 *            the dateValue to set
	 */
	public void setDateValue(String dateValue) {
		this.dateValue = dateValue;
	}

	/**
	 * @return the lstYears
	 */
	public List<StatusBean> getLstYears() {
		return lstYears;
	}

	/**
	 * @param lstYears
	 *            the lstYears to set
	 */
	public void setLstYears(List<StatusBean> lstYears) {
		this.lstYears = lstYears;
	}

	/**
	 * @return the currentYear
	 */
	public int getCurrentYear() {
		return currentYear;
	}

	/**
	 * @param currentYear
	 *            the currentYear to set
	 */
	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}

	/**
	 * @return the currentMonth
	 */
	public int getCurrentMonth() {
		return currentMonth;
	}

	/**
	 * @param currentMonth
	 *            the currentMonth to set
	 */
	public void setCurrentMonth(int currentMonth) {
		this.currentMonth = currentMonth;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		exceptionDayMgr = (ExceptionDayMgr) context.getBean("exceptionDayMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			mapControl = getMapControlToken();
			lstReasons = apParamMgr.getOffDateReason();
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(DateUtil.now());
			dateValue = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			currentYear = calendar.get(Calendar.YEAR);
			currentMonth = calendar.get(Calendar.MONTH) + 1;
			lstYears = new ArrayList<StatusBean>();
			for (int i = (currentYear - 10); i <= (currentYear + 10); i++) {
				StatusBean year = new StatusBean();
				year.setName(String.valueOf(i));
				year.setValue(Long.valueOf(i));
				lstYears.add(year);
			}
			ExceptionDay excDay = exceptionDayMgr.getExceptionDayByDate(DateUtil.now(), -1L);
			if (excDay != null) {
				dateType = 1;
				if (excDay.getReason() != null) {
					reasonCode = excDay.getReason();
				}
			}
			ShopToken shToken = currentUser.getShopRoot();
			if (shToken != null && (ShopObjectType.NPP.getValue().equals(shToken.getObjectType()) || ShopObjectType.NPP_KA.getValue().equals(shToken.getObjectType()))) {
				shopCodeStr = shToken.getShopCode();
			}

			/** Check type trong working_date_config **/
			WorkingDateConfigFilter filter = new WorkingDateConfigFilter();
			filter.setShopId(shopId);
			lstWorkingDateConfig = exceptionDayMgr.getListWorkingDateConfigByFilter(filter);
			lstBeanMonth = new ArrayList<CellBean>();
			for (int i = 1; i <= 12; i++) {
				CellBean cell = new CellBean();
				cell.setContent1(String.valueOf(i));
				cell.setContent2(R.getResource("work.date.month.title" + String.valueOf(i)));
				lstBeanMonth.add(cell);
			}
			lstBeanDateType = new ArrayList<CellBean>();
			for (int i = 0; i < 2; i++) {
				CellBean cell = new CellBean();
				cell.setContent1(String.valueOf(i));
				cell.setContent2(R.getResource("work.date.type" + String.valueOf(i)));
				lstBeanDateType.add(cell);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "WorkingDatePlanAction.execute()" + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Save or update.
	 * 
	 * @return the string
	 * @author liemtpt
	 * @since 29/01/2015
	 */
	public String saveOrUpdate() {
		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				Long shopId = -1L;
				Date excDate = null;
				Shop shopTemp = null;
				if (!StringUtil.isNullOrEmpty(dateValue)) {
					excDate = DateUtil.parse(dateValue, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (excDate == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
					return JSON;
				}
				if (DateUtil.compareDateWithoutTime(excDate, DateUtil.now()) < 1) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.set.up.day.off.past"));
					return JSON;
				}
				if (!StringUtil.isNullOrEmpty(shopCodeStr)) {
					shopTemp = shopMgr.getShopByCode(shopCodeStr);
					if (shopTemp == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code")));
						return JSON;
					} else if (!checkShopInOrgAccessByCode(shopCodeStr)) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree")));
						return JSON;
					} else if (ActiveType.STOPPED.equals(shopTemp.getStatus())) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.promotion.effect", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code")));
						return JSON;
					} else {
						shopId = shopTemp.getId();
					}
				}
				ExceptionDay excDay = exceptionDayMgr.getExceptionDayByDate(excDate, shopId);
				if (dateType == 0) {
					if (excDay != null) {
						excDay.setUpdateDate(DateUtil.now());
						excDay.setUpdateUser(currentUser.getUserName());
						exceptionDayMgr.deleteExceptionDay(excDay, getLogInfoVO());
						exceptionDayMgr.callProceduceWorkingDate(shopId, excDate, excDate, WorkingDateProcedureType.XOA_NGAY_NGAY_NGHI);
						error = false;
					} else {
						error = false;
					}
				} else {
					//Them check XSS du lieu
					errMsg = "";
					if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(reasonCode)) {
						errMsg = ValidateUtil.validateField(reasonCode, "work.date.reason", 100, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
					}
					if (!StringUtil.isNullOrEmpty(errMsg)) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					if (excDay != null) {
						excDay.setReason(reasonCode);
						excDay.setUpdateDate(DateUtil.now());
						excDay.setUpdateUser(currentUser.getUserName());
						exceptionDayMgr.updateExceptionDay(excDay, getLogInfoVO());
						exceptionDayMgr.callProceduceWorkingDate(shopId, excDate, excDate, WorkingDateProcedureType.THEM_NGAY_NGAY_NGHI);
						error = false;
					} else {
						excDay = new ExceptionDay();
						excDay.setReason(reasonCode);
						excDay.setDayOff(excDate);
						if (shopTemp != null) {
							excDay.setShop(shopTemp);
						}
						excDay.setCreateDate(DateUtil.now());
						excDay.setCreateUser(currentUser.getUserName());
						exceptionDayMgr.createExceptionDay(excDay, getLogInfoVO());
						exceptionDayMgr.callProceduceWorkingDate(shopId, excDate, excDate, WorkingDateProcedureType.THEM_NGAY_NGAY_NGHI);
						error = false;
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, "WorkingDatePlanAction.saveOrUpdate()" + e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * inherit working date
	 * 
	 * @return the string
	 * @author liemtpt
	 * @since 29/01/2015
	 */
	public String inheritWorkingDate() {
		resetToken(result);
		if (currentUser != null) {
			try {
				Shop sh = shopMgr.getShopById(shopId);
				if (sh == null) {
					return JSON;
				}
				Integer value = exceptionDayMgr.inheritWorkingDate(sh, getLogInfoVO());
				if (value == WorkingDateType.BAN_THAN_DON_VI.getValue()) {
					Date sys = DateUtil.getCurrentGMTDate();
					Date sysNext = DateUtil.getFirstNextMonth(sys);
					exceptionDayMgr.callProceduceWorkingDate(sh.getId(), sysNext, null, WorkingDateProcedureType.THUA_KE_CHA);
				}
				result.put(ERROR, false);
			} catch (Exception e) {
				LogUtility.logError(e, "WorkingDatePlanAction.inheritWorkingDate()" + e.getMessage());
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		}
		return JSON;
	}

	public String checkTypeWorkingDateConfig() {
		Boolean flag = null;
		if (shopId != null) {
			WorkingDateConfigFilter filter = new WorkingDateConfigFilter();
			filter.setShopId(shopId);
			filter.setStatus(ActiveType.RUNNING);
			filter.setType(WorkingDateType.BAN_THAN_DON_VI);
			try {
				List<WorkingDateConfig> lst = exceptionDayMgr.getListWorkingDateConfigByFilter(filter);
				if (lst != null && lst.size() > 0) {
					flag = true;
				} else {
					flag = false;
				}
			} catch (BusinessException e) {
				LogUtility.logError(e, "WorkingDatePlanAction.checkTypeWorkingDateConfig()" + e.getMessage());
			}
		}
		result.put("checkType", flag);
		return JSON;
	}

	public String getShopParentWorkingDateConfig() {
		if (shopId != null) {
			try {
				Shop sh = exceptionDayMgr.getShopParentWorkingDateConfig(shopId, WorkingDateType.BAN_THAN_DON_VI);
				if (sh != null) {
					String parentCodeName = sh.getShopCode() + " - " + sh.getShopName();
					result.put("parentCodeName", parentCodeName);
				}
			} catch (BusinessException e) {
				LogUtility.logError(e, "WorkingDatePlanAction.getShopParentWorkingDateConfig()" + e.getMessage());
			}
		}
		return JSON;
	}

	/**
	 * get list childs shop by filter
	 * 
	 * @author liemtpt
	 * @since Feb 06, 2015
	 * @return JSON
	 * */
	public String getListChildShop() {
		try {
			BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
			filter.setUserId(currentUser.getUserId());
			filter.setId(currentUser.getShopRoot().getShopId());
			filter.setLongG(currentUser.getRoleToken().getRoleId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setStrShopId(getStrListShopId());
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			lstChildShop = shopMgr.getListChildShopVOByFilter(filter);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put("rows", lstChildShop);
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * @author vuongmq Save or update.
	 * @return the string
	 * @description tao thiet lap cho popup
	 * @since 30/01/2015
	 */
	public String createWorkDate() {
		resetToken(result);
		if (currentUser != null) {
			try {
				Shop shopTemp = null;
				if (shopId != null) {
					shopTemp = shopMgr.getShopById(shopId);
					if (shopTemp == null) {
						shopTemp = shopMgr.getShopByCode(shopCodeStr);
						if (shopTemp == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree")));
							return JSON;
						} else if (!checkShopInOrgAccessByCode(shopCodeStr)) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree")));
							return JSON;
						} else if (ActiveType.STOPPED.equals(shopTemp.getStatus())) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.promotion.effect", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code")));
							return JSON;
						}
					} else if (!checkShopInOrgAccessByCode(shopTemp.getShopCode())) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree")));
						return JSON;
					} else if (ActiveType.STOPPED.equals(shopTemp.getStatus())) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.promotion.effect", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code")));
						return JSON;
					}
				} else {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.not.exist"));
					return JSON;
				}
				// fromMonth
				if (StringUtil.isNullOrEmpty(fromMonth)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "work.date.chon.tu.thang.thiet.lap"));
					return JSON;
				}
				// toMonth
				if (StringUtil.isNullOrEmpty(toMonth)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "work.date.chon.den.thang.thiet.lap"));
					return JSON;
				}
				String[] toMonthStr = toMonth.split("/");
				if (!StringUtil.isNullOrEmpty(toMonthStr[1])) {
					Integer currentYear = DateUtil.getYear(DateUtil.getCurrentGMTDate());
					Integer toMonthCheck = Integer.parseInt(toMonthStr[1]);
					if (toMonthCheck > (currentYear + 10)) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "work.date.chon.tu.den.thang.thiet.lap.khong.qua.10.nam"));
						return JSON;
					}
				}
				if (type != null) {
					// xu ly tao thiet lap
					Integer value = exceptionDayMgr.createWorkDate(shopTemp, type, fromMonth, toMonth, lstWorkDate, lstDefaultWork, getLogInfoVO());
					if (value == WorkingDateType.DE_QUY.getValue()) {
						/** +Nếu đang sử dụng lịch theo lịch cha: */
						Date fromMonthDate = DateUtil.parse("01/" + fromMonth, DateUtil.DATE_FORMAT_DDMMYYYY);
						Date toMonthView = DateUtil.parse("01/" + toMonth, DateUtil.DATE_FORMAT_DDMMYYYY); // ngay dau thang
						Date toMonthDate = DateUtil.getLastDateInMonth(toMonthView); // ngay cuoi thang
						exceptionDayMgr.callProceduceWorkingDate(shopTemp.getId(), fromMonthDate, toMonthDate, WorkingDateProcedureType.THIET_LAP_RIENG);
					} else if (value == WorkingDateType.BAN_THAN_DON_VI.getValue()) {
						/** + Nếu đang sử dụng lịch riêng: */
						Date fromMonthDate = DateUtil.parse("01/" + fromMonth, DateUtil.DATE_FORMAT_DDMMYYYY);
						Date toMonthView = DateUtil.parse("01/" + toMonth, DateUtil.DATE_FORMAT_DDMMYYYY); // lay ngay dau cua thang de lay sql
						Date toMonthDate = DateUtil.getLastDateInMonth(toMonthView); // lay ngay cuoi thang de lay sanh sach ngay lam viec
						exceptionDayMgr.callProceduceWorkingDate(shopTemp.getId(), fromMonthDate, toMonthDate, WorkingDateProcedureType.THUA_KE_CHA);
					}
				} else {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "work.date.chon.thiet.lap"));
					return JSON;
				}
				result.put(ERROR, false);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				result.put(ERROR, true);
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				result.put("errMsg", errMsg);
			}
		}
		return JSON;
	}

	/**
	 * @author vuongmq
	 * @return the string
	 * @description lay cha thiet lap rieng
	 * @since 3/02/2015
	 */
	public String getShopParentWorkingDateConfigCheckParent() {
		try {
			Shop shopTemp = null;
			if (shopId != null && shopId > 0) {
				shopTemp = shopMgr.getShopById(shopId);
				if (shopTemp == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.not.exist"));
					return JSON;
				} else if (!checkShopInOrgAccessByCode(shopTemp.getShopCode())) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree")));
					return JSON;
				} else if (ActiveType.STOPPED.equals(shopTemp.getStatus())) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.promotion.effect", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code")));
					return JSON;
				} else {
					shopId = shopTemp.getId();
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.not.exist"));
				return JSON;
			}
			List<WorkingDateConfigVO> lstShopParent = exceptionDayMgr.getWorkDateConfigDateCheckParent(shopId, WorkingDateType.BAN_THAN_DON_VI, ActiveType.RUNNING);
			result.put("flagCheckExitsParent", false);
			if (lstShopParent != null && lstShopParent.size() > 0) {
				if (lstShopParent.get(0).getShopCode() != null && lstShopParent.get(0).getShopName() != null) {
					String parentCodeName = lstShopParent.get(0).getShopCode() + " - " + lstShopParent.get(0).getShopName();
					result.put("parentCodeName", parentCodeName);
				} else {
					result.put("parentCodeName", "");
				}
				result.put("flagCheckExitsParent", true);
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, "WorkingDatePlanAction.getShopParentWorkingDateConfigCheckParent()" + e.getMessage());
			result.put(ERROR, true);
		}
		return JSON;
	}

	/*
	 * public String importExcel() throws Exception { resetToken(result);
	 * isError = true; errMsg = ValidateUtil.validateExcelFile(excelFile,
	 * excelFileContentType); totalItem = 0; String message = ""; lstView = new
	 * ArrayList<CellBean>(); typeView = true; List<CellBean> lstFails = new
	 * ArrayList<CellBean>(); List<List<String>> lstData =
	 * getExcelData(excelFile, excelFileContentType, errMsg,3); if
	 * (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() >
	 * 0) { try { for(int i=0;i<lstData.size();i++){ if(lstData.get(i)!= null &&
	 * lstData.get(i).size() > 0){ message = ""; totalItem++; List<String> row =
	 * lstData.get(i);
	 * 
	 * Shop shop = null; if(row.size() > 0){ String value = row.get(0);
	 * if(!StringUtil.isNullOrEmpty(value)){ shop =
	 * shopMgr.getShopByCode(value); if(shop == null){ message +=
	 * ValidateUtil.getErrorMsgQuickly
	 * (ConstantManager.ERR_NOT_EXIST_DB,true,"catalog.focus.program.shop.code"
	 * ); }else
	 * if(!shop.getType().getObjectType().equals(ShopObjectType.GT.getValue())
	 * && !shop.getType().getObjectType().equals(ShopObjectType.MIEN.getValue())
	 * && !shop.getType().getObjectType().equals(ShopObjectType.VUNG.getValue())
	 * &&
	 * !shop.getType().getObjectType().equals(ShopObjectType.NPP.getValue())){
	 * message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	 * "catalog.display.program.not.apply.shop"); } else if
	 * (!checkShopInOrgAccessByCode(value)) { message +=
	 * ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN,
	 * true, "catalog.unit.tree"); } else
	 * if(!shop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
	 * message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	 * "catalog.display.program.shop.not.running"); } } else { message +=
	 * ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,true,
	 * "catalog.focus.program.shop.code"); } } //date Date saleDate = null;
	 * if(row.size() > 1){ String value = row.get(1);
	 * if(!StringUtil.isNullOrEmpty(value)){ try { if
	 * (!StringUtil.isNullOrEmpty(value)) { if
	 * (DateUtil.checkInvalidFormatDate(value)) { message +=
	 * ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID_DATE, true,
	 * "sale.plan.exception.day"); } else { saleDate = DateUtil.parse(value,
	 * DateUtil.DATE_FORMAT_STR); if(saleDate!= null &&
	 * DateUtil.compareDateWithoutTime(DateUtil.now(),saleDate) > -1){ message+=
	 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	 * "ss.greater.datenow.general",
	 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	 * "sale.plan.exception.day")); } else if(saleDate!= null){ Calendar
	 * calendar = new GregorianCalendar(); calendar.setTime(saleDate);
	 * if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){ message+=
	 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	 * "sale.plan.exception.sunday") + "\n"; } } } } } catch (Exception e) {
	 * LogUtility.logError(e, e.getMessage()); } String msg =
	 * ValidateUtil.getErrorMsgForInvalidFormatDate
	 * (value,Configuration.getResourceString
	 * (ConstantManager.VI_LANGUAGE,"sale.plan.exception.day"),null);
	 * if(!StringUtil.isNullOrEmpty(msg)){ message+= msg + "\n"; } } else {
	 * message +=
	 * ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,true
	 * ,"sale.plan.exception.day"); } }
	 * 
	 * // reason ApParam reason = null; String reasonCode = ""; if(row.size() >
	 * 2){ String value = row.get(2); if(!StringUtil.isNullOrEmpty(value)){
	 * reason = apParamMgr.getApParamByCodeX(value, ApParamType.OFFDATE, null);
	 * if(reason== null){ message +=
	 * ValidateUtil.getErrorMsgQuickly(ConstantManager
	 * .ERR_NOT_EXIST,true,"catalog.reason"); } else
	 * if(!ActiveType.RUNNING.getValue().equals(reason.getStatus().getValue())){
	 * message +=
	 * ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,
	 * true,"catalog.reason"); } else { reasonCode = value; } } else { message
	 * += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,true,
	 * "catalog.reason"); } }
	 * 
	 * if (StringUtil.isNullOrEmpty(message)){ ExceptionDay excDay =
	 * exceptionDayMgr.getExceptionDayByDate(saleDate,shop.getId()); if(excDay!=
	 * null){ excDay.setType(reasonCode.toUpperCase());
	 * excDay.setUpdateDate(DateUtil.now());
	 * excDay.setUpdateUser(currentUser.getUserName());
	 * exceptionDayMgr.updateExceptionDay(excDay, getLogInfoVO()); } else {
	 * excDay = new ExceptionDay(); excDay.setDayOff(saleDate);
	 * excDay.setType(reasonCode.toUpperCase()); excDay.setShop(shop);
	 * excDay.setCreateDate(DateUtil.now());
	 * excDay.setCreateUser(currentUser.getUserName());
	 * exceptionDayMgr.createExceptionDay(excDay, getLogInfoVO()); } } else {
	 * lstFails.add(StringUtil.addFailBean(row, message)); } typeView = false; }
	 * } //Export error getOutputFailExcelFile(lstFails,
	 * ConstantManager.TEMPLATE_SALE_DAY_EXCEPTION_FAIL); } catch (Exception e)
	 * { LogUtility.logError(e, e.getMessage()); errMsg =
	 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM); } } if
	 * (StringUtil.isNullOrEmpty(errMsg)) { isError = false; } return SUCCESS; }
	 * 
	 * public String exportExcel(){ try{ List<ExceptionDay> lstDays = null;
	 * List<CellBean> lstDayOff = new ArrayList<CellBean>(); Long shopId = -1L;
	 * if(!StringUtil.isNullOrEmpty(shopCode)){ if
	 * (!checkShopInOrgAccessByCode(shopCode)) { result.put(ERROR,true);
	 * result.put("errMsg",
	 * ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN,
	 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	 * "catalog.unit.tree"))); return JSON; } Shop s =
	 * shopMgr.getShopByCode(shopCode); if(s!=null){ shopId = s.getId(); } }
	 * if(month > 0 && year > 0){ lstDays =
	 * exceptionDayMgr.getExceptionDayByMonth(month, year,shopId); } else
	 * if(year > 0){ lstDays =
	 * exceptionDayMgr.getExceptionDayByYear(year,shopId); } if(lstDays!= null
	 * && !lstDays.isEmpty()){ for (ExceptionDay excDay : lstDays) { if(excDay!=
	 * null){ CellBean row = new CellBean(); if(excDay.getShop() != null){
	 * row.setContent1(excDay.getShop().getShopCode()); }
	 * row.setContent2(DateUtil.toDateString(excDay.getDayOff(),
	 * DateUtil.DATE_FORMAT_STR)); if(excDay.getType()!= null){
	 * row.setContent3(excDay.getType()); } lstDayOff.add(row); } }
	 * exportExcelDataNotSession
	 * (lstDayOff,ConstantManager.TEMPLATE_SALE_DAY_EXCEPTION_EXPORT);
	 * result.put(ERROR, false); }else{ result.put("hasData", false);
	 * result.put(ERROR, false); } }catch(Exception e){ LogUtility.logError(e,
	 * e.getMessage()); } return JSON; }
	 */

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public String getToMonth() {
		return toMonth;
	}

	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public List<Integer> getLstDefaultWork() {
		return lstDefaultWork;
	}

	public void setLstDefaultWork(List<Integer> lstDefaultWork) {
		this.lstDefaultWork = lstDefaultWork;
	}

	public List<WorkDateTempVO> getLstWorkDate() {
		return lstWorkDate;
	}

	public void setLstWorkDate(List<WorkDateTempVO> lstWorkDate) {
		this.lstWorkDate = lstWorkDate;
	}

	public List<WorkingDateConfig> getLstWorkingDateConfig() {
		return lstWorkingDateConfig;
	}

	public void setLstWorkingDateConfig(List<WorkingDateConfig> lstWorkingDateConfig) {
		this.lstWorkingDateConfig = lstWorkingDateConfig;
	}

	public List<CellBean> getLstBeanMonth() {
		return lstBeanMonth;
	}

	public void setLstBeanMonth(List<CellBean> lstBeanMonth) {
		this.lstBeanMonth = lstBeanMonth;
	}

	public List<CellBean> getLstBeanDateType() {
		return lstBeanDateType;
	}

	public void setLstBeanDateType(List<CellBean> lstBeanDateType) {
		this.lstBeanDateType = lstBeanDateType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<ShopVO> getLstChildShop() {
		return lstChildShop;
	}

	public void setLstChildShop(List<ShopVO> lstChildShop) {
		this.lstChildShop = lstChildShop;
	}
}

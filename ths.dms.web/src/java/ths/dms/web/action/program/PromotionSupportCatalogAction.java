package ths.dms.web.action.program;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.TreeGridNode;
import ths.dms.web.bean.TreeNode;
import ths.dms.web.business.common.TreeUtility;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CustomerAttributeMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerAttribute;
import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.PromotionCustAttrDetail;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PromotionProgramFilter;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.filter.PromotionCustomerFilter;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductInfoVO;
import ths.dms.core.entities.vo.PromotionCustAttUpdateVO;
import ths.dms.core.entities.vo.PromotionCustAttVO2;
import ths.dms.core.entities.vo.PromotionCustAttrVO;
import ths.dms.core.entities.vo.PromotionCustomerVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.PromotionShopVO;
import ths.dms.core.entities.vo.SaleCatLevelVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

public class PromotionSupportCatalogAction extends AbstractAction {
	private PromotionProgramMgr promotionProgramMgr;
	private CustomerMgr customerMgr;
	private ApParamMgr apParamMgr;
	//private ProductMgr productMgr;
	private static final long serialVersionUID = 1L;
	public static final int CUSTOMER_TYPE = 2;
	public static final int SALE_LEVEL = 3;
	public static final int AUTO_ATTRIBUTE = 1;
	public static final int NOT_NPP = 0;
	public static final int NPP = 1;

	private Boolean isVNMAdmin;
	private List<ApParam> lstTypeCode;
	private Integer proType;
	private String fromDate;
	private String toDate;
	private String lstTypeId;
	private Integer status;
	private String code;
	private String name;
	private Long promotionId;
	private PromotionProgram promotionProgram;
	private Long id;
	private Long shopId;
	private Integer quantity;
	private Integer quantityMax;
	private Integer amountMax;
	private List<Integer> lstQtt;
	private List<Long> lstId;
	private List<TreeGridNode<PromotionShopVO>> lstTree;
	private String address;
	private String excelFileContentType;
	private File excelFile;
	private List<PromotionCustAttrVO> lstPromotionCustAttrVO;
	private List<Integer> lstObjectType;
	private List<String> lstAttDataInField;
	private List<Long> lstCustomerType;
	private List<Long> lstSaleLevelCatId;
	private List<TreeNode> searchUnitTree;

	private String promotionCode;
	private String promotionName;
	private String startDate;
	private String endDate;
	private String typeCode;
	private String description;
	private String quantityObjectStr;
	private String quantityMaxObjectStr;
	private String amountMaxObjectStr;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		isVNMAdmin = true;
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		try {
			lstTypeCode = apParamMgr.getListApParam(ApParamType.PROMOTION_MANUAL, ActiveType.RUNNING);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.PromotionSupportCatalogAction.execute", createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * tra ve cau truc cay don vi tim kiem
	 * 
	 * @author trietptm
	 * @return dinh dang du lieu tra ve
	 * @since 24/06/2015
	 */
	public String buildSearchUnitTree() {
		TreeUtility treeUtility = new TreeUtility(context);
		searchUnitTree = treeUtility.buildUnitTree(currentUser.getShopRoot().getShopId(), true, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
		return JSON;
	}

	/**
	 * Tim kiem CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 21, 2014
	 * 
	 * @modify hunglm16
	 * @since 18/11/2015
	 */
	public String search() throws Exception {
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				return JSON;
			}			
			result.put("page", page);
			result.put("max", max);
			
			PromotionProgramFilter filter = new PromotionProgramFilter();
			//if(type == 0){
			KPaging<PromotionProgram> kPaging = new KPaging<PromotionProgram>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);

			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = ths.dms.web.utils.DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = ths.dms.web.utils.DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setToDate(tDate);
			}
			List<String> temp = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty(lstTypeId)) {
				if (lstTypeId.indexOf("-1") > -1) {
					temp = null;
				} else {
					String[] lstTmp = lstTypeId.split(",");
					if (lstTmp.length > 0) {
						Integer size = lstTmp.length;
						for (int i = 0; i < size; i++) {
							ApParam apParam = apParamMgr.getApParamById(Long.valueOf(lstTmp[i].trim()));
							if (apParam != null) {
								temp.add(apParam.getApParamCode());
							}
						}
					}
				}
			}
			filter.setLstType(temp);
			ActiveType at = null;
			if (status == null) {
				status = 1;
			}
			if (status != ConstantManager.NOT_STATUS) {
				at = ActiveType.parseValue(status);
				filter.setStatus(at);
			} 
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(shopCode);
			} else {
				filter.setShopCode(currentUser.getShopRoot().getShopCode());
				filter.setCreateUser(currentUser.getStaffRoot().getStaffCode());
			}
			filter.setPpCode(code);
			filter.setPpName(name);
			filter.setIsAutoPromotion(false);
			filter.setStrListShopId(getStrListShopId());
			ObjectVO<PromotionProgram> objVO = promotionProgramMgr.getListPromotionProgram(filter);

			result.put(ERROR, false);
			result.put("rows", objVO.getLstObject());
			result.put("total", objVO.getkPaging().getTotalRows());
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.search", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Them, cap nhat chuong trinh khuyen mai tay
	 * @modify hunglm16
	 * @return
	 * @since 16/11/2015
	 */
	public String update() {
		resetToken(result);
		try {
			if (promotionId != null && promotionId > 0) {
				PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
				if (promotionProgram == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
					return SUCCESS;
				}
				Date __startDate = null;
				Date __endDate = null;
				if (!StringUtil.isNullOrEmpty(startDate)) {
					__startDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (!StringUtil.isNullOrEmpty(endDate)) {
					__endDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (__startDate != null && __endDate != null && DateUtil.compareDateWithoutTime(__startDate, __endDate) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.fromdate.greater.todate"));
					return SUCCESS;
				}
				if (ActiveType.WAITING.equals(promotionProgram.getStatus()) && ActiveType.RUNNING.equals(ActiveType.parseValue(status))) {
					List<PromotionShopVO> listShopMap = promotionProgramMgr.getShopTreeInPromotionProgram(currentUser.getShopRoot().getShopId(), promotionProgram.getId(), null, null, null);
					if (listShopMap.isEmpty()) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.shop.map.is.empty"));
						result.put(ERROR, true);
						return SUCCESS;
					} 
				}
				//Them check XSS du lieu
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(promotionName)) {
					errMsg = ValidateUtil.validateField(promotionName, "catalog.promotion.name", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(description)) {
					errMsg = ValidateUtil.validateField(description, "common.description.lable.gc", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				
				promotionProgram.setPromotionProgramName(promotionName);
				promotionProgram.setFromDate(__startDate);
				promotionProgram.setToDate(__endDate);
				if (status != null) {
					promotionProgram.setStatus(ActiveType.parseValue(status));
				}
				promotionProgram.setDescription(description);
				promotionProgramMgr.updatePromotionProgram(promotionProgram, getLogInfoVO());
				result.put(ERROR, false);
				result.put("promotionId", promotionProgram.getId());
			} else {
				PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramByCode(promotionCode);
				if (promotionProgram != null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, promotionCode));
					result.put(ERROR, true);
					return SUCCESS;
				}
				Date __startDate = null;
				Date __endDate = null;
				if (!StringUtil.isNullOrEmpty(startDate)) {
					__startDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (!StringUtil.isNullOrEmpty(endDate)) {
					__endDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (__startDate != null && __endDate != null && DateUtil.compareDateWithoutTime(__startDate, __endDate) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.fromdate.greater.todate"));
					return SUCCESS;
				}
				//Them check XSS du lieu
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(promotionCode)) {
					errMsg = ValidateUtil.validateField(promotionCode, "catalog.promotion.code", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(promotionName)) {
					errMsg = ValidateUtil.validateField(promotionName, "catalog.promotion.name", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(description)) {
					errMsg = ValidateUtil.validateField(description, "common.description.lable.gc", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				promotionProgram = new PromotionProgram();
				promotionProgram.setPromotionProgramCode(promotionCode);
				promotionProgram.setPromotionProgramName(promotionName);
				promotionProgram.setFromDate(__startDate);
				promotionProgram.setToDate(__endDate);
				promotionProgram.setStatus(ActiveType.WAITING);
				promotionProgram.setType(typeCode);
				ApParam apParam = apParamMgr.getApParamByCode(typeCode, ApParamType.PROMOTION);
				if (apParam != null) {
					promotionProgram.setProFormat(apParam.getValue());
				}
				promotionProgram.setDescription(description);
				promotionProgram = promotionProgramMgr.createPromotionProgram(promotionProgram, getLogInfoVO());
				result.put(ERROR, false);
				result.put("promotionId", promotionProgram.getId());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.PromotionSupportCatalogAction.update", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	/**
	 * Xem thong tin CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 21, 2014
	 */
	public String viewDetail() {
		try {
			lstTypeCode = apParamMgr.getListApParam(ApParamType.PROMOTION_MANUAL, ActiveType.RUNNING);
			if (promotionId == null || promotionId == 0) {
				return SUCCESS;
			}
			promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
			id = promotionId;
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.viewDetail", createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * Xem thong tin don vi CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 14, 2014
	 */
	public String viewDetailShop() throws Exception {
		return SUCCESS;
	}

	/**
	 * Tim kiem don vi thuoc CTMK
	 * 
	 * @author lacnv1
	 * @since Aug 14, 2014
	 */
	public String searchShopOfPromotion() throws Exception {
		List<TreeGridNode<PromotionShopVO>> tree = new ArrayList<TreeGridNode<PromotionShopVO>>();
		try {
			Long shId = currentUser.getShopRoot().getShopId();
			if (promotionId == null || promotionId <= 0) {
				result.put("rows", tree);
				return JSON;
			}
			List<PromotionShopVO> lstTemp = promotionProgramMgr.getShopTreeInPromotionProgram(shId, promotionId, code, name, quantity);

			if (lstTemp == null || lstTemp.size() == 0) {
				result.put("rows", tree);
				return JSON;
			}
			
			//Filter cac shop trung
			ArrayList<PromotionShopVO> lst = new ArrayList<>();
			for(PromotionShopVO shopVO : lstTemp) {
				boolean isExist = false;
				
				for(PromotionShopVO shopAdd : lst) {
					if(shopVO.getId().equals(shopAdd.getId())) {
						isExist = true;
					}
				}
				
				if(!isExist) {
					lst.add(shopVO);
				}
			}

			// Tao cay
			int i = 0, sz = lst.size();
			PromotionShopVO vo = null;			
			boolean flag = false;
			for (i = 0; i < sz; i++) {
				vo = lst.get(i);
				if (shId.equals(vo.getId())) {
					i++;
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.put("rows", tree);
				return JSON;
			}
			//PromotionShopVO vo = lst.get(0);
			TreeGridNode<PromotionShopVO> node = new TreeGridNode<PromotionShopVO>();
			node.setNodeId(vo.getId().toString());
			node.setAttr(vo);
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
			node.setText(vo.getShopCode() + " - " + vo.getShopName());
			List<TreeGridNode<PromotionShopVO>> chidren = new ArrayList<TreeGridNode<PromotionShopVO>>();
			node.setChildren(chidren);
			tree.add(node);

			TreeGridNode<PromotionShopVO> tmp = null;
			TreeGridNode<PromotionShopVO> tmp2 = null;
			for (; i < sz; i++) {
				vo = lst.get(i);
				
				if (vo.getParentId() == null) {
					continue;
				}

				tmp2 = getNodeFromTree(tree, vo.getParentId().toString());
				if (tmp2 != null) {
					tmp = new TreeGridNode<PromotionShopVO>();
					tmp.setNodeId(vo.getId().toString());
					tmp.setAttr(vo);
					if (0 == vo.getIsNPP()) {
						tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					} else {
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

					if (tmp2.getChildren() == null) {
						tmp2.setChildren(new ArrayList<TreeGridNode<PromotionShopVO>>());
					}
					tmp2.getChildren().add(tmp);
				}
			}

			result.put("rows", tree);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.searchShopOfPromotion", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Lay node trong cay
	 * 
	 * @author lacnv1
	 * @since Aug 15, 2014
	 */
	private <T> TreeGridNode<T> getNodeFromTree(List<TreeGridNode<T>> treeTmp, String nodeId) throws Exception {
		if (treeTmp == null) {
			return null;
		}
		TreeGridNode<T> node = null;
		TreeGridNode<T> tmp = null;
		for (int i = 0, sz = treeTmp.size(); i < sz; i++) {
			node = treeTmp.get(i);
			if (node.getNodeId().equals(nodeId)) {
				return node;
			}
			tmp = getNodeFromTree(node.getChildren(), nodeId);
			if (tmp != null) {
				return tmp;
			}
		}
		return null;
	}

	/**
	 * Them don vi vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 20, 2014
	 * 
	 * Cap nhat So suat, So luong, So tien
	 * @author hunglm16
	 * @since 21/11/2015
	 */
	public String addPromotionShop() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || lstId == null || lstId.size() == 0) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			// kiem tra don vi, don vi con da thuoc CTKM
			List<PromotionShopVO> lstTmp = promotionProgramMgr.getListShopInPromotion(promotionId, lstId, false);
			if (lstTmp != null && lstTmp.size() > 0) {
				String msg = "";
				for (PromotionShopVO vo : lstTmp) {
					msg += (", " + vo.getShopCode());
				}
				msg = msg.replaceFirst(", ", "");
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.shop.map.exists", msg));
				return JSON;
			}
			lstTmp = null;
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}
			Map<Long, Integer> mapQuantity = new HashMap<Long, Integer>();
			Map<Long, Integer> mapQuantityMax = new HashMap<Long, Integer>();
			Map<Long, Integer> mapAmountMax = new HashMap<Long, Integer>();
			if (!StringUtil.isNullOrEmpty(quantityObjectStr)) {
				for (String rowQ: quantityObjectStr.split(",")) {
					String [] arrValQ = rowQ.split(";");
					if (arrValQ == null || arrValQ.length != 2 || !ValidateUtil.validateNumber(arrValQ[0]) || !ValidateUtil.validateNumber(arrValQ[1])) {
						result.put("errMsg", R.getResource("common.error.param.sha.or.md5.client.to.server"));
						return JSON;
					}
					mapQuantity.put(Long.valueOf(arrValQ[0]), Integer.valueOf(arrValQ[1]));
				}
			}
			if (!StringUtil.isNullOrEmpty(quantityMaxObjectStr)) {
				for (String rowQ: quantityMaxObjectStr.split(",")) {
					String [] arrValQ = rowQ.split(";");
					if (arrValQ == null || arrValQ.length != 2 || !ValidateUtil.validateNumber(arrValQ[0]) || !ValidateUtil.validateNumber(arrValQ[1])) {
						result.put("errMsg", R.getResource("common.error.param.sha.or.md5.client.to.server"));
						return JSON;
					}
					mapQuantityMax.put(Long.valueOf(arrValQ[0]), Integer.valueOf(arrValQ[1]));
				}
			}
			if (!StringUtil.isNullOrEmpty(amountMaxObjectStr)) {
				for (String rowQ: amountMaxObjectStr.split(",")) {
					String [] arrValQ = rowQ.split(";");
					if (arrValQ == null || arrValQ.length != 2 || !ValidateUtil.validateNumber(arrValQ[0]) || !ValidateUtil.validateNumber(arrValQ[1])) {
						result.put("errMsg", R.getResource("common.error.param.sha.or.md5.client.to.server"));
						return JSON;
					}
					mapAmountMax.put(Long.valueOf(arrValQ[0]), Integer.valueOf(arrValQ[1]));
				}
			}
			List<PromotionShopMap> lst = new ArrayList<PromotionShopMap>();
			PromotionShopMap psm = null;
			Shop shT = null;
			Date now = commonMgr.getSysDate();
			Long idt = null;
			for (int i = 0, sz = lstId.size(); i < sz; i++) {
				idt = lstId.get(i);
				shT = shopMgr.getShopById(idt);
				if (shT == null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "DV"));
					return JSON;
				}
				if (!ActiveType.RUNNING.equals(shT.getStatus())) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, shT.getShopCode()));
					return JSON;
				}
				psm = new PromotionShopMap();
				psm.setPromotionProgram(pro);
				psm.setShop(shT);
				psm.setQuantityMax(mapQuantity.get(idt));
				if (mapQuantityMax.get(idt) != null) {
					psm.setNumMax(BigDecimal.valueOf(mapQuantityMax.get(idt)));
				}
				if (mapAmountMax.get(idt) != null) {
					psm.setAmountMax(BigDecimal.valueOf(mapAmountMax.get(idt)));
				}
				psm.setStatus(ActiveType.RUNNING);
				psm.setCreateDate(now);
				psm.setCreateUser(staff.getStaffCode());
				psm.setFromDate(pro.getFromDate());
				psm.setToDate(pro.getToDate());

				lst.add(psm);
			}

			promotionProgramMgr.createListPromotionShopMap(lst, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.addPromotionShop", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Cap nhat so suat don vi
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 * 
	 * @modify hunglm16
	 * @since 19/11/2015
	 */
	public String updateShopQuantity() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || shopId == null || shopId < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			if (getMapShopChild().get(shopId) == null && !currentUser.getShopRoot().getShopId().equals(shopId)) {
				result.put("errMsg", R.getResource("common.cms.shop.undefined"));
				return JSON;
			}
			Shop shopTmp = commonMgr.getEntityById(Shop.class, shopId);
			if (shopTmp == null || shopTmp.getType() == null || !ShopSpecificType.NPP.equals(shopTmp.getType().getSpecificType())) {
				result.put("errMsg", R.getResource("common.cms.shop.undefined"));
				return JSON;
			}
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}
			PromotionShopMap psm = promotionProgramMgr.getPromotionShopMap(shopId, promotionId);
			if (psm == null || !ActiveType.RUNNING.equals(psm.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.shop.not.exist"));
				return JSON;
			}
			if ((quantity != null && quantity.intValue() <= 0) || (quantity != null && psm.getQuantityReceivedTotal() != null && quantity.intValue() < psm.getQuantityReceivedTotal().intValue())) {
				//So suat phai la so nguyen lon hon 0 & phai lon hon hoac bang so suat da khuyen mai
				result.put("errMsg", R.getResource("catalog.promotion.customer.map.quantity.error"));
				return JSON;
			} else {
				psm.setQuantityMax(quantity);
			}
			if ((quantityMax != null && quantityMax.intValue() <= 0) || (quantityMax != null && psm.getNumReceivedTotal() != null && quantityMax.intValue() < psm.getNumReceivedTotal().intValue())) {
				//So luong phai la so nguyen lon hon 0 & phai lon hon hoac bang So luong da khuyen mai
				result.put("errMsg", R.getResource("catalog.promotion.customer.map.quantity.max.error"));
				return JSON;
			} else {
				if(quantityMax == null) {
					psm.setNumMax(null);
				} else {
					psm.setNumMax(BigDecimal.valueOf(quantityMax));
				}
			}
			if ((amountMax != null && amountMax.intValue() <= 0) || (amountMax != null && psm.getAmountReceivedTotal() != null && amountMax.intValue() < psm.getAmountReceivedTotal().intValue())) {
				//So tien phai la so nguyen lon hon 0 & phai lon hon hoac bang So tien da khuyen mai
				result.put("errMsg", R.getResource("catalog.promotion.customer.map.amount.max.error"));
				return JSON;
			} else {
				if(amountMax == null) {
					psm.setAmountMax(null);
				} else {
					psm.setAmountMax(BigDecimal.valueOf(amountMax));
				}
			}
			promotionProgramMgr.updatePromotionShopMap(psm, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.updateShopQuantity", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xoa don vi
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String deleteShop() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || shopId == null || shopId < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}
			Shop sh = shopMgr.getShopById(shopId);
			if (sh == null) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.focus.program.shop.code"));
				return JSON;
			}
			promotionProgramMgr.deletePromotionShopMap(promotionId, shopId, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.deleteShop", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Tim kiem don vi them vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 14, 2014
	 * 
	 * @modify hunglm16
	 * @since 26/11/2015
	 * @description Bo sung phan quyen don vi
	 */
	public String searchShopOnDlg() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			if (id != null) {
				shopId = id;
			}
			Shop sh = null;
			if (shopId == null || shopId == 0) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			sh = shopMgr.getShopById(shopId);
			List<PromotionShopVO> lst = promotionProgramMgr.searchShopForPromotionProgram(promotionId, shopId, code, name, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
			lstTree = new ArrayList<TreeGridNode<PromotionShopVO>>();

			if (id == null) {
				PromotionShopVO vo = null;
				int i = 0;
				String state = null;
				if (StringUtil.isNullOrEmpty(code) && StringUtil.isNullOrEmpty(name)) {
					vo = new PromotionShopVO();
					vo.setId(sh.getId());
					vo.setShopCode(sh.getShopCode());
					vo.setShopName(sh.getShopName());
					/** author update nhutnn, since 23/04/2015 */
					PromotionShopMapFilter filter = new PromotionShopMapFilter();
					filter.setPromotionId(promotionId);
					filter.setShopRootId(shopId);
					filter.setStatus(ActiveType.RUNNING);
					List<PromotionShopMap> promotionShopMaps = promotionProgramMgr.getPromotionChildrenShopMap(filter);
					if (promotionShopMaps.isEmpty()) {
						vo.setIsExists(0);
					} else {
						vo.setIsExists(1);
					}
					if (sh.getType() != null && ShopSpecificType.NPP.equals(sh.getType().getSpecificType())) {
						vo.setIsNPP(NPP);
					} else {
						vo.setIsNPP(NOT_NPP);
					}

					state = ConstantManager.JSTREE_STATE_CLOSE;
					i = 0;
				} else {
					vo = lst.get(0);
					state = ConstantManager.JSTREE_STATE_OPEN;
					i = 1;
				}

				TreeGridNode<PromotionShopVO> node = new TreeGridNode<PromotionShopVO>();
				node.setNodeId(vo.getId().toString());
				node.setAttr(vo);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setText(vo.getShopCode() + " - " + vo.getShopName());
				List<TreeGridNode<PromotionShopVO>> chidren = new ArrayList<TreeGridNode<PromotionShopVO>>();
				node.setChildren(chidren);
				lstTree.add(node);

				if (lst == null || lst.size() == 0) {
					vo.setIsExists(0);
					return JSON;
				}

				// Tao cay			
				TreeGridNode<PromotionShopVO> tmp = null;
				TreeGridNode<PromotionShopVO> tmp2 = null;
				int sz = lst.size();
				for (; i < sz; i++) {
					vo = lst.get(i);

					tmp2 = getNodeFromTree(lstTree, vo.getParentId().toString());
					if (tmp2 != null) {
						tmp = new TreeGridNode<PromotionShopVO>();
						tmp.setNodeId(vo.getId().toString());
						tmp.setAttr(vo);
						if (0 == vo.getIsNPP()) {
							tmp.setState(state);
						} else {
							tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
						}
						tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

						if (tmp2.getChildren() == null) {
							tmp2.setChildren(new ArrayList<TreeGridNode<PromotionShopVO>>());
						}
						tmp2.getChildren().add(tmp);
					}
				}
			} else {
				// Tao cay			
				TreeGridNode<PromotionShopVO> tmp = null;
				PromotionShopVO vo = null;
				for (int i = 0, sz = lst.size(); i < sz; i++) {
					vo = lst.get(i);

					tmp = new TreeGridNode<PromotionShopVO>();
					tmp.setNodeId(vo.getId().toString());
					tmp.setAttr(vo);
					if (0 == vo.getIsNPP()) {
						tmp.setState(ConstantManager.JSTREE_STATE_CLOSE);
					} else {
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

					lstTree.add(tmp);
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.searchShopOnDlg", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Tim kiem khach hang thuoc CTMK
	 * 
	 * @author lacnv1
	 * @since Aug 15, 2014
	 * 
	 * @modify hunglm16
	 * @since 16/11/2015
	 */
	public String searchCustomerOfPromotion() throws Exception {
		result.put("rows", new ArrayList<PromotionCustomerVO>());
		result.put("total", 0);
		if (promotionId == null || promotionId <= 0) {
			return JSON;
		}
		try {
			result.put("page", page);
			result.put("max", max);
			KPaging<PromotionCustomerVO> paging = new KPaging<PromotionCustomerVO>();
			paging.setPage(page - 1);
			paging.setPageSize(max);
			PromotionCustomerFilter filter = new PromotionCustomerFilter();
			filter.setkPaging(paging);
			filter.setPromotionId(promotionId);
			filter.setCode(code);
			filter.setName(name);
			filter.setAddress(address);
			filter.setIsCustomerOnly(true);
			if (shopId == null || shopId <= 0l) {
				filter.setStrListShopId(getStrListShopId());
			} else {
				filter.setShopId(shopId);
			}
			ObjectVO<PromotionCustomerVO> obj = promotionProgramMgr.getCustomerInPromotionProgram(filter);
			if (obj == null) {
				result.put("rows", new ArrayList<PromotionCustomerVO>());
				result.put("total", 0);
				return JSON;
			}

			result.put("rows", obj.getLstObject());
			result.put("total", obj.getkPaging().getTotalRows());
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.searchCustomerOfPromotion", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Tim kiem KH them vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 18, 2014
	 */
	public String searchCustomerOnDlg() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			result.put("rows", new ArrayList<PromotionCustomerVO>());
			result.put("total", 0);
			return JSON;
		}
		try {
			KPaging<PromotionCustomerVO> paging = new KPaging<PromotionCustomerVO>();
			paging.setPage(page - 1);
			paging.setPageSize(max);
			PromotionCustomerFilter filter = new PromotionCustomerFilter();
			filter.setkPaging(paging);
			filter.setPromotionId(promotionId);
			filter.setCode(code);
			filter.setName(name);
			filter.setAddress(address);
			filter.setShopId(shopId);
			ObjectVO<PromotionCustomerVO> obj = promotionProgramMgr.searchCustomerForPromotionProgram(filter);
			if (obj == null) {
				result.put("rows", new ArrayList<PromotionCustomerVO>());
				result.put("total", 0);
				return JSON;
			}

			result.put("rows", obj.getLstObject());
			result.put("total", obj.getkPaging().getTotalRows());
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.searchCustomerOnDlg", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Them KH vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String addPromotionCustomer() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || shopId == null || shopId < 1 || lstId == null || lstId.size() == 0) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			List<PromotionCustomerVO> lstTmp = promotionProgramMgr.getListCustomerInPromotion(promotionId, lstId);
			if (lstTmp != null && lstTmp.size() > 0) {
				String msg = "";
				for (PromotionCustomerVO vo : lstTmp) {
					msg += (", " + vo.getCustomerCode());
				}
				msg = msg.replaceFirst(", ", "");
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.customer.map.exists", msg));
				return JSON;
			}
			lstTmp = null;
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}
			PromotionShopMap psm = promotionProgramMgr.getPromotionShopMap(shopId, promotionId);
			if (psm == null || !ActiveType.RUNNING.equals(psm.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.shop.not.exist"));
				return JSON;
			}

			List<PromotionCustomerMap> lst = new ArrayList<PromotionCustomerMap>();
			PromotionCustomerMap pcm = null;
			Customer cust = null;
			Date now = DateUtil.now();
			for (Long idt : lstId) {
				cust = customerMgr.getCustomerById(idt);
				if (cust == null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "KH"));
					return JSON;
				}
				if (!ActiveType.RUNNING.equals(cust.getStatus())) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, cust.getShortCode()));
					return JSON;
				}
				pcm = new PromotionCustomerMap();
				pcm.setPromotionShopMap(psm);
				pcm.setShop(psm.getShop());
				pcm.setCustomer(cust);
				if (quantity != null && quantity.intValue() > 0) {
					pcm.setQuantityMax(quantity);
				}
				if (quantityMax != null && quantityMax.intValue() > 0) {
					pcm.setNumMax(BigDecimal.valueOf(quantityMax));					
				}
				if (amountMax != null && amountMax.intValue() > 0) {
					pcm.setAmountMax(BigDecimal.valueOf(amountMax));
				}
				pcm.setStatus(ActiveType.RUNNING);
				pcm.setCreateDate(now);
				pcm.setCreateUser(staff.getStaffCode());

				lst.add(pcm);
			}
			promotionProgramMgr.createListPromotionCustomerMap(psm, lst, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.addPromotionCustomer", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Cap nhat so suat KH
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 * 
	 * @author hunglm16
	 * @since 16/11/2015
	 * @description bo sung Max So Luong, Max So Tien
	 */
	public String updateCustomerQuantity() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || id == null || id < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}
			PromotionCustomerMap pcm = promotionProgramMgr.getPromotionCustomerMapById(id);
			if (pcm == null || !ActiveType.RUNNING.equals(pcm.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.customer.map.not.exists"));
				return JSON;
			}
			if ((quantity != null && quantity.intValue() <= 0) || (quantity != null && pcm.getQuantityReceived() != null && quantity.intValue() < pcm.getQuantityReceived().intValue())) {
				//So suat phai la so nguyen lon hon 0 & phai lon hon hoac bang so suat da khuyen mai
				result.put("errMsg", R.getResource("catalog.promotion.customer.map.quantity.error"));
				return JSON;
			} else {
				pcm.setQuantityMax(quantity);
			}
			if ((quantityMax != null && quantityMax.intValue() <= 0) || (quantityMax != null && pcm.getNumReceived() != null && quantityMax.intValue() < pcm.getNumReceived().intValue())) {
				//So luong phai la so nguyen lon hon 0 & phai lon hon hoac bang So luong da khuyen mai
				result.put("errMsg", R.getResource("catalog.promotion.customer.map.quantity.max.error"));
				return JSON;
			} else {
				if(quantityMax == null) {
					pcm.setNumMax(null);
				} else {
					pcm.setNumMax(BigDecimal.valueOf(quantityMax));
				}
			}
			if ((amountMax != null && amountMax.intValue() <= 0) || (amountMax != null && pcm.getAmountReceived() != null && amountMax.intValue() < pcm.getAmountReceived().intValue())) {
				//So tien phai la so nguyen lon hon 0 & phai lon hon hoac bang So tien da khuyen mai
				result.put("errMsg", R.getResource("catalog.promotion.customer.map.amount.max.error"));
				return JSON;
			} else {
				if(amountMax == null) {
					pcm.setAmountMax(null);
				} else {
					pcm.setAmountMax(BigDecimal.valueOf(amountMax));
				}
			}
			promotionProgramMgr.updatePromotionCustomerMap(pcm, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.updateCustomerQuantity", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xoa KH
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String deleteCustomer() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || id == null || id < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}

			PromotionCustomerMap pcm = promotionProgramMgr.getPromotionCustomerMapById(id);
			if (pcm == null || !ActiveType.RUNNING.equals(pcm.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.customer.map.not.exists"));
				return JSON;
			}
			pcm.setStatus(ActiveType.DELETED);
			promotionProgramMgr.updatePromotionCustomerMap(pcm, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.deleteCustomer", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xuat excel ds don vi thuoc CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 20, 2014
	 */
	public String exportPromotionShop() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			result.put(ERROR, false);
			result.put("hasData", false);
			return JSON;
		}
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			result.put(ERROR, true);
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			PromotionShopMapFilter filter = new PromotionShopMapFilter();
			filter.setProgramId(promotionId);
			filter.setStatus(ActiveType.RUNNING);
			filter.setParentShopId(currentUser.getShopRoot().getShopId());
			filter.setShopCode(code);
			filter.setShopName(name);
			filter.setQuantityMax(quantity);
			List<PromotionShopMapVO> lst = promotionProgramMgr.getListPromotionShopMapVO(filter, null);
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
			StringBuilder sb = new StringBuilder(folder).append(ConstantManager.TEMPLATE_PROMOTION_SHOP_MAP_EXPORT).append(FileExtension.XLS.getValue());
			String templateFileName = sb.toString();
			templateFileName = templateFileName.replace('/', File.separatorChar);

			sb = new StringBuilder(ConstantManager.TEMPLATE_PROMOTION_SHOP_MAP_EXPORT).append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append(FileExtension.XLS.getValue());
			String outputName = sb.toString();
			sb = null;
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			Map<String, Object> beans = new HashMap<String, Object>();

			beans.put("hasData", 1);
			beans.put("lstShop", lst);

			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.exportPromotionShop", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);				
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Import don vi tu excel
	 * 
	 * @author lacnv1
	 * @since Aug 10, 2014
	 * 
	 * Them max so luong, max so tien
	 * @modify hunglm16
	 * @since 17/11/2015
	 */
	public String importPromotionShop() throws Exception {
		resetToken(result);
		isError = true;
		try {
			PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramById(id);
			if (promotionProgram == null) {
				isError = true;
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM");
				return SUCCESS;
			}
			if (!ActiveType.WAITING.equals(promotionProgram.getStatus())) {
				isError = true;
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect");
				return SUCCESS;
			}
			int maxlengthByNum = 9;
			int maxlengByColumn = 8;
			totalItem = 0;
			String message = "";
			lstView = new ArrayList<CellBean>();
			typeView = true;
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, maxlengByColumn);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				Date curentSys = commonMgr.getSysDate();
				PromotionShopMap promotionShopMap = null;
				PromotionCustomerMap promotionCustomerMap = null;
				List<String> row = null;
				Shop shop = null;
				String msg = "";
				boolean isEdit;
				boolean isCreateCustomer;
				PromotionShopMap psm = null;
				Customer c = null;
				LogInfoVO logInfo = getLogInfoVO();
				String value = null;
				for (int i = 0; i < lstData.size(); i++) {
					boolean flagContinue = true;
					for (int j = 0; j < maxlengByColumn; j++) {
						String cellTmp = lstData.get(i).get(j);
						if (!StringUtil.isNullOrEmpty(cellTmp)) {
							flagContinue = false;
							break;
						}
					}
					if (flagContinue) {
						//Bo qua dong du lieu trong
						continue;
					}
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						message = "";
						totalItem++;
						row = lstData.get(i);
						isEdit = false;
						isCreateCustomer = false;
						value = null;
						//Ma DV
						promotionShopMap = new PromotionShopMap();
						promotionShopMap.setPromotionProgram(promotionProgram);
						promotionShopMap.setFromDate(promotionProgram.getFromDate());
						promotionShopMap.setToDate(promotionProgram.getToDate());
						value = row.get(0);
						if (!StringUtil.isNullOrEmpty(value)) {
							shop = shopMgr.getShopByCode(value);
							if (shop == null) {
								message += R.getResource("promotion.support.import.shop.undefiend") + " \n";
							} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
								message += R.getResource("promotion.support.import.shop.is.running.eror") + " \n";
							} else if (getMapShopChild().get(shop.getId()) == null && !currentUser.getShopRoot().getShopId().equals(shop.getId())) {
								message += R.getResource("common.cms.shop.undefined.imp") + " \n";
							}
						} else {
							message += R.getResource("promotion.support.import.shop.null") + " \n";
						}
						if (StringUtil.isNullOrEmpty(message)) {
							boolean flag1 = promotionProgramMgr.isExistChildJoinProgram(shop.getShopCode(), promotionId);
							if (flag1) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.support.import.shop.children.in.promotion.shopmap") + " \n";
							}
						}
						if (shop != null) {
							psm = promotionProgramMgr.getPromotionShopMap(shop.getId(), promotionProgram.getId());
						}
						if (StringUtil.isNullOrEmpty(message)) {
							promotionShopMap.setShop(shop);
							if (psm != null) {
								isEdit = true;
								promotionShopMap = psm;
							}
						}
						//Kiem tra don vi co phai la NPP
						boolean isCheckNPP = true;
						if (shop == null || !ShopSpecificType.NPP.equals(shop.getType().getSpecificType())) {
							isCheckNPP = false;
						}
						//So suat DV
						msg = "";
						value = row.get(1);
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "promotion.support.import.shop.so.suat", maxlengthByNum, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								if (!ValidateUtil.validateNumber(value) || Integer.valueOf(value) <= 0) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("promotion.support.import.shop.so.suat"));
								} else if (!isCheckNPP) {
									msg = R.getResource("promotion.support.import.input.shop.islevel5", R.getResource("promotion.support.import.shop.so.suat").toLowerCase()) + " \n";
								} else if (psm != null && psm.getQuantityReceived() != null && psm.getQuantityReceived() > Integer.valueOf(value)) {
									msg = R.getResource("promotion.support.import.shop.num.less.than.quantityM") + " \n";
								} else {
									promotionShopMap.setQuantityMax(Integer.valueOf(value));
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						//So luong DV
						value = row.get(2);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "promotion.support.import.shop.so.luong", maxlengthByNum, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								if (!ValidateUtil.validateNumber(value) || Integer.valueOf(value) <= 0) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("promotion.support.import.shop.so.luong"));
								} else if (!isCheckNPP) {
									msg = R.getResource("promotion.support.import.input.shop.islevel5", R.getResource("promotion.support.import.shop.so.luong").toLowerCase()) + " \n";
								} else if (psm != null && psm.getNumReceived() != null && psm.getNumReceived().intValue() > Integer.valueOf(value).intValue()) {
									msg = R.getResource("promotion.support.import.shop.num.less.than.numM") + " \n";
								} else {
									promotionShopMap.setNumMax(BigDecimal.valueOf(Integer.valueOf(value)));
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						//So tien DV
						value = row.get(3);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "promotion.support.import.shop.so.tien", maxlengthByNum, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								if (!ValidateUtil.validateNumber(value) || Integer.valueOf(value) <= 0) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("promotion.support.import.shop.so.tien")) + " \n";
								} else if (!isCheckNPP) {
									msg = R.getResource("promotion.support.import.input.shop.islevel5", R.getResource("promotion.support.import.shop.so.tien").toLowerCase()) + " \n";
								} else if (psm != null && psm.getAmountReceived() != null && psm.getAmountReceived().intValue() > Integer.valueOf(value).intValue()) {
									msg = R.getResource("promotion.support.import.shop.num.less.than.amountM") + " \n";
								} else {
									promotionShopMap.setAmountMax(BigDecimal.valueOf(Double.valueOf(value)));
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						//Ma KH
						value = row.get(4);
						if (!StringUtil.isNullOrEmpty(value) && shop != null) {
							c = customerMgr.getCustomerByCode(value + "_" + shop.getShopCode());
							if (psm != null && c != null) {
								promotionCustomerMap = promotionProgramMgr.getPromotionCustomerMap(psm.getId(), c.getId());
								if (promotionCustomerMap == null) {
									isCreateCustomer = true;
									promotionCustomerMap = new PromotionCustomerMap();
								}
							}
							if (c == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.program.customer.not.exists") + " \n";
							} else if (c.getShop() != null && !c.getShop().getId().equals(shop.getId())) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.program.customer.not.in.shop") + " \n";
							} else if (!ActiveType.RUNNING.equals(c.getStatus())) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "sp.create.order.customer.is.empty");
							} else {
								if (promotionCustomerMap == null) {
									promotionCustomerMap = new PromotionCustomerMap();
								}
								promotionCustomerMap.setPromotionShopMap(promotionShopMap);
								promotionCustomerMap.setCustomer(c);
							}
						}
						// So suat KH
						value = row.get(5);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "promotion.support.import.cus.so.suat", maxlengthByNum, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								if (!ValidateUtil.validateNumber(value) || Integer.valueOf(value) <= 0) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("promotion.support.import.cus.so.suat")) + " \n";
								} else if (!isCheckNPP) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("promotion.support.import.cus.so.suat").toLowerCase()) + " \n";
								} else if (promotionCustomerMap != null && promotionCustomerMap.getQuantityReceived() != null && promotionCustomerMap.getQuantityReceived() > Integer.valueOf(value)) {
									msg = R.getResource("promotion.support.import.cus.num.less.than.quantityM") + " \n";
								} else {
									if (promotionCustomerMap == null) {
										promotionCustomerMap = new PromotionCustomerMap();
									}
									promotionCustomerMap.setQuantityMax(Integer.valueOf(value));
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						//So luong KH
						value = row.get(6);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "promotion.support.import.cus.so.luong", maxlengthByNum, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								if (!ValidateUtil.validateNumber(value) || Integer.valueOf(value) <= 0) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("promotion.support.import.cus.so.luong")) + " \n";
								} else if (!isCheckNPP) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("promotion.support.import.cus.so.luong")) + " \n";
								} else if (promotionCustomerMap != null && promotionCustomerMap.getNumReceived() != null && promotionCustomerMap.getNumReceived().intValue() > Integer.valueOf(value)) {
									msg = R.getResource("promotion.support.import.cus.num.less.than.numM") + " \n";
								} else {
									if (promotionCustomerMap == null) {
										promotionCustomerMap = new PromotionCustomerMap();
									}
									promotionCustomerMap.setNumMax(BigDecimal.valueOf(Integer.valueOf(value)));
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						//So tien KH
						value = row.get(7);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "common.num.amount", maxlengthByNum, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								if (!ValidateUtil.validateNumber(value) || Integer.valueOf(value) <= 0) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("promotion.support.import.cus.so.tien")) + " \n";
								} else if (!isCheckNPP) {
									msg = R.getResource("promotion.support.import.num.pramater", R.getResource("common.num.amount")) + " \n";
								} else if (promotionCustomerMap != null && promotionCustomerMap.getAmountReceived() != null && promotionCustomerMap.getAmountReceived().intValue() > Integer.valueOf(value)) {
									msg = R.getResource("promotion.support.import.cus.num.less.than.amountM") + " \n";
								} else {
									if (promotionCustomerMap == null) {
										promotionCustomerMap = new PromotionCustomerMap();
									}
									promotionCustomerMap.setAmountMax(BigDecimal.valueOf(Double.valueOf(value)));
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						if (StringUtil.isNullOrEmpty(message)) {
							try {
								if (isEdit) {
									promotionShopMap.setUpdateDate(curentSys);
									promotionShopMap.setCreateUser(currentUser.getUserName());
									promotionProgramMgr.updatePromotionShopMap(promotionShopMap, logInfo);
									if (promotionCustomerMap != null && promotionCustomerMap.getCustomer() != null) {
										promotionCustomerMap.setShop(promotionShopMap.getShop());
										if (isCreateCustomer) {
											promotionCustomerMap.setCreateDate(curentSys);
											promotionCustomerMap.setCreateUser(currentUser.getUserName());
											promotionProgramMgr.createPromotionCustomerMap(promotionCustomerMap, logInfo);
										} else {
											promotionCustomerMap.setUpdateDate(curentSys);
											promotionCustomerMap.setUpdateUser(currentUser.getUserName());
											promotionProgramMgr.updatePromotionCustomerMap(promotionCustomerMap, logInfo);
										}
									}
								} else {
									if (ActiveType.WAITING.equals(promotionProgram.getStatus())) {
										promotionShopMap.setCreateDate(curentSys);
										promotionShopMap.setCreateUser(currentUser.getUserName());
										PromotionShopMap s = promotionProgramMgr.createPromotionShopMap(promotionShopMap, logInfo);
										if (promotionCustomerMap != null && promotionCustomerMap.getCustomer() != null) {
											promotionCustomerMap.setPromotionShopMap(s);
											promotionCustomerMap.setCreateDate(curentSys);
											promotionCustomerMap.setCreateUser(currentUser.getUserName());
											promotionCustomerMap.setShop(promotionShopMap.getShop());
											promotionProgramMgr.createPromotionCustomerMap(promotionCustomerMap, logInfo);
										}
									}
								}
								isError = false;
							} catch (BusinessException be) {
								LogUtility.logErrorStandard(be, "ths.dms.web.action.program.PromotionSupportCatalogAction.importPromotionShop", createLogErrorStandard(actionStartTime));
								errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
						typeView = false;
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_PP_MANUAL_SHOP_FAIL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				isError = false;
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.importPromotionShop", createLogErrorStandard(actionStartTime));
			isError = true;
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}

	/**
	 * Xem thong thuoc tinh KH CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 16, 2014
	 */
	public String viewCustomerAttribute() throws Exception {
		if (promotionId != null && promotionId != 0) {
			try {
				lstPromotionCustAttrVO = promotionProgramMgr.getListPromotionCustAttrVOCanBeSet(null, promotionId);
				//listPromotionCustAttrVOAlreadySet = promotionProgramMgr.getListPromotionCustAttrVOAlreadySet(null, promotionId);
			} catch (Exception ex) {
				LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.viewCustomerAttribute", createLogErrorStandard(actionStartTime));
				isError = true;
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		return SUCCESS;
	}

	/**
	 * Lay danh sach thuoc tinh da co trong CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 22, 2014
	 */
	public String loadAppliedAttributes() throws Exception {
		if (promotionId != null && promotionId > 0) {
			try {
				CustomerAttributeMgr customerAttributeMgr = (CustomerAttributeMgr) context.getBean("customerAttributeMgr");
				List<PromotionCustAttrVO> lst = promotionProgramMgr.getListPromotionCustAttrVOAlreadySet(null, promotionId);
				CustomerAttribute attribute = null;
				AttributeColumnType attColType = null;
				PromotionCustAttVO2 voTmp = null;
				List<PromotionCustAttVO2> lstTmp = promotionProgramMgr.getListPromotionCustAttVOValue(promotionId);
				List<PromotionCustAttVO2> lstTmpDetail = promotionProgramMgr.getListPromotionCustAttVOValueDetail(promotionId);
				ArrayList<Object> lstData = null;
				List<AttributeDetailVO> lstAttDetail = null;
				AttributeDetailVO voDetail = null;
				int j, sz;
				int k, szk;
				if (lst != null && lst.size() > 0) {
					for (PromotionCustAttrVO vo : lst) {
						if (vo.getObjectType() == AUTO_ATTRIBUTE) {
							attribute = customerAttributeMgr.getCustomerAttributeById(vo.getObjectId());
							attColType = attribute.getValueType();
							vo.setValueType(attColType);//setValueType
							if (attColType == AttributeColumnType.CHARACTER || attColType == AttributeColumnType.NUMBER || attColType == AttributeColumnType.DATE_TIME) {
								//Value cua thuoc tinh dong type (1,2,3)
								if (lstTmp != null && lstTmp.size() > 0) {
									if (attColType == AttributeColumnType.CHARACTER) {
										for (j = 0, sz = lstTmp.size(); j < sz; j++) {
											voTmp = lstTmp.get(j);
											if (voTmp.getAttributeId().equals(vo.getObjectId())) {
												lstData = new ArrayList<Object>();
												lstData.add(voTmp.getFromValue());
												vo.setListData(lstData);//setListData
											}
										}
									} else if (attColType == AttributeColumnType.NUMBER) {
										//										valueType=2;
										for (j = 0, sz = lstTmp.size(); j < sz; j++) {
											voTmp = lstTmp.get(j);
											if (voTmp.getAttributeId().equals(vo.getObjectId())) {
												lstData = new ArrayList<Object>();
												lstData.add(voTmp.getFromValue());
												lstData.add(voTmp.getToValue());
												vo.setListData(lstData);//setListData
											}
										}
									} else if (attColType == AttributeColumnType.DATE_TIME) {
										//										valueType=3;
										for (j = 0, sz = lstTmp.size(); j < sz; j++) {
											voTmp = lstTmp.get(j);
											if (voTmp.getAttributeId().equals(vo.getObjectId())) {
												lstData = new ArrayList<Object>();
												lstData.add(DateUtil.convertFormatStrFromAtt(voTmp.getFromValue()));
												lstData.add(DateUtil.convertFormatStrFromAtt(voTmp.getToValue()));
												vo.setListData(lstData);//setListData
											}
										}
									}
								}
							} else if (attColType == AttributeColumnType.CHOICE || attColType == AttributeColumnType.MULTI_CHOICE) {
								//Value cua thuoc tinh dong type (4,5)
								if (lstTmpDetail != null && lstTmpDetail.size() > 0) {
									lstAttDetail = null;
									for (j = 0, sz = lstTmpDetail.size(); j < sz; j++) {
										voTmp = lstTmpDetail.get(j);
										if (voTmp.getAttributeId().equals(attribute.getId())) {
											if (lstAttDetail == null) {
												lstAttDetail = promotionProgramMgr.getListPromotionCustAttVOCanBeSet(attribute.getId());
											}
											if (lstAttDetail != null && lstAttDetail.size() > 0) {
												for (k = 0, szk = lstAttDetail.size(); k < szk; k++) {
													voDetail = lstAttDetail.get(k);
													if (voDetail.getEnumId().equals(voTmp.getAttributeEnumId())) {
														voDetail.setChecked(true);
														break;
													}
												}
											}
										}
									}
									vo.setListData(lstAttDetail);//setListData
								}
							}
						} else if (vo.getObjectType() == CUSTOMER_TYPE) {
							List<ChannelTypeVO> listChannelTypeVO = promotionProgramMgr.getListChannelTypeVO();
							List<ChannelTypeVO> listSelectedChannelTypeVO = promotionProgramMgr.getListChannelTypeVOAlreadySet(null, promotionId);
							if (listChannelTypeVO != null && listChannelTypeVO.size() > 0 && listSelectedChannelTypeVO != null && listSelectedChannelTypeVO.size() > 0) {
								for (ChannelTypeVO channelTypeVO : listChannelTypeVO) {
									for (ChannelTypeVO channelTypeVO1 : listSelectedChannelTypeVO) {
										if (channelTypeVO1.getIdChannelType().equals(channelTypeVO.getIdChannelType())) {
											channelTypeVO.setChecked(true);
											break;
										}
									}
								}
							}
							vo.setListData(listChannelTypeVO);//setListData
						} else if (vo.getObjectType() == SALE_LEVEL) {
							List<SaleCatLevelVO> listSelectedSaleCatLevelVO = promotionProgramMgr.getListSaleCatLevelVOByIdProAlreadySetSO(null, promotionId);
							vo.setListData(listSelectedSaleCatLevelVO);
							List<ProductInfoVO> listProductInfoVO = promotionProgramMgr.getListProductInfoVO();
							if (listProductInfoVO != null && listProductInfoVO.size() > 0) {
								for (ProductInfoVO productInfoVO : listProductInfoVO) {
									List<SaleCatLevelVO> listSaleCatLevelVO = promotionProgramMgr.getListSaleCatLevelVOByIdPro(productInfoVO.getIdProductInfoVO());
									productInfoVO.setListSaleCatLevelVO(listSaleCatLevelVO);
								}
								vo.setListProductInfoVO(listProductInfoVO);
							} else {
								vo.setListProductInfoVO(new ArrayList<ProductInfoVO>());
							}
						}
					}

					result.put("list", lst);//Neu ko co list de put thi nho new 1 arrayList roi put a.
				}
			} catch (Exception ex) {
				LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.loadAppliedAttributes", createLogErrorStandard(actionStartTime));
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
				return JSON;
			}
		}
		return JSON;
	}

	/**
	 * Lay danh sach gia tri cho thuoc tinh KH
	 * 
	 * @author lacnv1
	 * @since Aug 22, 2014
	 */
	public String getAllDataForAttributes() throws Exception {
		try {
			if (lstObjectType != null && lstObjectType.size() > 0 && lstId != null && lstId.size() > 0 && lstObjectType.size() == lstId.size()) { //lstId = listObjectId
				CustomerAttributeMgr customerAttributeMgr = (CustomerAttributeMgr) context.getBean("customerAttributeMgr");
				List<PromotionCustAttrVO> lst = new ArrayList<PromotionCustAttrVO>();
				PromotionCustAttrVO vo = null;
				Integer objectType = null;
				Long objectId = null;
				CustomerAttribute attribute = null;
				AttributeColumnType attColType = null;
				List<AttributeDetailVO> lstAttDetail = null;
				for (int i = 0; i < lstObjectType.size(); i++) {
					objectType = lstObjectType.get(i);
					objectId = lstId.get(i);
					if (objectType != null) {
						vo = new PromotionCustAttrVO();
						vo.setObjectType(objectType);
						vo.setObjectId(objectId);
						if (objectType.equals(AUTO_ATTRIBUTE)) {
							attribute = customerAttributeMgr.getCustomerAttributeById(objectId);
							if (attribute != null) {
								attColType = attribute.getValueType();
								vo.setValueType(attColType);// setValueType
								vo.setName(attribute.getName());
								// chi set du lieu cho kieu dropdownlist:
								if (attColType == AttributeColumnType.CHOICE || attColType == AttributeColumnType.MULTI_CHOICE) {
									lstAttDetail = promotionProgramMgr.getListPromotionCustAttVOCanBeSet(attribute.getId());
									vo.setListData(lstAttDetail);// setListData
								}
							}
						} else if (objectType.equals(CUSTOMER_TYPE)) {
							List<ChannelTypeVO> listChannelTypeVO = null;
							listChannelTypeVO = promotionProgramMgr.getListChannelTypeVO();
							vo.setListData(listChannelTypeVO);//setListData
						} else if (objectType.equals(SALE_LEVEL)) {
							List<ProductInfoVO> listProductInfoVO = promotionProgramMgr.getListProductInfoVO();
							if (listProductInfoVO != null && listProductInfoVO.size() > 0) {
								for (ProductInfoVO productInfoVO : listProductInfoVO) {
									List<SaleCatLevelVO> listSaleCatLevelVO = promotionProgramMgr.getListSaleCatLevelVOByIdPro(productInfoVO.getIdProductInfoVO());
									productInfoVO.setListSaleCatLevelVO(listSaleCatLevelVO);
								}
								vo.setListProductInfoVO(listProductInfoVO);
							}
						}

						lst.add(vo);
					} else {
						return JSON;
					}
				}
				result.put("list", lst);
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.getAllDataForAttributes", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * Luu thuoc tinh KH tham gia CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 22, 2014
	 */
	public String savePromotionCustAtt() throws Exception {
		boolean error = true;
		List<PromotionCustAttUpdateVO> listPromotionCustAttUpdateVO = new ArrayList<PromotionCustAttUpdateVO>();
		try {
			PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (promotionProgram == null) {
				result.put(ERROR, error);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));//Loi ko dung id promotionProgram
				return JSON;
			}
			if (lstId != null && lstId.size() > 0 && lstAttDataInField != null && lstAttDataInField.size() > 0) {
				Long attributeI = null;
				PromotionCustAttr promotionCustAttr = null;
				PromotionCustAttr pca = null;
				CustomerAttribute attribute = null;
				CustomerAttributeMgr customerAttributeMgr = (CustomerAttributeMgr) context.getBean("customerAttributeMgr");
				String dataI = null;
				AttributeColumnType attColType = null;
				PromotionCustAttUpdateVO promotionCustAttUpdateVO = null;
				for (int i = 0, sz = lstId.size(); i < sz; i++) {
					if (lstId.get(i) != null && lstId.get(i).equals(-2l)) {//-2 la customerType
						if (lstCustomerType != null && lstCustomerType.size() > 0) {
							pca = promotionProgramMgr.getPromotionCustAttrByPromotion(promotionId, 2, null);
							if (pca != null) {
								promotionCustAttr = pca;
								promotionCustAttr.setUpdateUser(currentUser.getUserName());
							} else {
								promotionCustAttr = new PromotionCustAttr();
								promotionCustAttr.setCreateUser(currentUser.getUserName());
							}
							promotionCustAttr.setPromotionProgram(promotionProgram);
							promotionCustAttr.setObjectType(2);
							promotionCustAttr.setSeq(i);//set seq theo chi so trong List.
							List<PromotionCustAttrDetail> listPromotionCustAttrDetail = new ArrayList<PromotionCustAttrDetail>();
							if (pca != null && pca.getId() != null) {
								PromotionCustAttrDetail tmpPCAD;
								Long promotionCustAttrId = pca.getId();
								for (Long chanelTypeId : lstCustomerType) {
									PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
									promotionCustAttrDetail.setObjectType(2);
									promotionCustAttrDetail.setObjectId(chanelTypeId);
									tmpPCAD = promotionProgramMgr.getPromotionCustAttrDetail(promotionCustAttrId, 2L, chanelTypeId);
									if (tmpPCAD != null) {
										promotionCustAttrDetail.setCreateUser(tmpPCAD.getCreateUser());
										promotionCustAttrDetail.setUpdateUser(currentUser.getUserName());
									} else {
										promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
									}
									tmpPCAD = null;
									listPromotionCustAttrDetail.add(promotionCustAttrDetail);
								}
							} else {
								for (Long chanelTypeId : lstCustomerType) {
									PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
									promotionCustAttrDetail.setObjectType(2);
									promotionCustAttrDetail.setObjectId(chanelTypeId);
									promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
									listPromotionCustAttrDetail.add(promotionCustAttrDetail);
								}
							}
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(listPromotionCustAttrDetail);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						}
					} else if (lstId.get(i) != null && lstId.get(i).equals(-3l)) {//-3 la saleLevel
						if (lstSaleLevelCatId != null && lstSaleLevelCatId.size() > 0) {
							pca = promotionProgramMgr.getPromotionCustAttrByPromotion(promotionId, 3, null);
							if (pca != null) {
								promotionCustAttr = pca;
								promotionCustAttr.setUpdateUser(currentUser.getUserName());
							} else {
								promotionCustAttr = new PromotionCustAttr();
								promotionCustAttr.setCreateUser(currentUser.getUserName());
							}
							promotionCustAttr.setPromotionProgram(promotionProgram);
							promotionCustAttr.setObjectType(3);
							promotionCustAttr.setSeq(i);//set seq de load len cho dung thu tu
							List<PromotionCustAttrDetail> listPromotionCustAttrDetail = new ArrayList<PromotionCustAttrDetail>();
							if (pca != null && pca.getId() != null) {
								PromotionCustAttrDetail tmpPCAD;
								Long promotionCustAttrId = pca.getId();
								for (Long saleLevelCatId : lstSaleLevelCatId) {
									if (saleLevelCatId > -1L) {
										PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
										promotionCustAttrDetail.setObjectType(3);
										promotionCustAttrDetail.setObjectId(saleLevelCatId);
										tmpPCAD = promotionProgramMgr.getPromotionCustAttrDetail(promotionCustAttrId, 3L, saleLevelCatId);
										if (tmpPCAD != null) {
											promotionCustAttrDetail.setCreateUser(tmpPCAD.getCreateUser());
											promotionCustAttrDetail.setUpdateUser(currentUser.getUserName());
										} else {
											promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
										}
										tmpPCAD = null;
										listPromotionCustAttrDetail.add(promotionCustAttrDetail);
									}
								}
							} else {
								for (Long saleLevelCatId : lstSaleLevelCatId) {
									if (saleLevelCatId > -1L) {
										PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
										promotionCustAttrDetail.setObjectType(3);
										promotionCustAttrDetail.setObjectId(saleLevelCatId);
										promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
										listPromotionCustAttrDetail.add(promotionCustAttrDetail);
									}
								}
							}
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(listPromotionCustAttrDetail);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						}
					} else {
						attributeI = lstId.get(i);
						attribute = customerAttributeMgr.getCustomerAttributeById(attributeI);
						//set vao entity:
						pca = promotionProgramMgr.getPromotionCustAttrByPromotion(promotionId, 1, attributeI);
						if (pca != null) {
							promotionCustAttr = pca;
							promotionCustAttr.setUpdateUser(currentUser.getUserName());
						} else {
							promotionCustAttr = new PromotionCustAttr();
							promotionCustAttr.setCreateUser(currentUser.getUserName());
						}
						promotionCustAttr.setPromotionProgram(promotionProgram);
						promotionCustAttr.setObjectType(1);
						promotionCustAttr.setObjectId(attribute.getId());
						promotionCustAttr.setSeq(i);
						//
						dataI = lstAttDataInField.get(i);
						attColType = attribute.getValueType();
						if (attColType == AttributeColumnType.CHARACTER) {
							promotionCustAttr.setFromValue(dataI);
							// set vao VO to save:
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(null);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						} else if (attColType == AttributeColumnType.NUMBER) {
							String[] arr = dataI.split(",");
							if (arr != null) {
								if (arr.length == 2) {
									promotionCustAttr.setFromValue(arr[0]);
									promotionCustAttr.setToValue(arr[1]);
								} else if (arr.length == 1) {
									promotionCustAttr.setFromValue(arr[0]);
									promotionCustAttr.setToValue("");
								}
							}
							// set vao VO to save:
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(null);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);

						} else if (attColType == AttributeColumnType.DATE_TIME) {
							String[] arr = dataI.split(",");
							if (arr != null) {
								if (arr.length == 2) {
									String fromValueTmp = DateUtil.convertFormatAttFromStr(arr[0]);
									String toValueTmp = DateUtil.convertFormatAttFromStr(arr[1]);
									promotionCustAttr.setFromValue(fromValueTmp);
									promotionCustAttr.setToValue(toValueTmp);
								} else if (arr.length == 1) {
									String fromValueTmp = DateUtil.convertFormatAttFromStr(arr[0]);
									promotionCustAttr.setFromValue(fromValueTmp);
									promotionCustAttr.setToValue("");
								}
							}
							// set vao VO to save:
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(null);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						} else if (attColType == AttributeColumnType.CHOICE || attColType == AttributeColumnType.MULTI_CHOICE) {
							List<PromotionCustAttrDetail> listPromotionCustAttrDetail = new ArrayList<PromotionCustAttrDetail>();
							String[] arr = dataI.split(",");
							if (pca != null && pca.getId() != null) {
								PromotionCustAttrDetail tmpPCAD;
								Long promotionCustAttrId = pca.getId();
								if (arr != null && arr.length > 0) {
									for (int k = 0, szk = arr.length; k < szk; k++) {
										PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
										promotionCustAttrDetail.setObjectType(1);
										promotionCustAttrDetail.setObjectId(Long.valueOf(arr[k]));
										tmpPCAD = promotionProgramMgr.getPromotionCustAttrDetail(promotionCustAttrId, 1L, Long.valueOf(arr[k]));
										if (tmpPCAD != null) {
											promotionCustAttrDetail.setCreateUser(tmpPCAD.getCreateUser());
											promotionCustAttrDetail.setUpdateUser(currentUser.getUserName());
										} else {
											promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
										}
										tmpPCAD = null;
										listPromotionCustAttrDetail.add(promotionCustAttrDetail);
									}
								}
							} else {
								if (arr != null && arr.length > 0) {
									for (int k = 0, szk = arr.length; k < szk; k++) {
										PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
										promotionCustAttrDetail.setObjectType(1);
										promotionCustAttrDetail.setObjectId(Long.valueOf(arr[k]));
										promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
										listPromotionCustAttrDetail.add(promotionCustAttrDetail);
									}
								}
							}
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(listPromotionCustAttrDetail);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						}
					}
				}
			}

			promotionProgramMgr.createOrUpdatePromotionCustAttVO(promotionId, listPromotionCustAttUpdateVO, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.PromotionSupportCatalogAction.savePromotionCustAtt", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
		}
		return JSON;
	}

	public Boolean getIsVNMAdmin() {
		return isVNMAdmin;
	}

	public void setIsVNMAdmin(Boolean isVNMAdmin) {
		this.isVNMAdmin = isVNMAdmin;
	}

	public List<ApParam> getLstTypeCode() {
		return lstTypeCode;
	}

	public void setLstTypeCode(List<ApParam> lstTypeCode) {
		this.lstTypeCode = lstTypeCode;
	}

	public Integer getProType() {
		return proType;
	}

	public void setProType(Integer proType) {
		this.proType = proType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getLstTypeId() {
		return lstTypeId;
	}

	public void setLstTypeId(String lstTypeId) {
		this.lstTypeId = lstTypeId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	public PromotionProgram getPromotionProgram() {
		return promotionProgram;
	}

	public void setPromotionProgram(PromotionProgram promotionProgram) {
		this.promotionProgram = promotionProgram;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public List<Integer> getLstQtt() {
		return lstQtt;
	}

	public void setLstQtt(List<Integer> lstQtt) {
		this.lstQtt = lstQtt;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<TreeGridNode<PromotionShopVO>> getLstTree() {
		return lstTree;
	}

	public void setLstTree(List<TreeGridNode<PromotionShopVO>> lstTree) {
		this.lstTree = lstTree;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public List<PromotionCustAttrVO> getLstPromotionCustAttrVO() {
		return lstPromotionCustAttrVO;
	}

	public void setLstPromotionCustAttrVO(List<PromotionCustAttrVO> lstPromotionCustAttrVO) {
		this.lstPromotionCustAttrVO = lstPromotionCustAttrVO;
	}

	public List<Integer> getLstObjectType() {
		return lstObjectType;
	}

	public void setLstObjectType(List<Integer> lstObjectType) {
		this.lstObjectType = lstObjectType;
	}

	public List<String> getLstAttDataInField() {
		return lstAttDataInField;
	}

	public void setLstAttDataInField(List<String> lstAttDataInField) {
		this.lstAttDataInField = lstAttDataInField;
	}

	public List<Long> getLstCustomerType() {
		return lstCustomerType;
	}

	public void setLstCustomerType(List<Long> lstCustomerType) {
		this.lstCustomerType = lstCustomerType;
	}

	public List<Long> getLstSaleLevelCatId() {
		return lstSaleLevelCatId;
	}

	public void setLstSaleLevelCatId(List<Long> lstSaleLevelCatId) {
		this.lstSaleLevelCatId = lstSaleLevelCatId;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<TreeNode> getSearchUnitTree() {
		return searchUnitTree;
	}

	public void setSearchUnitTree(List<TreeNode> searchUnitTree) {
		this.searchUnitTree = searchUnitTree;
	}

	public Integer getQuantityMax() {
		return quantityMax;
	}

	public void setQuantityMax(Integer quantityMax) {
		this.quantityMax = quantityMax;
	}

	public Integer getAmountMax() {
		return amountMax;
	}

	public void setAmountMax(Integer amountMax) {
		this.amountMax = amountMax;
	}

	public String getQuantityObjectStr() {
		return quantityObjectStr;
	}

	public void setQuantityObjectStr(String quantityObjectStr) {
		this.quantityObjectStr = quantityObjectStr;
	}

	public String getQuantityMaxObjectStr() {
		return quantityMaxObjectStr;
	}

	public void setQuantityMaxObjectStr(String quantityMaxObjectStr) {
		this.quantityMaxObjectStr = quantityMaxObjectStr;
	}

	public String getAmountMaxObjectStr() {
		return amountMaxObjectStr;
	}

	public void setAmountMaxObjectStr(String amountMaxObjectStr) {
		this.amountMaxObjectStr = amountMaxObjectStr;
	}
	
}

package ths.dms.web.action.program;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.TreeNode;
import ths.dms.web.business.common.TreeUtility;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.Unicode2English;
import ths.dms.web.utils.ValidateUtil;

import org.apache.struts2.ServletActionContext;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.CycleMgr;
import ths.dms.core.business.KeyShopMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.KS;
import ths.dms.core.entities.KSCusProductReward;
import ths.dms.core.entities.KSCustomer;
import ths.dms.core.entities.KSCustomerLevel;
import ths.dms.core.entities.KSLevel;
import ths.dms.core.entities.KSProduct;
import ths.dms.core.entities.KSRegistedHis;
import ths.dms.core.entities.KSShopMap;
import ths.dms.core.entities.KSShopTarget;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.KeyShopType;
import ths.dms.core.entities.enumtype.ProductInfoObjectType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.RewardType;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.vo.AllocateShopVO;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.KeyShopCustomerHistoryVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.memcached.MemcachedUtils;

public class KeyShopAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8051124597792850084L;
	private static final int RESULT_SUCCESS = 6;
	private static final int RESULT_NO_SUCCESS = 7;
	private KeyShopMgr keyShopMgr;
	private ProductMgr productMgr;
	private CustomerMgr customerMgr;
	private CycleMgr cycleMgr;
	private ProductInfoMgr productInfoMgr;

	private File excelFile;
	private String excelFileContentType;

	private List<Integer> lstYear;
	private List<Integer> lstCycle;
	private List<CycleVO> lstCycleVO;
	private List<Long> lstProductInfoId;
	private List<Long> lstProductInfoChildId;
	private List<Long> lstProductId;
	private List<TreeNode> searchUnitTree;

	private String code;
	private String name;
	private String ksCode;
	private String ksName;
	private String ksLevelCode;
	private String ksLevelName;
	private String description;
	private String productCode;
	private String arrOjectText;
	private String changeDateStr;

	private Long shopId;
	private Long ksId;
	private Long ksLevelId;
	private Long fromCycleId;
	private Long toCycleId;
	private Long cycleId;
	private Long productInfoId;
	private Long productId;
	private Long currentCycleId;
	private Long lastCycleId;
	private Long customerId;

	private Integer year;
	private Integer num;
	private Integer processType;
	private Integer status;
	private Integer resultReward;
	private Integer excelType;
	private Integer ksType;
	private Integer minPhoto;
	private Integer quantity;
	private Integer flagChange;

	private Boolean isOrderByCode;
	private Boolean check;

	private BigDecimal amount;
	private List<Long> lstLong;
	private List<Long> arrLong;
	private List<Long> listKsLevelId;
	private List<Integer> listMultiplier;
	private Object permissionMgr;
	private List<Long> lstKsCustomerId;
	private int lockStatus;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		keyShopMgr = (KeyShopMgr) context.getBean("keyShopMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		cycleMgr = (CycleMgr) context.getBean("cycleMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		try {
			if (currentUser != null) {
				if (currentUser.getShopRoot() != null) {
					shopId = currentUser.getShopRoot().getShopId();
				}
			}
			Date sysdate = commonMgr.getSysDate();
			CycleFilter filter = new CycleFilter();
			filter.setLessYear(1);
			filter.setMoreYear(3);
			lstCycleVO = cycleMgr.getListCycleByFilter(filter);
			if (lstCycleVO != null && lstCycleVO.size() > 0) {
				for (CycleVO vo : lstCycleVO) {
					if (DateUtil.compareDateWithoutTime(sysdate, vo.getBeginDate()) >= 0 && DateUtil.compareDateWithoutTime(sysdate, vo.getEndDate()) <= 0) {
						currentCycleId = vo.getCycleId();
					}
				}
				lastCycleId = lstCycleVO.get(lstCycleVO.size() - 1).getCycleId();
			}
			lstYear = cycleMgr.getListYearOfAllCycle();
			Cycle curCycle = cycleMgr.getCycleByDate(sysdate);
			year = DateUtil.getYear(curCycle.getYear());
			num = curCycle.getNum();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.execute", createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * Lay danh sach lich su khach hang CTHTTM
	 * @author hunglm16
	 * @return JSON
	 * @throws Exception
	 * @since 24/11/2015
	 */
	public String getKSRegistedHistory() throws Exception {
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopCustomerHistoryVO>());
		try {
			KPaging<KeyShopCustomerHistoryVO> kPaging = new KPaging<KeyShopCustomerHistoryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			Date updateDate = null;
			if (!StringUtil.isNullOrEmpty(changeDateStr)) {
				updateDate = DateUtil.parse(changeDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			
			ObjectVO<KeyShopCustomerHistoryVO> rows = keyShopMgr.getKSRegistedHistory(kPaging, ksId, cycleId, ksLevelId, customerId, updateDate);

			if (rows != null) {
				result.put("total", rows.getkPaging().getTotalRows());
				result.put("rows", rows.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.getKSRegistedHistory", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Tim kiem ks
	 * @throws Exception
	 */
	public String searchKS() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopVO>());
		try {
			KPaging<KeyShopVO> kPaging = new KPaging<KeyShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			KeyShopFilter filter = new KeyShopFilter();
			filter.setkPagingVO(kPaging);
			filter.setCodeAndName(code);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setStatus(status);
			filter.setYear(year);
			filter.setNum(num);
			filter.setKsType(ksType);
			filter.setStrLstShopId(super.getStrListShopId());
			if (shopId != null && super.getMapShopChild().get(shopId) != null) {
				filter.setShopId(shopId);
			} else if (currentUser.getShopRoot() != null) {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			} else {
				return JSON;
			}
			if (currentUser.getShopRoot() != null && shopId.equals(currentUser.getShopRoot().getShopId())) {
				//neu chon la shop_root (tat ca) thi se xet theo admin or createuser
				filter.setCreateUser(currentUser.getUserName());
				if (currentUser.getStaffRoot() != null) {
					filter.setIsAdmin(StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()));
				}
			}
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKeyShopVOByFilter(filter);

			if (vo != null) {
				if (vo.getkPaging() != null) {
					result.put("total", vo.getkPaging().getTotalRows());
				}
				
				List<KeyShopVO> lstKeyShop = vo.getLstObject();
				for(KeyShopVO ks : lstKeyShop) {
					Integer fromCycleYear = ks.getFromCycleYear();
					Integer toCycleYear = ks.getToCycleYear();
					
					if(ks.getLstYear() == null) {
						ks.setLstYear(new ArrayList<Integer>()); 
					}
					
					for (int i = fromCycleYear; i <= toCycleYear; i++) {
						ks.getLstYear().add(i);
					}
				}
				
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.searchKS", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * @return the string
	 * @throws Exception the exception
	 * @author longnh15
	 * @since 29/07/2015
	 */
	public String lockCustomerReward() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			String errorLines = "";
			Date sysDate = commonMgr.getSysDate();
			if (lstKsCustomerId == null || lstKsCustomerId.size() == 0) {
				errorLines = errorLines + R.getResource("keyshop.belong.empty") + " | ";
			} else
				for (int i = 0; i < lstKsCustomerId.size(); ++i) {
					KSCustomer kc = null;
					kc = keyShopMgr.getKSCustomerById(lstKsCustomerId.get(i));

					if (kc != null) {
						String tmpCode = kc.getCustomer().getCustomerCode();
						if (lockStatus == 1) {
							if (kc.getRewardType() != null) {
								if (kc.getRewardType().getValue() == RewardType.MO_KHOA_CHUA_TRA.getValue()) {
									kc.setRewardType(RewardType.KHOA);
								} else
									errorLines = errorLines + R.getResource("keyshop.belong.lock", tmpCode) + " | ";
							} else
								errorLines = errorLines + R.getResource("keyshop.belong.lock", tmpCode) + " | ";
							//kc.setRewardType(RewardType.KHOA);						
						} else {
							if (kc.getRewardType() != null) {
								if (kc.getRewardType().getValue() == RewardType.KHOA.getValue()) {
									kc.setRewardType(RewardType.MO_KHOA_CHUA_TRA);
								} else
									errorLines = errorLines + R.getResource("keyshop.belong.unlock", tmpCode) + " | ";
							} else
								errorLines = errorLines + R.getResource("keyshop.belong.unlock", tmpCode) + " | ";

						}
						kc.setUpdateUser(getCurrentUser().getUserName());
						kc.setUpdateDate(sysDate);
						commonMgr.updateEntity(kc);
					}

				}
			errMsg = errorLines;
			if ("".equals(errMsg)) {
				error = false;
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.lockCustomerReward", createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put(ERROR, error);
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Chinh sua ks
	 * @throws Exception
	 */
	public String editKS() {
		resetToken(result);
		String errMsg = "";
		result.put(ERROR, true);
		result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		if (currentUser == null) {
			return PAGE_NOT_PERMISSION;
		}
		try {
			errMsg = ValidateUtil.validateField(ksCode, "key.shop.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(ksName, "key.shop.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(description, "key.shop.description", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			}
			KS ks = null;
			KeyShopType type = KeyShopType.parseValue(ksType);
			Cycle fromC = null;
			Cycle toC = null;
			if (StringUtil.isNullOrEmpty(errMsg)) {
				fromC = cycleMgr.getCycleById(fromCycleId);
				toC = cycleMgr.getCycleById(toCycleId);
				if (fromC == null || toC == null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.cycle.code");
				} else if (DateUtil.compareDateWithoutTime(fromC.getEndDate(), toC.getEndDate()) > 0) {
					errMsg = R.getResource("key.shop.import.err.from.large.to");
				}
			}
			if (StringUtil.isNullOrEmpty(errMsg) && (ksType == null || KeyShopType.parseValue(ksType) == null)) {
				errMsg = R.getResource("key.shop.input.not.exists.kstype");
			}
			if (StringUtil.isNullOrEmpty(errMsg) && type == null) {
				errMsg = R.getResource("key.shop.import.err.invalid");
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put("errMsg", errMsg);
				return JSON;
			}
			if (ksId != null) {//cap nhat
				ks = keyShopMgr.getKSById(ksId);
				if (ks == null)
					return JSON;
				//check chu ky cap nhat co anh huong khach hang hay khong
				//Neu co khach hang nao da tham gia chuong trinh voi thoi gian nam ngoai time update thi loi
				KeyShopFilter filter = new KeyShopFilter();
				filter.setBeginDate(fromC.getBeginDate());
				filter.setEndDate(toC.getEndDate());
				filter.setKsId(ks.getId());
				if (keyShopMgr.checkExistsCustomerNotInKSCycle(filter)) {
					result.put("errMsg", R.getResource("key.shop.import.err.exists.cust.without.cycle.update"));
					return JSON;
				}
				if (!ks.getProgramType().equals(type) && keyShopMgr.checkExistsRewardInKS(filter)) {
					result.put("errMsg", R.getResource("key.shop.exists.reward.ks.change.type"));
					return JSON;
				}
			} else {//them moi
				if (keyShopMgr.getKSByCode(ksCode) != null) {
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, true, "key.shop.code"));
					return JSON;
				}
				ks = new KS();
			}
			ks.setFromCycle(fromC);
			ks.setToCycle(toC);
			ks.setDescription(description);
			ks.setKsCode(ksCode.toUpperCase());
			ks.setName(ksName);
			ks.setMinPhotoNum(minPhoto);
			ks.setProgramType(KeyShopType.parseValue(ksType));
			ks.setStatus(ActiveType.parseValue(status));
			if (ks.getId() != null) {
				keyShopMgr.updateKS(ks, getLogInfoVO());
			} else {
				keyShopMgr.createKS(ks, getLogInfoVO());
			}
			result.put(ERROR, false);
			result.put("errMsg", "");
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.editKS", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Chinh sua ksLevel
	 * @throws Exception
	 */
	public String editKSLevel() {
		resetToken(result);
		result.put(ERROR, true);
		String errMsg = "";
		result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		if (currentUser == null) {
			return PAGE_NOT_PERMISSION;
		}
		try {
			errMsg = ValidateUtil.validateField(ksLevelCode, "key.shop.level.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(ksLevelName, "key.shop.level.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(errMsg) && amount == null) {
				errMsg = ValidateUtil.validateField("", "key.shop.amount", 200, ConstantManager.ERR_REQUIRE);
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put("errMsg", errMsg);
				return JSON;
			}
			if (ksLevelId == null) {//them moi
				KS ks = keyShopMgr.getKSById(ksId);
				if (ks == null)
					return JSON;
				KSLevel kl = keyShopMgr.getKSLevelByCode(ksLevelCode, ks.getKsCode());
				if (kl != null) {
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, true, "key.shop.level.code"));
					return JSON;
				}
				kl = new KSLevel();
				kl.setKs(ks);
				kl.setKsLevelCode(ksLevelCode.toUpperCase());
				kl.setAmount(amount);
				kl.setQuantity(quantity);
				kl.setName(ksLevelName);
				kl.setNameText(Unicode2English.codau2khongdau(ksLevelName).toUpperCase());
				kl.setCreateDate(commonMgr.getSysDate());
				kl.setCreateUser(currentUser.getUserName());
				commonMgr.createEntity(kl);
			} else {//cap nhat
				KSLevel kl = keyShopMgr.getKSLevelById(ksLevelId);
				if (kl == null) {
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.level.code"));
					return JSON;
				}
				kl.setAmount(amount);
				kl.setQuantity(quantity);
				kl.setName(ksLevelName);
				kl.setNameText(Unicode2English.codau2khongdau(ksLevelName).toUpperCase());
				kl.setUpdateDate(commonMgr.getSysDate());
				kl.setUpdateUser(currentUser.getUserName());
				commonMgr.updateEntity(kl);
			}
			result.put(ERROR, false);
			result.put("errMsg", "");
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.editKSLevel", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Them ksproduct vao ks
	 * @throws Exception
	 */
	public String addKSProduct() {
		resetToken(result);
		result.put(ERROR, true);
		result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		if (currentUser == null) {
			return PAGE_NOT_PERMISSION;
		}
		try {
			if (ksId == null || lstProductId == null || lstProductId.size() == 0) {
				return JSON;
			}
			KS ks = keyShopMgr.getKSById(ksId);
			if (ks == null) {
				return JSON;
			}
			Date sysdate = DateUtil.now();
			for (Long id : lstProductId) {
				Product p = productMgr.getProductById(id);
				if (p != null) {
					KSProduct kp = new KSProduct();
					kp.setKs(ks);
					kp.setProduct(p);
					kp.setCreateDate(sysdate);
					kp.setCreateUser(currentUser.getUserName());
					commonMgr.createEntity(kp);
				}
			}
			result.put(ERROR, false);
			result.put("errMsg", "");
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.addKSProduct", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Danh sach muc ksProduct
	 * @throws Exception
	 */
	public String deleteKSProduct() {
		resetToken(result);
		result.put(ERROR, true);
		result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		if (currentUser == null) {
			return PAGE_NOT_PERMISSION;
		}
		try {
			if (ksId == null)
				return JSON;
			Date sysdate = DateUtil.now();
			KeyShopFilter filter = new KeyShopFilter();
			filter.setKsId(ksId);
			filter.setProductId(productId);
			ObjectVO<KSProduct> vo = keyShopMgr.getListKSProductByFilter(filter);
			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
				for (KSProduct kp : vo.getLstObject()) {
					kp.setStatus(ActiveType.DELETED);
					kp.setUpdateDate(sysdate);
					kp.setUpdateUser(currentUser.getUserName());
					commonMgr.updateEntity(kp);
				}
			}
			result.put(ERROR, false);
			result.put("errMsg", "");
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.deleteKSProduct", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Xoa kslevel
	 * @throws Exception
	 */
	public String deleteKSLevel() {
		resetToken(result);
		result.put(ERROR, true);
		result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		if (currentUser == null) {
			return PAGE_NOT_PERMISSION;
		}
		try {
			KSLevel kl = keyShopMgr.getKSLevelById(ksLevelId);
			if (kl != null) {
				KeyShopFilter filter = new KeyShopFilter();
				filter.setKsLevelId(kl.getId());
				if (keyShopMgr.checkExistsCustomerNotInKSLevel(filter)) {
					result.put("errMsg", R.getResource("key.shop.import.err.delete.level.exists.cust"));
					return JSON;
				}
				kl.setUpdateDate(commonMgr.getSysDate());
				kl.setUpdateUser(currentUser.getUserName());
				kl.setStatus(ActiveType.DELETED);
				commonMgr.updateEntity(kl);
				result.put(ERROR, false);
				result.put("errMsg", "");
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.deleteKSLevel", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Danh sach muc KS
	 * @throws Exception
	 */
	public String searchKSLevel() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopVO>());
		try {
			KPaging<KeyShopVO> kPaging = new KPaging<KeyShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			KeyShopFilter filter = new KeyShopFilter();
			filter.setkPagingVO(kPaging);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setKsId(ksId);
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSLevelVOByFilter(filter);

			if (vo != null) {
				if (vo.getkPaging() != null) {
					result.put("total", vo.getkPaging().getTotalRows());
				}
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.searchKSLevel", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * @author longnh15
	 * @since 23/7/2015
	 * @return Danh sach muc KS của KH
	 * @throws Exception
	 */
	public String searchKSCustomerLevel() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopVO>());
		try {
			KPaging<KeyShopVO> kPaging = new KPaging<KeyShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			KeyShopFilter filter = new KeyShopFilter();
			filter.setkPagingVO(kPaging);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setKsId(ksId);
			filter.setCycleId(cycleId);
			filter.setCustomerId(customerId);
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSCustomerLevelVOByFilter(filter);

			if (vo != null) {
				if (vo.getkPaging() != null) {
					result.put("total", vo.getkPaging().getTotalRows());
				}
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.searchKSCustomerLevel", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * @author longnh15
	 * @since 21/7/2015
	 * @return Danh sach khach hang KS
	 * @throws Exception
	 */
	public String searchKSCustomer() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopVO>());
		try {
			KPaging<KeyShopVO> kPaging = new KPaging<KeyShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			KeyShopFilter filter = new KeyShopFilter();
			filter.setkPagingVO(kPaging);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setKsId(ksId);
			filter.setCodeAndName(code);
			filter.setShopId(shopId);
			filter.setStatus(status);
			filter.setYear(year);
			filter.setNum(num);
			filter.setStrLstShopId(super.getStrListShopId());
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSCustomerVOByFilter(filter);
			if (vo != null) {
				if (vo.getkPaging() != null) {
					result.put("total", vo.getkPaging().getTotalRows());
				}
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.searchKSCustomer", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * @author longnh15
	 * @since 28/7/2015
	 * @return Danh sach khach hang tra thuongKS
	 * @throws Exception
	 */
	public String searchKSReward() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopVO>());
		try {
			KPaging<KeyShopVO> kPaging = new KPaging<KeyShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			KeyShopFilter filter = new KeyShopFilter();
			filter.setkPagingVO(kPaging);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setKsId(ksId);
			filter.setCodeAndName(code);
			filter.setShopId(shopId);
			filter.setStatus(status);
			filter.setYear(year);
			filter.setNum(num);
			filter.setResult(resultReward);
			filter.setStrLstShopId(super.getStrListShopId());
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSRewardVOByFilter(filter); //Reward

			if (vo != null) {
				if (vo.getkPaging() != null) {
					result.put("total", vo.getkPaging().getTotalRows());
				}
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.searchKSReward", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Danh sach muc KS
	 * @throws Exception
	 */
	public String searchLevel() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopVO>());
		try {
			KPaging<KeyShopVO> kPaging = new KPaging<KeyShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			KeyShopFilter filter = new KeyShopFilter();
			filter.setkPagingVO(kPaging);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setExceptKSId(ksId);
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSLevelVOByFilter(filter);

			if (vo != null) {
				if (vo.getkPaging() != null) {
					result.put("total", vo.getkPaging().getTotalRows());
				}
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.searchLevel", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Danh sach san pham theo KS
	 * @throws Exception
	 */
	public String searchKSProduct() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopVO>());
		try {
			KPaging<KeyShopVO> kPaging = new KPaging<KeyShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			KeyShopFilter filter = new KeyShopFilter();
			filter.setkPagingVO(kPaging);
			filter.setCodeAndName(code);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setKsId(ksId);
			filter.setLstProductInfoId(lstProductInfoId);
			filter.setLstProductInfoChildId(lstProductInfoChildId);
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSProductVOByFilter(filter);

			if (vo != null) {
				if (vo.getkPaging() != null) {
					result.put("total", vo.getkPaging().getTotalRows());
				}
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.searchKSProduct", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @return Danh sach san pham
	 * @throws Exception
	 */
	public String searchProduct() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		result.put("total", 0);
		result.put("rows", new ArrayList<KeyShopVO>());
		try {
			KPaging<KeyShopVO> kPaging = new KPaging<KeyShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			KeyShopFilter filter = new KeyShopFilter();
			filter.setkPagingVO(kPaging);
			filter.setCode(code);
			filter.setName(name);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setExceptKSId(ksId);
			filter.setStatus(ActiveType.RUNNING.getValue());
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSProductVOByFilter(filter);

			if (vo != null) {
				if (vo.getkPaging() != null) {
					result.put("total", vo.getkPaging().getTotalRows());
				}
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.searchProduct", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Lay danh sach product info (nganh hang, nhan hieu, huong vi)
	 * 
	 * @author tungmt
	 * @return JSON
	 * @since 09/07/2015
	 */
	public String getListCategories() {
		result.put(ERROR, false);
		try {
			int itype = 1;
			if (status != null) {
				itype = status.intValue();
			}
			ProductType type = ProductType.parseValue(itype);
			List<ProductInfoObjectType> objTypes = new ArrayList<ProductInfoObjectType>();
			objTypes.add(ProductInfoObjectType.SP);
			boolean orderByCode = false;
			if (isOrderByCode != null && isOrderByCode) {
				orderByCode = true;
			}

			ObjectVO<ProductInfo> lstCategoryTmp = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, type, objTypes, orderByCode);
			List<ProductInfo> lst = lstCategoryTmp.getLstObject();
			if (lst == null) {
				lst = new ArrayList<ProductInfo>();
			}
			result.put("rows", lst);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.KeyShopAction.getListCategories", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Lay danh sach product info (nganh hang, nhan hieu, huong vi)
	 * 
	 * @author tungmt
	 * @return JSON
	 * @since 09/07/2015
	 */
	public String getListCategoriesChild() {
		result.put(ERROR, false);
		try {
			int itype = 2;
			if (status != null) {
				itype = status.intValue();
			}
			ProductType type = ProductType.parseValue(itype);
			List<ProductInfo> lstCategoryChildTmp;
			if (productInfoId != null && productInfoId > 0L) {
				lstCategoryChildTmp = productInfoMgr.getListSubCat(ActiveType.RUNNING, type, productInfoId);
			} else {
				lstCategoryChildTmp = productInfoMgr.getListSubCatByListCat(ActiveType.RUNNING, type, lstProductInfoId);
			}
			if (lstCategoryChildTmp == null) {
				lstCategoryChildTmp = new ArrayList<ProductInfo>();
			}
			result.put("rows", lstCategoryChildTmp);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.program.KeyShopAction.getListCategoriesChild", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String exportTemplateExcel() {
		String reportToken = retrieveReportToken(reportCode);
		if (StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		if (excelType == 1) {
			return this.exportTemplateExcelKS();
		} else if (excelType == 2) {
			return this.exportTemplateExcelProduct();
		} else if (excelType == 3) {
			return this.exportTemplateExcelShop();
		} else if (excelType == 4) {
			return this.exportTemplateExcelCustomer();
		} else if (excelType == 5) {
			return this.exportTemplateExcelReward();
		}
		return JSON;
	}

	/**
	 * xuat excel danh sach keyshop
	 * 
	 * @author trietptm
	 * @return JSON
	 * @since Aug 06, 2015
	 */
	public String exportExcelKS() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			KeyShopFilter filter = new KeyShopFilter();
			filter.setCodeAndName(code);
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setStatus(status);
			filter.setYear(year);
			filter.setNum(num);
			filter.setKsType(ksType);
			filter.setStrLstShopId(super.getStrListShopId());
			if (shopId != null && super.getMapShopChild().get(shopId) != null) {
				filter.setShopId(shopId);
			} else if (currentUser.getShopRoot() != null) {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			} else {
				return JSON;
			}
			if (currentUser.getShopRoot() != null && shopId.equals(currentUser.getShopRoot().getShopId())) {
				//neu chon la shop_root (tat ca) thi se xet theo admin or createuser
				filter.setCreateUser(currentUser.getUserName());
				if (currentUser.getStaffRoot() != null) {
					filter.setIsAdmin(StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()));
				}
			}
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKeyShopVOByFilter(filter);
			if (vo.getLstObject() == null || vo.getLstObject().size() == 0) {
				return JSON;
			}
			List<KeyShopVO> lstkeyShop = vo.getLstObject();
			List<Long> lstKsId = new ArrayList<Long>();
			for (int i = 0, n = lstkeyShop.size(); i < n; i++) {
				KeyShopVO keyShop = lstkeyShop.get(i);
				lstKsId.add(keyShop.getKsId());
			}
			// lay tat ca san pham theo list keyshop
			filter = new KeyShopFilter();
			filter.setLstKsId(lstKsId);
			ObjectVO<KeyShopVO> lstVOProduct = keyShopMgr.getListKSProductVOByFilter(filter);
			List<KeyShopVO> lstProductAll = lstVOProduct.getLstObject();
			// lay tat ca cac muc theo list keyshop
			ObjectVO<KeyShopVO> lstVOLevel = keyShopMgr.getListKSLevelVOByFilter(filter);
			List<KeyShopVO> lstLevelAll = lstVOLevel.getLstObject();
			// lay tat ca cac nha phan phoi tham gia theo list keyshop
			filter.setStrLstShopId(super.getStrListShopId());
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			List<AllocateShopVO> lstShopAll = keyShopMgr.getListAllocateShop(filter);

			for (int i = 0, n = lstkeyShop.size(); i < n; i++) {
				KeyShopVO keyShop = lstkeyShop.get(i);
				Long keyShopId = keyShop.getKsId();
				List<KeyShopVO> lstProduct = new ArrayList<KeyShopVO>();
				List<KeyShopVO> lstLevel = new ArrayList<KeyShopVO>();
				List<AllocateShopVO> lstShop = new ArrayList<AllocateShopVO>();
				if (lstProductAll != null) {
					for (int j = 0, m = lstProductAll.size(); j < m; j++) {
						KeyShopVO product = lstProductAll.get(j);
						if (keyShopId.equals(product.getKsId())) {
							lstProduct.add(product);
						}
					}
				}
				keyShop.setLstProduct(lstProduct);
				//====
				if (lstLevelAll != null) {
					for (int j = 0, m = lstLevelAll.size(); j < m; j++) {
						KeyShopVO level = lstLevelAll.get(j);
						if (keyShopId.equals(level.getKsId())) {
							lstLevel.add(level);
						}
					}
				}
				keyShop.setLstLevel(lstLevel);
				//===
				if (lstShopAll != null) {
					for (int j = 0, m = lstShopAll.size(); j < m; j++) {
						AllocateShopVO shop = lstShopAll.get(j);
						if (keyShopId.equals(shop.getKsId())) {
							lstShop.add(shop);
						}
					}
				}
				keyShop.setLstShop(lstShop);
				keyShop.setKsNameUpper(keyShop.getKsName().toUpperCase());
				if (keyShop.getKsType() != null && KeyShopType.KEY_SHOP.getValue() == keyShop.getKsType()) {
					keyShop.setStrKSType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "key.shop.type.value.clb"));
				} else if (keyShop.getKsType() != null && KeyShopType.DISPLAY.getValue() == keyShop.getKsType()) {
					keyShop.setStrKSType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "key.shop.type.value.display"));
				}
				if (keyShop.getStatus() != null && ActiveType.RUNNING.getValue() == keyShop.getStatus()) {
					keyShop.setStrStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.active"));
				} else if (keyShop.getStatus() != null && ActiveType.STOPPED.getValue() == keyShop.getStatus()) {
					keyShop.setStrStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.stopped"));
				}
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("lstkeyShop", lstkeyShop);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.EXPORT_EXCEL_KEY_SHOP;
			exportExcelDataTemplate(params, ConstantManager.EXPORT_EXCEL_KEY_SHOP, outputName);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.exportExcelKS", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * xuat excel danh sach khach hang
	 * 
	 * @author trietptm
	 * @return JSON
	 * @since Aug 06, 2015
	 */
	public String exportExcelCustomer() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			KS ks = keyShopMgr.getKSById(ksId);
			if (ks == null) {
				return JSON;
			}
			KeyShopFilter filter = new KeyShopFilter();
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setKsId(ksId);
			filter.setCodeAndName(code);
			filter.setShopId(shopId);
			filter.setStatus(status);
			filter.setYear(year);
			filter.setNum(num);
			filter.setStrLstShopId(super.getStrListShopId());
			filter.setIsGetParentShop(true);
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSCustomerVOByFilter(filter);
			if (vo.getLstObject() == null || vo.getLstObject().size() == 0) {
				return JSON;
			}
			List<KeyShopVO> lstCustomer = vo.getLstObject();
			for (int i = 0, n = lstCustomer.size(); i < n; i++) {
				KeyShopVO customer = lstCustomer.get(i);
				customer.setKsNameUpper(ks.getName().toUpperCase());
				if (ActiveType.RUNNING.getValue() == customer.getStatus()) {
					customer.setStrStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.approved"));
				} else if (ActiveType.WAITING.getValue() == customer.getStatus()) {
					customer.setStrStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.not.approved"));
				} else if (ActiveType.REJECTED.getValue() == customer.getStatus()) {
					customer.setStrStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.reject"));
				} else if (ActiveType.STOPPED.getValue() == customer.getStatus()) {
					customer.setStrStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.stopped"));
				}
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("lstCustomer", lstCustomer);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.EXPORT_EXCEL_KEY_SHOP_CUSTOMER;
			exportExcelDataTemplate(params, ConstantManager.EXPORT_EXCEL_KEY_SHOP_CUSTOMER, outputName);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "KeyShopAction.exportExcelCustomer - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * xuat excel danh sach trả thưởng
	 * 
	 * @author trietptm
	 * @return JSON
	 * @since Aug 06, 2015
	 */
	public String exportExcelReward() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			KS ks = keyShopMgr.getKSById(ksId);
			if (ks == null) {
				return JSON;
			}
			KeyShopFilter filter = new KeyShopFilter();
			filter.setOrder(order);
			filter.setSort(sort);
			filter.setKsId(ksId);
			filter.setCodeAndName(code);
			filter.setShopId(shopId);
			filter.setStatus(status);
			filter.setYear(year);
			filter.setNum(num);
			filter.setResult(resultReward);
			filter.setStrLstShopId(super.getStrListShopId());
			filter.setIsGetParentShop(true);
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKSRewardVOByFilter(filter);
			if (vo.getLstObject() == null || vo.getLstObject().size() == 0) {
				return JSON;
			}
			List<KeyShopVO> lstReward = vo.getLstObject();
			for (int i = 0, n = lstReward.size(); i < n; i++) {
				KeyShopVO reward = lstReward.get(i);
				reward.setKsNameUpper(ks.getName().toUpperCase());
				if (RESULT_SUCCESS == reward.getResult()) {
					reward.setStrResult(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.success"));
				} else if (RESULT_NO_SUCCESS == reward.getResult()) {
					reward.setStrResult(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.success"));
				}
				if (reward.getRewardType() == null) {
					reward.setStrRewardStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.reward.type.not.import"));
				} else if (RewardType.CHUYEN_KHOAN.getValue() == reward.getRewardType()) {
					reward.setStrRewardStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.reward.type.transfer"));
				} else if (RewardType.KHOA.getValue() == reward.getRewardType()) {
					reward.setStrRewardStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.reward.type.not.open"));
				} else if (RewardType.MO_KHOA_CHUA_TRA.getValue() == reward.getRewardType()) {
					reward.setStrRewardStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.reward.type.opened"));
				} else if (RewardType.DA_TRA_MOT_PHAN.getValue() == reward.getRewardType()) {
					reward.setStrRewardStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.reward.type.not.full"));
				} else if (RewardType.DA_TRA_TOAN_BO.getValue() == reward.getRewardType()) {
					reward.setStrRewardStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.reward.type.full"));
				}
				if (reward.getRewardType() == null || RewardType.KHOA.getValue() == reward.getRewardType() || RewardType.MO_KHOA_CHUA_TRA.getValue() == reward.getRewardType() || RewardType.DA_TRA_MOT_PHAN.getValue() == reward.getRewardType()
						|| RewardType.DA_TRA_TOAN_BO.getValue() == reward.getRewardType()) {
					reward.setStrRewardType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.reward.type.coupon"));
				} else if (RewardType.CHUYEN_KHOAN.getValue() == reward.getRewardType()) {
					reward.setStrRewardType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.reward.type.transfer"));
				}
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("lstReward", lstReward);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.EXPORT_EXCEL_KEY_SHOP_REWARD;
			exportExcelDataTemplate(params, ConstantManager.EXPORT_EXCEL_KEY_SHOP_REWARD, outputName);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "KeyShopAction.exportExcelReward - " + e.getMessage());
		}
		return JSON;
	}

	public String exportTemplateExcelKS() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_KEY_SHOP_KS;
			exportExcelDataTemplate(params, ConstantManager.TEMPLATE_KEY_SHOP_KS, outputName);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "KeyShopAction.exportTemplateExcelKS - " + e.getMessage());
		}
		return JSON;
	}

	public String exportTemplateExcelProduct() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_KEY_SHOP_PRODUCT;
			exportExcelDataTemplate(params, ConstantManager.TEMPLATE_KEY_SHOP_PRODUCT, outputName);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "KeyShopAction.exportTemplateExcelProduct - " + e.getMessage());
		}
		return JSON;
	}

	public String exportTemplateExcelShop() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_KEY_SHOP_SHOP;
			exportExcelDataTemplate(params, ConstantManager.TEMPLATE_KEY_SHOP_SHOP, outputName);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "KeyShopAction.exportTemplateExcelShop - " + e.getMessage());
		}
		return JSON;
	}

	public String exportTemplateExcelCustomer() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_KEY_SHOP_CUSTOMER;
			exportExcelDataTemplate(params, ConstantManager.TEMPLATE_KEY_SHOP_CUSTOMER, outputName);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "KeyShopAction.exportTemplateExcelCustomer - " + e.getMessage());
		}
		return JSON;
	}

	public String exportTemplateExcelReward() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_KEY_SHOP_REWARD;
			exportExcelDataTemplate(params, ConstantManager.TEMPLATE_KEY_SHOP_REWARD, outputName);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "KeyShopAction.exportTemplateExcelReward - " + e.getMessage());
		}
		return JSON;
	}

	private void exportExcelDataTemplate(Map<String, Object> lstParam, String tempFileName, String outputName) throws Exception {
		try {
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathKeyShop() + tempFileName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			Map<String, Object> params = new HashMap<String, Object>();
			if (lstParam != null && lstParam.size() > 0) {
				params.putAll(lstParam);
			}

			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(retrieveReportToken(reportCode), outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			throw e;
		}
	}

	public String importExcel() {
		resetToken(result);
		isError = true;
		totalItem = 0;
		if (excelType == 1) {//key shop
			return importKS();
		} else if (excelType == 2) {//product
			return importProduct();
		} else if (excelType == 3) {//shop
			return importShop();
		} else if (excelType == 4) {//khach hang
			return importCustomer();
		} else if (excelType == 5) {//tra thuong
			return importReward();
		}
		return SUCCESS;
	}

	private static Integer SHOP_KS_CODE = 0;
	private static Integer SHOP_KS_NAME = 1;
	private static Integer SHOP_SHOP_CODE = 2;
	private static Integer SHOP_SHOP_NAME = 3;
	private static Integer SHOP_TOTAL_COLS = 4;

	private String importShop() {
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, SHOP_TOTAL_COLS);

		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				KeyShopFilter filter = new KeyShopFilter();
				Date sysdate = commonMgr.getSysDate();
				for (int i = 0; i < lstData.size(); i++) {
					List<String> row = lstData.get(i);
					if (row == null || row.size() == 0) {
						continue;
					}
					message = "";
					totalItem++;
					KS ks = null;
					Shop s = null;
					//Ma CLB
					if (StringUtil.isNullOrEmpty(message) && row.size() > SHOP_KS_CODE) {
						String value = row.get(SHOP_KS_CODE);
						message += ValidateUtil.validateField(value, "key.shop.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							ks = keyShopMgr.getKSByCode(value);
							if (ks == null) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.code");
							} else if (!ActiveType.RUNNING.equals(ks.getStatus())) {
								message += R.getResource("key.shop.import.err.ks.not.running");
							}
						}
					}
					//Ma NPP
					if (StringUtil.isNullOrEmpty(message) && row.size() > SHOP_SHOP_CODE) {
						String value = row.get(SHOP_SHOP_CODE);
						message += ValidateUtil.validateField(value, "key.shop.import.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							s = shopMgr.getShopByCode(value);
							if (s == null) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.import.npp.code");
							} else if (super.getMapShopChild().get(s.getId()) == null) {
								message += R.getResource("key.shop.import.err.shop.not.belong");
							} else {
								filter = new KeyShopFilter();
								filter.setShopId(s.getId());
								filter.setKsId(ks.getId());
								if (keyShopMgr.checkKSCustomerExistsShop(filter)) {
									message += R.getResource("key.shop.import.err.customer.exists.shop.same.level");
								}
							}
						}
					}

					if (StringUtil.isNullOrEmpty(message)) {
						filter = new KeyShopFilter();
						filter.setShopId(s.getId());
						filter.setKsId(ks.getId());
						filter.setUpdateUser(currentUser.getUserName());

						//xoa cac shop con
						filter.setIsGetChild(true);
						keyShopMgr.removeShopMapByShop(filter);

						//xoa cac shop cha
						filter.setIsGetChild(false);
						keyShopMgr.removeShopMapByShop(filter);

						//them dong vao shop_map
						KSShopMap ksm = new KSShopMap();
						ksm.setKs(ks);
						ksm.setShop(s);
						ksm.setCreateDate(sysdate);
						ksm.setCreateUser(currentUser.getUserName());
						commonMgr.createEntity(ksm);
					} else {
						lstFails.add(StringUtil.addFailBean(row, message));
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_KEY_SHOP_SHOP_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	private static Integer RW_KS_CODE = 0;
	private static Integer RW_KS_NAME = 1;
	private static Integer RW_SHOP_CODE = 2;
	private static Integer RW_SHOP_NAME = 3;
	private static Integer RW_CUSTOMER_CODE = 4;
	private static Integer RW_CUSTOMER_NAME = 5;
	private static Integer RW_CUSTOMER_ADDRESS = 6;
	private static Integer RW_CYCLE_CODE = 7;
	private static Integer RW_TOTAL_REWARD_MONEY = 8;
	private static Integer RW_AMOUNT_DISCOUNT = 9;
	private static Integer RW_OVER_AMOUNT_DISCOUNT = 10;
	private static Integer RW_DISPLAY_MONEY = 11;
	private static Integer RW_SUPPORT_MONEY = 12;
	private static Integer RW_ODD_DISCOUNT = 13;
	private static Integer RW_ADD_MONEY = 14;
	private static Integer RW_PRODUCT_CODE = 15;
	private static Integer RW_PRODUCT_QUANTITY = 16;
	private static Integer RW_REWARD_TYPE = 17;
	private static Integer RW_TOTAL_COLS = 18;

	private String importReward() {
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, RW_TOTAL_COLS);

		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				KeyShopFilter filter = new KeyShopFilter();
				Date sysdate = commonMgr.getSysDate();
				String[] rewardType = R.getResource("key.shop.import.reward.type.enum").split(";");
				for (int i = 0; i < lstData.size(); i++) {
					List<String> row = lstData.get(i);
					if (row == null || row.size() == 0) {
						continue;
					}
					message = "";
					totalItem++;
					KS ks = null;
					Product p = null;
					Shop s = null;
					Customer c = null;
					Cycle cycle = null;
					KSCustomer kc = null;
					KSCusProductReward kcpr = null;
					//Ma CLB
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_KS_CODE) {
						String value = row.get(RW_KS_CODE);
						message += ValidateUtil.validateField(value, "key.shop.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							ks = keyShopMgr.getKSByCode(value);
							if (ks == null) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.code");
							}
						}
					}
					//Ma NPP
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_SHOP_CODE) {
						String value = row.get(RW_SHOP_CODE);
						message += ValidateUtil.validateField(value, "key.shop.import.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							s = shopMgr.getShopByCode(value);
							if (s == null) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.import.npp.code");
							} else if (super.getMapShopChild().get(s.getId()) == null) {
								message += R.getResource("key.shop.import.err.shop.not.belong");
							} else {
								filter = new KeyShopFilter();
								filter.setShopId(s.getId());
								filter.setKsId(ks.getId());
								List<Shop> lst = keyShopMgr.getListShopForKSByFilter(filter);
								if (lst == null || lst.size() == 0) {
									message += R.getResource("key.shop.import.err.shop.not.belong.ks");
								}
							}
						}
					}
					//Ma KH
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_CUSTOMER_CODE) {
						String value = row.get(RW_CUSTOMER_CODE);
						message += ValidateUtil.validateField(value, "key.shop.customer.code", 13, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							c = customerMgr.getCustomerByCodeAndShop(value, s.getId());
							if (c == null) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.customer.code");
							}
						}
					}
					//Ma chu ky
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_CYCLE_CODE) {
						String value = row.get(RW_CYCLE_CODE);
						message += ValidateUtil.validateField(value, "key.shop.cycle.code", 7, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FORMAT_CYCLE_YEAR);
						if (StringUtil.isNullOrEmpty(message)) {
							String[] str = value.split("/");
							Integer num = Integer.parseInt(str[0]);
							Integer year = Integer.parseInt(str[1]);
							if (num == null || num < ConstantManager.MIN_CYCLE_NUM || num > ConstantManager.MAX_CYCLE_NUM) {
								message += R.getResource("key.shop.import.err.cycle.1.13");
							} else {
								cycle = cycleMgr.getCycleByYearAndNum(year, num);
								if (cycle == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.cycle.code");
								}
							}
						}
					}
					if (StringUtil.isNullOrEmpty(message)) {
						//kiem tra kscustomer da ton tai chua, neu chua thi bao loi, co roi thi lay len update
						filter = new KeyShopFilter();
						filter.setKsId(ks.getId());
						filter.setCycleId(cycle.getId());
						filter.setCustomerId(c.getId());
						ObjectVO<KSCustomer> vo = keyShopMgr.getListKSCustomerByFilter(filter);
						if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
							kc = vo.getLstObject().get(0);
							if (RewardType.MO_KHOA_CHUA_TRA.equals(kc.getRewardType()) || RewardType.DA_TRA_MOT_PHAN.equals(kc.getRewardType()) || RewardType.DA_TRA_TOAN_BO.equals(kc.getRewardType())) {
								message += R.getResource("key.shop.import.err.rewarded.for.customer");
							} else if (!ActiveType.RUNNING.equals(kc.getStatus())) {
								message += R.getResource("key.shop.import.err.customer.not.running");
							}
						} else {
							message += R.getResource("key.shop.import.err.ks.customer.not.exists");
						}
					}
					//Tong tien tra thuong
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_TOTAL_REWARD_MONEY) {
						String value = row.get(RW_TOTAL_REWARD_MONEY);
						message += ValidateUtil.validateField(value, "key.shop.total.reward.money", 26, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE);
						if (StringUtil.isNullOrEmpty(message)) {
							BigDecimal valueBig = new BigDecimal(value);
							if (valueBig.longValue() < 0) {
								message += R.getResource("common.not.int", R.getResource("key.shop.total.reward.money"));
							} else {
								kc.setTotalRewardMoney(valueBig);
							}
						}
					}
					//Chiet khau doanh so
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_AMOUNT_DISCOUNT) {
						String value = row.get(RW_AMOUNT_DISCOUNT).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							message += ValidateUtil.validateField(value, "key.shop.amount.discount", 26, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(message)) {
								BigDecimal valueBig = new BigDecimal(value);
								if (valueBig.longValue() < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.amount.discount"));
								} else {
									kc.setAmountDiscount(valueBig);
								}
							}
						}
					}
					//Chiet khau vuot doanh so
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_OVER_AMOUNT_DISCOUNT) {
						String value = row.get(RW_OVER_AMOUNT_DISCOUNT).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							message += ValidateUtil.validateField(value, "key.shop.over.amount.discount", 26, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(message)) {
								BigDecimal valueBig = new BigDecimal(value);
								if (valueBig.longValue() < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.over.amount.discount"));
								} else {
									kc.setOverAmountDiscount(valueBig);
								}
							}
						}
					}
					//Tien trung bay
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_DISPLAY_MONEY) {
						String value = row.get(RW_DISPLAY_MONEY).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							message += ValidateUtil.validateField(value, "key.shop.display.money", 26, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(message)) {
								BigDecimal valueBig = new BigDecimal(value);
								if (valueBig.longValue() < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.display.money"));
								} else {
									kc.setDisplayMoney(valueBig);
								}
							}
						}
					}
					//Tien ho tro
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_SUPPORT_MONEY) {
						String value = row.get(RW_SUPPORT_MONEY).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							message += ValidateUtil.validateField(value, "key.shop.support.money", 26, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(message)) {
								BigDecimal valueBig = new BigDecimal(value);
								if (valueBig.longValue() < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.support.money"));
								} else {
									kc.setSupportMoney(valueBig);
								}
							}
						}
					}
					//Tru chiet khau phu le
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_ODD_DISCOUNT) {
						String value = row.get(RW_ODD_DISCOUNT).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							message += ValidateUtil.validateField(value, "key.shop.odd.discount", 26, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(message)) {
								BigDecimal valueBig = new BigDecimal(value);
								if (valueBig.longValue() < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.odd.discount"));
								} else {
									kc.setOddDicount(valueBig);
								}
							}
						}
					}
					//Tien thuong bo sung
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_ADD_MONEY) {
						String value = row.get(RW_ADD_MONEY).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							message += ValidateUtil.validateField(value, "key.shop.add.money", 26, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(message)) {
								BigDecimal valueBig = new BigDecimal(value);
								if (valueBig.longValue() < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.add.money"));
								} else {
									kc.setAddMoney(valueBig);
								}
							}
						}
					}
					//Ma SP
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_PRODUCT_CODE) {
						String value = row.get(RW_PRODUCT_CODE).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							message += ValidateUtil.validateField(value, "key.shop.product.code", 100, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(message)) {
								p = productMgr.getProductByCode(value);
								if (p == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.product.code");
								} else if (!ActiveType.RUNNING.equals(p.getStatus())) {
									message += R.getResource("key.shop.import.sku.not.running");
								} else {
									filter = new KeyShopFilter();
									filter.setKsCustomerId(kc.getId());
									filter.setProductId(p.getId());
									ObjectVO<KSCusProductReward> vo = keyShopMgr.getListKSCusProductRewardByFilter(filter);
									if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
										kcpr = vo.getLstObject().get(0);
										kcpr.setUpdateDate(sysdate);
										kcpr.setUpdateUser(currentUser.getUserName());
									} else {
										kcpr = new KSCusProductReward();
										kcpr.setKsCustomer(kc);
										kcpr.setProduct(p);
										kcpr.setCreateDate(sysdate);
										kcpr.setCreateUser(currentUser.getUserName());
										kcpr.setKs(ks);
										kcpr.setShop(s);
									}
								}
							}
						}
					}
					//So luong san pham tra thuong
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_PRODUCT_QUANTITY) {
						String value = row.get(RW_PRODUCT_QUANTITY);
						if (kcpr != null) {
							message += ValidateUtil.validateField(value, "key.shop.product.quantity", 6, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE);
							if (StringUtil.isNullOrEmpty(message)) {
								int quantity = Integer.parseInt(value);
								if (quantity < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.product.quantity"));
								} else {
									kcpr.setProductNum(quantity);
								}
							}
						} else if (!StringUtil.isNullOrEmpty(value.trim())) {//neu ko nhap sp ma nhap sl thi bao loi
							message += R.getResource("key.shop.import.err.not.exists.product");
						}
					}
					//Loai tra thuong
					if (StringUtil.isNullOrEmpty(message) && row.size() > RW_REWARD_TYPE) {
						String value = row.get(RW_REWARD_TYPE);
						message += ValidateUtil.validateField(value, "key.shop.reward.type", 20, ConstantManager.ERR_REQUIRE);
						if (StringUtil.isNullOrEmpty(message)) {
							if (rewardType[0].equalsIgnoreCase(value)) {//Chuyen khoan
								kc.setRewardType(RewardType.CHUYEN_KHOAN);
							} else if (rewardType[1].equalsIgnoreCase(value)) {//coupon
								kc.setRewardType(RewardType.KHOA);
							} else {
								message += R.getResource("key.shop.import.err.reward.type.enum");
							}
						}
					}

					if (StringUtil.isNullOrEmpty(message)) {
						//update KC
						kc.setUpdateDate(sysdate);
						kc.setUpdateUser(currentUser.getUserName());
						commonMgr.updateEntity(kc);
						if (kcpr != null) {
							kcpr.setKsCustomer(kc);
							if (kcpr.getId() == null) {
								commonMgr.createEntity(kcpr);
							} else {
								commonMgr.updateEntity(kcpr);
							}
						}
					} else {
						lstFails.add(StringUtil.addFailBean(row, message));
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_KEY_SHOP_REWARD_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	private static Integer KH_KS_CODE = 0;
	private static Integer KH_KS_NAME = 1;
	private static Integer KH_SHOP_CODE = 2;
	private static Integer KH_SHOP_NAME = 3;
	private static Integer KH_CUSTOMER_CODE = 4;
	private static Integer KH_CUSTOMER_NAME = 5;
	private static Integer KH_CYCLE_CODE = 6;
	private static Integer KH_LEVEL_CODE = 7;
	private static Integer KH_LEVEL_NAME = 8;
	private static Integer KH_MULTIPLIER = 9;
	private static Integer KH_TOTAL_COLS = 10;

	/**
	 * Them khach hang vao chuong trinh
	 * @return
	 * 
	 * @modify hunglm16
	 * @since 20/11/2015
	 * @description Kiem tra voi So xuat NPP hop le
	 */
	private String importCustomer() {
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, KH_TOTAL_COLS);

		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				KeyShopFilter filter = new KeyShopFilter();
				Date sysdate = commonMgr.getSysDate();
				KSRegistedHis ksRegisHis = null;
				KS ks = null;
				KSLevel lv = null;
				Shop s = null;
				Customer c = null;
				Cycle cycle = null;
				KSCustomer kc = null;
				KSCustomerLevel kcl = null;
				
				//Kiem tra xem tong muc > 0 thi moi cho import
				int multiRow = 0;
				for (int i = 0; i < lstData.size(); i++) {
					message = "";
					totalItem++;
					List<String> row = lstData.get(i);
					if (row == null || row.size() == 0) {
						continue;
					}
					
					if (row.size() > KH_MULTIPLIER) {
						String value = row.get(KH_MULTIPLIER);
						
						if (!StringUtil.isNullOrEmpty(value.trim())) {
							message += ValidateUtil.validateField(value, "key.shoo.multiplier", 6, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE);
							if (StringUtil.isNullOrEmpty(message)) {
								int multiplier = Integer.parseInt(value);
								
								multiRow += multiplier;
								
								if (multiplier < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shoo.multiplier"));
								} else if (multiplier == 0) {
									message += R.getResource("key.shop.not.choose.multiplier");
								}
							}
						} else {
							message += R.getResource("key.shop.not.choose.multiplier");
						}
						
						if(!StringUtil.isNullOrEmpty(message)) {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				
				if(multiRow > 0) {
					lstFails.clear();
					message = "";
					totalItem = 0;
				}
				
				//Neu tong cac boi so cua muc ko bi loi thi moi kiem tra cap nhat tung row
				if(multiRow > 0) {
					for (int i = 0; i < lstData.size(); i++) {
						kc = new KSCustomer();
						List<String> row = lstData.get(i);
						if (row == null || row.size() == 0) {
							continue;
						}
						message = "";
						totalItem++;
						//Ma CLB
						if (StringUtil.isNullOrEmpty(message) && row.size() > KH_KS_CODE) {
							String value = row.get(KH_KS_CODE);
							message += ValidateUtil.validateField(value, "key.shop.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(message)) {
								ks = keyShopMgr.getKSByCode(value);
								if (ks == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.code");
								}
							}
						}
						//Ma NPP
						if (StringUtil.isNullOrEmpty(message) && row.size() > KH_SHOP_CODE) {
							String value = row.get(KH_SHOP_CODE);
							message += ValidateUtil.validateField(value, "key.shop.import.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(message)) {
								s = shopMgr.getShopByCode(value);
								if (s == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.import.npp.code");
								} else if (super.getMapShopChild().get(s.getId()) == null) {
									message += R.getResource("key.shop.import.err.shop.not.belong");
								} else {
									filter = new KeyShopFilter();
									filter.setShopId(s.getId());
									filter.setKsId(ks.getId());
									List<Shop> lst = keyShopMgr.getListShopForKSByFilter(filter);
									if (lst == null || lst.size() == 0) {
										message += R.getResource("key.shop.import.err.shop.not.belong.ks");
									}
								}
							}
						}
						//Ma KH
						if (StringUtil.isNullOrEmpty(message) && row.size() > KH_CUSTOMER_CODE) {
							String value = row.get(KH_CUSTOMER_CODE);
							message += ValidateUtil.validateField(value, "key.shop.customer.code", 13, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(message)) {
								c = customerMgr.getCustomerByCodeAndShop(value, s.getId());
								if (c == null) {
									message += R.getResource("key.shop.import.err.customer.not.belong.shop");
								}
							}
						}
						//Ma chu ky
						if (StringUtil.isNullOrEmpty(message) && row.size() > KH_CYCLE_CODE) {
							String value = row.get(KH_CYCLE_CODE);
							message += ValidateUtil.validateField(value, "key.shop.cycle.code", 7, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FORMAT_CYCLE_YEAR);
							if (StringUtil.isNullOrEmpty(message)) {
								String[] str = value.split("/");
								Integer num = Integer.parseInt(str[0]);
								Integer year = Integer.parseInt(str[1]);
								if (num == null || num < ConstantManager.MIN_CYCLE_NUM || num > ConstantManager.MAX_CYCLE_NUM) {
									message += R.getResource("key.shop.import.err.cycle.1.13");
								} else {
									cycle = cycleMgr.getCycleByYearAndNum(year, num);
									if (cycle == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.cycle.code");
									} else if (DateUtil.compareDateWithoutTime(sysdate, cycle.getEndDate()) > 0) {
										message += R.getResource("key.shop.import.err.cycle.before.sysdate");
									} else if (DateUtil.compareDateWithoutTime(ks.getFromCycle().getBeginDate(), cycle.getBeginDate()) > 0 || DateUtil.compareDateWithoutTime(cycle.getEndDate(), ks.getToCycle().getEndDate()) > 0) {
										message += R.getResource("key.shop.import.err.cycle.not.in.kscycle");
									}
								}
							}
						}
						if (StringUtil.isNullOrEmpty(message)) {
							//kiem tra kscustomer da ton tai chua, neu chua thi new, co roi thi lay len update
							filter = new KeyShopFilter();
							filter.setKsId(ks.getId());
							filter.setCycleId(cycle.getId());
							filter.setCustomerId(c.getId());
							ObjectVO<KSCustomer> vo = keyShopMgr.getListKSCustomerByFilter(filter);
							if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
								kc = vo.getLstObject().get(0);
								if (kc.getRewardType() != null) {
									message += R.getResource("key.shop.import.err.customer.rewarded");
								}
							} else {
								kc = new KSCustomer();
								kc.setCycle(cycle);
								kc.setKs(ks);
								kc.setCustomer(c);
								kc.setShop(s);
								kc.setCreateDate(sysdate);
								kc.setCreateUser(currentUser.getUserName());
							}
						}
						//Ma muc
						if (StringUtil.isNullOrEmpty(message) && row.size() > KH_LEVEL_CODE) {
							String value = row.get(KH_LEVEL_CODE);
							message += ValidateUtil.validateField(value, "key.shop.level.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(message)) {
								lv = keyShopMgr.getKSLevelByCode(value, ks.getKsCode());
								if (lv == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.level.code");
								}
							}
						}
						
						if (StringUtil.isNullOrEmpty(message) && kc.getId() != null) {
							//lay doi tuong kcl neu co, ko co trong DB thi new
							filter = new KeyShopFilter();
							filter.setKsCustomerId(kc.getId());
							filter.setKsLevelId(lv.getId());
							ObjectVO<KSCustomerLevel> vo = keyShopMgr.getListKSCustomerLevelByFilter(filter);
							if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
								kcl = vo.getLstObject().get(0);
								kcl.setUpdateDate(sysdate);
								kcl.setUpdateUser(currentUser.getUserName());
							} else {
								kcl = new KSCustomerLevel();
								kcl.setKsLevel(lv);
								kcl.setCreateDate(sysdate);
								kcl.setCreateUser(currentUser.getUserName());
								kcl.setKs(ks);
								kcl.setShop(s);
							}
						} else {
							kcl = new KSCustomerLevel();
							kcl.setKsLevel(lv);
							kcl.setCreateDate(sysdate);
							kcl.setCreateUser(currentUser.getUserName());
							kcl.setKs(ks);
							kcl.setShop(s);
						}
						
						boolean isInsertOrUpdate = false;
						//Boi so muc
						if (StringUtil.isNullOrEmpty(message) && row.size() > KH_MULTIPLIER) {
							String value = row.get(KH_MULTIPLIER);
							
							if (!StringUtil.isNullOrEmpty(value.trim())) {
								message += ValidateUtil.validateField(value, "key.shoo.multiplier", 6, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE);
								if (StringUtil.isNullOrEmpty(message)) {
									int multiplier = Integer.parseInt(value);
									if (multiplier < 0) {
										message += R.getResource("common.not.int", R.getResource("key.shoo.multiplier"));
//									} else if (multiplier >= 0) {
									} else {
										//Update ma gia tri moi khac gia tri cu thi cap nhat
										if(kcl.getId() != null) {
											Integer oldMultiplier = kcl.getMultiplier();
											if(oldMultiplier == null || (oldMultiplier != null && oldMultiplier.intValue() != multiplier)) {
												isInsertOrUpdate = true;
												kcl.setMultiplier(Integer.valueOf(multiplier));
											}
										} else {
											if (multiplier > 0) {
												isInsertOrUpdate = true;
												kcl.setMultiplier(multiplier);
											}
										}
										
	//									if(kcl.getId() != null) {
	//										isInsertOrUpdate = true;
	//									}
	//									if (kcl.getId() == null) {//chua ton tai khong the xoa
	//										message += R.getResource("key.shop.import.err.delete.level.customer.not.exists");
	//									} else {
	//										kcl.setStatus(ActiveType.DELETED);
	//										//kiem tra neu chi co 1 dong kcl ma con bi xoa thi se xoa luon kc
	//										filter = new KeyShopFilter();
	//										filter.setKsCustomerId(kc.getId());
	//										ObjectVO<KSCustomerLevel> vo = keyShopMgr.getListKSCustomerLevelByFilter(filter);
	//										if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() == 1 && kcl.getId().equals(vo.getLstObject().get(0).getId())) {
	//											kc.setStatus(ActiveType.DELETED);
	//											kc.setUpdateDate(sysdate);
	//											kc.setUpdateUser(currentUser.getUserName());
	//										}
	//									}
	//								} else if (multiplier > 0) {
	//									kcl.setMultiplier(multiplier);
									}
								}
							} else {
								//Update ma gia tri moi khac gia tri cu thi cap nhat
								if(kcl.getId() != null) {
									Integer oldMultiplier = kcl.getMultiplier(); 
									if(oldMultiplier != null && oldMultiplier >= 0) {
										isInsertOrUpdate = true;
										kcl.setMultiplier(null);
									}
								}
							}
						}
						if (StringUtil.isNullOrEmpty(message) && kc.getId() == null) {
							//Kiem tra tinh hop le doi voi So suat NPP
							List<ShopVO> lstShopVOKeyShop = shopMgr.getShopForConnectChildrenByKeyShopVO(ks.getId(), kc.getCycle().getId(), ActiveType.RUNNING.getValue(), currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
							for (ShopVO rowShop : lstShopVOKeyShop) {
								if (rowShop.getId().equals(c.getShop().getId())) {
									if (rowShop.getQuantityTarget() == null) {
										rowShop.setQuantityTarget(0);
									}
									if (rowShop.getQuantity() == null) {
										rowShop.setQuantity(0);
									}
									if (rowShop.getQuantityTarget() > 0 && rowShop.getQuantityTarget() < rowShop.getQuantity() + 1) {
										//Loi: So luong khach hang lon hon So suat cua NPP
										message = R.getResource("key.shop.import.err.cus.quantiy.elder.quantitytarget.param", rowShop.getQuantityTarget());
									}
								}
							}
						}
						
						if (StringUtil.isNullOrEmpty(message)) {
							if(isInsertOrUpdate == true) {
								//xu ly KC
								if (kc.getId() == null) {//create kc
									kc = commonMgr.createEntity(kc);
								} else if (ActiveType.DELETED.equals(kc.getStatus())) {
									commonMgr.updateEntity(kc);
								}
								kcl.setKsCustomer(kc);
								if (kcl.getId() != null) {
									commonMgr.updateEntity(kcl);
								} else {
									commonMgr.createEntity(kcl);
								}
								if (ActiveType.RUNNING.equals(kc.getStatus())) {
									//Xu ly ghi lich su KS_REGISTED_HIS
									ksRegisHis = new KSRegistedHis();
									ksRegisHis.setAmount(amount);
									ksRegisHis.setCustomerId(c.getId());
									ksRegisHis.setCycleId(cycle.getId());
									ksRegisHis.setKsCustomerId(kc.getId());
									ksRegisHis.setKsId(ks.getId());
									ksRegisHis.setKsLevelId(lv.getId());
									ksRegisHis.setMultiPlyer(kcl.getMultiplier());
									ksRegisHis.setUpdateDate(sysdate);
									ksRegisHis.setUpdateUser(currentUser.getUserName());
									commonMgr.createEntity(ksRegisHis);
								}
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
						//Xu ly cac bien tam
						ks = null;
						lv = null;
						s = null;
						c = null;
						cycle = null;
						kc = null;
						kcl = null;
						ksRegisHis = null;
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_KEY_SHOP_CUSTOMER_FAIL);
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.importCustomer", createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	private static Integer PRODUCT_KS_CODE = 0;
	private static Integer PRODUCT_KS_NAME = 1;
	private static Integer PRODUCT_PRODUCT_CODE = 2;
	private static Integer PRODUCT_TOTAL_COLS = 3;

	private String importProduct() {
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, PRODUCT_TOTAL_COLS);

		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				KeyShopFilter filter = new KeyShopFilter();
				Date sysdate = commonMgr.getSysDate();
				for (int i = 0; i < lstData.size(); i++) {
					List<String> row = lstData.get(i);
					if (row == null || row.size() == 0) {
						continue;
					}
					message = "";
					totalItem++;
					KS ks = null;
					Product p = null;
					//Ma CLB
					KSProduct kp = null;
					if (StringUtil.isNullOrEmpty(message) && row.size() > PRODUCT_KS_CODE) {
						String value = row.get(PRODUCT_KS_CODE);
						message += ValidateUtil.validateField(value, "key.shop.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							ks = keyShopMgr.getKSByCode(value);
							if (ks == null) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.code");
							} else if (!ActiveType.RUNNING.equals(ks.getStatus())) {
								message += R.getResource("key.shop.import.err.ks.not.running");
							}
						}
					}
					//Ma sku
					if (StringUtil.isNullOrEmpty(message) && row.size() > PRODUCT_PRODUCT_CODE) {
						String value = row.get(PRODUCT_PRODUCT_CODE);
						message += ValidateUtil.validateField(value, "key.shop.import.sku", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							p = productMgr.getProductByCode(value);
							if (p == null) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.import.sku");
								;
							} else if (!ActiveType.RUNNING.equals(p.getStatus())) {
								message += R.getResource("key.shop.import.sku.not.running");
							} else {
								filter = new KeyShopFilter();
								filter.setKsId(ks.getId());
								filter.setProductId(p.getId());
								ObjectVO<KSProduct> vo = keyShopMgr.getListKSProductByFilter(filter);
								if (vo == null || vo.getLstObject() == null || vo.getLstObject().size() == 0) {
									kp = new KSProduct();
									kp.setProduct(p);
									kp.setKs(ks);
									kp.setCreateUser(currentUser.getUserName());
									kp.setCreateDate(sysdate);
								}
							}
						}
					}

					if (StringUtil.isNullOrEmpty(message)) {
						if (kp != null && kp.getId() == null) {//chi cho them moi khong cap nhat
							commonMgr.createEntity(kp);
						}
					} else {
						lstFails.add(StringUtil.addFailBean(row, message));
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_KEY_SHOP_PRODUCT_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	private static Integer KS_KS_CODE = 0;
	private static Integer KS_KS_NAME = 1;
	private static Integer KS_TYPE = 2;
	private static Integer KS_FROM_CYCLE = 3;
	private static Integer KS_TO_CYCLE = 4;
	private static Integer KS_DESCRIPTION = 5;
	private static Integer KS_MIN_COUNT_IMAGE = 6;
	private static Integer KS_LEVEL_CODE = 7;
	private static Integer KS_LEVEL_NAME = 8;
	private static Integer KS_AMOUNT = 9;
	private static Integer KS_QUANTITY = 10;
	private static Integer KS_DELETE = 11;
	private static Integer KS_TOTAL_COLS = 12;

	private String importKS() {
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, KS_TOTAL_COLS);

		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				String typeCLB = R.getResource("key.shop.type.value.clb");
				String typeDisplay = R.getResource("key.shop.type.value.display");
				Date sysdate = commonMgr.getSysDate();
				KeyShopFilter filter = new KeyShopFilter();
				for (int i = 0; i < lstData.size(); i++) {
					List<String> row = lstData.get(i);
					if (row == null || row.size() == 0) {
						continue;
					}
					message = "";
					totalItem++;
					KS ks = null;
					KSLevel lv = null;
					//Ma CLB
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_KS_CODE) {
						String value = row.get(KS_KS_CODE);
						message += ValidateUtil.validateField(value, "key.shop.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							ks = keyShopMgr.getKSByCode(value);
							if (ks == null) {
								ks = new KS();
								ks.setKsCode(value.toUpperCase());
							} else if (!ActiveType.RUNNING.equals(ks.getStatus())) {
								message += R.getResource("key.shop.import.err.ks.not.running");
							}
						}
					}
					//Ten CLB
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_KS_NAME) {
						String value = row.get(KS_KS_NAME);
						message += ValidateUtil.validateField(value, "key.shop.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							ks.setName(value);
						}
					}
					//Loai
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_TYPE) {
						String value = row.get(KS_TYPE);
						message += ValidateUtil.validateField(value, "key.shop.type", 200, ConstantManager.ERR_REQUIRE);
						if (StringUtil.isNullOrEmpty(message)) {
							KeyShopType type = null;
							if (typeCLB.equalsIgnoreCase(value)) {
								type = KeyShopType.KEY_SHOP;
							} else if (typeDisplay.equalsIgnoreCase(value)) {
								type = KeyShopType.DISPLAY;
							} else {
								message += R.getResource("key.shop.import.err.invalid");
							}
							if (ks.getId() != null && type != null && !ks.getProgramType().equals(type)) {
								filter = new KeyShopFilter();
								filter.setKsId(ks.getId());
								if (keyShopMgr.checkExistsRewardInKS(filter)) {
									message += R.getResource("key.shop.exists.reward.ks.change.type");
								}
							}
							ks.setProgramType(type);
						}
					}
					//Tu chu ky
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_FROM_CYCLE) {
						String value = row.get(KS_FROM_CYCLE);
						message += ValidateUtil.validateField(value, "key.shop.from.cycle", 7, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FORMAT_CYCLE_YEAR);
						if (StringUtil.isNullOrEmpty(message)) {
							String[] str = value.split("/");
							Integer num = Integer.parseInt(str[0]);
							Integer year = Integer.parseInt(str[1]);
							if (num == null || num < ConstantManager.MIN_CYCLE_NUM || num > ConstantManager.MAX_CYCLE_NUM) {
								message += R.getResource("key.shop.import.err.cycle.1.13");
							} else {
								Cycle cycle = cycleMgr.getCycleByYearAndNum(year, num);
								if (cycle != null) {
									ks.setFromCycle(cycle);
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.from.cycle");
								}
							}
						}
					}
					//Den chu ky
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_TO_CYCLE) {
						String value = row.get(KS_TO_CYCLE);
						message += ValidateUtil.validateField(value, "key.shop.to.cycle", 7, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FORMAT_CYCLE_YEAR);
						if (StringUtil.isNullOrEmpty(message)) {
							String[] str = value.split("/");
							Integer num = Integer.parseInt(str[0]);
							Integer year = Integer.parseInt(str[1]);
							if (num == null || num < ConstantManager.MIN_CYCLE_NUM || num > ConstantManager.MAX_CYCLE_NUM) {
								message += R.getResource("key.shop.import.err.cycle.1.13");
							} else {
								Cycle cycle = cycleMgr.getCycleByYearAndNum(year, num);
								if (cycle != null) {
									ks.setToCycle(cycle);
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.to.cycle");
								}
							}
						}
					}
					if (StringUtil.isNullOrEmpty(message) && DateUtil.compareDateWithoutTime(ks.getFromCycle().getBeginDate(), ks.getToCycle().getBeginDate()) > 0) {
						message += R.getResource("key.shop.import.err.from.large.to");
					}
					if (StringUtil.isNullOrEmpty(message) && ks.getId() != null) {
						//check chu ky cap nhat co anh huong khach hang hay khong
						//Neu co khach hang nao da tham gia chuong trinh voi thoi gian nam ngoai time update thi loi
						filter = new KeyShopFilter();
						filter.setBeginDate(ks.getFromCycle().getBeginDate());
						filter.setEndDate(ks.getToCycle().getEndDate());
						filter.setKsId(ks.getId());
						if (keyShopMgr.checkExistsCustomerNotInKSCycle(filter)) {
							message += R.getResource("key.shop.import.err.exists.cust.without.cycle.update");
						}
					}
					//Mo ta
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_DESCRIPTION) {
						String value = row.get(KS_DESCRIPTION);
						message += ValidateUtil.validateField(value, "key.shop.description", 500, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							ks.setDescription(value);
						}
					}
					//So lan chup hinh
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_MIN_COUNT_IMAGE) {
						String value = row.get(KS_MIN_COUNT_IMAGE);
						if (!StringUtil.isNullOrEmpty(value.trim())) {
							message += ValidateUtil.validateField(value, "key.shop.min.count.image", 3, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(message)) {
								int minCount = Integer.parseInt(value);
								if (minCount < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.min.count.image"));
								} else {
									ks.setMinPhotoNum(Integer.parseInt(value));
								}
							}
						}
					}
					//Ma muc
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_LEVEL_CODE) {
						String value = row.get(KS_LEVEL_CODE);
						message += ValidateUtil.validateField(value, "key.shop.level.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							lv = keyShopMgr.getKSLevelByCode(value, ks.getKsCode());
							if (lv == null) {
								lv = new KSLevel();
								lv.setKsLevelCode(value.toUpperCase());
							}
						}
					}
					//Ten muc
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_LEVEL_NAME) {
						String value = row.get(KS_LEVEL_NAME);
						message += ValidateUtil.validateField(value, "key.shop.level.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(message)) {
							lv.setName(value);
							lv.setNameText(Unicode2English.codau2khongdau(value).toUpperCase());
						}
					}
					//So tien
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_AMOUNT) {
						String value = row.get(KS_AMOUNT);
						message += ValidateUtil.validateField(value, "key.shop.amount", 26, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE);
						if (StringUtil.isNullOrEmpty(message)) {
							BigDecimal amount = new BigDecimal(value);
							if (amount.longValue() < 0) {
								message += R.getResource("common.not.int", R.getResource("key.shop.amount"));
							} else {
								lv.setAmount(amount);
							}
						}
					}
					//So luong
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_QUANTITY) {
						String value = row.get(KS_QUANTITY);
						if (!StringUtil.isNullOrEmpty(value.trim())) {
							message += ValidateUtil.validateField(value, "key.shop.quantity", 20, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(message)) {
								Integer quantity = Integer.parseInt(value);
								if (quantity < 0) {
									message += R.getResource("common.not.int", R.getResource("key.shop.quantity"));
								} else {
									lv.setQuantity(quantity);
								}
							}
						}
					}
					//Xoa muc
					if (StringUtil.isNullOrEmpty(message) && row.size() > KS_DELETE) {
						String value = row.get(KS_DELETE);
						if ("X".equalsIgnoreCase(value)) {
							if (lv.getId() != null) {
								filter = new KeyShopFilter();
								filter.setKsLevelId(lv.getId());
								if (keyShopMgr.checkExistsCustomerNotInKSLevel(filter)) {
									message += R.getResource("key.shop.import.err.delete.level.exists.cust");
								} else {
									lv.setStatus(ActiveType.DELETED);
									lv.setUpdateUser(currentUser.getUserName());
									lv.setUpdateDate(sysdate);
								}
							} else {
								message += R.getResource("key.shop.import.err.create.level.not.in.db");
							}
						}
					}

					if (StringUtil.isNullOrEmpty(message)) {
						if (lv.getId() != null) {
							lv.setUpdateDate(sysdate);
							lv.setUpdateUser(currentUser.getUserName());
						} else {
							lv.setCreateDate(sysdate);
							lv.setCreateUser(currentUser.getUserName());
						}
						if (ks.getId() == null) {//create
							ks = keyShopMgr.createKS(ks, getLogInfoVO());
							lv.setKs(ks);
							commonMgr.createEntity(lv);
						} else {//update ks
							keyShopMgr.updateKS(ks, getLogInfoVO());
							lv.setKs(ks);
							if (lv.getId() == null) {//create level
								commonMgr.createEntity(lv);
							} else {//update level
								commonMgr.updateEntity(lv);
							}
						}
					} else {
						lstFails.add(StringUtil.addFailBean(row, message));
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_KEY_SHOP_KS_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * load cay don vi phan quyen du lieu
	 * 
	 * @author tuannd20
	 * @return
	 * @since 06/04/2015
	 */
	public String searchKSShop() {
		searchUnitTree = buildSearchUnitTree();
		result = new HashMap<String, Object>();
		result.put("rows", searchUnitTree);
		return JSON;
	}

	/**
	 * build cay phan quyen
	 * 
	 * @author tuannd20
	 * @param isOrgAccessTree
	 *            true: cay don vi phan quyen. false: cay don vi loai tru
	 * @return
	 * @since 06/04/2015
	 */
	public List<TreeNode> buildSearchUnitTree() {
		TreeUtility treeUtility = new TreeUtility(shopMgr);
		ShopFilter shopFilter = new ShopFilter();
		shopFilter.setStatus(ActiveType.RUNNING);
		shopFilter.setShopId(shopId);
		shopFilter.setStrShopId(getStrListShopId());
		shopFilter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
		shopFilter.setRoleId(currentUser.getRoleToken().getRoleId());
		shopFilter.setShopRootId(currentUser.getShopRoot().getShopId());
		List<TreeNode> tempTree = treeUtility.buildUnitTreeForDataPermission(ksId, check, shopFilter, currentUser.getShopRoot().getShopId());
		if (tempTree != null) {
			TreeNode root = tempTree.get(0);
			expandTree(root);
		}
		return tempTree;
	}

	/**
	 * expand tree
	 * 
	 * @author longnh15
	 * @param node
	 * @return Cay duoc expand het cac muc
	 * @since 01/08/2015
	 */
	private boolean expandTree(TreeNode node) {
		if (node != null) {
			Map<String, Object> attributes = node.getAttributes();
			if (attributes != null && attributes.containsKey("selected") && attributes.get("selected") instanceof Boolean) {
				if ((Boolean) attributes.get("selected")) {
					node.setState("open");
					return true;
				}
			}
			if (node.getChildren() != null || node.getChildren().size() > 0) {
				boolean expand = false;
				for (Integer i = 0, n = node.getChildren().size(); i < n; i++) {
					if (expandTree(node.getChildren().get(i))) {
						expand = true;
					}
				}
				if (expand) {
					node.setState("open");
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Cap nhat quyen voi tham so voi danh sach su kien tuong ung
	 * 
	 * @author longnh15 merge
	 * @since JUNE 27,2014
	 * @description Update in Key Shop Map
	 * 
	 * @modify hunglm16
	 * @since 18/11/2015
	 * @description Bo sung so suat
	 * */
	@Transactional
	public String updateKSShop() {
		resetToken(result);
		errMsg = "";
		KeyShopFilter filter = new KeyShopFilter();
		String userName = currentUser.getUserName();
		try {
			if (ksId == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.error.not.exist.keyshop"));
				return JSON;
			}
			KS ks = keyShopMgr.getKSById(ksId);
			if (ks == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.error.not.exist.keyshop"));
				return JSON;
			}
			//Kiem tra tinh dung dan du lieu voi So suat
			Map<Long, Integer> mapShopTagert = new HashMap<Long, Integer>();
			if (!StringUtil.isNullOrEmpty(arrOjectText)) {
				List<ShopVO> lstShopVOKeyShop = shopMgr.getShopForConnectChildrenByKeyShopVO(ks.getId(), null, ActiveType.RUNNING.getValue(), currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
				for (String cellItem : arrOjectText.split(",")) {
					String[] shopMapTarget = cellItem.split(";");
					if (shopMapTarget.length != 2 || !ValidateUtil.validateNumber(shopMapTarget[0]) || !ValidateUtil.validateNumber(shopMapTarget[1])) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("keyshop.error.change.quantity.param"));
						return JSON;
					}
					Long shopId = Long.valueOf(shopMapTarget[0]);
					Integer quantityShop = Integer.valueOf(shopMapTarget[1]);
					for (ShopVO shopTg: lstShopVOKeyShop) {
						if (shopId.equals(shopTg.getId())) {
							//Kiem tra tinh dung dan du lieu
							if (shopTg.getQuantity() == null) {
								shopTg.setQuantity(0);
							}
							if (quantityShop.intValue() < shopTg.getQuantity().intValue()) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("keyshop.error.change.shop.quantity.error.min", shopTg.getShopName(), shopTg.getQuantity().intValue()));
								return JSON;
							}
						}
					}
					mapShopTagert.put(shopId, Integer.valueOf(shopMapTarget[1]));
				}
			}
			Date sysdate = commonMgr.getSysDate();
//			List<Shop> lstChildShop = new ArrayList<Shop>(); 
//			if (lstLong != null && !lstLong.isEmpty()) {
//				int size = lstLong.size();
//				Long [] arrPshopId = new Long[size];
//				for (int i = 0; i < size; i++) {
//					arrPshopId[i] = lstLong.get(i);
//				}
//				lstChildShop = shopMgr.getListChildShopId(ActiveType.RUNNING.getValue(), arrPshopId);
//			}
			if (arrLong != null && !arrLong.isEmpty()) {
				for (int i = 0; i < arrLong.size(); i++) {
					Shop s = null;
					Long ksmShopId = arrLong.get(i);
					s = shopMgr.getShopById(ksmShopId);
					if (s == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.error.not.exist.keyshop"));
						return JSON;
					}
					filter = new KeyShopFilter();
					filter.setShopId(s.getId());
					filter.setKsId(ks.getId());
					filter.setUpdateUser(currentUser.getUserName());
					//Xu ly truong hop shop cha duoc lua chon
					List<Shop> lstParentShop = new ArrayList<Shop>(); 
					lstParentShop = shopMgr.getListParentShopId(ActiveType.RUNNING.getValue(), s.getId());
					
					List<Shop> lstChildShop = new ArrayList<Shop>(); 
					lstChildShop = shopMgr.getListChildShopId(ActiveType.RUNNING.getValue(), s.getId());
					
					boolean flagDelete = true;
					//Kiem tra cac shop cha co check hay ko?
					for (Shop shopParent: lstParentShop) {
						if (lstLong != null && !lstLong.isEmpty()) {
							for (Long shopId : lstLong) {
								if (shopParent.getId().equals(shopId)) {
									flagDelete = false;
									break;
								}
							}
						}
						
					}
					
					List<Shop> lstChildShopNotInsert = new ArrayList<>();
					if (lstLong != null && !lstLong.isEmpty()) {
						for (Shop shopChild: lstChildShop) {
							boolean isExist = false;

							for (Long shopId : lstLong) {
								if (shopChild.getId().equals(shopId)) {
//									flagDelete = false;
									isExist = true;
									break;
								}
							}

							if(!isExist) {
								lstChildShopNotInsert.add(shopChild);
							}
						}
					} else {
						lstChildShopNotInsert.addAll(lstChildShop);
					}
					
					//Shop khong duoc check
					if (flagDelete) {
						filter.setShopId(s.getId());
						if (keyShopMgr.checkKSCustomerExistsShopDel(filter)) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.error.exists.customer"));
							return JSON;
						} 		
						
						//Kiem tra co customer duyet trong ks_customer cua cac shop KHONG duoc check -> Co thi loi
						if(lstChildShopNotInsert.size() > 0) {
							for (Shop shopChild: lstChildShopNotInsert) {
								filter.setShopId(shopChild.getId());
								if (keyShopMgr.checkKSCustomerExistsShopDel(filter)) {
									result.put(ERROR, true);
									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.error.exists.customer"));
									return JSON;
								} 	
							}
						}
					}
					//xoa cac shop con
					filter.setIsGetChild(true);
					keyShopMgr.removeShopMapByShop(filter);
					//xoa cac shop cha
					filter.setIsGetChild(false);
					keyShopMgr.removeShopMapByShop(filter);
				}
			}
			if (lstLong != null && !lstLong.isEmpty()) {
				for (int i = 0; i < lstLong.size(); i++) {
					Shop s = null;
					Long ksmShopId = lstLong.get(i);
					s = shopMgr.getShopById(ksmShopId);

					if (s == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "keyshop.error.not.exist.keyshop"));
						return JSON;
					} else {
						filter = new KeyShopFilter();
						filter.setShopId(s.getId());
						filter.setKsId(ks.getId());
						filter.setUpdateUser(userName);

						//xoa cac shop con
						filter.setIsGetChild(true);
						keyShopMgr.removeShopMapByShop(filter);

						//xoa cac shop cha
						filter.setIsGetChild(false);
						keyShopMgr.removeShopMapByShop(filter);

						//them dong vao shop_map
						KSShopMap ksm = new KSShopMap();
						ksm.setKs(ks);
						ksm.setShop(s);
						ksm.setCreateDate(sysdate);
						//ksm.setUpdateDate(sysdate);
						ksm.setCreateUser(currentUser.getUserName());
						//ksm.setUpdateUser(currentUser.getUserName());
						ksm.setStatus(ActiveType.RUNNING);
						commonMgr.createEntity(ksm);
					}
				}
			}
			//Thuc hien xoa tat ca cac du lieu KS_SHOP_TARGET voi keyShop
			shopMgr.updateKSShopTagertByKeyShop(ks.getId(), currentUser.getUserName());
			//Kiem tra tinh dung dan du lieu
			List<Shop> lstShopKeyMap = shopMgr.getShopForConnectChildrenByKeyShop(ks.getId(), ActiveType.RUNNING.getValue());
			if (lstShopKeyMap != null && !lstShopKeyMap.isEmpty() && mapShopTagert != null && mapShopTagert.size() > 0) {
				//Thuc hien them du lieu So suat
				for (Entry<Long, Integer> entry : mapShopTagert.entrySet()) {
					Long shopId = entry.getKey();
					for (Shop shopTarg : lstShopKeyMap) {
						if (shopTarg.getId().equals(shopId)) {
							KSShopTarget ksTargetEtt = new KSShopTarget();
							ksTargetEtt.setKs(ks);
							ksTargetEtt.setShop(shopTarg);
							ksTargetEtt.setQuantity(entry.getValue());
							ksTargetEtt.setStatus(StatusType.ACTIVE.getValue());
							ksTargetEtt.setCreateDate(sysdate);
							ksTargetEtt.setCreateUser(currentUser.getUserName());
							commonMgr.createEntity(ksTargetEtt);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.updateKSShop", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * Cap nhat, them moi khach hang vao CTHTTM
	 * @return JSON
	 * 
	 * @modify hunglm16
	 * @since 20/11/2015
	 * @description validate So suat NPP
	 */
	@Transactional
	public String updateKSCustomerLevel() {
		resetToken(result);
		errMsg = "";
		String message = "";
		try {
			KeyShopFilter filter = new KeyShopFilter();
			Date sysdate = commonMgr.getSysDate();
			KS ks = null;
			KSLevel lv = null;
			Customer c = null;
			Cycle cycle = null;
			KSCustomer kc = null;
			//Ma CLB
			if (StringUtil.isNullOrEmpty(message)) {
				ks = keyShopMgr.getKSById(ksId);
				if (ks == null) {
					message = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.code");
				}
			}
			//Ma KH
			if (StringUtil.isNullOrEmpty(message)) {
				c = customerMgr.getCustomerById(customerId);
				if (c == null || c.getShop() == null) {
					message = R.getResource("key.shop.import.err.customer.not.belong.shop");
				}
			}
			//Ma chu ky
			if (StringUtil.isNullOrEmpty(message)) {
				cycle = cycleMgr.getCycleById(cycleId);
				if (cycle == null) {
					message = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.cycle.code");
				} else if (DateUtil.compareDateWithoutTime(sysdate, cycle.getEndDate()) > 0) {
					message = R.getResource("key.shop.import.err.cycle.before.sysdate");
				} else if (DateUtil.compareDateWithoutTime(ks.getFromCycle().getBeginDate(), cycle.getBeginDate()) > 0 || DateUtil.compareDateWithoutTime(cycle.getEndDate(), ks.getToCycle().getEndDate()) > 0) {
					message = R.getResource("key.shop.import.err.cycle.not.in.kscycle");
				}
			}
			if (StringUtil.isNullOrEmpty(message)) {
				//kiem tra kscustomer da ton tai chua, neu chua thi new, co roi thi lay len update
				filter = new KeyShopFilter();
				filter.setKsId(ks.getId());
				filter.setCycleId(cycle.getId());
				filter.setCustomerId(c.getId());
				ObjectVO<KSCustomer> vo = keyShopMgr.getListKSCustomerByFilter(filter);
				if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
					kc = vo.getLstObject().get(0);
					if (kc.getRewardType() != null) {
						message = R.getResource("key.shop.import.err.customer.rewarded");
					}
				} else {
					//Kiem tra tinh dung dan du lieu So suat khi them khach hang voi CTHTTM
					List<ShopVO> lstShopVOKeyShop = shopMgr.getShopForConnectChildrenByKeyShopVO(ks.getId(), cycle.getId(), ActiveType.RUNNING.getValue(), currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
					for (ShopVO row : lstShopVOKeyShop) {
						if (row.getId().equals(c.getShop().getId())) {
							if (row.getQuantityTarget() == null) {
								row.setQuantityTarget(0);
							}
							if (row.getQuantity() == null) {
								row.setQuantity(0);
							}
							if (row.getQuantityTarget() > 0 && row.getQuantityTarget() < row.getQuantity() + 1) {
								//Loi: So luong khach hang lon hon So suat cua NPP
								message = R.getResource("key.shop.import.err.cus.quantiy.elder.quantitytarget.param", row.getQuantityTarget());
								result.put(ERROR, true);
								result.put("errMsg", message);
								return JSON;
							}
						}
					}
					kc = new KSCustomer();
					kc.setCycle(cycle);
					kc.setKs(ks);
					kc.setCustomer(c);
					kc.setShop(c.getShop());
					kc.setCreateDate(sysdate);
					kc.setCreateUser(currentUser.getUserName());
					kc.setStatus(ActiveType.RUNNING);
					kc = commonMgr.createEntity(kc);
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", message);
				return JSON;
			}
			
			if (listKsLevelId != null && !listKsLevelId.isEmpty() && listMultiplier != null && !listMultiplier.isEmpty() && listKsLevelId.size() == listMultiplier.size()) {
				KSCustomerLevel kcl = null;
				KSRegistedHis ksRegisHis = null;
				for (int i = 0, sizelv = listKsLevelId.size(); i < sizelv; i++) {
					//Ma muc
					if (StringUtil.isNullOrEmpty(message)) {
						lv = keyShopMgr.getKSLevelById(listKsLevelId.get(i));
						if (lv == null) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.level.code");
						}
					}
					//Boi so muc
					Integer multiplier = listMultiplier.get(i);
					
					boolean isInsertHis = false;
					
					if (StringUtil.isNullOrEmpty(message) && kc.getId() != null) {
						//lay doi tuong kcl neu co, ko co trong DB thi new
						filter = new KeyShopFilter();
						filter.setKsCustomerId(kc.getId());
						filter.setKsLevelId(lv.getId());
						ObjectVO<KSCustomerLevel> vo = keyShopMgr.getListKSCustomerLevelByFilter(filter);
						if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
							kcl = vo.getLstObject().get(0);
							Integer oldMultiplier = kcl.getMultiplier();
							if((multiplier == null && oldMultiplier != null) || (multiplier != null && !multiplier.equals(oldMultiplier))) {
								isInsertHis = true;
								kcl.setUpdateDate(sysdate);
								kcl.setUpdateUser(currentUser.getUserName());
								kcl.setMultiplier(multiplier);
								commonMgr.updateEntity(kcl);
							}
						} else {
							if(multiplier != null && multiplier.intValue() > 0) {
								isInsertHis = true;
								kcl = new KSCustomerLevel();
								kcl.setKsCustomer(kc);
								kcl.setKsLevel(lv);
								kcl.setCreateDate(sysdate);
								kcl.setCreateUser(currentUser.getUserName());
								kcl.setMultiplier(multiplier);
								kcl.setStatus(ActiveType.RUNNING);
								kcl.setKs(ks);
								kcl.setShop(c.getShop());
								commonMgr.createEntity(kcl);
							}
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", message);
						return JSON;
					}
					if (isInsertHis && kc.getStatus() != null && ActiveType.RUNNING.getValue().equals(kc.getStatus().getValue())) {
						//Xu ly ghi lich su KS_REGISTED_HIS
						ksRegisHis = new KSRegistedHis();
						ksRegisHis.setAmount(amount);
						ksRegisHis.setCustomerId(c.getId());
						ksRegisHis.setCycleId(cycle.getId());
						ksRegisHis.setKsCustomerId(kc.getId());
						ksRegisHis.setKsId(ks.getId());
						ksRegisHis.setKsLevelId(lv.getId());
						ksRegisHis.setMultiPlyer(multiplier);
						ksRegisHis.setUpdateDate(sysdate);
						ksRegisHis.setUpdateUser(currentUser.getUserName());
						commonMgr.createEntity(ksRegisHis);
					}
				}
			}
			
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.updateKSCustomerLevel", createLogErrorStandard(actionStartTime));
			result.put("errMsg", message);
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	@Transactional
	public String processKSCustomerLevel() {
		resetToken(result);
		errMsg = "";
		String message = "";
		try {
			KeyShopFilter filter = new KeyShopFilter();
			KeyShopFilter filterLevel = new KeyShopFilter();
			Date sysdate = commonMgr.getSysDate();

			KS ks = null;
			Customer c = null;
			Cycle cycle = null;
			KSCustomer kc = null;
			KSRegistedHis ksRegisHis = null;

			//Ma CLB
			if (StringUtil.isNullOrEmpty(message)) {
				ks = keyShopMgr.getKSById(ksId);
				if (ks == null) {
					message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.code");
				}
			}
			//Ma KH
			if (StringUtil.isNullOrEmpty(message)) {
				c = customerMgr.getCustomerById(customerId);
				if (c == null) {
					message += R.getResource("key.shop.import.err.customer.not.belong.shop");
				}
			}
			//Ma chu ky
			if (StringUtil.isNullOrEmpty(message)) {
				cycle = cycleMgr.getCycleById(cycleId);
				if (cycle == null) {
					message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "key.shop.cycle.code");
				} else if (DateUtil.compareDateWithoutTime(sysdate, cycle.getEndDate()) > 0) {
					message += R.getResource("key.shop.import.err.cycle.before.sysdate");
				} else if (DateUtil.compareDateWithoutTime(ks.getFromCycle().getBeginDate(), cycle.getBeginDate()) > 0 || DateUtil.compareDateWithoutTime(cycle.getEndDate(), ks.getToCycle().getEndDate()) > 0) {
					message += R.getResource("key.shop.import.err.cycle.not.in.kscycle");
				}
			}
			if (StringUtil.isNullOrEmpty(message) && ActiveType.RUNNING.getValue().equals(processType)) {
				//Kiem tra tinh hop le doi voi So suat NPP
				List<ShopVO> lstShopVOKeyShop = shopMgr.getShopForConnectChildrenByKeyShopVO(ks.getId(), cycle.getId(), ActiveType.RUNNING.getValue(), currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
				for (ShopVO row : lstShopVOKeyShop) {
					if (row.getId().equals(c.getShop().getId())) {
						if (row.getQuantityTarget() == null) {
							row.setQuantityTarget(0);
						}
						if (row.getQuantity() == null) {
							row.setQuantity(0);
						}
						if (row.getQuantityTarget() > 0 && row.getQuantityTarget() < row.getQuantity() + 1) {
							//Loi: So luong khach hang lon hon So suat cua NPP
							message = R.getResource("key.shop.import.err.cus.quantiy.elder.quantitytarget.param", row.getQuantityTarget());
							result.put(ERROR, true);
							result.put("errMsg", message);
							return JSON;
						}
					}
				}
			}
			
			if (StringUtil.isNullOrEmpty(message)) {
				//kiem tra kscustomer da ton tai chua, neu chua thi new, co roi thi lay len update
				filter = new KeyShopFilter();
				filter.setKsId(ks.getId());
				filter.setCycleId(cycle.getId());
				filter.setCustomerId(c.getId());
				if (processType == 1)
					filter.setStatus(2);
				if (processType == 3)
					filter.setStatus(2);
				ObjectVO<KSCustomer> vo = keyShopMgr.getListKSCustomerByFilter(filter);
				if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
					kc = vo.getLstObject().get(0);
					if (kc.getRewardType() != null) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("key.shop.import.err.customer.rewarded"));
						return JSON;
					}
					kc.setUpdateDate(sysdate);
					kc.setUpdateUser(currentUser.getUserName());
					if (ActiveType.DELETED.getValue().equals(processType)) {
						kc.setStatus(ActiveType.DELETED);
					} else if (ActiveType.RUNNING.getValue().equals(processType)) {
						kc.setStatus(ActiveType.RUNNING);								
					} else if (ActiveType.REJECTED.getValue().equals(processType)) {
						kc.setStatus(ActiveType.REJECTED);
					}
					commonMgr.updateEntity(kc);

					filterLevel = new KeyShopFilter();
					filterLevel.setKsCustomerId(kc.getId());
					if (processType == -1) {
						filterLevel.setStatus(1);
					} else if (processType == 1) {
						filterLevel.setStatus(1);
					}else if (processType == 3) {
						filterLevel.setStatus(1);
					}
					ObjectVO<KSCustomerLevel> voLevel = keyShopMgr.getListKSCustomerLevelByFilter(filterLevel);
					if (voLevel != null && voLevel.getLstObject() != null && voLevel.getLstObject().size() > 0) {
						for (KSCustomerLevel kscl : voLevel.getLstObject()) {
							kscl.setUpdateDate(sysdate);
							kscl.setUpdateUser(currentUser.getUserName());
							if (ActiveType.DELETED.getValue().equals(processType)) {
								kscl.setStatus(ActiveType.DELETED);
							} else if (ActiveType.RUNNING.getValue().equals(processType)) {
								kscl.setStatus(ActiveType.RUNNING);								
							} else if (ActiveType.REJECTED.getValue().equals(processType)) {
								kscl.setStatus(ActiveType.RUNNING);
							}
							commonMgr.updateEntity(kscl);
							
							//Duyet KH tham gia keyshop thi luu lich su
							if (ActiveType.RUNNING.getValue().equals(processType)) {
								//Xu ly ghi lich su KS_REGISTED_HIS
								ksRegisHis = new KSRegistedHis();
								ksRegisHis.setAmount(amount);
								ksRegisHis.setCustomerId(c.getId());
								ksRegisHis.setCycleId(cycle.getId());
								ksRegisHis.setKsCustomerId(kc.getId());
								ksRegisHis.setKsId(ks.getId());
								ksRegisHis.setKsLevelId(kscl.getKsLevel().getId());
								ksRegisHis.setMultiPlyer(kscl.getMultiplier());
								ksRegisHis.setUpdateDate(sysdate);
								ksRegisHis.setUpdateUser(currentUser.getUserName());
								commonMgr.createEntity(ksRegisHis);
							}
						}
					}
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", message);
				return JSON;
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, "ths.dms.web.action.program.KeyShopAction.processKSCustomerLevel", createLogErrorStandard(actionStartTime));
			result.put("errMsg", message);
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * Cac phuong thuc GETTER/SETTER
	 */
	public List<Integer> getLstYear() {
		return lstYear;
	}

	public void setLstYear(List<Integer> lstYear) {
		this.lstYear = lstYear;
	}

	public List<Integer> getLstCycle() {
		return lstCycle;
	}

	public void setLstCycle(List<Integer> lstCycle) {
		this.lstCycle = lstCycle;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getKsId() {
		return ksId;
	}

	public void setKsId(Long ksId) {
		this.ksId = ksId;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getExcelType() {
		return excelType;
	}

	public void setExcelType(Integer excelType) {
		this.excelType = excelType;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Integer getKsType() {
		return ksType;
	}

	public void setKsType(Integer ksType) {
		this.ksType = ksType;
	}

	public List<CycleVO> getLstCycleVO() {
		return lstCycleVO;
	}

	public void setLstCycleVO(List<CycleVO> lstCycleVO) {
		this.lstCycleVO = lstCycleVO;
	}

	public String getKsCode() {
		return ksCode;
	}

	public void setKsCode(String ksCode) {
		this.ksCode = ksCode;
	}

	public String getKsName() {
		return ksName;
	}

	public void setKsName(String ksName) {
		this.ksName = ksName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getFromCycleId() {
		return fromCycleId;
	}

	public void setFromCycleId(Long fromCycleId) {
		this.fromCycleId = fromCycleId;
	}

	public Long getToCycleId() {
		return toCycleId;
	}

	public void setToCycleId(Long toCycleId) {
		this.toCycleId = toCycleId;
	}

	public Integer getMinPhoto() {
		return minPhoto;
	}

	public void setMinPhoto(Integer minPhoto) {
		this.minPhoto = minPhoto;
	}

	public List<Long> getLstProductInfoId() {
		return lstProductInfoId;
	}

	public void setLstProductInfoId(List<Long> lstProductInfoId) {
		this.lstProductInfoId = lstProductInfoId;
	}

	public Long getProductInfoId() {
		return productInfoId;
	}

	public void setProductInfoId(Long productInfoId) {
		this.productInfoId = productInfoId;
	}

	public Boolean getIsOrderByCode() {
		return isOrderByCode;
	}

	public void setIsOrderByCode(Boolean isOrderByCode) {
		this.isOrderByCode = isOrderByCode;
	}

	public List<Long> getLstProductInfoChildId() {
		return lstProductInfoChildId;
	}

	public void setLstProductInfoChildId(List<Long> lstProductInfoChildId) {
		this.lstProductInfoChildId = lstProductInfoChildId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Long> getLstProductId() {
		return lstProductId;
	}

	public void setLstProductId(List<Long> lstProductId) {
		this.lstProductId = lstProductId;
	}

	public Long getKsLevelId() {
		return ksLevelId;
	}

	public void setKsLevelId(Long ksLevelId) {
		this.ksLevelId = ksLevelId;
	}

	public String getKsLevelCode() {
		return ksLevelCode;
	}

	public void setKsLevelCode(String ksLevelCode) {
		this.ksLevelCode = ksLevelCode;
	}

	public String getKsLevelName() {
		return ksLevelName;
	}

	public void setKsLevelName(String ksLevelName) {
		this.ksLevelName = ksLevelName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getCurrentCycleId() {
		return currentCycleId;
	}

	public void setCurrentCycleId(Long currentCycleId) {
		this.currentCycleId = currentCycleId;
	}

	public Long getLastCycleId() {
		return lastCycleId;
	}

	public void setLastCycleId(Long lastCycleId) {
		this.lastCycleId = lastCycleId;
	}

	public List<TreeNode> getSearchUnitTree() {
		return searchUnitTree;
	}

	public void setSearchUnitTree(List<TreeNode> searchUnitTree) {
		this.searchUnitTree = searchUnitTree;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public List<Long> getLstLong() {
		return lstLong;
	}

	public void setLstLong(List<Long> lstLong) {
		this.lstLong = lstLong;
	}

	public List<Long> getArrLong() {
		return arrLong;
	}

	public void setArrLong(List<Long> arrLong) {
		this.arrLong = arrLong;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public List<Long> getListKsLevelId() {
		return listKsLevelId;
	}

	public void setListKsLevelId(List<Long> listKsLevelId) {
		this.listKsLevelId = listKsLevelId;
	}

	public List<Integer> getListMultiplier() {
		return listMultiplier;
	}

	public void setListMultiplier(List<Integer> listMultiplier) {
		this.listMultiplier = listMultiplier;
	}

	public Integer getProcessType() {
		return processType;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	public Integer getResultReward() {
		return resultReward;
	}

	public void setResultReward(Integer resultReward) {
		this.resultReward = resultReward;
	}

	public List<Long> getLstKsCustomerId() {
		return lstKsCustomerId;
	}

	public void setLstKsCustomerId(List<Long> lstKsCustomerId) {
		this.lstKsCustomerId = lstKsCustomerId;
	}

	public int getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(int lockStatus) {
		this.lockStatus = lockStatus;
	}

	public String getArrOjectText() {
		return arrOjectText;
	}

	public void setArrOjectText(String arrOjectText) {
		this.arrOjectText = arrOjectText;
	}

	public Integer getFlagChange() {
		return flagChange;
	}

	public void setFlagChange(Integer flagChange) {
		this.flagChange = flagChange;
	}

	public String getChangeDateStr() {
		return changeDateStr;
	}

	public void setChangeDateStr(String changeDateStr) {
		this.changeDateStr = changeDateStr;
	}

}

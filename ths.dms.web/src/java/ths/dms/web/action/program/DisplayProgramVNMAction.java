package ths.dms.web.action.program;

import java.util.ArrayList;
import java.util.Date;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.DisplayProgramVNMMgr;
import ths.dms.core.entities.DisplayProgramVNM;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;

public class DisplayProgramVNMAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private Integer status;
	private String fromDate;
	private String toDate;
	private String code;
	private String name;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
	}

	/*DS CTTB tra thuong*/
	@Override
	public String execute() throws Exception {
		try {
			resetToken(result);
		} catch (Exception ex) {
			LogUtility.logError(ex, "DisplayProgramVNMAction" + ex.getMessage());
		}
		return SUCCESS;
	}
	
	public String searchVNM() throws Exception {
	    result.put("page", page);
	    result.put("max", max);
	    try{
	    	Shop shop = null;
	    	Long shId = null;
			if (currentUser != null && currentUser.getUserName() != null) {
				ShopToken shToken = currentUser.getShopRoot();
				if (shToken != null) {
					shop = shopMgr.getShopById(shToken.getShopId());
					if (shop != null) {
						shId = shop.getId();
						shopCode = shop.getShopCode();
					}
				}
			}
			
			KPaging<DisplayProgramVNM> kPaging = new KPaging<DisplayProgramVNM>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);		
			Date froDate = null;
			Date tDate = null;
			if(!StringUtil.isNullOrEmpty(fromDate)){
				froDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);			
			}
			if((!StringUtil.isNullOrEmpty(fromDate) && froDate == null) || (!StringUtil.isNullOrEmpty(toDate) && tDate==null)){
				 result.put("total", null);
				 result.put("rows",null);
			} else {
				ActiveType statusCTTBVNM = null;
				if(status != null && status > -1) {
					if(status == 1) {
						statusCTTBVNM = ActiveType.RUNNING;
					} else {
						statusCTTBVNM = ActiveType.STOPPED;
					}
				}
				DisplayProgramVNMMgr displayProgramVNMMgr = (DisplayProgramVNMMgr)context.getBean("displayProgramVNMMgr");
				ObjectVO<DisplayProgramVNM> displayProgramVNMVO = displayProgramVNMMgr.getListDisplayProgramVNM(kPaging, shId, code, name, statusCTTBVNM, froDate, tDate);		
				
				if(displayProgramVNMVO!= null){
				    result.put("total", displayProgramVNMVO.getkPaging().getTotalRows());
				    result.put("rows", displayProgramVNMVO.getLstObject());		
				} else {
					result.put("total", 0);
				    result.put("rows", new ArrayList<DisplayProgramVNM>());
				}
			}			    		
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
/*
 * 
 */
package ths.dms.web.action.program;


import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableWorkbook;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.JsPromotionShopTreeNode;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.enumtype.VSARole;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelJXLSAPIUtils;
import viettel.passport.client.ShopToken;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DisplayProgramVNMMgr;
import ths.dms.core.business.DisplayProgrameMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.CustomerDisplayPrograme;
import ths.dms.core.entities.DisplayShopMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StDisplayPdGroup;
import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayPlAmtDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.StDisplayProgramExclusion;
import ths.dms.core.entities.StDisplayProgramLevel;
import ths.dms.core.entities.StDisplayProgramVNM;
import ths.dms.core.entities.StDisplayStaffMap;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.DisplayProgramExclusionFilter;
import ths.dms.core.entities.enumtype.DisplayProgramFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.CustomerDisplayProgrameVO;
import ths.dms.core.entities.vo.NewTreeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.ExceptionCode;

/**
 * The Class ProgrammeDisplayCatalogAction.
 */
public class ProgrammeDisplayCatalogAction extends AbstractAction {

	private static final long serialVersionUID = -3011089933555994527L;
	
	private CustomerMgr customerMgr;
	private DisplayProgrameMgr displayProgramMgr;
	private DisplayProgramVNMMgr displayProgramVNMMgr;
	private ProductMgr productMgr;
	
	private Integer year;
	private Boolean isUpdate;
	private Boolean flag;
	private Boolean flagCheckAll;
	private Long productId;
	private String code;
	private Integer convfact;
	private List<Long> lstProductId;
	private List<Long> lstId;
	private String month;
	private List<Integer> lstConvfact;
	private Long groupDtlId;
	private List<Long> groupDtlIdLst;
	private List<String> lstGroupProductLv;
	private String lstIdStr;
	private static final int IMPORT_SHOP = 6;
	private static final int IMPORT_EXCEL_DISPLAY_PROGRAM = 1;
	private int excelType;
	private String strShopIds;
	private File excelFile;
	private File excelFileCus;
	private String excelFileContentType;
	private String importProgram;
	private Long currentShopId;
	private List<StDisplayProgram> listDisplayProgram;
	private String displayProgramCode;
	private Integer quantityLv;
	private String productCode;
	private String productName;
	private Date sysDate;
	private Boolean isCheckAll;
	private List<Long> lstIdStaffMap;
	private List<Long> lstCustomerId;
	private Integer isOutOfDate;
	private ActiveType statusCTTBVNM;
	private String name;
	private Integer status;
	private String fromDate;
	private String toDate;
	private Long id;
	private Integer amountType;
	private String displayProgramExclusion;
	private Integer htad;
	private int quotaStatus;
	private int levelStatus;
	private boolean devIsUpdate;
	private List<ApParam> lstDisplayProgramType;
	private String toDateActionLog;
	private String fromDateActionLog;
	private List<StatusBean> st;
	private Integer typeGroup;
	private Long groupId;
	private String groupCode;
	private String groupName;
	private boolean permissionEdit;
	private boolean permissionUser;
	private boolean permission;
	private List<Long> listShopId;
	private String listShopIdString;
	private int staffRole;
	private Long shopId;
	private String shopCode;
	private String shopName;
	
	public Integer getIsOutOfDate() {
		return isOutOfDate;
	}

	public void setIsOutOfDate(Integer isOutOfDate) {
		this.isOutOfDate = isOutOfDate;
	}

	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}
	
	public String getStrShopIds() {
		return strShopIds;
	}

	public void setStrShopIds(String strShopIds) {
		this.strShopIds = strShopIds;
	}

	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

	public Boolean getIsCheckAll() {
		return isCheckAll;
	}

	public void setIsCheckAll(Boolean isCheckAll) {
		this.isCheckAll = isCheckAll;
	}

	public List<Long> getLstIdStaffMap() {
		return lstIdStaffMap;
	}

	public void setLstIdStaffMap(List<Long> lstIdStaffMap) {
		this.lstIdStaffMap = lstIdStaffMap;
	}

	public File getExcelFileCus() {
		return excelFileCus;
	}
	
	public String getListShopIdString() {
		return listShopIdString;
	}

	public void setListShopIdString(String listShopIdString) {
		this.listShopIdString = listShopIdString;
	}

	public List<Long> getListShopId() {
		return listShopId;
	}

	public void setListShopId(List<Long> listShopId) {
		this.listShopId = listShopId;
	}

	public Long getShopId() {
		return shopId;
	}

	public List<Long> getGroupDtlIdLst() {
		return groupDtlIdLst;
	}

	public void setGroupDtlIdLst(List<Long> groupDtlIdLst) {
		this.groupDtlIdLst = groupDtlIdLst;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public boolean isPermission() {
		return permission;
	}

	public void setPermission(boolean permission) {
		this.permission = permission;
	}

	public Integer getTypeGroup() {
		return typeGroup;
	}

	public void setTypeGroup(Integer typeGroup) {
		this.typeGroup = typeGroup;
	}
	
	public boolean isPermissionUser() {
		return permissionUser;
	}

	public void setPermissionUser(boolean permissionUser) {
		this.permissionUser = permissionUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAmountType() {
		return amountType;
	}

	public void setAmountType(Integer amountType) {
		this.amountType = amountType;
	}

	public String getDisplayProgramExclusion() {
		return displayProgramExclusion;
	}

	public void setDisplayProgramExclusion(String displayProgramExclusion) {
		this.displayProgramExclusion = displayProgramExclusion;
	}

	public Integer getHtad() {
		return htad;
	}

	public void setHtad(Integer htad) {
		this.htad = htad;
	}

	public int getQuotaStatus() {
		return quotaStatus;
	}

	public void setQuotaStatus(int quotaStatus) {
		this.quotaStatus = quotaStatus;
	}

	public int getLevelStatus() {
		return levelStatus;
	}

	public void setLevelStatus(int levelStatus) {
		this.levelStatus = levelStatus;
	}

	public boolean isPermissionEdit() {
		return permissionEdit;
	}

	public void setPermissionEdit(boolean permissionEdit) {
		this.permissionEdit = permissionEdit;
	}

	public boolean isDevIsUpdate() {
		return devIsUpdate;
	}

	public void setDevIsUpdate(boolean devIsUpdate) {
		this.devIsUpdate = devIsUpdate;
	}

	public List<ApParam> getLstDisplayProgramType() {
		return lstDisplayProgramType;
	}

	public void setLstDisplayProgramType(List<ApParam> lstDisplayProgramType) {
		this.lstDisplayProgramType = lstDisplayProgramType;
	}

	public String getToDateActionLog() {
		return toDateActionLog;
	}

	public void setToDateActionLog(String toDateActionLog) {
		this.toDateActionLog = toDateActionLog;
	}

	public String getFromDateActionLog() {
		return fromDateActionLog;
	}

	public void setFromDateActionLog(String fromDateActionLog) {
		this.fromDateActionLog = fromDateActionLog;
	}

	public List<StatusBean> getSt() {
		return st;
	}

	public void setSt(List<StatusBean> st) {
		this.st = st;
	}
	
	public int getStaffRole() {
		return staffRole;
	}

	public void setStaffRole(int staffRole) {
		this.staffRole = staffRole;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */

	@Override
	public void prepare() throws Exception {
		super.prepare();
		 displayProgramMgr = (DisplayProgrameMgr) context.getBean("displayProgrameMgr");
		 productMgr = (ProductMgr) context.getBean("productMgr");
		 customerMgr = (CustomerMgr) context.getBean("customerMgr");
		 displayProgramVNMMgr = (DisplayProgramVNMMgr) context.getBean("displayProgramVNMMgr");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */

	@Override
	public String execute() throws Exception {
		staff = getStaffByCurrentUser();
//		staffRole = staff.getStaffType().getObjectType();
		return SUCCESS;
	}
	
	/**
	 * Copy focus program.
	 *
	 * @return the string
	 * @author hungtt
	 * @since Jul 5, 2013
	 */
	public String copyDisplayProgram() {
		try {
			StDisplayProgram dp = displayProgramMgr.getStDisplayProgramByCode(code);
			if(dp != null) {
				result.put(ERROR, true);
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.exists"));
				return JSON;
			}
			StDisplayProgram pp = displayProgramMgr.copyDisplayProgram(id,getLogInfoVO(), code, name);
			if(pp != null){
				result.put("displayProgramId", pp.getId());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);

		}
		return JSON;
	}
	
	
	/**
	 * @author tientv11
	 * @danh sach CTTB
	 */
	public String staffMapPageLoadDP() throws Exception {
		try {
			Staff staffSign = getStaffByCurrentUser();
	    	if(staffSign==null) {
	    		return JSON;
	    	}
	    	if (listShopId != null && listShopId.size() > 0) { // remove nhung thang khong thuoc quyen
				for (int i = 0, sz = listShopId.size(); i < sz; i++) {
					if (!checkShopInOrgAccessById(listShopId.get(i))) {
						listShopId.remove(i);
						i--;
						sz--;
					}
				}
			}
			if (listShopId == null ||  listShopId.size() == 0) {
				listShopId = new ArrayList<Long>();
				ShopToken shToken = currentUser.getShopRoot();
				if (shToken != null) {
					listShopId.add(shToken.getShopId());
				}
			}
			Date fDate = null;
			if(!StringUtil.isNullOrEmpty(month)){
				fDate = DateUtil.parse("01/"+month, DateUtil.DATE_FORMAT_DDMMYYYY);
			}else 
				fDate=DateUtil.getFirstDateInMonth(DateUtil.now());
			listDisplayProgram = displayProgramMgr.getListDisplayProgramSysdateEX(listShopId,fDate);
		}
		catch(Exception ex)	{
			LogUtility.logError(ex,"DisplayProgramAction.staffMapPageLoadDP");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}
	
	/**
	 * Search staff.
	 *
	 * @return the string
	 * @author tientv11
	 */
	public String searchStaff() throws Exception {   
	    try{	
	    	List<Long> shopIds = new ArrayList<Long>();
	    	result.put("page", page);
		    result.put("rows", rows);		    
		    KPaging<StDisplayStaffMap> kPaging = new KPaging<StDisplayStaffMap>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
	    	Staff staffSign = getStaffByCurrentUser();
	    	if(staffSign==null) {
	    		return JSON;
	    	}
	    	Shop shop = null;
	    	ShopToken shToken = currentUser.getShopRoot();
	    	if (shToken != null) {
	    		shop = shopMgr.getShopById(shToken.getShopId());
	    	} else {
	    		shop = staffSign.getShop();
	    	}
	    	if(StringUtil.isNullOrEmpty(strShopIds)	&& (shopId==null || shopId==0)){
	    		shopId = shop.getId();
	    		shopIds.add(shopId);
	    	}else if(shopId!=null && shopId>0){
	    		Shop shopTmp = shopMgr.getShopById(shopId);
				if(shopTmp != null && checkShopInOrgAccessByCode(shopTmp.getShopCode())) {
					shopId = shopTmp.getId();
				}else{
					shopId = shop.getId();
				}
				shopIds.add(shopId);
	    	}else{
	    		String[] str = strShopIds.split(",");
	    		for (String s : str) {
	    			Shop shopTmp = shopMgr.getShopById(Long.valueOf(s));
					if(shopTmp != null && checkShopInOrgAccessByCode(shopTmp.getShopCode())) {
						shopId = shopTmp.getId();
						shopIds.add(shopId);
					}
	    		}
	    	}
	    	if(StringUtil.isNullOrEmpty(month)){
	    		month = DateUtil.toMonthYearString(DateUtil.now());
	    	}
	    	boolean b = false;
	    	if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
	    		b = true;
	    	}
	    	long parentStaffId = currentUser.getStaffRoot().getStaffId();
			ObjectVO<StDisplayStaffMap> displayStaffMapVO = displayProgramMgr.getListDisplayStaffMap(kPaging, id,shopIds,month, b, parentStaffId);
			if (displayStaffMapVO!= null) {			  
			    result.put("rows", displayStaffMapVO.getLstObject());	
			    result.put("total", displayStaffMapVO.getkPaging().getTotalRows());
			} else {
				 result.put("rows",new ArrayList<StDisplayStaffMap>());
				 result.put("total",0);
			}
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	/**
	 * @author tientv
	 */
	public String exportExcelStaff() throws Exception {
		try {	
			List<Long> shopIds = new ArrayList<Long>();
			HashMap<String, Object> beans = new HashMap<String, Object>();
			Staff staffSign = getStaffByCurrentUser();
	    	if(staffSign==null) {
	    		return JSON;
	    	}
	    	Shop shop = null;
	    	ShopToken shToken = currentUser.getShopRoot();
	    	if (shToken != null) {
	    		shop = shopMgr.getShopById(shToken.getShopId());
	    	} else {
	    		shop = staffSign.getShop();
	    	}
	    	if (StringUtil.isNullOrEmpty(strShopIds)	&& (shopId==null || shopId==0)) {
	    		shopId = shop.getId();
	    		shopIds.add(shopId);
	    	} else if (shopId!=null && shopId>0) {
	    		if(!checkShopInOrgAccessById(shopId)) {
					shopId = shop.getId();
				}
	    		shopIds.add(shopId);
	    	} else {
	    		String[] str = strShopIds.split(",");
	    		for (String s : str) {
	    			shopId = Long.valueOf(s);
	    			if(checkShopInOrgAccessById(shopId)) {
						shopIds.add(shopId);
					}
	    		}
	    	}
	    	boolean b = false;
	    	if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
	    		b = true;
	    	}
	    	long parentStaffId = currentUser.getStaffRoot().getStaffId();
			ObjectVO<StDisplayStaffMap> displayStaffMapVO = displayProgramMgr.getListDisplayStaffMap(null, id,shopIds,month, b, parentStaffId);
			if(displayStaffMapVO!= null && displayStaffMapVO.getLstObject().size()>0){
				for(StDisplayStaffMap map:displayStaffMapVO.getLstObject()){
					map.setCreateUser(DateUtil.toDateString(map.getMonth(), DateUtil.DATE_FORMAT_DDMMYYYY));
					map.setUpdateUser(DateUtil.toDateString(map.getUploadDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					map.getDisplayProgram().setCreateUser(DateUtil.toDateString(map.getUpdateDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				}				
				beans.put("lst", displayStaffMapVO.getLstObject());
				String outputDownload = ReportUtils.exportExcelJxls(beans, ShopReportTemplate.CTTB_STAFF,FileExtension.XLS);
				result.put(ERROR, false);
				result.put(LIST, outputDownload);
				result.put("hasData", true);
			}else{
				result.put("hasData", false);
			}			
			
		} catch (Exception e) {		
			LogUtility.logError(e, "export(): " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	
	/**
	 * 
	 * @return
	 * @author tientv
	 */
	public String importStaff() throws Exception {
		/*Staff userLogin = getStaffByCurrentUser();
		if(userLogin==null ){
			isError = true;
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.not.belong.area");
			return SUCCESS;
		}
		ShopToken shToken = currentUser.getShopRoot();
    	boolean checkMap = false;
    	if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
    		checkMap = true;
    	}
    	long parentStaffId = currentUser.getStaffRoot().getStaffId();
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";		
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,4);		
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				if(lstData.size()>5000){
					isError = true;
					errMsg = "Chỉ hỗ trợ import tối đa 5000 dòng dữ liệu.";
					return SUCCESS;
				}
				lstView= new ArrayList<CellBean>();	
				List<StDisplayStaffMap> listImport = new ArrayList<StDisplayStaffMap>();
				BasicFilter<BasicVO> filter = new BasicFilter<BasicVO>();
				filter.setParentId(parentStaffId);
				filter.setInheritUserPriv(filter.getParentId());
				filter.setFlagCMS(true);
				
				for(int i=0;i<lstData.size();i++){
					totalItem++;
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						List<String> row = lstData.get(i);	
						Staff staff = null;
						StDisplayProgram displayProgram = null;
						StDisplayStaffMap displayStaffMap = new StDisplayStaffMap();
						*//** Check trung du lieu cac record *//*
						message = "";
						message = check1(lstData, i);
						if (!StringUtil.isNullOrEmpty(message)){
							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
							continue;
						}						
						*//** Ma chuong trinh trung bay *//*
						if(row.size() > 0){
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "customer.display.program.code", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
							if(!StringUtil.isNullOrEmpty(msg)){
								message += msg+"\n";								
							} else {
								value = value.trim().toUpperCase();
								displayProgram = displayProgramMgr.getStDisplayProgramByCode(value);
								if(displayProgram==null){
									message += "CTTB không tồn tại trong hệ thống" + "\n";
								}else if(!ActiveType.RUNNING.equals(displayProgram.getStatus())){
									message += "CTTB có trạng thái không hoạt động" + "\n";
								}else if(displayProgram.getToDate()!=null 
										&&	DateUtil.compareDateWithoutTime(displayProgram.getToDate(),DateUtil.now())<0){
									message += "CTTB - Đã quá ngày kết thúc chương trình!"+ "\n";									
								}
							}
						}
						*//** Nhan vien ban hang *//*
						if(row.size() > 1){
							String value = row.get(1);
							String msg = ValidateUtil.validateField(value, "catalog.staff.code.saleplan", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
							if(!StringUtil.isNullOrEmpty(msg)){
								message += msg+"\n";								
							} else {
								staff = staffMgr.getStaffByCode(value);
								if(staff == null || staff.getShop()==null){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.staff.code.saleplan");								
								
								}else if(!ActiveType.RUNNING.equals(staff.getStatus())){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.staff.code.saleplan");
								}else if(staff.getStaffType()==null || 
										(!staff.getStaffType().getObjectType().equals(StaffRoleType.NVBH_ON_VAN.getValue()) &&
										!staff.getStaffType().getObjectType().equals(StaffRoleType.NVBH_PRE.getValue()))){
									message += "Nhân viên không phải là NVBH" + "\n";
								} else if(displayProgram!=null 
										&& displayProgramMgr.checkShopOfStaffJoinDisplayProgram(displayProgram.getId(), staff.getShop().getId())<=0){
										message += "NVBH thuộc đơn vị không tham gia CTTB." + "\n";
								} else {
									filter.setId(staff.getId());
									filter.setShopRootId(shToken.getShopId());
									if ((checkMap && !commonMgr.checkStaffInCMSByFilterWithCMS(filter))
											|| !checkShopInOrgAccessById(staff.getShop().getId())) {
										message += "Mã NVBH không thuộc quản lý của user đăng nhập." + "\n";
									}
								}
								displayStaffMap.setStaff(staff);
							}
						}
						*//** Thang *//*
						Date fromDate = null;
						if(row.size() > 2){
							String value = row.get(2);														
							try {									
								if (!StringUtil.isNullOrEmpty(value)) {	
									Date importD  = DateUtil.parse(value, DateUtil.DATE_FORMAT_STR);
									value = "01/" + DateUtil.toMonthYearString(importD);
									
									Date tmp = DateUtil.parse(value, DateUtil.DATE_FORMAT_STR);
									if(displayProgram!=null){
										String dateStr = DateUtil.toDateString(displayProgram.getFromDate(), DateUtil.DATE_M_Y);
										Date tmpFDate = DateUtil.parse("01/" + dateStr, DateUtil.DATE_FORMAT_STR);										
										if(tmp!=null){										
											if(DateUtil.compareDateWithoutTime(tmp, tmpFDate)<0){
												message +="Giá trị ngày nhập vào trước ngày bắt đầu của chương trình" + "\n";
											
											}else if(displayProgram.getToDate()!=null
													&& DateUtil.compareDateWithoutTime(tmp, displayProgram.getToDate())>0){
												message +="Giá trị ngày nhập vào sau ngày kết thúc của chương trình";
											
											}else if(DateUtil.compareDateWithoutTime(tmp, DateUtil.getDateFirtMonthOfNow())<0){
												message +="Tháng nhập vào phải lớn hơn hoặc bằng tháng hiện tại"+ "\n";
											
											}else{											
												fromDate = DateUtil.getFirstDateInMonth(tmp);
											}
										}else{
											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.fromDate"),null);
										}
									}									
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "ss.traningplan.date.code.month");																	
								}  
								displayStaffMap.setMonth(fromDate);
							} catch (Exception e) {
								LogUtility.logError(e, e.getMessage());
								message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.fromDate"),null);
							}							
						}						
						*//** So suat trung bay *//*
						Integer quantity = 0;
						if (row.size() > 3) {
							try{
								String value = row.get(3);
								String checkMessage = ValidateUtil.validateField(value, "catalog.promotiontype.qty.ss", 9,
										ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,
										ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
								if (!StringUtil.isNullOrEmpty(checkMessage)) {
									message += checkMessage;
								}else{
									quantity =  Integer.valueOf(value);
									displayStaffMap.setQuantityMax(quantity);
								}								
							}catch(Exception ex){
								message += "Số suất NVBH không hợp lệ " + "\n";
							}
						}
						
						if(StringUtil.isNullOrEmpty(message) && staff!=null && fromDate!=null && displayProgram!=null){
							*//** Kiem tra ton tai ban ghi trong thang *//*
							StDisplayStaffMap staffMapCheckExists = displayProgramMgr.getDisplayStaffMap(displayProgram.getId(), staff.getId(),fromDate);
							if(staffMapCheckExists!=null){
								displayStaffMap = staffMapCheckExists;
								if(quantity!=null && quantity<displayStaffMap.getQuantityReceived()){
									message += "Số suất phân bổ phải lớn hơn hoặc bằng số suất đã phân bổ " + "\n";
								}
							}
						}
						if(StringUtil.isNullOrEmpty(message)){
							if(displayStaffMap.getId()==null){	
								displayStaffMap.setDisplayProgram(displayProgram);
								displayStaffMap.setQuantityReceived(0);
								displayStaffMap.setQuantityMax(quantity);
								displayStaffMap.setCreateDate(new Date());
								displayStaffMap.setUploadDate(new Date());
								displayStaffMap.setCreateUser(userLogin.getStaffCode());
							}else{
								displayStaffMap.setQuantityMax(quantity);
								displayStaffMap.setUpdateDate(new Date());
								displayStaffMap.setUploadDate(new Date());
								displayStaffMap.setUpdateUser(userLogin.getStaffCode());
								*//** Update *//*
							}
							listImport.add(displayStaffMap);
						}else{
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}				
				if(listImport.size()>0){
					displayProgramMgr.appendStDisplayStaffMap(listImport, getLogInfoVO());
				}			
				getOutputFailExcelFile(lstFails,ConstantManager.TEMPLATE_CATALOG_PROGRAME_STAFF_MAP);
				
			}catch (Exception e) {	
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;*/
		return SUCCESS;
	}
	
	/**
	 * Save info.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 30/07/2012
	 */
	public String saveInfo(){		
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		Date ttDate = null;
		Date ffDate = null;
		if (currentUser != null) {
			try {
				StDisplayProgram displayProgram = displayProgramMgr.getStDisplayProgramByCode(code);
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					ffDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.fromDate")));
					return JSON;
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					ttDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (id != null && id > 0) {
					if (displayProgram != null) {
						if (id.intValue() != displayProgram.getId().intValue()) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.display.program.code");
						} else {
							boolean isAccept = false;
							if (ActiveType.WAITING.getValue().equals(status) && ActiveType.RUNNING.getValue().equals(displayProgram.getStatus().getValue())) {//Dang hoat dong khong the update thanh du thao
								result.put(ERROR, true);
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.error.running.to.waiting"));
								return JSON;
							}
							try {
								if (ActiveType.RUNNING.getValue().equals(status) && ActiveType.WAITING.getValue().equals(displayProgram.getStatus().getValue())) {
									if (!displayProgramMgr.checkUpdateDisplayProgramForActive(id).equals(ExceptionCode.ERR_ACTIVE_PROGRAM_OK)) {
										result.put(ERROR, true);
										result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "display.no.shop.product"));
										return JSON;
									}
								}
								Integer typeOne = displayProgramMgr.checkExistsProductDisplayGr(id, DisplayProductGroupType.DS);
								Integer typeTwo = displayProgramMgr.checkExistsProductDisplayGr(id, DisplayProductGroupType.SL);
								if (typeOne > 0 && typeTwo > 0) {
									displayProgram.setProgrameType(3);
								} else if (typeOne > 0) {
									displayProgram.setProgrameType(1);
								} else if (typeTwo > 0) {
									displayProgram.setProgrameType(2);
								} else {
									if (displayProgram.getStatus().getValue() != ActiveType.WAITING.getValue()) {
										result.put(ERROR, true);
										result.put("errMsg", "CTTB không có sản phẩm thuộc nhóm doanh số");
										return JSON;
									}
								}
							} catch (IllegalArgumentException e) {
								result.put(ERROR, true);
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ERR_ACTIVE_DISPLAY_PROGRAM_ALL"));
								return JSON;
							}
							if (displayProgram.getToDate() == null) {
								isAccept = true;
							} else {
								if (ActiveType.WAITING.getValue() == status) {
									isAccept = true;
								} else {
									if (ttDate != null && status != ActiveType.DELETED.getValue() && DateUtil.compareDateWithoutTime(ttDate, DateUtil.now()) < 0) {
										result.put(ERROR, true);
										result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.end.date.update"));
										return JSON;
									} else {
										isAccept = true;
									}
								}
							}
							if (isAccept && status != ActiveType.DELETED.getValue()) {
								displayProgram.setFromDate(ffDate);
								displayProgram.setToDate(ttDate);
							}
							displayProgram.setDisplayProgramCode(code);
							displayProgram.setDisplayProgramName(name);
							displayProgram.setStatus(ActiveType.parseValue(status));
							displayProgram.setUpdateUser(currentUser.getUserName());
							displayProgramMgr.updateDisplayProgram(displayProgram, getLogInfoVO());
							error = false;
							result.put("id", id);
						}
					}
				} else {
					if (displayProgram != null) {
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.display.program.code");
					} else {
						StDisplayProgram newDisplayProgram = new StDisplayProgram();
						newDisplayProgram.setDisplayProgramCode(code);
						newDisplayProgram.setDisplayProgramName(name);
						newDisplayProgram.setCreateUser(currentUser.getUserName());
						if (status != ActiveType.WAITING.getValue()) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.status"));
							return JSON;
						}
						newDisplayProgram.setStatus(ActiveType.WAITING);
						newDisplayProgram.setFromDate(ffDate);
						newDisplayProgram.setToDate(ttDate);
						newDisplayProgram.setCreateDate(new Date());
						newDisplayProgram = displayProgramMgr.createDisplayProgram(newDisplayProgram, getLogInfoVO());
						if (newDisplayProgram != null) {
							error = false;
							result.put("id", newDisplayProgram.getId());
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Gets the change form.
	 * 
	 * @return the change form
	 * @author loctt
	 * @since 16/09/2012
	 */
	public String getChangeForm() {	
		
		return SUCCESS;

	}

	public String importExcel(){
		switch (excelType) {
		case IMPORT_EXCEL_DISPLAY_PROGRAM:
			return importDisplayProgram();
//		case IMPORT_EXCEL_QUOTA_GROUP:
//			return importQuotaGroup();
//		case IMPORT_EXCEL_LEVEL:
//			return importLevel();
//		case IMPORT_EXCEL_PRODUCT:
//			return importProduct();
//		case IMPORT_EXCEL_CUSTOMER:
//			return importCustomer();
		case IMPORT_SHOP:
			return importShopOfDisplayProgram();
		default:
			break;
		}		
		return importShopOfDisplayProgram();
	}
	
	public String importDisplayProgram(){
		/*isError = true;
		//errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = false;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		
		staff = getStaffByCurrentUser();
		if(staff == null || staff.getStaffType().getObjectType() != StaffRoleType.VNM.getValue()){
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION);
		}
		List<List<String>> lstData = getExcelDataWithHeader(excelFile, excelFileContentType, errMsg,7);		
		if(lstData != null && lstData.size() == 0){
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.has.no.result");
		}
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				int c = 0;
				if(lstData.size() > 5000){
					message += R.getResource("import.cttb.err.rowQuantity");
				}
				List<String> rowHeader = lstData.get(0);
				String value = rowHeader.get(c);
				if(StringUtil.isNullOrEmpty(message)
						&&(StringUtil.isNullOrEmpty(value) || !value.equals(R.getResource("import.cttb.header.ma")))){
					errMsg = R.getResource("import.cttb.err.header");
				}
				value = rowHeader.get(++c);
				if(StringUtil.isNullOrEmpty(message)
						&&(StringUtil.isNullOrEmpty(value) || !value.equals(R.getResource("import.cttb.header.ten")))){
					errMsg = R.getResource("import.cttb.err.header");
				}
				value = rowHeader.get(++c);
				if(StringUtil.isNullOrEmpty(message)
						&&(StringUtil.isNullOrEmpty(value) || !value.equals(R.getResource("import.cttb.header.tuNgay")))){
					errMsg = R.getResource("import.cttb.err.header");
				}
				value = rowHeader.get(++c);
				if(StringUtil.isNullOrEmpty(message)
						&&(StringUtil.isNullOrEmpty(value) || !value.equals(R.getResource("import.cttb.header.denNgay")))){
					errMsg = R.getResource("import.cttb.err.header");
				}
				value = rowHeader.get(++c);
				if(StringUtil.isNullOrEmpty(message)
						&&(StringUtil.isNullOrEmpty(value) || !value.equals(R.getResource("import.cttb.header.mien")))){
					errMsg = R.getResource("import.cttb.err.header");
				}
				value = rowHeader.get(++c);
				if(StringUtil.isNullOrEmpty(message)
						&&(StringUtil.isNullOrEmpty(value) || !value.equals(R.getResource("import.cttb.header.vung")))){
					errMsg = R.getResource("import.cttb.err.header");
				}
				value = rowHeader.get(++c);
				if(StringUtil.isNullOrEmpty(message)
						&&(StringUtil.isNullOrEmpty(value) || !value.equals(R.getResource("import.cttb.header.npp")))){
					errMsg = R.getResource("import.cttb.err.header");
				}
				
				if(StringUtil.isNullOrEmpty(message)){
					for(int i=1;i<lstData.size();i++){
						c=0;
						if(lstData.get(i)!= null && lstData.get(i).size() > 0){
							totalItem++;
							message = "";
							List<String> row = lstData.get(i);
							
							//check ma cttb
							String strMa = row.get(c);
							message+= ValidateUtil.validateField(strMa, "import.cttb.header.ma", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
							if(StringUtil.isNullOrEmpty(message)){
								StDisplayProgram dp = displayProgramMgr.getStDisplayProgramByCode(strMa);
								if(dp != null){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST,true,"import.cttb.header.ma");
								}
							}
							
							//check ten cttb
							String strTen = row.get(++c);
							message += ValidateUtil.validateField(strTen, "import.cttb.header.ten", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							
							//check tu ngay
							String strTuNgay = row.get(++c);
							message += ValidateUtil.validateField(strTuNgay, "import.cttb.header.tuNgay", null, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
							
							//check den ngay
							String strDenNgay = row.get(++c);
							message += ValidateUtil.validateField(strDenNgay, "import.cttb.header.denNgay", null, ConstantManager.ERR_INVALID_DATE);
							
							
							if (StringUtil.isNullOrEmpty(message)){
								Date fDate = DateUtil.parse(strTuNgay, DateUtil.DATE_FORMAT_STR);
								Date tDate = DateUtil.parse(strDenNgay, DateUtil.DATE_FORMAT_STR);
								if(DateUtil.compareDateWithoutTime(DateUtil.now(),fDate) == 1){
									message += R.getResource("common.fromdate.greater.currentdate")+"\n";
								}
								String strTemp = ValidateUtil.getErrorMsgForInvalidToDate(fDate, tDate);
								if(!StringUtil.isNullOrEmpty(strTemp)){
									message += strTemp+"\n";
								}
								
								String strMien = row.get(++c);
								List<Shop> mien = new ArrayList<Shop>();
								if(!StringUtil.isNullOrEmpty(strMien)){
									for(String str: strMien.split(",")){
										Shop s = shopMgr.getShopByCode(str.trim());
										if(s == null){
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "import.cttb.header.mien");
										}else{
											if(ActiveType.STOPPED.getValue().equals(s.getStatus().getValue())){
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,true,"import.cttb.header.mien");
											}else{
												if(!s.getType().getObjectType().equals(ShopObjectType.MIEN.getValue())){
													message += R.getResource("catalog.display.program.shop.not.mien")+"\n";
												}else{
													mien.add(s);
												}
											}
										}
									}
								}
								String strVung = row.get(++c);
								List<Shop> vung = new ArrayList<Shop>();
								if(!StringUtil.isNullOrEmpty(strVung)){
									for(String str: strVung.split(",")){
										Shop s = shopMgr.getShopByCode(str.trim());
										if(s == null){
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "import.cttb.header.vung");
										}else{
											if(ActiveType.STOPPED.getValue().equals(s.getStatus().getValue())){
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,true,"import.cttb.header.vung");
											}else{
												if(!s.getType().getObjectType().equals(ShopObjectType.VUNG.getValue())){
													message += R.getResource("catalog.display.program.shop.not.vung")+"\n";
												}else{
													vung.add(s);
												}
											}
										}
									}
								}
								String strNPP = row.get(++c);
								List<Shop> npp = new ArrayList<Shop>();
								if(!StringUtil.isNullOrEmpty(strNPP)){
									for(String str: strNPP.split(",")){
										Shop s = shopMgr.getShopByCode(str.trim());
										if(s == null){
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "import.cttb.header.npp");
										}else{
											if(ActiveType.STOPPED.getValue().equals(s.getStatus().getValue())){
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,true,"import.cttb.header.npp");
											}else{
												if(!s.getType().getObjectType().equals(ShopObjectType.NPP.getValue())){
													message += R.getResource("catalog.display.program.shop.not.npp")+"\n";
												}else{
													npp.add(s);
												}
											}
										}
									}
								}
								
								if(StringUtil.isNullOrEmpty(message)){
									StDisplayProgram dp = new StDisplayProgram();
									dp.setDisplayProgramCode(strMa.toUpperCase());
									dp.setDisplayProgramName(strTen);
									dp.setFromDate(fDate);
									dp.setToDate(tDate);
									dp.setCreateDate(DateUtil.now());
									dp.setCreateUser(getCurrentUser().getUserName());
									dp.setStatus(ActiveType.WAITING);
									dp = displayProgramMgr.createDisplayProgram(dp, getLogInfoVO());
									if(npp.size() == 0 && vung.size() == 0 && mien.size() == 0){
										DisplayShopMap dsm = new DisplayShopMap();
										dsm.setDisplayProgram(dp);
										//TODO: dsm.setShop(shopMgr.getShopVNM()); // phai sua
										dsm.setStatus(ActiveType.RUNNING);
										dsm.setCreateDate(DateUtil.now());
										dsm.setCreateUser(getCurrentUser().getUserName());
										displayProgramMgr.createDisplayShopMap(dsm, getLogInfoVO());
									}else{
										for(Shop sNPP: npp){
											for(int k=0, size = vung.size();k< size;k++){
												Shop sVung = vung.get(k);
												if(checkShopInOrgAccessByCode(sNPP.getShopCode())){
													vung.remove(sVung);
													size--;
													k--;
												}
											}
											for(int k=0, size = mien.size();k< size;k++){
												Shop sMien = mien.get(k);
												if(checkShopInOrgAccessByCode(sNPP.getShopCode())){
													mien.remove(sMien);
													size--;
													k--;
												}
											}
											createShopMapImport(dp,sNPP);
										}
										for(Shop sVung: vung){
											for(int k=0, size = mien.size();k< size;k++){
												Shop sMien = mien.get(k);
												if(checkShopInOrgAccessByCode(sVung.getShopCode())){
													mien.remove(sMien);
													size--;
													k--;
												}
											}
											createShopMapImport(dp,sVung);
										}
										for(Shop sMien: mien){
											createShopMapImport(dp,sMien);
										}
									}
								}else{
									lstFails.add(StringUtil.addFailBean(row, message));
								}
							}else{
								lstFails.add(StringUtil.addFailBean(row, message));
							}
						}
					}
					getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_DISPLAY_PROGRAM_FAIL);
				}
			}catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}		
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}*/
		return SUCCESS;
	}
	
	private void createShopMapImport(StDisplayProgram dp, Shop s) throws BusinessException{
		DisplayShopMap dsm = new DisplayShopMap();
		dsm.setDisplayProgram(dp);
		dsm.setShop(s);
		dsm.setStatus(ActiveType.RUNNING);
		dsm.setCreateDate(DateUtil.now());
		dsm.setCreateUser(getCurrentUser().getUserName());
		displayProgramMgr.createDisplayShopMap(dsm, getLogInfoVO());
	}
	
	public String importShopOfDisplayProgram(){
		/*isError = true;
		//errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = false;
		List<CellBean> lstFails = new ArrayList<CellBean>();		
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,2);		
		if(lstData != null && lstData.size() == 0){
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.has.no.result");
		}
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				for(int i=0;i<lstData.size();i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
						DisplayShopMap displayShopMap = new DisplayShopMap();						
						StDisplayProgram displayProgram = null; 						
						List<String> row = lstData.get(i);

						displayProgram = displayProgramMgr.getStDisplayProgramById(id);
						if(displayProgram != null && displayProgram.getStatus().getValue() == ActiveType.DELETED.getValue()){
							errMsg=Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "focus.program.incorrect");
							return SUCCESS;
						}
						if(displayProgram != null && displayProgram.getToDate() != null){
							if(DateUtil.compareDateWithoutTime(displayProgram.getToDate(),DateUtil.now()) == -1){
								errMsg=Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "display.program.error.date");
								return SUCCESS;
							}
						}
						displayShopMap.setDisplayProgram(displayProgram);
						String parentShopCode = null;
						if(session!= null && session.getAttribute(ConstantManager.SESSION_SHOP)!= null){
					    	parentShopCode = ((Shop)session.getAttribute(ConstantManager.SESSION_SHOP)).getShopCode();
					    }
						//shop code
						Shop shop = null;	
						if(row.size() > 0){
							String value = row.get(0);
							if(!StringUtil.isNullOrEmpty(value)){
								shop = shopMgr.getShopByCode(value);
								if(shop == null){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"catalog.focus.program.shop.code");
								}else if(!shop.getType().getObjectType().equals(ShopObjectType.GT.getValue())
										&& !shop.getType().getObjectType().equals(ShopObjectType.MIEN.getValue())
										&& !shop.getType().getObjectType().equals(ShopObjectType.VUNG.getValue())
										&& !shop.getType().getObjectType().equals(ShopObjectType.NPP.getValue())){
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.not.apply.shop");
								}else if(!shop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.shop.not.running");
								}else if(!StringUtil.isNullOrEmpty(parentShopCode) 
										&& !checkShopInOrgAccessByCode(shop.getShopCode())){
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_SHOP,shop.getShopCode(),currentUser.getUserName()) + "\n";
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,true,"catalog.focus.program.shop.code");									
							}
							if(StringUtil.isNullOrEmpty(message)){
								displayShopMap.setShop(shop);
							}
						}
						boolean isExists = false;
						if(shop!=null && displayProgram!=null && displayProgramMgr.checkExistRecordDisplayShopMap(shop.getId(),displayProgram.getId())){
							isExists = true;							
						}
						if(isExists && displayProgram.getId()!=null && !StringUtil.isNullOrEmpty(shop.getShopCode())){
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.shop.is.exists") + "\n";
						}
//						if(StringUtil.isNullOrEmpty(message)){
//							boolean flag1 = displayProgramMgr.checkExistChildDisplayShopMap(shop.getShopId(),id);
//							if(flag1){
//								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.shop.con.is.exists") + "\n";
//							}
//						}
						if (StringUtil.isNullOrEmpty(message)){
								displayShopMap.setCreateDate(commonMgr.getSysDate());
								displayShopMap.setCreateUser(currentUser.getUserName());
								displayShopMap.setStatus(ActiveType.RUNNING);
								displayProgramMgr.createDisplayShopMap(displayShopMap, getLogInfoVO());
								importProgram = "shop";						
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}				
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_DISPLAY_SHOP_MAP_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}		
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}*/
		return SUCCESS;
	}
	
	public String deleteDisplayStaffMap() throws Exception {
		try {
			if(isCheckAll != null && isCheckAll == true) {
				if(!StringUtil.isNullOrEmpty(code)){
		    		StDisplayProgram pppp = displayProgramMgr.getStDisplayProgramByCode(code);
		    		if(pppp==null) {
		    			return JSON;
		    		}
		    		id = pppp.getId();
		    	}
				boolean b = false;
		    	if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
		    		b = true;
		    	}
		    	long parentStaffId = currentUser.getStaffRoot().getStaffId();
				displayProgramMgr.deleteAllDisplayStaffMap(id, month, b, parentStaffId, getLogInfoVO());
			} else {
				displayProgramMgr.deleteListDisplayStaffMap(lstIdStaffMap, getLogInfoVO());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	public String exportExcelShop()
	{
		try {
			List<Shop> objectFSM = displayProgramMgr.getListShopActiveInDisplayProgram(id, shopCode, shopName);
			if(objectFSM == null || (objectFSM != null && objectFSM.size()==0)){
				result.put("hasData", false);
				return JSON;
			}
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if (objectFSM != null) {
				int size = objectFSM.size();
				for (int i = 0; i < size; i++) {
					Shop temp = objectFSM.get(i);
					CellBean cellBean = new CellBean();
					if (temp != null) {
						cellBean.setContent1(temp.getShopCode());
					}
					lstFails.add(cellBean);
				}
			}
			exportExcelDataNotSession(lstFails,
					ConstantManager.TEMPLATE_CATALOG_DISPLAY_SHOP_MAP_EXPORT);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Search shop.
	 *
	 * @return the string
	 * @author hungtt
	 * @since Sep 20, 2013
	 */
	public String searchShop() {
	    result.put("page", page);
	    result.put("max", max);
	    try{  		
	    	List<JsPromotionShopTreeNode> lstStaffTypeTree = new ArrayList<JsPromotionShopTreeNode>();
			JsPromotionShopTreeNode bean = new JsPromotionShopTreeNode();
	    	//NewTreeVO<Shop,ProgrameExtentVO> shopTree = shopMgr.getShopTreeInDisplayProgram(id, shopCode, shopName);
			NewTreeVO<Shop,ProgrameExtentVO> shopTree = new NewTreeVO<Shop, ProgrameExtentVO>();
	    	if (shopTree.getObject() == null
					&& shopTree.getListChildren() != null) {
				for (int i = 0; i < shopTree.getListChildren().size(); i++) {
					JsPromotionShopTreeNode tmp = new JsPromotionShopTreeNode();
					tmp = getJsTreeNode(tmp, shopTree.getListChildren().get(i));					
					lstStaffTypeTree.add(tmp);
				}
			} else {
				bean = getJsTreeNode(bean, shopTree);
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
				lstStaffTypeTree.add(bean);
			}
	    	result.put("rows", lstStaffTypeTree);
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	/**
	 * Gets the js tree node.
	 *
	 * @param node the node
	 * @param channelType the channel type
	 * @return the js tree node
	 * @author hungtt
	 * @since Sep 20, 2013
	 */
	private JsPromotionShopTreeNode getJsTreeNode(JsPromotionShopTreeNode node,
			NewTreeVO<Shop, ProgrameExtentVO> channelType) {
		if (channelType != null && channelType.getObject() != null) {
			node.setData(channelType.getObject().getShopCode()+"-"+channelType.getObject().getShopName());
			node.setId(channelType.getObject().getId());
			node.setIsShop(channelType.getDetail().getIsNPP());
			node.setParentId((channelType.getObject().getParentShop() != null )?channelType.getObject().getParentShop().getId():null);
			List<JsPromotionShopTreeNode> lstChild = new ArrayList<JsPromotionShopTreeNode>();
			if (channelType.getListChildren() != null
					&& channelType.getListChildren().size() > 0) {
				for (int i = 0; i < channelType.getListChildren().size(); i++) {
					JsPromotionShopTreeNode subBean = new JsPromotionShopTreeNode();
					subBean = getJsTreeNode(subBean, channelType
							.getListChildren().get(i));
					lstChild.add(subBean);
				}
				node.setChildren(lstChild);
				//node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			}
		}
		return node;
	}
	
	public String createDisplayProgramShopMap(){
		/*try
		{
			List<DisplayShopMap> listFSM = new ArrayList<DisplayShopMap>();
			if (listShopId != null) {
				for (int i = 0; i < listShopId.size(); i++) {
					DisplayShopMap fsm = new DisplayShopMap();
					Shop s = shopMgr.getShopById(listShopId.get(i));
					StDisplayProgram pp = displayProgramMgr.getStDisplayProgramById(id);
					if(pp != null && (pp.getStatus()==null || (pp.getStatus().getValue() != ActiveType.WAITING.getValue() && pp.getStatus().getValue() != ActiveType.RUNNING.getValue()))){
						result.put(ERROR, true);
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "focus.program.incorrect"));
						return JSON;
					}
					if(pp != null && pp.getToDate() != null){
						if(DateUtil.compareDateWithoutTime(pp.getToDate(),DateUtil.now()) == -1){
							result.put(ERROR, true);
							result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "display.program.error.date"));
							return JSON;
						}
					}
					if(s == null){
						result.put(ERROR, true);
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.not.exist"));
						return JSON;
					}
//					if(s.getStatus().getValue() != ActiveType.RUNNING.getValue()){
//						result.put(ERROR, true);
//						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.incorrect"));
//						return JSON;
//					}
					fsm.setShop(s);
					fsm.setDisplayProgram(pp);
					fsm.setCreateDate(DateUtil.now());
					if(currentUser != null)
					{
						fsm.setCreateUser(currentUser.getUserName());
					}
					if (fsm != null && s.getType() != null && s.getType().getObjectType() == ShopType.SHOP.getValue()) {
						listFSM.add(fsm);
					}
					
				}
				displayProgramMgr.createListDisplayShopMap(listFSM,getLogInfoVO());
			}
		}
		catch(Exception e)
		{
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}*/
		return JSON;
	}
	
	public String deleteDisplayProgramShopMap(){
		/*try
		{
			if (listShopId != null) {
				for (int i = 0; i < listShopId.size(); i++) {
					Shop s = shopMgr.getShopById(listShopId.get(i));
					if(s != null && s.getType().getObjectType() == ShopType.SHOP.getValue())
					{
						displayProgramMgr.deleteDisplayShopMap(s.getId(), id,getLogInfoVO());
//						shopCode = (s != null)?s.getShopCode():"";
//						result.put("shopCode", shopCode);
					}
					if(s == null)
					{
						result.put(ERROR, true);
						result.put("promotionNull", true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PROMOTION_NOT_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"promotion.shop.not.exist")));
					}
				}
			}
//			Shop s = shopMgr.getShopById(shopId);
//			if(s != null)
//			{
//				displayProgramMgr.deleteDisplayShopMap(shopId, id,getLogInfoVO());
//				shopCode = (s != null)?s.getShopCode():"";
//				result.put("shopCode", shopCode);
//			}
//			else
//			{
//				result.put(ERROR, true);
//				result.put("promotionNull", true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PROMOTION_NOT_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"promotion.shop.not.exist")));
//			}
		}
		catch(Exception e)
		{
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}*/
		return JSON;
	}
	
	public String searchCustomerDisplayProgram() {
		/*result.put("page", page);
		result.put("rows", rows);
		if(rows == 0) rows = 10;
		if(page == 0) page = 1;
		try {
			boolean isExistShop = true;//checkExistShopOfUserLogin();
			if(isExistShop) {
				KPaging<CustomerDisplayProgrameVO> kPaging = new KPaging<CustomerDisplayProgrameVO>();
				kPaging.setPageSize(rows);
				kPaging.setPage(page - 1);
				Shop shopTmp = null;
				Shop shopParent = null;
				Staff s = staffMgr.getStaffByCode(currentUser.getUserName());
				if(s.getStaffType().getObjectType() == StaffRoleType.GSNPP.getValue()
	    				&& session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE)!=null){
	    			shopParent = (Shop)session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
				}else{
					shopParent = s.getShop();
				}
				if(shopId != null ) {
					shopTmp = shopMgr.getShopById(shopId);
				}
				if(shopTmp != null && checkShopInOrgAccessByCode(shopTmp.getShopCode())) {
					shopId = shopTmp.getId();
				}else {
					shopId = shopParent.getId();
				}
				if(shopId != null) {
					if(StringUtil.isNullOrEmpty(month)){
						Date date =  commonMgr.getSysDate();
						int monthI=DateUtil.getMonth(date);
						int yearI=DateUtil.getYear(date);
						String monthS=monthI>9?String.valueOf(monthI):'0'+String.valueOf(monthI);
						month = String.valueOf(monthS+'/'+String.valueOf(yearI));
					}
					//StDisplayProgram stdp = displayProgramMgr.getStDisplayProgramById(id);
					//String displayProgramCode = (stdp!=null)?stdp.getDisplayProgramCode():null;
					ObjectVO<CustomerDisplayProgrameVO> listCustomerVO = displayProgramMgr.getListCustomerDisplayPrograme(kPaging, shopId, id, month);
					if(listCustomerVO != null) {
					    result.put("total", listCustomerVO.getkPaging().getTotalRows());
					    result.put("rows", listCustomerVO.getLstObject());	
					}
				}	
			}
			//updateTotalRowGrid();
		}
		catch(Exception ex) {
			LogUtility.logError(ex,"DisplayProgramAction.searchCustomerDisplayProgram");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}*/
		return JSON;
	}
	
	public String searchCustomerDisplayProgramEX() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		if(rows == 0) rows = 10;
		if(page == 0) page = 1;
		try {
//			boolean isExistShop = true;//checkExistShopOfUserLogin();
//			if(isExistShop) {
				KPaging<CustomerDisplayProgrameVO> kPaging = new KPaging<CustomerDisplayProgrameVO>();
				kPaging.setPageSize(rows);
				kPaging.setPage(page - 1);
				if (StringUtil.isNullOrEmpty(month)) {
					Date date =  commonMgr.getSysDate();
					int monthI=DateUtil.getMonth(date);
					int yearI=DateUtil.getYear(date);
					String monthS=monthI>9?String.valueOf(monthI):'0'+String.valueOf(monthI);
					month = String.valueOf(monthS+'/'+String.valueOf(yearI));
				}
				//StDisplayProgram stdp = displayProgramMgr.getStDisplayProgramById(id);
				//String displayProgramCode = (stdp!=null)?stdp.getDisplayProgramCode():null;
				String[] temp = null;
				listShopId = new ArrayList<Long>();
				if(!StringUtil.isNullOrEmpty(listShopIdString)){
					temp = listShopIdString.split(",");
					if(temp != null){
						try{
							for(int i = 0;i<temp.length;i++){
								Long shopIdTemp = Long.parseLong(temp[i]);
								if (checkShopInOrgAccessById(shopIdTemp)) {
									listShopId.add(shopIdTemp);
								}
							}
						}catch(Exception ex){
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return JSON;
						}
						
					}
				}else{
					/*Staff s = staffMgr.getStaffByCode(currentUser.getUserName());
					if(s!=null){
						if(s.getStaffType().getObjectType() == StaffRoleType.GSNPP.getValue()
								&& session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE)!=null){
							Shop shop = (Shop)session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
							shopId = shop.getId();
						}else{
							shopId = s.getShop().getId();
						}
					}*/
					ShopToken shToken = currentUser.getShopRoot();
					if (shToken != null) {
						shopId = shToken.getShopId();
					}
					listShopId.add(shopId);
				}
				boolean b = false;
				if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
					b = true;
				}
				long parentStaffId = currentUser.getStaffRoot().getStaffId();
				ObjectVO<CustomerDisplayProgrameVO> listCustomerVO = displayProgramMgr.getListCustomerDisplayProgrameEX(kPaging, listShopId, id, month, b, parentStaffId);
				if(listCustomerVO != null) {
				    result.put("total", listCustomerVO.getkPaging().getTotalRows());
				    result.put("rows", listCustomerVO.getLstObject());	
				}	
//			}
			//updateTotalRowGrid();
		}
		catch(Exception ex) {
			LogUtility.logError(ex,"DisplayProgramAction.searchCustomerDisplayProgram");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	public String exportCTTB_Customer(){
		try {
			Shop shop = null;
			if(shopId != null && shopId > 0){
				shop = shopMgr.getShopById(shopId);
				if(shop==null){
					return SUCCESS;
				}
			}
			//HashMap<String, Object> beans = new HashMap<String, Object>();
			//StDisplayProgram dp = null;
//			if(!StringUtil.isNullOrEmpty(displayProgramCode)){
//				dp = displayProgramMgr.getDisplayProgrameByCode(displayProgramCode);
//			}
			/*if(id != null){
				dp = displayProgramMgr.getStDisplayProgramById(id);
			}*/
			String[] temp = null;
			listShopId = new ArrayList<Long>();
			if(listShopIdString != null){
				temp = listShopIdString.split(",");
				if(temp != null){
					try{
						for(int i = 0;i<temp.length;i++){
							Long shopIdTemp = Long.parseLong(temp[i]);
							if (checkShopInOrgAccessById(shopIdTemp)) {
								listShopId.add(shopIdTemp);
							}
						}
					}catch(Exception ex){
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
						return SUCCESS;
					}
					
				}
			}
			boolean b = false;
			if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
				b = true;
			}
			long parentStaffId = currentUser.getStaffRoot().getStaffId();
			ObjectVO<CustomerDisplayProgrameVO> lst = displayProgramMgr.getListCustomerDisplayProgrameEX(null, listShopId, id, month, b, parentStaffId);
			if (lst.getLstObject() != null && lst.getLstObject().size()>0) {
				List<CustomerDisplayProgrameVO> datasource = lst.getLstObject();
				//Create Template
				String outputName = ConstantManager.EXPORT_EXCEL_DISPLAY_PROGRAM_CUSTOMER + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)+".xls";  
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				File file = new File(exportFileName);
				WorkbookSettings wbSettings = new WorkbookSettings();
				wbSettings.setLocale(new Locale("vi", "VI"));
				WritableWorkbook workbook = jxl.Workbook.createWorkbook(file, wbSettings);
				
				Map<String, WritableCellFormat> myWorkbook = ExcelJXLSAPIUtils.createStylesWritableWorkbook(workbook);
				workbook.createSheet(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.sheet.name"), 0);
				jxl.write.WritableSheet excelSheet = workbook.getSheet(0);
				//Size Row
				excelSheet.setRowView(0, 15*20);
				excelSheet.setRowView(1, 25*20);//For Title
				excelSheet.setRowView(2, 15*20);//For Title
				//Size Column
				int c = 0, d =0;
				for(c=0; c<10; c++){
					excelSheet.setColumnView(c, 30);
				}
				excelSheet.setColumnView(0, 6);//STT
				excelSheet.setRowView(0, 25*20);
			    //HEADER - CO DINH
			    excelSheet.addCell(new Label(0, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.STT"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(1, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.maCTTB"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(2, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.mucCTTB"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(3, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.maNPP"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(4, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.nvbb"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(5, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.khachHang"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(6, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.diaChi"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(7, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.thang"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(8, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.ngayTao"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
			    excelSheet.addCell(new Label(9, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"expExcel.general.header.ngayDieuChinh"), myWorkbook.get(ExcelJXLSAPIUtils.HEADER_GRAY_ALL_THIN)));
				//PROFESSIONALLY PROCESS
			    for(d=0; d<datasource.size(); d++){
			    	datasource.get(d).safeSetNull();
			    	excelSheet.setRowView(d+2, 15*20);
			    	excelSheet.addCell(new jxl.write.Label(0, d+2, String.valueOf(d+1), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_CENTRE)));
			    	excelSheet.addCell(new jxl.write.Label(1, d+2, String.valueOf(datasource.get(d).getDisplayProgramCode()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_LEFT)));
			    	excelSheet.addCell(new jxl.write.Label(2, d+2, String.valueOf(datasource.get(d).getLevelCode()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_LEFT)));
			    	excelSheet.addCell(new jxl.write.Label(3, d+2, String.valueOf(datasource.get(d).getShopCode()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_LEFT)));
			    	excelSheet.addCell(new jxl.write.Label(4, d+2, String.valueOf(datasource.get(d).getStaffName()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_LEFT)));
			    	excelSheet.addCell(new jxl.write.Label(5, d+2, String.valueOf(datasource.get(d).getCustomerName()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_LEFT)));
			    	excelSheet.addCell(new jxl.write.Label(6, d+2, String.valueOf(datasource.get(d).getHouseNumber()+'-'+datasource.get(d).getStreet()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_LEFT)));
			    	excelSheet.addCell(new jxl.write.Label(7, d+2, String.valueOf(datasource.get(d).getFromDate()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_CENTRE)));
			    	excelSheet.addCell(new jxl.write.Label(8, d+2, String.valueOf(datasource.get(d).getOneMonth()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_CENTRE)));
			    	excelSheet.addCell(new jxl.write.Label(9, d+2, String.valueOf(datasource.get(d).getUpdateDate()), myWorkbook.get(ExcelJXLSAPIUtils.ROW_ALL_THIN_CENTRE)));
			    	
			    }
				workbook.write();
			    workbook.close();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(LIST, outputPath);
				result.put(ERROR, false);
				result.put("hasData", true);
			} else {
				result.put("hasData", false);
				result.put(ERROR, false);
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}
	
	public String importCustomer() throws Exception {
		/*try {
		    isError = true;
	        totalItem = 0;
	        lstView = new ArrayList<CellBean>();
	        typeView = true;
	        String message = "";
	        //String duplicated_msg = "";
	        String msg;
	        boolean flag = true;
	        List<CellBean> lstFails = new ArrayList<CellBean>();
	        if(StringUtil.isNullOrEmpty(excelFileContentType)){
	            excelFileContentType = "application/vnd.ms-excel";
	        }
	        List<List<String>> lstData = getExcelData(excelFileCus, excelFileContentType, errMsg,6);
	        if ((lstData == null || lstData.size() == 0) && StringUtil.isNullOrEmpty(errMsg)) {
	            errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.display.program.nodata");
	            return ERROR;
	        }
	        boolean isExistShop = true;//checkExistShopOfUserLogin();
	        if(isExistShop == false) {
	            errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP);
	            return ERROR;
	        }
	        
	        boolean checkMap = false;
	    	if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
	    		checkMap = true;
	    	}
	    	long parentStaffId = currentUser.getStaffRoot().getStaffId();
	    	ShopToken shToken = currentUser.getShopRoot();
	    	
	        if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
                CustomerDisplayPrograme  customerDisplayPrograme = new CustomerDisplayPrograme();
                Shop shop = null;
                Staff staff = null;
                Customer customer = null;
                StDisplayProgramLevel displayProgrameLevel = null;

                BasicFilter<BasicVO> filter = new BasicFilter<BasicVO>();
				filter.setParentId(parentStaffId);
				filter.setInheritUserPriv(filter.getParentId());
				filter.setFlagCMS(true);
				
                for(int i=0;i<lstData.size();i++){
                    if(lstData.get(i)!= null && lstData.get(i).size() > 0){
                        message= "";
                        totalItem++;
                        message = check2(lstData, i);
                        List<String> row = lstData.get(i);
                        String value = "";
                        Date ffDate = null;
                        StDisplayStaffMap stdmInsert = null;
                        //Boolean isExistDB=true;
                        if(StringUtil.isNullOrEmpty(message) && row!=null && row.size()>=6){
                            StDisplayProgram  stdp = null;
                            if(row.size() >0){
                                value = row.get(0);
                                msg = ValidateUtil.validateField(value, "customer.display.program.code", 50,ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH);
                                if(!StringUtil.isNullOrEmpty(msg)){
                                    message += msg;
                                }
                                stdp = displayProgramMgr.getStDisplayProgramByCode(value);
                                if(stdp != null){
                                    id = stdp.getId();
                                    if(stdp.getToDate() != null && DateUtil.compareDateWithoutTime(stdp.getToDate(),DateUtil.now()) == -1){
                                        message +=Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "display.program.error.date");
                                    }
                                    if(stdp.getStatus()==null || stdp.getStatus().getValue() != ActiveType.RUNNING.getValue()){
                                        message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.display.programe.status.over.active");
                                    }
                                }
                                if(stdp!=null){
                                    customerDisplayPrograme.setDisplayProgramCode(stdp.getDisplayProgramCode().toUpperCase());
                                }else{
                                    if(StringUtil.isNullOrEmpty(message)){
                                        message +=Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.not.exists");
                                    }
                                    
                                }
                            }
                            //Muc CTTB                          
                            if(StringUtil.isNullOrEmpty(message) && row.size()>1 ){
                                value = row.get(1);                              
                                msg = ValidateUtil.validateField(value, "customer.display.program.level", 50,ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH);
                                if(!StringUtil.isNullOrEmpty(msg)){
                                    message += msg;
                                }else if(stdp!=null){                               
                                    displayProgrameLevel = displayProgramMgr.checkLevelCodeExists(id,value.toUpperCase());
                                    if(displayProgrameLevel==null){
                                        //Muc khong thuoc CTTB
                                        message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.level.not.shop");
                                    }else if(displayProgrameLevel.getStatus()== null || !displayProgrameLevel.getStatus().equals(ActiveType.RUNNING)){
                                        message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.level.status.over.active");
                                    }
                                    customerDisplayPrograme.setStDisplayProgramLevel(displayProgrameLevel);
                                }
                                customerDisplayPrograme.setLevelCode(value.toUpperCase());
                                
                            }
                            
                            //Ma NPP
                            if(StringUtil.isNullOrEmpty(message) && row.size()>2){
                                value = row.get(2);
                                msg = ValidateUtil.validateField(value, "customer.display.program.npp.code", 50,ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
                                    if(!StringUtil.isNullOrEmpty(msg)){
                                        message += msg;
                                    }else{
                                        shop = shopMgr.getShopByCode(value);
                                        //Date date = DateUtil.getFirstDateInMonth(commonMgr.getSysDate());
                                        if (shop == null) {
                                            message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST,true, "customer.display.program.npp.code");
                                        }else if(!shop.getType().getObjectType().equals(ShopObjectType.GT.getValue())
        										&& !shop.getType().getObjectType().equals(ShopObjectType.MIEN.getValue())
        										&& !shop.getType().getObjectType().equals(ShopObjectType.VUNG.getValue())
        										&& !shop.getType().getObjectType().equals(ShopObjectType.NPP.getValue())){
                                        	message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.not.apply.shop");
                                        }else if(!ShopObjectType.NPP.getValue().equals(shop.getType().getObjectType())){                                      
                                            message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.npp.not.exists");
                                        }else if(null == shop.getStatus() || !ActiveType.RUNNING.equals(shop.getStatus())){
                                        	message += ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_ACTIVE, "");
                                        }else{
                                        	if(!checkShopInOrgAccessByCode(shop.getShopCode())){
                                        		message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.not.npp.shop");
                                        	}
                                        }
                                        if(shop != null && StringUtil.isNullOrEmpty(message) && displayProgramMgr.checkExistRecordDisplayShopMap(shop.getId(),id) == false){
                                            message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.program.not.in.shop");
                                        }
                                    }
                            }
                            // Ma NVBH
                            if(StringUtil.isNullOrEmpty(message) && row.size()>3){
                                value = row.get(3);
                                msg = ValidateUtil.validateField(value, "customer.display.program.staff.code", 50,ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
                                    if(!StringUtil.isNullOrEmpty(msg)){
                                        message += msg;
                                    }else{
                                        staff = staffMgr.getStaffByCode(value);
                                        if (staff == null) {
                                            message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST,true, "customer.display.program.staff.code");
                                        }else if((null != staff.getStaffType() && !staff.getStaffType().getObjectType().equals(StaffRoleType.NVBH_ON_VAN.getValue()) &&
                                        		!staff.getStaffType().getObjectType().equals(StaffRoleType.NVBH_PRE.getValue())) || null == staff.getStaffType()){
                                            // Nhan vien khong phai la nhan vien ban hang.
                                            message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.display.program.not.sale.staff");
                                        }else if(shop!=null && !staff.getShop().getId().equals(shop.getId())){
                                            //khong phai NVBH cua nha phan phoi
                                            message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.display.program.staff.not.shop");
                                        }else if(staff.getStatus()==null || !staff.getStatus().equals(ActiveType.RUNNING)){
                                            // Ma NVBH het hieu luc
                                            message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.display.programe.sale.staff.over.active");
//                                          message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,true, "customer.display.program.staff.code");
                                        } else {
                                        	filter.setId(staff.getId());
        									filter.setShopRootId(shToken.getShopId());
        									if ((checkMap && !commonMgr.checkStaffInCMSByFilterWithCMS(filter))
        											|| !checkShopInOrgAccessById(staff.getShop().getId())) {
        										message += "Mã NVBH không thuộc quản lý của user đăng nhập." + "\n";
        									}
                                        }
                                        String month = "";
                                        if(row.size() > 5){
                                            month = row.get(5);
                                        }
                                        ffDate = DateUtil.parse(month, DateUtil.DATE_FORMAT_STR);
                                        if(staff!= null && StringUtil.isNullOrEmpty(message) && staff.getStatus().getValue() == ActiveType.RUNNING.getValue()){

                                            StDisplayStaffMap stdm = new StDisplayStaffMap();
                                            
                                            stdm = displayProgramMgr.getDisplayStaffMap(id, staff.getId(), ffDate);
                                            if(stdm != null){
                                                if(stdm.getQuantityMax() != null && stdm.getQuantityReceived() != null && stdm.getQuantityMax() == stdm.getQuantityReceived()){
                                                    message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.display.program.out.of.quantity");
                                                }else{
                                                    customerDisplayPrograme.setStDisplayStaffMap(stdm);
                                                }
                                            }else{
                                            }
                                        }
                                        customerDisplayPrograme.setStaff(staff);
                                    }
                                
                            }
                            //Ma KH:
                            if(StringUtil.isNullOrEmpty(message) && row.size()>4){
                                value = row.get(4);
                                msg = ValidateUtil.validateField(value, "customer.display.program.customer.code", 50,ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
                                    if(!StringUtil.isNullOrEmpty(msg)){
                                        message += msg;
                                    }else if(shop!=null) {
                                        String customerCode = StringUtil.getFullCode(shop.getShopCode(), value);
                                        customer = customerMgr.getCustomerByCode(customerCode);
                                        if (customer == null) {
                                            message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST,true, "customer.display.program.customer.code");
                                        }else if(customer.getStatus()==null || !customer.getStatus().equals(ActiveType.RUNNING)){
                                            //KH da het hieu luc
                                            message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.programe.customer.over.active");
//                                          message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,true, "customer.display.program.customer.code");
                                        }else if(!customer.getShop().getId().equals(shop.getId())){
                                            // Khong phai KH cua NPP
                                            message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.customer.not.shop");
                                        }
                                        customerDisplayPrograme.setCustomer(customer);
                                    }
                            }
                            
                            // Ngay
                            if(StringUtil.isNullOrEmpty(message) && row.size()>5){
                                value = row.get(5);
                                msg = ValidateUtil.validateField(value, "customer.display.program.date", 10,ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH);
                                    if(!StringUtil.isNullOrEmpty(msg)){
                                        message += msg;
                                    }else{
                                    	msg =  ValidateUtil.getErrorMsgForInvalidFormatDate(value, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.display.program.date"), false);
                                        if(StringUtil.isNullOrEmpty(msg)){
                                            Date date = DateUtil.parse(value,DateUtil.DATE_FORMAT_STR);
                                            // Ngay import truoc FromDate
                                            if(StringUtil.isNullOrEmpty(message) && stdp.getFromDate()!= null && DateUtil.compareDateWithoutDay(stdp.getFromDate(),date)> 0 ){
                                                message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.date.before.from.data");
                                            }
                                            //Ngay import sau ToDate
                                            if(StringUtil.isNullOrEmpty(message) && stdp.getToDate()!= null && DateUtil.compareDateWithoutDay(stdp.getToDate(),date)< 0 ){
                                                message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.date.after.to.data");
                                            }
                                            //Ngay import >= thang hien tai
                                            if(StringUtil.isNullOrEmpty(message) && DateUtil.compareDateWithoutDay(DateUtil.now(),date)>0){
                                                message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"customer.display.program.date.before.now");
                                            }
                                            date = DateUtil.getFirstDateInMonth(date);
                                            customerDisplayPrograme.setFromDate(date);
                                        }else{
                                            message +=msg;
                                        }
                                    }
                            }

                            if(StringUtil.isNullOrEmpty(message) && customer!=null && stdp!=null && staff!=null && displayProgrameLevel!= null){
                                if(customer.getId()!=null && stdp.getDisplayProgramCode()!=null && staff.getId()!=null && displayProgrameLevel.getLevelCode()!=null ){
                                    Date firstDate = DateUtil.getFirstDateInMonth(ffDate);
                                    Date lastDate = DateUtil.getLastDateInMonth(ffDate);
                                    String dateString = DateUtil.toDateString(firstDate, DateUtil.DATE_FORMAT_STR);
                                    CustomerDisplayPrograme cdpr = displayProgramMgr.checkExistDisplayCustomer(id, customer.getId(), staff.getId(), displayProgrameLevel.getLevelCode(), dateString);
                                    if(cdpr == null){
                                        cdpr = displayProgramMgr.checkExistDisplayCustomer(id, customer.getId(), null, displayProgrameLevel.getLevelCode(), dateString);
                                        if(cdpr == null){
                                            cdpr = displayProgramMgr.checkExistDisplayCustomer(id, customer.getId(), staff.getId(), null, dateString);
                                            if(cdpr == null){ // kiem tra co trung nhan vien ko
                                            	cdpr = displayProgramMgr.checkExistDisplayCustomer(id, customer.getId(), null, null, dateString);
                                            	StDisplayStaffMap oldStaffMap  = null;
                                            	if(cdpr == null){
                                            		cdpr = new CustomerDisplayPrograme();
                                            		cdpr.setCustomer(customer);
                                            		cdpr.setFromDate(firstDate);
                                            		cdpr.setToDate(lastDate);
                                            		cdpr.setCreateDate(DateUtil.now());
                                            		cdpr.setCreateUser(getStaffByCurrentUser().getStaffCode());
                                            	}else{
                                            		cdpr.setUpdateDate(DateUtil.now());
                                            		cdpr.setUpdateUser(getStaffByCurrentUser().getStaffCode());
                                            		oldStaffMap = cdpr.getStDisplayStaffMap();
                                            	}
                                            	cdpr.setStDisplayProgramLevel(displayProgrameLevel);
                                                StDisplayStaffMap stdm = new StDisplayStaffMap();
                                                stdm = displayProgramMgr.getDisplayStaffMap(id, staff.getId(), ffDate);
                                                if(stdm==null){
                                                    StDisplayStaffMap  stdm1 = new StDisplayStaffMap();
                                                    stdm1.setMonth(firstDate);
                                                    stdm1.setQuantityMax(null);
                                                    stdm1.setStaff(staff);
                                                    stdm1.setQuantityReceived(0);
                                                    stdm1.setCreateDate(DateUtil.now());
                                                    stdm1.setCreateUser(currentUser.getUserName());
                                                    stdm1.setDisplayProgram(stdp);
                                                    stdmInsert = displayProgramMgr.createDisplayStaffMap(stdm1, getLogInfoVO());
                                                    customerDisplayPrograme.setStDisplayStaffMap(stdmInsert);
                                                    cdpr.setStDisplayStaffMap(stdmInsert);
                                                }else{
                                                	cdpr.setStDisplayStaffMap(stdm);
                                                }
                                                //cdp.setStDisplayStaffMap(stdm);
                                                if(cdpr.getId() == null){
                                                	displayProgramMgr.createCustomerDisplayPrograme(cdpr);
                                                }else{
                                                	displayProgramMgr.updateCustomerDisplayPrograme(cdpr);
                                                }
                                                Integer qtt = 0;
                                                Integer qttOld = 0;
                                                if(oldStaffMap != null){
                                                	qttOld = (oldStaffMap.getQuantityReceived() != null)?oldStaffMap.getQuantityReceived() - 1:null;
                                                	oldStaffMap.setQuantityReceived(qttOld);
                                                	displayProgramMgr.updateDisplayStaffMap(oldStaffMap, getLogInfoVO());
                                                }
                                                if(stdm != null){
                                                    qtt =(stdm.getQuantityReceived() != null)?stdm.getQuantityReceived() +1:null;
                                                    stdm.setQuantityReceived(qtt);
                                                    displayProgramMgr.updateDisplayStaffMap(stdm, getLogInfoVO());
                                                }
                                                else{
                                                    qtt =(stdmInsert.getQuantityReceived() != null)?stdmInsert.getQuantityReceived() +1:null;
                                                    stdmInsert.setQuantityReceived(qtt);
                                                    //stdmInsert.setUpdateDate(DateUtil.now());
                                                    //stdmInsert.setUpdateUser(currentUser.getUserName());
                                                    displayProgramMgr.updateDisplayStaffMap(stdmInsert, getLogInfoVO());
                                                }
                                                
                                            }
                                            else{
                                                cdpr.setUpdateDate(DateUtil.now());
                                                cdpr.setUpdateUser(getStaffByCurrentUser().getStaffCode());
                                                //cdpr.setLevelCode(customerDisplayPrograme.getLevelCode());
                                                cdpr.setStDisplayProgramLevel(customerDisplayPrograme.getStDisplayProgramLevel());
                                                Staff s1 = staffMgr.getStaffByCode(row.get(3));
                                                displayProgramMgr.updateCustomerDisplayPrograme(cdpr);
                                                
                                                Long idOld = s1.getId() ;
                                                Long idNew = customerDisplayPrograme.getStDisplayStaffMap().getStaff().getId();
                                                if(idOld.compareTo(idNew) != 0){
                                                    StDisplayStaffMap st1 = displayProgramMgr.getDisplayStaffMap(id, s1.getId(), ffDate);
                                                    StDisplayStaffMap st2 = displayProgramMgr.getDisplayStaffMap(id, customerDisplayPrograme.getStDisplayStaffMap().getStaff().getId(), ffDate);
                                                    
                                                    int qtt1 = (st1.getQuantityReceived() != null)?st1.getQuantityReceived() +1:null;
                                                    int qtt2 = (st2.getQuantityReceived() != null)?st2.getQuantityReceived() -1:null;
                                                    st1.setQuantityReceived(qtt1);
                                                    st2.setQuantityReceived(qtt2);
                                                    
                                                    displayProgramMgr.updateDisplayStaffMap(st1, getLogInfoVO());
                                                    displayProgramMgr.updateDisplayStaffMap(st2, getLogInfoVO());
                                                }
                                            }
                                        }else{//kiem tra co trung levelCode ko
                                            cdpr.setUpdateDate(DateUtil.now());
                                            cdpr.setUpdateUser(getStaffByCurrentUser().getStaffCode());
                                            //cdpr.setLevelCode(customerDisplayPrograme.getLevelCode());
                                            cdpr.setStDisplayProgramLevel(customerDisplayPrograme.getStDisplayProgramLevel());
                                            
                                            CustomerDisplayPrograme cdpr1 = displayProgramMgr.checkExistDisplayCustomer(id, customer.getId(), staff.getId(), null, dateString);
                                            //StDisplayStaffMap stdm = displayProgramMgr.getDisplayStaffMap(id, staff.getStaffId(), ffDate);
                                            if( cdpr1!= null){
                                                Staff s1 = staffMgr.getStaffByCode(row.get(3));
                                                
                                                Long idOld = s1.getId() ;
                                                Long idNew = cdpr.getStDisplayStaffMap().getStaff().getId();
                                                if(idOld.compareTo(idNew) != 0){
                                                    StDisplayStaffMap st1 = displayProgramMgr.getDisplayStaffMap(id, s1.getId(), ffDate);
                                                    StDisplayStaffMap st2 = displayProgramMgr.getDisplayStaffMap(id, cdpr.getStDisplayStaffMap().getStaff().getId(), ffDate);
                                                    
                                                    int qtt1 = (st1.getQuantityReceived() != null)?st1.getQuantityReceived() +1:null;
                                                    int qtt2 = (st2.getQuantityReceived() != null)?st2.getQuantityReceived() -1:null;
                                                    st1.setQuantityReceived(qtt1);
                                                    st2.setQuantityReceived(qtt2);
                                                    
                                                    displayProgramMgr.updateDisplayStaffMap(st1, getLogInfoVO());
                                                    displayProgramMgr.updateDisplayStaffMap(st2, getLogInfoVO());
                                                }
                                            }else{
                                                StDisplayStaffMap stdm = displayProgramMgr.getDisplayStaffMap(id, staff.getId(), ffDate);
                                                if(stdm == null){
                                                    StDisplayStaffMap  stdm1 = new StDisplayStaffMap();
                                                    stdm1.setMonth(firstDate);
                                                    stdm1.setQuantityMax(null);
                                                    stdm1.setStaff(staff);
                                                    stdm1.setQuantityReceived(1);
                                                    stdm1.setCreateDate(DateUtil.now());
                                                    stdm1.setCreateUser(currentUser.getUserName());
                                                    stdm1.setDisplayProgram(stdp);
                                                    stdmInsert = displayProgramMgr.createDisplayStaffMap(stdm1, getLogInfoVO());
                                                    StDisplayStaffMap st1 = cdpr.getStDisplayStaffMap();
                                                    st1.setQuantityReceived(st1.getQuantityReceived() -1);
                                                    displayProgramMgr.updateDisplayStaffMap(st1, getLogInfoVO());
                                                    cdpr.setStDisplayStaffMap(stdmInsert);
                                                }else{
                                                    Staff s1 = staffMgr.getStaffByCode(row.get(3));
                                                    
                                                    Long idOld = s1.getId() ;
                                                    Long idNew = cdpr.getStDisplayStaffMap().getStaff().getId();
                                                    if(idOld.compareTo(idNew) != 0){
                                                        StDisplayStaffMap st1 = displayProgramMgr.getDisplayStaffMap(id, s1.getId(), ffDate);
                                                        StDisplayStaffMap st2 = displayProgramMgr.getDisplayStaffMap(id, cdpr.getStDisplayStaffMap().getStaff().getId(), ffDate);
                                                        
                                                        int qtt1 = (st1.getQuantityReceived() != null)?st1.getQuantityReceived() +1:null;
                                                        int qtt2 = (st2.getQuantityReceived() != null)?st2.getQuantityReceived() -1:null;
                                                        st1.setQuantityReceived(qtt1);
                                                        st2.setQuantityReceived(qtt2);
                                                        
                                                        displayProgramMgr.updateDisplayStaffMap(st1, getLogInfoVO());
                                                        displayProgramMgr.updateDisplayStaffMap(st2, getLogInfoVO());
                                                        cdpr.setStDisplayStaffMap(st1);
                                                    }
                                                }
                                                
                                            }
                                            displayProgramMgr.updateCustomerDisplayPrograme(cdpr);
                                        }
                                    }
                                }   
                            }
                        }
                                
                        if (!StringUtil.isNullOrEmpty(message)) {   
                            flag = false;   
                            lstFails.add(StringUtil.addFailBean(row, message));
                        } else {
                            importProgram = "customer";
                        }
                    }
                }
                getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CUSTOMER_DISPLAY_PROGRAM_FAIL);
	            if (StringUtil.isNullOrEmpty(errMsg) && flag) {
	                isError = false;
	                return SUCCESS;
	            }
	        }
        }catch (Exception e) {
            errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
            LogUtility.logError(e, e.getMessage());
            isError = true;
        }*/
        return SUCCESS;
	}
	
	public String deleteCustomer() throws Exception {
		try{
			String[] temp = null;
			listShopId = new ArrayList<Long>();
			if(StringUtil.isNullOrEmpty(listShopIdString)==false){
				temp = listShopIdString.split(",");
				if(temp != null){
					try{
						for(int i = 0;i<temp.length;i++){
							Long shopIdTemp = Long.parseLong(temp[i]);
							if (checkShopInOrgAccessById(shopIdTemp)) {
								listShopId.add(shopIdTemp);
							}
						}
					}catch(Exception ex){
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
						return JSON;
					}
					
				}
			}else{
				/*Staff s = staffMgr.getStaffByCode(currentUser.getUserName());
				if(s!=null){
					shopId = s.getShop().getId();
				}*/
				ShopToken shToken = currentUser.getShopRoot();
				if (shToken != null) {
					shopId = shToken.getShopId();
					listShopId.add(shopId);
				}
			}
			if(isCheckAll!=null && isCheckAll && listShopId!=null){
				lstCustomerId = new ArrayList<Long>();
				boolean b = false;
				if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
					b = true;
				}
				long parentStaffId = currentUser.getStaffRoot().getStaffId();
				ObjectVO<CustomerDisplayProgrameVO> listCustomerVO = displayProgramMgr.getListCustomerDisplayProgrameEX(null, listShopId, id, month, b, parentStaffId);
				if(listCustomerVO != null) {
				    for(CustomerDisplayProgrameVO vo:listCustomerVO.getLstObject()){
				    	lstCustomerId.add(vo.getId());
				    }
				}	
			}
			if(lstCustomerId != null && lstCustomerId.size() >0){
				for(int i=0;i<lstCustomerId.size();i++){
					CustomerDisplayPrograme cdp = displayProgramMgr.getCustomerDisplayProgrameById(lstCustomerId.get(i));
					if(cdp!=null){						
						cdp.setStatus(ActiveType.DELETED);
						cdp.setUpdateDate(DateUtil.now());
						cdp.setUpdateUser(currentUser.getUserName());
						displayProgramMgr.updateCustomerDisplayPrograme(cdp);
						StDisplayStaffMap stdm = cdp.getStDisplayStaffMap();
						if(stdm != null && stdm.getQuantityReceived() != null){
							int qtt = stdm.getQuantityReceived() - 1;
							stdm.setQuantityReceived(qtt);
							stdm.setUpdateDate(DateUtil.now());
							stdm.setUpdateUser(currentUser.getUserName());
							displayProgramMgr.updateDisplayStaffMap(stdm, getLogInfoVO());
						}
					}
				}
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public String checkDuplicateCustomer(List<List<String>> lstData, int k){
		String mes = "";
		if(lstData.get(k)== null || lstData.get(k).size() < 5){
			mes += "1";//Du lieu khong hop le
		}else{
			String maCTTB = lstData.get(k).get(0).trim();
			String maNPP = lstData.get(k).get(1).trim();
			String nVBH = lstData.get(k).get(2).trim();
			String maKH = lstData.get(k).get(3).trim();
			String ngay = lstData.get(k).get(4).trim();
			if(StringUtil.isNullOrEmpty(maCTTB) || StringUtil.isNullOrEmpty(maNPP)|| StringUtil.isNullOrEmpty(nVBH)|| StringUtil.isNullOrEmpty(maKH)|| StringUtil.isNullOrEmpty(ngay) ){
				mes += "1";//Du lieu khong hop le
			}else{
				for(int i=0;i<lstData.size();i++){
					if(i!= k && lstData.get(i)!= null && lstData.get(i).size() >= 5){
						List<String> row = lstData.get(i);
						if(maCTTB.equals(row.get(0).trim()) && maNPP.equals(row.get(1).trim()) && nVBH.equals(row.get(2).trim()) && maKH.equals(row.get(3).trim()) && ngay.equals(row.get(4).trim()) ){
//							if(isView == 0)
								mes += (i+2) + " , ";//stt dong import khac voi dong xem
//							else
//								mes += (i+1) + " , ";
						}
					}
				}
				if(mes.length()>0) 
					mes = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.display.program.import.duplicate") + mes.substring(0, mes.length()-3);
			}
		}
		return mes;
	}
	
	/**
	 * @author thachnn
	 * @return
	 */
	public String search(){
	    
	   /* result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<StDisplayProgram> kPaging = new KPaging<StDisplayProgram>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);		
			Date froDate = null;
			Date tDate = null;
			if(!StringUtil.isNullOrEmpty(fromDate)){
				froDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_STR);
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_STR);			
			}
			if((!StringUtil.isNullOrEmpty(fromDate) && fromDate == null) || (!StringUtil.isNullOrEmpty(toDate) && toDate==null)){
				 result.put("total", null);
				 result.put("rows",null);
			} else {
				StDisplayProgramFilter filter = new StDisplayProgramFilter();
				filter.setDisplayProgramCode(code);
				filter.setDisplayProgramName(name);
				if(currentUser != null){
					Staff s = staffMgr.getStaffByCode(currentUser.getUserName());
					if(s != null && s.getShop() != null){
						filter.setShopId(s.getShop().getId());
					}
				}
				
				filter.setStatus(ActiveType.parseValue(status));
				filter.setFromDate(froDate);
				filter.setToDate(tDate);
				StaffRoleType staffRoleType = null;
				
				Staff userLogin = getStaffByCurrentUser();
				if(userLogin==null ){
					return JSON;
				}
				if(userLogin.getStaffType().getObjectType() == StaffRoleType.VNM.getValue()){
					///
					permissionUser = true;
					staffRoleType = StaffRoleType.VNM;
				}
				else if(userLogin.getStaffType().getObjectType() == StaffRoleType.GSNPP.getValue()){
					//
					permissionUser = false;
					staffRoleType = StaffRoleType.GSNPP;
					if(session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE)!=null){
						Shop s = (Shop)session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
						filter.setShopId(s.getId());
					}
					
				}else if(userLogin.getStaffType().getObjectType() == StaffRoleType.KTNPP.getValue()){
					//
					permissionUser = false;
					staffRoleType = StaffRoleType.KTNPP;
				}
				
				ObjectVO<StDisplayProgram> displayProgramVO = displayProgramMgr.getListDisplayProgram(kPaging,filter,staffRoleType);			
				
				if(displayProgramVO!= null){
				    result.put("total", displayProgramVO.getkPaging().getTotalRows());
				    result.put("rows", displayProgramVO.getLstObject());		
				}
			}			    		
	    }catch (Exception e) {
		LogUtility.logError(e, e.getMessage());
	    }*/
	    return JSON;
	}

	/**
	 * Search exclusion program.
	 *
	 * @return the string
	 * @author loctt
	 * @since Sep 17, 2013
	 */
	public String searchExclusionProgram(){
		result.put("page", page);
		result.put("max", max);
		try
		{
//			KPaging<StDisplayProgramExclusion> kPaging = new KPaging<StDisplayProgramExclusion>();
//			kPaging.setPageSize(max);
//			kPaging.setPage(page-1);
			
			DisplayProgramExclusionFilter filter = new DisplayProgramExclusionFilter();
			filter.setDisplayProgramExclusionCode(code);
			filter.setDisplayProgramExclusionName(name);
			filter.setDisplayProgramId(id);
			filter.setStatus(ActiveType.RUNNING);
			StaffRole role = null;
			if(checkRoles(VSARole.VNM_SALESONLINE_HO)) role = StaffRole.VNM_SALESONLINE_HO;
			else if(checkRoles(VSARole.VNM_SALESONLINE_GS)) role =StaffRole.VNM_SALESONLINE_GS;
			ObjectVO<StDisplayProgramExclusion> displayProgramVO = displayProgramMgr.getListDisplayProgramExclusion(null,filter,role);			
			if(displayProgramVO!= null){
	//		    result.put("total", displayProgramVO.getkPaging().getTotalRows());
			    result.put("rows", displayProgramVO.getLstObject());		
			}
			
		}
		catch(Exception e){
			LogUtility.logError(e,"Error ProgrammeDisplayCatalogAction.searchExclusionProgram");
			result.put(ERROR, true);
			result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	/**
	 * Search exclusion program popup.
	 *
	 * @return the string
	 * @author loctt
	 * @since Sep 17, 2013
	 */
	public String searchExclusionProgramPopup(){
		result.put("page", page);
		result.put("max", max);
		try
		{
			KPaging<StDisplayProgram> kPaging = new KPaging<StDisplayProgram>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			
			DisplayProgramFilter filter = new DisplayProgramFilter();
			filter.setDisplayProgramCode(code);
			filter.setDisplayProgramName(name);
			filter.setDisplayProgramId(id);
			filter.setStatus(ActiveType.RUNNING);
			ObjectVO<StDisplayProgram> displayProgramVO = displayProgramMgr.getListDisplayProgramForExclusion(kPaging,filter);			
			if(displayProgramVO!= null){
			    result.put("total", displayProgramVO.getkPaging().getTotalRows());
			    result.put("rows", displayProgramVO.getLstObject());		
			}
			
		}
		catch(Exception e){
			LogUtility.logError(e,"Error ProgrammeDisplayCatalogAction.searchExclusionProgramPopup");
			result.put(ERROR, true);
			result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	/**
	 * @author LocHP
	 * @return
	 */
	public String uploadPdfFile(){
		//FormFile file = new FormFile();
		System.out.println("upload file");
		
		if (this.pdfFile != null){
			System.out.println("da lum dc file + "+ this.pdfFile.getName() + " ___ "+ this.fakePdffilepc);
		}
		return SUCCESS;
	}


	public void setPdfFile(File pdfFile) {
		this.pdfFile = pdfFile;
	}


	public File getPdfFile() {
		return pdfFile;
	}


	public void setFakePdffilepc(String fakePdffilepc) {
		this.fakePdffilepc = fakePdffilepc;
	}


	public String getFakePdffilepc() {
		return fakePdffilepc;
	}
	/**
	 * Creates the exclusion program.
	 *
	 * @return the string
	 * @author loctt
	 * @since Sep 17, 2013
	 */
	private List<Long> listExclusionId;
	public List<Long> getListExclusionId() {
		return listExclusionId;
	}


	public void setListExclusionId(List<Long> listExclusionId) {
		this.listExclusionId = listExclusionId;
	}


	public String createExclusionProgram(){
		try
		{
			StDisplayProgram displayProgram = displayProgramMgr.getStDisplayProgramById(id);
			if(displayProgram == null){
				result.put(ERROR, true);
		//		result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FOCUS_PROGRAM_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.not.exists")));
				return JSON;
			}
			displayProgramMgr.createListDisplayProgramExclusion(displayProgram, listExclusionId, getLogInfoVO());
		}
		catch(Exception e){
			LogUtility.logError(e,"Error ProgrammeDisplayCatalogAction.createExclusionProgram");
			result.put(ERROR, true);
			result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	/**
	 * Delete exclusion program.
	 *
	 * @return the string
	 * @author loctt
	 * @since Sep 18, 2013
	 */
	public String deleteExclusionProgram(){
		try
		{
			StDisplayProgramExclusion displayProgramExclusion = displayProgramMgr.getDisplayProgramExclusionById(id);
			if(displayProgramExclusion == null){
				result.put(ERROR, true);
				return JSON;
			}
			displayProgramExclusion.setStatus(ActiveType.DELETED);
			displayProgramMgr.updateDisplayProgramExclusion(displayProgramExclusion, getLogInfoVO());
		}
		catch(Exception e){
			LogUtility.logError(e,"Error ProgrammeDisplayCatalogAction.deleteExclusionProgram");
			result.put(ERROR, true);
			result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	/**
	 * Search level.
	 *
	 * @return the string
	 * @author loctt
	 * @since 18sep2013
	 */
	public String searchLevel(){
	    result.put("page", page);
	    result.put("max", max);
	    try{		
			ObjectVO<StDisplayProgramLevel> displayProgramLevelVO = displayProgramMgr.getListDisplayProgramLevel(null, id);			
			if(displayProgramLevelVO!= null){
			    result.put("rows", displayProgramLevelVO.getLstObject());		
			}    		
	    }catch (Exception e)  {
	    	LogUtility.logError(e, e.getMessage());			
	    }
	    return JSON;
	}
	
	/**
	 * @author thachnn
	 */
	
	/*public String productGroup(){
		 result.put("page", page);
		 result.put("max", max);
		    try{
				KPaging<StDisplayPdGroup> kPaging = new KPaging<StDisplayPdGroup>();
				kPaging.setPageSize(max);
				kPaging.setPage(page-1);
				StDisplayPdGroupFilter filter = new StDisplayPdGroupFilter();
				filter.setType(DisplayProductGroupType.parseValue(typeGroup));
				filter.setId(id);
				StaffRoleType staffRoleType = null;
				
				Staff userLogin = getStaffByCurrentUser();
				if(userLogin==null ){
					return JSON;
				}
				if(userLogin.getRoleType() == StaffRoleType.VNM.getValue()){
					///
					permissionUser = true;
					staffRoleType = StaffRoleType.VNM;
				}else if(userLogin.getRoleType() == StaffRoleType.GSNPP.getValue()){
					//
					permissionUser = false;
					staffRoleType = StaffRoleType.GSNPP;
					
				}else if(userLogin.getRoleType() == StaffRoleType.GSKA.getValue()){
					//
					permissionUser = false;
					staffRoleType = StaffRoleType.GSKA;
				}
				
				ObjectVO<StDisplayPdGroup> displayProgramVO = displayProgramMgr.getListDisplayGroup(kPaging,filter,staffRoleType);
				if(displayProgramVO.equals(null)){
					result.put("total",0);
					result.put("rows", new ObjectVO<StDisplayPdGroup>());
				} else {
					result.put("total",displayProgramVO.getkPaging().getTotalRows());
					result.put("rows", displayProgramVO.getLstObject());
				}
							    		
		    }catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		    }
		    return JSON;
	}*/
	
	/**
	 * @author thachnn
	 */
	public String productGrDetail(){
	    /*try{
	    	//StaffRoleType staffRoleType = null;
	    	Staff userLogin = getStaffByCurrentUser();
			if(userLogin==null ){
				return JSON;
			}
			if(userLogin.getStaffType().getObjectType() == StaffRoleType.VNM.getValue()){
				///
				permissionUser = true;
				//staffRoleType = StaffRoleType.VNM;
			}else if(userLogin.getRoleType() == StaffRoleType.GSNPP.getValue()){
				//
				permissionUser = false;
				staffRoleType = StaffRoleType.GSNPP;
				
			}else if(userLogin.getRoleType() == StaffRoleType.GSKA.getValue()){
				//
				permissionUser = false;
				staffRoleType = StaffRoleType.GSKA;
			}
			List<StDisplayPDGroupDTL> displayPDGroupDTLs  = new ArrayList<StDisplayPDGroupDTL>();
			if(groupDtlIdLst!=null){
				for(Long l:groupDtlIdLst){
					StDisplayPDGroupDTL dtl = new StDisplayPDGroupDTL();
					dtl.setGroupId(l);
					dtl.setDisplayPDGroupDTLVOs(displayProgramMgr.getDisplayPDGroupDTLVOs(levelId,l));
					displayPDGroupDTLs.add(dtl);
				}
			}
			result.put("rows", displayPDGroupDTLs);
						    		
	    }catch (Exception e) {
		LogUtility.logError(e, e.getMessage());
	    }*/
	    return JSON;
}		
	
	/**
	 * @author thachnn
	 */
	public String productGroupDetail(){
	    /*try{			
	    	StaffRoleType staffRoleType = null;
	    	Staff userLogin = getStaffByCurrentUser();
			if(userLogin==null ){
				return JSON;
			}
			if(userLogin.getStaffType().getObjectType() == StaffRoleType.VNM.getValue()){
				///
				permissionUser = true;
				staffRoleType = StaffRoleType.VNM;
			}else if(userLogin.getRoleType() == StaffRoleType.GSNPP.getValue()){
				//
				permissionUser = false;
				staffRoleType = StaffRoleType.GSNPP;
				
			}else if(userLogin.getRoleType() == StaffRoleType.GSKA.getValue()){
				//
				permissionUser = false;
				staffRoleType = StaffRoleType.GSKA;
			}
			ObjectVO<StDisplayPdGroupDtl> StDisplayPdGroupDtlVO = displayProgramMgr.getListDisplayGroupDetail(null,Long.valueOf(getGroupId()),staffRoleType);
			if(StDisplayPdGroupDtlVO.equals(null)){
				result.put("total",0);
				result.put("rows", new ArrayList<StDisplayPdGroupDtl>());
			} else {
				result.put("rows", StDisplayPdGroupDtlVO.getLstObject());
			}
						    		
	    }catch (Exception e) {
		LogUtility.logError(e, e.getMessage());
	    }*/
	    return JSON;
}		
	/**
	 * Search product.
	 * @since 18Sep2013
	 * @return the string
	 * @author loctt
	 */
	public String searchProduct(){
	    result.put("page", page);
	    result.put("max", max);
	    try{
//	    	KPaging<StDisplayPdGroup> kPaging = new KPaging<StDisplayPdGroup>();
//	    	kPaging.setPageSize(max);
//	    	kPaging.setPage(max -1);
			if(id != null && id > 0){
				ObjectVO<StDisplayPdGroup> displayProgramDetailVO = displayProgramMgr.getListDisplayProductGroup(null, id, DisplayProductGroupType.parseValue(typeGroup));		
				if(displayProgramDetailVO!= null){
//					result.put("total", displayProgramDetailVO.getLstObject().size());
					result.put("rows", displayProgramDetailVO.getLstObject());		
				}  
				else{
//					result.put("total", 0);
					result.put("rows", new ObjectVO<StDisplayPdGroup>());
				}
			}
	    }catch (Exception e) {
		LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	/**
	 * Save level.
	 *
	 * @return the string
	 * @author loctt
	 * @since 19sep2013
	 */
	private Long levelId;
	private StDisplayProgramLevel displayProgramLevel;
	private BigDecimal amount;
	private Integer sku;
	private String levelCode;
	public String getLevelCode() {
		return levelCode;
	}


	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}


	public StDisplayProgramLevel getDisplayProgramLevel() {
		return displayProgramLevel;
	}


	public void setDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel) {
		this.displayProgramLevel = displayProgramLevel;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public Long getLevelId() {
		return levelId;
	}


	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}
	public Integer getSku() {
		return sku;
	}

	public void setSku(Integer sku) {
		this.sku = sku;
	}

	/**
	 * Chinh sua level
	 * @modify hunglm16
	 * @return
	 * @since October 09,2015
	 */
	public String changeLevel() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			sysDate = commonMgr.getSysDate();
			Staff staffSigIn = getStaffByCurrentUser();
			if (staffSigIn == null) {
				return JSON;
			}
			StDisplayProgram displayProgram = displayProgramMgr.getStDisplayProgramById(id);
			if (displayProgram != null) {
				if (levelId != null && levelId > 0) {
					/** Thong tin LEVEL update */
					displayProgramLevel = displayProgramMgr.getDisplayProgramLevelById(levelId);
					displayProgramLevel.setAmount(amount);
					displayProgramLevel.setNumSku(sku);
					displayProgramLevel.setUpdateDate(sysDate);
					displayProgramLevel.setQuantity(quantityLv);
					displayProgramLevel.setUpdateUser(staffSigIn.getStaffCode());
					/** Thong tin nhom phat sinh doanh so */
					List<StDisplayPlAmtDtl> lstDetail = putDataForLevelDTL();
					List<StDisplayPlDpDtl> lstProduct = putDataForProductDTL();
					displayProgramMgr.createOrUpdateDisplayProgramLevel(displayProgramLevel, lstDetail, lstProduct, getLogInfoVO(), true);
					error = false;
				} else {
					if (!StringUtil.isNullOrEmpty(levelCode)) {
						displayProgramLevel = displayProgramMgr.getDisplayProgramLevelByCode(levelCode, id);
						if (displayProgramLevel != null) {
							result.put(ERROR, error);
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.display.program.level.code");
							result.put("errMsg", errMsg);
							return JSON;
						} else {
							//Them check XSS du lieu
							if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(levelCode)) {
								errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
							}
							if (!StringUtil.isNullOrEmpty(errMsg)) {
								result.put(ERROR, error);
								result.put("errMsg", errMsg);
								return JSON;
							}
							displayProgramLevel = new StDisplayProgramLevel();
							displayProgramLevel.setStDisplayProgram(displayProgram);
							displayProgramLevel.setLevelCode(levelCode.toUpperCase());
							displayProgramLevel.setAmount(amount);
							displayProgramLevel.setNumSku(sku);
							displayProgramLevel.setQuantity(quantityLv);
							displayProgramLevel.setCreateDate(sysDate);
							displayProgramLevel.setCreateUser(currentUser.getUserName().toUpperCase());
							List<StDisplayPlAmtDtl> lstDetail = putDataForLevelDTL();
							List<StDisplayPlDpDtl> lstProduct = putDataForProductDTL();
							displayProgramMgr.createOrUpdateDisplayProgramLevel(displayProgramLevel, lstDetail, lstProduct, getLogInfoVO(), false);
							error = false;
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	private List<Long> lstGroupId;
	public List<Long> getLstGroupId() {
		return lstGroupId;
	}


	public void setLstGroupId(List<Long> lstGroupId) {
		this.lstGroupId = lstGroupId;
	}
//loctt 20sep2013
	private List<BigDecimal> lstMinValue;
	public List<BigDecimal> getLstMinValue() {
		return lstMinValue;
	}


	public void setLstMinValue(List<BigDecimal> lstMinValue) {
		this.lstMinValue = lstMinValue;
	}


	public List<BigDecimal> getLstMaxValue() {
		return lstMaxValue;
	}


	public void setLstMaxValue(List<BigDecimal> lstMaxValue) {
		this.lstMaxValue = lstMaxValue;
	}


	public List<Float> getLstPercentValue() {
		return lstPercentValue;
	}


	public void setLstPercentValue(List<Float> lstPercentValue) {
		this.lstPercentValue = lstPercentValue;
	}
	private List<BigDecimal> lstMaxValue;
	private List<Float> lstPercentValue;
	
	/**
	 * @author loctt
	 * @since 19sep2013
	 * @return
	 */
	private List<StDisplayPlAmtDtl> putDataForLevelDTL(){
		List<StDisplayPlAmtDtl> lstDetail = new ArrayList<StDisplayPlAmtDtl>();
		try{
			if(lstGroupId != null && lstGroupId.size() > 0){
				for(int i = 0; i < lstGroupId.size() ; i++){
					StDisplayPlAmtDtl detail = null;
					if(displayProgramLevel!=null && displayProgramLevel.getId()!=null){
						detail = displayProgramMgr.getStDisplayPlAmtDtlByAmtGr(displayProgramLevel.getId(),lstGroupId.get(i));
					}
					if(detail!=null){
						detail.setUpdateDate(sysDate);
						detail.setUpdateUser(currentUser.getUserName().toUpperCase());
					}else if(!BigDecimal.ZERO.equals(lstMinValue.get(i)) 
							|| !BigDecimal.ZERO.equals(lstMaxValue.get(i))
							|| lstPercentValue.get(i) > 0){	
						StDisplayPdGroup group = displayProgramMgr.getDisplayProductGroupById(lstGroupId.get(i));
						detail = new StDisplayPlAmtDtl();						
						detail.setDisplayProductGroup(group);
						detail.setDisplayProgramLevel(displayProgramLevel);						
						detail.setCreateDate(sysDate);
						detail.setCreateUser(currentUser.getUserName().toUpperCase());
					}else {
						continue;
					}
					if(!BigDecimal.ZERO.equals(lstMinValue.get(i))){
						detail.setMin(lstMinValue.get(i));
					}else{
						detail.setMin(null);
					}
					if(!BigDecimal.ZERO.equals(lstMaxValue.get(i))){
						detail.setMax(lstMaxValue.get(i));
					}else{
						detail.setMax(null);
					}
					if(lstPercentValue.get(i) > 0){
						detail.setPrecent(lstPercentValue.get(i)); 
					}else{
						detail.setPrecent(null);
					}
					detail.setStatus(ActiveType.RUNNING);	
					lstDetail.add(detail);
				}
			}
		}
		catch(Exception e) {
    		LogUtility.logError(e, e.getMessage());	    		
		}	   
		return lstDetail;
	}
	
	private List<BigDecimal> lstQuantityValue;
	public List<BigDecimal> getLstQuantityValue() {
		return lstQuantityValue;
	}


	public void setLstQuantityValue(List<BigDecimal> lstQuantity) {
		this.lstQuantityValue = lstQuantity;
	}


	/**
	 * @author loctt
	 * @since 25sep2013
	 * @return
	 */
	private List<StDisplayPlDpDtl> putDataForProductDTL(){
		List<StDisplayPlDpDtl> lstDetail = new ArrayList<StDisplayPlDpDtl>();
		try{
			if(lstGroupProductLv != null && lstGroupProductLv.size() > 0){
				for(int i = 0; i < lstGroupProductLv.size() ; i++){		
					String item = lstGroupProductLv.get(i);
					String[] items = item.split(";");
					System.out.println(items);
					Long grouId = Long.valueOf(item.split(";")[0]);
					String strQty = "0";
					if(items.length>2){
						strQty = item.split(";")[2];
					}				
					Product product = productMgr.getProductByCode(item.split(";")[1]);
					StDisplayPdGroupDtl stDisplayPdGroupDtl = displayProgramMgr.getDisplayProductGroupDTLByGroupId(grouId, product.getId());
					StDisplayPlDpDtl detail = null;
					if(displayProgramLevel!=null && displayProgramLevel.getId()!=null){
						detail = displayProgramMgr.getStDisplayPlPrDtlByGr(displayProgramLevel.getId(),stDisplayPdGroupDtl.getId());
						if(detail!=null){							
							detail.setUpdateDate(sysDate);
							detail.setUpdateUser(currentUser.getUserName());
						}else{	
							detail = new StDisplayPlDpDtl();
							detail.setDisplayProductGroupDetail(stDisplayPdGroupDtl);
							detail.setCreateDate(sysDate);
							detail.setCreateUser(currentUser.getUserName());
						}
					}else{
						detail = new StDisplayPlDpDtl();
						detail.setQuantity(new BigDecimal(strQty));
						detail.setDisplayProductGroupDetail(stDisplayPdGroupDtl);
						detail.setCreateDate(sysDate);
						detail.setCreateUser(currentUser.getUserName());
					}		
					if(strQty.equals("0")){
						detail.setQuantity(null);
					}else{
						detail.setQuantity(new BigDecimal(strQty));
					}					
					detail.setStatus(ActiveType.RUNNING);	
					lstDetail.add(detail);
				}
			}
		}
		catch(Exception e) {
    		LogUtility.logError(e, e.getMessage());	    		
		}	   
		return lstDetail;
	}
	
	/**
	 * @author thachnn
	 * 
	 * @modify hunglm16
	 * @since October 09,2015
	 */
	public String changeProductGroup() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		StDisplayPdGroup stDisplayPdGroup = new StDisplayPdGroup();
		if (currentUser != null) {
			try {
				if (id != null && id > 0) {
					StDisplayProgram stDisplayProgram = displayProgramMgr.getStDisplayProgramById(id);
					if (stDisplayProgram != null) {
						//Them check XSS du lieu
						if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupCode)) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
						}
						if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupName)) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
						}
						if (!StringUtil.isNullOrEmpty(errMsg)) {
							result.put(ERROR, error);
							result.put("errMsg", errMsg);
							return JSON;
						}
						if (groupId != null && groupId > 0) {
							stDisplayPdGroup = displayProgramMgr.getDisplayProductGroupById(Long.valueOf(groupId));
							if (stDisplayPdGroup != null) {
								stDisplayPdGroup.setDisplayProductGroupName(groupName);
								stDisplayPdGroup.setUpdateDate(commonMgr.getSysDate());
								stDisplayPdGroup.setUpdateUser(currentUser.getUserName());
								displayProgramMgr.updateStDisplayPdGroup(stDisplayPdGroup, getLogInfoVO());
								result.put("groupId", stDisplayPdGroup.getId());
								error = false;
							}
						} else {
							StDisplayPdGroup tmp = displayProgramMgr.getStDisplayPdGroupByCode(id, DisplayProductGroupType.parseValue(typeGroup), groupCode);
							if (tmp != null) {
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "display.group.code.exist");
								result.put("errMsg", errMsg);
								result.put(ERROR, error);
							} else {
								stDisplayPdGroup = new StDisplayPdGroup();
								stDisplayPdGroup.setDisplayProgram(stDisplayProgram);
								stDisplayPdGroup.setDisplayProductGroupCode(groupCode);
								stDisplayPdGroup.setDisplayProductGroupName(groupName);
								stDisplayPdGroup.setCreateDate(commonMgr.getSysDate());
								stDisplayPdGroup.setCreateUser(currentUser.getUserName());
								stDisplayPdGroup.setType(DisplayProductGroupType.parseValue(typeGroup));
								stDisplayPdGroup = displayProgramMgr.createStDisplayPdGroup(stDisplayPdGroup, getLogInfoVO());
								result.put("groupId", stDisplayPdGroup.getId());
								error = false;
							}
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * @author thachnn
	 */
	public String deleteStGroup(){
		//Su dung ham deleteAmountProductGroup cua lochp voi Type = 4
		return deleteAmountProductGroup();
	}
	
	
	/**
	 * @author tientv
	 * @ so suat nhan vien ban hang
	 */
	public String viewStaffMap() throws Exception {
		return SUCCESS;
	}
	
	
	/**
	 * Delete level.
	 *
	 * @return the string
	 * @author loctt
	 * @since 20sep2013
	 */
	public String deleteLevel() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				StDisplayProgramLevel displayProgramLevel = displayProgramMgr.getDisplayProgramLevelById(levelId);
				if (displayProgramLevel != null) {
					displayProgramLevel.setStatus(ActiveType.DELETED);
					displayProgramLevel.setUpdateDate(DateUtil.now());
					displayProgramLevel.setUpdateUser((currentUser != null) ? currentUser.getUserName() : null);
					displayProgramMgr.updateDisplayProgramLevel(displayProgramLevel, getLogInfoVO());
					error = false;
				}

			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
/*
 * LocHP
 * hien thi danh sach doanh so
 */
	public String amountProductGroup(){
//		if (currentUser.)
		/*Staff staff = getStaffByCurrentUser();
		staffRole=staff.getStaffType().getObjectType();		
		if (this.getStaffRole() == StaffRoleType.VNM.getValue()){
			permissionUser = true;
		}else{
			result.put("rows", new ObjectVO<StDisplayPdGroup>().getLstObject());
			return JSON;
		}
		result.put("page", page);
		 result.put("max", max);
		    try{
//				KPaging<StDisplayPdGroup> kPaging = new KPaging<StDisplayPdGroup>();
//				kPaging.setPageSize(max);
//				kPaging.setPage(page-1);
				StDisplayPdGroupFilter filter = new StDisplayPdGroupFilter();
//				filter.setType(DisplayProductGroupType.parseValue(typeGroup));
				filter.setId(id);
				StaffRole staffRole = null;
				
//				if(checkRoles(VSARole.VNM_TABLET_PORTAL)){
//					///
//					permissionUser = true;
//					staffRole = StaffRole.VNM_TABLET_PORTAL;
//				}else if(checkRoles(VSARole.VNM_TABLET_PORTAL_GS)){
//					//
//					permissionUser = false;
//					staffRole = StaffRole.VNM_TABLET_PORTAL_GS;
//					
//				}else if(checkRoles(VSARole.VNM_TABLET_PORTAL_GSKA)){
//					//
//					permissionUser = false;
//					staffRole = StaffRole.VNM_TABLET_PORTAL_GSKA;
//				}
				
				ObjectVO<StDisplayPdGroup> displayProgramVO = displayProgramMgr.getListDisplayProgrameAmount(null,filter,staffRole);
				if(displayProgramVO.equals(null)){
	//				result.put("total",0);
					result.put("rows", new ObjectVO<StDisplayPdGroup>().getLstObject());
				} else {
		//			result.put("total",displayProgramVO.getkPaging().getTotalRows());
					result.put("rows", displayProgramVO.getLstObject());
				}
							    		
		    }catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		    }*/
		    return JSON;
	}
	/*
	 * author: lochp
	 * hien thi bang con
	 */
	public String searchProductDetail(){

		/*Staff staff = getStaffByCurrentUser();
		staffRole=staff.getStaffType().getObjectType();		
		if (this.getStaffRole() == StaffRoleType.VNM.getValue()){
			permissionUser = true;
		}else{
			result.put("rows", new ObjectVO<StDisplayPdGroup>().getLstObject());
			return JSON;
		}
		try{
			if (getGroupId() != null){
				StDisplayPdGroupFilter filter = new StDisplayPdGroupFilter();
				filter.setId(Long.valueOf(getGroupId()));
				StaffRole staffRole = null;

				ObjectVO<StDisplayPdGroupDtl> product = 
					displayProgramMgr.getListDisplayProgrameAmountProductDetail(null, filter, staffRole);
				if (product == null){
					result.put("total", 0);
					result.put("rows", new ObjectVO<Product>());
				}else{
					result.put("total", product.getLstObject().size());
					result.put("rows", product.getLstObject());
//					result.put("row", product.getLstObject());
				}
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}*/
		return JSON;
	}
	
	/*
	 * lochp
	 * change amount group
	 * 21/09/2013
	 */

	public String changeAmountGroup(){
		return JSON;
	}

	public String deleteAmountProductGroup(){
		return JSON;
	}
	
	public String changeAmountProduct(){   //lochp: change convfact cua product  
		return JSON;
	}
	
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}


	public String getGroupCode() {
		return groupCode;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}


	public Long getGroupId() {
		return groupId;
	}
	
	/**
	 * @author thachnn
	 * Load trang thai CTTB
	 * @return
	 */
	public String loadStatus()
	{
		return SUCCESS;
	}
	
	
	public void setIsUpdate(Boolean isUpdate) {
		this.isUpdate = isUpdate;
	}


	public Boolean getIsUpdate() {
		return isUpdate;
	}


	public void setProductId(Long productId) {
		this.productId = productId;
	}


	public Long getProductId() {
		return productId;
	}


	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}


	public Integer getConvfact() {
		return convfact;
	}


	public void setLstProductId(List<Long> lstProductId) {
		this.lstProductId = lstProductId;
	}


	public List<Long> getLstProductId() {
		return lstProductId;
	}


	public void setLstConvfact(List<Integer> lstConvfact) {
		this.lstConvfact = lstConvfact;
	}


	public List<Integer> getLstConvfact() {
		return lstConvfact;
	}
	
	public String getLstIdStr() {
		return lstIdStr;
	}

	public void setLstIdStr(String lstIdStr) {
		this.lstIdStr = lstIdStr;
	}

	public String getCode() {
		return code;
	}

	public Boolean getFlagCheckAll() {
		return flagCheckAll;
	}


	public void setFlagCheckAll(Boolean flagCheckAll) {
		this.flagCheckAll = flagCheckAll;
	}
	
	/**
	 * Save product.
	 *
	 * @return the string
	 * @author thachnn
	 */
	public String changeProduct(){
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null && id!= null && id > 0 ){
	    	try {
	    		StDisplayProgram displayProgram = displayProgramMgr.getStDisplayProgramById(id);
	    		if(displayProgram != null){
	    			StDisplayPdGroup stDisplayPdGroup = displayProgramMgr.getDisplayProductGroupById(Long.valueOf(groupId));
	    			if(stDisplayPdGroup != null){
	    				if(isUpdate != null && isUpdate){
	    					if(productId != null && productId > 0){
	    						StDisplayPdGroupDtl stDisplayPdGroupDTL = displayProgramMgr.getStDisplayPdGroupDtlById(productId);
	    						if(stDisplayPdGroupDTL != null){
	    							stDisplayPdGroupDTL.setConvfact(convfact);
	    							stDisplayPdGroupDTL.setUpdateDate(DateUtil.now());
	    							stDisplayPdGroupDTL.setUpdateUser(currentUser.getUserName());
	    							displayProgramMgr.updateDisplayProductGroupDTL(stDisplayPdGroupDTL, getLogInfoVO());
	    							error = false;
	    						}
	    					}
	    				}else{
	    					if(lstProductId!= null && lstProductId.size() > 0 ){
	    						int size = lstProductId.size();
	    						List<StDisplayPdGroupDtl> lstDetail = new ArrayList<StDisplayPdGroupDtl>(); 
	    						for(int i = 0; i < size;i++){
	    							Product product = productMgr.getProductById(lstProductId.get(i));
	    							if(product != null){
	    								StDisplayPdGroupDtl stDisplayPdGroupDTL = new StDisplayPdGroupDtl();
	    								stDisplayPdGroupDTL.setCreateDate(DateUtil.now());
	    								stDisplayPdGroupDTL.setCreateUser(currentUser.getUserName());
	    								if(lstConvfact != null ){
	    									stDisplayPdGroupDTL.setConvfact(lstConvfact.get(i));
	    								}
	    								stDisplayPdGroupDTL.setProduct(product);
	    								stDisplayPdGroupDTL.setStatus(ActiveType.RUNNING);
	    								stDisplayPdGroupDTL.setDisplayProductGroup(stDisplayPdGroup);
	    								lstDetail.add(stDisplayPdGroupDTL);
	    							}
	    						}
	    						if(lstDetail != null && lstDetail.size() > 0){
	    							displayProgramMgr.createListStDisplayPdGroupDtl(lstDetail, getLogInfoVO());
	    							error = false;
	    						}
	    					}
	    				}
	    			}
	    		}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
	    return JSON;
	}
	
	/**
	 * Delete product.
	 *
	 * @return the string
	 * @author thachnn
	 */
	public String deleteProduct(){
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
	    	try{
	    		if(groupId != null && groupId > 0 && productId != null && productId > 0){
    				StDisplayPdGroupDtl detail = displayProgramMgr.getStDisplayPdGroupDtlByGroupId(groupId, productId);
    				if(detail != null){
						detail.setStatus(ActiveType.DELETED);
						displayProgramMgr.updateDisplayProductGroupDTL(detail, getLogInfoVO());
						error = false;
    				}
	    		}
	    	}catch (Exception e) {
	    		LogUtility.logError(e, e.getMessage());
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Search level product group.
	 * @since 25Sep2013
	 * @return the string
	 * @author loctt
	 */
	public String searchLevelProductGroup(){	   
	    try{	    	
			if(id != null && id > 0){
				ObjectVO<StDisplayPdGroup> displayProgramDetailVO = displayProgramMgr.getListLevelProductGroup(null, id, DisplayProductGroupType.parseValue(typeGroup),levelId);		
				if(displayProgramDetailVO!= null){
					result.put("rows", displayProgramDetailVO.getLstObject());		
				}  
				else{
					result.put("rows", new ArrayList<StDisplayPdGroup>());
				}
			}
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}

	public String loadSP(){
		result.put("page", page);
	    result.put("max", max);
	    try{
	    	//KPaging<Product> kPaging = new KPaging<Product>();
	    	KPaging<ProductVO> kPaging = new KPaging<ProductVO>();
	    	kPaging.setPageSize(max);
	    	kPaging.setPage(page -1);
			if(id != null && id > 0){
				//ObjectVO<Product> displayProgramDetailVO = displayProgramMgr.getListProduct(kPaging, id, code, name, DisplayProductGroupType.CB);
				ObjectVO<ProductVO> displayProgramDetailVO = 
						displayProgramMgr.getListGroupProductVO(kPaging, code, name, 
								id, DisplayProductGroupType.CB);
				if(displayProgramDetailVO!= null){
					result.put("total", displayProgramDetailVO.getkPaging().getTotalRows());
					result.put("rows", displayProgramDetailVO.getLstObject());		
				}  
				else{
					result.put("total", 0);
					result.put("rows", new ObjectVO<Product>());
				}
			}
	    }catch (Exception e) {
		LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	/** thachnn9 */
	public String searchGroupProduct(){
		result.put("page", page);
		result.put("max", max);
		try{
			KPaging<ProductVO> kPaging = new KPaging<ProductVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (id != null && id >= 0){
				ObjectVO<ProductVO> vo = 
					displayProgramMgr.getListGroupProductVO(kPaging, productCode, productName, 
							id, DisplayProductGroupType.parseValue(typeGroup));
				if (vo != null ){
					result.put("total", vo.getkPaging().getTotalRows());
					result.put("rows", vo.getLstObject());
				}else{
					result.put("total", 0);
					result.put("rows", new ObjectVO<ProductVO>());
				}
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public String ViewCustomer() throws Exception {
		return SUCCESS;
	}
	
	public String loadDisplayProgramMonth() throws Exception {
		try {
			Date fDate = null;
			if (!StringUtil.isNullOrEmpty(month)) {
				fDate = DateUtil.parse("01/"+month, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				fDate=DateUtil.getFirstDateInMonth(commonMgr.getSysDate());
			}
			if (listShopId != null && listShopId.size() > 0) { // remove nhung thang khong thuoc quyen
				for (int i = 0, sz = listShopId.size(); i < sz; i++) {
					if (!checkShopInOrgAccessById(listShopId.get(i))) {
						listShopId.remove(i);
						i--;
						sz--;
					}
				}
			}
			if (listShopId == null ||  listShopId.size() == 0) {
				listShopId = new ArrayList<Long>();
				ShopToken shToken = currentUser.getShopRoot();
				if (shToken != null) {
					listShopId.add(shToken.getShopId());
				}
				/*Staff st1 = staffMgr.getStaffByCode(currentUser.getUserName());
				if (st1 != null) {
					if (st1.getStaffType().getObjectType() == StaffRoleType.GSNPP.getValue()
							&& session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE)!=null) {
						Shop shop = (Shop)session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
						listShopId.add(shop.getId());
					} else {
						listShopId.add(st1.getShop().getId());
					}
				}*/
			}
			listDisplayProgram = displayProgramMgr.getListDisplayProgramSysdateEX(listShopId,fDate);
		} catch(Exception ex) {
			LogUtility.logError(ex,"DisplayProgramAction.searchCustomerDisplayProgram");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}
	
	
	
	public void setGroupDtlId(Long groupDtlId) {
		this.groupDtlId = groupDtlId;
	}

	/**
	 * GETTER/SETTER
	 * */
	public Long getGroupDtlId() {
		return groupDtlId;
	}
	
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setExcelFileCus(File excelFileCus) {
		this.excelFileCus = excelFileCus;
	}

	public Integer getQuantityLv() {
		return quantityLv;
	}

	public void setQuantityLv(Integer quantityLv) {
		this.quantityLv = quantityLv;
	}

	public String getDisplayProgramCode() {
		return displayProgramCode;
	}

	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}

	public List<StDisplayProgram> getListDisplayProgram() {
		return listDisplayProgram;
	}

	public void setListDisplayProgram(List<StDisplayProgram> listDisplayProgram) {
		this.listDisplayProgram = listDisplayProgram;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	public Long getCurrentShopId() {
		return currentShopId;
	}


	public void setCurrentShopId(Long currentShopId) {
		this.currentShopId = currentShopId;
	}
	
	public List<String> getLstGroupProductLv() {
		return lstGroupProductLv;
	}

	public void setLstGroupProductLv(List<String> lstGroupProductLv) {
		this.lstGroupProductLv = lstGroupProductLv;
	}

	public int getExcelType() {
		return excelType;
	}

	public void setExcelType(int excelType) {
		this.excelType = excelType;
	}


	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getImportProgram() {
		return importProgram;
	}

	public void setImportProgram(String importProgram) {
		this.importProgram = importProgram;
	}
	private File pdfFile;

	private String fakePdffilepc;
	
	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	
	
	public String check1(List<List<String>> lstData, int j) { // check trung khoa
		String message = "";
		try{
			if (lstData.get(j) == null) {
				return message;
			}
			List<String> row1 = lstData.get(j);		
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) == null || k == j) {
					continue;
				}
				List<String> row2 = lstData.get(k);
				if (!(row2.size()>1 && row1.size()>1)){
					continue;
				}				
				if(row1.get(2).split("/").length<2 || row2.get(2).split("/").length<2){
					continue;
				}			
				String inMonthOne = StringUtil.toStringNumberMMYYY(row1.get(2).substring(row1.get(2).indexOf('/')+1));
				String inMonthTwo = StringUtil.toStringNumberMMYYY(row2.get(2).substring(row2.get(2).indexOf('/')+1));			
				if (row1.get(0).toUpperCase().equalsIgnoreCase(row2.get(0).toUpperCase()) /** Ma CTTB */
						&& row1.get(1).toUpperCase().equalsIgnoreCase(row2.get(1).toUpperCase()) /** Ma NVBH */
						&& inMonthOne.equalsIgnoreCase(inMonthTwo)) /** Thang */
				{
					if (StringUtil.isNullOrEmpty(message)) {
						message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"huanluyen.dong.thu")+ (j + 2) + ", " + (k + 2);
					} else {
						message += ", " + (k + 2);
					}

				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " "+ Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"huanluyen.trung.nhau");
			}
		}catch (Exception e) {}				
		return message;
	}
	public String check2(List<List<String>> lstData, int j) { 
        String message = "";
        try{
            if (lstData.get(j) == null) {
                return message;
            }
            List<String> row1 = lstData.get(j);     
            for (int k = 0; k < lstData.size(); k++) {
                if (lstData.get(k) == null || k == j) {
                    continue;
                }
                List<String> row2 = lstData.get(k);
                if (!(row2.size()>1 && row1.size()>1)){
                    continue;
                }                           
                if (row1.get(0).toUpperCase().equalsIgnoreCase(row2.get(0).toUpperCase()) /** Ma NPP */
                        && row1.get(1).toUpperCase().equalsIgnoreCase(row2.get(1).toUpperCase())
                        && row1.get(2).toUpperCase().equalsIgnoreCase(row2.get(2).toUpperCase())
                        && row1.get(3).toUpperCase().equalsIgnoreCase(row2.get(3).toUpperCase())
                        && row1.get(4).toUpperCase().equalsIgnoreCase(row2.get(4).toUpperCase())
                        && row1.get(5).toUpperCase().equalsIgnoreCase(row2.get(5).toUpperCase())/** Ma KH */                        )
                {
                    if (StringUtil.isNullOrEmpty(message)) {
                        message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"huanluyen.dong.thu")+ (j + 2) + ", " + (k + 2);
                    } else {
                        message += ", " + (k + 2);
                    }
                }
            }
            if (!StringUtil.isNullOrEmpty(message)) {
                message += " "+ Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"huanluyen.trung.nhau");
            }
        }catch (Exception e) {}             
        return message;
    }
	
	/**
	 * Get list Display Program By Year
	 * @author hunglm16
	 * @since January 17, 2014
	 * */
	public String getListDisplayProgramByYear(){
		//Lay danh sach CTTB dua theo nam
		result.put("page", page);
		result.put("rows", rows);
		if(rows == 0) rows = 10;
		if(page == 0) page = 1;
		try{
			Staff currentStaff = null;
			if (currentUser != null) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null){
					return JSON;
				}
				if(shopId==null){
					shopId = currentStaff.getShop().getId();
				}
			} else{
				return JSON;
			}
			KPaging<StDisplayProgram> kPaging = new KPaging<StDisplayProgram>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			ObjectVO<StDisplayProgram> displayProgramVO = new ObjectVO<StDisplayProgram>();
			displayProgramVO = displayProgramMgr.getListDisplayProgramByYear(kPaging, shopId, year, code, name);
			if(displayProgramVO!= null){
			    result.put("total", displayProgramVO.getkPaging().getTotalRows());
			    result.put("rows", displayProgramVO.getLstObject());		
			}else{
				result.put("total", 0);
			    result.put("rows", new ArrayList<StDisplayProgram>());
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Private Get list Display Program By List id
	 * @author hunglm16
	 * @since January 17, 2014
	 * */
	public String getListDisplayProgramByListId(){
		//Lay danh sach CTTB dua theo danh sach ma CTTB
		try{
			Staff currentStaff = null;
			if (currentUser != null) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null){
					return JSON;
				}
				if(shopId==null){
					shopId = currentStaff.getShop().getId();
				}
			} else{
				return JSON;
			}
			int  i = 0;
			lstId = new ArrayList<Long>();
			if(!StringUtil.isNullOrEmpty(lstIdStr)){
				String[] arrStrId = lstIdStr.split(",");
				for(i=0; i<arrStrId.length; i++){
					lstId.add(Long.parseLong(arrStrId[i].trim()));
				}
			}
			List<StDisplayProgram> lstData = new ArrayList<StDisplayProgram>();
			lstData = displayProgramMgr.getListDisplayProgramByListId(shopId, lstId, flagCheckAll, year, code, name);
			result.put("lstData", lstData);
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	
	public String getListDisplayProgramInTowDay(){
		//Lay danh sach CTTB dua theo nam
		result.put("page", page);
		result.put("rows", rows);
		if(rows == 0) rows = 10;
		if(page == 0) page = 1;
		try{
			Staff currentStaff = null;
			if (currentUser != null) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null){
					return JSON;
				}
				if(shopId==null){
					shopId = currentStaff.getShop().getId();
				}
			} else{
				return JSON;
			}
			KPaging<StDisplayProgramVNM> kPaging = new KPaging<StDisplayProgramVNM>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			ObjectVO<StDisplayProgramVNM> displayProgramVO = new ObjectVO<StDisplayProgramVNM>();
			
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			
			displayProgramVO = displayProgramMgr.getListDisplayProgramInTowDate(kPaging, shopId, fDate, tDate, code, name);
			if(displayProgramVO!= null){
			    result.put("total", displayProgramVO.getkPaging().getTotalRows());
			    result.put("rows", displayProgramVO.getLstObject());		
			}else{
				result.put("total", 0);
			    result.put("rows", new ArrayList<StDisplayProgramVNM>());
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/*DS CTTB tra thuong*/
	public String loadStatusVNM() throws Exception {
		st = new ArrayList<StatusBean>();
		st.add(new StatusBean(Long.valueOf(0), "Tạm ngưng"));
		st.add(new StatusBean(Long.valueOf(1), "Hoạt động"));
		status = ActiveType.RUNNING.getValue();
		resetToken(result);
		return SUCCESS;
	}
	
	public String searchVNM() throws Exception {
	    return JSON;
	}
}

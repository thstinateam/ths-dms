package ths.dms.web.action.program;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.FocusProgramMgr;
import ths.dms.core.business.LogMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.ActionAudit;
import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.FocusChannelMap;
import ths.dms.core.entities.FocusChannelMapProduct;
import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActionAuditType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.FocusProgramFilter;
import ths.dms.core.entities.enumtype.HTBHFilter;
import ths.dms.core.entities.enumtype.HTBHVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.NewTreeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.JsPromotionShopTreeNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.VSARole;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class FocusProgramAction extends AbstractAction {	
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8051124597792850084L;
	
	/** The focus program mgr. */
	private FocusProgramMgr focusProgramMgr;
	
	/** The shop mgr. */
	private ShopMgr shopMgr;
	
	private ApParamMgr apParamMgr;
	/** The product mgr. */
	private ProductMgr productMgr;
	
	private CommonMgr commonMgr;
	
	/** The id. */
	private Long id;
	
	private Integer userHO;
	
	public Integer getUserHO() {
		return userHO;
	}

	public void setUserHO(Integer userHO) {
		this.userHO = userHO;
	}

	private Long excelFCId;
	
	/** The shop id. */
	private Long shopId;
	
	/** The code. */
	private String code;
	
	/** The name. */
	private String name;
	
	/** The shop code. */
	private String shopCode;
	
	/** The shop name. */
	private String shopName;
	
	/** The from date. */
	private String fromDate;
	
	/** The to date. */
	private String toDate;
	
	/** The excel file. */
	private File excelFile;
	
	/** The excel file content type. */
	private String excelFileContentType;
	
	/** The lst shop. */
	private List<Shop> lstRegion;
	
	/** The lst area. */
	private List<Shop> lstArea;
	
	/** The shop map id. */
	private Long shopMapId;
	
	/** The region id. */
	private Long regionId;
	
	/** The area id. */
	private Long areaId;
	
	/** The channel map id. */
	private Long channelMapId;
	
	/** The staff type code. */
	private String staffTypeCode;
	
	/** The staff type id. */
	private Long staffTypeId;
	
	/** The cat code. */
	private String catCode;
	
	/** The sub cat code. */
	private String subCatCode;
	
	/** The product code. */
	private String productCode;
	
	/** The product id. */
	private Long productId;
	
	/** The lst staff type. */
	private List<FocusChannelMap> lstStaffType;
	
	private int excelType;
	
	private Integer status;
	
	/** The lst shop id. */
	private List<Long> lstShopId;
	
	/** The lst product id. */
	private List<Long> lstProductId;
	
	/** The channel type id. */
	private Long channelTypeId;
	
	/** The lst channel type. */
	private List<ApParam> lstChannelType;
	private List<ApParam> lstType;
	
	/** The permission edit. */
	private boolean permissionEdit;
	
	/** The tab order. */
	private String tabOrder;
	
	/** The type. */
	/*private int type;*/
	private Integer type;
	
	private Integer numUpdate;
	private List<Long> listShopId;
	private LogMgr logMgr;
	
	private Boolean isUpdate;
	private Long focusMapProductId;
	private String focusProductType;
	private List<FocusChannelMap> lstFocusChannelMap;
	private String importProgram;
	private Integer subContentId;
	
	private Integer userHo;
	
	


	
	public Integer getUserHo() {
		return userHo;
	}

	public void setUserHo(Integer userHo) {
		this.userHo = userHo;
	}

	public Integer getSubContentId() {
		return subContentId;
	}

	public void setSubContentId(Integer subContentId) {
		this.subContentId = subContentId;
	}

	public List<FocusChannelMap> getLstFocusChannelMap() {
		return lstFocusChannelMap;
	}

	public void setLstFocusChannelMap(List<FocusChannelMap> lstFocusChannelMap) {
		this.lstFocusChannelMap = lstFocusChannelMap;
	}

	public String getFocusProductType() {
		return focusProductType;
	}

	public void setFocusProductType(String focusProductType) {
		this.focusProductType = focusProductType;
	}

	public Boolean getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(Boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	public Long getFocusMapProductId() {
		return focusMapProductId;
	}

	public void setFocusMapProductId(Long focusMapProductId) {
		this.focusMapProductId = focusMapProductId;
	}

	/**
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}
	
	/**
	 * @return the shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public Long getExcelFCId() {
		return excelFCId;
	}

	public void setExcelFCId(Long excelFCId) {
		this.excelFCId = excelFCId;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * @param shopCode the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the excelFile
	 */
	public File getExcelFile() {
		return excelFile;
	}

	/**
	 * @param excelFile the excelFile to set
	 */
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	/**
	 * @return the excelFileContentType
	 */
	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	/**
	 * @param excelFileContentType the excelFileContentType to set
	 */
	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/**
	 * @return the lstRegion
	 */
	public List<Shop> getLstRegion() {
		return lstRegion;
	}

	/**
	 * @param lstRegion the lstRegion to set
	 */
	public void setLstRegion(List<Shop> lstRegion) {
		this.lstRegion = lstRegion;
	}

	/**
	 * @return the lstArea
	 */
	public List<Shop> getLstArea() {
		return lstArea;
	}

	/**
	 * @param lstArea the lstArea to set
	 */
	public void setLstArea(List<Shop> lstArea) {
		this.lstArea = lstArea;
	}

	/**
	 * @return the shopMapId
	 */
	public Long getShopMapId() {
		return shopMapId;
	}

	/**
	 * @param shopMapId the shopMapId to set
	 */
	public void setShopMapId(Long shopMapId) {
		this.shopMapId = shopMapId;
	}

	/**
	 * @return the regionId
	 */
	public Long getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the areaId
	 */
	public Long getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId the areaId to set
	 */
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the channelMapId
	 */
	public Long getChannelMapId() {
		return channelMapId;
	}

	/**
	 * @param channelMapId the channelMapId to set
	 */
	public void setChannelMapId(Long channelMapId) {
		this.channelMapId = channelMapId;
	}

	/**
	 * @return the staffTypeCode
	 */
	public String getStaffTypeCode() {
		return staffTypeCode;
	}

	/**
	 * @param staffTypeCode the staffTypeCode to set
	 */
	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	/**
	 * @return the staffTypeId
	 */
	public Long getStaffTypeId() {
		return staffTypeId;
	}

	/**
	 * @param staffTypeId the staffTypeId to set
	 */
	public void setStaffTypeId(Long staffTypeId) {
		this.staffTypeId = staffTypeId;
	}

	/**
	 * @return the catCode
	 */
	public String getCatCode() {
		return catCode;
	}

	/**
	 * @param catCode the catCode to set
	 */
	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	/**
	 * @return the subCatCode
	 */
	public String getSubCatCode() {
		return subCatCode;
	}

	/**
	 * @param subCatCode the subCatCode to set
	 */
	public void setSubCatCode(String subCatCode) {
		this.subCatCode = subCatCode;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * @return the lstStaffType
	 */
	public List<FocusChannelMap> getLstStaffType() {
		return lstStaffType;
	}

	/**
	 * @param lstStaffType the lstStaffType to set
	 */
	public void setLstStaffType(List<FocusChannelMap> lstStaffType) {
		this.lstStaffType = lstStaffType;
	}

	/**
	 * @return the excelType
	 */
	public int getExcelType() {
		return excelType;
	}

	/**
	 * @param excelType the excelType to set
	 */
	public void setExcelType(int excelType) {
		this.excelType = excelType;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the lstShopId
	 */
	public List<Long> getLstShopId() {
		return lstShopId;
	}

	/**
	 * @param lstShopId the lstShopId to set
	 */
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	/**
	 * @return the lstProductId
	 */
	public List<Long> getLstProductId() {
		return lstProductId;
	}

	/**
	 * @param lstProductId the lstProductId to set
	 */
	public void setLstProductId(List<Long> lstProductId) {
		this.lstProductId = lstProductId;
	}

	/**
	 * @return the channelTypeId
	 */
	public Long getChannelTypeId() {
		return channelTypeId;
	}

	/**
	 * @param channelTypeId the channelTypeId to set
	 */
	public void setChannelTypeId(Long channelTypeId) {
		this.channelTypeId = channelTypeId;
	}


	public List<ApParam> getLstChannelType() {
		return lstChannelType;
	}

	public void setLstChannelType(List<ApParam> lstChannelType) {
		this.lstChannelType = lstChannelType;
	}
	
	public List<ApParam> getLstType() {
		return lstType;
	}

	public void setLstType(List<ApParam> lstType) {
		this.lstType = lstType;
	}

	/**
	 * @return the permissionEdit
	 */
	public boolean isPermissionEdit() {
		return permissionEdit;
	}

	/**
	 * @param permissionEdit the permissionEdit to set
	 */
	public void setPermissionEdit(boolean permissionEdit) {
		this.permissionEdit = permissionEdit;
	}

	/**
	 * @return the tabOrder
	 */
	public String getTabOrder() {
		return tabOrder;
	}

	/**
	 * @param tabOrder the tabOrder to set
	 */
	public void setTabOrder(String tabOrder) {
		this.tabOrder = tabOrder;
	}

	

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @return the numUpdate
	 */
	public Integer getNumUpdate() {
		return numUpdate;
	}

	/**
	 * @param numUpdate the numUpdate to set
	 */
	public void setNumUpdate(Integer numUpdate) {
		this.numUpdate = numUpdate;
	}

	public List<Long> getListShopId() {
		return listShopId;
	}

	public void setListShopId(List<Long> listShopId) {
		this.listShopId = listShopId;
	}

	public String getImportProgram() {
		return importProgram;
	}

	public void setImportProgram(String importProgram) {
		this.importProgram = importProgram;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {		
		super.prepare();
		focusProgramMgr = (FocusProgramMgr)context.getBean("focusProgramMgr");
		shopMgr = (ShopMgr)context.getBean("shopMgr");
		productMgr = (ProductMgr)context.getBean("productMgr");
		apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
		logMgr = (LogMgr)context.getBean("logMgr");
		commonMgr = (CommonMgr)context.getBean("commonMgr");
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		status = ActiveType.RUNNING.getValue();
		if(checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)){
			userHo = 1;
		}else{
			userHo = 0;
		}
		resetToken(result);
		return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String search(){
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<FocusProgram> kPaging = new KPaging<FocusProgram>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);		
			Date fDate = null;
			Date tDate = null;
			FocusProgramFilter fpf = new FocusProgramFilter();
			if(!StringUtil.isNullOrEmpty(fromDate)){
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				fpf.setFromDate(fDate);
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				fpf.setToDate(tDate);
			}
			if((!StringUtil.isNullOrEmpty(fromDate) && fDate == null) || (!StringUtil.isNullOrEmpty(toDate) && tDate==null)){
				 result.put("total", null);
				 result.put("rows",null);
			} else {
				Shop shop = new Shop();
				if (shopId != null && shopId > 0) {
					shop = shopMgr.getShopById(shopId);
					if (shop != null && shop.getShopCode() != null) {
						shopCode = shop.getShopCode();
						fpf.setShopCode(shopCode);
					}
					fpf.setShopId(shopId);
				} else {
					Staff st = getStaffByCurrentUser();
					if (checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) {
						fpf.setShopCode(st.getShop().getShopCode());
						fpf.setShopId(st.getShop().getId());
					} else if (checkRoles(VSARole.VNM_SALESONLINE_HO) && shopId == -1) {
						shop = st.getShop();
						shopId = shop.getId();
						shopCode = shop.getShopCode();
						fpf.setShopCode(shopCode);
						fpf.setShopId(shopId);
					}
				}
				fpf.setFocusProgramCode(code);
				fpf.setFocusProgramName(name);
				fpf.setStatus(ActiveType.parseValue(status));
				
				//ActiveType at =ActiveType.parseValue(status);
				String role = "";
				if (checkRoles(VSARole.VNM_SALESONLINE_HO)) {
					role = "VNM_SALESONLINE_HO";
				} else if (checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) {
					role = "VNM_SALESONLINE_DISTRIBUTOR";
				}

				fpf.setStaffRootId(currentUser.getStaffRoot().getStaffId());
				fpf.setRollId(currentUser.getRoleToken().getRoleId());
				fpf.setShopRootId(currentUser.getShopRoot().getShopId());
				ObjectVO<FocusProgram> focusProgramVO = focusProgramMgr.getListFocusProgram(kPaging,fpf,StaffRole.parseValue(role));			
				if(focusProgramVO!= null){
				    result.put("total", focusProgramVO.getkPaging().getTotalRows());
				    result.put("rows", focusProgramVO.getLstObject());		
				}
			}			    		
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	/**
	 * Import excel.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	private String importFocusProgram() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = true;
		List<CellBean> lstFails = new ArrayList<CellBean>();		
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,4);		
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				for(int i=0;i<lstData.size();i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
						FocusProgram focusProgram = new FocusProgram();						
						List<String> row = lstData.get(i);
						boolean isUpdate = false;
						FocusProgram program = null;
						//code
						if(row.size() > 0){
							String value = row.get(0);							
							focusProgram.setFocusProgramCode(value);	
							String msg = ValidateUtil.validateField(value, "catalog.focus.program.code", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
							if(!StringUtil.isNullOrEmpty(msg)){
								message += msg;								
							}else{
								program = focusProgramMgr.getFocusProgramByCode(value);
								if(program!=null){
									if(program.getStatus().getValue().equals(ActiveType.WAITING.getValue())){
										isUpdate = true;
									}else{
										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED2, value) +"\n";
									}
								}
							}
						}						
						//name
						if(row.size() > 1){
							String value = row.get(1);
							focusProgram.setFocusProgramName(value);
							message += ValidateUtil.validateField(value, "catalog.focus.program.name", 100, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH);							
						}						
						//fromDate
						if(row.size() > 2){
							String value = row.get(2);
							Date fromDate = null;							
							try {									
								if (!StringUtil.isNullOrEmpty(value)) {
									fromDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true,"catalog.display.program.fromDate");																				
								}  
							} catch (Exception e) {
								LogUtility.logError(e, e.getMessage());
							}
							focusProgram.setFromDate(fromDate);
							message+= ValidateUtil.getErrorMsgForInvalidFormatDate(value,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.fromDate"),null);
						}						
						//toDate
						if(row.size() > 3){
							String value = row.get(3);
							Date toDate = null;							
							try {									
								if (!StringUtil.isNullOrEmpty(value)) {
									toDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								}  
							} catch (Exception e) {
								LogUtility.logError(e, e.getMessage());
							}
							focusProgram.setToDate(toDate);
							message+= ValidateUtil.getErrorMsgForInvalidFormatDate(value,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.toDate"),null);
						}
						message += ValidateUtil.getErrorMsgForInvalidToDate(focusProgram.getFromDate(), focusProgram.getToDate());
						if(isView == 0){
							if (StringUtil.isNullOrEmpty(message)){
								//CRM Update			
								if(isUpdate){
									program.setFocusProgramName(focusProgram.getFocusProgramName());
									program.setUpdateUser(currentUser.getUserName());
									program.setFromDate(focusProgram.getFromDate());
									program.setToDate(focusProgram.getToDate());
									focusProgramMgr.updateFocusProgram(program,getLogInfoVO());
								}else{
									focusProgram.setCreateUser(currentUser.getUserName());
									focusProgram.setStatus(ActiveType.WAITING);
									focusProgramMgr.createFocusProgram(focusProgram,getLogInfoVO());
								}
							} else {
								lstFails.add(StringUtil.addFailBean(row, message));
							}
							typeView = false;
						}else{
							if(lstView.size()<100){
								if (StringUtil.isNullOrEmpty(message)){
									message = "OK";
								}
								message = StringUtil.convertHTMLBreakLine(message);
								lstView.add(StringUtil.addFailBean(row,message));
							}
							typeView = true;
						}
					}
				}				
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_FOCUS_PROGRAM_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}		
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	
	/**
	 * Save info.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String saveInfo(){
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
		if (currentUser != null) {
			try {
				FocusProgram focusProgram = focusProgramMgr.getFocusProgramByCode(code);
				if (id != null && id > 0) {
					if (focusProgram != null) {
						if (id.intValue() != focusProgram.getId().intValue()) {
							errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, code);
						} else {
							if (isActivePermission()) {//active httm
								/** Khong tim thay y nghia trong doan code nay
								 * @author hunglm16
								 * @sice October 09, 2015
								 * @description Dong code du thua
								if (focusProgram.getStatus().getValue().equals(ActiveType.WAITING.getValue())) {//chi dc sua trang thai

								} else if (focusProgram.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {

								}**/
							} else {
								//httm
								if (focusProgram.getStatus().getValue().equals(ActiveType.WAITING.getValue())) {
									if (status != ActiveType.WAITING.getValue()) {
										result.put(ERROR, true);
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED2, code));
										return JSON;
									}
								}
							}
							if (ActiveType.WAITING.getValue().equals(status) && ActiveType.RUNNING.equals(focusProgram.getStatus())) {//Dang hoat dong khong the update thanh du thao
								result.put(ERROR, true);
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.error.running.to.waiting"));
								return JSON;
							}
							//kiem tra update tu du thao len hoat dong neu chua day du du~ lieu ( dvtg , sp ... ) thi bao loi.
							try {
								if (ActiveType.RUNNING.getValue().equals(status) && ActiveType.WAITING.getValue().equals(focusProgram.getStatus().getValue())) {
									if (!focusProgramMgr.checkUpdateFocusProgramForActive(id).equals(ExceptionCode.ERR_ACTIVE_PROGRAM_OK)) {
										result.put(ERROR, true);
										result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "focus.no.shop.product"));
										return JSON;
									}
								}
							} catch (IllegalArgumentException e) {
								result.put(ERROR, true);
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ERR_ACTIVE_FOCUS_PROGRAM_ALL"));
								return JSON;
							}
							//Kien tra ATTT XSS
							errMsg = "";
							if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(code)) {
								errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
							}
							if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(name)) {
								errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
							}
							if (!StringUtil.isNullOrEmpty(errMsg)) {
								result.put(ERROR, true);
								result.put("errMsg", errMsg);
								return JSON;
							}
							focusProgram.setFocusProgramCode(code);
							focusProgram.setFocusProgramName(name);
							focusProgram.setStatus(ActiveType.parseValue(status));
							if (!StringUtil.isNullOrEmpty(fromDate)) {
								focusProgram.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
							}
							if (!StringUtil.isNullOrEmpty(toDate)) {
								focusProgram.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
							} else {
								focusProgram.setToDate(null);
							}
							focusProgram.setUpdateDate(DateUtil.now());
							focusProgram.setUpdateUser(currentUser.getUserName());
							focusProgramMgr.updateFocusProgram(focusProgram, getLogInfoVO());
							error = false;
							result.put("id", id);
						}
					}

				} else {
					if (focusProgram != null) {
						errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, code);
					} else {
						if (status != ActiveType.WAITING.getValue()) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED2, code));
							return JSON;
						}
						FocusProgram newFocusProgram = new FocusProgram();
						newFocusProgram.setFocusProgramCode(code);
						newFocusProgram.setCreateUser(currentUser.getUserName());
						newFocusProgram.setFocusProgramName(name);
						newFocusProgram.setStatus(ActiveType.parseValue(status));
						if (!StringUtil.isNullOrEmpty(fromDate)) {
							newFocusProgram.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
						}
						if (!StringUtil.isNullOrEmpty(toDate)) {
							newFocusProgram.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
						}
						//	    				saveAttributeData(newFocusProgram);
						newFocusProgram = focusProgramMgr.createFocusProgram(newFocusProgram, getLogInfoVO());
						if (newFocusProgram != null) {
							error = false;
							result.put("id", newFocusProgram.getId());
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Search shop.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String searchShop(){
	    
	    result.put("page", page);
	    result.put("max", max);
	    try{
//	    	Shop ownerShop = (Shop) request.getSession().getAttribute(ConstantManager.SESSION_SHOP);
//			KPaging<FocusShopMap> kPaging = new KPaging<FocusShopMap>();
//			kPaging.setPageSize(max);
//			kPaging.setPage(page-1);		
//			ObjectVO<FocusShopMap> focusShopMapVO = focusProgramMgr.getListFocusShopMap(kPaging, id,code,name,ActiveType.parseValue(status), ownerShop == null ? null : ownerShop.getId())	;		
//			if(focusShopMapVO!= null){
//			    result.put("total", focusShopMapVO.getkPaging().getTotalRows());
//			    result.put("rows", focusShopMapVO.getLstObject());		
//			}    		
	    	List<JsPromotionShopTreeNode> lstStaffTypeTree = new ArrayList<JsPromotionShopTreeNode>();
			JsPromotionShopTreeNode bean = new JsPromotionShopTreeNode();
			/*
			 * comment code loi compile
			 * @modified by tuannd20
			 * @date 20/12/2014
			 */
			/*NewTreeVO<Shop,ProgrameExtentVO> shopTree = shopMgr.getShopTreeInFocusProgram(id, shopCode, shopName);
	    	if (shopTree.getObject() == null
					&& shopTree.getListChildren() != null) {
				for (int i = 0; i < shopTree.getListChildren().size(); i++) {
					JsPromotionShopTreeNode tmp = new JsPromotionShopTreeNode();
					tmp = getJsTreeNode(tmp, shopTree.getListChildren().get(i));
					 //tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					lstStaffTypeTree.add(tmp);
				}
			} else {
				bean = getJsTreeNode(bean, shopTree);
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
				lstStaffTypeTree.add(bean);
			}*/
	    	result.put("rows", lstStaffTypeTree);
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	/**
	 * Save shop.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String saveShop() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null && lstShopId != null && lstShopId.size() > 0 && id != null && id > 0) {
			try {
				//check shop co phai VNM ko http://192.168.1.92:8000/mantis/view.php?id=3794
				/*
				 * for(int i = 0; i < lstShopId.size(); i++) { Shop chooseShop =
				 * shopMgr.getShopById(lstShopId.get(i)); if(chooseShop == null
				 * || ActiveType.DELETED.equals(chooseShop.getStatus())) {
				 * errMsg =
				 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				 * result.put(ERROR, true); result.put("errMsg", errMsg); return
				 * JSON; } else if(chooseShop.getType() != null &&
				 * chooseShop.getType
				 * ().getChannelTypeCode().equals(ShopChannelType
				 * .VNM.getValue())) { errMsg =
				 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
				 * "common.catalog.focus.program.shop.not.vnm");
				 * result.put(ERROR, true); result.put("errMsg", errMsg); return
				 * JSON; } }
				 */
				focusProgramMgr.createListFocusShopMapEx(id, lstShopId, ActiveType.parseValue(status), getLogInfoVO());
				error = false;
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Delete shop.
	 * @modifier: phut -> sua delete - > delete status
	 * 
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String deleteShop() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				FocusShopMap focusShopMap = focusProgramMgr.getFocusShopMapById(shopMapId);
				if (focusShopMap != null) {
					focusShopMap.setStatus(ActiveType.DELETED);
					//	    			focusProgramMgr.deleteFocusShopMap(focusShopMap,getLogInfoVO());
					focusProgramMgr.updateFocusShopMap(focusShopMap, getLogInfoVO());
					error = false;
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Search product.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String searchProduct(){
	    
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<FocusChannelMapProduct> kPaging = new KPaging<FocusChannelMapProduct>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			ObjectVO<FocusChannelMapProduct> focusChannelMapProductVO = focusProgramMgr.getListFocusChannelMapProduct(kPaging,code,name, id, null);		
			if(focusChannelMapProductVO!= null){
				int size = focusChannelMapProductVO.getLstObject().size();
				for(int i=0;i<size;i++){
					FocusChannelMapProduct tmp = focusChannelMapProductVO.getLstObject().get(i);
					if(tmp.getFocusChannelMap() != null){
						tmp.setCreateUser(tmp.getFocusChannelMap().getSaleTypeCode());
					}
				}
			    result.put("total", focusChannelMapProductVO.getkPaging().getTotalRows());
			    result.put("rows", focusChannelMapProductVO.getLstObject());		
			}    		
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	/**
	 * Save product.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String saveProduct() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			if (currentUser != null) {
				if (isUpdate != null && isUpdate) {
					FocusChannelMapProduct obj = focusProgramMgr.getFocusChannelMapProductById(focusMapProductId);
					if (obj != null) {
						ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, id, staffTypeCode, ActiveType.RUNNING);
						if (focusChannelMapVO != null && focusChannelMapVO.getLstObject() != null && focusChannelMapVO.getLstObject().size() > 0) {
							obj.setFocusChannelMap(focusChannelMapVO.getLstObject().get(0));
						}
						ApParam focusType = apParamMgr.getApParamByCode(focusProductType, ApParamType.FOCUS_PRODUCT_TYPE);
						if (focusType != null) {
							obj.setType(focusProductType);
						}
						focusProgramMgr.updateFocusChannelMapProduct(obj, getLogInfoVO());
						error = false;
					}
				} else {
					if (lstProductId != null && lstProductId.size() > 0 && id != null && id > 0) {
						FocusChannelMap focusChannelMap = focusProgramMgr.getFocusChannelMapById(channelTypeId);
						String apParamCode = null;
						if (focusChannelMap != null) {
							apParamCode = focusChannelMap.getSaleTypeCode();
						}
						ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, id, apParamCode, ActiveType.RUNNING);
						if (focusChannelMapVO == null || focusChannelMapVO.getLstObject() == null || focusChannelMapVO.getLstObject().size() == 0) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_CORRECT, null, "catalog.sale.type");
						} else if (type != null && type == -2) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.sale.focus.program.type");
						} else {
							ApParam focusType = apParamMgr.getApParamById(Long.valueOf(type.toString()));
							if (focusType != null) {
								focusProgramMgr.createListFocusChannelMapProduct(lstProductId, id, apParamCode, getLogInfoVO(), focusType.getApParamCode());
								error = false;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Delete product.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String deleteProduct() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				FocusChannelMapProduct focusChannelMapProduct = focusProgramMgr.getFocusChannelMapProductById(productId);
				if (focusChannelMapProduct != null) {
					focusProgramMgr.deleteFocusChannelMapProduct(focusChannelMapProduct, getLogInfoVO());
					error = false;
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Import shop.
	 *
	 * @return the string
	 * @author hungtx,tientv11
	 * @since Aug 25, 2012
	 */
	public String importShop() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = false;
		List<CellBean> lstFails = new ArrayList<CellBean>();		
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,1);		
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				FocusShopMap focusShopMap = null;
				FocusProgram focusProgram = focusProgramMgr.getFocusProgramById(id);
				List<String> row = null;
				String checkFileImport = null;
				for(int i=0;i<lstData.size();i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
						focusShopMap = new FocusShopMap();
						row = lstData.get(i);
						
						checkFileImport = checkFileImportShop(lstData, i);
						if (!StringUtil.isNullOrEmpty(checkFileImport)) {
							typeView = false;
							lstFails.add(StringUtil.addFailBean(row, checkFileImport));
							continue;
						}
						focusShopMap.setFocusProgram(focusProgram);
						String parentShopCode = null;
						if(session!= null && session.getAttribute(ConstantManager.SESSION_SHOP)!= null){
					    	parentShopCode = ((Shop)session.getAttribute(ConstantManager.SESSION_SHOP)).getShopCode();
					    }
						//shop code
						Shop shop = null;	
						if(row.size() > 0){
							String value = row.get(0);
							if(!StringUtil.isNullOrEmpty(value)){
								shop = shopMgr.getShopByCode(value);
								if(shop == null){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"catalog.focus.program.shop.code");
								}
								else if(ActiveType.STOPPED.getValue().equals(shop.getStatus().getValue())){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,true,"catalog.focus.program.shop.code");										
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,true,"catalog.focus.program.shop.code");									
							}
							focusShopMap.setShop(shop);
						}
						boolean isExists = false;
						if(shop!=null && focusProgram!=null && focusProgramMgr.checkIfFocusShopMapExist(focusProgram.getId(), shop.getShopCode(), null)){
							isExists = true;							
						}
						if(isExists && focusProgram.getId()!=null && !StringUtil.isNullOrEmpty(shop.getShopCode())){
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.shop.is.exists") + "\n";
						}
						if(StringUtil.isNullOrEmpty(message)){
							boolean flag1 = focusProgramMgr.isExistChildJoinProgram(shop.getShopCode(),id);
							if(flag1){
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.shop.con.is.exists") + "\n";
							}
						}
						if (StringUtil.isNullOrEmpty(message)){
								focusShopMap.setCreateDate(commonMgr.getSysDate());
								focusShopMap.setCreateUser(currentUser.getUserName());
								focusShopMap.setStatus(ActiveType.RUNNING);
								focusProgramMgr.createFocusShopMap(focusShopMap, getLogInfoVO());
								importProgram = "shop";						
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}				
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_FOCUS_PROGRAM_SHOP_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}		
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	/**
	 * Import staff.
	 *
	 * @return the string
	 * @author hungtx
	 * @since Aug 25, 2012
	 */
	private String importStaff() {
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = true;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,3);
//		int n = 0;
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				String checkFileImport = null;
				List<String> row = null;
				FocusProgram focusProgram = focusProgramMgr.getFocusProgramById(id);
				for (int  i= 0, sz = lstData.size(); i < sz; i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
//						FocusChannelMap focusChannelMap = new FocusChannelMap();//Ko duoc tao focusChannelMap trong tab san pham.							
						row = lstData.get(i);
//						n++;
						checkFileImport = checkFileImport(lstData, i);
						if (!StringUtil.isNullOrEmpty(checkFileImport)) {
							typeView = false;
							lstFails.add(StringUtil.addFailBean(row,checkFileImport));
							continue;
						}
						if(focusProgram != null){
//							focusChannelMap.setFocusProgram(focusProgram);//Ko duoc tao focusChannelMap trong tab san pham.
							//sale type
							ApParam channelType = null;						 
							if(row.size() > 0){
								String value = row.get(0);
								if(!StringUtil.isNullOrEmpty(value)){
									channelType = apParamMgr.getApParamByCodeX(value, ApParamType.STAFF_SALE_TYPE, null);
									if(channelType == null){
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true,"catalog.sale.type.code");										
									} else if(!ActiveType.RUNNING.equals(channelType.getStatus())){
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true,"catalog.sale.type.code");
									} else if(focusProgram!= null){
										ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, focusProgram.getId(),value,ActiveType.RUNNING);	
										if(!(focusChannelMapVO!=null && focusChannelMapVO.getLstObject()!= null && focusChannelMapVO.getLstObject().size()>0)){
											message+= Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.sale.type.not.in.focusprogram");
											message+= "\n";
										}
//										if(focusChannelMapVO!= null && focusChannelMapVO.getLstObject()!= null 
//												&& focusChannelMapVO.getLstObject().size()>0 && focusChannelMapVO.getLstObject().get(0)!= null){
//										message+= Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.sale.type.exist");
//										message+= "\n";
//											focusChannelMap = focusChannelMapVO.getLstObject().get(0);//Ko duoc tao focusChannelMap trong tab san pham.
//										}else{
//											
//										}
									}
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true,"catalog.sale.type.code");
								}	
//								if(channelType!=null){
//									focusChannelMap.setSaleTypeCode(channelType.getApParamCode());//Ko duoc tao focusChannelMap trong tab san pham.
//								}
							}						
							//product code
							Product product = null;
							String productCode = null;						
							if(row.size() > 1){
								String value = row.get(1);
								productCode = value;
								if(!StringUtil.isNullOrEmpty(productCode)){
									product = productMgr.getProductByCode(productCode);
									if(product == null){
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true,"catalog.product.code");										
									}else if(ActiveType.STOPPED.getValue().equals(product.getStatus().getValue())){										
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true,"catalog.product.code");
									}
//									if(focusProgram!=null && product!=null && focusProgramMgr.checkIfProductInFocusProgram(focusProgram.getId(), productCode)){
//										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_FOCUS_PROGRAM_EXIST_PRODUCT, product.getProductCode());									
//									}
								} else {									
									message+= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.product.code");
								}
							}						
							ApParam apParam  =null;
							if(row.size() > 2){
								String value = row.get(2).trim();
								String msg = ValidateUtil.validateField(value, "catalog.focus.program.product.type",20,ConstantManager.ERR_REQUIRE);
								if(!StringUtil.isNullOrEmpty(msg)){
									message += msg;
								}else{
									apParam = apParamMgr.getApParamByCodeX(value, ApParamType.FOCUS_PRODUCT_TYPE, null);
									if(apParam == null){
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.product.type");
									} else if(!ActiveType.RUNNING.equals(apParam.getStatus())){
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true,"catalog.focus.program.product.type");
									}
								}
							}
							
							if (StringUtil.isNullOrEmpty(message)) {
								boolean b = focusProgramMgr.checkExistsProductMap(focusProgram.getId(), row.get(0), row.get(1), row.get(2));
								if (b) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "focus.import.product.exist.db");
								}
							}
							
							if(isView == 0){
								if (StringUtil.isNullOrEmpty(message)){							
//									if(focusChannelMap.getId() == null){//Ko duoc tao focusChannelMap trong tab san pham.
//										focusChannelMap.setCreateUser(currentUser.getUserName());//Ko duoc tao focusChannelMap trong tab san pham.
//										focusChannelMap.setStatus(ActiveType.RUNNING);//Ko duoc tao focusChannelMap trong tab san pham.
//										focusProgramMgr.createFocusChannelMap(focusChannelMap, getLogInfoVO());//Ko duoc tao focusChannelMap trong tab san pham.						
//									}
									List<Long> lstProduct = new ArrayList<Long>();
									lstProduct.add(product.getId());
									focusProgramMgr.createListFocusChannelMapProduct(lstProduct, focusProgram.getId(),  channelType.getApParamCode(), getLogInfoVO(),apParam.getApParamCode());							
								} else {
									lstFails.add(StringUtil.addFailBean(row, message));
								}	
								typeView = false;
							}else{
								if(lstView.size()<100){
									if (StringUtil.isNullOrEmpty(message)){
										message = "OK";
									}
									message = StringUtil.convertHTMLBreakLine(message);
									lstView.add(StringUtil.addFailBean(row,message));
								}
								typeView = true;
							}
						}
					}
				}				
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_FOCUS_PROGRAM_STAFF_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	/**
	 * Import excel.
	 *
	 * @return the string
	 * @author hungtx
	 * @since Aug 25, 2012
	 */
	public String importExcel(){		
		if(excelType == 1){
			return importFocusProgram();
		} else if(excelType == 2){
			return importShop();
		}
		return importStaff(); 
	}
	
	public String getListExcelData(){
		if(excelType == 1){
			return exportFocusProgram();
		} else if(excelType == 2){
			return exportFocusShopMap();
		}
		return exportStaff();
	}
	
	public String exportFocusProgram(){
		try{
	    	Date fDate = null;
			Date tDate = null;
			FocusProgramFilter fpf = new FocusProgramFilter();
			if(!StringUtil.isNullOrEmpty(fromDate)){
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				fpf.setFromDate(fDate);
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				fpf.setToDate(tDate);
			}

			fpf.setFocusProgramCode(code);
			fpf.setFocusProgramName(name);
			fpf.setShopCode(shopCode);
			fpf.setStatus(ActiveType.parseValue(status));
			StaffRole role = null;
			if (checkRoles(VSARole.VNM_SALESONLINE_HO)) {
				role = StaffRole.VNM_SALESONLINE_HO;
			} else if (checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) {
				role = StaffRole.VNM_SALESONLINE_DISTRIBUTOR;
			}

			if (shopId != null) {
				fpf.setShopId(shopId);	
			}
			fpf.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			fpf.setRollId(currentUser.getRoleToken().getRoleId());
			fpf.setShopRootId(currentUser.getShopRoot().getShopId());
			
			ObjectVO<FocusProgram> focusProgramVO = focusProgramMgr.getListFocusProgram(null, fpf, role);
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if(focusProgramVO != null && focusProgramVO.getLstObject() != null){
				int size = focusProgramVO.getLstObject().size();
				for(int i=0;i<size;i++){
					FocusProgram temp = focusProgramVO.getLstObject().get(i);
					CellBean cellBean = new CellBean();
					cellBean.setContent1(temp.getFocusProgramCode());
					cellBean.setContent2(temp.getFocusProgramName());
					if(temp.getFromDate() != null){
						cellBean.setContent3(DateUtil.toDateString(temp.getFromDate(), ConstantManager.FULL_DATE_FORMAT));
					}
					if(temp.getToDate() != null){
						cellBean.setContent4(DateUtil.toDateString(temp.getToDate(), ConstantManager.FULL_DATE_FORMAT));
					}
					if(temp.getStatus() != null){
						if(ActiveType.RUNNING.equals(temp.getStatus())){
							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.name"));
						}else if(ActiveType.STOPPED.equals(temp.getStatus())){
							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"pause.status.name"));
						}else if(ActiveType.WAITING.equals(temp.getStatus())){
							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.waiting"));
						}
					}
					lstFails.add(cellBean);
				}
			}
			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_FOCUS_EXPORT);
	    }catch (Exception e) {
	    	result.put(ERROR, true);
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	public String exportShop(){
		try{
			Date fDate = null;
			Date tDate = null;
			if(!StringUtil.isNullOrEmpty(fromDate)){
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			ObjectVO<FocusShopMap> focusShopMapVO = focusProgramMgr.getListFocusShopMapV2(null, code, name, shopCode, shopName, fDate, tDate,ActiveType.parseValue(status));
			
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if(focusShopMapVO != null && focusShopMapVO.getLstObject() != null){
				int size = focusShopMapVO.getLstObject().size();
				for(int i=0;i<size;i++){
					FocusShopMap temp = focusShopMapVO.getLstObject().get(i);
					CellBean cellBean = new CellBean();
					if(temp.getFocusProgram() != null){
						cellBean.setContent1(temp.getFocusProgram().getFocusProgramCode());
					}
					if(temp.getShop() != null){
						cellBean.setContent2(temp.getShop().getShopCode());
						cellBean.setContent3(temp.getShop().getShopName());
					}
					if(temp.getStatus() != null){
						if(ActiveType.RUNNING.equals(temp.getStatus())){
							cellBean.setContent4(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.name"));
						}else if(ActiveType.STOPPED.equals(temp.getStatus())){
							cellBean.setContent4(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"pause.status.name"));
						}
					}
					lstFails.add(cellBean);
				}
			}
			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_FOCUS_SHOP_EXPORT);
	    }catch (Exception e) {
	    	result.put(ERROR, true);
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	public String exportStaff(){
		try{
			List<CellBean> lstFails = new ArrayList<CellBean>();
			ObjectVO<FocusChannelMapProduct> lstFCMP = focusProgramMgr.getListFocusChannelMapProductEx(null, code, name, id, null , null);
			for(FocusChannelMapProduct fcmp : lstFCMP.getLstObject()) {
				CellBean cellBean = new CellBean();
				if(fcmp.getFocusChannelMap() != null){
					cellBean.setContent1(fcmp.getFocusChannelMap().getSaleTypeCode());
				}
				cellBean.setContent2(fcmp.getProduct() != null ? fcmp.getProduct().getProductCode() : null);
				cellBean.setContent3(fcmp.getType());
				lstFails.add(cellBean);
			}
			
			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_FOCUS_STAFF_EXPORT);
	    }catch (Exception e) {
	    	result.put(ERROR, true);
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	public String searchInfoChange(){
	    
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<ActionAudit> kPaging = new KPaging<ActionAudit>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);	
			Date fDate = null;
			Date tDate = null;
			if(!StringUtil.isNullOrEmpty(fromDate)){
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(fromDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.fromDate"), null);
				if(StringUtil.isNullOrEmpty(checkMessage)){
					fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else{
					return JSON;
				}
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(toDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.toDate"), null);
				if(StringUtil.isNullOrEmpty(checkMessage)){
					tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else{
					return JSON;
				}
			}
			
			if(!DateUtil.compareDate(fDate, tDate)){
				return JSON;
			}
			
			if(id!= null){				
				ObjectVO<ActionAudit> lstActionAudit = logMgr.getListActionAudit(kPaging,null,id,ActionAuditType.FOCUS_PROGRAM,fDate,tDate);
				if(lstActionAudit!= null){
				   	result.put("total", lstActionAudit.getkPaging().getTotalRows());
				   	result.put("rows", lstActionAudit.getLstObject());		
			    }
			}
					
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	
	public String searchInfoChangeDetail(){
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<ActionAuditDetail> kPaging = new KPaging<ActionAuditDetail>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			if(id!=null && id!=0){
				ObjectVO<ActionAuditDetail> lstActionAuditDetail = logMgr.getListActionAuditDetail(kPaging,id);
			    if(lstActionAuditDetail!= null){
				   	result.put("total", lstActionAuditDetail.getkPaging().getTotalRows());
				   	result.put("rows", lstActionAuditDetail.getLstObject());		
			    }
			}
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	public String saveFocusChanelMap(){
		try {
			
		} catch (Exception e) {
		}
		return JSON;
	}
	
	/**
	 * Copy focus program.
	 *
	 * @return the string
	 * @author hungtt
	 * @since May 27, 2013
	 */
	public String copyFocusProgram() {
		try {
			FocusProgram fp = focusProgramMgr.getFocusProgramByCode(code);
			if(fp != null)
			{
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FOCUS_PROGRAM_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.focus.program.exists")));
				return JSON;
			}
			FocusProgram pp = focusProgramMgr.copyFocusProgram(id,getLogInfoVO(), code, name);
			if(pp != null)
			{
				result.put("focusId", pp.getId());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);

		}
		return JSON;
	}
	
	/**
	 * Gets the js tree node.
	 *
	 * @param node the node
	 * @param channelType the channel type
	 * @return the js tree node
	 * @author hungtt
	 * @since Jun 3, 2013
	 */
	private JsPromotionShopTreeNode getJsTreeNode(JsPromotionShopTreeNode node,
			NewTreeVO<Shop, ProgrameExtentVO> channelType) {
		if (channelType != null && channelType.getObject() != null) {
			node.setData(channelType.getObject().getShopCode()+"-"+channelType.getObject().getShopName());
			node.setId(channelType.getObject().getId());
			node.setIsShop(channelType.getDetail().getIsNPP());
			List<JsPromotionShopTreeNode> lstChild = new ArrayList<JsPromotionShopTreeNode>();
			if (channelType.getListChildren() != null
					&& channelType.getListChildren().size() > 0) {
				for (int i = 0; i < channelType.getListChildren().size(); i++) {
					JsPromotionShopTreeNode subBean = new JsPromotionShopTreeNode();
					subBean = getJsTreeNode(subBean, channelType
							.getListChildren().get(i));
					lstChild.add(subBean);
				}
				node.setChildren(lstChild);
				//node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			}
		}
		return node;
	}

	/**
	 * Creates the focus program.
	 *
	 * @return the string
	 * @author hungtt
	 * @since Jun 3, 2013
	 */
	public String createFocusProgram(){
		try
		{
			List<FocusShopMap> listFSM = new ArrayList<FocusShopMap>();
			if (listShopId != null) {
				for (int i = 0; i < listShopId.size(); i++) {
					FocusShopMap fsm = new FocusShopMap();
					Shop s = shopMgr.getShopById(listShopId.get(i));
					FocusProgram pp = focusProgramMgr.getFocusProgramById(id);
					if(pp != null && pp.getStatus().getValue() != ActiveType.WAITING.getValue()){
						result.put(ERROR, true);
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "focus.program.incorrect"));
						return JSON;
					}
					if(s == null){
						result.put(ERROR, true);
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.not.exist"));
						return JSON;
					}
					if(s.getStatus().getValue() != ActiveType.RUNNING.getValue()){
						result.put(ERROR, true);
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.incorrect"));
						return JSON;
					}
					fsm.setShop(s);
					fsm.setFocusProgram(pp);
					fsm.setCreateDate(DateUtil.now());
					if(currentUser != null)
					{
						fsm.setCreateUser(currentUser.getUserName());
					}
					if (fsm != null) {
						listFSM.add(fsm);
					}
					
				}
				focusProgramMgr.createListFocusShopMap(listFSM,getLogInfoVO());
			}
		}
		catch(Exception e)
		{
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}
	
	public String deleteFocusShopMap(){
		try
		{
			Shop s = shopMgr.getShopById(shopId);
			if(s != null)
			{
				focusProgramMgr.deleteFocusShopMap(shopId, id, currentUser.getShopRoot().getShopId(), getLogInfoVO());
				shopCode = (s != null)?s.getShopCode():"";
				result.put("shopCode", shopCode);
			}
			else
			{
				result.put(ERROR, true);
				result.put("promotionNull", true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PROMOTION_NOT_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"promotion.shop.not.exist")));
			}
		}
		catch(Exception e)
		{
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}
	
	/**
	 * Export focus shop map.
	 *
	 * @return the string
	 * @author hungtt
	 * @since Jun 4, 2013
	 */
	public String exportFocusShopMap()
	{
		try {
			ObjectVO<FocusShopMap> objectFSM = focusProgramMgr.getListFocusShopMap(null, id, shopCode, shopName, ActiveType.RUNNING, null);
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if (objectFSM != null && objectFSM.getLstObject() != null) {
				int size = objectFSM.getLstObject().size();
				for (int i = 0; i < size; i++) {
					FocusShopMap temp = objectFSM.getLstObject().get(i);
					CellBean cellBean = new CellBean();
					if (temp.getShop() != null) {
						cellBean.setContent1(temp.getShop().getShopCode());
					}
					
					lstFails.add(cellBean);
				}
			}
			exportExcelDataNotSession(lstFails,
					ConstantManager.TEMPLATE_CATALOG_FOCUS_SHOP_MAP_EXPORT);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	/**
	 * Tra ve man hinh chi tiet cua CTTT(Bao gom them moi va cap nhat)
	 * @author nhanlt
	 * @return getChangedForm
	 * 
	 * @modify hunglm16
	 * @since October 09, 2015
	 * @description ra soat code va author
	 */
	public String getChangedForm(){
		resetToken(result);
		try {
			if (checkRoles(VSARole.VNM_SALESONLINE_HO))
				roleType = 1;
			else if (checkRoles(VSARole.VNM_SALESONLINE_GS))
				roleType = 2;
			else if (checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR))
				roleType = 3;
			else
				roleType = 0;

			if (id != null && id > 0) {
				focusProgram = focusProgramMgr.getFocusProgramById(id);
				if (focusProgram != null) {
					code = focusProgram.getFocusProgramCode();
					name = focusProgram.getFocusProgramName();
					if (focusProgram.getFromDate() != null) {
						fromDate = DateUtil.toDateString(focusProgram.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					if (focusProgram.getToDate() != null) {
						toDate = DateUtil.toDateString(focusProgram.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					status = focusProgram.getStatus().getValue();
				}
			} else {
				status = ActiveType.RUNNING.getValue();
			}
			ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, id, null, ActiveType.RUNNING);
			if (focusChannelMapVO != null) {
				lstFocusChannelMap = focusChannelMapVO.getLstObject();
			}
			lstType = apParamMgr.getListApParam(ApParamType.FOCUS_PRODUCT_TYPE, ActiveType.RUNNING);

			permissionEdit = false;
			if ((isCommercialSupportPermission() && (id == null || (id != null && ActiveType.WAITING.getValue() == status))) || (isActivePermission() && id != null && ActiveType.STOPPED.getValue() != status)) {
				permissionEdit = true;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	private Integer roleType ;
	private FocusProgram focusProgram;
	private Long focusProgramId ;
	private List<Long> lstApParamId;
	
	/**
	 * Tim kiem HTBH cua CTTT
	 * @return searchHTBH
	 * @author thongnm 
	 */
	public String searchHTBH(){
	    try{
		    result.put("page", page);
		    result.put("rows", rows);
		    if(page == 0)
		    {
		    	page = 1;
		    }
		    if(rows == 0){
		    	rows = 10;
		    }
			HTBHFilter filter = new HTBHFilter();
			filter.setCode(code);
			filter.setName(name);
			filter.setFocusProgramId(focusProgramId);
			filter.setStatus(ActiveType.RUNNING);
			
			if(checkRoles(VSARole.VNM_SALESONLINE_HO)) {
				ObjectVO<HTBHVO> vo = focusProgramMgr.getListHTBHVO(filter, null);
			    if(vo != null){
			    	result.put("total", vo.getLstObject().size());
				    result.put("rows", vo.getLstObject());	
			    } else {
			    	result.put("total", 0);
				    result.put("rows", new ArrayList<HTBHVO>());	
			    }
			    result.put("page", page);
			} else {
				filter.setIsHO(false);
				KPaging<HTBHVO> kPaging = new KPaging<HTBHVO>();
				kPaging.setPageSize(rows);
				kPaging.setPage(page - 1);
				if(focusProgramId!=null && focusProgramId > 0){
					ObjectVO<HTBHVO> vo = focusProgramMgr.getListHTBHVO(filter, kPaging);
				    if(vo != null){
				    	result.put("total", vo.getkPaging().getTotalRows());
					    result.put("rows", vo.getLstObject());	
				    }
				}
				updateTotalRowGrid();
			}
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	/**
	 * Luu HTBH cua CTTT
	 * @return searchHTBH
	 * @author nhanlt
	 * 
	 * @modify hunglm16
	 * @since October 09, 2015
	 * @description ra soat code va author
	 */
	public String saveHTBH() {
		resetToken(result);
		try {
			//CHECK QUEN NHO DOI LAI LA HO VA TON TAI DON VI CUA USER DANG NHAP
			boolean isPermission = checkRoles(VSARole.VNM_SALESONLINE_HO);
			if (isPermission == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "catalog.focus.program.update.htbh.perimission"));
				return ERROR;
			}
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			FocusProgram focusProgram = focusProgramMgr.getFocusProgramById(focusProgramId);
			if (focusProgram == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
				return ERROR;
			}

			List<FocusChannelMap> lstFocusChannelMapOlds = focusProgramMgr.getListFocusChannelMap(null, focusProgramId, null, ActiveType.RUNNING).getLstObject();
			List<String> lstApParamCode = new ArrayList<String>();
			List<FocusChannelMap> lstFocusChannelMapsUpdate = new ArrayList<FocusChannelMap>();
			List<FocusChannelMap> lstFocusChannelMapsCreate = new ArrayList<FocusChannelMap>();

			if (lstApParamId != null && lstApParamId.size() > 0) {
				for (int i = 0, n = lstApParamId.size(); i < n; ++i) {
					ApParam apParam = apParamMgr.getApParamById(lstApParamId.get(i));
					lstApParamCode.add(apParam.getApParamCode());
				}
			}
			if (lstFocusChannelMapOlds != null && lstFocusChannelMapOlds.size() > 0) {
				for (FocusChannelMap focusChannelMap : lstFocusChannelMapOlds) {
					String saleTypeCode = focusChannelMap.getSaleTypeCode();
					if (lstApParamCode.contains(focusChannelMap.getSaleTypeCode())) {
						lstFocusChannelMapsUpdate.add(focusChannelMap);
						lstApParamCode.remove(saleTypeCode);
					} else {
						focusChannelMap.setStatus(ActiveType.DELETED);
						lstFocusChannelMapsUpdate.add(focusChannelMap);
					}
				}
			}
			for (String apParamCode : lstApParamCode) {
				ApParam apParam = apParamMgr.getApParamByCodeEx(apParamCode, ActiveType.RUNNING);
				FocusChannelMap focusChannelMap = new FocusChannelMap();
				focusChannelMap.setFocusProgram(focusProgram);
				focusChannelMap.setSaleTypeCode(apParam.getApParamCode());
				focusChannelMap.setStatus(ActiveType.RUNNING);
				lstFocusChannelMapsCreate.add(focusChannelMap);
			}
			focusProgramMgr.createListFocusChannelMap(lstFocusChannelMapsUpdate, lstFocusChannelMapsCreate, getLogInfoVO());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "Error BaryCentricProgrammeCatalogAction.saveHTBH: " + e.getMessage());
			return ERROR;
		}
		return SUCCESS;
	}
	
	/*****************************	GETTER-SETTER ****************************************/
	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	public FocusProgram getFocusProgram() {
		return focusProgram;
	}

	public void setFocusProgram(FocusProgram focusProgram) {
		this.focusProgram = focusProgram;
	}

	public Long getFocusProgramId() {
		return focusProgramId;
	}

	public void setFocusProgramId(Long focusProgramId) {
		this.focusProgramId = focusProgramId;
	}
	public List<Long> getLstApParamId() {
		return lstApParamId;
	}

	public void setLstApParamId(List<Long> lstApParamId) {
		this.lstApParamId = lstApParamId;
	}
	
	public String checkFileImport(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			List<String> row2 = null;
			message = "";
			String htbh = row1.get(0).toUpperCase();
			String sp = row1.get(1).toUpperCase();
			String loai = row1.get(2).toUpperCase();
			String htbh1 = null;
			String sp1 = null;
			String loai1 = null;
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					row2 = lstData.get(k);
					htbh1 = row2.get(0).toUpperCase();
					sp1 = row2.get(1).toUpperCase();
					loai1 = row2.get(2).toUpperCase();
					if (htbh.equals(htbh1) && sp.equals(sp1) && loai.equals(loai1)) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.import.duplicate", k + 2) ;
						} else {
							message += ", " + (k + 2);
						}
					}
				}
			}
		}
		return message;
	}
	
	public String checkFileImportShop(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			List<String> row2 = null;
			message = "";
			String ma = row1.get(0).toUpperCase();
			String ma1 = null;
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					row2 = lstData.get(k);
					ma1 = row2.get(0).toUpperCase();
					if (ma.equals(ma1)) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.import.duplicate", k + 2) ;
						} else {
							message += ", " + (k + 2);
						}
					}
				}
			}
		}
		return message;
	}
}


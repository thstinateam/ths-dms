package ths.dms.web.action.program;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.TreeGridNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;
import ths.dms.web.utils.report.excel.SXSSFReportHelper;

import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CustomerAttributeMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DisplayProgramVNMMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerAttribute;
import ths.dms.core.entities.GroupLevel;
import ths.dms.core.entities.GroupMapping;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductGroup;
import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.PromotionCustAttrDetail;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductGroupType;
import ths.dms.core.entities.enumtype.PromotionProgramFilter;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.enumtype.PromotionType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.PromotionCustomerFilter;
import ths.dms.core.entities.filter.PromotionStaffFilter;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.ExMapping;
import ths.dms.core.entities.vo.ExcelPromotionDetail;
import ths.dms.core.entities.vo.ExcelPromotionHeader;
import ths.dms.core.entities.vo.GroupKM;
import ths.dms.core.entities.vo.GroupLevelVO;
import ths.dms.core.entities.vo.GroupMua;
import ths.dms.core.entities.vo.GroupSP;
import ths.dms.core.entities.vo.LevelMappingVO;
import ths.dms.core.entities.vo.ListGroupKM;
import ths.dms.core.entities.vo.ListGroupMua;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MapMuaKM;
import ths.dms.core.entities.vo.NewLevelMapping;
import ths.dms.core.entities.vo.NewProductGroupVO;
import ths.dms.core.entities.vo.Node;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PPConvertVO;
import ths.dms.core.entities.vo.ProductGroupVO;
import ths.dms.core.entities.vo.ProductInfoVO;
import ths.dms.core.entities.vo.PromotionCustAttUpdateVO;
import ths.dms.core.entities.vo.PromotionCustAttVO2;
import ths.dms.core.entities.vo.PromotionCustAttrVO;
import ths.dms.core.entities.vo.PromotionCustomerVO;
import ths.dms.core.entities.vo.PromotionProductOpenVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.PromotionShopVO;
import ths.dms.core.entities.vo.PromotionStaffVO;
import ths.dms.core.entities.vo.SaleCatLevelVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

public class PromotionCatalogAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	public static final Integer ALL_INTEGER_G = -2;
	public static final int CUSTOMER_TYPE = 2;
	public static final int SALE_LEVEL = 3;
	public static final int AUTO_ATTRIBUTE = 1;
	public static final int ZEZO = 0;
	public static final int maxlengthNumber = 9;
	public static final String QUANTITY = "QUANTITY";
	public static final String AMOUNT = "AMOUNT";
	public static final String PERCENT = "PERCENT";

	private String promotionCode;
	private String promotionName;
	private String startDate;
	private String endDate;
	private String typeCode;
	private Boolean canEdit;
	private Integer isEdited;
	private String description;

	private PromotionProgramMgr promotionProgramMgr;
	private CustomerMgr customerMgr;
	private DisplayProgramVNMMgr displayProgramVNMMgr;

	private PromotionProgram promotionProgram;

	private List<PromotionCustAttrVO> lstPromotionCustAttrVO;
	//private List<PromotionCustAttrVO> listPromotionCustAttrVOAlreadySet;
	private String groupCode;
	private Long mappingId;
	private Long levelMuaId;
	private Long levelKMId;
	private Long levelId;
	private String levelCode;
	private Long levelDetailId;
	private String groupMuaCode;
	private Integer orderLevelMua;
	private String groupKMCode;
	private Integer orderLevelKM;
	private String groupName;
	private Long groupId;
	private Long groupMuaId;
	private Long groupKMId;
	private Integer minQuantity;
	private BigDecimal minAmount;
	private Integer maxQuantity;
	private BigDecimal maxAmount;
	private Boolean multiple;
	private Boolean recursive;
	private Integer stt;
	private Integer copyNum;
	private List<Long> lstId;
	private List<Long> lstCustomerType;
	private List<Long> lstSaleLevelCatId;
	private List<Integer> lstQtt;
	private List<Boolean> lstEdit;
	private List<Integer> lstObjectType;
	private List<String> lstAttDataInField;
	private List<LevelMappingVO> listLevelMapping;
	private List<Long> listLevelId;
	private List<NewLevelMapping> listNewMapping;
	private List<BigDecimal> lstAmt;
	private List<BigDecimal> lstNum;

	private Integer muaMinQuantity;
	private BigDecimal muaMinAmount;
	private BigDecimal amount;
	private BigDecimal number;
	private Float percentKM;
	private Integer kmMaxQuantity;
	private BigDecimal kmMaxAmount;
	private List<ExMapping> listSubLevelMua;
	private List<ExMapping> listSubLevelKM;

	private Long id;
	private Boolean isVNMAdmin;
	private String excelFileContentType;
	public List<CellBean> lstHeaderError;
	public List<CellBean> lstDetailError;
	private List<ApParam> lstTypeCode;
	private List<Product> listProduct;
	private List<Integer> listMinQuantity;
	private List<BigDecimal> listMinAmount;
	private List<Integer> listMaxQuantity;
	private List<BigDecimal> listMaxAmount;
	private List<Integer> listOrder;
	private List<String> listProductDetail;
	private List<Float> listPercent;

	private String code;
	private String name;
	private String address;
	private String fromDate;
	private String toDate;
	private String shopCode;
	private String lstTypeId;

	private Long promotionId;
	private Long shopId;
	private Integer quantity;
	private Integer status;
	private Integer promotionStatus;
	private Integer proType;
	private String promotionType;
	private Integer quantiMonthNewOpen;

	private Integer fromLevel;
	private Integer toLevel;

	private File excelFile;

	private List<ProductGroup> lstGroupSale;
	private List<ProductGroup> lstGroupFree;
	private List<NewProductGroupVO> lstGroupNew;

	private List<PPConvertVO> listConvertGroup;
	private List<PromotionProductOpenVO> listProductOpen;

	private List<GroupLevelVO> lstLevel;

	public static long indexMua = 1;
	public static long indexKM = 1000001;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		displayProgramVNMMgr = (DisplayProgramVNMMgr) context.getBean("displayProgramVNMMgr");
		//Staff loginStaff = getStaffByCurrentUser();
		isVNMAdmin = StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType());
		staff = staffMgr.getStaffById(currentUser.getUserId());
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		try {
			lstTypeCode = apParamMgr.getListApParam(ApParamType.PROMOTION, ActiveType.RUNNING);
			proType = ConstantManager.PROMOTION_AUTO;
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 21, 2014
	 */
	public String search() throws Exception {
		result.put("rows", new ArrayList<PromotionProgram>());
		result.put("total", 0);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				return JSON;
			}
			result.put("page", page);
			result.put("max", max);

			PromotionProgramFilter filter = new PromotionProgramFilter();
			KPaging<PromotionProgram> kPaging = new KPaging<PromotionProgram>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);

			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setToDate(tDate);
			}
			List<String> temp = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty(lstTypeId)) {
				if (lstTypeId.indexOf("-1") > -1) {
					temp = null;
				} else {
					String[] lstTmp = lstTypeId.split(",");
					if (lstTmp.length > 0) {
						Integer size = lstTmp.length;
						for (int i = 0; i < size; i++) {
							ApParam apParam = apParamMgr.getApParamById(Long.valueOf(lstTmp[i].trim()));
							if (apParam != null) {
								temp.add(apParam.getApParamCode());
							}
						}
					}
				}
			}
			filter.setLstType(temp);
			ActiveType at = ActiveType.RUNNING;
			if (status == null) {
				status = ActiveType.RUNNING.getValue();
			}
			if (!ALL_INTEGER_G.equals(status)) {
				at = ActiveType.parseValue(status);
				/**
				 * @author kieupp
				 * Them dieu kien tim kiem dang het han chuong trinh khuyen mai voi
				 * Chuong trinh KM hoat dong nhung to_date nho hon ngay hien tai
				 * dat bien flag den biet dang tim kiem dang o o dang het han
				 */
				if (ActiveType.HET_HAN.getValue().equals(status)) {
					tDate = DateUtil.now(DateUtil.DATE_FORMAT_DDMMYYYY);
					tDate = DateUtil.addDate(tDate, -1);
					filter.setToDate(tDate);
					filter.setFlag(true);
					status = ActiveType.RUNNING.getValue();
					at = ActiveType.parseValue(status);
					filter.setStatus(at);
				} else {
					filter.setStatus(at);
				}
			}
			if (StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(currentUser.getShopRoot().getShopCode());
				filter.setCreateUser(currentUser.getStaffRoot().getStaffCode());
				if (isVNMAdmin) {
					filter.setIsVNM(true);
				}
			} else {
				filter.setShopCode(shopCode);
			}
			filter.setPpCode(code);
			filter.setPpName(name);
			if (proType != null && proType.intValue() == ConstantManager.PROMOTION_AUTO) {
				filter.setIsAutoPromotion(true);				
			} else {
				filter.setIsAutoPromotion(false);
			}
			filter.setStrListShopId(getStrListShopId());
			ObjectVO<PromotionProgram> objVO = promotionProgramMgr.getListPromotionProgram(filter);

			result.put(ERROR, false);
			result.put("rows", objVO.getLstObject());
			result.put("total", objVO.getkPaging().getTotalRows());
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.search - " + ex.getMessage());
			result.put(ERROR, true);
		}
		return JSON;
	}

	public String update() {
		resetToken(result);
		try {
			if (promotionId != null && promotionId > 0) {
				Date __startDate = null;
				Date __endDate = null;
				if (!StringUtil.isNullOrEmpty(startDate)) {
					__startDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (!StringUtil.isNullOrEmpty(endDate)) {
					__endDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
				if (promotionProgram == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
					return SUCCESS;
				}
				if (ActiveType.WAITING.equals(ActiveType.parseValue(status)) 
					&& ActiveType.RUNNING.getValue().equals(promotionProgram.getStatus().getValue())) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.status.error"));
					result.put(ERROR, true);
					return SUCCESS;
				}
				errMsg = ValidateUtil.validateField(promotionName, "catalog.promotion.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(description, "common.description", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				if (ActiveType.WAITING.equals(promotionProgram.getStatus()) && ActiveType.RUNNING.equals(ActiveType.parseValue(status))) {
					List<PromotionShopVO> listShopMap = promotionProgramMgr.getShopTreeInPromotionProgram(currentUser.getShopRoot().getShopId(), promotionProgram.getId(), null, null, null);
					if (listShopMap.isEmpty()) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.shop.map.is.empty"));
						result.put(ERROR, true);
						return SUCCESS;
					}
					List<NewProductGroupVO> __listGroup = promotionProgramMgr.getListNewProductGroupByPromotionId(promotionProgram.getId());
					Boolean hasMapping = false;
					if (!__listGroup.isEmpty()) {
						if (__listGroup.get(0).getStt() == null || __listGroup.get(0).getStt() == 0) {
							for (int i = 0; i < __listGroup.size(); i++) {
								if (__listGroup.get(i).getStt() != null && __listGroup.get(i).getStt() != 0) {
									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.group.order.is.not.null", __listGroup.get(i).getGroupMuaCode()));
									result.put(ERROR, true);
									return SUCCESS;
								}
							}
						} else {
							List<Integer> arrOrder = new ArrayList<Integer>();
							for (int i = 0; i < __listGroup.size(); i++) {
								if (__listGroup.get(i).getStt() == null || __listGroup.get(i).getStt() == 0) {
									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.group.order.is.null", __listGroup.get(i).getGroupMuaCode()));
									result.put(ERROR, true);
									return SUCCESS;
								} else if (arrOrder.indexOf(__listGroup.get(i).getStt()) != -1) {
									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.group.order.is.null", __listGroup.get(i).getGroupMuaCode()));
									result.put(ERROR, true);
									return SUCCESS;
								} else {
									arrOrder.add(__listGroup.get(i).getStt());
								}
							}
						}

						for (NewProductGroupVO groupProduct : __listGroup) {
							List<NewLevelMapping> listMapping = promotionProgramMgr.getListMappingLevel(groupProduct.getGroupMuaId(), groupProduct.getGroupKMId(), null, null).getLstObject();
							if (!listMapping.isEmpty()) {
								hasMapping = true;
								for (NewLevelMapping mapping : listMapping) {
									if (mapping.getListExLevelMua() == null || mapping.getListExLevelMua().isEmpty() || mapping.getListExLevelKM() == null || mapping.getListExLevelKM().isEmpty()) {
										result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.has.no.sub.level.or.product.detail"));
										result.put(ERROR, true);
										return SUCCESS;
									}
								}
							}
						}
						if (!hasMapping) {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.mapping.is.empty"));
							result.put(ERROR, true);
							return SUCCESS;
						}
					} else {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.group.is.empty"));
						result.put(ERROR, true);
						return SUCCESS;
					}

					/*
					 * //check level da phan bo het cho cac co cau hay chua
					 * List<GroupLevel> listLevelNotMapping =
					 * promotionProgramMgr
					 * .getListLevelNotInMapping(promotionProgram.getId());
					 * if(!listLevelNotMapping.isEmpty()) { result.put("errMsg",
					 * Configuration
					 * .getResourceString(ConstantManager.VI_LANGUAGE,
					 * "promotion.product.has.exist.level.not.mapping"));
					 * result.put(ERROR, true); return SUCCESS; }
					 */

					if (PromotionType.ZV23.getValue().equals(promotionProgram.getType())) {
						listConvertGroup = promotionProgramMgr.listPromotionProductConvertVO(promotionProgram.getId(), null);
						boolean isHasRoot = false;
						boolean isHasMore2 = false;
						for (int k = 0; k < listConvertGroup.size(); k++) {
							for (int kk = 0; kk < listConvertGroup.get(k).getListDetail().size(); kk++) {
								if (kk > 0) {
									isHasMore2 = true;
								}
								if (listConvertGroup.get(k).getListDetail().get(kk).getIsSourceProduct() == 1) {
									isHasRoot = true;
								}
							}
							if (!isHasMore2) {
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.product.convert.error.1", listConvertGroup.get(k).getName()));
								result.put(ERROR, true);
								return SUCCESS;
							}
							if (!isHasRoot) {
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.product.convert.error.2", listConvertGroup.get(k).getName()));
								result.put(ERROR, true);
								return SUCCESS;
							}
						}
					}

					if (PromotionType.ZV24.getValue().equals(promotionProgram.getType())) {
						listProductOpen = promotionProgramMgr.listPromotionProductOpenVO(promotionId);
						if (listProductOpen.isEmpty()) {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.product.customer.open.new"));
							result.put(ERROR, true);
							return SUCCESS;
						}
					}
					
					//TungMT comment code
//					if(PromotionType.ZV22.getValue().equals(promotionProgram.getType()) || PromotionType.ZV23.getValue().equals(promotionProgram.getType()) || PromotionType.ZV24.getValue().equals(promotionProgram.getType())) {
//						Boolean checkCoCau = promotionProgramMgr.checkCoCau(promotionProgram.getId());
//						if(!checkCoCau) {
//							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.co.cau.trung.error"));
//							result.put(ERROR, true);
//							return SUCCESS;
//						}
//					}
//					
						
					//check sp ctkm co ton tai trong cac ctkm khac dang hoat dong ko?
//					PromotionProgram existPromotion = promotionProgramMgr.checkProductExistInOrPromotion(promotionProgram.getId());
//					if (existPromotion != null) {
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.exists.in.other.promotion", existPromotion.getPromotionProgramCode()));
//						result.put(ERROR, true);
//						return SUCCESS;
//					}
				}
				promotionProgram.setPromotionProgramName(promotionName);
				promotionProgram.setFromDate(__startDate);
				promotionProgram.setToDate(__endDate);
				if (status != null) {
					promotionProgram.setStatus(ActiveType.parseValue(status));
				}
				promotionProgram.setIsEdited(isEdited);
				promotionProgram.setDescription(description);
				promotionProgram.setQuantiMonthNewOpen(quantiMonthNewOpen);
				promotionProgramMgr.updatePromotionProgram(promotionProgram, getLogInfoVO());
				result.put(ERROR, false);
				result.put("promotionId", promotionProgram.getId());
			} else {
				errMsg = ValidateUtil.validateField(promotionCode, "catalog.promotion.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(promotionName, "catalog.promotion.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(description, "common.description", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramByCode(promotionCode);
				if (promotionProgram != null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, promotionCode));
					result.put(ERROR, true);
					return SUCCESS;
				}
				Date __startDate = null;
				Date __endDate = null;
				if (!StringUtil.isNullOrEmpty(startDate)) {
					__startDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (!StringUtil.isNullOrEmpty(endDate)) {
					__endDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				promotionProgram = new PromotionProgram();
				promotionProgram.setPromotionProgramCode(promotionCode);
				promotionProgram.setPromotionProgramName(promotionName);
				promotionProgram.setFromDate(__startDate);
				promotionProgram.setToDate(__endDate);
				promotionProgram.setStatus(ActiveType.WAITING);
				promotionProgram.setType(typeCode);
				ApParam apParam = apParamMgr.getApParamByCode(typeCode, ApParamType.PROMOTION);
				if (apParam != null) {
					promotionProgram.setProFormat(apParam.getValue());
				}
				promotionProgram.setIsEdited((!StringUtil.isNullOrEmpty(typeCode) && PromotionType.ZV23.getValue().equals(typeCode)) ? 1 : isEdited);
				promotionProgram.setDescription(description);
				promotionProgram.setQuantiMonthNewOpen(quantiMonthNewOpen);
				promotionProgram = promotionProgramMgr.createPromotionProgram(promotionProgram, getLogInfoVO());
				result.put(ERROR, false);
				result.put("promotionId", promotionProgram.getId());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "PromotionCatalogAction.update - " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	/**
	 * Import Chuong trinh khuyen mai
	 * @modify hunglm16
	 * @return
	 * @throws Exception
	 * @since 10/09/2015
	 */
	public String importExcel() throws Exception {
		resetToken(result);
		//Kiem tra tap tin hop le
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			isError = true;
			return SUCCESS;
		}
		return importExcel21ZV();
	}

	private String importExcel21ZV() throws Exception {
		/**
		 * get data for listHeader, listDetail, mapPromotionMua, mapPromotionKM,
		 * mapMuaKM
		 */
		final int NUM_SHEETS = 2;
		try {
			getExcelData21ZV();
			for (String promotionCode : mapErrorPromotion.keySet()) {
				for (int i = 0; i < listHeader.size(); i++) {
					ExcelPromotionHeader header = listHeader.get(i);
					if (!StringUtil.isNullOrEmpty(header.promotionCode) && header.promotionCode.equals(promotionCode)) {
						listHeader.remove(i);
						totalItem = totalItem > 0 ? totalItem - 1 : totalItem;
					}
				}
				mapHeader.remove(promotionCode);
				mapPromotionMua.remove(promotionCode);
				mapPromotionKM.remove(promotionCode);
				mapType.remove(promotionCode);
			}
			String messageError = "";
			for (String promotionCode : mapPromotionMua.keySet()) {
				ListGroupMua listGroupMua = mapPromotionMua.get(promotionCode);
				ListGroupKM listGroupKM = mapPromotionKM.get(promotionCode);
				if (PromotionType.ZV01.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV01(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV02.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV02(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV03.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV03(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV04.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV04(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV05.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV05(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV06.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV06(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV07.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV07(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV08.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV08(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV09.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV09(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV10.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV10(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV11.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV11(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV12.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV12(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV13.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV13(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV14.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV14(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV15.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV15(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV16.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV16(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV17.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV17(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV18.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV18(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV19.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV19(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV20.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV20(promotionCode, listGroupMua, listGroupKM);
				} else if (PromotionType.ZV21.getValue().equals(mapType.get(promotionCode))) {
					messageError = validateZV21(promotionCode, listGroupMua, listGroupKM);
				}
			}

			for (ExcelPromotionHeader header : listHeader) {
				if (mapPromotionMua.get(header.promotionCode) == null || mapPromotionKM.get(header.promotionCode) == null) {
					messageError = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.header.not.has.detail", header.promotionCode);
					break;
				}
			}

			if (!StringUtil.isNullOrEmpty(messageError)) {
				errMsg = messageError;
				isError = true;
				return SUCCESS;
			} else {
				fileNameFail = exportFail();
				if (StringUtil.isNullOrEmpty(fileNameFail)) {
					promotionProgramMgr.importPromotionVNM(listHeader, mapPromotionMua, mapPromotionKM, mapMuaKM, lstPromotionShop, getLogInfoVO());
					for (ExcelPromotionHeader header : listHeader) {
						PromotionProgram pp = promotionProgramMgr.getPromotionProgramByCode(header.promotionCode.toUpperCase());
						if (pp != null) {
							promotionProgramMgr.updateMD5ValidCode(pp, getLogInfoVO());
						}
					}
				}
			}

		} catch (Exception e) {
			if (ERR_NUM_SHEET.equals(e.getMessage())) {
				errMsg = R.getResource("catalog.promotion.import.num.sheet.error", NUM_SHEETS);
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
				LogUtility.logError(e, "PromotionCatalogAction.importExcel - " + e.getMessage());
			}
		} finally {
			// TODO: don dep resource map, list, ...
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		} else {
			isError = true;
		}
		System.gc();
		return SUCCESS;
	}

	public boolean checkMatch2Level(GroupSP preLevel, String productCode) {
		boolean isMatchProduct = false;
		for (int k = 0; k < preLevel.lstSP.size(); k++) {
			if (preLevel.lstSP.get(k).productCode.equals(productCode)) {
				isMatchProduct = true;
				break;
			}
		}
		return isMatchProduct;
	}

	public String validateZV01(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateLineQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreePercent(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, PERCENT);
	}

	public String validateZV02(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateLineQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeAmount(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, AMOUNT);
	}

	public String validateZV03(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateLineQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeItem(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, QUANTITY);
	}

	public String validateZV04(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateLineAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreePercent(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, PERCENT);
	}

	public String validateZV05(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateLineAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeAmount(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, AMOUNT);
	}

	public String validateZV06(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateLineAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeItem(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, QUANTITY);
	}

	public String validateZV07(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateGroupQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreePercent(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, PERCENT);
	}

	public String validateZV08(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateGroupQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeAmount(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, AMOUNT);
	}

	public String validateZV09(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateGroupQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeItem(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, QUANTITY);
	}

	public String validateZV10(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateGroupAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreePercent(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, PERCENT);
	}

	public String validateZV11(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateGroupAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeAmount(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, AMOUNT);
	}

	public String validateZV12(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateGroupAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeItem(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, QUANTITY);
	}

	public String validateZV13(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateBundleQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreePercent(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, PERCENT);
	}

	public String validateZV14(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateBundleQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeAmount(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, AMOUNT);
	}

	public String validateZV15(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateBundleQuantity(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeItem(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, QUANTITY);
	}

	public String validateZV16(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateBundleAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreePercent(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, PERCENT);
	}

	public String validateZV17(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateBundleAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeAmount(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, AMOUNT);
	}

	public String validateZV18(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateBundleAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeItem(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, QUANTITY);
	}

	public String validateZV19(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreePercent(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, PERCENT);
	}

	public String validateZV20(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeAmount(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, AMOUNT);
	}

	public String validateZV21(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		errorMessage = validateAmount(promotionCode, listGroupMua);
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		errorMessage = validateFreeItem(promotionCode, listGroupKM);
		return validateCountLevel(errorMessage, promotionCode, listGroupMua, listGroupKM, QUANTITY);
	}

	public String validateZV22(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		return errorMessage;
	}

	public String validateZV23(String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM) {
		String errorMessage = "";
		return errorMessage;
	}

	public String validateCountLevel(String errorMessage, String promotionCode, ListGroupMua listGroupMua, ListGroupKM listGroupKM, String type) {
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			return errorMessage;
		}
		Integer countLevelMua = 0;
		Integer countLevelKM = 0;
		if (listGroupMua != null) {
			for (GroupMua gm : listGroupMua) {
				if (gm.lstLevel != null) {
					countLevelMua += gm.lstLevel.size();
				}
			}
		}
		if (listGroupKM != null) {
			for (GroupKM gkm : listGroupKM) {
				if (gkm.lstLevel != null) {
					countLevelKM += gkm.lstLevel.size();
				}
			}
		}
		if (!countLevelMua.equals(countLevelKM)) {
			if (QUANTITY.equals(type)) {
				errorMessage = R.getResource("catalog.promotion.import.type.zv09.free.quantity.not.same", promotionCode);
			} else if (AMOUNT.equals(type)) {
				errorMessage = R.getResource("catalog.promotion.import.type.zv09.amount.not.same", promotionCode);
			} else {
				errorMessage = R.getResource("catalog.promotion.import.type.zv09.percent.not.same", promotionCode);
			}
		}
		//SPMua
		if (StringUtil.isNullOrEmpty(errorMessage)) {
			//kiem tra cac mức có andor giong nhau chua
			boolean isErrAndOr = false;
			if (listGroupMua != null) {
				for (GroupMua gm : listGroupMua) {
					if (gm.lstLevel != null && gm.lstLevel.size() > 0) {
						for (GroupSP groupSP : gm.lstLevel) {
							if (groupSP.lstSP != null && groupSP.lstSP.size() > 0) {
								Boolean isRequired = groupSP.lstSP.get(0).isRequired;
								if (isRequired != null) {
									for (Node node : groupSP.lstSP) {
										if (!isRequired.equals(node.isRequired)) {
											isErrAndOr = true;
											break;
										}
									}
									if (isErrAndOr) {
										break;
									}
								}
							}
						}
						if (isErrAndOr) {
							break;
						}
					}
				}
				if (isErrAndOr) {
					errorMessage = R.getResource("catalog.promotion.import.andor.not.match.in.level", promotionCode);
				}
			}
		}

		//SPKM
		if (StringUtil.isNullOrEmpty(errorMessage)) {
			//kiem tra cac mức có andor giong nhau chua
			boolean isErrAndOr = false;
			if (listGroupKM != null) {
				for (GroupKM gkm : listGroupKM) {
					if (gkm.lstLevel != null && gkm.lstLevel.size() > 0) {
						for (GroupSP groupSP : gkm.lstLevel) {
							if (groupSP.lstSP != null && groupSP.lstSP.size() > 0) {
								Boolean isRequired = groupSP.lstSP.get(0).isRequired;
								if (isRequired != null) {
									for (Node node : groupSP.lstSP) {
										if (!isRequired.equals(node.isRequired)) {
											isErrAndOr = true;
											break;
										}
									}
									if (isErrAndOr) {
										break;
									}
								}
							}
						}
						if (isErrAndOr) {
							break;
						}
					}
				}
				if (isErrAndOr) {
					errorMessage = R.getResource("catalog.promotion.import.andor.not.match.in.level", promotionCode);
				}
			}
		}

		return errorMessage;
	}

	private String exportFail() throws Exception {
		SXSSFWorkbook workbook = null;
		OutputStream out = null;
		try {
			if ((lstHeaderError == null || lstHeaderError.isEmpty()) && (lstDetailError == null || lstDetailError.isEmpty()) && (lstPromotionShopError == null || lstPromotionShopError.isEmpty())) {
				return "";
			}
			numFail = 0;
			numFail = lstHeaderError != null ? lstHeaderError.size() : 0;
            numFail = numFail + (lstDetailError != null ? lstDetailError.size() : 0);
            numFail = numFail + (lstPromotionShopError != null ? lstPromotionShopError.size() : 0);
            
//			if (lstHeaderError != null && lstHeaderError.size() > 0) {
//				numFail = lstHeaderError.size();
//			}
//			if (lstDetailError != null && lstDetailError.size() > 0) {
//				numFail = numFail + lstDetailError.size();
//			}
//			if (lstPromotionShopError != null && lstPromotionShopError.size() > 0) {
//				numFail = numFail + lstPromotionShopError.size();
//			}
            
			String outputName = "import_ctkm_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			workbook = new SXSSFWorkbook(200);
			workbook.setCompressTempFiles(true);

			String stmp = R.getResource("catalog.promotion.import.sheetNames");
			String[] str = null;
			if (stmp != null) {
				str = stmp.split(";");
			} else {
				str = new String[] { "1", "2", "3" };
			}
			SXSSFSheet sheetHeader = (SXSSFSheet) workbook.createSheet(str.length > 0 ? str[0] : "1");
			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
			sheetHeader.setDefaultRowHeight((short) (15 * 20));
			sheetHeader.setDefaultColumnWidth(13);
			XSSFCellStyle headerStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.MENU).clone();
			ExcelPOIProcessUtils.setBorderForCell(headerStyle, BorderStyle.THIN, ExcelPOIProcessUtils.poiBlack);
			XSSFCellStyle leftStyle = style.get(ExcelPOIProcessUtils.ROW_LEFT);
			XSSFCellStyle rightStyle = style.get(ExcelPOIProcessUtils.ROW_RIGHT);
			XSSFFont rFont = (XSSFFont) workbook.createFont();
			ExcelPOIProcessUtils.setFontPOI(rFont, ExcelPOIProcessUtils.ARIAL_FONT_NAME, 9, false, ExcelPOIProcessUtils.poiRed);
			XSSFCellStyle errStyle = (XSSFCellStyle) leftStyle.clone();
			errStyle.setFont(rFont);
			errStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);
			errStyle.setWrapText(true);
			style = null;

			SXSSFReportHelper.addCell(sheetHeader, 0, 0, "ProgCode", headerStyle);
			SXSSFReportHelper.addCell(sheetHeader, 1, 0, "ProgDescr", headerStyle);
			SXSSFReportHelper.addCell(sheetHeader, 2, 0, "Release", headerStyle);
			SXSSFReportHelper.addCell(sheetHeader, 3, 0, "ConditionTypeCode", headerStyle);
			SXSSFReportHelper.addCell(sheetHeader, 4, 0, "User1", headerStyle);
			SXSSFReportHelper.addCell(sheetHeader, 5, 0, "FromDate", headerStyle);
			SXSSFReportHelper.addCell(sheetHeader, 6, 0, "ToDate", headerStyle);
			SXSSFReportHelper.addCell(sheetHeader, 7, 0, "errMsg", headerStyle);
			for (int i = 0; i < lstHeaderError.size(); i++) {
				SXSSFReportHelper.addCell(sheetHeader, 0, i + 1, lstHeaderError.get(i).getContent1(), leftStyle);
				SXSSFReportHelper.addCell(sheetHeader, 1, i + 1, lstHeaderError.get(i).getContent2(), leftStyle);
				SXSSFReportHelper.addCell(sheetHeader, 2, i + 1, lstHeaderError.get(i).getContent3(), rightStyle);
				SXSSFReportHelper.addCell(sheetHeader, 3, i + 1, lstHeaderError.get(i).getContent4(), leftStyle);
				SXSSFReportHelper.addCell(sheetHeader, 4, i + 1, lstHeaderError.get(i).getContent5(), leftStyle);
				SXSSFReportHelper.addCell(sheetHeader, 5, i + 1, lstHeaderError.get(i).getContent6(), leftStyle);
				SXSSFReportHelper.addCell(sheetHeader, 6, i + 1, lstHeaderError.get(i).getContent7(), leftStyle);
				SXSSFReportHelper.addCell(sheetHeader, 7, i + 1, lstHeaderError.get(i).getContent8(), errStyle);
			}

			SXSSFSheet sheetDetail = (SXSSFSheet) workbook.createSheet(str.length > 1 ? str[1] : "2");
			sheetDetail.setDefaultRowHeight((short) (15 * 20));
			sheetDetail.setDefaultColumnWidth(13);
			SXSSFReportHelper.addCell(sheetDetail, 0, 0, "ProgCode2", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 1, 0, "ConditionTypeCode3", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 2, 0, "SaleProItem", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 3, 0, "SaleQty", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 4, 0, "SaleUOM", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 5, 0, "SaleAmt", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 6, 0, "DiscAmt", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 7, 0, "DiscPer", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 8, 0, "FreeItemCode", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 9, 0, "FreeQty", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 10, 0, "FreeUOM", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 11, 0, "AllFreeItemcode", headerStyle);
			SXSSFReportHelper.addCell(sheetDetail, 12, 0, "errMsg", headerStyle);
			for (int i = 0; i < lstDetailError.size(); i++) {
				SXSSFReportHelper.addCell(sheetDetail, 0, i + 1, lstDetailError.get(i).getContent1(), leftStyle);
				SXSSFReportHelper.addCell(sheetDetail, 1, i + 1, lstDetailError.get(i).getContent2(), leftStyle);
				SXSSFReportHelper.addCell(sheetDetail, 2, i + 1, lstDetailError.get(i).getContent3(), leftStyle);
				SXSSFReportHelper.addCell(sheetDetail, 3, i + 1, lstDetailError.get(i).getContent4(), rightStyle);
				SXSSFReportHelper.addCell(sheetDetail, 4, i + 1, lstDetailError.get(i).getContent5(), leftStyle);
				SXSSFReportHelper.addCell(sheetDetail, 5, i + 1, lstDetailError.get(i).getContent6(), rightStyle);
				SXSSFReportHelper.addCell(sheetDetail, 6, i + 1, lstDetailError.get(i).getContent7(), rightStyle);
				SXSSFReportHelper.addCell(sheetDetail, 7, i + 1, lstDetailError.get(i).getContent8(), rightStyle);
				SXSSFReportHelper.addCell(sheetDetail, 8, i + 1, lstDetailError.get(i).getContent9(), leftStyle);
				SXSSFReportHelper.addCell(sheetDetail, 9, i + 1, lstDetailError.get(i).getContent10(), rightStyle);
				SXSSFReportHelper.addCell(sheetDetail, 10, i + 1, lstDetailError.get(i).getContent11(), leftStyle);
				SXSSFReportHelper.addCell(sheetDetail, 11, i + 1, lstDetailError.get(i).getContent12(), rightStyle);
				SXSSFReportHelper.addCell(sheetDetail, 12, i + 1, lstDetailError.get(i).getContent13(), errStyle);
			}

			String exportFileName = Configuration.getStoreImportDownloadPath() + outputName;
			out = new FileOutputStream(exportFileName);
			workbook.write(out);
			
			return Configuration.getStoreImportFailDownloadPath() + outputName;
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put("errMsg", errMsg);
		} finally {
			if (out != null) {
				IOUtils.closeQuietly(out);
			}
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return null;
	}

	public String validateLineQuantity(String promotionCode, ListGroupMua listGroupMua) {
		String errorMessage = "";
		for (int i = 0; i < listGroupMua.size(); i++) {
			GroupMua groupMua = listGroupMua.get(i);
			if (groupMua.lstLevel.size() == 1) {
				if (groupMua.lstLevel.get(0).lstSP.size() != 1) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.product.only.one", promotionCode);
					return errorMessage;
				}
				if (StringUtil.isNullOrEmpty(groupMua.lstLevel.get(0).lstSP.get(0).productCode) || groupMua.lstLevel.get(0).lstSP.get(0).quantity == null) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
					return errorMessage;
				}
			} else {
				GroupSP lastLevel = groupMua.lstLevel.get(0);
				if (lastLevel.lstSP.size() != 1) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.product.only.one", promotionCode);
					return errorMessage;
				}
				if (StringUtil.isNullOrEmpty(lastLevel.lstSP.get(0).productCode) || lastLevel.lstSP.get(0).quantity == null) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
					return errorMessage;
				}
				for (int j = 1; j < groupMua.lstLevel.size(); j++) {
					if (groupMua.lstLevel.get(j).lstSP.size() != 1) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.product.only.one", promotionCode);
						return errorMessage;
					}
					if (StringUtil.isNullOrEmpty(groupMua.lstLevel.get(j).lstSP.get(0).productCode) || groupMua.lstLevel.get(j).lstSP.get(0).quantity == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
						return errorMessage;
					}
					if (!lastLevel.lstSP.get(0).productCode.equals(groupMua.lstLevel.get(j).lstSP.get(0).productCode)) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.product.not.same", promotionCode);
						return errorMessage;
					}
					if (lastLevel.lstSP.get(0).quantity > groupMua.lstLevel.get(j).lstSP.get(0).quantity) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.quantity.continue", promotionCode);
						return errorMessage;
					}
					lastLevel = groupMua.lstLevel.get(j);
				}
			}
		}
		return errorMessage;
	}

	public String validateLineAmount(String promotionCode, ListGroupMua listGroupMua) {
		String errorMessage = "";
		for (int i = 0; i < listGroupMua.size(); i++) {
			GroupMua groupMua = listGroupMua.get(i);
			if (groupMua.lstLevel.size() == 1) {
				if (groupMua.lstLevel.get(0).lstSP.size() != 1) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.product.only.one", promotionCode);
					return errorMessage;
				}
				if (StringUtil.isNullOrEmpty(groupMua.lstLevel.get(0).lstSP.get(0).productCode) && groupMua.lstLevel.get(0).lstSP.get(0).amount == null) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
					return errorMessage;
				}
			} else {
				GroupSP lastLevel = groupMua.lstLevel.get(0);
				if (lastLevel.lstSP.size() != 1) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.product.only.one", promotionCode);
					return errorMessage;
				}
				if (StringUtil.isNullOrEmpty(lastLevel.lstSP.get(0).productCode) || lastLevel.lstSP.get(0).amount == null) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.amount", promotionCode);
					return errorMessage;
				}
				for (int j = 0; j < groupMua.lstLevel.size(); j++) {
					if (groupMua.lstLevel.get(j).lstSP.size() != 1) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.product.only.one", promotionCode);
						return errorMessage;
					}
					if (StringUtil.isNullOrEmpty(groupMua.lstLevel.get(j).lstSP.get(0).productCode) && groupMua.lstLevel.get(j).lstSP.get(0).amount == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
						return errorMessage;
					}
					if (!lastLevel.lstSP.get(0).productCode.equals(groupMua.lstLevel.get(j).lstSP.get(0).productCode)) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.product.not.same", promotionCode);
						return errorMessage;
					}
					if (lastLevel.lstSP.get(0).amount.compareTo(groupMua.lstLevel.get(j).lstSP.get(0).amount) > 0) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.amount.continue", promotionCode);
						return errorMessage;
					}
					lastLevel = groupMua.lstLevel.get(j);
				}
			}
		}
		return errorMessage;
	}

	public String validateGroupQuantity(String promotionCode, ListGroupMua listGroupMua) {
		String errorMessage = "";
		for (int i = 0; i < listGroupMua.size(); i++) {
			GroupMua groupMua = listGroupMua.get(i);
			if (groupMua.lstLevel.size() == 1) {
				for (int ii = 0; ii < groupMua.lstLevel.get(0).lstSP.size(); ii++) {
					if (StringUtil.isNullOrEmpty(groupMua.lstLevel.get(0).lstSP.get(ii).productCode) || groupMua.lstLevel.get(0).lstSP.get(ii).quantity == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
						return errorMessage;
					}
					if (ii != 0) {
						if (groupMua.lstLevel.get(0).lstSP.get(ii).quantity != null && groupMua.lstLevel.get(0).lstSP.get(ii - 1).quantity != null
								&& !groupMua.lstLevel.get(0).lstSP.get(ii).quantity.equals(groupMua.lstLevel.get(0).lstSP.get(ii - 1).quantity)) {
							errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.quantity.not.same", promotionCode);
							return errorMessage;
						}
					}
				}
			} else {
				for (int j = 1; j < groupMua.lstLevel.size(); j++) {
					GroupSP preLevel = groupMua.lstLevel.get(j - 1);
					GroupSP level = groupMua.lstLevel.get(j);
					Integer preQuantity = 0;
					Integer quantity = 0;
					if (preLevel.lstSP.size() != level.lstSP.size()) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.product.not.same", promotionCode);
						return errorMessage;
					} else {
						for (int ii = 0; ii < level.lstSP.size(); ii++) {
							if (StringUtil.isNullOrEmpty(level.lstSP.get(ii).productCode) || level.lstSP.get(ii).quantity == null) {
								errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
								return errorMessage;
							}
							if (!checkMatch2Level(preLevel, level.lstSP.get(ii).productCode)) {
								errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.product.not.same", promotionCode);
								return errorMessage;
							}
							if (ii != 0) {
								if (level.lstSP.get(ii).quantity != null && level.lstSP.get(ii - 1).quantity != null && !level.lstSP.get(ii).quantity.equals(level.lstSP.get(ii - 1).quantity)) {
									errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.quantity.not.same", promotionCode);
									return errorMessage;
								}
							}
						}
					}
					preQuantity = preLevel.lstSP.get(0).quantity;
					quantity = level.lstSP.get(0).quantity;
					if (preQuantity >= quantity) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.quantity.continue", promotionCode);
						return errorMessage;
					}
				}
			}
		}
		return errorMessage;
	}

	public String validateGroupAmount(String promotionCode, ListGroupMua listGroupMua) {
		String errorMessage = "";
		for (int i = 0; i < listGroupMua.size(); i++) {
			GroupMua groupMua = listGroupMua.get(i);
			if (groupMua.lstLevel.size() == 1) {
				for (int ii = 0; ii < groupMua.lstLevel.get(0).lstSP.size(); ii++) {
					if (StringUtil.isNullOrEmpty(groupMua.lstLevel.get(0).lstSP.get(ii).productCode) || groupMua.lstLevel.get(0).lstSP.get(ii).amount == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
						return errorMessage;
					}
					if (ii != 0) {
						if (groupMua.lstLevel.get(0).lstSP.get(ii).amount != null && groupMua.lstLevel.get(0).lstSP.get(ii - 1).amount != null
								&& !groupMua.lstLevel.get(0).lstSP.get(ii).amount.equals(groupMua.lstLevel.get(0).lstSP.get(ii - 1).amount)) {
							errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.sale.amount.not.same", promotionCode);
							return errorMessage;
						}
					}
				}
			} else {
				for (int j = 1; j < groupMua.lstLevel.size(); j++) {
					GroupSP preLevel = groupMua.lstLevel.get(j - 1);
					GroupSP level = groupMua.lstLevel.get(j);
					BigDecimal preAmount = BigDecimal.ZERO;
					BigDecimal amount = BigDecimal.ZERO;
					if (preLevel.lstSP.size() != level.lstSP.size()) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.product.not.same", promotionCode);
						return errorMessage;
					} else {
						for (int ii = 0; ii < level.lstSP.size(); ii++) {
							if (StringUtil.isNullOrEmpty(level.lstSP.get(ii).productCode) || level.lstSP.get(ii).amount == null) {
								errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
								return errorMessage;
							}
							if (!checkMatch2Level(preLevel, level.lstSP.get(ii).productCode)) {
								errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.product.not.same", promotionCode);
								return errorMessage;
							}
							if (ii != 0) {
								if (level.lstSP.get(ii).amount != null && level.lstSP.get(ii - 1).amount != null && !level.lstSP.get(ii).amount.equals(level.lstSP.get(ii - 1).amount)) {
									errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.amount.not.same", promotionCode);
									return errorMessage;
								}
							}
						}
					}
					preAmount = preLevel.lstSP.get(0).amount;
					amount = level.lstSP.get(0).amount;
					if (preAmount.compareTo(amount) > 0) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.amount.continue", promotionCode);
						return errorMessage;
					}
				}
			}
		}
		return errorMessage;
	}

	public String validateBundleQuantity(String promotionCode, ListGroupMua listGroupMua) {
		String errorMessage = "";
		for (int i = 0; i < listGroupMua.size(); i++) {
			GroupMua groupMua = listGroupMua.get(i);
			if (groupMua.lstLevel.size() == 1) {
				for (int j = 0; j < groupMua.lstLevel.get(0).lstSP.size(); j++) {
					if (StringUtil.isNullOrEmpty(groupMua.lstLevel.get(0).lstSP.get(j).productCode) || groupMua.lstLevel.get(0).lstSP.get(j).quantity == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv14.product.quantity", promotionCode);
						return errorMessage;
					}
				}
			} else {
				for (int j = 1; j < groupMua.lstLevel.size(); j++) {
					GroupSP preLevel = groupMua.lstLevel.get(j - 1);
					GroupSP level = groupMua.lstLevel.get(j);
					if (preLevel.lstSP.size() != level.lstSP.size()) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.product.not.same", promotionCode);
					} else {
						for (int ii = 0; ii < level.lstSP.size(); ii++) {
							if (StringUtil.isNullOrEmpty(level.lstSP.get(ii).productCode) || level.lstSP.get(ii).quantity == null) {
								errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
								return errorMessage;
							}
							if (!checkMatch2Level(preLevel, level.lstSP.get(ii).productCode)) {
								errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.product.not.same", promotionCode);
								return errorMessage;
							}
						}
					}
				}
			}
		}
		return errorMessage;
	}

	public String validateBundleAmount(String promotionCode, ListGroupMua listGroupMua) {
		String errorMessage = "";
		for (int i = 0; i < listGroupMua.size(); i++) {
			GroupMua groupMua = listGroupMua.get(i);
			if (groupMua.lstLevel.size() == 1) {
				for (int j = 0; j < groupMua.lstLevel.get(0).lstSP.size(); j++) {
					if (StringUtil.isNullOrEmpty(groupMua.lstLevel.get(0).lstSP.get(j).productCode) || groupMua.lstLevel.get(0).lstSP.get(j).amount == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv14.product.amount", promotionCode);
						return errorMessage;
					}
				}
			} else {
				for (int j = 1; j < groupMua.lstLevel.size(); j++) {
					GroupSP preLevel = groupMua.lstLevel.get(j - 1);
					GroupSP level = groupMua.lstLevel.get(j);
					if (preLevel.lstSP.size() != level.lstSP.size()) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.product.not.same", promotionCode);
					} else {
						for (int ii = 0; ii < level.lstSP.size(); ii++) {
							if (StringUtil.isNullOrEmpty(level.lstSP.get(ii).productCode) || level.lstSP.get(ii).amount == null) {
								errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.no.product.or.no.quantity", promotionCode);
								return errorMessage;
							}
							if (!checkMatch2Level(preLevel, level.lstSP.get(ii).productCode)) {
								errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.product.not.same", promotionCode);
								return errorMessage;
							}
						}
					}
				}
			}
		}
		return errorMessage;
	}

	public String validateAmount(String promotionCode, ListGroupMua listGroupMua) {
		String errorMessage = "";
		for (int i = 0; i < listGroupMua.size(); i++) {
			GroupMua groupMua = listGroupMua.get(i);
			if (groupMua.lstLevel.size() == 1) {
				if (groupMua.lstLevel.get(0).lstSP.get(0).amount == null) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv19.amount", promotionCode);
					return errorMessage;
				}
			} else {
				GroupSP lastLevel = groupMua.lstLevel.get(0);
				if (lastLevel.lstSP.get(0).amount == null) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv19.amount", promotionCode);
					return errorMessage;
				}
				for (int j = 1; j < groupMua.lstLevel.size(); j++) {
					if (groupMua.lstLevel.get(j).lstSP.get(0).amount == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv19.amount", promotionCode);
						return errorMessage;
					}
					if (groupMua.lstLevel.get(j).lstSP.get(0).amount.equals(lastLevel.lstSP.get(0).amount)) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv19.trung.du.lien", promotionCode);
						return errorMessage;
					}
					lastLevel = groupMua.lstLevel.get(j);
				}
			}
		}
		return errorMessage;
	}

	public String validateFreeItem(String promotionCode, ListGroupKM listGroupKM) {
		String errorMessage = "";
		for (int i = 0; i < listGroupKM.size(); i++) {
			GroupKM groupKM = listGroupKM.get(i);
			if (groupKM.lstLevel.size() == 1) {
				for (int ii = 0; ii < groupKM.lstLevel.get(0).lstSP.size(); ii++) {
					if (StringUtil.isNullOrEmpty(groupKM.lstLevel.get(0).lstSP.get(ii).productCode) || groupKM.lstLevel.get(0).lstSP.get(ii).quantity == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.free.no.product.or.no.quantity", promotionCode);
						return errorMessage;
					}
				}
			} else {
				for (int j = 0; j < groupKM.lstLevel.size(); j++) {
					GroupSP level = groupKM.lstLevel.get(j);
					for (int ii = 0; ii < level.lstSP.size(); ii++) {
						if (StringUtil.isNullOrEmpty(level.lstSP.get(ii).productCode) || level.lstSP.get(ii).quantity == null) {
							errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv09.free.no.product.or.no.quantity", promotionCode);
							return errorMessage;
						}
					}
				}
			}
		}
		return errorMessage;
	}

	public String validateFreePercent(String promotionCode, ListGroupKM listGroupKM) {
		String errorMessage = "";
		for (int i = 0; i < listGroupKM.size(); i++) {
			GroupKM groupKM = listGroupKM.get(i);
			if (groupKM.lstLevel.size() == 1) {
				if (groupKM.lstLevel.get(0).lstSP.get(0).percent == null) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.free.no.percent", promotionCode);
				}
			} else {
				for (int j = 0; j < groupKM.lstLevel.size(); j++) {
					if (groupKM.lstLevel.get(j).lstSP.get(0).percent == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv01.free.no.percent", promotionCode);
					}
				}
			}
		}
		return errorMessage;
	}

	public String validateFreeAmount(String promotionCode, ListGroupKM listGroupKM) {
		String errorMessage = "";
		for (int i = 0; i < listGroupKM.size(); i++) {
			GroupKM groupKM = listGroupKM.get(i);
			if (groupKM.lstLevel.size() == 1) {
				if (groupKM.lstLevel.get(0).lstSP.get(0).amount == null) {
					errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv14.free.amount", promotionCode);
					return errorMessage;
				}
			} else {
				for (int j = 0; j < groupKM.lstLevel.size(); j++) {
					if (groupKM.lstLevel.get(j).lstSP.get(0).amount == null) {
						errorMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.zv14.free.amount", promotionCode);
						return errorMessage;
					}
				}
			}
		}
		return errorMessage;
	}

	//sap xep theo so luong va ten SP mua, cac truong free, amount, percent dc sap xep theo
	public void sortQuantityProduct(String[] arrProduct, Integer[] arrSaleQuantity, Boolean[] arrAndOr, String[] arrFreeProduct, Integer[] arrFreeQuantity, BigDecimal[] arrFreeAmount, Float[] arrPercent) {
		boolean isAmount = false, isQuantity = false, isPercent = false;
		if (arrFreeProduct != null)
			isQuantity = true;//swap san pham + so luong KM
		if (arrFreeAmount != null)
			isAmount = true;//swap amount
		if (arrPercent != null)
			isPercent = true;//swap percent
		String name = "";
		Integer quantity = 0;
		Boolean andOr = true;
		Float percent = 0f;
		BigDecimal amount = BigDecimal.ZERO;
		if (arrProduct != null) {
			for (int i = 0; i < arrSaleQuantity.length - 1; i++) {
				if (arrSaleQuantity[i] != null) {
					for (int j = i + 1; j < arrSaleQuantity.length; j++) {
						//sap xep theo so luong roi toi productCode
						if ((arrSaleQuantity[j] != null && arrSaleQuantity[j].compareTo(arrSaleQuantity[i]) < 0)
								|| (arrSaleQuantity[j] != null && arrSaleQuantity[j].equals(arrSaleQuantity[i]) && arrProduct[j] != null && arrProduct[j].compareTo(arrProduct[i]) < 0)) {
							name = arrProduct[i];
							arrProduct[i] = arrProduct[j];
							arrProduct[j] = name;
							quantity = arrSaleQuantity[i];
							arrSaleQuantity[i] = arrSaleQuantity[j];
							arrSaleQuantity[j] = quantity;
							andOr = arrAndOr[i];
							arrAndOr[i] = arrAndOr[j];
							arrAndOr[j] = andOr;
							if (isQuantity) {
								name = arrFreeProduct[i];
								arrFreeProduct[i] = arrFreeProduct[j];
								arrFreeProduct[j] = name;
								quantity = arrFreeQuantity[i];
								arrFreeQuantity[i] = arrFreeQuantity[j];
								arrFreeQuantity[j] = quantity;
							}
							if (isAmount) {
								amount = arrFreeAmount[i];
								arrFreeAmount[i] = arrFreeAmount[j];
								arrFreeAmount[j] = amount;
							}
							if (isPercent) {
								percent = arrPercent[i];
								arrPercent[i] = arrPercent[j];
								arrPercent[j] = percent;
							}
						}
					}
				}
			}
		}
	}

	//sap xep theo so luong va ten SP mua, cac truong free, amount, percent dc sap xep theo
	public void sortAmountProduct(String[] arrProduct, BigDecimal[] arrSaleAmount, Boolean[] arrAndOr, String[] arrFreeProduct, Integer[] arrFreeQuantity, BigDecimal[] arrFreeAmount, Float[] arrPercent) {
		boolean isAmount = false, isQuantity = false, isPercent = false;
		if (arrFreeProduct != null)
			isQuantity = true;//swap san pham + so luong KM
		if (arrFreeAmount != null)
			isAmount = true;//swap amount
		if (arrPercent != null)
			isPercent = true;//swap percent
		String name = "";
		Boolean andOr = true;
		Float percent = 0f;
		BigDecimal amount = BigDecimal.ZERO;
		if (arrProduct != null) {
			for (int i = 0; i < arrSaleAmount.length - 1; i++) {
				if (arrSaleAmount[i] != null) {
					for (int j = i + 1; j < arrSaleAmount.length; j++) {
						//sap xep theo so luong roi toi productCode
						if ((arrSaleAmount[j] != null && arrSaleAmount[j].compareTo(arrSaleAmount[i]) < 0)
								|| (arrSaleAmount[j] != null && arrSaleAmount[j].equals(arrSaleAmount[i]) && arrProduct[j] != null && arrProduct[j].compareTo(arrProduct[i]) < 0)) {
							name = arrProduct[i];
							arrProduct[i] = arrProduct[j];
							arrProduct[j] = name;
							amount = arrSaleAmount[i];
							arrSaleAmount[i] = arrSaleAmount[j];
							arrSaleAmount[j] = amount;
							andOr = arrAndOr[i];
							arrAndOr[i] = arrAndOr[j];
							arrAndOr[j] = andOr;
							if (isQuantity) {
								name = arrFreeProduct[i];
								arrFreeProduct[i] = arrFreeProduct[j];
								arrFreeProduct[j] = name;
								quantity = arrFreeQuantity[i];
								arrFreeQuantity[i] = arrFreeQuantity[j];
								arrFreeQuantity[j] = quantity;
							}
							if (isAmount) {
								amount = arrFreeAmount[i];
								arrFreeAmount[i] = arrFreeAmount[j];
								arrFreeAmount[j] = amount;
							}
							if (isPercent) {
								percent = arrPercent[i];
								arrPercent[i] = arrPercent[j];
								arrPercent[j] = percent;
							}
						}
					}
				}
			}
		}
	}

	//sap xep theo so luong cac truong free, amount, percent dc sap xep theo
	public void sortAmount(BigDecimal[] arrSaleAmount, Boolean[] arrAndOr, String[] arrFreeProduct, Integer[] arrFreeQuantity, BigDecimal[] arrFreeAmount, Float[] arrPercent) {
		boolean isAndOr = false, isAmount = false, isQuantity = false, isPercent = false;
		if (arrAndOr != null)
			isAndOr = true;//swap san pham + so luong KM
		if (arrFreeProduct != null)
			isQuantity = true;//swap san pham + so luong KM
		if (arrFreeAmount != null)
			isAmount = true;//swap amount
		if (arrPercent != null)
			isPercent = true;//swap percent
		String name = "";
		Boolean andOr = true;
		Float percent = 0f;
		BigDecimal amount = BigDecimal.ZERO;
		if (arrSaleAmount != null) {
			for (int i = 0; i < arrSaleAmount.length - 1; i++) {
				if (arrSaleAmount[i] != null) {
					for (int j = i + 1; j < arrSaleAmount.length; j++) {
						//sap xep theo so luong roi toi productCode
						if (arrSaleAmount[j] != null && arrSaleAmount[j].compareTo(arrSaleAmount[i]) < 0) {
							amount = arrSaleAmount[i];
							arrSaleAmount[i] = arrSaleAmount[j];
							arrSaleAmount[j] = amount;
							if (isAndOr) {
								andOr = arrAndOr[i];
								arrAndOr[i] = arrAndOr[j];
								arrAndOr[j] = andOr;
							}
							if (isQuantity) {
								name = arrFreeProduct[i];
								arrFreeProduct[i] = arrFreeProduct[j];
								arrFreeProduct[j] = name;
								quantity = arrFreeQuantity[i];
								arrFreeQuantity[i] = arrFreeQuantity[j];
								arrFreeQuantity[j] = quantity;
							}
							if (isAmount) {
								amount = arrFreeAmount[i];
								arrFreeAmount[i] = arrFreeAmount[j];
								arrFreeAmount[j] = amount;
							}
							if (isPercent) {
								percent = arrPercent[i];
								arrPercent[i] = arrPercent[j];
								arrPercent[j] = percent;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Kiem tra cac dong duplicate TungMT
	 * 
	 * @param type
	 * @return
	 */
	public String checkDuplicate(String type, List<Row> rowIter, Row myRow) {
		String messageError = "";
		String productCode = "", freeProductCode = "";
		Integer quantity = null;
		BigDecimal amount = null;
		try {
			if (myRow.getCell(2) != null && StringUtil.isNullOrEmpty(messageError)) {
				productCode = myRow.getCell(2).getStringCellValue();
			}
			if (myRow.getCell(3) != null) {
				if (myRow.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC) {
					quantity = (int) myRow.getCell(3).getNumericCellValue();
				}
			}
			if (myRow.getCell(5) != null) {
				if (myRow.getCell(5).getCellType() == Cell.CELL_TYPE_NUMERIC) {
					amount = BigDecimal.valueOf(myRow.getCell(5).getNumericCellValue());
				}
			}
			if (myRow.getCell(8) != null) {
				freeProductCode = myRow.getCell(8).getStringCellValue();
			}
		} catch (Exception e) {
			messageError += R.getResource("system.error");
		}
		for (int i = 0; i < rowIter.size(); i++) {//for tu 0 den dong truoc dong dang xet 
			Row row = rowIter.get(i);
			if (PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV02.getValue().equals(type) || PromotionType.ZV07.getValue().equals(type) || PromotionType.ZV08.getValue().equals(type)) {
				//trung spmua va sl mua
				if (productCode.equals(row.getCell(2).getStringCellValue()) && row.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC && quantity.equals((int) row.getCell(3).getNumericCellValue())) {
					messageError += R.getResource("catalog.promotion.import.duplicate.sp.sl");
					break;
				}
			} else if (PromotionType.ZV04.getValue().equals(type) || PromotionType.ZV05.getValue().equals(type) || PromotionType.ZV10.getValue().equals(type) || PromotionType.ZV11.getValue().equals(type) || PromotionType.ZV16.getValue().equals(type)
					|| PromotionType.ZV17.getValue().equals(type)) {
				//trung spmua va amount
				if (productCode.equals(row.getCell(2).getStringCellValue()) && row.getCell(5).getCellType() == Cell.CELL_TYPE_NUMERIC && amount.equals(BigDecimal.valueOf(row.getCell(5).getNumericCellValue()))) {
					messageError += R.getResource("catalog.promotion.import.duplicate.sp.st");
					break;
				}
			} else if (PromotionType.ZV03.getValue().equals(type) || PromotionType.ZV09.getValue().equals(type)) {
				//trung spmua va sl mua va spkm
				if (productCode.equals(row.getCell(2).getStringCellValue()) && freeProductCode.equals(row.getCell(8).getStringCellValue()) && row.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC
						&& quantity.equals((int) row.getCell(3).getNumericCellValue())) {
					messageError += R.getResource("catalog.promotion.import.duplicate.sp.sl.spkm");
					break;
				}
			} else if (PromotionType.ZV06.getValue().equals(type) || PromotionType.ZV12.getValue().equals(type) || PromotionType.ZV18.getValue().equals(type)) {
				//trung spmua va amount va spkm
				if (productCode.equals(row.getCell(2).getStringCellValue()) && freeProductCode.equals(row.getCell(8).getStringCellValue()) && row.getCell(5).getCellType() == Cell.CELL_TYPE_NUMERIC
						&& amount.equals(BigDecimal.valueOf(row.getCell(5).getNumericCellValue()))) {
					messageError += R.getResource("catalog.promotion.import.duplicate.sp.st.spkm");
					break;
				}
			} else if (PromotionType.ZV19.getValue().equals(type) || PromotionType.ZV20.getValue().equals(type)) {
				//trung amount
				if (row.getCell(5).getCellType() == Cell.CELL_TYPE_NUMERIC && amount.equals(BigDecimal.valueOf(row.getCell(5).getNumericCellValue()))) {
					messageError += R.getResource("catalog.promotion.import.duplicate.st");
					break;
				}
			} else if (PromotionType.ZV21.getValue().equals(type)) {
				//trung amount va SPKM
				if (freeProductCode.equals(row.getCell(8).getStringCellValue()) && row.getCell(5).getCellType() == Cell.CELL_TYPE_NUMERIC && amount.equals(BigDecimal.valueOf(row.getCell(5).getNumericCellValue()))) {
					messageError += R.getResource("catalog.promotion.import.duplicate.st.spkm");
					break;
				}
			}
		}
		return messageError;
	}

	/**
	 * Kiem tra cac cot co can thiet khong TungMT
	 * 
	 * @param type
	 * @param col
	 *            : 2.productCode, 3.SaleQty, 5.SaleAmt, 6.DisAmount,
	 *            7.DisPercent, 8.FreeItemCode, 9.FreeQty, 11.AndOr
	 * @return true: can cho CTKM, false: khong can cho CTKM
	 */
	public Boolean checkColumnNecessary(String type, int col) {
		if (col == 2) {//productCode
			//Neu CTKM dang doc thi khong can productCode
			if (PromotionType.ZV19.getValue().equals(type) || PromotionType.ZV20.getValue().equals(type) || PromotionType.ZV21.getValue().equals(type)) {
				return false;
			} else {
				return true;
			}
		} else if (col == 3) {//sale quantity
			if (PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV02.getValue().equals(type) || PromotionType.ZV03.getValue().equals(type) || PromotionType.ZV07.getValue().equals(type) || PromotionType.ZV08.getValue().equals(type)
					|| PromotionType.ZV09.getValue().equals(type) || PromotionType.ZV13.getValue().equals(type) || PromotionType.ZV14.getValue().equals(type) || PromotionType.ZV15.getValue().equals(type)) {
				return true;
			} else {
				return false;
			}
		} else if (col == 5) {//sale amt nguoc voi quantity
			if (PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV02.getValue().equals(type) || PromotionType.ZV03.getValue().equals(type) || PromotionType.ZV07.getValue().equals(type) || PromotionType.ZV08.getValue().equals(type)
					|| PromotionType.ZV09.getValue().equals(type) || PromotionType.ZV13.getValue().equals(type) || PromotionType.ZV14.getValue().equals(type) || PromotionType.ZV15.getValue().equals(type)) {
				return false;
			} else {
				return true;
			}
		} else if (col == 6) {//discAmount
			if (PromotionType.ZV02.getValue().equals(type) || PromotionType.ZV05.getValue().equals(type) || PromotionType.ZV08.getValue().equals(type) || PromotionType.ZV11.getValue().equals(type) || PromotionType.ZV14.getValue().equals(type)
					|| PromotionType.ZV17.getValue().equals(type) || PromotionType.ZV20.getValue().equals(type)) {
				return true;
			} else {
				return false;
			}
		} else if (col == 7) {//discPercent
			if (PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV04.getValue().equals(type) || PromotionType.ZV07.getValue().equals(type) || PromotionType.ZV10.getValue().equals(type) || PromotionType.ZV13.getValue().equals(type)
					|| PromotionType.ZV16.getValue().equals(type) || PromotionType.ZV19.getValue().equals(type)) {
				return true;
			} else {
				return false;
			}
		} else if (col == 8 || col == 9) {//freeItemCode and FreeQty
			if (PromotionType.ZV03.getValue().equals(type) || PromotionType.ZV06.getValue().equals(type) || PromotionType.ZV09.getValue().equals(type) || PromotionType.ZV12.getValue().equals(type) || PromotionType.ZV15.getValue().equals(type)
					|| PromotionType.ZV18.getValue().equals(type) || PromotionType.ZV21.getValue().equals(type)) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	List<ExcelPromotionHeader> listHeader;
	List<ExcelPromotionDetail> listDetail;
	Map<String, ExcelPromotionHeader> mapHeader;
	Map<String, String> mapErrorPromotion;
	Map<String, String> mapType;
	Map<String, ListGroupMua> mapPromotionMua;
	Map<String, ListGroupKM> mapPromotionKM;
	Map<String, String> mapPromotionTypeCheck;
	MapMuaKM mapMuaKM;
	private List<PromotionShopVO> lstPromotionShop;
	private List<CellBean> lstPromotionShopError;
	private final String ERR_NUM_SHEET = "ERR_NUM_SHEET";

	/**
	 * Doc du lieu tap tin Excel Import CTKM
	 * 
	 * @author hunglm16
	 * @throws Exception
	 * @since 13/09/2015
	 * @description Cap nhat Code (Mager)
	 */
	private void getExcelData21ZV() throws Exception {
		listHeader = new ArrayList<ExcelPromotionHeader>();
		listDetail = new ArrayList<ExcelPromotionDetail>();
		mapHeader = new HashMap<String, ExcelPromotionHeader>();
		mapErrorPromotion = new HashMap<String, String>();
		mapType = new HashMap<String, String>();
		mapPromotionMua = new HashMap<String, ListGroupMua>();
		mapPromotionKM = new HashMap<String, ListGroupKM>();
		mapMuaKM = new MapMuaKM();
		mapPromotionTypeCheck = new HashMap<String, String>();
		lstHeaderError = new ArrayList<CellBean>();
		lstDetailError = new ArrayList<CellBean>();

		lstHeaderError = new ArrayList<CellBean>();
		lstDetailError = new ArrayList<CellBean>();
		Map<String, Integer> mapCheckHeaderDuplicate = new HashMap<String, Integer>();
		Map<String, String> mapCheckType = apParamMgr.getMapPromotionType();
		Workbook myWorkBook = null;
		InputStream is = new FileInputStream(excelFile);
		int MAX_ARRAY = 100000;
		final int NUM_SHEETS = 2;

		if (!is.markSupported()) {
			is = new PushbackInputStream(is, 8);
		}
		if (POIFSFileSystem.hasPOIFSHeader(is)) {
			myWorkBook = new HSSFWorkbook(is);
		} else if (POIXMLDocument.hasOOXMLHeader(is)) {
			myWorkBook = new XSSFWorkbook(OPCPackage.open(is));
		}
		if (myWorkBook != null) {
			if (myWorkBook.getNumberOfSheets() < NUM_SHEETS) {
				throw new Exception(ERR_NUM_SHEET);
			}
			Sheet headerSheet = myWorkBook.getSheetAt(0);
			Sheet detailSheet = myWorkBook.getSheetAt(1);
			int iRun = 0;
			totalItem = 0;
			int maxSizeSheet1 = 7;
			if (headerSheet != null) {
				Iterator<?> rowIter = headerSheet.rowIterator();
				while (rowIter.hasNext()) {
					Row myRow = (Row) rowIter.next();
					if (iRun == 0) {
						iRun++;
						continue;
					}
					boolean isContinue = true;
					//Kiem tra su hop le cua Row Import
					for (int i = 0; i < maxSizeSheet1; i++) {
						if (myRow.getCell(i) != null && !StringUtil.isNullOrEmpty(myRow.getCell(i).getStringCellValue())) {
							isContinue = false;
							break;
						}
					}
					if (isContinue) {
						continue;
					}
					ExcelPromotionHeader header = new ExcelPromotionHeader();
					CellBean errRow = new CellBean();
					String messageError = "";
					totalItem++;
					//0	get promotionCode
					String promotionCode = "";
					try {
						Cell cellPromotionCode = myRow.getCell(0);
						if (cellPromotionCode != null && StringUtil.isNullOrEmpty(messageError)) {
							try {
								promotionCode = cellPromotionCode.getStringCellValue();								
							} catch (Exception e) {
								promotionCode = String.valueOf(cellPromotionCode.getNumericCellValue());
							}
							promotionCode = promotionCode != null ? promotionCode.toUpperCase().trim() : "";
							header.promotionCode = promotionCode;
							errRow.setContent1(promotionCode);
							if (StringUtil.isNullOrEmpty(promotionCode)) {
								messageError += R.getResource("catalog.promotion.import.column.null", "ProgCode");
							} else {
								messageError += ValidateUtil.validateField(promotionCode, "catalog.promotion.import.column.progcode", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							}
							if (mapCheckHeaderDuplicate.get(promotionCode) != null) {
								messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.import.duplicate", mapCheckHeaderDuplicate.get(promotionCode));
								messageError += "\n";
							} else {
								mapCheckHeaderDuplicate.put(promotionCode, myRow.getRowNum());
							}
							PromotionProgram existPromotion = promotionProgramMgr.getPromotionProgramByCode(promotionCode);
							if (existPromotion != null && !ActiveType.WAITING.equals(existPromotion.getStatus())) {
								messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.program.exists");
								messageError += "\n";
							}
						}
					} catch (Exception e) {
						messageError += R.getResource("catalog.promotion.import.get.promotion.error", promotionCode);
//						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "ProgCode");
					}
					//1	get description
					try {
						Cell cellDescription = myRow.getCell(1);
						if (cellDescription != null && StringUtil.isNullOrEmpty(messageError)) {
							String description = cellDescription.getStringCellValue();
							header.description = description;
							errRow.setContent2(description);
							messageError += ValidateUtil.validateField(description, "catalog.promotion.import.column.progpescr", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "ProgDescr");
					}
					//2	get release
					try {
						if (myRow.getCell(2) != null && StringUtil.isNullOrEmpty(messageError)) {
							errRow.setContent3(String.valueOf(myRow.getCell(2).getNumericCellValue()));
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.number", iRun, "Release");
					}
					//3	get promotion type
					String type = null;
					try {
						Cell cellPromotionType = myRow.getCell(3);
						if (cellPromotionType != null && StringUtil.isNullOrEmpty(messageError)) {
							type = cellPromotionType.getStringCellValue();
							type = type != null ? type.toUpperCase().trim() : "";
							header.type = type;
							errRow.setContent4(type);
							if (StringUtil.isNullOrEmpty(type)) {
								messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.type"));
								messageError += "\n";
							}
							if (mapCheckType.get(type) == null) {
								messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.exists.before", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.type"));
								messageError += "\n";
							}
							mapType.put(header.promotionCode, type);
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "ConditionTypeCode");
					}
					//4 get format
					try {
						Cell cellFormat = myRow.getCell(4);
						if (cellFormat != null && StringUtil.isNullOrEmpty(messageError)) {
							String format = cellFormat.getStringCellValue();
							header.format = format;
							errRow.setContent5(format);
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "User1");
					}
					//5 get fromDate
					try {
						Cell cellFromDate = myRow.getCell(5);
						if (cellFromDate != null && StringUtil.isNullOrEmpty(messageError)) {
							if (cellFromDate.getCellType() == Cell.CELL_TYPE_NUMERIC && cellFromDate.getCellStyle() != null && DateUtil.HSSF_DATE_FORMAT_M_D_YY.equals(cellFromDate.getCellStyle().getDataFormatString())) {
								if (cellFromDate.getDateCellValue() != null || !StringUtil.isNullOrEmpty(cellFromDate.getStringCellValue())) {
									String __fromDate = DateUtil.toDateString(cellFromDate.getDateCellValue(), DateUtil.DATE_FORMAT_DDMMYYYY);
									Date fromDate = DateUtil.toDate(__fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
									header.fromDate = fromDate;
									errRow.setContent6(__fromDate);
								} else {
									messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "imp.epx.tuyen.clmn.tuNgay"));
									messageError += "\n";
								}
							} else if (cellFromDate.getCellType() == Cell.CELL_TYPE_NUMERIC && cellFromDate.getCellStyle() != null && DateUtil.DATE_FORMAT_VISIT.equals(cellFromDate.getCellStyle().getDataFormatString())) {
								if (cellFromDate.getDateCellValue() != null || !StringUtil.isNullOrEmpty(cellFromDate.getStringCellValue())) {
									String __fromDate = DateUtil.toDateString(cellFromDate.getDateCellValue(), DateUtil.DATE_FORMAT_VISIT);
									Date fromDate = DateUtil.toDate(__fromDate, DateUtil.DATE_FORMAT_VISIT);
									header.fromDate = fromDate;
									errRow.setContent6(__fromDate);
								} else {
									messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "imp.epx.tuyen.clmn.tuNgay"));
									messageError += "\n";
								}
							} else {
								if (!StringUtil.isNullOrEmpty(cellFromDate.getStringCellValue())) {
									try {
										String __fromDate = cellFromDate.getStringCellValue();
										if (DateUtil.checkInvalidFormatDate(__fromDate)) {
											messageError += R.getResource("common.invalid.format.date", R.getResource("imp.epx.tuyen.clmn.tuNgay"));
											messageError += "\n";
											errRow.setContent6(__fromDate);
										} else {
											Date fromDate = DateUtil.toDate(__fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
											header.fromDate = fromDate;
											errRow.setContent6(__fromDate);
										}
									} catch (Exception e1) {
										messageError += R.getResource("common.invalid.format.date", R.getResource("imp.epx.tuyen.clmn.tuNgay"));
										messageError += "\n";
										errRow.setContent6(cellFromDate.getStringCellValue());
									}
								} else {
									messageError += R.getResource("common.required", R.getResource("imp.epx.tuyen.clmn.tuNgay"));
									messageError += "\n";
								}
							}
						}
					} catch (Exception e) {
						messageError += R.getResource("catalog.promotion.import.cant.read.cell.date", iRun, "FromDate");
					}
					//6 get toDate
					try {
						Cell cellToDate = myRow.getCell(6);
						if (cellToDate != null && StringUtil.isNullOrEmpty(messageError)) {
							if (cellToDate.getCellType() == Cell.CELL_TYPE_NUMERIC && cellToDate.getCellStyle() != null && DateUtil.HSSF_DATE_FORMAT_M_D_YY.equals(cellToDate.getCellStyle().getDataFormatString())) {
								if (cellToDate.getDateCellValue() != null || !StringUtil.isNullOrEmpty(cellToDate.getStringCellValue())) {
									String __toDate = DateUtil.toDateString(cellToDate.getDateCellValue(), DateUtil.DATE_FORMAT_DDMMYYYY);
									Date toDate = DateUtil.toDate(__toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
									header.toDate = toDate;
									errRow.setContent7(__toDate);
								}
							} else if (cellToDate.getCellType() == Cell.CELL_TYPE_NUMERIC && cellToDate.getCellStyle() != null && DateUtil.DATE_FORMAT_VISIT.equals(cellToDate.getCellStyle().getDataFormatString())) {
								if (cellToDate.getDateCellValue() != null || !StringUtil.isNullOrEmpty(cellToDate.getStringCellValue())) {
									String __toDate = DateUtil.toDateString(cellToDate.getDateCellValue(), DateUtil.DATE_FORMAT_VISIT);
									Date toDate = DateUtil.toDate(__toDate, DateUtil.DATE_FORMAT_VISIT);
									header.toDate = toDate;
									errRow.setContent7(__toDate);
								}
							} else if (!StringUtil.isNullOrEmpty(cellToDate.getStringCellValue())) {
								try {
									String __toDate = cellToDate.getStringCellValue();
									if (DateUtil.checkInvalidFormatDate(__toDate)) {
										messageError += R.getResource("common.invalid.format.date", R.getResource("imp.epx.tuyen.clmn.denNgay"));
										messageError += "\n";
										errRow.setContent7(__toDate);
									} else {
										Date toDate = DateUtil.toDate(__toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
										header.toDate = toDate;
										errRow.setContent7(__toDate);
									}
								} catch (Exception e1) {
									messageError += R.getResource("common.invalid.format.date", R.getResource("imp.epx.tuyen.clmn.denNgay"));
									messageError += "\n";
									errRow.setContent7(cellToDate.getStringCellValue());
								}
							}
						}
						if (header.fromDate != null && header.toDate != null) {
							if (DateUtil.compareDateWithoutTime(header.fromDate, header.toDate) > 0) {
								messageError += R.getResource("common.fromdate.greater.todate") + "\n";
							}
						}
					} catch (Exception e) {
						messageError += R.getResource("catalog.promotion.import.cant.read.cell.date", iRun, "ToDate");
					}

					if (StringUtil.isNullOrEmpty(messageError)) {
						listHeader.add(header);
						mapHeader.put(header.promotionCode, header);
					} else {
						errRow.setContent8(messageError);
						lstHeaderError.add(errRow);
						if (mapErrorPromotion.get(header.promotionCode) == null) {
							mapErrorPromotion.put(header.promotionCode, messageError);
						}
					}
					iRun++;
				}
			}

			//Sheet Co cau KM
			if (detailSheet != null) {
				Iterator<?> rowIter = detailSheet.rowIterator();
				String previousPromotionCode = null;
				int typeKM = -1;
				Map<String, Integer> mapPromotionType = new HashMap<String, Integer>();
				iRun = 0;
				Map<String, String[]> mapArrayProduct = new HashMap<String, String[]>();
				Map<String, Integer[]> mapArraySaleQuantity = new HashMap<String, Integer[]>();
				Map<String, BigDecimal[]> mapArraySaleAmount = new HashMap<String, BigDecimal[]>();
				Map<String, BigDecimal[]> mapArrayDiscountAmount = new HashMap<String, BigDecimal[]>();
				Map<String, Float[]> mapArrayDiscountPercent = new HashMap<String, Float[]>();
				Map<String, String[]> mapArrayFreeProduct = new HashMap<String, String[]>();
				Map<String, Integer[]> mapArrayFreeQuantity = new HashMap<String, Integer[]>();
				Map<String, Boolean[]> mapArrayAndOr = new HashMap<String, Boolean[]>();
				LinkedHashMap<String, Integer> lstProductPromo = new LinkedHashMap<String, Integer>();
				Map<String, List<Row>> lstRow = new HashMap<String, List<Row>>();
				int indexProductPromo = 0;
				int maxSizeSheet2 = 12;
				PromotionProgram existPromotion = null;
				while (rowIter.hasNext()) {
					Row myRow = (Row) rowIter.next();
					if (iRun == 0) {
						iRun++;
						continue;
					}
					boolean isContinue = true;
					//Kiem tra su hop le cua Row Import
					for (int i = 0; i < maxSizeSheet2; i++) {
						if (myRow.getCell(i) != null) {
							isContinue = false;
							break;
						}
					}
					if (isContinue) {
						continue;
					}
					ExcelPromotionDetail detail = new ExcelPromotionDetail();
					CellBean errRow = new CellBean();
					String messageError = "";
					//0 get promotionCode
					String promotionCode = null;
					try {
						Cell cellPromotionCode = myRow.getCell(0);
						if (cellPromotionCode != null && StringUtil.isNullOrEmpty(messageError)) {
							try {
								promotionCode = cellPromotionCode.getStringCellValue();
							} catch (Exception e) {
								promotionCode = String.valueOf(cellPromotionCode.getNumericCellValue());
							}
							promotionCode = promotionCode != null ? promotionCode.trim().toUpperCase().trim() : "";
							if (StringUtil.isNullOrEmpty(promotionCode)) {
								continue;
							} else {
								messageError += ValidateUtil.validateField(promotionCode, "catalog.promotion.import.column.progcode", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							}
							existPromotion = promotionProgramMgr.getPromotionProgramByCode(promotionCode);
							if (existPromotion == null && mapHeader.get(promotionCode) == null) {
								messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.program.not.exists");
								messageError += "\n";
							} else if (mapHeader.get(promotionCode) == null) {
								messageError += R.getResource("catalog.promotion.import.not.init") + "\n";
							}
							detail.promotionCode = promotionCode;
							errRow.setContent1(promotionCode);
							if (StringUtil.isNullOrEmpty(promotionCode)) {
								messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.code"));
								messageError += "\n";
							}
						} else {
							continue;
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "ProgCode") + "\n";
					}
					//1 get type
					try {
						if (myRow.getCell(1) != null && StringUtil.isNullOrEmpty(messageError)) {
							try {
								detail.type = myRow.getCell(1).getStringCellValue();								
							} catch (Exception e) {
								detail.type = String.valueOf(myRow.getCell(1).getNumericCellValue());
							}
							detail.type = detail.type != null ? detail.type.trim().toUpperCase().trim() : "";
							if (StringUtil.isNullOrEmpty(detail.type)) {
								continue;
							} else {
								if (mapCheckType.get(detail.type) == null) {
									messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.exists.before", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.type"));
									messageError += "\n";
								} else if (mapType.get(promotionCode) == null) {
									if (null != existPromotion && existPromotion.getType().equalsIgnoreCase(detail.type)) {
										mapType.put(detail.promotionCode, detail.type);
									} else {
										messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.is.not.same2") + "\n";
									}
								} else if (!mapType.get(promotionCode).equals(detail.type)) {
									messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.type.is.not.same2") + "\n";
								}
							}
							errRow.setContent2(myRow.getCell(1).getStringCellValue());
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "ConditionTypeCode3") + "\n";
					}
					//2 get productCode
					String productCode = "";
					try {
						if (checkColumnNecessary(detail.type, 2) && StringUtil.isNullOrEmpty(messageError)) {
							Cell cellProductCode = myRow.getCell(2);
							if (cellProductCode != null) {
								try {
									productCode = cellProductCode.getStringCellValue();
								} catch (Exception ex) {
									productCode = String.valueOf(cellProductCode.getNumericCellValue());
								}
								if (!StringUtil.isNullOrEmpty(productCode)) {
									Product product = productMgr.getProductByCode(productCode.trim());
									if (product == null) {
										messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist.in.db", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.buyproduct.code"));
										messageError += "\n";
									}
									detail.productCode = productCode.toUpperCase().trim();
								}
							} else {
								messageError += R.getResource("catalog.promotion.import.column.null", "SaleProItem");
							}
						}
						if (myRow.getCell(2) != null) {
							errRow.setContent3(String.valueOf(myRow.getCell(2)));
						}
					} catch (Exception e) {
						messageError +=  R.getResource("catalog.promotion.import.get.product.error", productCode);
//						messageError +=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "SaleProItem");
					}
					//3 getQuantity
					try {
						if (checkColumnNecessary(detail.type, 3) && StringUtil.isNullOrEmpty(messageError)) {
							Cell cellQuantity = myRow.getCell(3);
							if (cellQuantity != null && cellQuantity.getCellType() != Cell.CELL_TYPE_BLANK) {
								if (cellQuantity.getCellType() == Cell.CELL_TYPE_NUMERIC) {
									Integer quantity = (int) cellQuantity.getNumericCellValue();
									if (quantity == null || quantity <= 0) {
										messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.date", "SaleQty");
									}
									detail.saleQuantity = quantity;
								} else {
									messageError += R.getResource("catalog.promotion.import.column.invalid.format.number", "SaleQty");
								}
							} else {
								messageError += R.getResource("catalog.promotion.import.column.null", "SaleQty");
							}
						}
						if (myRow.getCell(3) != null) {
							errRow.setContent4(String.valueOf(myRow.getCell(3)));
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.number", iRun, "SaleQty");
					}
					//4 get UOM
					try {
						if (checkColumnNecessary(detail.type, 4)) {
							if (myRow.getCell(4) != null && StringUtil.isNullOrEmpty(messageError)) {
								errRow.setContent5(myRow.getCell(4).getStringCellValue());
							}
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "SaleUOM");
					}
					//5 getAmount
					try {
						if (checkColumnNecessary(detail.type, 5) && StringUtil.isNullOrEmpty(messageError)) {
							Cell cellAmount = myRow.getCell(5);
							if (cellAmount != null && cellAmount.getCellType() != Cell.CELL_TYPE_BLANK) {
								if (cellAmount.getCellType() == Cell.CELL_TYPE_NUMERIC) {
									BigDecimal amount = BigDecimal.valueOf(cellAmount.getNumericCellValue());
									if (amount == null || amount.compareTo(BigDecimal.ZERO) < 0) {
										messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.date", "SaleAmt");
									}
									detail.saleAmount = amount;
								} else {
									messageError += R.getResource("catalog.promotion.import.column.invalid.format.number", "SaleAmt");
								}
							} else {
								messageError += R.getResource("catalog.promotion.import.column.null", "SaleAmt");
							}
						}
						if (myRow.getCell(5) != null) {
							errRow.setContent6(String.valueOf(myRow.getCell(5)));
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.number", iRun, "SaleAmt");
					}
					//6 getDiscount Amount
					try {
						if (checkColumnNecessary(detail.type, 6) && StringUtil.isNullOrEmpty(messageError)) {
							Cell cellDiscountAmount = myRow.getCell(6);
							if (cellDiscountAmount != null && cellDiscountAmount.getCellType() != Cell.CELL_TYPE_BLANK) {
								if (cellDiscountAmount.getCellType() == Cell.CELL_TYPE_NUMERIC) {
									BigDecimal discountAmount = BigDecimal.valueOf(cellDiscountAmount.getNumericCellValue());
									if (discountAmount == null || discountAmount.compareTo(BigDecimal.ZERO) < 0) {
										messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.date", "DiscAmt");
									}
									detail.discountAmount = discountAmount;
								} else {
									messageError += R.getResource("catalog.promotion.import.column.invalid.format.number", "DiscAmt");
								}
							} else {
								messageError += R.getResource("catalog.promotion.import.column.null", "DiscAmt");
							}
						}
						if (myRow.getCell(6) != null) {
							errRow.setContent7(String.valueOf(myRow.getCell(6)));
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.number", iRun, "DiscAmt");
					}
					//7 get discount percent
					try {
						if (checkColumnNecessary(detail.type, 7) && StringUtil.isNullOrEmpty(messageError)) {
							Cell cellDiscountPercent = myRow.getCell(7);
							if (cellDiscountPercent != null && cellDiscountPercent.getCellType() != Cell.CELL_TYPE_BLANK) {
								if (cellDiscountPercent.getCellType() == Cell.CELL_TYPE_NUMERIC) {
									Float discountPercent = (float) cellDiscountPercent.getNumericCellValue();
									if (discountPercent == null || discountPercent < 0 || discountPercent > 100) {
										messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.date", "DiscPer");
									}
									detail.discountPercent = discountPercent;
								} else {
									messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.column.invalid.format.float", "DiscPer");
								}
							} else {
								messageError += R.getResource("catalog.promotion.import.column.null", "DiscPer");
							}
						}
						if (myRow.getCell(7) != null) {
							errRow.setContent8(String.valueOf(myRow.getCell(7)));
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.number", iRun, "DiscPer");
					}
					//8 get Free product code
					try {
						if (checkColumnNecessary(detail.type, 8) && StringUtil.isNullOrEmpty(messageError)) {
							Cell cellFreeProductCode = myRow.getCell(8);
							if (cellFreeProductCode != null) {
								String freeProductCode = cellFreeProductCode.getStringCellValue();
								if (!StringUtil.isNullOrEmpty(freeProductCode)) {
									Product freeProduct = productMgr.getProductByCode(freeProductCode.trim());
									if (freeProduct == null) {
										messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist.in.db", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.disproduct.code"));
										messageError += "\n";
									}
									detail.freeProductCode = freeProductCode.toUpperCase().trim();
								}
							} else {
								messageError += R.getResource("catalog.promotion.import.column.null", "FreeItemCode");
							}
						}
						if (myRow.getCell(8) != null) {
							errRow.setContent9(String.valueOf(myRow.getCell(8)));
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "FreeItemCode");
					}
					//9 get free Quantity
					try {
						if (checkColumnNecessary(detail.type, 9) && StringUtil.isNullOrEmpty(messageError)) {
							Cell cellFreeQuantity = myRow.getCell(9);
							if (cellFreeQuantity != null && cellFreeQuantity.getCellType() != Cell.CELL_TYPE_BLANK) {
								if (cellFreeQuantity.getCellType() == Cell.CELL_TYPE_NUMERIC) {
									Integer freeQuantity = (int) cellFreeQuantity.getNumericCellValue();
									if (freeQuantity == null || freeQuantity < 0) {
										messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.date", "FreeQty");
									}
									detail.freeQuantity = freeQuantity;
								} else {
									messageError += R.getResource("catalog.promotion.import.column.invalid.format.number", "FreeQty");
								}
							} else {
								messageError += R.getResource("catalog.promotion.import.column.null", "FreeQty");
							}
						}
						if (myRow.getCell(9) != null) {
							errRow.setContent10(String.valueOf(myRow.getCell(9)));
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.number", iRun, "FreeQty");
					}
					//10 get Free UOM
					try {
						if (checkColumnNecessary(detail.type, 10) && StringUtil.isNullOrEmpty(messageError)) {
							if (myRow.getCell(10) != null) {
								errRow.setContent11(myRow.getCell(10).getStringCellValue());
							}
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.cant.read.cell.string", iRun, "FreeUOM");
					}
					//11 get And Or
					try {
						if (checkColumnNecessary(detail.type, 11) && StringUtil.isNullOrEmpty(messageError)) {
							Cell cellAndOrCell = myRow.getCell(11);
							if (cellAndOrCell != null) {
								String value = "";
								if (cellAndOrCell.getCellType() != Cell.CELL_TYPE_NUMERIC) {
									value = cellAndOrCell.getStringCellValue();
								} else {
									value = String.valueOf((float) cellAndOrCell.getNumericCellValue());
								}
								if ("X".equals(value.trim().toUpperCase())) {
									detail.andOr = true;
									errRow.setContent12(value);
								} else if ("".equals(value.trim().toUpperCase())) {
									detail.andOr = false;
									errRow.setContent12(value);
								} else {
									errRow.setContent12(value);
									messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.read.cell.format.invalid", iRun, "AllFreeItemcode", "['','X']");
								}
							} else {
								detail.andOr = false;
								errRow.setContent12("");
							}
						}
					} catch (Exception e) {
						messageError += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.read.cell.format.invalid", iRun, "AllFreeItemcode", "['','X']");
					}

					if (mapPromotionTypeCheck.get(detail.promotionCode) == null) {
						mapPromotionTypeCheck.put(detail.promotionCode, detail.type);
					}

					if (!promotionCode.equals(previousPromotionCode)) {
						if (mapPromotionType.get(detail.promotionCode) != null) {//da ton tai ctkm nay truoc do roi
							typeKM = mapPromotionType.get(detail.promotionCode);//=> lay ra loai cua no thoi
						} else {
							if (!StringUtil.isNullOrEmpty(detail.productCode) && detail.saleQuantity != null && detail.saleQuantity > 0 && !StringUtil.isNullOrEmpty(detail.freeProductCode) && detail.freeQuantity != null && detail.freeQuantity > 0) {
								typeKM = 1;
							} else if (!StringUtil.isNullOrEmpty(detail.productCode) && detail.saleQuantity != null && detail.saleQuantity > 0 && detail.discountAmount != null && detail.discountAmount.compareTo(BigDecimal.ZERO) > 0) {
								typeKM = 2;
							} else if (!StringUtil.isNullOrEmpty(detail.productCode) && detail.saleQuantity != null && detail.saleQuantity > 0 && detail.discountPercent != null && detail.discountPercent > 0) {
								typeKM = 3;
							} else if (!StringUtil.isNullOrEmpty(detail.productCode) && detail.saleAmount != null && detail.saleAmount.compareTo(BigDecimal.ZERO) > 0 && !StringUtil.isNullOrEmpty(detail.freeProductCode) && detail.freeQuantity != null
									&& detail.freeQuantity > 0) {
								typeKM = 4;
							} else if (!StringUtil.isNullOrEmpty(detail.productCode) && detail.saleAmount != null && detail.saleAmount.compareTo(BigDecimal.ZERO) > 0 && detail.discountAmount != null
									&& detail.discountAmount.compareTo(BigDecimal.ZERO) > 0) {
								typeKM = 5;
							} else if (!StringUtil.isNullOrEmpty(detail.productCode) && detail.saleAmount != null && detail.saleAmount.compareTo(BigDecimal.ZERO) > 0 && detail.discountPercent != null && detail.discountPercent > 0) {
								typeKM = 6;
							} else if (detail.saleAmount != null && detail.saleAmount.compareTo(BigDecimal.ZERO) > 0 && !StringUtil.isNullOrEmpty(detail.freeProductCode) && detail.freeQuantity != null && detail.freeQuantity > 0) {
								typeKM = 7;
							} else if (detail.saleAmount != null && detail.saleAmount.compareTo(BigDecimal.ZERO) > 0 && detail.discountAmount != null && detail.discountAmount.compareTo(BigDecimal.ZERO) > 0) {
								typeKM = 8;
							} else if (detail.saleAmount != null && detail.saleAmount.compareTo(BigDecimal.ZERO) > 0 && detail.discountPercent != null && detail.discountPercent > 0) {
								typeKM = 9;
							} else {
								typeKM = -1;
							}
							mapPromotionType.put(detail.promotionCode, typeKM);
							previousPromotionCode = detail.promotionCode;
						}
					} else {
						typeKM = mapPromotionType.get(detail.promotionCode);
					}

					if (StringUtil.isNullOrEmpty(messageError)) {
						List<Row> lstR = lstRow.get(detail.promotionCode);
						if (lstR == null) {
							lstR = new ArrayList<Row>();
						}
						messageError = checkDuplicate(mapType.get(detail.promotionCode), lstR, myRow);
						lstR.add(myRow);
						lstRow.put(detail.promotionCode, lstR);
					}
					if (StringUtil.isNullOrEmpty(messageError)) {
						if (mapArrayProduct.get(detail.promotionCode) == null) {
							String[] arrProduct = new String[MAX_ARRAY];
							arrProduct[iRun] = detail.productCode;
							mapArrayProduct.put(detail.promotionCode, arrProduct);
						} else {
							String[] arrProduct = mapArrayProduct.get(detail.promotionCode);
							arrProduct[iRun] = detail.productCode;
						}
						if (mapArraySaleQuantity.get(detail.promotionCode) == null) {
							Integer[] arrSaleQuantity = new Integer[MAX_ARRAY];
							arrSaleQuantity[iRun] = detail.saleQuantity;
							mapArraySaleQuantity.put(detail.promotionCode, arrSaleQuantity);
						} else {
							Integer[] arrSaleQuantity = mapArraySaleQuantity.get(detail.promotionCode);
							arrSaleQuantity[iRun] = detail.saleQuantity;
						}
						if (mapArraySaleAmount.get(detail.promotionCode) == null) {
							BigDecimal[] arrSaleAmount = new BigDecimal[MAX_ARRAY];
							arrSaleAmount[iRun] = detail.saleAmount;
							mapArraySaleAmount.put(detail.promotionCode, arrSaleAmount);
						} else {
							BigDecimal[] arrSaleAmount = mapArraySaleAmount.get(detail.promotionCode);
							arrSaleAmount[iRun] = detail.saleAmount;
						}
						if (mapArrayFreeProduct.get(detail.promotionCode) == null) {
							String[] arrFreeProduct = new String[MAX_ARRAY];
							arrFreeProduct[iRun] = detail.freeProductCode;
							mapArrayFreeProduct.put(detail.promotionCode, arrFreeProduct);
						} else {
							String[] arrFreeProduct = mapArrayFreeProduct.get(detail.promotionCode);
							arrFreeProduct[iRun] = detail.freeProductCode;
						}
						if (mapArrayFreeQuantity.get(detail.promotionCode) == null) {
							Integer[] arrFreeQuantity = new Integer[MAX_ARRAY];
							arrFreeQuantity[iRun] = detail.freeQuantity;
							mapArrayFreeQuantity.put(detail.promotionCode, arrFreeQuantity);
						} else {
							Integer[] arrFreeProduct = mapArrayFreeQuantity.get(detail.promotionCode);
							arrFreeProduct[iRun] = detail.freeQuantity;
						}
						if (mapArrayDiscountAmount.get(detail.promotionCode) == null) {
							BigDecimal[] arrDiscountAmount = new BigDecimal[MAX_ARRAY];
							arrDiscountAmount[iRun] = detail.discountAmount;
							mapArrayDiscountAmount.put(detail.promotionCode, arrDiscountAmount);
						} else {
							BigDecimal[] arrDiscountAmount = mapArrayDiscountAmount.get(detail.promotionCode);
							arrDiscountAmount[iRun] = detail.discountAmount;
						}
						if (mapArrayDiscountPercent.get(detail.promotionCode) == null) {
							Float[] arrDiscountPercent = new Float[MAX_ARRAY];
							arrDiscountPercent[iRun] = detail.discountPercent;
							mapArrayDiscountPercent.put(detail.promotionCode, arrDiscountPercent);
						} else {
							Float[] arrDiscountPercent = mapArrayDiscountPercent.get(detail.promotionCode);
							arrDiscountPercent[iRun] = detail.discountPercent;
						}
						if (mapArrayAndOr.get(detail.promotionCode) == null) {
							Boolean[] arrAndOr = new Boolean[MAX_ARRAY];
							arrAndOr[iRun] = detail.andOr;
							mapArrayAndOr.put(detail.promotionCode, arrAndOr);
						} else {
							Boolean[] arrAndOr = mapArrayAndOr.get(detail.promotionCode);
							arrAndOr[iRun] = detail.andOr;
						}
						if (!StringUtil.isNullOrEmpty(detail.productCode) && lstProductPromo.get(detail.promotionCode + "-" + detail.productCode) == null) {
							lstProductPromo.put(detail.promotionCode + "-" + detail.productCode, indexProductPromo++);
						}
					} else {
						//error
						errRow.setContent13(messageError);
						lstDetailError.add(errRow);
						if (mapErrorPromotion.get(detail.promotionCode) == null) {
							mapErrorPromotion.put(detail.promotionCode, messageError);
						}
					}
					totalItem++;
					iRun++;
				}

				/**
				 * put vao group level
				 */
				for (String promotionProgramCode : mapPromotionType.keySet()) {
					if (mapPromotionType.get(promotionProgramCode) == 1) {
						/**
						 * mua A(1), B(1) dc km ... C(1), D(1)
						 */
						String[] arrProduct = mapArrayProduct.get(promotionProgramCode);
						Integer[] arrSaleQuantity = mapArraySaleQuantity.get(promotionProgramCode);
						Boolean[] arrAndOr = mapArrayAndOr.get(promotionProgramCode);
						String[] arrFreeProduct = mapArrayFreeProduct.get(promotionProgramCode);
						Integer[] arrFreeQuantity = mapArrayFreeQuantity.get(promotionProgramCode);
						//Sort theo saleQuantity
						sortQuantityProduct(arrProduct, arrSaleQuantity, arrAndOr, arrFreeProduct, arrFreeQuantity, null, null);
						for (int i = 0; arrProduct != null && i < arrProduct.length; i++) {
							if (!StringUtil.isNullOrEmpty(arrProduct[i]) && arrSaleQuantity[i] != null && !StringUtil.isNullOrEmpty(arrFreeProduct[i]) && arrFreeQuantity[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleQuantity[i], indexMua++, i, arrProduct, arrSaleQuantity, arrFreeProduct, arrFreeQuantity);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleQuantity[i], indexMua++, i, arrProduct, arrSaleQuantity, arrFreeProduct, arrFreeQuantity);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrFreeProduct[i], arrFreeQuantity[i], arrAndOr[i], indexKM++, i, arrFreeProduct, arrFreeQuantity, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrFreeProduct[i], arrFreeQuantity[i], arrAndOr[i], indexKM++, i, arrFreeProduct, arrFreeQuantity, null);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					} else if (mapPromotionType.get(promotionProgramCode) == 2) {
						/**
						 * mua A(1), B(1) dc km ... 10.000
						 */
						String[] arrProduct = mapArrayProduct.get(promotionProgramCode);
						Integer[] arrSaleQuantity = mapArraySaleQuantity.get(promotionProgramCode);
						Boolean[] arrAndOr = mapArrayAndOr.get(promotionProgramCode);
						BigDecimal[] arrFreeAmount = mapArrayDiscountAmount.get(promotionProgramCode);
						//Sort theo saleQuantity
						sortQuantityProduct(arrProduct, arrSaleQuantity, arrAndOr, null, null, arrFreeAmount, null);
						for (int i = 0; arrProduct != null && i < arrProduct.length; i++) {
							if (!StringUtil.isNullOrEmpty(arrProduct[i]) && arrSaleQuantity[i] != null && arrFreeAmount[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleQuantity[i], indexMua++, i, arrProduct, arrSaleQuantity, null, null);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleQuantity[i], indexMua++, i, arrProduct, arrSaleQuantity, null, null);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(arrFreeAmount[i], indexKM++, i, arrFreeAmount, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(arrFreeAmount[i], indexKM++, i, arrFreeAmount, lstLevelKM);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					} else if (mapPromotionType.get(promotionProgramCode) == 3) {
						/**
						 * mua A(1), B(1) dc km ... 10%
						 */
						String[] arrProduct = mapArrayProduct.get(promotionProgramCode);
						Integer[] arrSaleQuantity = mapArraySaleQuantity.get(promotionProgramCode);
						Boolean[] arrAndOr = mapArrayAndOr.get(promotionProgramCode);
						Float[] arrPercent = mapArrayDiscountPercent.get(promotionProgramCode);
						//Sort theo saleQuantity
						sortQuantityProduct(arrProduct, arrSaleQuantity, arrAndOr, null, null, null, arrPercent);
						for (int i = 0; arrProduct != null && i < arrProduct.length; i++) {
							if (!StringUtil.isNullOrEmpty(arrProduct[i]) && arrSaleQuantity[i] != null && arrPercent[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleQuantity[i], indexMua++, i, arrProduct, arrSaleQuantity, null, null);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleQuantity[i], indexMua++, i, arrProduct, arrSaleQuantity, null, null);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(arrPercent[i], indexKM++, i, arrPercent, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(arrPercent[i], indexKM++, i, arrPercent, lstLevelKM);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					} else if (mapPromotionType.get(promotionProgramCode) == 4) {
						/**
						 * mua A(10.000), B(10.000) dc km ... C(1), D(1)
						 */
						String[] arrProduct = mapArrayProduct.get(promotionProgramCode);
						BigDecimal[] arrSaleAmount = mapArraySaleAmount.get(promotionProgramCode);
						Boolean[] arrAndOr = mapArrayAndOr.get(promotionProgramCode);
						String[] arrFreeProduct = mapArrayFreeProduct.get(promotionProgramCode);
						Integer[] arrFreeQuantity = mapArrayFreeQuantity.get(promotionProgramCode);
						//Sort theo saleQuantity
						sortAmountProduct(arrProduct, arrSaleAmount, arrAndOr, arrFreeProduct, arrFreeQuantity, null, null);
						for (int i = 0; arrProduct != null && i < arrProduct.length; i++) {
							if (!StringUtil.isNullOrEmpty(arrProduct[i]) && arrSaleAmount[i] != null && !StringUtil.isNullOrEmpty(arrFreeProduct[i]) && arrFreeQuantity[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleAmount[i], indexMua++, i, arrProduct, arrSaleAmount, arrFreeProduct, arrFreeQuantity);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleAmount[i], indexMua++, i, arrProduct, arrSaleAmount, arrFreeProduct, arrFreeQuantity);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrFreeProduct[i], arrFreeQuantity[i], arrAndOr[i], indexKM++, i, arrFreeProduct, arrFreeQuantity, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrFreeProduct[i], arrFreeQuantity[i], arrAndOr[i], indexKM++, i, arrFreeProduct, arrFreeQuantity, null);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					} else if (mapPromotionType.get(promotionProgramCode) == 5) {
						/**
						 * mua A(10.000), B(10.000) dc km ... 10.000
						 */
						String[] arrProduct = mapArrayProduct.get(promotionProgramCode);
						BigDecimal[] arrSaleAmount = mapArraySaleAmount.get(promotionProgramCode);
						Boolean[] arrAndOr = mapArrayAndOr.get(promotionProgramCode);
						BigDecimal[] arrFreeAmount = mapArrayDiscountAmount.get(promotionProgramCode);
						//Sort theo saleQuantity
						sortAmountProduct(arrProduct, arrSaleAmount, arrAndOr, null, null, arrFreeAmount, null);
						for (int i = 0; arrProduct != null && i < arrProduct.length; i++) {
							if (!StringUtil.isNullOrEmpty(arrProduct[i]) && arrSaleAmount[i] != null && arrFreeAmount[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleAmount[i], indexMua++, i, arrProduct, arrSaleAmount, null, null);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleAmount[i], indexMua++, i, arrProduct, arrSaleAmount, null, null);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(arrFreeAmount[i], indexKM++, i, arrFreeAmount, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(arrFreeAmount[i], indexKM++, i, arrFreeAmount, lstLevelKM);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					} else if (mapPromotionType.get(promotionProgramCode) == 6) {
						/**
						 * mua A(10.000), B(10.000) dc km ... 10%
						 */
						String[] arrProduct = mapArrayProduct.get(promotionProgramCode);
						BigDecimal[] arrSaleAmount = mapArraySaleAmount.get(promotionProgramCode);
						Boolean[] arrAndOr = mapArrayAndOr.get(promotionProgramCode);
						Float[] arrPercent = mapArrayDiscountPercent.get(promotionProgramCode);
						//Sort theo saleQuantity
						sortAmountProduct(arrProduct, arrSaleAmount, arrAndOr, null, null, null, arrPercent);
						for (int i = 0; arrProduct != null && i < arrProduct.length; i++) {
							if (!StringUtil.isNullOrEmpty(arrProduct[i]) && arrSaleAmount[i] != null && arrPercent[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleAmount[i], indexMua++, i, arrProduct, arrSaleAmount, null, null);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrProduct[i], arrSaleAmount[i], indexMua++, i, arrProduct, arrSaleAmount, null, null);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(arrPercent[i], indexKM++, i, arrPercent, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(arrPercent[i], indexKM++, i, arrPercent, lstLevelKM);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					} else if (mapPromotionType.get(promotionProgramCode) == 7) {
						/**
						 * mua 10000 dc km ... C(1), D(1)
						 */
						BigDecimal[] arrSaleAmount = mapArraySaleAmount.get(promotionProgramCode);
						String[] arrFreeProduct = mapArrayFreeProduct.get(promotionProgramCode);
						Integer[] arrFreeQuantity = mapArrayFreeQuantity.get(promotionProgramCode);
						Boolean[] arrAndOr = mapArrayAndOr.get(promotionProgramCode);
						sortAmount(arrSaleAmount, arrAndOr, arrFreeProduct, arrFreeQuantity, null, null);
						for (int i = 0; arrSaleAmount != null && i < arrSaleAmount.length; i++) {
							if (arrSaleAmount[i] != null && !StringUtil.isNullOrEmpty(arrFreeProduct[i]) && arrFreeQuantity[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(arrSaleAmount[i], indexMua++, i, arrSaleAmount, arrFreeProduct, arrFreeQuantity);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(arrSaleAmount[i], indexMua++, i, arrSaleAmount, arrFreeProduct, arrFreeQuantity);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrFreeProduct[i], arrFreeQuantity[i], arrAndOr[i], indexKM++, i, arrFreeProduct, arrFreeQuantity, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(mapPromotionTypeCheck.get(promotionProgramCode), arrFreeProduct[i], arrFreeQuantity[i], arrAndOr[i], indexKM++, i, arrFreeProduct, arrFreeQuantity, null);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					} else if (mapPromotionType.get(promotionProgramCode) == 8) {
						/**
						 * mua 10000 dc km ... 10.000
						 */
						BigDecimal[] arrSaleAmount = mapArraySaleAmount.get(promotionProgramCode);
						BigDecimal[] arrFreeAmount = mapArrayDiscountAmount.get(promotionProgramCode);
						sortAmount(arrSaleAmount, null, null, null, arrFreeAmount, null);
						for (int i = 0; arrSaleAmount != null && i < arrSaleAmount.length; i++) {
							if (arrSaleAmount[i] != null && arrFreeAmount[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(arrSaleAmount[i], indexMua++, i, arrSaleAmount, null, null);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(arrSaleAmount[i], indexMua++, i, arrSaleAmount, null, null);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(arrFreeAmount[i], indexKM++, i, arrFreeAmount, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(arrFreeAmount[i], indexKM++, i, arrFreeAmount, lstLevelKM);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					} else if (mapPromotionType.get(promotionProgramCode) == 9) {
						/**
						 * mua 10000 dc km ... 10%
						 */
						BigDecimal[] arrSaleAmount = mapArraySaleAmount.get(promotionProgramCode);
						Float[] arrPercent = mapArrayDiscountPercent.get(promotionProgramCode);
						sortAmount(arrSaleAmount, null, null, null, null, arrPercent);
						for (int i = 0; arrSaleAmount != null && i < arrSaleAmount.length; i++) {
							if (arrSaleAmount[i] != null && arrPercent[i] != null) {
								GroupMua groupMua = null;
								GroupKM groupKM = null;
								if (mapPromotionMua.get(promotionProgramCode) != null && mapPromotionKM.get(promotionProgramCode) != null) {
									groupMua = mapPromotionMua.get(promotionProgramCode).get(mapPromotionMua.get(promotionProgramCode).size() - 1);
									groupKM = mapPromotionKM.get(promotionProgramCode).get(mapPromotionKM.get(promotionProgramCode).size() - 1);
								} else {
									ListGroupMua lstGroupMua = new ListGroupMua();
									ListGroupKM lstGroupKM = new ListGroupKM();
									groupMua = new GroupMua();
									groupMua.groupCode = "N" + (lstGroupMua.size() + 1);
									groupKM = new GroupKM();
									groupKM.groupCode = "N" + (lstGroupKM.size() + 1);
									groupMua.order = lstGroupMua.size() + 1;
									lstGroupMua.add(groupMua);
									groupKM.order = lstGroupKM.size() + 1;
									lstGroupKM.add(groupKM);
									mapPromotionMua.put(promotionProgramCode, lstGroupMua);
									mapPromotionKM.put(promotionProgramCode, lstGroupKM);
								}
								GroupSP groupSPMua = groupMua.add2Level(arrSaleAmount[i], indexMua++, i, arrSaleAmount, null, null);
								if (groupSPMua == null) {
									groupMua = new GroupMua();
									List<GroupMua> lstGroupMua = mapPromotionMua.get(promotionProgramCode);
									lstGroupMua.add(groupMua);
									groupSPMua = groupMua.add2Level(arrSaleAmount[i], indexMua++, i, arrSaleAmount, null, null);
								}
								List<Long> lstIndex = mapMuaKM.get(groupSPMua.index);//lay danh sach cac index muc duoc map voi muc sp mua
								List<GroupSP> lstLevelKM = groupKM.searchIndex(lstIndex);//lay danh sach cac muc duoc map voi muc sp mua
								GroupSP groupSPKM = groupKM.add2Level(arrPercent[i], indexKM++, i, arrPercent, lstLevelKM);
								if (groupSPKM == null) {
									groupKM = new GroupKM();
									List<GroupKM> lstGroupKM = mapPromotionKM.get(promotionProgramCode);
									lstGroupKM.add(groupKM);
									groupSPKM = groupKM.add2Level(arrPercent[i], indexKM++, i, arrPercent, lstLevelKM);
								}
								mapMuaKM.put(groupSPMua.index, groupSPKM.index);
							}
						}
					}
				}
				for (String promotionProgramCode : mapPromotionType.keySet()) {
					splitGroup(mapPromotionMua.get(promotionProgramCode), mapPromotionKM.get(promotionProgramCode), mapMuaKM, promotionProgramCode, lstProductPromo);
				}
			}
		}
	}
	
	/**
	 * Lay du lieu trong 1 o cell
	 * 
	 * @param cellData - cell du lieu doc tu workbook
	 * 
	 * @return chuoi ki tu chua trong cell (khong null)
	 * 
	 * @author lacnv1
	 * @since Apr 13, 2015
	 */
	private String getCellValue(Cell cellData) throws Exception {
		if (cellData == null) {
			return "";
		}
		switch (cellData.getCellType()) {
		case Cell.CELL_TYPE_BLANK:
			return "";
		case Cell.CELL_TYPE_NUMERIC:
			if (HSSFDateUtil.isCellDateFormatted(cellData)) {
				return DateUtil.toDateString(cellData.getDateCellValue(), DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				double val = cellData.getNumericCellValue();
				Double dVal = Double.valueOf(val);
				if (BigDecimal.valueOf(dVal).compareTo(BigDecimal.valueOf(dVal.longValue())) == 0) {
					return String.valueOf(BigDecimal.valueOf(dVal).longValue());
				} else {
					return String.valueOf(val);
				}
			}
		case Cell.CELL_TYPE_STRING:
			return cellData.getStringCellValue().trim();
		default:
			return cellData.toString();
		}
	}

	/**
	 * tach cac nhom san pham mua
	 * 
	 * @author tungmt
	 * @since 11/09/2014
	 */
	public void splitGroup(ListGroupMua lstGroupMua, ListGroupKM lstGroupKM, MapMuaKM mapMua, String promotionProgramCode, LinkedHashMap<String, Integer> lstProductPromo) {
		if (lstGroupMua != null && lstGroupKM != null) {
			for (int k = 0; k < lstGroupMua.size(); k++) {
				GroupMua groupMua = lstGroupMua.get(k);
				GroupKM groupKM = lstGroupKM.get(k);
				for (int j = 1; j < groupMua.lstLevel.size(); j++) {//so sanh khac nhau giua 2 level 
					GroupSP level1 = groupMua.lstLevel.get(0);
					GroupSP level2 = groupMua.lstLevel.get(j);
					GroupSP levelKM2 = groupKM.lstLevel.get(j);
					if (checkDif2Level(level1, level2)) {
						GroupMua newGroupMua = new GroupMua();
						GroupKM newGroupKM = new GroupKM();
						if (lstGroupMua.size() <= k + 1) {//chua co nhom tiep theo thi tao moi
							newGroupMua = new GroupMua();
							newGroupMua.groupCode = "N" + (lstGroupMua.size() + 1);
							newGroupMua.order = lstGroupMua.size() + 1;
							newGroupKM = new GroupKM();
							newGroupKM.groupCode = "N" + (lstGroupKM.size() + 1);
							newGroupKM.order = lstGroupKM.size() + 1;
							lstGroupMua.add(newGroupMua);
							lstGroupKM.add(newGroupKM);
						} else {
							newGroupMua = lstGroupMua.get(k + 1);
							newGroupKM = lstGroupKM.get(k + 1);
						}
						newGroupMua.lstLevel.add(new GroupSP(level2));
						newGroupKM.lstLevel.add(new GroupSP(levelKM2));
						groupMua.lstLevel.remove(j);
						groupKM.lstLevel.remove(j);
						j--;//lui lai 1 vi lstLevel da bi xoa 1 phan tu
					}
				}
			}
			//set lai order cho group
			// cac group phai co san pham khac nhau
			if (lstGroupMua != null && lstGroupMua.size() > 1) {
				//reset lai cac order
				for (GroupMua g : lstGroupMua) {
					g.order = 0;
				}
				int order = 1;
				for (Entry<String, Integer> entry : lstProductPromo.entrySet()) {
					String pp = entry.getKey();
					for (int i = 0; i < lstGroupMua.size(); i++) {
						if (lstGroupMua.get(i).order == 0) {
							List<Node> lstNode = lstGroupMua.get(i).lstLevel.get(0).lstSP;
							for (Node n : lstNode) {
								if (pp.toUpperCase().equals(promotionProgramCode + "-" + n.productCode)) {
									lstGroupMua.get(i).order = order;
									order++;
									break;
								}
							}
						}
					}
				}
				//truong hop co nhom chua set order thi mac dinh
				for (GroupMua g : lstGroupMua) {
					if (g.order == 0) {
						g.order = order;
						order++;
					}
				}
			}

			//set lai order cho grouplevel
			for (int k = 0; k < lstGroupMua.size(); k++) {
				GroupMua groupMua = lstGroupMua.get(k);
				GroupKM groupKM = lstGroupKM.get(k);
				int n = groupMua.lstLevel.size();
				for (int j = 0; j < groupMua.lstLevel.size(); j++) {//so sanh khac nhau giua 2 level 
					groupMua.lstLevel.get(j).order = n - j;
				}
				n = groupKM.lstLevel.size();
				for (int j = 0; j < groupKM.lstLevel.size(); j++) {//so sanh khac nhau giua 2 level 
					groupKM.lstLevel.get(j).order = n - j;
				}
			}

			//set va map mua va km
			mapPromotionMua.put(promotionProgramCode, lstGroupMua);
			mapPromotionKM.put(promotionProgramCode, lstGroupKM);
		}
	}

	/**
	 * neu 2 level khac san pham thi return true else false
	 * 
	 * @author tungmt
	 * @since 11/09/2014
	 */
	public boolean checkDif2Level(GroupSP g1, GroupSP g2) {
		for (Node n1 : g1.lstSP) {
			for (Node n2 : g2.lstSP) {
				if (!StringUtil.isNullOrEmpty(n1.productCode) && !StringUtil.isNullOrEmpty(n2.productCode) && n1.productCode.equals(n2.productCode)) {
					return false;
				} else if (StringUtil.isNullOrEmpty(n1.productCode) && StringUtil.isNullOrEmpty(n2.productCode)) {//type
					return false;
				}
			}
		}
		return true;
	}

	Integer isViewCustomerTab = 0;

	public Integer getIsViewCustomerTab() {
		return isViewCustomerTab;
	}

	public void setIsViewCustomerTab(Integer isViewCustomerTab) {
		this.isViewCustomerTab = isViewCustomerTab;
	}

	/**
	 * Xem thong tin CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 21, 2014
	 */
	public String viewDetail() {
		resetToken(result);
		try {
			lstTypeCode = apParamMgr.getListApParam(ApParamType.PROMOTION, ActiveType.RUNNING);
			if (promotionId == null || promotionId == 0) {
				return SUCCESS;
			}
			promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (promotionProgram == null) {
				isError = true;
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM");
				return SUCCESS;
			}
			ObjectVO<Product> listProductVO = productMgr.getListProduct(null, ActiveType.RUNNING);
			if (listProductVO != null) {
				listProduct = listProductVO.getLstObject();
			} else {
				listProduct = new ArrayList<Product>();
			}
			
			id = promotionId;
			if (promotionProgramMgr.checkExistPromotionShopMapByListShop(getStrListShopId(), promotionId)) {
				isViewCustomerTab = 1;
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.viewDetail - " + ex.getMessage());
		}
		return SUCCESS;
	}

	public String detailGroupProduct() {
		return SUCCESS;
	}

	public String addSaleProductGroup() {
		resetToken(result);
		String errMsg = "";
		if ((StringUtil.isNullOrEmpty(groupCode) || StringUtil.isNullOrEmpty(groupName)) && maxQuantity == null && maxAmount == null) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.user.input");
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		try {
			//Them check XSS du lieu
			if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupCode)) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
			}
			if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupName)) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return SUCCESS;
			}
			if (groupId != null && groupId > 0) {
				ProductGroup productGroup = promotionProgramMgr.getProductGroupById(groupId);
				if (productGroup == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				productGroup.setProductGroupName(groupName);
				productGroup.setMinQuantity(minQuantity);
				productGroup.setMinAmount(minAmount);
				productGroup.setMultiple(multiple != null && multiple ? 1 : 0);
				productGroup.setRecursive(recursive != null && recursive ? 1 : 0);
				productGroup.setOrder(stt);
				promotionProgramMgr.updateProductGroup(productGroup, getLogInfoVO());
				if (productGroup != null && productGroup.getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(productGroup.getPromotionProgram(), getLogInfoVO());
				}
			} else {
				ProductGroup productGroup = promotionProgramMgr.getProductGroupByCode(groupCode, ProductGroupType.MUA, promotionId);
				if (productGroup != null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, groupCode);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				productGroup = promotionProgramMgr.createProductGroup(promotionId, groupCode, groupName, ProductGroupType.MUA, minQuantity, maxQuantity, minAmount, maxAmount, multiple, recursive, stt, getLogInfoVO());
				if (productGroup != null && productGroup.getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(productGroup.getPromotionProgram(), getLogInfoVO());
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, "PromotionCatalogAction.addSaleProductGroup - " + e.getMessage());
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	/**
	 * Them thong tin nhom
	 * @return
	 * @since 08-09-2015
	 * @description cap nhat code
	 */
	public String addNewProductGroup() {
		resetToken(result);
		if ((StringUtil.isNullOrEmpty(groupCode) || StringUtil.isNullOrEmpty(groupName)) && maxQuantity == null && maxAmount == null) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.user.input");
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		errMsg = ValidateUtil.validateField(groupCode, "catalog.promotion.group.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(groupName, "catalog.promotion.group.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		try {
			if (groupMuaId != null && groupMuaId > 0 && groupKMId != null && groupKMId > 0) {
				ProductGroup productGroupMua = promotionProgramMgr.getProductGroupById(groupMuaId);
				ProductGroup productGroupKM = promotionProgramMgr.getProductGroupById(groupKMId);
				if (productGroupMua == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				if (productGroupKM == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				productGroupMua.setProductGroupName(groupName);
				productGroupMua.setMinQuantity(minQuantity);
				productGroupMua.setMinAmount(minAmount);
				productGroupMua.setMultiple(multiple != null && multiple ? 1 : 0);
				productGroupMua.setRecursive(recursive != null && recursive ? 1 : 0);
				productGroupMua.setOrder(stt);
				promotionProgramMgr.updateProductGroup(productGroupMua, getLogInfoVO());
				productGroupKM.setProductGroupName(groupName);
				productGroupKM.setMaxQuantity(maxQuantity);
				productGroupKM.setMaxAmount(maxAmount);
				productGroupKM.setMultiple(multiple != null && multiple ? 1 : 0);
				productGroupKM.setRecursive(recursive != null && recursive ? 1 : 0);
				productGroupKM.setOrder(stt);
				promotionProgramMgr.updateProductGroup(productGroupKM, getLogInfoVO());
				if (productGroupMua != null && productGroupMua.getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(productGroupMua.getPromotionProgram(), getLogInfoVO());
				}
			} else {
				final List<String> lstZVMultiGroup = Arrays.asList(PromotionType.ZV01.getValue(), PromotionType.ZV02.getValue(), PromotionType.ZV03.getValue(), PromotionType.ZV04.getValue(), PromotionType.ZV05.getValue(), PromotionType.ZV06
						.getValue(), PromotionType.ZV09.getValue(), PromotionType.ZV21.getValue()
				//						PromotionType.ZV22.getValue(),
				//						PromotionType.ZV23.getValue(),
				//						PromotionType.ZV24.getValue()
						);
				PromotionProgram pp = promotionProgramMgr.getPromotionProgramById(promotionId);
				if (pp == null) {
					return SUCCESS;
				}
				String type = pp.getType();
				if (!lstZVMultiGroup.contains(type)) {
					List<ProductGroup> lstGroupTmp = promotionProgramMgr.getListProductGroupByPromotionId(promotionId, ProductGroupType.MUA);
					if (lstGroupTmp != null && lstGroupTmp.size() > 0) {
						errMsg = R.getResource("promotion.program.multi.group.invalid");
						result.put("errMsg", errMsg);
						result.put(ERROR, true);
						return SUCCESS;
					}
				}
				ProductGroup productGroupMua = promotionProgramMgr.getProductGroupByCode(groupCode, ProductGroupType.MUA, promotionId);
				ProductGroup productGroupKM = promotionProgramMgr.getProductGroupByCode(groupCode, ProductGroupType.KM, promotionId);
				if (productGroupMua != null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, groupCode);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				if (productGroupKM != null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, groupCode);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				productGroupMua = promotionProgramMgr.createProductGroup(promotionId, groupCode, groupName, ProductGroupType.MUA, minQuantity, null, minAmount, null, multiple, recursive, stt, getLogInfoVO());
				productGroupKM = promotionProgramMgr.createProductGroup(promotionId, groupCode, groupName, ProductGroupType.KM, null, maxQuantity, null, maxAmount, multiple, recursive, stt, getLogInfoVO());
				if (productGroupMua != null && productGroupMua.getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(productGroupMua.getPromotionProgram(), getLogInfoVO());
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, "PromotionCatalogAction.addSaleProductGroup - " + e.getMessage());
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	/**
	 * @modify hunglm16
	 * @return
	 * @since October 09, 2015
	 * @description bo sung kiem tra ATTT XSS
	 */
	public String updateSaleProductGroup() {
		resetToken(result);
		try {
			errMsg = "";
			if (groupId != null && groupId > 0) {
				ProductGroup productGroup = promotionProgramMgr.getProductGroupById(groupId);
				if (productGroup == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				}
				//Them check XSS du lieu
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupName)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				productGroup.setProductGroupName(groupName);
				productGroup.setMinQuantity(minQuantity);
				productGroup.setMinAmount(minAmount);
				productGroup.setMultiple(multiple != null && multiple ? 1 : 0);
				productGroup.setRecursive(recursive != null && recursive ? 1 : 0);
				productGroup.setOrder(stt);
				if (listMinQuantity == null || listMinQuantity.isEmpty() || listMinAmount == null || listMinAmount.isEmpty() || listOrder == null || listOrder.isEmpty()) {
					promotionProgramMgr.updateProductGroup(productGroup, getLogInfoVO());
					if (productGroup != null && productGroup.getPromotionProgram() != null) {
						promotionProgramMgr.updateMD5ValidCode(productGroup.getPromotionProgram(), getLogInfoVO());
					}
					return SUCCESS;
				}
				promotionProgramMgr.addLevel2SaleProductGroup(productGroup, listLevelId, listMinQuantity, listMinAmount, listOrder, listProductDetail, getLogInfoVO());
				if (productGroup != null && productGroup.getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(productGroup.getPromotionProgram(), getLogInfoVO());
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, "PromotionCatalogAction.addSaleProductGroup - " + e.getMessage());
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	public String addFreeProductGroup() {
		resetToken(result);
		errMsg = "";
		if ((StringUtil.isNullOrEmpty(groupCode) || StringUtil.isNullOrEmpty(groupName)) && maxQuantity == null && maxAmount == null) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.user.input");
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		try {
			if (groupId != null && groupId > 0) {
				ProductGroup productGroup = promotionProgramMgr.getProductGroupById(groupId);
				if (productGroup == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				//Them check XSS du lieu
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupName)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				productGroup.setProductGroupName(groupName);
				productGroup.setMaxQuantity(maxQuantity);
				productGroup.setMaxAmount(maxAmount);
				promotionProgramMgr.updateProductGroup(productGroup, getLogInfoVO());
				if (productGroup != null && productGroup.getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(productGroup.getPromotionProgram(), getLogInfoVO());
				}
			} else {
				ProductGroup productGroup = promotionProgramMgr.getProductGroupByCode(groupCode, ProductGroupType.KM, promotionId);
				if (productGroup != null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, groupCode);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				//Them check XSS du lieu
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupCode)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupName)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				productGroup = promotionProgramMgr.createProductGroup(promotionId, groupCode, groupName, ProductGroupType.KM, minQuantity, maxQuantity, minAmount, maxAmount, multiple, recursive, stt, getLogInfoVO());
				if (productGroup != null && productGroup.getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(productGroup.getPromotionProgram(), getLogInfoVO());
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, "PromotionCatalogAction.addSaleProductGroup - " + e.getMessage());
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	public String updateFreeProductGroup() {
		resetToken(result);
		errMsg = "";
		try {
			if (groupId != null && groupId > 0) {
				ProductGroup productGroup = promotionProgramMgr.getProductGroupById(groupId);
				if (productGroup == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				//Them check XSS du lieu
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(groupName)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return SUCCESS;
				}
				productGroup.setProductGroupName(groupName);
				productGroup.setMinQuantity(maxQuantity);
				productGroup.setMinAmount(maxAmount);
				if (listMaxQuantity == null || listMaxQuantity.isEmpty() || listMaxAmount == null || listMaxAmount.isEmpty() || listOrder == null || listOrder.isEmpty() || listPercent == null || listPercent.isEmpty()) {
					promotionProgramMgr.updateProductGroup(productGroup, getLogInfoVO());
				} else {
					promotionProgramMgr.addLevel2FreeProductGroup(productGroup, listLevelId, listMaxQuantity, listOrder, listMaxAmount, listPercent, listProductDetail, getLogInfoVO());
				}
				if (productGroup != null && productGroup.getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(productGroup.getPromotionProgram(), getLogInfoVO());
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, "PromotionCatalogAction.addSaleProductGroup - " + e.getMessage());
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	public String deleteProductGroup() {
		resetToken(result);
		try {
			ProductGroup productGroup = promotionProgramMgr.getProductGroupById(groupId);
			promotionProgramMgr.deleteProductGroup(productGroup, getLogInfoVO());
			if (productGroup != null && productGroup.getPromotionProgram() != null) {
				promotionProgramMgr.updateMD5ValidCode(productGroup.getPromotionProgram(), getLogInfoVO());
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, "PromotionCatalogAction.deleteSaleProductGroup - " + e.getMessage());
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	/**
	 * Xoa nhom san pham km
	 * 
	 * @modify hunglm16
	 * @return
	 * @description Update Co cau
	 */
	public String deleteProductGroupNew() {
		resetToken(result);
		try {
			ProductGroup productGroupMua = promotionProgramMgr.getProductGroupById(groupMuaId);
			ProductGroup productGroupKM = promotionProgramMgr.getProductGroupById(groupKMId);
			promotionProgramMgr.deleteProductGroup(productGroupMua, getLogInfoVO());
			promotionProgramMgr.deleteProductGroup(productGroupKM, getLogInfoVO());
			if (productGroupMua != null && productGroupMua.getPromotionProgram() != null) {
				promotionProgramMgr.updateMD5ValidCode(productGroupMua.getPromotionProgram(), getLogInfoVO());
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, "PromotionCatalogAction.deleteSaleProductGroup - " + e.getMessage());
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return SUCCESS;
		}
		return SUCCESS;
	}

	public String groupSaleProduct() {
		try {
			lstGroupSale = promotionProgramMgr.getListProductGroupByPromotionId(id, ProductGroupType.MUA);
		} catch (Exception e) {
			LogUtility.logError(e, "PromotionCatalogAction.groupSaleProduct - " + e.getMessage());
			lstGroupSale = new ArrayList<ProductGroup>();
		}
		return SUCCESS;
	}

	public String groupFreeProduct() {
		try {
			lstGroupFree = promotionProgramMgr.getListProductGroupByPromotionId(id, ProductGroupType.KM);
		} catch (Exception e) {
			LogUtility.logError(e, "PromotionCatalogAction.groupFreeProduct - " + e.getMessage());
			lstGroupSale = new ArrayList<ProductGroup>();
			LogUtility.logError(e, "PromotionCatalogAction.listLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String groupNewProduct() {
		try {
			lstGroupNew = promotionProgramMgr.getListNewProductGroupByPromotionId(id);
		} catch (Exception e) {
			LogUtility.logError(e, "PromotionCatalogAction.groupSaleProduct - " + e.getMessage());
			lstGroupNew = new ArrayList<NewProductGroupVO>();
		}
		return SUCCESS;
	}

	public String listLevel() {
		try {
			lstLevel = promotionProgramMgr.getListGroupLevelVO(groupId);
		} catch (Exception e) {
			lstLevel = new ArrayList<GroupLevelVO>();
			LogUtility.logError(e, "PromotionCatalogAction.listLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String deleteMuaLevel() {
		resetToken(result);
		try {
			GroupLevel level = promotionProgramMgr.getGroupLevelByLevelId(levelMuaId);
			if (level == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.group.level.not.exist"));
				return SUCCESS;
			}
			List<GroupMapping> listMapping = promotionProgramMgr.getListGroupMappingByLevelId(levelMuaId, levelKMId);
			if (!listMapping.isEmpty()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.group.level.mapping.exist"));
				return SUCCESS;
			}

			promotionProgramMgr.deleteGroupLevel(levelMuaId, getLogInfoVO());
			if (level != null && level.getProductGroup() != null && level.getProductGroup().getPromotionProgram() != null) {
				promotionProgramMgr.updateMD5ValidCode(level.getProductGroup().getPromotionProgram(), getLogInfoVO());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.deleteLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String deleteKMLevel() {
		resetToken(result);
		try {
			GroupLevel level = promotionProgramMgr.getGroupLevelByLevelId(levelKMId);
			if (level == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.group.level.not.exist"));
				return SUCCESS;
			}
			List<GroupMapping> listMapping = promotionProgramMgr.getListGroupMappingByLevelId(levelMuaId, levelKMId);
			if (!listMapping.isEmpty()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.group.level.mapping.exist"));
				return SUCCESS;
			}

			promotionProgramMgr.deleteGroupLevel(levelKMId, getLogInfoVO());
			if (level != null && level.getProductGroup() != null && level.getProductGroup().getPromotionProgram() != null) {
				promotionProgramMgr.updateMD5ValidCode(level.getProductGroup().getPromotionProgram(), getLogInfoVO());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.deleteLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String allGroupOfPromotion() {
		try {
			List<ProductGroupVO> lstGroupSaleVO = promotionProgramMgr.getListProductGroupVO(id, ProductGroupType.MUA);
			List<ProductGroupVO> lstGroupFreeVO = promotionProgramMgr.getListProductGroupVO(id, ProductGroupType.KM);
			result.put("lstGroupSale", lstGroupSaleVO);
			result.put("lstGroupFree", lstGroupFreeVO);
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.listLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String listLevelOfGroup() {
		try {
			List<GroupLevelVO> listLevelMua = promotionProgramMgr.getListGroupLevelVO(groupMuaId);
			List<GroupLevelVO> listLevelKM = promotionProgramMgr.getListGroupLevelVO(groupKMId);
			result.put("listLevelMua", listLevelMua);
			result.put("listLevelKM", listLevelKM);
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	public String listMapping() {
		try {
			/*
			 * if(groupMuaId == null || groupMuaId <= 0 || groupKMId == null ||
			 * groupKMId <= 0) { listLevelMapping = new
			 * ArrayList<LevelMappingVO>(); return SUCCESS; }
			 */
			listLevelMapping = promotionProgramMgr.getListLevelMappingByGroupId(id, groupMuaId, groupKMId);
		} catch (Exception e) {
			listLevelMapping = new ArrayList<LevelMappingVO>();
			LogUtility.logError(e, "PromotionCatalogAction.listLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String deleteMapping() {
		resetToken(result);
		try {
			GroupMapping groupMapping = null;
			if (mappingId != null) {
				groupMapping = promotionProgramMgr.getGroupMappingById(mappingId);
				if (groupMapping != null) {
					promotionProgramMgr.deleteGroupMapping(groupMapping);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
					return SUCCESS;
				}
			} else if (!StringUtil.isNullOrEmpty(groupMuaCode) && !StringUtil.isNullOrEmpty(groupKMCode) && orderLevelMua != null && orderLevelKM != null) {
				groupMapping = promotionProgramMgr.getGroupMappingByGroupCodeAndOrder(id, groupMuaCode, orderLevelMua, groupKMCode, orderLevelKM);
				if (groupMapping != null) {
					promotionProgramMgr.deleteGroupMapping(groupMapping);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.group.mapping.doesnt.exist", groupMuaCode, orderLevelMua, groupKMCode, orderLevelKM));
					return SUCCESS;
				}
			}
			if (groupMapping != null && groupMapping.getPromotionGroup() != null && groupMapping.getPromotionGroup().getPromotionProgram() != null) {
				promotionProgramMgr.updateMD5ValidCode(groupMapping.getPromotionGroup().getPromotionProgram(), getLogInfoVO());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	public String saveMapping() {
		try {
			List<GroupMapping> listExisted = promotionProgramMgr.getGroupMappingByGroupCodeAndOrderMua(id, groupMuaCode, orderLevelMua);
			if (!listExisted.isEmpty()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.group.mapping.existed", groupMuaCode, orderLevelMua));
				return SUCCESS;
			}
			GroupMapping groupMapping = promotionProgramMgr.createGroupMappingByGroupCodeAndOrder(id, groupMuaCode, orderLevelMua, groupKMCode, orderLevelKM, getLogInfoVO());
			LevelMappingVO __groupMapping = new LevelMappingVO();
			__groupMapping.setMappingId(groupMapping.getId());
			__groupMapping.setIdLevelMua(groupMapping.getSaleGroupLevel().getId());
			__groupMapping.setGroupMuaCode(groupMapping.getSaleGroup().getProductGroupCode());
			__groupMapping.setGroupMuaName(groupMapping.getSaleGroup().getProductGroupName());
			__groupMapping.setOrderLevelMua(groupMapping.getSaleGroupLevel().getOrder());
			__groupMapping.setMinQuantityMua(groupMapping.getSaleGroupLevel().getMinQuantity());
			__groupMapping.setMinAmountMua(groupMapping.getSaleGroupLevel().getMinAmount());

			__groupMapping.setIdLevelKM(groupMapping.getPromotionGroupLevel().getId());
			__groupMapping.setGroupKMCode(groupMapping.getPromotionGroup().getProductGroupCode());
			__groupMapping.setGroupKMName(groupMapping.getPromotionGroup().getProductGroupName());
			__groupMapping.setOrderLevelKM(groupMapping.getPromotionGroupLevel().getOrder());
			__groupMapping.setMaxQuantityKM(groupMapping.getPromotionGroupLevel().getMaxQuantity());
			__groupMapping.setPercentKM(groupMapping.getPromotionGroupLevel().getPercent());
			if (groupMapping != null && groupMapping.getPromotionGroup() != null && groupMapping.getPromotionGroup().getPromotionProgram() != null) {
				promotionProgramMgr.updateMD5ValidCode(groupMapping.getPromotionGroup().getPromotionProgram(), getLogInfoVO());
			}
			result.put(ERROR, false);
			result.put("groupMapping", __groupMapping);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * load list mapping
	 * 
	 * @author phut
	 * @return
	 */
	public String newListMapLevel() {
		try {
			ObjectVO<NewLevelMapping> vo = promotionProgramMgr.getListMappingLevel(groupMuaId, groupKMId, fromLevel, toLevel);
			listNewMapping = vo.getLstObject();
			result.put("listNewMapping", listNewMapping);
			result.put("total", vo.getkPaging().getMaxResult());
			if (listNewMapping != null && listNewMapping.isEmpty()) {
				List<NewLevelMapping> __listNewMapping = promotionProgramMgr.getListMappingLevel(groupMuaId, groupKMId, null, null).getLstObject();
				if (__listNewMapping != null && __listNewMapping.isEmpty()) {
					result.put("isNew", true);
				} else {
					result.put("isNew", false);
				}
			} else {
				result.put("isNew", false);
			}
		} catch (Exception e) {
			result.put("isNew", false);
			result.put("listNewMapping", new ArrayList<NewLevelMapping>());
			result.put("total", 0);
			LogUtility.logError(e, "PromotionCatalogAction.newListMapLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String getMaxOrderNumber() {
		try {
			Integer stt = promotionProgramMgr.getMaxOrderNumberOfGroupLevel(groupMuaId);
			result.put(ERROR, false);
			if(stt == null || stt == 0) {
				stt = 1;
			} else {
				stt = stt + 1;
			}
			result.put("stt", stt);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "PromotionCatalogAction.newListMapLevel - " + e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * tao 2 level cho 2 group map cho 2 level
	 * 
	 * @return NewLevelMapping
	 * 
	 * @modify hunglm16
	 * @since 08-09-2015
	 */
	public String newAddLevel() {
		resetToken(result);
		try {
			GroupLevel existGroupLevel = new GroupLevel();
			if (levelMuaId == null || levelMuaId == 0 || levelKMId == null || levelKMId == 0) {
				existGroupLevel = promotionProgramMgr.getGroupLevelByLevelCode(groupMuaId, null, stt);
				if (existGroupLevel != null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.group.level.code.exist"));
					return SUCCESS;
				}
				NewLevelMapping newMapping = promotionProgramMgr.newAddLevel(groupMuaId, groupKMId, levelCode, stt, getLogInfoVO());
				result.put(ERROR, false);
				result.put("newMapping", newMapping);
			} else {
				existGroupLevel = promotionProgramMgr.getGroupLevelByLevelCode(groupMuaId, null, stt);
				if (existGroupLevel != null && !existGroupLevel.getId().equals(levelMuaId)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.group.level.code.exist"));
					return SUCCESS;
				}
				NewLevelMapping newMapping = promotionProgramMgr.newUpdateLevel(groupMuaId, groupKMId, levelMuaId, levelKMId, levelCode, stt, getLogInfoVO());
				if (existGroupLevel != null && existGroupLevel.getProductGroup() != null && existGroupLevel.getProductGroup().getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(existGroupLevel.getProductGroup().getPromotionProgram(), getLogInfoVO());
				}
				result.put(ERROR, false);
				result.put("newMapping", newMapping);
			}
		} catch (Exception e) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			LogUtility.logError(e, "PromotionCatalogAction.newAddLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Cap nhat ham them nhom co cau khuyen mai
	 * 
	 * @modify hunglm16
	 * @return
	 * @since September 08, 2015 
	 */
	public String newSaveLevel() {
		resetToken(result);
		try {
			if (mappingId == null || mappingId == 0) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				result.put(ERROR, true);
				return SUCCESS;
			}
			/**
			 * check Cac muc trong nhom phai co so luong/so tien khac nhau.
			 * Khong the muc 1 la 1A, 2B muc 2 cung la 1A, 2B
			 */
			Integer checkLevel = promotionProgramMgr.checkLevel(mappingId, listSubLevelMua, listSubLevelKM);
			if (checkLevel == 1) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.group.level.code.maping.exist"));
				result.put(ERROR, true);
				return SUCCESS;
			} else if (checkLevel == 2) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.group.level.code.maping.net.same"));
				result.put(ERROR, true);
				return SUCCESS;
			} else if (checkLevel == 3) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.group.level.code.maping.value.not.same"));
				result.put(ERROR, true);
				return SUCCESS;
			}

			Map<String, Object> returnMap = promotionProgramMgr.newSaveSublevel(mappingId, stt, listSubLevelMua, listSubLevelKM, getLogInfoVO());
			result.put(ERROR, false);
			if (returnMap.get("listSubLevelMua") != null) {
				result.put("listSubLevelMua", returnMap.get("listSubLevelMua"));
			}
			if (returnMap.get("listSubLevelKM") != null) {
				result.put("listSubLevelKM", returnMap.get("listSubLevelKM"));
			}
			if (mappingId != null) {
				GroupMapping mapping = promotionProgramMgr.getGroupMappingById(mappingId);
				if (mapping != null && mapping.getPromotionGroup() != null && mapping.getPromotionGroup().getPromotionProgram() != null) {
					PromotionProgram program = mapping.getPromotionGroup().getPromotionProgram();
					promotionProgramMgr.updateMD5ValidCode(program, getLogInfoVO());
					if (mapping.getSaleGroup() != null) {
						LogInfoVO logInfoVO = getLogInfoVO();
						try {
							promotionProgramMgr.updateGroupLevelOrderNumber(program.getId(), mapping.getSaleGroup().getId(), logInfoVO);
						} catch (Exception e1) {
							LogUtility.logError(e1, e1.getMessage());
						}
					}
				}
			}
		} catch (IllegalArgumentException ie) {
			String msg = ie.getMessage();
			if (msg != null) {
				if (msg.contains(PromotionProgramMgr.DUPLICATE_PRODUCT_IN_PRODUCT_GROUPS)) {
					int idx = msg.indexOf(PromotionProgramMgr.DUPLICATE_PRODUCT_IN_PRODUCT_GROUPS);
					if (idx > -1) {
						String err = msg.substring(idx);
						err = err.replace(PromotionProgramMgr.DUPLICATE_PRODUCT_IN_PRODUCT_GROUPS, "");
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("promotion.catalog.import.duplicate.product.in.product.groups", err));
						return SUCCESS;
					}
				} else if (msg.contains(PromotionProgramMgr.DUPLICATE_LEVELS)) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("promotion.catalog.import.duplicate.level"));
					return SUCCESS;
				}
			}
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		} catch (Exception e) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	public String newCopyLevel() {
		resetToken(result);
		try {
			listNewMapping = promotionProgramMgr.newCopyLevel(mappingId, copyNum, getLogInfoVO());
			if (mappingId != null) {
				GroupMapping mapping = promotionProgramMgr.getGroupMappingById(mappingId);
				if (mapping != null && mapping.getPromotionGroup() != null && mapping.getPromotionGroup().getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(mapping.getPromotionGroup().getPromotionProgram(), getLogInfoVO());
				}
			}
			result.put("list", listNewMapping);
		} catch (Exception e) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			LogUtility.logError(e, "PromotionCatalogAction.newCopyLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String newDeleteLevel() {
		resetToken(result);
		try {
			promotionProgramMgr.newDeleteLevel(mappingId, levelMuaId, levelKMId, getLogInfoVO());
			if (mappingId != null) {
				GroupMapping mapping = promotionProgramMgr.getGroupMappingById(mappingId);
				if (mapping != null && mapping.getPromotionGroup() != null && mapping.getPromotionGroup().getPromotionProgram() != null) {
					promotionProgramMgr.updateMD5ValidCode(mapping.getPromotionGroup().getPromotionProgram(), getLogInfoVO());
				}
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			LogUtility.logError(e, "PromotionCatalogAction.newDeleteLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String newDeleteSubLevel() {
		resetToken(result);
		try {
			promotionProgramMgr.newDeleteSubLevel(levelId, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			LogUtility.logError(e, "PromotionCatalogAction.newDeleteSubLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String newDetailDetailLevel() {
		resetToken(result);
		try {
			promotionProgramMgr.deleteLevelDetail(levelDetailId, getLogInfoVO());
		} catch (Exception e) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			LogUtility.logError(e, "PromotionCatalogAction.newDetailDetailLevel - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Xem thong tin don vi CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 14, 2014
	 */
	public String viewDetailShop() throws Exception {
		return SUCCESS;
	}

	/**
	 * Tim kiem don vi thuoc CTMK
	 * 
	 * @author lacnv1
	 * @since Aug 14, 2014
	 * 
	 * @author hunglm16
	 * @since March 23,2015
	 */
	public String searchShopOfPromotion() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			return JSON;
		}
		try {
			Long shId = currentUser.getShopRoot().getShopId();
			List<PromotionShopVO> lstTemp = promotionProgramMgr.getShopTreeInPromotionProgram(shId, promotionId, code, name, quantity);
//			PromotionShopMapFilter filter = new PromotionShopMapFilter();
//			filter.setPromotionId(promotionId);
//			filter.setShopCode(code);
//			filter.setShopName(name);
//			filter.setQuantity(quantity);
//			filter.setShopRootId(currentUser.getShopRoot().getShopId());
//			filter.setIsLevel(currentUser.getShopRoot().getIsLevel());
//			List<PromotionShopVO> lst = promotionProgramMgr.searchShopTreeInPromotionProgramImpr(filter);
			List<TreeGridNode<PromotionShopVO>> tree = new ArrayList<TreeGridNode<PromotionShopVO>>();
			if (lstTemp == null || lstTemp.size() == 0) {
				result.put("rows", tree);
				return JSON;
			}
			
			//Filter cac shop trung
			ArrayList<PromotionShopVO> lst = new ArrayList<>();
			for(PromotionShopVO shopVO : lstTemp) {
				boolean isExist = false;
				
				for(PromotionShopVO shopAdd : lst) {
					if(shopVO.getId().equals(shopAdd.getId())) {
						isExist = true;
					}
				}
				
				if(!isExist) {
					lst.add(shopVO);
				}
			}
			// Tao cay
			int i = 0, sz = lst.size();
			PromotionShopVO vo = null;
			boolean flag = false;
			for (i = 0; i < sz; i++) {
				vo = lst.get(i);
				if (shId.equals(vo.getId())) {
					i++;
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.put("rows", tree);
				return JSON;
			}
			//PromotionShopVO vo = lst.get(0);
			TreeGridNode<PromotionShopVO> node = new TreeGridNode<PromotionShopVO>();
			node.setNodeId(vo.getId().toString());
			node.setAttr(vo);
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
			node.setText(vo.getShopCode() + " - " + vo.getShopName());
			List<TreeGridNode<PromotionShopVO>> chidren = new ArrayList<TreeGridNode<PromotionShopVO>>();
			node.setChildren(chidren);
			tree.add(node);

			TreeGridNode<PromotionShopVO> tmp = null;
			TreeGridNode<PromotionShopVO> tmp2 = null;
			for (; i < sz; i++) {
				vo = lst.get(i);

				if (vo.getParentId() == null) {
					continue;
				}

				tmp2 = getNodeFromTree(tree, vo.getParentId().toString());
				if (tmp2 != null) {
					tmp = new TreeGridNode<PromotionShopVO>();
					tmp.setNodeId(vo.getId().toString());
					tmp.setAttr(vo);
					if (0 == vo.getIsNPP()) {
						tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					} else {
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

					if (tmp2.getChildren() == null) {
						tmp2.setChildren(new ArrayList<TreeGridNode<PromotionShopVO>>());
					}
					tmp2.getChildren().add(tmp);
				}
			}

			result.put("rows", tree);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.searchShopOfPromotion - " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay node trong cay
	 * 
	 * @author lacnv1
	 * @since Aug 15, 2014
	 */
	private <T> TreeGridNode<T> getNodeFromTree(List<TreeGridNode<T>> treeTmp, String nodeId) throws Exception {
		if (treeTmp == null) {
			return null;
		}
		TreeGridNode<T> node = null;
		TreeGridNode<T> tmp = null;
		for (int i = 0, sz = treeTmp.size(); i < sz; i++) {
			node = treeTmp.get(i);
			if (node.getNodeId().equals(nodeId)) {
				return node;
			}
			tmp = getNodeFromTree(node.getChildren(), nodeId);
			if (tmp != null) {
				return tmp;
			}
		}
		return null;
	}

	/**
	 * Xem thong thuoc tinh KH CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 16, 2014
	 */
	public String viewCustomerAttribute() throws Exception {
		if (promotionId != null && promotionId != 0) {
			try {
				lstPromotionCustAttrVO = promotionProgramMgr.getListPromotionCustAttrVOCanBeSet(null, promotionId);
				//listPromotionCustAttrVOAlreadySet = promotionProgramMgr.getListPromotionCustAttrVOAlreadySet(null, promotionId);
			} catch (Exception ex) {
				LogUtility.logError(ex, "PromotionCatalogAction.viewCustomerAttribute - " + ex.getMessage());
				isError = true;
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem khach hang thuoc CTMK
	 * 
	 * @author lacnv1
	 * @since Aug 15, 2014
	 */
	public String searchCustomerOfPromotion() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			result.put("rows", new ArrayList<PromotionCustomerVO>());
			result.put("total", 0);
			return JSON;
		}
		try {
			PromotionCustomerFilter filter = new PromotionCustomerFilter();
			filter.setPromotionId(promotionId);
			filter.setCode(code);
			filter.setName(name);
			filter.setAddress(address);
			filter.setIsCustomerOnly(false);
			if (shopId == null || shopId == 0) {
				filter.setStrListShopId(getStrListShopId());
			} else {
				filter.setShopId(shopId);
			}
			ObjectVO<PromotionCustomerVO> obj = promotionProgramMgr.getCustomerInPromotionProgram(filter);
			List<PromotionCustomerVO> lst = obj.getLstObject();

			List<TreeGridNode<PromotionCustomerVO>> tree = new ArrayList<TreeGridNode<PromotionCustomerVO>>();
			if (lst == null || lst.size() == 0) {
				result.put("rows", tree);
				return JSON;
			}

			// Tao cay
			int i = 0, sz = lst.size();
			PromotionCustomerVO vo = null;
			Long shId = currentUser.getShopRoot().getShopId();
			for (i = 0; i < sz; i++) {
				vo = lst.get(i);
				if (vo.getIsCustomer() == 0 && shId.equals(vo.getId())) {
					i++;
					break;
				}
			}
			//PromotionStaffVO vo = lst.get(0);
			TreeGridNode<PromotionCustomerVO> node = new TreeGridNode<PromotionCustomerVO>();
			node.setNodeId("sh" + vo.getId());
			node.setAttr(vo);
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
			node.setText(vo.getCustomerCode() + " - " + vo.getCustomerName());
			List<TreeGridNode<PromotionCustomerVO>> chidren = new ArrayList<TreeGridNode<PromotionCustomerVO>>();
			node.setChildren(chidren);
			tree.add(node);

			TreeGridNode<PromotionCustomerVO> tmp = null;
			TreeGridNode<PromotionCustomerVO> tmp2 = null;
			for (; i < sz; i++) {
				vo = lst.get(i);

				tmp2 = getNodeFromTree(tree, "sh" + vo.getParentId());
				if (tmp2 != null) {
					tmp = new TreeGridNode<PromotionCustomerVO>();
					tmp.setAttr(vo);
					if (0 == vo.getIsCustomer()) {
						tmp.setNodeId("sh" + vo.getId());
						tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					} else {
						tmp.setNodeId("st" + vo.getId());
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getCustomerCode() + " - " + vo.getCustomerName());

					if (tmp2.getChildren() == null) {
						tmp2.setChildren(new ArrayList<TreeGridNode<PromotionCustomerVO>>());
					}
					tmp2.getChildren().add(tmp);
				}
			}

			result.put("rows", tree);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.searchCustomerOfPromotion - " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem don vi thuoc CTMK
	 * 
	 * @author lacnv1
	 * @since Aug 14, 2014
	 */
	public String searchSalerOfPromotion() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			return JSON;
		}
		try {
			List<PromotionStaffVO> lst = promotionProgramMgr.getSalerInPromotionProgram(promotionId, code, name, quantity);

			List<TreeGridNode<PromotionStaffVO>> tree = new ArrayList<TreeGridNode<PromotionStaffVO>>();
			if (lst == null || lst.size() == 0) {
				result.put("rows", tree);
				return JSON;
			}

			// Tao cay
			int i = 0, sz = lst.size();
			PromotionStaffVO vo = null;
			Long shId = currentUser.getShopRoot().getShopId();
			for (i = 0; i < sz; i++) {
				vo = lst.get(i);
				if (vo.getIsSaler() == 0 && shId.equals(vo.getId())) {
					i++;
					break;
				}
			}
			//PromotionStaffVO vo = lst.get(0);
			TreeGridNode<PromotionStaffVO> node = new TreeGridNode<PromotionStaffVO>();
			node.setNodeId("sh" + vo.getId());
			node.setAttr(vo);
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
			node.setText(vo.getCode() + " - " + vo.getName());
			List<TreeGridNode<PromotionStaffVO>> chidren = new ArrayList<TreeGridNode<PromotionStaffVO>>();
			node.setChildren(chidren);
			tree.add(node);

			TreeGridNode<PromotionStaffVO> tmp = null;
			TreeGridNode<PromotionStaffVO> tmp2 = null;
			for (; i < sz; i++) {
				vo = lst.get(i);

				tmp2 = getNodeFromTree(tree, "sh" + vo.getParentId());
				if (tmp2 != null) {
					tmp = new TreeGridNode<PromotionStaffVO>();
					tmp.setAttr(vo);
					if (0 == vo.getIsSaler()) {
						tmp.setNodeId("sh" + vo.getId());
						tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					} else {
						tmp.setNodeId("st" + vo.getId());
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getCode() + " - " + vo.getName());

					if (tmp2.getChildren() == null) {
						tmp2.setChildren(new ArrayList<TreeGridNode<PromotionStaffVO>>());
					}
					tmp2.getChildren().add(tmp);
				}
			}

			result.put("rows", tree);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.searchSalerOfPromotion - " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem KH them vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 18, 2014
	 */
	public String searchCustomerOnDlg() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			result.put("rows", new ArrayList<PromotionCustomerVO>());
			result.put("total", 0);
			return JSON;
		}
		try {
			KPaging<PromotionCustomerVO> paging = new KPaging<PromotionCustomerVO>();
			paging.setPage(page - 1);
			paging.setPageSize(max);
			PromotionCustomerFilter filter = new PromotionCustomerFilter();
			filter.setkPaging(paging);
			filter.setPromotionId(promotionId);
			filter.setCode(code);
			filter.setName(name);
			filter.setAddress(address);
			if (shopId == null || shopId == 0) {
				filter.setStrListShopId(getStrListShopId());
			} else {
				filter.setShopId(shopId);
			}
			ObjectVO<PromotionCustomerVO> obj = promotionProgramMgr.searchCustomerForPromotionProgram(filter);;
			if (obj == null) {
				result.put("rows", new ArrayList<PromotionCustomerVO>());
				result.put("total", 0);
				return JSON;
			}

			result.put("rows", obj.getLstObject());
			result.put("total", obj.getkPaging().getTotalRows());
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.searchCustomerOfPromotion - " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Them KH vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String addPromotionCustomer() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || lstId == null || lstId.size() == 0) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			if (shopId == null || shopId <= 0) {
				shopId = getCurrentShop().getId();
			}

			List<PromotionCustomerVO> lstTmp = promotionProgramMgr.getListCustomerInPromotion(promotionId, lstId);
			if (lstTmp != null && lstTmp.size() > 0) {
				String msg = "";
				for (PromotionCustomerVO vo : lstTmp) {
					msg += (", " + vo.getCustomerCode());
				}
				msg = msg.replaceFirst(", ", "");
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.customer.map.exists", msg));
				return JSON;
			}
			lstTmp = null;
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}
			List<PromotionShopMap> promotionShopMaps = promotionProgramMgr.getPromotionShopMapByCustomer(lstId, promotionId);
			
			List<PromotionCustomerMap> lst = new ArrayList<PromotionCustomerMap>();
			
			PromotionCustomerMap pcm = null;
			Customer cust = null;
			Date now = DateUtil.now();
			for (Long idt : lstId) {
				cust = customerMgr.getCustomerById(idt);
				if (cust == null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "KH"));
					return JSON;
				}
				if (!ActiveType.RUNNING.equals(cust.getStatus())) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, cust.getShortCode()));
					return JSON;
				}
				for (PromotionShopMap psm : promotionShopMaps) {
					if (psm.getShop().getId().equals(cust.getShop().getId())) {
						List<PromotionCustomerMap> list = promotionProgramMgr.getListPromotionCustomerMapEntity(cust.getId(), psm);
						if (list != null && list.size() > 0) {
							for (PromotionCustomerMap customerMap : list) {
								String customerCode = customerMap.getCustomer().getCustomerCode();
								StringBuilder sb = new StringBuilder("Khách hàng có mã ");
								sb.append(customerCode);
								sb.append(" có ");
								if (quantity != null && customerMap.getQuantityReceivedTotal() != null && quantity.compareTo(customerMap.getQuantityReceivedTotal()) < 0) {
									sb.append("số suất phân bổ không được bé hơn số suất đã nhận.");
									result.put("errMsg", sb.toString());
									return JSON;
								} else if (amount != null && customerMap.getAmountReceivedTotal() != null && amount.compareTo(customerMap.getAmountReceivedTotal()) < 0) {
									sb.append("số tiền phân bổ không được bé hơn số tiền đã nhận.");
									result.put("errMsg", sb.toString());
									return JSON;
								} else if (number != null && customerMap.getNumReceivedTotal() != null && number.compareTo(customerMap.getNumReceivedTotal()) < 0) {
									sb.append("số lượng phân bổ không được bé hơn số lượng đã nhận.");
									result.put("errMsg", sb.toString());
									return JSON;
								}
								pcm.setQuantityMax(quantity);
								pcm.setAmountMax(amount);
								pcm.setNumMax(number);
								lst.add(customerMap);
							}
						} else {
							pcm = new PromotionCustomerMap();
							pcm.setPromotionShopMap(psm);
							pcm.setShop(psm.getShop());
							pcm.setCustomer(cust);
							pcm.setQuantityMax(quantity);
							pcm.setAmountMax(amount);
							pcm.setNumMax(number);
							pcm.setStatus(ActiveType.RUNNING);
							pcm.setCreateDate(now);
							pcm.setCreateUser(staff.getStaffCode());
							lst.add(pcm);
						}
					}
				}
			}
			promotionProgramMgr.createListPromotionCustomerMapEx(promotionShopMaps, lst, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.addCustomer - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Cap nhat so suat KH
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String updateCustomerQuantity() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || id == null || id < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}

			PromotionCustomerMap pcm = promotionProgramMgr.getPromotionCustomerMapById(id);
			if (pcm == null || !ActiveType.RUNNING.equals(pcm.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.customer.map.not.exists"));
				return JSON;
			}
			if(quantity != null && pcm.getQuantityReceivedTotal() != null && quantity.compareTo(pcm.getQuantityReceivedTotal()) < 0 ) {
				result.put("errMsg", "Số suất phân bổ không được bé hơn số suất đã nhận.");
				return JSON;
			} else if(amount != null && pcm.getAmountReceivedTotal() != null && amount.compareTo(pcm.getAmountReceivedTotal()) < 0){
				result.put("errMsg", "Số tiền phân bổ không được bé hơn số tiền đã nhận.");
				return JSON;
			} else if(number != null && pcm.getNumReceivedTotal() != null && number.compareTo(pcm.getNumReceivedTotal()) < 0) {
				result.put("errMsg", "Số lượng phân bổ không được bé hơn số lượng đã nhận.");
				return JSON;
			} else {
				pcm.setQuantityMax(quantity);
				pcm.setAmountMax(amount);
				pcm.setNumMax(number);
				promotionProgramMgr.updatePromotionCustomerMap(pcm, getLogInfoVO());
				result.put(ERROR, false);
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.updateCustomerQuantity - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xoa KH
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String deleteCustomer() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || id == null || id < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}

			PromotionCustomerMap pcm = promotionProgramMgr.getPromotionCustomerMapById(id);
			if (pcm == null || !ActiveType.RUNNING.equals(pcm.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.customer.map.not.exists"));
				return JSON;
			}
			pcm.setStatus(ActiveType.DELETED);
			promotionProgramMgr.updatePromotionCustomerMap(pcm, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.updateCustomerQuantity - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Cap nhat so suat don vi
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String updateShopQuantity() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || shopId == null || shopId < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}

			PromotionShopMap psm = promotionProgramMgr.getPromotionShopMap(shopId, promotionId);
			if (psm == null || !ActiveType.RUNNING.equals(psm.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.shop.not.exist"));
				return JSON;
			}
			if(quantity != null && psm.getQuantityReceivedTotal() != null && quantity.compareTo(psm.getQuantityReceivedTotal()) < 0 ) {
				result.put("errMsg", "Số suất phân bổ không được bé hơn số suất đã nhận.");
				return JSON;
			} else if(amount != null && psm.getAmountReceivedTotal() != null && amount.compareTo(psm.getAmountReceivedTotal()) < 0){
				result.put("errMsg", "Số tiền phân bổ không được bé hơn số tiền đã nhận.");
				return JSON;
			} else if(number != null && psm.getNumReceivedTotal() != null && number.compareTo(psm.getNumReceivedTotal()) < 0) {
				result.put("errMsg", "Số lượng phân bổ không được bé hơn số lượng đã nhận.");
				return JSON;
			} else {
				psm.setQuantityMax(quantity);
				psm.setIsQuantityMaxEdit(isEdited);
				psm.setAmountMax(amount);
				psm.setNumMax(number);
				promotionProgramMgr.updatePromotionShopMap(psm, getLogInfoVO());
				result.put(ERROR, false);
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.updateShopQuantity - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xoa don vi
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String deleteShop() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || shopId == null || shopId < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}

			Shop sh = shopMgr.getShopById(shopId);
			if (sh == null) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.focus.program.shop.code"));
				return JSON;
			}

			promotionProgramMgr.deletePromotionShopMap(promotionId, shopId, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.updateCustomerQuantity - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	private List<TreeGridNode<PromotionShopVO>> lstTree;

	/**
	 * Tim kiem don vi them vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 14, 2014
	 * 
	 * @modify hunglm16
	 * @since 26/11/2015
	 * @description Bo sung phan quyen don vi
	 */
	public String searchShopOnDlg() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			if (id != null) {
				shopId = id;
			}
			Shop sh = null;
			if (shopId == null || shopId == 0) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			sh = shopMgr.getShopById(shopId);
			List<PromotionShopVO> lst = promotionProgramMgr.searchShopForPromotionProgram(promotionId, shopId, code, name, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
			lstTree = new ArrayList<TreeGridNode<PromotionShopVO>>();
			if (id == null) {
				PromotionShopVO vo = null;
				int i = 0;
				int sz = lst.size();
				String state = null;

				if (StringUtil.isNullOrEmpty(code) && StringUtil.isNullOrEmpty(name)) {
					vo = new PromotionShopVO();
					vo.setId(sh.getId());
					vo.setShopCode(sh.getShopCode());
					vo.setShopName(sh.getShopName());
					/** author update nhutnn, since 23/04/2015 */
					PromotionShopMapFilter filter = new PromotionShopMapFilter();
					filter.setPromotionId(promotionId);
					filter.setShopRootId(shopId);
					filter.setStatus(ActiveType.RUNNING);
					List<PromotionShopMap> promotionShopMaps = promotionProgramMgr.getPromotionChildrenShopMap(filter);
					if (promotionShopMaps.isEmpty()) {
						vo.setIsExists(0);
					} else {
						vo.setIsExists(1);
					}
					if (ShopSpecificType.NPP.equals(sh.getType().getSpecificType())) {
						vo.setIsNPP(1);
					} else {
						vo.setIsNPP(0);
					}
					state = ConstantManager.JSTREE_STATE_CLOSE;
					i = 0;
				} else {
					vo = lst.get(0);
					state = ConstantManager.JSTREE_STATE_OPEN;
					i = 1;
				}

				TreeGridNode<PromotionShopVO> node = new TreeGridNode<PromotionShopVO>();
				node.setNodeId(vo.getId().toString());
				node.setAttr(vo);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setText(vo.getShopCode() + " - " + vo.getShopName());
				List<TreeGridNode<PromotionShopVO>> chidren = new ArrayList<TreeGridNode<PromotionShopVO>>();
				node.setChildren(chidren);
				lstTree.add(node);

				if (lst == null || lst.size() == 0) {
					vo.setIsExists(0);
					return JSON;
				}

				// Tao cay			
				TreeGridNode<PromotionShopVO> tmp = null;
				TreeGridNode<PromotionShopVO> tmp2 = null;
				for (; i < sz; i++) {
					vo = lst.get(i);

					if (vo.getParentId() == null) {
						continue;
					}

					tmp2 = getNodeFromTree(lstTree, vo.getParentId().toString());
					if (tmp2 != null) {
						tmp = new TreeGridNode<PromotionShopVO>();
						tmp.setNodeId(vo.getId().toString());
						tmp.setAttr(vo);
						if (0 == vo.getIsNPP()) {
							tmp.setState(state);
						} else {
							tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
						}
						tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

						if (tmp2.getChildren() == null) {
							tmp2.setChildren(new ArrayList<TreeGridNode<PromotionShopVO>>());
						}
						tmp2.getChildren().add(tmp);
					}
				}
			} else {
				// Tao cay			
				TreeGridNode<PromotionShopVO> tmp = null;
				PromotionShopVO vo = null;
				for (int i = 0, sz = lst.size(); i < sz; i++) {
					vo = lst.get(i);

					tmp = new TreeGridNode<PromotionShopVO>();
					tmp.setNodeId(vo.getId().toString());
					tmp.setAttr(vo);
					if (0 == vo.getIsNPP()) {
						tmp.setState(ConstantManager.JSTREE_STATE_CLOSE);
					} else {
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

					lstTree.add(tmp);
				}
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.searchShopOfPromotion - " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Them don vi vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 20, 2014
	 */
	public String addPromotionShop() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || lstQtt == null || lstQtt.size() == 0 || lstId == null || lstId.size() == 0 || lstId.size() != lstQtt.size()) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			// kiem tra don vi, don vi con da thuoc CTKM
			List<PromotionShopVO> lstTmp = promotionProgramMgr.getListShopInPromotion(promotionId, lstId, false);
			if (lstTmp != null && lstTmp.size() > 0) {
				String msg = "";
				for (PromotionShopVO vo : lstTmp) {
					msg += (", " + vo.getShopCode());
				}
				msg = msg.replaceFirst(", ", "");
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.shop.map.exists", msg));
				return JSON;
			}
			lstTmp = null;
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}

			List<PromotionShopMap> lst = new ArrayList<PromotionShopMap>();
			PromotionShopMap psm = null;
			Shop shT = null;
			Date now = DateUtil.now();
			Long idt = null;
			for (int i = 0, sz = lstId.size(); i < sz; i++) {
				idt = lstId.get(i);
				shT = shopMgr.getShopById(idt);
				if (shT == null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "DV"));
					return JSON;
				}
				if (!ActiveType.RUNNING.equals(shT.getStatus())) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, shT.getShopCode()));
					return JSON;
				}
				psm = new PromotionShopMap();
				psm.setPromotionProgram(pro);
				psm.setShop(shT);
				psm.setIsQuantityMaxEdit((lstEdit.get(i) != null && lstEdit.get(i)) ? 1 : 0);
				if (lstQtt.get(i) > ZEZO) {
					psm.setQuantityMax(lstQtt.get(i));
				}
				if (BigDecimal.ZERO.compareTo(lstAmt.get(i)) < 0) {
					psm.setAmountMax(lstAmt.get(i));
				}
				if (BigDecimal.ZERO.compareTo(lstNum.get(i)) < 0) {
					psm.setNumMax(lstNum.get(i));
				}
				psm.setStatus(ActiveType.RUNNING);
				psm.setCreateDate(now);
				psm.setCreateUser(staff.getStaffCode());
				psm.setFromDate(pro.getFromDate());
				psm.setToDate(pro.getToDate());

				lst.add(psm);
			}

			promotionProgramMgr.createListPromotionShopMap(lst, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.addCustomer - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xuat excel ds don vi thuoc CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 20, 2014
	 */
	public String exportPromotionShop() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			result.put(ERROR, false);
			result.put("hasData", false);
			return JSON;
		}
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			result.put(ERROR, true);
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			PromotionShopMapFilter filter = new PromotionShopMapFilter();
			filter.setProgramId(promotionId);
			filter.setStatus(ActiveType.RUNNING);
			filter.setParentShopId(currentUser.getShopRoot().getShopId());
			filter.setShopCode(code);
			filter.setShopName(name);
			filter.setQuantityMax(quantity);
			List<PromotionShopMapVO> lst = promotionProgramMgr.getPromotionShopMapVOByFilter(filter, null);
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
			StringBuilder sb = new StringBuilder(folder).append(ConstantManager.TEMPLATE_PROMOTION_SHOP_MAP_EXPORT).append(FileExtension.XLS.getValue());
			String templateFileName = sb.toString();
			templateFileName = templateFileName.replace('/', File.separatorChar);

			sb = new StringBuilder(ConstantManager.TEMPLATE_PROMOTION_SHOP_MAP_EXPORT).append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append(FileExtension.XLS.getValue());
			String outputName = sb.toString();
			sb = null;
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			Map<String, Object> beans = new HashMap<String, Object>();

			beans.put("hasData", 1);
			beans.put("lstShop", lst);

			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.exportPromotionShop - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Import don vi tu excel
	 * 
	 * @author lacnv1
	 * @since Aug 10, 2014
	 * 
	 * @modify hunglm16
	 * @since 14/09/2015
	 * @description Them So tien, So luong va Hotfix
	 */
	public String importPromotionShop() throws Exception {
		resetToken(result);
		isError = true;
		try {
			PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (promotionProgram == null) {
				isError = true;
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM");
				return SUCCESS;
			}
			if (!ActiveType.WAITING.equals(promotionProgram.getStatus())) {
				isError = true;
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect");
				return SUCCESS;
			}
			totalItem = 0;
			String message = "";
			lstView = new ArrayList<CellBean>();
			typeView = true;
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, 4);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				Date currentSysDate = commonMgr.getSysDate();
				PromotionShopMap promotionShopMap = null;
//				PromotionCustomerMap promotionCustomerMap = null;
				List<String> row = null;
				Shop shop = null;
				//List<String> parentShopCode = null;
				//Customer c = null;
				boolean isEdit;
//				boolean isCreateCustomer;
				PromotionShopMap psm = null;
				LogInfoVO logInfo = getLogInfoVO();
				String msg = "";
				String value = "";
				for (int i = 0, size = lstData.size(); i < size; i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						boolean flagContinue = true;
						//Bo qua cac dong trong
						for (int j = 0; j < 4; j++) {
							if (!StringUtil.isNullOrEmpty(lstData.get(i).get(j))) {
								flagContinue = false;
							}
						}
						if (flagContinue) {
							continue;
						}
						message = "";
						value = "";
						totalItem++;
						row = lstData.get(i);
						isEdit = false;
//						isCreateCustomer = false;
						promotionShopMap = new PromotionShopMap();
						promotionShopMap.setPromotionProgram(promotionProgram);
						promotionShopMap.setFromDate(promotionProgram.getFromDate());
						promotionShopMap.setToDate(promotionProgram.getToDate());
						if (row.size() > 0) {
							value = row.get(0);
							if (!StringUtil.isNullOrEmpty(value)) {
								shop = shopMgr.getShopByCode(value);
								if (shop == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.shop.code");
								} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
								} else if (!checkShopPermission(shop.getShopCode())) {
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_SHOP, shop.getShopCode(), currentUser.getUserName()) + "\n";
								} else if (shop.getType() == null || shop.getType().getSpecificType() == null || !ShopSpecificType.NPP.getValue().equals(shop.getType().getSpecificType().getValue())) {
									//Don vi khong phai la NPP
									message += R.getResource("common.cms.shop.islevel5.undefined");
								}
							} else {
								message += R.getResource("common.missing.not.in.system.p",R.getResource("catalog.focus.program.shop.code")) + "\n";
							}
							if (StringUtil.isNullOrEmpty(message)) {
								boolean flag1 = promotionProgramMgr.isExistChildJoinProgram(shop.getShopCode(), promotionId);
								if (flag1) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.program.child.shop.is.exists") + "\n";
								}
							}

							if (shop != null) {
								psm = promotionProgramMgr.getPromotionShopMap(shop.getId(), promotionProgram.getId());
							}

							if (StringUtil.isNullOrEmpty(message)) {
								promotionShopMap.setShop(shop);
								if (psm != null) {
									isEdit = true;
									promotionShopMap = psm;
								}
							}
						}
						// So suat NPP
						value = row.get(1);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.suatNPP.code"));
							} else {
								if (Integer.valueOf(value) > 0) {
									promotionShopMap.setQuantityMax(Integer.valueOf(value));
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.suatNPP.code"));
								}
							}
						} else if (promotionShopMap != null) {
							promotionShopMap.setQuantityMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}

						//So tien
						value = row.get(2);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.promotion.import.khtg.ctkm.clmn.amount"));
							} else {
								if (Integer.valueOf(value) > 0) {
									promotionShopMap.setAmountMax(BigDecimal.valueOf(Long.valueOf(value)));
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.khtg.ctkm.clmn.amount"));
								}
							}
						} else if (promotionShopMap != null) {
							promotionShopMap.setAmountMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						//So luong
						value = row.get(3);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.promotion.import.khtg.ctkm.clmn.quantity"));
							} else {
								if (Integer.valueOf(value) > 0) {
									promotionShopMap.setNumMax(BigDecimal.valueOf(Long.valueOf(value)));
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.khtg.ctkm.clmn.quantity"));
								}
							}
						} else if (promotionShopMap != null) {
							promotionShopMap.setNumMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						if (StringUtil.isNullOrEmpty(message)) {
							try {
								if (isEdit) {
									promotionShopMap.setUpdateDate(currentSysDate);
									promotionShopMap.setCreateUser(currentUser.getUserName());
									promotionProgramMgr.updatePromotionShopMap(promotionShopMap, logInfo);
//									if (promotionCustomerMap != null && promotionCustomerMap.getCustomer() != null) {
//										promotionCustomerMap.setShop(promotionShopMap.getShop());
//										if (isCreateCustomer) {
//											promotionCustomerMap.setCreateDate(currentSysDate);
//											promotionCustomerMap.setCreateUser(currentUser.getUserName());
//											promotionProgramMgr.createPromotionCustomerMap(promotionCustomerMap, logInfo);
//										} else {
//											promotionCustomerMap.setUpdateDate(currentSysDate);
//											promotionCustomerMap.setUpdateUser(currentUser.getUserName());
//											promotionProgramMgr.updatePromotionCustomerMap(promotionCustomerMap, logInfo);
//										}
//									}
								} else {
									if (ActiveType.WAITING.equals(promotionProgram.getStatus())) {
										promotionShopMap.setCreateDate(currentSysDate);
										promotionShopMap.setCreateUser(currentUser.getUserName());
										PromotionShopMap s = promotionProgramMgr.createPromotionShopMap(promotionShopMap, logInfo);
//										if (promotionCustomerMap != null && promotionCustomerMap.getCustomer() != null) {
//											promotionCustomerMap.setPromotionShopMap(s);
//											promotionCustomerMap.setCreateDate(currentSysDate);
//											promotionCustomerMap.setCreateUser(currentUser.getUserName());
//											promotionCustomerMap.setShop(promotionShopMap.getShop());
//											promotionProgramMgr.createPromotionCustomerMap(promotionCustomerMap, logInfo);
//										}
									}
								}
								isError = false;
							} catch (BusinessException be) {
								LogUtility.logError(be, be.getMessage());
								errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
						typeView = false;
					}
				}
				// Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_PP_SHOP_FAIL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				isError = false;
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.importPromotionShop - " + ex.getMessage());
			isError = true;
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}

	/**
	 * Lay danh sach thuoc tinh da co trong CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 22, 2014
	 */
	public String loadAppliedAttributes() throws Exception {
		if (promotionId != null && promotionId > 0) {
			try {
				CustomerAttributeMgr customerAttributeMgr = (CustomerAttributeMgr) context.getBean("customerAttributeMgr");
				List<PromotionCustAttrVO> lst = promotionProgramMgr.getListPromotionCustAttrVOAlreadySet(null, promotionId);
				CustomerAttribute attribute = null;
				AttributeColumnType attColType = null;
				PromotionCustAttVO2 voTmp = null;
				List<PromotionCustAttVO2> lstTmp = promotionProgramMgr.getListPromotionCustAttVOValue(promotionId);
				List<PromotionCustAttVO2> lstTmpDetail = promotionProgramMgr.getListPromotionCustAttVOValueDetail(promotionId);
				ArrayList<Object> lstData = null;
				List<AttributeDetailVO> lstAttDetail = null;
				AttributeDetailVO voDetail = null;
				int j, sz;
				int k, szk;
				if (lst != null && lst.size() > 0) {
					for (PromotionCustAttrVO vo : lst) {
						if (vo.getObjectType() == AUTO_ATTRIBUTE) {
							attribute = customerAttributeMgr.getCustomerAttributeById(vo.getObjectId());
							attColType = attribute.getValueType();
							vo.setValueType(attColType);//setValueType
							if (attColType == AttributeColumnType.CHARACTER || attColType == AttributeColumnType.NUMBER || attColType == AttributeColumnType.DATE_TIME) {
								//Value cua thuoc tinh dong type (1,2,3)
								if (lstTmp != null && lstTmp.size() > 0) {
									if (attColType == AttributeColumnType.CHARACTER) {
										for (j = 0, sz = lstTmp.size(); j < sz; j++) {
											voTmp = lstTmp.get(j);
											if (voTmp.getAttributeId().equals(vo.getObjectId())) {
												lstData = new ArrayList<Object>();
												lstData.add(voTmp.getFromValue());
												vo.setListData(lstData);//setListData
											}
										}
									} else if (attColType == AttributeColumnType.NUMBER) {
										//										valueType=2;
										for (j = 0, sz = lstTmp.size(); j < sz; j++) {
											voTmp = lstTmp.get(j);
											if (voTmp.getAttributeId().equals(vo.getObjectId())) {
												lstData = new ArrayList<Object>();
												lstData.add(voTmp.getFromValue());
												lstData.add(voTmp.getToValue());
												vo.setListData(lstData);//setListData
											}
										}
									} else if (attColType == AttributeColumnType.DATE_TIME) {
										//										valueType=3;
										for (j = 0, sz = lstTmp.size(); j < sz; j++) {
											voTmp = lstTmp.get(j);
											if (voTmp.getAttributeId().equals(vo.getObjectId())) {
												lstData = new ArrayList<Object>();
												lstData.add(DateUtil.convertFormatStrFromAtt(voTmp.getFromValue()));
												lstData.add(DateUtil.convertFormatStrFromAtt(voTmp.getToValue()));
												vo.setListData(lstData);//setListData
											}
										}
									}
								}
							} else if (attColType == AttributeColumnType.CHOICE || attColType == AttributeColumnType.MULTI_CHOICE) {
								//Value cua thuoc tinh dong type (4,5)
								if (lstTmpDetail != null && lstTmpDetail.size() > 0) {
									lstAttDetail = null;
									for (j = 0, sz = lstTmpDetail.size(); j < sz; j++) {
										voTmp = lstTmpDetail.get(j);
										if (voTmp.getAttributeId().equals(attribute.getId())) {
											if (lstAttDetail == null) {
												lstAttDetail = promotionProgramMgr.getListPromotionCustAttVOCanBeSet(attribute.getId());
											}
											if (lstAttDetail != null && lstAttDetail.size() > 0) {
												for (k = 0, szk = lstAttDetail.size(); k < szk; k++) {
													voDetail = lstAttDetail.get(k);
													if (voDetail.getEnumId().equals(voTmp.getAttributeEnumId())) {
														voDetail.setChecked(true);
														break;
													}
												}
											}
										}
									}
									vo.setListData(lstAttDetail);//setListData
								}
							}
						} else if (vo.getObjectType() == CUSTOMER_TYPE) {
							List<ChannelTypeVO> listChannelTypeVO = promotionProgramMgr.getListChannelTypeVO();
							List<ChannelTypeVO> listSelectedChannelTypeVO = promotionProgramMgr.getListChannelTypeVOAlreadySet(null, promotionId);
							if (listChannelTypeVO != null && listChannelTypeVO.size() > 0 && listSelectedChannelTypeVO != null && listSelectedChannelTypeVO.size() > 0) {
								for (ChannelTypeVO channelTypeVO : listChannelTypeVO) {
									for (ChannelTypeVO channelTypeVO1 : listSelectedChannelTypeVO) {
										if (channelTypeVO1.getIdChannelType().equals(channelTypeVO.getIdChannelType())) {
											channelTypeVO.setChecked(true);
											break;
										}
									}
								}
							}
							vo.setListData(listChannelTypeVO);//setListData
						} else if (vo.getObjectType() == SALE_LEVEL) {
							List<SaleCatLevelVO> listSelectedSaleCatLevelVO = promotionProgramMgr.getListSaleCatLevelVOByIdProAlreadySetSO(null, promotionId);
							vo.setListData(listSelectedSaleCatLevelVO);
							List<ProductInfoVO> listProductInfoVO = promotionProgramMgr.getListProductInfoVO();
							if (listProductInfoVO != null && listProductInfoVO.size() > 0) {
								for (ProductInfoVO productInfoVO : listProductInfoVO) {
									List<SaleCatLevelVO> listSaleCatLevelVO = promotionProgramMgr.getListSaleCatLevelVOByIdPro(productInfoVO.getIdProductInfoVO());
									productInfoVO.setListSaleCatLevelVO(listSaleCatLevelVO);
								}
								vo.setListProductInfoVO(listProductInfoVO);
							} else {
								vo.setListProductInfoVO(new ArrayList<ProductInfoVO>());
							}
						}
					}

					result.put("list", lst);//Neu ko co list de put thi nho new 1 arrayList roi put a.
				}
			} catch (Exception ex) {
				LogUtility.logError(ex, "PromotionCatalogAction.loadAppliedAttributes - " + ex.getMessage());
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
				return JSON;
			}
		}
		return JSON;
	}

	/**
	 * Lay danh sach gia tri cho thuoc tinh KH
	 * 
	 * @author lacnv1
	 * @since Aug 22, 2014
	 */
	public String getAllDataForAttributes() throws Exception {
		try {
			if (lstObjectType != null && lstObjectType.size() > 0 && lstId != null && lstId.size() > 0 && lstObjectType.size() == lstId.size()) { //lstId = listObjectId
				CustomerAttributeMgr customerAttributeMgr = (CustomerAttributeMgr) context.getBean("customerAttributeMgr");
				List<PromotionCustAttrVO> lst = new ArrayList<PromotionCustAttrVO>();
				PromotionCustAttrVO vo = null;
				Integer objectType = null;
				Long objectId = null;
				CustomerAttribute attribute = null;
				AttributeColumnType attColType = null;
				List<AttributeDetailVO> lstAttDetail = null;
				for (int i = 0; i < lstObjectType.size(); i++) {
					objectType = lstObjectType.get(i);
					objectId = lstId.get(i);
					if (objectType != null) {
						vo = new PromotionCustAttrVO();
						vo.setObjectType(objectType);
						vo.setObjectId(objectId);
						if (objectType.equals(AUTO_ATTRIBUTE)) {
							attribute = customerAttributeMgr.getCustomerAttributeById(objectId);
							if (attribute != null) {
								attColType = attribute.getValueType();
								vo.setValueType(attColType);// setValueType
								vo.setName(attribute.getName());
								// chi set du lieu cho kieu dropdownlist:
								if (attColType == AttributeColumnType.CHOICE || attColType == AttributeColumnType.MULTI_CHOICE) {
									lstAttDetail = promotionProgramMgr.getListPromotionCustAttVOCanBeSet(attribute.getId());
									vo.setListData(lstAttDetail);// setListData
								}
							}
						} else if (objectType.equals(CUSTOMER_TYPE)) {
							List<ChannelTypeVO> listChannelTypeVO = null;
							listChannelTypeVO = promotionProgramMgr.getListChannelTypeVO();
							vo.setListData(listChannelTypeVO);//setListData
						} else if (objectType.equals(SALE_LEVEL)) {
							List<ProductInfoVO> listProductInfoVO = promotionProgramMgr.getListProductInfoVO();
							if (listProductInfoVO != null && listProductInfoVO.size() > 0) {
								for (ProductInfoVO productInfoVO : listProductInfoVO) {
									List<SaleCatLevelVO> listSaleCatLevelVO = promotionProgramMgr.getListSaleCatLevelVOByIdPro(productInfoVO.getIdProductInfoVO());
									productInfoVO.setListSaleCatLevelVO(listSaleCatLevelVO);
								}
								vo.setListProductInfoVO(listProductInfoVO);
							}
						}

						lst.add(vo);
					} else {
						return JSON;
					}
				}
				result.put("list", lst);
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.getAllDataForAttributes - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * Luu thuoc tinh KH tham gia CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 22, 2014
	 */
	public String savePromotionCustAtt() throws Exception {
		resetToken(result);
		boolean error = true;
		List<PromotionCustAttUpdateVO> listPromotionCustAttUpdateVO = new ArrayList<PromotionCustAttUpdateVO>();
		try {
			PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (promotionProgram == null) {
				result.put(ERROR, error);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));//Loi ko dung id promotionProgram
				return JSON;
			}
			if (lstId != null && lstId.size() > 0 && lstAttDataInField != null && lstAttDataInField.size() > 0) {
				Long attributeI = null;
				PromotionCustAttr promotionCustAttr = null;
				PromotionCustAttr pca = null;
				CustomerAttribute attribute = null;
				CustomerAttributeMgr customerAttributeMgr = (CustomerAttributeMgr) context.getBean("customerAttributeMgr");
				String dataI = null;
				AttributeColumnType attColType = null;
				PromotionCustAttUpdateVO promotionCustAttUpdateVO = null;
				for (int i = 0, sz = lstId.size(); i < sz; i++) {
					if (lstId.get(i) != null && lstId.get(i).equals(-2l)) {//-2 la customerType
						if (lstCustomerType != null && lstCustomerType.size() > 0) {
							pca = promotionProgramMgr.getPromotionCustAttrByPromotion(promotionId, 2, null);
							if (pca != null) {
								promotionCustAttr = pca;
								promotionCustAttr.setUpdateUser(currentUser.getUserName());
							} else {
								promotionCustAttr = new PromotionCustAttr();
								promotionCustAttr.setCreateUser(currentUser.getUserName());
							}
							promotionCustAttr.setPromotionProgram(promotionProgram);
							promotionCustAttr.setObjectType(2);
							promotionCustAttr.setSeq(i);//set seq theo chi so trong List.
							List<PromotionCustAttrDetail> listPromotionCustAttrDetail = new ArrayList<PromotionCustAttrDetail>();
							if (pca != null && pca.getId() != null) {
								PromotionCustAttrDetail tmpPCAD;
								Long promotionCustAttrId = pca.getId();
								for (Long chanelTypeId : lstCustomerType) {
									PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
									promotionCustAttrDetail.setObjectType(2);
									promotionCustAttrDetail.setObjectId(chanelTypeId);
									tmpPCAD = promotionProgramMgr.getPromotionCustAttrDetail(promotionCustAttrId, 2L, chanelTypeId);
									if (tmpPCAD != null) {
										promotionCustAttrDetail.setCreateUser(tmpPCAD.getCreateUser());
										promotionCustAttrDetail.setUpdateUser(currentUser.getUserName());
									} else {
										promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
									}
									tmpPCAD = null;
									listPromotionCustAttrDetail.add(promotionCustAttrDetail);
								}
							} else {
								for (Long chanelTypeId : lstCustomerType) {
									PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
									promotionCustAttrDetail.setObjectType(2);
									promotionCustAttrDetail.setObjectId(chanelTypeId);
									promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
									listPromotionCustAttrDetail.add(promotionCustAttrDetail);
								}
							}
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(listPromotionCustAttrDetail);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						}
					} else if (lstId.get(i) != null && lstId.get(i).equals(-3l)) {//-3 la saleLevel
						if (lstSaleLevelCatId != null && lstSaleLevelCatId.size() > 0) {
							pca = promotionProgramMgr.getPromotionCustAttrByPromotion(promotionId, 3, null);
							if (pca != null) {
								promotionCustAttr = pca;
								promotionCustAttr.setUpdateUser(currentUser.getUserName());
							} else {
								promotionCustAttr = new PromotionCustAttr();
								promotionCustAttr.setCreateUser(currentUser.getUserName());
							}
							promotionCustAttr.setPromotionProgram(promotionProgram);
							promotionCustAttr.setObjectType(3);
							promotionCustAttr.setSeq(i);//set seq de load len cho dung thu tu
							List<PromotionCustAttrDetail> listPromotionCustAttrDetail = new ArrayList<PromotionCustAttrDetail>();
							if (pca != null && pca.getId() != null) {
								PromotionCustAttrDetail tmpPCAD;
								Long promotionCustAttrId = pca.getId();
								for (Long saleLevelCatId : lstSaleLevelCatId) {
									if (saleLevelCatId > -1L) {
										PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
										promotionCustAttrDetail.setObjectType(3);
										promotionCustAttrDetail.setObjectId(saleLevelCatId);
										tmpPCAD = promotionProgramMgr.getPromotionCustAttrDetail(promotionCustAttrId, 3L, saleLevelCatId);
										if (tmpPCAD != null) {
											promotionCustAttrDetail.setCreateUser(tmpPCAD.getCreateUser());
											promotionCustAttrDetail.setUpdateUser(currentUser.getUserName());
										} else {
											promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
										}
										tmpPCAD = null;
										listPromotionCustAttrDetail.add(promotionCustAttrDetail);
									}
								}
							} else {
								for (Long saleLevelCatId : lstSaleLevelCatId) {
									if (saleLevelCatId > -1L) {
										PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
										promotionCustAttrDetail.setObjectType(3);
										promotionCustAttrDetail.setObjectId(saleLevelCatId);
										promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
										listPromotionCustAttrDetail.add(promotionCustAttrDetail);
									}
								}
							}
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(listPromotionCustAttrDetail);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						}
					} else {
						attributeI = lstId.get(i);
						attribute = customerAttributeMgr.getCustomerAttributeById(attributeI);
						//set vao entity:
						pca = promotionProgramMgr.getPromotionCustAttrByPromotion(promotionId, 1, attributeI);
						if (pca != null) {
							promotionCustAttr = pca;
							promotionCustAttr.setUpdateUser(currentUser.getUserName());
						} else {
							promotionCustAttr = new PromotionCustAttr();
							promotionCustAttr.setCreateUser(currentUser.getUserName());
						}
						promotionCustAttr.setPromotionProgram(promotionProgram);
						promotionCustAttr.setObjectType(1);
						promotionCustAttr.setObjectId(attribute.getId());
						promotionCustAttr.setSeq(i);
						//
						dataI = lstAttDataInField.get(i);
						attColType = attribute.getValueType();
						if (attColType == AttributeColumnType.CHARACTER) {
							promotionCustAttr.setFromValue(dataI);
							// set vao VO to save:
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(null);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						} else if (attColType == AttributeColumnType.NUMBER) {
							//							if (arr != null && arr.length == 2) {
							//								promotionCustAttr.setFromValue(arr[0]);
							//								promotionCustAttr.setToValue(arr[1]);
							//						    }
							String[] arr = dataI.split(",");
							if (arr != null) {
								if (arr.length == 2) {
									promotionCustAttr.setFromValue(arr[0]);
									promotionCustAttr.setToValue(arr[1]);
								} else if (arr.length == 1) {
									promotionCustAttr.setFromValue(arr[0]);
									promotionCustAttr.setToValue("");
								}
							}
							// set vao VO to save:
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(null);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);

						} else if (attColType == AttributeColumnType.DATE_TIME) {
							String[] arr = dataI.split(",");
							if (arr != null) {
								if (arr.length == 2) {
									String fromValueTmp = DateUtil.convertFormatAttFromStr(arr[0]);
									String toValueTmp = DateUtil.convertFormatAttFromStr(arr[1]);
									promotionCustAttr.setFromValue(fromValueTmp);
									promotionCustAttr.setToValue(toValueTmp);
								} else if (arr.length == 1) {
									String fromValueTmp = DateUtil.convertFormatAttFromStr(arr[0]);
									promotionCustAttr.setFromValue(fromValueTmp);
									promotionCustAttr.setToValue("");
								}
							}
							// set vao VO to save:
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(null);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						} else if (attColType == AttributeColumnType.CHOICE || attColType == AttributeColumnType.MULTI_CHOICE) {
							List<PromotionCustAttrDetail> listPromotionCustAttrDetail = new ArrayList<PromotionCustAttrDetail>();
							String[] arr = dataI.split(",");
							if (pca != null && pca.getId() != null) {
								PromotionCustAttrDetail tmpPCAD;
								Long promotionCustAttrId = pca.getId();
								if (arr != null && arr.length > 0) {
									for (int k = 0, szk = arr.length; k < szk; k++) {
										PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
										promotionCustAttrDetail.setObjectType(1);
										promotionCustAttrDetail.setObjectId(Long.valueOf(arr[k]));
										tmpPCAD = promotionProgramMgr.getPromotionCustAttrDetail(promotionCustAttrId, 1L, Long.valueOf(arr[k]));
										if (tmpPCAD != null) {
											promotionCustAttrDetail.setCreateUser(tmpPCAD.getCreateUser());
											promotionCustAttrDetail.setUpdateUser(currentUser.getUserName());
										} else {
											promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
										}
										tmpPCAD = null;
										listPromotionCustAttrDetail.add(promotionCustAttrDetail);
									}
								}
							} else {
								if (arr != null && arr.length > 0) {
									for (int k = 0, szk = arr.length; k < szk; k++) {
										PromotionCustAttrDetail promotionCustAttrDetail = new PromotionCustAttrDetail();
										promotionCustAttrDetail.setObjectType(1);
										promotionCustAttrDetail.setObjectId(Long.valueOf(arr[k]));
										promotionCustAttrDetail.setCreateUser(currentUser.getUserName());
										listPromotionCustAttrDetail.add(promotionCustAttrDetail);
									}
								}
							}
							promotionCustAttUpdateVO = new PromotionCustAttUpdateVO();
							promotionCustAttUpdateVO.setPromotionCustAttr(promotionCustAttr);
							promotionCustAttUpdateVO.setLstPromotionCustAttrDetail(listPromotionCustAttrDetail);
							listPromotionCustAttUpdateVO.add(promotionCustAttUpdateVO);
						}
					}
				}
			}

			promotionProgramMgr.createOrUpdatePromotionCustAttVO(promotionId, listPromotionCustAttUpdateVO, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
		}
		return JSON;
	}

	/**
	 * Cap nhat so suat NVBH
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	public String updateSalerQuantity() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || id == null || id < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}

			PromotionStaffMap psm = promotionProgramMgr.getPromotionStaffMapById(id);
			if (psm == null || !ActiveType.RUNNING.equals(psm.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.staff.map.not.exists"));
				return JSON;
			}
			if(quantity != null && psm.getQuantityReceivedTotal() != null && quantity.compareTo(psm.getQuantityReceivedTotal()) < 0 ) {
				result.put("errMsg", "Số suất phân bổ không được bé hơn số suất đã nhận.");
				return JSON;
			} else if(amount != null && psm.getAmountReceivedTotal() != null && amount.compareTo(psm.getAmountReceivedTotal()) < 0){
				result.put("errMsg", "Số tiền phân bổ không được bé hơn số tiền đã nhận.");
				return JSON;
			} else if(number != null && psm.getNumReceivedTotal() != null && number.compareTo(psm.getNumReceivedTotal()) < 0) {
				result.put("errMsg", "Số lượng phân bổ không được bé hơn số lượng đã nhận.");
				return JSON;
			} else {
				psm.setQuantityMax(quantity);
				psm.setAmountMax(amount);
				psm.setNumMax(number);
				promotionProgramMgr.updatePromotionStaffMap(psm, getLogInfoVO());
				result.put(ERROR, false);
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.updateSalerQuantity - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xoa so suat NVBH
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	public String deleteStaffMap() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			/*
			 * if (!ActiveType.WAITING.equals(pro.getStatus())) {
			 * result.put("errMsg",
			 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
			 * "promotion.program.incorrect")); return JSON; }
			 */
			if (id != null && id > 0) {
				PromotionStaffMap psm = promotionProgramMgr.getPromotionStaffMapById(id);
				if (psm == null || !ActiveType.RUNNING.equals(psm.getStatus())) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.staff.map.not.exists"));
					return JSON;
				}
				psm.setStatus(ActiveType.DELETED);
				promotionProgramMgr.updatePromotionStaffMap(psm, getLogInfoVO());
				result.put(ERROR, false);
				return JSON;
			}
			if (shopId == null || shopId <= 0) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.focus.program.shop.code"));
				result.put(ERROR, true);
				return JSON;
			}
			Shop sh = shopMgr.getShopById(shopId);
			if (sh == null) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.focus.program.shop.code"));
				return JSON;
			}
			promotionProgramMgr.removePromotionStaffMap(promotionId, shopId, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.updateCustomerQuantity - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	private List<TreeGridNode<PromotionStaffVO>> lstStaffTree;

	/**
	 * Tim kiem don vi them vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 14, 2014
	 */
	public String searchSalerOnDlg() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			if (id != null) {
				shopId = id;
			}
			PromotionStaffFilter filter = new PromotionStaffFilter();
			
			Shop sh = null;
			if (shopId == null || shopId == 0) {
				//sh = staff.getShop();
				//shopId = sh.getId();
				shopId = currentUser.getShopRoot().getShopId();
			}
			sh = shopMgr.getShopById(shopId);
			filter.setPromotionId(promotionId);
			filter.setStrListShopId(getStrListShopId());
			filter.setShopId(shopId);
			if (sh != null && sh.getType().getSpecificType() != null) {
				filter.setShopType(sh.getType().getSpecificType().getValue());
			}
			filter.setCode(code);
			filter.setName(name);
			List<PromotionStaffVO> lst = promotionProgramMgr.searchStaffForPromotion(filter);
			lstStaffTree = new ArrayList<TreeGridNode<PromotionStaffVO>>();
			if (lst == null || lst.size() == 0) {
				return JSON;
			}

			if (id == null) {
				PromotionStaffVO vo = null;
				int i = 0;
				int sz = lst.size();
				String state = null;

				if (StringUtil.isNullOrEmpty(code) && StringUtil.isNullOrEmpty(name)) {
					vo = new PromotionStaffVO();
					vo.setId(sh.getId());
					vo.setCode(sh.getShopCode());
					vo.setName(sh.getShopName());
					vo.setIsSaler(0);

					state = ConstantManager.JSTREE_STATE_CLOSE;
					i = 0;
				} else {
					vo = lst.get(0);
					state = ConstantManager.JSTREE_STATE_OPEN;
					i = 1;
				}

				TreeGridNode<PromotionStaffVO> node = new TreeGridNode<PromotionStaffVO>();
				node.setNodeId("sh" + vo.getId());
				node.setAttr(vo);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setText(vo.getCode() + " - " + vo.getName());
				List<TreeGridNode<PromotionStaffVO>> chidren = new ArrayList<TreeGridNode<PromotionStaffVO>>();
				node.setChildren(chidren);
				lstStaffTree.add(node);

				// Tao cay			
				TreeGridNode<PromotionStaffVO> tmp = null;
				TreeGridNode<PromotionStaffVO> tmp2 = null;
				for (; i < sz; i++) {
					vo = lst.get(i);

					tmp2 = getNodeFromTree(lstStaffTree, "sh" + vo.getParentId());
					if (tmp2 != null) {
						tmp = new TreeGridNode<PromotionStaffVO>();
						tmp.setAttr(vo);
						if (0 == vo.getIsSaler()) {
							tmp.setNodeId("sh" + vo.getId());
							tmp.setState(state);
						} else {
							tmp.setNodeId("st" + vo.getId());
							tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
						}
						tmp.setText(vo.getCode() + " - " + vo.getName());

						if (tmp2.getChildren() == null) {
							tmp2.setChildren(new ArrayList<TreeGridNode<PromotionStaffVO>>());
						}
						tmp2.getChildren().add(tmp);
					}
				}
			} else {
				// Tao cay			
				TreeGridNode<PromotionStaffVO> tmp = null;
				PromotionStaffVO vo = null;
				for (int i = 0, sz = lst.size(); i < sz; i++) {
					vo = lst.get(i);

					tmp = new TreeGridNode<PromotionStaffVO>();
					tmp.setAttr(vo);
					if (0 == vo.getIsSaler()) {
						tmp.setNodeId("sh" + vo.getId());
						tmp.setState(ConstantManager.JSTREE_STATE_CLOSE);
					} else {
						tmp.setNodeId("st" + vo.getId());
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getCode() + " - " + vo.getName());

					lstStaffTree.add(tmp);
				}
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.searchSalerOfPromotion - " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Them NVBH vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	public String addPromotionStaff() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (promotionId == null || promotionId < 1 || lstId == null || lstId.size() == 0 || lstQtt == null || lstQtt.size() == 0 || lstId.size() != lstQtt.size()) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return JSON;
		}
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			List<PromotionStaffVO> lstTmp = promotionProgramMgr.getListStaffInPromotion(promotionId, lstId);
			if (lstTmp != null && lstTmp.size() > 0) {
				String msg = "";
				for (PromotionStaffVO vo : lstTmp) {
					msg += (", " + vo.getCode());
				}
				msg = msg.replaceFirst(", ", "");
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.customer.map.exists", msg));
				return JSON;
			}
			lstTmp = null;
			PromotionProgram pro = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (pro == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM"));
				return JSON;
			}
			if (!ActiveType.WAITING.equals(pro.getStatus()) && !ActiveType.RUNNING.equals(pro.getStatus())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.incorrect"));
				return JSON;
			}
			List<PromotionStaffMap> lst = new ArrayList<PromotionStaffMap>();
			PromotionShopMap psm = null;
			PromotionStaffMap pstm = null;
			Staff st = null;
			Date now = DateUtil.now();
			Long shIdT = null;
			for (int i = 0, sz = lstId.size(); i < sz; i++) {
				st = staffMgr.getStaffById(lstId.get(i));
				if (staff == null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "NVBH"));
					return JSON;
				}
				if (!ActiveType.RUNNING.equals(st.getStatus())) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, st.getStaffCode()));
					return JSON;
				}
				if (!StaffSpecificType.STAFF.getValue().equals(st.getStaffType().getSpecificType().getValue()) && !StaffSpecificType.SUPERVISOR.getValue().equals(st.getStaffType().getSpecificType().getValue())) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.not.nvbh", st.getStaffCode()));
					return JSON;
				}
				if (psm == null || !st.getShop().getId().equals(shIdT)) {
					psm = promotionProgramMgr.getPromotionParentShopMap(promotionId, st.getShop().getId(), ActiveType.RUNNING.getValue());
				}
				if (psm == null || !ActiveType.RUNNING.equals(psm.getStatus())) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.shop.not.exist"));
					return JSON;
				}
				shIdT = st.getShop().getId();
				List<PromotionStaffMap> listPromotionStaffMap = promotionProgramMgr.getListPromotionStaffMapAddPromotionStaff(psm.getId(), st.getId(), shIdT);
				if(listPromotionStaffMap != null && listPromotionStaffMap.size() > 0) {
					for (PromotionStaffMap staffMap : listPromotionStaffMap) {
						String staffCode = staffMap.getStaff().getStaffCode();
						StringBuilder sb = new StringBuilder("Nhân viên có mã ");
						sb.append(staffCode);
						sb.append(" có ");
						if (lstQtt.get(i) != null && staffMap.getQuantityReceivedTotal() != null && lstQtt.get(i).compareTo(staffMap.getQuantityReceivedTotal()) < 0) {
							sb.append("số suất phân bổ không được bé hơn số suất đã nhận.");
							result.put("errMsg", sb.toString());
							return JSON;
						} else if (lstAmt.get(i) != null && staffMap.getAmountReceivedTotal() != null && lstAmt.get(i).compareTo(staffMap.getAmountReceivedTotal()) < 0) {
							sb.append("số tiền phân bổ không được bé hơn số tiền đã nhận.");
							result.put("errMsg", sb.toString());
							return JSON;
						} else if (lstNum.get(i) != null && staffMap.getNumReceivedTotal() != null && lstNum.get(i).compareTo(staffMap.getNumReceivedTotal()) < 0) {
							sb.append("số lượng phân bổ không được bé hơn số lượng đã nhận.");
							result.put("errMsg", sb.toString() );
							return JSON;
						}
						if (lstQtt.get(i) > ZEZO) {
							staffMap.setQuantityMax(lstQtt.get(i));		
						}
						if (BigDecimal.ZERO.compareTo(lstAmt.get(i)) < 0) {
							staffMap.setAmountMax(lstAmt.get(i));					
						}
						if (BigDecimal.ZERO.compareTo(lstNum.get(i)) < 0) {
							staffMap.setNumMax(lstNum.get(i));
						}
						lst.add(staffMap);
					}
				} else {
					pstm = new PromotionStaffMap();
					pstm.setPromotionShopMap(psm);
					pstm.setShop(st.getShop());
					pstm.setStaff(st);
					if (lstQtt.get(i) > ZEZO) {
						pstm.setQuantityMax(lstQtt.get(i));		
					}
					if (BigDecimal.ZERO.compareTo(lstAmt.get(i)) < 0) {
						pstm.setAmountMax(lstAmt.get(i));					
					}
					if (BigDecimal.ZERO.compareTo(lstNum.get(i)) < 0) {
						pstm.setNumMax(lstNum.get(i));
					}
					pstm.setStatus(ActiveType.RUNNING);
					pstm.setCreateDate(now);
					pstm.setCreateUser(staff.getStaffCode());
	
					lst.add(pstm);
				}
			}
			promotionProgramMgr.createListPromotionStaffMap(lst, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.addCustomer - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xuat excel ds NVBH thuoc CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	public String exportPromotionStaff() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			result.put(ERROR, false);
			result.put("hasData", false);
			return JSON;
		}
		try {
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			PromotionShopMapFilter filter = new PromotionShopMapFilter();
			filter.setProgramId(promotionId);
			filter.setStatus(ActiveType.RUNNING);
			filter.setParentShopId(currentUser.getShopRoot().getShopId());
//			filter.setShopCode(code);
//			filter.setShopName(name);
//			filter.setQuantityMax(quantity);
			List<PromotionShopMapVO> lst = promotionProgramMgr.getListPromotionShopMapVO2(filter);
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
			StringBuilder sb = new StringBuilder(folder).append(ConstantManager.TEMPLATE_PROMOTION_STAFF_MAP_EXPORT).append(FileExtension.XLS.getValue());
			String templateFileName = sb.toString();
			templateFileName = templateFileName.replace('/', File.separatorChar);

			sb = new StringBuilder(ConstantManager.TEMPLATE_PROMOTION_STAFF_MAP_EXPORT).append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append(FileExtension.XLS.getValue());
			String outputName = sb.toString();
			sb = null;
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			Map<String, Object> beans = new HashMap<String, Object>();

			beans.put("hasData", 1);
			beans.put("lstShop", lst);

			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.exportPromotionStaff - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xuat excel ds NVBH thuoc CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	public String exportPromotionCustomer() throws Exception {
		if (promotionId == null || promotionId <= 0) {
			result.put(ERROR, false);
			result.put("hasData", false);
			return JSON;
		}
		try {
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			PromotionShopMapFilter filter = new PromotionShopMapFilter();
			filter.setPromotionId(promotionId);
			filter.setAddress(address);
			filter.setProgramId(promotionId);
			filter.setStatus(ActiveType.RUNNING);
			filter.setParentShopId(currentUser.getShopRoot().getShopId());
			filter.setCusCode(code);
			filter.setCusName(name);
			List<PromotionShopMapVO> lst = promotionProgramMgr.getListPromotionShopMapVO3(filter);
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
			StringBuilder sb = new StringBuilder(folder).append(ConstantManager.TEMPLATE_PROMOTION_CUSTOMER_MAP_EXPORT).append(FileExtension.XLS.getValue());
			String templateFileName = sb.toString();
			templateFileName = templateFileName.replace('/', File.separatorChar);

			sb = new StringBuilder(ConstantManager.TEMPLATE_PROMOTION_CUSTOMER_MAP_EXPORT).append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append(FileExtension.XLS.getValue());
			String outputName = sb.toString();
			sb = null;
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			Map<String, Object> beans = new HashMap<String, Object>();

			beans.put("hasData", 1);
			beans.put("lstShop", lst);

			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.exportPromotionStaff - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Nhap so suat NVBH tu excel
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 * 
	 * @modify hunglm16
	 * @sice 14/09/2015
	 * @description Bo sung them cot So tien va So luong
	 */
	public String importPromotionStaff() throws Exception {
		resetToken(result);
		isError = true;
		try {
			PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (promotionProgram == null) {
				isError = true;
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM");
				return SUCCESS;
			}
			totalItem = 0;
			String message = "";
			String msg = "";
			lstView = new ArrayList<CellBean>();
			typeView = true;
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, 5);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				PromotionStaffMap promotionStaffMap = null;
				List<String> row = null;
				Shop shop = null;
				boolean isCreateStaff;
				PromotionShopMap psm = null;
				Staff st = null;
				LogInfoVO logInfo = getLogInfoVO();
				String value = null;

				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						boolean flagContinue = true;
						//Bo qua cac dong trong
						for (int j = 0; j < 5; j++) {
							if (!StringUtil.isNullOrEmpty(lstData.get(i).get(j))) {
								flagContinue = false;
							}
						}
						if (flagContinue) {
							continue;
						}
						message = "";
						msg = "";
						totalItem++;
						row = lstData.get(i);
						isCreateStaff = false;
						promotionStaffMap = new PromotionStaffMap();
						// Ma don vi
						value = row.get(0);
						if (!StringUtil.isNullOrEmpty(value)) {
							shop = shopMgr.getShopByCode(value);
							if (shop == null) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.shop.code");
							} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
							} else if (!checkShopPermission(shop.getShopCode())) {
								message += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_SHOP, shop.getShopCode(), currentUser.getUserName()) + "\n";
							}
						} else {
							message += R.getResource("common.missing.not.in.system.p",R.getResource("catalog.focus.program.shop.code")) + "\n";
						}
						if (shop != null) {
							psm = promotionProgramMgr.getPromotionShopMap(shop.getId(), promotionProgram.getId());
						}
						if (StringUtil.isNullOrEmpty(message)) {
							if (psm != null) {
								if (!ActiveType.RUNNING.equals(psm.getStatus())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.shop.not.exist") + "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.shop.not.exist") + "\n";
							}
						}
						// NVBH
						value = row.get(1);
						if (!StringUtil.isNullOrEmpty(value) && shop != null) {
							st = staffMgr.getStaffByCode(value);
							if (st == null) {
								message += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "NV " + value) + "\n";
							} else {
								if (!ActiveType.RUNNING.equals(st.getStatus())) {
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, "NV") + "\n";
								} else if (st.getStaffType() == null || st.getStaffType().getSpecificType() == null || !StaffSpecificType.STAFF.getValue().equals(st.getStaffType().getSpecificType().getValue())) {
									//&& !StaffSpecificType.SUPERVISOR.getValue().equals(st.getStaffType().getSpecificType().getValue())
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.not.nvbh", st.getStaffCode()) + "\n";
								} else if (st.getShop() != null && !st.getShop().getId().equals(shop.getId())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.not.in.shop", "NV") + "\n";
								}
								if (psm != null && st != null) {
									promotionStaffMap = promotionProgramMgr.getPromotionStaffMapByShopMapAndStaff(psm.getId(), st.getId());
									if (promotionStaffMap == null) {
										isCreateStaff = true;
										promotionStaffMap = new PromotionStaffMap();
									}
								}
								if (promotionStaffMap != null) {
									promotionStaffMap.setPromotionShopMap(psm);
									promotionStaffMap.setStaff(st);
								}
							}
						} else {
							message += R.getResource("common.missing.not.in.system.p",R.getResource("ss.traningplan.salestaff.code")) + "\n";
						}
						// So suat NVBH
						value = row.get(2);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.suatKH.code"));
							} else {
								if (Integer.valueOf(value) > 0) {
									promotionStaffMap.setQuantityMax(Integer.valueOf(value));
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.suatNPP.code"));
								}								
							}
						} else if (promotionStaffMap != null) {
							promotionStaffMap.setQuantityMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}

						//So tien
						value = row.get(3);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.promotion.import.khtg.ctkm.clmn.amount"));
							} else {
								if (Integer.valueOf(value) > 0) {
									promotionStaffMap.setAmountMax(BigDecimal.valueOf(Long.valueOf(value)));
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.khtg.ctkm.clmn.amount"));
								}
							}
						} else if (promotionStaffMap != null) {
							promotionStaffMap.setAmountMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						//So luong
						value = row.get(4);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.promotion.import.khtg.ctkm.clmn.quantity"));
							} else {
								if (Integer.valueOf(value) > 0) {
									promotionStaffMap.setNumMax(BigDecimal.valueOf(Long.valueOf(value)));
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.khtg.ctkm.clmn.quantity"));
								}
							}
						} else if (promotionStaffMap != null) {
							promotionStaffMap.setNumMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						//Kiem tra dieu kien thoa man va cap nhat xuong DB
						if (StringUtil.isNullOrEmpty(message)) {
							try {
								if (promotionStaffMap != null && promotionStaffMap.getStaff() != null) {
									promotionStaffMap.setShop(st.getShop());
									if (isCreateStaff) {
										promotionProgramMgr.createPromotionStaffMap(promotionStaffMap, logInfo);
									} else {
										promotionProgramMgr.updatePromotionStaffMap(promotionStaffMap, logInfo);
									}
								}
								isError = false;
							} catch (BusinessException be) {
								LogUtility.logError(be, be.getMessage());
								errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
						typeView = false;
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_PP_STAFF_FAIL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				isError = false;
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.importPromotionStaff - " + ex.getMessage());
			isError = true;
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}

	/**
	 * Nhap so suat NVBH tu excel
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 * 
	 * @modify hunglm16
	 * @sice 14/09/2015
	 * @description Bo sung them cot So tien va So luong
	 */
	public String importPromotionCustomer() throws Exception {
		resetToken(result);
		isError = true;
		try {
			PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramById(promotionId);
			if (promotionProgram == null) {
				isError = true;
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "CTKM");
				return SUCCESS;
			}
			totalItem = 0;
			String message = "";
			String msg = "";
			lstView = new ArrayList<CellBean>();
			typeView = true;
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, 5);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				PromotionCustomerMap promotionCustomerMap = null;
				List<String> row = null;
				Shop shop = null;
				boolean isCreateCustomer;
				PromotionShopMap psm = null;
				Customer cust = null;
				LogInfoVO logInfo = getLogInfoVO();
				String value = null;

				for (int i = 0, sizei = lstData.size(); i < sizei; i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						boolean flagContinue = true;
						//Bo qua cac dong trong
						for (int j = 0; j < 5; j++) {
							if (!StringUtil.isNullOrEmpty(lstData.get(i).get(j))) {
								flagContinue = false;
							}
						}
						if (flagContinue) {
							continue;
						}
						message = "";
						totalItem++;
						row = lstData.get(i);
						isCreateCustomer = false;
						promotionCustomerMap = new PromotionCustomerMap();
						// Ma don vi
						value = row.get(0);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (!StringUtil.isNullOrEmpty(value)) {
								shop = shopMgr.getShopByCode(value);
								if (shop == null) {
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.shop.code");
								} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
								} else if (!checkShopPermission(shop.getShopCode())) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_SHOP, shop.getShopCode(), currentUser.getUserName()) + "\n";
								} 
							} else {
								shop = getCurrentShop();
							}
						} else {
							message += R.getResource("common.missing.not.in.system.p",R.getResource("catalog.focus.program.shop.code")) + "\n";
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						if (shop != null) {
							psm = promotionProgramMgr.getPromotionShopMap(shop.getId(), promotionProgram.getId());
						}
						if (StringUtil.isNullOrEmpty(message)) {
							if (psm != null) {
								if (!ActiveType.RUNNING.equals(psm.getStatus())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.shop.not.exist") + "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.shop.not.exist") + "\n";
							}
						}

						// KH
						value = row.get(1);
						if (!StringUtil.isNullOrEmpty(value) && shop != null) {
							cust = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), value));
							if (cust == null) {
								message += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "KH " + value) + "\n";
							} else {
								if (!ActiveType.RUNNING.equals(cust.getStatus())) {
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, "KH") + "\n";
								}
							}
							if (psm != null && cust != null) {
								promotionCustomerMap = promotionProgramMgr.getPromotionCustomerMap(psm.getId(), cust.getId());
								if (promotionCustomerMap == null) {
									isCreateCustomer = true;
									promotionCustomerMap = new PromotionCustomerMap();
								}
							}
							if (promotionCustomerMap != null) {
								promotionCustomerMap.setPromotionShopMap(psm);
								promotionCustomerMap.setCustomer(cust);
							}
						} else {
							message += R.getResource("common.missing.not.in.system.p",R.getResource("ss.traningplan.salestaff.code")) + "\n";
						}
						// So suat NVBH
						value = row.get(2);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.suatKH.code"));
							} else {
								if (Integer.valueOf(value) > 0) {
									if (shop != null) {
										if (psm != null && cust != null) {
											if (promotionCustomerMap != null && promotionCustomerMap.getQuantityReceived() != null && promotionCustomerMap.getQuantityReceived() > Integer.valueOf(value)) {
												msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.program.quantityR.less.than.quantityM") + "\n";
											}
										}
										if (StringUtil.isNullOrEmpty(message)) {
											promotionCustomerMap.setQuantityMax(Integer.valueOf(value));
										}
									}
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.suatNPP.code"));
								}
							}
						} else if (promotionCustomerMap != null) {
							promotionCustomerMap.setQuantityMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}

						//So tien
						value = row.get(3);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.promotion.import.khtg.ctkm.clmn.amount"));
							} else {
								if (Long.valueOf(value) > 0) {
									promotionCustomerMap.setAmountMax(BigDecimal.valueOf(Long.valueOf(value)));
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.khtg.ctkm.clmn.amount"));
								}
							}
						} else if (promotionCustomerMap != null) {
							promotionCustomerMap.setAmountMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						
						//So luong
						value = row.get(4);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							if (value.trim().length() > maxlengthNumber || !StringUtil.isNumberInt(value)) {
								msg = R.getResource("catalog.promotion.import.khtg.ctkm.value.number.integer", R.getResource("catalog.promotion.import.khtg.ctkm.clmn.quantity"));
							} else {
								if (Long.valueOf(value) > 0) {
									promotionCustomerMap.setNumMax(BigDecimal.valueOf(Long.valueOf(value)));
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.negative", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.import.khtg.ctkm.clmn.quantity"));
								}
							}
						} else if (promotionCustomerMap != null) {
							promotionCustomerMap.setNumMax(null);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}

						//Kiem tra tinh hop le va cap DB
						if (StringUtil.isNullOrEmpty(message)) {
							try {
								if (promotionCustomerMap != null && promotionCustomerMap.getCustomer() != null) {
									promotionCustomerMap.setShop(cust.getShop());
									if (isCreateCustomer) {
										promotionProgramMgr.createPromotionCustomerMap(promotionCustomerMap, logInfo);
									} else {
										promotionProgramMgr.updatePromotionCustomerMap(promotionCustomerMap, logInfo);
									}
								}
								isError = false;
							} catch (BusinessException be) {
								LogUtility.logError(be, be.getMessage());
								errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
						typeView = false;
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_PP_STAFF_FAIL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				isError = false;
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "PromotionCatalogAction.importPromotionStaff - " + ex.getMessage());
			isError = true;
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}

	public String deleteLevelDetail() {
		resetToken(result);
		try {
			promotionProgramMgr.deleteLevelDetail(levelDetailId, getLogInfoVO());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.deleteLevelDetail - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * load page
	 * 
	 * @return
	 */
	public String productConvertDetail() {
		try {
			promotionProgram = promotionProgramMgr.getPromotionProgramById(id);
			listProduct = promotionProgramMgr.getListProductInSaleLevel(id);
		} catch (Exception e) {
			LogUtility.logError(e, "PromotionCatalogAction.productConvertDetail - " + e.getMessage());
			listProduct = new ArrayList<Product>();
		}
		return SUCCESS;
	}

	/**
	 * load list
	 * 
	 * @author phut
	 * @return
	 */
	public String productConvertLoadGroup() {
		try {
			listConvertGroup = promotionProgramMgr.listPromotionProductConvertVO(promotionId, null);
		} catch (Exception e) {
			LogUtility.logError(e, "PromotionCatalogAction.deleteLevelDetail - " + e.getMessage());
			listConvertGroup = new ArrayList<PPConvertVO>();
		}
		return SUCCESS;
	}

	/**
	 * save list ctkm sp quy doi
	 * 
	 * @author phut
	 * @return
	 */
	public String saveProductConvert() {
		resetToken(result);
		try {
			if (listConvertGroup != null && listConvertGroup.size() > 0) {
				for (PPConvertVO vo : listConvertGroup) {
					if (null == vo.getId()) {						
						List<PPConvertVO> __result = promotionProgramMgr.listPromotionProductConvertVO(promotionId, vo.getName());
						if (__result.size() > 0) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.product.convert.duplicate.group.error", vo.getName()));
							return SUCCESS;
						}
					}else{
						/** @author update nhutnn, @since 14/05/2015 */
						if (vo.getListDetail() != null) {
							for (int i=0, sz = vo.getListDetail().size(); i < sz; i++){
								if (vo.getListDetail().get(i) != null) {
									String proCode = vo.getListDetail().get(i).getProductCode();
									if (!StringUtil.isNullOrEmpty(proCode)) {
										Product product = productMgr.getProductByCode(proCode);
										if (product == null) {
											result.put(ERROR, true);
											result.put("errMsg", R.getResource("catalog.display.program.update.sale.product.product.not.exists", proCode));
											return SUCCESS;
										}
									}else{
										result.put(ERROR, true);
										result.put("errMsg", R.getResource("price.manage.err.imp.product.isnull"));
										return SUCCESS;
									}
								}
							}
						}
					}
				}
				promotionProgramMgr.savePPConvert(promotionId, listConvertGroup, getLogInfoVO());
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.saveProductConvert - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * delete 1 dong CTKM sp quy doi
	 * 
	 * @author phut
	 * @return
	 */
	public String deleteProductConvert() {
		resetToken(result);
		try {
			promotionProgramMgr.deletePPConvert(id, getLogInfoVO());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.deleteProductConvert - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * delete 1 sp cua 1 dong CTKM sp quy doi
	 * 
	 * @author phut
	 * @return
	 */
	public String deleteProductConvertDetail() {
		resetToken(result);
		try {
			promotionProgramMgr.deletePPConvertDetail(id, getLogInfoVO());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.deleteProductConvertDetail - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * load page
	 * 
	 * @author phut
	 * @return
	 */
	public String productOpenNewDetail() throws Exception {
		promotionProgram = promotionProgramMgr.getPromotionProgramById(id);
		return SUCCESS;
	}

	/**
	 * load list
	 * 
	 * @author phut
	 * @return
	 */
	public String productOpenNewLoadProduct() {
		try {
			listProductOpen = promotionProgramMgr.listPromotionProductOpenVO(promotionId);
		} catch (Exception e) {
			listProductOpen = new ArrayList<PromotionProductOpenVO>();
			LogUtility.logError(e, "PromotionCatalogAction.deleteProductConvertDetail - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * new/update list ctkm KH mo moi
	 * 
	 * @author phut
	 * @return
	 */
	public String productOpenNewSave() {
		try {
			promotionProgramMgr.saveListPromotionProductOpen(promotionId, listProductOpen, getLogInfoVO());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.productOpenNewSave - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * delete 1 dong ctkm KH mo moi
	 * 
	 * @author phut
	 * @return
	 */
	public String productOpenNewDelete() {
		resetToken(result);
		try {
			promotionProgramMgr.deletePromotionProductOpen(id);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "PromotionCatalogAction.productOpenNewDelete - " + e.getMessage());
		}
		return SUCCESS;
	}

	public String getReportNameFormat(String name, String fDate, String tDate, String fileExtension) {
		String nameRpt = "";
		if (fDate.isEmpty() && tDate.isEmpty()) {
			nameRpt = nameRpt + name;
		} else {
			Date fD = DateUtil.parse(fDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tD = DateUtil.parse(tDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			fDate = DateUtil.toDateString(fD, DateUtil.DATE_FORMAT_CSV);
			tDate = DateUtil.toDateString(tD, DateUtil.DATE_FORMAT_CSV);
			nameRpt = nameRpt + name + "_" + fDate + "-" + tDate;
		}
		Random rand = new Random();
		Integer n = rand.nextInt(100) + 1;
		return nameRpt + "_" + n.toString() + fileExtension;
	}

	/**
	 * Xuat danh sach CTKM
	 * 
	 * @author tungmt
	 * @since 27/02/14
	 * @description Xuất XLSX
	 */
	public String export() throws Exception {
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {		
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			result.put(ERROR, true);
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				return JSON;
			}

			PromotionProgramFilter filter = new PromotionProgramFilter();
			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = ths.dms.web.utils.DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = ths.dms.web.utils.DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setToDate(tDate);
			}
			List<String> temp = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty(lstTypeId)) {
				if (lstTypeId.indexOf("-1") > -1) {
					temp = null;
				} else {
					String[] lstTmp = lstTypeId.split(",");
					if (lstTmp.length > 0) {
						Integer size = lstTmp.length;
						for (int i = 0; i < size; i++) {
							ApParam apParam = apParamMgr.getApParamById(Long.valueOf(lstTmp[i].trim()));
							if (apParam != null) {
								temp.add(apParam.getApParamCode());
							}
						}
					}
				}
			}
			filter.setLstType(temp);
			ActiveType at = null;
			if (status == null) {
				status = 1;
			}
			if (status != ConstantManager.NOT_STATUS) {
				at = ActiveType.parseValue(status);
				filter.setStatus(at);
			}
			
			if (StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(currentUser.getShopRoot().getShopCode());
				filter.setCreateUser(currentUser.getStaffRoot().getStaffCode());
				if (isVNMAdmin) {
					filter.setIsVNM(true);
				}
			} else {
				filter.setShopCode(shopCode);
			}
			filter.setPpCode(code);
			filter.setPpName(name);
			filter.setIsAutoPromotion(ConstantManager.PROMOTION_AUTO == proType);
			filter.setStrListShopId(getStrListShopId());
			ObjectVO<PromotionProgram> objVO = promotionProgramMgr.getListPromotionProgram(filter);
			List<PromotionProgram> lst = objVO != null ? objVO.getLstObject() : null;
			/** Lay va kiem tra co du lieu */
			//			List<RptDM1_1TTKH> lst = hoReportMgr.getDM1_1TTKH(lstShopId, strStatus);
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			String fileName = R.getResource("promotion.program.export.name");
			fileName = getReportNameFormat(fileName, "", "", FileExtension.XLSX.getValue());
			//Init XSSF workboook
			workbook = new SXSSFWorkbook(200);
			workbook.setCompressTempFiles(true);
			//Tao sheet
			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
			ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

			SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Danh_sach_ctkm");
			//Set Getting Defaul
			sheetData.setDefaultRowHeight((short) (15 * 20));
			sheetData.setDefaultColumnWidth(13);
			//set static Column width
			mySheet.setColumnsWidth(sheetData, 0, 125, 350, 250, 125, 125, 125);
			//Size Row
			mySheet.setRowsHeight(sheetData, 0, 15, 25, 15, 15);
			sheetData.setDisplayGridlines(false);

			/** */
			// header
			int colIdx = 0;
			int rowIdx = 0;
			// title
			rowIdx++;
			String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "promotion.program.export.title");
			mySheet.addCellsAndMerged(sheetData, 0, rowIdx, 5, rowIdx++, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK));

			rowIdx++;
			String[] menu = R.getResource("promotion.program.export.menu").split(";");

			for (int i = 0; i < menu.length; i++) {
				mySheet.addCell(sheetData, colIdx++, rowIdx, menu[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
			}
			// data
			rowIdx++;
			colIdx = 0;
			PromotionProgram vo = null;
			String action = R.getResource("action.status.name");
			String stop = R.getResource("pause.status.name");
			String wait = R.getResource("action.status.waiting");
			for (int i = 0, size = lst.size(); i < size; i++, rowIdx++) {
				vo = lst.get(i);
				colIdx = 0;
				mySheet.setRowsHeight(sheetData, rowIdx, 15);
				mySheet.addCell(sheetData, colIdx++, rowIdx, vo.getPromotionProgramCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				mySheet.addCell(sheetData, colIdx++, rowIdx, vo.getPromotionProgramName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				mySheet.addCell(sheetData, colIdx++, rowIdx, vo.getType() != null ? vo.getType() + " - " + vo.getProFormat() : "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				mySheet.addCell(sheetData, colIdx++, rowIdx, vo.getFromDate() != null ? DateUtil.toDateString(vo.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY) : "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				mySheet.addCell(sheetData, colIdx++, rowIdx, vo.getToDate() != null ? DateUtil.toDateString(vo.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY) : "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				if (ActiveType.RUNNING.equals(vo.getStatus())) {
					mySheet.addCell(sheetData, colIdx++, rowIdx, action, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				} else if (ActiveType.STOPPED.equals(vo.getStatus())) {
					mySheet.addCell(sheetData, colIdx++, rowIdx, stop, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				} else {
					mySheet.addCell(sheetData, colIdx++, rowIdx, wait, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				}
			}

			out = new FileOutputStream(Configuration.getStoreRealPath() + fileName);
			workbook.write(out);
			result.put(ERROR, false);
			result.put("hasData", true);
			String filePath = Configuration.getExportExcelPath() + fileName;
			result.put(REPORT_PATH, filePath);
			MemcachedUtils.putValueToMemcached(reportToken, filePath, retrieveReportMemcachedTimeout());
			lst = null;
			System.gc();
		} catch (Exception ex) {
			LogUtility.logError(ex, "CatalogHOReportAction.exportDM1.1_TTKH_Xlsx - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}

	private ApParamMgr apParamMgr;
	private ProductMgr productMgr;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<TreeGridNode<PromotionShopVO>> getLstTree() {
		return lstTree;
	}

	public void setLstTree(List<TreeGridNode<PromotionShopVO>> lstTree) {
		this.lstTree = lstTree;
	}

	public List<Integer> getLstQtt() {
		return lstQtt;
	}

	public void setLstQtt(List<Integer> lstQtt) {
		this.lstQtt = lstQtt;
	}

	public List<Boolean> getLstEdit() {
		return lstEdit;
	}

	public void setLstEdit(List<Boolean> lstEdit) {
		this.lstEdit = lstEdit;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getProType() {
		return proType;
	}

	public void setProType(Integer proType) {
		this.proType = proType;
	}

	public PromotionProgram getPromotionProgram() {
		return promotionProgram;
	}

	public void setPromotionProgram(PromotionProgram promotionProgram) {
		this.promotionProgram = promotionProgram;
	}

	public String getLstTypeId() {
		return lstTypeId;
	}

	public void setLstTypeId(String lstTypeId) {
		this.lstTypeId = lstTypeId;
	}

	public List<PromotionCustAttrVO> getLstPromotionCustAttrVO() {
		return lstPromotionCustAttrVO;
	}

	public void setLstPromotionCustAttrVO(List<PromotionCustAttrVO> lstPromotionCustAttrVO) {
		this.lstPromotionCustAttrVO = lstPromotionCustAttrVO;
	}

	public List<Integer> getLstObjectType() {
		return lstObjectType;
	}

	public void setLstObjectType(List<Integer> lstObjectType) {
		this.lstObjectType = lstObjectType;
	}

	public List<Long> getLstCustomerType() {
		return lstCustomerType;
	}

	public void setLstCustomerType(List<Long> lstCustomerType) {
		this.lstCustomerType = lstCustomerType;
	}

	public List<Long> getLstSaleLevelCatId() {
		return lstSaleLevelCatId;
	}

	public void setLstSaleLevelCatId(List<Long> lstSaleLevelCatId) {
		this.lstSaleLevelCatId = lstSaleLevelCatId;
	}

	public List<String> getLstAttDataInField() {
		return lstAttDataInField;
	}

	public void setLstAttDataInField(List<String> lstAttDataInField) {
		this.lstAttDataInField = lstAttDataInField;
	}

	public List<TreeGridNode<PromotionStaffVO>> getLstStaffTree() {
		return lstStaffTree;
	}

	public void setLstStaffTree(List<TreeGridNode<PromotionStaffVO>> lstStaffTree) {
		this.lstStaffTree = lstStaffTree;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<CellBean> getLstHeaderError() {
		return lstHeaderError;
	}

	public void setLstHeaderError(List<CellBean> lstHeaderError) {
		this.lstHeaderError = lstHeaderError;
	}

	public List<CellBean> getLstDetailError() {
		return lstDetailError;
	}

	public void setLstDetailError(List<CellBean> lstDetailError) {
		this.lstDetailError = lstDetailError;
	}

	public List<ApParam> getLstTypeCode() {
		return lstTypeCode;
	}

	public void setLstTypeCode(List<ApParam> lstTypeCode) {
		this.lstTypeCode = lstTypeCode;
	}

	public Boolean getIsVNMAdmin() {
		return isVNMAdmin;
	}

	public void setIsVNMAdmin(Boolean isVNMAdmin) {
		this.isVNMAdmin = isVNMAdmin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getMaxQuantity() {
		return maxQuantity;
	}

	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	public BigDecimal getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}

	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}

	public List<ProductGroup> getLstGroupSale() {
		return lstGroupSale;
	}

	public void setLstGroupSale(List<ProductGroup> lstGroupSale) {
		this.lstGroupSale = lstGroupSale;
	}

	public List<ProductGroup> getLstGroupFree() {
		return lstGroupFree;
	}

	public void setLstGroupFree(List<ProductGroup> lstGroupFree) {
		this.lstGroupFree = lstGroupFree;
	}

	public static long getIndexMua() {
		return indexMua;
	}

	public static void setIndexMua(long indexMua) {
		PromotionCatalogAction.indexMua = indexMua;
	}

	public static long getIndexKM() {
		return indexKM;
	}

	public static void setIndexKM(long indexKM) {
		PromotionCatalogAction.indexKM = indexKM;
	}

	public List<ExcelPromotionHeader> getListHeader() {
		return listHeader;
	}

	public void setListHeader(List<ExcelPromotionHeader> listHeader) {
		this.listHeader = listHeader;
	}

	public List<ExcelPromotionDetail> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<ExcelPromotionDetail> listDetail) {
		this.listDetail = listDetail;
	}

	public Map<String, ListGroupMua> getMapPromotionMua() {
		return mapPromotionMua;
	}

	public void setMapPromotionMua(Map<String, ListGroupMua> mapPromotionMua) {
		this.mapPromotionMua = mapPromotionMua;
	}

	public Map<String, ListGroupKM> getMapPromotionKM() {
		return mapPromotionKM;
	}

	public void setMapPromotionKM(Map<String, ListGroupKM> mapPromotionKM) {
		this.mapPromotionKM = mapPromotionKM;
	}

	public Map<String, String> getMapPromotionTypeCheck() {
		return mapPromotionTypeCheck;
	}

	public void setMapPromotionTypeCheck(Map<String, String> mapPromotionTypeCheck) {
		this.mapPromotionTypeCheck = mapPromotionTypeCheck;
	}

	public MapMuaKM getMapMuaKM() {
		return mapMuaKM;
	}

	public void setMapMuaKM(MapMuaKM mapMuaKM) {
		this.mapMuaKM = mapMuaKM;
	}

	public Boolean getMultiple() {
		return multiple;
	}

	public void setMultiple(Boolean multiple) {
		this.multiple = multiple;
	}

	public Boolean getRecursive() {
		return recursive;
	}

	public void setRecursive(Boolean recursive) {
		this.recursive = recursive;
	}

	public Integer getStt() {
		return stt;
	}

	public void setStt(Integer stt) {
		this.stt = stt;
	}

	public Integer getMinQuantity() {
		return minQuantity;
	}

	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}

	public BigDecimal getMinAmount() {
		return minAmount;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public List<GroupLevelVO> getLstLevel() {
		return lstLevel;
	}

	public void setLstLevel(List<GroupLevelVO> lstLevel) {
		this.lstLevel = lstLevel;
	}

	public List<Product> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<Product> listProduct) {
		this.listProduct = listProduct;
	}

	public List<Integer> getListMinQuantity() {
		return listMinQuantity;
	}

	public void setListMinQuantity(List<Integer> listMinQuantity) {
		this.listMinQuantity = listMinQuantity;
	}

	public List<BigDecimal> getListMinAmount() {
		return listMinAmount;
	}

	public void setListMinAmount(List<BigDecimal> listMinAmount) {
		this.listMinAmount = listMinAmount;
	}

	public List<Integer> getListOrder() {
		return listOrder;
	}

	public void setListOrder(List<Integer> listOrder) {
		this.listOrder = listOrder;
	}

	public List<String> getListProductDetail() {
		return listProductDetail;
	}

	public void setListProductDetail(List<String> listProductDetail) {
		this.listProductDetail = listProductDetail;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getCanEdit() {
		return canEdit;
	}

	public void setCanEdit(Boolean canEdit) {
		this.canEdit = canEdit;
	}

	public List<Integer> getListMaxQuantity() {
		return listMaxQuantity;
	}

	public void setListMaxQuantity(List<Integer> listMaxQuantity) {
		this.listMaxQuantity = listMaxQuantity;
	}

	public List<BigDecimal> getListMaxAmount() {
		return listMaxAmount;
	}

	public void setListMaxAmount(List<BigDecimal> listMaxAmount) {
		this.listMaxAmount = listMaxAmount;
	}

	public Long getGroupMuaId() {
		return groupMuaId;
	}

	public void setGroupMuaId(Long groupMuaId) {
		this.groupMuaId = groupMuaId;
	}

	public Long getGroupKMId() {
		return groupKMId;
	}

	public void setGroupKMId(Long groupKMId) {
		this.groupKMId = groupKMId;
	}

	public List<LevelMappingVO> getListLevelMapping() {
		return listLevelMapping;
	}

	public void setListLevelMapping(List<LevelMappingVO> listLevelMapping) {
		this.listLevelMapping = listLevelMapping;
	}

	public List<Float> getListPercent() {
		return listPercent;
	}

	public void setListPercent(List<Float> listPercent) {
		this.listPercent = listPercent;
	}

	public Long getMappingId() {
		return mappingId;
	}

	public void setMappingId(Long mappingId) {
		this.mappingId = mappingId;
	}

	public String getGroupMuaCode() {
		return groupMuaCode;
	}

	public void setGroupMuaCode(String groupMuaCode) {
		this.groupMuaCode = groupMuaCode;
	}

	public Integer getOrderLevelMua() {
		return orderLevelMua;
	}

	public void setOrderLevelMua(Integer orderLevelMua) {
		this.orderLevelMua = orderLevelMua;
	}

	public String getGroupKMCode() {
		return groupKMCode;
	}

	public void setGroupKMCode(String groupKMCode) {
		this.groupKMCode = groupKMCode;
	}

	public Integer getOrderLevelKM() {
		return orderLevelKM;
	}

	public void setOrderLevelKM(Integer orderLevelKM) {
		this.orderLevelKM = orderLevelKM;
	}

	public List<Long> getListLevelId() {
		return listLevelId;
	}

	public void setListLevelId(List<Long> listLevelId) {
		this.listLevelId = listLevelId;
	}

	public Long getLevelMuaId() {
		return levelMuaId;
	}

	public void setLevelMuaId(Long levelMuaId) {
		this.levelMuaId = levelMuaId;
	}

	public Long getLevelKMId() {
		return levelKMId;
	}

	public void setLevelKMId(Long levelKMId) {
		this.levelKMId = levelKMId;
	}

	public Long getLevelDetailId() {
		return levelDetailId;
	}

	public void setLevelDetailId(Long levelDetailId) {
		this.levelDetailId = levelDetailId;
	}

	public String getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	public Long getLevelId() {
		return levelId;
	}

	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public Integer getCopyNum() {
		return copyNum;
	}

	public void setCopyNum(Integer copyNum) {
		this.copyNum = copyNum;
	}

	public List<NewLevelMapping> getListNewMapping() {
		return listNewMapping;
	}

	public void setListNewMapping(List<NewLevelMapping> listNewMapping) {
		this.listNewMapping = listNewMapping;
	}

	public Integer getMuaMinQuantity() {
		return muaMinQuantity;
	}

	public void setMuaMinQuantity(Integer muaMinQuantity) {
		this.muaMinQuantity = muaMinQuantity;
	}

	public BigDecimal getMuaMinAmount() {
		return muaMinAmount;
	}

	public void setMuaMinAmount(BigDecimal muaMinAmount) {
		this.muaMinAmount = muaMinAmount;
	}

	public Float getPercentKM() {
		return percentKM;
	}

	public void setPercentKM(Float percentKM) {
		this.percentKM = percentKM;
	}

	public Integer getKmMaxQuantity() {
		return kmMaxQuantity;
	}

	public void setKmMaxQuantity(Integer kmMaxQuantity) {
		this.kmMaxQuantity = kmMaxQuantity;
	}

	public BigDecimal getKmMaxAmount() {
		return kmMaxAmount;
	}

	public void setKmMaxAmount(BigDecimal kmMaxAmount) {
		this.kmMaxAmount = kmMaxAmount;
	}

	public List<ExMapping> getListSubLevelMua() {
		return listSubLevelMua;
	}

	public void setListSubLevelMua(List<ExMapping> listSubLevelMua) {
		this.listSubLevelMua = listSubLevelMua;
	}

	public List<ExMapping> getListSubLevelKM() {
		return listSubLevelKM;
	}

	public void setListSubLevelKM(List<ExMapping> listSubLevelKM) {
		this.listSubLevelKM = listSubLevelKM;
	}

	public List<NewProductGroupVO> getLstGroupNew() {
		return lstGroupNew;
	}

	public void setLstGroupNew(List<NewProductGroupVO> lstGroupNew) {
		this.lstGroupNew = lstGroupNew;
	}

	public List<PPConvertVO> getListConvertGroup() {
		return listConvertGroup;
	}

	public void setListConvertGroup(List<PPConvertVO> listConvertGroup) {
		this.listConvertGroup = listConvertGroup;
	}

	public List<PromotionProductOpenVO> getListProductOpen() {
		return listProductOpen;
	}

	public void setListProductOpen(List<PromotionProductOpenVO> listProductOpen) {
		this.listProductOpen = listProductOpen;
	}

	public Integer getQuantiMonthNewOpen() {
		return quantiMonthNewOpen;
	}

	public void setQuantiMonthNewOpen(Integer quantiMonthNewOpen) {
		this.quantiMonthNewOpen = quantiMonthNewOpen;
	}

	public Integer getIsEdited() {
		return isEdited;
	}

	public void setIsEdited(Integer isEdited) {
		this.isEdited = isEdited;
	}

	public Integer getPromotionStatus() {
		return promotionStatus;
	}

	public void setPromotionStatus(Integer promotionStatus) {
		this.promotionStatus = promotionStatus;
	}

	public Integer getFromLevel() {
		return fromLevel;
	}

	public void setFromLevel(Integer fromLevel) {
		this.fromLevel = fromLevel;
	}

	public Integer getToLevel() {
		return toLevel;
	}

	public void setToLevel(Integer toLevel) {
		this.toLevel = toLevel;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getNumber() {
		return number;
	}

	public void setNumber(BigDecimal number) {
		this.number = number;
	}

	public List<BigDecimal> getLstAmt() {
		return lstAmt;
	}

	public void setLstAmt(List<BigDecimal> lstAmt) {
		this.lstAmt = lstAmt;
	}

	public List<BigDecimal> getLstNum() {
		return lstNum;
	}

	public void setLstNum(List<BigDecimal> lstNum) {
		this.lstNum = lstNum;
	}
	
}
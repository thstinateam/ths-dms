package ths.dms.web.action.organization;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.ibm.icu.util.Calendar;
import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.OrganizationMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.Organization;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopType;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrganizationFilter;
import ths.dms.core.entities.enumtype.OrganizationNodeType;
import ths.dms.core.entities.enumtype.OrganizationUnitTypeFilter;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.filter.OrganizationSystemFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrganizationUnitTypeVO;
import ths.dms.core.entities.vo.OrganizationVO;
import ths.dms.core.entities.vo.ShopTreeVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.JETreeNode;
import ths.dms.web.bean.JETreeNodeAttr;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.ImageUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.Unicode2English;
import ths.dms.web.utils.ValidateUtil;

public class OrganizationSystemAction extends AbstractAction {
	private static final long serialVersionUID = -2128120975032582873L;

	public static final Long MAX_UPLOAD_SIZE = (long) (Configuration.getMaxImageUploadSize() * 1024 * 1024);
	/** The ap param mgr. */
	private ApParamMgr apParamMgr;

	private ShopMgr shopMgr;

	private OrganizationMgr organizationMgr;

	private Long idSearch;
	private Long id;
	private Integer nodeType;
	private String codeOrName;
	private Long nodeTypeId;
	private OrganizationUnitTypeVO organizationVO;

	/** phuocddh2 declare **/
	private String description;

	private String name;

	private String prefix;

	private String iconUrl;

	private String order;

	private String sort;

	private Integer isManage;

	private List<File> icon;
	private List<String> iconContentType;
	private List<String> iconFileName;

	private List<JETreeNode> lstOrganizationSystem;

	private List<Long> lstOrgIdSort;
	private List<Integer> lstOrgSortNodeOrdinal;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		organizationMgr = (OrganizationMgr) context.getBean("organizationMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			OrganizationSystemFilter<Organization> filter = new OrganizationSystemFilter<Organization>();
			List<Organization> lstOrg = organizationMgr.getListOrganization(filter);
			if (lstOrg != null && lstOrg.size() > 0) {
				id = 1L; // khong hien thi them moi organization
			} else {
				id = 0L; //hien thi them moi organization
			}
		} catch (Exception e) {
			LogUtility.logError(e, "OrganizationSystemAction.execute()" + e.getMessage());
		}
		return SUCCESS;
	}

	public String Search() {
		try {

		} catch (Exception e) {
			LogUtility.logError(e, "OrganizationSystemAction.execute()" + e.getMessage());
		}
		return SUCCESS;
	}

	/** BEGIN VUONGMQ */
	/**
	 * @author vuongmq
	 * @date 11/02/2015 lay cay to chuc de hien thi, table: organization
	 * @return
	 */
	public String getListOrganizationSystem() {
		try {
			lstOrganizationSystem = new ArrayList<JETreeNode>();
			//Shop currentShop = null;
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shopCode = currentUser.getShopRoot().getShopCode();
				//currentShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				if (idSearch != null) {
					OrganizationVO org = organizationMgr.getOrganizationVOById(idSearch);
					OrganizationVO orgParent = organizationMgr.getOrganizationVOByParent();
					/*
					 * OrganizationSystemFilter<OrganizationVO> filter = new
					 * OrganizationSystemFilter<OrganizationVO>();
					 * filter.setId(org.getId()); List<OrganizationVO> lstOrg =
					 * organizationMgr.getListOrganizationSystemVO(filter);
					 */
					List<OrganizationVO> lstOrg = this.getListOrganizationParent(org, orgParent.getId());

					JETreeNode bean = new JETreeNode();
					bean.setId(orgParent.getId().toString());
					bean.setState(ConstantManager.JSTREE_STATE_OPEN);
					JETreeNodeAttr attrNode = new JETreeNodeAttr();
					attrNode.setShop(this.converterShopTreeVO(orgParent, 1));
					bean.setAttributes(attrNode);
					if (ActiveType.RUNNING.getValue().equals(org.getStatus())) {
						if (org.getCode() != null && org.getCode().length() > 0) {
							bean.setText(org.getCode() + " - " + org.getName());
						} else {
							bean.setText(org.getName() + " - " + org.getName());
						}
					} else {
						if (org.getCode() != null && org.getCode().length() > 0) {
							bean.setText(org.getCode() + " - " + org.getName());
						} else {
							bean.setText(org.getName() + " - " + org.getName());
						}
					}
					if (org.getIconUrl() != null && org.getIconUrl().length() > 0) {
						bean.setIconCls(Configuration.getImgServerPath() + org.getIconUrl());
					}
					//phuocdh2 modified
					bean.setIsManage(org.getIsManage());
					lstOrganizationSystem.add(bean);

					if (lstOrg.size() > 0) {
						List<JETreeNode> lstT = this.getTreeNodeByShopParentX(lstOrg, lstOrg.size() - 1, false);
						bean.setChildren(lstT);
					}
					if (bean.getChildren() == null || bean.getChildren().size() == 0) {
						bean.setIconCls("triconline");
					}
				} else if (id != null) {
					OrganizationVO org = organizationMgr.getOrganizationVOById(id);
					/*
					 * if (currentShop != null &&
					 * checkShopInOrgAccessByCode(currentShop.getShopCode())) {
					 * lstOrganizationSystem =
					 * this.getTreeNodeByShop(org.getId()); }
					 */
					if (shopCode != null && checkShopInOrgAccessByCode(shopCode)) {
						lstOrganizationSystem = this.getTreeNodeByShop(org.getId());
					}
				} else {
					OrganizationVO org = organizationMgr.getOrganizationVOByParent();
					JETreeNode cur = new JETreeNode();
					//phuocdh2 modified
					cur.setIsManage(org.getIsManage());
					cur.setId(org.getId().toString());
					cur.setState(ConstantManager.JSTREE_STATE_OPEN);
					JETreeNodeAttr attrNode = new JETreeNodeAttr();
					attrNode.setShop(this.converterShopTreeVO(org, 1));
					cur.setAttributes(attrNode);
					if (org.getCode() != null && org.getCode().length() > 0) {
						cur.setText(org.getCode() + " - " + org.getName());
					} else {
						cur.setText(org.getName() + " - " + org.getName());
					}
					cur.setChildren(this.getTreeNodeByShop(org.getId()));
					if (org.getIconUrl() != null && org.getIconUrl().length() > 0) {
						cur.setIconCls(Configuration.getImgServerPath() + org.getIconUrl());
					}

					lstOrganizationSystem.add(cur);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "OrganizationSystemAction.getListOrganizationSystem()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author vuongmq
	 * @date 12/02/2015 lay cay to chuc: lay parent
	 * @return
	 */
	/*
	 * private List<Shop> getListOrganizationParent(Shop shop, Long shopIdRoot)
	 * { List<Shop> lstShop = new ArrayList<Shop>(); try { while (shop != null)
	 * { // Chi lay den cha la shopIdRoot if (shopIdRoot != null &&
	 * shopIdRoot.equals(shop.getId())){ lstShop.add(shop); shop =
	 * shop.getParentShop(); break; } else { lstShop.add(shop); shop =
	 * shop.getParentShop(); } } } catch (Exception e) { LogUtility.logError(e,
	 * "OrganizationSystemAction.getListOrganizationParent()" + e.getMessage());
	 * } return lstShop; }
	 */
	private List<OrganizationVO> getListOrganizationParent(OrganizationVO shop, Long shopIdRoot) {
		List<OrganizationVO> lstShop = new ArrayList<OrganizationVO>();
		try {
			while (shop != null) {
				// Chi lay den cha la shopIdRoot
				if (shopIdRoot != null && shopIdRoot.equals(shop.getId())) {
					lstShop.add(shop);
					OrganizationVO orgVOParent = organizationMgr.getOrganizationVOById(shop.getParentOrgId());
					shop = orgVOParent;
					break;
				} else {
					lstShop.add(shop);
					OrganizationVO orgVOParent = organizationMgr.getOrganizationVOById(shop.getParentOrgId());
					shop = orgVOParent;
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "OrganizationSystemAction.getListOrganizationParent()" + e.getMessage());
		}
		return lstShop;
	}

	/**
	 * @author vuongmq
	 * @date 12/02/2015 lay cay to chuc: lay shoptree là Organization
	 * @return
	 */
	private ShopTreeVO converterShopTreeVO(OrganizationVO orgVO, Integer parent) {
		ShopTreeVO st = new ShopTreeVO();
		try {
			if (orgVO != null) {
				OrganizationVO org1 = organizationMgr.getOrganizationVOById(orgVO.getId());
				st.setIsManage(org1.getIsManage());
				st.setId(orgVO.getId());
				st.setShopCode(orgVO.getCode());
				st.setShopName(orgVO.getName());
				if (orgVO.getNodeType() != null) {
					if (OrganizationNodeType.SHOP.getValue().equals(orgVO.getNodeType())) {
						st.setNodeType(OrganizationNodeType.SHOP);
					} else {
						st.setNodeType(OrganizationNodeType.STAFF);
					}
				}
				if (orgVO.getStatus() != null) {
					if (ActiveType.RUNNING.getValue().equals(orgVO.getStatus())) {
						st.setStatus(ActiveType.RUNNING);
					} else {
						st.setStatus(ActiveType.STOPPED);
					}
				}
				st.setNodeOrdinal(orgVO.getNodeOrdinal());
				if (parent == 1 && orgVO.getParentOrgId() != null) {
					OrganizationVO orgVOParent = organizationMgr.getOrganizationVOById(orgVO.getParentOrgId());
					st.setParentShop(this.converterShopTreeVO(orgVOParent, 0));
				} else {
					st.setParentShop(null);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "OrganizationSystemAction.converterShopTreeVO()" + e.getMessage());
		}
		return st;
	}

	/**
	 * @author vuongmq
	 * @date 12/02/2015 lay cay to chuc: lay getTreeNodeByShop là Organization
	 * @return
	 */
	private List<JETreeNode> getTreeNodeByShop(Long id) {
		List<JETreeNode> lstOrganizationSystem = new ArrayList<JETreeNode>();
		try {
			OrganizationVO org = organizationMgr.getOrganizationVOById(id);
			OrganizationSystemFilter<OrganizationVO> filter = new OrganizationSystemFilter<OrganizationVO>();
			if (org != null) {
				if (OrganizationNodeType.SHOP.getValue().equals(org.getNodeType())) {
					filter.setId(id);
					List<OrganizationVO> lstOrgStaff = organizationMgr.getListOrganizationSystemVOStaff(filter);
					if (lstOrgStaff != null) {
						List<JETreeNode> lstNodeStaff = this.parseOrganization(lstOrgStaff);
						for (JETreeNode temp : lstNodeStaff) {
							lstOrganizationSystem.add(temp);
						}
					}
				}
				filter = new OrganizationSystemFilter<OrganizationVO>();
				filter.setId(org.getId());
				List<OrganizationVO> lstOrgShop = organizationMgr.getListOrganizationSystemVOShop(filter);
				List<JETreeNode> lstNodeShop = this.parseOrganization(lstOrgShop);
				for (JETreeNode temp : lstNodeShop) {
					if (temp.getAttributes() != null && temp.getAttributes().getShop() != null) {
						lstOrganizationSystem.add(temp);
						Long idOrg = temp.getAttributes().getShop().getId();
						this.getTreeNodeByShop(idOrg);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "OrganizationSystemAction.getTreeNodeByShop()" + e.getMessage());
		}
		return lstOrganizationSystem;
	}

	/**
	 * @author vuongmq
	 * @date 12/02/2015 lay cay to chuc: lay getTreeNodeByShopParentX là
	 *       Organization parent
	 * @return
	 */
	private List<JETreeNode> getTreeNodeByShopParentX(List<OrganizationVO> lstOrg, Integer level, boolean isStaffSearching) throws Exception {
		List<JETreeNode> lstUnitTree = new ArrayList<JETreeNode>();
		OrganizationVO shT = lstOrg.get(level);
		if (level.equals(0)) {
			if (shT != null) {
				lstUnitTree = getTreeNodeByShop(lstOrg.get(level).getId());
			}
			return lstUnitTree;
		}

		lstUnitTree = getTreeNodeByShop(shT.getId());
		List<JETreeNode> lstUnitTreeChild = getTreeNodeByShopParentX(lstOrg, level - 1, isStaffSearching);
		OrganizationVO sh = lstOrg.get(level - 1);
		ShopTreeVO attSh = null;
		for (JETreeNode temp : lstUnitTree) {
			attSh = temp.getAttributes().getShop();
			if (attSh == null) {
				continue;
			}
			if (attSh.getId().equals(sh.getId())) {
				temp.setChildren(lstUnitTreeChild);
				if (level == 1 && !isStaffSearching) {
					temp.setState(ConstantManager.JSTREE_STATE_CLOSE);
				} else {
					temp.setState(ConstantManager.JSTREE_STATE_OPEN);
				}
				break;
			}
		}
		return lstUnitTree;
	}

	/**
	 * @author vuongmq
	 * @date 12/02/2015 lay cay to chuc: lay parseOrganization là Organization
	 *       add vao JETreeNode
	 * @return
	 */
	private List<JETreeNode> parseOrganization(List<OrganizationVO> lst) {
		List<JETreeNode> lstBean = new ArrayList<JETreeNode>();
		try {
			for (OrganizationVO vo : lst) {
				if (vo != null) {
					JETreeNode bean = new JETreeNode();
					JETreeNodeAttr attr = new JETreeNodeAttr();
					attr.setShop(this.converterShopTreeVO(vo, 1));
					bean.setId(vo.getId().toString());
					bean.setAttributes(attr);
					if (vo.getIconUrl() != null && vo.getIconUrl().length() > 0) {
						bean.setIconCls(Configuration.getImgServerPath() + vo.getIconUrl());
					}
					if (ActiveType.RUNNING.getValue().equals(vo.getStatus())) {
						if (vo.getCode() != null && vo.getCode().length() > 0) {
							bean.setText(vo.getCode() + " - " + vo.getName());
						} else {
							bean.setText(vo.getName() + " - " + vo.getName());
						}
					} else {
						if (vo.getCode() != null && vo.getCode().length() > 0) {
							bean.setText(vo.getCode() + " - " + vo.getName());
						} else {
							bean.setText(vo.getName() + " - " + vo.getName());
						}
					}
					if (OrganizationNodeType.SHOP.getValue().equals(vo.getNodeType())) {
						bean.setState(ConstantManager.JSTREE_STATE_CLOSE);
					} else {
						//bean.setState(ConstantManager.JSTREE_STATE_LEAF);
						bean.setState(ConstantManager.JSTREE_STATE_OPEN);
					}
					lstBean.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "OrganizationSystemAction.parseOrganization()" + e.getMessage());
		}
		return lstBean;
	}

	/**
	 * @author vuongmq
	 * @date 12/02/2015 cay to chuc: save Organization dung de cap nhat theo thu
	 *       tu keo tha cua cay nodeOrdinal
	 * @return
	 */
	public String saveOrganization() {
		try {
			resetToken(result);
			result.put(ERROR, false);
			if (lstOrgIdSort == null && lstOrgSortNodeOrdinal == null) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_no_change_drag");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
			}
			organizationMgr.saveOrganization(lstOrgIdSort, lstOrgSortNodeOrdinal, getLogInfoVO());
		} catch (Exception e) {
			LogUtility.logError(e, "OrganizationSystemAction.saveOrganization()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author vuongmq
	 * @date 12/02/2015 cay to chuc: delete deleteShopAndStaffTree
	 * @return
	 */
	public String deleteShopAndStaffTree() {
		resetToken(result);
		result.put(ERROR, false);
		try {
			if (nodeTypeId != null && nodeType != null) {
				Organization org = organizationMgr.getOrganizationById(nodeTypeId);
				if (org == null) {
					if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_not_loai_don_vi");
					} else {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_not_chuc_vu");
					}
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				ShopFilter filter = new ShopFilter();
				List<Shop> lstShopOrg = shopMgr.getListShopOrganization(filter);
				if (lstShopOrg != null && lstShopOrg.size() > 0) {
					if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_don_vi_ap_dung_loai_don_vi");
					} else {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_don_vi_ap_dung_chuc_vu");
					}
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				org.setUpdateUser(currentUser.getUserName());
				org.setUpdateDate(DateUtil.now());
				org.setStatus(ActiveType.DELETED);
				organizationMgr.updateOrganization(org);
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "OrganizationSystemAction.deleteShopAndStaffTree()" + e.getMessage());
		}
		return JSON;
	}

	/** END VUONGMQ */
	/**
	 * search.
	 * 
	 * @return the string
	 * @author liemtpt
	 * @description: load danh sach va tim kiem them loai don vi
	 * @createDate 22/01/2015
	 */
	public String getListOrganizationType() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<OrganizationUnitTypeVO> kPaging = new KPaging<OrganizationUnitTypeVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			OrganizationUnitTypeFilter filter = new OrganizationUnitTypeFilter();
			filter.setCodeOrName(codeOrName);
			filter.setNodeType(OrganizationNodeType.parseValue(nodeType));
			/** Sort grid */
			filter.setOrder(order);
			filter.setSort(sort);
			ObjectVO<OrganizationUnitTypeVO> lst = organizationMgr.getListOrganizationUnitType(filter, kPaging);
			if (lst != null) {
				result.put("total", lst.getkPaging().getTotalRows());
				result.put("rows", lst.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<OrganizationUnitTypeVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.OrganizationSystemAction.getListOrganizationType()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * delete organization type
	 * 
	 * @return the string
	 * @createDate 22/01/2015
	 * @author liemtpt
	 * @description: Xoa loai gia tri (shop|staff).
	 */
	public String deleteOrganizationType() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			if (nodeTypeId != null) {
				OrganizationFilter filter = new OrganizationFilter();
				filter.setStatus(ActiveType.RUNNING.getValue());
				filter.setNodeTypeId(nodeTypeId);
				filter.setNodeType(nodeType);
				List<Organization> lstOrg = organizationMgr.getListOrganizationByFilter(filter);
				if (lstOrg != null && lstOrg.size() > 0) {
					error = true;
					result.put(ERROR, error);
					if (error && StringUtil.isNullOrEmpty(errMsg)) {
						if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_delete_shop_type_exist_node_type_id");
						} else {
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_choose_staff_type_exist_node_type_id");
						}
					}
					result.put("errMsg", errMsg);
					return JSON;
				}
				if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
					ShopType shopType = organizationMgr.getShopTypeById(nodeTypeId);
					if (shopType != null) {
						shopType.setStatus(ActiveType.DELETED);
						organizationMgr.updateShopType(shopType);
						error = false;
					}
				} else {
					StaffType staffType = organizationMgr.getStaffTypeById(nodeTypeId);
					if (staffType != null) {
						staffType.setStatus(ActiveType.DELETED);
						organizationMgr.updateStaffType(staffType);
						error = false;
					}
				}
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.OrganizationSystemAction.deleteOrganizationType()"), createLogErrorStandard(actionStartTime));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * choose organization type
	 * 
	 * @return the string
	 * @author liemtpt ,phuocdh2 modified Chon chuc vu la quan ly mot don vi
	 * @description: tao moi doi tuong cho cay to chuc
	 * @createDate 22/01/2015
	 */
	public String chooseOrganizationType() {
		try {
			boolean error = true;
			//String errMsg = "";
			result.put(ERROR, true);
			if (organizationVO == null) {
				return JSON;
			}
			/** vuongmq; 24/02/2015; xu ly tao organization lan dau tien */
			if (organizationVO != null && organizationVO.getOrganizationId() != null && organizationVO.getOrganizationId() == SIZE_ZERO.longValue()) {
				OrganizationSystemFilter<Organization> filter = new OrganizationSystemFilter<Organization>();
				List<Organization> lstOrg = organizationMgr.getListOrganization(filter);
				if (lstOrg != null && lstOrg.size() > 0) {
					// khong hien thi them moi organization
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_exists_system");
					result.put("errMsg", errMsg);
					return JSON;
				} else {
					//hien thi them moi organization
					organizationMgr.chooseOrganizationType(organizationVO, getLogInfoVO());
					result.put(ERROR, false);
					return JSON;
				}
			}

			Long orgId = organizationVO.getOrganizationId();
			Integer nodeType = organizationVO.getNodeType();
			OrganizationFilter filter = new OrganizationFilter();
			filter.setParentOrgId(orgId);
			filter.setNodeType(nodeType);
			filter.setNodeTypeId(organizationVO.getNodeTypeId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			// check kiem tra 2 don vi, chuc vu cung cha
			List<Organization> lstOrganizationExists = organizationMgr.getListOrganizationByFilter(filter);
			if (lstOrganizationExists != null && lstOrganizationExists.size() > 0) {
				error = true;
				result.put(ERROR, error);
				if (error && StringUtil.isNullOrEmpty(errMsg)) {
					if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_choose_shop_type_exist_node_type_id");
					} else {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_choose_staff_type_exist_node_type_id");
					}
				}
				result.put("errMsg", errMsg);
				return JSON;
			}
			// check da ton tai don vi chuc vu o node cha hay node con chua
			OrganizationFilter filterorg = new OrganizationFilter();
			if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
				filterorg.setOrganizationId(organizationVO.getOrganizationId());
			} else {
				filterorg.setParentOrgId(organizationVO.getOrganizationId());
			}
			filterorg.setNodeType(organizationVO.getNodeType());
			filterorg.setNodeTypeId(organizationVO.getNodeTypeId());
			filterorg.setStatus(ActiveType.RUNNING.getValue());
			// check kiem tra 2 don vi, chuc vu cung cha
			List<Organization> lstOrgParent = organizationMgr.getListOrgExistParentNodeByFilter(filterorg);
			if (lstOrgParent != null && lstOrgParent.size() > 0) {
				error = true;
				result.put(ERROR, error);
				if (error && StringUtil.isNullOrEmpty(errMsg)) {
					if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_choose_shop_type_exist_parent_node_type");
					} else {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_choose_staff_type_exist_parent_node_type");
					}
				}
				result.put("errMsg", errMsg);
				return JSON;
			}
			// check kiem tra 2 don vi, chuc vu cung con
			/*
			 * List<Organization> lstOrgSub =
			 * organizationMgr.getListOrgExistSubNodeByFilter(filterorg);
			 * if(lstOrgSub!=null && lstOrgSub.size()>0){ error = true;
			 * result.put(ERROR, error); if (error &&
			 * StringUtil.isNullOrEmpty(errMsg)) {
			 * if(OrganizationNodeType.SHOP.getValue().equals(nodeType)){ errMsg
			 * = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
			 * "organization_err_choose_shop_type_exist_sub_node_type"); }else{
			 * errMsg =
			 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
			 * "organization_err_choose_staff_type_exist_sub_node_type"); } }
			 * result.put("errMsg", errMsg); return JSON; }
			 */
			//phuocdh2

			organizationMgr.chooseOrganizationType(organizationVO, getLogInfoVO());
			//phuocdh2 modified
			if (organizationVO.getIsManage() != null && organizationVO.getOrganizationId() != null) {
				//Organization org = organizationMgr.getOrganizationById(organizationVO.getNodeTypeId());
				OrganizationFilter filterOrg = new OrganizationFilter();
				//filterOrg.setStatus(1);//Active
				filterOrg.setStatus(ActiveType.RUNNING.getValue());
				filterOrg.setNodeType(organizationVO.getNodeType());
				filterOrg.setNodeTypeId(organizationVO.getNodeTypeId());
				filterOrg.setParentOrgId(organizationVO.getOrganizationId());
				List<Organization> lstOrg = organizationMgr.getListOrganizationByFilter(filterOrg); // lay org dua vao node_type_id va parent_id
				Organization org = new Organization();
				if (lstOrg.size() > 0) {
					org = lstOrg.get(0);
					result.put("organizationId", org.getId());
				} else {
					org = null;
				}
				if (org == null) {
					if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_not_loai_don_vi");
					} else {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
					}
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				if (organizationVO.getNodeTypeId() != null) {
					if (organizationMgr.getShopTypeById(organizationVO.getNodeTypeId()) != null) {
						organizationVO.setName(organizationMgr.getShopTypeById(organizationVO.getNodeTypeId()).getName());
					}
					org.setNodeTypeName(organizationVO.getName());
				}
				org.setIsManage(organizationVO.getIsManage());
				org.setUpdateUser(currentUser.getUserName());
				org.setUpdateDate(DateUtil.now());
				org.setNodeTypeName(organizationVO.getName());
				organizationMgr.updateOrganization(org);
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.OrganizationSystemAction.chooseOrganizationType()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/** BEGIN PHUOCDH2 **/

	/**
	 * saveShopType: Them moi hay cap nhat doi tuong.
	 * 
	 * @return the string
	 * @author liemtpt
	 * @description: tao moi hay cap nhat loai don vi
	 * @createDate 22/01/2015
	 */
	public String saveShopType() {
		resetToken(result);
		// validate
		if (StringUtil.isNullOrEmpty(name)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_shop_type_not_exist_node_type_id"));
			return JSON;
		}
		Integer numberRow = 0;
		try {
			ShopType shopType = new ShopType();
			String iconFileStorePath = "";
			if (icon != null && icon.size() > 0 && iconFileName != null && iconFileName.size() > 0) {
				/**
				 * vuongmq; 05/03/2015; check file đung dinh dang image, size
				 * 10mb
				 */
				errMsg = this.checkFormatUploadImage(icon.get(0));
				if (errMsg != null && errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				iconFileStorePath = this.storeFileOnDisk(icon.get(0), iconFileName.get(0));
			}
			if (nodeTypeId != null && nodeTypeId > 0) {
				/** cap nhat shopType */
				//validate name 
				numberRow = organizationMgr.getCountByPrefixShop(name);
				shopType = organizationMgr.getShopTypeById(nodeTypeId);
				if (shopType == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_not_loai_don_vi"));
					return JSON;
				}
				if (name != null && name.equals(shopType.getName())) {
					if (numberRow != null && numberRow >= 2) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_prefix_exist"));
						return JSON;
					}
				} else {
					if (numberRow != null && numberRow >= 1) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_prefix_exist"));
						return JSON;
					}
				}
				shopType.setName(name);
				shopType.setPrefix(prefix);
				if (!StringUtil.isNullOrEmpty(prefix)) {
					shopType.setNameText(Unicode2English.codau2khongdau(name) + " - " + prefix);
				} else {
					shopType.setNameText(Unicode2English.codau2khongdau(name));
				}
				shopType.setDescription(description);
				/**
				 * cap nhat shopType; thi truong hop hinh khac null moi cho cap
				 * nhat lai
				 */
				if (iconFileStorePath != null && iconFileStorePath.length() > 0) {
					shopType.setIconUrl(iconFileStorePath);
				}
				shopType.setUpdateDate(DateUtil.now());
				shopType.setUpdateUser(currentUser.getUserName());
				organizationMgr.updateShopType(shopType);
				if (shopType.getId() != null && shopType.getId() > 0) {
					result.put(ERROR, false);
					return JSON;
				}
			} else {
				/** them moi shopType */
				// validate name
				numberRow = organizationMgr.getCountByPrefixShop(name);
				if (numberRow != null && numberRow >= 1) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_prefix_exist"));
					return JSON;
				}
				shopType.setName(name);
				shopType.setPrefix(prefix);
				if (!StringUtil.isNullOrEmpty(prefix)) {
					shopType.setNameText(Unicode2English.codau2khongdau(name) + " - " + prefix);
				} else {
					shopType.setNameText(Unicode2English.codau2khongdau(name));
				}
				shopType.setDescription(description);
				shopType.setIconUrl(iconFileStorePath);
				shopType.setCreateDate(DateUtil.now());
				shopType.setCreateUser(currentUser.getUserName());
				shopType = organizationMgr.createShopType(shopType);
				if (shopType.getId() != null && shopType.getId() > 0) {
					result.put(ERROR, false);
					return JSON;
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "OrganizationSystemAction.saveShopType()" + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * checkIsManageStaffTree: Danh dau nhan vien la quan ly.
	 * 
	 * @return the string
	 * @author phuocdh2
	 * @description: danh dau hay bo dau mot nhan vien la quan ly
	 * @createDate 09/03/2015
	 */
	public String checkIsManageStaffTree() {
		resetToken(result);
		result.put(ERROR, false);
		try {
			if (nodeTypeId != null && nodeType != null) {
				Organization org = organizationMgr.getOrganizationById(nodeTypeId);
				if (org == null) {
					if (OrganizationNodeType.SHOP.getValue().equals(nodeType)) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_not_loai_don_vi");
					} else {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_not_chuc_vu");
					}
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				if (isManage != null) {
					org.setIsManage(isManage);
				} else {
					org.setIsManage(null);
				}
				org.setUpdateUser(currentUser.getUserName());
				org.setUpdateDate(DateUtil.now());
				organizationMgr.updateOrganization(org);
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "OrganizationSystemAction.checkIsManageStaffTree()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * saveStaffType: Them moi hay cap nhat chuc vu
	 * 
	 * @return the string
	 * @author phuocdh2
	 * @description: them moi cap nhat chuc vu voi icon di kem
	 * @createDate 12/02/2015
	 */
	public String saveStaffType() {
		resetToken(result);
		// validate
		if (StringUtil.isNullOrEmpty(name)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_staff_type_not_exist_node_type_id"));
			return JSON;
		}
		Integer numberRow = 0;
		try {

			StaffType staffType = null;
			String iconFileStorePath = "";
			if (icon != null && icon.size() > 0 && iconFileName != null && iconFileName.size() > 0) {
				/**
				 * vuongmq; 05/03/2015; check file đung dinh dang image, size
				 * 10mb
				 */
				errMsg = this.checkFormatUploadImage(icon.get(0));
				if (errMsg != null && errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				iconFileStorePath = this.storeFileOnDisk(icon.get(0), iconFileName.get(0));
			}
			if (nodeTypeId != null && nodeTypeId > 0) {
				//validate prefix
				numberRow = organizationMgr.getCountByPrefixStaff(name);
				staffType = organizationMgr.getStaffTypeById(nodeTypeId);
				if (staffType == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_not_chuc_vu"));
					return JSON;
				}
				if (name != null && name.equals(staffType.getName())) {
					if (numberRow != null && numberRow >= 2) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_prefix_exist"));
						return JSON;
					}
				} else {
					if (numberRow != null && numberRow >= 1) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_prefix_exist"));
						return JSON;
					}
				}
				staffType.setName(name);
				staffType.setPrefix(prefix);
				if (!StringUtil.isNullOrEmpty(prefix)) {
					staffType.setNameText(Unicode2English.codau2khongdau(name) + " - " + prefix);
				} else {
					staffType.setNameText(Unicode2English.codau2khongdau(name));
				}
				staffType.setDescription(description);
				if (!StringUtil.isNullOrEmpty(iconFileStorePath)) {
					staffType.setIconUrl(iconFileStorePath);
				}
				staffType.setUpdateDate(DateUtil.now());
				staffType.setUpdateUser(currentUser.getUserName());
				organizationMgr.updateStaffType(staffType);
				result.put(ERROR, false);
			} else {
				//validate name
				numberRow = organizationMgr.getCountByPrefixStaff(name);
				if (numberRow != null && numberRow >= 1) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_err_prefix_exist"));
					return JSON;
				}
				staffType = new StaffType();
				staffType.setName(name);
				staffType.setPrefix(prefix);
				if (!StringUtil.isNullOrEmpty(prefix)) {
					staffType.setNameText(Unicode2English.codau2khongdau(name) + " - " + prefix);
				} else {
					staffType.setNameText(Unicode2English.codau2khongdau(name));
				}
				staffType.setDescription(description);
				staffType.setIconUrl(iconFileStorePath);
				staffType.setCreateDate(DateUtil.now());
				staffType.setCreateUser(currentUser.getUserName());
				staffType = organizationMgr.createStaffType(staffType);
				if (staffType.getId() != null && staffType.getId() > 0) {
					result.put(ERROR, false);
					return JSON;
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "OrganizationSystemAction.saveStaffType()" + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * @author vuongmq
	 * @description: check file đung dinh dang image, size 10mb
	 * @createDate 05/03/2015
	 */
	private String checkFormatUploadImage(File receivedFile) {
		StringBuilder err = new StringBuilder();
		if (receivedFile != null && receivedFile.length() > 0) {
			if (FileUtility.getMime(receivedFile) == null || (FileUtility.getMime(receivedFile) != null && Configuration.getImageUploadFileSupport().indexOf(FileUtility.getMime(receivedFile)) == -1)) {
				String iconNotFormatStr = StringUtil.getFormatSupport(Configuration.getImageUploadFileSupport());
				err.append(R.getResource("organization_icon_not_format")).append(" ").append(iconNotFormatStr);
			}
			if (receivedFile.length() > MAX_UPLOAD_SIZE) {
				if (err.length() > 0) {
					err.append(" ").append(R.getResource("organization_size_image_maximum_upload"));
				} else {
					err.append(R.getResource("organization_size_image_maximum_upload"));
				}
			}
		}
		return err.toString();
	}

	/**
	 * storeFileOnDisk: luu hinh anh icon da upload
	 * 
	 * @param receivedFile
	 * @param receivedFileName
	 * @return the string
	 * @author phuocdh2
	 * @description: luu hinh anh icon da upload
	 * @createDate 12/02/2015
	 */
	private String storeFileOnDisk(File receivedFile, String receivedFileName) {
		String fileName = "";
		try {
			String imageFolder = Configuration.getStoreStaticPath();
			Calendar cal = Calendar.getInstance();
			String newFileStr = imageFolder + "/iconOrganization";
			if (!FileUtility.isFolderExist(newFileStr)) {
				FileUtility.createDirectory(newFileStr);
			}
			fileName = Long.toString(cal.getTimeInMillis()) + receivedFileName;
			String fileNameTmp = (newFileStr.endsWith("/") ? "" : "/") + fileName;
			String newFilePath = newFileStr + fileNameTmp;
			File newFile = new File(newFilePath);

			Boolean resThumb = ImageUtility.createThumbnail(receivedFile.getAbsolutePath(), newFilePath, ImageUtility.INT_THUMB_SIZE_128, ImageUtility.INT_THUMB_SIZE_128);
			if (resThumb == null || !resThumb) {
				FileUtils.copyFile(receivedFile, newFile);
			}
		} catch (IOException ioe) {
			LogUtility.logError(ioe, "OrganizationSystemAction.storeFileOnDisk()" + ioe.getMessage());
		}
		if (!StringUtil.isNullOrEmpty(fileName)) {
			fileName = "/iconOrganization/" + fileName;
		}
		return fileName;

	}

	/** END PHUOCDH2 **/

	public List<JETreeNode> getLstOrganizationSystem() {
		return lstOrganizationSystem;
	}

	public void setLstOrganizationSystem(List<JETreeNode> lstOrganizationSystem) {
		this.lstOrganizationSystem = lstOrganizationSystem;
	}

	public Integer getNodeType() {
		return nodeType;
	}

	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}

	public String getCodeOrName() {
		return codeOrName;
	}

	public void setCodeOrName(String codeOrName) {
		this.codeOrName = codeOrName;
	}

	public ApParamMgr getApParamMgr() {
		return apParamMgr;
	}

	public void setApParamMgr(ApParamMgr apParamMgr) {
		this.apParamMgr = apParamMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public OrganizationMgr getOrganizationMgr() {
		return organizationMgr;
	}

	public void setOrganizationMgr(OrganizationMgr organizationMgr) {
		this.organizationMgr = organizationMgr;
	}

	public Long getIdSearch() {
		return idSearch;
	}

	public void setIdSearch(Long idSearch) {
		this.idSearch = idSearch;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNodeTypeId() {
		return nodeTypeId;
	}

	public void setNodeTypeId(Long nodeTypeId) {
		this.nodeTypeId = nodeTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public OrganizationUnitTypeVO getOrganizationVO() {
		return organizationVO;
	}

	public void setOrganizationVO(OrganizationUnitTypeVO organizationVO) {
		this.organizationVO = organizationVO;
	}

	/** DECLARE PHUOCDH2 **/

	public List<File> getIcon() {
		return icon;
	}

	public void setIcon(List<File> icon) {
		this.icon = icon;
	}

	public List<String> getIconContentType() {
		return iconContentType;
	}

	public void setIconContentType(List<String> iconContentType) {
		this.iconContentType = iconContentType;
	}

	public List<String> getIconFileName() {
		return iconFileName;
	}

	public void setIconFileName(List<String> iconFileName) {
		this.iconFileName = iconFileName;
	}

	public List<Long> getLstOrgIdSort() {
		return lstOrgIdSort;
	}

	public void setLstOrgIdSort(List<Long> lstOrgIdSort) {
		this.lstOrgIdSort = lstOrgIdSort;
	}

	public List<Integer> getLstOrgSortNodeOrdinal() {
		return lstOrgSortNodeOrdinal;
	}

	public void setLstOrgSortNodeOrdinal(List<Integer> lstOrgSortNodeOrdinal) {
		this.lstOrgSortNodeOrdinal = lstOrgSortNodeOrdinal;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public Integer getIsManage() {
		return isManage;
	}

	public void setIsManage(Integer isManage) {
		this.isManage = isManage;
	}

	/** END DECLARE PHUOCDH2 **/

}

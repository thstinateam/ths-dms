/*
 * 
 */
package ths.dms.web.action.po;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.PoMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.PoProductType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.WarehouseType;
import ths.dms.core.entities.filter.PoVnmFilterBasic;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.PoVnmStockInVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;


/**
 * The Class POStockInAction.
 */
public class POStockInAction extends AbstractAction{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	private StaffMgr staffMgr;
	private PoMgr poMgr;
	private StockMgr stockMgr;
	
	private Staff staff;
	private Shop shop;
	private String shopCode;
	private String staffCode;  //shop_code
	
	private Long poVnmId;
	private String poCoNumber;
	private String PoCode;
	private String saleOrderNumber;
	private String poAutoNumber;
	private String invoiceNumber;
	private String titleDetail;
	private Integer typePo;

	private List<PoVnm> lstPovnm;
	private List<PoVnmStockInVO> lstPoVnmStockInVO;
	private List<PoVnmDetailLotVO> lstPovnmDetail;
	
	private Integer status;   //trang thai
	private String fromDate;  // ngay bat dau
	private String toDate;    // ngay ket thuc 
	private String amount_check; //gia tri kiem soat
	private String quantity_check; //so luong kiem soat
	private String discount_check; //so luong kiem soat
	private BigDecimal discount; //chiet khau
	private String quantity;
	private BigDecimal totalmoney;
	private int totalcount;
	private BigDecimal pay;
	private BigDecimal vat;
	private Long shopId;
	private String poVnmDate;
	private String invoiceDate;
	private String deliveryDate;
	
	private String poVNMCode;
	private Long wareHouseId;
	private Long productId;
	
	
	public String getDiscount_check() {
		return discount_check;
	}

	public void setDiscount_check(String discount_check) {
		this.discount_check = discount_check;
	}

	public String getPoVNMCode() {
		return poVNMCode;
	}

	public void setPoVNMCode(String poVNMCode) {
		this.poVNMCode = poVNMCode;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public PoMgr getPoMgr() {
		return poMgr;
	}

	public void setPoMgr(PoMgr poMgr) {
		this.poMgr = poMgr;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getPoVnmId() {
		return poVnmId;
	}

	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}

	public String getPoCoNumber() {
		return poCoNumber;
	}

	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}

	public String getPoCode() {
		return PoCode;
	}

	public void setPoCode(String poCode) {
		PoCode = poCode;
	}

	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}

	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getTitleDetail() {
		return titleDetail;
	}

	public void setTitleDetail(String titleDetail) {
		this.titleDetail = titleDetail;
	}

	public List<PoVnm> getLstPovnm() {
		return lstPovnm;
	}

	public void setLstPovnm(List<PoVnm> lstPovnm) {
		this.lstPovnm = lstPovnm;
	}

	public List<PoVnmDetailLotVO> getLstPovnmDetail() {
		return lstPovnmDetail;
	}

	public void setLstPovnmDetail(List<PoVnmDetailLotVO> lstPovnmDetail) {
		this.lstPovnmDetail = lstPovnmDetail;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getAmount_check() {
		return amount_check;
	}

	public void setAmount_check(String amount_check) {
		this.amount_check = amount_check;
	}

	public String getQuantity_check() {
		return quantity_check;
	}

	public void setQuantity_check(String quantity_check) {
		this.quantity_check = quantity_check;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTotalmoney() {
		return totalmoney;
	}

	public void setTotalmoney(BigDecimal totalmoney) {
		this.totalmoney = totalmoney;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public BigDecimal getPay() {
		return pay;
	}

	public void setPay(BigDecimal pay) {
		this.pay = pay;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getPoVnmDate() {
		return poVnmDate;
	}

	public void setPoVnmDate(String poVnmDate) {
		this.poVnmDate = poVnmDate;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		poMgr = (PoMgr) context.getBean("poMgr");
		stockMgr = (StockMgr) context.getBean("stockMgr");
	}
	
	public boolean checkExistShopOfUserLogin(){
		actionStartTime = DateUtil.now();
		if (currentUser != null && currentUser.getShopRoot() != null) {
			try {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			} catch (BusinessException e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.POStockInAction.checkExistShopOfUserLogin()"), createLogErrorStandard(actionStartTime));
				return false;
			}
		}
		if (shop == null) {
			return false;
		} else {
			return true;
		}
	}
	
	/** @author vuongmq
	 * @since 09 September, 2014
	 * @description Nhập hàng về NPP
	 */
	@Override
	public String execute() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			boolean isExistShop = this.checkExistShopOfUserLogin();
			if (isExistShop) {
				shopCode = shop.getShopCode();
				shopId = shop.getId();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.POStockInAction.execute()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * Gets the info.
	 * @author CuongND
	 * @return the info
	 */
	/** @author vuongmq
	 * @since 09 September, 2014
	 * @description Lấy danh sách PO VNM
	 */
	public String getInfo() {
		actionStartTime = DateUtil.now();
		try {
			lstPoVnmStockInVO = new ArrayList<PoVnmStockInVO>();
			//boolean isPermission = checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR);
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop) {
				PoVnmFilter filter = new PoVnmFilter();
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					Shop sh = shopMgr.getShopByCode(shopCode);
					if (sh != null) {
						filter.setShopId(sh.getId());
					}
				} else {
					if (shop != null) {
						filter.setShopId(shop.getId());
					}
				}
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					filter.setFromDate(DateUtil.parse(fromDate,DateUtil.DATE_FORMAT_DDMMYYYY));
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					filter.setToDate(DateUtil.parse(toDate,DateUtil.DATE_FORMAT_DDMMYYYY));
				}
				if (!StringUtil.isNullOrEmpty(invoiceNumber)) {
					filter.setInvoiceNumber(invoiceNumber);
				}
				if (!StringUtil.isNullOrEmpty(poCoNumber)) {
					filter.setPoCoNumber(poCoNumber);
				}
				if (!StringUtil.isNullOrEmpty(saleOrderNumber)) {
					filter.setOrderNumber(saleOrderNumber);
				}
				if (!StringUtil.isNullOrEmpty(poAutoNumber)) {
					filter.setPoAutoNumber(poAutoNumber);
				}
				if (typePo != null && typePo > ActiveType.DELETED.getValue()) {
					filter.setPoType(PoType.parseValue(typePo));
				} else {
					List<Integer> lstPoType = new ArrayList<Integer>();
					lstPoType.add(PoType.PO_CONFIRM.getValue());
					lstPoType.add(PoType.RETURNED_SALES_ORDER.getValue());
					filter.setLstPoVnmType(lstPoType);
				}
				if (status != null && status > ActiveType.DELETED.getValue()) {
					filter.setPoStatus(PoVNMStatus.parseValue(status));
				}
				filter.setHasFindChildShop(true); 
				filter.setIsLikePoNumber(true);
				ObjectVO<PoVnmStockInVO> listVoPoconfirm = poMgr.getListObjectVOPoConfirm(filter);
				if (listVoPoconfirm != null) {
					lstPoVnmStockInVO = listVoPoconfirm.getLstObject();
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.POStockInAction.getInfo()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * Gets the detail.
	 * @return the detail
	 * @throws NumberFormatException the number format exception
	 * @throws BusinessException the business exception
	 */
	/**
	 * @author vuongmq
	 * @date 10/08/2015
	 * @description Lay thong tin cua record PO_VNM
	 */
	public String getDetail() {
		actionStartTime = DateUtil.now();
		try {
			totalcount = 0;
			totalmoney = BigDecimal.ZERO;
			pay = BigDecimal.ZERO;
			vat = BigDecimal.ZERO;
			discount = BigDecimal.ZERO;
			/*staff = getStaffByCurrentUser();
			if(staff==null){
				return LIST;
			}
			shop = staff.getShop();	*/
			if (currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			PoVnm vnm =  poMgr.getPoVnmById(poVnmId);
			if (vnm == null) {
				return LIST;
			}
			titleDetail = R.getResource("povnm_title_detail_input");
			if (vnm.getType() != null) {
				typePo = vnm.getType().getValue();
				if (PoType.RETURNED_SALES_ORDER.equals(vnm.getType())) {
					titleDetail = R.getResource("povnm_title_detail_output");
				}
			}
			if (vnm.getStatus() != null) {
				status = vnm.getStatus().getValue();
			}
			//poVnmDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_STR);
			if (vnm.getPoVnmDate() != null) {
				poVnmDate = DateUtil.toDateString(vnm.getPoVnmDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (vnm.getDeliveryDate() != null) {
				deliveryDate = DateUtil.toDateString(vnm.getDeliveryDate(), DateUtil.DATETIME_FORMAT_STR);
			}
			poCoNumber = vnm.getSaleOrderNumber();
			invoiceNumber = vnm.getInvoiceNumber();
			if (vnm.getQuantityCheck() != null) {
				quantity_check = vnm.getQuantityCheck().toString();
			}
			if (vnm.getAmountCheck() != null) {
				amount_check = vnm.getAmountCheck().toString();
			}
			totalcount = vnm.getQuantity();
			totalmoney = vnm.getAmount();
			discount = vnm.getDiscount();
			if (vnm.getDiscount() == null) {
				vnm.setDiscount(BigDecimal.ZERO);
			}
			pay = (vnm.getAmount().subtract(vnm.getDiscount())).multiply(BigDecimal.valueOf(1.1));
//			vat = vnm.getAmount().multiply(BigDecimal.valueOf(10)).divide(BigDecimal.valueOf(110),0, RoundingMode.HALF_UP);			
			vat = (vnm.getAmount().subtract(vnm.getDiscount())).multiply(BigDecimal.valueOf(0.1));
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.po.POStockInAction.getDetail()"), createLogErrorStandard(actionStartTime));
		}
		return LIST;
	}
	
	/**
	 *@author tientv
	 */
	/**
	 * @author vuongmq
	 * @date 10/08/2015
	 * @description Lay danh sach detail cua PO_VNM_DETAIL
	 */
	public String getInforDetail() {
		actionStartTime = DateUtil.now();
		try {
			vat = BigDecimal.ZERO;
			lstPovnmDetail = new ArrayList<PoVnmDetailLotVO>();	
			ObjectVO<PoVnmDetailLotVO> temp = poMgr.getListPoVnmDetailLotVOByPoVnmIdStockIn(null, poVnmId);
			if (temp != null && temp.getLstObject() != null && temp.getLstObject().size() > 0) {
				lstPovnmDetail = temp.getLstObject();
				for (int i = 0, sz = lstPovnmDetail.size(); i < sz; i++) {
					PoVnmDetailLotVO item = lstPovnmDetail.get(i);
					if (item != null && item.getWareHouseId() == null) {
						StockTotalFilter filter = new StockTotalFilter();
						filter.setOwnerId(item.getShopId());
						if (PoProductType.SALE.getValue().equals(item.getProductType())) {
							filter.setWarehouseType(WarehouseType.SALES);
						} else if (PoProductType.PROMOTION.getValue().equals(item.getProductType()) || PoProductType.POSM.getValue().equals(item.getProductType())) {
							filter.setWarehouseType(WarehouseType.PROMOTION);
						}
						filter.setStatus(ActiveType.RUNNING.getValue());
						List<Warehouse> lstWarehouse = stockMgr.getListWarehouseByFilter(filter);
						if (lstWarehouse != null && lstWarehouse.size() > 0) {
							// set danh sach Kho
							item.setLstWarehouse(lstWarehouse);
							// lay quantity stock cua kho dau tien
							StockTotalFilter filter2 = new StockTotalFilter();
							filter2.setWarehouseId(lstWarehouse.get(0).getId());
							filter2.setProductId(item.getProductId());
							filter2.setOwnerType(StockObjectType.SHOP);
							if (lstWarehouse.get(0).getShop() != null) {
								filter2.setOwnerId(lstWarehouse.get(0).getShop().getId());
							}
							filter2.setStatus(ActiveType.RUNNING.getValue());
							StockTotal stockTotal = stockMgr.getStockTotalByEntities(filter2);
							// set Ton kho quantity cua Kho dau tien
							if (stockTotal != null) {
								item.setQuantityStock(stockTotal.getQuantity());
							}
						}
					} else {
						Warehouse warehouse = commonMgr.getEntityById(Warehouse.class, item.getWareHouseId());
						List<Warehouse> lstWarehouse = new ArrayList<Warehouse>();
						lstWarehouse.add(warehouse);
						if (lstWarehouse != null && lstWarehouse.size() > 0) {
							item.setLstWarehouse(lstWarehouse);
						}
					}
				}
			}				
			result.put("rows", lstPovnmDetail);			
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.po.POStockInAction.getInforDetail()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * StockinAction.
	 * @author CuongND
	 * @since Aug 22, 2012
	 * Stockin action.
	 */
	/**
	 * StockinAction.
	 * @author vuongmq
	 * @since Aug 11, 2015
	 * Xu ly nhap/ tra hang
	 */
	public String getStockinAction() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		errMsg = "";
		try {
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return JSON;
			}
			if (poVnmId == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("povnm_ma_po_confirm_khong_ton_tai"));
				return JSON;
			}
			PoVnm poVnmTmp = poMgr.getPoVnmById(poVnmId);
			if (poVnmTmp == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("povnm_ma_po_confirm_khong_ton_tai_he_thong"));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(errMsg) && status == null) {
				errMsg = R.getResource("povnm_trang_thai_xu_ly_khong_ton_tai");
			}
			if (StringUtil.isNullOrEmpty(errMsg) && StringUtil.isNullOrEmpty(poCoNumber)) {
				errMsg = R.getResource("povnm_so_don_hang_khong_de_trong");
			}
			if (StringUtil.isNullOrEmpty(errMsg) && StringUtil.isNullOrEmpty(invoiceNumber)) {
				errMsg = R.getResource("povnm_so_hoa_don_khong_de_trong");
			}
			if (StringUtil.isNullOrEmpty(errMsg) && lstPovnmDetail == null || (lstPovnmDetail != null && lstPovnmDetail.size() < 0)) {
				errMsg = R.getResource("povnm_ds_sp_khong_ton_tai");
			}
			Date deliveryDateTmp = null;
			if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(deliveryDate)) {
				deliveryDateTmp = DateUtil.parse(deliveryDate, DateUtil.DATE_FORMAT_NOW);
				if (deliveryDateTmp == null) {
					if (PoType.PO_CONFIRM.equals(poVnmTmp.getType())) {
						errMsg = R.getResource("povnm_thoi_gian_xe_ve");
					} else if (PoType.RETURNED_SALES_ORDER.equals(poVnmTmp.getType())) {
						errMsg = R.getResource("povnm_thoi_gian_xe_di");
					}
				}
				/*if (StringUtil.isNullOrEmpty(errMsg) && DateUtil.compareDateWithoutTimeFull(DateUtil.now(), deliveryDateTmp) > 0) {
					if (PoVNMStatus.IMPORTING.getValue().equals(status)) {
						errMsg = R.getResource("povnm_thoi_gian_xe_ve_less_now_err");
					} else {
						errMsg = R.getResource("povnm_thoi_gian_xe_di_less_now_err");
					}
				}*/
			}
			Date invoiceDateTmp = null;
			if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(invoiceDate)) {
				invoiceDateTmp = DateUtil.parse(invoiceDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (invoiceDateTmp == null) {
					errMsg = R.getResource("povnm_ngay_tao_hoa_don_ko_dung_dinh_dang");
				}
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				if (PoVNMStatus.IMPORTED.equals(poVnmTmp.getStatus())) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_STOCKIN_FINISH);
				} else if(PoVNMStatus.SUSPEND.equals(poVnmTmp.getStatus())) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_STOCKIN_SUSPEND);
				}
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				if (StringUtil.isNullOrEmpty(discount_check)) {
					if (discount == null) {
						discount = BigDecimal.ZERO;
					}
				} else {
					discount = new BigDecimal(discount_check);
				}
				//poMgr.updateInputPoVnm(shop.getId(), new Long(poVnmId), poCoNumber, invoiceNumber, new Integer(quantity_check),new BigDecimal(amount_check), discount,currentUser.getUserName());
				PoVnmFilterBasic<PoVnm> filter = new PoVnmFilterBasic<PoVnm>();
				filter.setPoVnmId(poVnmId);
				filter.setStatus(status); // trang thai: 1: da nhap/tra mot phan; 2: da nhap/tra
				filter.setPoCoNumber(poCoNumber);
				filter.setInvoiceNumber(invoiceNumber);
				filter.setDiscount(discount);
				filter.setUserName(currentUser.getUserName());
				filter.setInvoiceDate(invoiceDateTmp);
				filter.setDeliveryDate(deliveryDateTmp);
				filter.setLstPovnmDetail(lstPovnmDetail);
				poMgr.updateInputPoVnm(filter);
				result.put(ERROR, false);
			} else {
				result.put(ERROR, true);
			    result.put("errMsg", errMsg);
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			String s = e.getMessage();
			if (s.startsWith("KHONG_CO_WAREHOUSE-")) {
				s = s.replaceFirst("KHONG_CO_WAREHOUSE-", "");
				result.put("errMsg", R.getResource("povnm_khong_co_kho") + " " + R.getResource("common.trang.dong") + " " + s);
			} else if (s.startsWith("KHONG_CO_STOCKTOTAL-")) {
				s = s.replaceFirst("KHONG_CO_STOCKTOTAL-", "");
				result.put("errMsg", R.getResource("povnm_khong_co_ton_kho") + " " + R.getResource("common.trang.dong") + " " + s);
			} else if (s.startsWith("TON_KHO_KHONG_DU-")) {
				s = s.replaceFirst("TON_KHO_KHONG_DU-", "");
				result.put("errMsg", R.getResource("povnm_khong_du_ton_kho") + " " + R.getResource("common.trang.dong") + " " + s);
			} else if (s.startsWith("VUOT_QUA_SO_LUONG_NHAP-")) {
				s = s.replaceFirst("VUOT_QUA_SO_LUONG_NHAP-", "");
				result.put("errMsg", R.getResource("povnm_vuot_nhap_hang") + " " + R.getResource("common.trang.dong") + " " + s);
			} else if (s.startsWith("VUOT_QUA_SO_LUONG_TRA-")) {
				s = s.replaceFirst("VUOT_QUA_SO_LUONG_TRA-", "");
				result.put("errMsg", R.getResource("povnm_vuot_tra_hang") + " " + R.getResource("common.trang.dong") + " " + s);
			} else if (s.startsWith("CONG_NO_CHUA_XU_LY-")) {
				s = s.replaceFirst("CONG_NO_CHUA_XU_LY-", "");
				result.put("errMsg", R.getResource("povnm_cong_no_chua_xu_ly"));
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.POStockInAction.getStockinAction()"), createLogErrorStandard(actionStartTime));
			return JSON;
		}
	    return JSON;
	}
	
	/**
	 * removePOConfirm.
	 * @author vuongmq
	 * @since Aug 11, 2015
	 * Huy don hang nhap/ tra hang
	 */
	public String removePOConfirm() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return JSON;
			}
			PoVnm tmp = poMgr.getPoVnmById(poVnmId);
			if (tmp.getStatus().getValue().equals(PoVNMStatus.IMPORTED.getValue())) {
				errMsg =  ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_STOCKIN_FINISH);
			} else if (tmp.getStatus().getValue().equals(PoVNMStatus.SUSPEND.getValue())) {
				errMsg =  ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_STOCKIN_SUSPEND);
			} else {
				if (discount == null) {
					discount = BigDecimal.ZERO;
				}
				//poMgr.updateInputPoVnm(shop.getId(), new Long(poVnmId), poCoNumber, invoiceNumber, new Integer(quantity_check),new BigDecimal(amount_check), discount,staff.getStaffCode());
				poMgr.removePOConfirm(poVnmId);
				error = false;
			}			
		} catch(Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.POStockInAction.removePOConfirm()"), createLogErrorStandard(actionStartTime));
			return JSON;
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}
	
	
	/**
	 * warehouseStockTotal.
	 * @author vuongmq
	 * @since Aug 11, 2015
	 * Xu ly  lay ton kho ung voi san pham cua kho
	 */
	public String warehouseStockTotal() throws NumberFormatException, BusinessException{
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return JSON;
			}
			StockTotalFilter filter = new StockTotalFilter();
			filter.setWarehouseId(wareHouseId);
			filter.setProductId(productId);
			filter.setOwnerType(StockObjectType.SHOP);
			filter.setOwnerId(shopId);
			filter.setStatus(ActiveType.RUNNING.getValue());
			StockTotal stockTotal = stockMgr.getStockTotalByEntities(filter);
			result.put("stockTotal", stockTotal);
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.POStockInAction.warehouseStockTotal()"), createLogErrorStandard(actionStartTime));
			return JSON;
		}
	    return JSON;
	}

	public Integer getTypePo() {
		return typePo;
	}

	public void setTypePo(Integer typePo) {
		this.typePo = typePo;
	}

	public Long getWareHouseId() {
		return wareHouseId;
	}

	public void setWareHouseId(Long wareHouseId) {
		this.wareHouseId = wareHouseId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public List<PoVnmStockInVO> getLstPoVnmStockInVO() {
		return lstPoVnmStockInVO;
	}

	public void setLstPoVnmStockInVO(List<PoVnmStockInVO> lstPoVnmStockInVO) {
		this.lstPoVnmStockInVO = lstPoVnmStockInVO;
	}
}

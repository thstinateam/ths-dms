/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.action.po.manual;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ths.dms.core.business.PoManualMgr;
import ths.dms.core.business.PoMgr;
import ths.dms.core.entities.Po;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActionType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoManualType;
import ths.dms.core.entities.enumtype.PoObjectType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.filter.PoManualFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PoManualProductVO;
import ths.dms.core.entities.vo.PoManualVOSave;
import ths.dms.core.entities.vo.PoVnmManualVO;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Mo ta class POManualCreateAction.java
 * @author vuongmq
 * @since Dec 4, 2015
 */
public class POManualCreateAction extends AbstractAction {

	/** field serialVersionUID  field long */
	private static final long serialVersionUID = 1L;
	
	private Long shopId;
	private Long poId; // dung edit po
	private Long warehouseId;
	private Long poVnmId;
	private String shopCode;
	private String fromDate;
	private String toDate;
	private String requestDate;
	private String note;
	private String poAutoNumber; // asn
	private String orderNumber;
	private Integer type;
	private Integer valueEdit;
	
	private Shop shop;
	
	private List<PoManualProductVO> lstSaleProduct;
	
	private PoManualMgr poManualMgr;
	private PoMgr poMgr;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		poManualMgr = (PoManualMgr) context.getBean("poManualMgr");
		poMgr = (PoMgr) context.getBean("poMgr");
	}

	@Override
	public String execute() {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				shopId = shop.getId();
			}
			fromDate = DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualCreateAction.execute()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * Xu ly saveOrderManual
	 * @author vuongmq
	 * @return String
	 * @since Dec 4, 2015
	 */
	public String saveOrderManual() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			String msg = this.checkPoManualSave();
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			PoManualVOSave poManualVOSave = new PoManualVOSave();
			poManualVOSave.setShopCode(shopCode);
			poManualVOSave.setType(type);
			poManualVOSave.setNote(note);
			poManualVOSave.setRequestDate(requestDate);
			poManualVOSave.setLstProduct(lstSaleProduct);
			ActionType action = ActionType.parseValue(valueEdit);
			if (ActionType.INSERT.equals(action)) {
				if (PoManualType.PO_IMPORT.getValue().equals(type)) {
					poManualVOSave.setWarehouseId(warehouseId);
				} else {
					poManualVOSave.setAsn(poAutoNumber); // asn
				}
			} else {
				poManualVOSave.setPoId(poId);
			}
			poManualVOSave.setAction(action); // xy ly: 1: insert; 2: update
			String poNumber = poManualMgr.saveOrderManual(poManualVOSave, getLogInfoVO());
			if (ActionType.INSERT.equals(action)) {
				if (!StringUtil.isNullOrEmpty(poNumber)) {
					result.put("poNumber", poNumber);
				}
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			String err = e.getMessage();
			String msgErr = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			if (!StringUtil.isNullOrEmpty(err)) {
				if (err.startsWith(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_ERR)) {
					msgErr = R.getResource("po.manual.asn.parent.product.err", err.replaceFirst(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_ERR, ""));
				} else if (err.startsWith(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR)) {
					msgErr = R.getResource("po.manual.asn.parent.product.quantity.available.err", err.replaceFirst(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR, ""));
				} else if (err.startsWith(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_OLD_ERR)) {
					msgErr = R.getResource("po.manual.asn.parent.product.quantity.old.err", err.replaceFirst(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_OLD_ERR, ""));
				} else if (err.startsWith(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_CONVFACT_ERR)) {
					msgErr = R.getResource("po.manual.asn.parent.product.convfact.err", err.replaceFirst(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_CONVFACT_ERR, ""));
				}
			}
			result.put(ERROR, true);
			result.put(ERR_MSG, msgErr);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualCreateAction.saveOrderManual()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Xu ly checkPoManualSave
	 * @author vuongmq
	 * @return String
	 * @since Dec 8, 2015
	*/
	private String checkPoManualSave() {
		actionStartTime = DateUtil.now();
		String msg = null;
		try {
			if (currentUser == null || currentUser.getUserId() == null) {
				msg = R.getResource("common.cms.undefined");
			}
			Shop shop = null;
			if (StringUtil.isNullOrEmpty(msg)) {
				if (StringUtil.isNullOrEmpty(shopCode)) {
					msg = R.getResource("catalog.unit.tree.not.exist");
				} else {
					shop = shopMgr.getShopByCode(shopCode);
					if (shop != null) {
						if (super.getMapShopChild().get(shop.getId()) == null) {
							msg = R.getResource("common.cms.shop.undefined");
						}
					} else {
						msg = R.getResource("common.cms.shop.undefined");
					}
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (!ActionType.INSERT.getValue().equals(valueEdit) && !ActionType.UPDATE.getValue().equals(valueEdit)) {
					msg = R.getResource("po.manual.action.type.err");	
				}
			}
			// xu ly create Po manual
			if (StringUtil.isNullOrEmpty(msg)) {
				if (!PoManualType.checkPoManualStatus(type)) {
					msg = R.getResource("po.manual.type.err");	
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (!StringUtil.isNullOrEmpty(note)) {
					msg = ValidateUtil.validateField(note, "common.description.lable.gc", 200, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (!StringUtil.isNullOrEmpty(requestDate)) {
					Date requestDateTmp = DateUtil.parse(requestDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					if (requestDateTmp != null) {
						if (DateUtil.compareDateWithoutTime(commonMgr.getSysDate(), requestDateTmp) > 0) {
							msg = R.getResource("po.manual.request.date.more.than.or.equal.sys.err");
						}
					} else {
						msg = R.getResource("po.manual.request.date.format.err");
					}
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (lstSaleProduct == null || lstSaleProduct.size() == 0) {
					msg = R.getResource("po.manual.lst.product.require.err");
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				PoVnm poVnm = null;
				// kiem tra chung update: don dat, don tra
				if (ActionType.UPDATE.getValue().equals(valueEdit)) {
					if (poId != null) {
						Po po = poManualMgr.getPoById(poId);
						if (po != null) {
							poVnm = po.getPoVnm();
							if (po.getWarehouse() == null) {
								msg = R.getResource("po.manual.warehouse.err");	
							}
							if (po.getShop() == null || !shop.getId().equals(po.getShop().getId())) {
								msg = R.getResource("po.manual.not.shop.err");
							}
						} else {
							msg = R.getResource("saleorder.aiad.err.isJoinOderNumber");
						}
					} else {
						msg = R.getResource("po.manual.asn.parent.id.err");	
					}
				}
				if (PoManualType.PO_IMPORT.getValue().equals(type)) {
					if (ActionType.INSERT.getValue().equals(valueEdit)) {
						if (warehouseId == null) {
							msg = R.getResource("po.manual.warehouse.err");	
						}
					}
					if (StringUtil.isNullOrEmpty(msg)) {
						for (int i = 0, sz = lstSaleProduct.size(); i < sz; i++) {
							PoManualProductVO poManualProductVO = lstSaleProduct.get(i);
							if (poManualProductVO.getQuantity() == null || !ValidateUtil.validateNumber(poManualProductVO.getQuantity().toString()) || poManualProductVO.getQuantity().intValue() <= 0) {
								msg = R.getResource("po.manual.lst.product.require.quantity.err", poManualProductVO.getProductCode());
								break;
							}
						}
					}
				} else {
					if (ActionType.INSERT.getValue().equals(valueEdit)) {
						if (!StringUtil.isNullOrEmpty(poAutoNumber)) {
							PoVnmFilter filter = new PoVnmFilter();
							filter.setShopId(shop.getId());
							filter.setOrderNumber(poAutoNumber); // asn
							filter.setPoType(PoType.PO_CONFIRM);
							filter.setPoStatus(PoVNMStatus.IMPORTED);
							poVnm = poMgr.getPoDVKHBySaleOrderNumber(filter);
							if (poVnm != null) {
								if (poVnm.getWarehouse() == null) {
									msg = R.getResource("po.manual.asn.parent.warehouse.err");
								}
							} else {
								msg = R.getResource("saleorder.aiad.err.isJoinOderNumber");
							}
						} else {
							msg = R.getResource("po.manual.asn.parent.code.err");
						}
					}
					if (StringUtil.isNullOrEmpty(msg)) {
						boolean flagReturn = true;
						// don tra
						PoVnmFilter filter = new PoVnmFilter();
						filter.setShopId(shop.getId());
						filter.setPoVnmId(poVnm.getId());
						filter.setWarehouseId(poVnm.getWarehouse().getId());
						for (int i = 0, sz = lstSaleProduct.size(); i < sz; i++) {
							PoManualProductVO poManualProductVO = lstSaleProduct.get(i);
							if (poManualProductVO.getProductId() != null) {
								filter.setProductId(poManualProductVO.getProductId());
								List<OrderProductVO> listPoVNMDetail = poManualMgr.getListPoVNMDetailByFilter(filter);
								if (listPoVNMDetail != null && listPoVNMDetail.size() > 0) {
									if (poManualProductVO.getQuantity() != null) {
										if (!ValidateUtil.validateNumber(poManualProductVO.getQuantity().toString()) || poManualProductVO.getQuantity().intValue() <= 0) {
											msg = R.getResource("po.manual.asn.parent.product.quantity.err", poManualProductVO.getProductCode());
											break;
										}
										OrderProductVO orderProductVO = listPoVNMDetail.get(0);
										if (orderProductVO.getConvfact() != null) {
											//int soluongLe = poManualProductVO.getQuantity().intValue() * orderProductVO.getConvfact().intValue();
											int soluongLe = poManualProductVO.getQuantity().intValue();
											if (orderProductVO.getAvailableQuantity() != null) {
												if (soluongLe > orderProductVO.getAvailableQuantity().intValue()) {
													msg = R.getResource("po.manual.asn.parent.product.quantity.available.err", poManualProductVO.getProductCode());
													break;
												}
											} else {
												msg = R.getResource("po.manual.asn.parent.product.quantity.available.err", poManualProductVO.getProductCode());
												break;
											}
											if (orderProductVO.getOldQuantity() == null || soluongLe > orderProductVO.getOldQuantity().intValue()) {
												// oldQuantity: so luong thung
												msg = R.getResource("po.manual.asn.parent.product.quantity.old.err", poManualProductVO.getProductCode());
												break;
											}
										} else {
											msg = R.getResource("po.manual.asn.parent.product.convfact.err", poManualProductVO.getProductCode());
											break;
										}
										flagReturn = false;
									}
								} else {
									msg = R.getResource("po.manual.asn.parent.product.err", poManualProductVO.getProductCode());
									break;
								}
							}
						}
						if (StringUtil.isNullOrEmpty(msg)) {
							if (flagReturn) {
								msg = R.getResource("po.manual.asn.parent.product.quantity.return.not.exists.err");
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualCreateAction.checkPoManualSave()"), createLogErrorStandard(actionStartTime));
		}
		return msg;
	}

	/**
	 * Xu ly getProduct
	 * @author vuongmq
	 * @return String
	 * @since Dec 7, 2015
	 */
	public String getProduct() {
		actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		try {
			String msg = super.checkCurrentUserAndShopId(shopId);
			if (StringUtil.isNullOrEmpty(msg) && warehouseId == null) {
				msg = R.getResource("po.manual.warehouse.err");	
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			PoManualFilter<OrderProductVO> filter = new PoManualFilter<>();
			filter.setShopId(shopId);
			filter.setWarehouseId(warehouseId);
			List<OrderProductVO> listOrderProduct = poManualMgr.getListOrderProductVO(filter);
			if (listOrderProduct != null) {
				result.put("listSaleProduct", listOrderProduct);
			} else {
				result.put("listSaleProduct", new ArrayList<OrderProductVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualCreateAction.getProduct()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xu ly changePoNumberParent
	 * @author vuongmq
	 * @return String
	 * @since Dec 25, 2015
	 */
	public String changePoNumberParent() {
		actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		try {
			String msg = super.checkCurrentUserAndShopId(shopId);
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			PoVnmFilter filter = new PoVnmFilter();
			filter.setShopId(shopId);
			filter.setObjectType(PoObjectType.NPP);
			filter.setPoType(PoType.PO_CONFIRM);
			filter.setPoStatus(PoVNMStatus.IMPORTED);
			filter.setPoAutoNumber(poAutoNumber); // asn; ma don goc po confirm
			List<PoVnmManualVO> lstPoManual = poManualMgr.getListPoVNMManualByFilter(filter);
			if (lstPoManual != null && lstPoManual.size() > 0) {
				PoVnmManualVO poVnmManualVO = lstPoManual.get(0);
				result.put("poParent", poVnmManualVO.getAsn());
				result.put("id", poVnmManualVO.getId());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualCreateAction.changePoNumberParent()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Xu ly getPoVnmReturn
	 * @author vuongmq
	 * @return
	 * @since Dec 14, 2015
	 */
	public String getPoVnmReturn() {
		actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		result.put("page", page);
		result.put("max", max);
		try {
			String msg = super.checkCurrentUserAndShopId(shopId);
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			PoVnmFilter filter = new PoVnmFilter();
			KPaging<PoVnmManualVO> kPaging = new KPaging<PoVnmManualVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPagingManualVO(kPaging);
			
			filter.setShopId(shopId);
			filter.setObjectType(PoObjectType.NPP);
			filter.setPoType(PoType.PO_CONFIRM);
			filter.setPoStatus(PoVNMStatus.IMPORTED);
			filter.setOrderNumber(orderNumber);
			filter.setPoAutoNumber(poAutoNumber); // asn
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			filter.setFromDate(fDate);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			filter.setToDate(tDate);
			ObjectVO<PoVnmManualVO> vo = poManualMgr.getListObjectVOPoVNMManualByFilter(filter);
			if (vo != null && vo.getLstObject() != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<PoVnmManualVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualCreateAction.getPoVnmReturn()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Xu ly getPoVnmDetailReturn
	 * @author vuongmq
	 * @return String
	 * @since Dec 14, 2015
	 */
	public String getPoVnmDetailReturn() {
		actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		try {
			String msg = super.checkCurrentUserAndShopId(shopId);
			PoVnm poVnm =  null;
			if (StringUtil.isNullOrEmpty(msg)) {
				if (poVnmId == null) {
					msg = R.getResource("po.manual.asn.parent.id.err");	
				} else {
					poVnm = commonMgr.getEntityById(PoVnm.class, poVnmId);
				}
			}
			if (StringUtil.isNullOrEmpty(msg) && poVnm == null) {
				msg = R.getResource("saleorder.aiad.err.isJoinOderNumber");
			}
			if (StringUtil.isNullOrEmpty(msg) && poVnm.getWarehouse() == null) {
				msg = R.getResource("po.manual.warehouse.err");	
			}
			if (StringUtil.isNullOrEmpty(msg) && (poVnm.getShop() == null || !shopId.equals(poVnm.getShop().getId()))) {
				msg = R.getResource("po.manual.not.shop.err");	
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			PoVnmFilter filter = new PoVnmFilter();
			filter.setShopId(shopId);
			filter.setPoVnmId(poVnmId);
			filter.setWarehouseId(poVnm.getWarehouse().getId());
			List<OrderProductVO> listPoVNMDetail = poManualMgr.getListPoVNMDetailByFilter(filter);
			if (listPoVNMDetail != null) {
				result.put("listPoVNMDetail", listPoVNMDetail);
			} else {
				result.put("listPoVNMDetail", new ArrayList<OrderProductVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualCreateAction.getPoVnmDetailReturn()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	
	public Long getPoId() {
		return poId;
	}
	
	public void setPoId(Long poId) {
		this.poId = poId;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Long getPoVnmId() {
		return poVnmId;
	}

	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getValueEdit() {
		return valueEdit;
	}

	public void setValueEdit(Integer valueEdit) {
		this.valueEdit = valueEdit;
	}

	public List<PoManualProductVO> getLstSaleProduct() {
		return lstSaleProduct;
	}

	public void setLstSaleProduct(List<PoManualProductVO> lstSaleProduct) {
		this.lstSaleProduct = lstSaleProduct;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

}

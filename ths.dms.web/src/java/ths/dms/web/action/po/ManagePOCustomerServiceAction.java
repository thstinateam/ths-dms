package ths.dms.web.action.po;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.PoMgr;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.PoObjectType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoCSVO;
import ths.dms.core.entities.vo.PoConfirmVO;
import ths.dms.core.entities.vo.PoDVKHVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.memcached.MemcachedUtils;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class ManagePOCustomerServiceAction extends AbstractAction {

	private static final long serialVersionUID = -701060161120400556L;

	private PoMgr poMgr;

	private boolean cantEditShopCode;
	private Integer poVnmStatus;
	private String poNumber;
	private String startDate;
	private String endDate;
	private Long poVnmId;
	private Long warehouseId;
	private String poCoNumber;
	private String poAutoNumber;
	private Integer typePo;

	private BigDecimal totalAmountDetail;
	private Double totalAmountTypeDouble;
	private Integer totalQtyDetail;

	private Shop shop;

	private List<Integer> lstPoVnmStatus;
	private List<PoVnm> lstPOVNMConfirm;
	private List<Long> lstPOSuspendId;
	private List<PoVnmDetailLotVO> listPoConfirmDetail;
	private List<StatusBean> listPoVnmStatus;
	private List<PoCSVO> lstPOVNM;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		poMgr = (PoMgr) context.getBean("poMgr");
	}

	@Override
	public String execute() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			if (currentUser != null) {
				currentUser = getCurrentUser();
			} else {
				return PAGE_NOT_PERMISSION;
			}
			Shop sh = null;
			if (currentUser != null && currentUser.getShopRoot() != null) {
				sh = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			listPoVnmStatus = new ArrayList<StatusBean>();
			listPoVnmStatus.add(new StatusBean((long) PoVNMStatus.NOT_IMPORT.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.vnm.status.not.import.cbx")));
			listPoVnmStatus.add(new StatusBean((long) PoVNMStatus.IMPORTING.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.vnm.status.importing.cbx")));
			listPoVnmStatus.add(new StatusBean((long) PoVNMStatus.IMPORTED.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.vnm.status.imported.cbx")));
			listPoVnmStatus.add(new StatusBean((long) PoVNMStatus.SUSPEND.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.vnm.status.suspend.cbx")));
			Cycle cycleSys = cycleMgr.getCycleByCurrentSysdate();
			Date dateSys = commonMgr.getSysDate();
			Date fDate = null;
			Date tDate = null;
			//Lay mac dinh cho tu ngay dn ngay
			if (cycleSys != null && cycleSys.getBeginDate() != null) {
				fDate = cycleSys.getBeginDate();
				startDate = DateUtil.toDateString(cycleSys.getBeginDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				fDate = dateSys;
				startDate = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			tDate = dateSys;
			endDate = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (sh != null) {
				lstPoVnmStatus = new ArrayList<Integer>();
				lstPoVnmStatus.add(0);
				lstPoVnmStatus.add(1);
				//lstPOVNM = poMgr.getListPoCSVOEx(sh.getId(), null, null, fDate, tDate, lstPoVnmStatus, false);
				PoVnmFilter filter = new PoVnmFilter();
				filter.setShopId(sh.getId());
				filter.setFromDate(fDate);
				filter.setToDate(tDate);
				filter.setLstPoVnmStatus(lstPoVnmStatus);
				filter.setHasFindChildShop(true); // cu false
				filter.setPoType(PoType.PO_CUSTOMER_SERVICE); // IDP Chi lay don Po DV nhap; type = 1
				lstPOVNM = poMgr.getListPoCSVOFilter(filter);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.ManagePOCustomerServiceAction.execute()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * Search.
	 * 
	 * @return the string
	 * @author phut
	 * @since Oct 1, 2012
	 */
	public String search() {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser != null) {
				currentUser = getCurrentUser();
			} else {
				return PAGE_NOT_PERMISSION;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				if (currentUser != null && currentUser.getShopRoot() != null) {
					shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				}
			}
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("catalog.unit.tree.not.exist"));
				return ERROR;
			}
			if (StringUtil.isNullOrEmpty(startDate)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("povnm_ngay_tao_tu_khong_de_trong"));
				return ERROR;
			}
			if (StringUtil.isNullOrEmpty(endDate)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("povnm_ngay_tao_den_khong_de_trong"));
				return ERROR;
			}
			Date fromDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			//lstPOVNM = poMgr.getListPoCSVOEx(shop.getId(), poNumber, poCoNumber, fromDate, toDate, lstPoVnmStatus, false);
			PoVnmFilter filter = new PoVnmFilter();
			filter.setShopId(shop.getId());
			filter.setOrderNumber(poNumber);
			filter.setPoAutoNumber(poAutoNumber);
			//filter.setPoCoNumber(poCoNumber);
			filter.setWarehouseId(warehouseId);
			filter.setFromDate(fromDate);
			filter.setToDate(toDate);
			filter.setLstPoVnmStatus(lstPoVnmStatus);
			filter.setHasFindChildShop(true); // cu false
			filter.setPoType(PoType.PO_CUSTOMER_SERVICE); // IDP Chi lay don Po DV nhap; type = 1
			lstPOVNM = poMgr.getListPoCSVOFilter(filter);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.po.ManagePOCustomerServiceAction.search()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * @author vuongmq
	 * @since 03/08/2015 Suspend. xu ly treo Ql po DVKH
	 * @return the string
	 */
	public String suspend() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			if (currentUser != null) {
				currentUser = getCurrentUser();
			} else {
				return PAGE_NOT_PERMISSION;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				if (currentUser != null && currentUser.getShopRoot() != null) {
					shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				}
			}
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("catalog.unit.tree.not.exist"));
				return ERROR;
			}
			if (lstPOSuspendId != null && lstPOSuspendId.size() > 0) {
				List<PoVnm> listPoSuspend = new ArrayList<PoVnm>();
				PoVnm suspendPo = null;
				for (Long id : lstPOSuspendId) {
					suspendPo = poMgr.getPoVnmById(id);
					if (PoVNMStatus.SUSPEND.equals(suspendPo.getStatus())) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_DVKH_ALREADY_SUSPEND));
						return ERROR;
					}
					listPoSuspend.add(suspendPo);
				}
				poMgr.setSuspendPoVNM(listPoSuspend, shop.getId());
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.po.ManagePOCustomerServiceAction.suspend()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}

	/**
	 * Gets the detail po confirm.
	 * 
	 * @return the detail po confirm
	 */
	/**
	 * @author vuongmq
	 * @since 03/08/2015 getDetailPOConfirm. xu ly lay dánh ach detail cua PO
	 *        DVKH
	 * @return the string
	 */
	public String getDetailPOConfirm() {
		actionStartTime = DateUtil.now();
		try {
			if (poVnmId != null) {
				ObjectVO<PoVnmDetailLotVO> temp = poMgr.getListPoVnmDetailLotVOByPoVnmId(null, poVnmId);
				if (temp != null) {
					listPoConfirmDetail = temp.getLstObject();
					if (listPoConfirmDetail != null && listPoConfirmDetail.size() > 0) {
						totalAmountDetail = BigDecimal.ZERO;
						totalQtyDetail = 0;
						for (PoVnmDetailLotVO detailLotVO : listPoConfirmDetail) {
							if (detailLotVO != null) {
								if (detailLotVO.getAmount() != null) {
									totalAmountDetail = totalAmountDetail.add(detailLotVO.getAmount());
								}
								if (detailLotVO.getQuantity() != null) {
									totalQtyDetail += detailLotVO.getQuantity();
								}
							}
						}
					}
				}
			}
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.po.ManagePOCustomerServiceAction.getDetailPOConfirm()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * Gets the detail po confirm.
	 * 
	 * @author hunglm16
	 * @since February 7, 2014
	 * @return the full detail po confirm
	 */
	public String getFullDetailPOConfirm() {
		actionStartTime = DateUtil.now();
		try {
			totalAmountTypeDouble = 0.0;
			totalQtyDetail = 0;
			listPoConfirmDetail = new ArrayList<PoVnmDetailLotVO>();
			if (poVnmId != null) {
				List<Long> lstPoVnmId = new ArrayList<Long>();
				lstPoVnmId.add(poVnmId);
				ObjectVO<PoVnmDetailLotVO> temp = poMgr.getListFullPoVnmDetailVOByListPoVnmId(null, lstPoVnmId);
				if (temp != null) {
					listPoConfirmDetail = temp.getLstObject();
					if (listPoConfirmDetail != null && listPoConfirmDetail.size() > 0) {
						for (PoVnmDetailLotVO detailLotVO : listPoConfirmDetail) {
							if (detailLotVO != null) {
								if (detailLotVO.getAmount() != null) {
									totalAmountTypeDouble += detailLotVO.getAmount().doubleValue();
								}
								if (detailLotVO.getQuantity() != null) {
									totalQtyDetail += detailLotVO.getQuantity();
								}
							}
						}
					}
				}
			}
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.po.ManagePOCustomerServiceAction.getFullDetailPOConfirm()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		result.put("lstData", listPoConfirmDetail);
		result.put("totalQuantity", totalQtyDetail);
		result.put("totalAmount", totalAmountTypeDouble);
		return JSON;
	}

	/**
	 * Export.
	 * 
	 * @return the string
	 * @author phut
	 * @since Oct 1, 2012
	 */
	/**
	 * @author vuongmq
	 * @since 03/08/2015 Export PO DVKH
	 * @return the string
	 */
	public String export() throws Exception {
		actionStartTime = DateUtil.now();
		result = new HashMap<String, Object>();
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			Shop shop = null;
			if (currentUser != null) {
				currentUser = getCurrentUser();
			} else {
				return PAGE_NOT_PERMISSION;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				if (currentUser != null && currentUser.getShopRoot() != null) {
					shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				}
			}
			if (shop == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("catalog.unit.tree.not.exist"));
				return ERROR;
			}
			Date fromDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
			Map<String, Object> beans = new HashMap<String, Object>();
			List<Map<String, Object>> _lstPOCSVO = new ArrayList<Map<String, Object>>();
			List<PoDVKHVO> lstPOCSVO = null;
			PoDVKHVO pocs = null;
			List<PoConfirmVO> lstPoco = null;
			PoConfirmVO poConfirmVO = null;
			Map<String, Object> _listPoConfirmVO = null;
			//shopId = shop.getId(); // vi co chon tat ca
			for (Long poVnmId : lstPOSuspendId) {
				PoVnm poVnm = poMgr.getPoVnmById(poVnmId);
				if (poVnm == null || poVnm.getShop() == null || super.getMapShopChild().get(poVnm.getShop().getId()) == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
					return ERROR;
				}
				shopId = poVnm.getShop().getId();
				lstPOCSVO = poMgr.getPoDVKHforReport(shopId, fromDate, toDate, poVnmId);
				if (lstPOCSVO != null) {
					beans.put("hasData", 1);
					int remainTotal = 0;
					pocs = lstPOCSVO.get(0);
					if (pocs != null) {
						lstPoco = pocs.getListPoConfirmVo();
						if (lstPoco != null && lstPoco.size() > 0) {
							for (int j = 0, szj = lstPoco.size(); j < szj; j++) {
								poConfirmVO = lstPoco.get(j);
								remainTotal += poConfirmVO.getNotImportQuantity();
							}
						}
						_listPoConfirmVO = new HashMap<String, Object>();
						_listPoConfirmVO.put("saleOrderNumber", pocs.getSaleOrderNumber());
						_listPoConfirmVO.put("poType", pocs.getPoType()); // Integer (1: nhap; 4: tra)
						_listPoConfirmVO.put("remainTotal", remainTotal);
						_listPoConfirmVO.put("listPoConfirmVo", pocs.getListPoConfirmVo());
						_lstPOCSVO.add(_listPoConfirmVO);
						beans.put("report", _lstPOCSVO);
					}
				} else {
					beans.put("hasData", 0);
				}
			}
			beans.put("shop", shop);
			if (shop.getParentShop() != null) {
				beans.put("parentShop", shop.getParentShop().getShopName());
			}
			beans.put("printDate", printDate);
			beans.put("startDate", startDate);
			beans.put("endDate", endDate);

			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathPO(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.EXPORT_POCONFIRM_TEMPLATE;
			templateFileName = templateFileName.replace('/', File.separatorChar);

			String outputName = ConstantManager.EXPORT_POCONFIRM + "_" + shop.getShopCode() + "_" + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.po.ManagePOCustomerServiceAction.export()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (os != null) {
				os.flush();
				os.close();
			}
		}
		return SUCCESS;
	}

	/**
	 * @author vuongmq
	 * @since 03/08/2015 Lay danh sach detail PO DVKH
	 * @return the string
	 */
	public String getDetailPOCustomerService() {
		actionStartTime = DateUtil.now();
		try {
			if (poVnmId != null) {
				PoVnm poVnm = poMgr.getPoVnmById(poVnmId);
				if (poVnm != null) {
					poNumber = poVnm.getSaleOrderNumber();
					ObjectVO<PoVnm> teamp = poMgr.getListPoConfirmByPoCSFromPoId(null, poVnm, PoObjectType.NPP);
					if (teamp != null) {
						lstPOVNMConfirm = teamp.getLstObject();
					}
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex,  R.getResource("web.log.message.error", "ths.dms.web.action.po.ManagePOCustomerServiceAction.getDetailPOCustomerService()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/** getters + setters */
	public Double getTotalAmountTypeDouble() {
		return totalAmountTypeDouble;
	}

	public void setTotalAmountTypeDouble(Double totalAmountTypeDouble) {
		this.totalAmountTypeDouble = totalAmountTypeDouble;
	}

	public List<Integer> getLstPoVnmStatus() {
		return lstPoVnmStatus;
	}

	public void setLstPoVnmStatus(List<Integer> lstPoVnmStatus) {
		this.lstPoVnmStatus = lstPoVnmStatus;
	}

	public String getPoCoNumber() {
		return poCoNumber;
	}

	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}

	public Integer getPoVnmStatus() {
		return poVnmStatus;
	}

	public void setPoVnmStatus(Integer poVnmStatus) {
		this.poVnmStatus = poVnmStatus;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public List<PoCSVO> getLstPOVNM() {
		return lstPOVNM;
	}

	public void setLstPOVNM(List<PoCSVO> lstPOVNM) {
		this.lstPOVNM = lstPOVNM;
	}

	public Long getPoVnmId() {
		return poVnmId;
	}

	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}

	public List<PoVnm> getLstPOVNMConfirm() {
		return lstPOVNMConfirm;
	}

	public void setLstPOVNMConfirm(List<PoVnm> lstPOVNMConfirm) {
		this.lstPOVNMConfirm = lstPOVNMConfirm;
	}

	public List<Long> getLstPOSuspendId() {
		return lstPOSuspendId;
	}

	public void setLstPOSuspendId(List<Long> lstPOSuspendId) {
		this.lstPOSuspendId = lstPOSuspendId;
	}

	public List<PoVnmDetailLotVO> getListPoConfirmDetail() {
		return listPoConfirmDetail;
	}

	public void setListPoConfirmDetail(List<PoVnmDetailLotVO> listPoConfirmDetail) {
		this.listPoConfirmDetail = listPoConfirmDetail;
	}

	public BigDecimal getTotalAmountDetail() {
		return totalAmountDetail;
	}

	public void setTotalAmountDetail(BigDecimal totalAmountDetail) {
		this.totalAmountDetail = totalAmountDetail;
	}

	public Integer getTotalQtyDetail() {
		return totalQtyDetail;
	}

	public void setTotalQtyDetail(Integer totalQtyDetail) {
		this.totalQtyDetail = totalQtyDetail;
	}

	public boolean getCantEditShopCode() {
		return cantEditShopCode;
	}

	public void setCantEditShopCode(boolean cantEditShopCode) {
		this.cantEditShopCode = cantEditShopCode;
	}

	public List<StatusBean> getListPoVnmStatus() {
		return listPoVnmStatus;
	}

	public void setListPoVnmStatus(List<StatusBean> listPoVnmStatus) {
		this.listPoVnmStatus = listPoVnmStatus;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Integer getTypePo() {
		return typePo;
	}

	public void setTypePo(Integer typePo) {
		this.typePo = typePo;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}
}
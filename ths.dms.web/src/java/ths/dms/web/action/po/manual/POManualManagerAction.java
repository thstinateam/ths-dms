/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.action.po.manual;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.PoManualMgr;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.Po;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActionType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApprovalStepPo;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoManualType;
import ths.dms.core.entities.filter.PoManualFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PoManualVO;
import ths.dms.core.entities.vo.PoManualVODetail;
import ths.dms.core.entities.vo.PoManualVOError;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Mo ta class POManualManagerAction.java
 * @author vuongmq
 * @since Dec 18, 2015
 */
public class POManualManagerAction extends AbstractAction {

	/** field serialVersionUID  field long */
	private static final long serialVersionUID = 1L;
	
	private Long shopId;
	private Long poId;
	private Long warehouseId;
	private String shopCode;
	private String shopName;
	private String fromDate;
	private String toDate;
	private String requestDate;
	private String poNumber;
	private String refPoNumber;
	private String warehouseCode;
	private String note;
	private Integer type;
	private Integer status;
	private Integer approvedStep;
	private Integer valueEdit;

	private BigDecimal totalAmountDetail;
	
	private Shop shop;
	private List<Long> lstId;
	private List<PoManualVODetail> lstPoManualVODetail;
	private PoManualMgr poManualMgr;

	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		poManualMgr = (PoManualMgr) context.getBean("poManualMgr");
	}

	@Override
	public String execute() {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				shopId = shop.getId();
				Cycle cycleSys = cycleMgr.getCycleByCurrentSysdate();
				Date dateSys = commonMgr.getSysDate();
				//Lay mac dinh cho tu ngay den ngay
				if (cycleSys != null && cycleSys.getBeginDate() != null) {
					fromDate = DateUtil.toDateString(cycleSys.getBeginDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					fromDate = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				toDate = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualManagerAction.execute()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * Xu ly search
	 * @author vuongmq
	 * @return String
	 * @since Dec 18, 2015
	 */
	public String search() {
		Date actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			PoManualFilter<PoManualVO> filter = new PoManualFilter<>();
			KPaging<PoManualVO> kPaging = new KPaging<PoManualVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			filter.setStrListShopId(super.getStrListShopId());
			// seach danh sach don hang
			if (shopId != null) {
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				if (shop != null) {
					filter.setShopId(shop.getId());
				}
			} else {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			}
			filter.setPoNumber(poNumber);
			if (PoManualType.checkPoManualStatus(type)) {
				filter.setType(type);
			}
			if (ApprovalStepPo.checkApprovalStepPo(approvedStep)) {
				filter.setApprovedStep(approvedStep);
			}
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			ObjectVO<PoManualVO> vo = poManualMgr.getListPoManualVOByFilter(filter);
			if (vo != null && vo.getLstObject() != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<PoManualVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualManagerAction.search()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
	    return JSON;
	}
	
	/**
	 * Xu ly editPo
	 * @author vuongmq
	 * @return String
	 * @since Dec 18, 2015
	 */
	public String editPo() {
		actionStartTime = DateUtil.now();
		try {
			String msg = null;
			if (currentUser == null || currentUser.getUserId() == null) {
				msg = R.getResource("common.cms.undefined");
			}
			if (poId != null) {
				Po po = poManualMgr.getPoById(poId);
				if (po != null && po.getShop() != null && super.checkShopInOrgAccessById(po.getShop().getId())
					&& ApprovalStepPo.NEW.equals(po.getApprovedStep()) && ActiveType.RUNNING.equals(po.getStatus())) {
					valueEdit = ActionType.UPDATE.getValue();
					shopId = po.getShop().getId();
					shopCode = po.getShop().getShopCode();
					shopName = po.getShop().getShopName();
					poNumber = po.getPoNumber();
					note = po.getNote();
					if (PoManualType.PO_RETURN.equals(po.getPoType())) {
						refPoNumber = po.getRefPoNumber();
					}
					type = po.getPoType() != null ? po.getPoType().getValue() : null;
					if (po.getWarehouse() != null) {
						warehouseId = po.getWarehouse().getId();
						warehouseCode = po.getWarehouse().getWarehouseCode() + " - " + po.getWarehouse().getWarehouseName();
					}
					if (po.getPoDate() != null) {
						fromDate = DateUtil.toDateString(po.getPoDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					if (po.getRequestDate() != null) {
						requestDate = DateUtil.toDateString(po.getRequestDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
				} else {
					return PAGE_NOT_FOUND;
				}
			} else {
				return PAGE_NOT_FOUND;
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return SUCCESS;
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualManagerAction.editPo()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * Xu ly getPoProductEdit
	 * @author vuongmq
	 * @return String
	 * @since Dec 28, 2015
	 */
	public String getPoProductEdit() {
		actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		try {
			String msg = super.checkCurrentUserAndShopId(shopId);
			Po po =  null;
			if (StringUtil.isNullOrEmpty(msg)) {
				if (poId == null) {
					msg = R.getResource("po.manual.asn.parent.id.err");	
				} else {
					po = poManualMgr.getPoById(poId);
				}
			}
			if (StringUtil.isNullOrEmpty(msg) && po == null || po.getWarehouse() == null) {
				msg = R.getResource("po.manual.warehouse.err");	
			}
			if (StringUtil.isNullOrEmpty(msg) && !PoManualType.checkPoManualStatus(type)) {
				msg = R.getResource("po.manual.type.err");	
			}
			if (StringUtil.isNullOrEmpty(msg) && PoManualType.PO_RETURN.getValue().equals(type)) {
				if (po.getPoVnm() == null || po.getPoVnm().getId() == null) {
					msg = R.getResource("saleorder.aiad.err.isJoinOderNumber");
				}
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			PoManualFilter<OrderProductVO> filter = new PoManualFilter<OrderProductVO>();
			filter.setShopId(shopId);
			filter.setId(poId);
			filter.setWarehouseId(po.getWarehouse().getId());
			filter.setType(type);
			if (PoManualType.PO_RETURN.getValue().equals(type)) {
				filter.setRefAsnId(po.getPoVnm().getId()); // don tra chinh sua dung refAsnId
			}
			List<OrderProductVO> listPoProductEdit = poManualMgr.getListPoDetailEditByFilter(filter);
			if (listPoProductEdit != null) {
				result.put("listPoProductEdit", listPoProductEdit);
			} else {
				result.put("listPoProductEdit", new ArrayList<OrderProductVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualManagerAction.getPoProductEdit()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Xu ly getDetailPOManual
	 * @author vuongmq
	 * @return String
	 * @since Dec 21, 2015
	 */
	public String getDetailPOManual() {
		actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			if (poId != null && poId > ActiveType.DELETED.getValue()) {
				Po po = commonMgr.getEntityById(Po.class, poId);
				if (po != null && po.getShop() != null && super.checkShopInOrgAccessById(po.getShop().getId())) {
					poNumber = po.getPoNumber();
					type = po.getPoType() != null ? po.getPoType().getValue() : null;
					note = po.getNote();
					if (PoManualType.PO_RETURN.equals(po.getPoType())) {
						refPoNumber = po.getRefPoNumber();
					}
					PoManualFilter<PoManualVODetail> filter = new PoManualFilter<>();
					filter.setId(poId);
					lstPoManualVODetail = poManualMgr.getListPoManualDetailVOByFilter(filter);
					totalAmountDetail = BigDecimal.ZERO;
					if (lstPoManualVODetail != null && !lstPoManualVODetail.isEmpty()) {
						for (PoManualVODetail detail : lstPoManualVODetail) {
							if (detail != null && detail.getAmount() != null) {
								totalAmountDetail =  totalAmountDetail.add(detail.getAmount());								
							}
						}
					}
				} else {
					result.put(ERR_MSG, R.getResource("jsp.common.no.data"));
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualManagerAction.getDetailPOManual()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * Xu ly updateStatusOrderManual
	 * @author vuongmq
	 * @return String
	 * @since Dec 18, 2015
	 */
	public String updateStatusOrderManual() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			String msg = null;
			if (currentUser == null || currentUser.getUserId() == null) {
				msg = R.getResource("common.cms.undefined");
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (lstId == null || lstId.size() == 0) {
					msg = R.getResource("po.manual.lst.id.require.err");
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (!ApprovalStepPo.checkApprovalStepPo(approvedStep)) {
					msg = R.getResource("po.manual.select.status.err");
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				boolean flagInShop = false;
				for (int i = 0, sz = lstId.size(); i < sz; i++) {
					Long poId = lstId.get(i); 
					Po po = commonMgr.getEntityById(Po.class, poId);
					if (po == null || po.getShop() == null || !super.checkShopInOrgAccessById(po.getShop().getId())) {
						flagInShop = true;
						break;
					}
				}
				if (flagInShop) {
					msg = R.getResource("po.manual.lst.invalid.err");
				}
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			List<PoManualVOError> lstErr = poManualMgr.updateStatusOrderManual(lstId, approvedStep, getLogInfoVO());
			if (lstErr != null && !lstErr.isEmpty()) {
				result.put(ERROR, true);
				result.put("lstError", lstErr);
				int errSz = lstErr.size();
				int successSz = lstId.size() - errSz;
				msg = R.getResource("po.manual.lst.popup.err.msg", successSz, errSz);
				result.put(ERR_MSG, msg);
			} else {
				result.put(ERROR, false);
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.po.manual.POManualManagerAction.updateStatusOrderManual()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getPoId() {
		return poId;
	}

	public void setPoId(Long poId) {
		this.poId = poId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getRefPoNumber() {
		return refPoNumber;
	}

	public void setRefPoNumber(String refPoNumber) {
		this.refPoNumber = refPoNumber;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getApprovedStep() {
		return approvedStep;
	}

	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}

	public Integer getValueEdit() {
		return valueEdit;
	}

	public void setValueEdit(Integer valueEdit) {
		this.valueEdit = valueEdit;
	}

	public BigDecimal getTotalAmountDetail() {
		return totalAmountDetail;
	}

	public void setTotalAmountDetail(BigDecimal totalAmountDetail) {
		this.totalAmountDetail = totalAmountDetail;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<PoManualVODetail> getLstPoManualVODetail() {
		return lstPoManualVODetail;
	}

	public void setLstPoManualVODetail(List<PoManualVODetail> lstPoManualVODetail) {
		this.lstPoManualVODetail = lstPoManualVODetail;
	}

}

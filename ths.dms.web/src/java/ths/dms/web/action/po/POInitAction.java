package ths.dms.web.action.po;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.PoMgr;
import ths.dms.core.business.SaleDayMgr;
import ths.dms.core.entities.PoAutoDetail;
import ths.dms.core.entities.SaleDays;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.vo.PoAutoVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

public class POInitAction extends AbstractAction {

	private static final long serialVersionUID = -2128120975032582873L;

	private String fromDate;
	private String toDate;
	private Shop shop;
	private int buyingDate;//so ngay ban hang thuc te
	private int buyingDateInMonth;//so ngay ban hang trong thang
	private List<PoAutoVO> listPoAutoVo;
	private boolean canCreatePOAuto;
	private boolean checkAllProductType;
	private SaleDayMgr saleDayMgr;
	private PoMgr poMgr;
	private Long shopId;
	private Long staffId;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		saleDayMgr = (SaleDayMgr) context.getBean("saleDayMgr");
		poMgr = (PoMgr) context.getBean("poMgr");

	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		mapControl = getMapControlToken();
		checkAllProductType = true;
		Calendar calendar = new GregorianCalendar();

		Calendar calTmp = new GregorianCalendar();
		calTmp.set(Calendar.DATE, 1);

		Date beginDate = calTmp.getTime();

		this.fromDate = DateUtil.toDateString(beginDate, DateUtil.DATE_FORMAT_DDMMYYYY);

		this.toDate = DateUtil.toDateString(calendar.getTime(), DateUtil.DATE_FORMAT_DDMMYYYY);

		buyingDate = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(beginDate, calendar.getTime());

		SaleDays saleDay = saleDayMgr.getSaleDayByYear(calendar.get(Calendar.YEAR));

		switch (calendar.get(Calendar.MONTH)) {
		case 0:
			buyingDateInMonth = saleDay.getT1();
			break;
		case 1:
			buyingDateInMonth = saleDay.getT2();
			break;
		case 2:
			buyingDateInMonth = saleDay.getT3();
			break;
		case 3:
			buyingDateInMonth = saleDay.getT4();
			break;
		case 4:
			buyingDateInMonth = saleDay.getT5();
			break;
		case 5:
			buyingDateInMonth = saleDay.getT6();
			break;
		case 6:
			buyingDateInMonth = saleDay.getT7();
			break;
		case 7:
			buyingDateInMonth = saleDay.getT8();
			break;
		case 8:
			buyingDateInMonth = saleDay.getT9();
			break;
		case 9:
			buyingDateInMonth = saleDay.getT10();
			break;
		case 10:
			buyingDateInMonth = saleDay.getT11();
			break;
		case 11:
			buyingDateInMonth = saleDay.getT12();
			break;
		}
		return SUCCESS;
	}

	/**
	 * get list po auto by product type
	 * 
	 * @return the string
	 * @throws BusinessException
	 *             the business exception
	 * @author phut
	 * @since Aug 27, 2012
	 */
	public String getListPoAutoByProductType() {
		try {
			return getListPoAuto();
		} catch (Exception e) {
			LogUtility.logError(e, "Error POInitAction.getListPoAutoByProductType - " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}

	/**
	 * get list Po Auto
	 * 
	 * @return the string
	 * @throws BusinessException
	 *             the business exception
	 * @author phut
	 * @since Aug 27, 2012
	 */
	public String getListPoAuto() throws Exception {
		//		Staff staff = null;
		//		if(currentUser != null && currentUser.getUserName() != null){
		//			staff = staffMgr.getStaffByCode(currentUser.getUserName());
		//			if(staff != null && staff.getShop() != null){
		//				shop = staff.getShop();
		//			}
		//		}
		//		
		//		if(shop == null) {
		//			return SUCCESS;
		//		}
		//		
		//		 List<RoleToken> listRoles = currentUser.getListRole();
		//		 canCreatePOAuto = false;
		//		 for(RoleToken rToken : listRoles) {
		//			 if(rToken.getRoleCode().equals(VSARole.VNM_SALESONLINE_DISTRIBUTOR.getValue())) {
		//				 canCreatePOAuto = true;
		//				 break;
		//			 }
		//		 }
		ShopToken sh = currentUser.getShopRoot();
		shopId = sh.getShopId();
		listPoAutoVo = poMgr.getListPoAuto(shopId, checkAllProductType);
		return SUCCESS;
	}

	/**
	 * create PO Auto.
	 * 
	 * @return the string
	 * @throws BusinessException
	 *             the business exception
	 * @author phut
	 * @since Aug 27, 2012
	 */
	public String convertPO() throws Exception {
		resetToken(result);
		try {
			//			Staff staff = null;
			if (currentUser != null && currentUser.getUserId() != null) {
				staffId = currentUser.getUserId();
			}
			ShopToken sh = currentUser.getShopRoot();
			shopId = sh.getShopId();

			poMgr.createShopPoAuto(shopId, staffId, checkAllProductType);
			result.put(ERROR, false);
			return SUCCESS;
		} catch (Exception ex) {
			LogUtility.logError(ex, "Error POInitAction.convertPO - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}

	public String exportExcelPoAuto() throws Exception {
		try {
			if (currentUser != null && currentUser.getUserId() != null) {
				staffId = currentUser.getUserId();
			}
			ShopToken sh = currentUser.getShopRoot();
			shopId = sh.getShopId();

			checkAllProductType = true;
			Calendar calendar = new GregorianCalendar();

			Calendar calTmp = new GregorianCalendar();
			calTmp.set(Calendar.DATE, 1);

			Date beginDate = calTmp.getTime();

			fromDate = DateUtil.toDateString(beginDate, DateUtil.DATE_FORMAT_DDMMYYYY);

			toDate = DateUtil.toDateString(calendar.getTime(), DateUtil.DATE_FORMAT_DDMMYYYY);

			buyingDate = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(beginDate, calendar.getTime());
			buyingDateInMonth = saleDayMgr.getSaleDayByDate(beginDate);
			if (shopId != null) {
				Map<String, List<PoAutoDetail>> poAutoMap = poMgr.getMapPOAuto(shopId, staffId, checkAllProductType);
				if (!poAutoMap.isEmpty()) {
					//Init XSSF workboook
					String fileName = R.getResource("po.auto.export.file.name") + "_" + sh.getShopCode();
					String outputName = fileName + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//int rowAccessWindowSize = 1000;
					SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();
					SXSSFSheet sheetData = null;
					String titleTemp = null;
					String dateStrTilte = null;
					String ngayInTmp = null;
					List<PoAutoDetail> value = null;
					PoAutoDetail vo = null;
					for (Map.Entry<String, List<PoAutoDetail>> entry : poAutoMap.entrySet()) {
						//String key = entry.getKey();
						//Tao shett
						titleTemp = entry.getKey();
						if (StringUtil.isNullOrEmpty(titleTemp)) {
							titleTemp = "GROUP_NON_POAUTO_GROUP";
						}
						sheetData = (SXSSFSheet) workbook.createSheet(titleTemp);
						//Set Getting Defaul
						sheetData.setDefaultRowHeight((short) (15 * 20));
						sheetData.setDefaultColumnWidth(12);
						//set static Column width
						mySheet.setColumnsWidth(sheetData, 0, 20, 50, 100, 250);
						//Size Row
						mySheet.setRowsHeight(sheetData, 0, 20, 15, 15, 15, 20, 20);
						sheetData.setDisplayGridlines(false);
						//Tittle
						titleTemp = R.getResource("po.auto.export.title");
						mySheet.addCellsAndMerged(sheetData, 1, 0, 7, 0, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_BLUE));
						//Khoang ngay bao cao
						dateStrTilte = R.getResource("report.general.tuNgay") + fromDate + " - " + R.getResource("report.general.denNgay") + toDate;
						mySheet.addCellsAndMerged(sheetData, 1, 1, 7, 1, dateStrTilte, style.get(ExcelPOIProcessUtils.TIMES_TITLE));
						ngayInTmp = R.getResource("bc.f1.title.number.day.sale") + buyingDate + " - " + R.getResource("bc.f1.title.number.day.month") + buyingDateInMonth;
						mySheet.addCellsAndMerged(sheetData, 1, 2, 7, 2, ngayInTmp, style.get(ExcelPOIProcessUtils.TIMES_TITLE));

						//header
						int iRow = 4;
						int iCol = 1;
						mySheet.addCellsAndMerged(sheetData, iCol, iRow, iCol, iRow + 1, R.getResource("report.general.STT"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.mahang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.tenhang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.dauky"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.nhap"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.xuat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.ton"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.lkttt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.khttt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.dmkh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol + 1, iRow, R.getResource("po.auto.export.dutru"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, iCol, iRow + 1, R.getResource("po.auto.export.dutru.kh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, ++iCol, iRow + 1, R.getResource("po.auto.export.dutru.tt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol + 2, iRow, R.getResource("po.auto.export.tonkho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, iCol, iRow + 1, R.getResource("po.auto.export.tonkho.min"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, ++iCol, iRow + 1, R.getResource("po.auto.export.tonkho.next"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, ++iCol, iRow + 1, R.getResource("po.auto.export.tonkho.lead"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.yct"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.pocf"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol, iRow + 1, R.getResource("po.auto.export.podvkh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCellsAndMerged(sheetData, ++iCol, iRow, iCol + 3, iRow, R.getResource("po.auto.export.yeucau"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, iCol, iRow + 1, R.getResource("po.auto.export.yeucau.qc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, ++iCol, iRow + 1, R.getResource("po.auto.export.yeucau.slt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, ++iCol, iRow + 1, R.getResource("po.auto.export.yeucau.tt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						mySheet.addCell(sheetData, ++iCol, iRow + 1, R.getResource("po.auto.export.yeucau.cb"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						iRow += 2;
						value = entry.getValue();
						int nLst = value.size();
						int totalQuantity = 0;
						BigDecimal totalAmount = BigDecimal.ZERO;
						for (int i = 0; i < nLst; i++) {
							iCol = 1;
							vo = value.get(i);
							mySheet.addCell(sheetData, iCol++, iRow, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getProduct().getProductCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getProduct().getProductName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getOpenStock(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getImportQuantity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getExport(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getStock(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getMonthCumulate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getMonthPlan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getDayPlan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getDayReservePlan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getDayReserveReal(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getSafetyStockMin(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getNext(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getLead(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getRequimentStock(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getStockPoConfirm(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getStockPoDvkh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getConvfact(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getQuantity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol++, iRow, vo.getAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							mySheet.addCell(sheetData, iCol, iRow, vo.getWarning(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							totalQuantity += vo.getQuantity();
							if (vo.getAmount() != null) {
								totalAmount = totalAmount.add(vo.getAmount());
							}
							iRow++;
						}
						iRow++;
						sheetData.createFreezePane(8, 6);
						mySheet.addCell(sheetData, 3, iRow, R.getResource("po.auto.export.tsl"), style.get(ExcelPOIProcessUtils.BOLD_RIGHT));
						mySheet.addCell(sheetData, 4, iRow, totalQuantity, style.get(ExcelPOIProcessUtils.BOLD_RIGHT));
						mySheet.addCellsAndMerged(sheetData, 5, iRow, 6, iRow, R.getResource("po.auto.export.tt"), style.get(ExcelPOIProcessUtils.BOLD_RIGHT));
						mySheet.addCell(sheetData, 7, iRow, totalAmount, style.get(ExcelPOIProcessUtils.BOLD_RIGHT));
					}
					FileOutputStream out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "Error POInitAction.exportExcelPoAuto - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public int getBuyingDate() {
		return buyingDate;
	}

	public void setBuyingDate(int buyingDate) {
		this.buyingDate = buyingDate;
	}

	public int getBuyingDateInMonth() {
		return buyingDateInMonth;
	}

	public void setBuyingDateInMonth(int buyingDateInMonth) {
		this.buyingDateInMonth = buyingDateInMonth;
	}

	public List<PoAutoVO> getListPoAutoVo() {
		return listPoAutoVo;
	}

	public void setListPoAutoVo(List<PoAutoVO> listPoAutoVo) {
		this.listPoAutoVo = listPoAutoVo;
	}

	public void setCanCreatePOAuto(boolean canCreatePOAuto) {
		this.canCreatePOAuto = canCreatePOAuto;
	}

	public boolean isCanCreatePOAuto() {
		return canCreatePOAuto;
	}

	public boolean getCheckAllProductType() {
		return checkAllProductType;
	}

	public void setCheckAllProductType(boolean checkAllProductType) {
		this.checkAllProductType = checkAllProductType;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

}

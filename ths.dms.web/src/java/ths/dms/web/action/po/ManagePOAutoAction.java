package ths.dms.web.action.po;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.struts2.ServletActionContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.PoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.SaleDayMgr;
import ths.dms.core.business.SalePlanMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.PoAuto;
import ths.dms.core.entities.PoAutoDetail;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.SaleDays;
import ths.dms.core.entities.SalePlan;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.vo.PoAuto01VO;
import ths.dms.core.entities.vo.PoAutoGroupByCatVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.ExceptionCode;

public class ManagePOAutoAction extends AbstractAction {
	private static final long serialVersionUID = -701060161120400556L;

	private Shop shop;
	private Boolean isEditShopCode;
	private Long shopId;
	private Integer approveStatus;
	private String poNumber;
	private String startDate;
	private String endDate;
	private List<PoAuto01VO> lstPOAuto;
	private List<PoAutoDetail> lstPOAutoDetail;
	private List<StatusBean> listApproveStatus;
	private Long poAutoId;
	private String poAutoNumer;
	private BigDecimal totalAmountDetail;
	private List<Long> listChoosePOAutoId;
	private PoMgr poMgr;
	private SaleDayMgr saleDayMgr;
	private SalePlanMgr salePlanMgr;
	private ProductMgr productMgr;
	private Integer type;
	private String shopCode;
	private List<Shop> listShop;
	private String outPath;

	public List<Shop> getListShop() {
		return listShop;
	}

	public void setListShop(List<Shop> listShop) {
		this.listShop = listShop;
	}

	public String getOutPath() {
		return outPath;
	}

	public void setOutPath(String outPath) {
		this.outPath = outPath;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Boolean getIsEditShopCode() {
		return isEditShopCode;
	}

	public void setIsEditShopCode(Boolean isEditShopCode) {
		this.isEditShopCode = isEditShopCode;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(Integer approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public List<PoAuto01VO> getLstPOAuto() {
		return lstPOAuto;
	}

	public void setLstPOAuto(List<PoAuto01VO> lstPOAuto) {
		this.lstPOAuto = lstPOAuto;
	}

	public List<PoAutoDetail> getLstPOAutoDetail() {
		return lstPOAutoDetail;
	}

	public void setLstPOAutoDetail(List<PoAutoDetail> lstPOAutoDetail) {
		this.lstPOAutoDetail = lstPOAutoDetail;
	}

	public List<StatusBean> getListApproveStatus() {
		return listApproveStatus;
	}

	public void setListApproveStatus(List<StatusBean> listApproveStatus) {
		this.listApproveStatus = listApproveStatus;
	}

	public Long getPoAutoId() {
		return poAutoId;
	}

	public void setPoAutoId(Long poAutoId) {
		this.poAutoId = poAutoId;
	}

	public String getPoAutoNumer() {
		return poAutoNumer;
	}

	public void setPoAutoNumer(String poAutoNumer) {
		this.poAutoNumer = poAutoNumer;
	}

	public BigDecimal getTotalAmountDetail() {
		return totalAmountDetail;
	}

	public void setTotalAmountDetail(BigDecimal totalAmountDetail) {
		this.totalAmountDetail = totalAmountDetail;
	}

	public List<Long> getListChoosePOAutoId() {
		return listChoosePOAutoId;
	}

	public void setListChoosePOAutoId(List<Long> listChoosePOAutoId) {
		this.listChoosePOAutoId = listChoosePOAutoId;
	}

	public boolean checkExistShopOfUserLogin() {
		if (currentUser != null && currentUser.getUserName() != null) {
			try {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
			} catch (BusinessException e) {
				LogUtility.logError(e, e.getMessage());
				return false;
			}
			if (staff != null && staff.getShop() != null) {
				shop = staff.getShop();
			}
		}
		if (shop == null)
			return false;
		else
			return true;
	}

	@Override
	public void prepare() throws Exception {
		poMgr = (PoMgr) context.getBean("poMgr");
		saleDayMgr = (SaleDayMgr) context.getBean("saleDayMgr");
		salePlanMgr = (SalePlanMgr) context.getBean("salePlanMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		listApproveStatus = new ArrayList<StatusBean>();
		//chua duyet
		listApproveStatus.add(new StatusBean((long) ApprovalStatus.NOT_YET.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.not.approved")));
		//da gui
		listApproveStatus.add(new StatusBean((long) ApprovalStatus.APPROVED.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.approved.send")));
		//da huy
		listApproveStatus.add(new StatusBean((long) ApprovalStatus.DESTROY.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.destroy")));
		//tu choi
		listApproveStatus.add(new StatusBean((long) ApprovalStatus.REJECT.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.reject")));
		//da duyet NCC
		//		listApproveStatus.add(new StatusBean((long)ApprovalStatus.NCC_APPROVED.getValue(),
		//				Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.approved.ncc")));
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);

		boolean isExistShop = checkExistShopOfUserLogin();
		if (isExistShop) {
			ShopToken sh = currentUser.getShopRoot();
			if (ShopObjectType.NPP.getValue().equals(sh.getObjectType()) || ShopObjectType.NPP_KA.getValue().equals(sh.getObjectType()) || ShopObjectType.MIEN_ST.getValue().equals(sh.getObjectType())) {
				isEditShopCode = true;
			} else {
				isEditShopCode = false;
			}

			startDate = DateUtil.toDateString(DateUtil.getCurrentGMTDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			endDate = DateUtil.toDateString(DateUtil.getCurrentGMTDate(), DateUtil.DATE_FORMAT_DDMMYYYY);

			if (isEditShopCode) {
				lstPOAuto = poMgr.getListPoAuto01(sh.getShopId(), poNumber, DateUtil.getCurrentGMTDate(), DateUtil.getCurrentGMTDate(), null, false);
			} else {
				if (shopId == null) {
					lstPOAuto = poMgr.getListPoAuto01(sh.getShopId(), poNumber, DateUtil.getCurrentGMTDate(), DateUtil.getCurrentGMTDate(), ApprovalStatus.APPROVED, true);
				} else {
					lstPOAuto = poMgr.getListPoAuto01(shopId, poNumber, DateUtil.getCurrentGMTDate(), DateUtil.getCurrentGMTDate(), ApprovalStatus.APPROVED, false);
				}
			}
		}
		return SUCCESS;
	}

	/**
	 * Search.
	 * 
	 * @return the string
	 * @author phut
	 * @since Oct 1, 2012
	 */
	public String search() throws Exception {
		try {
			//			boolean isExistShop = checkExistShopOfUserLogin();
			if (currentUser != null && currentUser.getUserId() != null) {
				ShopToken sh = currentUser.getShopRoot();
				if (ShopObjectType.NPP.getValue().equals(sh.getObjectType()) || ShopObjectType.NPP_KA.getValue().equals(sh.getObjectType()) || ShopObjectType.MIEN_ST.getValue().equals(sh.getObjectType())) {
					isEditShopCode = true;
				} else {
					isEditShopCode = false;
				}

				Date fromDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date toDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (isEditShopCode) {
					lstPOAuto = poMgr.getListPoAuto01(sh.getShopId(), poNumber, fromDate, toDate, approveStatus == -1 ? null : ApprovalStatus.parseValue(approveStatus), false);
				} else {
					Boolean isFindShopChild = false;
					Integer shopObjectType = sh.getObjectType();
					if (shopCode == null || shopCode.length() == 0) {
						shopId = sh.getShopId();
						isFindShopChild = true;
					} else {
						shop = shopMgr.getShopByCode(shopCode);
						if (shop == null || !checkShopPermission(shopCode)) {
							return SUCCESS;
						}
						isFindShopChild = false;
						shopId = shop.getId();
						//						shopObjectType = shop.getType().getObjectType();
					}
					if (ShopObjectType.NPP.getValue().equals(shopObjectType) || ShopObjectType.NPP_KA.getValue().equals(shopObjectType) || ShopObjectType.MIEN_ST.getValue().equals(shopObjectType)) {
						isFindShopChild = false;
					} else {
						isFindShopChild = true;
					}
					lstPOAuto = poMgr.getListPoAuto01(shopId, poNumber, fromDate, toDate, ApprovalStatus.APPROVED, isFindShopChild);
				}
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "ManagePOAuto.search - " + ex.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Approve po auto.
	 * 
	 * @return the string
	 * @author phut
	 * @since Oct 1, 2012
	 */
	public String approvePOAuto() throws Exception {
		resetToken(result);
		try {
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			ShopToken sh = currentUser.getShopRoot();
			if (ShopObjectType.NPP.getValue().equals(sh.getObjectType()) || ShopObjectType.NPP_KA.getValue().equals(sh.getObjectType()) || ShopObjectType.MIEN_ST.getValue().equals(sh.getObjectType())) {
				isEditShopCode = true;
			} else {
				isEditShopCode = false;
			}

			Date fromDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);

			//lstPOAuto = poMgr.getListPoAuto01(shop.getId(), poNumber, fromDate, toDate, approveStatus == null ? null : ApprovalStatus.parseValue(approveStatus), false);

			if (isEditShopCode) {
				lstPOAuto = poMgr.getListPoAuto01(sh.getShopId(), poNumber, fromDate, toDate, approveStatus == -1 ? null : ApprovalStatus.parseValue(approveStatus), false);
			} else {
				Boolean isFindShopChild = false;
				if (shopCode == null || shopCode.length() == 0) {
					shopId = sh.getShopId();
					isFindShopChild = true;
				} else {
					shop = shopMgr.getShopByCode(shopCode);
					if (shop == null || !checkShopPermission(sh.getShopCode())) {
						return SUCCESS;
					}
					isFindShopChild = false;
					shopId = shop.getId();
				}
				lstPOAuto = poMgr.getListPoAuto01(shopId, poNumber, fromDate, toDate, ApprovalStatus.APPROVED, isFindShopChild);
			}

			if (listChoosePOAutoId != null && listChoosePOAutoId.size() > 0 && lstPOAuto != null && lstPOAuto.size() > 0) {
				for (Long poId : listChoosePOAutoId) {
					boolean isBelong = false;
					for (PoAuto01VO poAuto01VO : lstPOAuto) {
						if (poId.longValue() == poAuto01VO.getPoAutoId().longValue()) {
							isBelong = true;
						}
					}
					if (!isBelong) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "duyệt hóa đơn"));
						return ERROR;
					}
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				return ERROR;
			}

			List<PoAuto> listPoAutoApprove = new ArrayList<PoAuto>();
			for (Long poAutoId : listChoosePOAutoId) {
				PoAuto poAuto = poMgr.getPoAutoById(poAutoId);
				if (!poAuto.getStatus().equals(ApprovalStatus.NOT_YET)) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_POAUTO_NOT_YET));
					return ERROR;
				}
				if (poAuto != null) {
					listPoAutoApprove.add(poAuto);
				}
			}
			if (type != null && type == 0) {
				poMgr.refusePoAuto(listPoAutoApprove);
			} else {
				poMgr.approvedPoAuto(listPoAutoApprove);
				ApParam ap = apParamMgr.getApParamByCode("AUTO_PO_XML", ApParamType.AUTO_PO_XML); //bug:9114

				if (ap != null && ActiveType.RUNNING.equals(ap.getStatus()) && "1".equals(ap.getValue())) {
					Exp_Xml_PO_Auto_ZIP_FILE(listPoAutoApprove, false);
				}
			}
			result.put(ERROR, false);
		} catch (Exception ex) {
			result.put(ERROR, true);
			if (ex.equals(ExceptionCode.PO_AUTO_STATUS_NOT_YET)) {
				result.put("errorType", 2);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_POAUTO_NOT_YET));
			} else {
				LogUtility.logError(ex, "ManagePOAuto.approvePOAuto - " + ex.getMessage());
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * Load po auto detail.
	 * 
	 * @return the string
	 * @author phut
	 * @since Oct 1, 2012
	 */
	public String loadPOAutoDetail() throws Exception {
		try {
			if (poAutoId != null && poAutoId > 0) {
				lstPOAutoDetail = poMgr.getListPoAutoDetail(null, poAutoId);
				if (lstPOAutoDetail != null && lstPOAutoDetail.size() > 0) {
					totalAmountDetail = new BigDecimal(0);
					for (PoAutoDetail poAutoDetail : lstPOAutoDetail) {
						if (poAutoDetail.getAmount() != null) {
							totalAmountDetail = totalAmountDetail.add(poAutoDetail.getAmount());
						}
					}
				}
				PoAuto po = poMgr.getPoAutoById(poAutoId);
				if (po != null) {
					shop = po.getShop();
				}
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "ManagePOAuto.loadPOAutoDetail - " + ex.getMessage());

			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * Export po auto report.
	 * 
	 * @return the string
	 * @author liemtpt
	 * @throws BusinessException
	 */
	private String outputName;

	public String getOutputName() {
		return outputName;
	}

	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	/**
	 * Export po auto report.
	 * 
	 * @return the string
	 * @author phut
	 * @since Oct 1, 2012
	 */
	public String exportPOAutoReport() throws Exception {
		try {
			//currentUser = getCurrentUser();
			//List<RoleToken> listRole = currentUser.getRolesList();
			//			for(RoleToken token : listRole) {
			//				String roleCode = token.getRoleCode();
			//				if(roleCode.equals(VSARole.VNM_SALESONLINE_DISTRIBUTOR.getValue())) {
			//					isEditShopCode = true;
			//				} else if(roleCode.equals(VSARole.VNM_SALESONLINE_HO.getValue())) {
			//					isEditShopCode = false;
			//				}
			//			}
			Staff staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			ShopToken sh = currentUser.getShopRoot();
			if (ShopObjectType.NPP.getValue().equals(sh.getObjectType()) || ShopObjectType.NPP_KA.getValue().equals(sh.getObjectType()) || ShopObjectType.MIEN_ST.getValue().equals(sh.getObjectType())) {
				isEditShopCode = true;
			} else {
				isEditShopCode = false;
			}
			if (isEditShopCode) {
				shopId = sh.getShopId();
				shop = shopMgr.getShopById(shopId);
			} else {
				if (shopId == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, "NPP"));
					return ERROR;
				} else {
					shop = shopMgr.getShopById(shopId);
				}
			}
			List<PoAutoGroupByCatVO> listReport = poMgr.getDataForComparePOAutoReport(shopId, poAutoId);
			if (listReport == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.no.data.report"));
				return ERROR;
			}
			Map<String, Object> beans = new HashMap<String, Object>();

			Calendar calendar = new GregorianCalendar();
			Calendar calTmp = new GregorianCalendar();
			calTmp.set(Calendar.DATE, 1);
			Date beginDate = calTmp.getTime();
			Integer realWorkDateInMonth = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(beginDate, calendar.getTime());
			SaleDays saleDay = saleDayMgr.getSaleDayByYear(calendar.get(Calendar.YEAR));
			Integer workDateInMonth = 0;
			switch (calendar.get(Calendar.MONTH)) {
			case 0:
				workDateInMonth = saleDay.getT1();
				break;
			case 1:
				workDateInMonth = saleDay.getT2();
				break;
			case 2:
				workDateInMonth = saleDay.getT3();
				break;
			case 3:
				workDateInMonth = saleDay.getT4();
				break;
			case 4:
				workDateInMonth = saleDay.getT5();
				break;
			case 5:
				workDateInMonth = saleDay.getT6();
				break;
			case 6:
				workDateInMonth = saleDay.getT7();
				break;
			case 7:
				workDateInMonth = saleDay.getT8();
				break;
			case 8:
				workDateInMonth = saleDay.getT9();
				break;
			case 9:
				workDateInMonth = saleDay.getT10();
				break;
			case 10:
				workDateInMonth = saleDay.getT11();
				break;
			case 11:
				workDateInMonth = saleDay.getT12();
				break;
			}
			beans.put("shop", shop);
			if (shop != null) {
				/*
				 * Shop shopVNM = shopMgr.getShopByCode("VNM"); if(shopVNM !=
				 * null){ List<Shop> lstShopChildOfVNM =
				 * shopMgr.getListSubShopEx(shopVNM.getId(),ActiveType.RUNNING,
				 * null); if(lstShopChildOfVNM != null){ int num =
				 * lstShopChildOfVNM.size(); for(int i= 0;i<num;i++){
				 * if(checkShopPermission(shop.getShopCode())){
				 * beans.put("parentShop",
				 * lstShopChildOfVNM.get(i).getShopName()); break; } } } }
				 */
				if (shop.getParentShop() != null) {
					beans.put("parentShop", shop.getParentShop().getShopName());
				}
			}
			beans.put("printDate", displayDate(DateUtil.getCurrentGMTDate()));
			beans.put("realWorkDateInMonth", realWorkDateInMonth);
			beans.put("workDateInMonth", workDateInMonth);
			beans.put("report", listReport);
			beans.put("beginDate", displayDate(beginDate));
			beans.put("toDate", displayDate(DateUtil.getCurrentGMTDate()));
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathPO(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.EXPORT_POAUTO_TEMPLATE;
			templateFileName = templateFileName.replace('/', File.separatorChar);

			String outputName = ConstantManager.EXPORT_POAUTO + "_" + shop.getShopCode() + "_" + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();

			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * Xuat file XML
	 * 
	 * @since Sep 08, 2014
	 */
	public String exportPOAutoXML() throws Exception {
		resetToken(result);
		try {
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return JSON;
			}
			ShopToken sh = currentUser.getShopRoot();
			if (ShopObjectType.NPP.getValue().equals(sh.getObjectType()) || ShopObjectType.NPP_KA.getValue().equals(sh.getObjectType()) || ShopObjectType.MIEN_ST.getValue().equals(sh.getObjectType())) {
				isEditShopCode = true;
			} else {
				isEditShopCode = false;
			}

			Date fromDate = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);

			if (isEditShopCode) {
				lstPOAuto = poMgr.getListPoAuto01(sh.getShopId(), poNumber, fromDate, toDate, approveStatus == -1 ? null : ApprovalStatus.parseValue(approveStatus), false);
			} else {
				Boolean isFindShopChild = false;
				if (shopCode == null || shopCode.length() == 0) {
					shopId = sh.getShopId();
					isFindShopChild = true;
				} else {
					shop = shopMgr.getShopByCode(shopCode);
					if (shop == null || !checkShopPermission(sh.getShopCode())) {
						return SUCCESS;
					}
					isFindShopChild = false;
					shopId = shop.getId();
				}
				lstPOAuto = poMgr.getListPoAuto01(shopId, poNumber, fromDate, toDate, ApprovalStatus.APPROVED, isFindShopChild);
			}

			if (listChoosePOAutoId != null && listChoosePOAutoId.size() > 0 && lstPOAuto != null && lstPOAuto.size() > 0) {
				for (Long poId : listChoosePOAutoId) {
					boolean isBelong = false;
					for (PoAuto01VO poAuto01VO : lstPOAuto) {
						if (poId.longValue() == poAuto01VO.getPoAutoId().longValue()) {
							isBelong = true;
						}
					}
					if (!isBelong) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "duyệt hóa đơn"));
						return JSON;
					}
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				return JSON;
			}

			List<PoAuto> listPoAutoApprove = new ArrayList<PoAuto>();
			PoAuto poAuto = null;
			for (Long poAutoId : listChoosePOAutoId) {
				poAuto = poMgr.getPoAutoById(poAutoId);
				if (poAuto == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
					return JSON;
				}
				if (!ApprovalStatus.APPROVED.equals(poAuto.getStatus())) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("po.manage.poauto.status.not.approved.error2", poAuto.getPoAutoNumber()));
					return JSON;
				}
				listPoAutoApprove.add(poAuto);
			}

			String path = Exp_Xml_PO_Auto_ZIP_FILE(listPoAutoApprove, true);
			if (path != null) {
				result.put(ERROR, false);
				result.put(REPORT_PATH, path);
				return JSON;
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ManagePOAuto.exportPOAutoXML - " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
	}

	/**
	 * Xuat xml ra file zip
	 * 
	 * @author tungtt21
	 * @param listPoAutoApprove
	 * @throws BusinessException
	 */
	public String Exp_Xml_PO_Auto_ZIP_FILE(List<PoAuto> listPoAutoApprove, boolean isZip) throws Exception {
		String shopCode = null;
		String path = null;
		String fname = null;
		//try {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		// root elements = PO
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("PO");
		doc.appendChild(rootElement);

		Element newPO = null;
		Element poHeader = null;
		Element tmpE = null;
		org.w3c.dom.Text tmpT = null;
		Element poDetailE = null;
		Element line = null;
		List<PoAutoDetail> autoDetails = null;
		PoAutoDetail poAutoDetail = null;
		Price pr = null;
		BigDecimal tmpLineTotal = null;
		Integer maxVersion = null;
		SalePlan salePlan = null;

		int j, szj;
		for (PoAuto poAuto : listPoAutoApprove) {
			// NewPO elements
			newPO = doc.createElement("NewPO");
			rootElement.appendChild(newPO);

			//POHeader element
			poHeader = doc.createElement("POHeader");
			newPO.appendChild(poHeader);
			// DistCode elements
			//Element distCode = doc.createElement("DistCode");
			tmpE = doc.createElement("DistCode");
			shopCode = poAuto.getShop().getShopCode();
			tmpT = doc.createTextNode(shopCode);
			tmpE.appendChild(tmpT);
			poHeader.appendChild(tmpE);

			//PONumber elements
			//Element poNumber = doc.createElement("PONumber");
			tmpE = doc.createElement("PONumber");
			tmpT = doc.createTextNode(poAuto.getPoAutoNumber());
			tmpE.appendChild(tmpT);
			poHeader.appendChild(tmpE);

			//BillToLocation element
			//Element billToLocation = doc.createElement("BillToLocation");
			tmpE = doc.createElement("BillToLocation");
			tmpT = doc.createTextNode(poAuto.getBilltolocation() != null ? poAuto.getBilltolocation() : poAuto.getShop().getAddress());
			tmpE.appendChild(tmpT);
			poHeader.appendChild(tmpE);

			//ShipToLocation element
			//Element shipToLocation = doc.createElement("ShipToLocation");
			tmpE = doc.createElement("ShipToLocation");
			tmpT = doc.createTextNode(poAuto.getShiptolocation() != null ? poAuto.getShiptolocation() : "1");
			tmpE.appendChild(tmpT);
			poHeader.appendChild(tmpE);

			//OrderDate element
			//Element orderDate = doc.createElement("OrderDate");
			tmpE = doc.createElement("OrderDate");
			tmpT = doc.createTextNode(DateUtil.toDateString(poAuto.getPoAutoDate(), DateUtil.DATE_FORMAT_ATTRIBUTE) + "T00:00:00+07:00");
			tmpE.appendChild(tmpT);
			poHeader.appendChild(tmpE);

			//Status element
			//Element status = doc.createElement("Status");
			tmpE = doc.createElement("Status");
			tmpT = doc.createTextNode("O");
			tmpE.appendChild(tmpT);
			poHeader.appendChild(tmpE);

			//Total element
			//Element total = doc.createElement("Total");
			tmpE = doc.createElement("Total");
			tmpT = doc.createTextNode(poAuto.getAmount().toString());
			tmpE.appendChild(tmpT);
			poHeader.appendChild(tmpE);

			//PaymentTerm element
			//Element paymentTerm = doc.createElement("PaymentTerm");
			tmpE = doc.createElement("PaymentTerm");
			tmpT = doc.createTextNode("12 NET");
			tmpE.appendChild(tmpT);
			poHeader.appendChild(tmpE);

			//PODetail elements
			poDetailE = doc.createElement("PODetail");
			newPO.appendChild(poDetailE);

			autoDetails = poMgr.getListPoAutoDetail(null, poAuto.getId());
			for (j = 0, szj = autoDetails.size(); j < szj; j++) {
				poAutoDetail = autoDetails.get(j);
				//Line elements
				line = doc.createElement("Line");
				poDetailE.appendChild(line);

				// DistCode elements
				tmpE = doc.createElement("DistCode");
				tmpT = doc.createTextNode(shopCode);
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//PONumber elements
				tmpE = doc.createElement("PONumber");
				tmpT = doc.createTextNode(poAuto.getPoAutoNumber());
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//ItemCode element
				//Element itemCode = doc.createElement("ItemCode");
				tmpE = doc.createElement("ItemCode");
				tmpT = doc.createTextNode(poAutoDetail.getProduct().getProductCode());
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//ItemDescr element
				//Element itemDescr = doc.createElement("ItemDescr");
				tmpE = doc.createElement("ItemDescr");
				tmpT = doc.createTextNode(poAutoDetail.getProduct().getProductName());
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//UOM element
				//Element uom = doc.createElement("UOM");
				tmpE = doc.createElement("UOM");
				tmpT = doc.createTextNode(poAutoDetail.getProduct().getUom1());
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//SiteID element
				//Element siteID = doc.createElement("SiteID");
				tmpE = doc.createElement("SiteID");
				tmpT = doc.createTextNode("KHOCHINH");
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//Quantity element
				//Element quantity = doc.createElement("Quantity");
				tmpE = doc.createElement("Quantity");
				tmpT = doc.createTextNode(poAutoDetail.getQuantity() != null ? poAutoDetail.getQuantity().toString() : "0");
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//Price element
				pr = productMgr.getPriceByProductAndShopId(poAutoDetail.getProduct().getId(), poAuto.getShop().getId());
				//Element price = doc.createElement("Price");
				tmpE = doc.createElement("Price");
				if (pr != null) {
					tmpT = doc.createTextNode(pr.getPriceNotVat() != null ? pr.getPriceNotVat().toString() : "0");
					tmpE.appendChild(tmpT);
				}
				line.appendChild(tmpE);

				//LineTotal element
				tmpLineTotal = BigDecimal.ZERO;
				if (pr != null && pr.getPriceNotVat() != null && poAutoDetail.getQuantity() != null) {
					tmpLineTotal = pr.getPriceNotVat().multiply(new BigDecimal(poAutoDetail.getQuantity()));
				}
				//Element lineTotal = doc.createElement("LineTotal");
				tmpE = doc.createElement("LineTotal");
				tmpT = doc.createTextNode(tmpLineTotal.toString());
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//RequestDate element
				//Element requestDate = doc.createElement("RequestDate");
				tmpE = doc.createElement("RequestDate");
				tmpT = doc.createTextNode(DateUtil.toDateString(poAuto.getPoAutoDate(), DateUtil.DATE_FORMAT_ATTRIBUTE) + "T00:00:00+07:00");
				tmpE.appendChild(tmpT);
				line.appendChild(tmpE);

				//VERSION element
				//Element version = doc.createElement("VERSION");
				tmpE = doc.createElement("VERSION");
				maxVersion = salePlanMgr.getVersion(poAutoDetail.getPoAuto().getShop().getId(), poAutoDetail.getProduct().getId(), SalePlanOwnerType.SHOP, PlanType.MONTH, DateUtil.getMonth(poAuto.getPoAutoDate()), DateUtil.getYear(poAuto
						.getPoAutoDate()));
				if (maxVersion != null) {
					tmpT = doc.createTextNode(maxVersion.toString());
					tmpE.appendChild(tmpT);
				}
				line.appendChild(tmpE);

				//PLANQTY element
				salePlan = salePlanMgr.getSalePlanOfVNM(poAutoDetail.getPoAuto().getShop().getId(), poAutoDetail.getProduct().getId(), SalePlanOwnerType.SHOP, PlanType.MONTH, DateUtil.getMonth(poAuto.getPoAutoDate()), DateUtil.getYear(poAuto
						.getPoAutoDate()));
				//Element planqty = doc.createElement("PLANQTY");
				tmpE = doc.createElement("PLANQTY");
				if (salePlan != null) {
					tmpT = doc.createTextNode(salePlan.getQuantity() != null ? salePlan.getQuantity().toString() : "0");
					tmpE.appendChild(tmpT);
				}
				line.appendChild(tmpE);
			}
		}

		if (isZip) {
			path = Configuration.getStoreRealPath() + File.separator;
			String fileNameZIP = "Exp_PO_" + shopCode + "_" + genFileSuffix() + FileExtension.ZIP.getValue();
			fname = "Exp_PO_" + shopCode + "_" + genFileSuffix() + ".xml";
			File f1 = new File(path + fname);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f1);
			transformer.transform(source, result);

			ApParam ap = apParamMgr.getApParamByCode("AUTO_PO_ZIP_PASS", ApParamType.AUTO_PO_ZIP_PASS);
			ZipFile zipFile = new ZipFile(path + fileNameZIP);
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			parameters.setEncryptFiles(true);
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
			parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
			if (ap != null && ap.getStatus() != null && ap.getStatus().equals(ActiveType.RUNNING) && ap.getValue() != null) {
				parameters.setPassword(ap.getValue());
			}
			zipFile.addFile(f1, parameters);
			outPath = Configuration.getExportExcelPath() + fileNameZIP;
			f1.delete();
		} else {
			path = Configuration.getPORealPath() + File.separator + shopCode;
			fname = "Exp_PO_" + shopCode + "_" + genFileSuffix() + ".xml";

			File f = new File(path);
			File f1 = new File(fname);
			f.mkdirs();
			try {
				f1.createNewFile();
			} catch (IOException e) {
				LogUtility.logError(e, e.getMessage());
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(path + File.separator + fname));
			transformer.transform(source, result);
			outPath = path;
		}
		/*
		 * } catch (ParserConfigurationException pce) { LogUtility.logError(pce,
		 * pce.getMessage()); } catch (TransformerException tfe) {
		 * LogUtility.logError(tfe, tfe.getMessage()); } catch(Exception e){
		 * LogUtility.logError(e, e.getMessage()); }
		 */
		return outPath;
	}
}

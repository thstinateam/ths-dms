package ths.dms.web.action.po;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.PoMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.PoVnmDetail;
import ths.dms.core.entities.PoVnmLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.exceptions.BusinessException;

// TODO: Auto-generated Javadoc

/**
 * The Class ReturnProductAction.
 */
public class ReturnProductAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	/** The staff mgr. */
	private StaffMgr staffMgr;

	/** The po mgr. */
	private PoMgr poMgr;

	/** The shop mgr. */
	private ShopMgr shopMgr;

	/** The shop. */
	private Shop shop;

	/** The staff. */
	private Staff staff;

	/** The lst po confirm. */
	private List<PoVnm> lstPOConfirm;

	/** The invoice code. */
	private String invoiceCode;

	/** The from date. */
	private String fromDate;

	/** The to date. */
	private String toDate;

	/** The po confirm code. */
	private String poConfirmCode;

	/** The checkenable. */
	private Boolean checkenable;

	/** The po confirm code detail. */
	private String poConfirmCodeDetail;

	/** The lst po vnm detail l lot vo. */
	private List<PoVnmDetailLotVO> lstPOVnmDetailLLotVo;

	/** The lst po vnm detail l lot vo id. */
	private List<Long> lstPOVnmDetailLLotVoID;

	/** The lst po vnm detail l lot vo lot. */
	private List<String> lstPOVnmDetailLLotVoLot;

	/** The lst po vnm detail l lot vo value. */
	private List<Integer> lstPOVnmDetailLLotVoValue;

	/** The invoice code return. */
	private String invoiceCodeReturn;

	/** The invoice date return. */
	private String invoiceDateReturn;

	/** The po confirm code return. */
	private String poConfirmCodeReturn;

	/** The po vnm export code. */
	private String poVnmExportCode;

	private Integer totalQuantity;

	private BigDecimal totalAmount;

	private List<Long> lstWarehouseId;
	private List<Long> lstProductIdKho;

	public List<Long> getLstWarehouseId() {
		return lstWarehouseId;
	}

	public void setLstWarehouseId(List<Long> lstWarehouseId) {
		this.lstWarehouseId = lstWarehouseId;
	}

	public List<Long> getLstProductIdKho() {
		return lstProductIdKho;
	}

	public void setLstProductIdKho(List<Long> lstProductIdKho) {
		this.lstProductIdKho = lstProductIdKho;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * Gets the staff mgr.
	 * 
	 * @return the staff mgr
	 */
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	/**
	 * Sets the staff mgr.
	 * 
	 * @param staffMgr
	 *            the new staff mgr
	 */
	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	/**
	 * Gets the po mgr.
	 * 
	 * @return the po mgr
	 */
	public PoMgr getPoMgr() {
		return poMgr;
	}

	/**
	 * Sets the po mgr.
	 * 
	 * @param poMgr
	 *            the new po mgr
	 */
	public void setPoMgr(PoMgr poMgr) {
		this.poMgr = poMgr;
	}

	/**
	 * Gets the shop mgr.
	 * 
	 * @return the shop mgr
	 */
	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	/**
	 * Sets the shop mgr.
	 * 
	 * @param shopMgr
	 *            the new shop mgr
	 */
	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	/**
	 * Gets the shop.
	 * 
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 * 
	 * @param shop
	 *            the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#getStaff()
	 */
	public Staff getStaff() {
		return staff;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.web.action.general.AbstractAction#setStaff(ths.dms.core.entities
	 * .Staff)
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	/**
	 * Gets the lst po confirm.
	 * 
	 * @return the lst po confirm
	 */
	public List<PoVnm> getLstPOConfirm() {
		return lstPOConfirm;
	}

	/**
	 * Sets the lst po confirm.
	 * 
	 * @param lstPOConfirm
	 *            the new lst po confirm
	 */
	public void setLstPOConfirm(List<PoVnm> lstPOConfirm) {
		this.lstPOConfirm = lstPOConfirm;
	}

	/**
	 * Gets the invoice code.
	 * 
	 * @return the invoice code
	 */
	public String getInvoiceCode() {
		return invoiceCode;
	}

	/**
	 * Sets the invoice code.
	 * 
	 * @param invoiceCode
	 *            the new invoice code
	 */
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	/**
	 * Gets the from date.
	 * 
	 * @return the from date
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * Sets the from date.
	 * 
	 * @param fromDate
	 *            the new from date
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * Gets the to date.
	 * 
	 * @return the to date
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * Sets the to date.
	 * 
	 * @param toDate
	 *            the new to date
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * Gets the po confirm code.
	 * 
	 * @return the po confirm code
	 */
	public String getPoConfirmCode() {
		return poConfirmCode;
	}

	/**
	 * Sets the po confirm code.
	 * 
	 * @param poConfirmCode
	 *            the new po confirm code
	 */
	public void setPoConfirmCode(String poConfirmCode) {
		this.poConfirmCode = poConfirmCode;
	}

	/**
	 * Gets the checkenable.
	 * 
	 * @return the checkenable
	 */
	public Boolean getCheckenable() {
		return checkenable;
	}

	/**
	 * Sets the checkenable.
	 * 
	 * @param checkenable
	 *            the new checkenable
	 */
	public void setCheckenable(Boolean checkenable) {
		this.checkenable = checkenable;
	}

	/**
	 * Gets the po confirm code detail.
	 * 
	 * @return the po confirm code detail
	 */
	public String getPoConfirmCodeDetail() {
		return poConfirmCodeDetail;
	}

	/**
	 * Sets the po confirm code detail.
	 * 
	 * @param poConfirmCodeDetail
	 *            the new po confirm code detail
	 */
	public void setPoConfirmCodeDetail(String poConfirmCodeDetail) {
		this.poConfirmCodeDetail = poConfirmCodeDetail;
	}

	/**
	 * Gets the lst po vnm detail l lot vo.
	 * 
	 * @return the lst po vnm detail l lot vo
	 */
	public List<PoVnmDetailLotVO> getLstPOVnmDetailLLotVo() {
		return lstPOVnmDetailLLotVo;
	}

	/**
	 * Sets the lst po vnm detail l lot vo.
	 * 
	 * @param lstPOVnmDetailLLotVo
	 *            the new lst po vnm detail l lot vo
	 */
	public void setLstPOVnmDetailLLotVo(List<PoVnmDetailLotVO> lstPOVnmDetailLLotVo) {
		this.lstPOVnmDetailLLotVo = lstPOVnmDetailLLotVo;
	}

	/**
	 * Gets the lst po vnm detail l lot vo id.
	 * 
	 * @return the lst po vnm detail l lot vo id
	 */
	public List<Long> getLstPOVnmDetailLLotVoID() {
		return lstPOVnmDetailLLotVoID;
	}

	/**
	 * Sets the lst po vnm detail l lot vo id.
	 * 
	 * @param lstPOVnmDetailLLotVoID
	 *            the new lst po vnm detail l lot vo id
	 */
	public void setLstPOVnmDetailLLotVoID(List<Long> lstPOVnmDetailLLotVoID) {
		this.lstPOVnmDetailLLotVoID = lstPOVnmDetailLLotVoID;
	}

	/**
	 * Gets the lst po vnm detail l lot vo lot.
	 * 
	 * @return the lst po vnm detail l lot vo lot
	 */
	public List<String> getLstPOVnmDetailLLotVoLot() {
		return lstPOVnmDetailLLotVoLot;
	}

	/**
	 * Sets the lst po vnm detail l lot vo lot.
	 * 
	 * @param lstPOVnmDetailLLotVoLot
	 *            the new lst po vnm detail l lot vo lot
	 */
	public void setLstPOVnmDetailLLotVoLot(List<String> lstPOVnmDetailLLotVoLot) {
		this.lstPOVnmDetailLLotVoLot = lstPOVnmDetailLLotVoLot;
	}

	public List<Integer> getLstPOVnmDetailLLotVoValue() {
		return lstPOVnmDetailLLotVoValue;
	}

	public void setLstPOVnmDetailLLotVoValue(List<Integer> lstPOVnmDetailLLotVoValue) {
		this.lstPOVnmDetailLLotVoValue = lstPOVnmDetailLLotVoValue;
	}

	/**
	 * Gets the invoice code return.
	 * 
	 * @return the invoice code return
	 */
	public String getInvoiceCodeReturn() {
		return invoiceCodeReturn;
	}

	/**
	 * Sets the invoice code return.
	 * 
	 * @param invoiceCodeReturn
	 *            the new invoice code return
	 */
	public void setInvoiceCodeReturn(String invoiceCodeReturn) {
		this.invoiceCodeReturn = invoiceCodeReturn;
	}

	/**
	 * Gets the invoice date return.
	 * 
	 * @return the invoice date return
	 */
	public String getInvoiceDateReturn() {
		return invoiceDateReturn;
	}

	/**
	 * Sets the invoice date return.
	 * 
	 * @param invoiceDateReturn
	 *            the new invoice date return
	 */
	public void setInvoiceDateReturn(String invoiceDateReturn) {
		this.invoiceDateReturn = invoiceDateReturn;
	}

	/**
	 * Gets the po confirm code return.
	 * 
	 * @return the po confirm code return
	 */
	public String getPoConfirmCodeReturn() {
		return poConfirmCodeReturn;
	}

	/**
	 * Sets the po confirm code return.
	 * 
	 * @param poConfirmCodeReturn
	 *            the new po confirm code return
	 */
	public void setPoConfirmCodeReturn(String poConfirmCodeReturn) {
		this.poConfirmCodeReturn = poConfirmCodeReturn;
	}

	/**
	 * Gets the po vnm export code.
	 * 
	 * @return the po vnm export code
	 */
	public String getPoVnmExportCode() {
		return poVnmExportCode;
	}

	/**
	 * Sets the po vnm export code.
	 * 
	 * @param poVnmExportCode
	 *            the new po vnm export code
	 */
	public void setPoVnmExportCode(String poVnmExportCode) {
		this.poVnmExportCode = poVnmExportCode;
	}

	/**
	 * @author tientv
	 * @see
	 * 
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		poMgr = (PoMgr) context.getBean("poMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
	}

	/***
	 * @author tientv
	 */
	@Override
	public String execute() {
		resetToken(result);
		try {
			shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			Date lockDate = shopLockMgr.getNextLockedDay(shop.getId());
			if (lockDate == null) {
				lockDate = commonMgr.getSysDate();
			}
			fromDate = DateUtil.toDateString(lockDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			toDate = fromDate;
			PoVnmFilter filter = new PoVnmFilter();
			filter.setShopId(shop.getId());
			filter.setfDate(lockDate);
			filter.settDate(lockDate);
			filter.setPoStatus(PoVNMStatus.IMPORTED);
			filter.setHasFindChildShop(false);
			filter.setPoType(PoType.PO_CONFIRM);
//			filter.setSortField("SHOP_CODE, INVOICE_NUMBER");
			filter.setIsLikePoNumber(true);
			lstPOConfirm = poMgr.getListPoConfirmByCondition(filter);
			checkenable = false;
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Search. Ham search() su dung de tim kiem danh sach hoa don
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 06/09/2014
	 */
	public String search() {
		try {
			shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			checkenable = false;
			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			PoVnmFilter filter = new PoVnmFilter();
			filter.setShopId(shop.getId());
			//            filter.setFromDate(fDate);
			//            filter.setToDate(tDate);
			filter.setfDate(fDate);
			filter.settDate(tDate);
			filter.setPoAutoNumber(poConfirmCode);
			filter.setInvoiceNumber(invoiceCode);
			filter.setPoStatus(PoVNMStatus.IMPORTED);
			filter.setHasFindChildShop(false);
			// lay tu MgrImpl ra
			filter.setPoType(PoType.PO_CONFIRM);
//			filter.setSortField("SHOP_CODE, INVOICE_NUMBER");
			filter.setIsLikePoNumber(true);
			lstPOConfirm = poMgr.getListPoConfirmByCondition(filter);
		} catch (Exception e) {
			LogUtility.logError(e, "Error ReturnProductAction.search: " + e.getMessage());
		}
		return LIST;
	}

	/**
	 * Detail. Ham detail() su dung de tim kiem chi tiet don hang povnmdetail
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 06/09/2014
	 */
	public String detail() {
		try {
			shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			checkenable = false;
			if (StringUtil.isNullOrEmpty(poConfirmCodeDetail)) {
				return LIST;
			}
			PoVnm checkpoConfirm = poMgr.getPoVnmByCode(poConfirmCodeDetail);
			if (checkpoConfirm != null && checkpoConfirm.getShop().getId().equals(shop.getId())) {
				checkenable = poMgr.checkValidateDatePoReturn(checkpoConfirm, shop.getId());
				ObjectVO<PoVnmDetailLotVO> vos = poMgr.getListPoVnmDetailLotVOByPoVnmIdEx(null, checkpoConfirm.getId(), shop.getId());
				totalQuantity = 0;
				totalAmount = BigDecimal.ZERO;
				if (vos != null) {
					lstPOVnmDetailLLotVo = vos.getLstObject();
					Map<Long, Integer> mapSP = new HashMap<Long, Integer>();
					PoVnmDetailLotVO detail = null;
					Integer slTonKho = 0;
					Integer slTra = 0;
					Integer slTra2 = 0;
					for (int i = 0, sz = lstPOVnmDetailLLotVo.size(); i < sz; i++) {
						detail = lstPOVnmDetailLLotVo.get(i);
						if (detail.getPoVnmDetailLotId() <= 0) {
							continue;
						}
						slTra = detail.getQuantity() - detail.getQuantityPay();
						slTra2 = mapSP.get(detail.getProductId());
						if (slTra2 == null) {
							slTra2 = 0;
						}
						slTra = slTra + slTra2;
						slTonKho = detail.getQuantityStock();
						if (slTra < 0) {
							detail.setReturnValue(0);
							mapSP.put(detail.getProductId(), slTra);
						} else if (slTonKho >= slTra) {
							detail.setReturnValue(slTra);
							mapSP.put(detail.getProductId(), 0);
						} else {
							detail.setReturnValue(slTonKho);
							mapSP.put(detail.getProductId(), slTra - slTonKho);
						}
						totalQuantity += lstPOVnmDetailLLotVo.get(i).getReturnValue();
						lstPOVnmDetailLLotVo.get(i).setAmount(lstPOVnmDetailLLotVo.get(i).getPrice().multiply(new BigDecimal(lstPOVnmDetailLLotVo.get(i).getReturnValue())));
						totalAmount = totalAmount.add(lstPOVnmDetailLLotVo.get(i).getAmount());
					}

					for (int i = 0, sz = lstPOVnmDetailLLotVo.size(); i < sz; i++) {
						detail = lstPOVnmDetailLLotVo.get(i);
						if (detail.getPoVnmDetailLotId() > 0) {
							continue;
						}
						slTra = mapSP.get(detail.getProductId());
						slTonKho = detail.getQuantityStock();
						if (slTra < 0) {
							detail.setReturnValue(0);
							mapSP.put(detail.getProductId(), slTra);
						} else if (slTonKho >= slTra) {
							detail.setReturnValue(slTra);
							mapSP.put(detail.getProductId(), 0);
						} else {
							detail.setReturnValue(slTonKho);
							mapSP.put(detail.getProductId(), slTra - slTonKho);
						}
						totalQuantity += lstPOVnmDetailLLotVo.get(i).getReturnValue();
						lstPOVnmDetailLLotVo.get(i).setAmount(lstPOVnmDetailLLotVo.get(i).getPrice().multiply(new BigDecimal(lstPOVnmDetailLLotVo.get(i).getReturnValue())));
						totalAmount = totalAmount.add(lstPOVnmDetailLLotVo.get(i).getAmount());
					}
				}
			}
		} catch (BusinessException e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "ReturnProductAction.detail");
			if (e.getMessage().equals("PRODUCT_HAS_DUPLICATE_PRICE_IN_PO_VNM")) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "return.product.has.different.price.in.two.warehouse");
			}
			return LIST;
		} catch (Exception e) {

			LogUtility.logError(e, "Error ReturnProductAction.detail: " + e.getMessage());
		}
		return LIST;
	}

	/**
	 * Return product. Ham returnProduct dung de thuc hien chuc nang tra hang
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 06/09/2014
	 */
	public String returnProduct() {
		resetToken(result);
		PoVnm newPoVnm = null;
		try {
			shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			staff = staffMgr.getStaffById(getCurrentUser().getUserId());
			if (staff == null) {
				checkenable = false;
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.returnproduct.return.permission.error")));
				return ERROR;
			}
			if (lstPOVnmDetailLLotVoID.size() <= 0 || lstPOVnmDetailLLotVoLot.size() <= 0 && lstPOVnmDetailLLotVoValue.size() <= 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.returnproduct.return.checknullsize"));
				return ERROR;
			}
			if (lstPOVnmDetailLLotVoID.size() != lstPOVnmDetailLLotVoLot.size() || lstPOVnmDetailLLotVoID.size() != lstPOVnmDetailLLotVoValue.size()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.returnproduct.return.checksize"));
				return ERROR;
			}
			if (lstProductIdKho.size() != lstPOVnmDetailLLotVoValue.size() || lstPOVnmDetailLLotVoValue.size() != lstWarehouseId.size()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.returnproduct.return.checksize"));
				return ERROR;
			}
			Map<Long, Integer> mapProduct = new HashMap<Long, Integer>();
			Long productId = null;
			List<Long> lstPoVNMDetailId = new ArrayList<Long>(); // dung update cho Povnmdetail
			List<Integer> lstValue = new ArrayList<Integer>(); // dung update cho Povnmdetail
			List<Long> lstProductIdForPOVnmDetail = new ArrayList<Long>();// dung update cho Povnmdetail
			for (int i = 0; i < lstPOVnmDetailLLotVoValue.size(); i++) {
				if (lstPOVnmDetailLLotVoID.get(i) != null && lstPOVnmDetailLLotVoID.get(i) > 0) {
					lstPoVNMDetailId.add(lstPOVnmDetailLLotVoID.get(i));
					lstValue.add(lstPOVnmDetailLLotVoValue.get(i));
					lstProductIdForPOVnmDetail.add(lstProductIdKho.get(i));
				} else if (lstPOVnmDetailLLotVoID.get(i) != null && lstPOVnmDetailLLotVoID.get(i) < 0) {
					if (lstProductIdKho.get(i) != productId) {
						productId = lstProductIdKho.get(i);
						Integer value = lstPOVnmDetailLLotVoValue.get(i);
						mapProduct.put(productId, value);
					} else {
						Integer value2 = mapProduct.get(productId) + lstPOVnmDetailLLotVoValue.get(i); //update lai gia tri cho san phan
						mapProduct.put(productId, value2);
					}
				}
			}
			//cong don value cho cac poVNMdetail 
			for (int i = 0; i < lstPoVNMDetailId.size(); i++) {
				Integer value = mapProduct.get(lstProductIdForPOVnmDetail.get(i));
				if (value != null) {
					Integer vl = lstValue.get(i) + value;
					lstValue.set(i, vl);
					mapProduct.put(lstProductIdForPOVnmDetail.get(i), 0);
				}
			}
			PoVnm fromPoVnm = poMgr.getPoVnmByCode(poConfirmCodeReturn);
			if (fromPoVnm == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.returnproduct.return.poConfirmCode.error"));
				return ERROR;
			}
			List<Boolean> lstCheckLot = getlstCheckLot(lstPOVnmDetailLLotVoID, lstPOVnmDetailLLotVoLot);
			newPoVnm = poMgr.createReturnPoVnm(shop.getId(), fromPoVnm.getId(), lstPOVnmDetailLLotVoID, lstCheckLot, lstPOVnmDetailLLotVoValue, null, null, null, null, null, staff.getStaffCode(), lstWarehouseId, lstProductIdKho, lstValue,
					lstPoVNMDetailId);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "Error ReturnProductAction.returnProduct: " + e.getMessage());
			return ERROR;
		}
		result.put(ERROR, false);
		if (newPoVnm != null) {
			result.put("poVnmExportCode", newPoVnm.getPoCoNumber());
		}
		return JSON;
	}

	/**
	 * Check ks.
	 * 
	 * @param lstPOVnmDetailLLotVoID
	 *            the lst po vnm detail l lot vo id
	 * @param lstPOVnmDetailLLotVoLot
	 *            the lst po vnm detail l lot vo lot
	 * @param lstPOVnmDetailLLotVoValue
	 *            the lst po vnm detail l lot vo value
	 * @param countKS
	 *            the count ks
	 * @param valueKS
	 *            the value ks
	 * @return the boolean
	 */
	public Boolean checkKS(List<Long> lstPOVnmDetailLLotVoID, List<String> lstPOVnmDetailLLotVoLot, List<Integer> lstPOVnmDetailLLotVoValue) {
		try {
			int checkCountKS = 0;
			BigDecimal checkValueKS = new BigDecimal(0);
			for (int i = 0; i < lstPOVnmDetailLLotVoID.size(); i++) {
				String lot = lstPOVnmDetailLLotVoLot.get(i);
				if (lot.equals("") || lot == null) {
					Long poVnmDetailID = lstPOVnmDetailLLotVoID.get(i);
					PoVnmDetail checkPoVnmDetail = poMgr.getPoVnmDetailById(poVnmDetailID);

					checkCountKS += lstPOVnmDetailLLotVoValue.get(i);
					BigDecimal value = new BigDecimal(lstPOVnmDetailLLotVoValue.get(i));
					BigDecimal sum = checkPoVnmDetail.getPriceValue().multiply(value);
					checkValueKS = checkValueKS.add(sum);
				} else {
					Long poVnmLotID = lstPOVnmDetailLLotVoID.get(i);
					PoVnmLot checkPoVnmLot = poMgr.getPoVnmLotById(poVnmLotID);

					checkCountKS += lstPOVnmDetailLLotVoValue.get(i);
					BigDecimal value = new BigDecimal(lstPOVnmDetailLLotVoValue.get(i));
					BigDecimal sum = checkPoVnmLot.getPriceValue().multiply(value);
					checkValueKS = checkValueKS.add(sum);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error ReturnProductAction.checkKS(Exception): " + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Check count.
	 * 
	 * @param lstPOVnmDetailLLotVoID
	 *            the lst po vnm detail l lot vo id
	 * @param lstPOVnmDetailLLotVoLot
	 *            the lst po vnm detail l lot vo lot
	 * @param lstPOVnmDetailLLotVoValue
	 *            the lst po vnm detail l lot vo value
	 * @return the string
	 */
	public String checkCount(List<Long> lstPOVnmDetailLLotVoID, List<String> lstPOVnmDetailLLotVoLot, List<Integer> lstPOVnmDetailLLotVoValue) {
		String check = "";
		try {
			for (int i = 0; i < lstPOVnmDetailLLotVoID.size(); i++) {
				String lot = lstPOVnmDetailLLotVoLot.get(i);
				if (StringUtil.isNullOrEmpty(lot)) {
					Long poVnmDetailID = lstPOVnmDetailLLotVoID.get(i);
					PoVnmDetail checkPoVnmDetail = poMgr.getPoVnmDetailById(poVnmDetailID);
					List<PoVnmDetail> lstPoVnmHasSameProduct = poMgr.getListPoVnmVoDetail(checkPoVnmDetail.getPoVnm().getId(), null, checkPoVnmDetail.getProduct().getId());
					int count = 0;
					for (PoVnmDetail pvd : lstPoVnmHasSameProduct) {
						count += (pvd.getQuantity() - pvd.getQuantityPay());
					}
					if (lstPOVnmDetailLLotVoValue.get(i) > count) {
						if (check.equals("")) {
							check += "MH:" + checkPoVnmDetail.getProduct().getProductCode();
						} else {
							check += ";" + checkPoVnmDetail.getProduct().getProductCode();
						}
					}
				} else {
					Long poVnmLotID = lstPOVnmDetailLLotVoID.get(i);
					PoVnmLot checkPoVnmLot = poMgr.getPoVnmLotById(poVnmLotID);
					int count = checkPoVnmLot.getQuantity() - checkPoVnmLot.getQuantityPay();
					if (lstPOVnmDetailLLotVoValue.get(i) > count) {
						if (check.equals("")) {
							check += "MH:" + checkPoVnmLot.getProduct().getProductCode() + "-Lô:" + checkPoVnmLot.getLot();
						} else {
							check += ";MH:" + checkPoVnmLot.getProduct().getProductCode() + "-Lô:" + checkPoVnmLot.getLot();
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error ReturnProductAction.checkCount(Exception): " + e.getMessage());
			return "Exception";
		}
		return check;
	}

	/**
	 * Check stock.
	 * 
	 * @param lstPOVnmDetailLLotVoID
	 *            the lst po vnm detail l lot vo id
	 * @param lstPOVnmDetailLLotVoLot
	 *            the lst po vnm detail l lot vo lot
	 * @param lstPOVnmDetailLLotVoValue
	 *            the lst po vnm detail l lot vo value
	 * @return the string
	 */
	public String checkStock(List<Long> lstPOVnmDetailLLotVoID, List<String> lstPOVnmDetailLLotVoLot, List<Integer> lstPOVnmDetailLLotVoValue) {
		String check = "";
		try {
			for (int i = 0; i < lstPOVnmDetailLLotVoID.size(); i++) {
				String lot = lstPOVnmDetailLLotVoLot.get(i);
				if (StringUtil.isNullOrEmpty(lot)) {
					Long poVnmDetailID = lstPOVnmDetailLLotVoID.get(i);
					PoVnmDetail checkPoVnmDetail = poMgr.getPoVnmDetailById(poVnmDetailID);
					PoVnm checkPoVnm = checkPoVnmDetail.getPoVnm();
					Long shopID = checkPoVnm.getShop().getId();
					Long productID = checkPoVnmDetail.getProduct().getId();
					boolean checkQuantityProductInStockTotal = poMgr.checkQuantityInStockTotal(shopID, productID, lstPOVnmDetailLLotVoValue.get(i));
					if (!checkQuantityProductInStockTotal) {
						check = ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_RETURNPRODUCT_CHECKSTOCK, checkPoVnmDetail.getProduct().getProductCode());
						break;
					}
				} else {
					Long poVnmLotID = lstPOVnmDetailLLotVoID.get(i);
					PoVnmLot checkPoVnmLot = poMgr.getPoVnmLotById(poVnmLotID);
					PoVnm checkPoVnm = checkPoVnmLot.getPoVnm();
					Long shopID = checkPoVnm.getShop().getId();
					Long productID = checkPoVnmLot.getProduct().getId();
					if (lstPOVnmDetailLLotVoValue.get(i) > 0 && !poMgr.checkQuantityInProductLot(shopID, productID, lstPOVnmDetailLLotVoValue.get(i), lot)) {
						check = ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_RETURNPRODUCT_CHECKSTOCKLOT, checkPoVnmLot.getProduct().getProductCode(), checkPoVnmLot.getLot());
						break;
					} else if (lstPOVnmDetailLLotVoValue.get(i) > 0 && !poMgr.checkQuantityInStockTotal(shopID, productID, lstPOVnmDetailLLotVoValue.get(i))) {
						check = ValidateUtil.getErrorMsg(ConstantManager.ERR_PO_RETURNPRODUCT_CHECKSTOCK, checkPoVnmLot.getProduct().getProductCode());
						break;
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error ReturnProductAction.checkstock(Exception): " + e.getMessage());
			return "Exception";
		}
		return check;
	}

	/**
	 * Gets the lst check lot.
	 * 
	 * @param lstPOVnmDetailLLotVoID
	 *            the lst po vnm detail l lot vo id
	 * @param lstPOVnmDetailLLotVoLot
	 *            the lst po vnm detail l lot vo lot
	 * @return the lst check lot
	 */
	public List<Boolean> getlstCheckLot(List<Long> lstPOVnmDetailLLotVoID, List<String> lstPOVnmDetailLLotVoLot) {
		List<Boolean> lstCheckLotTemp = new ArrayList<Boolean>();
		for (int i = 0; i < lstPOVnmDetailLLotVoID.size(); i++) {
			String lot = lstPOVnmDetailLLotVoLot.get(i);
			if (StringUtil.isNullOrEmpty(lot)) {
				lstCheckLotTemp.add(false);
			} else {
				lstCheckLotTemp.add(true);
			}
		}
		return lstCheckLotTemp;
	}

	/**
	 * Convertlst po vnm detail l lot vo value.
	 * 
	 * @param lstPOVnmDetailLLotVoID
	 *            the lst po vnm detail l lot vo id
	 * @param lstPOVnmDetailLLotVoLot
	 *            the lst po vnm detail l lot vo lot
	 * @param lstPOVnmDetailLLotVoValue
	 *            the lst po vnm detail l lot vo value
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<Integer> convertlstPOVnmDetailLLotVoValue(List<Long> lstPOVnmDetailLLotVoID, List<String> lstPOVnmDetailLLotVoLot, List<String> lstPOVnmDetailLLotVoValue) throws Exception {
		List<Integer> lstPOVnmDetailLLotVoValueConvert = new ArrayList<Integer>();
		for (int i = 0; i < lstPOVnmDetailLLotVoID.size(); ++i) {
			String lot = lstPOVnmDetailLLotVoLot.get(i);
			if (lot.equals("") || lot == null) {
				Long poVnmDetailID = lstPOVnmDetailLLotVoID.get(i);
				PoVnmDetail checkPoVnmDetail = poMgr.getPoVnmDetailById(poVnmDetailID);
				int convfact = checkPoVnmDetail.getProduct().getConvfact();
				boolean hasSplash = lstPOVnmDetailLLotVoValue.get(i).contains("/");
				if (hasSplash) {
					String[] value = lstPOVnmDetailLLotVoValue.get(i).split("/");
					Integer valuevbox = Integer.parseInt(value[0]);
					Integer valuesingle = 0;
					if (value.length == 2) {
						valuesingle = Integer.parseInt(value[1]);
					}
					lstPOVnmDetailLLotVoValueConvert.add(convfact * valuevbox + valuesingle);
				} else {
					Integer valuesingle = Integer.parseInt(lstPOVnmDetailLLotVoValue.get(i));
					lstPOVnmDetailLLotVoValueConvert.add(valuesingle);
				}
			} else {
				Long poVnmLotID = lstPOVnmDetailLLotVoID.get(i);
				PoVnmLot checkPoVnmLot = poMgr.getPoVnmLotById(poVnmLotID);
				int convfact = checkPoVnmLot.getProduct().getConvfact();
				boolean hasSplash = lstPOVnmDetailLLotVoValue.get(i).contains("/");
				if (hasSplash) {
					String[] value = lstPOVnmDetailLLotVoValue.get(i).split("/");
					Integer valuevbox = Integer.parseInt(value[0]);
					Integer valuesingle = 0;
					if (value.length == 2) {
						valuesingle = Integer.parseInt(value[1]);
					}
					lstPOVnmDetailLLotVoValueConvert.add(convfact * valuevbox + valuesingle);
				} else {
					Integer valuesingle = Integer.parseInt(lstPOVnmDetailLLotVoValue.get(i));
					lstPOVnmDetailLLotVoValueConvert.add(valuesingle);
				}
			}
		}
		return lstPOVnmDetailLLotVoValueConvert;
	}

	/**
	 * Export excel. Ham Export excel dung de in hoa don tra hang
	 * 
	 * @return the string
	 * @author thongnm
	 * @since Oct 3, 2012
	 */
	public String exportExcel() {
		try {
			//CHECK TON TAI DON VI USER DANG NHAP
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			PoVnm poVnmExport = null;
			if (!StringUtil.isNullOrEmpty(poVnmExportCode)) {
				poVnmExport = poMgr.getPoVnmByCode(poVnmExportCode);
			}
			if (poVnmExport == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "po.returnproduct.export.povnmexport.not.exit"));
				return ERROR;
			}
			staff = staffMgr.getStaffByCode(getCurrentUser().getUserName());
			shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			//ObjectVO<PoVnmDetailLotVO> lstPOVnmDetailLLotVoTmp = poMgr.getListPoVnmDetailLotVOByPoVnmIdEx(null, poVnmExport.getId(), shop.getId());
			ObjectVO<PoVnmDetailLotVO> lstPOVnmDetailLLotVoTmp = poMgr.getListPoVnmDetailLotVOByPoVnmId(null, poVnmExport.getId());
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
			String poVnmExportCreateDate = DateUtil.toDateString(poVnmExport.getCreateDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			BigDecimal disCount = poVnmExport.getDiscount();

			Map<String, Object> beans = new HashMap<String, Object>();
			if (lstPOVnmDetailLLotVoTmp != null && lstPOVnmDetailLLotVoTmp.getLstObject().size() > 0) {
				Shop shop = poVnmExport.getShop();
				beans.put("hasData", 1);
				beans.put("disCount", disCount);
				beans.put("poVnmExportCode", poVnmExportCode);
				beans.put("poVnmExportCreateDate", poVnmExportCreateDate);
				beans.put("fromPoVnm", poVnmExport.getFromPoVnmId().getPoCoNumber());
				beans.put("lstPoVnmDetailLotVO", lstPOVnmDetailLLotVoTmp.getLstObject());
				beans.put("shopName", shop.getShopName());
				beans.put("address", shop.getAddress());
				beans.put("staffName", staff.getStaffName());
			} else {
				beans.put("hasData", 0);
			}
			beans.put("printDate", printDate);

			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathPO(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.EXPORT_RETURNPRODUCT_TEMPLATE;
			templateFileName = templateFileName.replace('/', File.separatorChar);

			String outputName = ConstantManager.EXPORT_RETURNPRODUCT + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
		} catch (Exception e) {
			LogUtility.logError(e, "ReturnProductAction.exportExcel: " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}
}

package ths.dms.web.action.po;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.PoMgr;
//import ths.dms.core.entities.Reason;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.vo.PoVnmVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;


public class InvoiceAdjustmentAction extends AbstractAction {
	
	private static final long serialVersionUID = -2128120975032582873L;

	private boolean isEditShopCode;
	private String invoiceNumber;
	private String createDateFrom;
	private String createDateTo;
	private String poNumber;
	
	private List<PoVnm> listPo;
	private List<ApParam>listReason;
	private List<PoVnmVO> lstPoVnmVO;
	
	private PoMgr poMgr;
	
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		poMgr = (PoMgr)context.getBean("poMgr");
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		ShopToken sh = currentUser.getShopRoot();
		shopId = sh.getShopId();
		this.search();
		shopCode = sh.getShopCode() + "-" + sh.getShopName();
		return SUCCESS;
	}
	
	/***
	 * @author vuongmq
	 * @date 06/08/2015
	 * @description danh seach danh sach PO confirm cua PODVKH
	 * @throws Exception
	 */
	private void search() {
		try {
			listReason = apParamMgr.getListApParam(ApParamType.PO_REASON_INVOICE, ActiveType.RUNNING);
			ShopToken sh = currentUser.getShopRoot();
			if (ShopObjectType.NPP.getValue().equals(sh.getObjectType())
				|| ShopObjectType.NPP_KA.getValue().equals(sh.getObjectType()) 
				|| ShopObjectType.MIEN_ST.getValue().equals(sh.getObjectType())) {
				isEditShopCode = true;
			} else {
				isEditShopCode = false;
			}
			
			Date toDate = null;
			Date fromDate = null;
			if (StringUtil.isNullOrEmpty(createDateFrom) && StringUtil.isNullOrEmpty(createDateTo)) {
				/*Calendar calendar = new GregorianCalendar();
				toDate = calendar.getTime();
				calendar.setTimeInMillis(toDate.getTime() - 24L * 60 * 60 * 1000);
				calendar.set(Calendar.DATE, calendar.get(Calendar.DATE));
				calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
				calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
				calendar.set(Calendar.HOUR, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				fromDate = calendar.getTime();
				createDateFrom = DateUtil.toDateString(fromDate, DateUtil.DATE_FORMAT_STR);
				createDateTo = DateUtil.toDateString(toDate, DateUtil.DATE_FORMAT_STR);*/
				toDate = DateUtil.now();
				fromDate = DateUtil.getFirstDateInMonth(toDate);
				createDateFrom = DateUtil.toDateString(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				createDateTo = DateUtil.toDateString(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				toDate = DateUtil.toDate(createDateTo, DateUtil.DATE_FORMAT_DDMMYYYY);
				fromDate = DateUtil.toDate(createDateFrom, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			/*if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shT = shopMgr.getShopByCode(shopCode.trim());
				if (shT != null && checkShopPermission(shopCode)) {
					shopId = shT.getId();
				} else {
					shopId = 0L;
				}
			} else {
				shopId = sh.getShopId();
			}*/
			if (shopId == null) {
				shopId = sh.getShopId();
			}
			PoVnmFilter filter = new PoVnmFilter();
			filter.setShopId(shopId);
			filter.setPoAutoNumber(poNumber);
			filter.setInvoiceNumber(invoiceNumber);
			filter.setFromDate(fromDate);
			filter.setToDate(toDate);
			filter.setfDate(fromDate);
			filter.settDate(toDate);
			//filter.setPoStatus(PoVNMStatus.IMPORTED);
			List<Integer> lstPoStatus = new ArrayList<Integer>();
			lstPoStatus.add(PoVNMStatus.IMPORTING.getValue());
			lstPoStatus.add(PoVNMStatus.IMPORTED.getValue());
			filter.setLstPoVnmStatus(lstPoStatus);
			filter.setHasFindChildShop(true);
			// lay tu MgrImpl ra
			//filter.setPoType(PoType.PO_CONFIRM);
			List<Integer> lstPoType = new ArrayList<Integer>();
			lstPoType.add(PoType.PO_CONFIRM.getValue());
			lstPoType.add(PoType.RETURNED_SALES_ORDER.getValue());
			filter.setLstPoVnmType(lstPoType);
//			String order = "SHOP_CODE, INVOICE_NUMBER";
//			filter.setSortField(order);
			filter.setIsLikePoNumber(true);
			//listPo = poMgr.getListPoConfirmByCondition(filter);
			listPo = poMgr.getListPoConfirmByFilter(filter);
		} catch (Exception ex) {
			LogUtility.logError(ex, "Error InvoiceAdjustmentAction.search - " + ex.getMessage());
		}
	}
	
	/**
	 * Search po.
	 * @return the string
	 * @author phut
	 * @throws Exception 
	 * @since Aug 22, 2012
	 */
	public String searchPO() {
		try {
			this.search();
		} catch (Exception ex) {
			LogUtility.logError(ex, "Error InvoiceAdjustmentAction.searchPO - " + ex.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Adjustment po confirm.
	 * @return the string
	 * @author phut
	 * @throws BusinessException 
	 * @since Aug 22, 2012
	 */
	public String adjustmentPOConfirm() {
		resetToken(result);
		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_LOGIN));
				return ERROR;
			}
			if (lstPoVnmVO == null || lstPoVnmVO.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("po.vnm.no.checked.invoice"));
				return ERROR;
			}
			List<PoVnm>lstPoVnm = new ArrayList<PoVnm>();
			Date now = commonMgr.getSysDate();
			PoVnmVO tmp = null;
			for (int i = 0, sz = lstPoVnmVO.size(); i < sz; i++) {
				tmp = lstPoVnmVO.get(i);
				if (tmp != null) {
					if (tmp.getPoVnmId() == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
						return ERROR;
					}
					if (StringUtil.isNullOrEmpty(tmp.getInvoiceNumber())) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "po.vnm.new.invoice.label"));
						return ERROR;
					}
					PoVnm poVnm = poMgr.getPoVnmById(tmp.getPoVnmId());
					if (poVnm == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
						return ERROR;
					}
					poVnm.setInvoiceNumber(tmp.getInvoiceNumber().toUpperCase());
					poVnm.setReasonCode(tmp.getReason());
					poVnm.setModifyDate(now);
					lstPoVnm.add(poVnm);
				}
			}
			poMgr.updateListPoVnm(lstPoVnm);
			result.put(ERROR, false);
			return SUCCESS;
		} catch (Exception ex) {
			LogUtility.logError(ex, "Error InvoiceAdjustmentAction.adjustmentPOConfirm - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}
	
	/** getters + setters */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCreateDateFrom() {
		return createDateFrom;
	}

	public void setCreateDateFrom(String createDateFrom) {
		this.createDateFrom = createDateFrom;
	}

	public String getCreateDateTo() {
		return createDateTo;
	}

	public void setCreateDateTo(String createDateTo) {
		this.createDateTo = createDateTo;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public void setListPo(List<PoVnm> listPo) {
		this.listPo = listPo;
	}

	public List<PoVnm> getListPo() {
		return listPo;
	}

	public boolean getIsEditShopCode() {
		return isEditShopCode;
	}
	
	public List<ApParam> getListReason() {
		return listReason;
	}

	public void setListReason(List<ApParam> listReason) {
		this.listReason = listReason;
	}

	public List<PoVnmVO> getLstPoVnmVO() {
		return lstPoVnmVO;
	}

	public void setLstPoVnmVO(List<PoVnmVO> lstPoVnmVO) {
		this.lstPoVnmVO = lstPoVnmVO;
	}
}
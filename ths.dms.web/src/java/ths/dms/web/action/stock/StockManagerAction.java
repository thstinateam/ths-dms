package ths.dms.web.action.stock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.WarehouseType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.WarehouseVO;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class StockManagerAction extends AbstractAction {
	private static final long serialVersionUID = -2128120975032582873L;
	
	private StockManagerMgr stockManagerMgr;
	
	private String sort;
	
	private String order;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockManagerMgr = (StockManagerMgr)context.getBean("stockManagerMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			shopVO = new ShopVO();
			if (currentUser.getShopRoot() != null) {
				shopVO.setShopId(currentUser.getShopRoot().getShopId());
				shopVO.setShopCode(currentUser.getShopRoot().getShopCode());
				shopVO.setShopName(currentUser.getShopRoot().getShopName());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	/**
	 * Trang Quan Ly Kho
	 * 
	 * @author hunglm16
	 * @since JUNE 27,2014
	 * @return Info HTML
	 * */
	public String infoManager() {
		resetToken(result);
		try {
			lstApparam = new ArrayList<ApParam>();
			lstApparam = apParamMgr.getListApParam(ApParamType.WAREHOUSE_TYPE, ActiveType.RUNNING);
			shopVO = new ShopVO();
			if (currentUser.getShopRoot() != null) {
				shopVO.setShopId(currentUser.getShopRoot().getShopId());
				shopVO.setShopCode(currentUser.getShopRoot().getShopCode());
				shopVO.setShopName(currentUser.getShopRoot().getShopName());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Tim kiem kho voi nhieu tham bien
	 * 
	 * @author hunglm16
	 * @since JUNE 27,2014
	 * @description Search Warehouse by Filter
	 * */
	public String searchWarehouse() {
		actionStartTime = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
			KPaging<WarehouseVO> kPaging = new KPaging<WarehouseVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					filter.setLongG(shop.getId());
				}
			} else {
				filter.setLongG(currentUser.getShopRoot().getShopId());
			}
			// lay du lieu phan quyen duoi DB
			filter.setStaffRootId(currentUser.getUserId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			if (status != null && status > -1) {
				filter.setStatus(status);
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCodeAndName(code);
			}
			if (warehouseType != null && WarehouseType.parseValue(warehouseType) != null) {
				filter.setWarehouseType(warehouseType);
			}
			if (!StringUtil.isNullOrEmpty(description)) {
				filter.setCode(description);
			}
			if (!StringUtil.isNullOrEmpty(textG) && !textG.trim().toUpperCase().equals("0FULL")) {
				filter.setTextG(textG);//Loai kho
			}
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(sort)) {
				if ("createDateStr".equals(sort)) {
					sort = "  to_date(createDateStr,'DD/MM/YYYY')  ";
				} else if ("shopCodeM".equals(sort)) {
					sort = " shopCodeM ";
				} else if ("shopCodeV".equals(sort)) {
					sort = " shopCodeV ";
				} else if ("seq".equals(sort)) {
					sort = " seq ";
				} else if ("shopCode".equals(sort)) {
					sort = " shopCode ";
				} else if ("shopName".equals(sort)) {
					sort = " shopName ";
				} else if ("warehouseCode".equals(sort)) {
					sort = " warehouseCode ";
				} else if ("warehouseName".equals(sort)) {
					sort = " warehouseName ";
				} else if ("warehouseType".equals(sort)) {
					sort = "warehouseType";
				} else if ("status".equals(sort)) {
					sort = "status";
				}
			}
			if (!StringUtil.isNullOrEmpty(sort)) {
				filter.setSort(sort);
			}
			if (!StringUtil.isNullOrEmpty(order)) {
				filter.setOrder(order);
			}
			filter.setStrShopId(getStrListShopId());
			ObjectVO<WarehouseVO> data = stockManagerMgr.getListWarehouseVOByFilter(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<WarehouseVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ManageTransStockAction.search()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Them moi hoac cap nhat thong tin Kho
	 * 
	 *@author hunglm16
	 *@since JUNE 27,2014
	 *@description  Insert or update in Warehouse
	 * */
	public String addorUpdateWarehouse() {
		resetToken(result);
		errMsg = "";
		String msg = "";
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			boolean flagG = false;
			Warehouse warehouse = new Warehouse();
			if (longG == null) {
				//Khong xac dinh duoc don vi
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.shopId");
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (this.getMapShopChild().get(longG) != null) {
					flagG = true;
				}
				if (!flagG) {
					//Khong xac dinh duoc don vi
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.not.belong.area");
				} else {
					Shop shop = shopMgr.getShopById(longG);
					if (shop != null && !ShopSpecificType.NPP.equals(shop.getType().getSpecificType())) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.warehouse.shop.notshop");
					}
				}
				if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(code)) {
					//Ma kho khong duoc rong
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseCode");
				}
				if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(name)) {
					//Ten kho khong duoc rong
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseName");
				}
				if (StringUtil.isNullOrEmpty(msg) && (status == null || status < 0)) {
					//Tinh trang khong duoc rong
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.status");
				}
				if (StringUtil.isNullOrEmpty(msg) && (seq == null || seq < 1)) {
					//So thu tu phai lon hon hoac bang 1
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseTypeSeq");
				}
				//Kiem tra XSS
				if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(code)) {
					msg = ValidateUtil.validateField(code, "jsp.common.warehouse.code", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
				}
				if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(name)) {
					msg = ValidateUtil.validateField(name, "jsp.common.warehouse.name", 100, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
				}
				if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(description)) {
					msg = ValidateUtil.validateField(description, "catalog.product.note", 500, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				if (!isFlag) {
					///THEM MOI KHO
					BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
					filter.setCode(code);
					filter.setLongG(longG);
					//Kiem tra tinh toan ven
					if (stockManagerMgr.checkIfRecordWarehouseExist(filter)) {
						//Ma kho da duoc gan voi Don vi
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.checkIfRecordWarehouseExist");
						result.put(ERROR, true);
						result.put("errMsg", msg);
						return JSON;
					}
					BasicFilter<Warehouse> filterS = new BasicFilter<Warehouse>();
					filterS.setLongG(longG);
					filterS.setSeq(seq);
					filterS.setWarehouseType(warehouseType);
					// lay du lieu phan quyen duoi DB
					filterS.setStaffRootId(currentUser.getUserId());
					filterS.setRoleId(currentUser.getRoleToken().getRoleId());
					filter.setStrShopId(getStrListShopId());
					filterS.setShopRootId(currentUser.getShopRoot().getShopId());
					List<Warehouse> firtWarehouse = stockManagerMgr.getListWarehouseByFilter(filterS);
					//Kiem tra tinh toan ven
					if (firtWarehouse != null && !firtWarehouse.isEmpty()) {
						//So thu tu da ton tai trong kho
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.isseq");
						result.put(ERROR, true);
						result.put("errMsg", msg);
						return JSON;
					}
					warehouse = new Warehouse();
					warehouse.setWarehouseCode(code);
					warehouse.setWarehouseName(name);
					warehouse.setDescription(description);
					warehouse.setWarehouseType(WarehouseType.parseValue(warehouseType));
					warehouse.setSeq(seq);
					warehouse.setShop(shopMgr.getShopById(longG));
					warehouse.setStatus(ActiveType.parseValue(status));
					warehouse.setCreateDate(new Date());
					warehouse.setCreateUser(currentUser.getUserName());
					stockManagerMgr.createWarehouse(warehouse, getLogInfoVO());
				} else {
					///CAP NHAT KHO
					flagG = true;
					if (id == null) {
						//Khong xac dinh duoc kho
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseId");
						flagG = false;
					}
					if (flagG) {
						warehouse = stockManagerMgr.getWarehouseById(id);
						if (warehouse == null) {
							//Khong xac dinh duoc kho
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseId");
						}
						if (StringUtil.isNullOrEmpty(msg) && !seq.equals(warehouse.getSeq())) {
							BasicFilter<Warehouse> filterS = new BasicFilter<Warehouse>();
							filterS.setLongG(longG);
							filterS.setSeq(seq);
							filterS.setWarehouseType(warehouseType);
							// lay du lieu phan quyen duoi DB
							filterS.setStaffRootId(currentUser.getUserId());
							filterS.setRoleId(currentUser.getRoleToken().getRoleId());
							filterS.setShopRootId(currentUser.getShopRoot().getShopId());
							filterS.setStrShopId(getStrListShopId());
							List<Warehouse> firtWarehouse = stockManagerMgr.getListWarehouseByFilter(filterS);
							//Kiem tra tinh toan ven
							if (firtWarehouse != null && !firtWarehouse.isEmpty()) {
								//So thu tu da ton tai trong kho
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.isseq");
								result.put(ERROR, true);
								result.put("errMsg", msg);
								return JSON;
							}
						}
					}
					if (StringUtil.isNullOrEmpty(msg)) {
						if (!status.equals(warehouse.getStatus().getValue()) && ActiveType.RUNNING.getValue().equals(warehouse.getStatus().getValue())) {
							//Kiem tra xem co ton tai giao dich hay chua
							BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
							filter.setId(warehouse.getId());
							if (stockManagerMgr.checkIfRecordWarehouseInStockTotalExist(filter)) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.warehouse.isdefined.instockktotal.isUd");
							}
						}
					}
					if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(name)) {
						msg = ValidateUtil.validateField(name, "jsp.common.warehouse.name", 100, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
					}
					if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(description)) {
						msg = ValidateUtil.validateField(description, "catalog.product.note", 500, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						result.put(ERROR, true);
						result.put("errMsg", msg);
						return JSON;
					}
					warehouse.setWarehouseName(name);
					warehouse.setDescription(description);
					warehouse.setSeq(seq);
					warehouse.setStatus(ActiveType.parseValue(status));
					warehouse.setUpdateDate(new Date());
					warehouse.setUpdateUser(currentUser.getUserName());
					stockManagerMgr.updateWarehouse(warehouse);
				}
			}
		} catch (Exception e) {
			actionStartTime = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.StockManagerkAction.addorUpdateWarehouse()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Cap nhat trang thai kho theo su kien cap nhat
	 * 
	 * @author hunglm16
	 * @since August 17,2014
	 * @return JSON
	 * @description Ham chi mang tinh chat cap nhat 1 truong trang thai la duy nhat
	 * */
	public String updateStatusWarehouseByEnvent() {
		resetToken(result);
		errMsg = "";
		String msg = "";
		try {
			if (id == null) {
				//Khong xac dinh duoc kho
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseId");
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				warehouse = new Warehouse();
				warehouse = stockManagerMgr.getWarehouseById(id);
				if (warehouse == null) {
					//Khong xac dinh duoc kho
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseId");
				}
			}
			if (StringUtil.isNullOrEmpty(msg) && !ActiveType.isValidValue(isEvent)) {
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.isdefined.wareHouseIsEvent");
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (!isEvent.equals(warehouse.getStatus().getValue()) && ActiveType.RUNNING.getValue().equals(warehouse.getStatus().getValue())) {
					//Kiem tra xem co ton tai giao dich hay chua
					BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
					filter.setId(warehouse.getId());
					if (stockManagerMgr.checkIfRecordWarehouseInStockTotalExist(filter)) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.warehouse.isdefined.instockktotal.isUd");
					}
				} else if (isEvent.equals(ActiveType.STOPPED.getValue()) && ActiveType.STOPPED.getValue().equals(warehouse.getStatus().getValue())) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.warehouse.isdefined.instockktotal.isUd.stop");
				}
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			warehouse.setStatus(ActiveType.parseValue(isEvent));
			warehouse.setUpdateDate(new Date());
			warehouse.setUpdateUser(currentUser.getUserName());
			stockManagerMgr.updateWarehouse(warehouse);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Xoa kho
	 * 
	 * @author hunglm16
	 * @since August 17,2014
	 * @return JSON
	 * @description Ham chi mang tinh chat cap nhat 1 truong trang thai la duy nhat
	 * */
	public String deleteWarehouse() {
		resetToken(result);
		errMsg = "";
		String msg = "";
		try {
			boolean flagG = true;
			if (id == null) {
				//Khong xac dinh duoc kho
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseId");
				flagG = false;
			}
			if (flagG) {
				warehouse = new Warehouse();
				warehouse = stockManagerMgr.getWarehouseById(id);
				if (warehouse == null) {
					//Khong xac dinh duoc kho
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.wareHouseId");
					flagG = false;
				}
			}
			if (flagG) {
				//Kiem tra xem co ton tai giao dich hay chua
				BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
				filter.setId(id);
				if (stockManagerMgr.checkIfRecordWarehouseInStockTotalExist(filter)) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.warehouse.isdefined.instockktotal");
				}
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			warehouse.setStatus(ActiveType.DELETED);
			warehouse.setUpdateDate(new Date());
			warehouse.setUpdateUser(currentUser.getUserName());
			stockManagerMgr.updateWarehouse(warehouse);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Sao chep kho
	 * 
	 * @author hunglm16
	 * @since August 18,2014
	 * @return JSON
	 * */
	public String copyWarehouse() {
		resetToken(result);
		errMsg = "";
		String msg = "";
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			Shop shopBg = new Shop();
			if (longG == null) {
				//Khong xac dinh duoc don vi
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.shopId");
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				shopBg = shopMgr.getShopById(longG);
				if (!ShopSpecificType.NPP.equals(shopBg.getType().getSpecificType())) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.warehouse.begin.notshop");
				}
			}
			if (StringUtil.isNullOrEmpty(msg) && status == null) {
				//Tinh trang khong duoc rong
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.status");
			}
			if (StringUtil.isNullOrEmpty(msg) && (arrlongG == null || arrlongG.isEmpty())) {
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.notdefined.warehouse.end.notshop");
			}

			List<Long> lstShopEndId = new ArrayList<Long>();
			String lstShopErr = "";
			if (StringUtil.isNullOrEmpty(msg)) {
				for (Long shopId : arrlongG) {
					Boolean flag = false;
					if (this.getMapShopChild().get(shopId) != null) {
						lstShopEndId.add(shopId);
						flag = true;
					}
					if (!flag) {
						Shop sTmp = shopMgr.getShopById(shopId);
						if (sTmp != null) {
							lstShopErr += sTmp.getShopCode() + ", ";
						}
					}
				}
				if (!StringUtil.isNullOrEmpty(lstShopErr)) {
					msg += R.getResource("stock.manager.coppy.fail.shop.not.belong", lstShopErr);
				}
			}

			if (lstShopEndId.isEmpty() && !StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}

			BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
			filter.setLongG(longG);
			filter.setArrlongG(lstShopEndId);
			if (status != null && status > -1) {
				filter.setStatus(status);
			}
			filter.setUserName(currentUser.getUserName());
			// lay du lieu phan quyen duoi DB
			filter.setStaffRootId(currentUser.getUserId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			Integer kq = stockManagerMgr.copyWarehouseByFilter(filter);
			if (kq < 1) {
				msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.isShop.notwarehouse.iscopy", shopBg.getShopCode().trim());
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			} else if (kq > 1) {
				msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.manager.err.isShop.notShop.iscopy", shopBg.getShopCode().trim());
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			} else if (!StringUtil.isNullOrEmpty(msg)) {
				msg = R.getResource("stock.manager.coppy.success") + msg;
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
		} catch (Exception e) {
			actionStartTime = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.StockManagerAction.copyWarehouse()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	////////GETTER/SETTER
	private Long id;
	private String code;
	private String name;
	private String description;
	private String textG;
	private String fromDateStr;
	private String toDateStr;
	private String shopCode;
	private Integer status;
	private Long longG;
	private List<ApParam> lstApparam = new ArrayList<ApParam>();
	private ShopVO shopVO;
	private Integer intG;
	private Integer type;
	private Boolean isFlag;
	private Integer seq;
	private Integer isEvent;
	private Warehouse warehouse;
	private List<Long> arrlongG;
	private Integer warehouseType;
	
	public List<Long> getArrlongG() {
		return arrlongG;
	}

	public void setArrlongG(List<Long> arrlongG) {
		this.arrlongG = arrlongG;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public Integer getIsEvent() {
		return isEvent;
	}

	public void setIsEvent(Integer isEvent) {
		this.isEvent = isEvent;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Boolean getIsFlag() {
		return isFlag;
	}

	public void setIsFlag(Boolean isFlag) {
		this.isFlag = isFlag;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIntG() {
		return intG;
	}

	public void setIntG(Integer intG) {
		this.intG = intG;
	}

	public ShopVO getShopVO() {
		return shopVO;
	}

	public void setShopVO(ShopVO shopVO) {
		this.shopVO = shopVO;
	}

	public List<ApParam> getLstApparam() {
		return lstApparam;
	}

	public void setLstApparam(List<ApParam> lstApparam) {
		this.lstApparam = lstApparam;
	}

	public Long getLongG() {
		return longG;
	}

	public void setLongG(Long longG) {
		this.longG = longG;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTextG() {
		return textG;
	}

	public void setTextG(String textG) {
		this.textG = textG;
	}

	public String getFromDateStr() {
		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Integer getWarehouseType() {
		return warehouseType;
	}

	public void setWarehouseType(Integer warehouseType) {
		this.warehouseType = warehouseType;
	}
}

package ths.dms.web.action.stock;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.ProductCycleCountBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CycleCountMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.CycleCountResult;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.MapProductFilter;
import ths.dms.core.entities.vo.MapProductVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.WarehouseVO;
import ths.core.entities.vo.rpt.RptCycleCountDiff3VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.memcached.MemcachedUtils;
import ths.dms.core.report.ShopReportMgr;

// TODO: Auto-generated Javadoc
/**
 * The Class InputCountingStockAction.
 */
public class InputCountingStockAction extends AbstractAction {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	
	/** The stock mgr. */
	private StockMgr stockMgr;
	
	/** The staff mgr. */
	private StaffMgr staffMgr;
	
	/** The cycle count mgr. */
	private CycleCountMgr cycleCountMgr;
	
	/** The product mgr. */
	private ProductMgr productMgr;
	//private CycleCountReportMgr cycleCountReportMgr;	
	/** The shop mgr. */
	private ShopMgr shopMgr;
	
	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;
	
	/** The common mgr. */
	private CommonMgr commonMgr;
	
	/** The shop report mgr. */
	private ShopReportMgr shopReportMgr;
	
	/** The start date. */
	private String startDate;
	
	/** The end date. */
	private String endDate;
	
	/** The type. */
	private Integer type;
	
	/** The shop code. */
	private String shopCode;
	private String shopCodeImp;
	
	private String code;
	private String name;
	
	private String description;
	
	private Integer cycleCountStatus;
	
	private Integer total;
	
	private Long wareHouseId;
	
	/** The shop. */
	private Shop shop;
	
	/** The staff. */
	private Staff staff;
	
	/** The cycle count. */
	private CycleCount cycleCount;
	
	/** The status. */
	private int status;
	
	/** The max stock card number. */
	private int maxStockCardNumber;
	
	/** The lst cc detail. */
	private List<CycleCountMapProduct> lstCCDetail;
	
	/** The cycle count results. */
	private List<CycleCountResult> cycleCountResults;
	
	/** The lst cc detail. */
	private List<MapProductVO> lstMapProductVO;
	
	/** The cycle count code. */
	private String productObject;
	private String cycleCountCode;
	private String cycleCountCodeImp;
	
	/** The from number. */
	private Integer fromNumber;
	
	/** The to number. */
	private Integer toNumber;
	
	/** The cycle count result id. */
	private Long cycleCountResultId;
	
	/** The total stock card number. */
	private int totalStockCardNumber;
	
	/** The lst product bean. */
	List<ProductCycleCountBean> lstProductBean; 

	/** The total max number. */
	private Integer totalMaxNumber;
	
	/** The cycle count map product id. */
	private Long cycleCountMapProductId;
	
	/** The excel file. */
	private File excelFile;
	
	/** The excel file content type. */
	private String excelFileContentType;
	
	/** The lst excel product code. */
	List<String> lstExcelProductCode;
	
	/** The lst excel lot. */
	List<String> lstExcelLot;
	
	/** The lst excel quantity. */
	List<String> lstExcelQuantity;
	
	/** The lst cc map product. */
	private List<String> lstCCMapProduct;
	
	/** The lst cc result delete. */
	private List<String> lstCCResultDelete;
	
	/** The ischeck count date. */
	private Boolean ischeckCountDate;
	
	/** The ischeck count2 date. */
	private Boolean ischeckCount2Date;
	
	private HashMap<String, Object> parametersReport;
	
	/**
	 * Gets the ischeck count2 date.
	 *
	 * @return the ischeck count2 date
	 */
	public Boolean getIscheckCount2Date() {
		return ischeckCount2Date;
	}

	/**
	 * Sets the ischeck count2 date.
	 *
	 * @param ischeckCount2Date the new ischeck count2 date
	 */
	public void setIscheckCount2Date(Boolean ischeckCount2Date) {
		this.ischeckCount2Date = ischeckCount2Date;
	}

	/**
	 * Gets the stock mgr.
	 *
	 * @return the stock mgr
	 */
	public StockMgr getStockMgr() {
		return stockMgr;
	}

	/**
	 * Sets the stock mgr.
	 *
	 * @param stockMgr the new stock mgr
	 */
	public void setStockMgr(StockMgr stockMgr) {
		this.stockMgr = stockMgr;
	}

	/**
	 * Gets the staff mgr.
	 *
	 * @return the staff mgr
	 */
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	/**
	 * Sets the staff mgr.
	 *
	 * @param staffMgr the new staff mgr
	 */
	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	/**
	 * Gets the cycle count mgr.
	 *
	 * @return the cycle count mgr
	 */
	public CycleCountMgr getCycleCountMgr() {
		return cycleCountMgr;
	}

	/**
	 * Sets the cycle count mgr.
	 *
	 * @param cycleCountMgr the new cycle count mgr
	 */
	public void setCycleCountMgr(CycleCountMgr cycleCountMgr) {
		this.cycleCountMgr = cycleCountMgr;
	}

	/**
	 * Gets the product mgr.
	 *
	 * @return the product mgr
	 */
	public ProductMgr getProductMgr() {
		return productMgr;
	}

	/**
	 * Sets the product mgr.
	 *
	 * @param productMgr the new product mgr
	 */
	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	/*public CycleCountReportMgr getCycleCountReportMgr() {
		return cycleCountReportMgr;
	}

	public void setCycleCountReportMgr(CycleCountReportMgr cycleCountReportMgr) {
		this.cycleCountReportMgr = cycleCountReportMgr;
	}*/

	/**
	 * Gets the shop mgr.
	 *
	 * @return the shop mgr
	 */
	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	/**
	 * Sets the shop mgr.
	 *
	 * @param shopMgr the new shop mgr
	 */
	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public Long getWareHouseId() {
		return wareHouseId;
	}

	public void setWareHouseId(Long wareHouseId) {
		this.wareHouseId = wareHouseId;
	}

	public Integer getCycleCountStatus() {
		return cycleCountStatus;
	}

	public void setCycleCountStatus(Integer cycleCountStatus) {
		this.cycleCountStatus = cycleCountStatus;
	}

	/**
	 * Gets the product info mgr.
	 *
	 * @return the product info mgr
	 */
	public ProductInfoMgr getProductInfoMgr() {
		return productInfoMgr;
	}

	/**
	 * Sets the product info mgr.
	 *
	 * @param productInfoMgr the new product info mgr
	 */
	public void setProductInfoMgr(ProductInfoMgr productInfoMgr) {
		this.productInfoMgr = productInfoMgr;
	}

	/**
	 * Gets the shop report mgr.
	 *
	 * @return the shop report mgr
	 */
	public ShopReportMgr getShopReportMgr() {
		return shopReportMgr;
	}

	/**
	 * Sets the shop report mgr.
	 *
	 * @param shopReportMgr the new shop report mgr
	 */
	public void setShopReportMgr(ShopReportMgr shopReportMgr) {
		this.shopReportMgr = shopReportMgr;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * Gets the shop code.
	 *
	 * @return the shop code
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * Sets the shop code.
	 *
	 * @param shopCode the new shop code
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * Gets the shop.
	 *
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 *
	 * @param shop the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#getStaff()
	 */
	public Staff getStaff() {
		return staff;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#setStaff(ths.dms.core.entities.Staff)
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	/**
	 * Gets the cycle count.
	 *
	 * @return the cycle count
	 */
	public CycleCount getCycleCount() {
		return cycleCount;
	}

	/**
	 * Sets the cycle count.
	 *
	 * @param cycleCount the new cycle count
	 */
	public void setCycleCount(CycleCount cycleCount) {
		this.cycleCount = cycleCount;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Gets the max stock card number.
	 *
	 * @return the max stock card number
	 */
	public int getMaxStockCardNumber() {
		return maxStockCardNumber;
	}

	/**
	 * Sets the max stock card number.
	 *
	 * @param maxStockCardNumber the new max stock card number
	 */
	public void setMaxStockCardNumber(int maxStockCardNumber) {
		this.maxStockCardNumber = maxStockCardNumber;
	}

	/**
	 * Gets the cycle count results.
	 *
	 * @return the cycle count results
	 */
	public List<CycleCountResult> getCycleCountResults() {
		return cycleCountResults;
	}

	/**
	 * Sets the cycle count results.
	 *
	 * @param cycleCountResults the new cycle count results
	 */
	public void setCycleCountResults(List<CycleCountResult> cycleCountResults) {
		this.cycleCountResults = cycleCountResults;
	}

	/**
	 * Gets the lst cc detail.
	 *
	 * @return the lst cc detail
	 */
	public List<CycleCountMapProduct> getLstCCDetail() {
		return lstCCDetail;
	}

	/**
	 * Sets the lst cc detail.
	 *
	 * @param lstCCDetail the new lst cc detail
	 */
	public void setLstCCDetail(List<CycleCountMapProduct> lstCCDetail) {
		this.lstCCDetail = lstCCDetail;
	}

	/**
	 * Gets the cycle count code.
	 *
	 * @return the cycle count code
	 */
	public String getCycleCountCode() {
		return cycleCountCode;
	}

	/**
	 * Sets the cycle count code.
	 *
	 * @param cycleCountCode the new cycle count code
	 */
	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}

	/**
	 * Gets the from number.
	 *
	 * @return the from number
	 */
	public Integer getFromNumber() {
		return fromNumber;
	}

	/**
	 * Sets the from number.
	 *
	 * @param fromNumber the new from number
	 */
	public void setFromNumber(Integer fromNumber) {
		this.fromNumber = fromNumber;
	}

	/**
	 * Gets the to number.
	 *
	 * @return the to number
	 */
	public Integer getToNumber() {
		return toNumber;
	}

	/**
	 * Sets the to number.
	 *
	 * @param toNumber the new to number
	 */
	public void setToNumber(Integer toNumber) {
		this.toNumber = toNumber;
	}

	/**
	 * Gets the cycle count result id.
	 *
	 * @return the cycle count result id
	 */
	public Long getCycleCountResultId() {
		return cycleCountResultId;
	}

	/**
	 * Sets the cycle count result id.
	 *
	 * @param cycleCountResultId the new cycle count result id
	 */
	public void setCycleCountResultId(Long cycleCountResultId) {
		this.cycleCountResultId = cycleCountResultId;
	}

	/**
	 * Gets the total stock card number.
	 *
	 * @return the total stock card number
	 */
	public int getTotalStockCardNumber() {
		return totalStockCardNumber;
	}

	/**
	 * Sets the total stock card number.
	 *
	 * @param totalStockCardNumber the new total stock card number
	 */
	public void setTotalStockCardNumber(int totalStockCardNumber) {
		this.totalStockCardNumber = totalStockCardNumber;
	}

	/**
	 * Gets the lst product bean.
	 *
	 * @return the lst product bean
	 */
	public List<ProductCycleCountBean> getLstProductBean() {
		return lstProductBean;
	}

	/**
	 * Sets the lst product bean.
	 *
	 * @param lstProductBean the new lst product bean
	 */
	public void setLstProductBean(List<ProductCycleCountBean> lstProductBean) {
		this.lstProductBean = lstProductBean;
	}

	/**
	 * Gets the total max number.
	 *
	 * @return the total max number
	 */
	public Integer getTotalMaxNumber() {
		return totalMaxNumber;
	}

	/**
	 * Sets the total max number.
	 *
	 * @param totalMaxNumber the new total max number
	 */
	public void setTotalMaxNumber(Integer totalMaxNumber) {
		this.totalMaxNumber = totalMaxNumber;
	}

	/**
	 * Gets the cycle count map product id.
	 *
	 * @return the cycle count map product id
	 */
	public Long getCycleCountMapProductId() {
		return cycleCountMapProductId;
	}

	/**
	 * Sets the cycle count map product id.
	 *
	 * @param cycleCountMapProductId the new cycle count map product id
	 */
	public void setCycleCountMapProductId(Long cycleCountMapProductId) {
		this.cycleCountMapProductId = cycleCountMapProductId;
	}

	/**
	 * Gets the excel file.
	 *
	 * @return the excel file
	 */
	public File getExcelFile() {
		return excelFile;
	}

	/**
	 * Sets the excel file.
	 *
	 * @param excelFile the new excel file
	 */
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	/**
	 * Gets the excel file content type.
	 *
	 * @return the excel file content type
	 */
	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	/**
	 * Sets the excel file content type.
	 *
	 * @param excelFileContentType the new excel file content type
	 */
	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/**
	 * Gets the lst excel product code.
	 *
	 * @return the lst excel product code
	 */
	public List<String> getLstExcelProductCode() {
		return lstExcelProductCode;
	}

	/**
	 * Sets the lst excel product code.
	 *
	 * @param lstExcelProductCode the new lst excel product code
	 */
	public void setLstExcelProductCode(List<String> lstExcelProductCode) {
		this.lstExcelProductCode = lstExcelProductCode;
	}

	/**
	 * Gets the lst excel lot.
	 *
	 * @return the lst excel lot
	 */
	public List<String> getLstExcelLot() {
		return lstExcelLot;
	}

	/**
	 * Sets the lst excel lot.
	 *
	 * @param lstExcelLot the new lst excel lot
	 */
	public void setLstExcelLot(List<String> lstExcelLot) {
		this.lstExcelLot = lstExcelLot;
	}

	/**
	 * Gets the lst excel quantity.
	 *
	 * @return the lst excel quantity
	 */
	public List<String> getLstExcelQuantity() {
		return lstExcelQuantity;
	}

	/**
	 * Sets the lst excel quantity.
	 *
	 * @param lstExcelQuantity the new lst excel quantity
	 */
	public void setLstExcelQuantity(List<String> lstExcelQuantity) {
		this.lstExcelQuantity = lstExcelQuantity;
	}

	/**
	 * Gets the lst cc map product.
	 *
	 * @return the lst cc map product
	 */
	public List<String> getLstCCMapProduct() {
		return lstCCMapProduct;
	}

	/**
	 * Sets the lst cc map product.
	 *
	 * @param lstCCMapProduct the new lst cc map product
	 */
	public void setLstCCMapProduct(List<String> lstCCMapProduct) {
		this.lstCCMapProduct = lstCCMapProduct;
	}

	

	/**
	 * Gets the lst cc result delete.
	 *
	 * @return the lst cc result delete
	 */
	public List<String> getLstCCResultDelete() {
		return lstCCResultDelete;
	}

	/**
	 * Sets the lst cc result delete.
	 *
	 * @param lstCCResultDelete the new lst cc result delete
	 */
	public void setLstCCResultDelete(List<String> lstCCResultDelete) {
		this.lstCCResultDelete = lstCCResultDelete;
	}

	/**
	 * Gets the ischeck count date.
	 *
	 * @return the ischeck count date
	 */
	public Boolean getIscheckCountDate() {
		return ischeckCountDate;
	}

	/**
	 * Sets the ischeck count date.
	 *
	 * @param ischeckCountDate the new ischeck count date
	 */
	public void setIscheckCountDate(Boolean ischeckCountDate) {
		this.ischeckCountDate = ischeckCountDate;
	}

	
	
	public String getShopCodeImp() {
		return shopCodeImp;
	}

	public void setShopCodeImp(String shopCodeImp) {
		this.shopCodeImp = shopCodeImp;
	}

	public String getCycleCountCodeImp() {
		return cycleCountCodeImp;
	}

	public void setCycleCountCodeImp(String cycleCountCodeImp) {
		this.cycleCountCodeImp = cycleCountCodeImp;
	}

	/** 
	 *@author tientv 
	 */
	@Override
	public void prepare() throws Exception{
		try {
				super.prepare();
		} catch (Exception e) {
			LogUtility.logError(e,e.getMessage());
		}
		stockMgr = (StockMgr)context.getBean("stockMgr"); 
		productMgr = (ProductMgr)context.getBean("productMgr");
		staffMgr = (StaffMgr)context.getBean("staffMgr");
		shopMgr = (ShopMgr)context.getBean("shopMgr");
		productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
		cycleCountMgr = (CycleCountMgr)context.getBean("cycleCountMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		shopReportMgr = (ShopReportMgr) context.getBean("shopReportMgr");
		staff = getStaffByCurrentUser();
		/*if(staff!=null){
			shop = staff.getShop();
			shopCode = shop.getShopCode();
		}*/
//		if (currentUser != null && currentUser.getShopRoot() != null) {
//			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
//			shopCode = currentUser.getShopRoot().getShopCode();
//		}
	}
	
	/** 
	 *@author tientv 
	 */
	@Override
	public String execute() {
		resetToken(result);
		if (currentUser == null || currentUser.getShopRoot() == null) {
			return PAGE_NOT_PERMISSION;
		}
		return SUCCESS;
	}

	/**
	 * Gets the info.
	 * @author tientv
	 * @return the info
	 * 
	 * @modify nhutnn
	 * @since 18/11/2014
	 * 
	 * Thay doi cach lay du lieu
	 * @modify hunglm16
	 * @since 18/11/2014
	 * */
	public String getInfo() {
		try {
			lstMapProductVO = new ArrayList<>();
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == true && !StringUtil.isNullOrEmpty(cycleCountCode)) {
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					shop = shopMgr.getShopByCode(shopCode);
				} else {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));
					return JSON;
				}
				if (shop == null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));
					return JSON;
				}
				List<Long> lstShop = this.getListShopChildId();
				if (!lstShop.contains(shop.getId())) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
					return JSON;
				}
				cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shop.getId());
				if (cycleCount == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("cycle.count.not.exists"));
					return LIST;
				}
				status = cycleCount.getStatus().getValue();
				MapProductFilter<MapProductVO> filter = new MapProductFilter<MapProductVO>();
				filter.setCycleCountId(cycleCount.getId());
				ObjectVO<MapProductVO> data = cycleCountMgr.searchCycleCountMapProductByFilter(filter);
				setTotalMaxNumber(0);
				if (cycleCountMgr.getMaxStockCardNumber(cycleCount.getId()) != null) {
					setTotalMaxNumber(cycleCountMgr.getMaxStockCardNumber(cycleCount.getId()));
				}
				lstMapProductVO = data.getLstObject();
				if (lstMapProductVO != null && !lstMapProductVO.isEmpty()) {
					for (int i = 0, n = lstMapProductVO.size(); i < n; i++) {
						lstMapProductVO.get(i).setStockCardNumber(i + 1);
					}
				}
				//ObjectVO<CycleCountMapProduct> lstTmp = cycleCountMgr.getListCycleCountMapProduct(null, cycleCount.getId(), null, null, null, null, null, null, false);
//				setTotalMaxNumber(0);
//				if (cycleCountMgr.getMaxStockCardNumber(cycleCount.getId()) != null) {
//					setTotalMaxNumber(cycleCountMgr.getMaxStockCardNumber(cycleCount.getId()));
//				}
//				lstCCDetail = lstTmp.getLstObject();
//				if (lstCCDetail != null) {
//					for (int i = 0, n = lstCCDetail.size(); i < n; i++) {
//						lstCCDetail.get(i).setStockCardNumber(i + 1);
//					}
//				}
				setMaxStockCardNumber(cycleCountMgr.getMaxStockCardNumber(cycleCount.getId()));
				totalStockCardNumber = cycleCountMgr.getTotalStockCardNumber(cycleCount.getId());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.stock.InputCountingStockAction.getInfo", createLogErrorStandard(actionStartTime));
		}
		return LIST;
	}
	
	/**
	 * Lay the kiem kho va bo sung them san pham
	 * @author hunglm16
	 * @return HTML
	 * @since 12/11/2015
	 */
	public String getInfoJoinProductInsert() {
		try {
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop && !StringUtil.isNullOrEmpty(cycleCountCode)) {
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					shop = shopMgr.getShopByCode(shopCode);
				} else {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));
					return JSON;
				}
				if (shop == null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));
					return JSON;
				}
				List<Long> lstShop = this.getListShopChildId();
				if (!lstShop.contains(shop.getId())) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
					return JSON;
				}
				cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shop.getId());
				if (cycleCount == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("cycle.count.not.exists"));
					return JSON;
				}
				status = cycleCount.getStatus().getValue();
				MapProductFilter<MapProductVO> filter = new MapProductFilter<MapProductVO>();
				filter.setCycleCountId(cycleCount.getId());
				List<Long> lstProductId = new ArrayList<Long>(); 
				if (!StringUtil.isNullOrEmpty(productObject)) {
					for (String productId: productObject.split(",")) {
						if (ValidateUtil.validateNumber(productId) && lstProductId.indexOf(Long.valueOf(productId)) == -1) {
							lstProductId.add(Long.valueOf(productId));
						}
					}
				}
				ObjectVO<MapProductVO> data = new ObjectVO<>();
				if (lstProductId != null && !lstProductId.isEmpty()) {
					data = cycleCountMgr.getCycleCountMapProductByProductInsertTemp(filter, lstProductId);
				} else {
					data = cycleCountMgr.searchCycleCountMapProductByFilter(filter);
				}
				setTotalMaxNumber(0);
				if (cycleCountMgr.getMaxStockCardNumber(cycleCount.getId()) != null) {
					setTotalMaxNumber(cycleCountMgr.getMaxStockCardNumber(cycleCount.getId()));
				}
				lstMapProductVO = data.getLstObject();
				total = 0;
				if (lstMapProductVO != null && !lstMapProductVO.isEmpty()) {
					if (lstProductId == null || lstProductId.isEmpty()) {
						lstProductId = new ArrayList<Long>();
					}
					List<MapProductVO> lstMapProductVOTmp = new ArrayList<>();
					for (int i = 0, n = lstMapProductVO.size(); i < n; i++) {
						lstMapProductVO.get(i).setStockCardNumber(i + 1);
						if (lstMapProductVO.get(i).getProductId() != null && lstProductId.indexOf(lstMapProductVO.get(i).getProductId()) > - 1) {
							lstMapProductVOTmp.add(lstMapProductVO.get(i));
						}
					}
					total = lstMapProductVO.size();
					if (lstMapProductVO != null && !lstMapProductVO.isEmpty()) {
						lstMapProductVO = lstMapProductVOTmp;
					}
				}
				setMaxStockCardNumber(cycleCountMgr.getMaxStockCardNumber(cycleCount.getId()));
				totalStockCardNumber = cycleCountMgr.getTotalStockCardNumber(cycleCount.getId());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.stock.InputCountingStockAction.getInfo", createLogErrorStandard(actionStartTime));
		}
		return LIST;
	}
	
	/**
	 * Danh sach san pham bo sung nhap kiem ke
	 * @author hunglm16
	 * @since 10/11/2015
	 */
	public String searchProductForInsertInputCheckStock() {
		result.put("total", 0);
		result.put("rows", new ArrayList<ProductVO>());
		try {
			KPaging<ProductVO> kPaging = new KPaging<ProductVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (StringUtil.isNullOrEmpty(cycleCountCode) || StringUtil.isNullOrEmpty(shopCode)) {
				return JSON;
			}
			Shop shop = shopMgr.getShopByCode(shopCode.toUpperCase().trim());
			if (shop == null || getMapShopChild().get(shop.getId()) == null) {
				return JSON;
			}
			cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shop.getId());
			if (cycleCount == null) {
				return JSON;
			}
			List<Long> lstProductIdExcep = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(productObject)) {
				for (String productId:productObject.split(",")) {
					if (ValidateUtil.validateNumber(productId)) {
						lstProductIdExcep.add(Long.valueOf(productId));
					}
				}
			}
			ObjectVO<ProductVO> data = cycleCountMgr.searchProductNotInCycleProductMap(kPaging, ActiveType.RUNNING.getValue(), cycleCount.getId(), null, lstProductIdExcep, code, name);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.stock.InputCountingStockAction.searchProductForInsertInputCheckStock", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * Gets the list cycle count result.
	 *@author tientv
	 * @return the list cycle count result
	 */
	public String getListCycleCountResult(){
		try {			
			ObjectVO<CycleCountResult> objectVO = cycleCountMgr.getListCycleCountResult(null, cycleCountMapProductId);
			if(objectVO!=null){					 
			    setCycleCountResults(objectVO.getLstObject());
			}	
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Tim kiem ds kiem ke
	 * 
	 * @author lacnv1
	 * @since Nov 03, 2014
	 */
	public String searchCycleCount() throws Exception {
		try {
			result.put("page", page);
			result.put("max", max);

			KPaging<CycleCount> kPaging = new KPaging<CycleCount>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			CycleCountType type = null;
			if (cycleCountStatus != null && cycleCountStatus != -1) {
				type = CycleCountType.parseValue(cycleCountStatus);
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop sTmp = shopMgr.getShopByCode(shopCode);
				if (sTmp != null) {
					shopId = sTmp.getId();
				} else {
					shopId = 0L;
				}
			}
			Date startTemp = null;
			Date endTemp  = null;
			if (!StringUtil.isNullOrEmpty(startDate)) {
				startTemp = ths.dms.web.utils.DateUtil.parse(startDate, ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(endDate)) {
				endTemp = ths.dms.web.utils.DateUtil.parse(endDate, ConstantManager.FULL_DATE_FORMAT);
			}

			ObjectVO<CycleCount> lstCycleCount;
			/*if (typeInventory == 0) {
				lstCycleCount = cycleCountMgr.getListCycleCount(kPaging, cycleCountCode, type, description, shopId, startTemp, endTemp);
			} else if (typeInventory == 1) {
				if (type == null) {
					lstCycleCount = cycleCountMgr.getListCycleCount(kPaging, cycleCountCode, CycleCountType.ONGOING, description, shopId, startTemp, endTemp);
				} else {
					lstCycleCount = cycleCountMgr.getListCycleCount(kPaging, cycleCountCode, type, description, shopId, startTemp, endTemp);
				}
			} else {*/
				lstCycleCount = cycleCountMgr.getListCycleCountEx(kPaging, cycleCountCode, type, description, shopId, startTemp, endTemp, null, wareHouseId, getStrListShopId());
			//}
			if (lstCycleCount != null) {
				result.put("total", lstCycleCount.getkPaging().getTotalRows());
				result.put("rows", lstCycleCount.getLstObject());
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "ApproveCountingStockAction.searchCycleCount - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Import excel.
	 *
	 * @return the string
	 * @author thongnm
	 * @since Oct 6, 2012
	 */
	public String importExcel() {	
		resetToken(result);
		isError = true;
		totalItem = 0;
		String message = "";
		String checkMessage ="";
		List<CellBean> lstFails = new ArrayList<CellBean>();
	    lstView = new ArrayList<CellBean>();
	    typeView = true;
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 2);	
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				//CHECK TON TAI DON VI USER DANG NHAP
				boolean isExistShop = checkExistShopOfUserLogin();
				if(isExistShop == false) {
					errMsg =  ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP);
					return SUCCESS;
				}
				//CHECK TON TAI THE KIEM KE
				if(StringUtil.isNullOrEmpty(cycleCountCodeImp)){
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.approve.cyclecount.not.empty");
					return SUCCESS;
				}
				if(!StringUtil.isNullOrEmpty(shopCodeImp) ){
			    	shop = shopMgr.getShopByCode(shopCodeImp);
			    } else {			
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null"));
					return SUCCESS;
			    }
		    	if(shop==null){							
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null"));
					return SUCCESS;
				}
		    	List<Long> lstShop = this.getListShopChildId();
		    	if (!lstShop.contains(shop.getId())) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					return SUCCESS;
		    	}
				cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCodeImp, shop.getId());
				if(cycleCount == null){
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "stock.cyclecount.code");
					return SUCCESS;
				}
				//CHECK TRANG THAI CUA THE KIEM KE (DANG THUC HIEN)
				if (CycleCountType.COMPLETED.equals(cycleCount.getStatus()) || CycleCountType.CANCELLED.equals(cycleCount.getStatus())) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "stock.approve.cyclecount.complete.or.cancel");
					return SUCCESS;
				}
				List<Long> lstProductIdUpdate = new ArrayList<Long>();
				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						message = "";
						checkMessage ="";
						totalItem++;
						List<String> row = lstData.get(i);
						List<CycleCountMapProduct> lstCycleCountMapProduct = new ArrayList<CycleCountMapProduct>();
						List<CycleCountResult> lstCycleCountResult = new ArrayList<CycleCountResult>();
						
						Product product = null;
						CycleCountMapProduct ccMap = null;
						CycleCountResult ccResult = null;
						String productCode = null;
						String lot= "";
						Integer quantity =null;
						ProductLot productLot = null;
						StockTotal stockTotal = null;
						
						// Ma SP
						if(row.size() > 0){
							String value = row.get(0);
							productCode = value;
							checkMessage = ValidateUtil.validateField(value, "catalog.product.code", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
							if(StringUtil.isNullOrEmpty(checkMessage)){
								product = productMgr.getProductByCode(value);
								if(product == null){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST,true,"catalog.product.code");										
								} else {
									if(!product.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.product.code");
									} else{
										//Check MH  co nam trong danh sach mat hang cua the kiem kho hay khong 
										ccMap = cycleCountMgr.getCycleCountMapProduct(cycleCount.getId(), product.getId());
										if(ccMap == null){
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STOCK_INPUT_EXIST_LIST, true, "catalog.product.code");
										} 
									}
								}
							} else{
								message += checkMessage;
							}
						}
						// So lo
						/*if(row.size() > 1){
							String value = row.get(1);
							// So lo (XEM XET Check cung 1 MH  neu 1 dong khong co so lo -> dong khac co so lo thi thong bao)
							if (StringUtil.isNullOrEmpty(value)) {
								if(product != null) {
									if(product.getCheckLot()== 1){
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STOCK_INPUT_IMPORTEXCEL_LOT_REQUIRE,true ,"catalog.product.code") ; 
									} else{
										//Check countdate co ton tai doi voi mat hang duoc nhap vao
										if(ccMap != null) {
											if(ccMap.getCountDate()!= null) {
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STOCK_INPUT_EXIT_INVENTORY, true, "catalog.product.code");
											}
										}
									}
								}
							} else{
								if(product != null) {
									if(product.getCheckLot()== 0){
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STOCK_INPUT_IMPORTEXCEL_LOT_NOTREQUIRE, true, "catalog.product.code") ; 
									} else{
										checkMessage = ValidateUtil.validateField(value, "stock.countinginput.lot", 6,ConstantManager.ERR_INVALID_LENGTH,ConstantManager.ERR_FORMAT_LOT);
										if(StringUtil.isNullOrEmpty(checkMessage)){
											ccResult = cycleCountMgr.getCycleCountResult(cycleCount.getId(), product.getId(), value);
											//Check ton tai cycleCountResult
											if(ccResult != null){
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STOCK_INPUT_EXIT_INVENTORY, true, "catalog.product.code");
											} else{
												lot= value;
											}
										} else{
											message += checkMessage;
										}
									}
								}
							}
						}*/
						// So luong
						if(row.size() > 1){
							String value = row.get(1);
							checkMessage = ValidateUtil.validateField(value, "stock.countinginput.quantity",9, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FORMAT_NUMBER_CONVFACT);
							if (StringUtil.isNullOrEmpty(checkMessage)) {
								try {
									if(product != null){
										Integer testvalue = getQuantity(value, product.getConvfact(), "Quantity");
										if(testvalue <0 ){
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INTEGER_ZERO, true, "stock.countinginput.quantity");
										} else {
											if(Math.abs(testvalue)> 999999999){
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INTEGER_MAXVALUE, true, "stock.countinginput.quantity");
											} else{
												quantity = testvalue;
											}
										}
									}
								} catch (Exception e) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_FORMAT_NUMBER_CONVFACT, true, "stock.countinginput.quantity");
								}
							} else {
								message += checkMessage;
							}
						}
						// Kiểm tra  productlot và stocktotal ton tai hay khong
						if(product != null){
							if(product.getCheckLot()==1){
								productLot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lot,cycleCount.getShop().getId(),StockObjectType.SHOP, product.getId());
							} else{
								stockTotal = stockMgr.getStockTotalByProductCodeAndOwner(product.getProductCode(), StockObjectType.SHOP, cycleCount.getShop().getId());
								if(stockTotal == null){
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.stocktotalquantity.not.exist",productCode);
								} 
							}
						}
						if(isView == 0){
							if(StringUtil.isNullOrEmpty(message)){
								if(product.getCheckLot()==1){
									ccResult = new CycleCountResult();
									ccResult.setCycleCount(cycleCount);
									ccResult.setQuantityCounted(quantity);
									ccResult.setLot(lot);
									ccResult.setProduct(product);
									ccResult.setCycleCountMapProduct(ccMap);
									ccResult.setCountDate(commonMgr.getSysDate());
									if(productLot == null){
										ccResult.setQuantityBeforeCount(0);
									} else{
										ccResult.setQuantityBeforeCount(productLot.getQuantity());
									}
									lstCycleCountResult.add(ccResult);
									ccMap.setCountDate(commonMgr.getSysDate());
								} else{
									ccMap.setQuantityCounted(quantity);
									ccMap.setQuantityBeforeCount(stockTotal.getQuantity());
									ccMap.setCountDate(commonMgr.getSysDate());
								}
								lstCycleCountMapProduct.add(ccMap);
								cycleCountMgr.updateListCycleCountDetailAndCreateListCycleCountResult(null,lstCycleCountMapProduct, lstCycleCountResult, null, null, null, getLogInfoVO());
								if(product.getCheckLot()==1 && !lstProductIdUpdate.contains(product.getId())){
									lstProductIdUpdate.add(product.getId());
								}
							}else {
								lstFails.add(StringUtil.addFailBean(row,message));
							}
							typeView = false;
						} else{
							if(lstView.size()<100){
								if(StringUtil.isNullOrEmpty(message)){
						            message = "OK";
						        } else{
						        	message = StringUtil.convertHTMLBreakLine(message);
						        }
								lstView.add(StringUtil.addFailBean(row, message));
							}
							typeView = true;
						}
					}
				}
				if(isView ==0){
					List<CycleCountResult> lstCycleCountResultUpdate = new ArrayList<CycleCountResult>();
					for(int k =0 ; k < lstProductIdUpdate.size() ;++k){
						Product product = productMgr.getProductById(lstProductIdUpdate.get(k));
						CycleCountMapProduct ccMap = cycleCountMgr.getCycleCountMapProduct(cycleCount.getId(), product.getId());
						List<CycleCountResult> lstCCResult = cycleCountMgr.getListCycleCountResultEx(null, null, cycleCount.getShop().getId(), StockObjectType.SHOP, product.getId());
						for(CycleCountResult ccResultTmp : lstCCResult){
							CycleCountResult ccResult  = cycleCountMgr.getCycleCountResult(cycleCount.getId(), product.getId(), ccResultTmp.getLot());
							if(ccResult ==null){
								ccResult = new CycleCountResult();
								ccResult.setCycleCount(cycleCount);
								ccResult.setQuantityCounted(0);
								ccResult.setLot(ccResultTmp.getLot());
								ccResult.setProduct(product);
								ccResult.setCycleCountMapProduct(ccMap);
								ccResult.setCountDate(commonMgr.getSysDate());
								ccResult.setQuantityBeforeCount(ccResultTmp.getQuantityBeforeCount());
								lstCycleCountResultUpdate.add(ccResult);
							}
						}
					}
					cycleCountMgr.updateListCycleCountDetailAndCreateListCycleCountResult(null,null, lstCycleCountResultUpdate, null, null, null, getLogInfoVO());
				}
				//Export error
				getOutputFailExcelFile(lstFails,ConstantManager.TEMPLATE_STOCK_COUNTINGINPUT_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}else{
			isError = true;
			errMsg = "Tập tin Excel chưa nhập sản phẩm để kiểm kê";
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Import Data.
	 *
	 * @return the string
	 * @author thongnm,tientv11,cuongnd3
	 * @since Oct 6, 2012
	 */
	public String importData(){
		resetToken(result);
		errMsg ="";
		try {
			// xu luu du lieu
			if(!SUCCESS.equals(processSaveInput())) {
				return ERROR;
			}
			
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "Error InputCountingStockAction.importData: " + e.getMessage());
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}
	
	/**
	 * Check cycle counted.
	 *
	 * @return the string
	 * @author tientv
	 * @see Kiem tra san pham da chot truoc do chua
	 */
	public String checkCycleCounted(){
		Integer count = 0;
		try{
			for(int i = 0; i< lstCCMapProduct.size(); ++i){
				String[] lstCCMapString = lstCCMapProduct.get(i).split(";");
				String ccMapString = lstCCMapString[0].toString();
				ccMapString = ccMapString.replace("(", "");
				ccMapString = ccMapString.replace(")", "");
				String [] value = ccMapString.split(",");
				long ccMapId = Long.parseLong( value[0].toString());
				CycleCountMapProduct ccMap = cycleCountMgr.getCycleCountMapProductById(ccMapId);
				if(ccMap==null){
					continue;
				}
				if(ccMap.getCountDate()!=null){					
					++count;
				}
			}
			result.put("ccmap_counted", count);			
		}catch(Exception ex){
			LogUtility.logError(ex, "Error InputCountingStockAction.importData: " + ex.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Xu ly luu du lieu nhap vao tren grid
	 *
	 * @return the string
	 * @author trietptm
	 * 
	 * Bo sung san pham them moi
	 * @modify hunglm16
	 * @since 13/11/2015
	 */
	private String processSaveInput() throws BusinessException {
		//CHECK TON TAI DON VI USER DANG NHAP
		boolean isExistShop = checkExistShopOfUserLogin();
		if (isExistShop == false) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
			return ERROR;
		}
		if (StringUtil.isNullOrEmpty(cycleCountCode)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.exceldialog.checknullsize"));
			return ERROR;
		}
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			shop = shopMgr.getShopByCode(shopCode);
		} else {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));
			return ERROR;
		}
		if (shop == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));
			return ERROR;
		}
		List<Long> lstShop = this.getListShopChildId();
		if (!lstShop.contains(shop.getId())) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
			return ERROR;
		}
		cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shop.getId());
		if (cycleCount == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.cycle.count.code")));
			return ERROR;
		}
		if (cycleCount.getWarehouse() == null) {
			result.put(ERROR, true);
			result.put("errMsg", R.getResource("stock.countinginput.warehouse.not.exists"));
			return ERROR;
		}
		//CHECK TRANG THAI CUA THE KIEM KE
		if (CycleCountType.COMPLETED.equals(cycleCount.getStatus()) || CycleCountType.CANCELLED.equals(cycleCount.getStatus())) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.approve.cyclecount.complete.or.cancel")));
			return ERROR;
		}

		List<CycleCountMapProduct> lstCycleCountMapProduct = new ArrayList<CycleCountMapProduct>();
		List<CycleCountResult> lstCycleCountResult = new ArrayList<CycleCountResult>();
		List<CycleCountResult> lstDeletedCycleCountResult = new ArrayList<CycleCountResult>();

		Map<Long, Integer> mapProductInsert = null;
		List<Long> lstProductIdInsert = new ArrayList<Long>();
		//Xy ly kiem tra voi San pham them moi
		if (!StringUtil.isNullOrEmpty(productObject)) {
			Map<Long, Integer> mapProductInsertTmp = new HashMap<Long, Integer>();
			for (String row: productObject.split(",")) {
				String [] arrCell = row.split(";");
				if (arrCell.length < 2 || StringUtil.isNullOrEmpty(arrCell[0]) || StringUtil.isNullOrEmpty(arrCell[1]) || !ValidateUtil.validateNumber(arrCell[0]) || !ValidateUtil.validateNumber(arrCell[1])) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("stock.countinginput.exist.ccmapproduct.product.insert"));
					return ERROR;
				}
				mapProductInsertTmp.put(Long.valueOf(arrCell[0]), Integer.valueOf(arrCell[1]));
				lstProductIdInsert.add(Long.valueOf(arrCell[0]));
			}
			ObjectVO<ProductVO> data = cycleCountMgr.searchProductNotInCycleProductMap(null, ActiveType.RUNNING.getValue(), cycleCount.getId(), lstProductIdInsert, null, null, null);
			if (data != null && data.getLstObject() != null && !data.getLstObject().isEmpty()) {
				mapProductInsert = new HashMap<Long, Integer>();
				for (ProductVO rowProductVO : data.getLstObject()) {
					Long productIdTmp = rowProductVO.getProductId();
					mapProductInsert.put(productIdTmp, mapProductInsertTmp.get(productIdTmp));
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("stock.countinginput.exist.ccmapproduct.product.insert"));
				return ERROR;
			}
		}
		
		if (lstCCMapProduct != null) {
			for (int i = 0; i < lstCCMapProduct.size(); ++i) {
				String[] lstCCMapString = lstCCMapProduct.get(i).split(";");
				String ccMapString = lstCCMapString[0].toString();
				ccMapString = ccMapString.replace("(", "");
				ccMapString = ccMapString.replace(")", "");
				String[] value = ccMapString.split(",");
				if (!ValidateUtil.validateNumber(value[0].toString())) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.exist.ccmapproduct"));
					return ERROR;
				}
				long ccMapId = Long.parseLong(value[0].toString());
				if (ccMapId == 0l) {
					//Them moi the kiem kho
					continue;
				}
				CycleCountMapProduct ccMap = cycleCountMgr.getCycleCountMapProductById(ccMapId);
				if (ccMap == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("stock.countinginput.exist.ccmapproduct"));
					return ERROR;
				}
				if (ccMap.getCycleCount() != null && !cycleCount.getId().equals(ccMap.getCycleCount().getId())) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("stock.countinginput.ccmapproduct.not.exist.stock.counting"));
					return ERROR;
				}
				Product product = ccMap.getProduct();
//				String productCode = product.getProductCode();
//				StockTotal stocktotal = stockMgr.getStockTotalByProductAndOwner(product.getId(), StockObjectType.SHOP, shop.getId(), cycleCount.getWarehouse().getId());
//				if (stocktotal == null) {
//					result.put("errMsg", R.getResource("stock.countinginput.stocktotalquantity.not.exist", productCode));
//					result.put(ERROR, true);
//					return ERROR;
//				}
				if (product.getCheckLot() == 1) {
					if (lstCCMapString.length > 1) {
						for (int j = 1; j < lstCCMapString.length; ++j) {
							String ccResultString = lstCCMapString[j].toString();
							ccResultString = ccResultString.replace("(", "");
							ccResultString = ccResultString.replace(")", "");
							String[] valueResult = ccResultString.split(",");
							String lot = valueResult[0].toString();
							Integer quantityresult = Integer.parseInt(valueResult[1].toString());
							CycleCountResult ccResult = cycleCountMgr.getCycleCountResult(cycleCount.getId(), product.getId(), lot);
							if (ccResult == null) {
								ccResult = new CycleCountResult();
								ccResult.setCycleCount(cycleCount);
								ccResult.setCycleCountMapProduct(ccMap);
								ccResult.setLot(lot);
								ccResult.setProduct(product);
								ccResult.setCountDate(commonMgr.getSysDate());
							}
							ProductLot productlot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lot, shop.getId(), StockObjectType.SHOP, product.getId());
							if (productlot == null) {
								productlot = new ProductLot();
								productlot.setQuantity(0);
								ccResult.setQuantityBeforeCount(productlot.getQuantity());
								ccResult.setCountDate(commonMgr.getSysDate());
							}
							ccResult.setQuantityCounted(quantityresult);
							if (ischeckCountDate == null || ischeckCountDate) {
								ccResult.setQuantityBeforeCount(productlot.getQuantity());
								ccResult.setCountDate(commonMgr.getSysDate());
							}
							lstCycleCountResult.add(ccResult);
						}
					} else {
						//TRUONG HOP CHI CHECK NHUNG KHONG MO DETAIL						
						ObjectVO<ProductLot> objectVoLot = stockMgr.getListProductLotWithProductID(null, shop.getId(), StockObjectType.SHOP, 0, ActiveType.RUNNING, product.getId());
						if (objectVoLot != null && objectVoLot.getLstObject().size() > 0) {
							for (ProductLot productLot : objectVoLot.getLstObject()) {
								CycleCountResult ccResult = cycleCountMgr.getCycleCountResult(cycleCount.getId(), product.getId(), productLot.getLot());
								if (ccResult == null) {
									ccResult = new CycleCountResult();
									ccResult.setCycleCount(cycleCount);
									ccResult.setCycleCountMapProduct(ccMap);
									ccResult.setLot(productLot.getLot());
									ccResult.setProduct(product);
									if (productLot.getQuantity() <= 0) {
										continue;
									}
									ccResult.setQuantityCounted(0);
									ccResult.setCountDate(commonMgr.getSysDate());
								}
								if (ischeckCountDate == null || ischeckCountDate) {
									ccResult.setQuantityBeforeCount(productLot.getQuantity());
									ccResult.setCountDate(commonMgr.getSysDate());
								}
								lstCycleCountResult.add(ccResult);
							}
						}
					}
				} else {
					int quantitymapproduct = Integer.parseInt(value[1].toString());
					ccMap.setQuantityCounted(quantitymapproduct);
					if (ischeckCountDate == null || ischeckCountDate) {
						StockTotal stocktotal = stockMgr.getStockTotalByProductAndOwner(product.getId(), StockObjectType.SHOP, shop.getId(), cycleCount.getWarehouse().getId());
						if (stocktotal != null) {
							ccMap.setQuantityBeforeCount(stocktotal.getQuantity());
						}
					}
				}
				if (ischeckCountDate == null || ischeckCountDate) {
					ccMap.setCountDate(commonMgr.getSysDate());
				}
				lstCycleCountMapProduct.add(ccMap);
			}
		}
		// UPDATE
		cycleCountMgr.updateListCycleCountDetailAndCreateListCycleCountResult(null, lstCycleCountMapProduct, lstCycleCountResult, null, lstDeletedCycleCountResult, mapProductInsert, getLogInfoVO());
		//Xu ly cho danh sach san pham them moi
		if (mapProductInsert != null && cycleCount != null) {
			Date sysCurentDate = commonMgr.getSysDate();
			for (Entry<Long, Integer> entry : mapProductInsert.entrySet()) {
				CycleCountMapProduct obj = new CycleCountMapProduct();
				obj.setCycleCount(cycleCount);
				obj.setProduct(commonMgr.getEntityById(Product.class, entry.getKey()));
				obj.setQuantityCounted(entry.getValue());
				obj.setQuantityBeforeCount(0);
				obj.setCountDate(sysCurentDate);
				obj.setCreateDate(sysCurentDate);
				obj.setCreateUser(currentUser.getUserName());
				commonMgr.createEntity(obj);
			}
		}
		if (lstCCResultDelete != null) {
			for (int i = 0, sz = lstCCResultDelete.size(); i < sz; ++i) {
				String cycleCountMapProductId = lstCCResultDelete.get(i).split(";")[0];
				String lot = lstCCResultDelete.get(i).split(";")[1];
				CycleCountMapProduct countMapProduct = cycleCountMgr.getCycleCountMapProductById(Long.valueOf(cycleCountMapProductId));
				if (countMapProduct == null) {
					continue;
				}
				CycleCountResult ccResult = cycleCountMgr.getCycleCountResult(cycleCount.getId(), countMapProduct.getProduct().getId(), lot);
				if (ccResult == null) {
					continue;
				}
				if (ccResult != null && ccResult.getCycleCount() != null && !cycleCount.getId().equals(ccResult.getCycleCount().getId())) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.approve.cyclecount.ccresultdelete"));
					return ERROR;
				}
				cycleCountMgr.deleteCycleCountResult(ccResult);
			}
		}
		return SUCCESS;
	}
	
	/**
	 * hoan thanh kiem kho
	 *
	 * @return the string
	 * @author trietptm
	 * @see Kiem tra san pham co lech kho de chuyen trang thai
	 */
	public String completeCheckStock(){
		resetToken(result);
		result.put(ERROR, true);
		try{
			// xu luu du lieu nhap tren grid
			if(!SUCCESS.equals(processSaveInput())) {
				return ERROR;
			}
			cycleCountMgr.completeCheckStock(cycleCount, getLogInfoVO());	
		}catch(Exception ex){
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(ex, "Error InputCountingStockAction.checkCycleCounted: " + ex.getMessage());
			return ERROR;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Export excel.
	 *
	 * @return the string
	 * @author thongnm
	 * @since Oct 6, 2012
	 */
	public String exportExcel(){
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			parametersReport = new HashMap<String, Object>();
			formatType = FileExtension.PDF.getName();
			//formatType = FileExtension.XLS.getName();			
			//CHECK TON TAI DON VI USER DANG NHAP
			boolean isExistShop = checkExistShopOfUserLogin();
			if(isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			//CHECK TON TAI THE KIEM KE
			if(StringUtil.isNullOrEmpty(cycleCountCode)){
				result.put(ERROR, true);
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.approve.cyclecount.not.empty"));
				return ERROR;
			}
			
			if(!StringUtil.isNullOrEmpty(shopCode) ){
		    	shop = shopMgr.getShopByCode(shopCode);
		    } else {			
		    	result.put(ERROR, true);
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return ERROR;
		    }
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return ERROR;
			}
	    	List<Long> lstShop = this.getListShopChildId();
	    	if (!lstShop.contains(shop.getId())) {
	    		result.put(ERROR, true);
	    		result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));	
				return ERROR;
	    	}
	    	parametersReport.put("shopName", shop.getShopName());
			if(!StringUtil.isNullOrEmpty(shop.getAddress())){
				parametersReport.put("address", shop.getAddress());
			} else {
				parametersReport.put("address", "");
			}
	    	
			cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shop.getId());
			if(cycleCount == null){
				result.put(ERROR, true);
				result.put("errMsg",ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "stock.cyclecount.code"));
				return ERROR;
			}
			parametersReport.put("cycleCountCode", cycleCountCode);

			if(CycleCountType.COMPLETED.equals(cycleCount.getStatus().getValue()) || CycleCountType.CANCELLED.equals(cycleCount.getStatus().getValue())) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION,false, "stock.approve.cyclecount.complete.or.cancel"));
				return ERROR;
			}
			// get data for parameters

			//parametersReport.put("fDate", DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_STR));

			// get data for datasource
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
			parametersReport.put("fDate",printDate);				
			String startDate = DateUtil.toDateString(cycleCount.getStartDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			parametersReport.put("sDateCount",startDate);
			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			
			Integer sumAllCounted = 0;
			Integer sumAllBeforeCount = 0;
			Integer sumAllQuantityCounted = 0;
			Integer sumAllQuantityCountedPackage = 0;
			Integer sumAllQuantityBeforeCount = 0;
			Integer sumAllQuantityBeforeCountPackage = 0;
			Integer sumAllQuantityDiff = 0;
			Integer sumAllQuantityDiffPackage = 0;
			Long sumAllAmount = 0L;
			Integer convfact = 0;
			List<RptCycleCountDiff3VO> rptDiff3VO = shopReportMgr.getListRptCycleCountDiff3VO(cycleCount.getId());
			if (rptDiff3VO != null) {
				for (int i = 0, n = rptDiff3VO.size(); i < n; i++) {
					RptCycleCountDiff3VO rptDiff3 = rptDiff3VO.get(i);
					convfact = rptDiff3.getConvfact();
					
					int quantityCounted = (rptDiff3.getQuantityCounted() == null ? 0 : rptDiff3.getQuantityCounted());
					sumAllQuantityCounted += quantityCounted % convfact;
					sumAllQuantityCountedPackage += quantityCounted / convfact;
					rptDiff3.setQuantityCountedConvfact(convertConvfact(quantityCounted, convfact));
					
					sumAllQuantityBeforeCount += rptDiff3.getQuantityBeforeCount() % convfact;
					sumAllQuantityBeforeCountPackage += rptDiff3.getQuantityBeforeCount() / convfact;				
					rptDiff3.setQuantityBeforeCountConvfact(convertConvfact(rptDiff3.getQuantityBeforeCount(), convfact));
					
					sumAllQuantityDiff += rptDiff3.getQuantityDiff() % convfact;
					sumAllQuantityDiffPackage += rptDiff3.getQuantityDiff() / convfact;
					rptDiff3.setQuantityDiffConvfact(convertConvfact(rptDiff3.getQuantityDiff(), convfact));
					
					sumAllCounted = sumAllCounted + quantityCounted;
					sumAllBeforeCount = sumAllBeforeCount + rptDiff3.getQuantityBeforeCount();
//					sumAllQuantityDiff = sumAllQuantityDiff + rptDiff3.getQuantityDiff();
					sumAllAmount = sumAllAmount + rptDiff3.getAmountDiff().longValue();
				}			
				
			} else {
				rptDiff3VO = new ArrayList<RptCycleCountDiff3VO>();
			}
			rptDiff3VO.add(0,new RptCycleCountDiff3VO());
			
			parametersReport.put("sumAllQuantityCounted", sumAllQuantityCountedPackage + "/" + sumAllQuantityCounted);
			parametersReport.put("sumAllQuantityBeforeCount", sumAllQuantityBeforeCountPackage + "/" + sumAllQuantityBeforeCount);
			parametersReport.put("sumAllQuantityDiff", sumAllQuantityDiffPackage + "/" + sumAllQuantityDiff);
			parametersReport.put("sumaAllAmount", sumAllAmount);
			
			Double p = 0.0;
			if(sumAllBeforeCount != 0 && sumAllCounted != 0){
				p = Double.parseDouble(sumAllCounted.toString()) * 100;
				p = p /sumAllBeforeCount;
			}
//			DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
//			decimalFormatSymbols.setDecimalSeparator('.');
//			decimalFormatSymbols.setGroupingSeparator(',');
//			DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
//			p = Double.valueOf(decimalFormat.format(p)); 
			String pTmp = convertMoney(BigDecimal.valueOf(p).setScale(2, BigDecimal.ROUND_HALF_UP));
			parametersReport.put("percent", pTmp);
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, rptDiff3VO);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, parametersReport);
			FileExtension ext = FileExtension.parseValue(formatType);
			JRDataSource dataSource = new JRBeanCollectionDataSource(rptDiff3VO);
			String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource,ShopReportTemplate.NKKHO);	
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			return JSON;

		}catch (Exception e) {
			LogUtility.logError(e, "InputCountingStockAction.exportExcel(): " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}

	/**
	 * Khai bao GETTER/SETTER
	 **/
	
	public List<MapProductVO> getLstMapProductVO() {
		return lstMapProductVO;
	}

	public void setLstMapProductVO(List<MapProductVO> lstMapProductVO) {
		this.lstMapProductVO = lstMapProductVO;
	}

	public String getProductObject() {
		return productObject;
	}

	public void setProductObject(String productObject) {
		this.productObject = productObject;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

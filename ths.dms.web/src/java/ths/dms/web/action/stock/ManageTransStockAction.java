package ths.dms.web.action.stock;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import ths.dms.core.business.ReportManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.entities.vo.StockTransVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

public class ManageTransStockAction extends AbstractAction {
	private static final long serialVersionUID = -2128120975032582873L;
	public static final String XUAT_KHO_NHAN_VIEN = "XUAT_KHO_NHAN_VIEN";
	public static final String NHAP_KHO_NHAN_VIEN = "NHAP_KHO_NHAN_VIEN";
	public static final String XUAT_KHO_DIEU_CHINH = "XUAT_KHO_DIEU_CHINH";
	public static final String NHAP_KHO_DIEU_CHINH = "NHAP_KHO_DIEU_CHINH";
	//me
	private HashMap<String, Object> parametersReport;
	private String stockTransDate;
	private String vansaleCode;
	private String code;
	private String transType;
	private List<StockTransLotVO> listData;
	//me
	private StockMgr stockMgr;
	private ShopMgr shopMgr;
	private StaffMgr staffMgr;
	private Integer role;

	private String startDate;
	private String endDate;
	private Integer type;
	private Integer status;
	private String shopCode;
	private String stockTransCode;
	private Long stockTransId;
	private BigDecimal totalWeight;
	private BigDecimal totalAmount;
	
	private Long curShopId;
	private Long shopId;
	private String lstShopId;
	
	private Shop shop;
	private Staff staff;
	private List<StockTransLotVO> lstProduct;  
	
	/** in pxkkvcnb*/
	private ReportManagerMgr reportManagerMgr;
	private String transacContent;
	private String transacName;
	private String issuedDate;
	private String staffCode;
	
	
	public String getTransacContent() {
		return transacContent;
	}

	public void setTransacContent(String transacContent) {
		this.transacContent = transacContent;
	}

	public String getTransacName() {
		return transacName;
	}

	public void setTransacName(String transacName) {
		this.transacName = transacName;
	}
	public String getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(String issuedDate) {
		this.issuedDate = issuedDate;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public List<StockTransLotVO> getListData() {
		return listData;
	}

	public void setListData(List<StockTransLotVO> listData) {
		this.listData = listData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getVansaleCode() {
		return vansaleCode;
	}

	public void setVansaleCode(String vansaleCode) {
		this.vansaleCode = vansaleCode;
	}

	public String getStockTransDate() {
		return stockTransDate;
	}

	public void setStockTransDate(String stockTransDate) {
		this.stockTransDate = stockTransDate;
	}

	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}

	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	public List<StockTransLotVO> getLstProduct() {
		return lstProduct;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setLstProduct(List<StockTransLotVO> lstProduct) {
		this.lstProduct = lstProduct;
	}
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
	public String getStockTransCode() {
		return stockTransCode;
	}

	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}
	
	public StockMgr getStockMgr() {
		return stockMgr;
	}

	public void setStockMgr(StockMgr stockMgr) {
		this.stockMgr = stockMgr;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}
	
	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}
	
	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
	public boolean checkExistShopOfUserLogin() {
		actionStartTime = DateUtil.now();
		if (currentUser != null && currentUser.getUserName() != null) {
			try {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
			} catch (BusinessException e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ManageTransStockAction.checkExistShopOfUserLogin()"), createLogErrorStandard(actionStartTime));
				return false;
			}
			if (staff != null && staff.getShop() != null) {
				shop = staff.getShop();
			}
		}
		if (shop == null) {
			return false;
		}
		return true;
	}
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockMgr = (StockMgr) context.getBean("stockMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		reportManagerMgr = (ReportManagerMgr)context.getBean("reportManagerMgr");
		
	}

	@Override
	public String execute() {
		actionStartTime = DateUtil.now();
		generateToken();
		try {
			//shopCode = currentUser.getShopRoot().getShopCode();
			if (currentUser != null && currentUser.getShopRoot() != null) {
				Date date = DateUtil.now();
				if (staff != null && staff.getShop() != null) {
					date = shopLockMgr.getApplicationDate(currentUser.getShopRoot().getShopId());
				}
				if (date != null) {
					startDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
					endDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					startDate = DateUtil.toDateString(shopLockMgr.getApplicationDate(currentUser.getShopRoot().getShopId()), DateUtil.DATE_FORMAT_DDMMYYYY);
					endDate = startDate;
				}
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				if (shop != null) {
					shopId = shop.getId();
					curShopId = shop.getId();
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ManageTransStockAction.execute()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * Search. Ham search() su dung de tim kiem danh sach giao dich kho	 * 
	 * @param :startDate (Tu ngay)& toDate(Đen Ngay) & type(Loai)
	 * @author hoanv25
	 * @since 14 - 11 - 2014
	 */
	public String search() {
		actionStartTime = DateUtil.now();
		result.put("total", 0);
		result.put("rows", new ArrayList<StockTransVO>());
		if (currentUser == null || currentUser.getShopRoot() == null) {
			return JSON;
		}
		try {
			KPaging<StockTransVO> kPaging = new KPaging<StockTransVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date startTemp = null;
			Date endTemp = null;
			if (!StringUtil.isNullOrEmpty(startDate)) {
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(startDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.managetrans.search.startdate"), null);
				if (StringUtil.isNullOrEmpty(checkMessage)) {
					startTemp = DateUtil.parse(startDate, ConstantManager.FULL_DATE_FORMAT);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", checkMessage);
					return ERROR;
				}
			}
			if (!StringUtil.isNullOrEmpty(endDate)) {
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(endDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.managetrans.search.enddate"), null);
				if (StringUtil.isNullOrEmpty(checkMessage)) {
					endTemp = DateUtil.parse(endDate, ConstantManager.FULL_DATE_FORMAT);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", checkMessage);
					return ERROR;
				}
			}
			StockStransFilter filter = new StockStransFilter();
			filter.setkPagingStockTransVO(kPaging);
			filter.setStatus(status);
			if (startTemp != null) {
				filter.setStartTemp(startTemp);
			}
			if (endTemp != null) {
				filter.setEndTemp(endTemp);
			}
			if (type != null && type != -1) {
				if (type == 0) {
					filter.setFromOwnerType(new Long(1));
					filter.setToOwnerType(new Long(2));
				} else if (type == 1) {
					filter.setFromOwnerType(new Long(2));
					filter.setToOwnerType(new Long(1));
				} else if (type == 2) {
					filter.setFromOwnerType(new Long(1));
					filter.setToOwnerType(null);
				} else if (type == 3) {
					filter.setFromOwnerType(null);
					filter.setToOwnerType(new Long(1));
				} else if (type == 4) {
					filter.setFromOwnerType(new Long(1));
					filter.setToOwnerType(new Long(1));
				}
			} else {
				// lay tat ca
				filter.setFromOwnerType(null);
				filter.setToOwnerType(null);
			}
			ObjectVO<StockTransVO> lstStockTrans = null;
			if (shopId != null) {
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				if (shop != null) {
					filter.setShopId(shop.getId());
					filter.setLstShopId(null);
				}
			} else {
				filter.setShopId(currentUser.getShopRoot().getShopId());
				filter.setLstShopId(null);
			}
			lstStockTrans = stockMgr.getListStockTransVOFilter(filter);
			if (lstStockTrans != null && lstStockTrans.getLstObject() != null && !lstStockTrans.getLstObject().isEmpty()) {
				for (int i = 0, sz = lstStockTrans.getLstObject().size(); i < sz; ++i) {
					lstStockTrans.getLstObject().get(i).setStockTransDateStr(DateUtil.toDateString(lstStockTrans.getLstObject().get(i).getStockTransDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				}
				result.put("total", lstStockTrans.getkPaging().getTotalRows());
				result.put("rows", lstStockTrans.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ManageTransStockAction.search()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * View detail stock. Ham viewDetailStock() su dung de tim kiem chi tiet
	 * thong tin quan ly giao dich kho
	 * 
	 * @param :stockTransCode (Ma giao dich)
	 * @author hoanv25
	 * @since 14-11-2014
	 */
	public String viewDetailStock() {
		actionStartTime = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		if (currentUser == null || currentUser.getShopRoot() == null) {
			return JSON;
		}
		try {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = staff.getShop();
			KPaging<StockTransLotVO> kPaging = new KPaging<StockTransLotVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
//			if (!StringUtil.isNullOrEmpty(stockTransCode)) {
			if (stockTransId != null) {
//				StockTrans stocktrans = stockMgr.getStockTransByCode(stockTransCode);
				StockTrans stocktrans = stockMgr.getStockTransById(stockTransId);
				if (stocktrans != null) {
					result.put("total", 0);
					result.put("rows", new ArrayList<StockTransLotVO>());
				}
				result.put("totalWeight", stocktrans.getTotalWeight());
				result.put("totalAmount", stocktrans.getTotalAmount());
				ObjectVO<StockTransLotVO> lstProductDetailLot = null;
				/*if (stocktrans.getShop().getId() == shop.getId()) {
					lstProductDetailLot = stockMgr.getListStockTransLotVO(kPaging, stocktrans.getId());
				} else {
					lstProductDetailLot = stockMgr.getListStockTransLotVO(kPaging, stocktrans.getId());
				}*/
				if (stocktrans.getShop() != null && checkShopInOrgAccessById(stocktrans.getShop().getId())) {
					lstProductDetailLot = stockMgr.getListStockTransLotVO(kPaging, stocktrans.getId());
				}
				if (lstProductDetailLot != null) {
					result.put("total", lstProductDetailLot.getkPaging().getTotalRows());
					result.put("rows", lstProductDetailLot.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ManageTransStockAction.viewDetailStock()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * View detail.
	 * Ham viewDetail() su dung de tim kiem chi tiet thong tin giao dich kho
	 * @param :stockTransCode (Ma giao dich) 
	 * @author thongnm
	 * @since Sep 21, 2012
	 */
	public String viewDetail() {
		actionStartTime = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<StockTransLotVO> kPaging = new KPaging<StockTransLotVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);			
			//boolean isPermission = checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR, VSARole.VNM_SALESONLINE_HO);
			boolean isExistShop = checkExistShopOfUserLogin();
			//if(isPermission && isExistShop && !StringUtil.isNullOrEmpty(stockTransCode)) {
//			if (isExistShop && !StringUtil.isNullOrEmpty(stockTransCode)) {
			if (isExistShop && stockTransId != null) {
//				StockTrans stocktrans = stockMgr.getStockTransByCode(stockTransCode);
				StockTrans stocktrans = stockMgr.getStockTransById(stockTransId);
				if (stocktrans != null) {
					result.put("total", 0);
					result.put("rows", new ArrayList<StockTransLotVO>());
				}
				result.put("totalWeight", stocktrans.getTotalWeight());
				result.put("totalAmount", stocktrans.getTotalAmount());
				ObjectVO<StockTransLotVO> lstProductDetailLot = null;
				/*if (checkRoles(VSARole.VNM_SALESONLINE_HO)) {
					lstProductDetailLot = stockMgr.getListStockTransLotVO(kPaging, stocktrans.getId());
				} else if (checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) {
					if (shop.getId() != null && shop.getId().equals(stocktrans.getShop().getId())) {
						lstProductDetailLot = stockMgr.getListStockTransLotVO(kPaging, stocktrans.getId());
					}
				}*/
				if (stocktrans.getShop() != null && checkShopInOrgAccessById(stocktrans.getShop().getId())) {
					lstProductDetailLot = stockMgr.getListStockTransLotVO(kPaging, stocktrans.getId());
				}
				if (lstProductDetailLot != null) {
					result.put("total", lstProductDetailLot.getkPaging().getTotalRows());
					result.put("rows", lstProductDetailLot.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ManageTransStockAction.viewDetail()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	public String printStockTrans() {
		actionStartTime = DateUtil.now();
		parametersReport = new HashMap<String, Object>();
		formatType = FileExtension.PDF.getName();
		// get data for parameters

		parametersReport.put("fDate", DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY));

		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			// get data for datasource
			StockTrans stockTrans = null;
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
			parametersReport.put("fDate", printDate);
			Staff newStaff = null;
			newStaff = staffMgr.getStaffByCode(vansaleCode);
			if (newStaff != null) {
				parametersReport.put("staffCode", codeNameDisplay(newStaff.getStaffCode(), newStaff.getStaffName()));
			}
			//			stockTrans = stockMgr.getStockTransByCode(code);
			if (stockTransId != null) {
				stockTrans = stockMgr.getStockTransById(stockTransId);
			}
			List<StockTransLotVO> listData = new ArrayList<StockTransLotVO>();
			if (stockTrans != null) {
				if (stockTrans.getShop() != null) {
					shop = stockTrans.getShop();
					parametersReport.put("shopName", shop.getShopName());
					if (!StringUtil.isNullOrEmpty(shop.getAddress())) {
						parametersReport.put("address", shop.getAddress());
					} else {
						parametersReport.put("address", "");
					}
				}

				parametersReport.put("code", code);
				parametersReport.put("totalWeight", stockTrans.getTotalWeight());
				parametersReport.put("totalAmount", stockTrans.getTotalAmount());
				parametersReport.put("stockTransCode", stockTrans.getStockTransCode());
				parametersReport.put("stockTransDate", DateUtil.toDateString(stockTrans.getStockTransDate(), DateUtil.DATE_FORMAT_DDMMYYYY));

				ObjectVO<StockTransLotVO> objectVO = stockMgr.getListStockTransLotVO(null, stockTrans.getId());
				if (objectVO != null) {
					listData = objectVO.getLstObject();
					if (listData != null && listData.size() > 0) {
						int n = listData.size();
						for (int i = 0; i < n; i++) {
							listData.get(i).setBigUnit(listData.get(i).getThung());
							listData.get(i).setSmallUnit(listData.get(i).getLe());
						}
					}
				}
			}
			listData.add(0, new StockTransLotVO());
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, listData);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, parametersReport);
			FileExtension ext = FileExtension.parseValue(formatType);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
			String outputPath = null;
			if (transType.equals(XUAT_KHO_NHAN_VIEN)) {
				outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.XKNV);
			} else if (transType.equals(NHAP_KHO_NHAN_VIEN)) {
				outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.NVTH);
			} else if (transType.equals(XUAT_KHO_DIEU_CHINH)) {
				outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.XKDC);
			} else if (transType.equals(NHAP_KHO_DIEU_CHINH)) {
				outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.NKDC);
			}
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			return JSON;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ManageTransStockAction.printStockTrans()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * In phieu xuat kho kiem van chuyen noi bo
	 * @author vuonghn
	 * @return
	 */
	public String print_PXNKVCNB() {
		actionStartTime = DateUtil.now();
		parametersReport = new HashMap<String, Object>();
		try {
			Shop curShop = getCurrentShop();
			if (curShop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return JSON;
			}
			StockTrans stocktrans;
			//if(!StringUtil.isNullOrEmpty(stockTransCode)) {
			if (stockTransId != null) {
				//stocktrans = stockMgr.getStockTransByCode(stockTransCode);
				stocktrans = stockMgr.getStockTransById(stockTransId);
				if (stocktrans == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST, "Giao dịch kho"));
					return JSON;
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, "Giao dịch kho"));
				return JSON;
			}
			ReportUrl reportUrl = reportManagerMgr.getReportFileName(stocktrans.getShop().getId(), StringUtil.TEMPLATE_XNB.trim());
			if (reportUrl == null || StringUtil.isNullOrEmpty(reportUrl.getUrl())) {
				//Chua co mau hoa don GTGT
				result.put(ERROR, true);
				result.put("errMsg", "Không có template XNB trong trong dữ liệu để xuất báo cáo.");
				return JSON;
			}
			String nameFile = reportUrl.getUrl();
			int idx = nameFile.lastIndexOf(".jrxml");
			nameFile = nameFile.substring(0, idx);
			nameFile = nameFile + ".jasper";
			String templatePath = Configuration.getReportTemplatePath() + nameFile;
			File fileReport = new File(templatePath);
			if (!fileReport.exists()) {
				result.put(ERROR, true);
				result.put("errMsg", "Không có template XNB trong hệ thống để xuất báo cáo.");
				return JSON;
			}

			List<RptPXKKVCNB_Stock> lstDataStock = new ArrayList<RptPXKKVCNB_Stock>();
			Date dCurrent = DateUtil.now();
			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			parametersReport.put("cDate", DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR));
			parametersReport.put("pDay", dCurrent.getDate());
			parametersReport.put("pMonth", dCurrent.getMonth() + 1);
			parametersReport.put("pYear", dCurrent.getYear() + 1900);

			parametersReport.put("noi_dung", this.transacContent);
			parametersReport.put("lenh_dieu_dong", this.transacName);
			//parametersReport.put("don_vi", this.transacCode);
			Integer transacDay = 0;
			Integer transacMonth = 0;
			Integer transacYear = 0;
			if (!StringUtil.isNullOrEmpty(issuedDate)) {
				transacDay = DateUtil.getDay(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				transacMonth = DateUtil.getMonth(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				transacYear = DateUtil.getYear(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				parametersReport.put("don_vi_ngay", transacDay);
				parametersReport.put("don_vi_thang", transacMonth);
				parametersReport.put("don_vi_nam", transacYear);
			} else {
				parametersReport.put("don_vi_ngay", dCurrent.getDate());
				parametersReport.put("don_vi_thang", dCurrent.getMonth() + 1);
				parametersReport.put("don_vi_nam", dCurrent.getYear() + 1900);
			}
			RptPXKKVCNB_Stock dataStock = stockMgr.getPXKKVCNB(stocktrans.getShop().getId(), staffCode, stocktrans.getId(), transacName, transacContent);
			lstDataStock.add(dataStock);
			if (lstDataStock.size() > 0) {
				//FileExtension ext = FileExtension.PDF;
				JRDataSource dataSource = new JRBeanCollectionDataSource(lstDataStock);
				String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(StringUtil.TEMPLATE_XNB.toLowerCase().trim() + "-" + curShop
						.getShopCode().toLowerCase() + ".pdf")).toString());

				String outputPath = Configuration.getReportTemplatePath() + outputFile;
				String outputDowload = Configuration.getReportRealPath() + outputFile;
				JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parametersReport, dataSource);
				JRAbstractExporter exporter = new JRPdfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
				exporter.exportReport();
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputDowload);
			} else {
				result.put(ERROR, false);
				result.put(ERROR, false);
				result.put("hasData", false);
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ManageTransStockAction.print_PXNKVCNB()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(String lstShopId) {
		this.lstShopId = lstShopId;
	}

	public Long getStockTransId() {
		return stockTransId;
	}

	public void setStockTransId(Long stockTransId) {
		this.stockTransId = stockTransId;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public final Long getCurShopId() {
		return curShopId;
	}

	public final void setCurShopId(Long curShopId) {
		this.curShopId = curShopId;
	}

	public final Long getShopId() {
		return shopId;
	}

	public final void setShopId(Long shopId) {
		this.shopId = shopId;
	}
}

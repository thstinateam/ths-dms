package ths.dms.web.action.stock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransDetailVO;
import ths.dms.core.entities.vo.StockTransLotVO;

// TODO: Auto-generated Javadoc
/**
 * The Class ReceivedStockAction.
 */
public class ReceivedStockAction extends AbstractAction {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	
	/** The staff code. */
	private String staffCode;
	
	/** The stock trans code. */
	private String stockTransCode;
	
	/** The stock mgr. */
	private StockMgr stockMgr;
	
	/** The staff mgr. */
	private StaffMgr staffMgr;
	
	/** The shop mgr. */
	private ShopMgr shopMgr;
	
	/** The staff id. */
	private Long staffId;
	
	/** The lst product. */
	private List<ProductLotVO> lstProduct;
	
	/** The staff. */
	private Staff staff;
	
	/** The shop. */
	private Shop shop;
	
	/** The total weight. */
	private Float totalWeight;
	
	/** The shop code. */
	private String shopCode;
	
	/** The product mgr. */
	private ProductMgr productMgr;
	
	/** The input date. */
	private String inputDate;
	
	/** The product code. */
	private String productCode;
	
	/** The product name. */
	private String productName;
	
	/** The lst product vo. */
	private String lstProductVO;
	
	private String staffTypeCode;
	
	private List<Staff> lstStaff;
	
	private HashMap<String, Object> parametersReport;
	
	private String vansaleCode;
	
	private String code;
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getVansaleCode() {
		return vansaleCode;
	}

	public void setVansaleCode(String vansaleCode) {
		this.vansaleCode = vansaleCode;
	}

	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}

	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	public List<Staff> getLstStaff() {
		return lstStaff;
	}

	public void setLstStaff(List<Staff> lstStaff) {
		this.lstStaff = lstStaff;
	}

	public String getStaffTypeCode() {
		return staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	/**
	 * Gets the lst product vo.
	 *
	 * @return the lst product vo
	 */
	public String getLstProductVO() {
		return lstProductVO;
	}

	/**
	 * Sets the lst product vo.
	 *
	 * @param lstProductVO the new lst product vo
	 */
	public void setLstProductVO(String lstProductVO) {
		this.lstProductVO = lstProductVO;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}
	
	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}
	
	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	/**
	 * Gets the shop mgr.
	 *
	 * @return the shop mgr
	 */
	public ShopMgr getShopMgr() {
		return shopMgr;
	}
	
	/**
	 * Sets the shop mgr.
	 *
	 * @param shopMgr the new shop mgr
	 */
	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}
	
	/**
	 * Gets the product mgr.
	 *
	 * @return the product mgr
	 */
	public ProductMgr getProductMgr() {
		return productMgr;
	}
	
	/**
	 * Sets the product mgr.
	 *
	 * @param productMgr the new product mgr
	 */
	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}
	
	/**
	 * Gets the stock mgr.
	 *
	 * @return the stock mgr
	 */
	public StockMgr getStockMgr() {
		return stockMgr;
	}
	
	/**
	 * Sets the stock mgr.
	 *
	 * @param stockMgr the new stock mgr
	 */
	public void setStockMgr(StockMgr stockMgr) {
		this.stockMgr = stockMgr;
	}
	
	/**
	 * Gets the staff mgr.
	 *
	 * @return the staff mgr
	 */
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}
	
	/**
	 * Sets the staff mgr.
	 *
	 * @param staffMgr the new staff mgr
	 */
	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}
	
	/**
	 * Gets the shop code.
	 *
	 * @return the shop code
	 */
	public String getShopCode() {
		return shopCode;
	}
	
	/**
	 * Sets the shop code.
	 *
	 * @param shopCode the new shop code
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/** The total amount. */
	private BigDecimal totalAmount = BigDecimal.ZERO;
	
	/**
	 * Gets the total weight.
	 *
	 * @return the total weight
	 */
	
	
	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	
	public Float getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(Float totalWeight) {
		this.totalWeight = totalWeight;
	}

	/**
	 * Sets the total amount.
	 *
	 * @param totalAmount the new total amount
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#getStaff()
	 */
	public Staff getStaff() {
		return staff;
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#setStaff(ths.dms.core.entities.Staff)
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}
	
	/**
	 * Gets the shop.
	 *
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}
	
	/**
	 * Sets the shop.
	 *
	 * @param shop the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	/**
	 * Gets the lst product.
	 *
	 * @return the lst product
	 */
	public List<ProductLotVO> getLstProduct() {
		return lstProduct;
	}
	
	/**
	 * Sets the lst product.
	 *
	 * @param lstProduct the new lst product
	 */
	public void setLstProduct(List<ProductLotVO> lstProduct) {
		this.lstProduct = lstProduct;
	}
	
	/**
	 * Gets the staff id.
	 *
	 * @return the staff id
	 */
	public Long getStaffId() {
		return staffId;
	}
	
	/**
	 * Sets the staff id.
	 *
	 * @param staffId the new staff id
	 */
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	
	/**
	 * Gets the staff code.
	 *
	 * @return the staff code
	 */
	public String getStaffCode() {
		return staffCode;
	}
	
	/**
	 * Sets the staff code.
	 *
	 * @param staffCode the new staff code
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	
	/**
	 * Gets the stock trans code.
	 *
	 * @return the stock trans code
	 */
	public String getStockTransCode() {
		return stockTransCode;
	}
	
	/**
	 * Sets the stock trans code.
	 *
	 * @param stockTransCode the new stock trans code
	 */
	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockMgr = (StockMgr) context.getBean("stockMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		productMgr = (ProductMgr)context.getBean("productMgr");
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {
		resetToken(result);
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null && staff.getShop() != null) {
					shop = staff.getShop();
					if (shop != null)
						shopCode = shop.getShopCode();
				}
			}
			stockTransCode = stockMgr.generateStockTransCodeVansale(getCurrentShop().getId(), "GO");
			Date date = null;
			if (shop != null) {
				date = shopLockMgr.getApplicationDate(shop.getId());
			}
			if (date != null) {
				inputDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				inputDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			setSeparationLot(separationLotAuto(ConstantManager.RECEIVED_STOCK_AP_PARAM_CODE));
			totalAmount = BigDecimal.ZERO;
			totalWeight = 0F;
			StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
			if (staffMgr != null) {
				ObjectVO<Staff> staffVO = null;
				String parentShopCode = null;
				staff = getStaffByCurrentUser();
				if (staff == null) {
					return JSON;
				}
				parentShopCode = staff.getShop().getShopCode();
				if (StringUtil.isNullOrEmpty(shopCode)) {
					shopCode = staff.getShop().getShopCode();
				}
				StaffObjectType staffType = null;
				if (!StringUtil.isNullOrEmpty(staffTypeCode)) {
					staffType = StaffObjectType.parseValue(Integer.valueOf(staffTypeCode));
				}
				List<Integer> listStaffType = new ArrayList<Integer>();
				listStaffType.add(1);
				listStaffType.add(2);
				if (!StringUtil.isNullOrEmpty(parentShopCode) && !StringUtil.isNullOrEmpty(shopCode) && shopMgr.checkAncestor(parentShopCode, shopCode)) {
					StaffFilter staffFilter = new StaffFilter();
					staffFilter.setStatus(ActiveType.RUNNING);
					staffFilter.setShopCode(shopCode);
					staffFilter.setStaffType(staffType);
					staffFilter.setChannelTypeType(ChannelTypeType.STAFF);
					staffFilter.setLstChannelObjectType(listStaffType);
					staffVO = staffMgr.getListStaffEx2(staffFilter);
				}
				if (staffVO != null) {
					lstStaff = staffVO.getLstObject();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Search product.
	 *
	 * @return the string
	 */
	public String searchProduct(){		
		result.put("page", page);
	    result.put("max", max);	    
	    try{			
	    	KPaging<StockTotalVO> kPaging = new KPaging<StockTotalVO>();	
			kPaging.setPage(page-1);
			kPaging.setPageSize(max);		
			if(!StringUtil.isNullOrEmpty(staffCode)){
				Staff sTmp = staffMgr.getStaffByCode(staffCode);
				Staff staff = getStaffByCurrentUser();				
				if(sTmp==null || staff==null){					
					return JSON;		
				}else if(!shopMgr.checkAncestor(staff.getShop().getShopCode(), sTmp.getShop().getShopCode())){					
					return JSON;	
				}else if(!sTmp.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){					
					return JSON;
				}
				staffId = sTmp.getId();
				/*
				 * comment code loi compile
				 * @modified by tuannd20
				 * @date 20/12/2014
				 */
				/*ObjectVO<StockTotalVO> stockTotalVO = 	stockMgr.getListStockTotalVO(kPaging, productCode, 
						productName, null, staffId, StockObjectType.STAFF, null, null,1, null,null,null);
				if(stockTotalVO!= null){
				    result.put("total", stockTotalVO.getkPaging().getTotalRows());
			    	result.put("rows", stockTotalVO.getLstObject());
				}*/    
			}		  		
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}

	/**
	 * Gets the info.
	 *
	 * @return the info
	 */
	public String getInfo() {
//		Float totalWeightT = 0f;
		totalAmount = BigDecimal.ZERO;
		totalWeight = 0F;
		lstProduct = new ArrayList<ProductLotVO>();
		setSeparationLot(separationLotAuto(ConstantManager.RECEIVED_STOCK_AP_PARAM_CODE));
		errMsg = null;		
		try {
			staff = getStaffByCurrentUser();	
			stockTransCode = stockMgr.generateStockTransCodeVansale(getCurrentShop().getId(),"GO");
			if(!StringUtil.isNullOrEmpty(staffCode)){
				Staff sTmp = staffMgr.getStaffByCode(staffCode);							
				if(sTmp==null){					
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
							Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code"));
					return LIST;				
				}else if(!shopMgr.checkAncestor(staff.getShop().getShopCode(), sTmp.getShop().getShopCode())){					
					errMsg =  ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code"));
					return LIST;	
				}else if(!sTmp.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){					
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, 
							Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code"));
					return LIST;	
				}
				staffId = sTmp.getId();
			}else{
				return LIST;
			}
			Integer sys_speration_auto = separationLotAuto(ConstantManager.RECEIVED_STOCK_AP_PARAM_CODE);
			ObjectVO<ProductLotVO> lstProductLot = stockMgr.getListProductInStock(null,staff.getShop().getId() , StockObjectType.SHOP, 
					staffId, StockObjectType.STAFF, 0, ActiveType.RUNNING, sys_speration_auto);
			if (lstProductLot != null) {
				lstProduct = lstProductLot.getLstObject();
				if(lstProduct !=null && lstProduct.size() > 0){						
					for(int i=0;i<lstProduct.size();i++){						
						if(lstProduct.get(i).getQuantity() != null){
							BigDecimal tmp = new BigDecimal(lstProduct.get(i).getQuantity());
							tmp = tmp.multiply(lstProduct.get(i).getPrice());
							totalAmount = totalAmount.add(tmp);							
							totalWeight += lstProduct.get(i).getGrossWeight() * Float.valueOf(lstProduct.get(i).getQuantity());
						}
					}					
				}
			}
						
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return LIST;
	}
	
	/**
	 * Import stock vansale.
	 * @author hungnm
	 * @return the string
	 */
	public String importStockVansale() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return JSON;
			}
			shop = staff.getShop();
			shopCode = shop.getShopCode();
			if (!StringUtil.isNullOrEmpty(staffCode) && !StringUtil.isNullOrEmpty(shopCode)) {
				Staff staffVanSales = staffMgr.getStaffByCode(staffCode);
				if (staffVanSales == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
					return JSON;
				} else if (!shopMgr.checkAncestor(shopCode, staffVanSales.getShop().getShopCode())) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
					return JSON;
				} else if (!staffVanSales.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
					return JSON;
				}
				StockTrans stockTrans = stockMgr.getStockTransByCode(stockTransCode);
				if (stockTrans != null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "stock.received.stocktran"));
					return JSON;
				}
				stockTrans = new StockTrans();
				stockTrans.setStockTransCode(stockTransCode);
				stockTrans.setTotalAmount(totalAmount);
				stockTrans.setShop(shop);
				stockTrans.setStaffId(staff);
				Date stock_trans_date = shopLockMgr.getApplicationDate(shop.getId());
				if (stock_trans_date == null) {
					stock_trans_date = DateUtil.now();
				}
				stockTrans.setStockTransDate(stock_trans_date);
				stockTrans.setCreateUser(staff.getStaffCode());
				stockTrans.setTransType("GO");
				if (lstProductVO == null || lstProductVO.length() <= 0) {
					return JSON;
				}
				String[] PRODUCTS = lstProductVO.split(";");
				List<StockTransDetailVO> lstStockTransDetailVO = new ArrayList<StockTransDetailVO>();
				String list_product = "";
				String stock_total_has_not_enough = "";
				for (String STR_PRODUCT_CODE : PRODUCTS) {
					String PRODUCT_CODE = STR_PRODUCT_CODE.split(":")[0];
					Product product = productMgr.getProductByCode(PRODUCT_CODE);
					if (product == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.product.code") + " " + PRODUCT_CODE));
						return JSON;
					}
					StockTransDetail stockTransDetail = new StockTransDetail();
					stockTransDetail.setStockTrans(stockTrans);
					stockTransDetail.setProduct(product);
					stockTransDetail.setStockTransDate(stock_trans_date);
					stockTransDetail.setCreateUser(staff.getStaffCode());

					StockTotalVO stock_total = stockMgr.getStockTotalByProductCodeAndOwnerEx(PRODUCT_CODE, StockObjectType.STAFF, staffVanSales.getId());
					if (stock_total == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STOCK_RECEIVED_PRODUCT_VANSALES, PRODUCT_CODE, staffVanSales.getStaffName()));
						return JSON;
					}
					Price price = productMgr.getPriceByProductId(shop.getId(), product.getId());//HungLM16 Update to 11/10/2013
					Integer total_quantity = 0;
					if (product.getCheckLot() == 1) {
						String[] STR_LOT = STR_PRODUCT_CODE.split(":")[1].split(",");
						String sub_total_has_enough = "";
						for (String REG_LOT : STR_LOT) {
							String lot = REG_LOT.split("/")[0];
							Integer quantity = Integer.valueOf(REG_LOT.split("/")[1]);
							total_quantity += quantity;
							ProductLot productLot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lot, staffVanSales.getId(), StockObjectType.STAFF, product.getId());
							if (productLot == null) {
								result.put(ERROR, true);
								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STOCK_RECEIVED_LOT_VANSALES, lot, PRODUCT_CODE, staffVanSales.getStaffCode()));
								return JSON;
							} else if ((productLot.getQuantity() < quantity) || productLot.getQuantity() <= 0) {
								sub_total_has_enough += productLot.getLot() + ",";
							}
							stockTransDetail = new StockTransDetail();
							stockTransDetail.setStockTrans(stockTrans);
							stockTransDetail.setProduct(product);
							stockTransDetail.setStockTransDate(stock_trans_date);
							stockTransDetail.setCreateUser(staff.getStaffCode());
							stockTransDetail.setQuantity(quantity);
							if (price != null) {
								stockTransDetail.setPrice(price.getPrice());
							}
							StockTransDetailVO stockTransDetailVO = new StockTransDetailVO();
							stockTransDetailVO.setLot(lot);
							stockTransDetailVO.setStockTransDetail(stockTransDetail);
							lstStockTransDetailVO.add(stockTransDetailVO);
						}
						if (!StringUtil.isNullOrEmpty(sub_total_has_enough)) {
							sub_total_has_enough = sub_total_has_enough.substring(0, sub_total_has_enough.length() - 1);
							stock_total_has_not_enough += product.getProductCode() + "[" + sub_total_has_enough + "]" + ",";
						}
					} else {
						StockTransDetailVO stockTransDetailVO = new StockTransDetailVO();
						Integer quantity = Integer.valueOf(STR_PRODUCT_CODE.split(":")[1]);
						total_quantity = quantity;
						stockTransDetail.setQuantity(quantity);
						if (price != null) {
							stockTransDetail.setPrice(price.getPrice());
						}
						stockTransDetailVO.setStockTransDetail(stockTransDetail);
						lstStockTransDetailVO.add(stockTransDetailVO);
						if (total_quantity > stock_total.getQuantity()) {
							stock_total_has_not_enough += product.getProductCode() + ",";
						}
					}
					if (!total_quantity.equals(stock_total.getQuantity())) {
						list_product += product.getProductCode() + ",";
					}
				}
				if (!StringUtil.isNullOrEmpty(stock_total_has_not_enough)) {
					stock_total_has_not_enough = stock_total_has_not_enough.substring(0, stock_total_has_not_enough.length() - 1);
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STOCK_UPDATE_HAS_NOT_EN_LOT, stock_total_has_not_enough));
					return JSON;
				} else if (!StringUtil.isNullOrEmpty(list_product)) {
					list_product = list_product.substring(0, list_product.length() - 1);
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STOCK_STQ_QUANTITY, list_product));
					return JSON;
				}
				stockMgr.createListStockTransDetail(stockTrans, lstStockTransDetailVO, separationLotAuto(ConstantManager.RECEIVED_STOCK_AP_PARAM_CODE), shop.getId());
				error = false;
				return JSON;
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "ReceivedStockAction.importStockVansale" + ex.getMessage());
			if (!StringUtil.isNullOrEmpty(ex.getMessage()) && ex.getMessage().equals("ths.dms.core.exceptions.DataAccessException: stock total has not enough quantity")) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.lot.quantity.not.enough"));
			} else
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			return JSON;
		}
		result.put(ERROR, error);
		return JSON;
	}
	
	/**
	 * Import stock vansale auto.
	 *
	 * @return the string
	 * @author tientv
	 */
	public String importStockVansaleAuto() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";		
		if (currentUser != null) {
			try {
				staff = getStaffByCurrentUser();
				if(staff==null){
					return JSON;
				}
				shop = staff.getShop();
				shopCode = shop.getShopCode();
				// Note: staffCode la ma nhan vien , id NPP tu currentUser
				// shopCode la shopCode cua NPP								
				BigDecimal totalWeightT = BigDecimal.ZERO;
				if(!StringUtil.isNullOrEmpty(staffCode) && !StringUtil.isNullOrEmpty(shopCode)){
					Staff tmp = staffMgr.getStaffByCode(staffCode);
						if(tmp==null){
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
									Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
							return JSON;				
						}else if(!shopMgr.checkAncestor(shopCode, tmp.getShop().getShopCode())){
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
							return JSON;
						}else if(!tmp.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, 
									Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
							return JSON;
						}
						staffId = tmp.getId();
						if(tmp != null && shop!=null){
							StockTrans temp = stockMgr.getStockTransByCode(stockTransCode);
							if (temp==null) // check trung phieu 
							{
								Integer sys_speration_auto = separationLotAuto(ConstantManager.RECEIVED_STOCK_AP_PARAM_CODE);
								ObjectVO<ProductLotVO> lstProductLot = stockMgr.getListProductInStock(null,staff.getShop().getId() , StockObjectType.SHOP, 
										staffId, StockObjectType.STAFF, 0, ActiveType.RUNNING, sys_speration_auto);
								List<ProductLotVO> lstProduct = lstProductLot.getLstObject();
								if(lstProduct.size() > 0){
									for(int i=0;i<lstProduct.size();i++){
										if(lstProduct.get(i).getNetWeight() != null){
											BigDecimal quantity =  new BigDecimal(lstProduct.get(i).getQuantity());
											BigDecimal grossweight = new BigDecimal(lstProduct.get(i).getGrossWeight());
											totalWeightT = totalWeightT.add(grossweight.multiply(quantity));
										}
										if(lstProduct.get(i).getQuantity() != null){
											BigDecimal tmp1 = new BigDecimal(String.valueOf(lstProduct.get(i).getQuantity()));
											tmp1 = tmp1.multiply(lstProduct.get(i).getPrice());
											totalAmount = totalAmount.add(tmp1);
										}
									}
									StockTrans stockTrans = new StockTrans();
									stockTrans.setStockTransCode(stockTransCode);
									stockTrans.setTotalWeight(totalWeightT);
									stockTrans.setTotalAmount(totalAmount);
									/*stockTrans.setFromOwnerType(StockObjectType.STAFF);
									stockTrans.setToOwnerType(StockObjectType.SHOP);
									stockTrans.setFromOwnerId(tmp.getId().intValue());
									stockTrans.setToOwnerId(shop.getId());*/
									stockTrans.setShop(shop);
									stockTrans.setStaffId(staff);									
//									String dd = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_STR);									
//									Date stock_trans_date = DateUtil.parse(dd, DateUtil.DATE_FORMAT_STR);
									Date stock_trans_date = shopLockMgr.getApplicationDate(shop.getId());
									if(stock_trans_date==null){
										stock_trans_date=DateUtil.now();
									}
									stockTrans.setStockTransDate(stock_trans_date);
									stockTrans.setCreateUser(staff.getStaffCode());
									stockTrans.setTransType("GO");
									if(stockTrans != null){
										List<StockTransDetailVO> lstStockTransDetailVO = new ArrayList<StockTransDetailVO>();
										for(int i = 0; i < lstProduct.size();i++){
											StockTransDetail stockTransDetail = new StockTransDetail();
											stockTransDetail.setStockTrans(stockTrans);
											Product product = productMgr.getProductByCode(lstProduct.get(i).getProductCode());
											stockTransDetail.setProduct(product);
											stockTransDetail.setStockTransDate(stock_trans_date);
											stockTransDetail.setCreateUser(staff.getStaffCode());
											stockTransDetail.setQuantity(lstProduct.get(i).getQuantity());											
											try {
												stockTransDetail.setPrice(lstProduct.get(i).getPrice());
											} catch (Exception e) {
												LogUtility.logError(e, e.getMessage());
											}
											StockTransDetailVO stockTransDetailVO =  new StockTransDetailVO();
											stockTransDetailVO.setStockTransDetail(stockTransDetail);
											stockTransDetailVO.setLot(lstProduct.get(i).getLot());
											lstStockTransDetailVO.add(stockTransDetailVO);
										}
										stockMgr.createListStockTransDetail(stockTrans, lstStockTransDetailVO,separationLotAuto(ConstantManager.RECEIVED_STOCK_AP_PARAM_CODE),shop.getId());
										error = false;
									}
								}else{
									errMsg +=  ValidateUtil.getErrorMsg(ConstantManager.ERR_STOCKTRAIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.stock.trans.null"));
								}
							}else{
								errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "stock.received.stocktran");
							}
						}else{
							if (staff==null) errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,null, "stock.received.staff"); 
						}
				}else{
					errMsg += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.code"));
				}
			} catch (Exception e) {
				
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}
	
	/**
	 * Check product export in day.
	 *
	 * @return the string
	 * @author tientv
	 * @see kiem tra thong tin lo nhap vao, co nam trong danh sach lo xuat trong ngay
	 * hay khong. Bo qua buoc kiem tra nhung lo khong ton tai.
	 */
	public String checkProductExportInDay(){
		result = new HashMap<String, Object>();
		String list_lots = "";
		try{	
			staff = getStaffByCurrentUser();
			if(staff==null){
				return JSON;
			}
			shop = staff.getShop();
			shopCode = shop.getShopCode();
			if(!StringUtil.isNullOrEmpty(staffCode) && !StringUtil.isNullOrEmpty(shopCode)){
				Staff staffVanSales = staffMgr.getStaffByCode(staffCode);
				if(staffVanSales==null){
					result.put("list_lots", list_lots);
					return JSON;				
				}else if(!shopMgr.checkAncestor(shopCode, staffVanSales.getShop().getShopCode())){
					result.put("list_lots", list_lots);
					return JSON;
				}else if(!staffVanSales.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
					result.put("list_lots", list_lots);
					return JSON;
				}
				Date stock_trans_date = shopLockMgr.getApplicationDate(shop.getId());
				if(stock_trans_date==null){
					stock_trans_date=DateUtil.now();
				}
				List<StockTransLotVO> lotVOs = stockMgr.getListLot(shop.getId(), StockObjectType.SHOP, staffVanSales.getId(),StockObjectType.STAFF, stock_trans_date);
				Map<String,List<String>> hash = new HashMap<String, List<String>>();
				for(StockTransLotVO vo:lotVOs){
					List<String> strings = hash.get(vo.getProductCode());
					if(strings==null){
						strings = new ArrayList<String>();
						strings.add(vo.getLot());
					}else{
						strings.add(vo.getLot());
					}
					hash.put(vo.getProductCode(), strings);
				}
				String[] PRODUCTS = lstProductVO.split(";");				
				for(String STR_PRODUCT_CODE:PRODUCTS){						
					String PRODUCT_CODE = STR_PRODUCT_CODE.split(":")[0];
					Product product = productMgr.getProductByCode(PRODUCT_CODE);
					if(product!=null&&product.getCheckLot()==1){						
						String[] STR_LOT = STR_PRODUCT_CODE.split(":")[1].split(",");
						List<String> list_lot = hash.get(product.getProductCode());						
						String result = "";
						for(String REG_LOT:STR_LOT){
							String lot = REG_LOT.split("/")[0];
							ProductLot productLot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lot, staffVanSales.getId(),StockObjectType.STAFF,product.getId());							
							if(productLot!=null && (list_lot==null || !list_lot.contains(lot))){
								result+=lot + ",";
							}
						}
						if(!StringUtil.isNullOrEmpty(result)){
							result = result.substring(0,result.length()-1);
							list_lots+= "<b>" + product.getProductCode()+"</b>[" + result + "] .";
						}
					}
				}
				if(!StringUtil.isNullOrEmpty(list_lots)){
					list_lots = list_lots.substring(0,list_lots.length()-1);
				}
			}			
			result.put("list_lots", list_lots);
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Sets the input date.
	 *
	 * @param inputDate the new input date
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
	/**
	 * Gets the input date.
	 *
	 * @return the input date
	 */
	public String getInputDate() {
		return inputDate;
	}
	
	public String exportReport(){
		parametersReport = new HashMap<String, Object>();
		formatType = FileExtension.PDF.getName();
		// get data for parameters

		parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
		parametersReport.put("fDate", DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY));

		try{
			if(currentUser!= null){
				Staff s = staffMgr.getStaffByCode(currentUser.getUserName());
				if(s!=null){
					Shop shop = s.getShop();
					if(shop!=null){
						parametersReport.put("shopName", shop.getShopName());
						if(!StringUtil.isNullOrEmpty(shop.getAddress())){
							parametersReport.put("address", shop.getAddress());
						} else {
							parametersReport.put("address", "");
						}
					}
				}
			}

			// get data for datasource
			StockTrans stockTrans = null;
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
			parametersReport.put("fDate",printDate);
			
			Staff newStaff = null;	
			newStaff = staffMgr.getStaffByCode(vansaleCode);
			if(newStaff != null){
				parametersReport.put("staffCode",codeNameDisplay(newStaff.getStaffName(),newStaff.getStaffCode()));
			}
			stockTrans = stockMgr.getStockTransByCode(code);
			List<StockTransLotVO> listData = new ArrayList<StockTransLotVO>();
			if(stockTrans != null){
				parametersReport.put("code", code);
				parametersReport.put("totalWeight", stockTrans.getTotalWeight());
				parametersReport.put("totalAmount", stockTrans.getTotalAmount());
				parametersReport.put("stockTransCode", stockTrans.getStockTransCode());
				parametersReport.put("stockTransDate", DateUtil.toDateString(stockTrans.getStockTransDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				
				ObjectVO<StockTransLotVO> objectVO = stockMgr.getListStockTransLotVO(null, stockTrans.getId());
				if(objectVO != null){
					listData = objectVO.getLstObject();
					if(listData != null && listData.size() >0){
						int n = listData.size();
						for(int i=0;i<n;i++){
							listData.get(i).setBigUnit(listData.get(i).getThung());
							listData.get(i).setSmallUnit(listData.get(i).getLe());
						}
					}
				}			
			}
			/*
			staff = getStaffByCurrentUser();
			if(!StringUtil.isNullOrEmpty(vansaleCode)){
				Staff sTmp = staffMgr.getStaffByCode(vansaleCode);
				if(sTmp != null){
					staffId = sTmp.getId();
				}
			}
			List<ProductLotVO> listData = new ArrayList<ProductLotVO>();
			if(stockTrans != null){
				Integer sys_speration_auto = separationLotAuto(ConstantManager.RECEIVED_STOCK_AP_PARAM_CODE);
				ObjectVO<ProductLotVO> objectVO = stockMgr.getListProductInStock(null,staff.getShop().getId() , StockObjectType.SHOP, 
						staffId, StockObjectType.STAFF, 0, ActiveType.RUNNING, sys_speration_auto);
				if(objectVO != null){
					listData = objectVO.getLstObject();
					if(listData != null && listData.size() >0){
						int n = listData.size();
						for(int i=0;i<n;i++){
							listData.get(i).setBigUnit(listData.get(i).getThung());
							listData.get(i).setSmallUnit(listData.get(i).getLe());
						}
					}
				}				
			}
			listData.add(0,new ProductLotVO());
			*/
			listData.add(0,new StockTransLotVO());
			//stockTransLotVOs = shopReportMgr.getRptExImStockOfStaff(shopId, listStaffId, fDate, tDate);
			//lstBKPTHNVGHData = shopReportMgr.getListRptReturnSaleOrderByDelivery(shopId,OrderType.CM,fDate,tDate,staffId);				
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, listData);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, parametersReport);
			FileExtension ext = FileExtension.parseValue(formatType);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
			String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource,ShopReportTemplate.NVTH);	
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			return JSON;
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Get trans code.
	 * @author loctt
	 * @since 17Oct2013
	 * @return the string
	 * 
	 * update tao ma bang ngay chot thay vi ngay hien tai
	 * @author hunglm16
	 * @since MAY 16, 2014
	 */
	public String getTransCode(){		
		try {
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
					if (shop!=null) shopCode = shop.getShopCode();
				}
				stockTransCode = stockMgr.generateStockTransCodeVansale(shop.getId(),"GO");
				result.put("stockTransCode", stockTransCode);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	    return JSON;
	}

}

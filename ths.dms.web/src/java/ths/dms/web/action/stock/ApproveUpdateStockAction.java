/*
 * Copyright 2014 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.action.stock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SaleOrderStockDetailVO;
import ths.dms.core.entities.vo.SaleOrderStockVO;
import ths.dms.core.entities.vo.SaleOrderStockVOError;
import ths.dms.core.entities.vo.SaleOrderStockVOTemp;
import ths.dms.core.entities.vo.SearchSaleTransactionErrorVO;
import ths.dms.core.entities.vo.StaffVO;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.TreeNode;
import ths.dms.web.business.common.TreeUtility;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * The Class ApproveUpdateStockAction.
 */
public class ApproveUpdateStockAction extends AbstractAction{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	
	/** The mgr. */
	private StockMgr stockMgr; 
	
	/*private ProductMgr productMgr;*/

	private SaleOrderMgr saleOrderMgr;
	
	/** The String. */
	private String code;
	private String name;
	private String orderNumber;
	private String deliveryStaffCode;
	private String saleStaffCode;
	private String orderType;
	private Integer orderSource;
	private String fromDate;
	private String toDate;
	private String numberValue;
	private Long numberValueOrder;
	private Integer isValueOrder;
	private Long shopId;
	private Staff staff;
	private Shop shop;
	private Date now;
	
	/** The List */
	
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH
	private List<StaffVO> lstNVGHVo;//Danh sach NVGH
	private List<ShopToken> listShopToken;
	private List<TreeNode> searchUnitTree;
	
	/** List lay du lieu */
	private List<SaleOrderStockVOTemp> listTemp;
	private List<Long> lstId;
	

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		//stockMgr = (StockMgr)context.getBean("stockMgr");
//		productMgr = (ProductMgr)context.getBean("productMgr");
		saleOrderMgr = (SaleOrderMgr)context.getBean("saleOrderMgr");
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		if (currentUser != null) {
			currentUser = getCurrentUser();
		}		
		return SUCCESS;
	}
	
	/**
	 * @author vuongmq
	 * @since 26 - August, 2014
	 * @return
	 * @description Lấy danh sách đơn hàng để cập nhật phải thu và kho (mặc định lấy đơn hàng IN)
	 */
	public String search() throws Exception {
		try {
			result.put("page", page);
		    result.put("max", max);
		    staff = getStaffByCurrentUser();
		    if(shopId != null){
		    	shop = shopMgr.getShopById(shopId);
		    } else {
		    	result.put("total", 0);
		    	result.put("rows", new ArrayList<SaleOrderStockVO>());
		    	return JSON;
		    }
			if (shop != null) {
				checkCreateShopParamOrderSearch(numberValueOrder, shop.getId());
			}
			KPaging<SaleOrderStockVO> kPaging = new KPaging<SaleOrderStockVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);

			SoFilter soFilter = new SoFilter();
			soFilter.setShopId(shop.getId());
			if(!StringUtil.isNullOrEmpty(orderType) 
					&& (orderType.equals(OrderType.DC.getValue()) || orderType.equals(OrderType.DCT.getValue()) || orderType.equals(OrderType.DCG.getValue()))) {
				soFilter.setOrderType(OrderType.parseValue(orderType));
			} else {
				List<OrderType> orderTypes = new ArrayList<OrderType>();
				orderTypes.add(OrderType.DC);
				orderTypes.add(OrderType.DCT);
				orderTypes.add(OrderType.DCG);
				soFilter.setListOrderType(orderTypes);
			}
			soFilter.setOrderNumber(orderNumber);
			soFilter.setStrListShopId(getStrListShopId());
			ObjectVO<SaleOrderStockVO> obSaleOrder = saleOrderMgr.getListSaleOrderStockUpdate(kPaging, soFilter);
			if(obSaleOrder != null && obSaleOrder.getLstObject().size() > 0) {
				result.put("total", obSaleOrder.getkPaging().getTotalRows());
			    result.put("rows", obSaleOrder.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<SaleOrderStockVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "UpdateOrderStockAction.search()" + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * tra ve cau truc cay don vi tim kiem
	 * 
	 * @author trietptm
	 * @return dinh dang du lieu tra ve
	 * @since 24/07/2015
	 */
	public String buildSearchUnitTree() {
		TreeUtility treeUtility = new TreeUtility(context);
		setSearchUnitTree(treeUtility.buildUnitTree(currentUser.getShopRoot().getShopId(), false, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId()));
		return JSON;
	}

	/**
	 * Duyet dieu chinh kho
	 * @author vuongmq
	 * @since 04/09/2015 
	 */
	public String approveStockOrder() throws Exception {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			if (listTemp == null || listTemp.size() <= 0) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("stock.approve.cyclecount.no.code.sale.order"));
				return JSON;
			}
			String errMsgInfo = "";
			List<SaleOrderStockVOError> errorVOs = new ArrayList<SaleOrderStockVOError>();
			
			for (int i = 0, sz = listTemp.size(); i < sz; i++) {
				SaleOrderStockVOTemp tempOrder = listTemp.get(i);
				SaleOrderStockVOError errorVO = new SaleOrderStockVOError();
				String productError = "";
				try {
					// check kiem ke kho
					if (OrderType.DCG.getValue().equals(tempOrder.getOrderType())) {
						StockStransFilter filter = new StockStransFilter();
						filter.setStockTransId(tempOrder.getId());
						filter.setFromOwnerId(tempOrder.getId());
						if (saleOrderMgr.checkStockTransCycleCount(filter)) {
							errorVO.setId(tempOrder.getId());
							errorVO.setOrderNumber(tempOrder.getOrderNumber());
							errorVO.setErrOrder(R.getResource("stock.approve.cyclecount.process.err"));
						}
					} else if (OrderType.DCT.getValue().equals(tempOrder.getOrderType())) {
						StockStransFilter filter = new StockStransFilter();
						filter.setStockTransId(tempOrder.getId());
						filter.setToOwnerID(tempOrder.getId());
						if (saleOrderMgr.checkStockTransCycleCount(filter)) {
							errorVO.setId(tempOrder.getId());
							errorVO.setOrderNumber(tempOrder.getOrderNumber());
							errorVO.setErrOrder(R.getResource("stock.approve.cyclecount.process.err"));
						}
					} else if (OrderType.DC.getValue().equals(tempOrder.getOrderType())) {
						StockStransFilter filter = new StockStransFilter();
						filter.setStockTransId(tempOrder.getId());
						filter.setFromOwnerId(tempOrder.getId());
						StockStransFilter filterTo = new StockStransFilter();
						filterTo.setStockTransId(tempOrder.getId());
						filterTo.setToOwnerID(tempOrder.getId());
						if (saleOrderMgr.checkStockTransCycleCount(filter) || saleOrderMgr.checkStockTransCycleCount(filterTo)) {
							errorVO.setId(tempOrder.getId());
							errorVO.setOrderNumber(tempOrder.getOrderNumber());
							errorVO.setErrOrder(R.getResource("stock.approve.cyclecount.process.err"));
						}
					}
					if (errorVO.getId() == null) {
						List<SaleOrderStockDetailVO> listDetail = saleOrderMgr.getListSaleOrderStockDetailUpdate(tempOrder.getId(), tempOrder.getOrderType());
						if (listDetail != null && listDetail.size() > 0) {
							/*
							 * chuan bi thong tin ghi log
							 */
							functionCode = LogInfoVO.FC_STOCK_ORDER_UPDATE;
							actionTypeCode = LogInfoVO.ACTION_UPDATE;
							saleOrderMgr.updateSaleOrderStock(listDetail, tempOrder, currentUser.getUserName(), getLogInfoVO());
						} else {
							errMsgInfo += StringUtil.isNullOrEmpty(errMsgInfo) ? tempOrder.getOrderNumber() :  ", " + tempOrder.getOrderNumber();
							errorVO.setId(tempOrder.getId());
							errorVO.setOrderNumber(tempOrder.getOrderNumber());
							errorVO.setStockQuantity(R.getResource("stock.approve.cyclecount.no.list.product"));
							errorVO.setErrOrder("X");
						}
					} else {
						errMsgInfo += StringUtil.isNullOrEmpty(errMsgInfo) ? tempOrder.getOrderNumber() :  ", " + tempOrder.getOrderNumber();
					}
				} catch (Exception e) {
					errMsgInfo += StringUtil.isNullOrEmpty(errMsgInfo) ? tempOrder.getOrderNumber() :  ", " + tempOrder.getOrderNumber();
					String s = e.getMessage();
					if (s.startsWith("KHONG_DU_SOLUONG_KHO_NV_VANSALE-") || s.startsWith("KHONG_DU_SOLUONG_KHO_NPP-")) {
						s = s.replaceFirst("KHONG_DU_SOLUONG_KHO_NV_VANSALE-", "");
						s = s.replaceFirst("KHONG_DU_SOLUONG_KHO_NPP-", "");
						productError += ", " + s;
						errorVO.setId(tempOrder.getId());
						errorVO.setOrderNumber(tempOrder.getOrderNumber());
						errorVO.setErrOrder("X");
					} else if (s.startsWith("DON_HANG_DA_XU_LY-")) {
						errorVO.setId(tempOrder.getId());
						errorVO.setOrderNumber(tempOrder.getOrderNumber());
						errorVO.setErrOrder(R.getResource("stock.approve.cyclecount.approve.status"));
					} else {
						LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ApproveUpdateOrderStockAction.approveStockOrder"), createLogErrorStandard(startLogDate));
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
						return JSON;
					}
				}
				if (!StringUtil.isNullOrEmpty(errorVO.getOrderNumber())) {
					errorVO.setStockQuantity(productError.replaceFirst(", ", "")); //  các sản phẩm lỗi
					errorVOs.add(errorVO);
				}
			}
			if (errorVOs.size() > 0) {
				result.put(ERROR, true);
				result.put("errMsg", errMsgInfo + " " + R.getResource("stock.approve.cyclecount.accept.err"));
				result.put("rows", errorVOs);
			} else {
				result.put(ERROR, false);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.ApproveUpdateOrderStockAction.approveStockOrder()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Huy don DC, DCT, DCG
	 *  
	 * @author lacnv1
	 * @since Mar 11, 2015
	 */
	public String cancelStockTrans() throws Exception {
		resetToken(result);
		try {
			result.put(ERROR, true);
			// kiem tra dang nhap
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			ShopToken shToken = currentUser.getShopRoot();
			if (shToken == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
			// Kiem tra tham so truyen len
			if (lstId == null || lstId.size() == 0) {
				return JSON;
			}
			stockMgr = (StockMgr)context.getBean("stockMgr");
			List<StockTrans> lst = stockMgr.getListStockTransById(lstId);
			if (lst == null || lst.size() == 0) {
				result.put("errMsg", R.getResource("sale.product.print.order.cancel.no.order"));
				return JSON;
			}
			StockTrans stockTrans = null;
			for (int i = 0, sz = lst.size(); i < sz; i++) {
				stockTrans = lst.get(i);
				if (stockTrans == null
						|| (!OrderType.DC.getValue().equals(stockTrans.getTransType())
								&& !OrderType.DCG.getValue().equals(stockTrans.getTransType())
								&& !OrderType.DCT.getValue().equals(stockTrans.getTransType()))) {
					result.put("errMsg", R.getResource("sale.order.stock.update.quantity.cancel.error.type.order"));
					return JSON;
				}
				List<Long> listShop = getListShopChildId();
				if (listShop != null && (stockTrans.getShop() == null || !listShop.contains(stockTrans.getShop().getId()))) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					return JSON;
				}
			}
			
			List<SearchSaleTransactionErrorVO> lstErr = saleOrderMgr.cancelStockTransUpdate(lstId, dayLock, getLogInfoVO());
			result.put("lstErr", lstErr);
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, "UpdateOrderStockAction.cancelStockTrans: " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * lay ds nv theo shop don vi
	 * @author trietptm
	 * @return
	 * @throws Exception
	 */
	public String loadStaffByShopCode() throws Exception {
		try{
			if (shopId != null){
				shop = shopMgr.getShopById(shopId);					
				StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
//				if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
//					filter.setIsLstChildStaffRoot(true);
//				} else {
//					filter.setIsLstChildStaffRoot(false);
//				}
				filter.setIsLstChildStaffRoot(false);
				filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
				filter.setStatus(ActiveType.RUNNING.getValue());
				filter.setLstShopId(Arrays.asList(shopId));
				filter.setIsGetChildShop(false);
				//Lay NVBH
				filter.setObjectType(1);
				filter.setStaffIdListStr(getStrListUserId());
				ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
				lstNVBHVo = objVO.getLstObject();
				
				//Lay NVGH
				filter.setObjectType(4);
				filter.setStaffIdListStr(null);
				objVO = staffMgr.searchListStaffVOByFilter(filter);
				lstNVGHVo = objVO.getLstObject();
				
				//cau hinh show gia hay khong
				List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
				if (lstPr != null && lstPr.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())) {//cho show gia
					result.put("isAllowShowPrice", true);
				} else {
					result.put("isAllowShowPrice", false);				
				}
				
				ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), "CONFIRM_ORDER_SEARCH");
				if (numberValueOrderParam != null) {
					numberValue = numberValueOrderParam.getCode();
				} else {
					numberValue = "0";
				}
			}
			result.put("numberValue", numberValue);
			result.put("lstNVBHVo", lstNVBHVo);
			result.put("lstNVGHVo", lstNVGHVo);

		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
		}

		return JSON;
	}
	
	/**
	 * GETTER SETTER
	 * @return
	 */
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getDeliveryStaffCode() {
		return deliveryStaffCode;
	}

	public void setDeliveryStaffCode(String deliveryStaffCode) {
		this.deliveryStaffCode = deliveryStaffCode;
	}

	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Integer getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(Integer orderSource) {
		this.orderSource = orderSource;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Date getNow() {
		return now;
	}

	public void setNow(Date now) {
		this.now = now;
	}	

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public List<ShopToken> getListShopToken() {
		return listShopToken;
	}

	public void setListShopToken(List<ShopToken> listShopToken) {
		this.listShopToken = listShopToken;
	}

	public List<SaleOrderStockVOTemp> getListTemp() {
		return listTemp;
	}

	public void setListTemp(List<SaleOrderStockVOTemp> listTemp) {
		this.listTemp = listTemp;
	}

	public String getNumberValue() {
		return numberValue;
	}

	public void setNumberValue(String numberValue) {
		this.numberValue = numberValue;
	}

	public Long getNumberValueOrder() {
		return numberValueOrder;
	}

	public void setNumberValueOrder(Long numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}

	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}	

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<TreeNode> getSearchUnitTree() {
		return searchUnitTree;
	}

	public void setSearchUnitTree(List<TreeNode> searchUnitTree) {
		this.searchUnitTree = searchUnitTree;
	}
}

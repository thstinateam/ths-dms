package ths.dms.web.action.stock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductStockTotal;
import ths.dms.core.entities.vo.SaleOrderStockDetailVO;
import ths.dms.core.entities.vo.SaleOrderStockVO;
import ths.dms.core.entities.vo.SaleOrderStockVOError;
import ths.dms.core.entities.vo.SaleOrderStockVOTemp;
import ths.dms.core.entities.vo.SearchSaleTransactionErrorVO;
import ths.dms.core.entities.vo.StaffVO;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * The Class UpdateStockAction.
 */
public class UpdateOrderStockAction extends AbstractAction{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	
	/** The mgr. */
	private StockMgr stockMgr; 
	
	/*private ProductMgr productMgr;*/

	private SaleOrderMgr saleOrderMgr;
	
	/** The String. */
	private String code;
	private String name;
	private String orderNumber;
	private String deliveryStaffCode;
	private String saleStaffCode;
	private String orderType;
	private Integer orderSource;
	private String fromDate;
	private String toDate;
	private String numberValue;
	private Long numberValueOrder;
	private Integer isValueOrder;
	private Long shopId;
	private Staff staff;
	private Shop shop;
	private Date now;
	
	/** The List */
	
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH
	private List<StaffVO> lstNVGHVo;//Danh sach NVGH
	private List<ShopToken> listShopToken;
	
	/** List lay du lieu */
	private List<SaleOrderStockVOTemp> listTemp;
	private List<Long> lstId;
	

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockMgr = (StockMgr)context.getBean("stockMgr");
//		productMgr = (ProductMgr)context.getBean("productMgr");
		saleOrderMgr = (SaleOrderMgr)context.getBean("saleOrderMgr");
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		if (currentUser != null) {
			currentUser = getCurrentUser();
		}
		return SUCCESS;
	}
	/**
	 * @author vuongmq
	 * @since 26 - August, 2014
	 * @return
	 * @description Lấy danh sách đơn hàng để cập nhật phải thu và kho (mặc định lấy đơn hàng IN)
	 */
	public String search() throws Exception {
		try {
			result.put("page", page);
		    result.put("max", max);
		    staff = getStaffByCurrentUser();
			//shop = staff.getShop();
		    if(!StringUtil.isNullOrEmpty(shopCode) ){
		    	shop = shopMgr.getShopByCode(shopCode);
		    } else {
		    	shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		    }
			if (shop != null) {
				checkCreateShopParamOrderSearch(numberValueOrder, shop.getId());
			}
			KPaging<SaleOrderStockVO> kPaging = new KPaging<SaleOrderStockVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			
			OrderType orderT = null; // gan mac dinh null la tat ca
			if(!StringUtil.isNullOrEmpty(orderType)) {
				if (orderType.equals(OrderType.IN.getValue()) || orderType.equals(OrderType.CM.getValue()) || orderType.equals(OrderType.SO.getValue()) || orderType.equals(OrderType.CO.getValue())
				|| orderType.equals(OrderType.GO.getValue()) || orderType.equals(OrderType.DP.getValue()) || orderType.equals(OrderType.DC.getValue()) || orderType.equals(OrderType.DCT.getValue()) || orderType.equals(OrderType.DCG.getValue()) ){
					orderT = OrderType.parseValue(orderType);
				} else {
					orderT = null;
				}
			}
			SaleOrderSource orderS = null; // gan mac dinh null la tat ca
			if(orderSource != null && orderSource != 0) {
				if(orderSource.equals(SaleOrderSource.TABLET.getValue()) || orderSource.equals(SaleOrderSource.WEB.getValue()) ){
					orderS = SaleOrderSource.parseValue(orderSource);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", "Lỗi Mã tạo trên Web hoặc Tablet tồn tại.");
					return JSON;
				}
			}
			Long staffId = null;
			if(!StringUtil.isNullOrEmpty(saleStaffCode)){
				Staff st = staffMgr.getStaffByCodeAndShopId(saleStaffCode, shop.getId());
				if(st != null) {
					staffId = st.getId();
				}
			}
			Long deliveryId = null;
			if(!StringUtil.isNullOrEmpty(deliveryStaffCode)){
				Staff st = staffMgr.getStaffByCodeAndShopId(deliveryStaffCode, shop.getId());
				if(st != null) {
					deliveryId = st.getId();
				}
			}
			// gan danh sach vao filter
			SoFilter soFilter = new SoFilter();
			soFilter.setShopId(shop.getId());
			soFilter.setOrderType(orderT);
			soFilter.setOrderNumber(orderNumber);
			soFilter.setOrderSource(orderS);
//			soFilter.setFromDate(startTemp);
//			soFilter.setToDate(endTemp);
			soFilter.setShortCode(code);
			soFilter.setCustomeName(name);
			soFilter.setStaffId(staffId);
			soFilter.setDeliveryId(deliveryId);
//			soFilter.setStaffCode(saleStaffCode);
//			soFilter.setDeliveryCode(deliveryStaffCode);
			if (numberValueOrder != null) {
				soFilter.setNumberValueOrder(BigDecimal.valueOf(numberValueOrder));
			}
			soFilter.setIsValueOrder(isValueOrder);
			soFilter.setStrListUserId(super.getStrListUserId());
			ObjectVO<SaleOrderStockVO> obSaleOrder = saleOrderMgr.getListSaleOrderStock(kPaging, soFilter);
			if(obSaleOrder != null && obSaleOrder.getLstObject().size() > 0) {
				result.put("total", obSaleOrder.getkPaging().getTotalRows());
			    result.put("rows", obSaleOrder.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<SaleOrderStockVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.UpdateOrderStockAction.search"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String approveStockOrder() throws Exception {
		resetToken(result);
		try {
			if (listTemp == null || listTemp.size() <= 0) {
				result.put(ERROR, true);
				result.put("errMsg", "Lỗi không có mã đơn hàng nào được chọn.");
				return JSON;
			}
			String errMsgInfo = "";
			List<SaleOrderStockVOError> errorVOs = new ArrayList<SaleOrderStockVOError>();
			for (int i = 0; i < listTemp.size(); i++) {
				SaleOrderStockVOTemp tempOrder = listTemp.get(i);
				SaleOrderStockVOError errorVO = new SaleOrderStockVOError();
				String productError = "";
				try {
					//boolean flag = UpdateSaleOrderStock(StockTotalVOFilter);
					List<SaleOrderStockDetailVO> listDetail = saleOrderMgr.getListSaleOrderStockDetail(tempOrder.getId(), tempOrder.getOrderType());
					if (listDetail.size() > 0) {
						/*
						 * chuan bi thong tin ghi log
						 */
						functionCode = LogInfoVO.FC_STOCK_ORDER_UPDATE;
						actionTypeCode = LogInfoVO.ACTION_UPDATE;
						saleOrderMgr.updateSaleOrderStock(listDetail, tempOrder, currentUser.getUserName(), getLogInfoVO());
						SaleOrder so = null;
						StockTrans st = null;
						List<ProductStockTotal> lstProductStock = null;
						List<ProductStockTotal> lstProductSalePlan = null;
						if (tempOrder.getOrderType().equals(OrderType.IN.getValue()) || tempOrder.getOrderType().equals(OrderType.CM.getValue()) || tempOrder.getOrderType().equals(OrderType.SO.getValue())
								|| tempOrder.getOrderType().equals(OrderType.CO.getValue())) {
							so = saleOrderMgr.getSaleOrderById(tempOrder.getId());
							lstProductStock = saleOrderMgr.lstProductStockTotal(so, null, 0);
							lstProductSalePlan = saleOrderMgr.lstProductSalePlan(so, null, 0);
						} else if (tempOrder.getOrderType().equals(OrderType.GO.getValue()) || tempOrder.getOrderType().equals(OrderType.DP.getValue()) || tempOrder.getOrderType().equals(OrderType.DC.getValue())
								|| tempOrder.getOrderType().equals(OrderType.DCT.getValue()) || tempOrder.getOrderType().equals(OrderType.DCG.getValue())) {
							st = stockMgr.getStockTransById(tempOrder.getId());
							lstProductStock = saleOrderMgr.lstProductStockTotal(null, st, 1);
							lstProductSalePlan = saleOrderMgr.lstProductSalePlan(null, st, 1);
						}
						StringBuilder errCode = new StringBuilder();
						if (lstProductSalePlan != null) {
							if (lstProductStock != null) {
								int size = lstProductSalePlan.size();
								for (ProductStockTotal productStock : lstProductStock) {
									for (int j = 0; j < size; j++) {
										ProductStockTotal productSalePlan = lstProductSalePlan.get(j);
										long productStockId = productStock.getProductId();
										long productSalePlanId = productSalePlan.getProductId();
										if (productStockId == productSalePlanId && productSalePlan.getNesStock() != null && productStock.getQuantity() != null) {
											long stockQuantity = productStock.getQuantity().longValue();
											long salePlanQuantity = productSalePlan.getNesStock();
											if (stockQuantity < salePlanQuantity) {
												errCode.append(productStock.getProductCode()).append(",");
											}
										}
									}
								}
							}
						}
						if (errCode.toString().length() > 0) {
							String code = "";
							if (errCode.toString().endsWith(",")) {
								code = errCode.substring(0, errCode.toString().length() - 2);
							}
							result.put("errCode", code);
							return JSON;
						}
					} else {
						errMsgInfo += tempOrder.getOrderNumber() + ", ";
						errorVO.setId(tempOrder.getId());
						errorVO.setOrderNumber(tempOrder.getOrderNumber());
						errorVO.setStockQuantity("Không có danh sách sản phẩm");
					}
				} catch (Exception e) {
					errMsgInfo += tempOrder.getOrderNumber() + ", ";
					String s = e.getMessage();
					if (s.startsWith("KHONG_DU_SOLUONG_KHO_NV_VANSALE-") || s.startsWith("KHONG_DU_SOLUONG_KHO_NPP-")) {
						s = s.replaceFirst("KHONG_DU_SOLUONG_KHO_NV_VANSALE-", "");
						s = s.replaceFirst("KHONG_DU_SOLUONG_KHO_NPP-", "");
						productError += ", " + s;
						errorVO.setId(tempOrder.getId());
						errorVO.setOrderNumber(tempOrder.getOrderNumber());
					} else {
						LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.UpdateOrderStockAction.approveStockOrder"), createLogErrorStandard(actionStartTime));
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
						if (e.getMessage() != null && e.getMessage().equals("DON_HANG_DA_XU_LY")) {
							result.put("errMsg", "DON_HANG_DA_XU_LY");
						}
						return JSON;
					}
				}
				if (!StringUtil.isNullOrEmpty(errorVO.getOrderNumber())) {
					errorVO.setStockQuantity(productError.replaceFirst(", ", "")); //  các sản phẩm lỗi
					errorVOs.add(errorVO);
				}
			}
			if (errorVOs.size() > 0) {
				result.put(ERROR, true);
				result.put("errMsg", errMsgInfo + " lỗi chưa xác nhận được đơn hàng.");
				result.put("rows", errorVOs);
			} else {
				result.put(ERROR, false);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.UpdateOrderStockAction.approveStockOrder"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Huy don DC, DCT, DCG
	 *  
	 * @author lacnv1
	 * @since Mar 11, 2015
	 */
	public String cancelStockTrans() throws Exception {
		resetToken(result);
		try {
			result.put(ERROR, true);
			// kiem tra dang nhap
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			shop = shopMgr.getShopByCode(shopCode);
			if (shop == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			// Kiem tra tham so truyen len
			if (lstId == null || lstId.size() == 0) {
				return JSON;
			}
			stockMgr = (StockMgr) context.getBean("stockMgr");
			List<StockTrans> lst = stockMgr.getListStockTransById(lstId);
			if (lst == null || lst.size() == 0) {
				result.put("errMsg", R.getResource("sale.product.print.order.cancel.no.order"));
				return JSON;
			}
			StockTrans stockTrans = null;
			for (int i = 0, sz = lst.size(); i < sz; i++) {
				stockTrans = lst.get(i);
				if (stockTrans == null || (!OrderType.DC.getValue().equals(stockTrans.getTransType()) && !OrderType.DCG.getValue().equals(stockTrans.getTransType()) && !OrderType.DCT.getValue().equals(stockTrans.getTransType()))) {
					result.put("errMsg", R.getResource("sale.order.stock.update.quantity.cancel.error.type.order"));
					return JSON;
				}
				if (stockTrans.getShop() == null || !shop.getId().equals(stockTrans.getShop().getId())) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					return JSON;
				}
			}
			List<SearchSaleTransactionErrorVO> lstErr = saleOrderMgr.cancelStockTrans(lstId, shop.getId(), dayLock, getLogInfoVO());
			result.put("lstErr", lstErr);
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.stock.UpdateOrderStockAction.cancelStockTrans"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * lay ds nv theo shop don vi
	 * @author trietptm
	 * @return
	 * @throws Exception
	 */
	public String loadStaffByShopCode() throws Exception {
		try{
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)){
				shop = shopMgr.getShopByCode(shopCode);
				shopId = shop.getId();			
				
				StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
				filter.setIsLstChildStaffRoot(false);
				filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
				filter.setStatus(ActiveType.RUNNING.getValue());
				filter.setLstShopId(Arrays.asList(shopId));
				filter.setIsGetChildShop(false);
				//Lay NVBH
				filter.setObjectType(StaffSpecificType.STAFF.getValue());
				filter.setUserId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				filter.setStaffIdListStr(getStrListUserId());
				ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
				lstNVBHVo = objVO.getLstObject();
				
				//Lay NVGH
				filter.setObjectType(StaffSpecificType.NVGH.getValue());
				filter.setUserId(null);
				filter.setRoleId(null);
				objVO = staffMgr.searchListStaffVOByFilter(filter);
				lstNVGHVo = objVO.getLstObject();
				
				//cau hinh show gia hay khong
				List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
				if (lstPr != null && lstPr.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())) {//cho show gia
					result.put("isAllowShowPrice", true);
				} else {
					result.put("isAllowShowPrice", false);				
				}
				
				ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), "CONFIRM_ORDER_SEARCH");
				if (numberValueOrderParam != null) {
					numberValue = numberValueOrderParam.getCode();
				} else {
					numberValue = "0";
				}
			}
			result.put("numberValue", numberValue);
			result.put("shopId", shopId);
			result.put("lstNVBHVo", lstNVBHVo);
			result.put("lstNVGHVo", lstNVGHVo);

		} catch(Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.stock.UpdateOrderStockAction.loadStaffByShopCode"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}

		return JSON;
	}
	
	/**
	 * GETTER SETTER
	 * @return
	 */
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getDeliveryStaffCode() {
		return deliveryStaffCode;
	}

	public void setDeliveryStaffCode(String deliveryStaffCode) {
		this.deliveryStaffCode = deliveryStaffCode;
	}

	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Integer getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(Integer orderSource) {
		this.orderSource = orderSource;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Date getNow() {
		return now;
	}

	public void setNow(Date now) {
		this.now = now;
	}	

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public List<ShopToken> getListShopToken() {
		return listShopToken;
	}

	public void setListShopToken(List<ShopToken> listShopToken) {
		this.listShopToken = listShopToken;
	}

	public List<SaleOrderStockVOTemp> getListTemp() {
		return listTemp;
	}

	public void setListTemp(List<SaleOrderStockVOTemp> listTemp) {
		this.listTemp = listTemp;
	}

	public String getNumberValue() {
		return numberValue;
	}

	public void setNumberValue(String numberValue) {
		this.numberValue = numberValue;
	}

	public Long getNumberValueOrder() {
		return numberValueOrder;
	}

	public void setNumberValueOrder(Long numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}

	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}	

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
}

/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */



package ths.dms.web.action.stock;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.TreeNode;
import ths.dms.web.business.common.TreeUtility;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.struts2.ServletActionContext;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.CycleCountMgr;
import ths.dms.core.business.CycleMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.CycleCountFilter;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.vo.CycleCountDiffVO;
import ths.dms.core.entities.vo.CycleCountSearchVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.memcached.MemcachedUtils;


/**
 * Duyet kiem kho
 * 
 * @author lacnv1
 * @since Nov 01, 2014
 */
public class ApproveCountingStockAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private CycleCountMgr cycleCountMgr;
	private StockMgr stockMgr;
	
	private int checkListEmpty;
	private Long wareHouseId;
	private Long cycleCountId;
	private Integer cycleCountStatus;
	private Integer startNumber;
	private Integer endNumber;
	private Integer cycleCountType;
	private Integer maxNumber;
	private Integer totalNumber;
	private String startDate;
	private String endDate;
	private String cycleCountCode;
	private String description;
	private String productCode;
	private String productName;
	private String lstProductString;
	private String reason;
	
	private List<String> lstCCMapProductNew;
	private List<String> lstCCMapProduct;
	private List<TreeNode> searchUnitTree;
	
	private CycleMgr cycleMgr;
	private Integer yearPeriod;	
	private Integer numPeriod;
	
	private CycleCount cycleCount;
	
	private List<CycleCountMapProduct> lstCycleCountMapProducts;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		cycleCountMgr = (CycleCountMgr) context.getBean("cycleCountMgr");
		cycleMgr = (CycleMgr) context.getBean("cycleMgr");
	}
	
	@Override
	public String execute() throws Exception {
		resetToken(result);
		try {
			if (currentUser == null) {
				return PAGE_NOT_PERMISSION;
			}
			ShopToken shT = currentUser.getShopRoot();
			if (shT == null) {
				return PAGE_NOT_PERMISSION;
			}
			shopId = shT.getShopId();
			shopCode = shT.getShopCode();
			shopName = shT.getShopName();
			lstYear = cycleMgr.getListYearOfAllCycle();
			Date curdate = commonMgr.getSysDate();
			yearPeriod = DateUtil.getYear(curdate);
			Cycle curCycle = cycleMgr.getCycleByDate(curdate);
			if (curCycle != null) {
				numPeriod = curCycle.getNum();
			} else {
				numPeriod = 1;
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "ApproveCountingStockAction - " + ex.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * tra ve cau truc cay don vi tim kiem
	 * 
	 * @author trietptm
	 * @return dinh dang du lieu tra ve
	 * @since 24/06/2015
	 */
	public String buildSearchUnitTree() {
		TreeUtility treeUtility = new TreeUtility(context);
		setSearchUnitTree(treeUtility.buildUnitTree(currentUser.getShopRoot().getShopId(), true, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId()));
		return JSON;
	}
	
	/**
	 * Tim kiem ds kiem ke
	 * 
	 * @author lacnv1
	 * @since Nov 03, 2014
	 */
	public String searchCycleCount() throws Exception {
		try {
			result.put("page", page);
			result.put("max", max);

			CycleCountFilter filter = new CycleCountFilter();
			KPaging<CycleCountSearchVO> kPaging = new KPaging<CycleCountSearchVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			filter.setCycleCountCode(cycleCountCode);
			if (cycleCountStatus != null && cycleCountStatus != -1) {
				filter.setStatus(CycleCountType.parseValue(cycleCountStatus));
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop sTmp = shopMgr.getShopByCode(shopCode);
				if (sTmp != null) {
					shopId = sTmp.getId();
				} else {
					shopId = 0L;
				}
			}
			filter.setShopId(shopId);
			Date startTemp = null;
			Date endTemp  = null;
			if (!StringUtil.isNullOrEmpty(startDate)) {
				startTemp = ths.dms.web.utils.DateUtil.parse(startDate, ConstantManager.FULL_DATE_FORMAT);
				filter.setFromDate(startTemp);
			}
			if (!StringUtil.isNullOrEmpty(endDate)) {
				endTemp = ths.dms.web.utils.DateUtil.parse(endDate, ConstantManager.FULL_DATE_FORMAT);
				filter.setToDate(endTemp);
			}
			filter.setDescription(description);
			filter.setWareHouseId(wareHouseId);
			filter.setStrListShopId(getStrListShopId());
			filter.setNumPeriod(numPeriod);
			filter.setYearPeriod(yearPeriod);
			ObjectVO<CycleCountSearchVO> lstCycleCount = cycleCountMgr.getListCycleCountSearchVO(filter);
			if (lstCycleCount != null) {
				result.put("total", lstCycleCount.getkPaging().getTotalRows());
				result.put("rows", lstCycleCount.getLstObject());
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "ApproveCountingStockAction.searchCycleCount - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Lay thong tin the kiem kho
	 * 
	 * @author lacnv1
	 * @since Nov 03, 2014
	 */
	public String search() throws Exception {
		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			ShopToken shT = currentUser.getShopRoot();
			if (shT == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			/*Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode) ){
		    	shop = shopMgr.getShopByCode(shopCode);
		    } else {			
		    	result.put(ERROR, true);
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return ERROR;
		    }
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return ERROR;
			}
			List<Long> lstShop = this.getListShopChildId();
	    	if (!lstShop.contains(shop.getId())) {
	    		result.put(ERROR, true);
	    		result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return ERROR;
	    	}
			
			checkListEmpty = 1;
			
			if (!StringUtil.isNullOrEmpty(cycleCountCode)) {
				//Shop sh = shopMgr.getShopById(shT.getShopId());
				cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shop.getId());
				
				if (cycleCount == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("cycle.count.not.exists"));
					return JSON;
				}
				if (CycleCountType.CANCELLED.equals(cycleCount.getStatus())) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("cycle.count.cancelled"));
					return JSON;
				}
				
				if (cycleCount.getShop().getId().equals(shop.getId())) {
					ObjectVO<CycleCountMapProduct> lstCCMapProduct = cycleCountMgr.getListCycleCountMapProduct(null,
									cycleCount.getId(), null, null, null, null, null, null, startNumber, endNumber);
					if (lstCCMapProduct != null) {
						lstCycleCountMapProducts = lstCCMapProduct.getLstObject();
					}
					if (lstCycleCountMapProducts == null || lstCycleCountMapProducts.size() <= 0) {
						checkListEmpty = 0;
					}
					cycleCountType = cycleCount.getStatus().getValue();
					maxNumber = cycleCountMgr.getMaxStockCardNumber(cycleCount.getId());
					totalNumber = cycleCountMgr.getTotalStockCardNumber(cycleCount.getId());
				} else {
					cycleCount = null;
				}
			}*/	
			
			cycleCount = cycleCountMgr.getCycleCountById(cycleCountId);
			if (cycleCount == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("cycle.count.not.exists"));
				return JSON;
			}
//			if (CycleCountType.CANCELLED.equals(cycleCount.getStatus())) {
//				result.put(ERROR, true);
//				result.put("errMsg", R.getResource("cycle.count.cancelled"));
//				return JSON;
//			}
			List<Long> lstShop = this.getListShopChildId();
	    	if (lstShop.contains(cycleCount.getShop().getId())) {
	    		ObjectVO<CycleCountMapProduct> lstCCMapProduct = null;
	    		if (CycleCountType.WAIT_APPROVED.equals(cycleCount.getStatus())) {
	    			lstCCMapProduct = cycleCountMgr.getListCycleCountMapProduct(null, cycleCount.getId(), null, null, null, null, null, null, true);
	    		} else {
	    			lstCCMapProduct = cycleCountMgr.getListCycleCountMapProduct(null, cycleCount.getId(), null, null, null, null, null, null, false);
	    		}
				if (lstCCMapProduct != null) {
					lstCycleCountMapProducts = lstCCMapProduct.getLstObject();
				}
				if (lstCycleCountMapProducts == null || lstCycleCountMapProducts.size() <= 0) {
					checkListEmpty = 0;
				} else {
					for (int i = 0, n = lstCycleCountMapProducts.size(); i < n; i++) {
						lstCycleCountMapProducts.get(i).setStockCardNumber(i + 1);
					}
				}
				cycleCountType = cycleCount.getStatus().getValue();
//				maxNumber = cycleCountMgr.getMaxStockCardNumber(cycleCount.getId());
//				totalNumber = cycleCountMgr.getTotalStockCardNumber(cycleCount.getId());
			} else {
				cycleCount = null;
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "ApproveCountingStockAction.search - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return LIST;
	}
	
	/**
	 * Tim kiem san pham
	 * 
	 * @author lacnv1
	 * @since Nov 03, 2014
	 */
	public String searchProduct() throws Exception {
	    try{
	    	result.put("page", page);
		    result.put("max", max);
		    
		    if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			ShopToken shT = currentUser.getShopRoot();
			if (shT == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
		    
			if (!StringUtil.isNullOrEmpty(cycleCountCode)) {
				KPaging<StockTotalVO> paging = new KPaging<StockTotalVO>();		
				paging.setPage(page-1);
				paging.setPageSize(max);
				cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shT.getShopId());
				
				if (cycleCount != null && CycleCountType.ONGOING.equals(cycleCount.getStatus()) && cycleCount.getShop().getId().equals(shT.getShopId())) {
					List<Long> lstProductId = new ArrayList<Long>();
					//Loai bo san pham moi duoc chon
					if (!StringUtil.isNullOrEmpty(lstProductString)) {
						String[] lstProduct = lstProductString.split(",");
						for (int i = 0, sz = lstProduct.length; i < sz; i++) {
							lstProductId.add(Long.valueOf(lstProduct[i]));
						}
					}
					
					StockTotalVOFilter filter = new StockTotalVOFilter();
					filter.setProductCode(productCode);
					filter.setProductName(productName);
					filter.setShopId(shT.getShopId());
					filter.setExceptProductId(lstProductId);
					filter.setWarehouseId(cycleCount.getWarehouse().getId());
					
					stockMgr = (StockMgr)context.getBean("stockMgr");
					ObjectVO<StockTotalVO> stockTotalVO = stockMgr.getListProductForCycleCount(paging, cycleCount.getId(), filter);
					if (stockTotalVO != null) {
						result.put("total", stockTotalVO.getkPaging().getTotalRows());
						result.put("rows", stockTotalVO.getLstObject());
					}
					/*
					 * comment code loi compile
					 * @modified by tuannd20
					 * @date 20/12/2014
					 */
					/*ObjectVO<StockTotalVO> stockTotalVO = stockMgr.getListStockTotalVO(kPaging, productCode,
							productName, null, shop.getId(), StockObjectType.SHOP, category, sub_category, fromAmnt, toAmnt,lstProductId,null);
					if(stockTotalVO!= null){
					    result.put("total", stockTotalVO.getkPaging().getTotalRows());
				    	result.put("rows", stockTotalVO.getLstObject());
					}*/    
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error ApproveCountingStockAction.searchProduct - "+ e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Duyet kiem kho
	 */
	public String approveStockCount() throws Exception {
		resetToken(result);
		errMsg ="";
		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			ShopToken shT = currentUser.getShopRoot();
			if (shT == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			cycleCount = cycleCountMgr.getCycleCountById(cycleCountId);
			if (cycleCount == null) {
				result.put(ERROR, true);
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.cycle.count.code");
				result.put("errMsg", errMsg);
				return ERROR;
			}
			//CHECK THE KIEM KHO CO DUNG DON VI VOI USER DANG NHAP KO
			List<Long> listShop = getListShopChildId();
			if (!listShop.contains(cycleCount.getShop().getId())) {
				result.put(ERROR, true);
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "stock.countinginput.not.exist.in.shop.cms");
				result.put("errMsg", errMsg);
				return ERROR;
			}
			//CHECK TRANG THAI CUA THE KIEM KE
			if (cycleCountType == CycleCountType.COMPLETED.getValue() || cycleCountType == CycleCountType.REJECTED.getValue()) {
				if (!CycleCountType.WAIT_APPROVED.equals(cycleCount.getStatus())) {
					result.put(ERROR, true);
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "stock.approve.cyclecount.not.approval");
					result.put("errMsg", errMsg);
					return ERROR;
				}
			}
			if (cycleCountType == CycleCountType.CANCELLED.getValue()) {
				if (CycleCountType.ONGOING.equals(cycleCount.getStatus()) || CycleCountType.COMPLETED.equals(cycleCount.getStatus())
						 || CycleCountType.CANCELLED.equals(cycleCount.getStatus())) {
					result.put(ERROR, true);
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "stock.approve.cyclecount.not.cancel");
					result.put("errMsg", errMsg);
					return ERROR;
				}
			}
			if (cycleCountType == CycleCountType.CANCELLED.getValue() || cycleCountType == CycleCountType.REJECTED.getValue()) {
				cycleCount.setReason(reason);
				cycleCount.setApprovedDate(commonMgr.getSysDate());
				cycleCount.setStatus(CycleCountType.parseValue(cycleCountType));
				cycleCountMgr.updateCycleCount(cycleCount);
				result.put(ERROR, false);
				return SUCCESS;
			}
			//CHECK COUNTDATE KHI CYCLECOUNT TYPE LA HOAN THANH
			if (cycleCountType == CycleCountType.COMPLETED.getValue()) {
				ObjectVO<CycleCountMapProduct> lstCCMapProductTmp = cycleCountMgr.getListCycleCountMapProductByCycleCountId(null, cycleCount.getId());
				if (lstCCMapProductTmp != null) {
					List<CycleCountMapProduct>  lstCCMapProductTest = lstCCMapProductTmp.getLstObject();
					if (lstCCMapProductTest.size() == 0) {
						result.put(ERROR, true);
						errMsg = R.getResource("stock.approve.cyclecount.not.product");
						result.put("errMsg", errMsg);
						return ERROR;
					}
					List<String> lstProductFail = cycleCountMgr.approveStockCount(cycleCount, lstCCMapProductTest, getLogInfoVO());
					if (lstProductFail.size() > 0) {
						StringBuilder productName = new StringBuilder();
						for (int i = 0, n = lstProductFail.size(); i < n; i++) {
							productName.append(", ").append(lstProductFail.get(i));
						}
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.counting.approve.fail", productName.toString().replaceFirst(", ", "")));
						return ERROR;
					}
				}
			}
		} catch (Exception ex) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(ex, "Error ApproveCountingStockAction.approveStockCount: " + ex.getMessage());
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}
	
	/**
	 * Xuat kiem ke ra file
	 * 
	 * @author lacnv1
	 * @since Nov 04, 2014
	 */
	public String exportExcel() throws Exception {
		InputStream inputStream = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			ShopToken shT = currentUser.getShopRoot();
			if (shT == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			Shop sh = shopMgr.getShopById(shT.getShopId());
			if (sh == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
//			if (StringUtil.isNullOrEmpty(cycleCountCode)) {
//				result.put(ERROR, true);
//				result.put("errMsg", R.getResource("stock.approve.cyclecount.not.empty"));
//				return ERROR;
//			}
			
			if (cycleCountId == null ) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("stock.approve.cyclecount.not.empty"));
				return ERROR;
			}
			
//			cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, sh.getId());
			cycleCount = cycleCountMgr.getCycleCountById(cycleCountId);
			if (cycleCount == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "stock.cyclecount.code"));
				return ERROR;
			}
			
			// check phan quyen
			List<Long> listShop = getListShopChildId();
			if (!listShop.contains(cycleCount.getShop().getId())) {
				result.put(ERROR, true);
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "stock.countinginput.not.exist.in.shop.cms");
				result.put("errMsg", errMsg);
				return ERROR;
			}
			
//			if (!cycleCount.getShop().getId().equals(sh.getId())) {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "stock.approve.permission.sameshop.error"));
//				return ERROR;
//			}
			
			List<CycleCountDiffVO> lst = cycleCountMgr.getListCycleCountDiff(cycleCount.getId());
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			int sumQuantityBeforeCount = 0;
			int sumQuantityBeforeCountPackage = 0;
			int sumQuantityCounted = 0;
			int sumQuantityCountedPackage = 0;
			int sumQuantityDiff = 0;
			int sumQuantityDiffPackage = 0;
			int convfact = 0;
			for (int i = 0, n = lst.size(); i < n; i++) {
				CycleCountDiffVO cycleCountDiffVO = lst.get(i);
				if (cycleCountDiffVO.getProductCode() == null) {
					cycleCountDiffVO.setQuantityBeforeCountConvfact(sumQuantityBeforeCountPackage + "/" + sumQuantityBeforeCount);
					cycleCountDiffVO.setQuantityCountedConvfact(sumQuantityCountedPackage + "/" + sumQuantityCounted);
					cycleCountDiffVO.setQuantityDiffConvfact(sumQuantityDiffPackage + "/" + sumQuantityDiff);
				} else {
					convfact = cycleCountDiffVO.getConvfact();
					sumQuantityBeforeCount += cycleCountDiffVO.getQuantityBeforeCount() % convfact;
					sumQuantityBeforeCountPackage += cycleCountDiffVO.getQuantityBeforeCount() / convfact;				
					cycleCountDiffVO.setQuantityBeforeCountConvfact(convertConvfact(cycleCountDiffVO.getQuantityBeforeCount(), convfact));
					
					sumQuantityCounted += cycleCountDiffVO.getQuantityCounted() % convfact;
					sumQuantityCountedPackage += cycleCountDiffVO.getQuantityCounted() / convfact;
					cycleCountDiffVO.setQuantityCountedConvfact(convertConvfact(cycleCountDiffVO.getQuantityCounted(), convfact));
					
					sumQuantityDiff += cycleCountDiffVO.getQuantityDiff() % convfact;
					sumQuantityDiffPackage += cycleCountDiffVO.getQuantityDiff() / convfact;
					cycleCountDiffVO.setQuantityDiffConvfact(convertConvfact(cycleCountDiffVO.getQuantityDiff(), convfact));
				}
			}
			
			String printDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			String startDate = DateUtil.toDateString(cycleCount.getStartDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			Map<String, Object> beans = new HashMap<String, Object>();
			
			beans.put("hasData", 1);
			beans.put("lst", lst);
			beans.put("shopName", cycleCount.getShop().getShopName());
			beans.put("address", cycleCount.getShop().getAddress());
			beans.put("printDate", printDate);
			beans.put("startDate", startDate);
			beans.put("cycleCountCode", cycleCount.getCycleCountCode());
			CycleCountDiffVO vo = lst.get(lst.size() - 1);
			Integer counted = vo.getQuantityCounted();
			Integer before = vo.getQuantityBeforeCount();
			double ration = counted * 1.0 / before;
			ration = Math.round(ration * 10000) / 10000.0;
			beans.put("ration", ration);
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathStock();
			String templateFileName = folder + ConstantManager.EXPORT_APPROVESTOCK_TEMPLATE2;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			
			String outputName = ConstantManager.EXPORT_APPROVESTOCK + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
			
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
//			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			LogUtility.logError(ex, "ApproveCountingStockAction.exportExcel() - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return JSON;
	}

	/** getters + setters */
	/** */
	public Integer getCycleCountStatus() {
		return cycleCountStatus;
	}

	public void setCycleCountStatus(Integer cycleCountStatus) {
		this.cycleCountStatus = cycleCountStatus;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCycleCountCode() {
		return cycleCountCode;
	}

	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(Integer startNumber) {
		this.startNumber = startNumber;
	}

	public Integer getEndNumber() {
		return endNumber;
	}

	public void setEndNumber(Integer endNumber) {
		this.endNumber = endNumber;
	}

	public CycleCount getCycleCount() {
		return cycleCount;
	}

	public void setCycleCount(CycleCount cycleCount) {
		this.cycleCount = cycleCount;
	}

	public List<CycleCountMapProduct> getLstCycleCountMapProducts() {
		return lstCycleCountMapProducts;
	}

	public void setLstCycleCountMapProducts(List<CycleCountMapProduct> lstCycleCountMapProducts) {
		this.lstCycleCountMapProducts = lstCycleCountMapProducts;
	}

	public int getCheckListEmpty() {
		return checkListEmpty;
	}

	public void setCheckListEmpty(int checkListEmpty) {
		this.checkListEmpty = checkListEmpty;
	}

	public Integer getCycleCountType() {
		return cycleCountType;
	}

	public void setCycleCountType(Integer cycleCountType) {
		this.cycleCountType = cycleCountType;
	}

	public Integer getMaxNumber() {
		return maxNumber;
	}

	public void setMaxNumber(Integer maxNumber) {
		this.maxNumber = maxNumber;
	}

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getLstProductString() {
		return lstProductString;
	}

	public void setLstProductString(String lstProductString) {
		this.lstProductString = lstProductString;
	}

	public List<String> getLstCCMapProduct() {
		return lstCCMapProduct;
	}

	public void setLstCCMapProduct(List<String> lstCCMapProduct) {
		this.lstCCMapProduct = lstCCMapProduct;
	}

	public List<String> getLstCCMapProductNew() {
		return lstCCMapProductNew;
	}

	public void setLstCCMapProductNew(List<String> lstCCMapProductNew) {
		this.lstCCMapProductNew = lstCCMapProductNew;
	}

	public Long getWareHouseId() {
		return wareHouseId;
	}

	public void setWareHouseId(Long wareHouseId) {
		this.wareHouseId = wareHouseId;
	}

	public List<TreeNode> getSearchUnitTree() {
		return searchUnitTree;
	}

	public void setSearchUnitTree(List<TreeNode> searchUnitTree) {
		this.searchUnitTree = searchUnitTree;
	}

	public Long getCycleCountId() {
		return cycleCountId;
	}

	public void setCycleCountId(Long cycleCountId) {
		this.cycleCountId = cycleCountId;
	}

	public Integer getYearPeriod() {
		return yearPeriod;
	}

	public void setYearPeriod(Integer yearPeriod) {
		this.yearPeriod = yearPeriod;
	}

	public Integer getNumPeriod() {
		return numPeriod;
	}

	public void setNumPeriod(Integer numPeriod) {
		this.numPeriod = numPeriod;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
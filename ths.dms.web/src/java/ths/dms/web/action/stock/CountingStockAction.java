package ths.dms.web.action.stock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CycleCountMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CycleCountSearchFilter;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.CycleType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.CycleCountVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.WarehouseVO;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CountingStockAction.
 */
public class CountingStockAction extends AbstractAction{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
		
	/** The Stock Manager mgr. */
	private StockManagerMgr stockManagerMgr;
	
	/** The stock mgr. */
	private StockMgr stockMgr;
	
	/** The shop mgr. */
	private ShopMgr shopMgr;
	
	/** The cycle count mgr. */
	private CycleCountMgr cycleCountMgr;
	
	/** The staff. */
	private Staff staff;
	
	/** The cycle count code. */
	private String cycleCountCode;
	
	/** The description. */
	private String description;
	
	/** The cycle count status. */
	private Integer cycleCountStatus;
	
	/** The shop code. */
	private String shopCode;
	
	/** The shop id. */
	private Long shopId;
	
	/** The cycle count. */
	private CycleCount cycleCount;
	
	/** The cycle count id. */
	private Long cycleCountId;
	
	/** The shop. @author tientv */
	private Shop shop;
	
	private String startDate;	
	
	private String fromCreated;
	
	private String toCreated;
	
	private String endDate;
	
	private Integer isChanged;
	
	private Boolean isSearch;
	
	private Date lockDay;
	
	/** List Warehouse */
	private List<WarehouseVO> lstWarehouseVO;
	
	/** The warehouse id. */
	private Long wareHouseId;
	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**  @author tientv */
	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    stockManagerMgr = (StockManagerMgr)context.getBean("stockManagerMgr");
	    stockMgr = (StockMgr)context.getBean("stockMgr");
	    shopMgr = (ShopMgr)context.getBean("shopMgr");
	    cycleCountMgr = (CycleCountMgr)context.getBean("cycleCountMgr");
//	    StaffMgr staffMgr = (StaffMgr)context.getBean("staffMgr");
//	    try {
//			if(currentUser != null && currentUser.getUserName() != null){
//				staff = staffMgr.getStaffByCode(currentUser.getUserName());
//				if(staff != null && staff.getShop() != null){
//					shop = staff.getShop();	
//					shopCode = shop.getShopCode();
//				}
//			}
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
	}
	
	/**
	 *
	 * @author tientv
	 * @return the string
	 */
	@Override
	public String execute() {	
	    resetToken(result);
	    return SUCCESS;
	}
	
	/**
	 * khi nguoi dung thay doi shop
	 * @author trietptm
	 * @return
	 * @throws Exception
	 */
	public String changeShop() throws Exception {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					lockDay = shopLockMgr.getNextLockedDay(shop.getId());
					//Lay kho tu Shop root
					BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
					filter.setLongG(shop.getId());
					filter.setStatus(ActiveType.RUNNING.getValue());
					filter.setIsExistStock(true);
					// lay du lieu phan quyen duoi DB
					filter.setStaffRootId(currentUser.getUserId());
					filter.setRoleId(currentUser.getRoleToken().getRoleId());
					filter.setShopRootId(currentUser.getShopRoot().getShopId());
					filter.setStrShopId(getStrListShopId());
					lstWarehouseVO = stockManagerMgr.getListWarehouseVOByFilter(filter).getLstObject();
				}
			}
			if (lstWarehouseVO == null || lstWarehouseVO.isEmpty()) {
				lstWarehouseVO = new ArrayList<WarehouseVO>();
			} else {
				for (int i = 0, n = lstWarehouseVO.size(); i < n; i++) {
					WarehouseVO vo = lstWarehouseVO.get(i);
					if (vo != null) {
						vo.setWarehouseName(vo.getWarehouseCode() + " - " + vo.getWarehouseName());
					}
				}
			}
			WarehouseVO voAll = new WarehouseVO();
			if (isSearch) {
				voAll.setWarehouseName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.tat.ca"));
			} else {
				voAll.setWarehouseName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.cyclecount.choice.stock"));
			}
			lstWarehouseVO.add(0, voAll);
			result.put("lstWarehouseVO", lstWarehouseVO);

			if (lockDay == null) {
				lockDay = commonMgr.getSysDate();
			}
			result.put("lockDay", DateUtil.toDateString(lockDay, "dd/MM/yyyy"));
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.stock.CountingStockAction.changeShop()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 */
	public String search(){	    
	    result.put("page", page);
	    result.put("max", max);	    
	    try{
	    	result.put("total", 0);
			result.put("rows", new ArrayList<CycleCountVO>());
			KPaging<CycleCountVO> kPaging = new KPaging<CycleCountVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			CycleCountType type = null;
			if(cycleCountStatus != null && cycleCountStatus != -1){
				type = CycleCountType.parseValue(cycleCountStatus);
			}
			
			if(!StringUtil.isNullOrEmpty(shopCode) ){
		    	shop = shopMgr.getShopByCode(shopCode);
		    } else {
		    	result.put(ERROR, true);				
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return JSON;
		    }
			if(shop==null){
				result.put(ERROR, true);				
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return JSON;
			}	
//			if(wareHouseId==null){
//				result.put(ERROR, true);				
//				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
//						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.warehouse.is.null")));	
//				return JSON;
//			}	
			Date dateStart = null,dateEnd = null;
			Date dateFrom = null,dateTo = null;			
			if(!StringUtil.isNullOrEmpty(startDate)){
				dateStart = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);			
			}	
			if(!StringUtil.isNullOrEmpty(endDate)){
				dateEnd = DateUtil.parse(endDate, DateUtil.DATE_FORMAT_DDMMYYYY);			
			}
			if(!StringUtil.isNullOrEmpty(fromCreated)){
				dateFrom = DateUtil.parse(fromCreated, DateUtil.DATE_FORMAT_DDMMYYYY);			
			}
			if(!StringUtil.isNullOrEmpty(toCreated)){
				dateTo = DateUtil.parse(toCreated, DateUtil.DATE_FORMAT_DDMMYYYY);			
			}
			ObjectVO<CycleCountVO> lstCycleCount = cycleCountMgr.getListCycleCountVO(kPaging,cycleCountCode, type,description, shop.getId(), 
					dateStart, dateEnd, dateFrom, dateTo, wareHouseId, getStrListShopId());
			if(lstCycleCount!= null){				
				result.put("total", lstCycleCount.getkPaging().getTotalRows());
				result.put("rows", lstCycleCount.getLstObject());		
			}	
				
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}	
	
	
	/**
	 * change 
	 *
	 * @return the string
	 * @author tientv
	 */
	public String getChanged(){
		try{		
//			if (currentUser.getShopRoot() != null) {
//				//Lay kho tu Shop root
//				BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
//				filter.setLongG(currentUser.getShopRoot().getShopId());
//				filter.setStatus(ActiveType.RUNNING.getValue());
//				lstWarehouseVO = stockManagerMgr.getListWarehouseVOByFilter(filter).getLstObject();
//				if (lstWarehouseVO == null || lstWarehouseVO.isEmpty()) {
//					lstWarehouseVO = new ArrayList<WarehouseVO>();
//				}
//			}
			isChanged = 1;
			if(cycleCountId==null || cycleCountId==0){
				cycleCountId = 0L;
				return SUCCESS;
			}
			cycleCount = cycleCountMgr.getCycleCountById(cycleCountId);
			List<Long> lstShop = this.getListShopChildId();
			if(cycleCount==null || cycleCount.getShop() == null || !lstShop.contains(cycleCount.getShop().getId())){
				cycleCount = null;
				cycleCountId = 0L;
				return SUCCESS;
			}
			shopCode = cycleCount.getShop().getShopCode();
			cycleCountId = cycleCount.getId();
			if(!cycleCount.getStatus().equals(CycleCountType.ONGOING)
					|| cycleCountMgr.checkIfCycleCountMapProductExist(cycleCountId, null, null)){
				isChanged = 0;				
			}
//			if(isChanged==1){
//				wareHouseId = cycleCount.getWarehouse().getId();
//			}
			wareHouseId = cycleCount.getWarehouse().getId();
		}catch (Exception e) {
	    	LogUtility.logError(e, "KiemKeKho.getChanged" + e.getMessage());
	    }
		return SUCCESS;
	}	
	
	/**
	 * Update.
	 *
	 * @return the string
	 * @author tientv
	 */
	public String update(){
		isChanged = 1;
	    resetToken(result);	        
	    try{
	    	Staff staff = getStaffByCurrentUser();
	    	if(staff==null){
	    		return JSON;
	    	}
	    	cycleCount = cycleCountMgr.getCycleCountById(cycleCountId);
	    	if(cycleCount == null){
	    		result.put(ERROR, true);				
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.couting.code")));	
				return JSON;
	    	}
	    	List<Long> lstShop = this.getListShopChildId();
			if(cycleCount.getShop() == null || !lstShop.contains(cycleCount.getShop().getId())){
				result.put(ERROR, true);				
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.not.exist.in.shop.cms"));	
				return JSON;
			}
	    	if(StringUtil.isNullOrEmpty(description)){
				result.put(ERROR, true);				
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.couting.description")));	
				return JSON;
			}
	    	if(StringUtil.isNullOrEmpty(startDate)){
				result.put(ERROR, true);				
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.couting.start.date")));	
				return JSON;
			}
			try{
				Date date = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);				
				cycleCount.setStartDate(date);				
			}catch(Exception ex){				
				result.put(ERROR, true);				
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.couting.start.date")));	
				return JSON;
			}			
//			if(wareHouseId==null){
//				result.put(ERROR, true);				
//				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
//						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.warehouse.is.null")));	
//				return JSON;
//			}	
//			Warehouse warehouse = stockManagerMgr.getWarehouseById(wareHouseId);
//			if(warehouse != null && warehouse.getStatus().equals(ActiveType.RUNNING)){
//				cycleCount.setWarehouse(warehouse);
//			}	
//	    	 ObjectVO<CycleCountMapProduct>  objectVO = cycleCountMgr.getListCycleCountMapProductByCycleCountId(null, cycleCount.getId());
//	    	 if(objectVO!=null){
//	    		 if(objectVO.getLstObject().size()>0){
//	    			result.put(ERROR, true);				
//	 				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_STOCK_CC,cycleCount.getCycleCountCode()));	
//	 				return JSON;
//	    		 }
//	    	 }
	    	cycleCount.setUpdateUser(currentUser.getUserName());	    	
			cycleCount.setDescription(description);			
			cycleCountMgr.updateCycleCount(cycleCount);
			if(!cycleCount.getStatus().equals(CycleCountType.ONGOING) || cycleCountMgr.checkIfCycleCountMapProductExist(cycleCountId, null, null)){
				isChanged = 0;
			}
			result.put("isChanged", isChanged);
			result.put(ERROR, false);	
	    }catch(Exception ex){
	    	result.put(ERROR, true);
	    	result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
	    	LogUtility.logError(ex, ex.getMessage());
	    }
	    return JSON;
	}
	
	/**
	 * Save.
	 *
	 * @return the string
	 * @author tientv
	 */
	public String save(){	
		isChanged = 1;
	    resetToken(result);	 
	    try{
	    	result.put(ERROR, true);
	    	Staff staff = getStaffByCurrentUser();
			if (staff == null) {
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
		    	shop = shopMgr.getShopByCode(shopCode);
		    } else {			
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return JSON;
		    }
			if (shop == null) {					
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return JSON;
			}
	    	List<Long> lstShop = this.getListShopChildId();
	    	if (!lstShop.contains(shop.getId())) {
	    		result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));	
				return JSON;
	    	}
			if (wareHouseId == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.warehouse.is.null")));
				return JSON;
			}	
	    	cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shop.getId());
	    	if(cycleCount!=null){	    				
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.cycle.count.code")));	
				return JSON;
	    	}
	    	
	    	CycleCountSearchFilter filter = new CycleCountSearchFilter();
	    	List<CycleCountType> statusLst = new ArrayList<CycleCountType>();
	    	statusLst.add(CycleCountType.ONGOING);
	    	statusLst.add(CycleCountType.WAIT_APPROVED);
	    	filter.setStatusLst(statusLst);
	    	filter.setShopId(shop.getId());
	    	filter.setWareHouseId(wareHouseId);
	    	filter.setStrListShopId(getStrListShopId());
			ObjectVO<CycleCount> lstCycleCount = cycleCountMgr.getListCycleCount(filter);
			if (lstCycleCount != null && lstCycleCount.getLstObject().size() >= 1) {				
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.cycle.count.ongoing"));
				return JSON;
			}
			cycleCount = new CycleCount();
			cycleCount.setStatus(CycleCountType.ONGOING);
			if (StringUtil.isNullOrEmpty(cycleCountCode)) {								
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.cycle.count.code")));	
				return JSON;
			}
			cycleCount.setCycleCountCode(cycleCountCode);
			if (StringUtil.isNullOrEmpty(description)) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.cycle.count.description")));
				return JSON;
			}			
			if (StringUtil.isNullOrEmpty(startDate)) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.couting.start.date")));
				return JSON;
			}
			try {
				Date date = DateUtil.parse(startDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				cycleCount.setStartDate(date);
			} catch (Exception ex) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.couting.start.date")));
				return JSON;
			}
			//Bo sung kiem tra ATTT XSS
			errMsg = "";
			if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(description)) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, null);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
			
			Warehouse warehouse = stockManagerMgr.getWarehouseById(wareHouseId);
			if (warehouse != null && warehouse.getStatus().equals(ActiveType.RUNNING)) {
				cycleCount.setWarehouse(warehouse);
			}
			lockDay = shopLockMgr.getNextLockedDay(shop.getId());
			cycleCount.setDescription(description);
			cycleCount.setCreateUser(currentUser.getUserName());
			cycleCount.setShop(shop);
			cycleCount.setCycleType(CycleType.ALL);
			cycleCount.setCreateDate(lockDay);
			cycleCount = cycleCountMgr.createCycleCount(cycleCount);
			cycleCountId = cycleCount.getId();
			if (!cycleCount.getStatus().equals(CycleCountType.ONGOING) || cycleCountMgr.checkIfCycleCountMapProductExist(cycleCountId, null, null)) {
				isChanged = 0;
			}
			cycleCountMgr.createListCycleCountMapProduct(cycleCount, null, true, getLogInfoVO());
			result.put("isChanged", isChanged);
			result.put("cycleCount", cycleCount);
			result.put(ERROR, false);
		} catch (Exception ex) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
			LogUtility.logError(ex, ex.getMessage());
		}
	    return JSON;
	}
	
	/**
	 * Removes the.
	 * @author tientv
	 * @return the string
	 */
	public String remove() {
		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				cycleCount = cycleCountMgr.getCycleCountById(cycleCountId);
				if (cycleCount != null) {
					if (CycleCountType.CANCELLED.equals(cycleCount.getStatus()) || CycleCountType.REJECTED.equals(cycleCount.getStatus())) {
						cycleCountMgr.deleteCycleCount(cycleCount);
					}
					error = false;
				}

			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
			}
		}
		result.put(ERROR, error);
		return JSON;
	}
	
	/**
	 * Removes the.
	 * @author tientv
	 * @return the string
	 */
	public String viewDetail(){
			    
		try{			
			if(cycleCountId==null && cycleCountId==0){
				result.put(ERROR, true);
		    	result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,
		    			Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.couting.code")));	 
		    	return JSON;
			}
			cycleCount = cycleCountMgr.getCycleCountById(cycleCountId);
			if(cycleCount==null){
				result.put(ERROR, true);
		    	result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,
		    			Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.couting.code")));	 
		    	return JSON;
			}
			result.put("cycleCountStatus", cycleCount.getStatus().getValue());
			result.put("description", cycleCount.getDescription());
			result.put("cycleCountCode", cycleCount.getCycleCountCode());
			result.put("startDate", DateUtil.toDateString(cycleCount.getStartDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
			result.put("dateCreated", DateUtil.toDateString(cycleCount.getCreateDate(), DateUtil.DATE_FORMAT_DDMMYYYY));	
			result.put(ERROR, false);
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
		    result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
		    result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Gets the cycle count id.
	 *
	 * @return the cycle count id
	 */
	public Long getCycleCountId() {
		return cycleCountId;
	}

	/**
	 * Sets the cycle count id.
	 *
	 * @param cycleCountId the new cycle count id
	 */
	public void setCycleCountId(Long cycleCountId) {
		this.cycleCountId = cycleCountId;
	}

	/**
	 * Gets the cycle count.
	 *
	 * @return the cycle count
	 */
	public CycleCount getCycleCount() {
		return cycleCount;
	}

	/**
	 * Sets the cycle count.
	 *
	 * @param cycleCount the new cycle count
	 */
	public void setCycleCount(CycleCount cycleCount) {
		this.cycleCount = cycleCount;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the cycle count code.
	 *
	 * @return the cycle count code
	 */
	public String getCycleCountCode() {
		return cycleCountCode;
	}

	/**
	 * Sets the cycle count code.
	 *
	 * @param cycleCountCode the new cycle count code
	 */
	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the cycle count status.
	 *
	 * @return the cycle count status
	 */
	public Integer getCycleCountStatus() {
		return cycleCountStatus;
	}

	/**
	 * Sets the cycle count status.
	 *
	 * @param cycleCountStatus the new cycle count status
	 */
	public void setCycleCountStatus(Integer cycleCountStatus) {
		this.cycleCountStatus = cycleCountStatus;
	}

	/**
	 * Gets the shop code.
	 *
	 * @return the shop code
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * Sets the shop code.
	 *
	 * @param shopCode the new shop code
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public StockMgr getStockMgr() {
		return stockMgr;
	}

	public void setStockMgr(StockMgr stockMgr) {
		this.stockMgr = stockMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public CycleCountMgr getCycleCountMgr() {
		return cycleCountMgr;
	}

	public void setCycleCountMgr(CycleCountMgr cycleCountMgr) {
		this.cycleCountMgr = cycleCountMgr;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getFromCreated() {
		return fromCreated;
	}

	public void setFromCreated(String fromCreated) {
		this.fromCreated = fromCreated;
	}

	public String getToCreated() {
		return toCreated;
	}

	public void setToCreated(String toCreated) {
		this.toCreated = toCreated;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getIsChanged() {
		return isChanged;
	}

	public void setIsChanged(Integer isChanged) {
		this.isChanged = isChanged;
	}

	public StockManagerMgr getStockManagerMgr() {
		return stockManagerMgr;
	}

	public void setStockManagerMgr(StockManagerMgr stockManagerMgr) {
		this.stockManagerMgr = stockManagerMgr;
	}

	public List<WarehouseVO> getLstWarehouseVO() {
		return lstWarehouseVO;
	}

	public void setLstWarehouseVO(List<WarehouseVO> lstWarehouseVO) {
		this.lstWarehouseVO = lstWarehouseVO;
	}

	public Long getWareHouseId() {
		return wareHouseId;
	}

	public void setWareHouseId(Long wareHouseId) {
		this.wareHouseId = wareHouseId;
	}

	public Boolean getIsSearch() {
		return isSearch;
	}

	public void setIsSearch(Boolean isSearch) {
		this.isSearch = isSearch;
	}

	public Date getLockDay() {
		return lockDay;
	}

	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}
	
	
}

package ths.dms.web.action.stock;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CarMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ReportManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductAmountVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransDetailVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.dms.core.report.ShopReportMgr;

/**
 * The Class IssuedStockAction.
 */
public class IssuedStockAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	
	private ShopReportMgr shopReportMgr;
	/** The car mgr. */
	private CarMgr carMgr;
	
	/** The stock mgr. */
	private StockMgr stockMgr; 
	
	/** The product mgr. */
	private ProductMgr productMgr;
	
	/** The staff mgr. */
	private StaffMgr staffMgr;
	
	/** The shop mgr. */
	private ShopMgr shopMgr;
	
	/** The staff. */
	private Staff staff;
	
	/** The code. */
	private String code;
	
	/** The issued date. */
	private String issuedDate;
	
	/** The issued staff. */
	private String issuedStaff;
	
	/** The shop code. */
	private String shopCode;
	
	private String shopName;
	
	private String isSysdate;
	
	private String isApplicationDate;
	
	private Integer isOutputStock;
	
	private String transacDate;
	private String transacContent;
	private String transacName;
	
	private String location;

	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTransacDate() {
		return transacDate;
	}
	public void setTransacDate(String transacDate) {
		this.transacDate = transacDate;
	}
	public String getTransacContent() {
		return transacContent;
	}
	public void setTransacContent(String transacContent) {
		this.transacContent = transacContent;
	}
	public String getTransacName() {
		return transacName;
	}
	public void setTransacName(String transacName) {
		this.transacName = transacName;
	}

	
	
	public Integer getIsOutputStock() {
		return isOutputStock;
	}
	public void setIsOutputStock(Integer isOutputStock) {
		this.isOutputStock = isOutputStock;
	}
	public String getIsSysdate() {
		return isSysdate;
	}
	public void setIsSysdate(String isSysdate) {
		this.isSysdate = isSysdate;
	}
	public String getIsApplicationDate() {
		return isApplicationDate;
	}
	public void setIsApplicationDate(String isApplicationDate) {
		this.isApplicationDate = isApplicationDate;
	}

	/** The staff code. */
	private String staffCode;
	
	/** The car id. */
	private Long carId;
	
	/** The shop id. */
	private Long shopId;
	
	/** The lst car. */
	private List<Car> lstCar;
	
	/** The product code. */
	private String productCode;
	
	/** The product name. */
	private String productName;
	
	/** The vansale code. */
	private String vansaleCode;
	
	/** The amount. */
	private Float amount;
	
	/** The total money. */
	private BigDecimal totalMoney;
	
	/** The lst product_ id. */
	private List<Long> lstProduct_Id;
	
	/** The lst product_ qty. */
	private List<String> lstProduct_Qty;
	
	/** The lst product lot. */
	private List<String> lstProductLot;
	
	/** The staff id. */
	private Long staffId;
	
	/** The output name. */
	private String outputName;	
	
	/** The shop. */
	private Shop  shop;	
	
	/** The product id. */
	private Long productId;
	
	private StockTotalVOFilter stockTotalVoFilter;
	
	

	/** The ap param mgr. */
	ApParamMgr apParamMgr;
	
	private List<Staff> listNVBH;
	
	private List<Staff> lstStaff;
	
	private Boolean isF9;
	
	private Boolean byShop;		
		
	private String exceptProductIdStr;
	
	private String staffTypeCode;
	
	private HashMap<String, Object> parametersReport;
	
	private String stockTransDate;
	
	private List<StockTransLotVO> stockTransLotVOs;
	
	private List<StockTransLotVO> listData;
	
	private ReportManagerMgr reportManagerMgr;
	private Long stockTransId;
	
	
	public Long getStockTransId() {
		return stockTransId;
	}
	public void setStockTransId(Long stockTransId) {
		this.stockTransId = stockTransId;
	}
	public StockTotalVOFilter getStockTotalVoFilter() {
		return stockTotalVoFilter;
	}
	public void setStockTotalVoFilter(StockTotalVOFilter stockTotalVoFilter) {
		this.stockTotalVoFilter = stockTotalVoFilter;
	}
	public List<StockTransLotVO> getListData() {
		return listData;
	}
	public void setListData(List<StockTransLotVO> listData) {
		this.listData = listData;
	}
	public List<StockTransLotVO> getStockTransLotVOs() {
		return stockTransLotVOs;
	}
	public void setStockTransLotVOs(List<StockTransLotVO> stockTransLotVOs) {
		this.stockTransLotVOs = stockTransLotVOs;
	}
	public String getStockTransDate() {
		return stockTransDate;
	}
	public void setStockTransDate(String stockTransDate) {
		this.stockTransDate = stockTransDate;
	}
	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}
	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}
	public String getExceptProductIdStr() {
		return exceptProductIdStr;
	}
	public void setExceptProductIdStr(String exceptProductIdStr) {
		this.exceptProductIdStr = exceptProductIdStr;
	}
	
	public Boolean getByShop() {
		return byShop;
	}
	public void setByShop(Boolean byShop) {
		this.byShop = byShop;
	}
	public List<Staff> getLstStaff() {
		return lstStaff;
	}	
	public String getStaffTypeCode() {
		return staffTypeCode;
	}
	public Boolean getIsF9() {
		return isF9;
	}
	public void setIsF9(Boolean isF9) {
		this.isF9 = isF9;
	}
	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
	public void setLstStaff(List<Staff> lstStaff) {
		this.lstStaff = lstStaff;
	}
	public List<Staff> getListNVBH() {
		return listNVBH;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}
	/**
	 * Gets the lst product lot.
	 *
	 * @return the lst product lot
	 */
	public List<String> getLstProductLot() {
		return lstProductLot;
	}

	/**
	 * Sets the lst product lot.
	 *
	 * @param lstProductLot the new lst product lot
	 */
	public void setLstProductLot(List<String> lstProductLot) {
		this.lstProductLot = lstProductLot;
	}



	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}



	/**
	 * Sets the product id.
	 *
	 * @param productId the new product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}



	/**
	 * Gets the output name.
	 * 
	 * @return the output name
	 */
	public String getOutputName() {
		return outputName;
	}
	
	

	/**
	 * Gets the shop.
	 *
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}



	/**
	 * Sets the shop.
	 *
	 * @param shop the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}



	/**
	 * Sets the output name.
	 * 
	 * @param outputName
	 *            the new output name
	 */
	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	/**
	 * Gets the staff id.
	 * 
	 * @return the staff id
	 */
	public Long getStaffId() {
		return staffId;
	}

	/**
	 * Sets the staff id.
	 * 
	 * @param staffId
	 *            the new staff id
	 */
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	/**
	 * Gets the staff.
	 * 
	 * @return the staff
	 */
	public Staff getStaff() {
		return staff;
	}

	/**
	 * Sets the staff.
	 * 
	 * @param staff
	 *            the new staff
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	/**
	 * Gets the shop mgr.
	 * 
	 * @return the shop mgr
	 */
	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	/**
	 * Sets the shop mgr.
	 * 
	 * @param shopMgr
	 *            the new shop mgr
	 */
	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	/**
	 * Gets the lst product_ qty.
	 * 
	 * @return the lst product_ qty
	 */
	public List<String> getLstProduct_Qty() {
		return lstProduct_Qty;
	}

	/**
	 * Sets the lst product_ qty.
	 * 
	 * @param lstProduct_Qty
	 *            the new lst product_ qty
	 */
	public void setLstProduct_Qty(List<String> lstProduct_Qty) {
		this.lstProduct_Qty = lstProduct_Qty;
	}

	/**
	 * Gets the lst product_ id.
	 * 
	 * @return the lst product_ id
	 */
	public List<Long> getLstProduct_Id() {
		return lstProduct_Id;
	}

	/**
	 * Sets the lst product_ id.
	 * 
	 * @param lstProduct_Id
	 *            the new lst product_ id
	 */
	public void setLstProduct_Id(List<Long> lstProduct_Id) {
		this.lstProduct_Id = lstProduct_Id;
	}

	/**
	 * Gets the amount.
	 * 
	 * @return the amount
	 */
	public Float getAmount() {
		return amount;
	}

	/**
	 * Gets the total money.
	 * 
	 * @return the total money
	 */
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	/**
	 * Sets the total money.
	 * 
	 * @param totalMoney
	 *            the new total money
	 */
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	/**
	 * Sets the amount.
	 * 
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(Float amount) {
		this.amount = amount;
	}

	/**
	 * Gets the vansale code.
	 * 
	 * @return the vansale code
	 */
	public String getVansaleCode() {
		return vansaleCode;
	}

	/**
	 * Sets the vansale code.
	 * 
	 * @param vansaleCode
	 *            the new vansale code
	 */
	public void setVansaleCode(String vansaleCode) {
		this.vansaleCode = vansaleCode;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the issued date.
	 *
	 * @return the issuedDate
	 */
	public String getIssuedDate() {
		return issuedDate;
	}

	/**
	 * Sets the issued date.
	 *
	 * @param issuedDate the issuedDate to set
	 */
	public void setIssuedDate(String issuedDate) {
		this.issuedDate = issuedDate;
	}

	/**
	 * Gets the issued staff.
	 *
	 * @return the issuedStaff
	 */
	public String getIssuedStaff() {
		return issuedStaff;
	}

	/**
	 * Sets the issued staff.
	 *
	 * @param issuedStaff the issuedStaff to set
	 */
	public void setIssuedStaff(String issuedStaff) {
		this.issuedStaff = issuedStaff;
	}

	/**
	 * Gets the shop code.
	 *
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * Sets the shop code.
	 *
	 * @param shopCode the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * Gets the staff code.
	 *
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}

	/**
	 * Sets the staff code.
	 *
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	/**
	 * Gets the car id.
	 *
	 * @return the carId
	 */
	public Long getCarId() {
		return carId;
	}

	/**
	 * Sets the car id.
	 *
	 * @param carId the carId to set
	 */
	public void setCarId(Long carId) {
		this.carId = carId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shopId
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the shopId to set
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the lst car.
	 *
	 * @return the lstCar
	 */
	public List<Car> getLstCar() {
		return lstCar;
	}

	/**
	 * Sets the lst car.
	 *
	 * @param lstCar the lstCar to set
	 */
	public void setLstCar(List<Car> lstCar) {
		this.lstCar = lstCar;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 *
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the product name.
	 *
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the product name.
	 *
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	
	/**
	 * Prepare.
	 *
	 * @author tientv
	 */
	public void prepare() {
		try {
			super.prepare();
		} catch (Exception e) {
			LogUtility.logError(e,e.getMessage());
		}
		carMgr = (CarMgr)context.getBean("carMgr");
		stockMgr = (StockMgr)context.getBean("stockMgr"); 
		productMgr = (ProductMgr)context.getBean("productMgr");
		staffMgr = (StaffMgr)context.getBean("staffMgr");
		shopMgr = (ShopMgr)context.getBean("shopMgr");
		apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
		shopReportMgr = (ShopReportMgr)context.getBean("shopReportMgr");
		reportManagerMgr = (ReportManagerMgr)context.getBean("reportManagerMgr");
		staff = getStaffByCurrentUser();
		if(staff!=null){
			shop = staff.getShop();	
			shopCode = shop.getShopCode();
			shopName = shop.getShopName();
			shopId = shop.getId();
			staffCode = staff.getStaffCode();
			staffId = staff.getId();
		}			
	}

	/**
	 * Execute.
	 *
	 * @return the string
	 * @author tientv
	 */
	@Override
	public String execute() {
		resetToken(result);
		ObjectVO<Car> carVO = new ObjectVO<Car>();
		try {
			if(isShopLocked()){
				return SHOP_LOCK;
			}
			
			if(staff==null){
				return PAGE_NOT_PERMISSION;
			}
			errMsg = "";
			if(shop==null){
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.issued.out.shop.null"));
				return SUCCESS;
			}
			carVO = carMgr.getListCar(null, shop.getId(), null, null, null, null, null, null, ActiveType.RUNNING, false);
			code = stockMgr.generateStockTransCodeVansale(getCurrentShop().getId(),"DB");
			if(currentUser!= null){
				issuedStaff = currentUser.getFullName();
			}
			if(carVO!= null){
				lstCar = carVO.getLstObject();
			}else{
				lstCar = new ArrayList<Car>();
			}
			ObjectVO<Staff> staffVos  = null;
			List<Integer> lstStaffType = new ArrayList<Integer>();
			lstStaffType.add(StaffObjectType.NVBH.getValue());
			lstStaffType.add(StaffObjectType.NVVS.getValue());				
			StaffFilter filter = new StaffFilter();
			filter.setStatus(ActiveType.RUNNING);
			filter.setShopCode(shopCode);			
			filter.setChannelTypeType(ChannelTypeType.STAFF);
			filter.setLstChannelObjectType(lstStaffType);
			staffVos = staffMgr.getListStaffEx2(filter);
			if(staffVos != null){
				lstStaff = staffVos.getLstObject();
			}else {
				lstStaff = new ArrayList<Staff>();
			}
			// add by lacnv1 - 10.03.2014
			Date date = shopLockMgr.getApplicationDate(shop.getId());
			if (date != null) {
				issuedDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				issuedDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			/** Tham so cau hinh cho phep xuat tu dong hay bang tay */
			setSeparationLot(separationLotAuto(ConstantManager.ISSUED_STOCK_AP_PARAM_CODE));
			/*
			//Lay ngay hien tai
			isSysdate = DateUtil.convertDateByString(new Date(), DateUtil.DATETIME_FORMAT_STR);
			//Lay ngay chot
			issuedDate = DateUtil.convertDateByString(shopLockMgr.getApplicationDate(getCurrentShop().getId()), DateUtil.DATETIME_FORMAT_STR);
			*/
			isOutputStock = 0;
			/*if(DateUtil.compareDateWithoutDay(new Date(), shopLockMgr.getApplicationDate(getCurrentShop().getId())) != 0){
				isOutputStock = 1;
			}*/
			if(DateUtil.compareTwoDate(new Date(), shopLockMgr.getApplicationDate(getCurrentShop().getId())) != 0){
				isOutputStock = 1;
			}
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		
		return SUCCESS;
	}
	
	/**
	 * Search product.
	 * 
	 * @return the string
	 * @author khanhnl,tientv
	 * @since Aug 28, 2012
	 *  
	 */
	/**
	 * Update: Tối ưu câu truy vấn
	 * @author hunglm16
	 * @since 16-10-2013
	 */
	public String searchProduct(){	    
	    result.put("page", page);
	    result.put("max", max);	    
	    try{	    	
	    	result.put("rows", new ArrayList<StockTotalVO>());
	    	result.put("stock_total", new ArrayList<StockTotal>());
			KPaging<StockTotalVO> kPaging = null;
			ObjectVO<StockTotalVO> stockTotalVO = new ObjectVO<StockTotalVO>();
			
			/** Lay ton kho nha phan phoi */
			stockTotalVoFilter = new StockTotalVOFilter();
			if(byShop!=null && byShop==true){
//				ObjectVO<StockTotal> vos = stockMgr.getListStockTotal(null, null, null, null,shopId, StockObjectType.SHOP, null, null, null, null, null);
				stockTotalVoFilter.setOwnerId(shopId);
				stockTotalVoFilter.setOwnerType(StockObjectType.SHOP);
				ObjectVO<StockTotal> vos = new ObjectVO<StockTotal>();
				vos = stockMgr.getListStockTotalNew(null, stockTotalVoFilter);
				if(vos!= null){
					result.put("stock_total", vos.getLstObject());
				}
			}else {
				/** Danh sach san pham thuoc nganh hang nhan vien co the ban. */
				List<Long> exceptionProductId = new ArrayList<Long>();
				if(isF9!=null && isF9){
					kPaging = new KPaging<StockTotalVO>();		
					kPaging.setPage(page-1);
					kPaging.setPageSize(max);
					if(!StringUtil.isNullOrEmpty(exceptProductIdStr)){
						String[] exceptProductIdArr = exceptProductIdStr.split(",");
						for(String s:exceptProductIdArr){
							exceptionProductId.add(Long.valueOf(s));
						}
					}
				}				
				Staff staffSaler = staffMgr.getStaffByCode(vansaleCode);
				if(staffSaler==null || !staffSaler.getStatus().equals(ActiveType.RUNNING)){
					return JSON;
				}
				StockLock st = stockMgr.getStockLock(shopId, staffSaler.getId());
				if(st != null && ActiveType.STOPPED.getValue().equals(st.getVanLock().getValue())){
					result.put(ERROR, true);
					result.put("errMsg", "Không thể thực hiện xuất kho nhân viên do kho nhân viên chưa mở");
					return JSON;
				}
//				stockTotalVO = stockMgr.getListStockTotalVO(kPaging, productCode, productName, null, shopId, StockObjectType.SHOP, null, null,1, null,exceptionProductId,staffSaler.getId());
				stockTotalVoFilter.setProductCode(productCode);
				stockTotalVoFilter.setProductName(productName);
				stockTotalVoFilter.setOwnerId(shopId);
				stockTotalVoFilter.setOwnerType(StockObjectType.SHOP);
				stockTotalVoFilter.setFromQuantity(1);
				stockTotalVoFilter.setExceptProductId(exceptionProductId);
				stockTotalVoFilter.setStaffSalerId(staffSaler.getId());
				stockTotalVoFilter.setShopId(shopId);
				stockTotalVO = stockMgr.getListStockTotalVONew(kPaging, stockTotalVoFilter);
				
				if(stockTotalVO!= null){
					if(isF9!=null && isF9){
						result.put("total", stockTotalVO.getkPaging().getTotalRows());
					}			    
			    	result.put("rows", stockTotalVO.getLstObject());
				}   
			}
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}
	
	/**
	 * Product lots.
	 *
	 * @return the string
	 * @author tientv
	 * @since
	 */
	
	public String productLots() {
		result = new HashMap<String, Object>();		
		try {
			if(staff==null){
				return JSON;
			}
			Product product = productMgr.getProductById(productId);
			if(staff==null || productId==null || product==null ){
				return JSON;
			}
			result.put("product", product);
			Integer NUM_DAY_LOT_EXPIRE  = 0;				
//			ApParam ap = apParamMgr.getApParamByCode(ConstantManager.AP_PARAM_CODE_NUM_DAY_LOT_EXPIRE, ApParamType.SALE_ORDER_APPROVED);
			ApParam ap = apParamMgr.getApParamByCode(ConstantManager.AP_PARAM_CODE_NUM_DAY_LOT_EXPIRE, ApParamType.SALE_ORDER_APPLOT);
			if(ap!=null){
				NUM_DAY_LOT_EXPIRE = Integer.valueOf(ap.getValue());
			}
			ObjectVO<ProductLot> vos = stockMgr.getListProductLotWithProductID(null, staff.getShop().getId(), StockObjectType.SHOP, 0, ActiveType.RUNNING, productId);
			
			List<ProductLot> list = new ArrayList<ProductLot>();
			if(vos!=null && vos.getLstObject().size()>0){
				if(NUM_DAY_LOT_EXPIRE>=0){
					Date moveDate = DateUtil.moveDate(DateUtil.now(), NUM_DAY_LOT_EXPIRE, DateUtil.DATE_MONTH);
					for(ProductLot pl:vos.getLstObject()){						
						if(pl.getExpiryDate()!=null && DateUtil.compareDateWithoutTime(pl.getExpiryDate(), moveDate)>=0){
							list.add(pl);
						}
					}
					if(list.size()==0){
						result.put(ERROR, true);
						return JSON;
					}	
				}else {
					list = vos.getLstObject();
				}							
			}
			result.put(ERROR, false);
			result.put("lots", list);			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}		
		return JSON;
	}
	
	/**
	 * XUAT KHO NHAN VIEN 
	 * 
	 * @return the string
	 * @author khanhnl,tientv
	 * @since Aug 29, 2012
	 * 
	 * @author hunglm16
	 * @since JUNE 03,2014
	 * @description Update Dac ta yeu cau Tuan 2, Thang 6/2014 
	 */
	public String stockOutVanSale(){	
		
		Product product = null;
		StockTrans stockTrans = null;
		Staff newStaff = null;
		Boolean isOK = true;
		try {
			//Kiem tra voi ngay chot
			if(DateUtil.compareTwoDate(new Date(), shopLockMgr.getApplicationDate(getCurrentShop().getId())) != 0){
				result.put(ERROR, true);
				result.put("errMsg", "Không thể thực hiện xuất kho nhân viên do ngày chốt khác ngày hiện tại");
				return JSON;
			}
			
			if(staff==null){
				result.put(ERROR, true);
				result.put("errMsg", "Không thể thực hiện xuất kho nhân viên do không xác định được nhân viên");
				return JSON;
			}else if(shop==null){
				shop = getCurrentShop();
			}
			newStaff = staffMgr.getStaffByCode(vansaleCode);
			if(newStaff == null){
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
				return JSON;
			}else if(!shopMgr.checkAncestor(shopCode,newStaff.getShop().getShopCode())){
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
				return JSON;
			}else if(!newStaff.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.issued.out.staff.code")));
				return JSON;
			}
			else{
				StockLock stockLock = stockMgr.getStockLock(getCurrentShop().getId(), newStaff.getId());
				if(stockLock!=null && !ActiveType.RUNNING.getValue().equals(stockLock.getVanLock().getValue())){
					result.put(ERROR, true);
					result.put("errMsg", "Không thể thực hiện xuất kho nhân viên do kho nhân viên chưa mở");
					return JSON;
				}else if(stockLock==null){
					//Them moi Stock_Lock - vablock = 1
					stockLock = new StockLock();
    				stockLock.setStaff(newStaff);
    				stockLock.setShop(shop);
    				stockLock.setVanLock(ActiveType.RUNNING);
    				stockLock.setCreateDate(new Date());
    				stockLock.setCreateUser(getCurrentUser().getUserName());
    				stockMgr.insertStockLock(stockLock);
				}
				
				stockTrans = stockMgr.getStockTransByCode(code);
				if(stockTrans != null){
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.exist",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.issued.out.vansale.code.vote")));
					return JSON;
				}
				else{
					if(lstProduct_Id.size() > 0 && lstProduct_Qty.size() > 0){
						List<ProductAmountVO> productAmountVOs = new ArrayList<ProductAmountVO>();
						for (int i=0;i<lstProduct_Id.size();i++) {
							ProductAmountVO productAmountVO = new ProductAmountVO();
							product = productMgr.getProductById(lstProduct_Id.get(i));
							productAmountVO.setOwnerId(shopId);
							productAmountVO.setOwnerType(StockObjectType.SHOP);
							productAmountVO.setPakage(getQuantity(lstProduct_Qty.get(i), product.getConvfact() , "Package"));
							productAmountVO.setProductId(lstProduct_Id.get(i));
							productAmountVO.setUnit(getQuantity(lstProduct_Qty.get(i), product.getConvfact() , "Unit"));
							productAmountVOs.add(productAmountVO);
						}						
						List<Boolean> booleans = stockMgr.checkIfProductHasEnough(productAmountVOs);
						for (int j=0;j<booleans.size();j++) {
							if(!booleans.get(j)){
								product = productMgr.getProductById(lstProduct_Id.get(j));
								result.put(ERROR, true);
								result.put("errMsg", product.getProductName() + Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.issued.out.vansale.out.export"));
								isOK = booleans.get(j);
								break;
							}
						}
						if(isOK){							
							stockTrans = new StockTrans();
							Car car = carMgr.getCarById(carId);
							if(car==null){
								return JSON;
							}
							stockTrans.setCar(car);
							stockTrans.setCreateUser(staff.getStaffCode());
							/*stockTrans.setFromOwnerId(shopId.intValue());
							stockTrans.setFromOwnerType(StockObjectType.SHOP);*/
							stockTrans.setShop(shopMgr.getShopById(shopId));
							stockTrans.setStaffId(staff);
							stockTrans.setStockTransCode(code.toUpperCase());
							
//							Date stockTransDate = new Date();
							Date stockTransDate = shopLockMgr.getApplicationDate(shop.getId());
							if(stockTransDate==null){
//								stockTransDate = DateUtil.parse(issuedDate,DateUtil.DATE_FORMAT_STR);
								stockTransDate = new Date();
							}							
							stockTrans.setStockTransDate(stockTransDate);
							/*stockTrans.setToOwnerId(newStaff.getId());
							stockTrans.setToOwnerType(StockObjectType.STAFF);*/
							//update JUNE 3,2014
							stockTrans.setTransType("DB");
							
							List<StockTransDetailVO> lstStockTransDetailVO = new ArrayList<StockTransDetailVO>();
							Integer sys_separation_auto = separationLotAuto(ConstantManager.ISSUED_STOCK_AP_PARAM_CODE);
							for(int k=0;k<lstProduct_Id.size();k++){
								product = productMgr.getProductById(lstProduct_Id.get(k));									
								StockTransDetail stockTransDetail = new StockTransDetail();
								stockTransDetail.setCreateUser(staff.getStaffCode());
								stockTransDetail.setProduct(product);							
								stockTransDetail.setStockTransDate(stockTransDate);
								stockTransDetail.setQuantity(Integer.valueOf(lstProduct_Qty.get(k)));
								
								/** Kiem tra so luong lo het han can xuat. */	
								if(product.getCheckLot()==1){
									Integer sumLotQuantity = 0;									
									List<ProductLot> vos = productMgr.getProductLotByProductAndOwner(product.getId(),shopId,StockObjectType.SHOP);									
									if(vos!=null && vos.size()>0){
										for(ProductLot pl:vos){
											sumLotQuantity += pl.getQuantity();
										}
									}									
									if(stockTransDetail.getQuantity()>sumLotQuantity){
										result.put(ERROR, true);
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_ISS_STOCK_OUT, product.getProductCode()));
										return JSON;
									}									
								}
								StockTransDetailVO stockTransDetailVO = new StockTransDetailVO();
								stockTransDetailVO.setStockTransDetail(stockTransDetail);
								if(sys_separation_auto==0 && product.getCheckLot()==1){
									List<String> listHandleLot = new ArrayList<String>();
									List<Integer> listHandleQuantity = new ArrayList<Integer>();
									String[] productLots = lstProductLot.get(k).split(";");	
									for(String productLot:productLots){
										String lot = productLot.split(":")[0];
										Integer quantityLot = Integer.valueOf(productLot.split(":")[1]);
										if(quantityLot>0){
											listHandleLot.add(lot);
											listHandleQuantity.add(quantityLot);
										}										
									}
									stockTransDetailVO.setListHandleLot(listHandleLot);
									stockTransDetailVO.setListHandleQuantity(listHandleQuantity);									
								}
								lstStockTransDetailVO.add(stockTransDetailVO);								
							}
							Long stockTransIdNew = stockMgr.createListStockTransDetail(stockTrans,lstStockTransDetailVO,sys_separation_auto,shopId);
							if(stockTransIdNew == null || stockTransIdNew <= Long.valueOf(0)){
								result.put(ERROR, true);
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"system.error"));
							}else{
								result.put("stockTransCode", stockTransIdNew);
								result.put(ERROR, false);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"system.error"));
			LogUtility.logError(e, e.getMessage());
		}		
		
		return JSON;
	}
	
	/**
	 * IN PHIEU XUAT KHO CHO NHAN VIEN
	 * 
	 * @return the string
	 * @author khanhnl,tientv,hungtt
	 * @since Sep 5, 2012
	 */
	public String exportStockOutVanSale(){
		
		parametersReport = new HashMap<String, Object>();
		formatType = FileExtension.PDF.getName();
		// get data for parameters

		parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
		parametersReport.put("fDate", DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY));

		try{
			if(shopId!= null){
				Shop shop = shopMgr.getShopById(shopId);
				if(shop!=null){
					parametersReport.put("shopName", shop.getShopName());
					if(!StringUtil.isNullOrEmpty(shop.getAddress())){
						parametersReport.put("address", shop.getAddress());
					} else {
						parametersReport.put("address", "");
					}
				}
			}

			// get data for datasource
			StockTrans stockTrans = null;
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
			parametersReport.put("fDate",printDate);
			parametersReport.put("stockTransDate", stockTransDate);
			Staff newStaff = null;	
//			Car car = null;//car ko thay trong mau phieu in,nen khoi put.
			if(staff==null){
				return JSON;
			}
//			car = carMgr.getCarById(carId);
//			if(car != null){
//				parametersReport.put("car", car.getCarNumber());
//			}
			newStaff = staffMgr.getStaffByCode(vansaleCode);
			if(newStaff != null){
//				parametersReport.put("issuedStaff",codeNameDisplay(newStaff.getStaffCode(),newStaff.getStaffName()));
				parametersReport.put("staffCode",codeNameDisplay(newStaff.getStaffName(),newStaff.getStaffCode()));
			}
//			parametersReport.put("staffCode",codeNameDisplay(staff.getStaffCode(),staff.getStaffName()));
			stockTrans = stockMgr.getStockTransByCode(code);
			if(stockTrans != null){
				parametersReport.put("code", code);
				parametersReport.put("totalWeight", stockTrans.getTotalWeight());
				parametersReport.put("totalAmount", stockTrans.getTotalAmount());
				parametersReport.put("stockTransCode", stockTrans.getStockTransCode());
			}
			parametersReport.put("sub_lot", ShopReportTemplate.XKNV_SUB_LOT.getTemplatePath(false, FileExtension.JASPER));
			parametersReport.put("sub_qty", ShopReportTemplate.XKNV_SUB_QTY.getTemplatePath(false, FileExtension.JASPER));		
			parametersReport.put("sub_price", ShopReportTemplate.XKNV_SUB_PRICE.getTemplatePath(false, FileExtension.JASPER));
			parametersReport.put("sub_total", ShopReportTemplate.XKNV_SUB_TOTAL.getTemplatePath(false, FileExtension.JASPER));
			stockTransDate = DateUtil.toDateString(stockTrans.getStockTransDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			if(stockTrans != null){
				ObjectVO<StockTransLotVO> objectVO = stockMgr.getListStockTransLotVO(null, stockTrans.getId());
				if(objectVO != null){
					listData = objectVO.getLstObject();
					if(listData != null && listData.size() >0){
						int n = listData.size();
						for(int i=0;i<n;i++){
							listData.get(i).setBigUnit(listData.get(i).getThung());
							listData.get(i).setSmallUnit(listData.get(i).getLe());
						}
					}
				}				
			}
			listData.add(0,new StockTransLotVO());
			//stockTransLotVOs = shopReportMgr.getRptExImStockOfStaff(shopId, listStaffId, fDate, tDate);
			//lstBKPTHNVGHData = shopReportMgr.getListRptReturnSaleOrderByDelivery(shopId,OrderType.CM,fDate,tDate,staffId);				
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, listData);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, parametersReport);
			FileExtension ext = FileExtension.parseValue(formatType);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
			String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource,ShopReportTemplate.XKNV);	
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			return JSON;
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * in phieu xuat kho kiem van chuyen noi bo
	 * @author vuonghn
	 */
	public String print_PXNKVCNB(){
		parametersReport = new HashMap<String, Object>();
		try{
			Shop curShop = getCurrentShop();
			Shop shop;
			if(curShop==null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return JSON;
			}
			if(shopId != null){
				shop = shopMgr.getShopById(shopId);
				if(shop!=null)	{
					if(!shopMgr.checkAncestor(curShop.getShopCode(), shop.getShopCode())) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_IN_SHOP, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree")));
						return JSON;
					}
				}else{
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_NPP));
					return JSON;
				}
			}else{
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,false,"catalog.unit.tree"));
				return JSON;
			}
			
			ReportUrl reportUrl = reportManagerMgr.getReportFileName(shopId, StringUtil.TEMPLATE_XNB.trim());		
			if(reportUrl == null || StringUtil.isNullOrEmpty(reportUrl.getUrl())){
				//Chua co mau hoa don GTGT
				result.put(ERROR, true);
				result.put("errMsg", "Không có template XNB trong trong dữ liệu để xuất báo cáo.");
				return JSON;
			}
			String nameFile = reportUrl.getUrl();
			int idx = nameFile.lastIndexOf(".jrxml");
			nameFile = nameFile.substring(0, idx);
			nameFile = nameFile + ".jasper";
			String templatePath = Configuration.getReportTemplatePath() + nameFile;
			File fileReport = new File(templatePath);
			if(!fileReport.exists()){
				result.put(ERROR, true);
				result.put("errMsg", "Không có template XNB trong hệ thống để xuất báo cáo.");
				return JSON;
			}
			
			List<RptPXKKVCNB_Stock> lstDataStock = new ArrayList<RptPXKKVCNB_Stock>();
			Date dCurrent = DateUtil.now();
			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			parametersReport.put("cDate", DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR));
			parametersReport.put("pDay", dCurrent.getDate());
			parametersReport.put("pMonth", dCurrent.getMonth() + 1);
			parametersReport.put("pYear", dCurrent.getYear() + 1900);
			
			parametersReport.put("noi_dung", this.transacContent);
			parametersReport.put("lenh_dieu_dong", this.transacName);
			//parametersReport.put("don_vi", this.transacCode);
			Integer transacDay = 0;
			Integer transacMonth = 0;
			Integer transacYear = 0;
			if(!StringUtil.isNullOrEmpty(issuedDate)){
				transacDay = DateUtil.getDay(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				transacMonth = DateUtil.getMonth(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				transacYear = DateUtil.getYear(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				parametersReport.put("don_vi_ngay", transacDay);
				parametersReport.put("don_vi_thang", transacMonth);
				parametersReport.put("don_vi_nam", transacYear);
			}else {
				parametersReport.put("don_vi_ngay", dCurrent.getDate());
				parametersReport.put("don_vi_thang", dCurrent.getMonth() + 1);
				parametersReport.put("don_vi_nam", dCurrent.getYear() + 1900);
			}
			RptPXKKVCNB_Stock dataStock = stockMgr.getPXKKVCNB(shopId, staffCode, stockTransId, transacName, transacContent);
			lstDataStock.add(dataStock);
			if(lstDataStock.size() > 0 ){
				//FileExtension ext = FileExtension.PDF;
				JRDataSource dataSource = new JRBeanCollectionDataSource(lstDataStock);
				String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE))
				.append("_").append(StringUtil.TEMPLATE_XNB.toLowerCase().trim()+"-"+shop.getShopCode().toLowerCase()+".pdf")).toString());
				
				String outputPath = Configuration.getReportTemplatePath() + outputFile;
				String outputDowload = Configuration.getReportRealPath() + outputFile;
				JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parametersReport, dataSource);
				JRAbstractExporter exporter = new JRPdfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
				exporter.exportReport();
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputDowload);
			}else{
				result.put(ERROR, false);
				result.put(ERROR, false);
				result.put("hasData", false);
			}
		}catch(Exception ex){
			LogUtility.logError(ex, "UpdateStockAction.in_pxkkvcnb : " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
}

package ths.dms.web.action.stock;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CycleCountMapBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.struts2.ServletActionContext;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CycleCountMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.CycleType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.exceptions.BusinessException;

public class CategoryStockAction extends AbstractAction{
	private static final long serialVersionUID = -2128120975032582873L;
	private CycleCountMgr cycleCountMgr;
	private StaffMgr staffMgr;
	private Staff staff = null;
	private Shop shop = null;
	private String startDate;
	private Integer cycleType;
	private String shopCode;
	private Long shopId;
	private String cycleCountCode;		
	private CycleCount cycleCount;
	private Integer sortBy;
	private Integer firstNumber;	
	private List<ProductInfo> lstCategoryType;
	private List<ProductInfo> lstSubCategoryType;
	private List<ProductInfo> lstSubCategoryCat;
	private List<String> lstProduct;
	ObjectVO<CycleCountMapProduct> lstCCMapProduct;		
	private String productCode;		
	private String productName;
	private Long category;
	private Long sub_category;
	private Integer fromAmnt;
	private Integer toAmnt;
	private Long cycleMapProductId;
	private String cycleCountDes;
	private List<Integer> lstIsDelete;
	private List<Integer> lstStockCardNumber;
	private Integer lastNumber;
	private Integer status;
	private Integer noPaging;
	private String listProductEx;

	private HashMap<String, Object> parametersReport;

	private Long idParentCat;

	private ProductMgr productMgr;
	
	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}
	
	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	public List<ProductInfo> getLstSubCategoryCat() {
		return lstSubCategoryCat;
	}
	public void setLstSubCategoryCat(List<ProductInfo> lstSubCategoryCat) {
		this.lstSubCategoryCat = lstSubCategoryCat;
	}
	public Long getIdParentCat() {
		return idParentCat;
	}
	public void setIdParentCat(Long idParentCat) {
		this.idParentCat = idParentCat;
	}
	/**
	 * Sets the cycle count des.
	 *
	 * @param 
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		cycleCountMgr = (CycleCountMgr) context.getBean("cycleCountMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");	
		productMgr = (ProductMgr) context.getBean("productMgr");
		try {
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();	
					shopId = shop.getId();
					shopCode = shop.getShopCode();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}
	
	
	
	/**
	 * Sets the cycle count des.
	 *
	 * @param 
	 * @author tientv
	 */
	@Override
	public String execute() {
		resetToken(result);
		try {
			lstCategoryType = new ArrayList<ProductInfo>();
			ProductInfoMgr productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
			ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfoStock(null, null, null, null, ActiveType.RUNNING, ProductType.CAT);
			if(tmp != null){
				lstCategoryType = tmp.getLstObject(); 
			}			
			tmp = productInfoMgr.getListProductInfoStock(null, null, null, null,  ActiveType.RUNNING, ProductType.SUB_CAT);
			lstSubCategoryType = new ArrayList<ProductInfo>();	
			if(tmp!=null){
				lstSubCategoryType = tmp.getLstObject(); 
			}
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					// lay danh sack kiem ke
					ObjectVO<CycleCount> lstCCTmp = cycleCountMgr.getListCycleCount(null, null, CycleCountType.ONGOING, null, staff.getShop().getId(), null, null, null, getStrListShopId());
					if(lstCCTmp != null && lstCCTmp.getLstObject() != null && lstCCTmp.getLstObject().size() > 0){
						// lay danh kiem ke moi nhat theo start_date
						cycleCount = lstCCTmp.getLstObject().get(0);
						if(cycleCount.getCycleType()!=null){
							cycleType= cycleCount.getCycleType().getValue();
						}
						status = cycleCount.getStatus().getValue();
						// kiem tra kiem ke co san pham kiem ke hay chua, co thi khong lam gi het, ngc lai: cho them danh sach mat hang
						if(cycleCountMgr.checkIfAnyProductCounted(cycleCount.getId())){
							status = -1;
						}
						
					}else{
						status = -1;
					}
				}
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	/**
	 * Sets the cycle count des.
	 *
	 * @param 
	 * @author tientv
	 */
	public String getListSubCat(){
		try{
			lstSubCategoryCat = new ArrayList<ProductInfo>();
			ProductInfoMgr productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
			lstSubCategoryCat = productInfoMgr.getListSubCat(ActiveType.RUNNING, ProductType.SUB_CAT,idParentCat);		
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	public String search(){
		
	    result.put("page", page);
	    result.put("max", max);	    
		try{
			KPaging<CycleCountMapProduct> kPaging = new KPaging<CycleCountMapProduct>();		
			kPaging.setPage(page-1);
			kPaging.setPageSize(max);
			if(!StringUtil.isNullOrEmpty(cycleCountCode)){
				cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode,shopId);
				lstCCMapProduct =  cycleCountMgr.getListCycleCountMapProductByCycleCountId(null, cycleCount.getId());				
				if(lstCCMapProduct!=null){					 
					 result.put("total", lstCCMapProduct.getkPaging().getTotalRows());
				     result.put("rows", lstCCMapProduct.getLstObject());
				}
			}
			result.put(ERROR, false);
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
			result.put("errMsg", errMsg);
		}
		return JSON;
	}
	/**
	 *
	 *
	 * @param 
	 * @author tientv
	 */
	public String saveProduct(){	
		resetToken(result);
	    boolean error = true;
		try{
			CommonMgr commonMgr = (CommonMgr)context.getBean("commonMgr");
			//Phieu kiem ke khong ton tai
			if(StringUtil.isNullOrEmpty(cycleCountCode)){
				 result.put(ERROR, error);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.cyclecount.code.null"));
				 return JSON;
			}
			if(shop==null){
				result.put(ERROR, true);				
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.shop.is.null")));	
				return JSON;
			}
			cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode,shopId);
			if(cycleCount==null){
				 result.put(ERROR, error);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.cyclecount.code.null"));
				 return JSON;
			}
			if(cycleCount.getStatus()!=CycleCountType.ONGOING){
				result.put(ERROR, error);
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.category.cyclecount.not.ongoing"));
				return JSON;
			}
			Date sysDate = commonMgr.getSysDate();
			cycleCount.setFirstNumber(firstNumber);
			cycleCount.setShop(shop);	
			//cycleCount.setSortByType(SortByType.parseValue(sortBy));
			cycleCount.setCycleType(CycleType.parseValue(cycleType));
			cycleCount.setUpdateDate(sysDate);
			cycleCount.setUpdateUser(staff.getStaffCode());
			
			ProductMgr productMgr = (ProductMgr)context.getBean("productMgr");
			Product product = null;
			List<CycleCountMapProduct> lstCountMapProducts = new ArrayList<CycleCountMapProduct>();
			if(lstProduct!=null){
				if(lstProduct.size()==0){
					result.put(ERROR, error);
					result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.category.cyclecount.list.product"));
					return JSON;
				}
				for(int i=0;i<lstProduct.size();++i){
					CycleCountMapProduct countMapProduct = new CycleCountMapProduct();
					countMapProduct.setCycleCount(cycleCount);
					countMapProduct.setCreateUser(staff.getStaffCode());
					countMapProduct.setStockCardNumber(cycleCount.getFirstNumber());
					product = productMgr.getProductByCode(lstProduct.get(i));
					if(lstIsDelete.get(i)==1){
						CycleCountMapProduct cycleCountMapProduct = cycleCountMgr.getCycleCountMapProduct(cycleCount.getId(), product.getId());
						if(cycleCountMapProduct!=null){
							cycleCountMgr.deleteCycleCountMapProduct(cycleCountMapProduct);							
						}
						continue;
					}
					if(product==null){
						continue;
					}								
					if(!cycleCountMgr.checkIfCycleCountMapProductExist(cycleCount.getId(),lstProduct.get(i),null)){
						countMapProduct.setProduct(product);
						countMapProduct.setStockCardNumber(lstStockCardNumber.get(i));
						lstCountMapProducts.add(countMapProduct);																						
					}else{
						CycleCountMapProduct temp = cycleCountMgr.getCycleCountMapProduct(cycleCount.getId(), product.getId());	
						temp.setStockCardNumber(lstStockCardNumber.get(i));
						if(temp!=null){
							temp.setUpdateDate(sysDate);
							temp.setUpdateUser(staff.getStaffCode());
							lstCountMapProducts.add(temp);
						}						
					}
				}
				cycleCountMgr.createListCycleCountMapProduct(cycleCount, lstCountMapProducts, false,getLogInfoVO());
				error=false;				
			}else{
				cycleCountMgr.createListCycleCountMapProduct(cycleCount, null, true,getLogInfoVO());
				error=false;
			}			
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, error);
			result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
		}
	    return JSON;
	}
	/**
	 * Sets the cycle count des.
	 *
	 * @param 
	 * @author tientv
	 */
	public String searchDetail(){
		result.put("list",new ArrayList<CycleCountMapProduct>());
		try{			
			if(!StringUtil.isNullOrEmpty(cycleCountCode)){
				cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode,shopId);
				if(cycleCount==null){
					 result.put(ERROR, true);
					 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.cyclecount.code.null"));
					 return JSON;
				}
				lstCCMapProduct =  cycleCountMgr.getListCycleCountMapProductByCycleCountId(null, cycleCount.getId());				
				if(lstCCMapProduct==null){
					result.put(ERROR, true);
				    return JSON;
				}
				if(lstCCMapProduct.getLstObject().size()>0){
					result.put("list", lstCCMapProduct.getLstObject());				   
				}else{
					result.put("list",new ArrayList<CycleCountMapProduct>());
				}
				result.put(ERROR, false);
			}			
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
			return JSON;
		}
		return JSON;	    	
	}
	
	
	/**
	 * Sets the cycle count des.
	 *
	 * @param 
	 * @author tientv
	 */
	public String searchProduct(){
		StockMgr stockMgr = (StockMgr)context.getBean("stockMgr");	
	    if(noPaging == null){	    
		    try{
		    	result.put("page", page);
			    result.put("max", max);	    
		    	if (currentUser == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					return JSON;
				}
				ShopToken shT = currentUser.getShopRoot();
				if (shT == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					return JSON;
				}		    	
		    	if (!StringUtil.isNullOrEmpty(cycleCountCode)) {
		    		KPaging<StockTotalVO> kPaging = new KPaging<StockTotalVO>();		
					kPaging.setPage(page-1);
					kPaging.setPageSize(max);					
					cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shT.getShopId());
					if (cycleCount != null && CycleCountType.ONGOING.equals(cycleCount.getStatus()) && cycleCount.getShop().getId().equals(shT.getShopId())) {
						List<Long> listProduct = new ArrayList<Long>();
				    	if(!StringUtil.isNullOrEmpty(listProductEx)){
				    		for(String str:listProductEx.split(",")){
				    			listProduct.add(Long.valueOf(str));
				    		}
				    	}
				    	StockTotalVOFilter filter = new StockTotalVOFilter();				
						filter.setProductCode(productCode);
						filter.setProductName(productName);				
						filter.setShopId(shT.getShopId());
						filter.setExceptProductId(listProduct);
						filter.setWarehouseId(cycleCount.getWarehouse().getId());
						filter.setCatId(category); // id nganh hang
						filter.setSubCatId(sub_category);//id nganh hang con phu
						filter.setFromQuantity(fromAmnt);
						filter.setToQuantity(toAmnt);
						ObjectVO<StockTotalVO> stockTotalVO = stockMgr.getListProductForCycleCount(kPaging, cycleCount.getId(), filter);
						if (stockTotalVO != null) {
							result.put("total", stockTotalVO.getkPaging().getTotalRows());
							result.put("rows", stockTotalVO.getLstObject());				    	
							}
						}  				
				} 		
			}catch (Exception e) {
			    	LogUtility.logError(e, e.getMessage());
			}
	    }else if(noPaging.intValue() == 1){	    	
	    	try {	    	    
		    	if (currentUser == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					return JSON;
				}
				ShopToken shT = currentUser.getShopRoot();
				if (shT == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					return JSON;
				}		    	
		    	if (!StringUtil.isNullOrEmpty(cycleCountCode)) {		    					
					cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, shT.getShopId());
					if (cycleCount != null && CycleCountType.ONGOING.equals(cycleCount.getStatus()) && cycleCount.getShop().getId().equals(shT.getShopId())) {
					 	StockTotalVOFilter filter = new StockTotalVOFilter();				
						filter.setProductCode(productCode);
						filter.setProductName(productName);				
						filter.setShopId(shT.getShopId());					
						filter.setWarehouseId(cycleCount.getWarehouse().getId());
						ObjectVO<StockTotalVO> stockTotalVO = stockMgr.getListProductForCycleCountCategoryStock(null, cycleCount.getId(), filter);
						if(stockTotalVO != null && stockTotalVO.getLstObject().size() > 0 ){
							result.put("list", stockTotalVO.getLstObject());
						} else {
							result.put("list", new ArrayList<StockTotalVO>());
						}
						result.put(ERROR, false);						
					}
				} 	
			} catch (BusinessException e) {
				LogUtility.logError(e, e.getMessage());
				result.put(ERROR, true);
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
				return JSON;
			}
	    }	    
	    return JSON;
	}	
	/**
	 * Sets the cycle count des.
	 *
	 * @param 
	 * @author tientv
	 */
	public String deleteCycleCountDetail(){
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			CycleCountMapProduct cycleCountMapProduct = cycleCountMgr.getCycleCountMapProductById(cycleMapProductId);
			if (cycleCountMapProduct != null) {
				if (cycleCountMapProduct.getCycleCount().getStatus() == CycleCountType.ONGOING) {
					cycleCountMgr.deleteCycleCountMapProduct(cycleCountMapProduct);
					error = false;
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.cyclecount.not.ongoing");
				}
			}

		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
		}
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }	    	    
	    result.put("errMsg", errMsg);
	    return JSON;
	}	
	
	/**
	 * Export PDF with Jasperreport
	 * @param
	 * @author hunglm16
	 * @since March 06, 2014
	 * @description Fix bug
	 */
	public String exportPdf(){
		parametersReport = new HashMap<String, Object>();
		formatType = FileExtension.PDF.getName();
		try
		{
			if(StringUtil.isNullOrEmpty(cycleCountCode)){
				result.put(ERROR, true);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.cyclecount.code.null"));	
				
				return JSON;
			}
			cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode,shopId);
			if(cycleCount==null){
				 result.put(ERROR, true);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.cyclecount.code.null"));				
				return JSON;
			}	
			lstCCMapProduct =  cycleCountMgr.getListCycleCountMapProduct(null, cycleCount.getId(), 
					null, null, null, null, null, null, false);
			if(lstCCMapProduct==null){
				 result.put(ERROR, true);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.print"));				
				return JSON;
			}
			if(lstCCMapProduct.getLstObject().size()==0){
				 result.put(ERROR, true);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.print"));				
				return JSON;
			}
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_NOW);
			parametersReport.put("printDate", printDate);
			if(shopId!= null){
				Shop shop = shopMgr.getShopById(shopId);
				if(shop!=null){
					parametersReport.put("shopName", shop.getShopName());
					if(!StringUtil.isNullOrEmpty(shop.getAddress())){
						parametersReport.put("address", shop.getAddress());
					} else {
						parametersReport.put("address", "");
					}
				}
			}
			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			parametersReport.put("cycleCountCode", cycleCount.getCycleCountCode());
			List<CycleCountMapBean> lst = new ArrayList<CycleCountMapBean>();
			for(CycleCountMapProduct ccMap : lstCCMapProduct.getLstObject()) {
				if(ccMap.getProduct().getCheckLot() != 1){
					CycleCountMapBean a = new CycleCountMapBean();
					a.setStockCardNumber(ccMap.getStockCardNumber());
					a.setProductCode(ccMap.getProduct().getProductCode());
					a.setProductName(ccMap.getProduct().getProductName());
					a.setProductLot("");
//					a.setQuantityCounted(ccMap.getQuantityCounted());
//					a.setDescription(ccMap.getCycleCount().getDescription());
					lst.add(a);
				}
				else{//loctt - Oct14, 2013
					List<ProductLot> productLot = productMgr.getProductLotByProductAndOwner(ccMap.getProduct().getId(),shopId,StockObjectType.SHOP);
					for (ProductLot pl : productLot) {
						CycleCountMapBean a1 = new CycleCountMapBean();
						a1.setStockCardNumber(ccMap.getStockCardNumber());
						a1.setProductCode(ccMap.getProduct().getProductCode());
						a1.setProductName(ccMap.getProduct().getProductName());
						a1.setProductLot(pl.getLot());
						lst.add(a1);
					}
				}
			}
			if (lst != null ) {
				lst.add(0, new CycleCountMapBean()); 
			}
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, lst);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, parametersReport);
			FileExtension ext = FileExtension.parseValue("PDF");
			
			JRDataSource dataSource = new JRBeanCollectionDataSource(lst);
			String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource,ShopReportTemplate.STOCK_CATEGORY_INFOR_EXPORT_INVENTORY);	
			result.put(ERROR, false);
			result.put(LIST, outputPath);
			return JSON;
		}
		catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"system.error"));
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * 
	 *
	 * @param 
	 * @author tientv
	 */
	public String exportExcel(){
		
		try{
			if(StringUtil.isNullOrEmpty(cycleCountCode)){
				result.put(ERROR, true);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.cyclecount.code.null"));	
				
				return JSON;
			}
			cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode,shopId);
			if(cycleCount==null){
				 result.put(ERROR, true);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.cyclecount.code.null"));				
				return JSON;
			}	
			lstCCMapProduct =  cycleCountMgr.getListCycleCountMapProduct(null, cycleCount.getId(), 
					null, null, null, null, null, null, false);
			if(lstCCMapProduct==null){
				 result.put(ERROR, true);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.print"));				
				return JSON;
			}
			if(lstCCMapProduct.getLstObject().size()==0){
				 result.put(ERROR, true);
				 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.category.print"));				
				return JSON;
			}
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_NOW);
			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("hasData", 1);
			beans.put("shop", shop);
			beans.put("printDate", printDate);
			beans.put("cycleCount", cycleCount);			
			beans.put("list", lstCCMapProduct.getLstObject());		
			
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathStock(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.EXPORT_CATEGORY_TEMPLATE;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			
			String outputName = ConstantManager.EXPORT_CATEGORY_STOCK + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (folder + outputName).replace('/', File.separatorChar);
			
			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream,	beans);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			result.put(ERROR, false);
			String outputPath = ServletActionContext.getServletContext().getContextPath() + Configuration.getExcelTemplatePathStock() + outputName;
			result.put(LIST, outputPath);
			
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"system.error"));
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	
	public CycleCountMgr getCycleCountMgr() {
		return cycleCountMgr;
	}

	public void setCycleCountMgr(CycleCountMgr cycleCountMgr) {
		this.cycleCountMgr = cycleCountMgr;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	@Override
	public Staff getStaff() {
		return staff;
	}

	@Override
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public Integer getCycleType() {
		return cycleType;
	}

	public void setCycleType(Integer cycleType) {
		this.cycleType = cycleType;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getCycleCountCode() {
		return cycleCountCode;
	}

	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}

	public CycleCount getCycleCount() {
		return cycleCount;
	}

	public void setCycleCount(CycleCount cycleCount) {
		this.cycleCount = cycleCount;
	}

	public Integer getSortBy() {
		return sortBy;
	}

	public void setSortBy(Integer sortBy) {
		this.sortBy = sortBy;
	}

	public Integer getFirstNumber() {
		return firstNumber;
	}

	public void setFirstNumber(Integer firstNumber) {
		this.firstNumber = firstNumber;
	}

	public List<ProductInfo> getLstCategoryType() {
		return lstCategoryType;
	}

	public void setLstCategoryType(List<ProductInfo> lstCategoryType) {
		this.lstCategoryType = lstCategoryType;
	}

	public Long getCategory() {
		return category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}

	public Long getSub_category() {
		return sub_category;
	}

	public void setSub_category(Long sub_category) {
		this.sub_category = sub_category;
	}
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<ProductInfo> getLstSubCategoryType() {
		return lstSubCategoryType;
	}

	public void setLstSubCategoryType(List<ProductInfo> lstSubCategoryType) {
		this.lstSubCategoryType = lstSubCategoryType;
	}

	public void setLstProduct(List<String> lstProduct) {
		this.lstProduct = lstProduct;
	}

	public List<String> getLstProduct() {
		return lstProduct;
	}

	public ObjectVO<CycleCountMapProduct> getLstCCMapProduct() {
		return lstCCMapProduct;
	}

	public void setLstCCMapProduct(ObjectVO<CycleCountMapProduct> lstCCMapProduct) {
		this.lstCCMapProduct = lstCCMapProduct;
	}

	public void setCycleMapProductId(Long cycleMapProductId) {
		this.cycleMapProductId = cycleMapProductId;
	}

	public Long getCycleMapProductId() {
		return cycleMapProductId;
	}

	public String getCycleCountDes() {
		return cycleCountDes;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * Sets the cycle count des.
	 *
	 * @param cycleCountDes the new cycle count des
	 */
	public void setCycleCountDes(String cycleCountDes) {
		this.cycleCountDes = cycleCountDes;
	}
	public List<Integer> getLstIsDelete() {
		return lstIsDelete;
	}
	public void setLstIsDelete(List<Integer> lstIsDelete) {
		this.lstIsDelete = lstIsDelete;
	}
	
	public List<Integer> getLstStockCardNumber() {
		return lstStockCardNumber;
	}
	public void setLstStockCardNumber(List<Integer> lstStockCardNumber) {
		this.lstStockCardNumber = lstStockCardNumber;
	}
	public Integer getLastNumber() {
		return lastNumber;
	}
	public void setLastNumber(Integer lastNumber) {
		this.lastNumber = lastNumber;
	}
	public Integer getNoPaging() {
		return noPaging;
	}
	public void setNoPaging(Integer noPaging) {
		this.noPaging = noPaging;
	}
	public String getListProductEx() {
		return listProductEx;
	}
	public void setListProductEx(String listProductEx) {
		this.listProductEx = listProductEx;
	}

	public ProductMgr getProductMgr() {
		return productMgr;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public Integer getFromAmnt() {
		return fromAmnt;
	}

	public void setFromAmnt(Integer fromAmnt) {
		this.fromAmnt = fromAmnt;
	}

	public Integer getToAmnt() {
		return toAmnt;
	}

	public void setToAmnt(Integer toAmnt) {
		this.toAmnt = toAmnt;
	}
	
	
	
}

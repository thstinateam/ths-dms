/*
 * Copyright 2014 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.action.stock;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopLockMgr;
import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StockGeneralFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StockGeneralVO;
import ths.dms.core.entities.vo.WarehouseVO;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class TransferInternalWarehouseAction extends AbstractAction {
	private static final long serialVersionUID = -2128120975032582873L;

	private StockManagerMgr stockManagerMgr;
	private ProductMgr productMgr;
	private ShopLockMgr shopLockMgr; 

	private List<WarehouseVO> lstWarehouseVO;

	private Warehouse wareHouse;

	private Long id;
	private Long wareHouseOutputId;
	private Long wareHouseInputId;

	private String shopCodeOutput;
	private String shopCodeInput;
	private String code;
	private String stockTransCode;
	private String name;
	private String description;
	private String stockTransDate;
	private String arrStockTransDetail;
	private String exception;
	private String productCode;
	private String productName;
	
	/**
	 * PREPARE
	 * 
	 * @author hunglm16
	 * @since October 30,2014
	 * @description Khoi tao tham bien mac dinh
	 * */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockManagerMgr = (StockManagerMgr) context.getBean("stockManagerMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		shopLockMgr = (ShopLockMgr) context.getBean("shopLockMgr");
	}

	/**
	 * EXCUTE
	 * 
	 * @author hunglm16
	 * @since October 30,2014
	 * @description Khoi tao ham mac dinh
	 * */
	@Override
	public String execute() {
		resetToken(result);
		return SUCCESS;
	}

	/**
	 * Lay Danh Sach San Pham Co Trong Kho
	 * 
	 * @author hunglm16
	 * @since October 31, 2014
	 * @return List Product
	 * */
	public String getListProductInWareHouse() {
		//result.put("total", 0);
		result.put("rows", new ArrayList<StockGeneralVO>());
		try {
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCodeOutput)) {
				shop = shopMgr.getShopByCode(shopCodeOutput);
			}
			if (shop == null) {
				return JSON;
			}
			StockGeneralFilter<StockGeneralVO> filter = new StockGeneralFilter<StockGeneralVO>();
			//			KPaging<StockGeneralVO> kPaging = new KPaging<StockGeneralVO>();
			//			kPaging.setPageSize(max);
			//			kPaging.setPage(page - 1);
			//			filter.setkPaging(kPaging);
			//Xet tham so bat buoc la 1 Don vi duoc chon khi dnag nhap
			filter.setShopId(shop.getId());
			//Xet tham so Ma Id cua Kho trong Don vi neu co
			if (wareHouseOutputId != null) {
				filter.setWareHouseId(wareHouseOutputId);
			}

			ObjectVO<StockGeneralVO> data = stockManagerMgr.searchProductInWareHouse(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				//result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Lay Danh Sach San Pham Co Trong Kho; popup them moi
	 * 
	 * @author hunglm16
	 * @since October 31, 2014
	 * @return List Product
	 * */
	public String searchListProductInWareHouse() {
		actionStartTime = DateUtil.now();
		result.put("total", 0);
		result.put("rows", new ArrayList<StockGeneralVO>());
		try {
			StockGeneralFilter<StockGeneralVO> filter = new StockGeneralFilter<StockGeneralVO>();
			KPaging<StockGeneralVO> kPaging = new KPaging<StockGeneralVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			//Xet tham so bat buoc la 1 Don vi duoc chon khi dang nhap
			//filter.setShopId(currentUser.getShopRoot().getShopId());
			/** vuongmq; 24/12/2015; Lay tham so don vi output*/
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCodeOutput)) {
				shop = shopMgr.getShopByCode(shopCodeOutput);
				if (shop != null) {
					if (super.getMapShopChild().get(shop.getId()) == null) {
						result.put(ERROR, true);
						result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
						return JSON;
					}
				}
			}
			if (shop == null) {
				return JSON;
			}
			filter.setShopId(shop.getId());
			//Xet tham so Ma Id cua Kho trong Don vi neu co
			if (wareHouseOutputId != null) {
				filter.setWareHouseId(wareHouseOutputId);
			}
			if (!StringUtil.isNullOrEmpty(exception)) {
				String [] arrId =  exception.split(",");
				List<Long> lstId = new ArrayList<Long>();
				for (String value:arrId) {
					lstId.add(Long.valueOf(value));
				}
				filter.setLstExcId(lstId);
			}
			if (!StringUtil.isNullOrEmpty(productCode)) {
				filter.setProductCode(productCode);
			}
			if (!StringUtil.isNullOrEmpty(productName)) {
				filter.setProductName(productName);
			}
			ObjectVO<StockGeneralVO> data = stockManagerMgr.searchProductInWareHouse(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.TransferInternalWarehouseAction.searchListProductInWareHouse()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Them moi hoac cap nhat thong tin Gia cua san pham
	 * 
	 * @author hunglm16
	 * @since August 30,2014
	 * */
	public String createStockTrains() {
		resetToken(result);
		errMsg = "";
		String msg = "";
		try {
			Shop shopOutput = null;
			Shop shopInput = null;
			if (!StringUtil.isNullOrEmpty(shopCodeOutput)) {
				shopOutput = shopMgr.getShopByCode(shopCodeOutput);
			}
			if (!StringUtil.isNullOrEmpty(shopCodeInput)) {
				shopInput = shopMgr.getShopByCode(shopCodeInput);
			}
			List<Long> lstShop = getListShopChildId();
			if (shopOutput == null || !lstShop.contains(shopOutput.getId()) || shopInput == null || !lstShop.contains(shopInput.getId())) {
				result.put(ERROR, true);
				return ERROR;
			}
			shopId = shopOutput.getId();
			List<StockTransDetail> lstDetail = new ArrayList<StockTransDetail>();
			if (wareHouseOutputId == null) {
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.wearehouse.ouput.undefined");
			} else {
				Warehouse wareHouseOutput = stockManagerMgr.getWarehouseById(wareHouseOutputId);
				if (wareHouseOutput == null) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.wearehouse.ouput.undefined");
				} else if (!shopOutput.getId().equals(wareHouseOutput.getShop().getId())) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.wearehouse.ouput.undefined.in.shoproot");
				} else {
					//Lay danh sach san pham thuoc Kho xuat
					StockGeneralFilter<StockGeneralVO> filter = new StockGeneralFilter<StockGeneralVO>();
					filter.setShopId(shopId);
					filter.setWareHouseId(wareHouseOutputId);
					List<StockGeneralVO> lstData = stockManagerMgr.searchProductInWareHouse(filter).getLstObject();

					if (lstData == null || lstData.isEmpty()) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.wearehouse.ouput.product.isnull");
					} else {
						if (wareHouseInputId == null) {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.wearehouse.input.undefined");
						} else {
							Warehouse wareHouseInput = stockManagerMgr.getWarehouseById(wareHouseInputId);
							if (wareHouseInput == null) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.wearehouse.input.undefined");
							}
							if (!shopInput.getId().equals(wareHouseInput.getShop().getId())) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.wearehouse.input.undefined.in.shoproot");
							} else if (wareHouseOutputId.equals(wareHouseInputId)) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.wearehouse.input.output.doule");
							} else {
								if (StringUtil.isNullOrEmpty(arrStockTransDetail)) {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.data.product.isnull");
								} else {
									String[] arrData = arrStockTransDetail.split(",");
									int indexRow = 0;
									// TODO Danh sach san pham duoc gui tu tren giao dien xuong
									for (String value : arrData) {
										String[] arrValue = value.split("ESC");
										if (arrValue == null || arrValue.length < 2) {
											msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.data.local.err");
											break;
										}
										if (msg.length() == 0) {
											//Xu ma san pham
											if (!StringUtil.isNumberInt(arrValue[0]) || !StringUtil.isNumberInt(arrValue[1])) {
												msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.data.product.params.indexrow", String.valueOf(indexRow + 1));
											}
											Product product = new Product();
											product = productMgr.getProductById(Long.valueOf(arrValue[0]));
											if (product == null) {
												msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.data.product.params.indexrow", String.valueOf(indexRow + 1));
											} else {
												if (!ActiveType.RUNNING.getValue().equals(product.getStatus().getValue())) {
													msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.data.product.isrunning.fail", product.getProductCode());
												} else if (Integer.valueOf(arrValue[1]) <= 0) {
													msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.data.product.quantiy.fail.min", product.getProductCode());
												} else {
													boolean flag = false;
													// TODO Kiem tra trong danh sach san pham tu kho xuat
													for (StockGeneralVO prdItem : lstData) {
														if (prdItem.getProductId().equals(product.getId())) {
															flag = true;
															if (prdItem.getAvailableQuantity() < Integer.valueOf(arrValue[1])) {
																msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.data.product.quantiy.fail.max", product.getProductCode());
																break;
															} else {
																//Them moi dong STOCK_TRANS_DETAIL
																StockTransDetail detailItem = new StockTransDetail();
																detailItem.setProduct(product);
																detailItem.setQuantity(Integer.valueOf(arrValue[1]));
																detailItem.setPrice(prdItem.getPrice());
																lstDetail.add(detailItem);
																//Update stock Total
																break;
															}
														}
													}
													if (!flag) {
														msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.data.product.undefined.warehouse.output", product.getProductCode(), wareHouseOutput.getWarehouseCode());
													}
												}
											}
											if (!StringUtil.isNullOrEmpty(msg)) {
												break;
											}
										}
										indexRow++;
									}
									result.put("indexRow", indexRow);
								}
							}
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(currentUser.getUserName());
			
			// TODO cap nhat database
			stockManagerMgr.createStockTransferInternalWarehouse(wareHouseOutputId, wareHouseInputId, shopId, lstDetail, logInfo);
			
			stockTransCode = stockManagerMgr.generateStockTransCodeByTranType(shopId , "DC");
			stockTransDate = DateUtil.convertDateByString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			result.put("stockTransCode", stockTransCode);
			result.put("stockTransDate", stockTransDate);
			result.put(ERROR, false);
			
		} catch (Exception e) {
			String exMsg = e.getMessage();
			if (exMsg != null && exMsg.contains("STOCK_TOTAL_NOT_ENOUGH_")) {
				int i = exMsg.indexOf("STOCK_TOTAL_NOT_ENOUGH_");
				exMsg = exMsg.substring(i);
				String pCode = exMsg.replaceFirst("STOCK_TOTAL_NOT_ENOUGH_", "");
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "npp.dcknb.err.data.product.quantiy.fail.max", pCode));
				result.put(ERROR, true);
				return JSON;
			}
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	
	/**
	 * xu ly khi thay doi shop
	 * 
	 * @author trietptm
	 * @since Jul 28,2015
	 * */
	public String changeShop() {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCodeOutput)) {
				shop = shopMgr.getShopByCode(shopCodeOutput);
			} else if (!StringUtil.isNullOrEmpty(shopCodeInput)) {
				shop = shopMgr.getShopByCode(shopCodeInput);
			}
			if (shop == null) {
				result.put("lstWarehouseVO", new ArrayList<WarehouseVO>());
				result.put("stockTransCode", "");
				result.put("stockTransDate", "");
				return JSON;
			}
			shopId = shop.getId();
			BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
			filter.setLongG(shopId);
			filter.setStatus(ActiveType.RUNNING.getValue());
//			filter.setIsExistStock(true);
			// lay du lieu phan quyen duoi DB
			filter.setStaffRootId(currentUser.getUserId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			filter.setStrShopId(getStrListShopId());
			lstWarehouseVO = stockManagerMgr.getListWarehouseVOByFilter(filter).getLstObject();
			if (lstWarehouseVO == null || lstWarehouseVO.isEmpty()) {
				lstWarehouseVO = new ArrayList<WarehouseVO>();
			}
			if (!StringUtil.isNullOrEmpty(shopCodeOutput)) {
				//Lay ma tu tao tu DB
				stockTransCode = stockManagerMgr.generateStockTransCodeByTranType(shopId , "DC");
				dayLock = shopLockMgr.getNextLockedDay(shopId);
				if (dayLock != null) {
					stockTransDate = DateUtil.convertDateByString(dayLock, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					stockTransDate = DateUtil.convertDateByString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				result.put("stockTransCode", stockTransCode);
				result.put("stockTransDate", stockTransDate);
			}
			result.put("lstWarehouseVO", lstWarehouseVO);
			
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.stock.TransferInternalWarehouseAction.changeShop()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	////////GETTER/SETTER

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	
	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getArrIdStr() {
		return arrIdStr;
	}

	public void setArrIdStr(String arrIdStr) {
		this.arrIdStr = arrIdStr;
	}

	private String arrIdStr;

	public String getStockTransCode() {
		return stockTransCode;
	}

	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}
	
	public Warehouse getWareHouse() {
		return wareHouse;
	}

	public void setWareHouse(Warehouse wareHouse) {
		this.wareHouse = wareHouse;
	}

	public Long getWareHouseOutputId() {
		return wareHouseOutputId;
	}

	public void setWareHouseOutputId(Long wareHouseOutputId) {
		this.wareHouseOutputId = wareHouseOutputId;
	}

	public Long getWareHouseInputId() {
		return wareHouseInputId;
	}

	public void setWareHouseInputId(Long wareHouseInputId) {
		this.wareHouseInputId = wareHouseInputId;
	}

	public String getArrStockTransDetail() {
		return arrStockTransDetail;
	}

	public void setArrStockTransDetail(String arrStockTransDetail) {
		this.arrStockTransDetail = arrStockTransDetail;
	}

	public List<WarehouseVO> getLstWarehouseVO() {
		return lstWarehouseVO;
	}

	public void setLstWarehouseVO(List<WarehouseVO> lstWarehouseVO) {
		this.lstWarehouseVO = lstWarehouseVO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStockTransDate() {
		return stockTransDate;
	}

	public void setStockTransDate(String stockTransDate) {
		this.stockTransDate = stockTransDate;
	}

	public String getShopCodeOutput() {
		return shopCodeOutput;
	}

	public void setShopCodeOutput(String shopCodeOutput) {
		this.shopCodeOutput = shopCodeOutput;
	}

	public String getShopCodeInput() {
		return shopCodeInput;
	}

	public void setShopCodeInput(String shopCodeInput) {
		this.shopCodeInput = shopCodeInput;
	}

	
}

package ths.dms.web.action.stock;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ReportManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductAmountVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransDetailVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class UpdateStockAction.
 */
public class UpdateStockAction extends AbstractAction {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	/** The Constant XUAT_KHO_DIEU_CHINH. */
	public static final String XUAT_KHO_DIEU_CHINH = "XUAT_KHO_DIEU_CHINH";

	/** The Constant NHAP_KHO_DIEU_CHINH. */
	public static final String NHAP_KHO_DIEU_CHINH = "NHAP_KHO_DIEU_CHINH";

	/** The TYP e_ export. */
	public final Integer TYPE_EXPORT = 1;

	/** The TYP e_ import. */
	public final Integer TYPE_IMPORT = 0;

	/** The parameters report. */
	private HashMap<String, Object> parametersReport;

	/** The shop code text field. */
	private String shopCodeTextField;

	/** The stock trans date. */
	private String stockTransDate;

	/** The vansale code. */
	private String vansaleCode;

	/** The trans type. */
	private String transType;

	/** The list data. */
	private List<StockTransLotVO> listData;

	/** The stock mgr. */
	private StockMgr stockMgr;

	/** The product mgr. */
	private ProductMgr productMgr;

	/** The staff mgr. */
	private StaffMgr staffMgr;

	/** The shop mgr. */
	private ShopMgr shopMgr;

	/** The staff. */
	private Staff staff;

	/** The code. */
	private String code;

	/** The name. */
	private String name;

	/** The issued date. */
	private String issuedDate;

	/** The issued staff. */
	private String issuedStaff;

	/** The issued staff. */
	private Integer staffRoleType;

	/**
	 * Gets the staff role type.
	 * 
	 * @return the staff role type
	 */
	public Integer getStaffRoleType() {
		return staffRoleType;
	}

	/**
	 * Sets the staff role type.
	 * 
	 * @param staffRoleType
	 *            the new staff role type
	 */
	public void setStaffRoleType(Integer staffRoleType) {
		this.staffRoleType = staffRoleType;
	}

	/** The shop code. */
	private String shopCode;

	/** The shop code. */
	private String shopCodeNPP;

	/**
	 * Gets the shop code npp.
	 * 
	 * @return the shop code npp
	 */
	public String getShopCodeNPP() {
		return shopCodeNPP;
	}

	/**
	 * Sets the shop code npp.
	 * 
	 * @param shopCodeNPP
	 *            the new shop code npp
	 */
	public void setShopCodeNPP(String shopCodeNPP) {
		this.shopCodeNPP = shopCodeNPP;
	}

	/** The shop name. */
	private String shopName;

	/** The staff code. */
	private String staffCode;

	/** The shop id. */
	private Long shopId;

	/** The product code. */
	private String productCode;

	/** The product name. */
	private String productName;

	/** The amount. */
	private Float amount;

	/** The total money. */
	private BigDecimal totalMoney;

	/** The lst product_ id. */
	private List<Long> lstProduct_Id;

	private List<Long> lstWarehouse_Id;

	/** The lst product_ qty. */
	private List<String> lstProduct_Qty;

	/** The lst product_ lot. */
	private List<String> lstProduct_Lot;

	/** The staff id. */
	private Long staffId;

	/** The type update. */
	private Integer typeUpdate;

	/** The excel file. */
	private File excelFile;

	/** The excel file content type. */
	private String excelFileContentType;

	/** The selected object. */
	private List<String> selectedObject;

	/** The config. */
	private Boolean config;

	/** The is key up f9. */
	private Boolean isKeyUpF9;

	/** The product id. */
	private Long productId;

	/** IN PXKKVCNB. */
	private String transacName;

	/** The transac content. */
	private String transacContent;

	/** The stock trans id. */
	private Long stockTransId;

	/** The report manager mgr. */
	private ReportManagerMgr reportManagerMgr;

	public List<Long> getLstWarehouse_Id() {
		return lstWarehouse_Id;
	}

	public void setLstWarehouse_Id(List<Long> lstWarehouse_Id) {
		this.lstWarehouse_Id = lstWarehouse_Id;
	}

	/**
	 * Gets the transac name.
	 * 
	 * @return the transac name
	 */
	public String getTransacName() {
		return transacName;
	}

	/**
	 * Sets the transac name.
	 * 
	 * @param transacName
	 *            the new transac name
	 */
	public void setTransacName(String transacName) {
		this.transacName = transacName;
	}

	/**
	 * Gets the transac content.
	 * 
	 * @return the transac content
	 */
	public String getTransacContent() {
		return transacContent;
	}

	/**
	 * Sets the transac content.
	 * 
	 * @param transacContent
	 *            the new transac content
	 */
	public void setTransacContent(String transacContent) {
		this.transacContent = transacContent;
	}

	/**
	 * Gets the stock trans id.
	 * 
	 * @return the stock trans id
	 */
	public Long getStockTransId() {
		return stockTransId;
	}

	/**
	 * Sets the stock trans id.
	 * 
	 * @param stockTransId
	 *            the new stock trans id
	 */
	public void setStockTransId(Long stockTransId) {
		this.stockTransId = stockTransId;
	}

	/**
	 * Gets the parameters report.
	 * 
	 * @return the parameters report
	 */
	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}

	/**
	 * Sets the parameters report.
	 * 
	 * @param parametersReport
	 *            the parameters report
	 */
	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	/**
	 * Gets the shop code text field.
	 * 
	 * @return the shop code text field
	 */
	public String getShopCodeTextField() {
		return shopCodeTextField;
	}

	/**
	 * Sets the shop code text field.
	 * 
	 * @param shopCodeTextField
	 *            the new shop code text field
	 */
	public void setShopCodeTextField(String shopCodeTextField) {
		this.shopCodeTextField = shopCodeTextField;
	}

	/**
	 * Gets the stock trans date.
	 * 
	 * @return the stock trans date
	 */
	public String getStockTransDate() {
		return stockTransDate;
	}

	/**
	 * Sets the stock trans date.
	 * 
	 * @param stockTransDate
	 *            the new stock trans date
	 */
	public void setStockTransDate(String stockTransDate) {
		this.stockTransDate = stockTransDate;
	}

	/**
	 * Gets the vansale code.
	 * 
	 * @return the vansale code
	 */
	public String getVansaleCode() {
		return vansaleCode;
	}

	/**
	 * Sets the vansale code.
	 * 
	 * @param vansaleCode
	 *            the new vansale code
	 */
	public void setVansaleCode(String vansaleCode) {
		this.vansaleCode = vansaleCode;
	}

	/**
	 * Gets the trans type.
	 * 
	 * @return the trans type
	 */
	public String getTransType() {
		return transType;
	}

	/**
	 * Sets the trans type.
	 * 
	 * @param transType
	 *            the new trans type
	 */
	public void setTransType(String transType) {
		this.transType = transType;
	}

	/**
	 * Gets the list data.
	 * 
	 * @return the list data
	 */
	public List<StockTransLotVO> getListData() {
		return listData;
	}

	/**
	 * Sets the list data.
	 * 
	 * @param listData
	 *            the new list data
	 */
	public void setListData(List<StockTransLotVO> listData) {
		this.listData = listData;
	}

	/**
	 * Gets the product id.
	 * 
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * Sets the product id.
	 * 
	 * @param productId
	 *            the new product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * Gets the checks if is key up f9.
	 * 
	 * @return the checks if is key up f9
	 */
	public Boolean getIsKeyUpF9() {
		return isKeyUpF9;
	}

	/**
	 * Sets the checks if is key up f9.
	 * 
	 * @param isKeyUpF9
	 *            the new checks if is key up f9
	 */
	public void setIsKeyUpF9(Boolean isKeyUpF9) {
		this.isKeyUpF9 = isKeyUpF9;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the selected object.
	 * 
	 * @return the selected object
	 */
	public List<String> getSelectedObject() {
		return selectedObject;
	}

	/**
	 * Sets the selected object.
	 * 
	 * @param selectedObject
	 *            the new selected object
	 */
	public void setSelectedObject(List<String> selectedObject) {
		this.selectedObject = selectedObject;
	}

	/**
	 * Gets the excel file content type.
	 * 
	 * @return the excelFileContentType
	 */
	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	/**
	 * Sets the excel file content type.
	 * 
	 * @param excelFileContentType
	 *            the excelFileContentType to set
	 */
	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/**
	 * Gets the excel file.
	 * 
	 * @return the excel file
	 */
	public File getExcelFile() {
		return excelFile;
	}

	/**
	 * Sets the excel file.
	 * 
	 * @param excelFile
	 *            the new excel file
	 */
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	/**
	 * Gets the type update.
	 * 
	 * @return the type update
	 */
	public Integer getTypeUpdate() {
		return typeUpdate;
	}

	/**
	 * Sets the type update.
	 * 
	 * @param typeUpdate
	 *            the new type update
	 */
	public void setTypeUpdate(Integer typeUpdate) {
		this.typeUpdate = typeUpdate;
	}

	/**
	 * Gets the lst product_ lot.
	 * 
	 * @return the lst product_ lot
	 */
	public List<String> getLstProduct_Lot() {
		return lstProduct_Lot;
	}

	/**
	 * Sets the lst product_ lot.
	 * 
	 * @param lstProduct_Lot
	 *            the new lst product_ lot
	 */
	public void setLstProduct_Lot(List<String> lstProduct_Lot) {
		this.lstProduct_Lot = lstProduct_Lot;
	}

	/**
	 * Gets the shop name.
	 * 
	 * @return the shop name
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * Gets the stock mgr.
	 * 
	 * @return the stock mgr
	 */
	public StockMgr getStockMgr() {
		return stockMgr;
	}

	/**
	 * Sets the stock mgr.
	 * 
	 * @param stockMgr
	 *            the new stock mgr
	 */
	public void setStockMgr(StockMgr stockMgr) {
		this.stockMgr = stockMgr;
	}

	/**
	 * Gets the product mgr.
	 * 
	 * @return the product mgr
	 */
	public ProductMgr getProductMgr() {
		return productMgr;
	}

	/**
	 * Sets the product mgr.
	 * 
	 * @param productMgr
	 *            the new product mgr
	 */
	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	/**
	 * Gets the staff mgr.
	 * 
	 * @return the staff mgr
	 */
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	/**
	 * Sets the staff mgr.
	 * 
	 * @param staffMgr
	 *            the new staff mgr
	 */
	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	/**
	 * Sets the shop name.
	 * 
	 * @param shopName
	 *            the new shop name
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * Gets the staff id.
	 * 
	 * @return the staff id
	 */
	public Long getStaffId() {
		return staffId;
	}

	/**
	 * Sets the staff id.
	 * 
	 * @param staffId
	 *            the new staff id
	 */
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	/**
	 * Gets the staff.
	 * 
	 * @return the staff
	 */
	public Staff getStaff() {
		return staff;
	}

	/**
	 * Gets the config.
	 * 
	 * @return the config
	 */
	public Boolean getConfig() {
		return config;
	}

	/**
	 * Sets the config.
	 * 
	 * @param config
	 *            the new config
	 */
	public void setConfig(Boolean config) {
		this.config = config;
	}

	/**
	 * Sets the staff.
	 * 
	 * @param staff
	 *            the new staff
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	/**
	 * Gets the shop mgr.
	 * 
	 * @return the shop mgr
	 */
	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	/**
	 * Sets the shop mgr.
	 * 
	 * @param shopMgr
	 *            the new shop mgr
	 */
	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	/**
	 * Gets the lst product_ qty.
	 * 
	 * @return the lst product_ qty
	 */
	public List<String> getLstProduct_Qty() {
		return lstProduct_Qty;
	}

	/**
	 * Sets the lst product_ qty.
	 * 
	 * @param lstProduct_Qty
	 *            the new lst product_ qty
	 */
	public void setLstProduct_Qty(List<String> lstProduct_Qty) {
		this.lstProduct_Qty = lstProduct_Qty;
	}

	/**
	 * Gets the lst product_ id.
	 * 
	 * @return the lst product_ id
	 */
	public List<Long> getLstProduct_Id() {
		return lstProduct_Id;
	}

	/**
	 * Sets the lst product_ id.
	 * 
	 * @param lstProduct_Id
	 *            the new lst product_ id
	 */
	public void setLstProduct_Id(List<Long> lstProduct_Id) {
		this.lstProduct_Id = lstProduct_Id;
	}

	/**
	 * Gets the amount.
	 * 
	 * @return the amount
	 */
	public Float getAmount() {
		return amount;
	}

	/**
	 * Gets the total money.
	 * 
	 * @return the total money
	 */
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	/**
	 * Sets the total money.
	 * 
	 * @param totalMoney
	 *            the new total money
	 */
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	/**
	 * Sets the amount.
	 * 
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(Float amount) {
		this.amount = amount;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the issued date.
	 * 
	 * @return the issuedDate
	 */
	public String getIssuedDate() {
		return issuedDate;
	}

	/**
	 * Sets the issued date.
	 * 
	 * @param issuedDate
	 *            the issuedDate to set
	 */
	public void setIssuedDate(String issuedDate) {
		this.issuedDate = issuedDate;
	}

	/**
	 * Gets the issued staff.
	 * 
	 * @return the issuedStaff
	 */
	public String getIssuedStaff() {
		return issuedStaff;
	}

	/**
	 * Sets the issued staff.
	 * 
	 * @param issuedStaff
	 *            the issuedStaff to set
	 */
	public void setIssuedStaff(String issuedStaff) {
		this.issuedStaff = issuedStaff;
	}

	/**
	 * Gets the shop code.
	 * 
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * Sets the shop code.
	 * 
	 * @param shopCode
	 *            the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * Gets the staff code.
	 * 
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}

	/**
	 * Sets the staff code.
	 * 
	 * @param staffCode
	 *            the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	/**
	 * Gets the shop id.
	 * 
	 * @return the shopId
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 * 
	 * @param shopId
	 *            the shopId to set
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the product code.
	 * 
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 * 
	 * @param productCode
	 *            the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the product name.
	 * 
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the product name.
	 * 
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() {
		try {
			super.prepare();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}

		stockMgr = (StockMgr) context.getBean("stockMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		reportManagerMgr = (ReportManagerMgr) context.getBean("reportManagerMgr");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {
		resetToken(result);
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
			}
			if (staff != null && staff.getShop() != null) {
				shopId = staff.getShop().getId();
				shopCode = staff.getShop().getShopCode();
				shopName = staff.getShop().getShopName();
				staffId = staff.getId();
			}
			// add by lacnv1 - 10.03.2014
			Date date = null;
			if (staff.getShop() != null) {
				date = shopLockMgr.getNextLockedDay(staff.getShop().getId());
			}
			if (date != null) {
				issuedDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				issuedDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (currentUser != null) {
				issuedStaff = currentUser.getFullName();
			}
			Shop shopNPP = getChooseShop();
			if (shopNPP != null) {
				shopCodeNPP = shopNPP.getShopCode();
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Gets the shop detail.
	 * 
	 * @return the shop detail
	 * @author tientv
	 * @since
	 */
	public String shopDetail() {
		try {
			result = new HashMap<String, Object>();
			Shop shop = shopMgr.getShopByCode(shopCode);
			if (shop == null) {
				result.put("error", false);
				return JSON;
			}
			result.put("shopCode", shop.getShopCode());
			result.put("shopId", shop.getId());
			result.put("shopName", shop.getShopName());
			result.put("error", true);
		} catch (Exception e) {
			result.put("error", false);
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay danh sach san pham khi F9
	 * 
	 * @return string
	 * @author tientv
	 * @since Aug 28, 2012
	 */
	public String searchProduct() {
		result.put("page", page);
		result.put("max", max);
		try {
			result.put(ERROR, true);
			result.put("rows", new ArrayList<StockTotalVO>());
			KPaging<StockTotalVO> kPaging = null;
			kPaging = new KPaging<StockTotalVO>();
			kPaging.setPage(page - 1);
			kPaging.setPageSize(max);
			Shop shop = shopMgr.getShopByCode(shopCode);
			if (shop == null) {
				return JSON;
			}
			result.put("shop", shop);
			ObjectVO<StockTotalVO> stockTotalVO = new ObjectVO<StockTotalVO>();
			StockTotalFilter filter = new StockTotalFilter();
			filter.setkPaging(kPaging);
			filter.setProductCode(productCode);
			filter.setProductName(productName);
			filter.setOwnerId(shop.getId());
			filter.setOwnerType(StockObjectType.SHOP);
			filter.setFromQuantity(1);

			if (typeUpdate == TYPE_EXPORT) {
				stockTotalVO = stockMgr.getListStockTotalVO(filter);
			} else if (typeUpdate == TYPE_IMPORT) {
				stockTotalVO = stockMgr.getListProductForImExport(kPaging, productCode, productName, shop.getId(), StockObjectType.SHOP, 0);
			} else {
				return JSON;
			}
			if (stockTotalVO != null) {
				result.put("total", stockTotalVO.getkPaging().getTotalRows());
				result.put("rows", stockTotalVO.getLstObject());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay danh sach san pham de nhap lieu tren Grid
	 * 
	 * @author tientv11
	 * @sine 10/10/2014
	 */
	public String getListProductOnGrid() {
		try {
			Shop shop = shopMgr.getShopByCode(shopCode);
			if (shop == null) {
				result.put("errMsg", R.getResource("common.catalog.code.not.exist", R.getResource("catalog.unit.tree.code") + " " + shopCode));
				result.put(ERROR, true);
				return JSON;
			} else if (shop != null && !ActiveType.RUNNING.getValue().equals(shop.getStatus().getValue())) {
				result.put("errMsg", R.getResource("common.status.pause", R.getResource("catalog.unit.tree") + " " + shopCode));
				result.put(ERROR, true);
				return JSON;
			}
			result.put("shop", shop);

			/** Lay danh sach san pham nhap lieu tren grid */
			List<StockTotalVO> rows = new ArrayList<StockTotalVO>();
			result.put("rows", rows);
			StockTotalFilter filter = new StockTotalFilter();
			if (typeUpdate == TYPE_EXPORT) {
				filter.setOwnerId(shop.getId());
			} else if (typeUpdate != TYPE_IMPORT) {
				return JSON;
			}
			rows = stockMgr.getListProductImportExport(filter);
			if (rows != null) {
				result.put("rows", rows);
			} else {
				result.put("rows", new ArrayList<StockTotalVO>());
			}
			/** Lay sach sach san pham de nhap lieu duoi local */
			filter.setTypeUpdate(typeUpdate);
			filter.setOwnerId(shop.getId());
			filter.setOwnerType(StockObjectType.SHOP);
			filter.setFromQuantity(1);
			ObjectVO<StockTotalVO> stockTotalVO = new ObjectVO<StockTotalVO>();
			if (typeUpdate == TYPE_EXPORT || typeUpdate == TYPE_IMPORT) {
				stockTotalVO = stockMgr.getListStockTotalVO(filter);
			} else {
				return JSON;
			}
			if (stockTotalVO != null) {
				result.put("stock_total", stockTotalVO.getLstObject());
			} else {
				result.put("stock_total", new ArrayList<StockTotalVO>());
			}
			filter.setStatus(ActiveType.RUNNING.getValue());
			List<Warehouse> warehouses = stockMgr.getListWarehouseByFilter(filter);
			result.put("warehouses", warehouses);

			Date date = shopLockMgr.getNextLockedDay(shop.getId());
			if (date != null) {
				issuedDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				issuedDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			result.put("dateStr", issuedDate);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Update stock.
	 * 
	 * @return the string
	 * @author khanhnl
	 * @since Sep 5, 2012
	 ********************* 
	 * @author hunglm16
	 * @since JUNE 3, 2014
	 * @description Update DTYC Tuan 2, thang 6/2014
	 */
	public String updateStock() {
		try {
			Product product = null;
			StockTrans stockTrans = null;
			Shop shop = null;
			Boolean isOK = true;
			result.put(ERROR, true);
			//shop = shopMgr.getShopById(shopId);
			shop = shopMgr.getShopByCode(shopCode);
			if (shop == null) {
				result.put("errMsg", R.getResource("common.catalog.code.not.exist", R.getResource("catalog.unit.tree.code") + " " + shopCode + " "));
				result.put(ERROR, true);
				return JSON;
			} else {
				if (!ActiveType.RUNNING.getValue().equals(shop.getStatus().getValue())) {
					result.put("errMsg", R.getResource("common.status.pause", R.getResource("catalog.unit.tree") + " " + shopCode));
					result.put(ERROR, true);
					return JSON;
				}
				Staff staff = getStaffByCurrentUser();
				if (staff == null) {
					return JSON;
				} else {

					if (lstProduct_Id.size() > 0 && lstProduct_Qty.size() > 0) {
						List<ProductAmountVO> productAmountVOs = new ArrayList<ProductAmountVO>();
						for (int i = 0; i < lstProduct_Id.size(); i++) {
							result.put("lot_id", selectedObject.get(i));
							ProductAmountVO productAmountVO = new ProductAmountVO();
							product = productMgr.getProductById(lstProduct_Id.get(i));

							/** San pham kiem tra lo : Khong lam trong GD 15/10 */
							if (product.getCheckLot() == 1 && lstProduct_Lot.size() > 0) {
								ProductLot productLot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lstProduct_Lot.get(i), shop.getId(), StockObjectType.SHOP, product.getId());
								Integer quantity = getQuantity(lstProduct_Qty.get(i), product.getConvfact(), "Quantity");
								if ((typeUpdate == TYPE_IMPORT) && ((productLot == null && config == null) || (productLot == null && !config))) {
									continue;

								} else if (typeUpdate == TYPE_IMPORT && productLot == null && config) {
									ProductLot productLotEntity = new ProductLot();
									productLotEntity.setLot(lstProduct_Lot.get(i));
									//productLotEntity.setObjectId(shopId);
									productLotEntity.setStatus(ActiveType.RUNNING);
									//productLotEntity.setObjectType(StockObjectType.SHOP);
									productLotEntity.setProduct(product);
									Date exprityDate = null;
									try {
										exprityDate = DateUtil.moveDate(DateUtil.converLotToDate(lstProduct_Lot.get(i), DateUtil.DATE_FORMAT_DDMMYYYY), product.getExpiryDate(), product.getExpiryType());
									} catch (Exception ex) {
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_FORMAT, R.getResource("stock.update.import.header.format.invalid.lot")));
										LogUtility.logError(ex, ex.getMessage());
										return JSON;
									}
									productLotEntity.setExpiryDate(exprityDate);
									Integer available_quantity = getQuantity(lstProduct_Qty.get(i), product.getConvfact(), "Quantity");
									productLotEntity.setAvailableQuantity(available_quantity);
									productLotEntity.setQuantity(available_quantity);

								} else if (typeUpdate == TYPE_EXPORT && productLot == null) {
									result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("stock.update.inport.column.lot.of.product") + product.getProductCode()
											+ " - " + product.getProductName()));
									isOK = false;
									return JSON;
								}
								if (typeUpdate == TYPE_EXPORT && productLot != null && (productLot.getQuantity() < quantity)) {
									result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_QUANTIY_HAS_LOT, productLot.getLot(), product.getProductCode(), product.getProductName()));
									return JSON;
								}
							} else if (!StringUtil.isNullOrEmpty(lstProduct_Lot.get(i))) {
								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STOCK_NOT_LOT, product.getProductCode(), product.getProductName()));
								return JSON;
							}
							productAmountVO.setOwnerId(shop.getId());
							productAmountVO.setOwnerType(StockObjectType.SHOP);
							productAmountVO.setPakage(getQuantity(lstProduct_Qty.get(i), product.getConvfact(), "Package"));
							productAmountVO.setProductId(lstProduct_Id.get(i));
							productAmountVO.setUnit(getQuantity(lstProduct_Qty.get(i), product.getConvfact(), "Unit"));
							productAmountVO.setWarehouseId(lstWarehouse_Id.get(i));
							productAmountVOs.add(productAmountVO);
						}
						//Kiem tra So luong neu Xuat dieu chinh
						if (isOK && typeUpdate == TYPE_EXPORT) {
							List<Boolean> booleans = stockMgr.checkIfProductHasEnough(productAmountVOs);
							for (int j = 0; j < booleans.size(); j++) {
								if (!booleans.get(j)) {
									product = productMgr.getProductById(lstProduct_Id.get(j));
									result.put("errMsg", R.getResource("stock.update.qty.not.available", product.getProductCode()));
									//									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.update.qty.not.available",
									//											Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.update.qty.not.update.exists") + product.getProductCode()));
									return JSON;
								}
							}
						}
						stockTrans = new StockTrans();
						stockTrans.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
						stockTrans.setApprovedStep(SaleOrderStep.CONFIRMED);
						stockTrans.setCreateUser(staff.getStaffCode());

						if (typeUpdate == TYPE_EXPORT) {
							stockTrans.setTransType("DCG");
						} else if (typeUpdate == TYPE_IMPORT) {
							stockTrans.setTransType("DCT");
						} else {
							return JSON;
						}
						stockTrans.setShop(shop);
						stockTrans.setStaffId(staff);

						//lay chot ngay nha phan phoi khi nhap xuat kho dieu chinh cho NPP
						Date stockTransDate = shopLockMgr.getNextLockedDay(shop.getId());
						if (stockTransDate == null) {
							stockTransDate = DateUtil.now();
						}
						stockTrans.setStockTransDate(stockTransDate);
						stockTrans.setStockDate(stockTrans.getStockTransDate());
						List<StockTransDetailVO> lstStockTransDetailVO = new ArrayList<StockTransDetailVO>();
						for (int k = 0; k < lstProduct_Id.size(); k++) {
							product = productMgr.getProductById(lstProduct_Id.get(k));

							StockTransDetail stockTransDetail = new StockTransDetail();
							stockTransDetail.setCreateUser(staff.getStaffCode());
							stockTransDetail.setProduct(product);
							stockTransDetail.setStockTransDate(stockTransDate);
							stockTransDetail.setQuantity(getQuantity(lstProduct_Qty.get(k), product.getConvfact(), "Quantity"));
							stockTransDetail.setWarehouseId(lstWarehouse_Id.get(k));

							StockTransDetailVO stockTransDetailVO = new StockTransDetailVO();
							stockTransDetailVO.setStockTransDetail(stockTransDetail);

							if (product.getCheckLot() == 1) {
								ProductLot productLot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lstProduct_Lot.get(k), shop.getId(), StockObjectType.SHOP, product.getId());
								if ((typeUpdate == 0) && (((productLot == null && config == null) || (productLot == null && !config)))) {
									continue;
								}
								stockTransDetailVO.setLot(lstProduct_Lot.get(k));
							}
							lstStockTransDetailVO.add(stockTransDetailVO);
						}
						if (lstStockTransDetailVO.size() > 0) {
							Long stockTransIdNew = stockMgr.createStockTransForUpdated(stockTrans, lstStockTransDetailVO, null, shop.getId());
							if (stockTransIdNew == null || stockTransIdNew <= Long.valueOf(0)) {
								result.put("errMsg", R.getResource("system.error"));
								return JSON;
							} else {
								StockTrans trans = stockMgr.getStockTransById(stockTransIdNew);
								result.put("stockTransId", trans.getId());
								result.put("transCode", trans.getStockTransCode());
							}
						}
					}
				}
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", R.getResource("system.error"));
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Prints the stock modifier.
	 * 
	 * @return the string
	 */
	public String printStockModifier() {

		parametersReport = new HashMap<String, Object>();
		formatType = FileExtension.PDF.getName();
		// get data for parameters

//		parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
		parametersReport.put("fDate", DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY));

		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			if (!StringUtil.isNullOrEmpty(shopCodeTextField)) {
				Shop shop = shopMgr.getShopByCode(shopCodeTextField);
				if (shop != null) {
					parametersReport.put("shopName", shop.getShopName());
					if (!StringUtil.isNullOrEmpty(shop.getAddress())) {
						parametersReport.put("address", shop.getAddress());
					} else {
						parametersReport.put("address", "");
					}
				}
			}

			// get data for datasource
			StockTrans stockTrans = null;
			String printDate = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
			parametersReport.put("fDate", printDate);
			/*
			 * Staff newStaff = null; newStaff =
			 * staffMgr.getStaffByCode(vansaleCode); if(newStaff != null){
			 * parametersReport
			 * .put("staffCode",codeNameDisplay(newStaff.getStaffCode
			 * (),newStaff.getStaffName())); }
			 */
			stockTrans = stockMgr.getStockTransByCode(code);
			List<StockTransLotVO> listData = new ArrayList<StockTransLotVO>();
			if (stockTrans != null) {
				parametersReport.put("code", code);
				parametersReport.put("totalWeight", stockTrans.getTotalWeight());
				parametersReport.put("totalAmount", stockTrans.getTotalAmount());
				parametersReport.put("stockTransCode", stockTrans.getStockTransCode());
				parametersReport.put("stockTransDate", DateUtil.toDateString(stockTrans.getStockTransDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				parametersReport.put("titlePhieuXuat", R.getResource("stock.update.title.phieu.xuat.kho.dieu.chinh").toUpperCase());
				parametersReport.put("titlePhieuNhap", R.getResource("stock.update.title.phieu.nhap.kho.dieu.chinh").toUpperCase());
				parametersReport.put("ngay", R.getResource("jsp.common.ngay.n"));
				parametersReport.put("soPhieu", R.getResource("stock.update.title.so.phieu"));
				parametersReport.put("trang", R.getResource("common.trang"));
				parametersReport.put("keToan", R.getResource("common.auditor.export.text"));
				parametersReport.put("nhanVienDieuChinh", R.getResource("stock.update.title.nv.dieu.chinh"));
				parametersReport.put("sign", R.getResource("stock.update.title.sign"));
				parametersReport.put("productCode", R.getResource("jsp.common.product.code"));
				parametersReport.put("productName", R.getResource("jsp.common.product.name"));
				parametersReport.put("warehouse", R.getResource("jsp.common.warehouse"));
				parametersReport.put("soLuong", R.getResource("jsp.common.so.luong"));
				parametersReport.put("le", R.getResource("jsp.common.le"));
				parametersReport.put("thung", R.getResource("jsp.common.thung"));
				parametersReport.put("ngayIn", R.getResource("jsp.common.ngay.in"));
				
				ObjectVO<StockTransLotVO> objectVO = stockMgr.getListStockTransLotVOForStockUpdate(null, stockTrans.getId());
				if (objectVO != null) {
					listData = objectVO.getLstObject();
					if (listData != null && listData.size() > 0) {
						int n = listData.size();
						for (int i = 0; i < n; i++) {
							listData.get(i).setBigUnit(listData.get(i).getThung());
							listData.get(i).setSmallUnit(listData.get(i).getLe());
						}
					}
				}
			}
			listData.add(0, new StockTransLotVO());
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, listData);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, parametersReport);
			FileExtension ext = FileExtension.parseValue(formatType);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
			String outputPath = null;
			if (transType.equals(XUAT_KHO_DIEU_CHINH)) {
				outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.XKDC);
			} else if (transType.equals(NHAP_KHO_DIEU_CHINH)) {
				outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.NKDC);
			}
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Import excel.
	 * 
	 * @return the string
	 * @author khanhnl,tientv11
	 * @since Sep 6, 2012
	 ********************** 
	 * @author hunglm16
	 * @since JUNE 3, 2014
	 * @description Update DTYC Tuan 2, thang 6/2014
	 */
	public String importExcel() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		Boolean isCreateStockTrans = false;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelDataAndEmptyRow(excelFile, excelFileContentType, errMsg, 6);
		List<StockTransDetailVO> lstStockTransDetailVO = new ArrayList<StockTransDetailVO>();
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				Staff userLogin = null;
				Map<String, String> mapChildShop = new HashMap<String, String>();
				Map<String, String> mapProductWarehouse = new HashMap<String, String>();
				if (currentUser != null && currentUser.getUserName() != null) {
					userLogin = staffMgr.getStaffByCode(currentUser.getUserName());
				}
				/*if (userLogin != null && userLogin.getShop() != null) {
					shopId = userLogin.getShop().getId();
				}*/
				if (currentUser != null && currentUser.getShopRoot() != null) {
					shopId = currentUser.getShopRoot().getShopId();
				}
				if (userLogin == null || shopId == null || currentUser == null || currentUser.getRoleToken() == null || currentUser.getShopRoot() == null) {
					return SUCCESS;
				}
				ShopFilter filter = new ShopFilter();
				filter.setShopId(shopId);
				filter.setStaffRootId(currentUser.getUserId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				filter.setShopRootId(currentUser.getShopRoot().getShopId());
				filter.setStrShopId(this.getStrListShopId());
				ObjectVO<Shop> lst = shopMgr.getListChildShop(filter);
				if (lst != null && lst.getLstObject() != null) {
					for (Shop s : lst.getLstObject()) {
						mapChildShop.put(s.getShopCode(), s.getShopCode());
					}
				}
				int firstRow = 0;
				StockTrans stockTrans = new StockTrans();
				Shop shop = null;
				boolean flagNotNull = false;
				String msg = "";
				Map<Long, Long> mapShopChild = this.getMapShopChild();
				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						message = "";
						String cell = "";
						flagNotNull = false;
						for (int j = 0; j < 4; j++) {
							if (!StringUtil.isNullOrEmpty(row.get(j).trim())) {
								flagNotNull = true;
								break;
							}
						}
						if (flagNotNull) {
							totalItem++;
						}
						//Header thong tin chung
						if (i == firstRow && flagNotNull) {
							// Loai file import.
							msg = "";
							cell = row.get(0).trim();
							if (!StringUtil.isNullOrEmpty(cell)) {
								try {
									typeUpdate = Integer.parseInt(cell);
									if (typeUpdate != TYPE_EXPORT && typeUpdate != TYPE_IMPORT) {
										msg += R.getResource("stock.update.import.header.type.invalid");
									}
								} catch (Exception ex) {
									msg += R.getResource("stock.update.inport.column.template.fail");
									//row = new ArrayList<String>();
								}

							} else {
								msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("stock.update.import.header.type.field"));
								//message += "\n";
							}
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg + "\n";
							}
							//Ma don vi	
							msg = "";
							cell = row.get(1).trim();
							if (!StringUtil.isNullOrEmpty(cell)) {
								shop = shopMgr.getShopByCode(cell);
								if (shop == null) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("catalog.unit.tree"));
								} else if (mapShopChild.get(shop.getId()) == null) {
									msg += R.getResource("common.shop.not.belong.area");
								} else if (!shop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("catalog.unit.tree"));
								} else if (mapChildShop.get(shop.getShopCode()) == null) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, R.getResource("catalog.unit.tree"));
								}
							} else {
								msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("catalog.unit.tree"));
							}
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
								message += "\n";
							}

							if (StringUtil.isNullOrEmpty(message)) {
								if (typeUpdate == TYPE_EXPORT) {
									stockTrans.setTransType("DCG");
								} else if (typeUpdate == TYPE_IMPORT) {
									stockTrans.setTransType("DCT");
								}
								stockTrans.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
								stockTrans.setApprovedStep(SaleOrderStep.CONFIRMED);
								stockTrans.setShop(shop);
								stockTrans.setStaffId(userLogin);
								stockTrans.setCreateUser(userLogin.getStaffCode());
								isCreateStockTrans = true;
								//lay chot ngay nha phan phoi khi nhap xuat kho dieu chinh cho NPP
								Date stockTransDate = shopLockMgr.getNextLockedDay(shop.getId());
								if (stockTransDate == null) {
									stockTransDate = DateUtil.now();
								}
								stockTrans.setStockTransDate(stockTransDate);
							} else {
								CellBean beanFail = new CellBean();
								beanFail.setContent1(row.size() > 0 ? row.get(0).toString().trim() : "");
								beanFail.setContent2(row.size() > 1 ? row.get(1).toString().trim() : "");
								beanFail.setContent3(row.size() > 2 ? row.get(2).toString().trim() : "");
//								beanFail.setContent4(row.size() > 3 ? row.get(3).toString().trim() : "");
								beanFail.setErrMsg(message);
								beanFail.setTypeRow(1);
								lstFails.add(beanFail);
							}
						} else if (flagNotNull) {
							if (!isCreateStockTrans) {
								CellBean beanFail = new CellBean();
								beanFail.setContent1(row.get(0).trim());
								beanFail.setContent2(row.get(1).trim());
								beanFail.setContent3(row.get(2).trim());
								beanFail.setErrMsg("");
								beanFail.setTypeRow(3);
								lstFails.add(beanFail);
								continue;
							}
							StockTransDetail stockTransDetail = new StockTransDetail();
							StockTransDetailVO stockTransDetailVO = new StockTransDetailVO();

							//Ma hang
							Product product = null;
							msg = "";
							cell = row.get(0).trim();
							if (!StringUtil.isNullOrEmpty(cell)) {
								product = productMgr.getProductByCode(cell);
								if (product == null) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("stock.update.import.header.format.invalid.product.code"));
								} else if (!product.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("stock.update.import.header.format.invalid.product.code"));
								} else {
									stockTransDetail.setProduct(product);
								}
							} else {
								msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("stock.update.import.header.format.invalid.product.code"));
							}
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
								message += "\n";
							}
							//Ma kho
							msg = "";
							Warehouse warehouse = null;
							cell = row.get(1).trim();
							msg = ValidateUtil.validateField(cell, "stock.update.stock.warehouse.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(msg) && shop != null) {
								warehouse = stockMgr.getWarehouseByCode(cell, shop.getId());
								if (warehouse == null) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("stock.update.stock.warehouse.code"));
								} else if (!ActiveType.RUNNING.equals(warehouse.getStatus())) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("stock.update.stock.warehouse.code"));
								} else {
									stockTransDetail.setWarehouseId(warehouse.getId());
								}

							}
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg + "\n";
							}
							if (product != null && warehouse != null) {
								String key = product.getProductCode() + "-" + warehouse.getWarehouseCode();
								if (mapProductWarehouse.get(key) != null) {
									message += R.getResource("stock.update.import.duplication.product", product.getProductCode(), warehouse.getWarehouseCode()) + "\n";
								} else {
									mapProductWarehouse.put(key, product.getProductCode());
								}
							}
							//So lo									
//							ProductLot productLot = null;
//							msg = "";
//							cell = row.get(2).trim();
//							if (product != null && product.getCheckLot() == 1) {
//								if (StringUtil.isNullOrEmpty(cell)) {
//									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("stock.update.inport.column.lot.of.product"));
//								} else {
//									if (shop != null) {
//										productLot = stockMgr.getProductLotByCodeAndOwnerAndProduct(cell, shop.getId(), StockObjectType.SHOP, product.getId());
//									}
//									if (productLot == null) {
//										msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("stock.update.inport.column.lot.of.product"));
//									}
//								}
//								stockTransDetailVO.setLot(cell);
//							}
//							if (product != null && product.getCheckLot() == 0 && !StringUtil.isNullOrEmpty(cell)) {
//								msg += R.getResource("stock.update.import.validate.product.not.lot", product.getProductCode());
//							}
//							if (!StringUtil.isNullOrEmpty(msg)) {
//								message += msg;
//								message += "\n";
//							}
							//So luong
							msg = "";
							cell = row.get(2);
							msg = ValidateUtil.validateField(cell, "stock.countinginput.quantity", 9, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FORMAT_NUMBER_CONVFACT);
							if (StringUtil.isNullOrEmpty(msg)) {
								try {
									if (product != null) {
										Integer vl = getQuantity(cell, product.getConvfact(), "Quantity");
										if (vl < 0) {
											msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_POSSITIVE_INTEGER, R.getResource("stock.update.inport.column.quantity.of.product") + " "
													+ product.getProductCode());
										} else {
											if (Math.abs(vl) > 999999999) {
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INTEGER_MAXVALUE, true, "stock.countinginput.quantity");
											} else if (product != null) {
												List<ProductAmountVO> productAmountVOs = new ArrayList<ProductAmountVO>();
												ProductAmountVO productAmountVO = new ProductAmountVO();
												productAmountVO.setOwnerId(shop.getId());
												productAmountVO.setOwnerType(StockObjectType.SHOP);
												productAmountVO.setPakage(getQuantity(cell, product.getConvfact(), "Package"));
												productAmountVO.setProductId(product.getId());
												productAmountVO.setUnit(getQuantity(cell, product.getConvfact(), "Unit"));
												productAmountVO.setWarehouseId(warehouse.getId());
												productAmountVOs.add(productAmountVO);
												if (typeUpdate == TYPE_EXPORT) {
													List<Boolean> booleans = stockMgr.checkIfProductHasEnough(productAmountVOs);
													for (int j = 0; j < booleans.size(); j++) {
														if (!booleans.get(j)) {
															msg += R.getResource("stock.update.qty.not.available", product.getProductCode());
															break;
														}
													}
//													if (cell != null && productLot != null && productLot.getQuantity() < vl) {
//														msg += R.getResource("stock.update.qty.not.available.and.lot", productLot.getLot());
//													}
												}
											}
											stockTransDetail.setQuantity(vl);
										}
									}
								} catch (Exception e) {
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_INT, R.getResource("stock.update.inport.column.quantity.of.product") + " " + product.getProductCode());
								}
							}
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
								message += "\n";
							}

							//UPDATE DATABASE
							if (StringUtil.isNullOrEmpty(message)) {
								stockTransDetail.setCreateUser(userLogin.getStaffCode());
								stockTransDetail.setStockTrans(stockTrans);
								stockTransDetail.setStockTransDate(stockTrans.getStockTransDate());
								Price price = productMgr.getPriceByProductId(shop.getId(), stockTransDetail.getProduct().getId());
								if (price != null) {
									stockTransDetail.setPrice(price.getPrice());
								}
								stockTransDetailVO.setStockTransDetail(stockTransDetail);
								lstStockTransDetailVO.add(stockTransDetailVO);
							} else {
								CellBean beanFail = new CellBean();
								beanFail.setContent1(row.get(0).trim()); // Ma san pham
								beanFail.setContent2(row.get(1).trim()); // Ma kho
								beanFail.setContent3(row.get(2).trim()); //  So luong
//								beanFail.setContent4(row.get(3).trim()); // So luong
								beanFail.setErrMsg(message);
								beanFail.setTypeRow(3);
								lstFails.add(beanFail);
							}
						}
					}
				}

				if (lstStockTransDetailVO.size() > 0 && lstFails.size() == 0) {
					stockMgr.createStockTransForUpdated(stockTrans, lstStockTransDetailVO, null, shop.getId());
				} else if (lstFails.size() == 0) {
					if (isCreateStockTrans) {
						CellBean beanFail = new CellBean();
						beanFail.setContent1(String.valueOf(typeUpdate));
						beanFail.setContent2(shop.getShopCode());
						//beanFail.setContent3(stockTrans.getStockTransCode());
						//beanFail.setContent3(DateUtil.toDateString(stockTrans.getStockTransDate(), DateUtil.DATE_FORMAT_STR));							
						beanFail.setErrMsg("");
						beanFail.setTypeRow(1);
						if (lstFails.size() == 0) {
							beanFail.setContent6(String.valueOf(2));
						} else {
							beanFail.setContent6(String.valueOf(0));
						}
						lstFails.add(beanFail);
					}
				} else {
					totalItem = lstFails.size();
				}
//				getOutputFailExcelFile(lstFails, ConstantManager.EXPORT_UPDATESTOCK_FAIL_TEMPLATE);
				getOutputFailExcelFileMultiLanguage(lstFails, ConstantManager.EXPORT_UPDATESTOCK_FAIL_TEMPLATE, initImportTempateHeader(), R.getResource("stock.update.title.import.template.error"));
			} catch (Exception e) {
				errMsg = R.getResource("system.error");
				LogUtility.logError(e, e.getMessage());
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	/**
	 * Check lot.
	 * 
	 * @return the string
	 * @author tientv
	 */
	public String checkLot() {
		result = new HashMap<String, Object>();
		try {
			StringBuilder lost = new StringBuilder();
			if (lstProduct_Id.size() > 0 && lstProduct_Lot.size() > 0 && typeUpdate == 0) {
				for (int index = 0; index < lstProduct_Id.size(); ++index) {
					Product product = productMgr.getProductById(lstProduct_Id.get(index).longValue());
					if (product != null && product.getCheckLot() == 1 && !StringUtil.isNullOrEmpty(lstProduct_Lot.get(index))) {
						ProductLot lot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lstProduct_Lot.get(index), shopId, StockObjectType.SHOP, product.getId());
						if (lot == null) {
							lost.append("<b>" + product.getProductCode() + "</b>[" + lstProduct_Lot.get(index) + "]. ");
						}
					}
				}
			}
			result.put(ERROR, false);
			result.put("lots", lost.toString());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Search shop.
	 * 
	 * @return the string
	 * @author tientv
	 */
	public String searchShop() {

		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Shop> kPaging = new KPaging<Shop>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null && currentUser != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null && currentUser.getShopRoot() != null) {
				ObjectVO<Shop> shopVO = null;
				Staff staff = getStaffByCurrentUser();
				Long shopId = null;
				if (staff == null) {
					return JSON;
				}
				shopId = staff.getShop().getId();
				ShopFilter sf = new ShopFilter();
				sf.setShopCode(code);
				sf.setShopName(name);
				sf.setkPaging(kPaging);
				sf.setShopId(shopId);
				sf.setStatus(ActiveType.RUNNING);
				sf.setSpecType(ShopSpecificType.NPP);
				sf.setStaffRootId(currentUser.getUserId());
				sf.setRoleId(currentUser.getRoleToken().getRoleId());
				sf.setShopRootId(currentUser.getShopRoot().getShopId());
				sf.setStrShopId(this.getStrListShopId());
				shopVO = shopMgr.getListChildShop(sf);
				if (shopVO != null) {
					result.put("total", shopVO.getkPaging().getTotalRows());
					result.put("rows", shopVO.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Search lots.
	 * 
	 * @return the string
	 * @author tientv
	 * @note: search lot
	 */
	public String searchLots() {
		result = new HashMap<String, Object>();
		try {
			if (StringUtil.isNullOrEmpty(shopCode)) {
				return JSON;
			}
			Shop chooseShop = shopMgr.getShopByCode(shopCode);
			if (chooseShop == null) {
				return JSON;
			}
			if (productId == null)
				return JSON;
			Product product = productMgr.getProductById(productId);
			if (product == null) {
				return JSON;
			}

			result.put("product", product);
			List<ProductLot> list = productMgr.getProductLotByProductAndOwnerEx(productId, chooseShop.getId(), StockObjectType.SHOP);
			if (list == null) {
				list = new ArrayList<ProductLot>();
			}
			result.put(ERROR, false);
			result.put("lots", list);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * in phieu xuat kho kiem van chuyen noi bo.
	 * 
	 * @return the string
	 * @author vuonghn
	 */
	public String print_PXNKVCNB() {
		parametersReport = new HashMap<String, Object>();
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			Staff staff = getStaffByCurrentUser();
			Shop curShop = staff.getShop();
			Shop shop;
			if (curShop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return JSON;
			}
			if (shopId != null) {
				shop = shopMgr.getShopById(shopId);
				if (shop == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_NPP));
					return JSON;
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.unit.tree"));
				return JSON;
			}

			ReportUrl reportUrl = reportManagerMgr.getReportFileName(shopId, StringUtil.TEMPLATE_XNB.trim());
			if (reportUrl == null || StringUtil.isNullOrEmpty(reportUrl.getUrl())) {
				result.put(ERROR, true);
				result.put("errMsg", "Không có template XNB trong trong dữ liệu để xuất báo cáo.");
				return JSON;
			}
			String nameFile = reportUrl.getUrl();
			int idx = nameFile.lastIndexOf(".jrxml");
			nameFile = nameFile.substring(0, idx);
			nameFile = nameFile + ".jasper";
			String templatePath = Configuration.getReportTemplatePath() + nameFile;
			File fileReport = new File(templatePath);
			if (!fileReport.exists()) {
				result.put(ERROR, true);
				result.put("errMsg", "Không có template XNB trong hệ thống để xuất báo cáo.");
				return JSON;
			}

			List<RptPXKKVCNB_Stock> lstDataStock = new ArrayList<RptPXKKVCNB_Stock>();
			Date dCurrent = DateUtil.now();
			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			parametersReport.put("cDate", DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR));
			parametersReport.put("pDay", DateUtil.getDay(dCurrent));
			parametersReport.put("pMonth", DateUtil.getMonth(dCurrent));
			parametersReport.put("pYear", DateUtil.getYear(dCurrent));

			parametersReport.put("noi_dung", this.transacContent);
			parametersReport.put("lenh_dieu_dong", this.transacName);

			Integer transacDay = 0;
			Integer transacMonth = 0;
			Integer transacYear = 0;
			if (!StringUtil.isNullOrEmpty(issuedDate)) {
				transacDay = DateUtil.getDay(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				transacMonth = DateUtil.getMonth(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				transacYear = DateUtil.getYear(DateUtil.parse(issuedDate, ConstantManager.FULL_DATE_FORMAT));
				parametersReport.put("don_vi_ngay", transacDay);
				parametersReport.put("don_vi_thang", transacMonth);
				parametersReport.put("don_vi_nam", transacYear);
			} else {
				parametersReport.put("don_vi_ngay", DateUtil.getDay(dCurrent));
				parametersReport.put("don_vi_thang", DateUtil.getMonth(dCurrent));
				parametersReport.put("don_vi_nam", DateUtil.getYear(dCurrent));
			}
			RptPXKKVCNB_Stock dataStock = stockMgr.getPXKKVCNB(shopId, staffCode, stockTransId, transacName, transacContent);
			lstDataStock.add(dataStock);
			if (lstDataStock.size() > 0) {
				//FileExtension ext = FileExtension.PDF;
				JRDataSource dataSource = new JRBeanCollectionDataSource(lstDataStock);
				String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(StringUtil.TEMPLATE_XNB.toLowerCase().trim() + "-"
						+ shop.getShopCode().toLowerCase() + ".pdf")).toString());

				String outputPath = Configuration.getReportTemplatePath() + outputFile;
				String outputDowload = Configuration.getReportRealPath() + outputFile;
				JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parametersReport, dataSource);
				JRAbstractExporter exporter = new JRPdfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
				exporter.exportReport();
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputDowload);
				MemcachedUtils.putValueToMemcached(reportToken, outputDowload, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, false);
				result.put(ERROR, false);
				result.put("hasData", false);
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "UpdateStockAction.in_pxkkvcnb : " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Tao file template dung de import 
	 * 
	 * @author tungmt
	 * @return Doi tuong chua duong dan toi file template
	 * @since 25/05/2015
	 */
	public String downloadTemplate() {
//		String importTemplateFileName = this.createImportTemplateFile();
//		String outputPath = Configuration.getExportExcelPath() + importTemplateFileName;
//		result.put(REPORT_PATH, outputPath);
//		result.put(LIST, outputPath);
//		result.put(ERROR, false);
//		result.put("hasData", true);
//		return JSON;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		lstFails.add(new CellBean());
		getOutputFailExcelFileMultiLanguage(lstFails, ConstantManager.EXPORT_UPDATESTOCK_TEMPLATE, initImportTempateHeader(), R.getResource("stock.update.title.file.name.import"));
		result.put(REPORT_PATH, fileNameFail);
		result.put(LIST, fileNameFail);
		result.put(ERROR, false);
		result.put("hasData", true);
		return JSON;
	}
	
	/**
	 * Tao file mau import gia san pham
	 * 
	 * @author tungmt
	 * @return Ten file template moi duoc tao
	 * @since 25/05/2015
	 */
	private String createImportTemplateFile() {
		String importTemplateFileName = null;

		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		XSSFWorkbook workbook = null;
//		OPCPackage opcPackage = null;
		try {
			/*
			 * tao file template moi tu file template mau
			 */
			String importTemplateFileAbsolutePath = this.generateTemplateImportFileFromOriginalFile();
			importTemplateFileName = Paths.get(importTemplateFileAbsolutePath).getFileName().toString();

			/*
			 * ghi du lieu
			 */
			fileInputStream = new FileInputStream(importTemplateFileAbsolutePath);
//			opcPackage = OPCPackage.open(fileInputStream);
			workbook = new XSSFWorkbook(fileInputStream);

			/*
			 * ghi header sheet import
			 */
			int IMPORT_SHEET_INDEX = 0;
			this.writeImportSheetHeader(workbook, IMPORT_SHEET_INDEX);

			fileOutputStream = new FileOutputStream(importTemplateFileAbsolutePath);
			workbook.write(fileOutputStream);
			fileOutputStream.close();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		} finally {
			IOUtils.closeQuietly(fileInputStream);
			IOUtils.closeQuietly(fileOutputStream);
//			IOUtils.closeQuietly(opcPackage);
		}

		return importTemplateFileName;
	}
	
	/**
	 * Ghi header sheet import
	 * 
	 * @author tungmt
	 * @return String Duong dan tuyet doi toi file template tra ve client
	 * @throws IOException 
	 * @since 25/05/2015
	 */
	private String generateTemplateImportFileFromOriginalFile() throws IOException {
		final String resourceDirectoryPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathStock();
		final String sourceTemplateFileName = ConstantManager.IMPORT_STOCK_UPDATE_TEMPATE_FILE_NAME + FileExtension.XLSX.getValue();
		final String sourceTemplateFileAbsoluteName = resourceDirectoryPath + File.separator + sourceTemplateFileName;

		String importTemplateFileName = R.getResource("stock.update.title.file.name.import") + "_"
				+ DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
		final String importTemplateFileAbsolutePath = Configuration.getStoreRealPath() + File.separator + importTemplateFileName;
		FileUtility.copyFile(sourceTemplateFileAbsoluteName, importTemplateFileAbsolutePath);
		return importTemplateFileAbsolutePath;
	}
	
	/**
	 * Ghi header sheet import
	 * 
	 * @author tungmt
	 * @param workbook Workbook ghi du lieu
	 * @param importSheetIndex Vi tri sheet ghi du lieu
	 * @since 25-05-2015
	 */
	private void writeImportSheetHeader(XSSFWorkbook workbook, int importSheetIndex) {
		XSSFSheet importSheet = workbook.getSheetAt(importSheetIndex);
		if (importSheet != null) {
			workbook.setSheetName(importSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.title.template.file.import.sheet_name"));
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			int row = 0;
			int col = 0;
			excelProcessUtil.writeCellData(importSheet, row, col++, R.getResource("stock.update.import.header.type.field"));
			excelProcessUtil.writeCellData(importSheet, row, col++, R.getResource("jsp.common.shop.code"));
			col = 0; row++;
			excelProcessUtil.writeCellData(importSheet, row, col++, "1");
			excelProcessUtil.writeCellData(importSheet, row, col++, "VP40071");
			col = 0; row++;
			excelProcessUtil.writeCellData(importSheet, row, col++, R.getResource("jsp.common.product.code"));
			excelProcessUtil.writeCellData(importSheet, row, col++, R.getResource("jsp.common.warehouse.code"));
			excelProcessUtil.writeCellData(importSheet, row, col++, R.getResource("jsp.common.so.luong"));
			col = 0; row++;
			excelProcessUtil.writeCellData(importSheet, row, col++, "02BA40");
			excelProcessUtil.writeCellData(importSheet, row, col++, "K001");
			excelProcessUtil.writeCellData(importSheet, row, col++, "3");
		}
	}
	
	private Map<String, Object> initImportTempateHeader(){
		Map<String, Object> fileHeaderParams = new HashMap<String, Object>();
		fileHeaderParams.put("typeUpdate", R.getResource("stock.update.import.header.type.field"));
		fileHeaderParams.put("shopCode", R.getResource("jsp.common.shop.code"));
		fileHeaderParams.put("productCode", R.getResource("jsp.common.product.code"));
		fileHeaderParams.put("warehouseCode", R.getResource("jsp.common.warehouse.code"));
		fileHeaderParams.put("soLuong", R.getResource("jsp.common.so.luong"));
		fileHeaderParams.put("listSP", R.getResource("stock.update.product.list.size.zero"));
		fileHeaderParams.put("errMsg", R.getResource("stock.update.title.import.loi"));
		return fileHeaderParams;
	}
}

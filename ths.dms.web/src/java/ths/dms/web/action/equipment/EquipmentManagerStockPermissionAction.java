
package ths.dms.web.action.equipment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.EquipStockPermissionMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleDetail;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.filter.EquipmentRoleDetailFilter;
import ths.dms.core.entities.vo.EquipProposalBorrowVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipmentRoleStockTempVO;
import ths.dms.core.entities.vo.EquipmentRoleVO;
import ths.dms.core.entities.vo.EquipmentRoleVOTempImport;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Action Quan ly quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 03,2015
 */
public class EquipmentManagerStockPermissionAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private static final String PAGING = "PAGING";
	private static final String NO_PAGING = "NO_PAGING";
	/**
	 * Khai bao cac bien toan cuc
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	CommonMgr commonMgr;
	EquipStockPermissionMgr equipStockPermissionMgr;
	
	EquipmentManagerMgr equipmentManagerMgr;

	/**
	 * Xu ly cac ham thuc thi ket noi
	 * 
	 * @author hoanv25
	 * @since March 02,2014
	 * */
	@Override
	public void prepare() throws Exception {
		equipStockPermissionMgr = (EquipStockPermissionMgr) context.getBean("equipStockPermissionMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		super.prepare();
	}

	/**
	 * Khai bao cac du lieu dung chung khong khai bao method
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	@Override
	public String execute() throws Exception {
		generateToken();
		if (currentUser != null && currentUser.getShopRoot() != null) {
			shopId = currentUser.getShopRoot().getShopId();
		}

		return SUCCESS;
	}

	/**
	 * Tim kiem Danh sach quyen quan ly kho
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since March 09,2015
	 */
	public String search() {
		result.put("rows", new ArrayList<EquipProposalBorrowVO>());
		result.put("total", 0);
		try {		
			//set cac tham so tim kiem
			EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = this.getFilterSearch(PAGING);
			//goi ham thuc thi tim kiem
			ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.searchListStockPermissionVOByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.search()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author vuongmq
	 * Param Tim kiem Danh sach kho; 
	 * @descripntion: dung chung cho tim kiem va xuat excel theo ket qua tim kiem
	 * @date Mar 23,2015
	 */
	private EquipProposalBorrowFilter<EquipProposalBorrowVO> getFilterSearch(String paging) {
		EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = new EquipProposalBorrowFilter<EquipProposalBorrowVO>();
		if (PAGING.equals(paging)) {
			KPaging<EquipProposalBorrowVO> kPaging = new KPaging<EquipProposalBorrowVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
		}
		if (!StringUtil.isNullOrEmpty(code)) {
			filter.setCode(code);
		}
		if (!StringUtil.isNullOrEmpty(name)) {
			filter.setName(name);
		}
		if (status != null && status != -2) {
			filter.setStatus(status);
		}
		return filter;
	}
	/**
	 * Tim kiem Danh sach kho
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since March 09,2015
	 */
	public String searchListStock() {
		result.put("rows", new ArrayList<EquipRecordVO>());
		result.put("total", 0);
		try {
			EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = new EquipProposalBorrowFilter<EquipProposalBorrowVO>();
			//set cac tham so tim kiem
			KPaging<EquipProposalBorrowVO> kPaging = new KPaging<EquipProposalBorrowVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			if (id != null) {
				filter.setId(id);
			}
			//goi ham thuc thi tim kiem
			ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.searchListStockPermissionVOAdd(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.searchListStock()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay danh sach kho theo don vi de them moi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since March 09,2015
	 */
	public String searchStock() {
		result.put("rows", new ArrayList<EquipRecordVO>());
		result.put("total", 0);
		try {
			EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = new EquipProposalBorrowFilter<EquipProposalBorrowVO>();
			//set cac tham so tim kiem
			KPaging<EquipProposalBorrowVO> kPaging = new KPaging<EquipProposalBorrowVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			filter.setShopId(currentUser.getShopRoot().getShopId());
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				/*if (!checkShopInOrgAccessByCode(shopCode)) {
					return JSON;
				}*/
				filter.setShopCode(shopCode);
			}
			if (!StringUtil.isNullOrEmpty(shopName)) {
				filter.setShopName(shopName);
			}
			if (!StringUtil.isNullOrEmpty(stockCode)) {
				filter.setStockCode(stockCode);
			}
			if (!StringUtil.isNullOrEmpty(stockName)) {
				filter.setStockName(stockName);
			}
			List<Long> lstEquipmentId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstEquipId)) {
				for (String str : lstEquipId.split(",")) {
					lstEquipmentId.add(Long.valueOf(str));
				}
			}
			List<Long> lstStockObjectId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstObjectId)) {
				for (String strc : lstObjectId.split(",")) {
					if (!StringUtil.isNullOrEmpty(strc)) {
						lstStockObjectId.add(Long.valueOf(strc));
					}
				}
			}
			List<Long> lstCheckId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstCheckSearchStockAdd)) {
				for (String strc : lstCheckSearchStockAdd.split(",")) {
					if (!StringUtil.isNullOrEmpty(strc)) {
						lstCheckId.add(Long.valueOf(strc));
					}
				}
			}
			List<Long> lstIdStock = new ArrayList<Long>();
			if (lstCheckId != null && lstCheckId.size() > 0) {
				for (int i = 0, sz = lstCheckId.size(); i < sz; i++) {
					EquipStock lstStock = equipStockPermissionMgr.getEquipStockById(lstCheckId.get(i));
					if (lstStock != null) {
						List<EquipProposalBorrowVO> lstShop = equipStockPermissionMgr.searchCheckShopId(lstStock.getShop().getId());
						if (lstShop != null) {
							for (int j = 0, s = lstShop.size(); j < s; j++) {
								lstIdStock.add(lstShop.get(j).getId());
							}
						}
					}
				}
			}
			if (lstIdStock != null) {
				filter.setLstIdStock(lstIdStock);
			}
			if (lstEquipmentId != null) {
				filter.setLstEquipId(lstEquipmentId);
			}
			if (lstStockObjectId != null) {
				filter.setLstObjectId(lstStockObjectId);
			}
			
			filter.setShopRootId(currentUser.getShopRoot() == null ? null : currentUser.getShopRoot().getShopId());
			filter.setStaffRootId(currentUser.getStaffRoot() == null ? null :currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken() == null ? null : currentUser.getRoleToken().getRoleId());
			
			//goi ham thuc thi tim kiem
			ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.searchStockByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.searchStock()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tao moi quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 13, 2015
	 * @return
	 */
	public String addStockPermission() {
		resetToken(result);
		String errMsg = "";
		try {
			EquipRole eq = new EquipRole();
			if (!StringUtil.isNullOrEmpty(code)) {
				eq = equipStockPermissionMgr.getEquipRoleByCodeEx(code, ActiveType.RUNNING);
				if (eq != null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "equipment.role.code")); // Ma quyen da ton tai trong he thong
					return JSON;
				}
			}
			eq = new EquipRole();
			eq.setCode(code);
			eq.setName(name);
			eq.setDescription(description);
			if (status != null) {
				ActiveType st = ActiveType.parseValue(status);
				eq.setStatus(st);
			}
			eq.setCreateUser(currentUser.getUserName());
			List<Long> listCheckStock = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(checkListStock)) {
				for (String str : checkListStock.split(",")) {
					if (!StringUtil.isNullOrEmpty(str)) {
						listCheckStock.add(Long.valueOf(str));
					}
				}
			}
			List<Long> listNoCheckStock = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(noCheckListStock)) {
				for (String str : noCheckListStock.split(",")) {
					if (!StringUtil.isNullOrEmpty(str)) {
						listNoCheckStock.add(Long.valueOf(str));
					}
				}
			}
			List<Long> listmapIdStock = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(mapIdStock)) {
				for (String str : mapIdStock.split(",")) {
					if (!StringUtil.isNullOrEmpty(str)) {
						listmapIdStock.add(Long.valueOf(str));
					}
				}
			}
			//Check kho trung chom 
			ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.checkStockPermissionAdd(listCheckStock, listNoCheckStock, listmapIdStock);
			if (objVO.getLstObject() != null && objVO.getLstObject().size() > 0 && !objVO.getLstObject().isEmpty()) {
				result.put(ERROR, true);
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.stock.permission.manage.err.exist");
				result.put("errMsg", errMsg);
				return JSON;
			}
			eq = equipStockPermissionMgr.createStockPermission(eq, getLogInfoVO());
			//Luu danh sach kho theo ma quyen
			EquipmentRoleDetailFilter<EquipmentRoleVO> filter = new EquipmentRoleDetailFilter<EquipmentRoleVO>();
			filter.setListCheckStock(listCheckStock);
			filter.setListNoCheckStock(listNoCheckStock);
			filter.setListmapIdStock(listmapIdStock);
			equipStockPermissionMgr.createListStockRoleDetail(filter, eq);
		} catch (Exception e) {
			// TODO: handle exception
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.addStockPermission()" + e.getMessage());
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Chinh sua quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 13, 2015
	 * @return
	 */
	public String saveStockPermission() {
		resetToken(result);
		String errMsg = "";
		try {
			if (currentUser.getUserName() != null) {
				if (id != null && id > 0) {
					// cap nhat quyen 													
					EquipRole eq = equipStockPermissionMgr.getStockPermissionById(id);
					if (eq != null) {
						eq.setName(name);
						eq.setDescription(description);
						if (status != null) {
							ActiveType st = ActiveType.parseValue(status);
							eq.setStatus(st);
						}
						eq.setUpdateUser(currentUser.getUserName());
						equipStockPermissionMgr.saveStockPermission(eq, getLogInfoVO());
						//Luu equip_role_detail
						List<Long> lstEquipStockDel = new ArrayList<Long>();
						if (!StringUtil.isNullOrEmpty(lstEquipStockGirdDel)) {
							for (String str : lstEquipStockGirdDel.split(",")) {
								if (!StringUtil.isNullOrEmpty(str)) {
									lstEquipStockDel.add(Long.valueOf(str));
								}
							}
						}
						if (lstEquipStockDel != null && !lstEquipStockDel.isEmpty()) {
							for (int i = 0, sz = lstEquipStockDel.size(); i < sz; i++) {
								EquipRoleDetail edt = equipStockPermissionMgr.getListEquipRoleDetailById(lstEquipStockDel.get(i));
								if (edt != null) {
									equipStockPermissionMgr.deleteEquipRoleDetail(edt);
								}
							}
						}
						/** lay list cu lstCheckStockNow */
						EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = new EquipProposalBorrowFilter<EquipProposalBorrowVO>();
						filter.setId(id);
						ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.searchListStockPermissionVOAdd(filter);
						/** tao lstStockNow */
						List<Long> lstStockNow = new ArrayList<Long>();
						if (objVO != null && objVO.getLstObject() != null && objVO.getLstObject().size() > 0) {
							for (EquipProposalBorrowVO vo : objVO.getLstObject()) {
								if (vo != null && vo.getIdStock() != null) {
									lstStockNow.add(vo.getIdStock());
								}
							}
						}
						List<Long> listMapEditIdStock = new ArrayList<Long>();
						if (!StringUtil.isNullOrEmpty(mapIdEditStock)) {
							for (String str : mapIdEditStock.split(",")) {
								if (!StringUtil.isNullOrEmpty(str)) {
									listMapEditIdStock.add(Long.valueOf(str));
								}
							}
						}
						List<Long> listEditIdRoleStock = new ArrayList<Long>();
						if (!StringUtil.isNullOrEmpty(mapIdEditRoleStock)) {
							for (String str : mapIdEditRoleStock.split(",")) {
								if (!StringUtil.isNullOrEmpty(str)) {
									listEditIdRoleStock.add(Long.valueOf(str));
								}
							}
						}
						/**kiem tra kho co trung chom hay khong*/
						ObjectVO<EquipProposalBorrowVO> obj = equipStockPermissionMgr.checkStockPermissionEdit(listMapEditIdStock,listEditIdRoleStock,checkListStockVO, noCheckListStockVO);
						if (obj.getLstObject() != null && obj.getLstObject().size() > 0 && !obj.getLstObject().isEmpty()) {
							result.put(ERROR, true);
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.stock.permission.manage.err.exist");
							result.put("errMsg", errMsg);
							return JSON;
						}
						/** id: id cua ma quyen Equip_Role */
						equipStockPermissionMgr.updateStockPermissionVODetail(id, lstStockNow, listMapEditIdStock, checkListStockVO, noCheckListStockVO, getLogInfoVO());
						result.put(ERROR, false);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.saveStockPermission()" + e.getMessage());
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Xoa quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 13, 2015
	 * @return
	 */
	public String deleteStockPermission() {
		resetToken(result);
		try {
			if (id != null && id > 0) {
				EquipRole eq = equipStockPermissionMgr.getStockPermissionById(id);
				ObjectVO<EquipProposalBorrowVO> listEquipRole = equipStockPermissionMgr.listEquipRoleById(id);
				if (listEquipRole != null && listEquipRole.getLstObject() != null && !listEquipRole.getLstObject().isEmpty()) {
					result.put(ERROR, true);
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.stock.permission.role.err.exist");
					result.put("errMsg", errMsg);
					return JSON;
				} else {
					if (eq != null) {
						eq.setStatus(ActiveType.DELETED);
						eq.setUpdateUser(currentUser.getUserName());
						equipStockPermissionMgr.saveStockPermission(eq, getLogInfoVO());					
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.deleteStockPermission()" + e.getMessage());
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * View detail stock. Ham viewDetailStock() su dung de tim kiem chi tiet
	 * thong tin kho ma quyen
	 * 
	 * @param :id (id quyen)
	 * @author hoanv25
	 * @since March 16, 2015
	 */
	public String viewDetailStock() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipProposalBorrowVO>());
		try {
			KPaging<EquipProposalBorrowVO> kPaging = new KPaging<EquipProposalBorrowVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (id != null) {
				ObjectVO<EquipProposalBorrowVO> lstStockPermission = equipStockPermissionMgr.getListtEquipStockByEquipRoleId(kPaging, id);
				if (lstStockPermission != null) {
					result.put("total", lstStockPermission.getkPaging().getTotalRows());
					result.put("rows", lstStockPermission.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error EquipmentManagerStockPermissionAction.viewDetail: " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Coppy quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 13, 2015
	 * @return
	 */
	public String copyStockPermission() {
		resetToken(result);
		String errMsg = "";
		try {
			EquipRole eq = null;
			if (!StringUtil.isNullOrEmpty(code)) {
				eq = equipStockPermissionMgr.getEquipRoleByCodeEx(code, ActiveType.RUNNING);
				if (eq != null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "equipment.role.code")); // Ma quyen da ton tai trong he thong
					return JSON;
				}
			}
			eq = new EquipRole();
			eq.setCode(code);
			eq.setName(name);
			eq.setDescription(description);
			if (status != null) {
				ActiveType st = ActiveType.parseValue(status);
				eq.setStatus(st);
			}
			eq.setCreateUser(currentUser.getUserName());
			eq = equipStockPermissionMgr.createStockPermission(eq, getLogInfoVO());
			EquipRoleDetail equipDetal = new EquipRoleDetail();
			//	EquipRole equipRole = equipStockPermissionMgr.getEquipRoleByCodeEx(code, ActiveType.RUNNING);
			ObjectVO<EquipProposalBorrowVO> eqd = equipStockPermissionMgr.getListEquipRoleDetailByEquipRoleCode(id);
			if (eqd != null && eqd.getLstObject() != null && eqd.getLstObject().size() > 0) {
				List<Long> lstStockNow = new ArrayList<Long>();
				if (eqd != null && eqd.getLstObject() != null && eqd.getLstObject().size() > 0) {
					for (EquipProposalBorrowVO vo : eqd.getLstObject()) {
						if (vo != null && vo.getId() != null) {
							lstStockNow.add(vo.getId());
						}
					}
				}
				for (int i = 0, sz = lstStockNow.size(); i < sz; i++) {
					EquipRoleDetail equip = equipStockPermissionMgr.getListEquipRoleDetailById(lstStockNow.get(i));
					if (equip != null) {
						equipDetal.setEquipRole(eq);
						equipDetal.setEquipStock(equip.getEquipStock());
						equipDetal.setIsUnder(equip.getIsUnder());
						equipDetal.setCreateUser(currentUser.getUserName());
						equipStockPermissionMgr.createEquipRoleDetail(equipDetal);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.copyStockPermission()" + e.getMessage());
		}
		result.put("errMsg", errMsg);
		return JSON;
	}	
	/**
	 * Xuat Excel theo Tim kiem quyen kho thiet bi
	 * @author vuongmq
	 * @since Mar 23,2015
	 * @return Excel
	 * */
	public String exportExcelBySearchEquipmentStockPermission() {
		try {
			/*begin vuongmq; 11/05/2015; xuat Excel ATTT*/
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			/*end vuongmq; 11/05/2015; xuat Excel ATTT*/
			/** set cac tham so tim kiem */
			EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = this.getFilterSearch(NO_PAGING);
			/** goi ham thuc thi tim kiem */
			ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.searchListStockPermissionVOByFilterExport(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				List<String> lstCodeRole = new ArrayList<String>();
				List<EquipProposalBorrowVO> lstViewExport = new ArrayList<EquipProposalBorrowVO>();
				for (EquipProposalBorrowVO item : objVO.getLstObject()) {
					if (item != null) {
						/***
						 * truong: stock: là gia tri "X" kho cap duoi; truong:
						 * representation là giá trị: hoạt động or tạm ngưng khi
						 * xuất excel
						 */
						if (item.getIsUnder() != null && ActiveType.RUNNING.getValue().equals(item.getIsUnder())) {
							item.setStock("X");
						} else {
							item.setStock("");
						}
						if (item.getStatus() != null && ActiveType.RUNNING.getValue().equals(item.getStatus())) {
							item.setRepresentation(R.getResource("equipment.manager.status.running").trim());
						} else {
							item.setRepresentation(R.getResource("equipment.manager.status.stopped").trim());
						}
						/**
						 * truong hop khong co trong lstCodeRole thi add code
						 * vao; va lay nguyen item ra
						 */
						if (!lstCodeRole.contains(item.getCode())) {
							lstCodeRole.add(item.getCode());
							lstViewExport.add(item);
						} else {
							/**
							 * truong hop co trong lstCodeRole thi lay nhung gia
							 * tri can thiet: ma kho, kho don vi cap duoi; khong
							 * can: ma quyen, ten quyen, trang thai nua
							 */
							item.setCode("");
							item.setName("");
							item.setRepresentation("");
							lstViewExport.add(item);
						}
					}
				}

				HashMap<String, Object> beans = new HashMap<String, Object>();
				beans.put("lst", objVO.getLstObject());
				String outputPath = ReportUtils.exportExcelJxlsx(beans, ShopReportTemplate.QLTB_QLQUYENKHOTHIETBI, FileExtension.XLSX);
				result.put(ERROR, false);
				result.put(REPORT_PATH, outputPath);
				/*vuongmq; 11/05/2015; xuat Excel ATTT*/
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				//common.export.excel.null = Không có dữ liệu để xuất file Excel
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.export.excel.null"));
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.exportExcelBySearchEquipmentStockPermission() " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Nhap Excel quyen kho thiet bi
	 * @author vuongmq
	 * @since Mar 23,2015
	 * @description Import Excel
	 * */
	public String importEquipStockPermission() {
		resetToken(result);
		isError = true;
		totalItem = 0;
		try {
			//Xu ly nghiep vu import file
			lstView = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 5);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				String message = "";
				String msg = "";
				//boolean flag = false;
				List<EquipmentRoleVOTempImport> lstRoleExcel = new ArrayList<EquipmentRoleVOTempImport>(); /** la mot quyen gon nhieu detail(kho)*/
				boolean  flagCodeRoleExists = false; /** ma role ton tai thi cung phai lay danh sach detail role view lên*/
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					message = "";
					msg = "";
					totalItem++;
					List<String> row = lstData.get(i);
					EquipmentRoleVOTempImport eRoleVO = new EquipmentRoleVOTempImport(); // la 1 record cho 1 dong trong excel
					//Ma quyen: equipment.role.code
					EquipRole equipRole = null;
					if(!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(4))) {
						if (lstRoleExcel != null && lstRoleExcel.size() > 0) {
							/** them moi lstRoleExcel, voi 1 quyen va nhieu danh sach kho */
							//EquipRole equipRoleCreate =  equipStockPermissionMgr.createEquipRoleAndDetail(lstRoleExcel, getLogInfoVO());
							equipStockPermissionMgr.createEquipRoleAndDetail(lstRoleExcel, getLogInfoVO());
							/** tạo mot quyen nhieu detail xong rui thi new lai lstRoleExcel*/
							lstRoleExcel = new ArrayList<EquipmentRoleVOTempImport>();
							flagCodeRoleExists = false;
						}
						String valueCodeRole = row.get(0);
						if (!StringUtil.isNullOrEmpty(valueCodeRole)) {
							valueCodeRole = valueCodeRole.trim();
							String errCodeRole = ValidateUtil.validateField(valueCodeRole, "equipment.role.code", 50, ConstantManager.ERR_MAX_LENGTH_EQUALS);
							if(!StringUtil.isNullOrEmpty(errCodeRole)){
								msg += errCodeRole;
							}
							errCodeRole = "";
							errCodeRole = ValidateUtil.getErrorMsgOfSpecialCharInCode(valueCodeRole, "equipment.role.code");
							if(!StringUtil.isNullOrEmpty(errCodeRole)){
								msg += errCodeRole;
							}
							equipRole = equipStockPermissionMgr.getEquipRoleByCodeEx(valueCodeRole, null);
							if (equipRole != null) {
								//common.exist.code = {0} đã tồn tại trong hệ thống.
								msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, valueCodeRole) +"\n";
								flagCodeRoleExists = true;
							}
							eRoleVO.setCode(valueCodeRole);
						} else {
							if(i == 0){
								// Ban chua nhap gia tri cho truong ma quyền
								msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("equipment.role.code")) + "\n";
							}
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						//Ten quyen : equip.manager.stock.role.name
						String valueNameRole = row.get(1);
						msg = "";
						if (!StringUtil.isNullOrEmpty(valueNameRole)) {
							valueNameRole = valueNameRole.trim();
							String errNameRole = ValidateUtil.validateField(valueNameRole, "equip.manager.stock.role.name", 250, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(errNameRole)) {
								msg += errNameRole;
							}
							eRoleVO.setName(valueNameRole);
						} else {
							// Ban chua nhap gia tri cho truong ten quyen
							msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("equip.manager.stock.role.name")) + "\n";
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						// trang thai: equipment.manager.group.status
						String valueStatus = row.get(4);
						ActiveType statusRole = null;
						msg = "";
						if (!StringUtil.isNullOrEmpty(valueStatus)) {
							valueStatus = valueStatus.trim();
							if (valueStatus.toUpperCase().equals(R.getResource("equipment.manager.status.running").trim().toUpperCase())
								 || valueStatus.toUpperCase().equals(R.getResource("equipment.manager.status.stopped").trim().toUpperCase())) {
								if (valueStatus.toUpperCase().equals(R.getResource("equipment.manager.status.running").trim().toUpperCase())) {
									statusRole = ActiveType.RUNNING;
								} else if (valueStatus.toUpperCase().equals(R.getResource("equipment.manager.status.stopped").trim().toUpperCase())) {
									statusRole = ActiveType.STOPPED;
								}							
							} else {
								// contract.invalid = {0} không hợp lệ
								msg = R.getResource("contract.invalid", R.getResource("equipment.manager.group.status")) + "\n ";
							}
							eRoleVO.setStatus(statusRole);
						} else {
							// Ban chua nhap gia tri cho truong trang thai
							msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("equipment.manager.group.status")) + "\n";
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						/*** Begin xu ly validate dong detail; truong hop la dong chinh co detail */
						// Ma kho: stock.update.stock.warehouse.code
						String valueStockCode = row.get(2);
						String valueCheckChild = row.get(3);
						msg = "";
						EquipStock equipStock = null;
						if (!StringUtil.isNullOrEmpty(valueStockCode)) {
							valueStockCode = valueStockCode.trim();
							/** ma kho khong duoc nhap ky tu dac biet*/
							String errStockRole = ValidateUtil.getErrorMsgOfSpecialCharInCode(valueStockCode, "stock.update.stock.warehouse.code");
							if(!StringUtil.isNullOrEmpty(errStockRole)){
								msg += errStockRole;
							}
							equipStock = equipmentManagerMgr.getEquipStockbyCode(valueStockCode);
							if (equipStock != null) {
								if (!ActiveType.RUNNING.equals(equipStock.getStatus())) {
									//common.catalog.status.in.active = {0} đang ở trạng thái không hoạt động.
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, valueStockCode) +"\n";
								}
								// id: equip_role_id; idcheck: EQUIP_STOCK_ID
								if (lstRoleExcel != null && lstRoleExcel.size() > 0) {
									//equip.manager.stock.role.err.assign.stock = {0} đã được gán cho quyền {1}
									/**luc nay chua co duoi DB nen chua check duoi DB dc; check tren VO cua 1 quyen*/
									boolean flagNotExists = false;
									for (EquipmentRoleVOTempImport roleVOCheck : lstRoleExcel) {
										if(roleVOCheck != null && roleVOCheck.getStockCode() != null && roleVOCheck.getStockCode().equals(valueStockCode)){
											msg += R.getResource("equip.manager.stock.role.err.assign.stock", valueStockCode, roleVOCheck.getCode()) +"\n";
											flagNotExists = true;
											continue;
										}
									}
									/** check ma kho tiep theo */
									/** ma kho khong trung; va truong hop (ma kho nay khong check va check don vi cap duoi)  thi cung kiem tra co phai la con cua kho cha co check viewUnder khong? lstRoleExcel */
									if (!flagNotExists) {
										Long IdShop = equipStock.getShop().getId(); /** id shop hien tai*/
										List<Long> lstLongShopParent = shopMgr.getListLongShopParentByShopId(IdShop);
										if (lstLongShopParent != null && lstLongShopParent.size() > 0) {
											for (EquipmentRoleVOTempImport roleCheck : lstRoleExcel) { /** danh sach id shop truoc do cua quyen nay*/
												if (roleCheck != null && ActiveType.RUNNING.getValue().equals(roleCheck.getViewUnder()) && roleCheck.getShop() != null) {
													if (lstLongShopParent.contains(roleCheck.getShop().getId())) {
														//equip.manager.stock.role.err.manage.child.check = Tồn tại Kho đơn vị cấp trên đã check chọn kho đơn vị cấp dưới 
														msg += R.getResource("equip.manager.stock.role.err.manage.child.check") +"\n";
														continue;
													}
												}
											}
										}
									}
									/** ma kho khong trung; va truong hop (ma kho nay check don vi cap duoi) thi kiem tra them con co ton tai hay khong? lstRoleExcel */
									if (!flagNotExists && !StringUtil.isNullOrEmpty(valueCheckChild) && "X".equals(valueCheckChild.toUpperCase())) {
										Long IdShop = equipStock.getShop().getId(); /** id shop hien tai*/
										List<Long> lstLongShopChild = shopMgr.getListLongShopChildByShopId(IdShop);
										if (lstLongShopChild != null && lstLongShopChild.size() > 0) {
											for (EquipmentRoleVOTempImport roleCheck : lstRoleExcel) { /** danh sach id shop truoc do cua quyen nay*/
												if (roleCheck != null && roleCheck.getShop() != null) {
													if (lstLongShopChild.contains(roleCheck.getShop().getId())) {
														//equip.manager.stock.manage.child.check.role.err = Tồn tại Kho đơn vị cấp dưới đã thuộc quyền này
														msg += R.getResource("equip.manager.stock.manage.child.check.role.err") +"\n";
														continue;
													}
												}
											}
										}
									}
								}
							} else {
								// {0} khong co trong he thong
								msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, valueStockCode) + "\n";
							}
							eRoleVO.setStockCode(valueStockCode);
							eRoleVO.setEquipStock(equipStock);
							if (equipStock != null) {
								eRoleVO.setShop(equipStock.getShop());
							}
						} else {
							// Ban chua nhap gia tri cho truong ma kho
							msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("stock.update.stock.warehouse.code")) + "\n";
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						// kho đơn vị cấp dưới
						msg = "";
						if (!StringUtil.isNullOrEmpty(valueCheckChild)) {
							if (!"X".equals(valueCheckChild.toUpperCase())) {
								msg += R.getResource("equip.manager.stock.role.err.manage.child.check.x") +"\n";
							}
							if (equipStock != null) {
								if (equipStock.getShop() != null) {
									if (equipStock.getShop().getType() != null && equipStock.getShop().getType().getSpecificType() != null && ShopSpecificType.NPP.getValue().equals(equipStock.getShop().getType().getSpecificType().getValue())) {
										//equip.manager.stock.role.err.manage.child = Kho đơn vị không được phép quản lý toàn bộ kho đơn vị cấp dưới
										msg += R.getResource("equip.manager.stock.role.err.manage.child") +"\n";
									}
								}
							}
							eRoleVO.setViewUnder(ActiveType.RUNNING.getValue());
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						/*** End xu ly validate dong detail; truong hop la dong chinh co detail */
					}  else { // end la dong chinh
						/*** Begin xu ly validate dong detail cua quyen */
						// Ma kho: stock.update.stock.warehouse.code
						String valueStockCode = row.get(2);
						String valueCheckChild = row.get(3);
						msg = "";
						EquipStock equipStock = null;
						if (!StringUtil.isNullOrEmpty(valueStockCode)) {
							valueStockCode = valueStockCode.trim();
							/** ma kho khong duoc nhap ky tu dac biet*/
							String errStockRole = ValidateUtil.getErrorMsgOfSpecialCharInCode(valueStockCode, "stock.update.stock.warehouse.code");
							if(!StringUtil.isNullOrEmpty(errStockRole)){
								msg += errStockRole;
							}
							equipStock = equipmentManagerMgr.getEquipStockbyCode(valueStockCode);
							if (equipStock != null) {
								if (!ActiveType.RUNNING.equals(equipStock.getStatus())) {
									//common.catalog.status.in.active = {0} đang ở trạng thái không hoạt động.
									msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, valueStockCode) +"\n";
								}
								// id: equip_role_id; idcheck: EQUIP_STOCK_ID
								if (lstRoleExcel != null && lstRoleExcel.size() > 0) {
									//equip.manager.stock.role.err.assign.stock = {0} đã được gán cho quyền {1}
									/**luc nay chua co duoi DB nen chua check duoi DB dc; check tren VO cua 1 quyen*/
									boolean flagNotExists = false;
									for (EquipmentRoleVOTempImport roleVOCheck : lstRoleExcel) {
										if (roleVOCheck != null && roleVOCheck.getStockCode() != null && roleVOCheck.getStockCode().equals(valueStockCode)){
											msg += R.getResource("equip.manager.stock.role.err.assign.stock", valueStockCode, roleVOCheck.getCode()) +"\n";
											flagNotExists = true;
											continue;
										}
									}
									/** check ma kho tiep theo */
									/** ma kho khong trung; va truong hop (ma kho nay khong check va check don vi cap duoi)  thi cung kiem tra co phai la con cua kho cha co check viewUnder khong? lstRoleExcel */
									if(!flagNotExists){
										Long IdShop = equipStock.getShop().getId(); /** id shop hien tai*/
										List<Long> lstLongShopParent = shopMgr.getListLongShopParentByShopId(IdShop);
										if(lstLongShopParent != null && lstLongShopParent.size() > 0){
											for (EquipmentRoleVOTempImport roleCheck : lstRoleExcel) { /** danh sach id shop truoc do cua quyen nay*/
												if (roleCheck != null && ActiveType.RUNNING.getValue().equals(roleCheck.getViewUnder()) && roleCheck.getShop() != null) {
													if (lstLongShopParent.contains(roleCheck.getShop().getId())) {
														//equip.manager.stock.role.err.manage.child.check = Tồn tại Kho đơn vị cấp trên đã check chọn kho đơn vị cấp dưới 
														msg += R.getResource("equip.manager.stock.role.err.manage.child.check") +"\n";
														continue;
													}
												}
											}
										}
									}
									/** ma kho khong trung; va truong hop (ma kho nay check don vi cap duoi) thi kiem tra them con co ton tai hay khong? lstRoleExcel */
									if (!flagNotExists && !StringUtil.isNullOrEmpty(valueCheckChild) && "X".equals(valueCheckChild.toUpperCase())) {
										Long IdShop = equipStock.getShop().getId(); /** id shop hien tai*/
										List<Long> lstLongShopChild = shopMgr.getListLongShopChildByShopId(IdShop);
										if (lstLongShopChild != null && lstLongShopChild.size() > 0) {
											for (EquipmentRoleVOTempImport roleCheck : lstRoleExcel) { /** danh sach id shop truoc do cua quyen nay*/
												if (roleCheck != null && roleCheck.getShop() != null) {
													if (lstLongShopChild.contains(roleCheck.getShop().getId())) {
														//equip.manager.stock.manage.child.check.role.err = Tồn tại Kho đơn vị cấp dưới đã thuộc quyền này
														msg += R.getResource("equip.manager.stock.manage.child.check.role.err") +"\n";
														continue;
													}
												}
											}
										}
									}
								}
							} else {
								// {0} khong co trong he thong
								msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, valueStockCode) + "\n";
							}
							eRoleVO.setStockCode(valueStockCode);
							eRoleVO.setEquipStock(equipStock);
							eRoleVO.setShop(equipStock.getShop());
						} else {
							// Ban chua nhap gia tri cho truong ma kho
							msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("stock.update.stock.warehouse.code")) + "\n";
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						// kho đơn vị cấp dưới
						msg = "";
						if (!StringUtil.isNullOrEmpty(valueCheckChild)) {
							if (!"X".equals(valueCheckChild.toUpperCase())) {
								msg += R.getResource("equip.manager.stock.role.err.manage.child.check.x") + "\n";
							}
							if (equipStock != null) {
								if (equipStock.getShop() != null) {
									if (equipStock.getShop().getType() != null && ShopSpecificType.NPP.getValue().equals(equipStock.getShop().getType().getSpecificType().getValue())) {
										//equip.manager.stock.role.err.manage.child = Kho đơn vị không được phép quản lý toàn bộ kho đơn vị cấp dưới
										msg += R.getResource("equip.manager.stock.role.err.manage.child") +"\n";
									}
								}
							}
							eRoleVO.setViewUnder(ActiveType.RUNNING.getValue());
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						}
						/*** End xu ly validate dong detail cua quyen */
					}
					//XU LY CAP NHAT lstRoleExcel; khi qua dong chinh moi thi them moi quyen quan ly nhung kho nao trong danh sach lstRoleExcel
					if (StringUtil.isNullOrEmpty(message) && !flagCodeRoleExists) { // flagCodeRoleExists; role ton tai DB, thi van lay detail vao lstView(lst loi)
						/*** thuc hien add EquipmentRoleVOTempImport eRoleVO vao lstRoleExcel; dung them mới  
						 * them moi trong truong hop: 1. Neu den dong la chinh cua ma quyen va lstRoleExcel != null;
						 * 							  2. hết danh sách import va lstRoleExcel != null
						 * them moi xong: lstRoleExcel gan ve new List moi
						 * */
						lstRoleExcel.add(eRoleVO);
					} else {
						lstView.add(StringUtil.addFailBean(row, message));
					}
				} // end for list danh sach import
				if (lstRoleExcel != null && lstRoleExcel.size() > 0) {
					/** them moi lstRoleExcel, voi 1 quyen va nhieu danh sach kho */
					//EquipRole equipRoleCreate =  equipStockPermissionMgr.createEquipRoleAndDetail(lstRoleExcel, getLogInfoVO());
					equipStockPermissionMgr.createEquipRoleAndDetail(lstRoleExcel, getLogInfoVO());
					/** tạo mot quyen nhieu detail xong rui thi new lai lstRoleExcel*/
					lstRoleExcel = new ArrayList<EquipmentRoleVOTempImport>();
					flagCodeRoleExists = false;
				}
				//Export error
				getOutputFailExcelFile(lstView, ConstantManager.EQUIP_STOCK_PERMISSION_IMPORT_FAIL);
			} else {
				isError = true;
				if(StringUtil.isNullOrEmpty(errMsg)){
					//customer.display.program.nodata = Không có dữ liệu import. 
					errMsg = R.getResource("customer.display.program.nodata");
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.importEquipStockPermission() " + e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	/**
	 * Kiem tra danh sach kho con
	 * 
	 * @author hoanv25
	 * @since March 26,2015
	 */
	public String checkParenIdStock() {
		try {
			EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = new EquipProposalBorrowFilter<EquipProposalBorrowVO>();
			//set cac tham so tim kiem		
			List<Long> lstAddStockId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstAddStock)) {
				for (String str : lstAddStock.split(",")) {
					lstAddStockId.add(Long.valueOf(str));
				}
			}

			if (lstAddStockId != null) {
				filter.setLstAddStockId(lstAddStockId);
			}
			//goi ham thuc thi tim kiem
			ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.checkParenIdStock(filter);
			if (objVO.getLstObject() != null) {
				check = objVO.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.checkAddStock() " + e.getMessage());
		}
		return JSON;
	}
	/**
	 * Kiem tra kho da chon co ton tai kho cha va kho con hay khong
	 * 
	 * @author hoanv25
	 * @since March 26,2015
	 */
	public String checkAddStock() {
		try {
			EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = new EquipProposalBorrowFilter<EquipProposalBorrowVO>();
			//set cac tham so tim kiem		
			List<Long> lstAddStockId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstAddStock)) {
				for (String str : lstAddStock.split(",")) {
					lstAddStockId.add(Long.valueOf(str));
				}
			}

			if (lstAddStockId != null) {
				filter.setLstAddStockId(lstAddStockId);
			}
			//goi ham thuc thi tim kiem
			ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.searchLstAddStockId(filter);
			if (objVO.getLstObject() != null) {
				equipments = objVO.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.checkAddStock() " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Kiem tra kho da chon va kho moi chon co trung chom hay khong
	 * 
	 * @author hoanv25
	 * @since March 26,2015
	 * @return check
	 */
	public String checkAddStockChild() {
		try {
			EquipProposalBorrowFilter<EquipProposalBorrowVO> filter = new EquipProposalBorrowFilter<EquipProposalBorrowVO>();
			//set cac tham so tim kiem			
			List<Long> lstAddStockId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstAddStock)) {
				for (String str : lstAddStock.split(",")) {
					lstAddStockId.add(Long.valueOf(str));
				}
			}
			if (lstAddStockId != null) {
				filter.setLstAddStockId(lstAddStockId);
			}
			List<Long> lstAddStockIdInsert = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstAddStockInsert)) {
				for (String str : lstAddStockInsert.split(",")) {
					lstAddStockIdInsert.add(Long.valueOf(str));
				}
			}
			if (lstAddStockIdInsert != null) {
				filter.setLstAddStockIdInsert(lstAddStockIdInsert);
			}
			//goi ham thuc thi tim kiem
			ObjectVO<EquipProposalBorrowVO> objVO = equipStockPermissionMgr.searchCheckAddStockChild(filter);
			if (objVO.getLstObject() != null) {
				check = objVO.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagerStockPermissionAction.checkAddStockChild() " + e.getMessage());
		}
		return JSON;
	}

	// TODO Khai bai cac thuoc tinh GETTER/SETTER
	/**
	 * Khai bao cac tham bien
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	private Long id;

	private Integer status;

	private String code;
	private String name;
	private String shopCode;
	private String shopName;
	private String stockCode;
	private String stockName;
	private String description;
	private String lstEquipId;
	private String lstCheckSearchStockAdd;
	private String lstInsert;
	private String lstStockInsert;
	private String lstEquipStockGirdDel;
	private String checkListStock;
	private String checkListAdd;
	private String checkListEdit;
	private String noCheckListStock;
	private String mapIdStock;	
	private String lstObjectId;
	private String lstStockNow;
	private String mapIdEditStock;
	private String mapIdEditRoleStock;
	private String lstAddStock;
	private String lstEddStock;
	private String lstAddStockInsert;

	/** cap nhat VO */
/*	private List<EquipmentRoleStockTempVO> mapIdEditStock;*/
	private List<EquipmentRoleStockTempVO> checkListStockVO;
	private List<EquipmentRoleStockTempVO> noCheckListStockVO;
	private List<EquipProposalBorrowVO> equipments;
	private List<EquipProposalBorrowVO> check;

	/***vuongmq; 23/03/2015; khai bao import file*/
	private File excelFile;
	private String excelFileContentType;
	
	private EquipmentRoleVO equipmentRoleVO = new EquipmentRoleVO();
	/**
	 * Khai bao cac phuong thuc getter/ setter
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	public Long getId() {
		return id;
	}

	public Integer getStatus() {
		return status;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShopCode() {
		return shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public String getStockCode() {
		return stockCode;
	}

	public String getStockName() {
		return stockName;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLstEquipId() {
		return lstEquipId;
	}

	public void setLstEquipId(String lstEquipId) {
		this.lstEquipId = lstEquipId;
	}

	public String getLstObjectId() {
		return lstObjectId;
	}

	public void setLstObjectId(String lstObjectId) {
		this.lstObjectId = lstObjectId;
	}

	public String getCheckListStock() {
		return checkListStock;
	}

	public void setCheckListStock(String checkListStock) {
		this.checkListStock = checkListStock;
	}

	public String getNoCheckListStock() {
		return noCheckListStock;
	}

	public void setNoCheckListStock(String noCheckListStock) {
		this.noCheckListStock = noCheckListStock;
	}

	public EquipmentRoleVO getEquipmentRoleVO() {
		return equipmentRoleVO;
	}

	public void setEquipmentRoleVO(EquipmentRoleVO equipmentRoleVO) {
		this.equipmentRoleVO = equipmentRoleVO;
	}

	public String getLstEquipStockGirdDel() {
		return lstEquipStockGirdDel;
	}

	public void setLstEquipStockGirdDel(String lstEquipStockGirdDel) {
		this.lstEquipStockGirdDel = lstEquipStockGirdDel;
	}

	public String getLstStockNow() {
		return lstStockNow;
	}

	public void setLstStockNow(String lstStockNow) {
		this.lstStockNow = lstStockNow;
	}

	public List<EquipmentRoleStockTempVO> getCheckListStockVO() {
		return checkListStockVO;
	}

	public List<EquipmentRoleStockTempVO> getNoCheckListStockVO() {
		return noCheckListStockVO;
	}

	public void setCheckListStockVO(List<EquipmentRoleStockTempVO> checkListStockVO) {
		this.checkListStockVO = checkListStockVO;
	}

	public void setNoCheckListStockVO(List<EquipmentRoleStockTempVO> noCheckListStockVO) {
		this.noCheckListStockVO = noCheckListStockVO;
	}
	public String getMapIdStock() {
		return mapIdStock;
	}

	public void setMapIdStock(String mapIdStock) {
		this.mapIdStock = mapIdStock;
	}
	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<EquipProposalBorrowVO> getEquipments() {
		return equipments;
	}

	public void setEquipments(List<EquipProposalBorrowVO> equipments) {
		this.equipments = equipments;
	}

	public String getLstInsert() {
		return lstInsert;
	}

	public String getLstStockInsert() {
		return lstStockInsert;
	}

	public void setLstInsert(String lstInsert) {
		this.lstInsert = lstInsert;
	}

	public void setLstStockInsert(String lstStockInsert) {
		this.lstStockInsert = lstStockInsert;
	}

	public String getLstAddStock() {
		return lstAddStock;
	}

	public String getLstEddStock() {
		return lstEddStock;
	}

	public void setLstAddStock(String lstAddStock) {
		this.lstAddStock = lstAddStock;
	}

	public void setLstEddStock(String lstEddStock) {
		this.lstEddStock = lstEddStock;
	}

	public List<EquipProposalBorrowVO> getCheck() {
		return check;
	}

	public void setCheck(List<EquipProposalBorrowVO> check) {
		this.check = check;
	}

	public String getLstAddStockInsert() {
		return lstAddStockInsert;
	}

	public void setLstAddStockInsert(String lstAddStockInsert) {
		this.lstAddStockInsert = lstAddStockInsert;
	}

	public String getCheckListAdd() {
		return checkListAdd;
	}

	public void setCheckListAdd(String checkListAdd) {
		this.checkListAdd = checkListAdd;
	}

	public String getCheckListEdit() {
		return checkListEdit;
	}

	public void setCheckListEdit(String checkListEdit) {
		this.checkListEdit = checkListEdit;
	}

	public String getMapIdEditStock() {
		return mapIdEditStock;
	}

	public void setMapIdEditStock(String mapIdEditStock) {
		this.mapIdEditStock = mapIdEditStock;
	}

	public String getMapIdEditRoleStock() {
		return mapIdEditRoleStock;
	}

	public void setMapIdEditRoleStock(String mapIdEditRoleStock) {
		this.mapIdEditRoleStock = mapIdEditRoleStock;
	}

	public String getLstCheckSearchStockAdd() {
		return lstCheckSearchStockAdd;
	}

	public void setLstCheckSearchStockAdd(String lstCheckSearchStockAdd) {
		this.lstCheckSearchStockAdd = lstCheckSearchStockAdd;
	}
}

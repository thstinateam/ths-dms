package ths.dms.web.action.equipment;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.vo.EquipmentCreateLendSuggestVO;
import ths.dms.core.entities.vo.EquipmentLendSuggestVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.utils.LogUtility;

public class EquipmentCreateLendSuggestAction extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CustomerMgr customerMgr;
	
	
	
	@Override
	public void prepare() throws Exception {
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
//		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}

		if (shop != null) {
			shopId = shop.getId();
		}
		return SUCCESS;
	}

//	public String search(){
//		result.put("page", page);
//		result.put("max", max);
//		try{
//		List<EquipmentCreateLendSuggestVO> ls = new ArrayList<EquipmentCreateLendSuggestVO>();
//		
//		EquipmentCreateLendSuggestVO vo1 = new EquipmentCreateLendSuggestVO();
//		vo1.setLoaiThietBi("Tủ mát");
//		vo1.setDungTich("220");
//		vo1.setKhachHang("OCX");
//		vo1.setTenKhachHang("Chị Lý");
//		vo1.setNgDungTenMuonTu("Trần Văn A");
//		vo1.setQuanHe("Chính chủ");
//		vo1.setDienThoai("0904945612");
//		vo1.setSoNha("01");
//		vo1.setTinh("HCM");
//		vo1.setHuyen("Q.1");
//		vo1.setXa("VNM");
//		vo1.setDuong("Nguyễn Trãi");
//		vo1.setSoCMND("32546145");
//		vo1.setNoiCapCMND("Long An");
//		vo1.setNgayCapCMND("01/01/2015");
//		vo1.setGiayPhepDKKD("ĐKKD 123");
//		vo1.setNgayCapDKKD("01/01/2015");
//		vo1.setNoiCapDKKD("Long An");
//		vo1.setThoiGianNhanTu("01/01/2015");
//		vo1.setTinhTrang("Mới 100%");
//		vo1.setKhoXuat("Kho NCC");
//		vo1.setDoanhSoTB("10,000,000");
//		ls.add(vo1);
//		
//		EquipmentCreateLendSuggestVO vo2 = new EquipmentCreateLendSuggestVO();
//		vo2.setLoaiThietBi("Tủ mát");
//		vo2.setDungTich("220");
//		vo2.setKhachHang("0R1");
//		vo2.setTenKhachHang("Chị Hải");
//		vo2.setNgDungTenMuonTu("Lê Thị Quyền");
//		vo2.setQuanHe("Vợ");
//		vo2.setDienThoai("0904945345");
//		vo2.setSoNha("01");
//		vo2.setTinh("HCM");
//		vo2.setHuyen("Q.1");
//		vo2.setXa("P.12");
//		vo2.setDuong("Nguyễn Trãi");
//		vo2.setSoCMND("321646621");
//		vo2.setNoiCapCMND("Long An");
//		vo2.setNgayCapCMND("21/01/2015");
//		vo2.setGiayPhepDKKD("ĐKKD 123");
//		vo2.setNgayCapDKKD("21/01/2015");
//		vo2.setNoiCapDKKD("Cần Thơ");
//		vo2.setThoiGianNhanTu("21/01/2015");
//		vo2.setTinhTrang("Đã qua sử dụng");
//		vo2.setKhoXuat("Kho Cty");
//		vo2.setDoanhSoTB("1,000,000");
//		ls.add(vo2);
//		
//		if (ls != null) {
//			result.put("rows", ls);
//		}
//		}catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
//		return JSON;
//	}
//	
}

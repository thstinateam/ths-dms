package ths.dms.web.action.equipment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.enumtype.ViewActionType;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import ths.dms.core.business.ApParamEquipMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipEvictionFormDtl;
import ths.dms.core.entities.EquipItem;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipRepairFormDtl;
import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.EquipStockEquipFilter;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipItemFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.EquipItemVO;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairFormVOList;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentExVO;
import ths.dms.core.entities.vo.EquipmentRepairFormDtlVO;
import ths.dms.core.entities.vo.EquipmentRepairPaymentRecord;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StockGeneralVO;
import ths.dms.core.memcached.MemcachedUtils;

public class EquipmentRepairManageAction extends AbstractAction {

	private static final Integer TYPE_VAT_TU = 1;
	private static final Integer TYPE_NHAN_CONG = 2;
	private static final String PAGING = "PAGING";
	private static final String PAGING_NO = "PAGING_NO";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	EquipmentManagerMgr equipmentManagerMgr;
	CustomerMgr customerMgr;
	EquipRecordMgr equipRecordMgr;
	ApParamEquipMgr apParamEquipMgr;
	
	private String customerAddr;
	private String customerName;
	private String customerCode;
	private String seri;
	private String equipCode;
	private String fromDate;
	private String toDate;
	private Integer statusAction;
	private Integer statusForm;
	private List<Long> lstShopId;
	private List<Long> lstId;
	private String strListShopId;
	private Long id;
	private File excelFile;
	private String excelFileContentType;
	private String shopCode;
	private String fromRePresentor;
	private String toRePresentor;
	private String stockCode;
	private String resultBB;
	private Integer viTri;
	private Long equipRepairId;
	private String newShopCode;
	private String newCustomerCode;
	private String formCode;
	private Integer status;
	private Integer statusPayment;
	EquipRepairForm equipRepairForm = new EquipRepairForm();
	private Integer isEdit;
	private String newAddress;
	private Long equipCategoryId;
	private Long equipGroupId;
	private List<String> lstEquipItemId;
	private List<String> lstBaoHanh;
	private List<String> lstDonGiaVT;
	private List<String> lstDonGiaNC;
	private List<String> lstSoLuong;
	private List<String> lstSoLanSuaChua;
	private List<String> lstNgayBatDauBaoHanh; // vuongmq them vao 14/04/2015
	private List<String> lstNgayHetHanBaoHanh; // vuongmq them vao 14/04/2015
	private List<String> lstTongTienDetail; // vuongmq them vao 14/04/2015
	private List<String> lstDonGiaVTDinhMucTu; // vuongmq them vao 24/04/2015; lay hang muc theo luong dung tich
	private List<String> lstDonGiaVTDinhMucDen; // vuongmq them vao 24/04/2015; lay hang muc theo luong dung tich
	private List<String> lstDonGiaNCDinhMucTu; // vuongmq them vao 24/04/2015; lay hang muc theo luong dung tich
	private List<String> lstDonGiaNCDinhMucDen; // vuongmq them vao 24/04/2015; lay hang muc theo luong dung tich
	private List<String> lstDescription; // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
	
	private BigDecimal totalAmount;
	private Integer repairCountForm;
	private String downloadPath;
	/*** vuongmq; 14/04/2015*/
	private String expiredDate; // ngay het han bao hanh
	private BigDecimal workerPrice; // gia nhan cong
	
	private List<Map<String, Object>> listObjectBeans;

	private List<File> lstFile;
	private List<String> lstFileContentType;
	private List<String> lstFileFileName;
	private List<FileVO> lstFileVo;

	private String equipAttachFileStr;

	/** so seri */
	private String seriNumber;

	/** loai thiet bi */
	private Long categoryId;

	/** nhom thiet bi */
	private Long groupId;

	/** nha cung cap thiet bi */
	private Long providerId;

	/** nam san xuat thiet bi */
	private Integer yearManufacture;
	
	private String createDate;
	
	private String note;

	Long idForm;
	private String rejectReason;
	private String reason;
	private String condition;
	private List<EquipCategory> lstEquipCategory = new ArrayList<EquipCategory>();
	private List<EquipProvider> lstEquipProvider = new ArrayList<EquipProvider>();

	private EquipmentRepairPaymentRecord equipmentRepairPaymentRecord; // phieu thanh toan cho phieu sua chua thiet bi
	private EquipRepairPayForm equipmentRepairPayment; // phieu thanh toan

	@Override
	public void prepare() throws Exception {
		super.prepare();
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
		apParamEquipMgr = (ApParamEquipMgr) context.getBean("apParamEquipMgr");
		Date fDate = commonMgr.getDateBySysdateForNumberDay(-7, false);
		Date tDate = commonMgr.getSysDate();
		fromDate = DateUtil.toDateSimpleFormatString(fDate);
		toDate = DateUtil.toDateSimpleFormatString(tDate);
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}

		if (shop != null) {
			shopId = shop.getId();
		}
		EquipmentFilter<EquipCategory> filterEC = new EquipmentFilter<EquipCategory>();
		filterEC.setStatus(StatusType.ACTIVE.getValue());
		lstEquipCategory = equipmentManagerMgr.getListEquipmentCategoryByFilter(filterEC);
		EquipmentFilter<EquipProvider> filterEP = new EquipmentFilter<EquipProvider>();
		filterEP.setStatus(StatusType.ACTIVE.getValue());
		lstEquipProvider = equipmentManagerMgr.getListEquipProviderByFilter(filterEP);
		EquipmentFilter<EquipmentVO> filterG = new EquipmentFilter<EquipmentVO>();
		filterG.setStatus(StatusType.ACTIVE.getValue());
		result.put("lstEquipGroup", equipmentManagerMgr.searchEquipmentGroupVObyFilter(filterG).getLstObject());
		
		/** vuongmq; 15/04/2014; lay cau hinh cua ngay het han bao hanh cua chi tiet hang muc; 1: cho phep thay doi; 0: ko cho phep; If 1: thi xuat template co cot ngay het han bao hanh, ngc lai khong*/
		expiredDate = String.valueOf(ActiveType.STOPPED.getValue());
		ApParamEquip apParamEquip = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.NGAY_HET_HAN_BAO_HANH.getValue(), ApParamEquipType.NGAY_HET_HAN_BAO_HANH, ActiveType.RUNNING);
		if (apParamEquip != null && !StringUtil.isNullOrEmpty(apParamEquip.getValue())) {
			expiredDate = apParamEquip.getValue();
		}
		return SUCCESS;
	}

	/**
	 * Lay danh sach nhom thiet bi theo loai thiet bi
	 * 
	 * @author phuongvm
	 * @since 29/12/2014
	 * @description Chuc nang Quan ly sua chua
	 * */
	public String getListEquipGroupVOByCategry() {
		result.put("rows", new ArrayList<EquipmentVO>());
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setStatus(StatusType.ACTIVE.getValue());
			if (equipCategoryId != null && equipCategoryId > 0) {
				filter.setEquipCategoryId(equipCategoryId);
			}
			ObjectVO<EquipmentVO> obj = equipmentManagerMgr.searchEquipmentGroupVObyFilter(filter);
			if (obj != null && obj.getLstObject() != null && !obj.getLstObject().isEmpty()) {
				result.put("rows", obj.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.getListEquipGroupVOByCategry()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach phieu sua chua
	 * get Filter cho search va cho Excport danh sach QL sua chua
	 * @author vuongmq
	 * @since 20/04/2015
	 * @return
	 * lay theo phan quyen CMS khong lay theo quyen kho
	 */
	private EquipRepairFilter getFilterRepairSeach (String paging) {
		EquipRepairFilter filter = new EquipRepairFilter();
		try {
			if (PAGING.equals(paging)) {
				KPaging<EquipRepairFormVO> kPaging = new KPaging<EquipRepairFormVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				filter.setkPaging(kPaging);
			}
			if (currentUser.getShopRoot() != null) {
				filter.setShopRoot(currentUser.getShopRoot().getShopId());
			}
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipCode = equipCode.trim();
				filter.setEuqipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seri)) {
				seri = seri.trim();
				filter.setSeri(seri);
			}
			if (!StringUtil.isNullOrEmpty(formCode)) {
				formCode = formCode.trim();
				filter.setFormCode(formCode);
			}
			if (status != null && status > -1) {
				filter.setStatus(status);
			}
			if (statusPayment != null && statusPayment > -1) {
				filter.setStatusPayment(statusPayment);
			}
			if (equipCategoryId != null && equipCategoryId > -1) {
				filter.setEquipCategoryId(equipCategoryId);
			}
			if (equipGroupId != null && (equipGroupId > -1 || equipGroupId.intValue() == -2)) {
				filter.setEquipGroupId(equipGroupId);
			}
			filter.setStrListShopId(getStrListShopId());
			/*String strListShopId = shopId.toString();
			if (!StringUtil.isNullOrEmpty(strListShopId)) {
				filter.setStrListShopId(strListShopId);
			}*/
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.getFilterRepairSeach()"), createLogErrorStandard(actionStartTime));
		}
		return filter;
	}
	/**
	 * Tim kiem danh sach phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @return
	 */
	/**
	 * search QL sua chua
	 * cap nhat lay filter theo ham getFilterRepairSeach
	 * @author vuongmq
	 * @since 22/04/2015
	 */
	public String searchRepair() {
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			} else {
				if (currentUser.getShopRoot() == null) {
					// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			}
			// lay filter Repair
			EquipRepairFilter filter = this.getFilterRepairSeach(PAGING);
			ObjectVO<EquipRepairFormVO> temp = equipmentManagerMgr.getListEquipRepair(filter);
			if (temp != null && temp.getLstObject().size() > 0) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipRepairFormVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.searchRepair()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach chi tiet phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @return
	 */
	
	/**
	 * Lay danh sach chi tiet hang muc tren grid (icon_view)
	 * @author vuongmq
	 * @since 18/04/2015
	 * @return
	 */
	public String searchRepairDetail() {
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipRepairFormDtl>());
			EquipRepairFilter filter = new EquipRepairFilter();
			if (page != 0 && max != 0) {
				KPaging<EquipRepairFormDtl> kPagingDetail = new KPaging<EquipRepairFormDtl>();
				kPagingDetail.setPageSize(max);
				kPagingDetail.setPage(page - 1);
				filter.setkPagingDetail(kPagingDetail);
			}
			if (equipRepairId != null && equipRepairId > -1) {
				filter.setEquipRepairId(equipRepairId);
			}
			ObjectVO<EquipRepairFormDtl> temp = equipmentManagerMgr.getListEquipRepairDtlByEquipRepairId(filter);

			if (temp != null && temp.getLstObject() != null && temp.getLstObject().size() > 0) {
				if (temp.getkPaging() != null) {
					result.put("total", temp.getkPaging().getTotalRows());
				}
				result.put("rows", temp.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.searchRepairDetail()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Lay danh sach chi tiet hang muc tren man hinh cap nhat phieu sua chua
	 * @author phuongvm
	 * @since 08/01/2015
	 * @return
	 */
	
	/**
	 * Lay danh sach chi tiet hang muc tren man hinh cap nhat phieu sua chua (icon_edit)
	 * @author vuongmq
	 * @since 18/04/2015
	 * @return
	 */
	public String getRepairDetail() {
		try {
			EquipRepairFilter filter = new EquipRepairFilter();
			if (equipRepairId != null && equipRepairId > -1) {
				filter.setEquipRepairId(equipRepairId);
			}
			ObjectVO<EquipRepairFormDtl> temp = equipmentManagerMgr.getListEquipRepairDtlByEquipRepairId(filter);

			if (temp != null) {
				List<EquipmentRepairFormDtlVO> lstVO = new ArrayList<EquipmentRepairFormDtlVO>();
				for (EquipRepairFormDtl detail : temp.getLstObject()) {
					EquipmentRepairFormDtlVO vo = new EquipmentRepairFormDtlVO();
					// ngay bat dau bao hanh
					if (detail.getWarrantyDate() != null) {
						String ngayBDBH = DateUtil.toDateString(detail.getWarrantyDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
						vo.setNgayBatDauBaoHanh(ngayBDBH);
					}
					// ngay het han bao hanh
					if (detail.getWarrantyExpireDate() != null) {
						String ngayHHBH = DateUtil.toDateString(detail.getWarrantyExpireDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
						vo.setNgayHetHanBaoHanh(ngayHHBH);
					}
					//Lay hang muc ben EquipItemConfigDtl cau hinh
					if (detail.getEquipItem() != null && detail.getEquipItem().getId() != null) {
						vo.setHangMuc(detail.getEquipItem().getName());
						vo.setId(detail.getEquipItem().getId());
						/** BEgin Gan gia tri dataView cua Id hang muc */
						EquipItem equipItem = commonMgr.getEntityById(EquipItem.class, detail.getEquipItem().getId());
						EquipItemVO equipItemVO = new EquipItemVO();
						if (equipItem != null) {
							/** cac gia tri goc cua equipItem*/
							equipItemVO.setId(equipItem.getId());
							equipItemVO.setType(equipItem.getType());
							equipItemVO.setWarranty(equipItem.getWarranty());
						}
						/**cac gia tri cua detail lay lai*/
						equipItemVO.setName(detail.getEquipItem().getName());
						equipItemVO.setRepairCount(detail.getRepairCount());
						equipItemVO.setFromMaterialPrice(detail.getFromMaterialPrice());
						equipItemVO.setToMaterialPrice(detail.getToMaterialPrice());
						equipItemVO.setFromWorkerPrice(detail.getFromWorkerPrice());
						equipItemVO.setToWorkerPrice(detail.getToWorkerPrice());
						/** End Gan gia tri dataView cua Id hang muc */
						vo.setDataView(equipItemVO);
						/*
						EquipItemFilter filter2 = new EquipItemFilter();
						filter2.setEpuipItemId(detail.getEquipItem().getId());
						ObjectVO<EquipItemVO> objVO = equipmentManagerMgr.getListEquipItemPopUpRepair(filter2);
						if (objVO != null && objVO.getLstObject() != null && objVO.getLstObject().size() > 0) {
							// don gia vat tu dinh muc
							vo.setDonGiaVatTuDinhMuc(this.getVatTuNhanCongHangMuc(objVO.getLstObject().get(0),TYPE_VAT_TU));
							// don gia nhan dinh muc
							vo.setDonGiaNhanCongDinhMuc(this.getVatTuNhanCongHangMuc(objVO.getLstObject().get(0),TYPE_NHAN_CONG));
							// gan dataView cho record
							vo.setDataView(objVO.getLstObject().get(0));
						}*/
					}
					// don gia vat tu dinh muc; chuoi trên grid
					vo.setDonGiaVatTuDinhMuc(this.getVatTuNhanCongHangMuc(detail,TYPE_VAT_TU));
					// don gia nhan dinh muc; chuoi trên grid
					vo.setDonGiaNhanCongDinhMuc(this.getVatTuNhanCongHangMuc(detail,TYPE_NHAN_CONG));
					vo.setBaoHanh(detail.getWarranty());
					vo.setDonGiaNhanCong(detail.getWorkerPrice());
					vo.setDonGiaVatTu(detail.getMaterialPrice());
					vo.setLanSua(detail.getRepairCount());
					vo.setSoLuong(detail.getQuantity());
					vo.setDescription(detail.getDescription());
					vo.setTongTien(detail.getTotalAmount());
					lstVO.add(vo);
				}
				result.put("rows", lstVO);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.getRepairDetail()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * tra ve don gia vat tu, don gia nhan cong, de view len
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	private String getVatTuNhanCongHangMuc(EquipRepairFormDtl row, Integer type) {
		String value = "";
		if (row != null) {
			if (type == TYPE_VAT_TU) {
				if (row.getFromMaterialPrice() != null && row.getToMaterialPrice() != null) {
					//value = row.getFromMaterialPrice() + " - " + row.getToMaterialPrice();
					if (!row.getFromMaterialPrice().equals(row.getToMaterialPrice())) {
						value = row.getFromMaterialPrice() + " - " + row.getToMaterialPrice();
					} else {
						// truong hop bang nhau lay 1 gia tri
						value = row.getFromMaterialPrice().toString();
					}
				} else if (row.getFromMaterialPrice() != null) {
					value = ">= "+row.getFromMaterialPrice();
				} else if (row.getToMaterialPrice() != null) {
					value = "<= "+row.getToMaterialPrice();
				}
			} else if (type == TYPE_NHAN_CONG) {
				if (row.getFromWorkerPrice() != null && row.getToWorkerPrice() != null) {
					//value = row.getFromWorkerPrice() + " - " + row.getToWorkerPrice();
					if (!row.getFromWorkerPrice().equals(row.getToWorkerPrice())) {
						value = row.getFromWorkerPrice() + " - " + row.getToWorkerPrice();
					} else {
						// truong hop bang nhau lay 1 gia tri
						value = row.getFromWorkerPrice().toString();
					}
				} else if (row.getFromWorkerPrice() != null) {
					value = ">= "+row.getFromWorkerPrice();
				} else if (row.getToWorkerPrice() != null) {
					value = "<= "+row.getToWorkerPrice();
				}
			}
		}
		return value;
	}

	/**
	 * cap nhat bien ban
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @return
	 */
	
	/**
	 * cap nhat phieu sua chua
	 * @author vuongmq
	 * @since 18/04/2014
	 * them cac dieu kien de sen email,..
	 * @return
	 */
	public String updateStatus() {
		resetToken(result);
		String username = getCurrentUser().getUserName();
		List<FormErrVO> lstErr = new ArrayList<FormErrVO>();
		try {
			if (lstId != null && lstId.size() > 0) {
				for (int i = 0, sz = lstId.size(); i < sz; i++) {
					FormErrVO errVO = new FormErrVO();
					int status = 1;//status = 0 thi ko cap nhat
					int statusEmail = 1;
					int VALUE_UPDATE_NO_0 = 0; // KO update duoc
					int VALUE_UPDATE_2 = 2; // update duoc
					int VALUE_EMAIL_4 = 4; //cap nhat Sua chua: moi gui email; o	Dự thảo -> Chờ duyệt.; o	Không duyệt -> Chờ duyệt.
					EquipRepairForm equipRepairForm = equipmentManagerMgr.getEquipRepairFormById(lstId.get(i));
					if (equipRepairForm != null && statusForm != null && statusForm > ActiveType.DELETED.getValue()) {
						//StatusRecordsEquip statusUpdate = StatusRecordsEquip.parseValue(statusForm);
						StatusRecordsEquip statusUpdate = StatusRecordsEquip.parseValue(statusForm);
						if (StatusRecordsEquip.DRAFT.getValue().equals(equipRepairForm.getStatus().getValue())) {//du thao
							if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusUpdate.getValue()) || StatusRecordsEquip.CANCELLATION.getValue().equals(statusUpdate.getValue())) { //du thao -> cho duyet,huy
								equipRepairForm.setStatus(statusUpdate);
								if (StatusRecordsEquip.CANCELLATION.getValue().equals(statusUpdate.getValue())) {
									/** du thao -> huy; ***	Bật cở trạng thái tham gia giao dịch thiết bị từ {Đang giao dịch} về {Bình thường} ; Loai giao dịch = {Null} */
									status = VALUE_UPDATE_2;
								} else {
									/** du thao -> cho duyet; luc nay sen email */
									statusEmail = VALUE_EMAIL_4;
								}
							} else {// khong cap nhat duoc
								status = VALUE_UPDATE_NO_0;
							}
						} else if (StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipRepairForm.getStatus().getValue())) { // khong duyet
							if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusUpdate.getValue()) || StatusRecordsEquip.CANCELLATION.getValue().equals(statusUpdate.getValue())) { //khong duyet -> cho duyet,huy
								if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusUpdate.getValue())) {
									/** Không duyệt -> Chờ duyệt; luc nay send email */
									if (equipRepairForm.getEquip() != null ) {
										/** 	Chỉ khi cờ trạng thái tham gia giao dịch = {Bình thường} và trạng thái thiết bị = {Đang ở kho, Đang sử dụng} thì mới cho phép chuyển trạng thái.*/
										if (EquipTradeStatus.NOT_TRADE.getValue().equals(equipRepairForm.getEquip().getTradeStatus())
												&& (EquipUsageStatus.IS_USED.equals(equipRepairForm.getEquip().getUsageStatus()) 
														|| (EquipUsageStatus.SHOWING_WAREHOUSE.equals(equipRepairForm.getEquip().getUsageStatus())))) {
											/** •	Bật cở trạng thái tham gia giao dịch thiết bị từ {Bình thường } về {Đang giao dịch} ; •	Loại giao dịch = {Sửa chữa} */
											equipRepairForm.setStatus(statusUpdate);
											statusEmail = VALUE_EMAIL_4;
											status = VALUE_UPDATE_2;
										} else {
											status = VALUE_UPDATE_NO_0;
										}
									}
								} else {
									/** Không duyệt -> huy*/
									equipRepairForm.setStatus(statusUpdate);
								}
								equipRepairForm.setStatus(statusUpdate);
							} else {// khong cap nhat duoc
								status = VALUE_UPDATE_NO_0;
							}
						} else if (StatusRecordsEquip.APPROVED.getValue().equals(equipRepairForm.getStatus().getValue())) { // duyet
							if (StatusRecordsEquip.ARE_CORRECTED.getValue().equals(statusUpdate.getValue())) { //duyet -> sua chua
								equipRepairForm.setStatus(statusUpdate);
							} else {// khong cap nhat duoc
								status = VALUE_UPDATE_NO_0;
							}
						} else if (StatusRecordsEquip.ARE_CORRECTED.getValue().equals(equipRepairForm.getStatus().getValue())) { // sua chua
							if (StatusRecordsEquip.COMPLETION_REPAIRS.getValue().equals(statusUpdate.getValue())) { //sua chua -> hoan tat
								equipRepairForm.setStatus(statusUpdate);
								status = VALUE_UPDATE_2;
							} else {// khong cap nhat duoc
								status = VALUE_UPDATE_NO_0;
							}
						} else if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipRepairForm.getStatus().getValue())) { // cho duyet
							if (StatusRecordsEquip.APPROVED.getValue().equals(statusUpdate.getValue()) || StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusUpdate.getValue())) { //cho duyet -> duyet,khong duyet
								equipRepairForm.setStatus(statusUpdate);
								if (!StringUtil.isNullOrEmpty(rejectReason)) {
									equipRepairForm.setRejectReason(rejectReason);
								}
							} else if (StatusRecordsEquip.CANCELLATION.getValue().equals(statusUpdate.getValue())) {
								/** vuongmq; 19/06/2015 va co cho duyet -> huy */
								equipRepairForm.setStatus(statusUpdate);
								status = VALUE_UPDATE_2;
							} else {// khong cap nhat duoc
								status = VALUE_UPDATE_NO_0;
							}
						} else {
							status = VALUE_UPDATE_NO_0;
						}
						if (status > VALUE_UPDATE_NO_0) {
							equipRepairForm.setUpdateUser(username);
							//equipRepairForm.setUpdateDate(now); // update ben Mgr
							equipmentManagerMgr.updateEquipRepairFormStatus(equipRepairForm, null, status, null, null);
							/** gui email thong bao cho nguoi duyet voi noi dung 
							* •	Đơn vị [Mã đơn vị - Tên đơn vị] gửi phiếu yêu cầu sửa chữa thiết bị [Mã thiết bị - Tên nhóm thiết bị].
							* •	Chỗ này cấu hình trong bảng tham số email của đối tượng sẽ được gửi mail theo phân cấp VNM/Miền/Vùng/NPP. */
							if (statusEmail == VALUE_EMAIL_4) {
								// vuongmq; 21/05/2015; cho cap nhat nut luu danh sach; luc nay gui nguyen list cua tung danh sách lstId
								/*String err = this.sendEmailRepair(equipRepairForm);
								if (!StringUtil.isNullOrEmpty(err)) {
									result.put("errMsg", err);
									result.put(ERROR, true);
									return JSON;
								}*/
							}
						} else {
							errVO.setFormCode(equipRepairForm.getEquipRepairFormCode());
							errVO.setFormStatusErr(1);
							lstErr.add(errVO);
						}
					}
				}
			}
			result.put("lstErr", lstErr);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.updateStatus()"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach phieu sua chua man hinh duyet phieu
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @return
	 * --- cap nhat; vuongmq 25/05/2015; lay danh sach theo CMS khong theo phan quyen Kho
	 */
	public String searchRepairEx() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<EquipRepairFormVO> kPaging = new KPaging<EquipRepairFormVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipRepairFilter filter = new EquipRepairFilter();
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipCode = equipCode.trim();
				filter.setEuqipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seri)) {
				seri = seri.trim();
				filter.setSeri(seri);
			}
			if (!StringUtil.isNullOrEmpty(formCode)) {
				formCode = formCode.trim();
				filter.setFormCode(formCode);
			}
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				customerCode = customerCode.trim();
				filter.setCustomerCode(customerCode);
			}
			if (!StringUtil.isNullOrEmpty(customerName)) {
				customerName = customerName.trim();
				filter.setCustomerName(customerName);
			}
			/** bat buoc phai co strListShopId **/
			if (!StringUtil.isNullOrEmpty(strListShopId)) {
				filter.setStrListShopId(strListShopId);
			} else {
				if (currentUser.getShopRoot() != null && currentUser.getShopRoot().getShopId() != null) {
					filter.setStrListShopId(currentUser.getShopRoot().getShopId().toString());
				} else {
					// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			}
			ObjectVO<EquipRepairFormVO> temp = equipmentManagerMgr.getListEquipRepairEx(filter);

			if (temp != null && temp.getLstObject() != null && temp.getLstObject().size() > 0) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipRepairFormVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.searchRepairEx()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua
	 * 
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	
	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua, 
	 * lay theo phan quyen; popup thiet bi
	 * equipmentManagerMgr.getListEquipmentPopUp(equipmentFilter);
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	public String searchEquipment() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			KPaging<EquipmentDeliveryVO> kPaging = new KPaging<EquipmentDeliveryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(kPaging);
			/** vuongmq; 25/05/2015; them ma NPP; va staff dang nhap */
			if (currentUser != null) {
				equipmentFilter.setStaffRoot(currentUser.getUserId());
				if (currentUser.getShopRoot() != null) {
					equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
				} else {
					// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			} else {
				// loi khong ton tai user dang nhap
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (categoryId != null) {
				equipmentFilter.setEquipCategoryId(categoryId);
			}
			if (groupId != null) {
				equipmentFilter.setEquipGroupId(groupId);
			}
			if (providerId != null) {
				equipmentFilter.setEquipProviderId(providerId);
			}
			if (yearManufacture != null) {
				equipmentFilter.setYearManufacture(yearManufacture);
			}
			if (!StringUtil.isNullOrEmpty(stockCode)) {
				equipmentFilter.setStockCode(stockCode);
			}
			//strListShopId = shopId.toString();
			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			//equipmentFilter.setUsageStatus(usageStatus);//2,3
			//equipmentFilter.setStrListShopId(strListShopId);
			//ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentDeliveryVOByFilterEx(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentPopUp(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getkPaging().getTotalRows());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.searchEquipment()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	// co ngay het han bao hanh thi: rowSize = 12; kiem tra 0 -> 11
	public boolean isEmtyRow(List<String> row) {
		//them cot ghi chu: rowSize = 13
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4))
				|| !StringUtil.isNullOrEmpty(row.get(5)) || !StringUtil.isNullOrEmpty(row.get(6)) || !StringUtil.isNullOrEmpty(row.get(7)) || !StringUtil.isNullOrEmpty(row.get(8))
				|| !StringUtil.isNullOrEmpty(row.get(9)) || !StringUtil.isNullOrEmpty(row.get(10)) || !StringUtil.isNullOrEmpty(row.get(11)) || !StringUtil.isNullOrEmpty(row.get(12))) {
			return false;
		}
		return true;
	}
	
	//Ko co ngay het han bao hanh thi: rowSize = 11; kiem tra 0 -> 10
	public boolean isEmtyRowSize12(List<String> row) {
		//them cot ghi chu: rowSize = 12
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4))
				|| !StringUtil.isNullOrEmpty(row.get(5)) || !StringUtil.isNullOrEmpty(row.get(6)) || !StringUtil.isNullOrEmpty(row.get(7)) || !StringUtil.isNullOrEmpty(row.get(8))
				|| !StringUtil.isNullOrEmpty(row.get(9)) || !StringUtil.isNullOrEmpty(row.get(10)) || !StringUtil.isNullOrEmpty(row.get(11))) {
			return false;
		}
		return true;
	}

//	private boolean isRowValid(List<String> row) {
//		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) 
//				|| !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) 
//				|| !StringUtil.isNullOrEmpty(row.get(4))) {
//			return false;
//		}
//		//neu co ngay het han: row cuoi 12 phai khac rong
//		if (ActiveType.RUNNING.getValue().equals(Integer.parseInt(expiredDate)) && !StringUtil.isNullOrEmpty(row.get(12))) {
//			return false;
//		}
//		// neu co ngay het han: row cuoi 11 phai khac rong
//		if (!StringUtil.isNullOrEmpty(row.get(11))) {
//			return false;
//		}
//
//		return true;
//	}
	
	/**
	 * Kiem tra dong bien ban
	 * 
	 * @author phuongvm
	 * @return true: la dong bien ban, false: la dong chi tiet
	 * @since 17/11/2015
	 */
	public boolean isANewRecord(List<String> row, boolean isHasCol12) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4))
				|| (isHasCol12 && !StringUtil.isNullOrEmpty(row.get(12)) || (!isHasCol12 && !StringUtil.isNullOrEmpty(row.get(11)))) ) {
			return true;
		}
		return false;
	}

	/**
	 * import phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 13/01/2015
	 * @return
	 */
	
	/**
	 * import phieu sua chua;
	 * co dong trong la het mot bien ban (phieu sua chua)
	 * expr
	 * @author vuongmq
	 * @since 21/04/2015
	 * @return
	 */
	public String importExcel() throws Exception {
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		boolean isNewRecord = true;
		boolean isValid = true;
		boolean isHasCol12 = true;
		Date now = DateUtil.now();
		String username = getCurrentUser().getUserName();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<CellBean> lstFailsInForm = new ArrayList<CellBean>();
		boolean isValidForm = true; //bien ban hop le (bao gom tat ca cac dong chi tiet)
		List<EquipRepairFormDtl> lstEquipRepairFormDtl = new ArrayList<EquipRepairFormDtl>();
		EquipRepairForm equipRepairForm = null;
		List<EquipItem> lstEquipItem = new ArrayList<EquipItem>();
		EquipItem equipItem = null;
		int numImportColumn = 13;
//		List<List<String>> lstData = getExcelDataExWithHeaderAndEmptyRow(excelFile, excelFileContentType, errMsg, numImportColumn);
		List<List<String>> lstData = getExcelDataWithHeader(excelFile, excelFileContentType, errMsg, numImportColumn);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 1) { // vi lay luon header
			try {
				//tamvnm: bo check ki hien tai
//				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
//					isError = true;
//					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//					return SUCCESS;
//				}
				/** vuongmq; 15/04/2014; lay cau hinh cua ngay het han bao hanh cua chi tiet hang muc; 1: cho phep thay doi; 0: ko cho phep; If 1: thi xuat template co cot ngay het han bao hanh, ngc lai khong*/
				expiredDate = String.valueOf(ActiveType.STOPPED.getValue());
				ApParamEquip apParamEquip = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.NGAY_HET_HAN_BAO_HANH.getValue(), ApParamEquipType.NGAY_HET_HAN_BAO_HANH, ActiveType.RUNNING);
				if (apParamEquip != null && !StringUtil.isNullOrEmpty(apParamEquip.getValue())) {
					expiredDate = apParamEquip.getValue();
				}
				String errExpiredDate = ValidateUtil.validateField(expiredDate, "equipment.ngay.het.han.bao.hanh", null, ConstantManager.ERR_INTEGER);
				if (!StringUtil.isNullOrEmpty(errExpiredDate)) {
					isError = true;
					//equip.lost.recorde.cau.hinh = Cấu hình
					errMsg = R.getResource("equip.lost.recorde.cau.hinh") + " " + errExpiredDate;
					return SUCCESS;
				}
				/** Kiem tra file co dung dinh dang hay khong; expiredDate: 1 co cot ngay het han bao hanh, ngc lai ko*/
				// column header
				//import.cttb.err.header = Biểu mẫu không đúng định dạng
				List<String> row0 = lstData.get(0); // dong header
				if (ActiveType.RUNNING.getValue().equals(Integer.parseInt(expiredDate))) {
					isHasCol12 = true;
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.repair.import");
					String[] lstHeaders = headers.split(";");
					for (int i = 0, len = lstHeaders.length; i < len; i++) {
						if (!StringUtil.isNullOrEmpty(row0.get(i)) && !StringUtil.isNullOrEmpty(lstHeaders[i]) 
							&& !row0.get(i).trim().equals(lstHeaders[i].trim())) {
							isError = true;
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "import.cttb.err.header");
							return SUCCESS;
						}
					} 
				} else {
					//dong header khong co ngay het han bao hanh
					isHasCol12 = false;
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.repair.import.no.cot.hethanbaohanh");
					String[] lstHeaders = headers.split(";");
					for (int i = 0, len = lstHeaders.length; i < len; i++) {
						if (!StringUtil.isNullOrEmpty(row0.get(i)) && !StringUtil.isNullOrEmpty(lstHeaders[i]) 
							&& !row0.get(i).trim().equals(lstHeaders[i].trim())) {
							isError = true;
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "import.cttb.err.header");
							return SUCCESS;
						}
					}
				}
				/** bat dau tiep dong du lieu; tu dong 1*/
				//Equipment equipmentMain = null; /** thiet bi dong chinh cua phieu;*/
				lstData.remove(0);
				//totalItem = totalItem - 1; /** vuongmq; 06/06/2015; cap nhat totalItem tru dong header **/ 
				Equipment equipment = null; /** thiet bi dong chinh cua phieu;*/
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						totalItem++;
						List<String> row = lstData.get(i);
						int rowSize = row.size();
						List<String> rowNext = null;
						if (i != lstData.size() - 1) {
							rowNext = lstData.get(i + 1);
						}
						if (isValid) {
							if (i == 0 || isANewRecord(row, isHasCol12)) { // check xem co phai la bien ban mới
								isNewRecord = true;
								if (rowNext != null && isEmtyRow(rowNext)) {
									isValid = false;
								}
								if (isValidForm && equipRepairForm != null && lstEquipRepairFormDtl != null && lstEquipRepairFormDtl.size() > 0) { // them moi
									equipRepairForm = this.addDoanhSoTB(equipRepairForm);
									equipmentManagerMgr.createEquipRepairFormByExcel(equipRepairForm, lstEquipRepairFormDtl);
									lstFailsInForm = new ArrayList<CellBean>();
								} else {
									lstFails.addAll(lstFailsInForm);
									lstFailsInForm = new ArrayList<CellBean>();
									isValidForm = true;
								}
								lstEquipItem = new ArrayList<EquipItem>();
								lstEquipRepairFormDtl = new ArrayList<EquipRepairFormDtl>();
								equipRepairForm = null; // sau khi qua mot dong trong (phieu moi) thi gan lai null
							} else {
								if (rowNext != null && isEmtyRow(rowNext)) {
									isValid = false;
								}
								isNewRecord = false;
							}
						} else {
							if (isValidForm && equipRepairForm != null && lstEquipRepairFormDtl != null && lstEquipRepairFormDtl.size() > 0) { // them moi 
								equipRepairForm = this.addDoanhSoTB(equipRepairForm);
								equipmentManagerMgr.createEquipRepairFormByExcel(equipRepairForm, lstEquipRepairFormDtl);
								lstFailsInForm = new ArrayList<CellBean>();
							} else {
								lstFails.addAll(lstFailsInForm);
								lstFailsInForm = new ArrayList<CellBean>();
								isValidForm = true;
							}
							lstEquipItem = new ArrayList<EquipItem>();
							lstEquipRepairFormDtl = new ArrayList<EquipRepairFormDtl>();
							equipRepairForm = null; // sau khi qua mot dong trong (phieu moi) thi gan lai null
							lstFailsInForm.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
							continue;
						}
						
						StatusRecordsEquip statusRecord = null;
						EquipRepairFormDtl equipRepairFormDtl = null;
						String lyDoDeNghi = null;
						String tinhTrangHuHong = null;
						Date ngayBatDaubaoHanh = null;
						Long baoHanh = null;
						int VALUE_30_NGAY = 30;
						Date ngayHetHanBaoHanh = null;
						BigDecimal donGiaVatTu = null;
						BigDecimal donGiaNhanCong = null;
						Long soLuong = null;
						Date ngayLap = null;
						String noteStr = null;
//						EquipPeriod equipPeriod = null;
						message = "";
						if (isNewRecord) { //Neu la bien ban moi 
							// ** Mã thiết bị */
							if (rowSize > 0) {
								String value = row.get(0);
								String msg = ValidateUtil.validateField(value, "device.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + "\n";
								} else {
									value = value.trim().toUpperCase();
									//equipment = equipmentManagerMgr.getEquipmentEntityByCode(value);
									EquipmentFilter<Equipment> filterE = new EquipmentFilter<Equipment>();
									/** vuongmq; 27/05/2015; them ma NPP; va staff dang nhap */
									if (currentUser != null) {
										filterE.setStaffRoot(currentUser.getUserId());
										if (currentUser.getShopRoot() != null) {
											filterE.setShopRoot(currentUser.getShopRoot().getShopId());
										} else {
											// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
											isError = true;
											errMsg = R.getResource("common.cms.shop.undefined");
											return SUCCESS;
										}
									} else {
										// loi khong ton tai user dang nhap
										isError = true;
										errMsg = R.getResource("common.not.login");
										return SUCCESS;
									}
									filterE.setCode(value); // value la: equipCode
									//filterE.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
									//filterE.setTradeType(null);
									filterE.setFlagNotUsageStatus(true); // vuongmq; 03/07/2015; khong lay trang thai su dung va o kho
									/** vuongmq; 27/05/2015; phai co staff dang nhap va shopRoot*/
									equipment = equipmentManagerMgr.getEquipmentByCodeFilter(filterE);
									//equipmentMain = equipment; /** gan vao thiet bi dong chinh, de dong chi tiet lay ra*/
									if (equipment == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code");
									} else if (!ActiveType.RUNNING.getValue().equals(equipment.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code");
									} else if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) { //trade_status = 0 thi moi cho lap bien ban
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.psc");
									} else if (!EquipUsageStatus.IS_USED.getValue().equals(equipment.getUsageStatus().getValue()) && !EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.su.dung.or.not.in.kho");
									}
									/*** kiem tra thiet bi co het han bao hanh khong? */
									/*if (equipment != null && equipment.getWarrantyExpiredDate() != null) {
										if (DateUtil.compareDateWithoutTime(equipment.getWarrantyExpiredDate(), DateUtil.now())  != -1) {
											*//** lay thiết bị phải đã hết bảo hành; WarrantyExpiredDate() >= now(): 0, 1 thi con han bao hanh, thong bao loi*//*
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.phai.het.han.bao.hanh") + "\n";
										}
									}*/
								}
							}

							// ** Trang thai bien ban*/
							if (rowSize > 1) {
								String value = row.get(1).trim();
								String msg = ValidateUtil.validateField(value, "report.status.label", 100, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + "\n";
								} else if (value.trim().toUpperCase().equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duthao").trim().toUpperCase())) {
									statusRecord = StatusRecordsEquip.DRAFT;
								} else if (value.trim().toUpperCase().equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.choduyet").trim().toUpperCase())) {
									statusRecord = StatusRecordsEquip.WAITING_APPROVAL;
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.phieu.sua.chua.trangthai");
								}
							}

							// ** Ngay lap */
							if (rowSize > 2) {
								String value = row.get(2);
								String msg = ValidateUtil.validateField(value, "equipment.create.form.date", 50, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}else{
									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date",null);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										ngayLap = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
										if (ngayLap == null) {
											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
										} else {
											if (DateUtil.compareTwoDate(ngayLap, now) == 1) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.big") + "\n";
//												
											} 
//											else {
//												//tamvnm: update dac ta moi. 30/06/2015
//												List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(ngayLap);
//												if (lstPeriod == null || lstPeriod.size() == 0) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//												} else if (lstPeriod.size() > 0) {
//													// gan gia tri ky dau tien.
//													equipPeriod = lstPeriod.get(0);
//												}
//											}
										}
									}
								}							
							}
							
							
							
							// ** tinh trang hu hong */
							if (rowSize > 3) {
								String value = row.get(3);
								if (!StringUtil.isNullOrEmpty(value)) {
									String msg = ValidateUtil.validateField(value, "equipment.manager.tinhtranghuhong", 500, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg + "\n";
									}
									if (message.length() == 0) {
										tinhTrangHuHong = value.trim();
									}
								}
							}
							

							// ** Ly do/de nghi */
							if (rowSize > 4) {
								String value = row.get(4);
								if (!StringUtil.isNullOrEmpty(value)) {
									String msg = ValidateUtil.validateField(value, "equipment.manager.lydodenghi", 500, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg + "\n";
									}
									if (message.length() == 0) {
										lyDoDeNghi = value.trim();
									}
								}
							}

							// ** Mã hạng mục */
							if (rowSize > 5) {
								String value = row.get(5);
								String msg = ValidateUtil.validateField(value, "device.code.hangmuc", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + "\n";
								} else {
									value = value.trim().toUpperCase();
									equipItem = equipmentManagerMgr.getEquipItemByCode(value);
									if (equipItem == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code.hangmuc");
									} else if (!ActiveType.RUNNING.getValue().equals(equipItem.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code.hangmuc");
									}
									if (equipItem != null && equipment != null) {
										/** Hạng mục phải chưa bảo hành hoặc đã hết hạn bảo hành*/
										EquipItemFilter filter = new EquipItemFilter();
										filter.setEpuipItemId(equipItem.getId());
										filter.setEquipId(equipment.getId());
										filter.setFlagWarrantyExpriedDate(true); // lay hang muc con hang bao hanh
										filter.setStatusRepair(StatusRecordsEquip.COMPLETION_REPAIRS); // hoan thanh sua chua
										EquipRepairFormDtl equipRepairFormDtl2 = equipmentManagerMgr.getEquipRepairFormDtlByFilter(filter);
										if (equipRepairFormDtl2 != null) {
											message += R.getResource("equipment.row.item.phai.chua.bao.hanh.hoac.het.han.bao.hanh")+"\n";
										}
									}
								}
							}
							// ngay bat dau bao hanh
							if (rowSize > 6) {
								String value = row.get(6);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.ngay.bat.dau.bao.hanh", null, ConstantManager.ERR_INVALID_DATE);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										ngayBatDaubaoHanh = DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}
								}
							}
							// so thang bao hanh
							if (rowSize > 7) {
								String value = row.get(7);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.bao.hanh.thang", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										Long val = Long.valueOf(value.trim());
										if (val > 0) {
											baoHanh = val;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong") + "\n";
										}
									}
								}
							}
							/*** kiem tra co cot ngay het han bao hanh hay khong? */
							Integer cot1 = 8;
							Integer cot2 = 9;
							Integer cot3 = 10;
							Integer cot4 = 11;
							if (ActiveType.RUNNING.getValue().equals(Integer.parseInt(expiredDate))) {
								// co ngay het han bao hanh
								String value = row.get(8);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.ngay.het.han.bao.hanh", null, ConstantManager.ERR_INVALID_DATE);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										ngayHetHanBaoHanh = DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}
								}
								cot1 = 9;
								cot2 = 10;
								cot3 = 11;
								cot4 = 12;
							} else {
								// khong co ngay het han bao hanh
								if (ngayBatDaubaoHanh != null) {
									if (baoHanh == null) {
										if (equipItem != null &&  equipItem.getWarranty() != null) {
											baoHanh = Long.valueOf(equipItem.getWarranty());
										} else {
											baoHanh = 0L;
										}
									}
									ngayHetHanBaoHanh = DateUtil.addDate(ngayBatDaubaoHanh, Integer.valueOf((int) (baoHanh*VALUE_30_NGAY)));
								}
							}
							
							// don gia vat tu
							if (rowSize > cot1) {
								String value = row.get(cot1);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.gia.vat.tu", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										BigDecimal val = BigDecimal.valueOf(Long.parseLong(value.trim()));
										if (val.compareTo(BigDecimal.ZERO) == 1) {
											donGiaVatTu = val;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong2") + "\n";
										}
									}
								}
							}
							// don gia nhan cong
							if (rowSize > cot2) {
								String value = row.get(cot2);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.gia.nhan.cong", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										BigDecimal val = BigDecimal.valueOf(Long.parseLong(value.trim()));
										if (val.compareTo(BigDecimal.ZERO) == 1) {
											donGiaNhanCong = val;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong3") + "\n";
										}
									}
								}
							}
							// so luong
							if (rowSize > cot3) {
								String value = row.get(cot3);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.so.luong", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										Long val = Long.valueOf(value.trim());
										if (val > 0) {
											soLuong = val;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong4") + "\n";
										}
									}
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong5") + "\n";
								}
							}
							// ** Ghi chu */
							if (rowSize > cot4) {
								String value = row.get(cot4);
								String msg = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									noteStr = value;
								}
							}
							
							
							/*if (soLuong != null && donGiaVatTu == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong6") + "\n";
							}*/
							if (donGiaVatTu != null && soLuong == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong5") + "\n";
							}
							if (donGiaVatTu == null && soLuong == null && donGiaNhanCong == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong8") + "\n";
							}
							if (donGiaVatTu == null && donGiaNhanCong == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.khong.vat.tu.khong.nhan.cong") + "\n";
							}
							lstFailsInForm.add(StringUtil.addFailBean(row, message));
							
						} else {
							//nguoc lai no la tung dong chi tiet cua bien ban
							//if(lstEquipItem!=null && !lstEquipItem.isEmpty()){
							// ** Mã hạng mục */
							if (rowSize > 5) {
								String value = row.get(5);
								String msg = ValidateUtil.validateField(value, "device.code.hangmuc", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + "\n";
								} else {
									value = value.trim().toUpperCase();
									equipItem = equipmentManagerMgr.getEquipItemByCode(value);
									if (equipItem == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code.hangmuc");
									} else if (!ActiveType.RUNNING.getValue().equals(equipItem.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code.hangmuc");
									} else {
										if (lstEquipItem != null && !lstEquipItem.isEmpty()) {
											for (int k = 0; k < lstEquipItem.size(); k++) {
												if (equipItem.getCode().equals(lstEquipItem.get(k).getCode())) {
													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.phieu.sua.chua2");
													break;
												}
											}
										}
										if (equipment != null) {
											/** Hạng mục phải chưa bảo hành hoặc đã hết hạn bảo hành*/
											EquipItemFilter filter = new EquipItemFilter();
											filter.setEpuipItemId(equipItem.getId());
											filter.setEquipId(equipment.getId());
											filter.setFlagWarrantyExpriedDate(true); // lay hang muc con hang bao hanh
											filter.setStatusRepair(StatusRecordsEquip.COMPLETION_REPAIRS); // hoan thanh sua chua
											EquipRepairFormDtl equipRepairFormDtl2 = equipmentManagerMgr.getEquipRepairFormDtlByFilter(filter);
											if (equipRepairFormDtl2 != null) {
												message += R.getResource("equipment.row.item.phai.chua.bao.hanh.hoac.het.han.bao.hanh")+"\n";
											}											
										}
											
									}
								}
							}
							// ngay bat dau bao hanh
							if (rowSize > 6) {
								String value = row.get(6);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.ngay.bat.dau.bao.hanh", null, ConstantManager.ERR_INVALID_DATE);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										ngayBatDaubaoHanh = DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}
								}
							}
							// So thang bao hanh
							if (rowSize > 7) {
								String value = row.get(7);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.bao.hanh.thang", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										Long val = Long.valueOf(value.trim());
										if (val > 0) {
											baoHanh = val;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong") + "\n";
										}
									}
								}
							}
							/*** kiem tra co cot ngay het han bao hanh hay khong? */
							Integer cot1 = 8;
							Integer cot2 = 9;
							Integer cot3 = 10;
							if (ActiveType.RUNNING.getValue().equals(Integer.parseInt(expiredDate))) {
								// co ngay het han bao hanh
								String value = row.get(8);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.ngay.het.han.bao.hanh", null, ConstantManager.ERR_INVALID_DATE);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										ngayHetHanBaoHanh = DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}
								}
								cot1 = 9;
								cot2 = 10;
								cot3 = 11;
							} else {
								// khong co ngay het han bao hanh
								if (ngayBatDaubaoHanh != null) {
									if (baoHanh == null) {
										if (equipItem != null &&  equipItem.getWarranty() != null) {
											baoHanh = Long.valueOf(equipItem.getWarranty());
										} else {
											baoHanh = 0L;
										}
									}
									ngayHetHanBaoHanh = DateUtil.addDate(ngayBatDaubaoHanh, Integer.valueOf((int) (baoHanh*VALUE_30_NGAY)));
								}
							}
							// don gia vat tu
							if (rowSize > cot1) {
								String value = row.get(cot1);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.gia.vat.tu", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										BigDecimal val = BigDecimal.valueOf(Long.parseLong(value.trim()));
										if (val.compareTo(BigDecimal.ZERO) == 1) {
											donGiaVatTu = val;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong2") + "\n";
										}
									}
								}
							}
							// don gia nhan cong
							if (rowSize > cot2) {
								String value = row.get(cot2);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.gia.nhan.cong", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										BigDecimal val = BigDecimal.valueOf(Long.parseLong(value.trim()));
										if (val.compareTo(BigDecimal.ZERO) == 1) {
											donGiaNhanCong = val;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong3") + "\n";
										}
									}
								}
							}
							// so luong
							if (rowSize > cot3) {
								String value = row.get(cot3);
								if (!StringUtil.isNullOrEmpty(value.trim())) {
									String msg = ValidateUtil.validateField(value, "equipment.so.luong", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										Long val = Long.valueOf(value.trim());
										if (val > 0) {
											soLuong = val;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong4") + "\n";
										}
									}
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong5") + "\n";
								}
							}
							/*if (soLuong != null && donGiaVatTu == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong6") + "\n";
							}*/
							if (donGiaVatTu != null && soLuong == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong5") + "\n";
							}
							if (donGiaVatTu == null && soLuong == null && donGiaNhanCong == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.bao.hanh.thang.lon.hon.khong8") + "\n";
							}
							if (donGiaVatTu == null && donGiaNhanCong == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.khong.vat.tu.khong.nhan.cong") + "\n";
							}
							lstFailsInForm.add(StringUtil.addFailBean(row, message));
//							if (!StringUtil.isNullOrEmpty(message)) {
//								isValidForm = false;
//							}
						}
						if (StringUtil.isNullOrEmpty(message)) {
							if (isNewRecord) { //la bien ban moi
								lstEquipItem = new ArrayList<EquipItem>();
								lstEquipRepairFormDtl = new ArrayList<EquipRepairFormDtl>();
								equipRepairForm = new EquipRepairForm();
								equipRepairForm.setEquip(equipment);
								if (equipment != null) {
									equipRepairForm.setWarrantyExpireDate(equipment.getWarrantyExpiredDate());
								}
								equipRepairForm.setStatus(statusRecord);
								//								equipRepairForm.setCreateDate(now);
								equipRepairForm.setCreateUser(username);
								equipRepairForm.setCondition(tinhTrangHuHong);
								equipRepairForm.setReason(lyDoDeNghi);
//								equipRepairForm.setEquipPeriod(equipPeriod);
								equipRepairForm.setCreateFormDate(ngayLap);
								equipRepairForm.setNote(noteStr);
								equipRepairForm.setStockCode(equipment.getStockCode());
								equipRepairForm.setStockId(equipment.getStockId());
								equipRepairForm.setStockType(equipment.getStockType());
							}
							equipRepairFormDtl = new EquipRepairFormDtl();
							//							equipRepairFormDtl.setCreateDate(now);
							equipRepairFormDtl.setCreateUser(username);
							equipRepairFormDtl.setEquipItem(equipItem);
							equipRepairFormDtl.setMaterialPrice(donGiaVatTu);
							equipRepairFormDtl.setWorkerPrice(donGiaNhanCong);
							equipRepairFormDtl.setWarrantyDate(ngayBatDaubaoHanh);
							equipRepairFormDtl.setWarranty(baoHanh);
							equipRepairFormDtl.setWarrantyExpireDate(ngayHetHanBaoHanh);
							if (soLuong != null) {
								equipRepairFormDtl.setQuantity(soLuong.intValue());
							}
							lstEquipItem.add(equipItem);
							lstEquipRepairFormDtl.add(equipRepairFormDtl);
						} else {
							isValidForm = false;
						}
					}
				}
				if (isValidForm && equipRepairForm != null && lstEquipRepairFormDtl != null && lstEquipRepairFormDtl.size() > 0) { // them moi phieu sua chua
					/** vuongmq; 06/06/2015; doanh so trung binh cua cau hinh so thang va nganh hang; chi ap dung cho kho Khach hang*/
					equipRepairForm = this.addDoanhSoTB(equipRepairForm);
					equipmentManagerMgr.createEquipRepairFormByExcel(equipRepairForm, lstEquipRepairFormDtl);
					//equipmentMain = null;
					for (int c = 0, csize = lstFailsInForm.size(); c < csize; c++) {
						if (!StringUtil.isNullOrEmpty(lstFailsInForm.get(c).getErrMsg())) {
							lstFails.add(lstFailsInForm.get(c));
						}
					}
					lstFailsInForm = new ArrayList<CellBean>();
				} else {
					lstFails.addAll(lstFailsInForm);
					lstFailsInForm = new ArrayList<CellBean>();
					isValidForm = true;
				}
				/** kiem tra expriedDate: 1: co ngay het han bao hanh, ngc lai khong*/
				if (ActiveType.RUNNING.getValue().equals(Integer.parseInt(expiredDate))) {
					// cai nay co ngay het han bao hanh
					getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_REPAIR_FAIL);
				} else {
					getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_REPAIR_ERR);
				}
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.importExcel()"), createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return SUCCESS;
			}
		} else {
			isError = true;
			//errMsg = "Tập tin Excel chưa nhập dữ liệu Import";
			errMsg = R.getResource("import.error.not.data");
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Chuyen trang tao moi hay chinh sua bien ban
	 * @author phuongvm
	 * @since 19/12/2014
	 */
	
	/**
	 * Tra ve tra lap phieu sua chua hay cap nhat phieu sua chua
	 * @author vuongmq
	 * @since 14/04/2015
	 * @return
	 */
	public String getChangeForm() {
		try {
			if (id != null && id > 0) {
				EquipRepairForm equipRepairFormEx = equipmentManagerMgr.getEquipRepairFormById(id);
				if (equipRepairFormEx != null) {
					if (equipRepairFormEx.getStockId() == null) {
						return PAGE_NOT_PERMISSION;
					}
					if (EquipStockTotalType.KHO.equals(equipRepairFormEx.getStockType())) {
						EquipStock eqStock = commonMgr.getEntityById(EquipStock.class, equipRepairFormEx.getStockId());
						if (eqStock == null || eqStock.getShop() == null || !checkShopInOrgAccessById(eqStock.getShop().getId())) {
							return PAGE_NOT_PERMISSION;
						}
					} else if (EquipStockTotalType.KHO_KH.equals(equipRepairFormEx.getStockType())) {
						Customer eqCus = commonMgr.getEntityById(Customer.class, equipRepairFormEx.getStockId());
						if (eqCus== null || eqCus.getShop() == null || !checkShopInOrgAccessById(eqCus.getShop().getId())) {
							return PAGE_NOT_PERMISSION;
						}
					}
					
					equipRepairForm = equipRepairFormEx;
					if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipRepairForm.getStatus().getValue()) || StatusRecordsEquip.COMPLETION_REPAIRS.getValue().equals(equipRepairForm.getStatus().getValue())
							|| StatusRecordsEquip.CANCELLATION.getValue().equals(equipRepairForm.getStatus().getValue())) { 
						/** 0: cho duyet,hoan tat, huy --> ko cho sua gi ca */
						isEdit = ActiveType.STOPPED.getValue(); 
					} else if (StatusRecordsEquip.APPROVED.getValue().equals(equipRepairForm.getStatus().getValue()) || StatusRecordsEquip.ARE_CORRECTED.getValue().equals(equipRepairForm.getStatus().getValue())) { 
						/** 2: duyet,sua chua --> chi cho doi trang thai */
						isEdit = ActiveType.WAITING.getValue();
					} else if (StatusRecordsEquip.DRAFT.getValue().equals(equipRepairForm.getStatus().getValue()) || StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipRepairForm.getStatus().getValue())) { 
						/** 1: du thao, ko duyet --> cho sua tat ca */
						isEdit = ActiveType.RUNNING.getValue();
					}
					idForm = id;
					if (EquipStockTotalType.KHO_KH.equals(equipRepairForm.getStockType())) {
						Customer cus = customerMgr.getCustomerById(equipRepairForm.getStockId());
						if (cus != null) {
							stockCode = cus.getShop().getShopCode() + cus.getShortCode() + " - " + cus.getCustomerName();
						}
					} else {
						/** Lay stockcode - name ben ban equipStock*/
						EquipStockEquipFilter<EquipStock> filter = new EquipStockEquipFilter<EquipStock>();
						filter.setStockId(equipRepairForm.getStockId());
						EquipStock equipStock = equipmentManagerMgr.getEquipStockByFilter(filter);
						if (equipStock != null) {
							stockCode = equipStock.getCode() + " - " + equipStock.getName();
						}
						/*Shop s = shopMgr.getShopById(equipRepairForm.getStockId());
						if (s != null) {
							stockCode = s.getShopCode() + " - " + s.getShopName();
						}*/
					}
					if (equipRepairForm.getCreateFormDate() != null) {
						createDate = DateUtil.convertDateByString(equipRepairForm.getCreateFormDate(),DateUtil.DATE_FORMAT_DDMMYYYY);
					} else {
//						Date lock = shopLockMgr.getLockedDay(currentUser.getShopRoot().getShopId());
//						if (lock != null) {
//							createDate = DateUtil.convertDateByString(lock,DateUtil.DATE_FORMAT_STR);
//						}
					}
					
					status = equipRepairForm.getStatus().getValue();
					//Lay danh sach tap tin dinh kem neu co
					EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
					filter.setObjectId(id);
					filter.setObjectType(EquipTradeType.CORRECTED.getValue());
					lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
					if (lstFileVo == null || lstFileVo.isEmpty()) {
						lstFileVo = new ArrayList<FileVO>();
					}
				} else {
					return PAGE_NOT_FOUND;
				}
			}
			/** vuongmq; 15/04/2014; lay cau hinh cua ngay het han bao hanh cua chi tiet hang muc; 1: cho phep thay doi; 0: ko cho phep*/
			expiredDate = String.valueOf(ActiveType.STOPPED.getValue());
			ApParamEquip apParamEquip = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.NGAY_HET_HAN_BAO_HANH.getValue(), ApParamEquipType.NGAY_HET_HAN_BAO_HANH, ActiveType.RUNNING);
			if (apParamEquip != null && !StringUtil.isNullOrEmpty(apParamEquip.getValue())) {
				expiredDate = apParamEquip.getValue();
			}
			
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.getChangeForm()"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}

	/**
	 * Chuyen trang tao moi hay chinh sua bien ban
	 * Chinh sua o MH phe duyet sua chua
	 * @author phuongvm
	 * @since 19/12/2014
	 */
	public String getChangeFormEx() {
		try {
			if (id != null && id > 0) {
				EquipRepairForm equipRepairFormEx = equipmentManagerMgr.getEquipRepairFormById(id);
				if (equipRepairFormEx != null) {
					equipRepairForm = equipRepairFormEx;
					status = equipRepairForm.getStatus().getValue();
					if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipRepairForm.getStatus().getValue())) { 
						/** 0: cho duyet --> ko cho sua gi ca */
						isEdit = ActiveType.STOPPED.getValue(); 
					}
					if (EquipStockTotalType.KHO_KH.getValue().equals(equipRepairForm.getStockType().getValue())) {
						Customer cus = customerMgr.getCustomerById(equipRepairForm.getStockId());
						if (cus != null) {
							stockCode = cus.getShop().getShopCode() + cus.getShortCode() + " - " + cus.getCustomerName();
						}
					} else {
						/** Lay stockcode - name ben ban equipStock*/
						EquipStockEquipFilter<EquipStock> filter = new EquipStockEquipFilter<EquipStock>();
						filter.setStockId(equipRepairForm.getStockId());
						EquipStock equipStock = equipmentManagerMgr.getEquipStockByFilter(filter);
						if (equipStock != null) {
							stockCode = equipStock.getCode() + " - " + equipStock.getName();
						}
						/*Shop s = shopMgr.getShopById(equipRepairForm.getStockId());
						if (s != null) {
							stockCode = s.getShopCode() + " - " + s.getShopName();
						}*/
					}
					if (equipRepairForm.getCreateFormDate() != null) {
						createDate = DateUtil.convertDateByString(equipRepairForm.getCreateFormDate(),DateUtil.DATE_FORMAT_DDMMYYYY);
					} else {
//						Date lock = shopLockMgr.getLockedDay(currentUser.getShopRoot().getShopId());
//						if (lock != null) {
//							createDate = DateUtil.convertDateByString(lock,DateUtil.DATE_FORMAT_STR);
//						}
					}
					
					//Lay danh sach tap tin dinh kem neu co
					EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
					filter.setObjectId(id);
					filter.setObjectType(EquipTradeType.CORRECTED.getValue());
					lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
					if (lstFileVo == null || lstFileVo.isEmpty()) {
						lstFileVo = new ArrayList<FileVO>();
					}
				} else {
					return PAGE_NOT_FOUND;
				}
				idForm = id;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.getChangeFormEx()"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}

	/**
	 * Lay mot vai danh muc cua thiet bi: nhom, ncc, loai
	 * 
	 * @author nhutnn
	 * @since 20/12/2014
	 * @return
	 */
	public String getEquipmentCatalog() {
		try {
			EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
			equipmentFilter.setkPaging(null);
			List<EquipmentVO> equipsCategory = new ArrayList<EquipmentVO>();
			List<EquipmentVO> equipProviders = new ArrayList<EquipmentVO>();
			equipsCategory = equipmentManagerMgr.getListEquipmentCategory(equipmentFilter);
			equipProviders = equipmentManagerMgr.getListEquipmentProvider(equipmentFilter);
			EquipmentFilter<EquipmentVO> filterG = new EquipmentFilter<EquipmentVO>();
			filterG.setStatus(StatusType.ACTIVE.getValue());
			result.put("lstEquipGroup", equipmentManagerMgr.searchEquipmentGroupVObyFilter(filterG).getLstObject());
			result.put("equipsCategory", equipsCategory);
			result.put("equipProviders", equipProviders);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.getEquipmentCatalog()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Lay nhom thiet bi
	 * 
	 * @author nhutnn
	 * @since 20/12/2014
	 * @return
	 */
	public String getEquipmentGroup() {
		try {
			EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
			equipmentFilter.setkPaging(null);
			List<EquipmentVO> equipGroups = new ArrayList<EquipmentVO>();
			if (id != null) {
				equipmentFilter.setEquipCategoryId(id);
			}
			equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
			ObjectVO<EquipmentVO> eGroup = equipmentManagerMgr.searchEquipmentGroupVObyFilter(equipmentFilter);
			if (eGroup.getLstObject() != null) {
				equipGroups = eGroup.getLstObject();
			}
			result.put("equipGroups", equipGroups);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.getEquipmentGroup()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/***
	 * @author vuongmq
	 * @date 06/06/2015
	 * @param equipRepairForm
	 * @return
	 * Ham lay doanh so TB theo cau hinh: nganh hang va so thang TB
	 */
	private EquipRepairForm addDoanhSoTB(EquipRepairForm equipRepairForm) {
		try {
			/** doanh so trung binh cua cau hinh so thang va nganh hang; chi ap dung cho kho Khach hang*/
			if (equipRepairForm != null & equipRepairForm.getEquip() != null) {
				Equipment equipment = equipRepairForm.getEquip();
				BigDecimal amount = BigDecimal.ZERO;
				ApParamEquip soThangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_THANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
				/** Truong hop khong cau hinh so thang thi setAmount = 0 */
				if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType()) && soThangParam != null && soThangParam.getValue() != null) {
					EquipRepairFilter filter = new EquipRepairFilter();
					filter.setCustomerId(equipment.getStockId());
					Integer soThang = null; // gan bang null, ben DAOImpl kiem tra khac null moi co dieu kien nay
					BigDecimal soThangBigDecimal = BigDecimal.ONE;
					if (soThangParam != null && soThangParam.getValue() != null) {
						soThang = Integer.valueOf(soThangParam.getValue());
						soThangBigDecimal = BigDecimal.valueOf(soThang);
					}
					filter.setSoThang(soThang);
					ApParamEquip nganhHangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_NGANH_HANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
					String nganhHangStr = "";
					if (nganhHangParam != null && nganhHangParam.getValue() != null) {
						nganhHangStr = nganhHangParam.getValue();
					}
					filter.setNganhHangStr(nganhHangStr);
					amount = (BigDecimal) equipmentManagerMgr.getDoanhSoEquipRepair(filter);
					if (!BigDecimal.ZERO.equals(soThangBigDecimal)) {
						amount = amount.divide(soThangBigDecimal, RoundingMode.CEILING);
						equipRepairForm.setAmount(amount);
					} else {
						equipRepairForm.setAmount(BigDecimal.ZERO);
					}
				} else {
					equipRepairForm.setAmount(BigDecimal.ZERO);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.addDoanhSoTB()"), createLogErrorStandard(actionStartTime));
		}
		return equipRepairForm;
	}

	/**
	 * Luu phieu sua chua
	 * @author phuongvm
	 * @since 07/01/2015
	 * @return
	 */
	
	/**
	 * cap nhat Luu phieu sua chua
	 * @author vuongmq
	 * @since 14/04/2015
	 * @return
	 */
	public String saveRepair() {
		resetToken(result);
		boolean error = true;
		errMsg = "";
		int statusCheck = 1;
		int statusErrEquipment = 1;
		int statusEmail = 1;
		try {
			List<Long> lstEquipAttachFileId = new ArrayList<Long>();
			Integer maxLength = 0;
			if (idForm != null && idForm > 0) {
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(idForm);
				filter.setObjectType(EquipTradeType.CORRECTED.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
				//Xu ly Xoa file dinh kem
				if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
					String[] arrEquipAttachFile = equipAttachFileStr.split(",");
					for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
						String value = arrEquipAttachFile[i];
						if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
							lstEquipAttachFileId.add(Long.parseLong(value));
						}
					}
				}
				maxLength = lstFileVo.size() - lstEquipAttachFileId.size();
				if (maxLength < 0) {
					maxLength = 0;
				}
			}
			errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}

			Date now = DateUtil.now();
			String username = getCurrentUser().getUserName();
			List<EquipRepairFormDtl> lstEquipRepairFormDtl = null;
			EquipRepairForm equipRepairForm = null;
			//Equipment equipment = null;
			int VALUE_UPDATE_2 = 2;
			int VALUE_DUTHAO_OR_KODUYET_3 = 3; // cap nhat sua chua: luc nay cho sua thiet bi, kem tra thông bao loi: lay thiet khi cap nhat ngc lai thi ko; phong tester nhap ma bang tay
			int VALUE_EMAIL_4 = 4; // cap nhat Sua chua: moi gui email; o	Dự thảo -> Chờ duyệt.; o	Không duyệt -> Chờ duyệt.
			if (StringUtil.isNullOrEmpty(errMsg)) {
				if (idForm != null && idForm > 0) {
					/** cap nhat phieu yeu cau sua chua*/
					StatusRecordsEquip statusUpdate = StatusRecordsEquip.DRAFT;
					equipRepairForm = equipmentManagerMgr.getEquipRepairFormById(idForm);
					equipRepairForm.setNote(note);
					if (equipRepairForm != null) {
						Equipment equipOld = equipRepairForm.getEquip();
						if (status != null && status > ActiveType.DELETED.getValue()) {
							statusUpdate = StatusRecordsEquip.parseValue(status);
						}
						if (StatusRecordsEquip.ARE_CORRECTED.equals(equipRepairForm.getStatus())) {//sua chua
							if (StatusRecordsEquip.APPROVED.equals(statusUpdate)) { //sua chua -> duyet -> loi~
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.equipment.repair");
								result.put("errMsg", errMsg);
								result.put(ERROR, error);
								return JSON;
							}
						} else if (StatusRecordsEquip.APPROVED.equals(equipRepairForm.getStatus())) {//duyet
							if (StatusRecordsEquip.COMPLETION_REPAIRS.equals(statusUpdate)) { //duyet -> hoan tat -> loi~
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.equipment.repair");
								result.put("errMsg", errMsg);
								result.put(ERROR, error);
								return JSON;
							}
						} else if (StatusRecordsEquip.NO_APPROVAL.equals(equipRepairForm.getStatus())) {//ko duyet
							statusErrEquipment = VALUE_DUTHAO_OR_KODUYET_3;
							if (StatusRecordsEquip.WAITING_APPROVAL.equals(statusUpdate)) { //ko duyet - > cho duyet
								if (equipRepairForm.getEquip() != null) {
									if (EquipTradeStatus.NOT_TRADE.getValue().equals(equipRepairForm.getEquip().getTradeStatus())
										 && (EquipUsageStatus.IS_USED.equals(equipRepairForm.getEquip().getUsageStatus()) 
												 || (EquipUsageStatus.SHOWING_WAREHOUSE.equals(equipRepairForm.getEquip().getUsageStatus())))) {
										statusCheck = VALUE_UPDATE_2;
										statusEmail = VALUE_EMAIL_4;
									} else {
										errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.equipment.repair.ex");
										result.put("errMsg", errMsg);
										result.put(ERROR, error);
										return JSON;
									}
								} else {
									errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.undefined");
									result.put("errMsg", errMsg);
									result.put(ERROR, error);
									return JSON;
								}
							}
						} else if (StatusRecordsEquip.DRAFT.equals(equipRepairForm.getStatus())) {//du thao
							statusErrEquipment = VALUE_DUTHAO_OR_KODUYET_3;
							if (StatusRecordsEquip.CANCELLATION.equals(statusUpdate)) { //du thao -> huy
								statusCheck = VALUE_UPDATE_2;
							}
							if (StatusRecordsEquip.WAITING_APPROVAL.equals(statusUpdate)) { //du thao -> cho duyet
								statusEmail = VALUE_EMAIL_4;
							}
						} else if (StatusRecordsEquip.WAITING_APPROVAL.equals(equipRepairForm.getStatus())) {//cho duyet
							if (StatusRecordsEquip.CANCELLATION.equals(statusUpdate)) { //cho duyet -> huy
								statusCheck = VALUE_UPDATE_2;
							}
						}
						if (StatusRecordsEquip.COMPLETION_REPAIRS.equals(statusUpdate)) {//hoan tat
							equipRepairForm.setCompleteDate(now);
						}
						equipRepairForm.setUpdateUser(username);
						equipRepairForm.setStatus(statusUpdate);

						Date cDate = null;
						if (!StringUtil.isNullOrEmpty(createDate)) {
							cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
						} else {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null"));
							result.put(ERROR, error);
							return JSON;
						}
						
						//tamvnm: update dac ta moi. 30/06/2015
						if (cDate != null) {
//							List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//							EquipPeriod equipPeriod = null;
//							if (lstPeriod == null || lstPeriod.size() == 0) {
//								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//								result.put(ERROR, error);
//								return JSON;
//							} else if (lstPeriod.size() > 0) {
//								// gan gia tri ky dau tien.
//								equipPeriod = lstPeriod.get(0);
//							}
//							equipRepairForm.setEquipPeriod(equipPeriod);
							equipRepairForm.setCreateFormDate(cDate);
						}
						
						if (!StringUtil.isNullOrEmpty(equipCode)) {
							equipCode = equipCode.trim().toUpperCase();
							//equipment = equipmentManagerMgr.getEquipmentEntityByCode(equipCode);
							if (equipRepairForm.getEquip() != null && !equipRepairForm.getEquip().getCode().equals(equipCode)) {
								// neu ma thiet bi moi khac thiet bi cu thi moi kiem tra set lai equipment
								EquipmentFilter<Equipment> filterE = new EquipmentFilter<Equipment>();
								/** vuongmq; 27/05/2015; them ma NPP; va staff dang nhap */
								if (currentUser != null) {
									filterE.setStaffRoot(currentUser.getUserId());
									if (currentUser.getShopRoot() != null) {
										filterE.setShopRoot(currentUser.getShopRoot().getShopId());
									} else {
										// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
										result.put("errMsg", R.getResource("common.cms.shop.undefined"));
										result.put(ERROR, error);
										return JSON;
									}
								} else {
									// loi khong ton tai user dang nhap
									result.put("errMsg", R.getResource("common.not.login"));
									result.put(ERROR, error);
									return JSON;
								}
								
								filterE.setCode(equipCode);
								filterE.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
								filterE.setTradeType(null);
								/** vuongmq; 27/05/2015; phai co staff dang nhap va shopRoot*/
								Equipment equipment = equipmentManagerMgr.getEquipmentByCodeFilter(filterE);
								if (equipment != null) {
									equipRepairForm.setEquip(equipment);
									equipRepairForm.setStockId(equipment.getStockId());
									equipRepairForm.setStockType(equipment.getStockType());
									equipRepairForm.setStockCode(equipment.getStockCode());
								} else {
									if (statusErrEquipment == VALUE_DUTHAO_OR_KODUYET_3) {
										//equip.repair.equip.code.trade.err = Mã thiết bị không đúng trạng thái, vui lòng nhấn F9
										errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.repair.equip.code.trade.err");
										result.put("errMsg", errMsg);
										result.put(ERROR, error);
										return JSON;
									}
								}
							}
							if (totalAmount != null) {
								equipRepairForm.setTotalAmount(totalAmount);
							}
							if (repairCountForm != null) {
								equipRepairForm.setRepairCount(repairCountForm);
							}
							if (!StringUtil.isNullOrEmpty(reason)) {
								equipRepairForm.setReason(reason);
							}
							if (!StringUtil.isNullOrEmpty(condition)) {
								equipRepairForm.setCondition(condition);
							}
							if (!StringUtil.isNullOrEmpty(rejectReason)) {
								equipRepairForm.setRejectReason(rejectReason);
							}
							/** ngay het han bao hanh*/
							if (!StringUtil.isNullOrEmpty(expiredDate)) {
								Date ngayHetHanBaoHanh = DateUtil.parse(expiredDate, DateUtil.DATE_FORMAT_DDMMYYYY);
								equipRepairForm.setWarrantyExpireDate(ngayHetHanBaoHanh);
							}
							/** gia nhan cong*/
							if (workerPrice != null ) {
								equipRepairForm.setWorkerPrice(workerPrice);
							} else {
								equipRepairForm.setWorkerPrice(BigDecimal.ZERO);
							}
							if (lstEquipItemId != null && lstEquipItemId.size() > 0 && lstBaoHanh != null && lstDonGiaVT != null && lstDonGiaNC != null && lstSoLuong != null && lstSoLanSuaChua != null
									&& !StatusRecordsEquip.CANCELLATION.getValue().equals(statusUpdate.getValue())) {
								lstEquipRepairFormDtl = new ArrayList<EquipRepairFormDtl>();
								for (int i = 0; i < lstEquipItemId.size(); i++) {
									EquipRepairFormDtl detail = new EquipRepairFormDtl();
									Long equipItemId = null;
									Date ngayBatDauBaoHanh = null;
									Date ngayHetHanBaoHanh = null;
									Long baoHanh = null;
									BigDecimal donGiaVT = BigDecimal.ZERO;
									BigDecimal donGiaNC = BigDecimal.ZERO;
									Integer soLuong = null;
									Integer soLanSuaChua = null;
									BigDecimal tongTien = BigDecimal.ZERO;
									BigDecimal donGiaVTDMTu = null;
									BigDecimal donGiaVTDMDen = null;
									BigDecimal donGiaNCDMTu = null;
									BigDecimal donGiaNCDMDen = null;
									String ghiChu = ""; // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
									if (!StringUtil.isNullOrEmpty(lstNgayBatDauBaoHanh.get(i))) {
										ngayBatDauBaoHanh = DateUtil.parse(lstNgayBatDauBaoHanh.get(i).trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}
									if (!StringUtil.isNullOrEmpty(lstNgayHetHanBaoHanh.get(i))) {
										ngayHetHanBaoHanh = DateUtil.parse(lstNgayHetHanBaoHanh.get(i).trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}
									if (!StringUtil.isNullOrEmpty(lstBaoHanh.get(i))) {
										baoHanh = Long.valueOf(lstBaoHanh.get(i).trim());
									}
									if (!StringUtil.isNullOrEmpty(lstDonGiaVT.get(i))) {
										//donGiaVT = Long.valueOf(lstDonGiaVT.get(i).trim());
										donGiaVT = BigDecimal.valueOf(Long.valueOf(lstDonGiaVT.get(i).trim()));
									}
									if (!StringUtil.isNullOrEmpty(lstDonGiaNC.get(i))) {
										//donGiaNC = Long.valueOf(lstDonGiaNC.get(i).trim());
										donGiaNC = BigDecimal.valueOf(Long.valueOf(lstDonGiaNC.get(i).trim()));
									}
									if (!StringUtil.isNullOrEmpty(lstSoLuong.get(i))) {
										soLuong = Integer.valueOf(lstSoLuong.get(i).trim());
									}
									if (!StringUtil.isNullOrEmpty(lstSoLanSuaChua.get(i))) {
										soLanSuaChua = Integer.valueOf(lstSoLanSuaChua.get(i).trim());
									}
									if (!StringUtil.isNullOrEmpty(lstTongTienDetail.get(i))) {
										//tongTien = Long.valueOf(lstTongTienDetail.get(i).trim());
										tongTien = BigDecimal.valueOf(Long.valueOf(lstTongTienDetail.get(i).trim()));
									}
									if (!StringUtil.isNullOrEmpty(lstEquipItemId.get(i))) {
										equipItemId = Long.valueOf(lstEquipItemId.get(i).trim());
									}
									if (equipItemId != null) {
										/** Luu hang muc vao*/
										EquipItem item = equipmentManagerMgr.getEquipItemById(equipItemId);
										if (item != null) {
											detail.setEquipItem(item);
										}
									}
									/** Don gia vat tu tu den va don gia nhan cong tu den*/
									if (!StringUtil.isNullOrEmpty(lstDonGiaVTDinhMucTu.get(i))) {
										donGiaVTDMTu = BigDecimal.valueOf(Long.valueOf(lstDonGiaVTDinhMucTu.get(i).trim()));
									}
									if (!StringUtil.isNullOrEmpty(lstDonGiaVTDinhMucDen.get(i))) {
										donGiaVTDMDen = BigDecimal.valueOf(Long.valueOf(lstDonGiaVTDinhMucDen.get(i).trim()));
									}
									if (!StringUtil.isNullOrEmpty(lstDonGiaNCDinhMucTu.get(i))) {
										donGiaNCDMTu = BigDecimal.valueOf(Long.valueOf(lstDonGiaNCDinhMucTu.get(i).trim()));
									}
									if (!StringUtil.isNullOrEmpty(lstDonGiaNCDinhMucDen.get(i))) {
										donGiaNCDMDen = BigDecimal.valueOf(Long.valueOf(lstDonGiaNCDinhMucDen.get(i).trim()));
									}
									// vuongmq; 01/07/2015; them ghi chu vuot dinh muc
									if (!StringUtil.isNullOrEmpty(lstDescription.get(i))) {
										ghiChu = lstDescription.get(i);
									}
									detail.setFromMaterialPrice(donGiaVTDMTu);
									detail.setToMaterialPrice(donGiaVTDMDen);
									detail.setFromWorkerPrice(donGiaNCDMTu);
									detail.setToWorkerPrice(donGiaNCDMDen);
									detail.setDescription(ghiChu); // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
									
									detail.setCreateUser(username);
									detail.setWarrantyDate(ngayBatDauBaoHanh);
									detail.setWarrantyExpireDate(ngayHetHanBaoHanh);
									detail.setMaterialPrice(donGiaVT);
									detail.setQuantity(soLuong);
									detail.setRepairCount(soLanSuaChua);
									detail.setWarranty(baoHanh);
									detail.setWorkerPrice(donGiaNC);
									detail.setTotalAmount(tongTien);
									lstEquipRepairFormDtl.add(detail);
								}
							}
							//if(lstEquipRepairFormDtl != null){
							EquipRecordFilter<EquipRepairForm> filter = new EquipRecordFilter<EquipRepairForm>();
							ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
							if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0) {
								BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
								//filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
								filterF.setLstId(lstEquipAttachFileId);
								filterF.setObjectId(idForm);
								filterF.setObjectType(EquipTradeType.CORRECTED.getValue());
								objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
							}
							//Luu file dinh kem
							filter.setLstEquipAttachFileId(lstEquipAttachFileId);
							filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));

							equipmentManagerMgr.updateEquipRepairFormStatus(equipRepairForm, lstEquipRepairFormDtl, statusCheck, equipOld, filter);
							error = false;
							//}
							if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0 && objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
								//Xu ly xoa cung file tren storeFileOnDisk
								List<String> lstFileName = new ArrayList<String>();
								for (EquipAttachFile itemFile : objVO.getLstObject()) {
									if (!StringUtil.isNullOrEmpty(itemFile.getUrl())) {
										lstFileName.add(itemFile.getUrl());
									}
								}
								FileUtility.deleteStoreFileOnDisk(lstFileName, Configuration.getFileEquipServerDocPath());
							}
							// cap nhat moi gui emai
							/** gui email thong bao cho nguoi duyet voi noi dung 
							* •	Đơn vị [Mã đơn vị - Tên đơn vị] gửi phiếu yêu cầu sửa chữa thiết bị [Mã thiết bị - Tên nhóm thiết bị].
							* •	Chỗ này cấu hình trong bảng tham số email của đối tượng sẽ được gửi mail theo phân cấp VNM/Miền/Vùng/NPP. */
							if (statusEmail == VALUE_EMAIL_4) {
								// vuongmq; 21/05/2015; cho cap nhat phieu sua chua o cho duyet
								/*String err = this.sendEmailRepair(equipRepairForm);
								if (!StringUtil.isNullOrEmpty(err)) {
									result.put("errMsg", err);
									result.put(ERROR, true);
									return JSON;
								}*/
							}
						}

					}
				} else {
					/** create  chi cho chon trang thai du thao va cho duyet */
					/** lay ky hien tai EquipPeriod */
//					EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//					if (equipPeriod == null || !equipPeriod.getStatus().equals(EquipPeriodType.OPENED)) {
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null"));
//						result.put(ERROR, error);
//						return JSON;
//					}
					equipRepairForm = new EquipRepairForm();
//					equipRepairForm.setEquipPeriod(equipPeriod);
					equipRepairForm.setCreateUser(username);
					equipRepairForm.setNote(note);
					if (!StringUtil.isNullOrEmpty(equipCode)) {
						equipCode = equipCode.trim().toUpperCase();
						//equipment = equipmentManagerMgr.getEquipmentEntityByCode(equipCode);
						EquipmentFilter<Equipment> filterE = new EquipmentFilter<Equipment>();
						/** vuongmq; 27/05/2015; them ma NPP; va staff dang nhap */
						if (currentUser != null) {
							filterE.setStaffRoot(currentUser.getUserId());
							if (currentUser.getShopRoot() != null) {
								filterE.setShopRoot(currentUser.getShopRoot().getShopId());
							} else {
								// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
								result.put("errMsg", R.getResource("common.cms.shop.undefined"));
								result.put(ERROR, error);
								return JSON;
							}
						} else {
							// loi khong ton tai user dang nhap
							result.put("errMsg", R.getResource("common.not.login"));
							result.put(ERROR, error);
							return JSON;
						}
						
						Date cDate = null;
						if (!StringUtil.isNullOrEmpty(createDate)) {
							cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
						} else {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null"));
							result.put(ERROR, error);
							return JSON;
						}
						
						//tamvnm: update dac ta moi. 30/06/2015
						if (cDate != null) {
//							List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//							EquipPeriod equipPeriod = null;
//							if (lstPeriod == null || lstPeriod.size() == 0) {
//								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//								result.put(ERROR, error);
//								return JSON;
//							} else if (lstPeriod.size() > 0) {
//								// gan gia tri ky dau tien.
//								equipPeriod = lstPeriod.get(0);
//							}
//							equipRepairForm.setEquipPeriod(equipPeriod);
							equipRepairForm.setCreateFormDate(cDate);
						}
						
						
						
						filterE.setCode(equipCode);
						filterE.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						filterE.setTradeType(null);
						/** vuongmq; 27/05/2015; phai co staff dang nhap va shopRoot*/
						Equipment equipment = equipmentManagerMgr.getEquipmentByCodeFilter(filterE);
						if (equipment != null) {
							equipRepairForm.setEquip(equipment);
							equipRepairForm.setStockId(equipment.getStockId());
							equipRepairForm.setStockType(equipment.getStockType());
							equipRepairForm.setStockCode(equipment.getStockCode());
							/** doanh so trung binh cua cau hinh so thang va nganh hang; chi ap dung cho kho Khach hang*/
							equipRepairForm = this.addDoanhSoTB(equipRepairForm);
							/*
							*//** doanh so trung binh cua cau hinh so thang va nganh hang; chi ap dung cho kho Khach hang*//*
							BigDecimal amount = BigDecimal.ZERO;
							ApParamEquip soThangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_THANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
							*//** Truong hop khong cau hinh so thang thi setAmount = 0 *//*
							if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType()) && soThangParam != null && soThangParam.getValue() != null) {
								EquipRepairFilter filter = new EquipRepairFilter();
								filter.setCustomerId(equipment.getStockId());
								Integer soThang = null; // gan bang null, ben DAOImpl kiem tra khac null moi co dieu kien nay
								BigDecimal soThangBigDecimal = BigDecimal.ONE;
								if (soThangParam != null && soThangParam.getValue() != null) {
									soThang = Integer.valueOf(soThangParam.getValue());
									soThangBigDecimal = BigDecimal.valueOf(soThang);
								}
								filter.setSoThang(soThang);
								ApParamEquip nganhHangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_NGANH_HANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
								String nganhHangStr = "";
								if (nganhHangParam != null && nganhHangParam.getValue() != null) {
									nganhHangStr = nganhHangParam.getValue();
								}
								filter.setNganhHangStr(nganhHangStr);
								amount = (BigDecimal) equipmentManagerMgr.getDoanhSoEquipRepair(filter);
								amount = amount.divide(soThangBigDecimal);
								equipRepairForm.setAmount(amount);
							} else {
								equipRepairForm.setAmount(amount);
							}*/
						} else {
							//equip.repair.equip.code.trade.err = Mã thiết bị không đúng trạng thái, vui lòng nhấn F9
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.repair.equip.code.trade.err");
							result.put("errMsg", errMsg);
							result.put(ERROR, error);
							return JSON;
						}
						if (status != null && status > ActiveType.DELETED.getValue()) { // du thao hoac cho duyet
							equipRepairForm.setStatus(StatusRecordsEquip.parseValue(status));
						}
						if (totalAmount != null) {
							equipRepairForm.setTotalAmount(totalAmount);
						}
						if (repairCountForm != null) {
							equipRepairForm.setRepairCount(repairCountForm);
						}
						if (!StringUtil.isNullOrEmpty(reason)) {
							equipRepairForm.setReason(reason);
						}
						if (!StringUtil.isNullOrEmpty(condition)) {
							equipRepairForm.setCondition(condition);
						}
						// tao moi khong co rejectReason
						if (!StringUtil.isNullOrEmpty(expiredDate)) {
							Date ngayHetHanBaoHanh = DateUtil.parse(expiredDate, DateUtil.DATE_FORMAT_DDMMYYYY);
							equipRepairForm.setWarrantyExpireDate(ngayHetHanBaoHanh);
						}
						if (workerPrice != null ) {
							equipRepairForm.setWorkerPrice(workerPrice);
						} else {
							equipRepairForm.setWorkerPrice(BigDecimal.ZERO);
						}
						/**danh sach chi tiet cua hang muc*/
						if (lstEquipItemId != null && lstEquipItemId.size() > 0 && lstBaoHanh != null && lstDonGiaVT != null && lstDonGiaNC != null && lstSoLuong != null && lstSoLanSuaChua != null) {
							lstEquipRepairFormDtl = new ArrayList<EquipRepairFormDtl>();
							for (int i = 0; i < lstEquipItemId.size(); i++) {
								EquipRepairFormDtl detail = new EquipRepairFormDtl();
								Long equipItemId = null;
								Date ngayBatDauBaoHanh = null;
								Date ngayHetHanBaoHanh = null;
								Long baoHanh = null;
								BigDecimal donGiaVT = BigDecimal.ZERO;
								BigDecimal donGiaNC = BigDecimal.ZERO;
								Integer soLuong = null;
								Integer soLanSuaChua = null;
								BigDecimal tongTien = BigDecimal.ZERO;
								BigDecimal donGiaVTDMTu = null;
								BigDecimal donGiaVTDMDen = null;
								BigDecimal donGiaNCDMTu = null;
								BigDecimal donGiaNCDMDen = null;
								String ghiChu = ""; // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
								if (!StringUtil.isNullOrEmpty(lstNgayBatDauBaoHanh.get(i))) {
									ngayBatDauBaoHanh = DateUtil.parse(lstNgayBatDauBaoHanh.get(i).trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
								}
								if (!StringUtil.isNullOrEmpty(lstNgayHetHanBaoHanh.get(i))) {
									ngayHetHanBaoHanh = DateUtil.parse(lstNgayHetHanBaoHanh.get(i).trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
								}
								if (!StringUtil.isNullOrEmpty(lstBaoHanh.get(i))) {
									baoHanh = Long.valueOf(lstBaoHanh.get(i).trim());
								}
								if (!StringUtil.isNullOrEmpty(lstDonGiaVT.get(i))) {
									//donGiaVT = Long.valueOf(lstDonGiaVT.get(i).trim());
									donGiaVT = BigDecimal.valueOf(Long.valueOf(lstDonGiaVT.get(i).trim()));
								}
								if (!StringUtil.isNullOrEmpty(lstDonGiaNC.get(i))) {
									//donGiaNC = Long.valueOf(lstDonGiaNC.get(i).trim());
									donGiaNC = BigDecimal.valueOf(Long.valueOf(lstDonGiaNC.get(i).trim()));
								}
								if (!StringUtil.isNullOrEmpty(lstSoLuong.get(i))) {
									soLuong = Integer.valueOf(lstSoLuong.get(i).trim());
								}
								if (!StringUtil.isNullOrEmpty(lstSoLanSuaChua.get(i))) {
									soLanSuaChua = Integer.valueOf(lstSoLanSuaChua.get(i).trim());
								}
								if (!StringUtil.isNullOrEmpty(lstTongTienDetail.get(i))) {
									//tongTien = Long.valueOf(lstTongTienDetail.get(i).trim());
									tongTien = BigDecimal.valueOf(Long.valueOf(lstTongTienDetail.get(i).trim()));
								}
								if (!StringUtil.isNullOrEmpty(lstEquipItemId.get(i))) {
									equipItemId = Long.valueOf(lstEquipItemId.get(i).trim());
								}
								if (equipItemId != null) {
									/** Luu hang muc vao*/
									EquipItem item = equipmentManagerMgr.getEquipItemById(equipItemId);
									if (item != null) {
										detail.setEquipItem(item);
									}
								}
								/** Don gia vat tu tu den va don gia nhan cong tu den*/
								if (!StringUtil.isNullOrEmpty(lstDonGiaVTDinhMucTu.get(i))) {
									donGiaVTDMTu = BigDecimal.valueOf(Long.valueOf(lstDonGiaVTDinhMucTu.get(i).trim()));
								}
								if (!StringUtil.isNullOrEmpty(lstDonGiaVTDinhMucDen.get(i))) {
									donGiaVTDMDen = BigDecimal.valueOf(Long.valueOf(lstDonGiaVTDinhMucDen.get(i).trim()));
								}
								if (!StringUtil.isNullOrEmpty(lstDonGiaNCDinhMucTu.get(i))) {
									donGiaNCDMTu = BigDecimal.valueOf(Long.valueOf(lstDonGiaNCDinhMucTu.get(i).trim()));
								}
								if (!StringUtil.isNullOrEmpty(lstDonGiaNCDinhMucDen.get(i))) {
									donGiaNCDMDen = BigDecimal.valueOf(Long.valueOf(lstDonGiaNCDinhMucDen.get(i).trim()));
								}
								// vuongmq; 01/07/2015; them ghi chu vuot dinh muc
								if (!StringUtil.isNullOrEmpty(lstDescription.get(i))) {
									ghiChu = lstDescription.get(i);
								}
								detail.setFromMaterialPrice(donGiaVTDMTu);
								detail.setToMaterialPrice(donGiaVTDMDen);
								detail.setFromWorkerPrice(donGiaNCDMTu);
								detail.setToWorkerPrice(donGiaNCDMDen);
								detail.setDescription(ghiChu); // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
								
								detail.setCreateUser(username);
								detail.setWarrantyDate(ngayBatDauBaoHanh);
								detail.setWarrantyExpireDate(ngayHetHanBaoHanh);
								detail.setMaterialPrice(donGiaVT);
								detail.setQuantity(soLuong);
								detail.setRepairCount(soLanSuaChua);
								detail.setWarranty(baoHanh);
								detail.setWorkerPrice(donGiaNC);
								detail.setTotalAmount(tongTien);
								lstEquipRepairFormDtl.add(detail);
							}
						}
						if (lstEquipRepairFormDtl != null && lstEquipRepairFormDtl.size() > 0) {
							EquipRecordFilter<EquipRepairForm> filter = new EquipRecordFilter<EquipRepairForm>();
							//Luu file dinh kem
							filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));
							equipmentManagerMgr.createEquipRepairForm(equipRepairForm, lstEquipRepairFormDtl, filter);
							error = false;
							// cap nhat moi gui emai
							/** gui email thong bao cho nguoi duyet voi noi dung 
							* •	Đơn vị [Mã đơn vị - Tên đơn vị] gửi phiếu yêu cầu sửa chữa thiết bị [Mã thiết bị - Tên nhóm thiết bị].
							* •	Chỗ này cấu hình trong bảng tham số email của đối tượng sẽ được gửi mail theo phân cấp VNM/Miền/Vùng/NPP. */
							if (statusEmail == VALUE_EMAIL_4) {
								// vuongmq; 21/05/2015; cho cap nhat phieu sua chua o cho duyet
								/*String err = this.sendEmailRepair(equipRepairForm);
								if (!StringUtil.isNullOrEmpty(err)) {
									result.put("errMsg", err);
									result.put(ERROR, true);
									return JSON;
								}*/
							}
						}
					} else {
						// ban chua gia tri cho truong ma thiet bi
						result.put("errMsg", R.getResource("common.required.field", R.getResource("device.code")));
						result.put(ERROR, error);
						return JSON;
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.saveRepair()"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * @author vuongmq
	 * @since 22/05/2015
	 * send email cho phieu sua chua o truong hop cho duyet
	 * */
//	private String sendEmailRepair(EquipRepairForm equipRepairForm) {
//		errMsg = "";
//		try {
//			// vuongmq; 21/05/2015; cho cap nhat phieu sua chua o cho duyet
//			ApParamEquip emailSend = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.EMAIL_SEND_REPAIR.getValue(), ApParamEquipType.EMAIL_SEND_REPAIR, ActiveType.RUNNING);
//			if (emailSend != null && !StringUtil.isNullOrEmpty(emailSend.getApParamEquipName()) && !StringUtil.isNullOrEmpty(emailSend.getValue())) {
//				// co name: ten email khac null va value: mat khau khac null moi send email
//				/** lay mail nhan tu equip_param */
//				Long shopId = null;
//				if (equipRepairForm != null && EquipStockTotalType.KHO.equals(equipRepairForm.getStockType())) {
//					EquipStock equipStock = equipmentManagerMgr.getEquipStockById(equipRepairForm.getStockId());
//					if (equipStock != null && equipStock.getShop() != null) {
//						shopId = equipStock.getShop().getId();
//					}
//				} else if (equipRepairForm != null && EquipStockTotalType.KHO_KH.equals(equipRepairForm.getStockType())) {
//					Customer customer = customerMgr.getCustomerById(equipRepairForm.getStockId());
//					if (customer != null && customer.getShop() != null) {
//						shopId = customer.getShop().getId();
//					}
//				}
//				// lay daanh sach mail can gui uu tien tu shop -> parent len
//				if (shopId != null) {
//					ApParamFilter paramFilter = new ApParamFilter();
//					paramFilter.setShopId(shopId);
//					paramFilter.setTypeStr(ApParamEquipType.EMAIL_TO_REPAIR.getValue());
//					List<ApParamEquipVO> lstEmailToVO = apParamEquipMgr.getListEmailToRepairByFilter(paramFilter);
//					if (lstEmailToVO != null && lstEmailToVO.size() > 0) {
//						//lay mail
//						String emailTo = "";
//						for (ApParamEquipVO apParamEquipVO : lstEmailToVO) {
//							if (apParamEquipVO != null && !StringUtil.isNullOrEmpty(apParamEquipVO.getApParamName())) {
//								emailTo = apParamEquipVO.getApParamName().trim();
//								break;
//							}
//						}
//						if (!StringUtil.isNullOrEmpty(emailTo)) {
//							//common.invalid.format.email = {0} nhập vào phải có định dạng email.
//							String errMailTo = ValidateUtil.validateField(emailTo, "common.email", null, ConstantManager.ERR_INVALID_EMAIL);
//							if (StringUtil.isNullOrEmpty(errMailTo)) {
//								// luc nay lay thong tin send Email
//								StringBuilder thongTin = new StringBuilder();
//								Shop shop = shopMgr.getShopById(shopId);
//								/**•	Đơn vị [Mã đơn vị - Tên đơn vị] gửi phiếu yêu cầu sửa chữa thiết bị [Mã thiết bị - Tên nhóm thiết bị]. */
//								//thongTin.append("Đơn vị ");
//								thongTin.append(R.getResource("equipment.repair.send.mail.don.vi"));
//								thongTin.append("[" +shop.getShopCode() + " - " + shop.getShopName() + "] ");
//								//thongTin.append("gửi phiếu yêu cầu sửa chữa thiết bị ");
//								thongTin.append(R.getResource("equipment.repair.send.mail.gui.phieu.sua.chua.tb"));
//								if (equipRepairForm != null && equipRepairForm.getEquip() != null && equipRepairForm.getEquip().getEquipGroup() != null) {
//									thongTin.append("[" +equipRepairForm.getEquip().getCode() + " - " + equipRepairForm.getEquip().getEquipGroup().getName() + "] ");
//								} else if (equipRepairForm != null && equipRepairForm.getEquip() != null ) {
//									thongTin.append("[" +equipRepairForm.getEquip().getCode() + "] ");
//								}
//								// luc nay se gui e mail
//								//Emailer emailer = new Emailer();
//								SendMailBySite emailer = new SendMailBySite();
//								StringBuilder body = new StringBuilder();
//								//body.append("Dear người phê duyệt, \n ");
//								body.append(R.getResource("equipment.repair.send.mail.gui.nguoi.phe.duyet"));
//								body.append(thongTin);
//								// ("\n Ma phieu sua chua: ")
//								if (!StringUtil.isNullOrEmpty(equipRepairForm.getEquipRepairFormCode())) {
//									body.append("\n").append(R.getResource("equipment.repair.send.mail.ma.phieu.sua.chua"));
//									body.append(equipRepairForm.getEquipRepairFormCode());
//								}
//								emailer.sendEmail(emailSend.getApParamEquipName(), emailSend.getValue(), emailTo, thongTin.toString(), body.toString());
//							} else {
//								//errMsg = "Param cấu hình email nhận không đúng định dạng.";
//								errMsg = R.getResource("equipment.repair.send.mail.nhan.khong.dung.dinh.dang");
//							}
//						} else {
//							//errMsg = "Param cấu hình email nhận không tồn tại.";
//							errMsg = R.getResource("equipment.repair.send.mail.nhan.khong.ton.tai");
//						}
//					} else {
//						//errMsg ="Param cấu hình không tồn tại email nhận.";
//						errMsg = R.getResource("equipment.repair.send.mail.nhan.khong.ton.tai.trong.cau.hinh");
//					}
//				} else {
//					//errMsg = "Đơn vị không tồn tại để send email phiếu Chờ duyệt.";
//					errMsg = R.getResource("equipment.repair.send.mail.don.vi.khong.ton.tai.de.send.email.cho.duyet");
//				}
//			} else {
//				//errMsg = "Email gửi không tồn tại hoặc không đúng password để gửi mail phiếu Chờ duyệt.";
//				errMsg = R.getResource("equipment.repair.send.mail.gui.khong.ton.tai.hoac.pass.khong.dung");
//			}
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.sendEmailRepair()"), createLogErrorStandard(actionStartTime));
//			//errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//			//errMsg = "Gửi mail người phê duyệt không thành công.";
//			errMsg = R.getResource("equipment.repair.send.mail.khong.thanh.cong");
//			
//		}
//		return errMsg;
//	}
	
	/**
	 * Upload File
	 * 
	 * @author hunglm16
	 * @since January 30,2015
	 * */
	private List<FileVO> saveUploadFileByEquip(List<File> lstFile, List<String> lstFileFileName, List<String> lstFileContentType) {
		try {
			if (lstFile != null && lstFile.size() > 0) {
				List<FileVO> lstFileVO = FileUtility.storeFileOnDisk(lstFile, lstFileFileName, lstFileContentType, null, EquipTradeType.CORRECTED.getValue());
				if (lstFileVO != null && !lstFileVO.isEmpty()) {
					String thumbUrl = Configuration.getEquipThumbUrlAttachFile();
					for (FileVO voFile : lstFileVO) {
						voFile.setThumbUrl(thumbUrl);
					}
				}
				return lstFileVO;
			}
			System.gc();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.saveUploadFileByEquip()"), createLogErrorStandard(actionStartTime));
		}
		return new ArrayList<FileVO>();
	}

	/**
	 * Lay danh sach thiet bi co trong bien ban giao nhan khach hang
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * @return
	 */
	public String getListEquipInCustomer() {
		try {
			result.put("rows", new ArrayList<StockGeneralVO>());
			if (!StringUtil.isNullOrEmpty(customerCode) && !StringUtil.isNullOrEmpty(shopCode)) {
				customerCode = StringUtil.getFullCode(shopCode, customerCode);
				Customer c = customerMgr.getCustomerByCode(customerCode);
				List<EquipmentExVO> lst = null;
				if (c != null) {
					if (id != null && id > -1) {
						List<EquipEvictionFormDtl> lstDetail = equipmentManagerMgr.getEquipEvictionFormDtlByEquipEvictionFormId(id);
						List<Long> lstEquipId = new ArrayList<Long>();
						if (lstDetail != null && lstDetail.size() > 0) {
							for (int i = 0; i < lstDetail.size(); i++) {
								lstEquipId.add(lstDetail.get(i).getEquip().getId());
							}
							result.put("lstEquipId", lstEquipId);
						}
						lst = equipmentManagerMgr.getListAllEquipmentDeliveryByCustomerId(c.getId(), lstEquipId);
					} else {
						lst = equipmentManagerMgr.getListEquipmentDeliveryByCustomerId(c.getId());
					}
					if (lst != null && lst.size() > 0) {
						result.put("rows", lst);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.getListEquipInCustomer()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * tra ve trang view lap phieu thanh toan: tao moi/chinh sua/xem
	 * 
	 * @author tuannd20
	 * @return trang lap phieu thanh toan
	 */
	
	/**
	 * tra ve trang view lap phieu thanh toan: tao moi/chinh sua/xem
	 * @author vuongmq
	 * cap nhat them tien: (tien nhan cong, vat tu, tong tien)
	 */
	public String fetchEquipmentRepairPaymentCreatePage() {
		generateToken();
		if (equipRepairId != null) {
			try {
				equipmentRepairPayment = equipmentManagerMgr.retrieveEquipmentRepairPayment(equipRepairId);
				if (equipmentRepairPayment == null) {
					setViewActionType(ViewActionType.CREATE);
				} else {
					setViewActionType(ViewActionType.EDIT);
				}
//				} else if (equipmentRepairPayment.getEquipPeriod() != null) {
//					if (equipmentRepairPayment.getEquipPeriod().getStatus() == EquipPeriodType.OPENED) {
//						setViewActionType(ViewActionType.EDIT);
//					} else {
//						setViewActionType(ViewActionType.READONLY);
//					}
//				}
				//List<EquipRepairPayFormDtl> equipmentRepairPaymentDetails = equipmentManagerMgr.retrieveEquipmentRepairPaymentDetails(equipRepairId);
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.fetchEquipmentRepairPaymentCreatePage()"), createLogErrorStandard(actionStartTime));
			}
		}
		return SUCCESS;
	}

	/**
	 * cap nhat phieu thanh toan cho phieu sua chua thiet bi
	 * 
	 * @author tuannd20
	 * @return ket qua cap nhat thanh cong/that bai, luu trong bien result
	 */
	
	/**
	 * cap nhat phieu thanh toan cho phieu sua chua thiet bi; 
	 * luc nay chi tao duoc thanh toan, khong cho sua nua, vi thanh toan xong la khong chinh sua nua
	 * @author vuongmq
	 * @return ket qua cap nhat thanh cong/that bai, luu trong bien result
	 */
	public String updateEquipmentRepairPaymentRecord() {
		try {
			//equipRepairId
			//equipmentRepairPaymentRecord
			if (equipRepairId == null || equipRepairId.compareTo(0l) <= 0) {
				// ma phieu sua chua ko hop le
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.repair.equip.code.payment.err"));
				result.put(ERROR, true);
				return JSON;
			}
			if (equipmentRepairPaymentRecord == null) {
				// du lieu ko hop le
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.repair.equip.data.payment.err"));
				result.put(ERROR, true);
				return JSON;
			} else {
				if (StringUtil.isNullOrEmpty(equipmentRepairPaymentRecord.getPaymentRecordCode())) {
					// common.required.field = Bạn chưa nhập giá trị cho trường {0}.
					result.put("errMsg", R.getResource("common.required.field", R.getResource("equip.repair.equip.code.payment")));
					result.put(ERROR, true);
					return JSON;
				}
				// Ma phieu ton tai trong DB; kiem tra trong MgrImpl
			}
			Date paymentDate = DateUtil.parse(equipmentRepairPaymentRecord.getPaymentDateInDDMMYYYFormat(), DateUtil.DATE_FORMAT_DDMMYYYY);
			equipmentRepairPaymentRecord.setPaymentDate(paymentDate);

			String updateMessage = equipmentManagerMgr.updateEquipmentRepairPaymentRecord(equipRepairId, equipmentRepairPaymentRecord, getLogInfoVO());
			if (StringUtil.isNullOrEmpty(updateMessage)) { // update success
				result.put(ERROR, false);
			} else { // update error
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, updateMessage));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.updateEquipmentRepairPaymentRecord()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * In phieu QL sua chua
	 * @author vuongmq
	 * @since 22/04/2015
	 * -- cap nhat: 28/05/2015; chi lay theo quyen CMS; ko theo quyen Kho
	 */
	public String printRepair() {
		try {
			if (currentUser != null) {
				/**ATTT duong dan*/
				String reportToken = retrieveReportToken(reportCode);
				if (StringUtil.isNullOrEmpty(reportToken)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
					return JSON;
				}
				/** Kiem tra du lieu */
				Staff st = getStaffByCurrentUser();
				result.put(ERROR, true);
				if (st == null || st.getStaffType() == null || st.getShop() == null) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					return JSON;
				}
				Shop sh = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				/** kiem tra quyen shopId */
				if (sh != null) {
					shopId = sh.getId();
				}
				if (shopId == null || !checkShopInOrgAccessById(shopId)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
					return JSON;
				}
				/** Kiem tra formatType xuat theo loai PDF */
				result.put(ERROR, true);
				/** Kiem tra co du lieu */
				//List<Rpt7_2_4_NVBH1VOList> lstData = shopReportMgr.P_7_2_4_CONGNO_NV_QUAHAN_1(sh.getId(), lstSalerId, staffIdRoot, flagCMS);
				EquipRepairFilter filter = new EquipRepairFilter();
				filter.setShopRoot(currentUser.getShopRoot().getShopId());
				if (lstId != null && lstId.size() > 0) {
					filter.setLstId(lstId);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.repair.print.not.check"));
					return JSON;
				}
				List<EquipRepairFormVOList> lstData = equipmentManagerMgr.getEquipRepairPrint(filter);
				if (lstData == null || lstData.size() == 0) {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				parametersReport = new HashMap<String, Object>();
				// get data for parameters
				parametersReport.put("cDate", DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY));
				parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
				parametersReport.put("sName", sh.getShopName());
				if (!StringUtil.isNullOrEmpty(sh.getAddress())) {
					parametersReport.put("sAddress", sh.getAddress());
				} else {
					parametersReport.put("sAddress", "");
				}
				FileExtension ext = FileExtension.PDF;
				JRDataSource dataSource = new JRBeanCollectionDataSource(lstData);
				//String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.DEBIT_7_2_4_NVBH1);
				String outputPath = ReportUtils.exportFromFormat(ext, (HashMap<String, Object>) parametersReport, dataSource, ShopReportTemplate.PHIEU_SUA_CHUA_THIET_BI);
				result.put(ERROR, false);
				result.put(REPORT_PATH, outputPath);
				/**ATTT duong dan*/
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.printRepair()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	/**
	 * Export ecxel
	 * @author nhutnn
	 * @since 16/01/2015
	 */
	
	/**
	 * Xuat Export ecxel, QL sua chua
	 * cap nhat lay filter theo ham getFilterRepairSeach
	 * @author vuongmq
	 * @throws IOException 
	 * @since 22/04/2015
	 */
	public String exportRepairRecord() throws IOException {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			/**ATTT duong dan*/
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			} else {
				if (currentUser.getShopRoot() == null) {
					// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			}
			// lay filter Repair
			EquipRepairFilter filter = this.getFilterRepairSeach(PAGING_NO);
			// ket qua tim kiem
			ObjectVO<EquipRepairFormVO> lstEquipRepairFormVO = equipmentManagerMgr.getListEquipRepair(filter);
			if (lstEquipRepairFormVO != null && lstEquipRepairFormVO.getLstObject() != null && lstEquipRepairFormVO.getLstObject().size() > 0) {
				// Init XSSF workboook
				String outputName = ConstantManager.TEMPLATE_REPAIR_RECORD_DELIVERY + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
				String exportFileName = Configuration.getStoreRealPath() + outputName;

				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				// Tao shett
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("equipment.manager.equipment.sheet.export.repair"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				//ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

				// Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(18);
				// set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 120, 200, 120, 250, 250, 150, 150, 150, 150, 150, 150, 100);
				// Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 25);
				sheetData.setDisplayGridlines(true);

				int iRow = 0, iColumn = 0;
				// tao column header
				String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.repair");
				String[] lstHeaders = headers.split(";");
				for (int i = 0, len = lstHeaders.length; i < len; i++) {
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				}
				XSSFCellStyle textStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.NORMAL).clone();
				DataFormat fmt = workbook.createDataFormat();
				textStyle.setDataFormat(fmt.getFormat("@"));
				// set data
				++iRow;
				for (int i = 0, size = lstEquipRepairFormVO.getLstObject().size(); i < size; i++) {
					EquipRepairFormVO equipRepairFormVO = lstEquipRepairFormVO.getLstObject().get(i);
					filter = new EquipRepairFilter();
					filter.setEquipRepairId(equipRepairFormVO.getId());
					ObjectVO<EquipmentRepairFormDtlVO> lstRepairDtl = equipmentManagerMgr.getListEquipRepairDtlVOByEquipRepairId(filter);
					if (null != equipRepairFormVO) {
						iColumn = 0;
						// set thong tin chung
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipRepairFormVO.getMaPhieu(), textStyle);
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipRepairFormVO.getMaThietBi(), textStyle);
						String status = "";
						//0. Dự thảo, 1. Chờ duyệt, 2. Duyệt, 3. Không duyệt, 4. Hủy, 5. Sửa chữa, 6. Hoàn tất
						if (StatusRecordsEquip.DRAFT.getValue().equals(equipRepairFormVO.getTrangThai())) {
							status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duthao");
						} else if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipRepairFormVO.getTrangThai())) {
							status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.choduyet");
						} else if (StatusRecordsEquip.APPROVED.getValue().equals(equipRepairFormVO.getTrangThai())) {
							status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duyet");
						} else if (StatusRecordsEquip.ARE_CORRECTED.getValue().equals(equipRepairFormVO.getTrangThai())) {
							status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.suachua");
						} else if (StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipRepairFormVO.getTrangThai())) {
							status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.khongduyet");
						} else if (StatusRecordsEquip.COMPLETION_REPAIRS.getValue().equals(equipRepairFormVO.getTrangThai())) {
							status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.hoantat");
						} else if (StatusRecordsEquip.CANCELLATION.getValue().equals(equipRepairFormVO.getTrangThai())) {
							status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.huy");
						}
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, status, textStyle);
						if (equipRepairFormVO.getCreateFormDate() != null) {
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, DateUtil.convertDateByString(equipRepairFormVO.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY), textStyle);
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", textStyle);
						}
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipRepairFormVO.getTinhTrangHuHong(), textStyle);
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipRepairFormVO.getLyDoDeNghi(), textStyle);
						ExcelPOIProcessUtils.addCell(sheetData, iColumn + 7, iRow, equipRepairFormVO.getNote(), textStyle);
						// set chi tiet
						int iColDtl = iColumn;
						if (lstRepairDtl != null && lstRepairDtl.getLstObject() != null && lstRepairDtl.getLstObject().size() > 0) {
							for (int j = 0, sizej = lstRepairDtl.getLstObject().size(); j < sizej; j++) {
								iColumn = iColDtl;
								EquipmentRepairFormDtlVO repairFormDtlVO = lstRepairDtl.getLstObject().get(j);
								// hang muc
								if (StringUtil.isNullOrEmpty(repairFormDtlVO.getHangMuc())) {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", textStyle);
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, repairFormDtlVO.getHangMuc(), textStyle);
								}
								// ngay bat dau bao hanh
								if (repairFormDtlVO.getNgayBatDauBaoHanh() == null) {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, repairFormDtlVO.getNgayBatDauBaoHanh().toString(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								}
								// bao hanh
								if (repairFormDtlVO.getBaoHanh() == null) {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, repairFormDtlVO.getBaoHanh().toString(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								}
								// ngay het han bao hanh
								if (repairFormDtlVO.getNgayHetHanBaoHanh() == null) {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, repairFormDtlVO.getNgayHetHanBaoHanh().toString(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								}
								// gia vat lieu
								if (repairFormDtlVO.getDonGiaVatTu() == null) {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, repairFormDtlVO.getDonGiaVatTu().toString(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								}
								// gia nhan cong
								if (repairFormDtlVO.getDonGiaNhanCong() == null) {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, repairFormDtlVO.getDonGiaNhanCong().toString(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								}
								// so luong
								if (repairFormDtlVO.getSoLuong() == null) {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, repairFormDtlVO.getSoLuong(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_RIGHT));
								}
								iRow++;
							}
						}
					}
				}
				out = new FileOutputStream(exportFileName);
				workbook.write(out);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(REPORT_PATH, outputPath);
				result.put(ERROR, false);
				result.put("hasData", true);
				/**ATTT duong dan*/
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.exportRepairRecord()"), createLogErrorStandard(actionStartTime));
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}

	/**
	 * Tim kiem kho
	 * 
	 * @author phuongvm
	 * @since 19/01/2015
	 * @return
	 */
	public String searchStock() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipStockFilter filter = new EquipStockFilter();
			filter.setkPaging(kPaging);
			/** vuongmq; 25/05/2015; them ma NPP; va staff dang nhap */
			if (currentUser != null) {
				filter.setStaffRoot(currentUser.getUserId());
				if (currentUser.getShopRoot() != null) {
					filter.setShopRoot(currentUser.getShopRoot().getShopId());
				} else {
					// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			} else {
				// loi khong ton tai user dang nhap
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shopCode = shopCode.trim();
				filter.setShopCode(shopCode);
			}
			if (!StringUtil.isNullOrEmpty(shopName)) {
				shopName = shopName.trim();
				filter.setShopName(shopName);
			}
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				customerCode = customerCode.trim();
				filter.setCustomerCode(customerCode);
			}
			if (!StringUtil.isNullOrEmpty(customerName)) {
				customerName = customerName.trim();
				filter.setCustomerNameOrAdrress(customerName);
			}
			
			ObjectVO<EquipStockVO> temp = equipmentManagerMgr.getListStockByFilter(filter);

			if (temp != null && temp.getLstObject().size() > 0) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipRepairFormVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairManageAction.searchStock()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	public List<File> getLstFile() {
		return lstFile;
	}

	public void setLstFile(List<File> lstFile) {
		this.lstFile = lstFile;
	}

	public List<String> getLstFileContentType() {
		return lstFileContentType;
	}

	public void setLstFileContentType(List<String> lstFileContentType) {
		this.lstFileContentType = lstFileContentType;
	}

	public List<String> getLstFileFileName() {
		return lstFileFileName;
	}

	public void setLstFileFileName(List<String> lstFileFileName) {
		this.lstFileFileName = lstFileFileName;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public String getCustomerAddr() {
		return customerAddr;
	}

	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getStatusAction() {
		return statusAction;
	}

	public void setStatusAction(Integer statusAction) {
		this.statusAction = statusAction;
	}

	public Integer getStatusForm() {
		return statusForm;
	}

	public void setStatusForm(Integer statusForm) {
		this.statusForm = statusForm;
	}

	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getFromRePresentor() {
		return fromRePresentor;
	}

	public void setFromRePresentor(String fromRePresentor) {
		this.fromRePresentor = fromRePresentor;
	}

	public String getToRePresentor() {
		return toRePresentor;
	}

	public void setToRePresentor(String toRePresentor) {
		this.toRePresentor = toRePresentor;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getResultBB() {
		return resultBB;
	}

	public void setResultBB(String resultBB) {
		this.resultBB = resultBB;
	}

	public Integer getViTri() {
		return viTri;
	}

	public void setViTri(Integer viTri) {
		this.viTri = viTri;
	}

	public String getNewShopCode() {
		return newShopCode;
	}

	public void setNewShopCode(String newShopCode) {
		this.newShopCode = newShopCode;
	}

	public String getNewCustomerCode() {
		return newCustomerCode;
	}

	public void setNewCustomerCode(String newCustomerCode) {
		this.newCustomerCode = newCustomerCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public EquipRepairForm getEquipRepairForm() {
		return equipRepairForm;
	}

	public void setEquipRepairForm(EquipRepairForm equipRepairForm) {
		this.equipRepairForm = equipRepairForm;
	}

	public Integer getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}

	public String getNewAddress() {
		return newAddress;
	}

	public void setNewAddress(String newAddress) {
		this.newAddress = newAddress;
	}

	public Long getIdForm() {
		return idForm;
	}

	public void setIdForm(Long idForm) {
		this.idForm = idForm;
	}

	public Long getEquipCategoryId() {
		return equipCategoryId;
	}

	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}

	public List<EquipCategory> getLstEquipCategory() {
		return lstEquipCategory;
	}

	public void setLstEquipCategory(List<EquipCategory> lstEquipCategory) {
		this.lstEquipCategory = lstEquipCategory;
	}

	public List<EquipProvider> getLstEquipProvider() {
		return lstEquipProvider;
	}

	public void setLstEquipProvider(List<EquipProvider> lstEquipProvider) {
		this.lstEquipProvider = lstEquipProvider;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public Integer getStatusPayment() {
		return statusPayment;
	}

	public void setStatusPayment(Integer statusPayment) {
		this.statusPayment = statusPayment;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public Long getEquipRepairId() {
		return equipRepairId;
	}

	public void setEquipRepairId(Long equipRepairId) {
		this.equipRepairId = equipRepairId;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public List<String> getLstEquipItemId() {
		return lstEquipItemId;
	}

	public void setLstEquipItemId(List<String> lstEquipItemId) {
		this.lstEquipItemId = lstEquipItemId;
	}

	public List<String> getLstBaoHanh() {
		return lstBaoHanh;
	}

	public void setLstBaoHanh(List<String> lstBaoHanh) {
		this.lstBaoHanh = lstBaoHanh;
	}

	public List<String> getLstDonGiaVT() {
		return lstDonGiaVT;
	}

	public void setLstDonGiaVT(List<String> lstDonGiaVT) {
		this.lstDonGiaVT = lstDonGiaVT;
	}

	public List<String> getLstDonGiaNC() {
		return lstDonGiaNC;
	}

	public void setLstDonGiaNC(List<String> lstDonGiaNC) {
		this.lstDonGiaNC = lstDonGiaNC;
	}

	public List<String> getLstSoLuong() {
		return lstSoLuong;
	}

	public void setLstSoLuong(List<String> lstSoLuong) {
		this.lstSoLuong = lstSoLuong;
	}

	public List<String> getLstSoLanSuaChua() {
		return lstSoLanSuaChua;
	}

	public void setLstSoLanSuaChua(List<String> lstSoLanSuaChua) {
		this.lstSoLanSuaChua = lstSoLanSuaChua;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getRepairCountForm() {
		return repairCountForm;
	}

	public void setRepairCountForm(Integer repairCountForm) {
		this.repairCountForm = repairCountForm;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public EquipmentRepairPaymentRecord getEquipmentRepairPaymentRecord() {
		return equipmentRepairPaymentRecord;
	}

	public void setEquipmentRepairPaymentRecord(EquipmentRepairPaymentRecord equipmentRepairPaymentRecord) {
		this.equipmentRepairPaymentRecord = equipmentRepairPaymentRecord;
	}

	public EquipRepairPayForm getEquipmentRepairPayment() {
		return equipmentRepairPayment;
	}

	public void setEquipmentRepairPayment(EquipRepairPayForm equipmentRepairPayment) {
		this.equipmentRepairPayment = equipmentRepairPayment;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public List<Map<String, Object>> getListObjectBeans() {
		return listObjectBeans;
	}

	public void setListObjectBeans(List<Map<String, Object>> listObjectBeans) {
		this.listObjectBeans = listObjectBeans;
	}

	public String getEquipAttachFileStr() {
		return equipAttachFileStr;
	}

	public void setEquipAttachFileStr(String equipAttachFileStr) {
		this.equipAttachFileStr = equipAttachFileStr;
	}

	public List<String> getLstNgayBatDauBaoHanh() {
		return lstNgayBatDauBaoHanh;
	}

	public void setLstNgayBatDauBaoHanh(List<String> lstNgayBatDauBaoHanh) {
		this.lstNgayBatDauBaoHanh = lstNgayBatDauBaoHanh;
	}

	public List<String> getLstNgayHetHanBaoHanh() {
		return lstNgayHetHanBaoHanh;
	}

	public void setLstNgayHetHanBaoHanh(List<String> lstNgayHetHanBaoHanh) {
		this.lstNgayHetHanBaoHanh = lstNgayHetHanBaoHanh;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public BigDecimal getWorkerPrice() {
		return workerPrice;
	}

	public void setWorkerPrice(BigDecimal workerPrice) {
		this.workerPrice = workerPrice;
	}

	public List<String> getLstTongTienDetail() {
		return lstTongTienDetail;
	}

	public void setLstTongTienDetail(List<String> lstTongTienDetail) {
		this.lstTongTienDetail = lstTongTienDetail;
	}

	public List<String> getLstDonGiaVTDinhMucTu() {
		return lstDonGiaVTDinhMucTu;
	}

	public void setLstDonGiaVTDinhMucTu(List<String> lstDonGiaVTDinhMucTu) {
		this.lstDonGiaVTDinhMucTu = lstDonGiaVTDinhMucTu;
	}

	public List<String> getLstDonGiaVTDinhMucDen() {
		return lstDonGiaVTDinhMucDen;
	}

	public void setLstDonGiaVTDinhMucDen(List<String> lstDonGiaVTDinhMucDen) {
		this.lstDonGiaVTDinhMucDen = lstDonGiaVTDinhMucDen;
	}

	public List<String> getLstDonGiaNCDinhMucTu() {
		return lstDonGiaNCDinhMucTu;
	}

	public void setLstDonGiaNCDinhMucTu(List<String> lstDonGiaNCDinhMucTu) {
		this.lstDonGiaNCDinhMucTu = lstDonGiaNCDinhMucTu;
	}

	public List<String> getLstDonGiaNCDinhMucDen() {
		return lstDonGiaNCDinhMucDen;
	}

	public void setLstDonGiaNCDinhMucDen(List<String> lstDonGiaNCDinhMucDen) {
		this.lstDonGiaNCDinhMucDen = lstDonGiaNCDinhMucDen;
	}

	public List<String> getLstDescription() {
		return lstDescription;
	}

	public void setLstDescription(List<String> lstDescription) {
		this.lstDescription = lstDescription;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}

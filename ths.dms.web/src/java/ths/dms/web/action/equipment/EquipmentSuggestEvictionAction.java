package ths.dms.web.action.equipment;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;
import ths.dms.web.utils.report.excel.SXSSFReportHelper;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipSuggestEvictionMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StatusDeliveryEquip;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.enumtype.TypeStockEquipLend;
import ths.dms.core.entities.filter.CustomerFilter;
import ths.dms.core.entities.filter.EquipSuggestEvictionFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipSuggestEvictionDetailVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.EquipmentSuggestEvictionVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

/**
 * Action Quan ly De nghi thu hoi thiet bi
 * 
 * @author nhutnn
 * @since 13/07/2015
 */
public class EquipmentSuggestEvictionAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CustomerMgr customerMgr;
	private ShopMgr shopMgr;
	private StaffMgr staffMgr;
	private EquipSuggestEvictionMgr equipSuggestEvictionMgr;
	
	private List<EquipSuggestEvictionDetailVO> lstDetails;
	
	private List<Long> lstIdRecord;
	private List<Long> lstCustomerId;

	private Long id;
	private String curShopCode;
	private Integer statusDelivery;
	private Integer statusRecord;
	private String code;
	private String lstShop;
	private String fromDate;
	private String toDate;
	private String createFormDate;

	private File excelFile;
	private String excelFileContentType;
	
	private String shopCode;
	private String shopCodeEditor;
	private String shopName;
	private String shortCode;
	private Long stockId;
	private String customerCode;
	private String customerName;
	private String equipCode;
	private String note;
	
	private Integer flagRecordStatus;
	private Boolean isNPP;
	
	@Override
	public void prepare() throws Exception {
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		equipSuggestEvictionMgr = (EquipSuggestEvictionMgr) context.getBean("equipSuggestEvictionMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		if (shop != null) {
			shopId = shop.getId();
			curShopCode = shop.getShopCode();
		}
		return SUCCESS;
	}

	public String search() {
		actionStartTime = DateUtil.now();
		result.put("rows", new ArrayList<EquipRecordVO>());
		result.put("total", 0);
		try {
			EquipSuggestEvictionFilter<EquipmentSuggestEvictionVO> filter = new EquipSuggestEvictionFilter<EquipmentSuggestEvictionVO>();
			//set cac tham so tim kiem
			KPaging<EquipmentSuggestEvictionVO> kPaging = new KPaging<EquipmentSuggestEvictionVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			List<Long> lstShopId = new ArrayList<Long>();
			if (StringUtil.isNullOrEmpty(lstShop)) {
				lstShopId.add(currentUser.getShopRoot().getShopId());
			} else {
				String[] temp = lstShop.split(",");
				for (int i = 0, sz = temp.length; i < sz; i++) {
					if (!checkShopInOrgAccessByCode(temp[i].trim().toUpperCase())) {
						result.put("errMsg", R.getResource("common.shop.not.belong.user", temp[i].trim().toUpperCase()));
						result.put(ERROR, true);
						return JSON;
					} else {
						Shop shop = shopMgr.getShopByCode(temp[i].trim().toUpperCase());
						if (shop == null) {
							result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("common.unit.name.err") + " " + temp[i].trim().toUpperCase()));
							result.put(ERROR, true);
							return JSON;
						} else if (!ActiveType.RUNNING.getValue().equals(shop.getStatus().getValue())) {
							result.put("errMsg", R.getResource("common.catalog.shop.status.not.active", temp[i].trim().toUpperCase()));
							result.put(ERROR, true);
							return JSON;
						}
						lstShopId.add(shop.getId());
					}
				}
			}
			filter.setLstShopId(lstShopId);

			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}

			if (statusRecord != null) {
				if (flagRecordStatus == null) {
					if (StatusRecordsEquip.DRAFT.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.APPROVED.getValue().equals(statusRecord)
						|| StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.COMPLETION_REPAIRS.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord)) {
						filter.setStatus(statusRecord);
					} else {
						filter.setStatus(StatusRecordsEquip.DRAFT.getValue());
					}
				} else {
					if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.APPROVED.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.COMPLETION_REPAIRS.getValue().equals(statusRecord)) {
						filter.setStatus(statusRecord);
					} else {
						filter.setStatus(StatusRecordsEquip.WAITING_APPROVAL.getValue());
					}
				}
			} else {
				if (flagRecordStatus == null) {
					List<Integer> lstStatus = new ArrayList<Integer>();
					lstStatus.add(StatusRecordsEquip.DRAFT.getValue());
					lstStatus.add(StatusRecordsEquip.WAITING_APPROVAL.getValue());
					lstStatus.add(StatusRecordsEquip.APPROVED.getValue());
					lstStatus.add(StatusRecordsEquip.NO_APPROVAL.getValue());
					lstStatus.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
					lstStatus.add(StatusRecordsEquip.CANCELLATION.getValue());
					filter.setLstStatus(lstStatus);
				} else {
					List<Integer> lstStatus = new ArrayList<Integer>();
					lstStatus.add(StatusRecordsEquip.WAITING_APPROVAL.getValue());
					lstStatus.add(StatusRecordsEquip.APPROVED.getValue());
					lstStatus.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
					filter.setLstStatus(lstStatus);
				}
			}

			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setToDate(tDate);
			}

			if (statusDelivery != null) {
				filter.setDeliveryStatus(statusDelivery);
			}

			//goi ham thuc thi tim kiem
			ObjectVO<EquipmentSuggestEvictionVO> objVO = equipSuggestEvictionMgr.searchListEquipSuggestEvictionVOByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.search()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Xu ly Luu chuyen trang thai tren trang tim kiem
	 * 
	 * @author nhutnn
	 * @since 03/07/2015
	 */
	public String saveStatusRecord() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			List<FormErrVO> lstError = new ArrayList<FormErrVO>();
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				for (int i = 0; i < lstIdRecord.size(); i++) {
					EquipSuggestEviction equipSuggestEviction = equipSuggestEvictionMgr.getEquipSuggestEvictionById(lstIdRecord.get(i));
					FormErrVO formErrVO = new FormErrVO();
					if (equipSuggestEviction != null && statusRecord != null && !statusRecord.equals(equipSuggestEviction.getStatus())) {
						// kiem tra trang thai khi chuyen
						if (equipSuggestEviction.getStatus() != null
							&& !((StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus()))
							|| (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus()))
							|| (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus()))
							|| (StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus()))
							|| (StatusRecordsEquip.APPROVED.getValue().equals(statusRecord) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipSuggestEviction.getStatus())) 
							|| (StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipSuggestEviction.getStatus())))) {
							formErrVO.setFormStatusErr(1);
						} else if (equipSuggestEviction.getStatus() != null 
								&& (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) 
								&& StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus()))) {
							// neu chuyen tu Khong duyet -> Cho duyet
							EquipSuggestEvictionFilter<EquipSuggestEvictionDTL> filter = new EquipSuggestEvictionFilter<EquipSuggestEvictionDTL>();
							List<Long> lstId = new ArrayList<Long>();
							lstId.add(equipSuggestEviction.getId());
							filter.setLstObjectId(lstId);
							List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs = equipSuggestEvictionMgr.getListEquipSuggestEvictionDetailByFilter(filter);

							// kiem tra thiet bi dang o trang thai tham gia giao dich
							String lstEquipErr = "";
							for (int j = 0, szj = lstEquipSuggestEvictionDTLs.size(); j < szj; j++) {
								Equipment equipment = lstEquipSuggestEvictionDTLs.get(j).getEquipment();
								if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) {
									lstEquipErr += equipment.getCode() + ", ";
								}
							}
							if (!StringUtil.isNullOrEmpty(lstEquipErr) && lstEquipErr.lastIndexOf(",") > -1) {
								lstEquipErr = lstEquipErr.substring(0, lstEquipErr.lastIndexOf(","));
								formErrVO.setEquipTrading(lstEquipErr);
							}
						}
						if (formErrVO.getFormStatusErr() == null && formErrVO.getPeriodError() == null && StringUtil.isNullOrEmpty(formErrVO.getEquipTrading())) {
							if (statusRecord != null && !statusRecord.equals(equipSuggestEviction.getStatus())) {
								EquipSuggestEvictionFilter<EquipSuggestEviction> filter = new EquipSuggestEvictionFilter<EquipSuggestEviction>();
								filter.setEquipSuggestEviction(equipSuggestEviction);
								filter.setStatus(statusRecord);
								equipSuggestEvictionMgr.updateEquipSuggestEviction(filter);
							}
						} else {
							formErrVO.setFormCode(equipSuggestEviction.getCode());
							lstError.add(formErrVO);
						}
					}
				}
				if (lstError.size() > 0) {
					result.put("lstRecordError", lstError);
				}
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.saveStatusRecord()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Xuat bien ban de nghi thu hoi tu
	 * 
	 * @author nhutnn
	 * @since 20/07/2015
	 * @throws BusinessException
	 */
	public String exportEquipSuggestEviction() {
		actionStartTime = DateUtil.now();
		final int NUMBER_COL = 24;
		SXSSFWorkbook workbook = null;
		try {
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter = new EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO>();
				filter.setLstId(lstIdRecord);
				List<EquipmentSuggestEvictionVO> data = equipSuggestEvictionMgr.getListEquipSuggestEvictionDetailVOForExportByFilter(filter);
				if (data == null || data.size() == 0) {
					result.put("errMsg", R.getResource("common.export.excel.null"));
					result.put(ERROR, true);
					return JSON;
				}
				/** Bo sung ATTT */
				String reportToken = retrieveReportToken(reportCode);
				if (StringUtil.isNullOrEmpty(reportToken)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
					return JSON;
				}
				/** workbook */
				workbook = new SXSSFWorkbook(200);
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				/** style */
				// HeaderLeft
				XSSFCellStyle headerStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK).clone();
				XSSFFont fontHeader = (XSSFFont) workbook.createFont();
				fontHeader.setFontHeight(10);
				fontHeader.setFontName("Arial");
				fontHeader.setBold(true);
				headerStyle.setFont(fontHeader);
				headerStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
				// HeaderLeftI
				XSSFCellStyle headerStyleI = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK).clone();
				XSSFFont fontHeaderI = (XSSFFont) workbook.createFont();
				fontHeaderI.setFontHeight(10);
				fontHeaderI.setItalic(true);
				fontHeaderI.setFontName("Arial");
				headerStyleI.setFont(fontHeaderI);
				headerStyleI.setAlignment(XSSFCellStyle.ALIGN_LEFT);
				// boldLeftI
				XSSFCellStyle boldLeftI = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.BOLD_LEFT).clone();
				XSSFFont fontBoldLeftI = (XSSFFont) workbook.createFont();
				fontBoldLeftI.setFontHeight(9);
				fontBoldLeftI.setBold(true);
				fontBoldLeftI.setItalic(true);
				fontBoldLeftI.setFontName("Arial");
				boldLeftI.setFont(fontBoldLeftI);
				// header
				XSSFCellStyle headerWrapText = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN).clone();
				headerWrapText.setWrapText(true);
				// normalCenterLeftI
				XSSFCellStyle normalCenterLeftI = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK).clone();
				XSSFFont normalCenterLeftIfont = (XSSFFont) workbook.createFont();
				normalCenterLeftIfont.setFontHeight(9);
				normalCenterLeftIfont.setItalic(true);
				normalCenterLeftIfont.setFontName("Arial");
				normalCenterLeftI.setFont(normalCenterLeftIfont);
				normalCenterLeftI.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				/** sheet */
				for (int j = 0, sz = data.size(); j < sz; j++) {
					EquipmentSuggestEvictionVO equipmentSuggestEvictionVO = data.get(j);
					List<EquipSuggestEvictionDetailVO> lstDetailVOs = equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDetailVOs();
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(equipmentSuggestEvictionVO.getCode());
					sheet.setDisplayGridlines(true);
					SXSSFReportHelper.setColumnWidths(sheet, 0, (short) 5, (short) 10, (short) 10, (short) 15, (short) 20, (short) 18, (short) 15);
					SXSSFReportHelper.setColumnWidths(sheet, 7, (short) 15, (short) 15, (short) 15, (short) 15, (short) 15, (short) 15);
					SXSSFReportHelper.setColumnWidths(sheet, 13, (short) 12, (short) 25, (short) 15, (short) 15, (short) 18, (short) 12, (short) 12);
					SXSSFReportHelper.setColumnWidths(sheet, 20, (short) 20, (short) 15, (short) 12, (short) 20, (short) 70);

					// header
					int colIdx = 0;
					int rowIdx = 0;
					// textbox
					/*XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, 21, 1, 24, 4);
					XSSFTextBox textbox = ((XSSFDrawing) sheet.createDrawingPatriarch()).createTextbox(anchor);
					textbox.setText(R.getResource("equip.proposal.export.sheet1.textbox1") + "\n" + R.getResource("equip.suggest.export.sheet1.textbox2") + "\n" + R.getResource("equip.proposal.export.sheet1.textbox3"));
					textbox.setLineWidth(1);
					textbox.setLineStyle(0);
					textbox.setLineStyleColor(0, 0, 0);
					textbox.setFillColor(255, 255, 255);*/
					//
//					SXSSFReportHelper.addCell(sheet, colIdx + 24, rowIdx++, R.getResource("equip.proposal.import.congty"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					SXSSFReportHelper.addCell(sheet, colIdx + 24, rowIdx++, R.getResource("equip.suggest.import.npp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					// title
					colIdx = 0;
					rowIdx = 0;
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.suggest.export.sheet1.title"), headerStyle);
					String printDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR);
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("report.general.ngayIn") + " " + printDate, headerStyleI);
//					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.suggest.export.sheet1.title1"), headerStyleI);
//					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.suggest.export.sheet1.title2"), headerStyleI);
					SXSSFReportHelper.addCell(sheet, colIdx + 10, rowIdx++, R.getResource("equip.suggest.export.sheet1.title3"), style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK));
					rowIdx++;
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.suggest.export.sheet1.title4"), headerStyleI);
					colIdx = 0;
					rowIdx += 1;
					SXSSFReportHelper.setRowHeights(sheet, rowIdx, (short) 25, (short) 30);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.stt"), headerWrapText);
//					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.mien"), headerWrapText);
//					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.kenh"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.don.vi"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.makhachhang"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.tencuahang"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.nguoidungten"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.quanhe"), headerWrapText);
					//
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx + 5, rowIdx, R.getResource("equip.proposal.export.sheet1.diachi"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.dienthoai"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.sonha"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.tenduong"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.phuong"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.quan"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.tinh"), headerWrapText);
					//
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.suggest.export.sheet1.soluongtu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.suggest.export.sheet1.loaitu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.suggest.export.sheet1.hieutu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equipment.manager.equipment.code"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.suggest.export.sheet1.seri"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.suggest.export.sheet1.tinhtrangtu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.suggest.export.sheet1.thoigianmongthuhoitu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.suggest.export.sheet1.khonhan"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.suggest.export.sheet1.lydo"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.hotengiamsat"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.dienthoaigs"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.ghichu"), headerWrapText);
					// dong header cuoi
					colIdx = 0;
					rowIdx += 2;
					String[] numberTemp = R.getResource("equip.suggest.export.sheet1.hearder.number").split(",");
					for (int k = 0, szk = numberTemp.length; k < szk; k++) {
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, numberTemp[k], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					}
					// data
					rowIdx++;
					for (int i = 0, szi = lstDetailVOs.size(); i < szi; i++) {
						colIdx = 0;
						EquipSuggestEvictionDetailVO vo = lstDetailVOs.get(i);
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, String.valueOf(i + 1), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getShopCodeMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getShopCodeKenh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getShopCode() + " - " + vo.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getCustomerCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getCustomerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getRepresentative(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getRelation(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						//
						String phoneCustomer = (!StringUtil.isNullOrEmpty(vo.getPhone()) && !StringUtil.isNullOrEmpty(vo.getMobile())) ? vo.getPhone() + " / " + vo.getMobile() : !StringUtil.isNullOrEmpty(vo.getPhone()) ? vo.getPhone() : vo.getMobile();
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, phoneCustomer, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getAddress(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getStreet(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getWard(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getDistrict(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getProvince(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						//
						SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, BigDecimal.ONE, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getEquipCategory() + " " + vo.getCapacity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getEquipBrand(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getEquipSeri(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getHealthStatus(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getTimeEviction(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						if (TypeStockEquipLend.NCC.getValue().equals(vo.getStockTypeValue())) {
							SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, R.getResource("equip.suggest.import.npp"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						} else {
							SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, R.getResource("equip.proposal.import.congty"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getReason(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getStaffCode() + " - " + vo.getStaffName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						String staffCustomer = (!StringUtil.isNullOrEmpty(vo.getStaffPhone()) && !StringUtil.isNullOrEmpty(vo.getStaffMobile())) ? vo.getStaffPhone() + " / " + vo.getStaffMobile() : !StringUtil.isNullOrEmpty(vo.getStaffPhone()) ? vo.getStaffPhone() : vo.getStaffMobile();
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, staffCustomer, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getNote(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
						rowIdx++;
					}

					// dong tong
					colIdx = 4;
					int SUM_NUMBER = 12;
					for (int h = 0; h < NUMBER_COL; h++) {
						if (h == SUM_NUMBER) {
							SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, equipmentSuggestEvictionVO.getTotalEquip(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
						} else if (h > 3) {
							SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
					}
					SXSSFReportHelper.addMergeCells(sheet, 0, rowIdx, 3, rowIdx, R.getResource("equip.proposal.export.sheet1.sum"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));

					rowIdx++;
					colIdx = 0;
					SXSSFReportHelper.addCell(sheet, colIdx + 21, rowIdx++, R.getResource("equip.proposal.export.sheet1.ngay.thang.nam"), normalCenterLeftI);

					SXSSFReportHelper.addCell(sheet, colIdx + 7, rowIdx, R.getResource("equip.proposal.export.sheet1.gd"), style.get(ExcelPOIProcessUtils.BOLD_CENTER));
					SXSSFReportHelper.addCell(sheet, colIdx + 12, rowIdx, R.getResource("equip.proposal.export.sheet1.tnkd"), style.get(ExcelPOIProcessUtils.BOLD_CENTER));
					SXSSFReportHelper.addCell(sheet, colIdx + 21, rowIdx, R.getResource("equip.proposal.export.sheet1.tbbh"), style.get(ExcelPOIProcessUtils.BOLD_CENTER));
				}
				/** Ghi file va tra ve ket qua */
				String outputPath = SXSSFReportHelper.exportXLSX(workbook, null, "equip.suggest.export.sheet1.name.file");
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				data = null;
			} else {
				result.put("errMsg", R.getResource("equip.proposal.export.not.record.export"));
				result.put(ERROR, true);
				return JSON;
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.exportEquipSuggestEviction()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * Kiem tra dong moi
	 * 
	 * @author nhutnn
	 * @since 21/07/2015
	 * @param row
	 * @return
	 */
	public boolean isANewRecord(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(8))) {
			return true;
		}
		return false;
	}

	/**
	 * Kiem tra giam sat co quan ly shop ko
	 * 
	 * @author nhutnn
	 * @param staffCode
	 * @param shopId
	 * @return Boolean
	 * @since 12/08/2015
	 * 
	 * @modified vuongmq 07/03/2016
	 */
	private Boolean checkNVGS(String staffCode, Long shopId) throws Exception {
		boolean flag = false;
		if (!StringUtil.isNullOrEmpty(staffCode) && shopId != null) {
			List<StaffVO> lstStaffGS = new ArrayList<StaffVO>();
			// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
			if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null) {
				StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
				filter.setUserId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				filter.setStatus(ActiveType.RUNNING.getValue());
				List<Integer> lstObjectType = new ArrayList<Integer>();
				lstObjectType.add(StaffSpecificType.GSMT.getValue());
				lstObjectType.add(StaffSpecificType.SUPERVISOR.getValue());
				filter.setLstObjectType(lstObjectType);
				filter.setShopId(shopId);
				Staff staff = staffMgr.getStaffByCode(staffCode);
				if (staff != null) {
					filter.setStaffId(staff.getId());
					lstStaffGS = staffMgr.getListGSByFilter(filter);
					if (lstStaffGS != null) {
						flag = true;
					}
				}
			}
		}
		return flag;
	}

	/**
	 * Import excel nhan hang
	 * 
	 * @author nhutnn
	 * @since 21/07/2015
	 */
	public String importExcelRecord() {
		actionStartTime = DateUtil.now();
		try {
			resetToken(result);
			isError = true;
			errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
			totalItem = 0;
			String msg = "";
			// bien ban hop le (bao gom tat ca cac dong chi tiet)
			boolean isValidForm = true;
			boolean isNewRecord = true;
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<CellBean> lstFailsInForm = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 9);

			// chuan bi du lieu
			List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs = null;
			EquipSuggestEviction equipSuggestEviction = null;

			// kiem tra ky
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			if (equipPeriod == null) {
//				result.put(ERROR, true);
//				result.put("errMsg", R.getResource("equip.period.null"));
//				return JSON;
//			}
//			EquipPeriod equipPeriod = null;
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				for (int i = 0, sz = lstData.size(); i < sz; i++) {	
					Date createFormDate = null;
					Shop shop = null;
					Customer customer = null;
					Date timeEviction = null;
					Equipment equipment = null;
					String reason = null;
					Staff staff = null;
					Integer objectType = null;
					String noteStr = null;

					msg = "";
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						String value = "";
						totalItem++;
						List<String> row = lstData.get(i);
						if (i == 0 || isANewRecord(row)) {
							isNewRecord = true;
							if (isValidForm && lstEquipSuggestEvictionDTLs != null && lstEquipSuggestEvictionDTLs.size() > 0 && equipSuggestEviction != null) {
								equipSuggestEviction = equipSuggestEvictionMgr.createEquipSuggestEvictionWithListDetail(equipSuggestEviction, lstEquipSuggestEvictionDTLs);
								lstFailsInForm = new ArrayList<CellBean>();
								lstEquipSuggestEvictionDTLs = null;
								equipSuggestEviction = null;
							} else {
								lstFails.addAll(lstFailsInForm);
								lstFailsInForm = new ArrayList<CellBean>();
								isValidForm = true;
								lstEquipSuggestEvictionDTLs = null;
								equipSuggestEviction = null;
							}
						} else {
							isNewRecord = false;
						}
						if (isNewRecord) {
							// dong muon thiet bi chinh
							if (row.size() > 0) {
								// ngay lap
								value = row.get(0).trim();
								String msgTemp = ValidateUtil.validateField(value, "equip.proposal.create.date", 8, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
								if (StringUtil.isNullOrEmpty(msgTemp)) {
									createFormDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
									if (createFormDate == null || DateUtil.compareDateWithoutTime(createFormDate, DateUtil.now()) > 0) {
										msg += R.getResource("equip.proposal.import.ngaylap.not.more.now") + "\n";
									} 
//									else {
//										List<EquipPeriod> lstEquipPeriods = equipmentManagerMgr.getListEquipPeriodByDate(createFormDate);
//										if (lstEquipPeriods == null || lstEquipPeriods.size() == 0) {
//											msg += R.getResource("equip.proposal.import.ngaylap.not.in.period") + "\n";
//										} else {
//											equipPeriod = lstEquipPeriods.get(0);
//										}
//									}
								} else {
									msg += msgTemp;
								}
							}
							// ** Ghi chu */
							if (row.size() > 8) {
								value = row.get(8);
								String msgTemp = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msgTemp)) {
									msg += msgTemp;
								} else {
									noteStr = value;
								}
							}
						}
						if (row.size() > 1) {
							// ma NPP
							value = row.get(1).trim().toUpperCase();
							String msgTemp = ValidateUtil.validateField(value, "customer.display.program.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								if (!checkShopInOrgAccessByCode(value)) {
									msg += R.getResource("common.shop.not.belong.user", value) + "\n";
								} else {
									shop = shopMgr.getShopByCode(value);
									if (shop == null) {
										msg += R.getResource("common.not.exist.in.db", R.getResource("customer.display.program.npp.code")) + "\n";
									} 
									//tamvnm: 09/09/2015 bo check status shop, kh
									else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
										msg += R.getResource("common.catalog.shop.status.not.active", value) + "\n";
									} 
									else if (shop.getType() == null 
											|| !(ShopSpecificType.NPP.equals(shop.getType().getSpecificType())
											|| ShopSpecificType.NPP_MT.equals(shop.getType().getSpecificType()))) {
										msg += R.getResource("customer.shop.not.npp") + "\n";
									}
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 2 && shop != null) {
							// ma KH
							value = row.get(2).trim().toUpperCase();
							String msgTemp = ValidateUtil.validateField(value, "khmuontu.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								value = StringUtil.getFullCode(shop.getShopCode(), value);
								customer = customerMgr.getCustomerByCode(value);
								if (customer == null) {
									msg += R.getResource("common.not.exist.in.db", R.getResource("khmuontu.customer.code")) + "\n";
								} 
								//tamvnm: 09/09/2015 bo check status shop, kh
//								else if (!ActiveType.RUNNING.equals(customer.getStatus())) {
//									msg += R.getResource("common.catalog.customer.status.not.active", value) + "\n";
//								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 3) {
							// thoi gian muon thu hoi tu
							value = row.get(3).trim();
							String msgTemp = ValidateUtil.validateField(value, "equip.suggest.export.sheet1.thoigianmongthuhoitu", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								timeEviction = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								if (DateUtil.compareDateWithoutTime(timeEviction, DateUtil.now()) == -1) {
									msg += R.getResource("equip.suggest.import.ngay.not.before.now", R.getResource("equip.suggest.export.sheet1.thoigianmongthuhoitu")) + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 4) {
							// ma thiet bi
							value = row.get(4).trim().toUpperCase();
							String msgTemp = ValidateUtil.validateField(value, "equipment.manager.equipment.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								equipment = equipmentManagerMgr.getEquipmentByCode(value);
								if (equipment == null) {
									msg += R.getResource("common.not.exist.in.db", R.getResource("equipment.manager.equipment.code")) + "\n";
								} else if (!StatusType.ACTIVE.equals(equipment.getStatus())) {
									msg += R.getResource("common.catalog.status.in.active", R.getResource("equipment.manager.equipment.code")) + "\n";
								} else if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) {
									msg += R.getResource("equip.suggest.eviction.err.equip.trading", R.getResource("equipment.manager.equipment.code")) + "\n";
								} else if (!EquipStockTotalType.KHO_KH.equals(equipment.getStockType()) || customer == null || !equipment.getStockId().equals(customer.getId())) {
									msg += R.getResource("equipment.row.invalid.makh.not.in.kho.manpp") + "\n";
								}
								if (StringUtil.isNullOrEmpty(msg)) {
									for (int j = i + 1, szj = lstData.size(); j < szj; j++) {
										if (!StringUtil.isNullOrEmpty(lstData.get(j).get(0))) {
											break;
										}
										String eqCode = lstData.get(j).get(4).trim().toUpperCase();
										if (!StringUtil.isNullOrEmpty(eqCode) && equipment != null && i != j && eqCode.equals(equipment.getCode())) {
											String t = R.getResource("equip.suggest.eviction.equip", eqCode);
											msg += t + R.getResource("promotion.product.import.duplicate", j + 2) + "\n";
											break;
										}
									}
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 5) {
							// ly do
							reason = row.get(5).trim();
							msg += ValidateUtil.validateField(reason, "equip.suggest.export.sheet1.lydo", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						}
						if (row.size() > 6) {
							// ma giam sat
							value = row.get(6).trim().toUpperCase();
							String msgTemp = ValidateUtil.validateField(value, "equip.suggest.import.giamsat", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								staff = staffMgr.getStaffByCode(value);
								if (staff == null) {
									msg += R.getResource("common.not.exist.in.db", R.getResource("equip.suggest.import.magiamsat", value)) + "\n";
								} else if (!ActiveType.RUNNING.equals(staff.getStatus())) {
									msg += R.getResource("common.catalog.status.in.active", R.getResource("equip.suggest.import.magiamsat", value)) + "\n";
								} else if (!(StaffSpecificType.SUPERVISOR.equals(staff.getStaffType().getSpecificType()) 
										|| StaffSpecificType.GSMT.equals(staff.getStaffType().getSpecificType()))) {
									msg += R.getResource("equipment.invalid.mgs") + "\n";
								} else if (shop == null || !checkNVGS(value, shop.getId())) {
									msg += R.getResource("equipment.manager.mgs.role.invalid") + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 7) {
							// kho nhan
							value = row.get(7).trim();
							String msgTemp = ValidateUtil.validateField(value, "equip.suggest.export.sheet1.khonhan", 250, ConstantManager.ERR_REQUIRE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								if (value.trim().toUpperCase().equals(R.getResource("equip.suggest.import.npp").trim().toUpperCase())) {
									objectType = TypeStockEquipLend.NCC.getValue();
								} else if (value.trim().toUpperCase().equals(R.getResource("equip.proposal.import.congty").trim().toUpperCase())) {
									objectType = TypeStockEquipLend.COMPANY.getValue();
								} else {
									msg += R.getResource("common.not.date", R.getResource("equip.suggest.export.sheet1.khonhan")) + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}					
						
						if (StringUtil.isNullOrEmpty(msg)) {
							if (isNewRecord) {
								equipSuggestEviction = new EquipSuggestEviction();
								equipSuggestEviction.setCreateFormDate(createFormDate);
								equipSuggestEviction.setCreateUser(currentUser.getStaffRoot().getStaffCode());
//								equipSuggestEviction.setEquipPriod(equipPeriod);
								equipSuggestEviction.setNote(noteStr);
								Shop sh = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
								equipSuggestEviction.setShop(sh);
								equipSuggestEviction.setStatus(StatusRecordsEquip.DRAFT.getValue());
								equipSuggestEviction.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());
							}
							if (lstEquipSuggestEvictionDTLs == null) {
								lstEquipSuggestEvictionDTLs = new ArrayList<EquipSuggestEvictionDTL>();
							}
							EquipSuggestEvictionDTL equipSuggestEvictionDTL = new EquipSuggestEvictionDTL();
							equipSuggestEvictionDTL.setCreateUser(currentUser.getStaffRoot().getStaffCode());
							equipSuggestEvictionDTL.setCustomer(customer);
							equipSuggestEvictionDTL.setCustomerCode(customer.getCustomerCode());
							equipSuggestEvictionDTL.setCustomerName(customer.getCustomerName());
							if (!StringUtil.isNullOrEmpty(customer.getPhone()) && !StringUtil.isNullOrEmpty(customer.getMobiphone())) {
								equipSuggestEvictionDTL.setMobile(customer.getPhone() + " / " + customer.getMobiphone());
							} else if (!StringUtil.isNullOrEmpty(customer.getPhone())) {
								equipSuggestEvictionDTL.setMobile(customer.getPhone());
							} else if (!StringUtil.isNullOrEmpty(customer.getMobiphone())) {
								equipSuggestEvictionDTL.setMobile(customer.getMobiphone());
							}

							EquipmentFilter<EquipmentRecordDeliveryVO> filter = new EquipmentFilter<EquipmentRecordDeliveryVO>();
							filter.setCustomerCode(customer.getShortCode());
							filter.setEquipCode(equipment.getCode());
							filter.setShopCode(customer.getShop().getShopCode());
							EquipmentRecordDeliveryVO equipmentRecordDeliveryVO = equipmentManagerMgr.getEquipmentRecordDeliveryVOByFilter(filter);
							if (equipmentRecordDeliveryVO != null) {
								equipSuggestEvictionDTL.setRelation(equipmentRecordDeliveryVO.getToRelation());
								equipSuggestEvictionDTL.setRefresentative(equipmentRecordDeliveryVO.getToRepresentative());
								equipSuggestEvictionDTL.setHousenumber(equipmentRecordDeliveryVO.getToHouseNumber());
								equipSuggestEvictionDTL.setStreet(equipmentRecordDeliveryVO.getToStreet());
								equipSuggestEvictionDTL.setWardName(equipmentRecordDeliveryVO.getToWardName());
								equipSuggestEvictionDTL.setDistrictName(equipmentRecordDeliveryVO.getToDistrictName());
								equipSuggestEvictionDTL.setProvinceName(equipmentRecordDeliveryVO.getToProvinceName());
							} else {
								equipSuggestEvictionDTL.setRelation("");
								equipSuggestEvictionDTL.setRefresentative("");
								equipSuggestEvictionDTL.setHousenumber("");
								equipSuggestEvictionDTL.setStreet("");
								equipSuggestEvictionDTL.setWardName("");
								equipSuggestEvictionDTL.setDistrictName("");
								equipSuggestEvictionDTL.setProvinceName("");
							}
							equipSuggestEvictionDTL.setEquipCategoryId(equipment.getEquipGroup().getEquipCategory().getId());
							equipSuggestEvictionDTL.setEquipCategoryName(equipment.getEquipGroup().getEquipCategory().getName());
							equipSuggestEvictionDTL.setHealthStatus(equipment.getHealthStatus());
							equipSuggestEvictionDTL.setObjectType(objectType);
							equipSuggestEvictionDTL.setShop(shop);
							equipSuggestEvictionDTL.setShopName(shop.getShopName());

							if (shop != null && shop.getType() != null && ShopSpecificType.NPP_MT.equals(shop.getType().getSpecificType())) {
//								equipSuggestEvictionDTL.setRegion(shop.getShopCode());
//								equipSuggestEvictionDTL.setChannel(shop.getParentShop().getShopCode());
								Shop kenh = shop.getParentShop();
								equipSuggestEvictionDTL.setChannel(kenh.getShopCode()); // kenh
								equipSuggestEvictionDTL.setRegion(kenh.getParentShop().getShopCode()); // IDP
							} else {
//								Shop mien = shop.getParentShop().getParentShop();
//								equipSuggestEvictionDTL.setRegion(mien.getShopCode());
//								equipSuggestEvictionDTL.setChannel(mien.getParentShop().getShopCode());
								Shop vung = shop.getParentShop();
								equipSuggestEvictionDTL.setChannel(vung.getShopCode()); // vung
								equipSuggestEvictionDTL.setRegion(vung.getParentShop().getShopCode()); // mien
							}

							equipSuggestEvictionDTL.setEquipCode(equipment.getCode());
							equipSuggestEvictionDTL.setEquipment(equipment);

							equipSuggestEvictionDTL.setStaff(staff);
							equipSuggestEvictionDTL.setStaffCode(staff.getStaffCode());
							equipSuggestEvictionDTL.setStaffName(staff.getStaffName());
							if (!StringUtil.isNullOrEmpty(staff.getPhone()) && !StringUtil.isNullOrEmpty(staff.getMobilephone())) {
								equipSuggestEvictionDTL.setPhone(staff.getPhone() + " / " + staff.getMobilephone());
							} else if (!StringUtil.isNullOrEmpty(staff.getPhone())) {
								equipSuggestEvictionDTL.setPhone(staff.getPhone());
							} else if (!StringUtil.isNullOrEmpty(staff.getMobilephone())) {
								equipSuggestEvictionDTL.setPhone(staff.getMobilephone());
							}

							equipSuggestEvictionDTL.setSerial(equipment.getSerial());
							equipSuggestEvictionDTL.setSuggestDate(timeEviction);
							equipSuggestEvictionDTL.setSuggestReason(reason);
							equipSuggestEvictionDTL.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());

							lstEquipSuggestEvictionDTLs.add(equipSuggestEvictionDTL);
						} else {
							// co loi trong nhan hang import
							isValidForm = false;
						}
						lstFailsInForm.add(StringUtil.addFailBean(row, msg));
					}
				}
				if (isValidForm && lstEquipSuggestEvictionDTLs != null && lstEquipSuggestEvictionDTLs.size() > 0 && equipSuggestEviction != null) {
					equipSuggestEviction = equipSuggestEvictionMgr.createEquipSuggestEvictionWithListDetail(equipSuggestEviction, lstEquipSuggestEvictionDTLs);
					lstFailsInForm = new ArrayList<CellBean>();
					lstEquipSuggestEvictionDTLs = null;
					equipSuggestEviction = null;
				} else {
					lstFails.addAll(lstFailsInForm);
					lstFailsInForm = new ArrayList<CellBean>();
					isValidForm = true;
					lstEquipSuggestEvictionDTLs = null;
					equipSuggestEviction = null;
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_RECORD_SUGGEST_EVICTION_FAIL);
			} else {
				isError = true;
				errMsg = R.getResource("import.excel.empty.content");
				return SUCCESS;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.importExcelRecord()"), createLogErrorStandard(actionStartTime));
			isError = true;
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}

		return SUCCESS;
	}

	/**
	 * Chuyen trang tao moi hay chinh sua bien ban
	 * 
	 * @author nhutnn
	 * @since 22/07/2015
	 */
	public String changeRecord() {
		actionStartTime = DateUtil.now();
		try {
			result.put(ERROR, false);
			if (id != null) {
				EquipSuggestEviction equipSuggestEviction = equipSuggestEvictionMgr.getEquipSuggestEvictionById(id);
				if (equipSuggestEviction != null) {
					if (equipSuggestEviction.getShop() != null && !checkShopInOrgAccessById(equipSuggestEviction.getShop().getId())) {
						return PAGE_NOT_PERMISSION;
					}
					code = equipSuggestEviction.getCode();
					createFormDate = DateUtil.toDateString(equipSuggestEviction.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					statusRecord = equipSuggestEviction.getStatus();
					note = equipSuggestEviction.getNote();
				} else {
					id = null;
					createFormDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
					statusRecord = StatusRecordsEquip.DRAFT.getValue();
				}
			} else {
				createFormDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
				statusRecord = StatusRecordsEquip.DRAFT.getValue();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.changeRecord()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}		
		return SUCCESS;
	}	
	
	/**
	 * Validate cac truong trong chi tiet bien ban
	 * 
	 * @author nhutnn 
	 * @param lstDetails
	 * @param lstEquipLendDetails
	 * @return
	 * @throws BusinessException
	 * @since 23/07/2015
	 */
	private String validateListDetail(List<EquipSuggestEvictionDetailVO> lstDetails, List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs, Long idRecord, Integer status) throws Exception {
		String msg = "";
		String value = "";
		for (int i = 0, sz = lstDetails.size(); i < sz; i++) {
			// lay chi tiet tu giao dien
			EquipSuggestEvictionDetailVO equipSuggestEvictionDetailVO = lstDetails.get(i);
			// chi tiet them vao danh sach dua len Database
			EquipSuggestEvictionDTL equipSuggestEvictionDTL = new EquipSuggestEvictionDTL();
			equipSuggestEvictionDTL.setId(equipSuggestEvictionDetailVO.getId());
			lstEquipSuggestEvictionDTLs.add(equipSuggestEvictionDTL);
			// kiem tra neu id==null la tu giao dien truyen xuong
			Shop shop = null;
			Customer customer = null;
			Date timeEviction = null;
			Equipment equipment = null;
			String reason = null;
			Staff staff = null;
			Integer objectType = null;
			
			// ma NPP
			value = equipSuggestEvictionDetailVO.getShopCode();
			msg = ValidateUtil.validateField(value, "customer.display.program.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
			if (StringUtil.isNullOrEmpty(msg)) {
				value = value.trim().toUpperCase();
				if (!checkShopInOrgAccessByCode(value)) {
					msg = R.getResource("common.shop.not.belong.user", value) + "\n";
				} else {
					shop = shopMgr.getShopByCode(value);
					if (shop == null) {
						msg = R.getResource("common.not.exist.in.db", R.getResource("customer.display.program.npp.code")) + "\n";
					} 
					//tamvnm: 09/09/2015 bo check status shop, kh
//					else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
//						msg = R.getResource("common.catalog.shop.status.not.active", value) + "\n";
//					} 
					else if (shop.getType() == null 
							|| !(ShopSpecificType.NPP.equals(shop.getType().getSpecificType())
							|| ShopSpecificType.NPP_MT.equals(shop.getType().getSpecificType()))) {
						msg += R.getResource("customer.shop.not.npp") + "\n";
					}
				}
			} 
			if (StringUtil.isNullOrEmpty(msg) && shop != null) {
				// ma KH
				value = equipSuggestEvictionDetailVO.getCustomerCode();
				msg = ValidateUtil.validateField(value, "khmuontu.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				if (StringUtil.isNullOrEmpty(msg)) {
					value = value.trim().toUpperCase();
					value = StringUtil.getFullCode(shop.getShopCode(), value);
					customer = customerMgr.getCustomerByCode(value);
					if (customer == null) {
						msg = R.getResource("common.not.exist.in.db", R.getResource("khmuontu.customer.code")) + "\n";
					} 
					//tamvnm: 09/09/2015 bo check status shop, kh
//					else if (!ActiveType.RUNNING.equals(customer.getStatus())) {
//						msg = R.getResource("common.catalog.customer.status.not.active", value) + "\n";
//					}
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				// thoi gian muon thu hoi tu
				value = equipSuggestEvictionDetailVO.getTimeEviction();
				msg = ValidateUtil.validateField(value, "equip.suggest.export.sheet1.thoigianmongthuhoitu", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
				if (StringUtil.isNullOrEmpty(msg)) {
					timeEviction = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
					if (DateUtil.compareDateWithoutTime(timeEviction, DateUtil.now()) == -1) {
						msg = R.getResource("equip.suggest.import.ngay.not.before.now", R.getResource("equip.suggest.export.sheet1.thoigianmongthuhoitu")) + "\n";
					}
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				// ma thiet bi
				value = equipSuggestEvictionDetailVO.getEquipCode();
				msg = ValidateUtil.validateField(value, "equipment.manager.equipment.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				if (StringUtil.isNullOrEmpty(msg)) {
					List<EquipSuggestEvictionDetailVO> lstVoTemp = null;
					if (idRecord != null) {
						EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter = new EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO>();
						List<Long> lstId = new ArrayList<Long>();
						lstId.add(id);
						filter.setLstId(lstId);
						filter.setEquipCode(value);
						lstVoTemp = equipSuggestEvictionMgr.getListEquipSuggestEvictionDetailVOByFilter(filter);
					}

					equipment = equipmentManagerMgr.getEquipmentByCode(value.trim().toUpperCase());
					if (equipment == null) {
						msg = R.getResource("common.not.exist.in.db", R.getResource("equipment.manager.equipment.code")) + "\n";
					} else if (!StatusType.ACTIVE.equals(equipment.getStatus())) {
						msg = R.getResource("common.catalog.status.in.active", R.getResource("equipment.manager.equipment.code")) + "\n";
					} else if ((lstVoTemp == null || lstVoTemp.size() == 0) && !EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) {
						msg = R.getResource("equip.suggest.eviction.err.equip.trading", R.getResource("equipment.manager.equipment.code")) + "\n";
					} else if (!EquipStockTotalType.KHO_KH.equals(equipment.getStockType()) || customer == null || !equipment.getStockId().equals(customer.getId())) {
						msg = R.getResource("equipment.row.invalid.makh.not.in.kho.manpp") + "\n";
					}
					if (StringUtil.isNullOrEmpty(msg)) {
						for (int j = 0, szj = lstDetails.size(); j < szj; j++) {
							String eqCode = lstDetails.get(j).getEquipCode();
							if (!StringUtil.isNullOrEmpty(eqCode) && equipment != null && i != j && eqCode.equals(equipment.getCode())) {
								String t = R.getResource("equip.suggest.eviction.equip", eqCode);
								msg = t + R.getResource("promotion.product.import.duplicate", j + 1) + "\n";
								break;
							}
						}
					}
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				// ly do
				reason = equipSuggestEvictionDetailVO.getReason();
				msg = ValidateUtil.validateField(reason, "equip.suggest.export.sheet1.lydo", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(msg) && (StatusRecordsEquip.DRAFT.getValue().equals(status) || StatusRecordsEquip.NO_APPROVAL.getValue().equals(status))) {
				// ma giam sat
				value = equipSuggestEvictionDetailVO.getStaffCode();
				msg = ValidateUtil.validateField(value, "equip.suggest.import.giamsat", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				if (StringUtil.isNullOrEmpty(msg)) {
					staff = staffMgr.getStaffByCode(value);
					if (staff == null) {
						msg = R.getResource("common.not.exist.in.db", R.getResource("equip.suggest.import.magiamsat", value)) + "\n";
					} else if (!ActiveType.RUNNING.equals(staff.getStatus())) {
						msg = R.getResource("common.catalog.status.in.active", R.getResource("equip.suggest.import.magiamsat", value)) + "\n";
					} else if (!(StaffSpecificType.SUPERVISOR.equals(staff.getStaffType().getSpecificType()) 
							|| StaffSpecificType.GSMT.equals(staff.getStaffType().getSpecificType()))) {
						msg = R.getResource("equipment.invalid.mgs") + "\n";
					} else if (shop == null || !checkNVGS(value, shop.getId())) {
						msg = R.getResource("equipment.manager.mgs.role.invalid") + "\n";
					} 
				} 
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				// kho nhan
				objectType = equipSuggestEvictionDetailVO.getStockTypeValue();
				msg = ValidateUtil.validateField(value, "equip.suggest.export.sheet1.khonhan", 250, ConstantManager.ERR_REQUIRE);
				if (objectType != null) {
					if (!TypeStockEquipLend.NCC.getValue().equals(objectType) && !TypeStockEquipLend.COMPANY.getValue().equals(objectType)) {
						msg = R.getResource("common.not.date", R.getResource("equip.suggest.export.sheet1.khonhan")) + "\n";
					}
				} else {
					msg = R.getResource("common.not.date", R.getResource("equip.suggest.export.sheet1.khonhan")) + "\n";
				}
			}

			if (!StringUtil.isNullOrEmpty(msg)) {
				msg = R.getResource("equip.proposal.update.dong", (i + 1)) + " " + msg;
				break;
			} else {
				if (StringUtil.isNullOrEmpty(equipSuggestEvictionDTL.getCreateUser())) {
					equipSuggestEvictionDTL.setCreateUser(currentUser.getStaffRoot().getStaffCode());
				} else {
					equipSuggestEvictionDTL.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
				}
				equipSuggestEvictionDTL.setCustomer(customer);
				equipSuggestEvictionDTL.setCustomerCode(customer.getCustomerCode());
				equipSuggestEvictionDTL.setCustomerName(customer.getCustomerName());
				if (!StringUtil.isNullOrEmpty(customer.getPhone()) && !StringUtil.isNullOrEmpty(customer.getMobiphone())) {
					equipSuggestEvictionDTL.setMobile(customer.getPhone() + " / " + customer.getMobiphone());
				} else if (!StringUtil.isNullOrEmpty(customer.getPhone())) {
					equipSuggestEvictionDTL.setMobile(customer.getPhone());
				} else if (!StringUtil.isNullOrEmpty(customer.getMobiphone())) {
					equipSuggestEvictionDTL.setMobile(customer.getMobiphone());
				}

				EquipmentFilter<EquipmentRecordDeliveryVO> filter = new EquipmentFilter<EquipmentRecordDeliveryVO>();
				filter.setCustomerCode(customer.getShortCode());
				filter.setEquipCode(equipment.getCode());
				filter.setShopCode(customer.getShop().getShopCode());
				EquipmentRecordDeliveryVO equipmentRecordDeliveryVO = equipmentManagerMgr.getEquipmentRecordDeliveryVOByFilter(filter);
				if (equipmentRecordDeliveryVO != null) {
					equipSuggestEvictionDTL.setRelation(equipmentRecordDeliveryVO.getToRelation());
					equipSuggestEvictionDTL.setRefresentative(equipmentRecordDeliveryVO.getToRepresentative());
					equipSuggestEvictionDTL.setHousenumber(equipmentRecordDeliveryVO.getToHouseNumber());
					equipSuggestEvictionDTL.setStreet(equipmentRecordDeliveryVO.getToStreet());
					equipSuggestEvictionDTL.setWardName(equipmentRecordDeliveryVO.getToWardName());
					equipSuggestEvictionDTL.setDistrictName(equipmentRecordDeliveryVO.getToDistrictName());
					equipSuggestEvictionDTL.setProvinceName(equipmentRecordDeliveryVO.getToProvinceName());
				} else {
					equipSuggestEvictionDTL.setRelation("");
					equipSuggestEvictionDTL.setRefresentative("");
					equipSuggestEvictionDTL.setHousenumber("");
					equipSuggestEvictionDTL.setStreet("");
					equipSuggestEvictionDTL.setWardName("");
					equipSuggestEvictionDTL.setDistrictName("");
					equipSuggestEvictionDTL.setProvinceName("");
				}
				equipSuggestEvictionDTL.setEquipCategoryId(equipment.getEquipGroup().getEquipCategory().getId());
				equipSuggestEvictionDTL.setEquipCategoryName(equipment.getEquipGroup().getEquipCategory().getName());
				equipSuggestEvictionDTL.setHealthStatus(equipment.getHealthStatus());
				equipSuggestEvictionDTL.setObjectType(objectType);
				equipSuggestEvictionDTL.setShop(shop);
				equipSuggestEvictionDTL.setShopName(shop.getShopName());

				if (shop != null && shop.getType() != null && ShopSpecificType.NPP_MT.equals(shop.getType().getSpecificType())) {
//					equipSuggestEvictionDTL.setRegion(shop.getShopCode());
//					equipSuggestEvictionDTL.setChannel(shop.getParentShop().getShopCode());
					Shop kenh = shop.getParentShop();
					equipSuggestEvictionDTL.setChannel(kenh.getShopCode()); // kenh
					equipSuggestEvictionDTL.setRegion(kenh.getParentShop().getShopCode()); // IDP
				} else {
//					Shop mien = shop.getParentShop().getParentShop();
//					equipSuggestEvictionDTL.setRegion(mien.getShopCode());
//					equipSuggestEvictionDTL.setChannel(mien.getParentShop().getShopCode());
					Shop vung = shop.getParentShop();
					equipSuggestEvictionDTL.setChannel(vung.getShopCode()); // vung
					equipSuggestEvictionDTL.setRegion(vung.getParentShop().getShopCode()); // mien
				}
				equipSuggestEvictionDTL.setEquipCode(equipment.getCode());
				equipSuggestEvictionDTL.setEquipment(equipment);

				equipSuggestEvictionDTL.setStaff(staff);
				equipSuggestEvictionDTL.setStaffCode(staff.getStaffCode());
				equipSuggestEvictionDTL.setStaffName(staff.getStaffName());
				if (!StringUtil.isNullOrEmpty(staff.getPhone()) && !StringUtil.isNullOrEmpty(staff.getMobilephone())) {
					equipSuggestEvictionDTL.setPhone(staff.getPhone() + " / " + staff.getMobilephone());
				} else if (!StringUtil.isNullOrEmpty(staff.getPhone())) {
					equipSuggestEvictionDTL.setPhone(staff.getPhone());
				} else if (!StringUtil.isNullOrEmpty(staff.getMobilephone())) {
					equipSuggestEvictionDTL.setPhone(staff.getMobilephone());
				}

				equipSuggestEvictionDTL.setSerial(equipment.getSerial());
				equipSuggestEvictionDTL.setSuggestDate(timeEviction);
				equipSuggestEvictionDTL.setSuggestReason(reason);
				equipSuggestEvictionDTL.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());
			}
		}

		return msg;
	}
	
	/**
	 * Tao moi hay chinh sua bien ban
	 * 
	 * @author nhutnn
	 * @since 23/07/2015
	 */
	public String updateRecord() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			result.put(ERROR, false);
			String msg = "";
			if (lstDetails != null && lstDetails.size() > 0) {
				Date dateForm = null;
//				EquipPeriod equipPeriod = null;
				if (id != null) {
					// chinh sua bien ban
					EquipSuggestEviction equipSuggestEviction = equipSuggestEvictionMgr.getEquipSuggestEvictionById(id);
					if (equipSuggestEviction != null) {
						// ngay lap
						if (createFormDate != null && !createFormDate.equals(equipSuggestEviction.getCreateFormDate()) 
							&& !StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord)
							&& StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus())) {
							dateForm = DateUtil.parse(createFormDate, DateUtil.DATE_FORMAT_DDMMYYYY);
							if (dateForm == null || DateUtil.compareDateWithoutTime(dateForm, DateUtil.now()) > 0) {
								msg = R.getResource("equip.proposal.import.ngaylap.not.more.now");
							} 
//							else {
//								List<EquipPeriod> lstEquipPeriods = equipmentManagerMgr.getListEquipPeriodByDate(dateForm);
//								if (lstEquipPeriods == null || lstEquipPeriods.size() == 0) {
//									msg = R.getResource("equip.proposal.import.ngaylap.not.in.period");
//								} else {
//									equipPeriod = lstEquipPeriods.get(0);
//								}
//							}
						}

						// danh sach chi tiet
						List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs = new ArrayList<EquipSuggestEvictionDTL>();
						if ((StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus()) || StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus())) && StringUtil.isNullOrEmpty(msg)) {
							msg = validateListDetail(lstDetails, lstEquipSuggestEvictionDTLs, id, equipSuggestEviction.getStatus());
						}
						// trang thai
						if (statusRecord != null && !statusRecord.equals(equipSuggestEviction.getStatus()) && StringUtil.isNullOrEmpty(msg)) {
							if (!((StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus()))
								|| (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus()))
								|| (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus()))
								|| (StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus()))
								|| (StatusRecordsEquip.APPROVED.getValue().equals(statusRecord) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipSuggestEviction.getStatus())) 
								|| (StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipSuggestEviction.getStatus())))) {
								msg += R.getResource("common.not.date", R.getResource("equip.proposal.import.status"));
							}
						}
						if (StringUtil.isNullOrEmpty(msg) && equipSuggestEviction.getStatus() != null
							&& (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) 
							&& StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus()))) {
							// neu chuyen tu Khong duyet -> Cho duyet
							// kiem tra thiet bi dang o trang thai tham gia giao dich
							for (int j = 0, szj = lstEquipSuggestEvictionDTLs.size(); j < szj; j++) {
								Equipment equipment = lstEquipSuggestEvictionDTLs.get(j).getEquipment();
								if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) {
									String t = R.getResource("equip.suggest.eviction.equip.in.record", equipment.getCode(), equipSuggestEviction.getCode());
									msg = R.getResource("equip.proposal.update.dong", (j + 1)) + " " + R.getResource("equip.suggest.eviction.err.equip.trading", t);
									break;
								}
							}
						}
						// thong bao loi 
						if (!StringUtil.isNullOrEmpty(msg)) {
							result.put(ERROR, true);
							result.put("errMsg", msg);
							return JSON;
						}
						// cap nhat du lieu
						if (dateForm != null) {
							equipSuggestEviction.setCreateFormDate(dateForm);
//							equipSuggestEviction.setEquipPriod(equipPeriod);
						}
						
						//ghi chu
						equipSuggestEviction.setNote(note);

						equipSuggestEviction.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
						EquipSuggestEvictionFilter<EquipSuggestEviction> filter = new EquipSuggestEvictionFilter<EquipSuggestEviction>();
						filter.setEquipSuggestEviction(equipSuggestEviction);
						filter.setLstEquipSuggestEvictionDTLs(lstEquipSuggestEvictionDTLs);
						filter.setStatus(statusRecord);
						equipSuggestEvictionMgr.updateEquipSuggestEviction(filter);
					}
				} else {
					// tao moi bien ban
					// kiem tra ky
//					equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//					if (equipPeriod == null) {
//						result.put(ERROR, true);
//						result.put("errMsg", R.getResource("equip.period.null"));
//						return JSON;
//					}
					// ngay lap
					msg = ValidateUtil.validateField(createFormDate, "equip.proposal.create.date", 8, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
					if (StringUtil.isNullOrEmpty(msg)) {
						dateForm = DateUtil.parse(createFormDate, DateUtil.DATE_FORMAT_DDMMYYYY);
						if (dateForm == null || DateUtil.compareDateWithoutTime(dateForm, DateUtil.now()) > 0) {
							msg = R.getResource("equip.proposal.import.ngaylap.not.more.now");
						} 
//						else {
//							List<EquipPeriod> lstEquipPeriods = equipmentManagerMgr.getListEquipPeriodByDate(dateForm);
//							if (lstEquipPeriods == null || lstEquipPeriods.size() == 0) {
//								msg = R.getResource("equip.proposal.import.ngaylap.not.in.period");
//							} else {
//								equipPeriod = lstEquipPeriods.get(0);
//							}
//						}
					}
					// trang thai
					if (!StatusRecordsEquip.DRAFT.getValue().equals(statusRecord)) {
						msg += R.getResource("common.not.date", R.getResource("equip.proposal.import.status"));
					}
					// danh sach chi tiet
					List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs = new ArrayList<EquipSuggestEvictionDTL>();
					if (StringUtil.isNullOrEmpty(msg)) {
						msg = validateListDetail(lstDetails, lstEquipSuggestEvictionDTLs, null, statusRecord);
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						result.put(ERROR, true);
						result.put("errMsg", msg);
						return JSON;
					}

					// tao moi du lieu
					EquipSuggestEviction equipSuggestEviction = new EquipSuggestEviction();
					equipSuggestEviction.setCreateFormDate(dateForm);
					equipSuggestEviction.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());
					equipSuggestEviction.setCreateUser(currentUser.getStaffRoot().getStaffCode());
//					equipSuggestEviction.setEquipPriod(equipPeriod);
					//ghi chu
					equipSuggestEviction.setNote(note);
					Shop sh = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
					equipSuggestEviction.setShop(sh);
					equipSuggestEviction.setStatus(StatusRecordsEquip.DRAFT.getValue());
					equipSuggestEviction = equipSuggestEvictionMgr.createEquipSuggestEvictionWithListDetail(equipSuggestEviction, lstEquipSuggestEvictionDTLs);
					result.put("id", equipSuggestEviction.getId());
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("equip.proposal.update.not.detail"));
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.updateRecord()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Lay chi tiet bien ban
	 * 
	 * @author nhutnn
	 * @since 22/07/2015
	 */
	public String getListDetailInRecord() {
		actionStartTime = DateUtil.now();
		try {
			result.put("rows", new ArrayList<EquipRecordVO>());
			result.put("total", 0);
			if(id != null) {
				EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter = new EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO>();
				List<Long> lstId = new ArrayList<Long>();
				lstId.add(id);
				filter.setLstId(lstId);
				List<EquipmentSuggestEvictionVO> lstEquipmentSuggestEvictionVOs = equipSuggestEvictionMgr.getListEquipSuggestEvictionDetailVOForExportByFilter(filter);
	
				if (lstEquipmentSuggestEvictionVOs != null && lstEquipmentSuggestEvictionVOs.size() > 0) {
					EquipmentSuggestEvictionVO equipmentSuggestEvictionVO = lstEquipmentSuggestEvictionVOs.get(0);
					if (equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDetailVOs() != null && !equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDetailVOs().isEmpty()) {
						result.put("rows", equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDetailVOs());
						result.put("total", equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDetailVOs().size());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.getListDetailInRecord()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * Tim kiem khach hang
	 * @author nhutnn
	 * @since 22/07/2015
	 * @return
	 */
	public String searchCustomer() {
		actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		result.put("rows", new ArrayList<CustomerVO>());
		result.put("total", 0);
		try {
			CustomerFilter<CustomerVO> filter = new CustomerFilter<CustomerVO>();
			//set cac tham so tim kiem
			KPaging<CustomerVO> kPaging = new KPaging<CustomerVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			
			List<Long> lstShopIdRoot = new ArrayList<Long>();
			lstShopIdRoot.add(currentUser.getShopRoot().getShopId());
			filter.setLstShopId(lstShopIdRoot);
			//tamvnm: 09/09/2015 bo check status shop, kh
//			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setIsCheckStatus(false);
			// Lan dau mo popUp neu co shopCodeEditor thi set vao shopCode
			if (!StringUtil.isNullOrEmpty(shopCodeEditor)) {
				filter.setShopCode(shopCodeEditor);
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				if (Boolean.TRUE.equals(isNPP)) {
					if (!checkShopInOrgAccessByCode(shopCode)) {
						return JSON;
					} else {
						Shop shop = shopMgr.getShopByCode(shopCode);
						if (shop != null
							&& (shop.getType() == null 
							|| !(ShopSpecificType.NPP.equals(shop.getType().getSpecificType()) 
							|| ShopSpecificType.NPP_MT.equals(shop.getType().getSpecificType())))) {
							return JSON;
						}
					}
				}
				filter.setShopCode(shopCode);
			}
			if (!StringUtil.isNullOrEmpty(shopName)) {
				filter.setShopName(shopName);
			}
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				filter.setShortCode(shortCode);
			}
			if (!StringUtil.isNullOrEmpty(customerName)) {
				filter.setCustomerName(customerName);
			}

			//goi ham thuc thi tim kiem
			ObjectVO<CustomerVO> objVO = customerMgr.searchListCustomerFullVOByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.searchCustomer()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Lay danh sach thiet bi
	 * @author nhutnn
	 * @since 23/07/2015
	 * @return
	 */
	public String getListEquip() {
		actionStartTime = DateUtil.now();
		result.put("rows", new ArrayList<EquipmentVO>());
		result.put("total", 0);
		try {
			if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null && currentUser.getRoleToken() != null) {
				EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
				filter.setStatus(StatusType.ACTIVE.getValue());
				filter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
				filter.setStockType(EquipStockTotalType.KHO_KH.getValue());
				
				filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				filter.setShopRootId(currentUser.getShopRoot().getShopId());
				if (!StringUtil.isNullOrEmpty(shopCode) && !StringUtil.isNullOrEmpty(shortCode)) {
					filter.setStockCode(shopCode + shortCode);
					Shop shop = shopMgr.getShopByCode(shopCode);
					if (shop != null && shop.getId() != null) {
						List<Long> lstShopId = new ArrayList<Long>();
						lstShopId.add(shop.getId());
						filter.setLstShopId(lstShopId);
					}
				}
				if (stockId != null) {
					filter.setStockId(stockId);
				}
	
				//goi ham thuc thi tim kiem
				ObjectVO<EquipmentVO> objVO = equipmentManagerMgr.getListEquipmentByFilterNew(filter);
				if (objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
					result.put("lstEquip", objVO.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.getListEquip()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Lay bien ban giao nhan
	 * @author nhutnn
	 * @since 23/07/2015
	 * @return
	 */
	public String getEquipDeliveryRecord() {
		actionStartTime = DateUtil.now();
		result.put("rows", new ArrayList<EquipmentVO>());
		result.put("total", 0);
		try {
			EquipmentFilter<EquipmentRecordDeliveryVO> filter = new EquipmentFilter<EquipmentRecordDeliveryVO>();
			if(!StringUtil.isNullOrEmpty(customerCode)){
				filter.setCustomerCode(customerCode);
			}
			if(!StringUtil.isNullOrEmpty(equipCode)){
				filter.setEquipCode(equipCode);
			}
			if(!StringUtil.isNullOrEmpty(shopCode)){
				filter.setShopCode(shopCode);
			}
			
			//goi ham thuc thi tim kiem
			EquipmentRecordDeliveryVO equipmentRecordDeliveryVO = equipmentManagerMgr.getEquipmentRecordDeliveryVOByFilter(filter);			
			result.put("equipmentRecordDeliveryVO", equipmentRecordDeliveryVO);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.getEquipDeliveryRecord()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
//	
//	/**
//	 * Lay danh sach nhan vien GS
//	 * 
//	 * @author nhutnn
//	 * @since 23/07/2015
//	 * @return
//	 */
//	public String getListGSNPP() {
//		result.put("rows", new ArrayList<StaffVO>());
//		try {
//			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
//			// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
//			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
//			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
//			filter.setStatus(ActiveType.RUNNING.getValue());
//			filter.setIsGetShopStopped(Boolean.TRUE);
//			
//			ShopToken shopVo = null;
//			if (!StringUtil.isNullOrEmpty(shopCode)) {
//				if (checkShopInOrgAccessByCode(shopCode)) {
//					shopVo = getShopTockenInOrgAccessByCode(shopCode);
//					filter.setShopId(shopVo.getShopId());
//				} else {
//					return JSON;
//				}
//			} else {
//				return JSON;
//			}
//			//			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
//			//				filter.setIsLstChildStaffRoot(true);
//			//				objVO = staffMgr.getListGSByStaffRootWithInParentStaffMap(filter);
//			//			} else 
//			if (shopVo != null
//				&& (ShopObjectType.MIEN_ST.getValue().equals(shopVo.getObjectType()) 
//				|| ShopObjectType.ST.getValue().equals(shopVo.getObjectType()) 
//				|| ShopObjectType.NPP_KA.getValue().equals(shopVo.getObjectType())
//				|| ShopObjectType.VUNG_KA.getValue().equals(shopVo.getObjectType()) 
//				|| ShopObjectType.MIEN_KA.getValue().equals(shopVo.getObjectType()) 
//				|| ShopObjectType.KA.getValue().equals(shopVo.getObjectType()))) {
//				List<Integer> lstObjectType = new ArrayList<Integer>();
//				lstObjectType.add(StaffObjectType.GSMT.getValue());
//				lstObjectType.add(StaffObjectType.GSKA.getValue());
//				lstObjectType.add(StaffObjectType.NVGS.getValue());
//				filter.setLstObjectType(lstObjectType);
//
//				filter.setIsLstChildStaffRoot(false);
//				objVO = staffMgr.getListGSByStaffRootKAMT(filter);
//			} else {
//				filter.setIsLstChildStaffRoot(false);
//				filter.setGsmt(StaffObjectType.GSMT.getValue());
//				filter.setGska(StaffObjectType.GSKA.getValue());
//				objVO = staffMgr.getListGSByStaffRootWithNVBH(filter);
//			}
//
//			if (objVO != null) {
//				result.put("lstStaff", objVO.getLstObject());
//			}
//
//		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
//		}
//		return JSON;
//	}
	
	/**
	 * Xu ly getListGSNPP
	 * @author vuongmq
	 * @return String
	 * @throws Exception
	 * @since Mar 7, 2016
	 */
	public String getListGSNPP() throws Exception {
		List<StaffVO> lstStaffGS = new ArrayList<StaffVO>();
		result.put("lstStaff", lstStaffGS);
		// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
		if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null) {
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setUserId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			List<Integer> lstObjectType = new ArrayList<Integer>();
			lstObjectType.add(StaffSpecificType.GSMT.getValue());
			lstObjectType.add(StaffSpecificType.SUPERVISOR.getValue());
			filter.setLstObjectType(lstObjectType);
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				if (checkShopInOrgAccessByCode(shopCode)) {
					Shop shop = shopMgr.getShopByCode(shopCode);
					if (shop != null) {
						filter.setShopId(shop.getId());
						lstStaffGS = staffMgr.getListGSByFilter(filter);
						if (lstStaffGS != null) {
							result.put("lstStaff", lstStaffGS);
						}
					} else {
						return JSON;
					}
				} else {
					return JSON;
				}
			} else {
				return JSON;
			}
		}
		return JSON;
	}
	
	/**
	 * In bien ban de nghi thu hoi
	 * 
	 * @author nhutnn
	 * @since 25/07/2015
	 * @return
	 */
	public String viewPrintEquipSuggestEviction() {
		actionStartTime = DateUtil.now();
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("errMsg", R.getResource("equip.proposal.export.not.record.export"));
				result.put(ERROR, true);
				return JSON;
			}
			EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter = new EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO>();
			filter.setLstId(lstIdRecord);
			List<EquipmentSuggestEvictionVO> data = equipSuggestEvictionMgr.getListEquipSuggestEvictionDetailVOForExportByFilter(filter);
			if (data == null || data.size() == 0) {
				result.put("errMsg", R.getResource("equip.err.excell"));
				result.put(ERROR, true);
				return JSON;
			}
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			// file in						
			String templatePath = ServletActionContext.getServletContext().getRealPath("/") + File.separatorChar + Configuration.getEquipmentPrintTemplatePath() + File.separator + ConstantManager.TEMPLATE_RECORD_SUGGEST_EVICTION_PRINT+ FileExtension.JASPER.getValue();
			File fileReport = new File(templatePath);
			if (!fileReport.exists()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.respon.not.template"));
				return JSON;
			}
			// dat Parameters
			Map<String, Object> parameters = new HashMap<String, Object>();
//			parameters.put("kenh", kenh);
//			parameters.put("mien", mien);
			parameters.put("printDate", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
			JRDataSource dataSource = new JRBeanCollectionDataSource(data);
			// file output
			String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(ConstantManager.TEMPLATE_RECORD_SUGGEST_EVICTION_PRINT + FileExtension.PDF.getValue())).toString());
			String outputPath = Configuration.getStoreRealPath() + outputFile;
			String downloadPath = Configuration.getExportExcelPath() + outputFile;
			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);

			JRAbstractExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
			exporter.exportReport();

			result.put(ERROR, false);
			String downloadUrl = ReportUtils.addReportTokenToDownloadFileUrl(downloadPath, reportToken);
			result.put(REPORT_PATH, downloadUrl);
			// set link
			session.setAttribute("downloadPath", downloadUrl);
			/** ATTT duong dan */
			MemcachedUtils.putValueToMemcached(reportToken, downloadPath, retrieveReportMemcachedTimeout());

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.viewPrintEquipSuggestEviction()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Xuat bien ban hop dong giao nhan
	 *  
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	public String exportEvictionRecord() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			result.put(ERROR, false);
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				List<FormErrVO> lstError = new ArrayList<FormErrVO>();				
				for(int i=0, sz = lstIdRecord.size(); i<sz; i++){
					FormErrVO formErrVO = new FormErrVO();
					EquipSuggestEviction equipSuggestEviction = equipSuggestEvictionMgr.getEquipSuggestEvictionById(lstIdRecord.get(i));
					if (equipSuggestEviction != null) {
						if (!StatusRecordsEquip.APPROVED.getValue().equals(equipSuggestEviction.getStatus())) {
							formErrVO.setFormCode(equipSuggestEviction.getCode());
							formErrVO.setFormStatusErr(1);
						} 
						if (StatusDeliveryEquip.EXPORTED.getValue().equals(equipSuggestEviction.getDeliveryStatus())) {
							formErrVO.setFormCode(equipSuggestEviction.getCode());
							formErrVO.setDeliveryStatusErr(1);
						}
					}
					if(formErrVO.getFormStatusErr() != null || formErrVO.getDeliveryStatusErr() != null){
						lstError.add(formErrVO);
						lstIdRecord.remove(i);
						sz--;
						i--;
					}
				}
				if(lstIdRecord.size() > 0){
					EquipSuggestEvictionFilter<EquipSuggestEviction> filter = new EquipSuggestEvictionFilter<EquipSuggestEviction>();
					filter.setLstId(lstIdRecord);
					filter.setStatus(StatusRecordsEquip.APPROVED.getValue());
					filter.setUserName(currentUser.getStaffRoot().getStaffCode());
					equipmentManagerMgr.exportListEquipEvictionExportByEquipSuggestEviction(filter);
				}
				if (lstError.size() > 0) {
					result.put("lstRecordError", lstError);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.exportEvictionRecord()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}	
	
	/**
	 * Lay chi tiet bien ban thu hoi
	 * 
	 * @author nhutnn
	 * @since 27/07/2015
	 */
	public String getListEvictionExportAgain() {
		actionStartTime = DateUtil.now();
		try {
			result.put(ERROR, false);
			result.put("rows", new ArrayList<EquipRecordVO>());
			result.put("total", 0);
			if(id != null) {				
				EquipmentFilter<EquipmentEvictionVO> equipmentFilter = new EquipmentFilter<EquipmentEvictionVO>();
				equipmentFilter.setCurShopCode(currentUser.getShopRoot().getShopCode());
				equipmentFilter.setEquipSuggestEvictionId(id);
				List<EquipmentEvictionVO> lstEquipmentEvictionVOs = equipmentManagerMgr.getListFormEvictionVOForEquipSuggestEvictionByFilter(equipmentFilter);
				if (lstEquipmentEvictionVOs != null && lstEquipmentEvictionVOs.size() > 0) {
					result.put("total", lstEquipmentEvictionVOs.size());
					result.put("rows", lstEquipmentEvictionVOs);
				} else {
					result.put("errMsg", R.getResource("equip.suggest.export.eviction.not.record.cancel"));
					result.put(ERROR, true);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.getListEvictionExportAgain()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Xuat lai bien ban thu hoi lan nua
	 *  
	 * @author nhutnn
	 * @since 27/07/2015
	 */
	public String exportEvictionRecordAgain() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			result.put(ERROR, false);
			if(id != null && lstCustomerId != null) {
				EquipSuggestEvictionFilter<EquipSuggestEviction> filter = new EquipSuggestEvictionFilter<EquipSuggestEviction>();
				List<Long> lstId = new ArrayList<Long>();
				lstId.add(id);
				filter.setLstId(lstId);
				filter.setLstCustomerId(lstCustomerId);
				filter.setStatus(StatusRecordsEquip.APPROVED.getValue());
				filter.setUserName(currentUser.getStaffRoot().getStaffCode());
				equipmentManagerMgr.exportListEquipEvictionExportByEquipSuggestEviction(filter);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentSuggestEvictionAction.exportEvictionRecordAgain()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	// GETTER && SETTER
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurShopCode() {
		return curShopCode;
	}

	public void setCurShopCode(String curShopCode) {
		this.curShopCode = curShopCode;
	}

	public Integer getStatusRecord() {
		return statusRecord;
	}

	public void setStatusRecord(Integer statusRecord) {
		this.statusRecord = statusRecord;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLstShop() {
		return lstShop;
	}

	public void setLstShop(String lstShop) {
		this.lstShop = lstShop;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(String createFormDate) {
		this.createFormDate = createFormDate;
	}

	public Integer getStatusDelivery() {
		return statusDelivery;
	}

	public void setStatusDelivery(Integer statusDelivery) {
		this.statusDelivery = statusDelivery;
	}

	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}

	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopCodeEditor() {
		return shopCodeEditor;
	}

	public void setShopCodeEditor(String shopCodeEditor) {
		this.shopCodeEditor = shopCodeEditor;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public List<EquipSuggestEvictionDetailVO> getLstDetails() {
		return lstDetails;
	}

	public void setLstDetails(List<EquipSuggestEvictionDetailVO> lstDetails) {
		this.lstDetails = lstDetails;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}

	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

	public Integer getFlagRecordStatus() {
		return flagRecordStatus;
	}

	public void setFlagRecordStatus(Integer flagRecordStatus) {
		this.flagRecordStatus = flagRecordStatus;
	}

	public Boolean getIsNPP() {
		return isNPP;
	}

	public void setIsNPP(Boolean isNPP) {
		this.isNPP = isNPP;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}

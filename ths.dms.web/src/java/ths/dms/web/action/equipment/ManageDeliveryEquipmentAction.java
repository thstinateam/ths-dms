package ths.dms.web.action.equipment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFooter;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.struts2.ServletActionContext;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipProposalBorrowMgr;
import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ReportManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipDeliveryRecDtl;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLender;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.DeliveryType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryPrintVO;
import ths.dms.core.entities.vo.EquipmentDeliveryReturnVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.EquipmentStockDeliveryVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

/**
 * 
 */
/**
 * @author tamvnm
 *
 */
public class ManageDeliveryEquipmentAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	CommonMgr commonMgr;
	StaffMgr staffMgr;
	ShopMgr shopMgr;
	CustomerMgr customerMgr;
	EquipmentManagerMgr equipmentManagerMgr;
	EquipRecordMgr equipRecordMgr;
	ReportManagerMgr reportManagerMgr;
	
	public static final int PRINT_PDA_XNKV_NPP = 0;
	public static final int PRINT_PDA = 1;
	public static final int PRINT_XNKV = 2;
	public static final int PRINT_NPP = 3;
	public static final int PRINT_DELIVERY = 4;
	public static final int PRINT_RECOR= 5;

	private static final Integer ENABLE = 2;
	/** ma nhan vien */
	private String staffCode;

	/** ma nhan vien */
	private String staffName;

	/** ma nhan vien */
	private Long staffId;

	/** ma khach hang */
	private String customerCode;

	/** ten khach hang */
	private String customerName;

	/** ngay tao tu */
	private String fromDate;

	/** ngay tao den */
	private String toDate;

	/** so hop dong */
	private String numberContract;

	/** Ngay hop dong */
	private String contractDate;

	/** ma bien ban */
	private String recordCode;

	/** trang thai bien ban */
	private Integer statusRecord;

	/** trang thai giao nhan */
	private Integer statusDelivery;

	/** trang thai giao nhan */
	private Long idRecordDelivery;
	private Long id;

	/** ma thiet bi */
	private String equipCode;

	/** so seri */
	private String seriNumber;

	/** loai thiet bi */
	private Long categoryId;

	/** nhom thiet bi */
	private Long groupId;

	/** nha cung cap thiet bi */
	private Long providerId;

	/** nam san xuat thiet bi */
	private Integer yearManufacture;

	/** ma kho thiet bi */
	private String stockCode;

	/** file excel import */
	private File excelFile;
	private String excelFileContentType;

	/** danh sach bien ban */
	private List<Long> lstIdRecord;
	
	/** don vi */
	private String shopCode;
	
	/** ma bien ban de nghi muon thiet bi */
	private String equipLendCode;
	
	/** ngay hop dong tu */
	private String fromContractDate;

	/** ngay hop dong den */
	private String toContractDate;
	
	/** trang thai in */
	private Integer statusPrint;
	
	/** Shop id user dang nhap */
	private Long shopId;
	
	/** Shop code user dang nhap */
	private String curShopCode;

	/** Don vi ben cho muon */
	private String fromShopCode;
	
	/** Dia chi ben cho muon */
	private String fromObjectAddress;
	
	/** Dien thoai ben cho muon */
	private String fromObjectPhone;
	
	/** Ma so ben cho muon */
	private String fromObjectTax;
	
	/** Dai dien ben cho muon */
	private String fromRepresentative;
	
	/** Chuc vu ben cho muon */
	private String fromObjectPosition;
	
	/** So FAX ben cho muon */
	private String fromFax;
	
	/** Giay uy quyen ben cho muon */
	private String fromPage;
	
	/** Noi cap giay uy quyen ben cho muon */
	private String fromPagePlace;
	
	/** Ngay cap giay uy quyen ben cho muon */
	private String fromPageDate;
	
	/** Don vi ben muon */
	private String toShopCode;
	
	/** Khach hang ben muon */
	private Long customerId;
	
	/** Dia chi ben muon */
	private String toObjectAddress;
	
	/** Dai dien ben muon */
	private String toRepresentative;
	
	/** Chuc vu ben muon */
	private String toObjectPosition;
	
	/** Tu mat */
	private BigDecimal freezer;
	
	/** Tu dong */
	private BigDecimal refrigerator;
	
	/** So CMND ben muon */
	private String toIdNO;
	
	/** Noi cap CMND ben muon */
	private String toIdNOPlace;
	
	/** Ngay cap CMND ben muon */
	private String toIdNODate;
	
	/** so giay phep DKKD*/
	private String toBusinessLicense;
	
	/** ngay cap GP KDDK*/
	private String toBusinessDate;
	
	/**Noi cap GP DKKD*/
	private String toBusinessPlace;
	
	/**Noi cap GP DKKD*/
	private String kenh;
	
	/**Noi cap GP DKKD*/
	private String mien;
	
	/** So nha*/
	private String addressName;
	
	/** duong-thon ap*/
	private String street;
	
	/**tinh - tp */
	private String provinceName;
	
	/** quan-huyen */
	private String districtName;
	
	/** phuong - xa*/
	private String wardName;
	
	/** ghi chu*/
	private String note;
	
	private String toPermanentAddress; //Dia chi thuong tru
	
	/** danh sach thong tin thiet bi */
	//private List<Long> lstIdEquip;
	private String lstEquipDelete;
	private String lstEquipAdd;
	private List<String> lstEquipCode;
	private List<String> lstSeriNumber;
	private List<String> lstContentDelivery;
	private List<String> lstGroup;
	private List<String> lstProvider;
	private List<String> lstStock;
	private List<String> lstHealth;
	private List<Integer> lstManufacturingYear;
	private List<BigDecimal> lstEquipPrice;
	
	/**List Danh sach noi dung giao nhan*/
	private List<ApParamEquip> lstContent;
	private ApParamEquip ContentNew;
	private List<Integer> lstDepreciation;
	List<Long> lstEquipGroupId;
	private String lstShop; 
	private List<Long> lstIdChecked;
	private Long equipGroupId;
	private Integer printType;
	/** danh sach bien ban */
	private List<StaffVO> lstGsnpp;

	private List<EquipmentVO> lstEquipProvider;
	private List<EquipmentVO> lstEquipGroup;
	private List<ApParam> lstHealthy;
	
	/** danh sach thiet bi */
	private List<EquipmentDeliveryVO> equipments;

	// lst bien ban de in
	private List<Map<String, Object>> listObjectBeans;
	
	// mot so thong tin de hien thi appletView
	private String downloadPath;
	private Integer windowWidth;	
	private Integer windowHeight;
	
	private Integer equipNumberDeprec;
	
	// mot so thong tin cho upload file
	private List<File> lstFile;
	private List<String> lstFileContentType;
	private List<String> lstFileFileName;
	private List<FileVO> lstFileVo;
	// danh sach file xoa
	private String equipAttachFileStr;

	private String lstEquipmentCode;
	private String code;
	private String name;
	private String fileNamePDAReal;
	private String fileNameXNKVReal;
	private List<String> lstPathReal;
	private String createDate;
	
	
	@Override
	public void prepare() throws Exception {
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
		equipProposalBorrowMgr = (EquipProposalBorrowMgr) context.getBean("equipProposalBorrowMgr");
		reportManagerMgr = (ReportManagerMgr) context.getBean("reportManagerMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		if(shop != null) { 
			shopId = shop.getId();
			curShopCode = shop.getShopCode();
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem danh sach bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since December 11,2014
	 */
	public String searchRecordDelivery() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentRecordDeliveryVO>());
		try {
			KPaging<EquipmentRecordDeliveryVO> kPaging = new KPaging<EquipmentRecordDeliveryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentRecordDeliveryVO>();
			equipmentFilter.setkPaging(kPaging);
			Date fDate = null;
			Date tDate = null;
			Date fContractDate = null;
			Date tContractDate  = null;

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				equipmentFilter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				equipmentFilter.setToDate(tDate);
			}
			if (!StringUtil.isNullOrEmpty(fromContractDate)) {
				fContractDate = DateUtil.parse(fromContractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				equipmentFilter.setFromContractDate(fContractDate);
			}
			if (!StringUtil.isNullOrEmpty(toContractDate)) {
				tContractDate = DateUtil.parse(toContractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				equipmentFilter.setToContractDate(tContractDate);
			}
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				equipmentFilter.setStaffCode(staffCode);
			}
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				equipmentFilter.setCustomerCode(customerCode);
			}
			
			Shop shop = null;
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			if(shop != null) { 
				curShopCode = shop.getShopCode();
				if (!StringUtil.isNullOrEmpty(curShopCode)) {
					equipmentFilter.setCurShopCode(curShopCode);
				}
			}
			
			
//			if (!StringUtil.isNullOrEmpty(customerName)) {
//				String nameText = Unicode2English.codau2khongdau(customerName);
//				equipmentFilter.setCustomerName(nameText);
//			}
			if (!StringUtil.isNullOrEmpty(numberContract)) {
				equipmentFilter.setNumberContract(numberContract);
			}
			if (!StringUtil.isNullOrEmpty(recordCode)) {
				equipmentFilter.setRecordCode(recordCode);
			}
			if (statusRecord != null) {
				equipmentFilter.setStatusRecord(statusRecord);
			}
			if (statusDelivery != null) {
				equipmentFilter.setStatusDelivery(statusDelivery);
			}
			
			if(statusPrint != null) {
				equipmentFilter.setStatusPrint(statusPrint);
			}
			if (!StringUtil.isNullOrEmpty(lstShop)) {
				equipmentFilter.setShopCode(lstShop);
			}
			
			if (!StringUtil.isNullOrEmpty(equipLendCode)) {
				equipmentFilter.setEquipLendCode(equipLendCode);
			}
			
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setEquipCode(equipCode);
			}
			
			ObjectVO<EquipmentRecordDeliveryVO> equipmentRecordDeliverys = equipmentManagerMgr.getListEquipmentRecordDeliveryVOByFilter(equipmentFilter);
			if (equipmentRecordDeliverys.getLstObject() != null) {
				result.put("total", equipmentRecordDeliverys.getkPaging().getTotalRows());
				result.put("rows", equipmentRecordDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Xuat file template import
	 * 
	 * @author tamvnm
	 * @since May 31,2015
	 */
	public String getFileTemplate() {
		String url = "";
		/**ATTT duong dan*/
		String reportToken = retrieveReportToken(reportCode);
		if (StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		try {
			List<ApParamEquip> lstContent = getListContentDelivety();
			// lay danh sach nhom thiet bi
			try {
				EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
				equipmentFilter.setkPaging(null);
				equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
				ObjectVO<EquipmentVO> eGroup = equipmentManagerMgr.searchEquipmentGroupVObyFilter(equipmentFilter);
				if (eGroup.getLstObject() != null) {
					lstEquipGroup = eGroup.getLstObject();
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
			
			// lay danh sach nha cung cap, tinh trang thiet bi
			try {
				EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
				equipmentFilter.setkPaging(null);
				equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
				lstEquipProvider = equipmentManagerMgr.getListEquipmentProvider(equipmentFilter);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
			lstHealthy = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
			
			//Danh sach kho
			List<EquipStockVO> lstStock = getListStockShop("");
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
			String templateFileName = folder + ConstantManager.TEMPLATE_IMPORT_RECORD_DELIVERY_EX;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_IMPORT_RECORD_DELIVERY_EX;
			String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("lstContent", lstContent);
			params.put("lstGroup", lstEquipGroup);
			params.put("lstProvider", lstEquipProvider);
			params.put("lstHealth", lstHealthy);
			params.put("lstStock", lstStock);
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (InvalidFormatException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (IOException e) {
				LogUtility.logError(e, e.getMessage());
			}
			url = Configuration.getStoreImportFailDownloadPath() + outputName;
			result.put(REPORT_PATH, url);
			result.put(ERROR, false);
			result.put("hasData", true);
			/**ATTT duong dan*/
			MemcachedUtils.putValueToMemcached(reportToken, url, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Tim kiem danh sach lich su giao nhan
	 * 
	 * @author nhutnn
	 * @since December 11,2014
	 */
	public String searchHistoryDelivery() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentEvictionVO>());
		try {
			List<EquipmentEvictionVO> equipmentHistoryDeliverys = equipmentManagerMgr.getHistory(idRecordDelivery, EquipTradeType.DELIVERY);
			if (equipmentHistoryDeliverys != null) {
				result.put("total", equipmentHistoryDeliverys.size());
				result.put("rows", equipmentHistoryDeliverys);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * lay danh sach noi dung muon tu tu` equipParam.
	 * 
	 * @author tamvnm
	 * @since May 26,2015
	 */
	public List<ApParamEquip> getListContentDelivety() {
		List<ApParamEquip> lstContent = new ArrayList<ApParamEquip>();
		try {
			lstContent = apParamEquipMgr.getListApParamEquipByType(ApParamEquipType.DELIVERY_CONTENT, ActiveType.RUNNING);
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstContent;
		
	}

	/**
	 * Kiem tra content co thuoc equipParam?
	 * 
	 * @author tamvnm
	 * @since May 24,2015
	 */
	public Boolean isContent(String equipParamCode, List<ApParamEquip> lstContent) {
		Boolean isTrue = false;
		try {
			if (lstContent == null || lstContent.size() == 0) {
				return false;
			}
			for (int i = 0, isize = lstContent.size(); i < isize; i++) {
				if (lstContent.get(i).getApParamEquipCode().equals(equipParamCode)) {
					isTrue = true;
				}
			}
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			return false;
		}
		return isTrue;
	}
	
	/**
	 * Kiem tra chi tiet bien ban da day` du chua
	 * 
	 * @author tamvnm
	 * @since May 24,2015
	 */
	public Boolean isTrueSaveStatus(EquipDeliveryRecord equipDelivery, List<ApParamEquip> lstContent){
		if (equipDelivery != null) {
			if (equipDelivery.getContractNumber() == null) {
				return false;
			}
			if (equipDelivery.getContractCreateDate() == null) {
				return false;
			}
			if (equipDelivery.getContractNumber() == null) {
				return false;
			}
			if (equipDelivery.getFromObjectName() == null) {
				return false;
			}
			if (equipDelivery.getFromObjectAddress() == null) {
				return false;
			}
			if (equipDelivery.getFromObjectPhone() == null) {
				return false;
			}
			if (equipDelivery.getFromObjectTax() == null) {
				return false;
			}
			if (equipDelivery.getToObjectName() == null) {
				return false;
			}
			if (equipDelivery.getToObjectAddress() == null) {
				return false;
			}
			if (equipDelivery.getToShop() == null) {
				return false;
			}
			if (equipDelivery.getCustomer() == null) {
				return false;
			}
//			if (equipDelivery.getToBusinessLincense() == null) {
//				return false;
//			}
//			if (equipDelivery.getToBusinessPlace() == null) {
//				return false;
//			}
//			if (equipDelivery.getToBusinessDate() == null) {
//				return false;
//			}
			if (equipDelivery.getToRepresentative() == null) {
				return false;
			}
			if (equipDelivery.getToPosition() == null) {
				return false;
			}
			if (equipDelivery.getStaff() == null) {
				return false;
			}
//			if (equipDelivery.getFreezer() == null) {
//				return false;
//			}
//			if (equipDelivery.getRefrigerator() == null) {
//				return false;
//			}
			if (equipDelivery.getStreet() == null) {
				return false;
			}
			if (equipDelivery.getDistrictName() == null) {
				return false;
			}
			if (equipDelivery.getProvinceName() == null) {
				return false;
			}
			
			if (equipDelivery.getId() != null) {
				try {
					List<EquipDeliveryRecDtl> lstEquipDeliveryDetail = equipmentManagerMgr.getListDeliveryDetailByRecordId(equipDelivery.getId());
					if (lstEquipDeliveryDetail != null && lstEquipDeliveryDetail.size() > 0) {
						for (int i = 0, isize = lstEquipDeliveryDetail.size(); i < isize; i++) {
//							if (!lstEquipDeliveryDetail.get(i).getContent().equals(EquipDeliveryContentType.NEW.getValue())
//									&& !lstEquipDeliveryDetail.get(i).getContent().equals(EquipDeliveryContentType.OTHER.getValue())
//									&& !lstEquipDeliveryDetail.get(i).getContent().equals(EquipDeliveryContentType.TRANSFER_FROM_OTHER_STOCK.getValue())) {
//								return false;
//							} 
							if (!isContent(lstEquipDeliveryDetail.get(i).getContent(), lstContent)) {
								return false;
							}
							if (lstEquipDeliveryDetail.get(i).getDepreciation() == null) {
								return false;
							}
							
							if (lstEquipDeliveryDetail.get(i).getEquipment() == null) {
								return false;
							} else if (lstEquipDeliveryDetail.get(i).getEquipment().getSerial() == null){
								return false;
							}
						}
					} else {
						return false;
					}
				} catch (BusinessException e) {
					LogUtility.logError(e, e.getMessage());
				}
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * Luu bien ban
	 * 
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	public String saveStatusRecord() {
		resetToken(result);
		result.put(ERROR, false);
		try {
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			if (equipPeriod == null) {
//				result.put(ERROR, true);
//				result.put("errMsg", "Kỳ hiện tại không MỞ!");
//				return JSON;
//			}
			List<FormErrVO> lstError = new ArrayList<FormErrVO>();
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				for (int i = 0, isize = lstIdRecord.size(); i < isize; i++) {
					EquipDeliveryRecord e = equipmentManagerMgr.getEquipDeliveryRecordById(lstIdRecord.get(i));
					List<ApParamEquip> lstContent = getListContentDelivety();
					FormErrVO formErrVO = new FormErrVO();
					if (!isTrueSaveStatus(e, lstContent) && statusRecord != null && !statusRecord.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
						formErrVO.setRecordNotComplete(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.complete.record"));
					}
					if(StatusRecordsEquip.CANCELLATION.getValue().equals(e.getRecordStatus())&& StatusRecordsEquip.APPROVED.getValue().equals(statusRecord)){						
						formErrVO.setFormStatusErr(1);
					}
					if(StatusRecordsEquip.APPROVED.getValue().equals(e.getRecordStatus()) && StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord)){
						formErrVO.setFormStatusErr(1);
					}
					if(statusRecord == null && StatusRecordsEquip.DRAFT.getValue().equals(e.getRecordStatus()) && statusDelivery != null){
						formErrVO.setFormStatusErr(1);
					}
					if(!StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && DeliveryType.NOTSEND.getValue().equals(e.getDeliveryStatus()) && DeliveryType.RECEIVED.getValue().equals(statusDelivery)){
						formErrVO.setDeliveryStatusErr(1);
					}
					if(!StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && DeliveryType.SENT.getValue().equals(e.getDeliveryStatus()) && DeliveryType.NOTSEND.getValue().equals(statusDelivery)||
						DeliveryType.RECEIVED.getValue().equals(e.getDeliveryStatus()) && DeliveryType.NOTSEND.getValue().equals(statusDelivery)){
						formErrVO.setDeliveryStatusErr(1);						
					}
					if(!StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && DeliveryType.RECEIVED.getValue().equals(e.getDeliveryStatus()) && DeliveryType.SENT.getValue().equals(statusDelivery)){
						formErrVO.setDeliveryStatusErr(1);	
					}
					if (StatusRecordsEquip.CANCELLATION.getValue().equals(e.getRecordStatus()) && (statusRecord != null || statusDelivery != null)) {
						formErrVO.setFormStatusErr(1);
					}
//					if((statusRecord != null && !e.getRecordStatus().equals(statusRecord)) && (!e.getEquipPeriod().getStatus().equals(EquipPeriodType.OPENED)||
//						e.getEquipPeriod().getFromDate().after(DateUtil.now())||
//						e.getEquipPeriod().getToDate().before(DateUtil.now()))){
//						formErrVO.setPeriodError(1);	
//					}
					if(formErrVO.getDeliveryStatusErr() == null && formErrVO.getFormStatusErr() == null && formErrVO.getPeriodError() == null && formErrVO.getRecordNotComplete() == null){						
						// status = 0 thi ko cap nhat
						Integer statusRe = 0;
						
						if (e != null  && statusRecord != null) {
							// du thao
							if (e.getRecordStatus() != null && e.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
								e.setUpdateDate(DateUtil.now());
								e.setUpdateUser(currentUser.getUserName());
								// duyet
								if (statusRecord.equals(StatusRecordsEquip.APPROVED.getValue())) {
									// trang thai tu du thao -> duyet
									statusRe = StatusRecordsEquip.APPROVED.getValue();
									e.setRecordStatus(statusRecord);
								} else if (statusRecord.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
									// trang thai tu du thao -> huy
									statusRe = StatusRecordsEquip.CANCELLATION.getValue();
									e.setRecordStatus(statusRecord);
								}
							}
						}
						if(e != null && statusDelivery != null){
							// neu trang thai bien ban la duyet thi cho cap nhat
							// giao nhan
							if (e.getRecordStatus() != null && e.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
								if (e.getDeliveryStatus() != null && (statusDelivery - e.getDeliveryStatus() - 1 == 0)) {
									e.setDeliveryStatus(statusDelivery);
									if (statusRe == 0) {
										e.setUpdateDate(DateUtil.now());
										e.setUpdateUser(currentUser.getUserName());
										statusRe = -1;
									} else if (statusRe.equals(StatusRecordsEquip.APPROVED.getValue())) {
										statusRe = -2;
									}
								}
							}
						}
						if (statusRe != 0) {
							equipmentManagerMgr.saveEquipRecordDelivery(e, statusRe);
							if (e.getEquipLend() != null) {
								//Tim nhung bien ban cua EquipLend da huy hoac da duyet - > cap nhat thanh HOAN TAT
								EquipLend el = equipmentManagerMgr.getEquipLendById(e.getEquipLend().getEquipLendId());
								if (el != null) {
									el.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
									equipProposalBorrowMgr.updateEquipLend(el);
								}	
							}
						}
					}else{
						formErrVO.setFormCode(e.getCode());
						lstError.add(formErrVO);
					}				
				}
				if(lstError.size()>0){
					result.put("lstRecordError", lstError);
				}
			}
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Kiem tra dong du lieu detail hop le
	 * @param: row - 1 dong du lieu
	 * @author tamvnm
	 * @since 21/06/2015
	 */
	public Boolean isValidDetail (List<String> row ) {
		for (int i = 0, isize = row.size(); i < isize; i++) {
			switch (i) {
			case 5:		// noi dung
				break;
			case 6: 	// nhom TB
				break;
			case 7:		// NCC	
				break;
			case 8:		// Ma TB
				break;
			case 9:		// seri
				break;
			case 10:	// so thang khau hao
				break;
			case 11:	// gia tri TBBH
				break;	
			case 12:	// Kho
				break;
			case 13: 	// Trinh trang TB
				break;
			case 14: 	// Nam sx
				break;
			default:
				if (!StringUtil.isNullOrEmpty(row.get(i))) {
					return false;
				}
				break;
			}
		}
		return true;
	}
	
	/**
	 * Ham validate du lieu dong excel import
	 * 
	 * @author tamvnm
	 * @param row - dong import excel
	 * @return true: dong moi, false: khong phai dong moi
	 * @since Oct 13 2015
	 */
	private Boolean isNewRecord(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) && !StringUtil.isNullOrEmpty(row.get(1)) && !StringUtil.isNullOrEmpty(row.get(2))
				&& !StringUtil.isNullOrEmpty(row.get(3)) && !StringUtil.isNullOrEmpty(row.get(4)) && !StringUtil.isNullOrEmpty(row.get(15))
				&& !StringUtil.isNullOrEmpty(row.get(24)) && !StringUtil.isNullOrEmpty(row.get(26)) && !StringUtil.isNullOrEmpty(row.get(27)) 
				&& !StringUtil.isNullOrEmpty(row.get(28)) && !StringUtil.isNullOrEmpty(row.get(29))) {
			return true;
		}
		return false;
	}
	
	
	
	/**
	 * import bien ban bang excel
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 */
//	public String importRecordDelivery() throws Exception {
//		resetToken(result);
//		isError = true;
//		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
//		totalItem = 0;
//		String message = "";
//		String messageRecord = "";
//		String msgLine = "";
//		Boolean isFirstLineContract = true;
//		Date sysDate = commonMgr.getSysDate();
//		List<CellBean> lstFails = new ArrayList<CellBean>();
//		List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, 32);
//		List<ApParamEquip> lstContent = getListContentDelivety();
//		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
//			//Bỏ dòng trống dư ở cuối biên bản.
//			for (int step = lstData.size() - 1; step > 0; step--) {
//				if (checkLineIsEmpty(lstData.get(step))) {
//					lstData.remove(step);
//				} else {
//					break;
//				}
//			}
//			if (lstData.size() == 0) {
//				if (StringUtil.isNullOrEmpty(errMsg)) {
//					isError = false;
//				}
//				isError = false;
//				return SUCCESS;
//			}
//			try {
////				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
////				if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())||
////					equipPeriod.getFromDate().after(DateUtil.now())||
////					equipPeriod.getToDate().before(DateUtil.now())) {
////					isError = true;
////					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
////					return SUCCESS;
////				}
//				EquipPeriod equipPeriod = null;
//				lstView = new ArrayList<CellBean>();
//				List<EquipDeliveryRecDtl> equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
//				EquipDeliveryRecord equipDeliveryRecord = new EquipDeliveryRecord();
//				EquipDeliveryRecDtl equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
//				Staff staff = null;
//				Customer customer = null;
//				String numberContract = null;
//				Date dateContract = null;
//				Shop shop = null;
//				String idNO = null;
//				String idNOPlace = null;
//				Date idNoDate = null;
//				String idDKKD = null;
//				String idDKKDPlace = null;
//				Date idDKKDDate = null;
//				String householdPlace = null;
//				String toPermanentAddress = "";
//				String soNha = null;
//				String duong = null;
//				String phuong = null;
//				String quan = null;
//				String tp = null;
//				String representative = null;
//				String position = null;
//				BigDecimal freezer = null;
//				BigDecimal refrigerator = null;
//				String note = null;
//				Integer stkh = null;
//				Boolean isTrueRecord = true;
//				Date ngayLap = null;
//				Map<String, Integer> processedEquipments = new HashMap<String, Integer>();
//				List<String> row = new ArrayList<String>();
//				EquipmentDeliveryReturnVO equipmentDeliveryReturnVO = new EquipmentDeliveryReturnVO();
//				//Lay thong tin ben cho muon
//				EquipLender equipLender = commonMgr.getFirtInformationEquipLender(currentUser.getShopRoot().getShopId());
//				for (int i = 0; i < lstData.size(); i++) {
//					totalItem++;
//					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
//						if (checkLineIsEmpty(lstData.get(i))) {
//							if (StringUtil.isNullOrEmpty(message) && equipmentDeliveryRecordDetails.size() > 0 && StringUtil.isNullOrEmpty(messageRecord) && isTrueRecord) {
//								equipDeliveryRecord.setStaff(staff);
//								equipDeliveryRecord.setContractNumber(numberContract);
//								equipDeliveryRecord.setCreateDate(DateUtil.now());
//								equipDeliveryRecord.setCreateUser(currentUser.getUserName());
//								equipDeliveryRecord.setCustomer(customer);
//								equipDeliveryRecord.setToAddress(customer.getAddress());
//								if (customer.getPhone() != null && customer.getMobiphone() != null) {
//									equipDeliveryRecord.setToPhone(customer.getPhone() + "/" + customer.getMobiphone());
//								} else if (customer.getPhone() != null && customer.getMobiphone() == null) {
//									equipDeliveryRecord.setToPhone(customer.getPhone());
//								} else if (customer.getPhone() == null && customer.getMobiphone() != null) {
//									equipDeliveryRecord.setToPhone(customer.getMobiphone());
//								}
//								// trang thai giao nhan: mac dinh la chua gui
//								equipDeliveryRecord.setDeliveryStatus(DeliveryType.NOTSEND.getValue());
//								equipDeliveryRecord.setEquipPeriod(equipPeriod);
//								equipDeliveryRecord.setCreateFormDate(ngayLap);
//								// Trang thai bien ban: mac dinh la du thao
//								equipDeliveryRecord.setRecordStatus(StatusRecordsEquip.DRAFT.getValue());
//								equipDeliveryRecord.setContractCreateDate(dateContract);
//								equipDeliveryRecord.setToIdNO(idNO);
//								equipDeliveryRecord.setToIdNODate(idNoDate);
//								equipDeliveryRecord.setToIdNOPlace(idNOPlace);
//								equipDeliveryRecord.setToBusinessLincense(idDKKD);
//								equipDeliveryRecord.setToBusinessPlace(idDKKDPlace);
//								equipDeliveryRecord.setToBusinessDate(idDKKDDate);
//								equipDeliveryRecord.setToObjectName(shop.getShopName());
//								//dia chi dat tu
//								equipDeliveryRecord.setAddress(soNha);
//								equipDeliveryRecord.setStreet(duong);
//								equipDeliveryRecord.setWardName(phuong);
//								equipDeliveryRecord.setProvinceName(tp);
//								equipDeliveryRecord.setDistrictName(quan);
//								
//								String objectAddress = "";
//								if (soNha != null ) {
//									objectAddress += soNha + " ";
//								}
//								if (duong != null ) {
//									objectAddress += duong + ", ";
//								}
//								if (phuong != null ) {
//									objectAddress += phuong + ", ";
//								}
//								if (quan != null ) {
//									objectAddress += quan + ", ";
//								}
//								if (tp != null ) {
//									objectAddress += tp;
//								}
//								
//								if (objectAddress.length() > 0) {
//									equipDeliveryRecord.setToObjectAddress(objectAddress);
//								}
//								equipDeliveryRecord.setToRepresentative(representative);
//								equipDeliveryRecord.setToPosition(position);
//								equipDeliveryRecord.setToShop(shop);
//								equipDeliveryRecord.setFreezer(freezer);
//								equipDeliveryRecord.setRefrigerator(refrigerator);
//								equipDeliveryRecord.setNote(note);
//								if (equipLender != null) {
//									/**
//									 * Lay thong tin Ben cho muon
//									 * 
//									 * @author hunglm16
//									 * @since August 18, 2015
//									 * */
//									equipDeliveryRecord.setFromObjectName(equipLender.getName());
//									equipDeliveryRecord.setFromObjectAddress(equipLender.getAddress());
//									equipDeliveryRecord.setFromObjectTax(equipLender.getTaxNumber());
//									equipDeliveryRecord.setFromObjectPhone(equipLender.getPhone());
//									equipDeliveryRecord.setFromRepresentative(equipLender.getRepressentative());
//									equipDeliveryRecord.setFromPosition(equipLender.getPosition());
//									equipDeliveryRecord.setFromPage(equipLender.getBusinessLicense());
//									equipDeliveryRecord.setFromPagePlace(equipLender.getBusinessPlace());
//									equipDeliveryRecord.setFromPageDate(equipLender.getBusinessDate());
//									equipDeliveryRecord.setFromFax(equipLender.getFax());
//								}
//								equipmentDeliveryReturnVO = equipmentManagerMgr.createRecordDeliveryByImportExcel(equipDeliveryRecord, equipmentDeliveryRecordDetails, null);
//								equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
//								if (equipmentDeliveryReturnVO != null) {
//									List<Equipment> lstEquipment = equipmentDeliveryReturnVO.getLstEquipment();
//									String m = "";
//									if (lstEquipment != null) {
//										for (int j = 0, size = lstEquipment.size(); j < size; j++) {
//											for (int count = 0; count <= i; count++) {
//												List<String> rowFail = lstData.get(count);
//												if (lstEquipment.get(j).getCode().equals(rowFail.get(4))) {
//													m += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code")//Thiet bi co ma
//															+ lstEquipment.get(j).getCode() // Ma thiet bi
//															+ Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code1") + "\n";//dang giao dich hoac khong o kho
//													lstFails.add(StringUtil.addFailBean(rowFail, m));
//												}
//											}
//										}
//										
//									}
//								}
//							} else {
//								lstFails.add(StringUtil.addFailBean(lstData.get(i), ""));
//							}
//							if (equipmentDeliveryRecordDetails.size() > 0 && isTrueRecord) {
//								equipDeliveryRecord.setStaff(staff);
//								equipDeliveryRecord.setContractNumber(numberContract);
//								equipDeliveryRecord.setCreateDate(DateUtil.now());
//								equipDeliveryRecord.setCreateUser(currentUser.getUserName());
//								equipDeliveryRecord.setCustomer(customer);
//								equipDeliveryRecord.setToAddress(customer.getAddress());
//								if (customer.getPhone() != null && customer.getMobiphone() != null) {
//									equipDeliveryRecord.setToPhone(customer.getPhone() + "/" + customer.getMobiphone());
//								} else if (customer.getPhone() != null && customer.getMobiphone() == null) {
//									equipDeliveryRecord.setToPhone(customer.getPhone());
//								} else if (customer.getPhone() == null && customer.getMobiphone() != null) {
//									equipDeliveryRecord.setToPhone(customer.getMobiphone());
//								}
//								// trang thai giao nhan: mac dinh la chua gui
//								equipDeliveryRecord.setDeliveryStatus(DeliveryType.NOTSEND.getValue());
//								equipDeliveryRecord.setEquipPeriod(equipPeriod);
//								equipDeliveryRecord.setCreateFormDate(ngayLap);
//								// Trang thai bien ban: mac dinh la du thao
//								equipDeliveryRecord.setRecordStatus(StatusRecordsEquip.DRAFT.getValue());
//								equipDeliveryRecord.setContractCreateDate(dateContract);
//								equipDeliveryRecord.setToIdNO(idNO);
//								equipDeliveryRecord.setToIdNODate(idNoDate);
//								equipDeliveryRecord.setToIdNOPlace(idNOPlace);
//								equipDeliveryRecord.setToBusinessLincense(idDKKD);
//								equipDeliveryRecord.setToBusinessPlace(idDKKDPlace);
//								equipDeliveryRecord.setToBusinessDate(idDKKDDate);
//								equipDeliveryRecord.setToObjectName(shop.getShopName());
////								equipDeliveryRecord.setToObjectAddress(householdPlace);
//								//dia chi dat tu
//								equipDeliveryRecord.setAddress(soNha);
//								equipDeliveryRecord.setStreet(duong);
//								equipDeliveryRecord.setWardName(phuong);
//								equipDeliveryRecord.setProvinceName(tp);
//								equipDeliveryRecord.setDistrictName(quan);
//								
//								String objectAddress = "";
//								if (soNha != null ) {
//									objectAddress += soNha + " ";
//								}
//								if (duong != null ) {
//									objectAddress += duong + ", ";
//								}
//								if (phuong != null ) {
//									objectAddress += phuong + ", ";
//								}
//								if (quan != null ) {
//									objectAddress += quan + ", ";
//								}
//								if (tp != null ) {
//									objectAddress += tp;
//								}
//								
//								if (objectAddress.length() > 0) {
//									equipDeliveryRecord.setToObjectAddress(objectAddress);
//								}
//								equipDeliveryRecord.setToRepresentative(representative);
//								equipDeliveryRecord.setToPosition(position);
//								equipDeliveryRecord.setToShop(shop);
//								equipDeliveryRecord.setFreezer(freezer);
//								equipDeliveryRecord.setRefrigerator(refrigerator);
//								equipDeliveryRecord.setNote(note);
//								if (equipLender != null) {
//									/**
//									 * Lay thong tin Ben cho muon
//									 * 
//									 * @author hunglm16
//									 * @since August 18, 2015
//									 * */
//									equipDeliveryRecord.setFromObjectName(equipLender.getName());
//									equipDeliveryRecord.setFromObjectAddress(equipLender.getAddress());
//									equipDeliveryRecord.setFromObjectTax(equipLender.getTaxNumber());
//									equipDeliveryRecord.setFromObjectPhone(equipLender.getPhone());
//									equipDeliveryRecord.setFromRepresentative(equipLender.getRepressentative());
//									equipDeliveryRecord.setFromPosition(equipLender.getPosition());
//									equipDeliveryRecord.setFromPage(equipLender.getBusinessLicense());
//									equipDeliveryRecord.setFromPagePlace(equipLender.getBusinessPlace());
//									equipDeliveryRecord.setFromPageDate(equipLender.getBusinessDate());
//									equipDeliveryRecord.setFromFax(equipLender.getFax());
//								}
//								equipmentDeliveryReturnVO = equipmentManagerMgr.createRecordDeliveryByImportExcel(equipDeliveryRecord, equipmentDeliveryRecordDetails, null);
//								if (equipmentDeliveryReturnVO != null) {
//									List<Equipment> lstEquipment = equipmentDeliveryReturnVO.getLstEquipment();
//									String m = "";
//									if (lstEquipment != null) {
//										for (int j = 0, size = lstEquipment.size(); j < size; j++) {
//											for (int count = 0; count <= i; count++) {
//												List<String> rowFail = lstData.get(count);
//												if (lstEquipment.get(j).getCode().equals(rowFail.get(4))) {
//													m += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code")//Thiet bi co ma
//															+ lstEquipment.get(j).getCode() // Ma thiet bi
//															+ Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code1") + "\n";//dang giao dich hoac khong o kho
//													lstFails.add(StringUtil.addFailBean(rowFail, m));
//												}
//											}
//										}
//										
//									}
//								}
//							}
//							equipPeriod = null;
//							isTrueRecord = true;
//							staff = null;
//							customer = null;
//							numberContract = null;
//							dateContract = null;
//							statusRecord = null;
//							statusDelivery = null;
//							shop = null;
//							idNO = null;
//							idNOPlace = null;
//							idNoDate = null;
//							idDKKD = null;
//							idDKKDPlace = null;
//							idDKKDDate = null;
//							householdPlace = null;
//							toPermanentAddress = "";
//							soNha = null;
//							duong = null;
//							phuong = null;
//							quan = null;
//							tp = null;
//							representative = null;
//							position = null;
//							freezer = null;
//							refrigerator = null;
//							note = null;
//							stkh = null;
//							message = "";
//							processedEquipments = new HashMap<String, Integer>();
//							isFirstLineContract = true;
//							equipDeliveryRecord = new EquipDeliveryRecord();
//							equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
//							equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
//							ngayLap = null;
//							continue;
//						}
//						row = lstData.get(i);
//						// ** Check trung du lieu cac record */
//						message = "";
//						if (!StringUtil.isNullOrEmpty(message)) {
//							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
//							continue;
//						}
//						String msgEquip = "";
//						if (isFirstLineContract) {
//							// ** So hop dong */
//							if (row.size() > 0) {
//								String value = row.get(0);
//								String msg = ValidateUtil.validateField(value, "equipment.manager.number.contract", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} 
////								else if (value.indexOf(" ") != -1) {
////									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.format.number.contract.space");
////								} 
//								else{
//									numberContract = value;
//								}
//							}
//							// ** Ma NPP */
//							if (row.size() > 1) {
//								String value = row.get(1);
//								String msg = ValidateUtil.validateField(value, "ss.traningplan.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									value = value.trim().toUpperCase();
//									shop = shopMgr.getShopByCode(value);
//									if(shop == null) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"ss.traningplan.npp.code");
//									}else if (!checkShopInOrgAccessByCode(value)) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
//										shop = null;
//									}
//									else if(!shop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
//										shop = null;
//									}
//							
//									if (shop != null) {
//										if(shop.getType().getObjectType().equals(ShopObjectType.NPP.getValue()) 
//												|| shop.getType().getObjectType().equals(ShopObjectType.MIEN_ST.getValue())
//											|| shop.getType().getObjectType().equals(ShopObjectType.NPP_KA.getValue())) {
//											//dieu kien dung
//										} else {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
//										}
//									}
//								}
//							}
//							// ** Ma GS */
//							if (row.size() > 2) {
//								String value = row.get(2);
//								String msg = ValidateUtil.validateField(value, "equipment.manager.staff.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									value = value.trim().toUpperCase();
//									staff = staffMgr.getStaffByCode(value);
//									if (staff == null || staff.getShop() == null) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.staff.code");
//									} else if (!ActiveType.RUNNING.equals(staff.getStatus())) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.staff.code");
//									} else if (shop!=null && !checkNVGS(value, shop.getId())) {
//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.mgs.role.invalid") + "\n";
//									}
//									if (staff != null) {
//										if (StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType())
//												|| StaffObjectType.GSKA.getValue().equals(staff.getStaffType().getObjectType())
//												|| StaffObjectType.GSMT.getValue().equals(staff.getStaffType().getObjectType())) {
//											//dieu kien dung
//										} else {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.mgs") + "\n";
//										}
//									}
//								}
//							}
//							// ** Ngay lap */
//							if (row.size() > 3) {
//								String value = row.get(3);
//								String msg = ValidateUtil.validateField(value, "equipment.create.form.date", 50, ConstantManager.ERR_REQUIRE);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								}else{
//									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date",null);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										ngayLap = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
//										if (ngayLap == null) {
//											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
//										} else {
//											if (DateUtil.compareTwoDate(ngayLap, sysDate) == 1) {
//												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.big") + "\n";
//											} else {
//												//tamvnm: update dac ta moi. 30/06/2015
//												List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(ngayLap);
//												if (lstPeriod == null || lstPeriod.size() == 0) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//												} else if (lstPeriod.size() > 0) {
//													// gan gia tri ky dau tien.
//													equipPeriod = lstPeriod.get(0);
//												}
//											}
//										}
//									}
//								}							
//							}
//							// ** Ngay hop dong */
//							if (row.size() > 4) {
//								String value = row.get(4);
//								String msg = ValidateUtil.validateField(value, "equipment.manager.date.contract", 50, ConstantManager.ERR_REQUIRE);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								}else{
//									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.manager.date.contract",null);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										dateContract = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
//										if (dateContract == null) {
//											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.manager.date.contract", null);
//										}
//									}
//								}							
//							}
//							// ** Ma KH */
//							if(shop != null){
//								if (row.size() > 14) {
//									String value = row.get(14);
//									String msg = ValidateUtil.validateField(value, "equipment.manager.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										value = value.trim().toUpperCase();
//										List<Customer> lstCus = customerMgr.getListCustomerByShortCode(shop.getId(), value);
//										if (lstCus != null && lstCus.size() > 0) {
//											customer = lstCus.get(0);
//										}
//										
//										if (customer == null) {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "pay.period.code.checked.customer.shop") + "\n";
//										} else if (!shop.getId().equals(customer.getShop().getId())) {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.manpp") + "\n";
//										} 
//									}
//								}
//							}
//							/*
//							 * validate thong tin thiet bi dong dau tien
//							 */
//							Boolean isHave = false;
//							Equipment equipment = new Equipment();
//							equipment.setUsageStatus(null);
//							String contentType = null;
//							if (row.size() > 5) {
//								String rawEquipmentDeliveryContentType = row.get(5);
//								if (isHave != null && !isHave) {
//									String msg = ValidateUtil.validateField(rawEquipmentDeliveryContentType, "equipment.manager.equipment.content", 100, ConstantManager.ERR_REQUIRE);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										msgEquip += msg;
//									} else if (lstContent != null && lstContent.size() > 0) {
//										for (int c = 0, csize = lstContent.size(); c < csize; c++) {
//											if (rawEquipmentDeliveryContentType.equals(lstContent.get(c).getValue())) {
//												contentType = lstContent.get(c).getApParamEquipCode();
//											}
//										}
//										if (contentType == null) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + " không hợp lệ " + "\n";
//										}
//									} else {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + " không hợp lệ " + "\n";
//									}
//								}
//							}
//							/*
//							 * validate for equipment code
//							 */
//							if (row.size() > 8) {
//								String rawEquipmentCode = row.get(8);
//								
//								String equipmentCode = rawEquipmentCode.trim();
//								/*
//								 * kiem tra trung
//								 */
//								if (processedEquipments.containsKey(equipmentCode)) {
//									// loi trung
//									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code");
//									msgEquip += " trùng với " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code") + " dòng " + processedEquipments.get(equipmentCode) + "\n";
//									isHave = true;
//								} else {
//									if (!StringUtil.isNullOrEmpty(equipmentCode)) {
//										processedEquipments.put(equipmentCode, i + 1);
//									}
//									// validate tiep
//									String msg = ValidateUtil.validateField(rawEquipmentCode, "equipment.manager.equipment.code", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//									if (StringUtil.isNullOrEmpty(msg)) {
////											equip.lost.recorde.err.ma.thiet.bi.must.require
//										if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipmentCode)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.ma.thiet.bi.ko.require") + "\n";
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipmentCode)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.ma.thiet.bi.must.require") + "\n";
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipmentCode)) {
//											//tao moi thiet bi
//											equipment = new Equipment();
//											//Trang thai su dung
//											equipment.setStatus(StatusType.ACTIVE);
//											equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
//											equipment.setTradeType(EquipTradeType.DELIVERY);
//											equipment.setUsageStatus(null);
//											equipment.setCreateDate(sysDate);
//											equipment.setCreateUser(currentUser.getUserName());
//										} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipmentCode)) {
//											//tim kiem thiet bi
//											equipment = validateEquipment(rawEquipmentCode);
//											if (equipment == null) {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.not.exits") + "\n";
//											} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
//												msgEquip += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
//											} else if (EquipTradeStatus.TRADING.getValue().equals(equipment.getTradeStatus())) {
//												msgEquip += "Mã TB" + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.trade.status") + "\n";
//											} else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
//												msgEquip += "Mã TB" + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock") + "\n";
//											} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
//												msgEquip += "Mã TB" + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.usage.status") + "\n";
//											}
//											isHave = false;
//										}
//									} else {
//										msgEquip += msg;
//										isHave = null;
//									}
//								}
//							}
//							/*
//							 * validate for equipment serial number
//							 */							
//							if (row.size() > 9) {
//								String rawEquipmentSerialNumber = row.get(9);
//								if (isHave != null && !isHave) {
//									String msg = ValidateUtil.validateField(rawEquipmentSerialNumber, "equipment.manager.equipment.seri", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SERIAL, ConstantManager.ERR_MAX_LENGTH);
//									if(!StringUtil.isNullOrEmpty(msg)){
//										msgEquip += msg;
//									}else if(StringUtil.isNullOrEmpty(msg) && rawEquipmentSerialNumber.indexOf(" ") != -1){
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial") + "\n";
//									} else if (equipment != null && equipment.getSerial() != null && !StringUtil.isNullOrEmpty(rawEquipmentSerialNumber)) {
//										msgEquip += "TB " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.serial") + "\n";
//									} else if (equipment != null && equipment.getSerial() == null && !StringUtil.isNullOrEmpty(rawEquipmentSerialNumber)) {
//										equipment.setSerial(rawEquipmentSerialNumber);
//									}
//								}
//							}
//							//Nhom thiet bi
//							if (row.size() > 6) {
//								String equipGroup = row.get(6);
//								if (isHave != null && !isHave) {
//									String msg = ValidateUtil.validateField(equipGroup, "equipment.group.product.equipment.group.code", 100, ConstantManager.ERR_MAX_LENGTH);
//									EquipGroup group = null;
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										msgEquip += msg;
//									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipGroup)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.group") + "\n";
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipGroup)) {
//										group  = equipmentManagerMgr.getEquipGroupByCode(equipGroup);
//										if (group != null) {
//											if (!ActiveType.RUNNING.getValue().equals(group.getStatus().getValue())) {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon") + "\n";
//											} else {
//												equipment.setEquipGroup(group);
//											}
//										} else {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined")+ "\n";
//										}
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipGroup)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.group.null")+ "\n";
//									}
//								}
//							}
//							//Nha cung cap
//							if (row.size() > 7) {
//								String provider = row.get(7);
//								if (isHave != null && !isHave) {
//									String msg = ValidateUtil.validateField(provider, "equip.import.equipprovider", 100, ConstantManager.ERR_MAX_LENGTH);
//									EquipProvider equipProvider = null;
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										msgEquip += msg;
//									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(provider)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.provider") + "\n";
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(provider)) {
//										equipProvider = equipmentManagerMgr.getEquipProviderByCode(provider);
//										if (equipProvider != null) {
//											if (!ActiveType.RUNNING.getValue().equals(equipProvider.getStatus().getValue())) {
//												msgEquip = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.unon")+ "\n";
//											} else {
//												equipment.setEquipProvider(equipProvider);
//											}
//										} else {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.undefined")+ "\n";
//										}
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(provider)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.equipprovider.null")+ "\n";
//									}
//								}
//							}
//							//Gia tri TBBH
//							if (row.size() > 11) {
//								String equipPrice = row.get(11);
//								if (isHave != null && !isHave) {
//									String msg = ValidateUtil.validateField(equipPrice, "equipment.delivery.tbbh", 17 , ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										msgEquip += msg;
//									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipPrice)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.price") + "\n";
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipPrice)) {
//										
//										BigDecimal price = BigDecimal.valueOf(Double.parseDouble(equipPrice));
//										if (price.doubleValue() < 1) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.price")+ "\n";
//										} else {
//											equipment.setPrice(price);
//										}
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipPrice)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.tbbh.null")+ "\n";
//									}
//								}
//							}
//							//trinh trang thiet bi
//							if (row.size() > 12) {
//								String equipHealth = row.get(12);
//								if (isHave != null && !isHave) {
//									String msg = ValidateUtil.validateField(equipHealth, "equipment.delivery.health", 100 , ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										msgEquip += msg;
//									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipHealth)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.heal") + "\n";
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipHealth)) {
//										String healthStatus = equipHealth;
//										List<ApParam> lstAppram = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
//										boolean flagHs = false;
//										for (ApParam ap : lstAppram) {
//											if (healthStatus.equals(ap.getApParamCode())) {
//												flagHs = true;
//												break;
//											}
//										}
//										if (!flagHs) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde")+ "\n";
//										} else {
//											equipment.setHealthStatus(healthStatus);
//										}
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipHealth)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.health.null")+ "\n";
//									}
//								}
//							}
//							//Nam san xuat
//							if (row.size() > 13) {
//								String equipYear = row.get(13);
//								if (isHave != null && !isHave) {
//									String msg = ValidateUtil.validateField(equipYear, "equipment.delivery.year", 4 ,  ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										msgEquip += msg;
//									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipYear)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.manufacturingYear") + "\n";
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipYear)) {
//										Integer year = Integer.parseInt(equipYear);
//										
//										Integer curYear = DateUtil.getYear(sysDate);
//										if (year != null && year < 0) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.error1") + "\n";
//										} else if (year != null && year.intValue() > curYear.intValue()) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.error2") + "\n";
//										} else {
//											equipment.setManufacturingYear(year);
//										}
//									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipYear)) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.null")+ "\n";
//									}
//								}
//							}
//							// So thang khau hao
//							if (row.size() > 10) {
//								String value = row.get(10);
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.stkh", 5, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									msgEquip += msg;
//								} else {
//									stkh = Integer.decode(value);
//									if (stkh.intValue() <= 0) {
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.stkh.condition") + "\n";
//									}
//								}
//							}
//
//							/*
//							 * all are OK, create equipment delivery record dong dau tien
//							 * detail
//							 */
//							if (StringUtil.isNullOrEmpty(msgEquip) && equipment != null) {
//								
//								equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
//								// set data
//								equipmentDeliveryRecDetail.setContent(contentType);
//								equipmentDeliveryRecDetail.setCreateDate(DateUtil.now());
//								equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
//								equipmentDeliveryRecDetail.setEquipment(equipment);
//								equipmentDeliveryRecDetail.setFromStockId(equipment.getStockId());
//								equipmentDeliveryRecDetail.setDepreciation(stkh);
//								equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
//							} else {
//								isTrueRecord = false;
//							}
//							// **Dia chi thuong tru */
//							if (row.size() > 15) {
//								String value = row.get(15);
//								if (!StringUtil.isNullOrEmpty(value)) {
//									String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.dc.thuong.tru", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										toPermanentAddress = value.trim();
//									}
//								}
//							}
//							// ** CMND */
//							if (row.size() > 16) {
//								String value = row.get(16);
//								if (!StringUtil.isNullOrEmpty(value)) {
//									String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.cmnd", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										idNO = value;
//									}
//								}
//							}
//							
//							// ** Noi cap CMND */
//							if (row.size() > 17) {
//								String value = row.get(17);
//								if (!StringUtil.isNullOrEmpty(value)) {
//									String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.noicap", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										idNOPlace = value;
//									}
//								}
//							}
//							
//							// ** Ngay cap CMND */
//							if (row.size() > 18) {
//								String value = row.get(18);
//								if (!StringUtil.isNullOrEmpty(value)) {
//									String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ngaycap", 20, ConstantManager.ERR_REQUIRE);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									}else{
//										msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.proposal.export.sheet1.ngaycap",null);
//										if (!StringUtil.isNullOrEmpty(msg)) {
//											message += msg;
//										} else {
//											idNoDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
//											if (idNoDate == null) {
//												message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.proposal.export.sheet1.ngaycap", null);
//											} else {
//												Date now = new Date();
//												if (DateUtil.compareTwoDate(idNoDate, now) == 1) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idNO.dateCreate") + "\n";
//												}
//											}
//										}
//									}
//								}
//							}
//							
//							// ** So GP DKKD */
//							if (row.size() > 19) {
//								String value = row.get(19);
//								if (!StringUtil.isNullOrEmpty(value)) {
//									String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.sodkkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										idDKKD = value;
//									}
//								}
//							}
//							
//							// ** Noi cap GP DKKD */
//							if (row.size() > 20) {
//								String value = row.get(20);
//								if (!StringUtil.isNullOrEmpty(value)) {
//									String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.noicapdkkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										idDKKDPlace = value;
//									}
//								}
//							}
//							
//							// ** Ngay cap GP DKKD */
//							if (row.size() > 21) {
//								String value = row.get(21);
//								if (!StringUtil.isNullOrEmpty(value)) {
//									String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ngaycapdkd", 50, ConstantManager.ERR_REQUIRE);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									}else{
//										msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.proposal.export.sheet1.ngaycapdkd",null);
//										if (!StringUtil.isNullOrEmpty(msg)) {
//											message += msg;
//										} else {
//											idDKKDDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
//											if (idDKKDDate == null) {
//												message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.proposal.export.sheet1.ngaycapdkd", null);
//											} else {
//												Date now = new Date();
//												if (DateUtil.compareTwoDate(idDKKDDate, now) == 1) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idDKKD.dateCreate") + "\n";
//												}
//											}
//										}
//									}	
//								}
//							}
//							// so nha
//							if (row.size() > 22) {
//								String value = row.get(22);
//								//tamvnm: update yeu cau: khong check so nha
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.so.nha", 250, ConstantManager.ERR_MAX_LENGTH);
////								String msg = ValidateUtil.validateField(value, "equipment.delivery.so.nha", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									soNha = value;
//								}
//							}
//							
//							//Ten duong pho/ thon ap
//							if (row.size() > 23) {
//								String value = row.get(23);
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.duong", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									duong = value;
//								}
//							}
//							
//							//phuong/xa
//							if (row.size() > 24) {
//								String value = row.get(24);
//								//tamvnm: update yeu cau: khong check phuong/xa
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.phuong", 250, ConstantManager.ERR_MAX_LENGTH);
////								String msg = ValidateUtil.validateField(value, "equipment.delivery.phuong", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									phuong = value;
//								}
//							}
//							//quan/huyen
//							if (row.size() > 25) {
//								String value = row.get(25);
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.quan", 250, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									quan = value;
//								}
//							}
//							//tinh/tp
//							if (row.size() > 26) {
//								String value = row.get(26);
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.tp", 250, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									tp = value;
//								}
//							}
//							// ** Nguoi dai dien */
//							if (row.size() > 27) {
//								String value = row.get(27);
//								String msg = ValidateUtil.validateField(value, "equipment.manager.customer.daidien", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									representative = value;
//								}
//							}
//							
//							// ** Chuc vu */
//							if (row.size() > 28) {
//								String value = row.get(28);
//								String msg = ValidateUtil.validateField(value, "common.customer.possContact.name", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									position = value;
//								}
//							}
//							// ** DS tu mat */
//							if (row.size() > 29) {
//								String value = row.get(29);
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.freezer", 17, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else if (!StringUtil.isNullOrEmpty(value)) {
//									freezer = BigDecimal.valueOf(Double.valueOf(value));
//									if (freezer.floatValue() <= 0) {
//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.freezer.condition") + "\n";
//									}
//								}
//							}
//							// ** DS tu dong */
//							if (row.size() > 30) {
//								String value = row.get(30);
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.refrigerator", 17, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else if (!StringUtil.isNullOrEmpty(value)) {
//									refrigerator = BigDecimal.valueOf(Double.valueOf(value));
//									if (refrigerator.floatValue() <= 0) {
//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.refrigerator.condition") + "\n";
//									}
//								}
//							}
//							// ** Ghi chu */
//							if (row.size() > 31) {
//								String value = row.get(31);
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									note = value;
//								}
//							}
//							messageRecord = message;
//						} else if (isTrueRecord) {
//							/*
//							 * validate thong tin thiet bi dong detail
//							 */
//							 Boolean isValidDetailRow = isValidDetail(row);
//							if (!isValidDetailRow) {
//								msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.invalid.detail") + "\n";
//							} else {
//								Boolean isHave = false;
//								Equipment equipment = new Equipment();
//								equipment.setUsageStatus(null);
//								
//								// Noi dung
//								String contentType = null;
//								if (row.size() > 5) {
//									String rawEquipmentDeliveryContentType = row.get(5);
//									if (isHave != null && !isHave) {
//										String msg = ValidateUtil.validateField(rawEquipmentDeliveryContentType, "equipment.manager.equipment.content", 100, ConstantManager.ERR_REQUIRE);
//										if (!StringUtil.isNullOrEmpty(msg)) {
//											msgEquip += msg;
//										} else if (lstContent != null && lstContent.size() > 0) {
//											for (int c = 0, csize = lstContent.size(); c < csize; c++) {
//												if (rawEquipmentDeliveryContentType.equals(lstContent.get(c).getValue())) {
//													contentType = lstContent.get(c).getApParamEquipCode();
//												}
//											}
//											if (contentType == null) {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + " không hợp lệ " + "\n";
//											}
//										} else {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + " không hợp lệ " + "\n";
//										}
//									}
//								}
//								/*
//								 * validate for equipment code
//								 */
//								if (row.size() > 8) {
//									String rawEquipmentCode = row.get(8);
//									String equipmentCode = rawEquipmentCode.trim();
//									/*
//									 * kiem tra trung
//									 */
//									if (processedEquipments.containsKey(equipmentCode)) {
//										// loi trung
//										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code");
//										msgEquip += " trùng với " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code") + " dòng " + processedEquipments.get(equipmentCode) + "\n";
//										isHave = true;
//									} else {
//										if (!StringUtil.isNullOrEmpty(equipmentCode)) {
//											processedEquipments.put(equipmentCode, i + 1);
//										}
//										// validate tiep
//										String msg = ValidateUtil.validateField(rawEquipmentCode, "equipment.manager.equipment.code", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//										if (StringUtil.isNullOrEmpty(msg)) {
////											equip.lost.recorde.err.ma.thiet.bi.must.require
//											if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipmentCode)) {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.ma.thiet.bi.ko.require") + "\n";
//											} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipmentCode)) {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.ma.thiet.bi.must.require") + "\n";
//											} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipmentCode)) {
//												//tao moi thiet bi
//												equipment = new Equipment();
//												//Trang thai su dung
//												equipment.setStatus(StatusType.ACTIVE);
//												equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
//												equipment.setTradeType(EquipTradeType.DELIVERY);
//												equipment.setUsageStatus(null);
//												equipment.setCreateDate(sysDate);
//												equipment.setCreateUser(currentUser.getUserName());
//											} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipmentCode)) {
//												//tim kiem thiet bi
//												equipment = validateEquipment(rawEquipmentCode);
//												if (equipment == null) {
//													msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.not.exits") + "\n";
//												} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
//													msgEquip += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
//												} else if (EquipTradeStatus.TRADING.getValue().equals(equipment.getTradeStatus())) {
//													msgEquip += "Mã TB" + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.trade.status") + "\n";
//												} else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
//													msgEquip += "Mã TB" + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock") + "\n";
//												} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
//													msgEquip += "Mã TB" + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.usage.status") + "\n";
//												}
//												isHave = false;
//											}
//										} else {
//											msgEquip += msg;
//											isHave = null;
//										}
//									}
//								}
//								/*
//								 * validate for equipment serial number
//								 */							
//								if (row.size() > 9) {
//									String rawEquipmentSerialNumber = row.get(9);
//									if (!StringUtil.isNullOrEmpty(rawEquipmentSerialNumber)) {
//										if (isHave != null && !isHave) {
//											String msg = ValidateUtil.validateField(rawEquipmentSerialNumber, "equipment.manager.equipment.seri", 50, ConstantManager.ERR_MAX_LENGTH);
//											if(!StringUtil.isNullOrEmpty(msg)){
//												msgEquip += msg;
//											}else if(StringUtil.isNullOrEmpty(msg) && rawEquipmentSerialNumber.indexOf(" ") != -1){
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial") + "\n";
//											} else if (equipment != null && equipment.getSerial() != null && !StringUtil.isNullOrEmpty(rawEquipmentSerialNumber)) {
//												msgEquip += "TB " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.serial") + "\n";
//											} else if (equipment != null && equipment.getSerial() == null && !StringUtil.isNullOrEmpty(rawEquipmentSerialNumber)) {
//												equipment.setSerial(rawEquipmentSerialNumber);
//											}
//										}
//									}
//								}	
//								//Nhom thiet bi
//								if (row.size() > 6) {
//									String equipGroup = row.get(6);
//									if (isHave != null && !isHave) {
//										String msg = ValidateUtil.validateField(equipGroup, "equipment.group.product.equipment.group.code", 100, ConstantManager.ERR_MAX_LENGTH);
//										EquipGroup group = null;
//										if (!StringUtil.isNullOrEmpty(msg)) {
//											msgEquip += msg;
//										} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipGroup)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.group") + "\n";
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipGroup)) {
//											group  = equipmentManagerMgr.getEquipGroupByCode(equipGroup);
//											if (group != null) {
//												if (!ActiveType.RUNNING.getValue().equals(group.getStatus().getValue())) {
//													msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon") + "\n";
//												} else {
//													equipment.setEquipGroup(group);
//												}
//											} else {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined")+ "\n";
//											}
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipGroup)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.group.null")+ "\n";
//										}
//									}
//								}
//								
//								//Nha cung cap
//								if (row.size() > 7) {
//									String provider = row.get(7);
//									if (isHave != null && !isHave) {
//										String msg = ValidateUtil.validateField(provider, "equip.import.equipprovider", 100, ConstantManager.ERR_MAX_LENGTH);
//										EquipProvider equipProvider = null;
//										if (!StringUtil.isNullOrEmpty(msg)) {
//											msgEquip += msg;
//										} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(provider)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.provider") + "\n";
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(provider)) {
//											equipProvider = equipmentManagerMgr.getEquipProviderByCode(provider);
//											if (equipProvider != null) {
//												if (!ActiveType.RUNNING.getValue().equals(equipProvider.getStatus().getValue())) {
//													msgEquip = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.unon")+ "\n";
//												} else {
//													equipment.setEquipProvider(equipProvider);
//												}
//											} else {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.undefined")+ "\n";
//											}
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(provider)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.equipprovider.null")+ "\n";
//										}
//									}
//								}
//								
//								//Gia tri TBBH
//								if (row.size() > 11) {
//									String equipPrice = row.get(11);
//									if (isHave != null && !isHave) {
//										String msg = ValidateUtil.validateField(equipPrice, "equipment.delivery.tbbh", 17 , ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
//										if (!StringUtil.isNullOrEmpty(msg)) {
//											msgEquip += msg;
//										} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipPrice)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.price") + "\n";
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipPrice)) {
//											
//											BigDecimal price = BigDecimal.valueOf(Double.parseDouble(equipPrice));
//											if (price.doubleValue() < 1) {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.price")+ "\n";
//											} else {
//												equipment.setPrice(price);
//											}
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipPrice)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.tbbh.null")+ "\n";
//										}
//									}
//								}
//								//trinh trang thiet bi
//								if (row.size() > 12) {
//									String equipHealth = row.get(12);
//									if (isHave != null && !isHave) {
//										String msg = ValidateUtil.validateField(equipHealth, "equipment.delivery.health", 100 , ConstantManager.ERR_MAX_LENGTH);
//										if (!StringUtil.isNullOrEmpty(msg)) {
//											msgEquip += msg;
//										} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipHealth)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.heal") + "\n";
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipHealth)) {
//											String healthStatus = equipHealth;
//											List<ApParam> lstAppram = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
//											boolean flagHs = false;
//											for (ApParam ap : lstAppram) {
//												if (healthStatus.equals(ap.getApParamCode())) {
//													flagHs = true;
//													break;
//												}
//											}
//											if (!flagHs) {
//												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde")+ "\n";
//											} else {
//												equipment.setHealthStatus(healthStatus);
//											}
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipHealth)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.health.null")+ "\n";
//										}
//									}
//								}
//								
//								//Nam san xuat
//								if (row.size() > 13) {
//									String equipYear = row.get(13);
//									if (isHave != null && !isHave) {
//										String msg = ValidateUtil.validateField(equipYear, "equipment.delivery.year", 4 ,  ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
//										if (!StringUtil.isNullOrEmpty(msg)) {
//											msgEquip += msg;
//										} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipYear)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.manufacturingYear") + "\n";
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(equipYear)) {
//											Integer year = Integer.parseInt(equipYear);
//											Integer curYear = DateUtil.getYear(sysDate);
//											if (year != null && year < 0) {
//												msgEquip += "Năm SX " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.error1") + "\n";
//											} else if (year != null && year.intValue() > curYear.intValue()) {
//												msgEquip += "Năm SX " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.error2") + "\n";
//											} else {
//												equipment.setManufacturingYear(year);
//											}
//										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(equipYear)) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.null")+ "\n";
//										}
//									}
//								}
//								// So thang khau hao
//								if (row.size() > 10) {
//									String value = row.get(10);
//									String msg = ValidateUtil.validateField(value, "equipment.delivery.stkh", 5, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										msgEquip += msg;
//									} else {
//										stkh = Integer.decode(value);
//										if (stkh.intValue() <= 0) {
//											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.stkh.condition") + "\n";
//										}
//									}
//								}
//								/*
//								 * all are OK, create equipment delivery record
//								 * detail
//								 */
//								if (StringUtil.isNullOrEmpty(msgEquip) && equipment != null) {
//									equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
//									// set data
//									if (contentType != null) {
//										equipmentDeliveryRecDetail.setContent(contentType);
//									}
//									equipmentDeliveryRecDetail.setCreateDate(DateUtil.now());
//									equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
//									equipmentDeliveryRecDetail.setEquipment(equipment);
//									equipmentDeliveryRecDetail.setFromStockId(equipment.getStockId());
//									equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
//									equipmentDeliveryRecDetail.setDepreciation(stkh);
//								}
//							}
//						} else { 
//							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.error.line.record") + "\n";
//						}
//
//						/*
//						 * kiem tra: phai co it nhat 1 equipment_delivery_record
//						 */
//						if (StringUtil.isNullOrEmpty(msgEquip) && equipmentDeliveryRecordDetails.size() == 0 && isTrueRecord) {
//							// loi
//							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.not.equipment") + "\n";
//						} else {
//							message += msgEquip;
//						}						
//					}
//					if (!StringUtil.isNullOrEmpty(message)) {
//						lstFails.add(StringUtil.addFailBean(row, message));
//						if (isFirstLineContract) {
//							isTrueRecord = false;
//						}
//						
//					}
//					
//					if (!StringUtil.isNullOrEmpty(msgLine) && isFirstLineContract) {
//						lstFails.add(StringUtil.addFailBean(row, msgLine));
//						msgLine = "";
//						if (isFirstLineContract) {
//							isTrueRecord = false;
//						}
//					}
//					isFirstLineContract = false;
//					if (i == lstData.size() - 1) {
//						if (equipmentDeliveryRecordDetails.size() > 0 && isTrueRecord) {
//							equipDeliveryRecord.setStaff(staff);
//							equipDeliveryRecord.setContractNumber(numberContract);
//							equipDeliveryRecord.setCreateDate(DateUtil.now());
//							equipDeliveryRecord.setCreateUser(currentUser.getUserName());
//							equipDeliveryRecord.setCustomer(customer);
//							equipDeliveryRecord.setToAddress(customer.getAddress());
//							if (customer.getPhone() != null && customer.getMobiphone() != null) {
//								equipDeliveryRecord.setToPhone(customer.getPhone() + "/" + customer.getMobiphone());
//							} else if (customer.getPhone() != null && customer.getMobiphone() == null) {
//								equipDeliveryRecord.setToPhone(customer.getPhone());
//							} else if (customer.getPhone() == null && customer.getMobiphone() != null) {
//								equipDeliveryRecord.setToPhone(customer.getMobiphone());
//							}
//							// trang thai giao nhan: mac dinh la chua gui
//							equipDeliveryRecord.setDeliveryStatus(DeliveryType.NOTSEND.getValue());
//							equipDeliveryRecord.setEquipPeriod(equipPeriod);
//							equipDeliveryRecord.setCreateFormDate(ngayLap);;
//							// Trang thai bien ban: mac dinh la du thao
//							equipDeliveryRecord.setRecordStatus(StatusRecordsEquip.DRAFT.getValue());
//							equipDeliveryRecord.setContractCreateDate(dateContract);
//							equipDeliveryRecord.setToIdNO(idNO);
//							equipDeliveryRecord.setToIdNODate(idNoDate);
//							equipDeliveryRecord.setToIdNOPlace(idNOPlace);
//							equipDeliveryRecord.setToBusinessLincense(idDKKD);
//							equipDeliveryRecord.setToBusinessPlace(idDKKDPlace);
//							equipDeliveryRecord.setToBusinessDate(idDKKDDate);
//							equipDeliveryRecord.setToObjectName(shop.getShopName());
////							equipDeliveryRecord.setToObjectAddress(householdPlace);
//							equipDeliveryRecord.setToPermanentAddress(toPermanentAddress);
//							//dia chi dat tu
//							equipDeliveryRecord.setAddress(soNha);
//							equipDeliveryRecord.setStreet(duong);
//							equipDeliveryRecord.setWardName(phuong);
//							equipDeliveryRecord.setProvinceName(tp);
//							equipDeliveryRecord.setDistrictName(quan);
//							
//							String objectAddress = "";
//							if (soNha != null ) {
//								objectAddress += soNha + " ";
//							}
//							if (duong != null ) {
//								objectAddress += duong + ", ";
//							}
//							if (phuong != null ) {
//								objectAddress += phuong + ", ";
//							}
//							if (quan != null ) {
//								objectAddress += quan + ", ";
//							}
//							if (tp != null ) {
//								objectAddress += tp;
//							}
//							
//							if (objectAddress.length() > 0) {
//								equipDeliveryRecord.setToObjectAddress(objectAddress);
//							}
//							equipDeliveryRecord.setToRepresentative(representative);
//							equipDeliveryRecord.setToPosition(position);
//							equipDeliveryRecord.setToShop(shop);
//							equipDeliveryRecord.setFreezer(freezer);
//							equipDeliveryRecord.setRefrigerator(refrigerator);
//							equipDeliveryRecord.setNote(note);
//							ApParamEquip shopCode = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_OBJECT_CODE.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopCode != null) {
//								equipDeliveryRecord.setFromObjectName(shopCode.getValue());
//							}
//							/**
//							 * Lay thong tin Ben cho muon
//							 * 
//							 * @author hunglm16
//							 * @since August 18, 2015
//							 * */
//							equipDeliveryRecord.setFromObjectName(equipLender.getName());
//							equipDeliveryRecord.setFromObjectAddress(equipLender.getAddress());
//							equipDeliveryRecord.setFromObjectTax(equipLender.getTaxNumber());
//							equipDeliveryRecord.setFromObjectPhone(equipLender.getPhone());
//							equipDeliveryRecord.setFromRepresentative(equipLender.getRepressentative());
//							equipDeliveryRecord.setFromPosition(equipLender.getPosition());
//							equipDeliveryRecord.setFromPage(equipLender.getBusinessLicense());
//							equipDeliveryRecord.setFromPagePlace(equipLender.getBusinessPlace());
//							equipDeliveryRecord.setFromPageDate(equipLender.getBusinessDate());
//							equipDeliveryRecord.setFromFax(equipLender.getFax());
//							/*
//							ApParamEquip shopAddress = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_OBJECT_ADDRESS.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopAddress != null) {
//								equipDeliveryRecord.setFromObjectAddress(shopAddress.getValue());
//							}
//							ApParamEquip shopTax = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_OBJECT_TAX.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopTax != null) {
//								equipDeliveryRecord.setFromObjectTax(shopTax.getValue());
//							}
//							ApParamEquip shopPhone = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_OBJECT_PHONE.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopPhone != null) {
//								equipDeliveryRecord.setFromObjectPhone(shopPhone.getValue());
//							}
//							ApParamEquip shopRepresentative = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_REPRESENTATIVE.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopRepresentative != null) {
//								equipDeliveryRecord.setFromRepresentative(shopRepresentative.getValue());
//							}
//							ApParamEquip shopRePosition = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_POSITION.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopRePosition != null) {
//								equipDeliveryRecord.setFromPosition(shopRePosition.getValue());
//							}
//							ApParamEquip shopPage = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_PAGE.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopPage != null) {
//								equipDeliveryRecord.setFromPage(shopPage.getValue());
//							}
//							ApParamEquip shopPagePlace = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_PAGE_PLACE.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopPagePlace != null) {
//								equipDeliveryRecord.setFromPagePlace(shopPagePlace.getValue());
//							}
//							ApParamEquip shopPageDate = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_PAGE_DATE.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (shopPageDate != null) {
//								equipDeliveryRecord.setFromPageDate(DateUtil.parse(shopPageDate.getValue(), DateUtil.DATE_FORMAT_DDMMYYYY));
//							}
//							ApParamEquip faxNumber = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_FAX.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
//							if (faxNumber != null) {
//								equipDeliveryRecord.setFromFax(faxNumber.getValue());
//							}
//							*/
//							equipmentDeliveryReturnVO = equipmentManagerMgr.createRecordDeliveryByImportExcel(equipDeliveryRecord, equipmentDeliveryRecordDetails, null);
//							if (equipmentDeliveryReturnVO != null) {
//								List<Equipment> lstEquipment = equipmentDeliveryReturnVO.getLstEquipment();
//								String m = "";
//								if (lstEquipment != null) {
//									for (int j = 0, size = lstEquipment.size(); j < size; j++) {
//										for (int count = 0; count <= i; count++) {
//											List<String> rowFail = lstData.get(count);
//											if (lstEquipment.get(j).getCode().equals(rowFail.get(4))) {
//												m += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code")//Thiet bi co ma
//														+ lstEquipment.get(j).getCode() // Ma thiet bi
//														+ Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code1") + "\n";//dang giao dich hoac khong o kho
//												lstFails.add(StringUtil.addFailBean(rowFail, m));
//											}
//										}
//									}
//									
//								}
//							}
//						} 
////						else {
////							//message += msgEquip;
////							lstFails.add(StringUtil.addFailBean(row, message));
////						}
//					}
//				}
////				//bo dong trong cuoi cùng
////				if (lstFails.size() > 0) {
////					lstFails.remove(lstFails.size() - 1);
////				}
//				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_RECORD_DELIVERY_FAIL);
//			} catch (Exception e) {
//				LogUtility.logError(e, e.getMessage());
//				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//				return SUCCESS;
//			}
//		}
//	if (StringUtil.isNullOrEmpty(errMsg)) {
//		isError = false;
//	}
//	isError = false;
//	
//	return SUCCESS;
//	}

	/**
	 * import bien ban bang excel khong co dong trong
	 * 
	 * @author tamvnm
	 * @throws Exception
	 * @since Oct 13 2015
	 */
	public String importRecordDeliveryNotNullLine() throws Exception {
		resetToken(result);
		Date startLogDate = DateUtil.now();
		isError = false;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		String msgFirstLine = "";
		Boolean isFirstLineContract = true;
		Date sysDate = commonMgr.getSysDate();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<CellBean> lstFailsInForms = new ArrayList<CellBean>();
		int limitColumn = 33;
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, limitColumn);
		List<ApParamEquip> lstContent = getListContentDelivety();
		List<EquipStockVO> lstStock = new ArrayList<EquipStockVO>();

		if (lstContent == null || lstContent.size() == 0) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.content.null");
			isError = true;
			return SUCCESS;
		}

		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && !lstData.isEmpty()) {
			try {
//				EquipPeriod equipPeriod = null;
				lstView = new ArrayList<CellBean>();
				List<EquipDeliveryRecDtl> equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
				EquipDeliveryRecord equipDeliveryRecord = new EquipDeliveryRecord();
				EquipDeliveryRecDtl equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
				Staff staff = null;
				Customer customer = null;
				String numberContract = null;
				Date dateContract = null;
				Shop shop = null;
				String idNO = null;
				String idNOPlace = null;
				Date idNoDate = null;
				String idDKKD = null;
				String idDKKDPlace = null;
				Date idDKKDDate = null;
				// String householdPlace = null;
				String toPermanentAddress = "";
				String soNha = null;
				String duong = null;
				String phuong = null;
				String quan = null;
				String tp = null;
				String representative = null;
				String position = null;
				BigDecimal freezer = null;
				BigDecimal refrigerator = null;
				String note = null;
				Integer stkh = null;
				Boolean isTrueRecord = true;
				Date ngayLap = null;
				String value = null;
				EquipStock equipStock = null;
				Map<String, Integer> processedEquipments = new HashMap<String, Integer>();
				List<String> row = new ArrayList<String>();
				EquipmentDeliveryReturnVO equipmentDeliveryReturnVO = new EquipmentDeliveryReturnVO();
				// Lay thong tin ben cho muon
				EquipLender equipLender = commonMgr.getFirtInformationEquipLender(currentUser.getShopRoot().getShopId());
				for (int i = 0; i < lstData.size(); i++) {
					totalItem++;
					row = lstData.get(i);
					int rowSize = row.size();
					isFirstLineContract = isNewRecord(row);
					if (i == 0 && !isFirstLineContract) {
						msgFirstLine = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.delivery.first.line.error") + "\n";
					}
					if (isFirstLineContract) {
						if (equipmentDeliveryRecordDetails.size() > 0 && isTrueRecord) {
							equipDeliveryRecord.setStaff(staff);
							equipDeliveryRecord.setContractNumber(numberContract);
							equipDeliveryRecord.setCreateDate(DateUtil.now());
							equipDeliveryRecord.setCreateUser(currentUser.getUserName());
							equipDeliveryRecord.setCustomer(customer);
							equipDeliveryRecord.setToAddress(customer.getAddress());
							if (customer.getPhone() != null && customer.getMobiphone() != null) {
								equipDeliveryRecord.setToPhone(customer.getPhone() + "/" + customer.getMobiphone());
							} else if (customer.getPhone() != null && customer.getMobiphone() == null) {
								equipDeliveryRecord.setToPhone(customer.getPhone());
							} else if (customer.getPhone() == null && customer.getMobiphone() != null) {
								equipDeliveryRecord.setToPhone(customer.getMobiphone());
							}
							// trang thai giao nhan: mac dinh la chua gui
							equipDeliveryRecord.setDeliveryStatus(DeliveryType.NOTSEND.getValue());
//							equipDeliveryRecord.setEquipPeriod(equipPeriod);
							equipDeliveryRecord.setCreateFormDate(ngayLap);

							// Trang thai bien ban: mac dinh la du thao
							equipDeliveryRecord.setRecordStatus(StatusRecordsEquip.DRAFT.getValue());
							equipDeliveryRecord.setContractCreateDate(dateContract);
							equipDeliveryRecord.setToIdNO(idNO);
							equipDeliveryRecord.setToIdNODate(idNoDate);
							equipDeliveryRecord.setToIdNOPlace(idNOPlace);
							equipDeliveryRecord.setToBusinessLincense(idDKKD);
							equipDeliveryRecord.setToBusinessPlace(idDKKDPlace);
							equipDeliveryRecord.setToBusinessDate(idDKKDDate);
							equipDeliveryRecord.setToObjectName(shop.getShopName());
							// equipDeliveryRecord.setToObjectAddress(householdPlace);
							equipDeliveryRecord.setToPermanentAddress(toPermanentAddress);
							// dia chi dat tu
							equipDeliveryRecord.setAddress(soNha);
							equipDeliveryRecord.setStreet(duong);
							equipDeliveryRecord.setWardName(phuong);
							equipDeliveryRecord.setProvinceName(tp);
							equipDeliveryRecord.setDistrictName(quan);

							String objectAddress = "";
							if (soNha != null) {
								objectAddress += soNha + " ";
							}
							if (duong != null) {
								objectAddress += duong + ", ";
							}
							if (phuong != null) {
								objectAddress += phuong + ", ";
							}
							if (quan != null) {
								objectAddress += quan + ", ";
							}
							if (tp != null) {
								objectAddress += tp;
							}

							if (objectAddress.length() > 0) {
								equipDeliveryRecord.setToObjectAddress(objectAddress);
							}
							equipDeliveryRecord.setToRepresentative(representative);
							equipDeliveryRecord.setToPosition(position);
							equipDeliveryRecord.setToShop(shop);
							equipDeliveryRecord.setFreezer(freezer);
							equipDeliveryRecord.setRefrigerator(refrigerator);
							equipDeliveryRecord.setNote(note);
							ApParamEquip shopCode = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_OBJECT_CODE.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
							if (shopCode != null) {
								equipDeliveryRecord.setFromObjectName(shopCode.getValue());
							}
							/**
							 * Lay thong tin Ben cho muon
							 * 
							 * @author hunglm16
							 * @since August 18, 2015
							 * */
							if (equipLender != null) {
								equipDeliveryRecord.setFromObjectName(equipLender.getName());
								equipDeliveryRecord.setFromObjectAddress(equipLender.getAddress());
								equipDeliveryRecord.setFromObjectTax(equipLender.getTaxNumber());
								equipDeliveryRecord.setFromObjectPhone(equipLender.getPhone());
								equipDeliveryRecord.setFromRepresentative(equipLender.getRepressentative());
								equipDeliveryRecord.setFromPosition(equipLender.getPosition());
								equipDeliveryRecord.setFromPage(equipLender.getBusinessLicense());
								equipDeliveryRecord.setFromPagePlace(equipLender.getBusinessPlace());
								equipDeliveryRecord.setFromPageDate(equipLender.getBusinessDate());
								equipDeliveryRecord.setFromFax(equipLender.getFax());
							}

							equipmentDeliveryReturnVO = equipmentManagerMgr.createRecordDeliveryByImportExcel(equipDeliveryRecord, equipmentDeliveryRecordDetails, null);
							if (equipmentDeliveryReturnVO != null) {
								List<Equipment> lstEquipment = equipmentDeliveryReturnVO.getLstEquipment();
								String m = "";
								if (lstEquipment != null) {
									for (int j = 0, size = lstEquipment.size(); j < size; j++) {
										for (int count = 0; count <= i; count++) {
											List<String> rowFail = lstData.get(count);
											if (lstEquipment.get(j).getCode().equals(rowFail.get(4))) {
												m += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code") + lstEquipment.get(j).getCode() + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code1") + "\n";
												lstFails.add(StringUtil.addFailBean(rowFail, m));
											}
										}
									}

								}
							}
						}
						for (int c = 0, csize = lstFailsInForms.size(); c < csize; c++) {
							if (!StringUtil.isNullOrEmpty(lstFailsInForms.get(c).getErrMsg())) {
								lstFails.add(lstFailsInForms.get(c));
							}
						}
						lstFailsInForms = new ArrayList<CellBean>();

//						equipPeriod = null;
						isTrueRecord = true;
						staff = null;
						customer = null;
						numberContract = null;
						dateContract = null;
						statusRecord = null;
						statusDelivery = null;
						shop = null;
						idNO = null;
						idNOPlace = null;
						idNoDate = null;
						idDKKD = null;
						idDKKDPlace = null;
						idDKKDDate = null;
						// householdPlace = null;
						toPermanentAddress = "";
						soNha = null;
						duong = null;
						phuong = null;
						quan = null;
						tp = null;
						representative = null;
						position = null;
						freezer = null;
						refrigerator = null;
						note = null;
						stkh = null;
						message = "";
						msgFirstLine = "";
						equipStock = null;
						processedEquipments = new HashMap<String, Integer>();
						equipDeliveryRecord = new EquipDeliveryRecord();
						equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
						equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
						ngayLap = null;
					} else {
						isFirstLineContract = false;
					}

					String msgEquip = "";
					if (isFirstLineContract) {
						// ** So hop dong */
						if (rowSize > 0) {
							value = row.get(0);
							String msg = ValidateUtil.validateField(value, "equipment.manager.number.contract", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg) && equipRecordMgr.isExistContractNumInDeliveryRecord(value)) {
								msg = R.getResource("equipment.manager.delivery.contract.number.is.exist");
							}
							if (!StringUtil.isNullOrEmpty(msg)) {
								msgEquip += msg;
							}
							// else if (value.indexOf(" ") != -1) {
							// message +=
							// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
							// "equipment.invalid.format.number.contract.space");
							// }
							else {
								numberContract = value;
							}
						}
						// ** Ma NPP */
						if (rowSize > 1) {
							value = row.get(1);
							String msg = ValidateUtil.validateField(value, "ss.traningplan.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim().toUpperCase();
								shop = shopMgr.getShopByCode(value);
								if (shop == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.npp.code");
								} else if (!checkShopInOrgAccessByCode(value)) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
									shop = null;
								} else if (!shop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
									shop = null;
								}

								if (shop != null) {
									if (shop.getType().getSpecificType().equals(ShopSpecificType.NPP) || shop.getType().getSpecificType().equals(ShopSpecificType.NPP_MT)) {
										// dieu kien dung
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
									}
								}
							}
						}
						// ** Ma GS */
						if (rowSize > 2) {
							value = row.get(2);
							String msg = ValidateUtil.validateField(value, "equipment.manager.staff.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim().toUpperCase();
								staff = staffMgr.getStaffByCode(value);
								if (staff == null || staff.getShop() == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.staff.code");
								} else if (!ActiveType.RUNNING.equals(staff.getStatus())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.staff.code");
								} else if (shop != null && !checkNVGS(value, shop.getId())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.mgs.role.invalid") + "\n";
								}
								if (staff != null) {
									if (StaffSpecificType.SUPERVISOR.equals(staff.getStaffType().getSpecificType()) || StaffSpecificType.GSMT.equals(staff.getStaffType().getSpecificType())) {
										// dieu kien dung
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.mgs") + "\n";
									}
								}
							}
						}
						// ** Ngay lap */
						if (rowSize > 3) {
							value = row.get(3);
							String msg = ValidateUtil.validateField(value, "equipment.create.form.date", 50, ConstantManager.ERR_REQUIRE);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									ngayLap = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
									if (ngayLap == null) {
										message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
									} else {
										if (DateUtil.compareTwoDate(ngayLap, sysDate) == 1) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.big") + "\n";
										} else {
//											// tamvnm: update dac ta moi.
//											// 30/06/2015
//											List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(ngayLap);
//											if (lstPeriod == null || lstPeriod.size() == 0) {
//												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//											} else if (lstPeriod.size() > 0) {
//												// gan gia tri ky dau tien.
//												equipPeriod = lstPeriod.get(0);
//											}
										}
									}
								}
							}
						}
						// ** Ngay hop dong */
						if (rowSize > 4) {
							value = row.get(4);
							String msg = ValidateUtil.validateField(value, "equipment.manager.date.contract", 50, ConstantManager.ERR_REQUIRE);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.manager.date.contract", null);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									dateContract = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
									if (dateContract == null) {
										message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.manager.date.contract", null);
									} else {
										// tamvnm: update dac ta moi.
										// Dec 29, 2015
//										List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(dateContract);
//										if (lstPeriod == null || lstPeriod.size() == 0) {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.contract.not.in") + ConstantManager.NEW_LINE_CHARACTER;
//										} else if (lstPeriod.size() > 0) {
//											// gan gia tri ky dau tien.
//											equipPeriod = lstPeriod.get(0);
//										}
									}
								}
							}
						}
						// ** Ma KH */
						if (shop != null) {
							if (rowSize > 15) {
								value = row.get(15);
								String msg = ValidateUtil.validateField(value, "equipment.manager.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									value = value.trim().toUpperCase();
									List<Customer> lstCus = customerMgr.getListCustomerByShortCode(shop.getId(), value);
									if (lstCus != null && lstCus.size() > 0) {
										customer = lstCus.get(0);
									}

									if (customer == null) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "pay.period.code.checked.customer.shop") + "\n";
									} else if (!shop.getId().equals(customer.getShop().getId())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.manpp") + "\n";
									}
								}
							}
						}
						/*
						 * validate thong tin thiet bi dong dau tien
						 */
						Boolean isSameEquipment = false;
						Equipment equipment = new Equipment();
						equipment.setUsageStatus(null);
						String contentType = null;
						if (rowSize > 5) {
							value = row.get(5);
							if (isSameEquipment != null && !isSameEquipment) {
								String msg = ValidateUtil.validateField(value, "equipment.manager.equipment.content", 100, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else if (lstContent != null && lstContent.size() > 0) {
									for (int c = 0, csize = lstContent.size(); c < csize; c++) {
										if (value.equals(lstContent.get(c).getValue())) {
											contentType = lstContent.get(c).getApParamEquipCode();
										}
									}
									if (contentType == null) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + " không hợp lệ " + "\n";
									}
								} else {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + " không hợp lệ " + "\n";
								}
							}
						}
						/*
						 * validate for equipment code
						 */
						if (rowSize > 8) {
							value = row.get(8) == null ? "" : row.get(8).trim();
							/*
							 * kiem tra trung
							 */
							if (processedEquipments.containsKey(value)) {
								// loi trung
								msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.same.index", value) + "\n";
								isSameEquipment = true;
							} else {
								if (!StringUtil.isNullOrEmpty(value)) {
									processedEquipments.put(value, i + 1);
								}
								// validate tiep
								String msg = ValidateUtil.validateField(value, "equipment.manager.equipment.code", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (StringUtil.isNullOrEmpty(msg)) {
									// equip.lost.recorde.err.ma.thiet.bi.must.require
									if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.ma.thiet.bi.ko.require") + "\n";
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.ma.thiet.bi.must.require") + "\n";
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
										// tao moi thiet bi
										equipment = new Equipment();
										// Trang thai su dung
										equipment.setStatus(StatusType.ACTIVE);
										equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
										equipment.setTradeType(EquipTradeType.DELIVERY);
										equipment.setUsageStatus(null);
										equipment.setCreateDate(sysDate);
										equipment.setCreateUser(currentUser.getUserName());
									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										// tim kiem thiet bi
										equipment = validateEquipment(value);
										if (equipment == null) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.not.exits") + "\n";
										} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
											msgEquip += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
										} else if (EquipTradeStatus.TRADING.getValue().equals(equipment.getTradeStatus())) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.trade.status") + "\n";
										} else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.not.in.stock") + "\n";
										} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.usage.status") + "\n";
										}
										isSameEquipment = false;
									}
								} else {
									msgEquip += msg;
									isSameEquipment = null;
								}
							}
						}
						/*
						 * validate for equipment serial number
						 */
						if (rowSize > 9) {
							value = row.get(9);
							if (isSameEquipment != null && !isSameEquipment) {
								String msg = ValidateUtil.validateField(value, "equipment.manager.equipment.seri", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SERIAL, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else if (StringUtil.isNullOrEmpty(msg) && value.indexOf(" ") != -1) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial") + "\n";
								} else if (equipment != null && equipment.getSerial() != null && !StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.serial") + "\n";
								} else if (equipment != null && equipment.getSerial() == null && !StringUtil.isNullOrEmpty(value)) {
									equipment.setSerial(value);
								}
							}
						}
						// Nhom thiet bi
						if (rowSize > 6) {
							value = row.get(6);
							if (isSameEquipment != null && !isSameEquipment) {
								String msg = ValidateUtil.validateField(value, "equipment.group.product.equipment.group.code", 100, ConstantManager.ERR_MAX_LENGTH);
								EquipGroup group = null;
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.group") + "\n";
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									group = equipmentManagerMgr.getEquipGroupByCode(value);
									if (group != null) {
										if (!ActiveType.RUNNING.getValue().equals(group.getStatus().getValue())) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon") + "\n";
										} else {
											equipment.setEquipGroup(group);
										}
									} else {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined") + "\n";
									}
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.group.null") + "\n";
								}
							}
						}
						// Nha cung cap
						if (rowSize > 7) {
							value = row.get(7);
							if (isSameEquipment != null && !isSameEquipment) {
								String msg = ValidateUtil.validateField(value, "equip.import.equipprovider", 100, ConstantManager.ERR_MAX_LENGTH);
								EquipProvider equipProvider = null;
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.provider") + "\n";
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									equipProvider = equipmentManagerMgr.getEquipProviderByCode(value);
									if (equipProvider != null) {
										if (!ActiveType.RUNNING.getValue().equals(equipProvider.getStatus().getValue())) {
											msgEquip = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.running") + "\n";
										} else {
											equipment.setEquipProvider(equipProvider);
										}
									} else {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.undefined") + "\n";
									}
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.equipprovider.null") + "\n";
								}
							}
						}
						// Gia tri TBBH
						if (rowSize > 11) {
							value = row.get(11);
							if (isSameEquipment != null && !isSameEquipment) {
								String msg = ValidateUtil.validateField(value, "equipment.delivery.tbbh", 17, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.price") + "\n";
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {

									BigDecimal price = BigDecimal.valueOf(Double.parseDouble(value));
									if (price.doubleValue() < 1) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.price") + "\n";
									} else {
										equipment.setPrice(price);
									}
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.tbbh.null") + "\n";
								}
							}
						}
						// kho
						if (rowSize > 12) {
							value = row.get(12);
							if (isSameEquipment != null && !isSameEquipment) {
								String msg = ValidateUtil.validateField(value, "equip.import.stock", 50, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.stock") + "\n";
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									equipStock = equipmentManagerMgr.getEquipStockbyCode(value);
									if (equipStock != null) {
										if (!ActiveType.RUNNING.getValue().equals(equipStock.getStatus().getValue())) {
											msgEquip = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
										} else if (shop != null) {
											lstStock = getListStockShop(shop.getShopCode());
											if (lstStock != null && lstStock.size() > 0) {
												Boolean isTrueStock = false;
												isTrueStock = checkStockIsValid(lstStock, equipStock);
												if (!isTrueStock) {
													msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
												} else {
													equipment.setStockId(equipStock.getId());
													equipment.setStockCode(equipStock.getCode());
													equipment.setStockName(equipStock.getName());
													equipment.setStockType(EquipStockTotalType.KHO);
												}
											} else {
												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
											}
										}
									} else {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
									}
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.stock.adjust.import.stock.empty.err") + "\n";
								}
							}
						}

						// trinh trang thiet bi
						if (rowSize > 13) {
							value = row.get(13);
							if (isSameEquipment != null && !isSameEquipment) {
								String msg = ValidateUtil.validateField(value, "equipment.delivery.health", 100, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.heal") + "\n";
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									String healthStatus = value;
									List<ApParam> lstAppram = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
									boolean flagHs = false;
									for (ApParam ap : lstAppram) {
										if (healthStatus.equals(ap.getApParamCode())) {
											flagHs = true;
											break;
										}
									}
									if (!flagHs) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde") + "\n";
									} else {
										equipment.setHealthStatus(healthStatus);
									}
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.health.null") + "\n";
								}
							}
						}
						// Nam san xuat
						if (rowSize > 14) {
							value = row.get(14);
							if (isSameEquipment != null && !isSameEquipment) {
								String msg = ValidateUtil.validateField(value, "equipment.delivery.year", 4, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.manufacturingYear") + "\n";
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
									Integer year = Integer.parseInt(value);

									Integer curYear = DateUtil.getYear(sysDate);
									if (year != null && year < 0) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.error1") + "\n";
									} else if (year != null && year.intValue() > curYear.intValue()) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.error2") + "\n";
									} else {
										equipment.setManufacturingYear(year);
									}
								} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.null") + "\n";
								}
							}
						}
						// So thang khau hao
						if (rowSize > 10) {
							value = row.get(10);
							String msg = ValidateUtil.validateField(value, "equipment.delivery.stkh", 5, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								msgEquip += msg;
							} else {
								stkh = Integer.decode(value);
								if (stkh.intValue() <= 0) {
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.stkh.condition") + "\n";
								}
							}
						}

						/*
						 * all are OK, create equipment delivery record dong dau
						 * tien detail
						 */
						if (StringUtil.isNullOrEmpty(msgEquip) && equipment != null) {

							equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
							// set data
							equipmentDeliveryRecDetail.setContent(contentType);
							equipmentDeliveryRecDetail.setCreateDate(DateUtil.now());
							equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
							equipmentDeliveryRecDetail.setEquipment(equipment);
							equipmentDeliveryRecDetail.setFromStockId(equipment.getStockId());
							equipmentDeliveryRecDetail.setDepreciation(stkh);
							equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
						} else {
							isTrueRecord = false;
							message += msgEquip;
						}
						// **Dia chi thuong tru */
						if (rowSize > 16) {
							value = row.get(16);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.dc.thuong.tru", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									toPermanentAddress = value.trim();
								}
							}
						}
						// ** CMND */
						if (rowSize > 17) {
							value = row.get(17);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.cmnd", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									idNO = value;
								}
							}
						}

						// ** Noi cap CMND */
						if (rowSize > 18) {
							value = row.get(18);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.noicap", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									idNOPlace = value;
								}
							}
						}

						// ** Ngay cap CMND */
						if (rowSize > 19) {
							value = row.get(19);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ngaycap", 20, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.proposal.export.sheet1.ngaycap", null);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										idNoDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
										if (idNoDate == null) {
											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.proposal.export.sheet1.ngaycap", null);
										} else {
											Date now = new Date();
											if (DateUtil.compareTwoDate(idNoDate, now) == 1) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idNO.dateCreate") + "\n";
											}
										}
									}
								}
							}
						}

						// ** So GP DKKD */
						if (rowSize > 20) {
							value = row.get(20);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.sodkkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									idDKKD = value;
								}
							}
						}

						// ** Noi cap GP DKKD */
						if (rowSize > 21) {
							value = row.get(21);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.noicapdkkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									idDKKDPlace = value;
								}
							}
						}

						// ** Ngay cap GP DKKD */
						if (rowSize > 22) {
							value = row.get(22);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ngaycapdkd", 50, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.proposal.export.sheet1.ngaycapdkd", null);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										idDKKDDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
										if (idDKKDDate == null) {
											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.proposal.export.sheet1.ngaycapdkd", null);
										} else {
											Date now = new Date();
											if (DateUtil.compareTwoDate(idDKKDDate, now) == 1) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idDKKD.dateCreate") + "\n";
											}
										}
									}
								}
							}
						}
						// so nha
						if (rowSize > 23) {
							value = row.get(23);
							// tamvnm: update yeu cau: khong check so nha
							String msg = ValidateUtil.validateField(value, "equipment.delivery.so.nha", 250, ConstantManager.ERR_MAX_LENGTH);
							// String msg = ValidateUtil.validateField(value,
							// "equipment.delivery.so.nha", 250,
							// ConstantManager.ERR_REQUIRE,
							// ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								soNha = value;
							}
						}

						// Ten duong pho/ thon ap
						if (rowSize > 24) {
							value = row.get(24);
							String msg = ValidateUtil.validateField(value, "equipment.delivery.duong", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								duong = value;
							}
						}

						// phuong/xa
						if (rowSize > 25) {
							value = row.get(25);
							// tamvnm: update yeu cau: khong check phuong/xa
							String msg = ValidateUtil.validateField(value, "equipment.delivery.phuong", 250, ConstantManager.ERR_MAX_LENGTH);
							// String msg = ValidateUtil.validateField(value,
							// "equipment.delivery.phuong", 250,
							// ConstantManager.ERR_REQUIRE,
							// ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								phuong = value;
							}
						}
						// quan/huyen
						if (rowSize > 26) {
							value = row.get(26);
							String msg = ValidateUtil.validateField(value, "equipment.delivery.quan", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								quan = value;
							}
						}
						// tinh/tp
						if (rowSize > 27) {
							value = row.get(27);
							String msg = ValidateUtil.validateField(value, "equipment.delivery.tp", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								tp = value;
							}
						}
						// ** Nguoi dai dien */
						if (rowSize > 28) {
							value = row.get(28);
							String msg = ValidateUtil.validateField(value, "equipment.manager.customer.daidien", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								representative = value;
							}
						}

						// ** Chuc vu */
						if (rowSize > 29) {
							value = row.get(29);
							String msg = ValidateUtil.validateField(value, "common.customer.possContact.name", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								position = value;
							}
						}
						// ** DS tu mat */
						if (rowSize > 30) {
							value = row.get(30);
							String msg = ValidateUtil.validateField(value, "equipment.delivery.freezer", 17, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else if (!StringUtil.isNullOrEmpty(value)) {
								freezer = BigDecimal.valueOf(Double.valueOf(value));
								if (freezer.floatValue() <= 0) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.freezer.condition") + "\n";
								}
							}
						}
						// ** DS tu dong */
						if (rowSize > 31) {
							value = row.get(31);
							String msg = ValidateUtil.validateField(value, "equipment.delivery.refrigerator", 17, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else if (!StringUtil.isNullOrEmpty(value)) {
								refrigerator = BigDecimal.valueOf(Double.valueOf(value));
								if (refrigerator.floatValue() <= 0) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.refrigerator.condition") + "\n";
								}
							}
						}
						// ** Ghi chu */
						if (rowSize > 32) {
							value = row.get(32);
							String msg = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								note = value;
							}
						}
						if (StringUtil.isNullOrEmpty(msgEquip) && !StringUtil.isNullOrEmpty(message)) {
							isTrueRecord = false;
						}
						lstFailsInForms.add(StringUtil.addFailBean(row, message));
					} else if (isTrueRecord) {
						/*
						 * validate thong tin thiet bi dong detail
						 */
						Boolean isValidDetailRow = isValidDetail(row);
						if (!isValidDetailRow) {
							msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.invalid.detail") + "\n";
						} else {
							Boolean isHave = false;
							Equipment equipment = new Equipment();
							equipment.setUsageStatus(null);

							// Noi dung
							String contentType = null;
							if (rowSize > 5) {
								value = row.get(5);
								if (isHave != null && !isHave) {
									String msg = ValidateUtil.validateField(value, "equipment.manager.equipment.content", 100, ConstantManager.ERR_REQUIRE);
									if (!StringUtil.isNullOrEmpty(msg)) {
										msgEquip += msg;
									} else if (lstContent != null && lstContent.size() > 0) {
										for (int c = 0, csize = lstContent.size(); c < csize; c++) {
											if (value.equals(lstContent.get(c).getValue())) {
												contentType = lstContent.get(c).getApParamEquipCode();
											}
										}
										if (contentType == null) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + " không hợp lệ " + "\n";
										}
									} else {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + " không hợp lệ " + "\n";
									}
								}
							}
							/*
							 * validate for equipment code
							 */
							if (rowSize > 8) {
								value = row.get(8) == null ? "" : row.get(8).trim();
								/*
								 * kiem tra trung
								 */
								if (processedEquipments.containsKey(value)) {
									// loi trung
									msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.same.index", value) + "\n";
									isHave = true;
								} else {
									if (!StringUtil.isNullOrEmpty(value)) {
										processedEquipments.put(value, i + 1);
									}
									// validate tiep
									String msg = ValidateUtil.validateField(value, "equipment.manager.equipment.code", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (StringUtil.isNullOrEmpty(msg)) {
										// equip.lost.recorde.err.ma.thiet.bi.must.require
										if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.ma.thiet.bi.ko.require") + "\n";
										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.ma.thiet.bi.must.require") + "\n";
										} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
											// tao moi thiet bi
											equipment = new Equipment();
											// Trang thai su dung
											equipment.setStatus(StatusType.ACTIVE);
											equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
											equipment.setTradeType(EquipTradeType.DELIVERY);
											equipment.setUsageStatus(null);
											equipment.setCreateDate(sysDate);
											equipment.setCreateUser(currentUser.getUserName());
										} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
											// tim kiem thiet bi
											equipment = validateEquipment(value);
											if (equipment == null) {
												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.not.exits") + "\n";
											} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
												msgEquip += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
											} else if (EquipTradeStatus.TRADING.getValue().equals(equipment.getTradeStatus())) {
												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.trade.status") + "\n";
											} else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.not.in.stock") + "\n";
											} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.usage.status") + "\n";
											}
											isHave = false;
										}
									} else {
										msgEquip += msg;
										isHave = null;
									}
								}
							}
							/*
							 * validate for equipment serial number
							 */
							if (rowSize > 9) {
								value = row.get(9);
								if (!StringUtil.isNullOrEmpty(value)) {
									if (isHave != null && !isHave) {
										String msg = ValidateUtil.validateField(value, "equipment.manager.equipment.seri", 50, ConstantManager.ERR_MAX_LENGTH);
										if (!StringUtil.isNullOrEmpty(msg)) {
											msgEquip += msg;
										} else if (StringUtil.isNullOrEmpty(msg) && value.indexOf(" ") != -1) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial") + "\n";
										} else if (equipment != null && equipment.getSerial() != null && !StringUtil.isNullOrEmpty(value)) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code.serial") + "\n";
										} else if (equipment != null && equipment.getSerial() == null && !StringUtil.isNullOrEmpty(value)) {
											equipment.setSerial(value);
										}
									}
								}
							}
							// Nhom thiet bi
							if (rowSize > 6) {
								value = row.get(6);
								if (isHave != null && !isHave) {
									String msg = ValidateUtil.validateField(value, "equipment.group.product.equipment.group.code", 100, ConstantManager.ERR_MAX_LENGTH);
									EquipGroup group = null;
									if (!StringUtil.isNullOrEmpty(msg)) {
										msgEquip += msg;
									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.group") + "\n";
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										group = equipmentManagerMgr.getEquipGroupByCode(value);
										if (group != null) {
											if (!ActiveType.RUNNING.getValue().equals(group.getStatus().getValue())) {
												msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon") + "\n";
											} else {
												equipment.setEquipGroup(group);
											}
										} else {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined") + "\n";
										}
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.group.null") + "\n";
									}
								}
							}

							// Nha cung cap
							if (rowSize > 7) {
								value = row.get(7);
								if (isHave != null && !isHave) {
									String msg = ValidateUtil.validateField(value, "equip.import.equipprovider", 100, ConstantManager.ERR_MAX_LENGTH);
									EquipProvider equipProvider = null;
									if (!StringUtil.isNullOrEmpty(msg)) {
										msgEquip += msg;
									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.provider") + "\n";
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										equipProvider = equipmentManagerMgr.getEquipProviderByCode(value);
										if (equipProvider != null) {
											if (!ActiveType.RUNNING.getValue().equals(equipProvider.getStatus().getValue())) {
												msgEquip = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.unon") + "\n";
											} else {
												equipment.setEquipProvider(equipProvider);
											}
										} else {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.undefined") + "\n";
										}
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.equipprovider.null") + "\n";
									}
								}
							}

							// kho
							if (rowSize > 12) {
								value = row.get(12);
								if (isHave != null && !isHave) {
									String msg = ValidateUtil.validateField(value, "equip.import.stock", 50, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										msgEquip += msg;
									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.stock") + "\n";
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										equipStock = equipmentManagerMgr.getEquipStockbyCode(value);
										if (equipStock != null) {
											if (!ActiveType.RUNNING.getValue().equals(equipStock.getStatus().getValue())) {
												msgEquip = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
											} else if (shop != null) {
												lstStock = getListStockShop(shop.getShopCode());
												if (lstStock != null && lstStock.size() > 0) {
													Boolean isTrueStock = false;
													isTrueStock = checkStockIsValid(lstStock, equipStock);
													if (!isTrueStock) {
														msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
													} else {
														equipment.setStockId(equipStock.getId());
														equipment.setStockCode(equipStock.getCode());
														equipment.setStockName(equipStock.getName());
														equipment.setStockType(EquipStockTotalType.KHO);
													}
												} else {
													msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
												}
											}
										} else {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
										}
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.stock.adjust.import.stock.empty.err") + "\n";
									}
								}
							}

							// Gia tri TBBH
							if (rowSize > 11) {
								value = row.get(11);
								if (isHave != null && !isHave) {
									String msg = ValidateUtil.validateField(value, "equipment.delivery.tbbh", 17, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										msgEquip += msg;
									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.price") + "\n";
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {

										BigDecimal price = BigDecimal.valueOf(Double.parseDouble(value));
										if (price.doubleValue() < 1) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.price") + "\n";
										} else {
											equipment.setPrice(price);
										}
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.tbbh.null") + "\n";
									}
								}
							}
							// trinh trang thiet bi
							if (rowSize > 13) {
								value = row.get(13);
								if (isHave != null && !isHave) {
									String msg = ValidateUtil.validateField(value, "equipment.delivery.health", 100, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										msgEquip += msg;
									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.heal") + "\n";
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										String healthStatus = value;
										List<ApParam> lstAppram = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
										boolean flagHs = false;
										for (ApParam ap : lstAppram) {
											if (healthStatus.equals(ap.getApParamCode())) {
												flagHs = true;
												break;
											}
										}
										if (!flagHs) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde") + "\n";
										} else {
											equipment.setHealthStatus(healthStatus);
										}
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.health.null") + "\n";
									}
								}
							}

							// Nam san xuat
							if (rowSize > 14) {
								value = row.get(14);
								if (isHave != null && !isHave) {
									String msg = ValidateUtil.validateField(value, "equipment.delivery.year", 4, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										msgEquip += msg;
									} else if (contentType != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content.manufacturingYear") + "\n";
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && !StringUtil.isNullOrEmpty(value)) {
										Integer year = Integer.parseInt(value);
										Integer curYear = DateUtil.getYear(sysDate);
										if (year != null && year < 0) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.error1") + "\n";
										} else if (year != null && year.intValue() > curYear.intValue()) {
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.error2") + "\n";
										} else {
											equipment.setManufacturingYear(year);
										}
									} else if (contentType != null && ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(contentType) && StringUtil.isNullOrEmpty(value)) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.year.null") + "\n";
									}
								}
							}
							// So thang khau hao
							if (rowSize > 10) {
								value = row.get(10);
								String msg = ValidateUtil.validateField(value, "equipment.delivery.stkh", 5, ConstantManager.ERR_INTEGER, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									msgEquip += msg;
								} else {
									stkh = Integer.decode(value);
									if (stkh.intValue() <= 0) {
										msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.stkh.condition") + "\n";
									}
								}
							}
							/*
							 * all are OK, create equipment delivery record
							 * detail
							 */
							if (StringUtil.isNullOrEmpty(msgEquip) && equipment != null) {
								equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
								// set data
								if (contentType != null) {
									equipmentDeliveryRecDetail.setContent(contentType);
								}
								equipmentDeliveryRecDetail.setCreateDate(DateUtil.now());
								equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
								equipmentDeliveryRecDetail.setEquipment(equipment);
								equipmentDeliveryRecDetail.setFromStockId(equipment.getStockId());
								equipmentDeliveryRecDetail.setDepreciation(stkh);
								equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
							}
						}
						if (!StringUtil.isNullOrEmpty(msgFirstLine)) {
							msgEquip += msgFirstLine;
						}
						lstFailsInForms.add(StringUtil.addFailBean(row, msgEquip));
					} else {
						// truong hop dong bien ban loi -> add het tat ca dong
						// detail vao list fail

						lstFailsInForms.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.error.line.record") + "\n"));
					}

					if (i == lstData.size() - 1) {
						if (equipmentDeliveryRecordDetails.size() > 0 && isTrueRecord && StringUtil.isNullOrEmpty(msgFirstLine)) {
							equipDeliveryRecord.setStaff(staff);
							equipDeliveryRecord.setContractNumber(numberContract);
							equipDeliveryRecord.setCreateDate(DateUtil.now());
							equipDeliveryRecord.setCreateUser(currentUser.getUserName());
							equipDeliveryRecord.setCustomer(customer);
							equipDeliveryRecord.setToAddress(customer.getAddress());
							if (customer.getPhone() != null && customer.getMobiphone() != null) {
								equipDeliveryRecord.setToPhone(customer.getPhone() + "/" + customer.getMobiphone());
							} else if (customer.getPhone() != null && customer.getMobiphone() == null) {
								equipDeliveryRecord.setToPhone(customer.getPhone());
							} else if (customer.getPhone() == null && customer.getMobiphone() != null) {
								equipDeliveryRecord.setToPhone(customer.getMobiphone());
							}
							// trang thai giao nhan: mac dinh la chua gui
							equipDeliveryRecord.setDeliveryStatus(DeliveryType.NOTSEND.getValue());
//							equipDeliveryRecord.setEquipPeriod(equipPeriod);
							equipDeliveryRecord.setCreateFormDate(ngayLap);
							;
							// Trang thai bien ban: mac dinh la du thao
							equipDeliveryRecord.setRecordStatus(StatusRecordsEquip.DRAFT.getValue());
							equipDeliveryRecord.setContractCreateDate(dateContract);
							equipDeliveryRecord.setToIdNO(idNO);
							equipDeliveryRecord.setToIdNODate(idNoDate);
							equipDeliveryRecord.setToIdNOPlace(idNOPlace);
							equipDeliveryRecord.setToBusinessLincense(idDKKD);
							equipDeliveryRecord.setToBusinessPlace(idDKKDPlace);
							equipDeliveryRecord.setToBusinessDate(idDKKDDate);
							equipDeliveryRecord.setToObjectName(shop.getShopName());
							// equipDeliveryRecord.setToObjectAddress(householdPlace);
							equipDeliveryRecord.setToPermanentAddress(toPermanentAddress);
							// dia chi dat tu
							equipDeliveryRecord.setAddress(soNha);
							equipDeliveryRecord.setStreet(duong);
							equipDeliveryRecord.setWardName(phuong);
							equipDeliveryRecord.setProvinceName(tp);
							equipDeliveryRecord.setDistrictName(quan);

							String objectAddress = "";
							if (soNha != null) {
								objectAddress += soNha + " ";
							}
							if (duong != null) {
								objectAddress += duong + ", ";
							}
							if (phuong != null) {
								objectAddress += phuong + ", ";
							}
							if (quan != null) {
								objectAddress += quan + ", ";
							}
							if (tp != null) {
								objectAddress += tp;
							}

							if (objectAddress.length() > 0) {
								equipDeliveryRecord.setToObjectAddress(objectAddress);
							}
							equipDeliveryRecord.setToRepresentative(representative);
							equipDeliveryRecord.setToPosition(position);
							equipDeliveryRecord.setToShop(shop);
							equipDeliveryRecord.setFreezer(freezer);
							equipDeliveryRecord.setRefrigerator(refrigerator);
							equipDeliveryRecord.setNote(note);
							ApParamEquip shopCode = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_OBJECT_CODE.getValue(), ApParamEquipType.EQUIP_DELIVERY_RECORD, ActiveType.RUNNING);
							if (shopCode != null) {
								equipDeliveryRecord.setFromObjectName(shopCode.getValue());
							}
							/**
							 * Lay thong tin Ben cho muon
							 * 
							 * @author hunglm16
							 * @since August 18, 2015
							 * */
							if (equipLender != null) {
								equipDeliveryRecord.setFromObjectName(equipLender.getName());
								equipDeliveryRecord.setFromObjectAddress(equipLender.getAddress());
								equipDeliveryRecord.setFromObjectTax(equipLender.getTaxNumber());
								equipDeliveryRecord.setFromObjectPhone(equipLender.getPhone());
								equipDeliveryRecord.setFromRepresentative(equipLender.getRepressentative());
								equipDeliveryRecord.setFromPosition(equipLender.getPosition());
								equipDeliveryRecord.setFromPage(equipLender.getBusinessLicense());
								equipDeliveryRecord.setFromPagePlace(equipLender.getBusinessPlace());
								equipDeliveryRecord.setFromPageDate(equipLender.getBusinessDate());
								equipDeliveryRecord.setFromFax(equipLender.getFax());
							}
							equipmentDeliveryReturnVO = equipmentManagerMgr.createRecordDeliveryByImportExcel(equipDeliveryRecord, equipmentDeliveryRecordDetails, null);
							if (equipmentDeliveryReturnVO != null) {
								List<Equipment> lstEquipment = equipmentDeliveryReturnVO.getLstEquipment();
								String m = "";
								if (lstEquipment != null) {
									for (int j = 0, size = lstEquipment.size(); j < size; j++) {
										for (int count = 0; count <= i; count++) {
											List<String> rowFail = lstData.get(count);
											if (lstEquipment.get(j).getCode().equals(rowFail.get(8))) {
												m += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code") + lstEquipment.get(j).getCode() + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code1") + "\n";
												lstFails.add(StringUtil.addFailBean(rowFail, m));
											}
										}
									}

								}
							}
						}
						for (int c = 0, csize = lstFailsInForms.size(); c < csize; c++) {
							if (!StringUtil.isNullOrEmpty(lstFailsInForms.get(c).getErrMsg())) {
								lstFails.add(lstFailsInForms.get(c));
							}
						}
					}
				}

				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_RECORD_DELIVERY_FAIL);
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.ManageDeliveryEquipmentAction"), createLogErrorStandard(startLogDate));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				isError = true;
				return SUCCESS;
			}
		}
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			isError = true;
		}
		return SUCCESS;
	}
	
	
	
	/**
	 * Lay thong tin thiet bi: Nhom thiet bi, nha cung cap, tinh trang
	 * 
	 * @author tamvnm
	 * @since June 17,2015
	 */
	public String getLstEquipInfo() {
		
		// lay danh sach nhom thiet bi
		try {
			EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
			equipmentFilter.setkPaging(null);
			if (equipLendCode != null && !equipLendCode.isEmpty()) {
				equipmentFilter.setEquipLendCode(equipLendCode);
			}
			if (customerId != null) {
				equipmentFilter.setCustomerId(customerId);
			}
			
			equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
			ObjectVO<EquipmentVO> eGroup = equipmentManagerMgr.searchEquipmentGroupVObyFilter(equipmentFilter);
			if (eGroup.getLstObject() != null) {
				lstEquipGroup = eGroup.getLstObject();
				result.put("lstEquipGroup", lstEquipGroup);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		
		// lay danh sach nha cung cap, tinh trang thiet bi
		try {
			EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
			equipmentFilter.setkPaging(null);
			if (equipLendCode != null && !equipLendCode.isEmpty()) {
				equipmentFilter.setEquipLendCode(equipLendCode);
			}
			lstEquipProvider = equipmentManagerMgr.getListEquipmentProvider(equipmentFilter);
			result.put("lstEquipProvider", lstEquipProvider);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Chuyen trang tao moi hay chinh sua bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since December 11,2014
	 */
	public String changeRecordDelivery() {
		generateToken();
		try {
			//Lay so khau hao thiet bi
			ApParamEquip equipNumberDeprecTmp = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.EQUIP_NUMBER_DEPRECIATION.getValue(), ApParamEquipType.EQUIP_NUMBER_DEPRECIATION, ActiveType.RUNNING);
			if (equipNumberDeprecTmp != null && equipNumberDeprecTmp.getValue() != null && StringUtil.isNumberInt(equipNumberDeprecTmp.getValue())) {
				equipNumberDeprec  = Integer.valueOf(equipNumberDeprecTmp.getValue());					
			}
			lstContent = getListContentDelivety();
			lstHealthy = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
			
			EquipDeliveryRecord equipDeliveryRecord = new EquipDeliveryRecord();
			if (id != null) {
				equipDeliveryRecord = equipmentManagerMgr.getEquipDeliveryRecordById(id);				
			}
			
			// lay danh sach nhom thiet bi
			try {
				EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
				equipmentFilter.setkPaging(null);
				equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
				if (equipDeliveryRecord != null && equipDeliveryRecord.getEquipLend() != null) {
					equipmentFilter.setEquipLendCode(equipDeliveryRecord.getEquipLend().getCode());
					if (equipDeliveryRecord.getCustomer() != null) {
						equipmentFilter.setCustomerId(equipDeliveryRecord.getCustomer().getId());
					}
				}

				ObjectVO<EquipmentVO> eGroup = equipmentManagerMgr.searchEquipmentGroupVObyFilter(equipmentFilter);
				if (eGroup.getLstObject() != null) {
					lstEquipGroup = eGroup.getLstObject();
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
			
			// lay danh sach nha cung cap, tinh trang thiet bi
			try {
				EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
				equipmentFilter.setkPaging(null);
				lstEquipProvider = equipmentManagerMgr.getListEquipmentProvider(equipmentFilter);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
			ContentNew = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue(), ApParamEquipType.DELIVERY_CONTENT, ActiveType.RUNNING);
			if (id != null) {				
				if(equipDeliveryRecord != null){
					idRecordDelivery = id;
					if(equipDeliveryRecord.getCustomer() != null) {
						shopCode = equipDeliveryRecord.getCustomer().getShop().getShopCode();
						customerCode = equipDeliveryRecord.getCustomer().getShortCode();
						customerId = equipDeliveryRecord.getCustomer().getId();
						customerCode = equipDeliveryRecord.getCustomer().getShortCode();
					}
					note = equipDeliveryRecord.getNote();
					recordCode = equipDeliveryRecord.getCode();
					equipLendCode = equipDeliveryRecord.getEquipLend() == null ? "" : equipDeliveryRecord.getEquipLend().getCode();
					if(equipDeliveryRecord.getStaff() != null) {
						staffCode = equipDeliveryRecord.getStaff().getStaffCode();
						staffName = equipDeliveryRecord.getStaff().getStaffName();
					}
					
					numberContract = equipDeliveryRecord.getContractNumber();
					
					if(equipDeliveryRecord.getContractCreateDate() != null) {
						contractDate = DateUtil.toDateString(equipDeliveryRecord.getContractCreateDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					
					if (equipDeliveryRecord.getCreateFormDate() != null) {
						createDate = DateUtil.convertDateByString(equipDeliveryRecord.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY) ;
					}
					
					statusDelivery = equipDeliveryRecord.getDeliveryStatus();
					statusRecord = equipDeliveryRecord.getRecordStatus();
					fromObjectAddress = equipDeliveryRecord.getFromObjectAddress();
					fromObjectTax = equipDeliveryRecord.getFromObjectTax();
					fromObjectPhone = equipDeliveryRecord.getFromObjectPhone();
					fromRepresentative = equipDeliveryRecord.getFromRepresentative();
					fromObjectPosition = equipDeliveryRecord.getFromPosition();
					fromPage = equipDeliveryRecord.getFromPage();
					fromPagePlace = equipDeliveryRecord.getFromPagePlace();
					fromFax = equipDeliveryRecord.getFromFax();
					
					if (equipDeliveryRecord.getFromPageDate() != null) {
						fromPageDate = DateUtil.toDateString(equipDeliveryRecord.getFromPageDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					toPermanentAddress = equipDeliveryRecord.getToPermanentAddress();
					addressName = equipDeliveryRecord.getAddress();
					street = equipDeliveryRecord.getStreet();
					wardName = equipDeliveryRecord.getWardName();
					provinceName = equipDeliveryRecord.getProvinceName();
					districtName = equipDeliveryRecord.getDistrictName();
					toRepresentative = equipDeliveryRecord.getToRepresentative();
					toObjectPosition = equipDeliveryRecord.getToPosition();
					toIdNO = equipDeliveryRecord.getToIdNO();
					
					if(equipDeliveryRecord.getToIdNODate() != null) {
						toIdNODate = DateUtil.toDateString(equipDeliveryRecord.getToIdNODate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					
					toIdNOPlace = equipDeliveryRecord.getToIdNOPlace();
					toBusinessLicense = equipDeliveryRecord.getToBusinessLincense();
					if(equipDeliveryRecord.getToBusinessDate() != null) {
						toBusinessDate = DateUtil.toDateString(equipDeliveryRecord.getToBusinessDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					toBusinessPlace = equipDeliveryRecord.getToBusinessPlace();
					freezer = equipDeliveryRecord.getFreezer();
					refrigerator = equipDeliveryRecord.getRefrigerator();
					
					if(equipDeliveryRecord.getEquipLend() != null) {
						equipLendCode = equipDeliveryRecord.getEquipLend().getCode();
						EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
						equipmentFilter.setkPaging(null);
						if (equipDeliveryRecord.getCustomer() != null) {
							equipmentFilter.setCustomerId(equipDeliveryRecord.getCustomer().getId());
						}
						
						equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
						ObjectVO<EquipmentVO> eGroup = equipmentManagerMgr.searchEquipmentGroupVObyFilter(equipmentFilter);
						if (eGroup.getLstObject() != null) {
							lstEquipGroupId = new ArrayList<Long>();
							for (int i = 0; i < eGroup.getLstObject().size(); i++) {
								if (eGroup.getLstObject().get(i) != null && eGroup.getLstObject().get(i).getEquipGroupId() != null) {
									lstEquipGroupId.add(eGroup.getLstObject().get(i).getEquipGroupId());
								}
							}
						}
					}
					/** bbo check shop ben cho muon*/				
//					if(equipDeliveryRecord.getFromShop() != null) { 
//						fromShopCode = equipDeliveryRecord.getFromShop().getShopCode();
//					}
					fromShopCode = equipDeliveryRecord.getFromObjectName();
					if(equipDeliveryRecord.getToShop() != null) { 
						toShopCode = equipDeliveryRecord.getToShop().getShopCode();
					}
				} 
				//Lay danh sach tap tin dinh kem neu co
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(id);
				filter.setObjectType(EquipTradeType.DELIVERY.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
			} else {
				EquipLender equipLender = commonMgr.getFirtInformationEquipLender (currentUser.getShopRoot().getShopId());
				if (equipLender != null) {
					fromShopCode = equipLender.getName();
					fromObjectAddress = equipLender.getAddress();
					fromObjectTax = equipLender.getTaxNumber();
					fromObjectPhone = equipLender.getPhone();
					fromRepresentative = equipLender.getRepressentative();
					fromObjectPosition = equipLender.getPosition();
					fromPage = equipLender.getBusinessLicense();
					fromPagePlace = equipLender.getBusinessPlace();
					if (equipLender.getBusinessDate() != null) {
						fromPageDate = DateUtil.convertDateByString(equipLender.getBusinessDate(), DateUtil.DATE_FORMAT_DDMMYYYY);				
					}
					fromFax = equipLender.getFax();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);

		}
		result.put(ERROR, false);
		return SUCCESS;
	}

	public Map<String,String> getMapCMSControl (){
		Map<String, String> map = new HashMap<String, String>();
//		FormToken form = getFormToken();
//		mapControl = new HashMap<String, Integer>();
//		if (form != null && form.getListControl() != null && form.getListControl().size() > 0) {
//			for (ControlToken controlToken : listControl) {
//				mapControl.put(controlToken.getControlCode(), controlToken.getStatus());
//			}
//		}
		String mess = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.control") ;
		String[] lstMess = mess.split(";");
		for (int i = 0; i < lstMess.length; i++) {
			String text = lstMess[i];
			String[] lstText = text.split("-");
			if (lstText.length == 2) {
				map.put(lstText[0], lstText[1]);
			} else {
				return null;
			}
		}
		return map;
	} 
	
	/**
	 * Chinh sua bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since December 11,2014
	 */
//	public String updateRecordDelivery() {
//		Integer statusRe = 0;
//		String errMsg = "";
//		Date dateContract = null;
//		Date fromDateLend = null;
//		Date toDateLend = null;
//		Date toBDate = null;
//		result.put(ERROR, false);
//		try {
//			if (statusRecord != null && statusRecord.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
//				// Xu ly file dinh kem
//				List<Long> lstFileIdDelete = new ArrayList<Long>();
//				Integer maxLength = 0;				
//				EquipRecordFilter<FileVO> filterFile = new EquipRecordFilter<FileVO>();
//				filterFile.setObjectId(idRecordDelivery);
//				filterFile.setObjectType(EquipTradeType.DELIVERY.getValue());
//				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filterFile).getLstObject();
//				if (lstFileVo == null || lstFileVo.isEmpty()) {
//					lstFileVo = new ArrayList<FileVO>();
//				}
//				// Xu ly Xoa file dinh kem
//				if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
//					String[] arrEquipAttachFile = equipAttachFileStr.split(",");
//					for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
//						String value = arrEquipAttachFile[i];
//						if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
//							lstFileIdDelete.add(Long.parseLong(value));
//						}
//					}
//				}
//				maxLength = lstFileVo.size() - lstFileIdDelete.size();
//				if (maxLength < 0) {
//					maxLength = 0;
//				}
//				errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
//				if (errMsg.length() > 0) {
//					result.put(ERROR, true);
//					result.put("errMsg", errMsg);
//					return JSON;
//				}				
//				// luu file tam de xoa tren server
//				ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
//				if (lstFileIdDelete != null && lstFileIdDelete.size() > 0) {
//					BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
//					filterF.setLstId(lstFileIdDelete);
//					filterF.setObjectId(idRecordDelivery);
//					filterF.setObjectType(EquipTradeType.DELIVERY.getValue());
//					objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
//				}
//				//filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
//				// Luu file dinh kem
//				List<FileVO> lstfileVOs = saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType);
//				EquipRecordFilter<EquipmentDeliveryVO> filter = new EquipRecordFilter<EquipmentDeliveryVO>();
//				filter.setLstFileVo(lstfileVOs);
//				filter.setLstEquipAttachFileId(lstFileIdDelete);
//				statusRe = StatusRecordsEquip.CANCELLATION.getValue();
//				EquipDeliveryRecord edelivery = equipmentManagerMgr.getEquipDeliveryRecordById(idRecordDelivery);
//				edelivery.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
//				equipmentManagerMgr.updateRecordDelivery(edelivery, new ArrayList<EquipDeliveryRecDtl>(), new ArrayList<String>(), statusRe,filter);
//				return JSON;
//			}
//			if (idRecordDelivery != null) {
//				EquipDeliveryRecord edeliveryX = equipmentManagerMgr.getEquipDeliveryRecordById(idRecordDelivery);
//				if (edeliveryX != null) {
//					if (statusRecord.equals(StatusRecordsEquip.APPROVED.getValue()) && edeliveryX.getRecordStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
//						result.put(ERROR, true);
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.cancel"));
//						return JSON;
//					}
//				}
//			}
//			map = getMapCMSControl();
//			if (map != null) {
//				if (idRecordDelivery != null) {
//					if (isTrue("numberContract")) {
//						if (StringUtil.isNullOrEmpty(numberContract)) {
//							result.put(ERROR, true);
//							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.customer.code")));
//							return JSON;
//						}else{
//							errMsg = ValidateUtil.validateField(numberContract, "equipment.manager.number.contract", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT, ConstantManager.ERR_MAX_LENGTH);
//							if (StringUtil.isNullOrEmpty(errMsg) && numberContract.indexOf(" ") != -1) {
//								errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.format.number.contract.space") + "\n";
//							}
//							if(!StringUtil.isNullOrEmpty(errMsg) ){
//								result.put(ERROR, true);
//								result.put("errMsg", errMsg);
//								return JSON;
//							}
//						}
//					}
//					if (isTrue("contractDate")) {
//						if (!StringUtil.isNullOrEmpty(contractDate)) {
//							dateContract = DateUtil.parse(contractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
//							if(dateContract == null){
//								result.put(ERROR, true);
//								result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
//								return JSON;
//							}
//						} else {
//							result.put(ERROR, true);
//							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
//							return JSON;
//						}
//					}
//					if (isTrue("fromPageDate")) {
//						if (!StringUtil.isNullOrEmpty(fromPageDate)) {
//							fromDateLend = DateUtil.parse(fromPageDate, DateUtil.DATE_FORMAT_DDMMYYYY);
//							if(fromDateLend == null){
//								result.put(ERROR, true);
//								result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
//								return JSON;
//							}
//						} else {
//							result.put(ERROR, true);
//							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
//							return JSON;
//						}
//					}
//					if (isTrue("toIdNODate")) {
//						if (!StringUtil.isNullOrEmpty(toIdNODate)) {
//							toDateLend = DateUtil.parse(toIdNODate, DateUtil.DATE_FORMAT_DDMMYYYY);
//							if(toDateLend == null){
//								result.put(ERROR, true);
//								result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
//								return JSON;
//							}
//						} else {
//							//bo check bat buoc
////							result.put(ERROR, true);
////							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.fromDate")));
////							return JSON;
//						}
//					}
//					if (isTrue("toBusinessDate")) {
//						if (!StringUtil.isNullOrEmpty(toBusinessDate)) {
//							toBDate = DateUtil.parse(toBusinessDate, DateUtil.DATE_FORMAT_DDMMYYYY);
//							if(toBDate == null){
//								result.put(ERROR, true);
//								result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
//								return JSON;
//							}
//						} else {
//							//bo check bat buoc
////							result.put(ERROR, true);
////							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
////							return JSON;
//						}
//					}
//					EquipDeliveryRecord e = equipmentManagerMgr.getEquipDeliveryRecordById(idRecordDelivery);
//					if (e.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
//						result.put(ERROR, true);
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equiment.manager.status"));
//						return JSON;
//					}
//					if (!e.getRecordStatus().equals(StatusRecordsEquip.APPROVED)) {
//						if (isTrue("numberContract")) {
//							e.setContractNumber(numberContract);
//						}
//						if (isTrue("numberContract")) {
//						e.setContractCreateDate(dateContract);
//						
//						//Ben cho muon
//						if (!StringUtil.isNullOrEmpty(fromObjectAddress)) {
//							e.setFromObjectAddress(fromObjectAddress);
//						}
//						if (!StringUtil.isNullOrEmpty(fromObjectTax)) {
//							e.setFromObjectTax(fromObjectTax);
//						}
//						if (!StringUtil.isNullOrEmpty(fromObjectPhone)) {
//							e.setFromObjectPhone(fromObjectPhone);
//						}
//						if (!StringUtil.isNullOrEmpty(fromRepresentative)) {
//							e.setFromRepresentative(fromRepresentative);
//						}
//						if (!StringUtil.isNullOrEmpty(fromObjectPosition)) {
//							e.setFromPosition(fromObjectPosition);
//						}
//						if (!StringUtil.isNullOrEmpty(fromFax)) {
//							e.setFromFax(fromFax);
//						}
//						if (!StringUtil.isNullOrEmpty(fromPage)) {
//							e.setFromPage(fromPage);
//						}
//						if (!StringUtil.isNullOrEmpty(fromPagePlace)) {
//							e.setFromPagePlace(fromPagePlace);
//						}
//						if (isTrue("fromPageDate")) {
//							e.setFromPageDate(fromDateLend);
//						}
//						//Ben muon
//						
//						if (!StringUtil.isNullOrEmpty(toObjectAddress)) {
//							e.setToObjectAddress(toObjectAddress);
//						}
//						if (!StringUtil.isNullOrEmpty(toRepresentative)) {
//							e.setToRepresentative(toRepresentative);
//						}
//						if (!StringUtil.isNullOrEmpty(toObjectPosition)) {
//							e.setToPosition(toObjectPosition);
//						}
////						if (!StringUtil.isNullOrEmpty(toIdNO)) {
//							e.setToIdNO(toIdNO);
////						}
////						if (!StringUtil.isNullOrEmpty(toIdNOPlace)) {
//							e.setToIdNOPlace(toIdNOPlace);
////						}
////						if (!StringUtil.isNullOrEmpty(toBusinessLicense)) {
//							e.setToBusinessLincense(toBusinessLicense);
////						}
////						if (!StringUtil.isNullOrEmpty(toBusinessPlace)) {
//							e.setToBusinessPlace(toBusinessPlace);
////						}
////						if (isTrue("toIdNODate")) {
//							e.setToIdNODate(toDateLend);
////						}
////						if (isTrue("toBusinessDate")) {
//							e.setToBusinessDate(toBDate);
////						}
//						
//						// cam ket doanh so
//						if (isTrue("freezer")) {
//							e.setFreezer(freezer);
//						}
//						if (isTrue("refrigerator")) {
//							e.setRefrigerator(refrigerator);
//						}
//					}
//					
//					Staff staff = null;
//					if (isTrue("staffCode")) {
//						if(!StringUtil.isNullOrEmpty(staffCode)) {
//							staff = staffMgr.getStaffByCode(staffCode);
//							if(staff != null) {
//								e.setStaff(staff);
//							} else {
//								result.put(ERROR, true);
//								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, String.format(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.staffowner.code.status.not.active"), staffCode)));
//								return JSON;
//							}
//						}
//					}
//					if (isTrue("fromShopCode")) {
//						/**bo check shop ben cho muon*/
//						if (!StringUtil.isNullOrEmpty(fromShopCode)) {
//	//						Shop fromShop = shopMgr.getShopByCode(fromShopCode);
//	//						if(fromShop != null && !fromShopCode.equals("VNM")) {
//	//							if (!e.getRecordStatus().equals(StatusRecordsEquip.APPROVED)) {
//	//								e.setFromShop(fromShop);
//	//								e.setFromObjectName(fromShop.getShopName());
//	//								e.setFromFax(fromShop.getFax());
//	//								if (StringUtil.isNullOrEmpty(fromObjectAddress)) {
//	//									e.setFromObjectAddress(fromShop.getAddress());
//	//								}
//	//							}
//	//						} else if (fromShopCode.equals("VNM")) {
//							
////							e.setFromObjectName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.vnm.name"));
////							if (StringUtil.isNullOrEmpty(fromObjectAddress)) {
////								e.setFromObjectAddress(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.vnm.address"));
////							}
////							if (StringUtil.isNullOrEmpty(fromObjectPhone)) {
////								e.setFromObjectPhone(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.vnm.phone"));
////							}
////								
////							e.setFromFax(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.vnm.fax"));
//							e.setFromObjectName(fromShopCode);
//							} else {
//								result.put(ERROR, true);
//								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, String.format(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.shop.status.not.active"), fromShopCode)));
//								return JSON;
//							}
//						}
//					Shop toShop = null;
//					if (isTrue("toShopCode")) {
//						if (!StringUtil.isNullOrEmpty(toShopCode)) {
//							toShopCode = toShopCode.toUpperCase();
//							toShop = shopMgr.getShopByCode(toShopCode);
//							if(toShop != null) {
//								if(e.getToShop() != null && toShop.getId() != e.getToShop().getId() && customerId == null) {
//									e.setCustomer(null);
//								}
//								if(e.getToShop() != null &&  toShop.getId() != e.getToShop().getId() && staff == null) {
//									e.setStaff(null);
//								}
//								if (!e.getRecordStatus().equals(StatusRecordsEquip.APPROVED)) {
//									e.setToShop(toShop);
//									e.setToObjectName(toShop.getShopName());
//		//							if (toShop.getPhone() != null) {
//		//								e.setToPhone(toShop.getPhone());
//		//							} else {
//		//								e.setToPhone(toShop.getMobiphone());
//		//							}
//								}
//							} else {
//								result.put(ERROR, true);
//								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, String.format(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "Mã đơn vị {0} đang ở trạng thái không hoạt động."), toShopCode)));
//								return JSON;
//							}
//						}
//					}
//					if (isTrue("customerCode")) {
//						if(customerId != null) {
//							Customer cus = commonMgr.getEntityById(Customer.class, customerId);
//							if(cus != null) {
//								if (!e.getRecordStatus().equals(StatusRecordsEquip.APPROVED)) {
//									e.setCustomer(cus);
//									e.setToAddress(cus.getAddress());
//									if (cus.getPhone() != null) {
//										e.setToPhone(cus.getPhone());
//									} else {
//										e.setToPhone(cus.getMobiphone());
//									}
//								}
//								if (toShop != null && cus.getShop().getId() != null &&  !cus.getShop().getId().equals(toShop.getId())) {
//									result.put(ERROR, true);
//									result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.customer.not.npp")));
//									return JSON;
//								} 
//							} else {
//								result.put(ERROR, true);
//								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.customer.not.exist.shop")));
//								return JSON;
//							}
//							
//						}
//					}
//					if (isTrue("statusRecord")) {
//						if (e != null && statusRecord != null) {
//							// du thao
//							if (e.getRecordStatus() != null && e.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
//								e.setUpdateDate(DateUtil.now());
//								e.setUpdateUser(currentUser.getUserName());						
//								// duyet
//								if (statusRecord.equals(StatusRecordsEquip.APPROVED.getValue())) {
//									// trang thai tu du thao -> duyet
//									statusRe = StatusRecordsEquip.APPROVED.getValue();
//									e.setRecordStatus(statusRecord);
//								} else if (statusRecord.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
//									// trang thai tu du thao -> huy
//									statusRe = StatusRecordsEquip.CANCELLATION.getValue();
//									e.setRecordStatus(statusRecord);
//								}
//							}
//						}
//					}
//					if (isTrue("statusDelivery")) {
//						if (e != null && statusDelivery != null) {
//							// neu trang thai bien ban la duyet thi cho cap nhat
//							// giao nhan
//							if (e.getRecordStatus() != null && e.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
//								if (e.getDeliveryStatus() != null && (statusDelivery - e.getDeliveryStatus() - 1 == 0)) {
//									e.setDeliveryStatus(statusDelivery);
//									if (statusRe == 0) {
//										e.setUpdateDate(DateUtil.now());
//										e.setUpdateUser(currentUser.getUserName());
//										statusRe = -1;
//									} else if (statusRe.equals(StatusRecordsEquip.APPROVED.getValue())) {
//										statusRe = -2;
//									}
//								}
//							}
//						}
//					}
//					List<String> lstDelete = new ArrayList<String>();
//					if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
//						String[] lst = lstEquipDelete.toString().split(",");
//						for (int i = 0; i < lst.length; i++) {
//							lstDelete.add(lst[i]);
//						}
//					}
//					
//					List<EquipDeliveryRecDtl> equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
//					if (lstEquipCode != null && !lstEquipCode.isEmpty()) {
//						for (int i = 0; i < lstEquipCode.size(); i++) {
//							if(lstEquipCode.lastIndexOf(lstEquipCode.get(i)) != i) {
//								result.put(ERROR, true);
//								result.put("errMsg", "Mã TB " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau") + "\n");
//								return JSON;
//							}
//							EquipDeliveryRecDtl equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
//							Equipment equipment = null;
//							if (lstEquipCode.get(i).equals("null")) {
//								continue;
//							} else {
//								equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
////								equipment = validateEquipment(lstEquipCode.get(i));
//							}
//	
//							if (equipment != null ) {
//								Equipment equipValidate = validateEquipment(equipment.getCode());
//								if (equipValidate == null) {
//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.role") + "\n";
//								} else if (lstDelete.indexOf(equipment.getCode())==-1 && !StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " đang ở trạng thái không hoạt động";
//								} else 
//	//								if (equipment.getStockType().getValue().equals(EquipStockTotalType.KHO_KH.getValue())) {
//	//								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock") + "\n";
//	//							} else 
//									if (lstDelete.indexOf(equipment.getCode())==-1 && !EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.usage.status") + "\n";
//								} else if (lstDelete.indexOf(equipment.getCode())==-1 && equipment.getTradeStatus().equals(EquipTradeStatus.TRADING.getValue()) 
//										&& equipment.getSerial() != null && lstSeriNumber != null && !StringUtil.isNullOrEmpty(lstSeriNumber.get(i))
//										&& !equipment.getSerial().equals(lstSeriNumber.get(i))) {
//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.trade.status") + "\n";
//								} else 
//	//								if (EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
//	//								if (equipment.getStockType().getValue().equals(EquipStockTotalType.KHO_NPP.getValue()) && staff != null && !staff.getShop().getShopCode().equals(equipment.getStockCode())) {
//	//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.npp") + "\n";
//	//								}
//	//								EquipStockTotal equipStockTotal = equipmentManagerMgr.getEquipStockTotalById(equipment.getStockId(), EquipStockTotalType.parseValue(equipment.getStockType().getValue()), equipment.getEquipGroup().getId());
//	//								if(equipStockTotal == null || equipStockTotal.getQuantity() <= 0){
//	//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.stock.invalid") + "\n";
//	//								}
//	//							}
//								if (equipment.getSerial() == null && lstContentDelivery.get(i) != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(lstContentDelivery.get(i))) {
//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content");
////								}
////								else if (StringUtil.isNullOrEmptyNotTrim(equipment.getSerial()) && lstSeriNumber != null && !StringUtil.isNullOrEmptyNotTrim(lstSeriNumber.get(i))) {
//	//								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.serial");
//								}else if(equipment.getSerial() == null && lstSeriNumber != null){
//									if(!ValidateUtil.validateFormatSerial(lstSeriNumber.get(i)) && !lstSeriNumber.get(i).isEmpty()){
//										errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.seri.format");
//									}else if(StringUtil.isNullOrEmpty(errMsg) && lstSeriNumber.get(i).indexOf(" ") != -1){
//										errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial") + "\n";
//									}
//								}							
//								if (StringUtil.isNullOrEmpty(equipment.getSerial()) && lstSeriNumber != null 
//										&& !StringUtil.isNullOrEmpty(lstSeriNumber.get(i)) && isTrue("seriNumberInGrid")) {
//									equipment.setSerial(lstSeriNumber.get(i));
//								}
//								
//								if (lstDepreciation != null && lstDepreciation.get(i) != null) {
//									equipmentDeliveryRecDetail.setDepreciation(lstDepreciation.get(i));
//								}
//								if (errMsg.length() > 0) {
//									result.put(ERROR, true);
//									result.put("errMsg", errMsg);
//									return JSON;
//								}
//								equipmentDeliveryRecDetail.setEquipment(equipment);
//							} else {
//								result.put(ERROR, true);
//								result.put("errMsg", "Không tìm thấy thiết bị có mã " + lstEquipCode.get(i));
//								return JSON;
//							}
//	
//							// thong tin chi tiet bien ban
//							equipmentDeliveryRecDetail.setContent(lstContentDelivery.get(i));
//							equipmentDeliveryRecDetail.setCreateDate(DateUtil.now());
//							equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
//							equipmentDeliveryRecDetail.setFromStockId(equipment.getStockId());
//	//						equipmentDeliveryRecDetail.setFromStockType(equipment.getStockType().getValue());
//							equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
//						}
//					}
//					e.setUpdateDate(DateUtil.now());
//					e.setUpdateUser(currentUser.getUserName());
//					
//					/** Xu ly truong hop xoa het thiet bi van cap nhat dc bien ban*/
//					EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
//					equipmentFilter.setkPaging(null);
//					if (idRecordDelivery != null) {
//						equipmentFilter.setIdRecordDelivery(idRecordDelivery);
//					}
//					ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentDeliveryVOByFilter(equipmentFilter);
//					if (equipmentDeliverys != null) {
//						List<EquipmentDeliveryVO> lstEquipDetail = equipmentDeliverys.getLstObject();
//						if (lstEquipDetail != null && lstEquipDetail.size() == lstDelete.size() && equipmentDeliveryRecordDetails.size() == 0) {
//							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.detail.null") + "\n";
//							result.put(ERROR, true);
//							result.put("errMsg", errMsg);
//							return JSON;
//						}
//					}
//					
//					// Xu ly file dinh kem
//					List<Long> lstFileIdDelete = new ArrayList<Long>();
//					Integer maxLength = 0;				
//					EquipRecordFilter<FileVO> filterFile = new EquipRecordFilter<FileVO>();
//					filterFile.setObjectId(idRecordDelivery);
//					filterFile.setObjectType(EquipTradeType.DELIVERY.getValue());
//					lstFileVo = equipRecordMgr.searchListFileVOByFilter(filterFile).getLstObject();
//					if (lstFileVo == null || lstFileVo.isEmpty()) {
//						lstFileVo = new ArrayList<FileVO>();
//					}
//					// Xu ly Xoa file dinh kem
//					if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
//						String[] arrEquipAttachFile = equipAttachFileStr.split(",");
//						for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
//							String value = arrEquipAttachFile[i];
//							if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
//								lstFileIdDelete.add(Long.parseLong(value));
//							}
//						}
//					}
//					maxLength = lstFileVo.size() - lstFileIdDelete.size();
//					if (maxLength < 0) {
//						maxLength = 0;
//					}
//					errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
//					if (errMsg.length() > 0) {
//						result.put(ERROR, true);
//						result.put("errMsg", errMsg);
//						return JSON;
//					}				
//					// luu file tam de xoa tren server
//					ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
//					if (lstFileIdDelete != null && lstFileIdDelete.size() > 0) {
//						BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
//						filterF.setLstId(lstFileIdDelete);
//						filterF.setObjectId(idRecordDelivery);
//						filterF.setObjectType(EquipTradeType.DELIVERY.getValue());
//						objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
//					}
//					//filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
//					// Luu file dinh kem
//					List<FileVO> lstfileVOs = saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType);
//					EquipRecordFilter<EquipmentDeliveryVO> filter = new EquipRecordFilter<EquipmentDeliveryVO>();
//					filter.setLstFileVo(lstfileVOs);
//					filter.setLstEquipAttachFileId(lstFileIdDelete);
//					equipmentManagerMgr.updateRecordDelivery(e, equipmentDeliveryRecordDetails, lstDelete, statusRe, filter);
//					if (e.getEquipLend() != null) {
//						EquipLend equipLend = equipmentManagerMgr.getEquipLendById(e.getEquipLend().getEquipLendId());
//						if (equipLend != null) {
//							equipLend.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
//							commonMgr.updateEntity(equipLend);
//						}
//					}
//					// xoa file tren server
//					if (lstFileIdDelete != null && lstFileIdDelete.size() > 0 && objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
//						// Xu ly xoa cung file tren storeFileOnDisk
//						List<String> lstFileName = new ArrayList<String>();
//						for (EquipAttachFile itemFile : objVO.getLstObject()) {
//							if (!StringUtil.isNullOrEmpty(itemFile.getUrl())) {
//								lstFileName.add(itemFile.getUrl());
//							}
//						}
//						FileUtility.deleteStoreFileOnDisk(lstFileName, Configuration.getFileEquipServerDocPath());
//					}
//				}
//			}
//			}
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//			result.put(ERROR, true);
//			result.put("errMsg", errMsg);
//		}
//		
//		return JSON;
//	}

	/**
	 * Chinh sua bien ban giao nhan
	 * 
	 * @author tamvnm
	 * @since 26/06/2015
	 */
	public String updateRecordDelivery() {
		resetToken(result);
		Integer statusRe = 0;
		String errMsg = "";
		Date dateContract = null;
		Date fromDateLend = null;
		Date toDateLend = null;
		Date toBDate = null;
		result.put(ERROR, false);
		try {
			/**
			 * Ra soat bo sung
			 * 
			 * @author hunglm16
			 * @since August 18,2015
			 * */
			if (idRecordDelivery != null) {
				EquipDeliveryRecord edeliveryX = equipmentManagerMgr.getEquipDeliveryRecordById(idRecordDelivery);
				if (edeliveryX != null) {
					if (edeliveryX.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
						//Cap nhat cho giam sat
						if (StringUtil.isNullOrEmpty(staffCode)) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.staffowner.undefined", staffCode));
							return JSON;
						} else {
							if (edeliveryX.getStaff() != null && !StringUtil.isNullOrEmpty(edeliveryX.getStaff().getStaffCode()) && staffCode.trim().toUpperCase().equals(edeliveryX.getStaff().getStaffCode().trim().toUpperCase())) {
								result.put(ERROR, true);
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.staffowner.as.current", staffCode));
								return JSON;
							}
							Staff staff = null;
							staff = staffMgr.getStaffByCode(staffCode);
							if (staff != null) {
								if (staff.getStatus() == null || !ActiveType.RUNNING.getValue().equals(staff.getStatus().getValue())) {
									result.put(ERROR, true);
									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.staffowner.code.status.not.active", staffCode));
									return JSON;
								}
								edeliveryX.setStaff(staff);
								edeliveryX.setUpdateDate(commonMgr.getSysDate());
								edeliveryX.setUpdateUser(currentUser.getUserName());
								commonMgr.updateEntity(edeliveryX);
							} else {
								result.put(ERROR, true);
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.staffowner.undefined", staffCode));
								return JSON;
							}
						}
						result.put(ERROR, false);
						return JSON;
					}
				}
			}
			
			Date now = commonMgr.getSysDate();
			if (statusRecord != null && statusRecord.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
				// Xu ly file dinh kem
				List<Long> lstFileIdDelete = new ArrayList<Long>();
				Integer maxLength = 0;				
				EquipRecordFilter<FileVO> filterFile = new EquipRecordFilter<FileVO>();
				filterFile.setObjectId(idRecordDelivery);
				filterFile.setObjectType(EquipTradeType.DELIVERY.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filterFile).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
				// Xu ly Xoa file dinh kem
				if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
					String[] arrEquipAttachFile = equipAttachFileStr.split(",");
					for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
						String value = arrEquipAttachFile[i];
						if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
							lstFileIdDelete.add(Long.parseLong(value));
						}
					}
				}
				maxLength = lstFileVo.size() - lstFileIdDelete.size();
				if (maxLength < 0) {
					maxLength = 0;
				}
				errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}				
				// luu file tam de xoa tren server
//				ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
//				if (lstFileIdDelete != null && lstFileIdDelete.size() > 0) {
//					BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
//					filterF.setLstId(lstFileIdDelete);
//					filterF.setObjectId(idRecordDelivery);
//					filterF.setObjectType(EquipTradeType.DELIVERY.getValue());
//					objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
//				}
				//filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
				// Luu file dinh kem
				List<FileVO> lstfileVOs = saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType);
				EquipRecordFilter<EquipmentDeliveryVO> filter = new EquipRecordFilter<EquipmentDeliveryVO>();
				filter.setLstFileVo(lstfileVOs);
				filter.setLstEquipAttachFileId(lstFileIdDelete);
				statusRe = StatusRecordsEquip.CANCELLATION.getValue();
				List<EquipDeliveryRecDtl> lstDetailDB = equipmentManagerMgr.getListDeliveryDetailByRecordId(idRecordDelivery);
				List<EquipDeliveryRecDtl> lstDetailDelete = new ArrayList<EquipDeliveryRecDtl>();
				for (int i = 0, isize = lstDetailDB.size(); i < isize; i++) {
					Boolean isDeleted = false;
					for (int j = 0, jsize = lstEquipCode.size(); j < jsize; j++) {
						if (lstDetailDB.get(i).getEquipment().getCode().equals(lstEquipCode.get(j))) {
							isDeleted = true;
							break;
						}
					}
					if (!isDeleted) {
						lstDetailDelete.add(lstDetailDB.get(i));
					}
				}
				
				EquipDeliveryRecord edelivery = equipmentManagerMgr.getEquipDeliveryRecordById(idRecordDelivery);
				edelivery.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
				
				equipmentManagerMgr.updateRecordDeliveryX(edelivery, new ArrayList<EquipDeliveryRecDtl>(), lstDetailDelete, statusRe,filter);
				return JSON;
			}
			if (idRecordDelivery != null) {
				Date cDate = null;
				if (!StringUtil.isNullOrEmpty(createDate)) {
					cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null");
				}
				
				if(!StringUtil.isNullOrEmpty(errMsg)){
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				
				if (StringUtil.isNullOrEmpty(numberContract)) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.customer.code")));
					return JSON;
				} else {
					errMsg = ValidateUtil.validateField(numberContract, "equipment.manager.number.contract", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//					if (StringUtil.isNullOrEmpty(errMsg) && numberContract.indexOf(" ") != -1) {
//						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.format.number.contract.space") + "\n";
//					}
					if(!StringUtil.isNullOrEmpty(errMsg) ){
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
				}
				if (!StringUtil.isNullOrEmpty(contractDate)) {
					dateContract = DateUtil.parse(contractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					if(dateContract == null){
						result.put(ERROR, true);
						result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
						return JSON;
					}
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
					return JSON;
				}
				
				if (!StringUtil.isNullOrEmpty(fromPageDate)) {
					fromDateLend = DateUtil.parse(fromPageDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					if(fromDateLend == null){
						result.put(ERROR, true);
						result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
						return JSON;
					}
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
					return JSON;
				}
				
				if (!StringUtil.isNullOrEmpty(toIdNODate)) {
					toDateLend = DateUtil.parse(toIdNODate, DateUtil.DATE_FORMAT_DDMMYYYY);
					if(toDateLend == null){
						result.put(ERROR, true);
						result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
						return JSON;
					}
				}
				if (!StringUtil.isNullOrEmpty(toBusinessDate)) {
					toBDate = DateUtil.parse(toBusinessDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					if(toBDate == null){
						result.put(ERROR, true);
						result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date")));
						return JSON;
					}
				}
				EquipDeliveryRecord e = equipmentManagerMgr.getEquipDeliveryRecordById(idRecordDelivery);
				if (e != null && statusRecord.equals(StatusRecordsEquip.APPROVED.getValue()) && e.getRecordStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.cancel"));
					return JSON;
				}
				if (StatusRecordsEquip.APPROVED.getValue().equals(e.getRecordStatus())) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equiment.manager.status"));
					return JSON;
				}
				if (!e.getRecordStatus().equals(StatusRecordsEquip.APPROVED)) {
					//tamvnm: update dac ta moi. 30/06/2015
					//tamvnm: thay doi createFormDate thanh contractDate
//					List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(dateContract);
//					EquipPeriod equipPeriod = null;
//					if (lstPeriod == null || lstPeriod.size() == 0) {
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.contract.not.in"));
//						result.put(ERROR, true);
//						return JSON;
//					} else if (lstPeriod.size() > 0) {
//						// gan gia tri ky dau tien.
//						equipPeriod = lstPeriod.get(0);
//					}
//					e.setEquipPeriod(equipPeriod);
					e.setCreateFormDate(cDate);
					if (!StringUtil.isNullOrEmpty(numberContract) && !numberContract.equals(e.getContractNumber())) {
						String errMsg1 = ValidateUtil.validateField(numberContract, "equipment.manager.number.contract", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(errMsg1) && equipRecordMgr.isExistContractNumInDeliveryRecord(numberContract)) {
							errMsg1 = R.getResource("equipment.manager.delivery.contract.number.is.exist");
						}
						if (!StringUtil.isNullOrEmpty(errMsg1)) {
							result.put(ERROR, true);
							result.put("errMsg", errMsg1);
							return JSON;
						}
					}
					e.setContractNumber(numberContract);
					e.setContractCreateDate(dateContract);
					
					//Ben cho muon
					if (!StringUtil.isNullOrEmpty(fromObjectAddress)) {
						e.setFromObjectAddress(fromObjectAddress);
					}
					if (!StringUtil.isNullOrEmpty(fromObjectTax)) {
						e.setFromObjectTax(fromObjectTax);
					}
					if (!StringUtil.isNullOrEmpty(fromObjectPhone)) {
						e.setFromObjectPhone(fromObjectPhone);
					}
					if (!StringUtil.isNullOrEmpty(fromRepresentative)) {
						e.setFromRepresentative(fromRepresentative);
					}
					if (!StringUtil.isNullOrEmpty(fromObjectPosition)) {
						e.setFromPosition(fromObjectPosition);
					}
					if (!StringUtil.isNullOrEmpty(fromFax)) {
						e.setFromFax(fromFax);
					}
					if (!StringUtil.isNullOrEmpty(fromPage)) {
						e.setFromPage(fromPage);
					}
					if (!StringUtil.isNullOrEmpty(fromPagePlace)) {
						e.setFromPagePlace(fromPagePlace);
					}
					e.setFromPageDate(fromDateLend);
					//Ben muon
					
					if (!StringUtil.isNullOrEmpty(toObjectAddress)) {
						e.setToObjectAddress(toObjectAddress);
					}
					//Dia chi thuong tru
					e.setToPermanentAddress(toPermanentAddress);
					//dia chi dat tu
					e.setAddress(addressName);
					e.setStreet(street);
					e.setWardName(wardName);
					e.setProvinceName(provinceName);
					e.setDistrictName(districtName);
					
					String objectAddress = "";
					if (addressName != null ) {
						objectAddress += addressName + " ";
					}
					if (street != null ) {
						objectAddress += street + ", ";
					}
					if (wardName != null ) {
						objectAddress += wardName + ", ";
					}
					if (districtName != null ) {
						objectAddress += districtName  + ", ";
					}
					if (provinceName != null ) {
						objectAddress += provinceName;
					}
					
					if (objectAddress.length() > 0) {
						e.setToObjectAddress(objectAddress);
					}

					
					if (!StringUtil.isNullOrEmpty(toRepresentative)) {
						e.setToRepresentative(toRepresentative);
					}
					if (!StringUtil.isNullOrEmpty(toObjectPosition)) {
						e.setToPosition(toObjectPosition);
					}
					e.setToIdNO(toIdNO);
					e.setToIdNOPlace(toIdNOPlace);
					e.setToBusinessLincense(toBusinessLicense);
					e.setToBusinessPlace(toBusinessPlace);
					e.setToIdNODate(toDateLend);
					e.setToBusinessDate(toBDate);
				
					// cam ket doanh so
					e.setFreezer(freezer);
					e.setRefrigerator(refrigerator);
					
					//ghi chu
					e.setNote(note);
				}
				
				Staff staff = null;
				if(!StringUtil.isNullOrEmpty(staffCode)) {
					staff = staffMgr.getStaffByCode(staffCode);
					if(staff != null) {
						e.setStaff(staff);
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, String.format(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.staffowner.code.status.not.active"), staffCode)));
						return JSON;
					}
				}
				
				/**bo check shop ben cho muon*/
				if (!StringUtil.isNullOrEmpty(fromShopCode)) {
					e.setFromObjectName(fromShopCode);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, String.format(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.shop.status.not.active"), fromShopCode)));
					return JSON;
				}
				
				Shop toShop = null;
				if (!StringUtil.isNullOrEmpty(toShopCode)) {
					toShopCode = toShopCode.toUpperCase();
					toShop = shopMgr.getShopByCode(toShopCode);
					
					if(toShop == null) {
						errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"ss.traningplan.npp.code");
					} else if (!checkShopInOrgAccessByCode(toShopCode)) {
						errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
					} else if(!toShop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
						errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
					} else if (toShop.getStatus().equals(ActiveType.STOPPED)) {
						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.not.running.shop");
					}
					
					if(toShop != null) {
						if(e.getToShop() != null && toShop.getId() != e.getToShop().getId() && customerId == null) {
							e.setCustomer(null);
						}
						if(e.getToShop() != null &&  toShop.getId() != e.getToShop().getId() && staff == null) {
							e.setStaff(null);
						}
						if (!e.getRecordStatus().equals(StatusRecordsEquip.APPROVED)) {
							e.setToShop(toShop);
							e.setToObjectName(toShop.getShopName());
						}
						if(toShop.getType().getSpecificType().equals(ShopSpecificType.NPP) 
//								|| toShop.getType().getObjectType().equals(ShopObjectType.MIEN_ST)
							|| toShop.getType().getSpecificType().equals(ShopSpecificType.NPP_MT)
							) {
							//dieu kien dung
						} else {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
						}
					    
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, String.format(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "Mã đơn vị {0} đang ở trạng thái không hoạt động."), toShopCode)));
						return JSON;
					}
				}
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				if(customerId != null) {
					Customer cus = commonMgr.getEntityById(Customer.class, customerId);
					if(cus != null) {
						if (!e.getRecordStatus().equals(StatusRecordsEquip.APPROVED)) {
							e.setCustomer(cus);
							e.setToAddress(cus.getAddress());
							if (cus.getPhone() != null && cus.getMobiphone() != null) {
								e.setToPhone(cus.getPhone() + "/" + cus.getMobiphone());
							} else if (cus.getPhone() != null && cus.getMobiphone() == null) {
								e.setToPhone(cus.getPhone());
							} else if (cus.getPhone() == null && cus.getMobiphone() != null) {
								e.setToPhone(cus.getMobiphone());
							}
						}
						if (toShop != null && cus.getShop().getId() != null &&  !cus.getShop().getId().equals(toShop.getId())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.customer.not.npp")));
							return JSON;
						} 
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.customer.not.exist.shop")));
						return JSON;
					}
					
				}
				if (e != null && statusRecord != null) {
					// du thao
					if (e.getRecordStatus() != null && e.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
						e.setUpdateDate(DateUtil.now());
						e.setUpdateUser(currentUser.getUserName());						
						// duyet
						if (statusRecord.equals(StatusRecordsEquip.APPROVED.getValue())) {
							// trang thai tu du thao -> duyet
							statusRe = StatusRecordsEquip.APPROVED.getValue();
							e.setRecordStatus(statusRecord);
						} else if (statusRecord.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
							// trang thai tu du thao -> huy
							statusRe = StatusRecordsEquip.CANCELLATION.getValue();
							e.setRecordStatus(statusRecord);
						}
					}
				}
				if (e != null && statusDelivery != null) {
					// neu trang thai bien ban la duyet thi cho cap nhat
					// giao nhan
					if (e.getRecordStatus() != null && e.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
						if (e.getDeliveryStatus() != null && (statusDelivery - e.getDeliveryStatus() - 1 == 0)) {
							e.setDeliveryStatus(statusDelivery);
							if (statusRe == 0) {
								e.setUpdateDate(DateUtil.now());
								e.setUpdateUser(currentUser.getUserName());
								statusRe = -1;
							} else if (statusRe.equals(StatusRecordsEquip.APPROVED.getValue())) {
								statusRe = -2;
							}
						}
					}
				}
				List<String> lstDelete = new ArrayList<String>();
				if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
					String[] lst = lstEquipDelete.toString().split(",");
					for (int i = 0; i < lst.length; i++) {
						lstDelete.add(lst[i]);
					}
				}
				
				//Kiem tra thiet bi trung.
				for (int i = 0; lstEquipCode != null && i < lstEquipCode.size(); i++) {
					String code = lstEquipCode.get(i);
					for (int j = i + 1, jsize = lstEquipCode.size(); j < jsize; j++)  {
						if (!code.equals("") && code.equals(lstEquipCode.get(j))) {
							result.put(ERROR, true);
							result.put("errMsg", "Mã TB " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau") + "\n");
							return JSON;
						}
					}
				}
				
				// kiem tra chi tiet bien ban
				List<EquipDeliveryRecDtl> equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
				List<EquipDeliveryRecDtl> lstDetailDB = equipmentManagerMgr.getListDeliveryDetailByRecordId(idRecordDelivery);
				for (int i = 0; lstContentDelivery!=null && i < lstContentDelivery.size(); i++) {
					Boolean isFound = false;
					EquipDeliveryRecDtl detailDB = new EquipDeliveryRecDtl();
					for (int j = 0, jsize = lstDetailDB.size(); j < jsize; j++) {
						if (lstDetailDB.get(j).getEquipment().getCode().equals(lstEquipCode.get(i))) {
							isFound = true;
							detailDB = lstDetailDB.get(j);
						}
					}
					if (isFound) {
						if (detailDB.getEquipment().getSerial() == null && !lstSeriNumber.get(i).equals("")) {
							detailDB.getEquipment().setSerial(lstSeriNumber.get(i));
						}
						equipmentDeliveryRecordDetails.add(detailDB);
						continue;
					}
					
					
					//lay danh sach kho NPP
					List<EquipStockVO> lstStocks = new ArrayList<EquipStockVO>();
					if (toShop != null) {
						lstStocks = getListStockShop(toShop.getShopCode());
					}
					
					EquipDeliveryRecDtl equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
					EquipStock equipStock = null;
					// Neu la cap moi.
					if (lstContentDelivery.get(i).equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
						Equipment equipment = new Equipment();
						equipment.setUsageStatus(null);
						//Nhom san pham
						if (StringUtil.isNullOrEmpty(errMsg) && lstGroup != null) {
							EquipLend el = e.getEquipLend();
							if (el != null) {
								EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
								equipmentFilter.setkPaging(null);
								equipmentFilter.setEquipLendCode(el.getCode());
								if (e.getCustomer() != null) {
									equipmentFilter.setCustomerId(e.getCustomer().getId());
								}
								
								equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
								ObjectVO<EquipmentVO> eGroup = equipmentManagerMgr.searchEquipmentGroupVObyFilter(equipmentFilter);
								
								if (eGroup.getLstObject() != null && eGroup.getLstObject().size() > 0 ) {
									List<EquipmentVO> lstEquipGroup = eGroup.getLstObject();
									EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupByCode(lstGroup.get(i));
									if (equipGroup != null) {
										Boolean isTrueGroup = false;
										for (int j = 0, jsize = lstEquipGroup.size(); j < jsize; j++) {
											if (lstEquipGroup.get(j).getCode().equals(equipGroup.getCode())) {
												isTrueGroup = true;
												break;
											}
										}
										
										if (!isTrueGroup) {
											errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.lend.undefined");
										} else if (!ActiveType.RUNNING.getValue().equals(equipGroup.getStatus().getValue())) {
											errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon");
										} else {
											equipment.setEquipGroup(equipGroup);
										}
									} else {
										errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined");
									}
								} else {
									errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.lend.undefined");
								}
							} else {
								EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupByCode(lstGroup.get(i));
								if (equipGroup != null) {
									if (!ActiveType.RUNNING.getValue().equals(equipGroup.getStatus().getValue())) {
										errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon");
									} else {
										equipment.setEquipGroup(equipGroup);
									}
								} else {
									errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined");
								}
							}
						} 
						
						//Nha cung cap
						if (StringUtil.isNullOrEmpty(errMsg) && lstProvider != null) {
							EquipProvider equipProvider = equipmentManagerMgr.getEquipProviderByCode(lstProvider.get(i));
							if (equipProvider != null) {
								if (!ActiveType.RUNNING.getValue().equals(equipProvider.getStatus().getValue())) {
									errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.unon");
								} else {
									equipment.setEquipProvider(equipProvider);
								}
							} else {
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.undefined");
							}
						} 
						
						//Kho
						if (StringUtil.isNullOrEmpty(errMsg) && lstStock != null) {
							equipStock = equipmentManagerMgr.getEquipStockbyCode(lstStock.get(i));
							if (equipStock != null) {
								if (!ActiveType.RUNNING.getValue().equals(equipStock.getStatus().getValue())) {
									errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.stop") + "\n";
								} else {
									Boolean isTrueStock = false;
									isTrueStock = checkStockIsValid(lstStocks, equipStock);
									if (!isTrueStock) {
										errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null");
									} else {
										equipment.setStockId(equipStock.getId());
										equipment.setStockCode(equipStock.getCode());
										equipment.setStockName(equipStock.getName());
										equipment.setStockType(EquipStockTotalType.KHO);
									}
								}
							} else {
								errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null");
							}
						} else {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null");
						}
						
						//Gia san pham
						if (StringUtil.isNullOrEmpty(errMsg) && lstEquipPrice != null) {
							BigDecimal price = lstEquipPrice.get(i);
							if (price.doubleValue() < 1) {
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.price");
							} else {
								equipment.setPrice(price);
							}
						} 

						//serial
						if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(lstSeriNumber.get(i))) {
							equipment.setSerial(lstSeriNumber.get(i));
						}

						//Trang thai su dung
						equipment.setStatus(StatusType.ACTIVE);
						equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
						equipment.setTradeType(EquipTradeType.DELIVERY);
						equipment.setCreateDate(now);
						equipment.setCreateUser(currentUser.getUserName());
						
						//Trang thai thiet bi
						if (StringUtil.isNullOrEmpty(errMsg) && lstHealth != null) {
							String healthStatus = lstHealth.get(i);
							List<ApParam> lstAppram = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
							boolean flagHs = false;
							for (ApParam row : lstAppram) {
								if (healthStatus.equals(row.getApParamCode())) {
									flagHs = true;
									break;
								}
							}
							if (!flagHs) {
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde");
							} else {
								equipment.setHealthStatus(healthStatus);
							}
						} 
						
						//Nam san xuat
						if (StringUtil.isNullOrEmpty(errMsg) && lstManufacturingYear != null) {
							Integer manufacturingYear = lstManufacturingYear.get(i);
							int curYear = DateUtil.getYear(now);
							if (manufacturingYear == null) {
								errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.manufacturingYear") + "\n";
							} else if (manufacturingYear.intValue() < 1 || manufacturingYear.intValue() > curYear) {
								errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.manufacturingYear") + "\n";
							} else {
								equipment.setManufacturingYear(manufacturingYear);
							}
						}
						
						if (errMsg.length() > 0) {
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						
						// thong tin chi tiet bien ban
						equipmentDeliveryRecDetail.setEquipment(equipment);
						equipmentDeliveryRecDetail.setContent(lstContentDelivery.get(i));
						equipmentDeliveryRecDetail.setCreateDate(now);
						equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
						if (equipStock != null) {
							equipmentDeliveryRecDetail.setFromStockId(equipStock.getId());
						}
						if (lstDepreciation != null && lstDepreciation.get(i) != null) {
							equipmentDeliveryRecDetail.setDepreciation(lstDepreciation.get(i));
						}
						equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
					} else {
						Equipment equipment = null;
						equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
						
						if (equipment != null) {
							Equipment equipValidate = validateEquipment(equipment.getCode());
							if (equipValidate == null) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.role") + "\n";
							} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " đang ở trạng thái không hoạt động";
							} else if (EquipStockTotalType.KHO_KH.getValue().equals(equipment.getStockType().getValue())) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock") + "\n";
							} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.usage.status") + "\n";
							} else if (EquipTradeStatus.TRADING.getValue().equals(equipment.getTradeStatus()) && equipment.getSerial() != null && !lstSeriNumber.get(i).equals("")) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.trade.status") + "\n";
							} else 
//								if (EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
//								if (equipment.getStockType().getValue().equals(EquipStockTotalType.KHO_NPP.getValue()) && staff != null && !staff.getShop().getShopCode().equals(equipment.getStockCode())) {
//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.npp") + "\n";
//								}
//								EquipStockTotal equipStockTotal = equipmentManagerMgr.getEquipStockTotalById(equipment.getStockId(), EquipStockTotalType.parseValue(equipment.getStockType().getValue()), equipment.getEquipGroup().getId());
//								if(equipStockTotal == null || equipStockTotal.getQuantity() <= 0){
//									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.stock.invalid") + "\n";
//								}
//							}
							if (equipment.getSerial() == null && lstContentDelivery.get(i) != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(lstContentDelivery.get(i))) {
//								Khong rang buoc so seri khi cap moi
//								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content");
//							}
//							else if (StringUtil.isNullOrEmptyNotTrim(equipment.getSerial()) && StringUtil.isNullOrEmptyNotTrim(lstSeriNumber.get(i))) {
//								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.serial");
							} else if(equipment.getSerial() == null) {
								if (!StringUtil.isNullOrEmpty(lstSeriNumber.get(i)) && !ValidateUtil.validateFormatSerial(lstSeriNumber.get(i))) {
									errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.seri.format");
								} else if(StringUtil.isNullOrEmpty(errMsg) && lstSeriNumber.get(i).indexOf(" ") != -1) {
									errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial") + "\n";
								}
							}	
							if (StringUtil.isNullOrEmpty(equipment.getSerial()) && lstSeriNumber != null && !StringUtil.isNullOrEmpty(lstSeriNumber.get(i))) {
								equipment.setSerial(lstSeriNumber.get(i));
							}
							if (errMsg.length() > 0) {
								result.put(ERROR, true);
								result.put("errMsg", errMsg);
								return JSON;
							}
							equipmentDeliveryRecDetail.setEquipment(equipment);
						} else {
							result.put(ERROR, true);
							result.put("errMsg", "Không tìm thấy thiết bị có mã " + lstEquipCode.get(i));
							return JSON;
						}

						// thong tin chi tiet bien ban
						equipmentDeliveryRecDetail.setContent(lstContentDelivery.get(i));
						equipmentDeliveryRecDetail.setCreateDate(DateUtil.now());
						equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
						equipmentDeliveryRecDetail.setFromStockId(equipment.getStockId());
						if (lstDepreciation != null && lstDepreciation.get(i) != null) {
							equipmentDeliveryRecDetail.setDepreciation(lstDepreciation.get(i));
						}
						equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
					}
				}
				e.setUpdateDate(DateUtil.now());
				e.setUpdateUser(currentUser.getUserName());
				
				
				//xu ly thiet bi da bi xoa
//				List<EquipDeliveryRecDtl> lstDetailDB = equipmentManagerMgr.getListDeliveryDetailByRecordId(idRecordDelivery);
				List<EquipDeliveryRecDtl> lstDetailDetele = new ArrayList<EquipDeliveryRecDtl>();
				Boolean isFound = false;
				for (int c = 0, csize = lstDetailDB.size(); c < csize; c++) {
					for (int n = 0, nsize = equipmentDeliveryRecordDetails.size(); n < nsize; n++) {
						if (lstDetailDB.get(c).getEquipment().getCode().equals(equipmentDeliveryRecordDetails.get(n).getEquipment().getCode())) {
							isFound = true;
							break;
						}
					}
					if (!isFound) {
						lstDetailDetele.add(lstDetailDB.get(c));
					}
					isFound = false;
				}
				
				/** Xu ly truong hop xoa het thiet bi van cap nhat dc bien ban*/
				EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
				equipmentFilter.setkPaging(null);
				if (idRecordDelivery != null) {
					equipmentFilter.setIdRecordDelivery(idRecordDelivery);
				}
				ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentDeliveryVOByFilter(equipmentFilter);
				if (equipmentDeliverys != null) {
					List<EquipmentDeliveryVO> lstEquipDetail = equipmentDeliverys.getLstObject();
					if (lstEquipDetail != null && lstEquipDetail.size() == lstDelete.size() && equipmentDeliveryRecordDetails.size() == 0) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.detail.null") + "\n";
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
				}
				
				// Xu ly file dinh kem
				List<Long> lstFileIdDelete = new ArrayList<Long>();
				Integer maxLength = 0;				
				EquipRecordFilter<FileVO> filterFile = new EquipRecordFilter<FileVO>();
				filterFile.setObjectId(idRecordDelivery);
				filterFile.setObjectType(EquipTradeType.DELIVERY.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filterFile).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
				// Xu ly Xoa file dinh kem
				if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
					String[] arrEquipAttachFile = equipAttachFileStr.split(",");
					for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
						String value = arrEquipAttachFile[i];
						if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
							lstFileIdDelete.add(Long.parseLong(value));
						}
					}
				}
				maxLength = lstFileVo.size() - lstFileIdDelete.size();
				if (maxLength < 0) {
					maxLength = 0;
				}
				errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}				
				// luu file tam de xoa tren server
				ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
				if (lstFileIdDelete != null && lstFileIdDelete.size() > 0) {
					BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
					filterF.setLstId(lstFileIdDelete);
					filterF.setObjectId(idRecordDelivery);
					filterF.setObjectType(EquipTradeType.DELIVERY.getValue());
					objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
				}
				//filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
				// Luu file dinh kem
				List<FileVO> lstfileVOs = saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType);
				EquipRecordFilter<EquipmentDeliveryVO> filter = new EquipRecordFilter<EquipmentDeliveryVO>();
				filter.setLstFileVo(lstfileVOs);
				filter.setLstEquipAttachFileId(lstFileIdDelete);
//				equipmentManagerMgr.updateRecordDelivery(e, equipmentDeliveryRecordDetails, lstDelete, statusRe, filter);
				equipmentManagerMgr.updateRecordDeliveryX(e, equipmentDeliveryRecordDetails, lstDetailDetele, statusRe, filter);
				if (e.getEquipLend() != null) {
					//Tim nhung bien ban cua EquipLend da huy hoac da duyet - > cap nhat thanh HOAN TAT
					EquipLend el = equipmentManagerMgr.getEquipLendById(e.getEquipLend().getEquipLendId());
					if (el != null) {
						el.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
						equipProposalBorrowMgr.updateEquipLend(el);
					}	
				}
				// xoa file tren server
				if (lstFileIdDelete != null && lstFileIdDelete.size() > 0 && objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
					// Xu ly xoa cung file tren storeFileOnDisk
					List<String> lstFileName = new ArrayList<String>();
					for (EquipAttachFile itemFile : objVO.getLstObject()) {
						if (!StringUtil.isNullOrEmpty(itemFile.getUrl())) {
							lstFileName.add(itemFile.getUrl());
						}
					}
					FileUtility.deleteStoreFileOnDisk(lstFileName, Configuration.getFileEquipServerDocPath());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Xu ly kiem tra stock thuoc quyen
	 * 
	 * @author tamvnm
	 * @param lstStocks - danh sach kho
	 * @param equipStock - kho can kiem tra
	 * @return dung: true, else sai
	 * @since Nov 25, 2015
	*/
	private Boolean checkStockIsValid(List<EquipStockVO> lstStocks, EquipStock equipStock) {
		for (int i = 0, isize = lstStocks.size(); i < isize; i++) {
			if (lstStocks.get(i).getEquipStockId().equals(equipStock.getId())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Lay danh sach kho NPP
	 * @author tamvnm
	 * @param shopCode
	 * @return list danh sach kho npp
	 * @since Oct 21 2015
	 */
	public List<EquipStockVO> getListStockShop(String shopCode) {
		Date startLogDate = DateUtil.now();
		List<EquipStockVO> lstStocks = new ArrayList<EquipStockVO>();
		try {
			EquipStockFilter filter = new EquipStockFilter();
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setToShopCode(shopCode);
			}
			filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			filter.setShopRoot(currentUser.getShopRoot().getShopId());
			ObjectVO<EquipStockVO> objVO = equipmentManagerMgr.getListEquipStockVOByRole(filter);
			if (objVO != null && objVO.getLstObject() != null) {
				lstStocks = objVO.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "vnm.web.action.ManageDeliveryEquipmentAction.getEquipmentStockCombobox"), createLogErrorStandard(startLogDate));
		}
		
		return lstStocks;
	}
	
	
	
	/**
	 * Tao moi bien ban
	 * 
	 * @author tamvnm
	 * @since 26/06/2015
	 * @return
	 */
	public String createRecordDelivery() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		Date dateContract = null;
		Date fromDatePage = null;
		Date toDateIdNO = null;
		Date toBDate = null;
		Shop toShop = null;
		try {
			Date now = commonMgr.getSysDate();
			EquipDeliveryRecord equipDeliveryRecord = new EquipDeliveryRecord();
			/** bo check shop ben cho muon*/
			if (!StringUtil.isNullOrEmpty(fromShopCode)) {
//				fromShop = shopMgr.getShopByCode(fromShopCode);
//				
//				if(fromShop == null ){
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"ss.traningplan.npp.code");
//				}else if(!fromShop.getType().getObjectType().equals(ShopObjectType.NPP.getValue()) && !fromShop.getShopCode().equals("VNM")){
//					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
//				}
//				else if (!checkShopInOrgAccessByCode(fromShopCode)) {
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
//				}
//				else if(!fromShop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
//				} 
//				if (fromShop != null && fromShop.getShopCode().equals("VNM")) {
//					equipDeliveryRecord.setFromFax(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.vnm.fax"));
//				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.fromShop")));
				return JSON;
			}
			
			Date cDate = null;
			if (!StringUtil.isNullOrEmpty(createDate)) {
				cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null");
			}
			
			if(!StringUtil.isNullOrEmpty(errMsg)){
				result.put("errMsg", errMsg);
				result.put(ERROR, error);
				return JSON;
			}
			
			
			if (!StringUtil.isNullOrEmpty(toShopCode)) {
				toShopCode = toShopCode.toUpperCase();
				toShop = shopMgr.getShopByCode(toShopCode);
				if(toShop == null) {
					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"ss.traningplan.npp.code");
				} else if (!checkShopInOrgAccessByCode(toShopCode)) {
					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
				} else if(!toShop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
				} else if (toShop.getStatus().equals(ActiveType.STOPPED)) {
					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.not.running.shop");
				}
				
				if (toShop != null) {
					if(ShopSpecificType.NPP.equals(toShop.getType().getSpecificType()) 
							|| ShopSpecificType.NPP_MT.equals(toShop.getType().getSpecificType())
//						|| toShop.getType().getObjectType().equals(ShopObjectType.NPP_KA.getValue())
						) {
						//dieu kien dung
					} else {
						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
					}
				}
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.traningplan.npp.code")));
				return JSON;
			}		
			
			Staff staff = null;
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				staff = staffMgr.getStaffByCode(staffCode);
				if (staff == null || staff.getShop() == null) {
					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.staff.code");
				} else if (!ActiveType.RUNNING.equals(staff.getStatus())) {
					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.staff.code");
//				} else if (toShop != null) {
//					if (staff.getShop().getId() != toShop.getId()) {
//						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.staff.not.npp");
//					} 
//					if (staff.getStaffType() != null && staff.getStaffType().getObjectType() != StaffObjectType.NVGS.getValue()) {
//						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.staff.not.gs");
//					}
				} 

				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipDeliveryRecord.setStaff(staff);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.staff.code")));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(customerCode) && toShop != null) {
				ShopToken shopVo = null;
				if (checkShopInOrgAccessByCode(toShopCode)) {
					shopVo = getShopTockenInOrgAccessByCode(toShopCode);
				}
				Customer customer = null;
				if (shopVo != null && (shopVo.getObjectType().equals(ShopSpecificType.NPP.getValue()) 
						|| shopVo.getObjectType().equals(ShopSpecificType.NPP_MT.getValue()))) {
					customerCode = customerCode.trim().toLowerCase();
					customer = customerMgr.getCustomerMultiChoine(null, shopVo.getShopId(), customerCode, null);
				} else {
					customerCode = customerCode.trim().toLowerCase();
					customerCode = StringUtil.getFullCode(toShop.getShopCode(), customerCode);
					customer = customerMgr.getCustomerByCode(customerCode);
				}

				
				//List<Customer> customers = customerMgr.getListCustomerByShortCode(shop.getId(), customerCode);
				if(customer == null ){
					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "pay.period.code.checked.customer.shop") + "\n";
					//errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_IN_SHOP, true, "equipment.manager.customer.code");
				}
//				else if (!ActiveType.RUNNING.getValue().equals(customer.getStatus().getValue())) {
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.customer.code");
//					
//				} 
				else if (toShop != null && customer != null && !customer.getShop().getId().equals(toShop.getId())) {
					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.customer.not.npp");
				}
				
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipDeliveryRecord.setCustomer(customer);
				equipDeliveryRecord.setToAddress(customer.getAddress());
				equipDeliveryRecord.setToShop(toShop);
				equipDeliveryRecord.setToObjectName(toShop.getNameText());
				if (customer.getPhone() != null && customer.getMobiphone() != null) {
					equipDeliveryRecord.setToPhone(customer.getPhone() + "/" + customer.getMobiphone());
				} else if (customer.getPhone() != null && customer.getMobiphone() == null) {
					equipDeliveryRecord.setToPhone(customer.getPhone());
				} else if (customer.getPhone() == null && customer.getMobiphone() != null) {
					equipDeliveryRecord.setToPhone(customer.getMobiphone());
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.customer.code")));
				return JSON;
			}
			
//			if (fromShop != null) {
//				equipDeliveryRecord.setFromShop(fromShop);
//				equipDeliveryRecord.setFromObjectName(fromShop.getNameText());
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.customer.code")));
//				return JSON;
//			}
			
			if (!StringUtil.isNullOrEmpty(numberContract)) {
				errMsg = ValidateUtil.validateField(numberContract, "equipment.manager.number.contract", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//				if (StringUtil.isNullOrEmpty(errMsg) && numberContract.indexOf(" ") != -1) {
//					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.format.number.contract.space") + "\n";
//				}
				if (StringUtil.isNullOrEmpty(errMsg) && equipRecordMgr.isExistContractNumInDeliveryRecord(numberContract)) {
					errMsg = R.getResource("equipment.manager.delivery.contract.number.is.exist");
				}
				if(!StringUtil.isNullOrEmpty(errMsg) ){
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipDeliveryRecord.setContractNumber(numberContract);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.customer.code")));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(contractDate)) {
				dateContract = DateUtil.parse(contractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
//				if (dateContract != null && DateUtil.compareDateWithoutTime(dateContract, now) != 1) {
				//tamvnm: 07/07/2015 bo check ngay hop dong nho hon hoac bang ngay hien tai
				if (dateContract != null) {
					equipDeliveryRecord.setContractCreateDate(dateContract);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.date.contract.invalid"));
					return JSON;
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.date.contract")));
				return JSON;
			}
			equipDeliveryRecord.setRecordStatus(statusRecord);
			equipDeliveryRecord.setDeliveryStatus(statusDelivery);
			equipDeliveryRecord.setFromObjectName(fromShopCode);
			if (!StringUtil.isNullOrEmpty(fromObjectAddress)) {
				equipDeliveryRecord.setFromObjectAddress(fromObjectAddress);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.fromAddress")));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(fromObjectPhone)) {
				equipDeliveryRecord.setFromObjectPhone(fromObjectPhone);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.fromPhone")));
				return JSON;
			}
			equipDeliveryRecord.setFromObjectTax(fromObjectTax);
			equipDeliveryRecord.setFromPage(fromPage);
			if (!StringUtil.isNullOrEmpty(fromPageDate)) {
				fromDatePage = DateUtil.parse(fromPageDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (fromDatePage != null && DateUtil.compareDateWithoutTime(fromDatePage, now) != 1) {
					equipDeliveryRecord.setFromPageDate(fromDatePage);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idNO.fromPageDate.big")));
					return JSON;
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idNO.fromPageDate")));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(toIdNODate)) {
				toDateIdNO = DateUtil.parse(toIdNODate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (toDateIdNO != null && DateUtil.compareDateWithoutTime(toDateIdNO, now) != 1) {
					equipDeliveryRecord.setToIdNODate(toDateIdNO);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idNO.dateCreate")));
					return JSON;
				}
				
			}
			if (!StringUtil.isNullOrEmpty(toBusinessDate)) {
				toBDate = DateUtil.parse(toBusinessDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (toBDate != null && DateUtil.compareDateWithoutTime(toBDate, now) != 1) {
					equipDeliveryRecord.setToBusinessDate(toBDate);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idDKKD.dateCreate")));
					return JSON;
				}
				
			}
			equipDeliveryRecord.setFromPagePlace(fromPagePlace);
			equipDeliveryRecord.setFromPosition(fromObjectPosition);
			equipDeliveryRecord.setFromRepresentative(fromRepresentative);
			equipDeliveryRecord.setFromFax(fromFax);
			equipDeliveryRecord.setToPosition(toObjectPosition);
			equipDeliveryRecord.setToRepresentative(toRepresentative);
			equipDeliveryRecord.setToIdNO(toIdNO);
			equipDeliveryRecord.setToIdNOPlace(toIdNOPlace);
			equipDeliveryRecord.setToBusinessLincense(toBusinessLicense);
			equipDeliveryRecord.setToBusinessPlace(toBusinessPlace);
			//Dia chi thuong tru
			equipDeliveryRecord.setToPermanentAddress(toPermanentAddress);
			//dia chi dat tu
			equipDeliveryRecord.setAddress(addressName);
			equipDeliveryRecord.setStreet(street);
			equipDeliveryRecord.setWardName(wardName);
			equipDeliveryRecord.setProvinceName(provinceName);
			equipDeliveryRecord.setDistrictName(districtName);
			
			String objectAddress = "";
			if (addressName != null ) {
				objectAddress += addressName + " ";
			}
			if (street != null ) {
				objectAddress += street + ", ";
			}
			if (wardName != null ) {
				objectAddress += wardName + ", ";
			}
			if (districtName != null ) {
				objectAddress += districtName  + ", ";
			}
			if (provinceName != null ) {
				objectAddress += provinceName;
			}
			
			if (objectAddress.length() > 0) {
				equipDeliveryRecord.setToObjectAddress(objectAddress);
			}
			

			if(freezer != null) {
				equipDeliveryRecord.setFreezer(freezer);
			}
			
			if(refrigerator != null) {
				equipDeliveryRecord.setRefrigerator(refrigerator);
			}
			
			//ghi chu
			equipDeliveryRecord.setNote(note);
			
			//Kiem tra thiet bi trung.
			for (int i = 0; lstEquipCode != null && i < lstEquipCode.size(); i++) {
				String code = lstEquipCode.get(i);
				for (int j = i + 1, jsize = lstEquipCode.size(); j < jsize; j++)  {
					if (!code.equals("") && code.equals(lstEquipCode.get(j))) {
						result.put(ERROR, true);
						result.put("errMsg", "Mã TB " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau") + "\n");
						return JSON;
					}
				}
			}
			
			//lay danh sach kho NPP
			List<EquipStockVO> lstStocks = new ArrayList<EquipStockVO>();
			if (toShop != null) {
				lstStocks = getListStockShop(toShop.getShopCode());
			}
			
			// kiem tra chi tiet bien ban
			List<EquipDeliveryRecDtl> equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
			for (int i = 0; lstContentDelivery!=null && i < lstContentDelivery.size(); i++) {
				
				EquipDeliveryRecDtl equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
				EquipStock equipStock = null;
				// Neu la cap moi.
				if (lstContentDelivery.get(i).equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
					Equipment equipment = new Equipment();
					equipment.setUsageStatus(null);
					//Nhom san pham
					if (StringUtil.isNullOrEmpty(errMsg) && lstGroup != null) {
						EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupByCode(lstGroup.get(i));
						if (equipGroup != null) {
							if (!ActiveType.RUNNING.getValue().equals(equipGroup.getStatus().getValue())) {
								errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon") + "\n";
							} else {
								equipment.setEquipGroup(equipGroup);
							}
						} else {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined")+ "\n";
						}
					} else {
						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined")+ "\n";
					}
					//Nha cung cap
					if (StringUtil.isNullOrEmpty(errMsg) && lstProvider != null) {
						EquipProvider equipProvider = equipmentManagerMgr.getEquipProviderByCode(lstProvider.get(i));
						if (equipProvider != null) {
							if (!ActiveType.RUNNING.getValue().equals(equipProvider.getStatus().getValue())) {
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.running")+ "\n";
							} else {
								equipment.setEquipProvider(equipProvider);
							}
						} else {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.undefined")+ "\n";
						}
					} else {
						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.running")+ "\n";
					}
					
					//Kho
					if (StringUtil.isNullOrEmpty(errMsg) && lstStock != null) {
						equipStock = equipmentManagerMgr.getEquipStockbyCode(lstStock.get(i));
						if (equipStock != null) {
							if (!ActiveType.RUNNING.getValue().equals(equipStock.getStatus().getValue())) {
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.stop") + "\n";
							} else {
								Boolean isTrueStock = false;
								isTrueStock = checkStockIsValid(lstStocks, equipStock);
								if (!isTrueStock) {
									errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
								} else {
									equipment.setStockId(equipStock.getId());
									equipment.setStockCode(equipStock.getCode());
									equipment.setStockName(equipStock.getName());
									equipment.setStockType(EquipStockTotalType.KHO);
								}

							}
						} else {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
						}
					} else {
						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.null") + "\n";
					}
					
					if (StringUtil.isNullOrEmpty(errMsg) && lstEquipPrice != null) {
						//Gia san pham
						BigDecimal price = lstEquipPrice.get(i);
						if (price.doubleValue() < 1) {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.price")+ "\n";
						} else {
							equipment.setPrice(price);
//							equipment.setPriceActually(price);
						}
					} 

					//serial
					if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(lstSeriNumber.get(i))) {
						equipment.setSerial(lstSeriNumber.get(i));
					}

					//Trang thai su dung
					equipment.setStatus(StatusType.ACTIVE);
					equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
					equipment.setTradeType(EquipTradeType.DELIVERY);
					equipment.setCreateDate(now);
					equipment.setCreateUser(currentUser.getUserName());
					
					//Trang thai thiet bi
					if (StringUtil.isNullOrEmpty(errMsg) && lstHealth != null) {
						String healthStatus = lstHealth.get(i);
						List<ApParam> lstAppram = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
						boolean flagHs = false;
						for (ApParam row : lstAppram) {
							if (healthStatus.equals(row.getApParamCode())) {
								flagHs = true;
								break;
							}
						}
						if (!flagHs) {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde")+ "\n";
						} else {
							equipment.setHealthStatus(healthStatus);
						}
					} else {
						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde") + "\n";
					}
					
					//Nam san xuat
					if (StringUtil.isNullOrEmpty(errMsg) && lstManufacturingYear != null) {
						Integer manufacturingYear = lstManufacturingYear.get(i);
						int curYear = DateUtil.getYear(now);
						if (manufacturingYear == null) {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.manufacturingYear") + "\n";
						} else if (manufacturingYear.intValue() < 1 || manufacturingYear.intValue() > curYear) {
							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.manufacturingYear") + "\n";
						} else {
							equipment.setManufacturingYear(manufacturingYear);
						}
					}
							
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					
					// thong tin chi tiet bien ban
					equipmentDeliveryRecDetail.setEquipment(equipment);
					equipmentDeliveryRecDetail.setContent(lstContentDelivery.get(i));
					equipmentDeliveryRecDetail.setCreateDate(now);
					equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
					if (equipStock != null) {
						equipmentDeliveryRecDetail.setFromStockId(equipStock.getId());
					}
					if (lstDepreciation != null && lstDepreciation.get(i) != null) {
						equipmentDeliveryRecDetail.setDepreciation(lstDepreciation.get(i));
					}
					
					equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
				} else { 
					Equipment equipment = null;
					equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
					
					if (equipment != null) {
						Equipment equipValidate = validateEquipment(equipment.getCode());
						if (equipValidate == null) {
							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.role") + "\n";
						} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " đang ở trạng thái không hoạt động";
						} else if (EquipStockTotalType.KHO_KH.getValue().equals(equipment.getStockType().getValue())) {
							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock") + "\n";
						} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.usage.status") + "\n";
						} else if (EquipTradeStatus.TRADING.getValue().equals(equipment.getTradeStatus())) {
							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.trade.status") + "\n";
						} else 
//							if (EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
//							if (equipment.getStockType().getValue().equals(EquipStockTotalType.KHO_NPP.getValue()) && staff != null && !staff.getShop().getShopCode().equals(equipment.getStockCode())) {
//								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.npp") + "\n";
//							}
//							EquipStockTotal equipStockTotal = equipmentManagerMgr.getEquipStockTotalById(equipment.getStockId(), EquipStockTotalType.parseValue(equipment.getStockType().getValue()), equipment.getEquipGroup().getId());
//							if(equipStockTotal == null || equipStockTotal.getQuantity() <= 0){
//								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.stock.invalid") + "\n";
//							}
//						}
						if (equipment.getSerial() == null && lstContentDelivery.get(i) != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(lstContentDelivery.get(i))) {
//							Khong rang buoc so seri khi cap moi
//							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content");
//						}
//						else if (StringUtil.isNullOrEmptyNotTrim(equipment.getSerial()) && StringUtil.isNullOrEmptyNotTrim(lstSeriNumber.get(i))) {
//							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.serial");
						} else if(equipment.getSerial() == null) {
							if (!StringUtil.isNullOrEmpty(lstSeriNumber.get(i)) && !ValidateUtil.validateFormatSerial(lstSeriNumber.get(i))) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.seri.format");
							} else if(StringUtil.isNullOrEmpty(errMsg) && lstSeriNumber.get(i).indexOf(" ") != -1) {
								errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial") + "\n";
							}
						}	
						if (StringUtil.isNullOrEmpty(equipment.getSerial()) && lstSeriNumber != null && !StringUtil.isNullOrEmpty(lstSeriNumber.get(i))) {
							equipment.setSerial(lstSeriNumber.get(i));
						}
						if (errMsg.length() > 0) {
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						equipmentDeliveryRecDetail.setEquipment(equipment);
					} else {
						result.put(ERROR, true);
						result.put("errMsg", "Không tìm thấy thiết bị có mã " + lstEquipCode.get(i));
						return JSON;
					}

					// thong tin chi tiet bien ban
					equipmentDeliveryRecDetail.setContent(lstContentDelivery.get(i));
					equipmentDeliveryRecDetail.setCreateDate(DateUtil.now());
					equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
					equipmentDeliveryRecDetail.setFromStockId(equipment.getStockId());
					if (lstDepreciation != null && lstDepreciation.get(i) != null) {
						equipmentDeliveryRecDetail.setDepreciation(lstDepreciation.get(i));
					}
					equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
				}
			}

			equipDeliveryRecord.setCreateDate(DateUtil.now());
			equipDeliveryRecord.setCreateUser(currentUser.getUserName());
			//tamvnm: update dac ta moi. 30/06/2015
//			List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(dateContract);
//			EquipPeriod equipPeriod = null;
//			if (lstPeriod == null || lstPeriod.size() == 0) {
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.contract.not.in"));
//				result.put(ERROR, error);
//				return JSON;
//			} else if (lstPeriod.size() > 0) {
//				// gan gia tri ky dau tien.
//				equipPeriod = lstPeriod.get(0);
//			}
			
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			equipDeliveryRecord.setEquipPeriod(equipPeriod);
			equipDeliveryRecord.setCreateFormDate(cDate);
			if (lstFile!=null && lstFile.size() > 0) {
				errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, 0);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
			}
			
			List<FileVO> lstfileVOs = saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType);
			EquipRecordFilter<EquipmentDeliveryVO> filter = new EquipRecordFilter<EquipmentDeliveryVO>();
			filter.setLstFileVo(lstfileVOs);
			EquipmentDeliveryReturnVO equipmentDeliveryReturnVO = new EquipmentDeliveryReturnVO();
			equipmentDeliveryReturnVO = equipmentManagerMgr.createRecordDeliveryByImportExcel(equipDeliveryRecord, equipmentDeliveryRecordDetails, filter);
			if (equipmentDeliveryReturnVO != null) {
				if (equipmentDeliveryReturnVO.getEquipDeliveryRevord() != null) {
					error = false;
				} 
				List<Equipment> lstEquipment = equipmentDeliveryReturnVO.getLstEquipment();
				if (lstEquipment != null && lstEquipment.size() > 0) {
					String m = "";
					for (int i = 0, size = lstEquipment.size(); i < size; i++) {
						m += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code")//Thiet bi co ma
								+ lstEquipment.get(i).getCode() // Ma thiet bi
								+ Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code1") + "\n";//dang giao dich hoac khong o kho
					}
					result.put(ERROR, true);
					result.put("errMsg", m);
					return JSON;
				}
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		result.put(ERROR, error);
		return JSON;
	}
	
//	/**
//	 * Tao moi bien ban
//	 * 
//	 * @author nhutnn
//	 * @since 22/12/2014
//	 * @return
//	 */
//	public String createRecordDelivery() {
//		resetToken(result);
//		boolean error = true;
//		String errMsg = "";
//		Date dateContract = null;
//		Date fromDatePage = null;
//		Date toDateIdNO = null;
//		Date toBDate = null;
//		Date now = new Date();
////		Shop fromShop = null;
//		Shop toShop = null;
//		try {
//			EquipDeliveryRecord equipDeliveryRecord = new EquipDeliveryRecord();
//			/** bo check shop ben cho muon*/
//			if (!StringUtil.isNullOrEmpty(fromShopCode)) {
////				fromShop = shopMgr.getShopByCode(fromShopCode);
////				
////				if(fromShop == null ){
////					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"ss.traningplan.npp.code");
////				}else if(!fromShop.getType().getObjectType().equals(ShopObjectType.NPP.getValue()) && !fromShop.getShopCode().equals("VNM")){
////					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
////				}
////				else if (!checkShopInOrgAccessByCode(fromShopCode)) {
////					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
////				}
////				else if(!fromShop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
////					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
////				} 
////				if (fromShop != null && fromShop.getShopCode().equals("VNM")) {
////					equipDeliveryRecord.setFromFax(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.vnm.fax"));
////				}
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.fromShop")));
//				return JSON;
//			}
//			
//			if (!StringUtil.isNullOrEmpty(toShopCode)) {
//				toShopCode = toShopCode.toUpperCase();
//				toShop = shopMgr.getShopByCode(toShopCode);
//				if(toShop == null){
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"ss.traningplan.npp.code");
//				}else if(!toShop.getType().getObjectType().equals(ShopObjectType.NPP.getValue())){
//					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
//				}
//				else if (!checkShopInOrgAccessByCode(toShopCode)) {
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
//				}
//				else if(!toShop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
//				} else if (toShop.getStatus().equals(ActiveType.STOPPED)) {
//					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.not.running.shop");
//				}
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.traningplan.npp.code")));
//				return JSON;
//			}
//			Staff staff = null;
//			if (!StringUtil.isNullOrEmpty(staffCode)) {
//				staff = staffMgr.getStaffByCode(staffCode);
//				if (staff == null || staff.getShop() == null) {
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.staff.code");
//				} else if (!ActiveType.RUNNING.equals(staff.getStatus())) {
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.staff.code");
////				} else if (toShop != null) {
////					if (staff.getShop().getId() != toShop.getId()) {
////						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.staff.not.npp");
////					} 
////					if (staff.getStaffType() != null && staff.getStaffType().getObjectType() != StaffObjectType.NVGS.getValue()) {
////						errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.staff.not.gs");
////					}
//				} 
//
//				if (errMsg.length() > 0) {
//					result.put(ERROR, true);
//					result.put("errMsg", errMsg);
//					return JSON;
//				}
//				equipDeliveryRecord.setStaff(staff);
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.staff.code")));
//				return JSON;
//			}
//			if (!StringUtil.isNullOrEmpty(customerCode) && toShop != null) {
//				customerCode = customerCode.trim().toLowerCase();
//				customerCode = StringUtil.getFullCode(toShop.getShopCode(), customerCode);
//				Customer customer = customerMgr.getCustomerByCode(customerCode);
//				//List<Customer> customers = customerMgr.getListCustomerByShortCode(shop.getId(), customerCode);
//				if(customer == null ){
//					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "pay.period.code.checked.customer.shop") + "\n";
//					//errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_IN_SHOP, true, "equipment.manager.customer.code");
//				}else if (!ActiveType.RUNNING.getValue().equals(customer.getStatus().getValue())) {
//					errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.customer.code");
//					
//				} else if (toShop != null && customer != null && !customer.getShop().getId().equals(toShop.getId())) {
//					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.customer.not.npp");
//				}
//				
//				if (errMsg.length() > 0) {
//					result.put(ERROR, true);
//					result.put("errMsg", errMsg);
//					return JSON;
//				}
//				equipDeliveryRecord.setCustomer(customer);
//				equipDeliveryRecord.setToAddress(customer.getAddress());
//				equipDeliveryRecord.setToShop(toShop);
//				equipDeliveryRecord.setToObjectName(toShop.getNameText());
//				if (customer.getPhone() != null) {
//					equipDeliveryRecord.setToPhone(customer.getPhone());
//				} else {
//					equipDeliveryRecord.setToPhone(customer.getMobiphone());
//				}
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.customer.code")));
//				return JSON;
//			}
//			
////			if (fromShop != null) {
////				equipDeliveryRecord.setFromShop(fromShop);
////				equipDeliveryRecord.setFromObjectName(fromShop.getNameText());
////			} else {
////				result.put(ERROR, true);
////				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.customer.code")));
////				return JSON;
////			}
//			
//			if (!StringUtil.isNullOrEmpty(numberContract)) {
//				errMsg = ValidateUtil.validateField(numberContract, "equipment.manager.number.contract", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT, ConstantManager.ERR_MAX_LENGTH);
//				if (StringUtil.isNullOrEmpty(errMsg) && numberContract.indexOf(" ") != -1) {
//					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.format.number.contract.space") + "\n";
//				}
//				if(!StringUtil.isNullOrEmpty(errMsg) ){
//					result.put(ERROR, true);
//					result.put("errMsg", errMsg);
//					return JSON;
//				}
//				equipDeliveryRecord.setContractNumber(numberContract);
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.customer.code")));
//				return JSON;
//			}
//			if (!StringUtil.isNullOrEmpty(contractDate)) {
//				dateContract = DateUtil.parse(contractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
//				if (dateContract != null && DateUtil.compareDateWithoutTime(dateContract, now) != 1) {
//					equipDeliveryRecord.setContractCreateDate(dateContract);
//				} else {
//					result.put(ERROR, true);
//					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.date.contract.big")));
//					return JSON;
//				}
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.date.contract")));
//				return JSON;
//			}
//			equipDeliveryRecord.setRecordStatus(statusRecord);
//			equipDeliveryRecord.setDeliveryStatus(statusDelivery);
//			equipDeliveryRecord.setFromObjectName(fromShopCode);
//			if (!StringUtil.isNullOrEmpty(fromObjectAddress)) {
//				equipDeliveryRecord.setFromObjectAddress(fromObjectAddress);
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.fromAddress")));
//				return JSON;
//			}
//			if (!StringUtil.isNullOrEmpty(fromObjectPhone)) {
//				equipDeliveryRecord.setFromObjectPhone(fromObjectPhone);
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.fromPhone")));
//				return JSON;
//			}
//			equipDeliveryRecord.setFromObjectTax(fromObjectTax);
//			equipDeliveryRecord.setFromPage(fromPage);
//			if (!StringUtil.isNullOrEmpty(fromPageDate)) {
//				fromDatePage = DateUtil.parse(fromPageDate, DateUtil.DATE_FORMAT_DDMMYYYY);
//				if (fromDatePage != null && DateUtil.compareDateWithoutTime(fromDatePage, now) != 1) {
//					equipDeliveryRecord.setFromPageDate(fromDatePage);
//				} else {
//					result.put(ERROR, true);
//					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idNO.fromPageDate.big")));
//					return JSON;
//				}
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idNO.fromPageDate")));
//				return JSON;
//			}
//			if (!StringUtil.isNullOrEmpty(toIdNODate)) {
//				toDateIdNO = DateUtil.parse(toIdNODate, DateUtil.DATE_FORMAT_DDMMYYYY);
//				if (toDateIdNO != null && DateUtil.compareDateWithoutTime(toDateIdNO, now) != 1) {
//					equipDeliveryRecord.setToIdNODate(toDateIdNO);
//				} else {
//					result.put(ERROR, true);
//					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idNO.dateCreate")));
//					return JSON;
//				}
//				
//			} 
////			else {
////				result.put(ERROR, true);
////				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.fromDate")));
////				return JSON;
////			}
//			if (!StringUtil.isNullOrEmpty(toBusinessDate)) {
//				toBDate = DateUtil.parse(toBusinessDate, DateUtil.DATE_FORMAT_DDMMYYYY);
//				if (toBDate != null && DateUtil.compareDateWithoutTime(toBDate, now) != 1) {
//					equipDeliveryRecord.setToBusinessDate(toBDate);
//				} else {
//					result.put(ERROR, true);
//					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idDKKD.dateCreate")));
//					return JSON;
//				}
//				
//			} else {
////				result.put(ERROR, true);
////				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.idDKKD.dateCreate")));
////				return JSON;
//			}
//			
//			
//			
//			equipDeliveryRecord.setFromPagePlace(fromPagePlace);
//			equipDeliveryRecord.setFromPosition(fromObjectPosition);
//			equipDeliveryRecord.setFromRepresentative(fromRepresentative);
//			equipDeliveryRecord.setFromFax(fromFax);
//			equipDeliveryRecord.setToObjectAddress(toObjectAddress);
//			equipDeliveryRecord.setToPosition(toObjectPosition);
//			equipDeliveryRecord.setToRepresentative(toRepresentative);
//			equipDeliveryRecord.setToIdNO(toIdNO);
//			equipDeliveryRecord.setToIdNOPlace(toIdNOPlace);
//			equipDeliveryRecord.setToBusinessLincense(toBusinessLicense);
//			equipDeliveryRecord.setToBusinessPlace(toBusinessPlace);
//
//			if(freezer != null) {
//				equipDeliveryRecord.setFreezer(freezer);
//			}
//			
//			if(refrigerator != null) {
//				equipDeliveryRecord.setRefrigerator(refrigerator);
//			}
//			
//			// kiem tra chi tiet bien ban
//			List<EquipDeliveryRecDtl> equipmentDeliveryRecordDetails = new ArrayList<EquipDeliveryRecDtl>();
//			for (int i = 0; lstEquipCode!=null && i < lstEquipCode.size(); i++) {
//				if(lstEquipCode.lastIndexOf(lstEquipCode.get(i)) != i) {
//					result.put(ERROR, true);
//					result.put("errMsg", "Mã TB " + lstEquipCode.get(i) + " " +Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau") + "\n");
//					return JSON;
//				}
//				EquipDeliveryRecDtl equipmentDeliveryRecDetail = new EquipDeliveryRecDtl();
//				Equipment equipment = null;
////				if (lstIdEquip.get(i) != -1) {
////					equipment = equipmentManagerMgr.getEquipmentById(lstIdEquip.get(i));
////				} else if (!StringUtil.isNullOrEmpty(lstEquipCode.get(i))) {
//				equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
////				equipment = validateEquipment(lstEquipCode.get(i));
////				}
//				
//				if (equipment != null) {
//					Equipment equipValidate = validateEquipment(equipment.getCode());
//					if (equipValidate == null) {
//						errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.role") + "\n";
//					} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
//						errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " đang ở trạng thái không hoạt động";
//					} else if (EquipStockTotalType.KHO_KH.getValue().equals(equipment.getStockType().getValue())) {
//						errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock") + "\n";
//					} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
//						errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.usage.status") + "\n";
//					} else if (EquipTradeStatus.TRADING.getValue().equals(equipment.getTradeStatus())) {
//						errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.trade.status") + "\n";
//					} else 
////						if (EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
////						if (equipment.getStockType().getValue().equals(EquipStockTotalType.KHO_NPP.getValue()) && staff != null && !staff.getShop().getShopCode().equals(equipment.getStockCode())) {
////							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.npp") + "\n";
////						}
////						EquipStockTotal equipStockTotal = equipmentManagerMgr.getEquipStockTotalById(equipment.getStockId(), EquipStockTotalType.parseValue(equipment.getStockType().getValue()), equipment.getEquipGroup().getId());
////						if(equipStockTotal == null || equipStockTotal.getQuantity() <= 0){
////							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.stock.invalid") + "\n";
////						}
////					}
//					if (equipment.getSerial() == null && lstContentDelivery.get(i) != null && !ApParamEquipType.DELIVERY_CONTENT_NEW.getValue().equals(lstContentDelivery.get(i))) {
//						errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.new.content");
////					}
////					else if (StringUtil.isNullOrEmptyNotTrim(equipment.getSerial()) && StringUtil.isNullOrEmptyNotTrim(lstSeriNumber.get(i))) {
////						errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.serial");
//					} else if(equipment.getSerial() == null) {
//						if (!StringUtil.isNullOrEmpty(lstSeriNumber.get(i)) && !ValidateUtil.validateFormatSerial(lstSeriNumber.get(i))) {
//							errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.invalid.seri.format");
//						} else if(StringUtil.isNullOrEmpty(errMsg) && lstSeriNumber.get(i).indexOf(" ") != -1) {
//							errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial") + "\n";
//						}
//					}	
//					if (StringUtil.isNullOrEmpty(equipment.getSerial()) && lstSeriNumber != null && !lstSeriNumber.get(i).equals("null") && !StringUtil.isNullOrEmpty(lstSeriNumber.get(i))) {
//						equipment.setSerial(lstSeriNumber.get(i));
//					}
//					if (errMsg.length() > 0) {
//						result.put(ERROR, true);
//						result.put("errMsg", errMsg);
//						return JSON;
//					}
//					equipmentDeliveryRecDetail.setEquipment(equipment);
//				} else {
//					result.put(ERROR, true);
//					result.put("errMsg", "Không tìm thấy thiết bị có mã " + lstEquipCode.get(i));
//					return JSON;
//				}
//
//				// thong tin chi tiet bien ban
//				equipmentDeliveryRecDetail.setContent(lstContentDelivery.get(i));
//				equipmentDeliveryRecDetail.setCreateDate(DateUtil.now());
//				equipmentDeliveryRecDetail.setCreateUser(currentUser.getUserName());
//				equipmentDeliveryRecDetail.setFromStockId(equipment.getStockId());
////				equipmentDeliveryRecDetail.setFromStockType(equipment.getStockType().getValue());
//				if (lstDepreciation != null && lstDepreciation.get(i) != null) {
//					equipmentDeliveryRecDetail.setDepreciation(lstDepreciation.get(i));
//				}
//				equipmentDeliveryRecordDetails.add(equipmentDeliveryRecDetail);
//				
//			}
//
//			equipDeliveryRecord.setCreateDate(DateUtil.now());
//			equipDeliveryRecord.setCreateUser(currentUser.getUserName());
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			equipDeliveryRecord.setEquipPeriod(equipPeriod);
//			
//			if (lstFile!=null && lstFile.size() > 0) {
//				errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, 0);
//				if (errMsg.length() > 0) {
//					result.put(ERROR, true);
//					result.put("errMsg", errMsg);
//					return JSON;
//				}
//			}
//			
//			List<FileVO> lstfileVOs = saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType);
//			EquipRecordFilter<EquipmentDeliveryVO> filter = new EquipRecordFilter<EquipmentDeliveryVO>();
//			filter.setLstFileVo(lstfileVOs);
//			EquipmentDeliveryReturnVO equipmentDeliveryReturnVO = new EquipmentDeliveryReturnVO();
//			equipmentDeliveryReturnVO = equipmentManagerMgr.createRecordDeliveryByImportExcel(equipDeliveryRecord, equipmentDeliveryRecordDetails, filter);
//			if (equipmentDeliveryReturnVO != null) {
//				if (equipmentDeliveryReturnVO.getEquipDeliveryRevord() != null) {
//					error = false;
//				} 
//				List<Equipment> lstEquipment = equipmentDeliveryReturnVO.getLstEquipment();
//				if (lstEquipment != null && lstEquipment.size() > 0) {
//					String m = "";
//					for (int i = 0, size = lstEquipment.size(); i > size; i++) {
//						m += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code")//Thiet bi co ma
//								+ lstEquipment.get(i).getCode() // Ma thiet bi
//								+ Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.code1") + "\n";//dang giao dich hoac khong o kho
//					}
//					result.put(ERROR, true);
//					result.put("errMsg", m);
//					return JSON;
//				}
//			}
//
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
//		result.put(ERROR, error);
//		if (error && StringUtil.isNullOrEmpty(errMsg)) {
//			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//		}
//		result.put("errMsg", errMsg);
//		result.put(ERROR, error);
//		return JSON;
//	}

	/**
	 * Lay danh sach thiet bi trong 1 bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 * @return
	 */
	public String searchEquipmentInRecord() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(null);
			if (idRecordDelivery != null) {
				equipmentFilter.setIdRecordDelivery(idRecordDelivery);
			}

			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentDeliveryVOByFilter(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getLstObject().size());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
			
			getLstEquipInfo();
			result.put("total", lstEquipGroup);
			result.put("total", lstEquipProvider);
			result.put("total", lstHealthy);
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach thiet bi bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since December 15,2014
	 */
	public String searchEquipmentDelivery() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			KPaging<EquipmentDeliveryVO> kPaging = new KPaging<EquipmentDeliveryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (categoryId != null) {
				equipmentFilter.setEquipCategoryId(categoryId);
			}
			if (groupId != null) {
				equipmentFilter.setEquipGroupId(groupId);
			}
			if (providerId != null) {
				equipmentFilter.setEquipProviderId(providerId);
			}
			if (yearManufacture != null) {
				equipmentFilter.setYearManufacture(yearManufacture);
			}
			if (!StringUtil.isNullOrEmpty(stockCode)) {
				equipmentFilter.setStockCode(stockCode);
			}
			if (!StringUtil.isNullOrEmpty(equipLendCode)) {
				equipmentFilter.setEquipLendCode(equipLendCode);
			} 
			if (customerId != null) {
				equipmentFilter.setCustomerId(customerId);
			}
//			List<String> lstEDelete = new ArrayList<String>();
//			if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
//				String[] lst = lstEquipDelete.toString().split(",");
//				for (int i = 0; i < lst.length; i++) {
//					lstEDelete.add(lst[i]); 
//				}
//				equipmentFilter.setLstEquipDelete(lstEDelete);
//			}
			
			List<String> lstECode = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipmentCode))) {
				String[] lst = lstEquipmentCode.toString().split(", ");
				for (int i = 0; i < lst.length; i++) {
					lstECode.add(lst[i].trim());
				}
				equipmentFilter.setLstEquipmentCode(lstECode);
			}
			
			List<String> lstEAdd = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipAdd))) {
				String[] lst = lstEquipAdd.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				if (equipmentFilter.getLstEquipmentCode() != null && equipmentFilter.getLstEquipmentCode().size() > 0) {
					lstEAdd.addAll(lstECode);
				}
				equipmentFilter.setLstEquipAdd(lstEAdd);
			}
			equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			equipmentFilter.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentByRole(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getkPaging().getTotalRows());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Kiem tra equipment thuoc phan quyen hay ko
	 * 
	 * @author nhutnn
	 * @since 17/12/2014
	 * @return
	 */
	public Equipment validateEquipment(String equipmentCode) {
		try {
	
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(null);
			if (!StringUtil.isNullOrEmpty(equipmentCode)) {
				equipmentFilter.setEquipCode(equipmentCode);
			} else {
				return null;
			}
			
	//		List<String> lstEDelete = new ArrayList<String>();
	//		if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
	//			String[] lst = lstEquipDelete.toString().split(",");
	//			for (int i = 0; i < lst.length; i++) {
	//				lstEDelete.add(lst[i]); 
	//			}
	//			equipmentFilter.setLstEquipDelete(lstEDelete);
	//		}
			
			List<String> lstECode = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipmentCode))) {
				String[] lst = lstEquipmentCode.toString().split(", ");
				for (int i = 0; i < lst.length; i++) {
					lstECode.add(lst[i].trim());
				}
				equipmentFilter.setLstEquipmentCode(lstECode);
			}
			
			List<String> lstEAdd = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipAdd))) {
				String[] lst = lstEquipAdd.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				if (equipmentFilter.getLstEquipmentCode() != null && equipmentFilter.getLstEquipmentCode().size() > 0) {
					lstEAdd.addAll(lstECode);
				}
				equipmentFilter.setLstEquipAdd(lstEAdd);
			}
			equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
//			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//			equipmentFilter.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
//			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentByRole(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null && equipmentDeliverys.getLstObject().size() > 0) {
				Long id = equipmentDeliverys.getLstObject().get(0).getEquipmentId();
				if (id != null) {
					Equipment e = commonMgr.getEntityById(Equipment.class, id);
					if (e != null) {
						return e;
					}
				}
				
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return null;
	}

	/**
	 * Xuat excel bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since 17/12/2014
	 * @return
	 */
	public String exportRecordDelivery() {
		try {
			/**ATTT duong dan*/
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			int NumberCol = 30;
			EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentRecordDeliveryVO>();
			Shop shop = null;
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			if (shop != null) {
				curShopCode = shop.getShopCode();
				if (!StringUtil.isNullOrEmpty(curShopCode)) {
					equipmentFilter.setCurShopCode(curShopCode);
				}
			}
			if (lstIdChecked != null && lstIdChecked.size() > 0) {
				equipmentFilter.setLstIdRecord(lstIdChecked);
			} else {
				Date fDate = null;
				Date tDate = null;
				Date fContractDate = null;
				Date tContractDate = null;

				if (!StringUtil.isNullOrEmpty(fromDate)) {
					fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					equipmentFilter.setFromDate(fDate);
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					equipmentFilter.setToDate(tDate);
				}
				if (!StringUtil.isNullOrEmpty(fromContractDate)) {
					fContractDate = DateUtil.parse(fromContractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					equipmentFilter.setFromContractDate(fContractDate);
				}
				if (!StringUtil.isNullOrEmpty(toContractDate)) {
					tContractDate = DateUtil.parse(toContractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					equipmentFilter.setToContractDate(tContractDate);
				}
				if (!StringUtil.isNullOrEmpty(staffCode)) {
					equipmentFilter.setStaffCode(staffCode);
				}
				if (!StringUtil.isNullOrEmpty(customerCode)) {
					equipmentFilter.setCustomerCode(customerCode);
				}

				// Shop shop = null;
				// shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				// if(shop != null) {
				// curShopCode = shop.getShopCode();
				// if (!StringUtil.isNullOrEmpty(curShopCode)) {
				// equipmentFilter.setCurShopCode(curShopCode);
				// }
				// }

//				if (!StringUtil.isNullOrEmpty(customerName)) {
//					String nameText = Unicode2English.codau2khongdau(customerName);
//					equipmentFilter.setCustomerName(nameText);
//				}
				if (!StringUtil.isNullOrEmpty(numberContract)) {
					equipmentFilter.setNumberContract(numberContract);
				}
				if (!StringUtil.isNullOrEmpty(recordCode)) {
					equipmentFilter.setRecordCode(recordCode);
				}
				if (statusRecord != null) {
					equipmentFilter.setStatusRecord(statusRecord);
				}
				if (statusDelivery != null) {
					equipmentFilter.setStatusDelivery(statusDelivery);
				}

				if (statusPrint != null) {
					equipmentFilter.setStatusPrint(statusPrint);
				}
				if (!StringUtil.isNullOrEmpty(lstShop)) {
					equipmentFilter.setShopCode(lstShop);
				}

				if (!StringUtil.isNullOrEmpty(equipLendCode)) {
					equipmentFilter.setEquipLendCode(equipLendCode);
				}

				if (!StringUtil.isNullOrEmpty(equipCode)) {
					equipmentFilter.setEquipCode(equipCode);
				}
			}
			List<EquipmentRecordDeliveryVO> equipmentRecordDeliverys = equipmentManagerMgr.getListDeliveryRecordForExport(equipmentFilter);
			if (equipmentRecordDeliverys != null && equipmentRecordDeliverys.size() > 0) {	
				SXSSFWorkbook workbook = null;
				FileOutputStream out = null;
				try {
					if (null != equipmentRecordDeliverys && equipmentRecordDeliverys.size() > 0) {
//						for(int i=0, size = equipmentRecordDeliverys.size(); i<size; i++){
//							EquipmentFilter<EquipmentDeliveryVO> filter = new EquipmentFilter<EquipmentDeliveryVO>();
//							filter.setkPaging(null);
//							filter.setIdRecordDelivery(equipmentRecordDeliverys.get(i).getIdRecord());
//							filter.setStatusEquip(StatusType.ACTIVE.getValue());
//							ObjectVO<EquipmentDeliveryVO> lstEquipmentDeliveryVOs = equipmentManagerMgr.getListEquipmentDeliveryVOByFilter(filter);
//							if(lstEquipmentDeliveryVOs.getLstObject() != null || lstEquipmentDeliveryVOs.getLstObject().size()>0){
//								equipmentRecordDeliverys.get(i).setLstEquipmentDeliveryVOs(lstEquipmentDeliveryVOs.getLstObject());
//							}else{
//								equipmentRecordDeliverys.get(i).setLstEquipmentDeliveryVOs(new ArrayList<EquipmentDeliveryVO>());
//							}
//						}
						List<Integer> lstPositon = formatDateForExportExcel(equipmentRecordDeliverys);
						// Init XSSF workboook
						String outputName = ConstantManager.TEMPLATE_IMPORT_RECORD_DELIVERY + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX;
						String exportFileName = Configuration.getStoreRealPath() + outputName;

						workbook = new SXSSFWorkbook(-1);
						workbook.setCompressTempFiles(true);
						// Tao shett
						SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
						Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
						ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

						// Set Getting Defaul
						sheetData.setDefaultRowHeight((short) (15 * 20));
						sheetData.setDefaultColumnWidth(18);
						// set static Column width
						mySheet.setColumnsWidth(sheetData, 0, 120, 100, 100, 100, 150, 100, 100, 100, 150);
						// Size Row
						mySheet.setRowsHeight(sheetData, 0, 20);
						sheetData.setDisplayGridlines(true);
						// text style
						XSSFCellStyle textStyle = (XSSFCellStyle)style.get(ExcelPOIProcessUtils.NORMAL_BORDER).clone();
						XSSFCellStyle textStyleCenter = (XSSFCellStyle)style.get(ExcelPOIProcessUtils.NORMAL_CENTER_BORDER).clone();
						XSSFCellStyle numberStyle = (XSSFCellStyle)style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_BORDER).clone();
						DataFormat fmt = workbook.createDataFormat();
						textStyle.setDataFormat(fmt.getFormat("@"));
						textStyleCenter.setDataFormat(fmt.getFormat("@"));
						
						int iRow = 0, iColumn = 0;
						// tao column header
						String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.delivery");
						String[] lstHeaders = headers.split(";");
						for (int i = 0; i < lstHeaders.length; i++) {
							mySheet.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
						}
						int index = 0;
						for (int j = 0; j < lstPositon.size(); j ++) {
							int row = lstPositon.get(j);
							int size = index + row;
							int loop = 0;
							
							for (int i = index; i < size; i++) {
								if (loop > 0) {
									String status = "";
									iColumn = 0;
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									
									//noi dung
									List<ApParamEquip> lContent = getListContentDelivety();
									if (lContent != null && lContent.size() > 0) {
										for (int c = 0, csize = lContent.size(); c < csize; c++) {
											if (lContent.get(c) != null && !StringUtil.isNullOrEmpty(equipmentRecordDeliverys.get(i).getContent())
													&& lContent.get(c).getApParamEquipCode().equals(equipmentRecordDeliverys.get(i).getContent())) {
												status = lContent.get(c).getValue();
												break;
											}
										}
									}
									mySheet.addCell(sheetData, iColumn++, iRow, status, textStyle);
									// nhom, ncc, ma, seri, so thang khau hao
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getEquipGroupCode(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getEquipGroup(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getEquipProvider(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getEquipmentCode(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getSeriNumber() == null ? "" : equipmentRecordDeliverys.get(i).getSeriNumber(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getDepreciation(), numberStyle);
//									
									//Gia tri TBBH, trinh trang, nam sx
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getPrice(), numberStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getHealth(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getManufacturingYear(), textStyle);
									
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", numberStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", numberStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, "", textStyle);
								} else {
									iColumn = 0;
									mySheet.addCell(sheetData, iColumn++, ++iRow, equipmentRecordDeliverys.get(i).getNumberContract(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getShopCode(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getStaffCode(), textStyle);
									if (equipmentRecordDeliverys.get(i).getCreateFormDate() != null) {
										mySheet.addCell(sheetData, iColumn++, iRow, DateUtil.convertDateByString(equipmentRecordDeliverys.get(i).getCreateFormDate(), DateUtil.DATE_FORMAT_STR_PRO), textStyleCenter);
									} else {
										mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getCreateFormDate(), textStyle);
									}
									
									if (equipmentRecordDeliverys.get(i).getContractCreateDate() != null) {
										mySheet.addCell(sheetData, iColumn++, iRow, DateUtil.convertDateByString(equipmentRecordDeliverys.get(i).getContractCreateDate(), DateUtil.DATE_FORMAT_STR_PRO), textStyleCenter);
									} else {
										mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getContractCreateDate(), textStyle);
									}
									 
									String status = "";
									//noi dung
									List<ApParamEquip> lContent = getListContentDelivety();
									if (lContent != null && lContent.size() > 0) {
										for (int c = 0, csize = lContent.size(); c < csize; c++) {
											if (lContent.get(c) != null && !StringUtil.isNullOrEmpty(equipmentRecordDeliverys.get(i).getContent())
													&& lContent.get(c).getApParamEquipCode().equals(equipmentRecordDeliverys.get(i).getContent())) {
												status = lContent.get(c).getValue();
												break;
											}
										}
									}
									mySheet.addCell(sheetData, iColumn++, iRow, status, textStyle);
									
									// nhom, ncc, ma, seri, so thang khau hao
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getEquipGroupCode(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getEquipGroup(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getEquipProvider(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getEquipmentCode(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getSeriNumber() == null ? "" : equipmentRecordDeliverys.get(i).getSeriNumber(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getDepreciation(), numberStyle);
									
									//Gia tri TBBH, trinh trang, nam sx
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getPrice(), numberStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getHealth(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getManufacturingYear(), textStyle);
									
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getCustomerCode(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getToPermanentAddress(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getIdNO(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getIdNOPlace(), textStyle);
									if (equipmentRecordDeliverys.get(i).getIdNODate() != null) {
										mySheet.addCell(sheetData, iColumn++, iRow, DateUtil.convertDateByString(equipmentRecordDeliverys.get(i).getIdNODate(), DateUtil.DATE_FORMAT_STR_PRO), textStyleCenter);
									} else {
										mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getIdNODate(), textStyle);
									}
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getToBusinessLicense(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getToBusinessPlace(), textStyle);
									if (equipmentRecordDeliverys.get(i).getToBusinessDate() != null) {
										mySheet.addCell(sheetData, iColumn++, iRow, DateUtil.convertDateByString(equipmentRecordDeliverys.get(i).getToBusinessDate(), DateUtil.DATE_FORMAT_STR_PRO), textStyleCenter);
									} else {
										mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getToBusinessDate(), textStyle);
									}
									
//									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getToObjectAddress(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getSoNha(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getDuong(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getPhuong(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getQuan(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getTp(), textStyle);
									
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getToRepresentative(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getToPosition(), textStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getFreezer(), numberStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getRefrigerator(), numberStyle);
									mySheet.addCell(sheetData, iColumn++, iRow, equipmentRecordDeliverys.get(i).getNote(), textStyle);
								}
								loop++;
								if (i != size - 1) {
									iRow++;
								}
							}
							// them 1 dong trong
//							for (int c = 0; c < NumberCol; c++) {
//								mySheet.addCell(sheetData, c, iRow, "", textStyle);
//							}
							index = size;
						}
						
						out = new FileOutputStream(exportFileName);
						workbook.write(out);
						out.close();
						workbook.dispose();
						String outputPath = Configuration.getExportExcelPath() + outputName;
						result.put(REPORT_PATH, outputPath);
						result.put(ERROR, false);
						result.put("hasData", true);
						/**ATTT duong dan*/
						MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
					} else {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
						result.put(ERROR, true);
						result.put("data.hasData", true);
						result.put("errMsg", errMsg);
					}
				} catch (Exception e) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					LogUtility.logError(e, e.getMessage());
					if (workbook != null) {
						workbook.dispose();
					}
					if (out != null) {
						out.close();
					}
				}
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
				result.put(ERROR, true);
				result.put("data.hasData", true);
				result.put("errMsg", errMsg);
			}
			System.gc();
			return JSON;
		} catch (Exception e) {
			result.put(ERROR, true);
		    result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay danh sach nha phan phoi
	 * 
	 * @author nhutnn
	 * @since 19/12/2014
	 * @return
	 */
	public String getListGSNPP() {
		result.put("rows", new ArrayList<StaffVO>());
		try {
			lstGsnpp = new ArrayList<StaffVO>();
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			ShopToken shopVo = null;
			if (checkShopInOrgAccessByCode(toShopCode)) {
				shopVo = getShopTockenInOrgAccessByCode(toShopCode);
				filter.setShopId(shopVo.getShopId());
			} else {
				//filter.setShopId(currentUser.getShopRoot().getShopId());
				return JSON;
			}
			
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				filter.setIsLstChildStaffRoot(true);
				objVO = staffMgr.getListGSByStaffRootWithInParentStaffMap(filter);
			} else if (shopVo != null && (shopVo.getObjectType().equals(ShopSpecificType.NPP.getValue()) 
					|| shopVo.getObjectType().equals(ShopSpecificType.NPP_MT.getValue()))) {
				List<Integer> lstObjectType = new ArrayList<Integer>();
				lstObjectType.add(StaffSpecificType.GSMT.getValue());
//				lstObjectType.add(StaffObjectType.GSKA.getValue());
				lstObjectType.add(StaffSpecificType.SUPERVISOR.getValue());
				filter.setLstObjectType(lstObjectType);
				
				filter.setIsLstChildStaffRoot(false);
				objVO = staffMgr.getListGSByStaffRootKAMT(filter);
//			} else if (shopVo != null && shopVo.getObjectType().equals(ShopObjectType.NPP_KA.getValue())) {
//				List<Integer> lstObjectType = new ArrayList<Integer>();
//				lstObjectType.add(StaffObjectType.GSMT.getValue());
//				lstObjectType.add(StaffObjectType.GSKA.getValue());
//				lstObjectType.add(StaffObjectType.NVGS.getValue());
//				filter.setLstObjectType(lstObjectType);
//				
//				filter.setIsLstChildStaffRoot(false);
//				objVO = staffMgr.getListGSByStaffRootKAMT(filter);
			} else {
				filter.setIsLstChildStaffRoot(false);
				
				filter.setGsmt(StaffSpecificType.GSMT.getValue());
//				filter.setGska(StaffObjectType.GSKA.getValue());
				objVO = staffMgr.getListGSByStaffRootWithNVBH(filter);
				
			}			
			
			if (objVO != null) {
				result.put("rows", objVO.getLstObject());
				//lstGsnpp = objVO.getLstObject();
			}
			
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}	

	/**
	 * Lay mot thiet bi
	 * 
	 * @author nhutnn
	 * @since 19/12/2014
	 * @return
	 */
	public String getEquipment() {
		try {
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(null);
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				Equipment e = validateEquipment(equipCode);
				if (e != null) {
					equipmentFilter.setCode(equipCode);
				} else {
					return JSON;
				}
				
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (customerId != null) {
				equipmentFilter.setCustomerId(customerId);
			}
			if (!StringUtil.isNullOrEmpty(equipLendCode)) {
				equipmentFilter.setEquipLendCode(equipLendCode);
			} else {
				List<String> lstEDelete = new ArrayList<String>();
				if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
					String[] lst = lstEquipDelete.toString().split(",");
					for (int i = 0; i < lst.length; i++) {
						lstEDelete.add(lst[i]); 
					}
					equipmentFilter.setLstEquipDelete(lstEDelete);
				}
			}
			
//			List<String> lstEDelete = new ArrayList<String>();
//			if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
//				String[] lst = lstEquipDelete.toString().split(",");
//				for (int i = 0; i < lst.length; i++) {
//					lstEDelete.add(lst[i]);
//				}
//				equipmentFilter.setLstEquipDelete(lstEDelete);
//			}
			List<String> lstEAdd = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipAdd))) {
				String[] lst = lstEquipAdd.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				equipmentFilter.setLstEquipAdd(lstEAdd);
			}
			equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			equipmentFilter.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentByRole(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				equipments = equipmentDeliverys.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem kho Don vi hien thi dang Datagrid.
	 * 
	 * @author nhutnn
	 * @since 20/12/2014
	 * @return List stock
	 */
	public String searchStockTotal() {
		result.put("total", 0);
		result.put("rows", new ArrayList<ShopVO>());
		try {
			EquipmentFilter<EquipmentStockDeliveryVO> filter = new EquipmentFilter<EquipmentStockDeliveryVO>();
			KPaging<EquipmentStockDeliveryVO> kPaging = new KPaging<EquipmentStockDeliveryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			filter.setShopParentId(currentUser.getShopRoot().getShopId());
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setCode(shopCode);
			}
			if (!StringUtil.isNullOrEmpty(shopName)) {
				filter.setName(shopName);
			}
			filter.setStatus(ActiveType.RUNNING.getValue());
			ObjectVO<EquipmentStockDeliveryVO> data = equipmentManagerMgr.getListEquipStockTotalVOByfilter(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay mot vai danh muc cua thiet bi: nhom, ncc, loai
	 * 
	 * @author nhutnn
	 * @since 20/12/2014
	 * @return
	 */
	public String getEquipmentCatalog() {
		try {
			EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
			equipmentFilter.setkPaging(null);
			if (equipLendCode != null && !equipLendCode.isEmpty()) {
				equipmentFilter.setEquipLendCode(equipLendCode);
			}
			if (customerId != null) {
				equipmentFilter.setCustomerId(customerId);
			}
			List<EquipmentVO> equipsCategory = new ArrayList<EquipmentVO>();
			List<EquipmentVO> equipProviders = new ArrayList<EquipmentVO>();
			equipsCategory = equipmentManagerMgr.getListEquipmentCategory(equipmentFilter);
			equipProviders = equipmentManagerMgr.getListEquipmentProvider(equipmentFilter);

			result.put("equipsCategory", equipsCategory);
			result.put("equipProviders", equipProviders);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay nhom thiet bi
	 * 
	 * @author nhutnn
	 * @since 20/12/2014
	 * @return
	 */
	public String getEquipmentGroup() {
		try {
			EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
			equipmentFilter.setkPaging(null);
			List<EquipmentVO> equipGroups = new ArrayList<EquipmentVO>();
			if (id != null) {
				equipmentFilter.setEquipCategoryId(id);
			}
			if (customerId != null) {
				equipmentFilter.setCustomerId(customerId);
			}
			
			if (equipLendCode != null && !equipLendCode.isEmpty()) {
				equipmentFilter.setEquipLendCode(equipLendCode);
			}
			equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
			ObjectVO<EquipmentVO> eGroup = equipmentManagerMgr.searchEquipmentGroupVObyFilter(equipmentFilter);
			if (eGroup.getLstObject() != null) {
				equipGroups = eGroup.getLstObject();
			}
			result.put("equipGroups", equipGroups);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * lay danh sach kho NPP cho combobox
	 * @author tamvnm
	 * @return danh sach kho cua NPP
	 * @since Oct 20 2015
	 */
	public String getEquipmentStockCombobox() {
		Date startLogDate = DateUtil.now();
		try {
			EquipStockFilter filter = new EquipStockFilter();
			if (!StringUtil.isNullOrEmpty(toShopCode)) {
				filter.setToShopCode(toShopCode);
			}
			filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			filter.setShopRoot(currentUser.getShopRoot().getShopId());
			ObjectVO<EquipStockVO> objVO = equipmentManagerMgr.getListEquipStockVOByRole(filter);
			if (objVO != null && objVO.getLstObject() != null) {
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "vnm.web.action.ManageDeliveryEquipmentAction.getEquipmentStockCombobox"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * Lay kho thiet bi duoc phan quyen
	 * 
	 * @author tamvnm
	 * @since 22/05/2015
	 * @return
	 */
	public String getEquipmentStock() {
		result.put("rows", new ArrayList<EquipStockVO>());
		result.put("total", 0);
		try {
			EquipStockFilter filter = new EquipStockFilter();
			// set cac tham so tim kiem
			KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			// goi ham thuc thi tim kiem
			// if (currentUser.getShopRoot()!=null) {
			// filter.setShopRoot(currentUser.getShopRoot().getShopId());
			// }
//			if (currentUser.getStaffRoot() != null) {
//				filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
//			}
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setShopCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setShopName(name);
			}
			filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			filter.setShopRoot(currentUser.getShopRoot().getShopId());
			filter.setStrListShopId(getStrListShopId());
			ObjectVO<EquipStockVO> objVO = equipmentManagerMgr.getListEquipStockVOByRole(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ManageDeliveryEquipmentAction.getListEquipStockVOByFilter()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Kiem tra ki
	 * 
	 * @author nhutnn
	 * @since 22/12/2014
	 * @return
	 */
//	public String checkEquipPeriod() {
//		try {
//			result.put(ERROR, false);
//			Date now = commonMgr.getSysDate();
//			if (id == null || id <= 0) {
//				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue()) ||
//					(DateUtil.compareDateWithoutTime(now, equipPeriod.getFromDate()) == -1) 
//					|| (DateUtil.compareDateWithoutTime(equipPeriod.getToDate(), now) == -1)) {
//					result.put(ERROR, true);
//					result.put("errMsg", "Kỳ hiện tại không MỞ!");
//				}
//			} else{
//				EquipDeliveryRecord equipDeliveryRecord = equipmentManagerMgr.getEquipDeliveryRecordById(id);
//		
//				if (((statusRecord!= null && !equipDeliveryRecord.getRecordStatus().equals(statusRecord)) 
//					|| !StringUtil.isNullOrEmpty(lstEquipDelete) || (lstEquipCode != null && lstEquipCode.size()>0))
//					&&( equipDeliveryRecord.getEquipPeriod() == null || !equipDeliveryRecord.getEquipPeriod().getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())
//					|| (DateUtil.compareDateWithoutTime(now, equipDeliveryRecord.getEquipPeriod().getFromDate()) == -1) 
//					|| (DateUtil.compareDateWithoutTime(equipDeliveryRecord.getEquipPeriod().getToDate(), now) == -1))) {
//					result.put(ERROR, true);
//					result.put("errMsg", "Kỳ của biên bản không hợp lệ để cho chỉnh sửa!");
//				}
//			}
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
//		return JSON;
//	}
	/**
	 * Xuat excel bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since 14/01/2015
	 * @return
	 */
	public String printDelivery() {
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.respon.not.data"));
				return JSON;
			}
			/**ATTT duong dan*/
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			// lay bien ban
			EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentRecordDeliveryVO>();
			equipmentFilter.setLstIdRecord(lstIdRecord);
			List<EquipmentRecordDeliveryVO> equipmentRecordDeliveryVOs = equipmentManagerMgr.getListEquipmentRecordDeliveryForExport(equipmentFilter);
			if (equipmentRecordDeliveryVOs == null || equipmentRecordDeliveryVOs.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.respon.not.data"));
				return JSON;
			}
			// set data
			listObjectBeans = new ArrayList<Map<String, Object>>();
			for(int i=0, size = equipmentRecordDeliveryVOs.size(); i< size; i++){
				EquipmentRecordDeliveryVO equipmentRecordDeliveryVO = equipmentRecordDeliveryVOs.get(i);
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("numberContract", equipmentRecordDeliveryVO.getNumberContract());	
				beans.put("fromShopName", equipmentRecordDeliveryVO.getFromShopName());
				beans.put("fromShopAddess", equipmentRecordDeliveryVO.getFromShopAddess());
				beans.put("fromPhone", equipmentRecordDeliveryVO.getFromPhone());
				beans.put("fromFax", equipmentRecordDeliveryVO.getFromFax());
				beans.put("fromRepresentative", equipmentRecordDeliveryVO.getFromRepresentative());
				beans.put("fromPosition", equipmentRecordDeliveryVO.getFromPosition());
				beans.put("toCustomerName", equipmentRecordDeliveryVO.getToCustomerName());
				beans.put("toCustomerCode", equipmentRecordDeliveryVO.getToCustomerCode());
				beans.put("toAddress", equipmentRecordDeliveryVO.getToPermanentAddress());
				beans.put("toObjectAddress", equipmentRecordDeliveryVO.getToObjectAddress());
				beans.put("toPhone", equipmentRecordDeliveryVO.getToPhone());
				beans.put("toBusinessLicense", equipmentRecordDeliveryVO.getToBusinessLicense());
				beans.put("toBusinessPlace", equipmentRecordDeliveryVO.getToBusinessPlace());
				if (equipmentRecordDeliveryVO.getToBusinessDate() != null) {
					beans.put("toBusinessDate", DateUtil.toDateString(equipmentRecordDeliveryVO.getToBusinessDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				}
				beans.put("toRepresentative", equipmentRecordDeliveryVO.getToRepresentative());
				beans.put("toPosition", equipmentRecordDeliveryVO.getToPosition());
				beans.put("idNO", equipmentRecordDeliveryVO.getIdNO());
				beans.put("idNOPlace", equipmentRecordDeliveryVO.getIdNOPlace());
				if (equipmentRecordDeliveryVO.getIdNODate() != null) {
					beans.put("idNODate", DateUtil.toDateString(equipmentRecordDeliveryVO.getIdNODate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				}
				
				Date contractDate = equipmentRecordDeliveryVO.getContractDate();
				if(contractDate != null){
					String dateContract = DateUtil.toDateString(contractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					beans.put("dayContract", dateContract.substring(0, 2));
					beans.put("monthContract", dateContract.substring(dateContract.indexOf("/")+1,dateContract.lastIndexOf("/")));
					beans.put("yearContract", dateContract.substring(dateContract.length()-4));			
				}			
				beans.put("numberEquip", equipmentRecordDeliveryVO.getNumberEquip());
				beans.put("lstEquipmentDeliveryVOs", equipmentRecordDeliveryVO.getLstEquipmentDeliveryVOs());
				beans.put("mien", equipmentRecordDeliveryVO.getMien());
				beans.put("kenh", equipmentRecordDeliveryVO.getKenh());
				beans.put("staffName", equipmentRecordDeliveryVO.getStaffName());
				listObjectBeans.add(beans);
			}
			JRDataSource dataSource = new JRBeanCollectionDataSource(listObjectBeans);
//			// dat Parameters
//			parametersReport = new HashMap<String, Object>();
//			parametersReport.put("kenh", kenh);
//			parametersReport.put("mien", mien);
//			FileExtension ext = FileExtension.PDF;
//			//String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.DEBIT_7_2_4_NVBH1);
//			String outputPath = ReportUtils.exportFromFormat(ext, (HashMap<String, Object>) parametersReport, dataSource, ShopReportTemplate.TEMPLATE_PRINT_RECORD_DELIVERY);
//			result.put(ERROR, false);
//			result.put(REPORT_PATH, outputPath);
			/**ATTT duong dan*/
//			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());	
			String error = createPDF(ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY, dataSource);
			if (!error.isEmpty()) {
				result.put("errMsg",error);
				result.put(ERROR, true);
				return JSON;
			}
			result.put(ERROR, false);
			String downloadUrl = ReportUtils.addReportTokenToDownloadFileUrl(downloadPath, reportToken);
			result.put(REPORT_PATH, downloadUrl);
			// set link
			session.setAttribute("downloadPath", downloadUrl);
			/**ATTT duong dan*/
			MemcachedUtils.putValueToMemcached(reportToken, downloadPath, retrieveReportMemcachedTimeout());	
			Date sysDateCrt = commonMgr.getSysDate();
			for (int i = 0; i < lstIdRecord.size(); i++) {
				if (lstIdRecord.get(i) != null) {
					EquipDeliveryRecord e = commonMgr.getEntityById(EquipDeliveryRecord.class, lstIdRecord.get(i));
					if (e != null ) {
						e.setPrintUser(currentUser.getUserName());
						e.setPrintDate(sysDateCrt);
						commonMgr.updateEntity(e);
					}
				}
			}
	
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Xuat excel bien ban giao nhan
	 * 
	 * @author tamvnm
	 * @since 23/04/2015
	 * @return
	 * 
	 * @description update Lay file mau .docx tu Quan ly mau bao cao
	 * @author update nhutnn
	 * @since update 19/08/2015 
	 */
	public String printRecord() {
		final String COLON = ": ";
		final String MONTH = " tháng";
		final String DONG = " đồng";
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.respon.not.data"));
				return JSON;
			}
			/**ATTT duong dan*/
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			List<String> exportedFilesName = new ArrayList<String>();
			
			for (int i = 0; i < lstIdRecord.size(); i++) {
				EquipDeliveryRecord equipDeliveryRecord = commonMgr.getEntityById(EquipDeliveryRecord.class, lstIdRecord.get(i));
				if (equipDeliveryRecord != null) {
					Shop shopMien = equipDeliveryRecord.getToShop().getParentShop();
					if (shopMien != null) {
						mien = shopMien.getShopCode();
					}
					equipDeliveryRecord.setToAddress(equipDeliveryRecord.getToPermanentAddress());
					listObjectBeans = new ArrayList<Map<String, Object>>();
					Map<String, Object> beans = new HashMap<String, Object>();
					beans.put("numberContract", equipDeliveryRecord.getContractNumber());
					beans.put("fromShopName", equipDeliveryRecord.getFromObjectName());
					beans.put("fromShopAddess", equipDeliveryRecord.getFromObjectAddress());
					beans.put("fromPhone", equipDeliveryRecord.getFromObjectPhone());
					beans.put("fromTax", equipDeliveryRecord.getFromObjectTax());
					beans.put("fromRepresentative", equipDeliveryRecord.getFromRepresentative());
					beans.put("fromPosition", equipDeliveryRecord.getFromPosition());
					beans.put("fromPage", equipDeliveryRecord.getFromPage());
					beans.put("fromPagePlace", equipDeliveryRecord.getFromPagePlace());
					if (equipDeliveryRecord.getFromPageDate() != null) {
						beans.put("fromPageDate", DateUtil.toDateString(equipDeliveryRecord.getFromPageDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					}
					Customer c = equipDeliveryRecord.getCustomer();
					if (c != null) {
						beans.put("toCustomerName", c.getCustomerName());
						beans.put("toCustomerCode", c.getShortCode());
					}
					
					beans.put("toAddress", equipDeliveryRecord.getToPermanentAddress());
					beans.put("toObjectAddress", equipDeliveryRecord.getToObjectAddress());
					beans.put("toPhone", equipDeliveryRecord.getToPhone());
					beans.put("toBusinessLicense", equipDeliveryRecord.getToBusinessLincense());
					beans.put("toBusinessPlace", equipDeliveryRecord.getToBusinessPlace());
					if (equipDeliveryRecord.getToBusinessDate() != null) {
						beans.put("toBusinessDate", DateUtil.toDateString(equipDeliveryRecord.getToBusinessDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					}
					beans.put("toRepresentative", equipDeliveryRecord.getToRepresentative());
					beans.put("toPosition", equipDeliveryRecord.getToPosition());
					beans.put("idNO", equipDeliveryRecord.getToIdNO());
					beans.put("idNOPlace", equipDeliveryRecord.getToIdNOPlace());
					if (equipDeliveryRecord.getToIdNODate() != null) {
						beans.put("idNODate", DateUtil.toDateString(equipDeliveryRecord.getToIdNODate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					}
					
					Date contractDate = equipDeliveryRecord.getContractCreateDate();
					if(contractDate != null){
						String dateContract = DateUtil.toDateString(contractDate, DateUtil.DATE_FORMAT_DDMMYYYY);
						String day = "ngày " + dateContract.substring(0, 2);
						String month = " tháng " +  dateContract.substring(dateContract.indexOf("/")+1,dateContract.lastIndexOf("/"));
						String year = " năm " + dateContract.substring(dateContract.length()-4);		
						beans.put("contractDate", day + month + year);
					}			
					if (equipDeliveryRecord.getFreezer() != null) {
						beans.put("freezer", equipDeliveryRecord.getFreezer());
					} else {
						beans.put("freezer", ".................");
					}
					if (equipDeliveryRecord.getRefrigerator() != null) {
						beans.put("refrigerator", equipDeliveryRecord.getRefrigerator());
					} else {
						beans.put("refrigerator", ".................");
					}
					beans.put("curUser", currentUser.getUserName());
					beans.put("printDate", DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY));
					if (equipDeliveryRecord.getStaff() != null) {
						beans.put("toStaffName", equipDeliveryRecord.getStaff().getStaffName());
					}
					
					EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
					equipmentFilter.setkPaging(null);
					equipmentFilter.setIdRecordDelivery(lstIdRecord.get(i));
					ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentDeliveryVOByFilter(equipmentFilter);
					if (equipmentDeliverys != null) {
						List<EquipmentDeliveryVO> lst = equipmentDeliverys.getLstObject();
						if (lst != null && lst.size() > 0) {
							for (int j = 0, szj = lst.size(); j < szj; j++) {
								EquipmentDeliveryVO equipmentDeliveryVO = lst.get(j);
								if (equipmentDeliveryVO.getPrice() != null) {
									equipmentDeliveryVO.setPriceMoney(StringUtil.convertMoney(equipmentDeliveryVO.getPrice()) + DONG);
								}
								if (equipmentDeliveryVO.getDepreciation() != null) {
									equipmentDeliveryVO.setDepreciationMonth(equipmentDeliveryVO.getDepreciation().toString() + MONTH);
								}
							}
						}
						beans.put("lstEquipmentDeliveryVOs", lst);
					}
					if (equipDeliveryRecord.getToShop() != null) {
						beans.put("toShopName", equipDeliveryRecord.getToObjectName());
						beans.put("toShopAddress", equipDeliveryRecord.getToShop().getAddress() == null ? ".................................." : equipDeliveryRecord.getToShop().getAddress());
						if (equipDeliveryRecord.getToShop().getPhone() != null) {
							beans.put("toShopPhone", equipDeliveryRecord.getToShop().getPhone());
						} else {
							beans.put("toShopPhone", equipDeliveryRecord.getToShop().getMobiphone() == null ? "..............." : equipDeliveryRecord.getToShop().getMobiphone());
						}

					}

					listObjectBeans.add(beans);

					// lay file mau .docx tu Quan ly mau bao cao
					Shop shop = equipDeliveryRecord.getToShop();
					ReportUrl reportUrl = null;
					while (shop != null && reportUrl == null) {
						reportUrl = reportManagerMgr.getReportFileName(shop.getId(), R.getResource("equipment.delivery.template.code"));
						if (reportUrl == null) {
							shop = shop.getParentShop();
						}
					}
					if (reportUrl == null || (reportUrl != null && StringUtil.isNullOrEmpty(reportUrl.getUrl()))) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("equip.proposal.code", equipDeliveryRecord.getCode()) + COLON + R.getResource("rpt.template.not.found"));
						return JSON;
					}
					// kiem tra file mau
					String nameFile = reportUrl.getUrl();
					int idx = nameFile.lastIndexOf(FileExtension.DOCX.getValue());
					if (idx < 0) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("equip.proposal.code", equipDeliveryRecord.getCode()) + COLON + R.getResource("equipment.delivery.template.error.file"));
						return JSON;
					}
					String templatePath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportTemplateImportPhysicalPath() + nameFile;
					File fileReport = new File(templatePath);
					if (!fileReport.exists()) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("equip.proposal.code", equipDeliveryRecord.getCode()) + COLON + R.getResource("rpt.template.not.found"));
						return JSON;
					}

					//String exportFileName = exportWordContract(beans, Configuration.getEquipmentPrintTemplatePath(), ConstantManager.TEMPLATE_PRINT_RECORD_LEND + FileExtension.DOCX.getValue());
					String exportFileName = exportWordContract(beans, ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportTemplateImportPhysicalPath(), nameFile);

					Date sysDateCrt = commonMgr.getSysDate();
					for (int j = 0; j < lstIdRecord.size(); j++) {
						if (lstIdRecord.get(j) != null) {
							EquipDeliveryRecord e = commonMgr.getEntityById(EquipDeliveryRecord.class, lstIdRecord.get(j));
							if (e != null) {
								e.setPrintUser(currentUser.getUserName());
								e.setPrintDate(sysDateCrt);
								commonMgr.updateEntity(e);
							}
						}
					}
					
					exportedFilesName.add(exportFileName);
				}
				
				// Xuat hop dong dinh dang Word
				if (exportedFilesName != null) {
					String downloadFilePath = null;
					if (exportedFilesName.size() == 1) {
						downloadFilePath = Configuration.getExportExcelPath() + File.separator + exportedFilesName.get(0);
					} else {
						final String compressedFileName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_FILE_EXPORT) + "_" + ConstantManager.TEMPLATE_PRINT_RECORD_LEND + FileExtension.ZIP.getValue();
						compressAllContracts(exportedFilesName, compressedFileName, Configuration.getStoreRealPath());
						downloadFilePath = Configuration.getExportExcelPath() + File.separator + compressedFileName;
					}
					result.put(ERROR, false);
					String downloadUrl = ReportUtils.addReportTokenToDownloadFileUrl(downloadFilePath, reportToken);
					result.put(REPORT_PATH, downloadUrl);
					MemcachedUtils.putValueToMemcached(reportToken, downloadFilePath, retrieveReportMemcachedTimeout());
				}
				
				/*JRDataSource dataSource = new JRBeanCollectionDataSource(listObjectBeans);
				String error = createPDF(ConstantManager.TEMPLATE_PRINT_RECORD_LEND, dataSource);
				if (!error.isEmpty()) {
					result.put("errMsg",error);
					result.put(ERROR, true);
					return JSON;
				}*/
				/*result.put(ERROR, false);
				String downloadUrl = ReportUtils.addReportTokenToDownloadFileUrl(downloadPath, reportToken);
				result.put(REPORT_PATH, downloadUrl);
				// set link
				session.setAttribute("downloadPath", downloadUrl);
				MemcachedUtils.putValueToMemcached(reportToken, downloadPath, retrieveReportMemcachedTimeout());*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Nen cac file hop dong vao 1 file zip
	 * @author tuannd20
	 * @param exportedFilesName
	 * @param compressedFileName
	 * @param compressedFilePath
	 * @return Duong dan toi file download
	 * @throws Exception 
	 * @since 21/06/2015
	 */
	private String compressAllContracts(List<String> exportedFilesName, String compressedFileName, String compressedFilePath) throws Exception {
		if (exportedFilesName == null || exportedFilesName.size() == 0) {
			return null;
		}
		List<File> files = new ArrayList<File>();
		for (String exportedFileName : exportedFilesName) {
			files.add(new File(compressedFilePath + File.separatorChar + exportedFileName));
		}
		FileUtility.compress(files, compressedFileName, compressedFilePath);
		return compressedFilePath + File.separatorChar + compressedFileName;
	}

	/**
	 * Xuat hop dong dinh dang Word
	 * @author tuannd20
	 * @param contractBean Doi tuong du lieu tren moi hop dong
	 * @param equipmentPrintTemplateFolderPath Duong dan thu muc file template in thiet bi
	 * @param templateFileName Ten file mau
	 * @param templatePrintRecordLend 
	 * @param fileExtension Phan mo rong cua file
	 * @return Duong dan toi file download
	 * @throws IOException 
	 * @since 20/06/2015
	 */
	private String exportWordContract(Map<String, Object> bean,
									final String equipmentPrintTemplateFolderPath, final String templateFileName) 
									throws IOException {
		if (bean == null) {
			return null;
		}
		
//		final String sourceTemplateFileAbsoluteName = ServletActionContext.getServletContext().getRealPath("/") + File.separatorChar + equipmentPrintTemplateFolderPath + File.separator + templateFileName;
		final String sourceTemplateFileAbsoluteName = File.separatorChar + equipmentPrintTemplateFolderPath + File.separator + templateFileName;
		String exportFileName = cloneOriginContractTemplate(equipmentPrintTemplateFolderPath, templateFileName);
		final String exportFileAbsolutePath = Configuration.getStoreRealPath() + File.separator + exportFileName;
		File docFile = new File(sourceTemplateFileAbsoluteName);
		
		FileInputStream fis = null;
		FileOutputStream out = null;
        if (!docFile.exists()) {
            throw new FileNotFoundException("The Word dcoument " + exportFileAbsolutePath + " does not exist.");
        }
        try {
            fis = new FileInputStream(docFile);
            XWPFDocument doc = new XWPFDocument(fis);
            
            List<XWPFParagraph> paragraphs = doc.getParagraphs();
            
            Pattern pattern = Pattern.compile("\\{(\\w+)\\}");
            
            /*
             * fill text in contract
             */
            for (int i = 0, pSize = paragraphs.size(); i < pSize; i++) {
            	XWPFParagraph paragraph = paragraphs.get(i);
//            	System.out.println("[" + (i + 1) + "] paragraph text: " + paragraph.getText());
            	List<XWPFRun> runs = paragraph.getRuns();
//            	System.out.println(" - runs: " + runs);
                for (XWPFRun run : runs) {
                	String text = run.getText(0);
                	
    				if (text != null && text.trim().length() != 0) {
    					Matcher matcher = pattern.matcher(text);
    					if (matcher.find()) {
    						String key = matcher.group(1);
    						String value = "";
    						if (bean.containsKey(key)) {
    							value = bean.get(key) != null ? bean.get(key).toString() : "";
    						}
    						text = replaceText(text, key, value);
    						run.setText(text, 0);
    					}
    				}
    			}
//                System.out.println(" - paragraph text new: " + paragraph.getText());
			}
            
            /*
             * fill data in table
             */
            List<EquipmentDeliveryVO> tableRowData = (List<EquipmentDeliveryVO>) bean.get("lstEquipmentDeliveryVOs");
            List<XWPFTable> tables = doc.getTables();
            if (tables != null && tables.size() > 0) {
            	/*
            	 * equipment table
            	 */
            	XWPFTable table = tables.get(0);
            	for (int i = 0, size = tableRowData.size(); i < size; i++) {
            		EquipmentDeliveryVO equipmentDeliveryVO = tableRowData.get(i);
            		int rowIdx = i + 1;
            		XWPFTableRow row = table.getRow(rowIdx);
            		if (row == null) {
            			table.insertNewTableRow(rowIdx);
            			row = table.getRow(rowIdx);
            		}
            		
            		int cellIdx = 0;
            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getTypeEquipment());
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getGroupEquipmentName());
            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getEquipmentBrand());
            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getSeriNumber());
            		//putCellValue(row, cellIdx++, equipmentDeliveryVO.getPrice().toString());
            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getPriceMoney());
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getDepreciation().toString());
            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getDepreciationMonth());
            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getHealthStatus());
            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getFirstYearInUse());
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getTypeEquipment(), true);
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getGroupEquipmentName(), true);
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getSeriNumber(), true);
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getPrice().toString(), true);
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getDepreciation().toString(), true);
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getHealthStatus(), true);
//            		putCellValue(row, cellIdx++, equipmentDeliveryVO.getFirstYearInUse(), true);
            	}
            	
            	/*
            	 * signature table
            	 */
            	table = tables.get(2);
            	for (int i = 0, numCell = 2; i < numCell; i++) {
            		paragraphs = table.getRow(1).getCell(i).getParagraphs();	// table from template, NOT NULL
            		for (XWPFParagraph paragraph : paragraphs) {
            			List<XWPFRun> runs = paragraph.getRuns();
            			for (XWPFRun run : runs) {
            				String text = run.getText(0);
            				Matcher matcher = pattern.matcher(text);
            				while (matcher.find()) {
            					String key = matcher.group(1);
            					String value = "";
            					if (bean.containsKey(key)) {
            						value = bean.get(key) != null ? bean.get(key).toString() : "";
            					}
            					text = replaceText(text, key, value);
            				}
            				run.setText(text, 0);
            			}
            		}
            	}
            }
    		
            /*
             * fill footer
             */
    		XWPFHeaderFooterPolicy policy = doc.getHeaderFooterPolicy();
    		XWPFFooter defaultFooter = policy.getDefaultFooter();
    		List<XWPFParagraph> footerParagraphs = defaultFooter.getParagraphs();
    		XWPFParagraph firstFooterParagraph = footerParagraphs.get(0);
    		List<XWPFRun> runs = firstFooterParagraph.getRuns();
//    		System.out.println(runs);
    		for (XWPFRun run : runs) {
            	String text = run.getText(0);
				if (text != null && text.trim().length() != 0) {
					Matcher matcher = pattern.matcher(text);
					if (matcher.find()) {
						String key = matcher.group(1);
						String value = "";
						if (bean.containsKey(key)) {
							value = bean.get(key) != null ? bean.get(key).toString() : "";
						}
						text = replaceText(text, key, value);
						run.setText(text, 0);
					}
				}
			}
    		
    		XWPFParagraph secondFooterParagraph = footerParagraphs.get(1);
    		runs = secondFooterParagraph.getRuns();
//    		System.out.println(runs);
    		for (XWPFRun run : runs) {
            	String text = run.getText(0);
				if (text != null && text.trim().length() != 0) {
					Matcher matcher = pattern.matcher(text);
					if (matcher.find()) {
						String key = matcher.group(1);
						String value = "";
						if (bean.containsKey(key)) {
							value = bean.get(key).toString();
						}
						text = replaceText(text, key, value);
						run.setText(text, 0);
					}
				}
			}
            
            out = new FileOutputStream(exportFileAbsolutePath);
            doc.write(out);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                    fis = null;
                } catch (IOException ioEx) {
                    System.out.println("IOException caught trying to close FileInputStream in the constructor of UpdateEmbeddedDoc.");
                }
            }
            if (out != null) {
            	out.close();
            	out = null;
            }
        }
        
		return exportFileName;
	}

	/**
	 * Dien gia tri vao cell
	 * @author tuannd20
	 * @param row
	 * @param cellIdx
	 * @param equipmentDeliveryVO
	 * @since 21/06/2015
	 */
	private void putCellValue(XWPFTableRow row, int cellIdx, String cellValue) {
		XWPFTableCell cell = row.getCell(cellIdx);
		if (cell == null) {
			row.addNewTableCell();
			cell = row.getCell(cellIdx);
		}
		cell.setText(cellValue);
	}
	
	private void putCellValue(XWPFTableRow row, int cellIdx, String cellValue, Boolean isSettingTableCellValue) {
		XWPFTableCell cell = row.getCell(cellIdx);
		if (cell == null) {
			row.addNewTableCell();
			cell = row.getCell(cellIdx);
		}
		
		List<XWPFParagraph> paragraphs = cell.getParagraphs();
		if (paragraphs != null && paragraphs.size() > 0) {
			for (int i = 0, isize = paragraphs.size(); i < isize; i++) {
				XWPFParagraph paragraph = paragraphs.get(i);
				List<XWPFRun> runs = paragraph.getRuns();
				if (runs == null || runs.size() == 0) {
					XWPFRun run = paragraph.createRun();
					runs.add(run);
					runs.add(paragraph.createRun());//khong the add run
				}
				runs.get(i).setFontFamily("Arial");
				runs.get(i).setFontSize(9);
				runs.get(i).setText(cellValue);
			}
			
		}
	}
	
	/**
	 * Thay the gia tri "key" trong chuoi "text" bang "value"
	 * @param source Chuoi chua gia tri can thay the
	 * @param placeholderKey Vi tri thay the
	 * @param replacedValue Gia tri thay the
	 * @return Gia tri sau khi thay the
	 * @since 21/06/2015
	 */
	private String replaceText(String source, String placeholderKey, String replacedValue) {
		return source.replace("{" + placeholderKey + "}", replacedValue);
		
	}
	
	/**
	 * Tao file xuat hop dong tu file template
	 * @author tuannd20
	 * @param equipmentPrintTemplateFolderPath
	 * @param templateFileName
	 * @return Ten file hop dong tai ve
	 * @throws IOException 
	 * @since 21/06/2015
	 */
	private String cloneOriginContractTemplate(String equipmentPrintTemplateFolderPath, String templateFileName) throws IOException {
		//final String sourceTemplateFileAbsoluteName = ServletActionContext.getServletContext().getRealPath("/") + File.separatorChar + equipmentPrintTemplateFolderPath + File.separator + templateFileName;
		final String sourceTemplateFileAbsoluteName = equipmentPrintTemplateFolderPath + File.separator + templateFileName;
		
		String exportFileName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE_MILISC) + "_" + templateFileName;
		final String exportTemplateFileAbsolutePath = Configuration.getStoreRealPath() + File.separator + exportFileName;
		FileUtility.copyFile(sourceTemplateFileAbsoluteName, exportTemplateFileAbsolutePath);
		
		return exportFileName;
	}

	/**
	 * in bien ban cho muon
	 * 
	 * @author tamvnm
	 * @since 22/04/2015
	 */
	public String printRecordLend (){
		if (lstIdRecord == null || lstIdRecord.size() == 0) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.respon.not.data"));
			return JSON;
		}
		/**ATTT duong dan*/
		String reportToken = retrieveReportToken(reportCode);
		if (StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		if (printType == null) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.respon.not.record"));
			return JSON;
		}
		String fileName = "";
		String outputPath = "";
		switch (printType) {
		case PRINT_PDA:
			fileName = ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_PDA;
			outputPath = getOutputExcelFile(printType,fileName);
			break;
		case PRINT_XNKV:
			fileName = ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_XNKV;
			outputPath = getOutputExcelFile(printType,fileName);
			break;
		case PRINT_NPP:
			fileName = ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_NPP;
			outputPath = getOutputExcelFileNPP(reportToken, false);
			break;
		case PRINT_PDA_XNKV_NPP:
			getOutputExcelFile(PRINT_PDA,ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_PDA);
			getOutputExcelFile(PRINT_XNKV,ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_XNKV);
			getOutputExcelFileNPP(reportToken, true);
			
			if (lstPathReal != null && lstPathReal.size() > 0 && !StringUtil.isNullOrEmpty(fileNamePDAReal) && !StringUtil.isNullOrEmpty(fileNameXNKVReal)) {
				lstPathReal.add(fileNamePDAReal);
				lstPathReal.add(fileNameXNKVReal);
				try {
					outputPath = FileUtility.createZipFromListFile(lstPathReal, ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_ALL_ZIP, null);
				} catch (Exception ex) {
					LogUtility.logError(ex, ex.getMessage());
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
					result.put(ERROR, true);
				}
			}

			break;
		default:
			break;
		}
		if (!StringUtil.isNullOrEmpty(outputPath)) {
			try {
				Date sysDateCrt = commonMgr.getSysDate();
				for (int i = 0; i < lstIdRecord.size(); i++) {
					if (lstIdRecord.get(i) != null) {
						EquipDeliveryRecord e = commonMgr.getEntityById(EquipDeliveryRecord.class, lstIdRecord.get(i));
						if (e != null ) {
							e.setPrintUser(currentUser.getUserName());
							e.setPrintDate(sysDateCrt);
							commonMgr.updateEntity(e);
						}
					}
				}
				result.put(REPORT_PATH, outputPath);
				result.put(ERROR, false);
				result.put("hasData", true);
				/**ATTT duong dan*/
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				result.put(ERROR, true);
			}
		} else {
			result.put(REPORT_PATH, "");
			result.put(ERROR, true);
			result.put("hasData", false);
		}

		return JSON;
	}
	
	/**
	 * format data cho in excel NPP
	 * 
	 * @author tamvnm
	 * @since 22/04/2015
	 */
	public List<List<EquipmentDeliveryPrintVO>> formatdata (List<EquipmentDeliveryPrintVO> equipmentDeliveryRecordVO){
		List<List<EquipmentDeliveryPrintVO>> result = new ArrayList<List<EquipmentDeliveryPrintVO>>();
		if (equipmentDeliveryRecordVO != null && equipmentDeliveryRecordVO.size() > 0) {
			if (equipmentDeliveryRecordVO.size() == 1) {
				result.add(equipmentDeliveryRecordVO);
				return result;
			}
			Long shopId = equipmentDeliveryRecordVO.get(0).getShopId();
			EquipmentDeliveryPrintVO ed = new EquipmentDeliveryPrintVO();
			List<EquipmentDeliveryPrintVO> lst = new ArrayList<EquipmentDeliveryPrintVO>();
			for (int i = 0, isize = equipmentDeliveryRecordVO.size(); i < isize; i++) {
				ed = equipmentDeliveryRecordVO.get(i);
				if (!shopId.equals(ed.getShopId())) {
					shopId = ed.getShopId();
					result.add(lst);
					
					lst = new ArrayList<EquipmentDeliveryPrintVO>();
					lst.add(ed);
					ed = new EquipmentDeliveryPrintVO();
				} else {
					lst.add(ed);
					
				}
				if (i == equipmentDeliveryRecordVO.size() - 1) {
					result.add(lst);
				}
			}
		}
		return result;
	}
	
	/**
	 * tao file excel NPP de in
	 * 
	 * @author tamvnm
	 * @since 22/04/2015
	 */
	public String getOutputExcelFileNPP(String reportToken, Boolean isAll){
		String outputPath = "";
		String shopCode =  "";
		String shopName = "";
//		String mien = "MIỀN: ";
		List<String> lstPath = new ArrayList<String>();
		lstPathReal = new ArrayList<String>();
		try {
			List<EquipmentDeliveryPrintVO> equipmentDeliveryRecordVO = equipmentManagerMgr.getListEquipDeliveryRecordForPrint(lstIdRecord);
//			if (equipmentDeliveryRecordVO != null && equipmentDeliveryRecordVO.size() > 0) {
//				mien += equipmentDeliveryRecordVO.get(0).getMien().toUpperCase();
//			}
			List<CellBean> lstPrint = new ArrayList<CellBean>();
			if (equipmentDeliveryRecordVO != null) {
				List<List<EquipmentDeliveryPrintVO>> lst = formatdata(equipmentDeliveryRecordVO);
				for (int j = 0, jsize = lst.size(); j < jsize; j++ ) {
					List<EquipmentDeliveryPrintVO> equipPrint = lst.get(j);
					if (equipPrint != null && equipPrint.size() > 0 ) {
						for (int i = 0, isize = equipPrint.size(); i < isize; i++) {
							CellBean c = new CellBean();
							c.setContent1((i + 1) + "");
//							c.setContent2(equipPrint.get(i).getMien());
							c.setContent3(equipPrint.get(i).getShopCode() + " - " + equipPrint.get(i).getShopName());
							if (equipPrint.get(i).getCustomerCode() != null && equipPrint.get(i).getShopCode() != null) {
								c.setContent4(equipPrint.get(i).getCustomerCode());
							} else {
								c.setContent4("");
							}
							
							c.setContent5(equipPrint.get(i).getCustomerName());
							c.setContent6(equipPrint.get(i).getRepresentative());
							c.setContent7(equipPrint.get(i).getToPhone());
							c.setContent8(equipPrint.get(i).getHouseNumber());
							c.setContent9(equipPrint.get(i).getStreet());
							c.setContent10(equipPrint.get(i).getWard());
							c.setContent11(equipPrint.get(i).getDistrict());
							c.setContent12(equipPrint.get(i).getCity());
							if (equipPrint.get(i).getNumberEquip() != null) {
								c.setContent13(equipPrint.get(i).getNumberEquip().toString());
							} else {
								c.setContent13("");
							}
							
							if (equipPrint.get(i).getEquipCategory() != null && equipPrint.get(i).getEquipGroup() != null) {
								c.setContent14(equipPrint.get(i).getEquipCategory() + " " + equipPrint.get(i).getEquipGroup());
							} else {
								c.setContent14("");
							}
							
							c.setContent15(equipPrint.get(i).getHealthStatus());
							c.setContent16(equipPrint.get(i).getThoiGianNhan());
							c.setContent17(equipPrint.get(i).getSeri());
							c.setContent18(equipPrint.get(i).getStaffName());
							c.setContent19(equipPrint.get(i).getStaffPhone());
							shopCode = equipPrint.get(i).getShopCode();
							shopName = equipPrint.get(i).getShopName();
							lstPrint.add(c);
						}
						if (lstPrint.size() > 0) {
							numFail = lstPrint.size();
							String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_NPP;
							templateFileName = templateFileName.replace('/', File.separatorChar);
							String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)+ "_" + shopCode + "_" + ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_NPP;
							String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
							outputFileName = outputFileName.replace('/', File.separatorChar);
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("report", lstPrint);
//							params.put("mien", mien);
							params.put("title2", String.format(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.NPP.title2"),shopName));
							params.put("title3", String.format(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.NPP.title3"),shopName));
							params.put("title4", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.giamdocduan"));
							XLSTransformer transformer = new XLSTransformer();
							try {
								transformer.transformXLS(templateFileName, params, outputFileName);
							} catch (ParsePropertyException e) {
								LogUtility.logError(e, e.getMessage());
							} catch (InvalidFormatException e) {
								LogUtility.logError(e, e.getMessage());
							} catch (IOException e) {
								LogUtility.logError(e, e.getMessage());
							}
							lstPath.add(Configuration.getStoreImportFailDownloadPath() + outputName);
							lstPathReal.add(outputFileName);
							lstPrint = new ArrayList<CellBean>();
						}
											}
				}
				if (lst.size() == 1 && lstPath != null && lstPath.size() == 1) {
					return lstPath.get(0);
				}
				if (!isAll) {
					outputPath = FileUtility.createZipFromListFile(lstPathReal, ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_NPP_ZIP, null);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return outputPath;
	}
	
	/**
	 * tao file excel phong du an. XNKV de in
	 * 
	 * @author tamvnm
	 * @since 22/04/2015
	 */
	public String getOutputExcelFile(int printNumber,String templateName) {
		String fileName = "";
//		String mien = "MIỀN: ";
		try {
			List<EquipmentDeliveryPrintVO> equipmentDeliveryRecordVO = equipmentManagerMgr.getListEquipDeliveryRecordForPrint(lstIdRecord);
//			if (equipmentDeliveryRecordVO != null && equipmentDeliveryRecordVO.size() > 0) {
//				mien += equipmentDeliveryRecordVO.get(0).getMien().toUpperCase();
//			}
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if (equipmentDeliveryRecordVO != null) {
				for (int i = 0; i < equipmentDeliveryRecordVO.size(); i++) {
					CellBean c = new CellBean();
					c.setContent1((i + 1) + "");
//					c.setContent2(equipmentDeliveryRecordVO.get(i).getMien());
					c.setContent3(equipmentDeliveryRecordVO.get(i).getShopCode() + " - " + equipmentDeliveryRecordVO.get(i).getShopName());
					if (equipmentDeliveryRecordVO.get(i).getCustomerCode() != null && equipmentDeliveryRecordVO.get(i).getShopCode() != null) {
						c.setContent4(equipmentDeliveryRecordVO.get(i).getCustomerCode());
					} else {
						c.setContent4("");
					}
//					c.setContent4(equipmentDeliveryRecordVO.get(i).getCustomerCode());
					
					c.setContent5(equipmentDeliveryRecordVO.get(i).getCustomerName());
					c.setContent6(equipmentDeliveryRecordVO.get(i).getRepresentative());
					c.setContent7(equipmentDeliveryRecordVO.get(i).getToPhone());
					c.setContent8(equipmentDeliveryRecordVO.get(i).getHouseNumber());
					c.setContent9(equipmentDeliveryRecordVO.get(i).getStreet());
					c.setContent10(equipmentDeliveryRecordVO.get(i).getWard());
					c.setContent11(equipmentDeliveryRecordVO.get(i).getDistrict());
					c.setContent12(equipmentDeliveryRecordVO.get(i).getCity());
					c.setContent13(equipmentDeliveryRecordVO.get(i).getNumberEquip().toString());
					c.setContent14(equipmentDeliveryRecordVO.get(i).getEquipCategory() + " " + equipmentDeliveryRecordVO.get(i).getEquipGroup());
					c.setContent15(equipmentDeliveryRecordVO.get(i).getHealthStatus());
					c.setContent16(equipmentDeliveryRecordVO.get(i).getThoiGianNhan());
					c.setContent17(equipmentDeliveryRecordVO.get(i).getSeri());
					c.setContent18(equipmentDeliveryRecordVO.get(i).getStaffName());
					c.setContent19(equipmentDeliveryRecordVO.get(i).getStaffPhone());
					lstFails.add(c);
				}
				if (lstFails.size() > 0) {
					numFail = lstFails.size();
					String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + templateName;
					templateFileName = templateFileName.replace('/', File.separatorChar);
					String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + templateName;
					String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
					outputFileName = outputFileName.replace('/', File.separatorChar);
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("report", lstFails);
//					params.put("mien", mien);
					switch (printNumber) {
					case PRINT_PDA:
//						params.put("title1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.PDA.title1"));
						params.put("title2", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.PDA.title2"));
						params.put("title3", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.PDA.title3"));
						params.put("title4", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.giamdocduan"));
						break;
					case PRINT_XNKV:
//						params.put("title1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.XNKV.title1"));
						params.put("title2", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.XNKV.title2"));
						params.put("title3", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.XNKV.title3"));
						params.put("title4", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.giamdocXNKV"));
						break;
					default:
						break;
					}
					XLSTransformer transformer = new XLSTransformer();
					try {
						transformer.transformXLS(templateFileName, params, outputFileName);
					} catch (ParsePropertyException e) {
						LogUtility.logError(e, e.getMessage());
					} catch (InvalidFormatException e) {
						LogUtility.logError(e, e.getMessage());
					} catch (IOException e) {
						LogUtility.logError(e, e.getMessage());
					}
					fileName = Configuration.getStoreImportFailDownloadPath() + outputName;
					if (printNumber == PRINT_PDA) {
						fileNamePDAReal = outputFileName;
					} else if (printNumber == PRINT_XNKV) {
						fileNameXNKVReal = outputFileName;
					}
					
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return fileName;
	}
	
	/**
	 * tao file excel phong du an de in
	 * 
	 * @author tamvnm
	 * @since 22/04/2015
	 */
//	public void printPDA_XNKV(Boolean isPDA){
//		try {
//			if (lstIdRecord != null && lstIdRecord.size() > 0) {
//				
//				List<EquipmentDeliveryPrintVO> equipmentDeliveryRecordVO = equipmentManagerMgr.getListEquipDeliveryRecordForPrint(lstIdRecord);		
//				SXSSFWorkbook workbook = null;
//				FileOutputStream out = null;
//				try {
//					if (null != equipmentDeliveryRecordVO && equipmentDeliveryRecordVO.size() > 0) {
//						
//						// Init XSSF workboook
//						String outputName = "";
//						if (isPDA) {
//							outputName = ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_PDA + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX;
//						} else {
//							outputName = ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY_XNKV + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX;
//						}
//						
//						String exportFileName = Configuration.getStoreRealPath() + outputName;
//
//						workbook = new SXSSFWorkbook(-1);
//						workbook.setCompressTempFiles(true);
//						// Tao shett
//						SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
//						Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
//						ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();
//
//						// Set Getting Defaul
//						sheetData.setDefaultRowHeight((short) (15 * 20));
////						sheetData.setDefaultColumnWidth(18);
//						sheetData.setDefaultColumnWidth(40);
//						// set static Column width
//						mySheet.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100);
//						// Size Row
//						mySheet.setRowsHeight(sheetData, 0, 20);
//						sheetData.setDisplayGridlines(true);
//						// text style
//						XSSFCellStyle textStyle = (XSSFCellStyle)style.get(ExcelPOIProcessUtils.NORMAL).clone();
//						DataFormat fmt = workbook.createDataFormat();
//						textStyle.setDataFormat(fmt.getFormat("@"));
//						
//						int iRow = 0, iColumn = 0;
//						//tao header
//						mySheet.addCell(sheetData, iColumn, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.proposal.export.sheet1.title"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_13));
//						mySheet.addCell(sheetData, iColumn, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.donvi.TNKD"), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_13));
//						mySheet.addCell(sheetData, iColumn, iRow + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.proposal.export.sheet1.title2"), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_13));
//						
//						mySheet.addCell(sheetData, iColumn + 8, iRow + 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.title"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_20));
//						if (isPDA) {
//							mySheet.addCell(sheetData, iColumn + 8, iRow + 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.PDA.title2"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_14));
//							mySheet.addCell(sheetData, iColumn, iRow + 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.PDA.title3"), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_13));
//						} else {
//							mySheet.addCell(sheetData, iColumn + 8, iRow + 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.XNKV.title2"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_14));
//							mySheet.addCell(sheetData, iColumn, iRow + 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.XNKV.title3"), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_13));
//						}
//						
//						
//						
//						iColumn = 0;
//						iRow = 8;
//						// tao column header
//						String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.print.delivery");
//						String[] lstHeaders = headers.split(";");
//						// Size Row
//						mySheet.setRowsHeight(sheetData, 0, 40);
//						for (int i = 0; i < lstHeaders.length; i++) {
//							mySheet.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_TIMES_BOLD_BRODER_YELLOW_13));
//						}
//						
//						iColumn = 0;
//						iRow = 8;
//						for (int i = 0; i < equipmentDeliveryRecordVO.size(); i++) {
//							iColumn = 0; 
//							iRow++;
//							mySheet.addCell(sheetData, iColumn++, iRow, i + 1, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getMien(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getKenh(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getCustomerCode(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getCustomerName(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getRepresentative(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getToPhone(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getHouseNumber(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getStreet(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getWard(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getDistrict(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getCity(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getNumberEquip(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getEquipCategory() + " " + equipmentDeliveryRecordVO.get(i).getEquipGroup(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getHealthStatus(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getThoiGianNhan(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getSeri(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getStaffName(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentDeliveryRecordVO.get(i).getStaffPhone(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//							//cot ghi chu de trong
//							mySheet.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.TIMES_NORMAL_BRODER_12));
//						}
//						iRow++;
//						for (int i = 0; i < 20; i++) {
//							mySheet.addCell(sheetData, i, iRow, "", style.get(ExcelPOIProcessUtils.HEADER_GRAY_THIN));
//						}
//						// Size Row
//						mySheet.setRowsHeight(sheetData, 0, 20);
//						iColumn = 0; 
//						iRow++;
//						mySheet.addCell(sheetData, iColumn, iRow++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.title4"), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_13));
//						if (isPDA) {
//							mySheet.addCell(sheetData, 5, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.giamdocduan"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_13));
//						} else {
//							mySheet.addCell(sheetData, 5, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.giamdocXNKV"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_13));
//						}
//						
//						mySheet.addCell(sheetData, 9, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.giamdockho"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_13));
//						mySheet.addCell(sheetData, 14, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.TNKD"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_13));
//						mySheet.addCell(sheetData, 18, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.nguoidenghi"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_13));
//						
//						out = new FileOutputStream(exportFileName);
//						workbook.write(out);
//						out.close();
//						workbook.dispose();
//						String outputPath = Configuration.getExportExcelPath() + outputName;
//						result.put(REPORT_PATH, outputPath);
//						result.put(ERROR, false);
//						result.put("hasData", true);
//						for (int i = 0; i < lstIdRecord.size(); i++) {
//							EquipDeliveryRecord ed = commonMgr.getEntityById(EquipDeliveryRecord.class, lstIdRecord.get(i));
//							if (ed != null) {
//								ed.setPrintDate(new Date());
//								ed.setPrintUser(currentUser.getUserName());
//							}
//						}
//					} else {
//						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
//						result.put(ERROR, true);
//						result.put("data.hasData", true);
//						result.put("errMsg", errMsg);
//					}
//				} catch (Exception e) {
//					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
//					result.put(ERROR, true);
//					result.put("errMsg", errMsg);
//					LogUtility.logError(e, e.getMessage());
//					if (workbook != null) {
//						workbook.dispose();
//					}
//					if (out != null) {
//						out.close();
//					}
//				}
//			} else {
//				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
//				result.put(ERROR, true);
//				result.put("data.hasData", true);
//				result.put("errMsg", errMsg);
//			}
//		} catch (Exception e) {
//			result.put(ERROR, true);
//		    result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			LogUtility.logError(e, e.getMessage());
//		}
//	}	
	
	
	/**
	 * tao file PDF cho in
	 * 
	 * @author tamvnm
	 * @since 22/04/2015
	 */
	public String createPDF(String fileName, JRDataSource dataSource){
		String error = "";
		try {
			String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/print-template/";
//			String templatePath = folder + ConstantManager.TEMPLATE_PRINT_RECORD_DELIVERY+ ".jasper";
			String templatePath = folder + fileName + ".jasper";
			templatePath = templatePath.replace('/', File.separatorChar);
			File fileReport = new File(templatePath);
			if (!fileReport.exists()) {
				error =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.respon.not.template");
				return error;
			}
			// dat Parameters
			Map<String, Object> parameters = new HashMap<String, Object>();
//			parameters.put("kenh", kenh);
//			parameters.put("mien", mien);
	
	//		JRDataSource dataSource = new JRBeanCollectionDataSource(listObjectBeans);
	
			String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(fileName + ".pdf")).toString());
	
			// file output
			//String outputPath = Configuration.getStoreRealPath() + outputFile;
			String outputPath = Configuration.getStoreRealPath() + outputFile;
			downloadPath = Configuration.getExportExcelPath() + outputFile;
	
			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);
	
			JRAbstractExporter exporter = new JRPdfExporter();
	
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
			exporter.exportReport();
	
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			error = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}

		return error;

	}
	
	/**
	 * Mo trang applet
	 * 
	 * @author nhutnn
	 * @since 15/01/2015
	 */
	public String openTaxView() {
		downloadPath = (String) session.getAttribute("downloadPath");
		session.removeAttribute("downloadPath");
		return SUCCESS;
	}
	/**
	 * Kiem tra trung dong import excel
	 * 
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	private String checkLikeRow(List<List<String>> lstData, int j) {
		String message = "";
		try {
			if (lstData.get(j) == null) {
				return message;
			}
			List<String> row1 = lstData.get(j);
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) == null || k == j) {
					continue;
				}
				List<String> row2 = lstData.get(k);
				if (row2.size() != row1.size()) {
					continue;
				}
				if (row2.size() == 0) {
					continue;
				}
				if (row1.get(3).split("/").length < 2 || row2.get(3).split("/").length < 2) {
					continue;
				}
				Boolean isLike = true;
				// for(int e=0; e< row1.size();e++){
				for (int f = 0; f < row2.size(); f++) {
					if (!row1.get(f).trim().equalsIgnoreCase(row2.get(f).trim())) {
						isLike = false;
						break;
					}
				}
				/*
				 * if (!isLike){ break; }
				 */
				// }
				if (isLike) // Trang thai giao nhan
				{
					if (StringUtil.isNullOrEmpty(message)) {
						message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
					} else {
						message += ", " + (k + 2);
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());		
		}
		return message;
	}
	
	/**
	 * Kiem tra dong rong
	 * 
	 * @author tamvnm
	 * @since 13/04/2015
	 */
	private Boolean checkLineIsEmpty(List<String> row) {
		if(row == null) {
			return null;
		}
		for (int i = 0; i < row.size(); i++) {
			if(!StringUtil.isNullOrEmpty(row.get(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * lay so luong thiet bi de lam header khi tao file export bien ban
	 * 
	 * @author nhutnn
	 * @since 17/12/2014
	 * @param equipmentRecordDeliveryVOs
	 * @return
	 */
	private Integer countColExport(List<EquipmentRecordDeliveryVO> equipmentRecordDeliveryVOs) {
		Integer max = 0;
		for (int i = 0; i < equipmentRecordDeliveryVOs.size(); i++) {
			if (equipmentRecordDeliveryVOs.get(i).getLstEquipmentDeliveryVOs().size() > max) {
				max = equipmentRecordDeliveryVOs.get(i).getLstEquipmentDeliveryVOs().size();
			}
		}
		return max;
	}

	private Boolean checkNVGS(String staffCode, Long shopId){
		try {
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setShopId(shopId);
			
			Shop toShop = commonMgr.getEntityById(Shop.class, shopId);
			if (toShop != null && toShop.getType() != null) {
				if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
					filter.setIsLstChildStaffRoot(true);
					objVO = staffMgr.getListGSByStaffRootWithInParentStaffMap(filter);
				} else if (toShop != null && toShop.getType() != null && (toShop.getType().getSpecificType().equals(ShopSpecificType.NPP)) 
						|| toShop.getType().getSpecificType().equals(ShopSpecificType.NPP_MT)
						) {
					List<Integer> lstObjectType = new ArrayList<Integer>();
					lstObjectType.add(StaffSpecificType.GSMT.getValue());
//					lstObjectType.add(StaffObjectType.GSKA.getValue());
					lstObjectType.add(StaffSpecificType.SUPERVISOR.getValue());
					filter.setLstObjectType(lstObjectType);
					
					filter.setIsLstChildStaffRoot(false);
					objVO = staffMgr.getListGSByStaffRootKAMT(filter);
//				} else if (toShop != null && toShop.getType().getObjectType().equals(ShopObjectType.NPP_KA.getValue())) {
//					filter.setObjectType(StaffObjectType.GSKA.getValue());
//					filter.setIsLstChildStaffRoot(false);
//					objVO = staffMgr.getListGSByStaffRootKAMT(filter);
				} else {
					filter.setIsLstChildStaffRoot(false);
//					filter.setGska(StaffObjectType.GSKA.getValue());
					filter.setGsmt(StaffSpecificType.GSMT.getValue());
					objVO = staffMgr.getListGSByStaffRootWithNVBH(filter);
				}			
	
				for(int i=0; i< objVO.getLstObject().size(); i++){
					if(staffCode.toUpperCase().equals(objVO.getLstObject().get(i).getStaffCode())){
						return true;
					}
				}
			}
			
			
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());			
		}
		return false;
	}
	
	/**
	 * Upload File
	 * 
	 * @author nhutnn
	 * @since 10/02/2015
	 * */
	private List<FileVO> saveUploadFileByEquip(List<File> lstFile, List<String> lstFileFileName, List<String> lstFileContentType) {
		try {
			if (lstFile != null && lstFile.size() > 0) {
				//String perfix = Configuration.getPrefixFileEquipServerDocPath();
				List<FileVO> lstFileVO = FileUtility.storeFileOnDisk(lstFile, lstFileFileName, lstFileContentType, null, EquipTradeType.DELIVERY.getValue());
				if (lstFileVO != null && !lstFileVO.isEmpty()) {
					String thumbUrl = Configuration.getEquipThumbUrlAttachFile();
					for (FileVO voFile : lstFileVO) {
						voFile.setThumbUrl(thumbUrl);
					}
				}
				return lstFileVO;
			}
			System.gc();
		} catch (Exception e) {
			LogUtility.logError(e, "ManageDeliveryEquipmentAction.saveUploadFileByEquip()" + e.getMessage());
		}
		return new ArrayList<FileVO>();
	}
	

	/**
	 * format du lieu de xuat excel
	 * 
	 * @author Tamvnm
	 * @since 15/04/2015
	 * */
	public List<Integer> formatDateForExportExcel(List<EquipmentRecordDeliveryVO> lstdata){
		List<Integer> lstPosition = new ArrayList<Integer>();
		if(lstdata != null && lstdata.size() > 0){
			if (lstdata.size() == 1) {
				lstPosition.add(1);
				return lstPosition;
			} else {
				String recordCode = lstdata.get(0).getRecordCode();
				int count = 1;
				for (int i = 1; i < lstdata.size(); i++) {
					if (lstdata.get(i) != null) {
						if (recordCode.equals(lstdata.get(i).getRecordCode())) {
							count++;
						} else {
							lstPosition.add(count);
							count = 1;
							recordCode = lstdata.get(i).getRecordCode();
						}
						if (i == lstdata.size() - 1) {
							lstPosition.add(count);
						}
					}
				}
				
			}
		}
		return lstPosition;
	}
	
	
	// TODO Xu ly GETTER/SETTER
	/**
	 * 
	 * 
	 * */

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getNumberContract() {
		return numberContract;
	}

	public void setNumberContract(String numberContract) {
		this.numberContract = numberContract;
	}

	public String getRecordCode() {
		return recordCode;
	}

	public void setRecordCode(String recordCode) {
		this.recordCode = recordCode;
	}

	public Integer getStatusRecord() {
		return statusRecord;
	}

	public void setStatusRecord(Integer statusRecord) {
		this.statusRecord = statusRecord;
	}

	public Integer getStatusDelivery() {
		return statusDelivery;
	}

	public void setStatusDelivery(Integer statusDelivery) {
		this.statusDelivery = statusDelivery;
	}

	public Long getIdRecordDelivery() {
		return idRecordDelivery;
	}

	public void setIdRecordDelivery(Long idRecordDelivery) {
		this.idRecordDelivery = idRecordDelivery;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}

	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}

	public List<StaffVO> getLstGsnpp() {
		return lstGsnpp;
	}

	public void setLstGsnpp(List<StaffVO> lstGsnpp) {
		this.lstGsnpp = lstGsnpp;
	}

	public List<EquipmentDeliveryVO> getEquipments() {
		return equipments;
	}

	public void setEquipments(List<EquipmentDeliveryVO> equipments) {
		this.equipments = equipments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public List<String> getLstEquipCode() {
		return lstEquipCode;
	}

	public void setLstEquipCode(List<String> lstEquipCode) {
		this.lstEquipCode = lstEquipCode;
	}

	public List<String> getLstSeriNumber() {
		return lstSeriNumber;
	}

	public void setLstSeriNumber(List<String> lstSeriNumber) {
		this.lstSeriNumber = lstSeriNumber;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}


	public List<String> getLstContentDelivery() {
		return lstContentDelivery;
	}

	public void setLstContentDelivery(List<String> lstContentDelivery) {
		this.lstContentDelivery = lstContentDelivery;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getLstEquipAdd() {
		return lstEquipAdd;
	}

	public void setLstEquipAdd(String lstEquipAdd) {
		this.lstEquipAdd = lstEquipAdd;
	}

	public String getLstEquipDelete() {
		return lstEquipDelete;
	}

	public void setLstEquipDelete(String lstEquipDelete) {
		this.lstEquipDelete = lstEquipDelete;
	}

	public List<Map<String, Object>> getListObjectBeans() {
		return listObjectBeans;
	}

	public void setListObjectBeans(List<Map<String, Object>> listObjectBeans) {
		this.listObjectBeans = listObjectBeans;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public Integer getWindowWidth() {
		return windowWidth;
	}

	public void setWindowWidth(Integer windowWidth) {
		this.windowWidth = windowWidth;
	}

	public Integer getWindowHeight() {
		return windowHeight;
	}

	public void setWindowHeight(Integer windowHeight) {
		this.windowHeight = windowHeight;
	}

	public List<File> getLstFile() {
		return lstFile;
	}

	public void setLstFile(List<File> lstFile) {
		this.lstFile = lstFile;
	}

	public List<String> getLstFileContentType() {
		return lstFileContentType;
	}

	public void setLstFileContentType(List<String> lstFileContentType) {
		this.lstFileContentType = lstFileContentType;
	}

	public List<String> getLstFileFileName() {
		return lstFileFileName;
	}

	public void setLstFileFileName(List<String> lstFileFileName) {
		this.lstFileFileName = lstFileFileName;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public String getEquipAttachFileStr() {
		return equipAttachFileStr;
	}

	public void setEquipAttachFileStr(String equipAttachFileStr) {
		this.equipAttachFileStr = equipAttachFileStr;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getEquipLendCode() {
		return equipLendCode;
	}

	public void setEquipLendCode(String equipLendCode) {
		this.equipLendCode = equipLendCode;
	}

	public String getFromContractDate() {
		return fromContractDate;
	}

	public void setFromContractDate(String fromContractDate) {
		this.fromContractDate = fromContractDate;
	}

	public String getToContractDate() {
		return toContractDate;
	}

	public void setToContractDate(String toContractDate) {
		this.toContractDate = toContractDate;
	}

	public Integer getStatusPrint() {
		return statusPrint;
	}

	public void setStatusPrint(Integer statusPrint) {
		this.statusPrint = statusPrint;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getCurShopCode() {
		return curShopCode;
	}

	public void setCurShopCode(String curShopCode) {
		this.curShopCode = curShopCode;
	}

	public String getLstShop() {
		return lstShop;
	}

	public void setLstShop(String lstShop) {
		this.lstShop = lstShop;
	}

	public String getFromObjectAddress() {
		return fromObjectAddress;
	}

	public void setFromObjectAddress(String fromObjectAddress) {
		this.fromObjectAddress = fromObjectAddress;
	}

	public String getFromObjectTax() {
		return fromObjectTax;
	}

	public void setFromObjectTax(String fromObjectTax) {
		this.fromObjectTax = fromObjectTax;
	}

	public String getFromRepresentative() {
		return fromRepresentative;
	}

	public void setFromRepresentative(String fromRepresentative) {
		this.fromRepresentative = fromRepresentative;
	}

	public String getFromObjectPosition() {
		return fromObjectPosition;
	}

	public void setFromObjectPosition(String fromObjectPosition) {
		this.fromObjectPosition = fromObjectPosition;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String getFromPagePlace() {
		return fromPagePlace;
	}

	public void setFromPagePlace(String fromPagePlace) {
		this.fromPagePlace = fromPagePlace;
	}


	

	public String getFromPageDate() {
		return fromPageDate;
	}

	public void setFromPageDate(String fromPageDate) {
		this.fromPageDate = fromPageDate;
	}

	public String getToObjectAddress() {
		return toObjectAddress;
	}

	public void setToObjectAddress(String toObjectAddress) {
		this.toObjectAddress = toObjectAddress;
	}

	public String getToRepresentative() {
		return toRepresentative;
	}

	public void setToRepresentative(String toRepresentative) {
		this.toRepresentative = toRepresentative;
	}

	public String getToObjectPosition() {
		return toObjectPosition;
	}

	public void setToObjectPosition(String toObjectPosition) {
		this.toObjectPosition = toObjectPosition;
	}

	public BigDecimal getFreezer() {
		return freezer;
	}

	public void setFreezer(BigDecimal freezer) {
		this.freezer = freezer;
	}

	public BigDecimal getRefrigerator() {
		return refrigerator;
	}

	public void setRefrigerator(BigDecimal refrigerator) {
		this.refrigerator = refrigerator;
	}

	public String getToIdNO() {
		return toIdNO;
	}

	public void setToIdNO(String toIdNO) {
		this.toIdNO = toIdNO;
	}

	public String getToIdNOPlace() {
		return toIdNOPlace;
	}

	public void setToIdNOPlace(String toIdNOPlace) {
		this.toIdNOPlace = toIdNOPlace;
	}


	public String getToIdNODate() {
		return toIdNODate;
	}

	public void setToIdNODate(String toIdNODate) {
		this.toIdNODate = toIdNODate;
	}

	public String getFromObjectPhone() {
		return fromObjectPhone;
	}

	public void setFromObjectPhone(String fromObjectPhone) {
		this.fromObjectPhone = fromObjectPhone;
	}

	public String getFromShopCode() {
		return fromShopCode;
	}

	public void setFromShopCode(String fromShopCode) {
		this.fromShopCode = fromShopCode;
	}

	public String getToShopCode() {
		return toShopCode;
	}

	public void setToShopCode(String toShopCode) {
		this.toShopCode = toShopCode;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public List<Long> getLstEquipGroupId() {
		return lstEquipGroupId;
	}

	public void setLstEquipGroupId(List<Long> lstEquipGroupId) {
		this.lstEquipGroupId = lstEquipGroupId;
	}

	public String getLstEquipmentCode() {
		return lstEquipmentCode;
	}

	public void setLstEquipmentCode(String lstEquipmentCode) {
		this.lstEquipmentCode = lstEquipmentCode;
	}


	public List<Long> getLstIdChecked() {
		return lstIdChecked;
	}

	public void setLstIdChecked(List<Long> lstIdChecked) {
		this.lstIdChecked = lstIdChecked;
	}

	public List<Integer> getLstDepreciation() {
		return lstDepreciation;
	}

	public void setLstDepreciation(List<Integer> lstDepreciation) {
		this.lstDepreciation = lstDepreciation;
	}

	public String getToBusinessDate() {
		return toBusinessDate;
	}

	public void setToBusinessDate(String toBusinessDate) {
		this.toBusinessDate = toBusinessDate;
	}

	public String getToBusinessLicense() {
		return toBusinessLicense;
	}

	public void setToBusinessLicense(String toBusinessLicense) {
		this.toBusinessLicense = toBusinessLicense;
	}

	public String getToBusinessPlace() {
		return toBusinessPlace;
	}

	public void setToBusinessPlace(String toBusinessPlace) {
		this.toBusinessPlace = toBusinessPlace;
	}

	public Integer getPrintType() {
		return printType;
	}

	public void setPrintType(Integer printType) {
		this.printType = printType;
	}

	public String getFromFax() {
		return fromFax;
	}

	public void setFromFax(String fromFax) {
		this.fromFax = fromFax;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ApParamEquip> getLstContent() {
		return lstContent;
	}

	public void setLstContent(List<ApParamEquip> lstContent) {
		this.lstContent = lstContent;
	}

	public ApParamEquip getContentNew() {
		return ContentNew;
	}

	public void setContentNew(ApParamEquip contentNew) {
		ContentNew = contentNew;
	}

	public String getFileNamePDAReal() {
		return fileNamePDAReal;
	}

	public void setFileNamePDAReal(String fileNamePDAReal) {
		this.fileNamePDAReal = fileNamePDAReal;
	}

	public String getFileNameXNKVReal() {
		return fileNameXNKVReal;
	}

	public void setFileNameXNKVReal(String fileNameXNKVReal) {
		this.fileNameXNKVReal = fileNameXNKVReal;
	}

	public List<String> getLstPathReal() {
		return lstPathReal;
	}

	public void setLstPathReal(List<String> lstPathReal) {
		this.lstPathReal = lstPathReal;
	}

	public List<EquipmentVO> getLstEquipProvider() {
		return lstEquipProvider;
	}

	public void setLstEquipProvider(List<EquipmentVO> lstEquipProvider) {
		this.lstEquipProvider = lstEquipProvider;
	}

	public List<EquipmentVO> getLstEquipGroup() {
		return lstEquipGroup;
	}

	public void setLstEquipGroup(List<EquipmentVO> lstEquipGroup) {
		this.lstEquipGroup = lstEquipGroup;
	}

	public List<ApParam> getLstHealthy() {
		return lstHealthy;
	}

	public void setLstHealthy(List<ApParam> lstHealthy) {
		this.lstHealthy = lstHealthy;
	}

	public List<String> getLstGroup() {
		return lstGroup;
	}

	public void setLstGroup(List<String> lstGroup) {
		this.lstGroup = lstGroup;
	}

	public List<String> getLstProvider() {
		return lstProvider;
	}

	public void setLstProvider(List<String> lstProvider) {
		this.lstProvider = lstProvider;
	}

	public List<String> getLstHealth() {
		return lstHealth;
	}

	public void setLstHealth(List<String> lstHealth) {
		this.lstHealth = lstHealth;
	}

	public List<BigDecimal> getLstEquipPrice() {
		return lstEquipPrice;
	}

	public void setLstEquipPrice(List<BigDecimal> lstEquipPrice) {
		this.lstEquipPrice = lstEquipPrice;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public List<Integer> getLstManufacturingYear() {
		return lstManufacturingYear;
	}

	public void setLstManufacturingYear(List<Integer> lstManufacturingYear) {
		this.lstManufacturingYear = lstManufacturingYear;
	}

	public String getAddressName() {
		return addressName;
	}

	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getWardName() {
		return wardName;
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	public String getToPermanentAddress() {
		return toPermanentAddress;
	}

	public void setToPermanentAddress(String toPermanentAddress) {
		this.toPermanentAddress = toPermanentAddress;
	}

	public Integer getEquipNumberDeprec() {
		return equipNumberDeprec;
	}

	public void setEquipNumberDeprec(Integer equipNumberDeprec) {
		this.equipNumberDeprec = equipNumberDeprec;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<String> getLstStock() {
		return lstStock;
	}

	public void setLstStock(List<String> lstStock) {
		this.lstStock = lstStock;
	}
	
}

package ths.dms.web.action.equipment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipPeriodOpenLog;
import ths.dms.core.entities.enumtype.EquipPeriodType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ValidateUtil;

/**
 * Quan ly ky trong quan ly thiet bi
 * @author tientv11
 * @since 17/12/2014
 *
 */
public class EquipPeriodManagerAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Integer ACTION_OPENED = 1;
	private static final Integer ACTION_CLOSED = 2;
	
	EquipmentManagerMgr equipmentManagerMgr;
	
	private EquipPeriod equipPeriod;
	
	private Integer year;
	
	private Long id;
	
	private Long periodIdCloseMaxToDate;
	
	private String name;
	
	private String fromDate;
	
	private String toDate;
	
	private Integer status;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
	}
	
	
	@Override
	public String execute(){
		try {
			generateToken();
			//Lay ky thiet bi dong gan nhat
			periodIdCloseMaxToDate = commonMgr.getEquipPeriodIdByCloseForMaxToDate();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	/**
	 * Tim kiem danh sach cac ky
	 * @author tientv11
	 * @since 17/12/2014
	 * @return
	 */
	public String search(){
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipPeriod>());
			
			KPaging<EquipPeriod> kPaging = new KPaging<EquipPeriod>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			EquipmentFilter<EquipPeriod> filter = new EquipmentFilter<EquipPeriod>();
			filter.setkPaging(kPaging);
			filter.setYearManufacture(year);
			ObjectVO<EquipPeriod> objs = equipmentManagerMgr.getListEquipPeriodByFilter(filter);
			
			if (objs != null && objs.getLstObject() != null) {
				result.put("total", objs.getkPaging().getTotalRows());
				result.put("rows", objs.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Xem chi tiet ky 
	 * @author tientv11
	 * @since 17/12/2014
	 * @return
	 */
	public String viewDetail(){
		try {
			generateToken();
			fromDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			toDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			if(id == null || id == 0L){
				return SUCCESS;
			}
			equipPeriod = commonMgr.getEntityById(EquipPeriod.class, id);
			if(equipPeriod == null){
				return SUCCESS;
			}
			if (equipPeriod.getStatus() != null && EquipPeriodType.CLOSED.getValue().equals(equipPeriod.getStatus().getValue()) && !equipPeriod.getId().equals(commonMgr.getEquipPeriodIdByCloseForMaxToDate())) {
				return PAGE_NOT_PERMISSION;
			}
			fromDate = DateUtil.toDateString(equipPeriod.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			toDate = DateUtil.toDateString(equipPeriod.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
		
		} catch (Exception e) {	
			LogUtility.logError(e, e.getMessage());
		}		
		return SUCCESS;
	}
	
	/**
	 * Luu thong tin 
	 * @author tientv11
	 * @since 18/12/2014
	 * @return
	 */
	public String save(){
		try {
			result.put(ERROR, true);
			Date sysDate = commonMgr.getSysDate();
			if (DateUtil.checkInvalidFormatDate(fromDate)) {
				result.put("errMsg", R.getResource("equipment.period.msg.from.date.invalid"));
				return JSON;
			}
			if (DateUtil.checkInvalidFormatDate(toDate)) {
				result.put("errMsg", R.getResource("equipment.period.msg.to.date.invalid"));
				return JSON;
			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			
			if (id != null && id != 0L) {
				equipPeriod = commonMgr.getEntityById(EquipPeriod.class, id);
				if(equipPeriod == null){
					return JSON;
				}
				EquipmentFilter<EquipPeriod> filter = new EquipmentFilter<EquipPeriod>();
				filter.setFromDate(fDate);
				filter.setToDate(tDate);
				filter.setIdExcepted(id);
				ObjectVO<EquipPeriod> objs = equipmentManagerMgr.getListEquipPeriodByFilter(filter);
				
				if(objs != null && objs.getLstObject().size() > 0){
					result.put("errMsg", R.getResource("equipment.period.msg.status.date"));
					return JSON;
				}
				if(EquipPeriodType.OPENED.getValue().equals(status) && EquipPeriodType.CLOSED.equals(equipPeriod.getStatus())){
					filter = new EquipmentFilter<EquipPeriod>();
					filter.setFromDate(equipPeriod.getFromDate());
					List<EquipPeriod> lst = equipmentManagerMgr.getListEquipPeriodClosedByFromDate(filter);	
					if(lst != null && lst.size() > 0){
						String cod = "";
						for(int i=0;i<lst.size();i++){
							cod+=lst.get(i).getCode();
							if(i!=lst.size()-1){
								cod+=",";
							}
						}
						result.put("errMsg", R.getResource("equipment.period.msg.status.open.period",cod,equipPeriod.getCode()));
						return JSON;
					}
				}
				/** Tai 1 thoi diem, chi co 1 ky duoc mo */
//				if(EquipPeriodType.OPENED.getValue().equals(status) && !EquipPeriodType.OPENED.equals(equipPeriod.getStatus())){
//					EquipmentFilter<EquipPeriod> filter = new EquipmentFilter<EquipPeriod>();
//					filter.setStatus(EquipPeriodType.OPENED.getValue());
//					ObjectVO<EquipPeriod> objs = equipmentManagerMgr.getListEquipPeriodByFilter(filter);	
//					
//					if(objs != null && objs.getLstObject().size() > 0){
//						result.put("errMsg", R.getResource("equipment.period.msg.status.eq"));
//						return JSON;
//					}
//				}
				equipPeriod.setName(name);
				equipPeriod.setFromDate(fDate);
				equipPeriod.setToDate(tDate);				
				equipPeriod.setUpdateDate(sysDate);
				equipPeriod.setUpdateUser(currentUser.getUserName());
				equipPeriod.setStatus(EquipPeriodType.parseValue(status));
				commonMgr.updateEntity(equipPeriod);
			}else{
				equipPeriod = new EquipPeriod();
				equipPeriod.setName(name);
				equipPeriod.setStatus(EquipPeriodType.parseValue(status));
				equipPeriod.setCreateDate(sysDate);
				equipPeriod.setCreateUser(currentUser.getUserName());
								
				EquipmentFilter<EquipPeriod> filter = new EquipmentFilter<EquipPeriod>();
				filter.setFromDate(fDate);
				filter.setToDate(tDate);
				ObjectVO<EquipPeriod> objs = equipmentManagerMgr.getListEquipPeriodByFilter(filter);
				
				if(objs != null && objs.getLstObject().size() > 0){
					result.put("errMsg", R.getResource("equipment.period.msg.status.date"));
					return JSON;
				}
//				if(EquipPeriodType.OPENED.getValue().equals(status)){
//					filter = new EquipmentFilter<EquipPeriod>();
//					filter.setStatus(EquipPeriodType.OPENED.getValue());
//					objs = equipmentManagerMgr.getListEquipPeriodByFilter(filter);
//					if(objs != null && objs.getLstObject().size() > 0){
//						result.put("errMsg", R.getResource("equipment.period.msg.status.eq"));
//						return JSON;
//					}
//				}	
				filter = new EquipmentFilter<EquipPeriod>();	
				//filter.setFirstRow(Boolean.TRUE);
				objs = equipmentManagerMgr.getListEquipPeriodByFilter(filter);
				if(objs != null && objs.getLstObject().size() > 0){
					String currentEquipmentPeriodCode = objs.getLstObject().get(0).getCode();
					currentEquipmentPeriodCode = currentEquipmentPeriodCode.replaceAll("[^\\d]", "");
					Long currentEquipmentPeriodNo = Long.valueOf(currentEquipmentPeriodCode);
					Long nextEquipmentPeriodNo = currentEquipmentPeriodNo + 1;
					final Integer CODE_NUMBER_PART_LENGTH = 8;
					String nextEquipmentPeriodCode = String.format("%0" + CODE_NUMBER_PART_LENGTH + "d", nextEquipmentPeriodNo);
					equipPeriod.setCode("K" + nextEquipmentPeriodCode);
				}else{
					equipPeriod.setCode("K00000001");
				}
				equipPeriod.setFromDate(fDate);
				equipPeriod.setToDate(tDate);
				equipPeriod = commonMgr.createEntity(equipPeriod);
			}
			
			//Cap nhat luu vet cho tien trinh
			if (EquipPeriodType.OPENED.getValue().equals(status) && commonMgr.compareDateWithoutTime(tDate, sysDate) < 0) {
				//Mo ky qua khu
				EquipPeriodOpenLog equipPeriodOpenLog = new EquipPeriodOpenLog();
				equipPeriodOpenLog.setAction(ACTION_OPENED);
				equipPeriodOpenLog.setLockDate(sysDate);
				equipPeriodOpenLog.setEquipPeriodId(equipPeriod.getId());
				equipPeriodOpenLog.setCreateDate(sysDate);
				equipPeriodOpenLog.setCreateUser(currentUser.getUserName());
				commonMgr.createEntity(equipPeriodOpenLog);
			} else if (EquipPeriodType.CLOSED.getValue().equals(status) && commonMgr.compareDateWithoutTime(tDate, sysDate) < 0) {
				//Mo ky qua khu
				EquipPeriodOpenLog equipPeriodOpenLog = new EquipPeriodOpenLog();
				equipPeriodOpenLog.setAction(ACTION_CLOSED);
				equipPeriodOpenLog.setLockDate(sysDate);
				equipPeriodOpenLog.setEquipPeriodId(equipPeriod.getId());
				equipPeriodOpenLog.setCreateDate(sysDate);
				equipPeriodOpenLog.setCreateUser(currentUser.getUserName());
				commonMgr.createEntity(equipPeriodOpenLog);
			}
			result.put("id", equipPeriod.getId());
			result.put("equipmentPeriodCode", equipPeriod.getCode());
			result.put(ERROR, false);
		} catch (Exception e) {	
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			
		}
		return JSON;
	}

	public Integer getYear() {
		return year;
	}


	public void setYear(Integer year) {
		this.year = year;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}


	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getFromDate() {
		return fromDate;
	}


	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	public String getToDate() {
		return toDate;
	}


	public void setToDate(String toDate) {
		this.toDate = toDate;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public Long getPeriodIdCloseMaxToDate() {
		return periodIdCloseMaxToDate;
	}


	public void setPeriodIdCloseMaxToDate(Long periodIdCloseMaxToDate) {
		this.periodIdCloseMaxToDate = periodIdCloseMaxToDate;
	}
	
}

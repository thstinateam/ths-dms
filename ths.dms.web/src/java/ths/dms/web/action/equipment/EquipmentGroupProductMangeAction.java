/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.action.equipment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipGroupProduct;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipmentGroupProductFilter;
import ths.dms.core.entities.filter.ProductGeneralFilter;
import ths.dms.core.entities.vo.EquipmentGroupProductVO;
import ths.dms.core.entities.vo.EquipmentGroupVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

/**
 * Quan ly cac request cua chuc nang: Danh sach san pham cho thiet bi
 * 
 * @author tuannd20
 * @version 1.0
 * @date 05/01/2015
 */
public class EquipmentGroupProductMangeAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	private static final int allType2ndStaus = -2;
	
	private ApParamMgr apParamMgr;
	private EquipmentManagerMgr equipmentManagerMgr;
	private ProductMgr productMgr;
	private ProductInfoMgr productInfoMgr;

	private List<ApParam> statusRecords; // cac trang thai tim kiem tren trang
	private EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter; // filter tim kiem nhom thiet bi

	private List<String> productCodes;
	private List<String> excProductCodes;
	private List<Long> equipGroupProductIds;
	private List<Long> productInforIds;
	private List<Integer> arrStatus;
	private List<Integer> productInforTypes;

	private Long equipGroupProductId;
	private Long productInforId;
	private Long equipGroupId;

	private String equipmentGroupCode;
	private String equipmentGroupName;
	private Integer equipmentGroupStatus;
	private String productName;
	private String productCode;
	private String mapProductFDate;
	private String mapProductTDate;
	private String fDateStr;

	private Integer status;
	private Integer productInforStatus;
	private Integer productInforType;

	/** file excel import */
	private File excelFile;
	private String excelFileContentType;
	
	private List<ProductInfo> lstProductInfo;

	/**
	 * khoi tao cac bien Mgr cho action
	 * 
	 * @author tuannd20
	 * @date 05/01/2015
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
	}

	/**
	 * xu ly trang view chinh cua chuc nang
	 * 
	 * @author tuannd20
	 * @date 05/01/2015
	 */
	@Override
	public String execute() {
		generateToken();
		try {

		} catch (Exception e) {
			LogUtility.logError(e, "Fail to initialize main page.");
			return PAGE_NOT_FOUND;
		}
		return SUCCESS;
	}

	/**
	 * Load trang chinh
	 * 
	 * @author hunglm16
	 * @since May 22, 2015
	 * */
	public String info() {
		generateToken();
		try {
			fDateStr = DateUtil.convertDateByString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			//lay danh sach trang thai tim kiem
			statusRecords = apParamMgr.getListApParam(ApParamType.ACTIVE_TYPE_STATUS, ActiveType.RUNNING);
			//Lay danh sach nghanh hang
			BasicFilter<ProductInfo> filter = new BasicFilter<ProductInfo>();
			filter.setType(ProductType.CAT.getValue());
			filter.setStatus(ActiveType.RUNNING.getValue());
			lstProductInfo = productInfoMgr.getListProductInfoByFilter(filter).getLstObject();
		} catch (Exception e) {
			LogUtility.logError(e, "Fail to initialize main page.");
			return PAGE_NOT_FOUND;
		}
		return SUCCESS;
	}

	/**
	 * tim kiem equipment group theo dieu kien tim kiem
	 * 
	 * @author tuannd20
	 * @return ket qua tim kiem equipment group, luu trong bien result
	 * @date 05/01/2015
	 */
	public String searchEquipmentGroup() {
		/*
		 * default result
		 */
		result.put("rows", new ArrayList<EquipmentGroupVO>());
		result.put("total", 0);

		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			if (equipmentGroupFilter == null) {
				return JSON;
			}

			KPaging<EquipmentGroupVO> kPaging = new KPaging<EquipmentGroupVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			equipmentGroupFilter.setkPaging(kPaging);
			if (equipmentGroupFilter.getStatus() != null && ActiveType.isValidValue(equipmentGroupFilter.getStatus())) {
				equipmentGroupFilter.setEquipmentGroupStatus(ActiveType.parseValue(equipmentGroupFilter.getStatus()));
			}
			ObjectVO<EquipmentGroupVO> searchResult = equipmentManagerMgr.searchEquipmentGroupVO(equipmentGroupFilter);
			if (searchResult != null && searchResult.getLstObject() != null) {
				List<EquipmentGroupVO> equipGroupVOs = searchResult.getLstObject();
				result.put("rows", equipGroupVOs);
				result.put("total", searchResult.getkPaging().getTotalRows());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, "Error happen while searching equipment group");
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * lay danh sach san pham trong equipment group
	 * 
	 * @author tuannd20
	 * @return danh sach san pham trong equipment group, luu trong bien result
	 * @date 05/01/2015
	 */
	public String retrieveEquipmentGroupProduct() {
		// default result
		result.put("rows", new ArrayList<EquipmentGroupProductVO>());
		result.put(ERROR, false);

		if (!StringUtil.isNullOrEmpty(equipmentGroupCode)) {
			try {
				if (currentUser == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					return JSON;
				}
				KPaging<EquipmentGroupProductVO> kPaging = new KPaging<EquipmentGroupProductVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				ObjectVO<EquipmentGroupProductVO> searchResult = equipmentManagerMgr.retrieveProductsInEquipmentGroup(equipmentGroupCode, null, kPaging);
				if (searchResult != null && searchResult.getLstObject() != null) {
					List<EquipmentGroupProductVO> productsInEquipmentGroup = searchResult.getLstObject();
					result.put("rows", productsInEquipmentGroup);
					result.put("total", searchResult.getkPaging().getTotalRows());
				}

				result.put(ERROR, false);
			} catch (BusinessException e) {
				LogUtility.logError(e, "Fail to retrieve products in equipment group: equipmentGroupCode = " + equipmentGroupCode);
				result.put(ERROR, true);
			}
		}
		return JSON;
	}

	/**
	 * xoa san pham ra khoi nhom thiet bi
	 * 
	 * @author tuannd20
	 * @return xoa thanh cong hay that bai, luu du lieu trong result
	 * @date 05/01/2015
	 * 
	 * @author hunglm16
	 * @description ra soat va update
	 * @since May 20,2015
	 */
	public String removeProductFromEquipmentGroup() {
		resetToken(result);
		if (productCodes == null || productCodes.isEmpty()) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.product.code.must.not.null"));
			return JSON;
		}
		if (StringUtil.isNullOrEmpty(equipmentGroupCode)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.equipment.group.code.must.not.null"));
			return JSON;
		}
		if (equipGroupProductIds == null || equipGroupProductIds.isEmpty()) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.get.list.remove.is.null", equipmentGroupCode));
			return JSON;
		}
		try {
			Date sysDateCp = commonMgr.getSysDate();
			//Kiem tra tinh hop le cho nghiep vu xoa
			EquipmentGroupProductFilter<EquipGroupProduct> filter = new EquipmentGroupProductFilter<EquipGroupProduct>();
			filter.setEquipGroupCodes(Arrays.asList(new String[] { equipmentGroupCode }));
			//filter.setProductCodes(productCodes);
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setEquipGroupProductIds(equipGroupProductIds);
			ObjectVO<EquipGroupProduct> ojbVO = equipmentManagerMgr.getListEquipGroupProductByFilter(filter);
			if (ojbVO == null || ojbVO.getLstObject() == null || ojbVO.getLstObject().isEmpty()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.get.list.remove.is.null", equipmentGroupCode));
				return JSON;
			} else {
				List<EquipGroupProduct> lstEquipGroupProduct = ojbVO.getLstObject();
				for (EquipGroupProduct equipGroupProduct : lstEquipGroupProduct) {
					if (equipGroupProduct.getToDate() != null && commonMgr.compareDateWithoutTime(equipGroupProduct.getToDate(), sysDateCp) == -1) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.delete.todate.nho.hon.sysdate", DateUtil.convertDateByString(equipGroupProduct.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY),
								DateUtil.convertDateByString(sysDateCp, DateUtil.DATE_FORMAT_DDMMYYYY)));
						return JSON;
					}
				}
			}
			equipmentManagerMgr.removeProductFromEquipmentGroup(equipmentGroupCode, equipGroupProductIds, getLogInfoVO());
			result.put(ERROR, false);
		} catch (BusinessException e) {
			LogUtility.logError(e, "Fail to delete equipmentGroupProduct. equipmentGroupCode = " + equipmentGroupCode + "loginStaffCode = " + getLogInfoVO().getStaffCode());
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * them san pham vao nhom thiet bi
	 * 
	 * @author tuannd20
	 * @return them thanh cong hay that bai, luu du lieu trong result
	 * 
	 * @author hunglm16
	 * @since May 22, 2015
	 * @description Ra soat va Update
	 */
	public String addProductToEquipmentGroup() {
		resetToken(result);
		int flag = 0;
		EquipGroup equipGroup = null;
		String productCodeError = "";
		String fDateError = "";
		if (StringUtil.isNullOrEmpty(mapProductFDate) || StringUtil.isNullOrEmpty(mapProductTDate)) {
			result.put("flag", flag);
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.product.code.must.not.null"));
			return JSON;
		}
		try {
			if (equipGroupId == null || equipGroupId < 0) {
				result.put("flag", flag);
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.equipment.group.code.must.not.null"));
				return JSON;
			} else {
				equipGroup = commonMgr.getEntityById(EquipGroup.class, equipGroupId);
				if (equipGroup == null) {
					result.put("flag", flag);
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined"));
					return JSON;
				} else if (equipGroup.getStatus() == null || !ActiveType.RUNNING.getValue().equals(equipGroup.getStatus().getValue())) {
					result.put("flag", flag);
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon"));
					return JSON;
				}
			}
			Map<String, String> mapPrdFromDate = new HashMap<String, String>();
			Map<String, String> mapPrdToDate = new HashMap<String, String>();
			List<String> lstProductCode = new ArrayList<String>();
			
			String[] arrMapProductFDate = mapProductFDate.split(",");
			for (String prdFdate : arrMapProductFDate) {
				String[] ojbPrd = prdFdate.split("_");
				if (ojbPrd.length < 2 || StringUtil.isNullOrEmpty(ojbPrd[0]) || StringUtil.isNullOrEmpty(ojbPrd[1]) || !DateUtil.isDateFull(ojbPrd[1])) {
					result.put("flag", flag);
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.err.data.is.not.validate"));
					return JSON;
				}
				mapPrdFromDate.put(ojbPrd[0].trim(), ojbPrd[1].trim());
				lstProductCode.add(ojbPrd[0].trim());
			}
			String[] arrMapProductTDate = mapProductTDate.split(",");
			for (String prdTdate : arrMapProductTDate) {
				String[] ojbPrd = prdTdate.split("_");
				if (ojbPrd.length < 2 || StringUtil.isNullOrEmpty(ojbPrd[0]) || StringUtil.isNullOrEmpty(ojbPrd[1]) || (!ojbPrd[1].trim().toUpperCase().equals("DDMMYYYY") && !DateUtil.isDateFull(ojbPrd[1]))) {
					result.put("flag", flag);
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.err.data.is.not.validate"));
					return JSON;
				}
				if (ojbPrd[1].trim().toUpperCase().equals("DDMMYYYY")) {
					mapPrdToDate.put(ojbPrd[0].trim(), "");
				} else {
					mapPrdToDate.put(ojbPrd[0].trim(), ojbPrd[1].trim());
				}
			}
			if (lstProductCode.size() == 0) {
				result.put("flag", flag);
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.product.code.must.not.null"));
				return JSON;
			}
			if (excProductCodes == null || excProductCodes.isEmpty()) {
				excProductCodes = new ArrayList<String>();
			}
			Date changeDate = commonMgr.getSysDate();
			int countSuccess = 0;
			boolean flagUpdate = false;
			Collections.sort(lstProductCode);
			for (String prdCode : lstProductCode) {
				flagUpdate = false;
				String fDatei = mapPrdFromDate.get(prdCode);
				productCodeError = prdCode;
				fDateError = fDatei;
				Date fDate = DateUtil.parse(fDatei, DateUtil.DATE_FORMAT_DDMMYYYY);
				String tDatei = mapPrdToDate.get(prdCode);
				Date tDate = null; 
				if (!StringUtil.isNullOrEmpty(tDatei)) {
					tDate = DateUtil.parse(tDatei, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (equipmentManagerMgr.checkFromDateToDateInEquipGroupProduct(equipGroupId, prdCode, fDatei, tDatei, 1) > 0) {
					if (fDate == null || commonMgr.compareDateWithoutTime(fDate, changeDate) < 1) {
						result.put("flag", flag);
						result.put("excProductCodes", excProductCodes);
						result.put("countSuccess", countSuccess);
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.err.fdate.tdate.not.success", prdCode, fDatei));
						return JSON;
					}
				}
				if (equipmentManagerMgr.checkFromDateToDateInEquipGroupProduct(equipGroupId, prdCode, fDatei, tDatei, 2) > 0) {
					flagUpdate = true;
					if (excProductCodes.indexOf(prdCode) < 0) {
						excProductCodes.add(prdCode);
						result.put("flag", 1);
						result.put("countSuccess", countSuccess);
						result.put("excProductCodes", excProductCodes);
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.err.tdate.old.is.null.2", prdCode));
						return JSON;
					}
				}
				result.put(ERROR, false);
				//Thuc hien them moi du lieu
				equipmentManagerMgr.addProductForEqupGroupProduct(equipGroupId, prdCode, fDate, tDate, flagUpdate, getLogInfoVO());
				countSuccess++;
			}
			//equipmentManagerMgr.addProductToEquipmentGroup(equipmentGroupCode, productCodes, getLogInfoVO());
		} catch (BusinessException e) {
			if (e.getMessage().startsWith("productGroupEquip is null")) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.err.fdate.tdate.not.success", productCodeError, fDateError));	
			} else {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
			}
			LogUtility.logError(e, "Fail to add equipmentGroupProduct. equipmentGroupCode = " + equipmentGroupCode + "loginStaffCode = " + getLogInfoVO().getStaffCode());
			
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Kiem tra dong rong
	 * 
	 * @author tamvnm
	 * @param list row
	 * @return dong trong: true, ko co dong trong: false
	 * @since Sep 17 2015
	 */
	private Boolean checkLineIsEmpty(List<String> row) {
		if (row == null) {
			return null;
		}
		for (int i = 0, isize = row.size(); i < isize; i++) {
			if (!StringUtil.isNullOrEmpty(row.get(i))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Import excel danh sach san pham
	 * 
	 * @author tamvnm
	 * @since Sep 17 2015
	 */
	public String importExcelGroupProduct() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			resetToken(result);
			isError = true;
			errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
			totalItem = 0;
			String message = "";
			Date sysDate = commonMgr.getSysDate();
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, 4);
			
 			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				//Bỏ dòng trống dư ở cuối biên bản.
				for (int step = lstData.size() - 1; step > 0; step--) {
					if (checkLineIsEmpty(lstData.get(step))) {
						lstData.remove(step);
					} else {
						break;
					}
				}
				
				List<String> row = new ArrayList<String>();
				EquipGroup equipGroup = null;
				Product product = null;
				Date startDate = null;
				Date endDate = null;
				String startDateStr = "";
				String endDateStr = "";
				Boolean isValidEndDate = true;
				for (int i = 0; i < lstData.size(); i++) {
					totalItem++;
					equipGroup = null;
					product = null;
					startDate = null;
					endDate = null;
					startDateStr = "";
					endDateStr = "";
					message = "";
					isValidEndDate = true;
					row = lstData.get(i);
					if (checkLineIsEmpty(row)) {
						continue;
					}
					
					//Ma nhom thiet bi
					if (row.size() > 0) {
						String value = row.get(0);
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.trim();
						}
						String msg = ValidateUtil.validateField(value, "equip.group.product.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						} else {
							equipGroup = equipmentManagerMgr.getEquipGroupByCode(value);
							if (equipGroup == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.group.product.invalid") + "\n";
							} else if (!ActiveType.RUNNING.equals(equipGroup.getStatus())) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.group.product.stop") + "\n";
							}
						}
					}
					
					//Ma san pham
					if (row.size() > 1) {
						String value = row.get(1);
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.trim();
						}
						String msg = ValidateUtil.validateField(value, "equip.product.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						} else {
							product = productMgr.getProductByCodeNotByStatus(value);
							if (product == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.product.invalid") + "\n";
							} else if (ActiveType.DELETED.equals(product.getStatus())) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.product.delete") + "\n";
							}
						}
					}
					
					//Ngay bat dau
					if (row.size() > 2) {
						String value = row.get(2);
						String msg = ValidateUtil.validateField(value, "equip.startDate", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						} else {
							msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.startDate", null);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								startDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								startDateStr = value;
								if (startDate == null) {
									message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.startDate", null);
								}
							}
						}							
					}
					
					//Ngay ket thuc
					if (row.size() > 3) {
						String value = row.get(3);
						String msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "equip.endDate", 10, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
								isValidEndDate = false;
							} else {
								msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.endDate", null);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									isValidEndDate = false;
								} else {
									endDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
									endDateStr = value;
									if (endDate == null) {
										message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equip.endDate", null);
										isValidEndDate = false;
									}
								}
							}	
						}
					}
					
					if (startDate != null && endDate != null && DateUtil.compareDateWithoutTime(startDate, endDate) == 1) {
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.err.fdate.lagger.tdate") + "\n";
					}
					
					if (product != null && ActiveType.STOPPED.equals(product.getStatus()) && isValidEndDate) {
						if (endDate != null && commonMgr.compareDateWithoutTime(endDate, sysDate) == 1) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.product.off.endDate") + "\n";
						} else if (endDate == null) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.product.off.endDate") + "\n";
						}
						
					}
					if (equipGroup != null && equipGroup.getId() != null && product != null && product.getProductCode() != null
							&& startDate != null) {
						//Truong hop trung chom
						if (equipmentManagerMgr.checkFromDateToDateInEquipGroupProduct(equipGroup.getId(), product.getProductCode(), startDateStr, endDateStr, 1) > 0) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.err.fdate.tdate.not.success", product.getProductCode(), startDateStr);
						}
						if (equipmentManagerMgr.checkFromDateToDateInEquipGroupProduct(equipGroup.getId(), product.getProductCode(), startDateStr, endDateStr, 2) > 0) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.group.product.err.tdate.old.is.null.3", product.getProductCode());
						}
					}
					
					if (StringUtil.isNullOrEmpty(message)) {
						EquipGroupProduct groupProduct = new EquipGroupProduct();
						groupProduct.setCreateDate(sysDate);
						groupProduct.setCreateUser(currentUser.getStaffRoot().getStaffCode());
						groupProduct.setEquipGroup(equipGroup);
						groupProduct.setProduct(product);
						groupProduct.setStatus(ActiveType.RUNNING);
						groupProduct.setFromDate(startDate);
						groupProduct.setToDate(endDate);
						commonMgr.createEntity(groupProduct);
					} else {
						lstFails.add(StringUtil.addFailBean(row, message));
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_GROUP_PRODUCT_FAIL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				isError = false;
			} 
			isError = false;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentGroupProductMangeAction.importExcelGroupProduct"), createLogErrorStandard(startLogDate));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			return SUCCESS;
		}
		
		return SUCCESS;
	}
	
	/**
	 * Export excel danh sach san pham
	 * 
	 * @author tamvnm
	 * @return JSON
	 * @since Sep 17 2015
	 */
	public String exportExcelGroupProduct () {
		Date startLogDate = DateUtil.now();
		try {
			/**ATTT duong dan*/
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter = new EquipmentGroupProductFilter<EquipmentGroupVO>();
			if (!StringUtil.isNullOrEmpty(equipmentGroupCode)) {
				equipmentGroupFilter.setEquipmentGroupCode(equipmentGroupCode);
			}
			if (!StringUtil.isNullOrEmpty(equipmentGroupName)) {
				equipmentGroupFilter.setEquipmentGroupName(equipmentGroupName);;
			}
			if (equipmentGroupStatus != null && ActiveType.isValidValue(equipmentGroupStatus)) {
				equipmentGroupFilter.setEquipmentGroupStatus(ActiveType.parseValue(equipmentGroupStatus));;
			}
			
			List<EquipmentGroupVO> lstGroupProduct = equipmentManagerMgr.getListEquipmentGroupVOForExport(equipmentGroupFilter);
			
			if (lstGroupProduct != null && lstGroupProduct.size() > 0) {
				SXSSFWorkbook workbook = null;
				FileOutputStream out = null;
				try {
					// Init XSSF workboook
					String outputName = ConstantManager.TEMPLATE_EXPORT_GROUP_PRODUCT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX;
					String exportFileName = Configuration.getStoreRealPath() + outputName;

					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					// Tao shett
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

					// Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(18);
					// set static Column width
					mySheet.setColumnsWidth(sheetData, 0, 250, 250, 150, 150);
					// Size Row
					mySheet.setRowsHeight(sheetData, 0, 20);
					sheetData.setDisplayGridlines(true);
					// text style
					XSSFCellStyle textStyle = (XSSFCellStyle)style.get(ExcelPOIProcessUtils.NORMAL_BORDER).clone();
					XSSFCellStyle textStyleCenter = (XSSFCellStyle)style.get(ExcelPOIProcessUtils.NORMAL_CENTER_BORDER).clone();
					XSSFCellStyle numberStyle = (XSSFCellStyle)style.get(ExcelPOIProcessUtils.NORMAL_NUMBER_BORDER).clone();
					DataFormat fmt = workbook.createDataFormat();
					textStyle.setDataFormat(fmt.getFormat("@"));
					textStyleCenter.setDataFormat(fmt.getFormat("@"));
					
					int iRow = 0, iColumn = 0;
					// tao column header
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.group.product.export.header");
					String[] lstHeaders = headers.split(";");
					for (int i = 0; i < lstHeaders.length; i++) {
						mySheet.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_GREEN04_ALl_THIN_WRAPTEXT));
					}
					
					iRow = 1;
					iColumn = 0;
					//danh sach
					for (int i = 0, isize = lstGroupProduct.size(); i < isize; i++) {
						iColumn = 0;
						mySheet.addCell(sheetData, iColumn++, iRow, lstGroupProduct.get(i).getCode(), textStyle);
						mySheet.addCell(sheetData, iColumn++, iRow, lstGroupProduct.get(i).getProductCode(), textStyle);
						mySheet.addCell(sheetData, iColumn++, iRow, lstGroupProduct.get(i).getFromDate(), textStyleCenter);
						mySheet.addCell(sheetData, iColumn++, iRow, lstGroupProduct.get(i).getToDate(), textStyleCenter);
						iRow++;
					}
					
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					/**ATTT duong dan*/
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
					
				} catch (Exception e) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentGroupProductMangeAction.exportExcelGroupProduct"), createLogErrorStandard(startLogDate));
					if (workbook != null) {
						workbook.dispose();
					}
					if (out != null) {
						out.close();
					}
				} finally {
					if (workbook != null) {
						workbook.dispose();
					}
					if (out != null) {
						out.close();
					}
				}
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
				result.put(ERROR, true);
				result.put("data.hasData", true);
				result.put("errMsg", errMsg);
			}
		} catch (Exception e) {
			result.put(ERROR, true);
		    result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		    LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentGroupProductMangeAction.exportExcelGroupProduct"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Lay danh sach san pham
	 * 
	 * @author hunglm16
	 * @since May 20, 2015
	 * */

	public String searchProductForInsert() {
		result.put("rows", new ArrayList<ProductVO>());
		result.put("total", 0);
		try {
			KPaging<ProductVO> kPaging = new KPaging<ProductVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			ProductGeneralFilter<ProductVO> filter = new ProductGeneralFilter<ProductVO>();
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(productCode)) {
				filter.setProductCode(productCode);
			}
			filter.setProductName(productName);
			if (status != null && status > ActiveType.DELETED.getValue()) {
				filter.setStatus(status);
			} else {
				filter.setArrStatus(Arrays.asList(new Integer[] { ActiveType.RUNNING.getValue(), ActiveType.STOPPED.getValue() }));
			}
			if (productInforId != null && productInforId > 0l) {
				filter.setProductInforIds(Arrays.asList(new Long[] { productInforId }));
			}
			if (productInforStatus != null) {
				filter.setProductInforStatus(productInforStatus);
			} else {
				filter.setProductInforStatus(ActiveType.RUNNING.getValue());
			}
			if (productInforType != null) {
				filter.setProductInforTypes(Arrays.asList(new Integer[] { productInforType }));
			} else {
				filter.setProductInforTypes(Arrays.asList(new Integer[] { ProductType.CAT.getValue() }));
			}
			ObjectVO<ProductVO> ojbVO = productMgr.getListProductVOByFilter(filter);
			if (ojbVO != null && ojbVO.getLstObject() != null && !ojbVO.getLstObject().isEmpty()) {
				result.put("rows", ojbVO.getLstObject());
				result.put("total", ojbVO.getkPaging().getTotalRows());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public List<ApParam> getStatusRecords() {
		return statusRecords;
	}

	public void setStatusRecords(List<ApParam> statusRecords) {
		this.statusRecords = statusRecords;
	}

	public EquipmentGroupProductFilter<EquipmentGroupVO> getEquipmentGroupFilter() {
		return equipmentGroupFilter;
	}

	public void setEquipmentGroupFilter(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) {
		this.equipmentGroupFilter = equipmentGroupFilter;
	}

	public String getEquipmentGroupCode() {
		return equipmentGroupCode;
	}

	public void setEquipmentGroupCode(String equipmentGroupCode) {
		this.equipmentGroupCode = equipmentGroupCode;
	}

	public List<String> getProductCodes() {
		return productCodes;
	}

	public void setProductCodes(List<String> productCodes) {
		this.productCodes = productCodes;
	}

	public Long getEquipGroupProductId() {
		return equipGroupProductId;
	}

	public void setEquipGroupProductId(Long equipGroupProductId) {
		this.equipGroupProductId = equipGroupProductId;
	}

	public List<Long> getEquipGroupProductIds() {
		return equipGroupProductIds;
	}

	public void setEquipGroupProductIds(List<Long> equipGroupProductIds) {
		this.equipGroupProductIds = equipGroupProductIds;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<Long> getProductInforIds() {
		return productInforIds;
	}

	public void setProductInforIds(List<Long> productInforIds) {
		this.productInforIds = productInforIds;
	}

	public List<Integer> getArrStatus() {
		return arrStatus;
	}

	public void setArrStatus(List<Integer> arrStatus) {
		this.arrStatus = arrStatus;
	}

	public Integer getProductInforStatus() {
		return productInforStatus;
	}

	public void setProductInforStatus(Integer productInforStatus) {
		this.productInforStatus = productInforStatus;
	}

	public List<ProductInfo> getLstProductInfo() {
		return lstProductInfo;
	}

	public void setLstProductInfo(List<ProductInfo> lstProductInfo) {
		this.lstProductInfo = lstProductInfo;
	}

	public List<Integer> getProductInforTypes() {
		return productInforTypes;
	}

	public void setProductInforTypes(List<Integer> productInforTypes) {
		this.productInforTypes = productInforTypes;
	}

	public Long getProductInforId() {
		return productInforId;
	}

	public void setProductInforId(Long productInforId) {
		this.productInforId = productInforId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Integer getProductInforType() {
		return productInforType;
	}

	public void setProductInforType(Integer productInforType) {
		this.productInforType = productInforType;
	}

	public String getMapProductFDate() {
		return mapProductFDate;
	}

	public void setMapProductFDate(String mapProductFDate) {
		this.mapProductFDate = mapProductFDate;
	}

	public String getMapProductTDate() {
		return mapProductTDate;
	}

	public void setMapProductTDate(String mapProductTDate) {
		this.mapProductTDate = mapProductTDate;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public List<String> getExcProductCodes() {
		return excProductCodes;
	}

	public void setExcProductCodes(List<String> excProductCodes) {
		this.excProductCodes = excProductCodes;
	}

	public String getfDateStr() {
		return fDateStr;
	}

	public void setfDateStr(String fDateStr) {
		this.fDateStr = fDateStr;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getEquipmentGroupName() {
		return equipmentGroupName;
	}

	public void setEquipmentGroupName(String equipmentGroupName) {
		this.equipmentGroupName = equipmentGroupName;
	}

	public Integer getEquipmentGroupStatus() {
		return equipmentGroupStatus;
	}

	public void setEquipmentGroupStatus(Integer equipmentGroupStatus) {
		this.equipmentGroupStatus = equipmentGroupStatus;
	}

	
}

package ths.dms.web.action.equipment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.Unicode2English;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.business.EquipSuggestEvictionMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipEvictionForm;
import ths.dms.core.entities.EquipEvictionFormDtl;
import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.DeliveryType;
import ths.dms.core.entities.enumtype.EquipDeliveryStatusType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.filter.EquipSuggestEvictionFilter;
import ths.dms.core.entities.filter.EquipmentEvictionFilter;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentEvictionVOs;
import ths.dms.core.entities.vo.EquipmentExVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StockGeneralVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;
//import ths.dms.core.entities.enumtype.ShopDecentralizationSTT;

public class EquipmentEvictionManageAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	EquipmentManagerMgr equipmentManagerMgr;
	CustomerMgr customerMgr;
	EquipRecordMgr equipRecordMgr;
	EquipSuggestEvictionMgr equipSuggestEvictionMgr;
	
	private String customerAddr;
	private String customerName;
	private String customerCode;
	private String seri;
	private String equipCode;
	private String fromDate;
	private String toDate;
	private String equipAttachFileStr;
	private Integer statusAction;
	private Integer statusForm;
	private List<Long> lstShopId;
	private List<Long> lstId;
	private String strListShopId;
	private Long id;
	private File excelFile;
	private String excelFileContentType;
	private String shopCode;
	private String fromRePresentor;
	private String toRePresentor;
	private String stockCode;
	private String resultBB;
	private Integer viTri;
	private String newShopCode;
	private String newCustomerCode;
	private Integer status;
	private Integer statusDelivery;
	private String equipSugEvictionCode;
	private String fromShop;
	EquipEvictionForm equipEvictionForm = new EquipEvictionForm();
	private Integer isEdit;
	private String newAddress;
	private String note;
	Long idForm;
	private String downloadPath;
	private Integer windowWidth;	
	private Integer windowHeight;
	private List<Map<String, Object>> listObjectBeans;
	public static final String EQUIP_EVICTION_REPRESENTOR_JOB_TITLE = "EQUIP_EVICTION_REPRESENTOR_JOB_TITLE";
	//danh sach cac dong dc chon de xuat ra file excel
	private List<Long> lstIdRecord;
	private String createDate;
	private List<File> lstFile;
	private List<String> lstFileContentType;
	private List<String> lstFileFileName;
	private List<FileVO> lstFileVo;
	private List<ApParamEquip> lstReasonEviction;
	
	@Override
	public void prepare() throws Exception {
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
		equipSuggestEvictionMgr = (EquipSuggestEvictionMgr) context.getBean("equipSuggestEvictionMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}

		if (shop != null) {
			shopId = shop.getId();
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem danh sach bien ban thu hoi thanh ly
	 * 
	 * @author phuongvm
	 * @since 15/12/2014
	 * @return
	 */
	public String searchEviction() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<EquipmentEvictionVO> kPaging = new KPaging<EquipmentEvictionVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentEvictionFilter filter = new EquipmentEvictionFilter();
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipCode = equipCode.trim();
				filter.setEuqipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seri)) {
				seri = seri.trim();
				filter.setSeri(seri);
			}
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				customerCode = Unicode2English.codau2khongdau(customerCode.trim());
				filter.setCustomerCode(customerCode);
			}
			if (statusAction != null && statusAction > -1) {
				filter.setStatusAction(statusAction);
			}
			if (statusForm != null && statusForm > -1) {
				filter.setStatusForm(statusForm);
			}
			if (!StringUtil.isNullOrEmpty(strListShopId)) {
				String[] lstStr = strListShopId.split(",");
				lstShopId = new ArrayList<Long>();
				for (int i = 0; i < lstStr.length; i++) {
					Long value = Long.valueOf(lstStr[i]);
					lstShopId.add(value);
				}
			}
			if (lstShopId != null && lstShopId.size() > 0) {
				filter.setLstShopId(lstShopId);
			} else {
				lstShopId = new ArrayList<Long>();
				lstShopId.add(currentUser.getShopRoot().getShopId());
				filter.setLstShopId(lstShopId);
			}
			if (!StringUtil.isNullOrEmpty(equipSugEvictionCode)) {
				filter.setEquipSugEvictionCode(equipSugEvictionCode);
			}
			ObjectVO<EquipmentEvictionVO> temp = equipmentManagerMgr.getListEquipmentEviction(filter);

			if (temp != null) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.searchEviction"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach lich su giao nhan
	 * 
	 * @author phuongvm
	 * @since 16/12/2014
	 * @return
	 */
	public String searchHistoryEviction() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentEvictionVO>());
		try {
			List<EquipmentEvictionVO> lst = new ArrayList<EquipmentEvictionVO>();
			if (id != null && id > 0) {
				lst = equipmentManagerMgr.getHistory(id, EquipTradeType.WITHDRAWAL_LIQUIDATION);
			}
			if (lst != null) {
				result.put("total", lst.size());
				result.put("rows", lst);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.searchHistoryEviction"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * cap nhat bien ban
	 * 
	 * @author phuongvm
	 * @since 16/12/2014
	 * @return
	 */
	public String updateStatus() {
		resetToken(result);
		String username = getCurrentUser().getUserName();
		List<FormErrVO> lstErr = new ArrayList<FormErrVO>();
		List<EquipSuggestEvictionDTL> lstequipSug = null;
		try {
			if (lstId != null && lstId.size() > 0) {
				for (int i = 0; i < lstId.size(); i++) {
					FormErrVO errVO = new FormErrVO();
					int status = 0;//status = 0 thi ko cap nhat
					EquipEvictionForm e = equipmentManagerMgr.getEquipEvictionFormById(lstId.get(i));
					if (e != null && statusAction != null && statusForm != null) {
						if (e.getShop() == null || !checkShopInOrgAccessById(e.getShop().getId())) {
							result.put("errMsg", R.getResource("common.permission.change"));
							result.put(ERROR, true);
							return JSON;
						}
						errVO.setFormCode(e.getCode());
						errVO.setDeliveryStatusErr(0);
						errVO.setFormStatusErr(0);
						errVO.setRecordNotComplete("");
						if (e.getFormStatus() != null && e.getFormStatus().intValue() == StatusRecordsEquip.DRAFT.getValue().intValue()) { //du thao
							if (statusForm > -1) {
								e.setFormStatus(statusForm);
								if (statusForm == StatusRecordsEquip.APPROVED.getValue().intValue()) {// duyet
									status = 2; // trang thai tu du thao -> duyet
								} else {
									status = 1;// trang thai tu du thao -> huy
								}
							}
						}
						if (statusForm > -1) {
							if (status == 0) {// ko cap nhat dc
								errVO.setFormStatusErr(1);
							}
						} else {
							errVO.setFormStatusErr(0);
						}
						if (e.getFormStatus() != null && e.getFormStatus().intValue() == StatusRecordsEquip.APPROVED.getValue().intValue()) { //neu trang thai bien ban la duyet thi cho cap nhat giao nhan
							if (e.getActionStatus() != null && (statusAction - e.getActionStatus() - 1 == 0)) {
								e.setActionStatus(statusAction);
								if (status == 0) {
									status = 1;
								}
							} else {//ko thoa man dieu kien
								if (statusAction > 0) {
									errVO.setDeliveryStatusErr(1);
								} else {
									errVO.setDeliveryStatusErr(0);
								}
							}
						}
						if (statusAction < 1) {
							errVO.setDeliveryStatusErr(0);
						}
						Boolean isValid = true;
//						if (e.getCurrentPostion() != null && e.getCurrentPostion() == EquipEvictionPositionType.NPP.getValue().intValue()
//								|| e.getCurrentPostion() == EquipEvictionPositionType.DIEM_BAN.getValue().intValue()) {
//							if (e.getCustomerNew() != null) {
//								isValid = false;
//							}
//							if (e.getNewShop() != null) {
//								isValid = false;
//							}
//							if (e.getNewAddress() != null) {
//								isValid = false;
//							}
//						} else if (e.getCurrentPostion() != null && e.getCurrentPostion() == EquipEvictionPositionType.CHO_CHUYEN_KHACH_HANG_KHAC.getValue().intValue()) {
//							if (e.getCustomerNew() == null) {
//								isValid = false;
//							}
//							if (e.getNewShop() == null) {
//								isValid = false;
//							}
//							if (e.getNewAddress() == null) {
//								isValid = false;
//							}
//						} else if (e.getCurrentPostion() == null) {
//							isValid = false;
//						}
						
						if (e.getShop() == null && !StatusRecordsEquip.CANCELLATION.getValue().equals(statusForm)) {
							isValid = false;
						}
						if (e.getCustomer() == null && !StatusRecordsEquip.CANCELLATION.getValue().equals(statusForm)) {
							isValid = false;
						}
						if (e.getFromRepresentor() == null && !StatusRecordsEquip.CANCELLATION.getValue().equals(statusForm)) {
							isValid = false;
						}
						if (e.getToRepresentator() == null && !StatusRecordsEquip.CANCELLATION.getValue().equals(statusForm)) {
							isValid = false;
						}
						if (e.getReason() == null && !StatusRecordsEquip.CANCELLATION.getValue().equals(statusForm)) {
							isValid = false;
						}
						if (e.getFormStatus() == null && !StatusRecordsEquip.CANCELLATION.getValue().equals(statusForm)) {
							isValid = false;
						}
						if (e.getActionStatus() == null && !StatusRecordsEquip.CANCELLATION.getValue().equals(statusForm)) {
							isValid = false;
						}
						
						//Kiem tra chi tiet bien ban
						if (e.getEquipSugEviction() != null) {
							List<EquipEvictionFormDtl> eDtl = equipmentManagerMgr.getEquipEvictionFormDtlByEquipEvictionFormId(e.getId());
							if (eDtl != null && eDtl.size() > 0) {
								for (int j = 0, jsize = eDtl.size(); j < jsize; j++ ) {
									if (eDtl.get(j).getEquip() == null) {
										isValid = false;
									}
								}
							} else {
								isValid = false;
							}
							if (e.getCreateFormDate() == null) {
								isValid = false;
							}
							
							if (!isValid) {
								errVO.setRecordNotComplete(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.not.complete.record"));
							} else {
								EquipSuggestEviction eSug = e.getEquipSugEviction();
								if (eSug != null) {
									if (StatusRecordsEquip.APPROVED.getValue().equals(statusForm)) {// trang thai tu du thao -> duyet 
										lstequipSug = equipmentManagerMgr.getListSuggestDetailByRecordId(e.getEquipSugEviction().getId(), null);
										
										if (eDtl != null && eDtl.size() > 0 && lstequipSug != null && lstequipSug.size() > 0) {
											// dong thiet bi trong bien ban de nghi thanh da xuat kho
											for (int c = 0, csize = eDtl.size(); c < csize; c++) {
												for (int n = 0, nsize = lstequipSug.size(); n < nsize; n++) {
													if (eDtl.get(c).getEquip() != null && lstequipSug.get(n).getEquipment() != null 
															&& eDtl.get(c).getEquip().getId().equals(lstequipSug.get(n).getEquipment().getId())) {
														lstequipSug.get(n).setDeliveryStatus(EquipDeliveryStatusType.EXPORTED.getValue());
														equipmentManagerMgr.updateStatusEquipSugEvicDTL(lstequipSug.get(n));
														break;
													}
												}
											}
											Boolean isComplete = true;
											lstequipSug = equipmentManagerMgr.getListSuggestDetailByRecordId(e.getEquipSugEviction().getId(), EquipDeliveryStatusType.NOT_EXPORT.getValue());
//											
											if (lstequipSug != null && lstequipSug.size() > 0) {
												isComplete = false;
											}
											if (isComplete) {
//												eSug.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
												EquipSuggestEvictionFilter<EquipSuggestEviction> filter = new EquipSuggestEvictionFilter<EquipSuggestEviction>();
												filter.setEquipSuggestEviction(eSug);
												filter.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
												equipSuggestEvictionMgr.updateEquipSuggestEviction(filter);
											}
										}
									} else if (StatusRecordsEquip.CANCELLATION.getValue().equals(statusForm)) {// trang thai tu du thao -> huy
										lstequipSug = equipmentManagerMgr.getListSuggestDetailByRecordId(e.getEquipSugEviction().getId(), null);
										if (eDtl != null && eDtl.size() > 0 && lstequipSug != null && lstequipSug.size() > 0) {
											// dong thiet bi trong bien ban de nghi thanh da xuat kho
											for (int c = 0, csize = eDtl.size(); c < csize; c++) {
												for (int n = 0, nsize = lstequipSug.size(); n < nsize; n++) {
													if (eDtl.get(c).getEquip() != null && lstequipSug.get(n).getEquipment() != null 
															&& eDtl.get(c).getEquip().getId().equals(lstequipSug.get(n).getEquipment().getId())) {
														lstequipSug.get(n).setDeliveryStatus(null);
														equipmentManagerMgr.updateStatusEquipSugEvicDTL(lstequipSug.get(n));
														break;
													}
												}
											}
											Boolean isComplete = true;
											lstequipSug = equipmentManagerMgr.getListSuggestDetailByRecordId(e.getEquipSugEviction().getId(), EquipDeliveryStatusType.NOT_EXPORT.getValue());
											if (lstequipSug != null && lstequipSug.size() > 0) {
												isComplete = false;
											}
											if (isComplete) {
//												eSug.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
												EquipSuggestEvictionFilter<EquipSuggestEviction> filter = new EquipSuggestEvictionFilter<EquipSuggestEviction>();
												filter.setEquipSuggestEviction(eSug);
												filter.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
												equipSuggestEvictionMgr.updateEquipSuggestEviction(filter);
											}
										}
									}
								}
								
							}
						}
						
						if (status > 0 && isValid) {
							equipmentManagerMgr.updateEquipEvictionFormStatus(e, username, status, lstequipSug);
						} else {
							if (statusForm > -1) {
								errVO.setFormStatusErr(1);
							}
							if (statusAction > 0) {
								errVO.setDeliveryStatusErr(1);
							}
						}
						if (errVO.getDeliveryStatusErr() > 0 || errVO.getFormStatusErr() > 0 || !StringUtil.isNullOrEmpty(errVO.getRecordNotComplete())) {
							lstErr.add(errVO);
						}
					}
				}
			}
			result.put("lstErr", lstErr);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.updateStatus"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	public boolean isEmtyRow(List<String> row) {
//		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4))
//				|| !StringUtil.isNullOrEmpty(row.get(5)) || !StringUtil.isNullOrEmpty(row.get(6)) || !StringUtil.isNullOrEmpty(row.get(7)) || !StringUtil.isNullOrEmpty(row.get(8)) || !StringUtil.isNullOrEmpty(row.get(9))
//				|| !StringUtil.isNullOrEmpty(row.get(10)) || !StringUtil.isNullOrEmpty(row.get(11))) {
//			return false;
//		}
//		return true;
		if (row == null || row.size() == 0) {
			return true;
		}
		for (String s : row) {
			if (!StringUtil.isNullOrEmpty(s)) {
				return false;
			}
		}
		return true;
	}

	public boolean isRowValid(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(5))
				|| !StringUtil.isNullOrEmpty(row.get(6)) || !StringUtil.isNullOrEmpty(row.get(7)) || !StringUtil.isNullOrEmpty(row.get(8)) || !StringUtil.isNullOrEmpty(row.get(9))
				) {
			return false;
		}
		return true;
	}

//	/**
//	 * import bien ban thu hoi va thanh ly
//	 * 
//	 * @author phuongvm
//	 * @since 17/12/2014
//	 * @return
//	 */
//	@SuppressWarnings("unused")
//	public String importExcel() throws Exception {
//		isError = true;
//		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
//		totalItem = 0;
//		String message = "";
//		boolean isNewRecord = true;
//		boolean isValid = true;
//		Date now = commonMgr.getSysDate();
//		List<Equipment> lstEquip = new ArrayList<Equipment>();
//		List<Equipment> lstEquipCheck = new ArrayList<Equipment>();
//		EquipEvictionForm equipEvictionForm = null;
//		String username = getCurrentUser().getUserName();
//		List<CellBean> lstFails = new ArrayList<CellBean>();
//		List<CellBean> lstFailsInForm = new ArrayList<CellBean>();
//		List<CellBean> lstFailsRowInvalid = new ArrayList<CellBean>();
//		Map<String, Integer> processedEquipments = new HashMap<String, Integer>();
//		boolean isValidForm = true; //bien ban hop le (bao gom tat ca cac dong chi tiet)
//		List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, 11);
//		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
//			try {
////				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
////				if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
////					isError = true;
////					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
////					return SUCCESS;
////				}
//				EquipPeriod equipPeriod = null;
//				for (int i = 0; i < lstData.size(); i++) {
//					totalItem++;
//					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
//						List<String> row = lstData.get(i);
//						List<String> rowNext = null;
//						if (i != lstData.size() - 1) {
//							rowNext = lstData.get(i + 1);
//						}
//						if (isValid) {
//							if (isEmtyRow(row)) {
//								isNewRecord = true;
//								if (rowNext != null && isEmtyRow(rowNext)) {
//									isValid = false;
//								}
//								
//								if (isValid) {
////									lstFailsInForm.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
//									lstFailsInForm.add(StringUtil.addFailBean(row, ""));
//								} else {
//									lstFailsRowInvalid.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
//								}
//								continue;
//							} else {
//								if ((i == 0) || (i > 0 && isEmtyRow(lstData.get(i - 1)))) {
//									isNewRecord = true;
//								} else {
//									isNewRecord = false;
//								}
//
//							}
//						} else {
//							//							lstFails.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
//							lstFailsRowInvalid.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
//							continue;
//						}
//						if (!isNewRecord && !isRowValid(row)) {
//							//							lstFails.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
//							lstFailsInForm.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
//							continue;
//						}
//						Shop shop = null;
//						Shop newShop = null;
//						Customer customer = null;
//						Customer newCustomer = null;
//						StatusRecordsEquip statusRecord = null;
//						DeliveryType statusDelivery = null;
//						String nguoiDaiDien = null;
//						Equipment equipment = null;
//						String benChoMuon = null;
//						EquipStock kho = null; // kho vnm hay kho npp
//						EquipStockTotalType loaiKho = null;
//						String lyDo = null;
//						Integer viTriHienTai = null;//0:ko xac dinh,1:NPP,2:KH
//						Integer choChuyenKH = 0;//0:khong chuyen,1:cho chuyen
//						Date ngayLap = null;
//						String note = null;
//						// ** Check trung du lieu cac record */
//						message = "";
//						if (isNewRecord) { //Neu la bien ban moi 
//							// ** Ma NPP */
//							if (row.size() > 0) {
//								String value = row.get(0);
//								String msg = ValidateUtil.validateField(value, "ss.traningplan.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + ConstantManager.NEW_LINE_CHARACTER;
//								} else {
//									value = value.trim().toUpperCase();
//									shop = shopMgr.getShopByCode(value);
//									if (shop == null) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.npp.code");
//									} else if (!shop.getType().getObjectType().equals(ShopObjectType.NPP.getValue())) {
//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
//									} else if (commonMgr.countShopInConectByShopRoot(currentUser.getShopRoot().getShopId(), shop.getId()) == 0) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
//									} else if (!ActiveType.RUNNING.getValue().equals(shop.getStatus().getValue()) && !ActiveType.STOPPED.getValue().equals(shop.getStatus().getValue())) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
//									}
//								}
//							}
//							// ** Ma KH */
//							if (StringUtil.isNullOrEmpty(message)) {
//								if (row.size() > 1) {
//									String value = row.get(1);
//									String msg = ValidateUtil.validateField(value, "equipment.manager.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg + ConstantManager.NEW_LINE_CHARACTER;
//									} else {
//										value = value.trim().toUpperCase();
//										value = StringUtil.getFullCode(shop.getShopCode(), value);
//										customer = customerMgr.getCustomerByCode(value);
//										if (customer == null) {
//											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.customer.code");
//										} else if (!ActiveType.RUNNING.getValue().equals(customer.getStatus().getValue()) && !ActiveType.STOPPED.getValue().equals(customer.getStatus().getValue())) {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.khach.hang") + ConstantManager.NEW_LINE_CHARACTER;
//										} else if (!shop.getId().equals(customer.getShop().getId())) {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.manpp") + ConstantManager.NEW_LINE_CHARACTER;
//										}
//									}
//								}
//							}
//							
//							// ** Ngay lap */
//							if (row.size() > 2) {
//								String value = row.get(2);
//								String msg = ValidateUtil.validateField(value, "equipment.create.form.date", 50, ConstantManager.ERR_REQUIRE);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								}else{
//									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date",null);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										ngayLap = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
//										if (ngayLap == null) {
//											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
//										} else {
//											if (DateUtil.compareTwoDate(ngayLap, now) == 1) {
//												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.big") + ConstantManager.NEW_LINE_CHARACTER;
////												
//											} else {
//												//tamvnm: update dac ta moi. 30/06/2015
//												List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(ngayLap);
//												if (lstPeriod == null || lstPeriod.size() == 0) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//												} else if (lstPeriod.size() > 0) {
//													// gan gia tri ky dau tien.
//													equipPeriod = lstPeriod.get(0);
//												}
//											}
//										}
//									}
//								}							
//							}
//							
//							
//							// ** Nguoi dai dien */
//							if (row.size() > 3) {
//								String value = row.get(3);
//								String msg = ValidateUtil.validateField(value, "equipment.manager.customer.daidien", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + ConstantManager.NEW_LINE_CHARACTER;
//								}
//								if (message.length() == 0) {
//									nguoiDaiDien = value.trim();
//								}
//							}
//							if (StringUtil.isNullOrEmpty(message)) {
//								// ** Mã thiết bị */
//								if (row.size() > 4) {
//									String value = row.get(4);
//									String msg = ValidateUtil.validateField(value, "device.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg + ConstantManager.NEW_LINE_CHARACTER;
//									} else {
//										value = value.trim().toUpperCase();
//										/*
//										 * kiem tra trung
//										 */
//										if (processedEquipments.containsKey(value)) {
//											// loi trung
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code");
//											message += " trùng với " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code") + " dòng " + processedEquipments.get(value) + ConstantManager.NEW_LINE_CHARACTER;
//										} else {
//											processedEquipments.put(value, i + 1);
//											List<EquipmentExVO> lst = null;
//											if (customer != null) {
//												lst = equipmentManagerMgr.getListEquipmentDeliveryByCustomerId(customer.getId());
//												if (lst != null && lst.size() > 0) {
//													for (int j = 0, jsize = lst.size(); j < jsize; j++) {
//														if (lst.get(j).getMaThietBi().equals(value)) {
//															equipment = equipmentManagerMgr.getEquipmentEntityByCode(value);
//															break;
//														}
//													}
//												}
//												if (equipment == null) {
//													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code");
//												} else if (!ActiveType.RUNNING.getValue().equals(equipment.getStatus().getValue())) {
//													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code");
//												} else if (!StatusType.INACTIVE.getValue().equals(equipment.getTradeStatus())) { //trade_status = 0 thi moi cho lap bien ban
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.tbgd");
//												} else if (!EquipStockTotalType.KHO_KH.getValue().equals(equipment.getStockType().getValue()) || !customer.getId().equals(equipment.getStockId())) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp");
//												}
//											}
//										}
//									}
//								}
//							}
//							// ** Ben cho muon */
//							if (row.size() > 5) {
//								String value = row.get(5);
//								String msg = ValidateUtil.validateField(value, "equipment.manager.customer.benchomuon", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + ConstantManager.NEW_LINE_CHARACTER;
//								}
//								if (message.length() == 0) {
//									benChoMuon = value.trim();
//								}
//							}
//							// ** Ma Kho : kho npp hay kho vnm*/ 
//							if (row.size() > 6) {
//								String value = row.get(6);
//								String msg = ValidateUtil.validateField(value, "stock.update.stock.warehouse.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + ConstantManager.NEW_LINE_CHARACTER;
//								} else {
//									value = value.trim().toUpperCase();
//									/**Tamvnm: thay doi, chon kho nguoi dung dc phan quyen*/
//									EquipStockFilter filterStock = new EquipStockFilter();
//									// set cac tham so tim kiem
//									KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
//									filterStock.setkPaging(null);
//
//									if (!StringUtil.isNullOrEmpty(value)) {
//										filterStock.setEquipStockCode(value);
//									}
//									filterStock.setShopRoot(currentUser.getShopRoot().getShopId());
//									filterStock.setStaffRoot(currentUser.getStaffRoot().getStaffId());
//									ObjectVO<EquipStockVO> equipStockVO = equipmentManagerMgr.getListEquipStockVOByRole(filterStock);
//									if (equipStockVO != null && !equipStockVO.getLstObject().isEmpty()) {
//										kho = commonMgr.getEntityById(EquipStock.class, equipStockVO.getLstObject().get(0).getEquipStockId());
//									}
//									if (kho == null) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "stock.update.stock.warehouse.code");
//									} 
////									else if (!kho.getType().getObjectType().equals(ShopObjectType.NPP.getValue()) && !kho.getType().getObjectType().equals(ShopObjectType.VNM.getValue())) {
////										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "stock.update.stock.warehouse.code");
////									} 
//									else {
//										//										List<EquipStockTotal> lstStock = equipmentManagerMgr.getListEquipStockTotal(kho.getId(), EquipStockTotalType.KHO_NPP, null);
//										//										List<EquipStockTotal> lstStocks = equipmentManagerMgr.getListEquipStockTotal(kho.getId(), EquipStockTotalType.KHO_VNM, null);
//										//										if(lstStock==null && lstStocks == null){
//										//											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"stock.update.stock.warehouse.code");
//										//										}else{
////										if (kho.getType().getObjectType().equals(ShopObjectType.NPP.getValue())) {
////											loaiKho = EquipStockTotalType.KHO;
////										} else {
////											//loaiKho = EquipStockTotalType.KHO_VNM;
////										}
//										loaiKho = EquipStockTotalType.KHO;
//										//										}
//									}
//								}
//							}
//							//Ly do thu hoi
//							if (row.size() > 7) {
//								String value = row.get(7).trim();
//								String msg = ValidateUtil.validateField(value, "report.equip.header.menu17", 100, ConstantManager.ERR_REQUIRE);
//								Boolean isTrueReason = false;
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + ConstantManager.NEW_LINE_CHARACTER;
//								} else {
//									List<ApParamEquip> lstReason = apParamEquipMgr.getListApParamEquipByType(ApParamEquipType.EQUIP_EVICTION_REASON_TYPE, ActiveType.RUNNING);
//									if (lstReason != null && lstReason.size() > 0) {
//										for (int c = 0, csize = lstReason.size(); c < csize; c++) {
//											if (lstReason.get(c).getValue().equals(value)) {
//												isTrueReason = true;
//												lyDo = lstReason.get(c).getApParamEquipCode();
//												break;
//											}
//										}
//									} else {
//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.reason.not") + ConstantManager.NEW_LINE_CHARACTER;
//									}
//									if (!isTrueReason) {
//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.reason.invalid") + ConstantManager.NEW_LINE_CHARACTER;
//									}
//									
//								}
//							}
//							// ** Trang thai bien ban*/
//							if (row.size() > 8) {
//								String value = row.get(8).trim();
//								String msg = ValidateUtil.validateField(value, "equipment.manager.status.recordex", 100, ConstantManager.ERR_REQUIRE);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + ConstantManager.NEW_LINE_CHARACTER;
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duthao"))) {
//									statusRecord = StatusRecordsEquip.DRAFT;
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duyet"))) {
//									statusRecord = StatusRecordsEquip.APPROVED;
//								}
//								if (value != null && statusRecord == null) {
//									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.err") + ConstantManager.NEW_LINE_CHARACTER;
//								}
//							}
//							// ** Trang thai giao nhan */
//							if (row.size() > 9) {
//								String value = row.get(9).trim();
//								String msg = ValidateUtil.validateField(value, "equipment.manager.status.delivery", 100, ConstantManager.ERR_REQUIRE);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + ConstantManager.NEW_LINE_CHARACTER;
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.chuagui"))) {
//									statusDelivery = DeliveryType.NOTSEND;
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.dagui"))) {
//									statusDelivery = DeliveryType.SENT;
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.danhan"))) {
//									statusDelivery = DeliveryType.RECEIVED;
//								}
//								if (value != null && statusDelivery == null) {
//									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.err.giaonhan") + ConstantManager.NEW_LINE_CHARACTER;
//								}
//							}
//							if (StatusRecordsEquip.DRAFT.equals(statusRecord)) { // neu la du thao
//								if (!DeliveryType.NOTSEND.equals(statusDelivery)) {
//									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.tbgd.trangthai") + ConstantManager.NEW_LINE_CHARACTER;
//								}
//							}else if (StatusRecordsEquip.APPROVED.equals(statusRecord)){
//								if (DeliveryType.RECEIVED.equals(statusDelivery)) {
//									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.tbgd.trangthai.ex") + ConstantManager.NEW_LINE_CHARACTER;
//								}
//							}
//							// ** Ghi chu */
//							if (row.size() > 10) {
//								String value = row.get(10).trim();
//								String msg = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + ConstantManager.NEW_LINE_CHARACTER;
//								} else {
//									note = value;
//								}
//							}
//							lstFailsInForm.add(StringUtil.addFailBean(row, message));
//						} else {//nguoc lai no la tung dong chi tiet cua bien ban
//							if (!lstEquip.isEmpty()) {
//								if (row.size() > 4) {
//									String value = row.get(4);
//									String msg = ValidateUtil.validateField(value, "device.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg + ConstantManager.NEW_LINE_CHARACTER;
//									} else {
//										value = value.trim().toUpperCase();
//										/*
//										 * kiem tra trung
//										 */
//										if (processedEquipments.containsKey(value)) {
//											// loi trung
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code");
//											message += " trùng với " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code") + " dòng " + processedEquipments.get(value) + ConstantManager.NEW_LINE_CHARACTER;
//										} else {
//											processedEquipments.put(value, i + 1);
//											List<EquipmentExVO> lst = null;
//											if (equipEvictionForm != null && equipEvictionForm.getCustomer() != null) {
//												lst = equipmentManagerMgr.getListEquipmentDeliveryByCustomerId(equipEvictionForm.getCustomer().getId());
//												if (lst != null && lst.size() > 0) {
//													for (int j = 0, jsize = lst.size(); j < jsize; j++) {
//														if (lst.get(j).getMaThietBi().equals(value)) {
//															equipment = equipmentManagerMgr.getEquipmentEntityByCode(value);
//															break;
//														}
//													}
//												}
//												if (equipment == null) {
//													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code");
//												} else if (!ActiveType.RUNNING.getValue().equals(equipment.getStatus().getValue())) {
//													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code");
//												} else if (!EquipStockTotalType.KHO_KH.getValue().equals(equipment.getStockType().getValue()) || !equipEvictionForm.getCustomer().getId().equals(equipment.getStockId())) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp");
//												} else if (!StatusType.INACTIVE.getValue().equals(equipment.getTradeStatus())) { //trade_status = 0 thi moi cho lap bien ban
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.tbgd");
//												} else {
//													for (int k = 0; k < lstEquipCheck.size(); k++) {
//														if (equipment.getCode().equals(lstEquipCheck.get(k).getCode())) {
//															message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err1");
//															break;
//														}
//													}
//												}
//											} else {
//												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.kh.code.ex");
//											}
//										}
//									}
//								}
//							} else {
//								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2");
//							}
//							lstFailsInForm.add(StringUtil.addFailBean(row, message));
//							if (!StringUtil.isNullOrEmpty(message)) {
//								isValidForm = false;
//							}
//						}
//						if (StringUtil.isNullOrEmpty(message)) {
//							if (isNewRecord) { //la bien ban moi
//								if (!lstEquip.isEmpty() && equipEvictionForm != null) {
//									if (isValidForm) {
//										//tao moi bien ban
//										equipmentManagerMgr.createEquipEvictionFormX(equipEvictionForm, lstEquip, null);
//										lstFailsInForm = new ArrayList<CellBean>();
//										isValidForm = true;
//										/** tamvnm: them*/
//										lstFailsInForm.add(StringUtil.addFailBean(row, message));
//									} else {
//										//sai: khong thuc hien tao moi bien ban, add vao list loi~
//										lstFails.addAll(lstFailsInForm);
//										lstFailsInForm = new ArrayList<CellBean>();
//										isValidForm = true;
//									}
//								} else {// truoc do co may bien ban sai
//									if (lstFailsInForm.size() > 0) {
//										lstFailsInForm.remove(lstFailsInForm.size() - 1);
//									}
//									lstFails.addAll(lstFailsInForm);
//									lstFailsInForm = new ArrayList<CellBean>();
//									lstFailsInForm.add(StringUtil.addFailBean(row, message));
//									isValidForm = true;
//								}
//								lstEquip = new ArrayList<Equipment>();
//								equipEvictionForm = new EquipEvictionForm();
//								equipEvictionForm.setActionStatus(statusDelivery.getValue());
//								equipEvictionForm.setCreateDate(now);
//								equipEvictionForm.setCreateUser(username);
//								equipEvictionForm.setCreateFormDate(ngayLap);
////								equipEvictionForm.setCurrentPostion(viTriHienTai);
//								equipEvictionForm.setCustomer(customer);
//								equipEvictionForm.setCustomerCode(customer.getShortCode());
//								equipEvictionForm.setCustomerName(customer.getCustomerName());
//								if (customer.getPhone() != null) {
//									equipEvictionForm.setPhone(customer.getPhone());
//								} else {
//									equipEvictionForm.setPhone(customer.getMobiphone());
//								}
//								equipEvictionForm.setAddress(customer.getAddress());
////								equipEvictionForm.setCustomerNew(newCustomer);
////								if (newCustomer != null) {
////									equipEvictionForm.setNewAddress(newCustomer.getAddress());
////								} else {
////									equipEvictionForm.setNewAddress(null);
////								}
//								equipEvictionForm.setNote(note);
//								equipEvictionForm.setEquipPriod(equipPeriod);
//								equipEvictionForm.setFormStatus(statusRecord.getValue());
//								equipEvictionForm.setFromRepresentor(nguoiDaiDien);
////								equipEvictionForm.setNewShop(newShop);
//								equipEvictionForm.setReason(lyDo);
//								equipEvictionForm.setShop(shop);
//								equipEvictionForm.setToRepresentator(benChoMuon);
//								equipEvictionForm.setToStockId(kho.getId());
//								equipEvictionForm.setToStockType(loaiKho.getValue());
//								equipEvictionForm.setToStockName(kho.getName());
//							}
//							lstEquip.add(equipment);
//							lstEquipCheck.add(equipment);
//						} else {
//							if (isNewRecord) {//la bien ban moi
//								if (isValidForm) { // cac dong bien ban truoc hop le
//									if (!lstEquip.isEmpty() && equipEvictionForm != null) {
//										equipmentManagerMgr.createEquipEvictionFormX(equipEvictionForm, lstEquip, null);
//										lstEquip = new ArrayList<Equipment>();
//										equipEvictionForm = new EquipEvictionForm();
//										for (int j = 0, jsize = lstFailsInForm.size(); j < jsize; j++) {
//											if (!StringUtil.isNullOrEmpty(lstFailsInForm.get(j).getErrMsg())) {
//												lstFails.add(lstFailsInForm.get(j));
//											}
//										}
//										if (lstFails.size() > 0) {
//											lstFails.remove(lstFails.size() - 1);
//										}
//										lstFailsInForm = new ArrayList<CellBean>();
//										lstFailsInForm.add(StringUtil.addFailBean(row, message));
//										isValidForm = true;
//									}
//								} else {
//									lstFails.addAll(lstFailsInForm);
//									lstFailsInForm = new ArrayList<CellBean>();
//									isValidForm = true;
//								}
//							}
//						}
//					}
//				}
//				if (!lstEquip.isEmpty() && equipEvictionForm != null) {
//					if (isValidForm) {
//						//tao moi bien ban
//						Boolean isTrue = true;
//						for (int c = 0, csize = lstFailsInForm.size(); c < csize; c++) {
//							if (!StringUtil.isNullOrEmpty(lstFailsInForm.get(c).getErrMsg())) {
//								isTrue = false;
//								break;
//							}
//						}
//						if (isTrue) {
//							equipmentManagerMgr.createEquipEvictionFormX(equipEvictionForm, lstEquip, null);
//						} else {
//							lstFails.addAll(lstFailsInForm);
//						}
//						if (!lstFailsRowInvalid.isEmpty()) {
//							lstFails.addAll(lstFailsRowInvalid);
//						}
//					} else {
//						//sai: khong thuc hien tao moi bien ban, add vao list loi~
//						lstFails.addAll(lstFailsInForm);
//						if (!lstFailsRowInvalid.isEmpty()) {
//							lstFails.addAll(lstFailsRowInvalid);
//						}
//					}
//				} else {
//					if (!lstFailsInForm.isEmpty()) {
//						lstFails.addAll(lstFailsInForm);
//					}
//					if (!lstFailsRowInvalid.isEmpty()) {
//						lstFails.addAll(lstFailsRowInvalid);
//					}
//				}
//				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_EVICTION_FAIL);
//			} catch (Exception e) {
//				LogUtility.logError(e, e.getMessage());
//				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//				return SUCCESS;
//			}
//		}
//		if (StringUtil.isNullOrEmpty(errMsg)) {
//			isError = false;
//		}
//		isError = false;
//		return SUCCESS;
//	}
	
	/**
	 * Ham validate du lieu dong excel import
	 * 
	 * @author phuongvm
	 * @param row - dong import excel
	 * @return true: dong moi, false: khong phai dong moi
	 * @since 22/10/2015
	 */
	public Boolean isNewRecord(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) && !StringUtil.isNullOrEmpty(row.get(1)) && !StringUtil.isNullOrEmpty(row.get(2))
				&& !StringUtil.isNullOrEmpty(row.get(3)) && !StringUtil.isNullOrEmpty(row.get(5)) && !StringUtil.isNullOrEmpty(row.get(6))) {
			return true;
		}
		return false;
	}
	
	/**
	 * Kiem tra dong du lieu detail hop le
	 * 
	 * @author phuongvm
	 * @param: row - 1 dong du lieu
	 * @return true: hop le, false: khong hop le
	 * @since 22/10/2015
	 */
	public Boolean isValidDetail(List<String> row) {
		for (int i = 0, isize = row.size(); i < isize; i++) {
			switch (i) {
			case 4: // Ma TB
				break;
			default:
				if (!StringUtil.isNullOrEmpty(row.get(i))) {
					return false;
				}
				break;
			}
		}
		return true;
	}
	
	/**
	 * import bien ban thu hoi va thanh ly
	 * 
	 * @author phuongvm
	 * @throws Exception
	 * @since 22/10/2015
	 *
	 */
	@SuppressWarnings("unused")
	public String importExcel() throws Exception {
		final int MAX_COL_IMPORT = 11;
		isError = false;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		String msgFirstLine = "";
		Boolean isFirstLineContract = true;
		Boolean isTrueRecord = true;
		Date now = commonMgr.getSysDate();
		List<Equipment> lstEquip = new ArrayList<Equipment>();
		List<Equipment> lstEquipCheck = new ArrayList<Equipment>();
		EquipEvictionForm equipEvictionForm = new EquipEvictionForm();
		String username = getCurrentUser().getUserName();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<CellBean> lstFailsInForms = new ArrayList<CellBean>();
		Map<String, Integer> processedEquipments = new HashMap<String, Integer>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, MAX_COL_IMPORT);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
//				EquipPeriod equipPeriod = null;
				Shop shop = null;
				Shop newShop = null;
				Customer customer = null;
				Customer newCustomer = null;
				StatusRecordsEquip statusRecord = null;
				DeliveryType statusDelivery = null;
				String nguoiDaiDien = null;
				Equipment equipment = null;
				String benChoMuon = null;
				EquipStock kho = null; // kho vnm hay kho npp
				EquipStockTotalType loaiKho = null;
				String lyDo = null;
				Integer viTriHienTai = null;//0:ko xac dinh,1:NPP,2:KH
				Integer choChuyenKH = 0;//0:khong chuyen,1:cho chuyen
				Date ngayLap = null;
				String note = null;
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						int rowSize = row.size();
						isFirstLineContract = isNewRecord(row);
						if (i == 0 && !isFirstLineContract) {
							msgFirstLine = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.delivery.first.line.error") + ConstantManager.NEW_LINE_CHARACTER;
						}
						if (isFirstLineContract) {
							for (int c = 0, csize = lstFailsInForms.size(); c < csize; c++) {
								if (!StringUtil.isNullOrEmpty(lstFailsInForms.get(c).getErrMsg())) {
									isTrueRecord = false;
									break;
								}
							}
							if (!lstEquip.isEmpty() && isTrueRecord) {
								equipEvictionForm.setActionStatus(statusDelivery.getValue());
								equipEvictionForm.setCreateDate(now);
								equipEvictionForm.setCreateUser(username);
								equipEvictionForm.setCreateFormDate(ngayLap);
								equipEvictionForm.setCustomer(customer);
								equipEvictionForm.setCustomerCode(customer.getShortCode());
								equipEvictionForm.setCustomerName(customer.getCustomerName());
								if (customer.getPhone() != null) {
									equipEvictionForm.setPhone(customer.getPhone());
								} else {
									equipEvictionForm.setPhone(customer.getMobiphone());
								}
								equipEvictionForm.setAddress(customer.getAddress());
								equipEvictionForm.setNote(note);
//								equipEvictionForm.setEquipPriod(equipPeriod);
								equipEvictionForm.setFormStatus(statusRecord.getValue());
								equipEvictionForm.setFromRepresentor(nguoiDaiDien);
								equipEvictionForm.setReason(lyDo);
								equipEvictionForm.setShop(shop);
								equipEvictionForm.setToRepresentator(benChoMuon);
								equipEvictionForm.setToStockId(kho.getId());
								equipEvictionForm.setToStockType(loaiKho.getValue());
								equipEvictionForm.setToStockName(kho.getName());
								equipmentManagerMgr.createEquipEvictionFormX(equipEvictionForm, lstEquip, null);
							} else {
								lstFails.addAll(lstFailsInForms);
							}
							lstFailsInForms = new ArrayList<CellBean>();
							isTrueRecord = true;
							shop = null;
							newShop = null;
							customer = null;
							newCustomer = null;
							statusRecord = null;
							statusDelivery = null;
							nguoiDaiDien = null;
							benChoMuon = null;
							kho = null; // kho vnm hay kho npp
							loaiKho = null;
							lyDo = null;
							viTriHienTai = null;//0:ko xac dinh,1:NPP,2:KH
							choChuyenKH = 0;//0:khong chuyen,1:cho chuyen
							ngayLap = null;
							note = null;
							message = "";
							msgFirstLine = "";
							processedEquipments = new HashMap<String, Integer>();
							lstEquip = new ArrayList<Equipment>();
							equipEvictionForm = new EquipEvictionForm();
						} else {
							isFirstLineContract = false;
						}

						String msgEquip = "";
						equipment = null;

						if (isFirstLineContract) {
							// ** Ma NPP */
							if (rowSize > 0) {
								String value = row.get(0);
								String msg = ValidateUtil.validateField(value, "ss.traningplan.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + ConstantManager.NEW_LINE_CHARACTER;
								} else {
									value = value.trim().toUpperCase();
									shop = shopMgr.getShopByCode(value);
									if (shop == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.npp.code");
									} else if (shop.getType() != null && (shop.getType().getSpecificType() == null
											|| (!ShopSpecificType.NPP.getValue().equals(shop.getType().getSpecificType().getValue()) && !ShopSpecificType.NPP_MT.getValue().equals(shop.getType().getSpecificType().getValue())))) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
									} else if (!getListShopChildId().contains(shop.getId())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
									} else if (!ActiveType.RUNNING.getValue().equals(shop.getStatus().getValue()) && !ActiveType.STOPPED.getValue().equals(shop.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
									}
								}
							}
							// ** Ma KH */
							if (StringUtil.isNullOrEmpty(message)) {
								if (rowSize > 1) {
									String value = row.get(1);
									String msg = ValidateUtil.validateField(value, "equipment.manager.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg + ConstantManager.NEW_LINE_CHARACTER;
									} else {
										value = value.trim().toUpperCase();
										value = StringUtil.getFullCode(shop.getShopCode(), value);
										customer = customerMgr.getCustomerByCode(value);
										if (customer == null) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.customer.code");
										} else if (!ActiveType.RUNNING.getValue().equals(customer.getStatus().getValue()) && !ActiveType.STOPPED.getValue().equals(customer.getStatus().getValue())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.khach.hang") + ConstantManager.NEW_LINE_CHARACTER;
										} else if (!shop.getId().equals(customer.getShop().getId())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.manpp") + ConstantManager.NEW_LINE_CHARACTER;
										} else {
											equipEvictionForm.setCustomer(customer);
										}
									}
								}
							}

							// ** Ngay lap */
							if (rowSize > 2) {
								String value = row.get(2);
								String msg = ValidateUtil.validateField(value, "equipment.create.form.date", 50, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										ngayLap = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
										if (ngayLap == null) {
											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
										} else {
											if (DateUtil.compareTwoDate(ngayLap, now) == 1) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.big") + ConstantManager.NEW_LINE_CHARACTER;
												//												
											} 
//											else {
//												//tamvnm: update dac ta moi. 30/06/2015
//												List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(ngayLap);
//												if (lstPeriod == null || lstPeriod.size() == 0) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//												} else if (lstPeriod.size() > 0) {
//													// gan gia tri ky dau tien.
//													equipPeriod = lstPeriod.get(0);
//												}
//											}
										}
									}
								}
							}

							// ** Nguoi dai dien */
							if (rowSize > 3) {
								String value = row.get(3);
								String msg = ValidateUtil.validateField(value, "equipment.manager.customer.daidien", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + ConstantManager.NEW_LINE_CHARACTER;
								}
								if (message.length() == 0) {
									nguoiDaiDien = value.trim();
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								// ** Mã thiết bị */
								if (rowSize > 4) {
									String value = row.get(4);
									String msg = ValidateUtil.validateField(value, "device.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg + ConstantManager.NEW_LINE_CHARACTER;
									} else {
										value = value.trim().toUpperCase();
										/*
										 * kiem tra trung
										 */
										if (processedEquipments.containsKey(value)) {
											// loi trung
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code");
											message += " trùng với " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code") + " dòng " + processedEquipments.get(value) + ConstantManager.NEW_LINE_CHARACTER;
										} else {
											processedEquipments.put(value, i + 1);
											List<EquipmentExVO> lst = null;
											if (customer != null) {
												lst = equipmentManagerMgr.getListEquipmentDeliveryByCustomerId(customer.getId());
												if (lst != null && lst.size() > 0) {
													for (int j = 0, jsize = lst.size(); j < jsize; j++) {
														if (lst.get(j).getMaThietBi().equals(value)) {
															equipment = equipmentManagerMgr.getEquipmentEntityByCode(value);
															break;
														}
													}
												}
												if (equipment == null) {
													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code");
												} else if (!ActiveType.RUNNING.getValue().equals(equipment.getStatus().getValue())) {
													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code");
												} else if (!StatusType.INACTIVE.getValue().equals(equipment.getTradeStatus())) { //trade_status = 0 thi moi cho lap bien ban
													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.tbgd");
												} else if (!EquipStockTotalType.KHO_KH.getValue().equals(equipment.getStockType().getValue()) || !customer.getId().equals(equipment.getStockId())) {
													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp");
												}
											}
										}
									}
								}
							}
							// ** Ben cho muon */
							if (rowSize > 5) {
								String value = row.get(5);
								String msg = ValidateUtil.validateField(value, "equipment.manager.customer.benchomuon", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + ConstantManager.NEW_LINE_CHARACTER;
								}
								if (message.length() == 0) {
									benChoMuon = value.trim();
								}
							}
							// ** Ma Kho : kho npp hay kho vnm*/ 
							if (rowSize > 6) {
								String value = row.get(6);
								String msg = ValidateUtil.validateField(value, "stock.update.stock.warehouse.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + ConstantManager.NEW_LINE_CHARACTER;
								} else {
									value = value.trim().toUpperCase();
									/**
									 * Tamvnm: thay doi, chon kho nguoi dung dc
									 * phan quyen
									 */
									EquipStockFilter filterStock = new EquipStockFilter();
									// set cac tham so tim kiem
									KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
									filterStock.setkPaging(null);

									if (!StringUtil.isNullOrEmpty(value)) {
										filterStock.setEquipStockCode(value);
									}
									filterStock.setShopRoot(currentUser.getShopRoot().getShopId());
									filterStock.setStaffRoot(currentUser.getStaffRoot().getStaffId());
									ObjectVO<EquipStockVO> equipStockVO = equipmentManagerMgr.getListEquipStockVOByRole(filterStock);
									if (equipStockVO != null && !equipStockVO.getLstObject().isEmpty()) {
										kho = commonMgr.getEntityById(EquipStock.class, equipStockVO.getLstObject().get(0).getEquipStockId());
									}
									if (kho == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "stock.update.stock.warehouse.code");
									} else {
										loaiKho = EquipStockTotalType.KHO;
									}
								}
							}
							//Ly do thu hoi
							if (rowSize > 7) {
								String value = row.get(7).trim();
								String msg = ValidateUtil.validateField(value, "report.equip.header.menu17", 100, ConstantManager.ERR_REQUIRE);
								Boolean isTrueReason = false;
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + ConstantManager.NEW_LINE_CHARACTER;
								} else {
									List<ApParamEquip> lstReason = apParamEquipMgr.getListApParamEquipByType(ApParamEquipType.EQUIP_EVICTION_REASON_TYPE, ActiveType.RUNNING);
									if (lstReason != null && lstReason.size() > 0) {
										for (int c = 0, csize = lstReason.size(); c < csize; c++) {
											if (lstReason.get(c).getValue().equals(value)) {
												isTrueReason = true;
												lyDo = lstReason.get(c).getApParamEquipCode();
												break;
											}
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.reason.not") + ConstantManager.NEW_LINE_CHARACTER;
									}
									if (!isTrueReason) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.reason.invalid") + ConstantManager.NEW_LINE_CHARACTER;
									}

								}
							}
							// ** Trang thai bien ban*/
							if (rowSize > 8) {
								String value = row.get(8).trim();
								String msg = ValidateUtil.validateField(value, "equipment.manager.status.recordex", 100, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + ConstantManager.NEW_LINE_CHARACTER;
								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.waiting"))) {
									statusRecord = StatusRecordsEquip.DRAFT;
								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.approved"))) {
									statusRecord = StatusRecordsEquip.APPROVED;
								}
								if (value != null && statusRecord == null) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.action.status.err") + ConstantManager.NEW_LINE_CHARACTER;
								}
							}
							// ** Trang thai giao nhan */
							if (rowSize > 9) {
								String value = row.get(9).trim();
								String msg = ValidateUtil.validateField(value, "equipment.manager.status.delivery", 100, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + ConstantManager.NEW_LINE_CHARACTER;
								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.action.status.waiting"))) {
									statusDelivery = DeliveryType.NOTSEND;
								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.action.status.sended"))) {
									statusDelivery = DeliveryType.SENT;
								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.action.status.received"))) {
									statusDelivery = DeliveryType.RECEIVED;
								}
								if (value != null && statusDelivery == null) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.action.status.delivery.err") + ConstantManager.NEW_LINE_CHARACTER;
								}
							}
							if (StatusRecordsEquip.DRAFT.equals(statusRecord)) { // neu la du thao
								if (!DeliveryType.NOTSEND.equals(statusDelivery)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.tbgd.trangthai") + ConstantManager.NEW_LINE_CHARACTER;
								}
							} else if (StatusRecordsEquip.APPROVED.equals(statusRecord)) {
								if (DeliveryType.RECEIVED.equals(statusDelivery)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.tbgd.trangthai.ex") + ConstantManager.NEW_LINE_CHARACTER;
								}
							}
							// ** Ghi chu */
							if (rowSize > 10) {
								String value = row.get(10).trim();
								String msg = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + ConstantManager.NEW_LINE_CHARACTER;
								} else {
									note = value;
								}
							}
							if (StringUtil.isNullOrEmpty(msgEquip) && !StringUtil.isNullOrEmpty(message)) {
								isTrueRecord = false;
							} else {
								if (equipment != null) {
									lstEquip.add(equipment);
								}
							}
							lstFailsInForms.add(StringUtil.addFailBean(row, message));
						} else if (isTrueRecord) {
							/*
							 * validate thong tin thiet bi dong detail
							 */
							Boolean isValidDetailRow = isValidDetail(row);
							if (!isValidDetailRow) {
								msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.invalid.detail") + ConstantManager.NEW_LINE_CHARACTER;
							} else {
								if (rowSize > 4) {
									String value = row.get(4);
									String msg = ValidateUtil.validateField(value, "device.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										msgEquip += msg + ConstantManager.NEW_LINE_CHARACTER;
									} else {
										value = value.trim().toUpperCase();
										/*
										 * kiem tra trung
										 */
										if (processedEquipments.containsKey(value)) {
											// loi trung
											msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code");
											msgEquip += " trùng với " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code") + " dòng " + processedEquipments.get(value) + ConstantManager.NEW_LINE_CHARACTER;
										} else {
											processedEquipments.put(value, i + 1);
											List<EquipmentExVO> lst = null;
											if (equipEvictionForm != null && equipEvictionForm.getCustomer() != null) {
												lst = equipmentManagerMgr.getListEquipmentDeliveryByCustomerId(equipEvictionForm.getCustomer().getId());
												if (lst != null && lst.size() > 0) {
													for (int j = 0, jsize = lst.size(); j < jsize; j++) {
														if (lst.get(j).getMaThietBi().equals(value)) {
															equipment = equipmentManagerMgr.getEquipmentEntityByCode(value);
															break;
														}
													}
												}
												if (equipment == null) {
													msgEquip += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code");
												} else if (!ActiveType.RUNNING.getValue().equals(equipment.getStatus().getValue())) {
													msgEquip += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code");
												} else if (!EquipStockTotalType.KHO_KH.getValue().equals(equipment.getStockType().getValue()) || !equipEvictionForm.getCustomer().getId().equals(equipment.getStockId())) {
													msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp");
												} else if (!StatusType.INACTIVE.getValue().equals(equipment.getTradeStatus())) { //trade_status = 0 thi moi cho lap bien ban
													msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.tbgd");
												} else {
													for (int k = 0; k < lstEquipCheck.size(); k++) {
														if (equipment.getCode().equals(lstEquipCheck.get(k).getCode())) {
															msgEquip += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err1");
															break;
														}
													}
												}
											} else {
												msgEquip += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.kh.code.ex");
											}
										}
									}
								}
								/*
								 * Tat ca hop le tao mot danh sach chi tiet
								 */
								if (StringUtil.isNullOrEmpty(msgEquip) && equipment != null) {
									lstEquip.add(equipment);
								}
							}
							if (!StringUtil.isNullOrEmpty(msgFirstLine)) {
								msgEquip += msgFirstLine;
							}
							lstFailsInForms.add(StringUtil.addFailBean(row, msgEquip));
						} else {
							// truong hop dong bien ban loi -> add het tat ca dong
							// detail vao list fail

							lstFailsInForms.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.delivery.error.line.record") + ConstantManager.NEW_LINE_CHARACTER));
						}

						if (i == lstData.size() - 1) {
							for (int c = 0, csize = lstFailsInForms.size(); c < csize; c++) {
								if (!StringUtil.isNullOrEmpty(lstFailsInForms.get(c).getErrMsg())) {
									isTrueRecord = false;
									lstFails.addAll(lstFailsInForms);
									break;
								}
							}
							if (!lstEquip.isEmpty() && isTrueRecord && StringUtil.isNullOrEmpty(msgFirstLine)) {
								equipEvictionForm.setActionStatus(statusDelivery.getValue());
								equipEvictionForm.setCreateDate(now);
								equipEvictionForm.setCreateUser(username);
								equipEvictionForm.setCreateFormDate(ngayLap);
								equipEvictionForm.setCustomer(customer);
								equipEvictionForm.setCustomerCode(customer.getShortCode());
								equipEvictionForm.setCustomerName(customer.getCustomerName());
								if (customer.getPhone() != null) {
									equipEvictionForm.setPhone(customer.getPhone());
								} else {
									equipEvictionForm.setPhone(customer.getMobiphone());
								}
								equipEvictionForm.setAddress(customer.getAddress());
								equipEvictionForm.setNote(note);
//								equipEvictionForm.setEquipPriod(equipPeriod);
								equipEvictionForm.setFormStatus(statusRecord.getValue());
								equipEvictionForm.setFromRepresentor(nguoiDaiDien);
								equipEvictionForm.setReason(lyDo);
								equipEvictionForm.setShop(shop);
								equipEvictionForm.setToRepresentator(benChoMuon);
								equipEvictionForm.setToStockId(kho.getId());
								equipEvictionForm.setToStockType(loaiKho.getValue());
								equipEvictionForm.setToStockName(kho.getName());
								equipmentManagerMgr.createEquipEvictionFormX(equipEvictionForm, lstEquip, null);
							}
						}
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_EVICTION_FAIL);
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.importExcel"), createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}
	
	/**
	 * Xuat excel thu hoi va thanh ly
	 * 
	 * @author tamvnm
	 * @since 13/01/2015
	 * @return
	 */
	public String exportRecordEviction() {
		try {
			/**ATTT duong dan*/
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}
			List<EquipmentEvictionVO> equipmentEvictionVO = equipmentManagerMgr.getListEquipmentEvictionVOForExport(lstIdRecord);

			SXSSFWorkbook workbook = null;
			FileOutputStream out = null;
			try {
				if (null != equipmentEvictionVO && equipmentEvictionVO.size() > 0) {
					// Init XSSF workboook
					String outputName = ConstantManager.TEMPLATE_EQUIP_LIST_EVICTION_EXPORT_FILE_NAME + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;

					workbook = new SXSSFWorkbook(200);
					workbook.setCompressTempFiles(true);
					// Tao shett
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

					// Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(18);
					// set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 100, 150, 100, 100, 100, 140);
					// Size Row
					ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 25);
					sheetData.setDisplayGridlines(true);

					int iRow = 0, iColumn = 0;
					// tao column header
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.eviction");
					String[] lstHeaders = headers.split(";");
					for (int i = 0; i < lstHeaders.length; i++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
//					int staticNumberCol = countColExport(equipmentLiquidationFormVOs);
//					for (int i = 0; i <  equipmentLiquidationFormVOs.size(); i++) {
//						mySheet.addCell(sheetData, iColumn++, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.code") + (i + 1), style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
//						mySheet.addCell(sheetData, iColumn++, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.seri") + (i + 1), style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
//						mySheet.addCell(sheetData, iColumn++, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.content") + (i + 1), style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
//					}
					Boolean isCode = false; // cung 1 ma Phieu.
					String tempMaPhieu = "";
					String maPhieu = "";
					for (int i = 0; i < equipmentEvictionVO.size(); i++) {
						iColumn = 0;
						
						tempMaPhieu = equipmentEvictionVO.get(i).getMaPhieu();
						if(tempMaPhieu != null){
							if(maPhieu.equals("")){
								maPhieu = tempMaPhieu;
								isCode = false;
							}else if(maPhieu.equals(tempMaPhieu)){
								isCode = true;
							}else {
								maPhieu = tempMaPhieu;
							}
						}else {
							isCode = false;
						}
						if(!isCode){
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, ++iRow, equipmentEvictionVO.get(i).getMaPhieu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getMaNPP() + " - " + equipmentEvictionVO.get(i).getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							
							Date cDate = equipmentEvictionVO.get(i).getCreateFormDate();
							if (cDate != null) {
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, DateUtil.convertDateByString(cDate, DateUtil.DATE_FORMAT_DDMMYYYY), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							}
							
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getNgDaiDienKhachHang(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getMaThietBi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getNgDaiDienThuHoi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							
							//Kho
							if(equipmentEvictionVO.get(i).getShopStockcode() != null){
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getShopStockcode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}else{
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							String lydo = "";
							if (equipmentEvictionVO.get(i).getLyDo() != null) {
								ApParamEquip equipReason = apParamEquipMgr.getApParamEquipByCodeX(equipmentEvictionVO.get(i).getLyDo(), ApParamEquipType.EQUIP_EVICTION_REASON_TYPE, ActiveType.RUNNING);
								if (equipReason != null) {
									lydo = equipReason.getValue();
								}
							}
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lydo, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							
//							String vitri = "";
//							if(equipmentEvictionVO.get(i).getViTriHienTai() != null){
//								if (equipmentEvictionVO.get(i).getViTriHienTai() == EquipEvictionPositionType.CHO_CHUYEN_KHACH_HANG_KHAC.getValue()) {
//									vitri = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.cckhk");
//								} else if (equipmentEvictionVO.get(i).getViTriHienTai() == EquipEvictionPositionType.NPP.getValue()) {
//									vitri = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.npp");
//								} else if (equipmentEvictionVO.get(i).getViTriHienTai() == EquipEvictionPositionType.DIEM_BAN.getValue()) {
//									vitri = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.diemban");
//								}
//							}
//							mySheet.addCell(sheetData, iColumn++, iRow, vitri, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//							
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getMaNPPNew(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//							mySheet.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getMaKHNew(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							
							String vitri = "";
							if(equipmentEvictionVO.get(i).getRecordStatus() != null){
								if (equipmentEvictionVO.get(i).getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
									vitri = "Dự thảo";
								} else if (equipmentEvictionVO.get(i).getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
									vitri = "Đã duyệt";
								} else if (equipmentEvictionVO.get(i).getRecordStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
									vitri = "Hủy";
								}
							}
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, vitri, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							
							vitri = "";
							if(equipmentEvictionVO.get(i).getDeliveryStatus() != null){
								if (equipmentEvictionVO.get(i).getDeliveryStatus().equals(DeliveryType.NOTSEND.getValue())) {
									vitri = "Chưa gửi";
								} else if (equipmentEvictionVO.get(i).getDeliveryStatus().equals(DeliveryType.SENT.getValue())) {
									vitri = "Đã gửi";
								} else if (equipmentEvictionVO.get(i).getDeliveryStatus().equals(DeliveryType.RECEIVED.getValue())) {
									vitri = "Đã nhận";
								}
							}
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, vitri, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getNote(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}else{
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, ++iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, equipmentEvictionVO.get(i).getMaThietBi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					/**ATTT duong dan*/
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
					result.put(ERROR, true);
					result.put("data.hasData", true);
					result.put("errMsg", errMsg);
				}
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.exportRecordEviction"), createLogErrorStandard(actionStartTime));
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				result.put(ERROR, true);
				if (workbook != null) {
					workbook.dispose();
				}
				if (out != null) {
					out.close();
				}
			}
			System.gc();
			return JSON;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.exportRecordEviction"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	public List<ApParamEquip> getListReson() {
		List<ApParamEquip> lst = null;
		try {
			lst = apParamEquipMgr.getListApParamEquipByType(ApParamEquipType.EQUIP_EVICTION_REASON_TYPE, ActiveType.RUNNING);
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.getListReson"), createLogErrorStandard(actionStartTime));
		}
		return lst;
	}
	
	/**
	 * Xuat file template import
	 * 
	 * @author tamvnm
	 * @since May 31,2015
	 */
	public String getFileTemplate() {
		String url = "";
		/**ATTT duong dan*/
		String reportToken = retrieveReportToken(reportCode);
		if (StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		try {
			List<ApParamEquip> lstReason = getListReson();
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + ConstantManager.TEMPLATE_IMPORT_EVICTON_EX;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_IMPORT_EVICTON_EX;
			String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("lstReason", lstReason);
			
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.getFileTemplate"), createLogErrorStandard(actionStartTime));
			} catch (InvalidFormatException e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.getFileTemplate"), createLogErrorStandard(actionStartTime));
			} catch (IOException e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.getFileTemplate"), createLogErrorStandard(actionStartTime));
			}
			url = Configuration.getStoreImportFailDownloadPath() + outputName;
			result.put(REPORT_PATH, url);
			result.put(ERROR, false);
			result.put("hasData", true);
			/**ATTT duong dan*/
			MemcachedUtils.putValueToMemcached(reportToken, url, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.getFileTemplate"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * In bien ban thu hoi thanh ly
	 * 
	 * @author phuongvm
	 * @since 15/01/2015
	 * @return
	 */
	public String printEviction() {
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}

			/**ATTT duong dan*/
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			// lay bien ban
			List<EquipmentEvictionVOs> equipmentEvictionVO = equipmentManagerMgr.getListEquipmentEvictionVOForPrint(lstIdRecord);
			ApParamEquip fResentative = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_REPRESENTATIVE.getValue(), ApParamEquipType.EQUIP_EVICTON_PRINT_TYPE, ActiveType.RUNNING);
			ApParamEquip fPosition = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_POSITION.getValue(), ApParamEquipType.EQUIP_EVICTON_PRINT_TYPE, ActiveType.RUNNING);
			String chucVu = "";
			String daiDien = "";
			if (fPosition!=null) {
				chucVu = fPosition.getValue();
			}
			if (fResentative != null) {
				daiDien = fResentative.getValue();
			}
			// set data
			if (equipmentEvictionVO != null && equipmentEvictionVO.size() > 0) {
				Long customerId = equipmentEvictionVO.get(0).getCustomerId();
				if (customerId != null) {
					Customer c = commonMgr.getEntityById(Customer.class, customerId);
					if (c != null && c.getShop() != null) {
//						Shop sMien = shopMgr.getParentShopByShopForIsLevel(c.getShop().getId(), ShopDecentralizationSTT.MIEN.getValue());
//						if (sMien != null) {
//							mien = sMien.getShopName();
//						}
//						Shop sKenh = shopMgr.getParentShopByShopForIsLevel(c.getShop().getId(), ShopDecentralizationSTT.KENH.getValue());
//						if (sKenh != null) {
//							kenh = sKenh.getShopName();
//						}
					}
				}
			}
			String company = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "company.name");
			listObjectBeans = new ArrayList<Map<String, Object>>();
			for(int i=0; i< equipmentEvictionVO.size(); i++){
				EquipmentEvictionVOs equipmentRecordDeliveryVO = equipmentEvictionVO.get(i);
				Map<String, Object> beans = new HashMap<String, Object>();
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getNgDaiDienThuHoi())){
					beans.put("fromShop", equipmentRecordDeliveryVO.getNgDaiDienThuHoi().toUpperCase());
				}else{
					beans.put("fromShop", "");
				}
				beans.put("staffName", daiDien);
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getNgDaiDienKhachHang())){
					beans.put("fromRepresentor", equipmentRecordDeliveryVO.getNgDaiDienKhachHang());
				}else{
					beans.put("fromRepresentor", "");
				}
				
				beans.put("staffType", chucVu.toUpperCase());
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getTenKH())){
					beans.put("customerName", equipmentRecordDeliveryVO.getTenKH());
				}else{
					beans.put("customerName", "");
				}
				
				if (!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getTenKH())) {
					beans.put("customerNameUp", equipmentRecordDeliveryVO.getTenKH().toUpperCase());
				} else {
					beans.put("customerNameUp", "");
				}
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getMaKH())){
					beans.put("customerCode", equipmentRecordDeliveryVO.getMaKH());
				}else{
					beans.put("customerCode", "");
				}
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getDiaChi())){
					beans.put("customerAddress", equipmentRecordDeliveryVO.getDiaChi());
				}else{
					beans.put("customerAddress", "");
				}
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getTenKHNew())){
					beans.put("customerNameNew", equipmentRecordDeliveryVO.getTenKHNew());
				}else{
					beans.put("customerNameNew", "");
				}
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getMaKHNew())){
					beans.put("customerCodeNew", equipmentRecordDeliveryVO.getMaKHNew());
				}else{
					beans.put("customerCodeNew", "");
				}
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getDiaChiNew())){
					beans.put("customerAddressNew", equipmentRecordDeliveryVO.getDiaChiNew());
				}else{
					beans.put("customerAddressNew", "");
				}
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getDienThoai())){
					beans.put("customerPhone", equipmentRecordDeliveryVO.getDienThoai());
				}else{
					beans.put("customerPhone", "");
				}
				
				if(!StringUtil.isNullOrEmpty(equipmentRecordDeliveryVO.getLyDo())){
					beans.put("result", equipmentRecordDeliveryVO.getLyDo());
				}else{
					beans.put("result", "");
				}
				if(equipmentRecordDeliveryVO.getViTri()!=null){
					beans.put("possitive", equipmentRecordDeliveryVO.getViTri());
				}else{
					beans.put("possitive", 0);
				}
				//Them hinh check, uncheck
				beans.put("urlUnChecked", ReportUtils.getVinamilkUncheckedSmall(request));
				beans.put("urlChecked", ReportUtils.getVinamilkCheckedSmall(request));
				beans.put("company", company);
				listObjectBeans.add(beans);
				beans.put("lstEquipEvictionVO", equipmentRecordDeliveryVO.getLstEquipEvictionVO());
			}
			// file in
			String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/print-template/";
			String templatePath = folder + ConstantManager.TEMPLATE_PRINT_RECORD_EVICTION+ ".jasper";
			templatePath = templatePath.replace('/', File.separatorChar);
			File fileReport = new File(templatePath);
			if (!fileReport.exists()) {
				result.put(ERROR, true);
				result.put("errMsg", "Lỗi không tồn tại file template In, cập nhật mẫu bên chức năng Quản lý giao nhận !");
				return JSON;
			}
			// dat Parameters
			Map<String, Object> parameters = new HashMap<String, Object>();

			JRDataSource dataSource = new JRBeanCollectionDataSource(listObjectBeans);

			String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(ConstantManager.TEMPLATE_PRINT_RECORD_EVICTION + ".pdf")).toString());

			// file output
			//String outputPath = Configuration.getStoreRealPath() + outputFile;
			String outputPath = Configuration.getStoreRealPath() + outputFile;
			downloadPath = Configuration.getExportExcelPath() + outputFile;

			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);

			JRAbstractExporter exporter = new JRPdfExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
			exporter.exportReport();

			// set link
//			session.setAttribute("downloadPath", downloadPath);
			
			result.put(ERROR, false);
			result.put("hasData", true);
			String downloadUrl = ReportUtils.addReportTokenToDownloadFileUrl(downloadPath, reportToken);
			result.put(REPORT_PATH, downloadUrl);
			// set link
			session.setAttribute("downloadPath", downloadUrl);
			/**ATTT duong dan*/
			MemcachedUtils.putValueToMemcached(reportToken, downloadPath, retrieveReportMemcachedTimeout());			
			return JSON;
			//return SUCCESS;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.printEviction"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			return ERROR;
		}	
	}
	/**
	 * Mo trang applet
	 * 
	 * @author phuongvm
	 * @since 16/01/2015
	 */
	public String openTaxView() {
		downloadPath = (String) session.getAttribute("downloadPath");
		session.removeAttribute("downloadPath");
		return SUCCESS;
	}
	
	/**
	 * Chuyen trang tao moi hay chinh sua bien ban
	 * 
	 * @author phuongvm
	 * @since 19/12/2014
	 */
	public String getChangeForm() {
		try {
			isEdit = 0;
			lstReasonEviction = apParamEquipMgr.getListApParamEquipByType(ApParamEquipType.EQUIP_EVICTION_REASON_TYPE, ActiveType.RUNNING);
			ApParamEquip fromShopLend = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.FROM_SHOP.getValue(), ApParamEquipType.EQUIP_EVICTION_FROM_SHOP_TYPE, ActiveType.RUNNING);
			if (fromShopLend != null) {
				fromShop = fromShopLend.getValue();
			}
			EquipEvictionForm equipEvictionFormEx = new EquipEvictionForm();
			if (id != null && id > 0) {
				equipEvictionFormEx = equipmentManagerMgr.getEquipEvictionFormById(id);
				if (equipEvictionFormEx != null) {
					equipEvictionForm = equipEvictionFormEx;
					if (equipEvictionFormEx.getShop() == null || !checkShopInOrgAccessById(equipEvictionFormEx.getShop().getId())) {
						result.put("errMsg", R.getResource("common.permission.change"));
						result.put(ERROR, true);
						return SUCCESS;
					}
					if (equipEvictionFormEx.getToStockId() != null) {
						EquipStock stock = commonMgr.getEntityById(EquipStock.class, equipEvictionFormEx.getToStockId());
						if (stock != null) {
							stockCode = stock.getCode();
						}
					}
					
					if (equipEvictionFormEx.getCreateFormDate() != null) {
						createDate = DateUtil.convertDateByString(equipEvictionFormEx.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY) ;
					}
					
					if (equipEvictionFormEx.getEquipSugEviction() != null) {
						equipSugEvictionCode = equipEvictionFormEx.getEquipSugEviction().getCode();
					} else {
						equipSugEvictionCode = "";
					}
					
//					viTri = equipEvictionForm.getCurrentPostion();
//					if (equipEvictionForm.getCustomerNew() != null) {
//						newCustomerCode = equipEvictionForm.getCustomerNew().getShortCode();
//						newAddress = equipEvictionForm.getCustomerNew().getAddress();
//					}
//					if (equipEvictionForm.getNewShop() != null) {
//						newShopCode = equipEvictionForm.getNewShop().getShopCode();
//					}
					isEdit = 1;
					idForm = id;
					//Lay danh sach tap tin dinh kem neu co
					EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
					filter.setObjectId(id);
					filter.setObjectType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
					lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
					if (lstFileVo == null || lstFileVo.isEmpty()) {
						lstFileVo = new ArrayList<FileVO>();
					}
				}
			} else {
				equipEvictionForm.setToRepresentator(fromShop);
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.getChangeForm"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * Luu bien ban thu hoi thanh ly thiet bi
	 * 
	 * @author phuongvm
	 * @since 22/12/2014
	 * @return
	 */
	public String saveEviction() {
		resetToken(result);
		boolean error = true;
		errMsg = "";
		try {
			List<Long> lstEquipAttachFileId = new ArrayList<Long>();
			Integer maxLength = 0;
			if (idForm != null && idForm > 0) {
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(idForm);
				filter.setObjectType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
				//Xu ly Xoa file dinh kem
				if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
					String[] arrEquipAttachFile = equipAttachFileStr.split(",");
					for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
						String value = arrEquipAttachFile[i];
						if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
							lstEquipAttachFileId.add(Long.parseLong(value));
						}
					}
				}
				maxLength = lstFileVo.size() - lstEquipAttachFileId.size();
				if (maxLength < 0) {
					maxLength = 0;
				}
			}
			errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
			
			Date now = commonMgr.getSysDate();
			String message = "";
			String username = getCurrentUser().getUserName();
			Customer customer = null;
			Shop shop = null;
			EquipStock kho = null;
			//Customer newCustomer = null;
			//Shop newShop = null;
			List<Equipment> lstEquipment = new ArrayList<Equipment>();
			EquipStockTotalType loaiKho = null;
			//Integer choChuyenKH = 0;
			EquipEvictionForm equipEvictionForm = null;
			if (StringUtil.isNullOrEmpty(errMsg)) {
				if (idForm != null && idForm > 0) { //update 
					equipEvictionForm = equipmentManagerMgr.getEquipEvictionFormById(idForm);
					if (equipEvictionForm != null) {
						if (equipEvictionForm.getShop() == null || !checkShopInOrgAccessById(equipEvictionForm.getShop().getId())) {
							result.put("errMsg", R.getResource("common.permission.change"));
							result.put(ERROR, true);
							return SUCCESS;
						}
						equipEvictionForm.setUpdateDate(now);
						equipEvictionForm.setUpdateUser(username);
						shopCode = shopCode.trim().toUpperCase();
						shop = shopMgr.getShopByCode(shopCode);

						if (shop == null) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.npp.code");
						} else if (shop.getType() != null && (shop.getType().getSpecificType() == null
								|| (!ShopSpecificType.NPP.getValue().equals(shop.getType().getSpecificType().getValue()) && !ShopSpecificType.NPP_MT.getValue().equals(shop.getType().getSpecificType().getValue())))) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
						} 
						else if (!getListShopChildId().contains(shop.getId())) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
						} 
						else if (!ActiveType.RUNNING.getValue().equals(shop.getStatus().getValue()) && !ActiveType.STOPPED.getValue().equals(shop.getStatus().getValue())) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
						}
						
						Date cDate = null;
						if (!StringUtil.isNullOrEmpty(createDate)) {
							cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null");
						}
						
						customerCode = StringUtil.getFullCode(shopCode, customerCode);
						customer = customerMgr.getCustomerByCode(customerCode);
						if (customer == null) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.customer.code");
						}
						if (!customer.getShop().getId().equals(shop.getId())) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.manpp");
						}
						if (!StringUtil.isNullOrEmpty(fromRePresentor)) {
							message = ValidateUtil.validateField(fromRePresentor.trim(), "equip.eviction.resentation", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
						} else {
							message = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.resentation"));
						}
						if (!StringUtil.isNullOrEmpty(toRePresentor)) {
							message = ValidateUtil.validateField(toRePresentor.trim(), "equip.eviction.lender", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
						} else {
							message = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.resentation"));
						}
						if (!StringUtil.isNullOrEmpty(note)) {
							message = ValidateUtil.validateField(note, "common.description.lable.gc", 500, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						}
						if(!StringUtil.isNullOrEmpty(message)){
							result.put("errMsg", message);
							result.put(ERROR, error);
							return JSON;
						}
						EquipStockFilter filterStock = new EquipStockFilter();
						// set cac tham so tim kiem
						//KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
						filterStock.setkPaging(null);

						if (!StringUtil.isNullOrEmpty(stockCode)) {
							filterStock.setEquipStockCode(stockCode);
						}
						filterStock.setShopRoot(currentUser.getShopRoot().getShopId());
						filterStock.setStaffRoot(currentUser.getStaffRoot().getStaffId());
						ObjectVO<EquipStockVO> equipStockVO = equipmentManagerMgr.getListEquipStockVOByRole(filterStock);
						if (equipStockVO != null && !equipStockVO.getLstObject().isEmpty()) {
							kho = commonMgr.getEntityById(EquipStock.class, equipStockVO.getLstObject().get(0).getEquipStockId());
						} else {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.code.undefined.permision"));
							result.put(ERROR, error);
							return JSON;
						}
						
						//tamvnm: update dac ta moi. 30/06/2015
//						List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//						EquipPeriod equipPeriod = null;
//						if (lstPeriod == null || lstPeriod.size() == 0) {
//							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//							result.put(ERROR, error);
//							return JSON;
//						} else if (lstPeriod.size() > 0) {
//							// gan gia tri ky dau tien.
//							equipPeriod = lstPeriod.get(0);
//						}
						
						
						
						/** khong check type, loai kho set lai la kho */
						loaiKho = EquipStockTotalType.KHO;
//						if (viTri != null && viTri > -1) {
//							if (viTri.intValue() == EquipEvictionPositionType.CHO_CHUYEN_KHACH_HANG_KHAC.getValue().intValue()) {
//								choChuyenKH = 1;
//								newShopCode = newShopCode.trim().toUpperCase();
//								newShop = shopMgr.getShopByCode(newShopCode);
//								if (newShop == null) {
//									result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.npp.code.ex"));
//									result.put(ERROR, error);
//									return JSON;
//								}
//								
//								newCustomerCode = StringUtil.getFullCode(newShopCode, newCustomerCode);
//								newCustomer = customerMgr.getCustomerByCode(newCustomerCode);
//								if (newCustomer == null) {
//									result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.kh.code.ex"));
//									result.put(ERROR, error);
//									return JSON;
//								}
//								if (!newCustomer.getShop().getId().equals(newShop.getId())) {
//									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.manpp.ex"));
//									result.put(ERROR, error);
//									return JSON;
//								}
//							}
//						}
//						equipEvictionForm.setCurrentPostion(viTri);
//						equipEvictionForm.setNewShop(newShop);
//						if (newCustomer != null) {
//							equipEvictionForm.setNewAddress(newCustomer.getAddress());
//						} else {
//							equipEvictionForm.setNewAddress(null);
//						}
//						equipEvictionForm.setCustomerNew(newCustomer);
						equipEvictionForm.setCustomer(customer);
						if (customer.getPhone() != null) {
							equipEvictionForm.setPhone(customer.getPhone());
						} else {
							equipEvictionForm.setPhone(customer.getMobiphone());
						}
						equipEvictionForm.setCustomerCode(customer.getShortCode());
						equipEvictionForm.setCustomerName(customer.getCustomerName());
						equipEvictionForm.setAddress(customer.getAddress());
						equipEvictionForm.setShop(shop);
						if (status != null && status > -1) {
							equipEvictionForm.setFormStatus(status);
						}
						if (statusDelivery != null && statusDelivery > -1) {
							equipEvictionForm.setActionStatus(statusDelivery);
						}
						if (resultBB != null) {
							equipEvictionForm.setReason(resultBB);
						}
						if (!StringUtil.isNullOrEmpty(toRePresentor)) {
							equipEvictionForm.setToRepresentator(toRePresentor);
						}
						if (!StringUtil.isNullOrEmpty(fromRePresentor)) {
							equipEvictionForm.setFromRepresentor(fromRePresentor);
						}
						equipEvictionForm.setCreateFormDate(cDate);
						equipEvictionForm.setToStockId(kho.getId());
						equipEvictionForm.setToStockType(loaiKho.getValue());
						equipEvictionForm.setToStockName(kho.getName());
//						equipEvictionForm.setEquipPriod(equipPeriod);
						equipEvictionForm.setNote(note);
						if (lstId != null && lstId.size() > 0) {
							for (int i = 0; i < lstId.size(); i++) {
								Equipment equipment = equipmentManagerMgr.getEquipmentById(lstId.get(i));
								if (equipment != null) {
									lstEquipment.add(equipment);
								}
							}
						}
						if (lstEquipment.size() > 0) {
							EquipRecordFilter<EquipRepairForm> filter = new EquipRecordFilter<EquipRepairForm>();
							ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
							if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0) {
								BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
								//filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
								filterF.setLstId(lstEquipAttachFileId);
								filterF.setObjectId(idForm);
								filterF.setObjectType(EquipTradeType.CORRECTED.getValue());
								objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
							}
							//Luu file dinh kem
							filter.setLstEquipAttachFileId(lstEquipAttachFileId);
							filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));
							
							equipmentManagerMgr.updateEquipEvictionForm(equipEvictionForm, lstEquipment, filter);
							error = false;
						
							//kiem tra dieu kien luu bien ban de nghi thu hoi
							EquipSuggestEviction eSug = equipEvictionForm.getEquipSugEviction();
							if (eSug != null) {
								List<EquipSuggestEvictionDTL> lstequipSug = new ArrayList<EquipSuggestEvictionDTL>();
								if (StatusRecordsEquip.APPROVED.getValue().equals(status)) {// trang thai tu du thao -> duyet 
									lstequipSug = equipmentManagerMgr.getListSuggestDetailByRecordId(equipEvictionForm.getEquipSugEviction().getId(), null);
									List<EquipEvictionFormDtl> eDtl = equipmentManagerMgr.getEquipEvictionFormDtlByEquipEvictionFormId(equipEvictionForm.getId());
									if (eDtl != null && eDtl.size() > 0 && lstequipSug != null && lstequipSug.size() > 0) {
										// dong thiet bi trong bien ban de nghi thanh da xuat kho
										for (int c = 0, csize = eDtl.size(); c < csize; c++) {
											for (int n = 0, nsize = lstequipSug.size(); n < nsize; n++) {
												if (eDtl.get(c).getEquip() != null && lstequipSug.get(n).getEquipment() != null 
														&& eDtl.get(c).getEquip().getId().equals(lstequipSug.get(n).getEquipment().getId())) {
													lstequipSug.get(n).setDeliveryStatus(EquipDeliveryStatusType.EXPORTED.getValue());
													equipmentManagerMgr.updateStatusEquipSugEvicDTL(lstequipSug.get(n));
													break;
												}
											}
										}
										Boolean isComplete = true;
										lstequipSug = equipmentManagerMgr.getListSuggestDetailByRecordId(equipEvictionForm.getEquipSugEviction().getId(), EquipDeliveryStatusType.NOT_EXPORT.getValue());
										if (lstequipSug != null && lstequipSug.size() > 0) {
											isComplete = false;
										}
										if (isComplete) {
//											eSug.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
											EquipSuggestEvictionFilter<EquipSuggestEviction> filterSug = new EquipSuggestEvictionFilter<EquipSuggestEviction>();
											filterSug.setEquipSuggestEviction(eSug);
											filterSug.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
											equipSuggestEvictionMgr.updateEquipSuggestEviction(filterSug);
										}
									}
								} else if (StatusRecordsEquip.CANCELLATION.getValue().equals(status)) {// trang thai tu du thao -> huy
									lstequipSug = equipmentManagerMgr.getListSuggestDetailByRecordId(equipEvictionForm.getEquipSugEviction().getId(), null);
									List<EquipEvictionFormDtl> eDtl = equipmentManagerMgr.getEquipEvictionFormDtlByEquipEvictionFormId(equipEvictionForm.getId());
									if (eDtl != null && eDtl.size() > 0 && lstequipSug != null && lstequipSug.size() > 0) {
										// dong thiet bi trong bien ban de nghi thanh da xuat kho
										for (int c = 0, csize = eDtl.size(); c < csize; c++) {
											for (int n = 0, nsize = lstequipSug.size(); n < nsize; n++) {
												if (eDtl.get(c).getEquip() != null && lstequipSug.get(n).getEquipment() != null 
														&& eDtl.get(c).getEquip().getId().equals(lstequipSug.get(n).getEquipment().getId())) {
													lstequipSug.get(n).setDeliveryStatus(null);
													equipmentManagerMgr.updateStatusEquipSugEvicDTL(lstequipSug.get(n));
													break;
												}
											}
										}
										Boolean isComplete = true;
										lstequipSug = equipmentManagerMgr.getListSuggestDetailByRecordId(equipEvictionForm.getEquipSugEviction().getId(), EquipDeliveryStatusType.NOT_EXPORT.getValue());
										if (lstequipSug != null && lstequipSug.size() > 0) {
											isComplete = false;
										}
										if (isComplete) {
//											eSug.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
											EquipSuggestEvictionFilter<EquipSuggestEviction> filterSug = new EquipSuggestEvictionFilter<EquipSuggestEviction>();
											filterSug.setEquipSuggestEviction(eSug);
											filterSug.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
											equipSuggestEvictionMgr.updateEquipSuggestEviction(filterSug);
										}
									}
								}
							}
							
							if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0 && objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
								//Xu ly xoa cung file tren storeFileOnDisk
								List<String> lstFileName = new ArrayList<String>();
								for (EquipAttachFile itemFile : objVO.getLstObject()) {
									if (!StringUtil.isNullOrEmpty(itemFile.getUrl())) {
										lstFileName.add(itemFile.getUrl());
									}
								}
								FileUtility.deleteStoreFileOnDisk(lstFileName, Configuration.getFileEquipServerDocPath());
							}
						}
					}
					error = false;
				} else {//create 
//					EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//					if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null"));
//						result.put(ERROR, error);
//						return JSON;
//					}
					equipEvictionForm = new EquipEvictionForm();
					if (!StringUtil.isNullOrEmpty(shopCode)) {
						shopCode = shopCode.trim().toUpperCase();
						shop = shopMgr.getShopByCode(shopCode);
						if (shop == null) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.npp.code");
						} else if (!getListShopChildId().contains(shop.getId())) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
						} else if (!ActiveType.RUNNING.getValue().equals(shop.getStatus().getValue()) && !ActiveType.STOPPED.getValue().equals(shop.getStatus().getValue())) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
						}
						
						if (shop.getType() != null && (shop.getType().getSpecificType() == null
								|| (!ShopSpecificType.NPP.getValue().equals(shop.getType().getSpecificType().getValue()) && !ShopSpecificType.NPP_MT.getValue().equals(shop.getType().getSpecificType().getValue())))) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.notNPP");
						}
						
						Date cDate = null;
						if (!StringUtil.isNullOrEmpty(createDate)) {
							cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null");
						}
					
						//tamvnm: update dac ta moi. 30/06/2015
//						List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//						EquipPeriod equipPeriod = null;
//						if (lstPeriod == null || lstPeriod.size() == 0) {
//							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//						} else if (lstPeriod.size() > 0) {
//							// gan gia tri ky dau tien.
//							equipPeriod = lstPeriod.get(0);
//						}
						customerCode = StringUtil.getFullCode(shopCode, customerCode);
						customer = customerMgr.getCustomerByCode(customerCode);
						if (customer == null) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.customer.code");
						}
						if (!customer.getShop().getId().equals(shop.getId())) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.manpp");
						}
						if (!StringUtil.isNullOrEmpty(fromRePresentor)) {
							message = ValidateUtil.validateField(fromRePresentor.trim(), "equip.eviction.resentation", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
						} else {
							message = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.resentation"));
						}
						if (!StringUtil.isNullOrEmpty(toRePresentor)) {
							message = ValidateUtil.validateField(toRePresentor.trim(), "equip.eviction.lender", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
						} else {
							message = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.eviction.resentation"));
						}
						if (!StringUtil.isNullOrEmpty(note)) {
							message = ValidateUtil.validateField(note, "common.description.lable.gc", 500, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						}
						if(!StringUtil.isNullOrEmpty(message)){
							result.put("errMsg", message);
							result.put(ERROR, error);
							return JSON;
						}
						EquipStockFilter filterStock = new EquipStockFilter();
						// set cac tham so tim kiem
						//KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
						filterStock.setkPaging(null);
						
						if (!StringUtil.isNullOrEmpty(stockCode)) {
							filterStock.setEquipStockCode(stockCode);
						}
						filterStock.setShopRoot(currentUser.getShopRoot().getShopId());
						filterStock.setStaffRoot(currentUser.getStaffRoot().getStaffId());
						ObjectVO<EquipStockVO> equipStockVO = equipmentManagerMgr.getListEquipStockVOByRole(filterStock);
						if (equipStockVO != null && !equipStockVO.getLstObject().isEmpty()) {
							kho = commonMgr.getEntityById(EquipStock.class, equipStockVO.getLstObject().get(0).getEquipStockId());
						} else {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.code.undefined.permision"));
							result.put(ERROR, error);
							return JSON;
						}

						/** khong check type, loai kho set lai la kho */
						loaiKho = EquipStockTotalType.KHO;

//						if (viTri != null && viTri > -1) {
//							if (viTri.intValue() == EquipEvictionPositionType.CHO_CHUYEN_KHACH_HANG_KHAC.getValue().intValue()) {
//								choChuyenKH = 1;
//								newShopCode = newShopCode.trim().toUpperCase();
//								newShop = shopMgr.getShopByCode(newShopCode);
//								if (newShop == null) {
//									result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.npp.code.ex"));
//									result.put(ERROR, error);
//									return JSON;
//								}
//								newCustomerCode = StringUtil.getFullCode(newShopCode, newCustomerCode);
//								newCustomer = customerMgr.getCustomerByCode(newCustomerCode);
//								if (newCustomer == null) {
//									result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.kh.code.ex"));
//									result.put(ERROR, error);
//									return JSON;
//								}
//								if (!newCustomer.getShop().getId().equals(newShop.getId())) {
//									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.manpp.ex"));
//									result.put(ERROR, error);
//									return JSON;
//								}
//							}
//						}
//						equipEvictionForm.setCurrentPostion(viTri);
//						equipEvictionForm.setNewShop(newShop);
//						equipEvictionForm.setCustomerNew(newCustomer);
//						if (newCustomer != null) {
//							equipEvictionForm.setNewAddress(newCustomer.getAddress());
//						} else {
//							equipEvictionForm.setNewAddress(null);
//						}
						equipEvictionForm.setCustomer(customer);
						equipEvictionForm.setCustomerCode(customer.getShortCode());
						equipEvictionForm.setCustomerName(customer.getCustomerName());
						if (customer.getPhone() != null) {
							equipEvictionForm.setPhone(customer.getPhone());
						} else {
							equipEvictionForm.setPhone(customer.getMobiphone());
						}
						
						equipEvictionForm.setAddress(customer.getAddress());
						equipEvictionForm.setShop(shop);
						equipEvictionForm.setCreateUser(username);
						if (status != null && status > -1) {
							equipEvictionForm.setFormStatus(status);
						}
						if (statusDelivery != null && statusDelivery > -1) {
							equipEvictionForm.setActionStatus(statusDelivery);
						}
						if (resultBB != null) {
							equipEvictionForm.setReason(resultBB);
						}
						if (!StringUtil.isNullOrEmpty(toRePresentor)) {
							equipEvictionForm.setToRepresentator(toRePresentor);
						}
						if (!StringUtil.isNullOrEmpty(fromRePresentor)) {
							equipEvictionForm.setFromRepresentor(fromRePresentor);
						}
						equipEvictionForm.setCreateDate(now);
						equipEvictionForm.setCreateFormDate(cDate);
						equipEvictionForm.setToStockId(kho.getId());
						equipEvictionForm.setToStockType(loaiKho.getValue());
						equipEvictionForm.setToStockName(kho.getName());
//						equipEvictionForm.setEquipPriod(equipPeriod);
						equipEvictionForm.setNote(note);
						if (lstId != null && lstId.size() > 0) {
							for (int i = 0; i < lstId.size(); i++) {
								Equipment equipment = equipmentManagerMgr.getEquipmentById(lstId.get(i));
								if (equipment != null) {
									lstEquipment.add(equipment);
								}
							}
						}
						if (lstEquipment.size() > 0) {
							EquipRecordFilter<EquipRepairForm> filter = new EquipRecordFilter<EquipRepairForm>();
							//Luu file dinh kem
							filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));
							equipmentManagerMgr.createEquipEvictionFormX(equipEvictionForm, lstEquipment, filter);
							error = false;
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.saveEviction"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	/**
	 * Upload File
	 * 
	 * @author hunglm16
	 * @since January 30,2015
	 * */
	private List<FileVO> saveUploadFileByEquip(List<File> lstFile, List<String> lstFileFileName, List<String> lstFileContentType) {
		try {
			if (lstFile != null && lstFile.size() > 0) {
				List<FileVO> lstFileVO = FileUtility.storeFileOnDisk(lstFile, lstFileFileName, lstFileContentType, null, EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
				if (lstFileVO != null && !lstFileVO.isEmpty()) {
					String thumbUrl = Configuration.getEquipThumbUrlAttachFile();
					for (FileVO voFile : lstFileVO) {
						voFile.setThumbUrl(thumbUrl);
					}
				}
				return lstFileVO;
			}
			System.gc();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.saveUploadFileByEquip"), createLogErrorStandard(actionStartTime));
		}
		return new ArrayList<FileVO>();
	}
	/**
	 * Lay danh sach thiet bi co trong bien ban giao nhan khach hang
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * @return
	 */
	public String getListEquipInCustomer() {
		try {
			result.put("rows", new ArrayList<StockGeneralVO>());
			if (!StringUtil.isNullOrEmpty(customerCode) && !StringUtil.isNullOrEmpty(shopCode)) {
				customerCode = StringUtil.getFullCode(shopCode, customerCode);
				Customer c = customerMgr.getCustomerByCode(customerCode);
				List<EquipmentExVO> lst = null;
				if (c != null) {
					if (id != null && id > -1) {
						List<EquipEvictionFormDtl> lstDetail = equipmentManagerMgr.getEquipEvictionFormDtlByEquipEvictionFormId(id);
						List<Long> lstEquipId = new ArrayList<Long>();
						if (lstDetail != null && lstDetail.size() > 0) {
							for (int i = 0; i < lstDetail.size(); i++) {
								lstEquipId.add(lstDetail.get(i).getEquip().getId());
							}
							result.put("lstEquipId", lstEquipId);
						}
						lst = equipmentManagerMgr.getListAllEquipmentDeliveryByCustomerId(c.getId(), lstEquipId);
					} else {
						lst = equipmentManagerMgr.getListEquipmentDeliveryByCustomerId(c.getId());
					}
					if (lst != null && lst.size() > 0) {
						result.put("rows", lst);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentEvictionManageAction.getListEquipInCustomer"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	public String getCustomerAddr() {
		return customerAddr;
	}

	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getStatusAction() {
		return statusAction;
	}

	public void setStatusAction(Integer statusAction) {
		this.statusAction = statusAction;
	}

	public Integer getStatusForm() {
		return statusForm;
	}

	public void setStatusForm(Integer statusForm) {
		this.statusForm = statusForm;
	}

	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getFromRePresentor() {
		return fromRePresentor;
	}

	public void setFromRePresentor(String fromRePresentor) {
		this.fromRePresentor = fromRePresentor;
	}

	public String getToRePresentor() {
		return toRePresentor;
	}

	public void setToRePresentor(String toRePresentor) {
		this.toRePresentor = toRePresentor;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getResultBB() {
		return resultBB;
	}

	public void setResultBB(String resultBB) {
		this.resultBB = resultBB;
	}

	public Integer getViTri() {
		return viTri;
	}

	public void setViTri(Integer viTri) {
		this.viTri = viTri;
	}

	public String getNewShopCode() {
		return newShopCode;
	}

	public void setNewShopCode(String newShopCode) {
		this.newShopCode = newShopCode;
	}

	public String getNewCustomerCode() {
		return newCustomerCode;
	}

	public void setNewCustomerCode(String newCustomerCode) {
		this.newCustomerCode = newCustomerCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatusDelivery() {
		return statusDelivery;
	}

	public void setStatusDelivery(Integer statusDelivery) {
		this.statusDelivery = statusDelivery;
	}

	public EquipEvictionForm getEquipEvictionForm() {
		return equipEvictionForm;
	}

	public void setEquipEvictionForm(EquipEvictionForm equipEvictionForm) {
		this.equipEvictionForm = equipEvictionForm;
	}

	public Integer getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}

	public String getNewAddress() {
		return newAddress;
	}

	public void setNewAddress(String newAddress) {
		this.newAddress = newAddress;
	}

	public Long getIdForm() {
		return idForm;
	}

	public void setIdForm(Long idForm) {
		this.idForm = idForm;
	}

	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}

	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public List<Map<String, Object>> getListObjectBeans() {
		return listObjectBeans;
	}

	public void setListObjectBeans(List<Map<String, Object>> listObjectBeans) {
		this.listObjectBeans = listObjectBeans;
	}

	public Integer getWindowWidth() {
		return windowWidth;
	}

	public void setWindowWidth(Integer windowWidth) {
		this.windowWidth = windowWidth;
	}

	public Integer getWindowHeight() {
		return windowHeight;
	}

	public void setWindowHeight(Integer windowHeight) {
		this.windowHeight = windowHeight;
	}

	public List<File> getLstFile() {
		return lstFile;
	}

	public void setLstFile(List<File> lstFile) {
		this.lstFile = lstFile;
	}

	public List<String> getLstFileContentType() {
		return lstFileContentType;
	}

	public void setLstFileContentType(List<String> lstFileContentType) {
		this.lstFileContentType = lstFileContentType;
	}

	public List<String> getLstFileFileName() {
		return lstFileFileName;
	}

	public void setLstFileFileName(List<String> lstFileFileName) {
		this.lstFileFileName = lstFileFileName;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public String getEquipAttachFileStr() {
		return equipAttachFileStr;
	}

	public void setEquipAttachFileStr(String equipAttachFileStr) {
		this.equipAttachFileStr = equipAttachFileStr;
	}

	public String getEquipSugEvictionCode() {
		return equipSugEvictionCode;
	}

	public void setEquipSugEvictionCode(String equipSugEvictionCode) {
		this.equipSugEvictionCode = equipSugEvictionCode;
	}

	public List<ApParamEquip> getLstReasonEviction() {
		return lstReasonEviction;
	}

	public void setLstReasonEviction(List<ApParamEquip> lstReasonEviction) {
		this.lstReasonEviction = lstReasonEviction;
	}

	public String getFromShop() {
		return fromShop;
	}

	public void setFromShop(String fromShop) {
		this.fromShop = fromShop;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}

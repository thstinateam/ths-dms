package ths.dms.web.action.equipment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipLostRecord;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipStockTransForm;
import ths.dms.core.entities.EquipStockTransFormDtl;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PerFormStatus;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.Rpt_TB_QLCK;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;
//import ths.core.entities.vo.rpt.Rpt_TB_QLCK;

/**
 * Action Quan ly giao dịch kho
 * 
 * @author hoanv25
 * @since December 14,2014
 */
public class EquipmentStockAction extends AbstractAction {

	private static final long serialVersionUID = 5330840248556870465L;

	EquipmentManagerMgr equipmentManagerMgr;
	EquipRecordMgr equipRecordMgr;
	CommonMgr commonMgr;
	StaffMgr staffMgr;
	ShopMgr shopMgr;

	// TODO Khai bai cac thuoc tinh GETTER/SETTER
	/**
	 * Xu ly khai bao cac ham getter/ setter
	 * 
	 * @author hoanv25
	 * @since December 14,2014
	 * */
	private EquipmentVO equipmentStockVO = new EquipmentVO();
	private EquipmentVO equipmentShopVO = new EquipmentVO();
	private File excelFile;
	private String excelFileContentType;
	private Long id;

	private String code;
	private String name;
	private String fromDate;
	private String toDate;
	private String createFromDate;
	private String createToDate;
	private String fromStock;
	private String toStock;
	private String stockCode;
	private String shopCode;
	private String shopName;
	private String equipCode;
	private String seriNumber;
	private String lstEquipDelete;
	private String lstEquipAdd;
	private String lstDell;
	private String lstEquipId;
	private String downloadPath;
	private String equipAttachFileStr;
	private String createDate;
	private String note;
	
	private Long categoryId;
	private Long groupId;
	private Long providerId;

	private Integer yearManufacture;
	private Integer status;
	private Integer statusPerform;
	private Integer statusStr;
	private Integer fromMonth;
	private Integer toMonth;
	private Integer statusRecord;
	private Integer perfromStatusRecord;
	private Integer statusStock;
	private Integer windowWidth;
	private Integer windowHeight;

	private List<Long> lstId;
	private List<Integer> lstPerformStatus;
	private List<Long> lstIdEquip;
	private List<Long> lstPrintRecodeStockTran;
	private List<String> lstEquipCode;
	private List<String> lstSeriNumber;
	private List<String> lstToStockCode;
	private List<String> deletedEquipmentCodes;
	private List<File> lstFile;
	private List<String> lstFileContentType;
	private List<String> lstFileFileName;
	private List<FileVO> lstFileVo;
	// private List<String> lstDell;

	private List<EquipmentVO> listEquipBrand;
	private List<EquipmentVO> listEquipProvider;
	private List<EquipmentVO> listEquipCategory;
	private List<EquipmentVO> listChangeEquipGroupManager;
	private List<Long> lstIdRecord;
	private List<EquipmentDeliveryVO> equipments;
	private Long fromStockId;
	private Date startLogDate;

	@Override
	public void prepare() throws Exception {
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
		super.prepare();
		startLogDate = DateUtil.now();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			listEquipCategory = equipmentManagerMgr.getListEquipmentCategory(filter);
			listEquipProvider = equipmentManagerMgr.getListEquipmentProvider(filter);

			Cycle cycleSys = cycleMgr.getCycleByCurrentSysdate();
			//Lay mac dinh cho tu ngay dn ngay
			if (cycleSys != null && cycleSys.getBeginDate() != null) {
				fromDate = DateUtil.toDateString(cycleSys.getBeginDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}

		return SUCCESS;
	}

	/**
	 * Tim kiem bien ban chuyen kho
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	public String searchEquipmentStockChange() {
		try {
			result.put("rows", new ArrayList<EquipmentVO>());
			result.put("total", 0);
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			KPaging<EquipmentVO> kPaging = new KPaging<EquipmentVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setkPaging(kPaging);
			Date fDate = null;
			Date tDate = null;
			Date cFDate = null;
			Date cTDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setToDate(tDate);
			}
			if (!StringUtil.isNullOrEmpty(createFromDate)) {
				cFDate = DateUtil.parse(createFromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setCreateFromDate(cFDate);
			}
			if (!StringUtil.isNullOrEmpty(createToDate)) {
				cTDate = DateUtil.parse(createToDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setCreateToDate(cTDate);
			}
			
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(toStock)) {
				filter.setToStock(toStock);
			}
			if (!StringUtil.isNullOrEmpty(fromStock)) {
				filter.setFromStock(fromStock);
			}
			if (status != null) {
				filter.setStatus(status);
			}
			if (statusPerform != null) {
				filter.setStatusPerform(statusPerform);
			}
			if (currentUser.getShopRoot() != null) {
				filter.setShopRoot(currentUser.getShopRoot().getShopId());
			}
			if (currentUser.getStaffRoot() != null) {
				filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			}
			ObjectVO<EquipmentVO> lstGroupEquipment = null;
			lstGroupEquipment = equipmentManagerMgr.searchEquipmentStockChange(filter);
			if (lstGroupEquipment != null && lstGroupEquipment.getLstObject() != null && !lstGroupEquipment.getLstObject().isEmpty()) {
				result.put("total", lstGroupEquipment.getkPaging().getTotalRows());
				result.put("rows", lstGroupEquipment.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return JSON;
	}

	/**
	 * Export ecxel
	 * 
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	public String exportExcel() {
		try {
			// tim kiem
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			Date fDate = null;
			Date tDate = null;
			Date cFDate = null;
			Date cTDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setToDate(tDate);
			}
			if (!StringUtil.isNullOrEmpty(createFromDate)) {
				cFDate = DateUtil.parse(createFromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setCreateFromDate(cFDate);
			}
			if (!StringUtil.isNullOrEmpty(createToDate)) {
				cTDate = DateUtil.parse(createToDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setCreateToDate(cTDate);
			}

			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(toStock)) {
				filter.setToStock(toStock);
			}
			if (!StringUtil.isNullOrEmpty(fromStock)) {
				filter.setFromStock(fromStock);
			}
			if (status != null) {
				filter.setStatus(status);
			}
			if (statusPerform != null) {
				filter.setStatusPerform(statusPerform);
			}
			if (currentUser.getShopRoot() != null) {
				filter.setShopRoot(currentUser.getShopRoot().getShopId());
			}
			List<EquipmentVO> lstGroupEquipment = null;
			lstGroupEquipment = equipmentManagerMgr.searchEquipmentStockChangeExcel(filter);

			if (lstGroupEquipment != null && lstGroupEquipment.size() > 0) {
				SXSSFWorkbook workbook = null;
				FileOutputStream out = null;
				try {
					// Init XSSF workboook
					String outputName = ConstantManager.TEMPLATE_STOCK_TRAN_FORM + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;

					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					// Tao shett
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

					// Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(18);
					// set static Column width
					mySheet.setColumnsWidth(sheetData, 0, 200, 250, 120, 170, 170, 170);
					// Size Row
					mySheet.setRowsHeight(sheetData, 0, 20);
					sheetData.setDisplayGridlines(true);

					int iRow = 0, iColumn = 0;
					// tao column header
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.stocktran");
					String[] lstHeaders = headers.split(";");
					for (int i = 0; i < lstHeaders.length; i++) {
						mySheet.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					}
					XSSFCellStyle textStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.NORMAL).clone();
					DataFormat fmt = workbook.createDataFormat();
					textStyle.setDataFormat(fmt.getFormat("@"));
					// set data
					for (int i = 0, size = lstGroupEquipment.size(); i < size; i++) {
						EquipmentVO equipRepairFormVO = lstGroupEquipment.get(i);
						if (null != equipRepairFormVO) {
							iColumn = 0;
							++iRow;
							// set thong tin chung
							mySheet.addCell(sheetData, iColumn++, iRow, equipRepairFormVO.getCode(), textStyle);
							mySheet.addCell(sheetData, iColumn++, iRow, equipRepairFormVO.getEquipCode(), textStyle);
							mySheet.addCell(sheetData, iColumn++, iRow, equipRepairFormVO.getStockCode(), textStyle);
							String status = "";
							String performStatus = "";
							//0. Dự thảo, 1. Chờ duyệt, 2. Duyệt, 3. Không duyệt, 4. Hủy, 5. Sửa chữa, 6. Hoàn tất
							if (StatusRecordsEquip.DRAFT.getValue().equals(equipRepairFormVO.getStatus())) {
								status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duthao");
							} else if (StatusRecordsEquip.APPROVED.getValue().equals(equipRepairFormVO.getStatus())) {
								status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duyet");
							} else if (StatusRecordsEquip.CANCELLATION.getValue().equals(equipRepairFormVO.getStatus())) {
								status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.huy");
							}
							if (PerFormStatus.ONGOING.getValue().equals(equipRepairFormVO.getPerformStatus())) {
								performStatus = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.perform.ongoing");
							} else if (PerFormStatus.SUCCESS.getValue().equals(equipRepairFormVO.getPerformStatus())) {
								performStatus = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.perform.success");
							} else if (PerFormStatus.CANCEL.getValue().equals(equipRepairFormVO.getPerformStatus())) {
								performStatus = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.perform.huy");
							}
							mySheet.addCell(sheetData, iColumn++, iRow, status, textStyle);
							mySheet.addCell(sheetData, iColumn++, iRow, performStatus, textStyle);
							mySheet.addCell(sheetData, iColumn++, iRow, equipRepairFormVO.getNote(), textStyle);
						}
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} catch (Exception e) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					LogUtility.logError(e, "EquipmentStockAction.exportExcel" + e.getMessage());
					if (workbook != null) {
						workbook.dispose();
					}
					if (out != null) {
						out.close();
					}
				}
				System.gc();
				return JSON;
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach thiet bi bien ban chuyen kho khi chuyen trang
	 * 
	 * @author hoanv25
	 * @since December 06/01,2015
	 */
	public String searchEquipmentStock() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			if (id != null && id > 0) {
				KPaging<EquipmentDeliveryVO> kPaging = new KPaging<EquipmentDeliveryVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);

				EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
				equipmentFilter.setkPaging(kPaging);

				if (fromStockId != null) {
					equipmentFilter.setFromStockId(fromStockId);
				}
				if (!StringUtil.isNullOrEmpty(code)) {
					equipmentFilter.setCode(code);
				}

				ObjectVO<EquipmentDeliveryVO> equipmentStockTransForm = equipmentManagerMgr.getListEquipmentequipmentStockTransFormVOByFilter(equipmentFilter);
				if (equipmentStockTransForm.getLstObject() != null) {
					result.put("total", equipmentStockTransForm.getkPaging().getTotalRows());
					result.put("rows", equipmentStockTransForm.getLstObject());
				}
			}
			// load lai khi chon F9
			/*
			 * if(shopFrom != null && shopTo != null){
			 * 
			 * }
			 */
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Xu ly Luu khi chuyen trang thai
	 * 
	 * @author hoanv25
	 * @since 26/12/2014
	 */
	public String saveStatusRecord() {
		resetToken(result);
		try {
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			if (equipPeriod == null) {
//				result.put(ERROR, true);
//				result.put("errMsg", "Kỳ hiện tại không MỞ!");
//				return JSON;
//			}
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
				return SUCCESS;
			}
			if (statusRecord == null || (!StatusRecordsEquip.APPROVED.getValue().equals(statusRecord) && !StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord))) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("action.status.err"));
				return JSON;
			}
			List<FormErrVO> lstError = new ArrayList<FormErrVO>();
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				for (int i = 0; i < lstIdRecord.size(); i++) {
					EquipStockTransForm e = equipmentManagerMgr.getEquipStockTransFormById(lstIdRecord.get(i), currentUser.getShopRoot().getShopId());
					FormErrVO formErrVO = new FormErrVO();
					if (e != null && statusRecord != null) {
						//Kiem tra ky
//						List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(e.getCreateFormDate());
//						int periodSize = lstPeriod == null ? 0 : lstPeriod.size();
//						EquipPeriod equipPeriod = null;
//						if (lstPeriod == null || periodSize == 0) {
//							result.put(ERROR, true);
//							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//							return JSON;
//						} else if (periodSize > 0) {
//							// gan gia tri ky dau tien.
//							equipPeriod = lstPeriod.get(0);
//						}
						
						
						
						if (e.getStatus() != null && e.getStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
							formErrVO.setFormStatusErr(1);
						}
						if (e.getStatus() != null && e.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
							formErrVO.setFormStatusErr(1);
						}
//						if (!e.getEquipPeriod().getStatus().equals(EquipPeriodType.OPENED) || e.getEquipPeriod().getFromDate().after(DateUtil.now()) || e.getEquipPeriod().getToDate().before(DateUtil.now())) {
//							formErrVO.setPeriodError(1);
//						}
						if (formErrVO.getFormStatusErr() == null && formErrVO.getPeriodError() == null) {
							int statusRe = 0;
							// du thao
							if (e.getStatus() != null && e.getStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
								if (statusRecord == 2) {
									// trang thai tu du thao -> duyet
									statusRe = 2;
//									e.setEquipPeriod(equipPeriod);
								} else if (statusRecord == 4) {
									// trang thai tu du thao -> huy
									statusRe = 4;
								}
								e.setUpdateDate(DateUtil.now());
								// e.setApproveDate(DateUtil.now());
								e.setUpdateUser(currentUser.getUserName());
								e.setStatus(statusRecord);
							}
							if (statusRe != 0) {
								equipmentManagerMgr.saveStatusEquipStockRecord(e, statusRe);
							}
						} else {
							formErrVO.setFormCode(e.getCode());
							lstError.add(formErrVO);
						}
					}
				}
				if (lstError.size() > 0) {
					result.put("lstRecordError", lstError);
				}
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentStockAction.saveStatusRecord"), createLogErrorStandard(startLogDate));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/** Luu trang thai thuc hien cua cua bien ban
	 * @author dungnt19
	 * @return
	 */
	public String saveDonePerformStatusRecord() {
		resetToken(result);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
				return SUCCESS;
			}
			
			PerFormStatus perFormStatus = PerFormStatus.parseValue(perfromStatusRecord);
			if (perFormStatus == null || (!perFormStatus.equals(PerFormStatus.ONGOING) && !perFormStatus.equals(PerFormStatus.SUCCESS))) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("equipment.manager.equipment.phone.number.format", R.getResource("action.status.perform")));
				return JSON;
			}
			
			List<FormErrVO> lstError = new ArrayList<FormErrVO>();
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				for (int i = 0; i < lstIdRecord.size(); i++) {
					EquipStockTransForm e = equipmentManagerMgr.getEquipStockTransFormById(lstIdRecord.get(i), currentUser.getShopRoot().getShopId());
					FormErrVO formErrVO = new FormErrVO();
					if (e != null && perfromStatusRecord != null) {
						//Kiem tra ky
//						List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(e.getCreateFormDate());
//						int periodSize = lstPeriod == null ? 0 : lstPeriod.size();
//						EquipPeriod equipPeriod = null;
//						if (lstPeriod == null || periodSize == 0) {
//							result.put(ERROR, true);
//							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//							return JSON;
//						} else if (periodSize > 0) {
//							// gan gia tri ky dau tien.
//							equipPeriod = lstPeriod.get(0);
//						}
						
						if (e.getPerformStatus() != null && e.getPerformStatus().equals(PerFormStatus.SUCCESS.getValue())) {
							formErrVO.setFormStatusErr(1);
						}
						
						if (formErrVO.getFormStatusErr() == null && formErrVO.getPeriodError() == null) {
							int performStatusRe = PerFormStatus.ONGOING.getValue();
							if (e.getPerformStatus() != null && e.getPerformStatus().equals(PerFormStatus.ONGOING)) {
								if (perfromStatusRecord == PerFormStatus.SUCCESS.getValue()) {
									// trang thai thuc hien tu dang thuc hien -> hoan thanh
									performStatusRe = PerFormStatus.SUCCESS.getValue();
//									e.setEquipPeriod(equipPeriod);
								}
								e.setUpdateDate(DateUtil.now());
								e.setUpdateUser(currentUser.getUserName());
								e.setPerformStatus(PerFormStatus.SUCCESS);
							}
							if (performStatusRe != PerFormStatus.ONGOING.getValue()) {
								equipmentManagerMgr.saveDonePerformStatusRecord(e, performStatusRe);
							}
						} else {
							formErrVO.setFormCode(e.getCode());
							lstError.add(formErrVO);
						}
					}
				}
				if (lstError.size() > 0) {
					result.put("lstRecordError", lstError);
				}
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentStockAction.saveDonePerformStatusRecord"), createLogErrorStandard(startLogDate));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Lay thong tin bien ban khi chinh sua
	 * 
	 * @author hoanv25
	 * @since 6/1/2014
	 */
	public String viewEquipStockChange() {
		resetToken(result);
		try {
			if (id != null && id > 0) {
				EquipStockTransForm lstEquipStockTransForm = equipmentManagerMgr.getEquipStockTransForm(id);
				if (lstEquipStockTransForm != null) {
					if (!StatusRecordsEquip.DRAFT.getValue().equals(lstEquipStockTransForm.getStatus())) {
						return PAGE_NOT_FOUND;
					}
					EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
					equipmentFilter.setCode(lstEquipStockTransForm.getCode());
					ObjectVO<EquipmentDeliveryVO> vos = equipmentManagerMgr.getListEquipmentequipmentStockTransFormVOByFilter(equipmentFilter);
					if (vos != null && vos.getLstObject() != null) {
						for (EquipmentDeliveryVO vo : vos.getLstObject()) {
							if (!StringUtil.isNullOrEmpty(vo.getStock())) {
								EquipStock eStock = equipmentManagerMgr.getEquipStockbyCode(vo.getStock());
								if (eStock == null || eStock.getShop() == null || !checkShopInOrgAccessById(eStock.getShop().getId())) {
									return PAGE_NOT_PERMISSION;
								}
							}
						}
					}
					
					equipmentStockVO.setCode(lstEquipStockTransForm.getCode());
					equipmentStockVO.setStatus(lstEquipStockTransForm.getStatus());
					equipmentStockVO.setPerformStatus(lstEquipStockTransForm.getPerformStatus().getValue());
					equipmentStockVO.setNote(lstEquipStockTransForm.getNote());
					if (lstEquipStockTransForm.getCreateFormDate() != null) {
						equipmentStockVO.setCreateFormDate(DateUtil.convertDateByString(lstEquipStockTransForm.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					}
					
					// Shop lstShop =
					// equipmentManagerMgr.getListEquipStockTransForm(lstEquipStockTransForm.getFromStockId());
					// equipmentShopVO.setFromStock(lstShop.getShopCode());
					// Shop lstToShop =
					// equipmentManagerMgr.getListToStockTransForm(lstEquipStockTransForm.getToStockId());
					// equipmentShopVO.setToStock(lstToShop.getShopCode());
				} else {
					return PAGE_NOT_FOUND;
				}
				// Lay danh sach tap tin dinh kem neu co
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(id);
				filter.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return SUCCESS;
	}
	
	/**
	 * Ham lay thong tin khi view bien ban trang thai duyet, huy
	 * 
	 * @author phuongvm
	 * @since 24/11/2015
	 */
	public String viewEquipStockChangeApproved() {
		resetToken(result);
		try {
			if (id != null && id > 0) {
				EquipStockTransForm lstEquipStockTransForm = equipmentManagerMgr.getEquipStockTransForm(id);
				if (lstEquipStockTransForm != null) {
					if (!StatusRecordsEquip.APPROVED.getValue().equals(lstEquipStockTransForm.getStatus()) && !StatusRecordsEquip.CANCELLATION.getValue().equals(lstEquipStockTransForm.getStatus())) {
						return PAGE_NOT_FOUND;
					}
					EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
					equipmentFilter.setCode(lstEquipStockTransForm.getCode());
					ObjectVO<EquipmentDeliveryVO> vos = equipmentManagerMgr.getListEquipmentequipmentStockTransFormVOByFilter(equipmentFilter);
					if (vos != null && vos.getLstObject() != null) {
						for (EquipmentDeliveryVO vo : vos.getLstObject()) {
							if (!StringUtil.isNullOrEmpty(vo.getStock())) {
								EquipStock eStock = equipmentManagerMgr.getEquipStockbyCode(vo.getStock());
								if (eStock == null || eStock.getShop() == null || !checkShopInOrgAccessById(eStock.getShop().getId())) {
									return PAGE_NOT_PERMISSION;
								}
							}
						}
					}
					equipmentStockVO.setCode(lstEquipStockTransForm.getCode());
					equipmentStockVO.setStatus(lstEquipStockTransForm.getStatus());
					equipmentStockVO.setPerformStatus(lstEquipStockTransForm.getPerformStatus().getValue());
					equipmentStockVO.setNote(lstEquipStockTransForm.getNote());
					if (lstEquipStockTransForm.getCreateFormDate() != null) {
						equipmentStockVO.setCreateFormDate(DateUtil.convertDateByString(lstEquipStockTransForm.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					}
				} else {
					return PAGE_NOT_FOUND;
				}
				// Lay danh sach tap tin dinh kem neu co
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(id);
				filter.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentStockAction.viewEquipStockChangeApproved"), createLogErrorStandard(startLogDate));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem danh sach thiet bi bien ban chuyen kho F9
	 * 
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	public String searchEquipmentDelivery() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			KPaging<EquipmentDeliveryVO> kPaging = new KPaging<EquipmentDeliveryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(kPaging);
			// if (!StringUtil.isNullOrEmpty(fromStock)) {
			// equipmentFilter.setStockCode(fromStock);
			// }else {
			// result.put(ERROR, true);
			// result.put("errMsg", "Bạn chưa nhập kho nguồn!");
			// return JSON;
			// }
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (categoryId != null) {
				equipmentFilter.setEquipCategoryId(categoryId);
			}
			if (groupId != null) {
				equipmentFilter.setEquipGroupId(groupId);
			}
			if (providerId != null) {
				equipmentFilter.setEquipProviderId(providerId);
			}
			if (yearManufacture != null) {
				equipmentFilter.setYearManufacture(yearManufacture);
			}
			if (!StringUtil.isNullOrEmpty(stockCode)) {
				equipmentFilter.setStockCode(stockCode);
			}
			List<Long> lstEquipmentId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstEquipId)) {
				for (String str : lstEquipId.split(",")) {
					lstEquipmentId.add(Long.valueOf(str));
				}
			}
			equipmentFilter.setLstEquipId(lstEquipmentId);

			List<String> lstEAdd = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipAdd))) {
				String[] lst = lstEquipAdd.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				equipmentFilter.setLstEquipAdd(lstEAdd);
			}
			if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
				lstEAdd = new ArrayList<String>();
				String[] lst = lstEquipDelete.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				equipmentFilter.setLstEquipDelete(lstEAdd);
			}
			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			if (currentUser.getStaffRoot() != null) {
				equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			}
			if (currentUser.getShopRoot() != null) {
				equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
			}
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentStockVOByFilter(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getkPaging().getTotalRows());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Kiem tra ki
	 * 
	 * @author hoanv25
	 * @since 6/1/2015
	 * @return
	 */
	public String checkEquipPeriod() {
		try {
			result.put(ERROR, false);
//			Date now = commonMgr.getSysDate();
//			EquipPeriod equipPeriod = null;
//			if (id == null || id <= 0) {
//				equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPeriod == null || !EquipPeriodType.OPENED.getValue().equals(equipPeriod.getStatus().getValue()) 
//						|| (DateUtil.compareDateWithoutTime(now, equipPeriod.getFromDate()) == -1) 
//						|| (DateUtil.compareDateWithoutTime(equipPeriod.getToDate(), now) == -1)) {
//					result.put(ERROR, true);
//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null"));
//				}
//			} else {
//				EquipStockTransForm equipStockTransForm = equipmentManagerMgr.getEquipStockTransFormById(id);
//				equipPeriod = equipStockTransForm.getEquipPeriod();
//				Date createFormDate = equipStockTransForm.getCreateFormDate();
//				if (equipPeriod == null || !EquipPeriodType.OPENED.getValue().equals(equipPeriod.getStatus().getValue()) 
//						|| (DateUtil.compareDateWithoutTime(createFormDate, equipPeriod.getFromDate()) == -1) 
//						|| (DateUtil.compareDateWithoutTime(equipPeriod.getToDate(), createFormDate) == -1)) {
//					result.put(ERROR, true);
//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.edit.invalid"));
//				}
//			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Kiem tra event ENTER ma thiet bi - so seri
	 * 
	 * @author hoanv25
	 * @since 08/1/2015
	 * @return
	 */
	public String getEquipment() {
		try {
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(null);
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (!StringUtil.isNullOrEmpty(fromStock)) {
				equipmentFilter.setStockCode(fromStock);
			}
			List<Long> lstEquipmentId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(lstEquipId)) {
				for (String str : lstEquipId.split(",")) {
					lstEquipmentId.add(Long.valueOf(str));
				}
			}
			equipmentFilter.setLstEquipId(lstEquipmentId);
			List<String> lstEDelete = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
				String[] lst = lstEquipDelete.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEDelete.add(lst[i]);
				}
				equipmentFilter.setLstEquipDelete(lstEDelete);
			}
			List<String> lstEAdd = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipAdd))) {
				String[] lst = lstEquipAdd.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				equipmentFilter.setLstEquipAdd(lstEAdd);
			}
			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentStockTransByFilter(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				equipments = equipmentDeliverys.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Kiem tra kho nguon co thiet bi khong
	 * 
	 * @author hoanv25
	 * @since 08/1/2015
	 * @return
	 */
	public String getCheckFromStockEquipment() {
		try {
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			KPaging<EquipmentDeliveryVO> kPaging = new KPaging<EquipmentDeliveryVO>();
			kPaging.setPage(0);
			kPaging.setPageSize(1);
			equipmentFilter.setkPaging(kPaging);

			if (!StringUtil.isNullOrEmpty(fromStock)) {
				equipmentFilter.setStockCode(fromStock);
			}
			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getCheckFromStockEquipment(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				equipments = equipmentDeliverys.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * cap nhat bien ban chuyen kho da duyet
	 * 
	 * @author phuongvm
	 * @since 3/4/2015
	 * @return
	 */
	public String createRecordStockChangeApproved() {
		resetToken(result);
		boolean error = false;
		String errMsg = "";
		Shop shop = null;
		try {
			List<Long> lstEquipAttachFileId = new ArrayList<Long>();
			Integer maxLength = 0;
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
//				isError = true;
//				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//				result.put("errMsg", errMsg);
//				result.put(ERROR, isError);
//				return JSON;
//			}
			if (id != null && id > 0) {
				// Xu ly update file
				EquipRecordFilter<FileVO> filterB = new EquipRecordFilter<FileVO>();
				filterB.setObjectId(id);
				filterB.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filterB).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
				// Xu ly Xoa file dinh kem
				if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
					String[] arrEquipAttachFile = equipAttachFileStr.split(",");
					for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
						String value = arrEquipAttachFile[i];
						if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
							lstEquipAttachFileId.add(Long.parseLong(value));
						}
					}
				}
				maxLength = lstFileVo.size() - lstEquipAttachFileId.size();
				if (maxLength < 0) {
					maxLength = 0;
				}
			}
			errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);

			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}

			Date sysDateIns = commonMgr.getSysDate();

			PerFormStatus perFormStatus = PerFormStatus.parseValue(statusPerform);
			if (perFormStatus == null || (!perFormStatus.equals(PerFormStatus.ONGOING) && !perFormStatus.equals(PerFormStatus.SUCCESS))) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("equipment.manager.equipment.phone.number.format", R.getResource("action.status.perform")));
				return JSON;
			}
			if (id == null) {
				errMsg = R.getResource("equipment.stock.change.record.invalid");
			} else if (id > 0) {
				// Cap nhat thong tin bien ban chuyen kho
				EquipStockTransForm equipStockTransForm = equipmentManagerMgr.getEquipStockTransFormById(id);
				if (equipStockTransForm == null ) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.stock.change.record.invalid"));
					return JSON;
				}
				
//				List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(equipStockTransForm.getCreateFormDate());
//				if (lstPeriod == null || lstPeriod.size() == 0) {
//					result.put(ERROR, true);
//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//					return JSON;
//				}
				
				equipStockTransForm.setUpdateDate(sysDateIns);
				equipStockTransForm.setUpdateUser(currentUser.getUserName());
				equipStockTransForm.setPerformStatus(perFormStatus);
				List<EquipStockTransFormDtl> equipmentStockTransFormDtl = new ArrayList<EquipStockTransFormDtl>();
				List<Long> addEquipmentIds = new ArrayList<Long>();
				if (lstId != null && !lstId.isEmpty() && lstPerformStatus != null && !lstPerformStatus.isEmpty()) {
					for (int i = 0; i < lstId.size(); i++) {
						// kiem tra kho dich
						EquipStock equipStock = null;
						Integer valPerformStatus = lstPerformStatus.get(i);
						if (!StringUtil.isNullOrEmpty(lstToStockCode.get(i))) {
							equipStock = equipmentManagerMgr.getEquipStockbyCode(lstToStockCode.get(i));
						}

						if (equipStock != null) {
							if (!StatusType.ACTIVE.getValue().equals(equipStock.getStatus().getValue())) {
								errMsg += " Kho " + lstToStockCode.get(i) + " đang ở trạng thái không hoạt động";
							}
						} else {
							result.put(ERROR, true);
							result.put("errMsg", "Kho nhận nhập vào không đúng");
							return JSON;
						}
						if (!StringUtil.isNullOrEmpty(errMsg)) {
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						EquipStockTransFormDtl equipmentStockTransFDtl = equipmentManagerMgr.getEquipStockTransFormDtlById(lstId.get(i));
						if (PerFormStatus.SUCCESS.equals(perFormStatus)) {
							if (PerFormStatus.ONGOING.equals(equipmentStockTransFDtl.getPerformStatus())) {
								valPerformStatus = PerFormStatus.SUCCESS.getValue();
							} else if (PerFormStatus.CANCEL.equals(equipmentStockTransFDtl.getPerformStatus())) { //trang thai bien ban la hoan thanh , trang thai chi tiet la huy thi bo qua ko cap nhat
								continue;
							}
						}
						if (!equipmentStockTransFDtl.getPerformStatus().getValue().equals(valPerformStatus) || !equipmentStockTransFDtl.getToStockId().equals(equipStock.getId())) {
							equipmentStockTransFDtl.setToStockId(equipStock.getId());
							equipmentStockTransFDtl.setToStockName(equipStock.getName());
							equipmentStockTransFDtl.setToShopName(equipStock.getShop().getShopName());
							if (!equipmentStockTransFDtl.getPerformStatus().getValue().equals(valPerformStatus)
									&& (PerFormStatus.SUCCESS.equals(equipmentStockTransFDtl.getPerformStatus()) || PerFormStatus.CANCEL.equals(equipmentStockTransFDtl.getPerformStatus()))) {
								result.put(ERROR, true);
								result.put("errMsg", "Không thể chuyển trạng thái thực hiện từ Hủy hoặc Hoàn thành sang Đang thực hiện");
								return JSON;
							}
							equipmentStockTransFDtl.setPerformStatus(PerFormStatus.parseValue(valPerformStatus));
							equipmentStockTransFDtl.setUpdateUser(currentUser.getUserName());
							equipmentStockTransFDtl.setUpdateDate(sysDateIns);
							equipmentStockTransFormDtl.add(equipmentStockTransFDtl);
						}
					}

					EquipRecordFilter<EquipStockTransForm> filter = new EquipRecordFilter<EquipStockTransForm>();
					ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
					if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0) {
						BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
						// filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
						filterF.setLstId(lstEquipAttachFileId);
						filterF.setObjectId(id);
						filterF.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
						objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
					}
					// Luu file dinh kem
					filter.setLstEquipAttachFileId(lstEquipAttachFileId);
					filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));

					equipStockTransForm = equipmentManagerMgr.updateStockTransFormApproved(equipStockTransForm, equipmentStockTransFormDtl, addEquipmentIds, filter, getLogInfoVO());

					if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0 && objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
						// Xu ly xoa cung file tren storeFileOnDisk
						List<String> lstFileName = new ArrayList<String>();
						for (EquipAttachFile itemFile : objVO.getLstObject()) {
							if (!StringUtil.isNullOrEmpty(itemFile.getUrl())) {
								lstFileName.add(itemFile.getUrl());
							}
						}
						FileUtility.deleteStoreFileOnDisk(lstFileName, Configuration.getFileEquipServerDocPath());
					}
				} else {
					EquipRecordFilter<EquipStockTransForm> filter = new EquipRecordFilter<EquipStockTransForm>();
					ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
					if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0) {
						BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
						// filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
						filterF.setLstId(lstEquipAttachFileId);
						filterF.setObjectId(id);
						filterF.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
						objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
					}
					// Luu file dinh kem
					filter.setLstEquipAttachFileId(lstEquipAttachFileId);
					filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));

					equipStockTransForm = equipmentManagerMgr.updateEquipmentStockTransForm(equipStockTransForm, addEquipmentIds, filter, getLogInfoVO());
				}

			}
		} catch (Exception e) {
			error = true;
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentStockAction.createRecordStockChangeApproved"), createLogErrorStandard(startLogDate));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Tao moi bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 6/1/2015
	 * @return
	 */
	public String createRecordStockChange() {
		resetToken(result);
		boolean error = false;
		String errMsg = "";
		Shop shop = null;
		try {
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
//				isError = true;
//				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//				result.put("errMsg", errMsg);
//				result.put(ERROR, isError);
//				return JSON;
//			}
			List<Long> lstEquipAttachFileId = new ArrayList<Long>();
			Integer maxLength = 0;
			if (id != null && id > 0) {
				// Xu ly update file
				EquipRecordFilter<FileVO> filterB = new EquipRecordFilter<FileVO>();
				filterB.setObjectId(id);
				filterB.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filterB).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
				// Xu ly Xoa file dinh kem
				if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
					String[] arrEquipAttachFile = equipAttachFileStr.split(",");
					for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
						String value = arrEquipAttachFile[i];
						if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
							lstEquipAttachFileId.add(Long.parseLong(value));
						}
					}
				}
				maxLength = lstFileVo.size() - lstEquipAttachFileId.size();
				if (maxLength < 0) {
					maxLength = 0;
				}
			}
			errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);

			Date cDate = null;
			if (!StringUtil.isNullOrEmpty(createDate)) {
				cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null");
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
			//tamvnm: update dac ta moi. 07/09/2015
			
//			List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//			int periodSize = lstPeriod == null ? 0 : lstPeriod.size();
//			EquipPeriod equipPeriod = null;
//			if (lstPeriod == null || periodSize == 0) {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//				return JSON;
//			} else if (periodSize > 0) {
//				// gan gia tri ky dau tien.
//				equipPeriod = lstPeriod.get(0);
//			}
			
			Date sysDateIns = commonMgr.getSysDate();
			
			// Kiem tra kho nguon thuoc VNM hay NPP
			PerFormStatus perFormStatus = PerFormStatus.parseValue(statusPerform);
			if (perFormStatus == null || (!perFormStatus.equals(PerFormStatus.ONGOING) && !perFormStatus.equals(PerFormStatus.SUCCESS))) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("equipment.manager.equipment.phone.number.format", R.getResource("action.status.perform")));
				return JSON;
			}
			if (status == null || (!StatusRecordsEquip.DRAFT.getValue().equals(status) && !StatusRecordsEquip.APPROVED.getValue().equals(status) && !StatusRecordsEquip.CANCELLATION.getValue().equals(status))) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("action.status.err"));
				return JSON;
			}
			if (perFormStatus.equals(PerFormStatus.SUCCESS) && !StatusRecordsEquip.APPROVED.getValue().equals(status)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("action.status.err"));
				return JSON;
			}
			if (id == null) {
				errMsg = "Không xác định được Biên bản chuyển kho";
			} else if (id > 0) {
				if (currentUser == null || currentUser.getShopRoot() == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
					return SUCCESS;
				}
				
				// Cap nhat thong tin bien ban chuyen kho
				EquipStockTransForm equipStockTransForm = equipmentManagerMgr.getEquipStockTransFormById(id, currentUser.getShopRoot().getShopId());
				equipStockTransForm.setStatus(status);
				equipStockTransForm.setUpdateDate(sysDateIns);
				equipStockTransForm.setUpdateUser(currentUser.getUserName());
				equipStockTransForm.setPerformStatus(perFormStatus);
				equipStockTransForm.setCreateFormDate(cDate);
				equipStockTransForm.setNote(note);
//				equipStockTransForm.setEquipPeriod(equipPeriod);
				//				List<Long> lstDelete = new ArrayList<Long>();
				//				String[] arrDeleteStr = lstDell.split(",");
				//				for (String value : arrDeleteStr) {
				//					if (StringUtil.isNumberInt(value)) {
				//						lstDelete.add(Long.valueOf(value));
				//					}
				//				}
				//				if (lstDelete != null && !lstDelete.isEmpty()) {
				//					equipmentManagerMgr.deleteStockTransFormDtl(lstDelete);
				//				}
				List<EquipStockTransFormDtl> equipmentStockTransFormDtl = new ArrayList<EquipStockTransFormDtl>();
				List<Long> addEquipmentIds = new ArrayList<Long>();
				if ((lstEquipCode != null && !lstEquipCode.isEmpty()) || (lstId != null && !lstId.isEmpty() && lstSeriNumber != null && !lstSeriNumber.isEmpty())) {
					if (lstId != null && !lstId.isEmpty()) {
						for (int i = 0; i < lstId.size(); i++) {
							EquipStockTransFormDtl equipmentStockTransFDtl = new EquipStockTransFormDtl();
							Equipment equipment = null;
							if (lstId != null) {
								if (lstId.get(i) != -1) {
									equipment = equipmentManagerMgr.getEquipmentById(lstId.get(i));
								} else if (!StringUtil.isNullOrEmpty(lstEquipCode.get(i))) {
									equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
								}
							}
							if (equipment != null) {
								if (!equipmentManagerMgr.checkExistStockTranFormDtlByEquip(id, equipment.getId())) {
									if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) { //trade_status = 0 thi moi cho lap bien ban
										errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.psc2.ex", lstEquipCode.get(i));
									} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
										errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.o.kho.ex", lstEquipCode.get(i));
									}
								}
								addEquipmentIds.add(equipment.getId());
								equipmentStockTransFDtl.setEquip(equipment);
								equipmentStockTransFDtl.setHealthStatus(equipment.getHealthStatus());
							} else {
								errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.o.kho.ex1", lstEquipCode.get(i));
							}

							if (!StringUtil.isNullOrEmpty(errMsg)) {
								result.put(ERROR, true);
								result.put("errMsg", errMsg);
								return JSON;
							}
							
//							if (equipment != null) {
//								addEquipmentIds.add(equipment.getId());
//								equipmentStockTransFDtl.setEquip(equipment);
//								equipmentStockTransFDtl.setHealthStatus(equipment.getHealthStatus());
//							} else {
//								result.put(ERROR, true);
//								result.put("errMsg", "Không tìm thấy thiết bị có mã " + lstEquipCode.get(i));
//								return JSON;
//							}

							if (lstSeriNumber != null && !lstSeriNumber.isEmpty()) {
								if (StringUtil.isNullOrEmpty(equipment.getSerial())) {
									if (ValidateUtil.validateFormatSerial(lstSeriNumber.get(i).trim())) {
										String[] spaceVld = lstSeriNumber.get(i).trim().split(" ");
										if (spaceVld.length > 1) {
											result.put(ERROR, true);
											result.put("errMsg", "Số seri không hợp lệ!");
											return JSON;
										}
										equipment.setSerial(lstSeriNumber.get(i));
										equipment.setFirstDateInUse(sysDateIns);
										equipmentManagerMgr.updateEquipment(equipment);
									} else {
										result.put(ERROR, true);
										result.put("errMsg", "Số seri không hợp lệ!");
										return JSON;
									}
								}
							}
							// kiem tra kho dich
							EquipStock equipStock = null;
							if (!StringUtil.isNullOrEmpty(lstToStockCode.get(i))) {
								equipStock = equipmentManagerMgr.getEquipStockbyCode(lstToStockCode.get(i));
							}

							if (equipStock != null) {
								if (!StatusType.ACTIVE.getValue().equals(equipStock.getStatus().getValue())) {
									errMsg += " Kho " + lstToStockCode.get(i) + " đang ở trạng thái không hoạt động";
								}
								equipmentStockTransFDtl.setToStockId(equipStock.getId());
								equipmentStockTransFDtl.setToStockName(equipStock.getName());
								equipmentStockTransFDtl.setToShopName(equipStock.getShop().getShopName());
							} else {
								result.put(ERROR, true);
								result.put("errMsg", "Kho nhận nhập vào không đúng");
								return JSON;
							}
							if (StringUtil.isNullOrEmpty(errMsg)) {
								if(equipment.getStockId().equals(equipStock.getId())){
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("equipment.row.invalid.makh.not.in.kho.manpp.err3"));
									return JSON;
								}
							}else {
								result.put(ERROR, true);
								result.put("errMsg", errMsg);
								return JSON;
							}
							equipmentStockTransFDtl.setEquipStockTransForm(equipStockTransForm);
							equipmentStockTransFDtl.setUpdateDate(sysDateIns);
							equipmentStockTransFDtl.setUpdateUser(currentUser.getUserName());
							equipmentStockTransFDtl.setCapacityFrom(equipment.getEquipGroup().getCapacityFrom());
							equipmentStockTransFDtl.setCapacityTo(equipment.getEquipGroup().getCapacityTo());
							equipmentStockTransFDtl.setEquipGroup(equipment.getEquipGroup());
							equipmentStockTransFDtl.setEquipGroupName(equipment.getEquipGroup().getName());
							equipmentStockTransFDtl.setPerformStatus(perFormStatus);
							if (equipment.getStockId() != null) {
								equipStock = equipmentManagerMgr.getEquipStockById(equipment.getStockId());
								if (equipStock != null) {
									equipmentStockTransFDtl.setFromStockId(equipment.getStockId());
									equipmentStockTransFDtl.setFromStockName(equipStock.getName());
									equipmentStockTransFDtl.setFromShopName(equipStock.getShop().getShopName());
								}
							}
							equipmentStockTransFormDtl.add(equipmentStockTransFDtl);
						}

						EquipRecordFilter<EquipStockTransForm> filter = new EquipRecordFilter<EquipStockTransForm>();
						ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
						if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0) {
							BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
							// filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
							filterF.setLstId(lstEquipAttachFileId);
							filterF.setObjectId(id);
							filterF.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
							objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
						}
						// Luu file dinh kem
						filter.setLstEquipAttachFileId(lstEquipAttachFileId);
						filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));

						equipStockTransForm = equipmentManagerMgr.updateStockTransForm(equipStockTransForm, equipmentStockTransFormDtl, addEquipmentIds, filter, getLogInfoVO());

						if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0 && objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
							// Xu ly xoa cung file tren storeFileOnDisk
							List<String> lstFileName = new ArrayList<String>();
							for (EquipAttachFile itemFile : objVO.getLstObject()) {
								if (!StringUtil.isNullOrEmpty(itemFile.getUrl())) {
									lstFileName.add(itemFile.getUrl());
								}
							}
							FileUtility.deleteStoreFileOnDisk(lstFileName, Configuration.getFileEquipServerDocPath());
						}

					} else if (lstEquipCode != null && !lstEquipCode.isEmpty()) {
						for (int i = 0; i < lstEquipCode.size(); i++) {
							Equipment equipment = null;
							equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
							if (equipment != null) {
								addEquipmentIds.add(equipment.getId());
							} else {
								result.put(ERROR, true);
								result.put("errMsg", "Không tìm thấy thiết bị có mã " + lstEquipCode.get(i));
								return JSON;
							}
							EquipRecordFilter<EquipStockTransForm> filter = new EquipRecordFilter<EquipStockTransForm>();
							ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
							if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0) {
								BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
								// filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
								filterF.setLstId(lstEquipAttachFileId);
								filterF.setObjectId(id);
								filterF.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
								objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
							}
							// Luu file dinh kem
							filter.setLstEquipAttachFileId(lstEquipAttachFileId);
							filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));

							equipStockTransForm = equipmentManagerMgr.updateStockTransForm(equipStockTransForm, equipmentStockTransFormDtl, addEquipmentIds, filter, getLogInfoVO());
						}
					}
				} else {
					EquipRecordFilter<EquipStockTransForm> filter = new EquipRecordFilter<EquipStockTransForm>();
					ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
					if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0) {
						BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
						// filterF.setLstEquipAttachFileId(lstEquipAttachFileId);
						filterF.setLstId(lstEquipAttachFileId);
						filterF.setObjectId(id);
						filterF.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
						objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
					}
					// Luu file dinh kem
					filter.setLstEquipAttachFileId(lstEquipAttachFileId);
					filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));

					equipStockTransForm = equipmentManagerMgr.updateEquipmentStockTransForm(equipStockTransForm, addEquipmentIds, filter, getLogInfoVO());
				}

			} else {
				if(status == null || (!StatusRecordsEquip.DRAFT.getValue().equals(status) && !StatusRecordsEquip.APPROVED.getValue().equals(status))){
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("action.status.err"));
					return JSON;
				}
				// TODO Xu ly cho them moi
				EquipStockTransForm equipStockTransForm = new EquipStockTransForm();
				equipStockTransForm.setStatus(status);
				equipStockTransForm.setPerformStatus(perFormStatus);
				equipStockTransForm.setCreateDate(sysDateIns);
				equipStockTransForm.setCreateUser(currentUser.getUserName());
//				equipStockTransForm.setEquipPeriod(equipPeriod);
				equipStockTransForm.setCreateFormDate(cDate);
				equipStockTransForm.setNote(note);
				// Luu grid thiet bi
				List<EquipStockTransFormDtl> equipmentStockTransFormDtl = new ArrayList<EquipStockTransFormDtl>();
				if (lstEquipCode != null && !lstEquipCode.isEmpty()) {
					for (int i = 0; i < lstEquipCode.size(); i++) {
						EquipStockTransFormDtl equipmentStockTransFDtl = new EquipStockTransFormDtl();
						Equipment equipment = null;
						if (lstId.get(i) != -1 && lstId != null) {
							equipment = equipmentManagerMgr.getEquipmentById(lstId.get(i));
						} else if (!StringUtil.isNullOrEmpty(lstEquipCode.get(i))) {
							equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
						}
						if (equipment != null) {
							if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + " đang ở trạng thái không hoạt động";
							} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.usage.status") + "\n";
							} else if (equipment.getTradeStatus().equals(EquipTradeStatus.TRADING.getValue())) {
								errMsg += " Thiết bị mã " + lstEquipCode.get(i) + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.trade.status") + "\n";
							}
							if (lstSeriNumber != null && !lstSeriNumber.isEmpty()) {
								if (StringUtil.isNullOrEmpty(equipment.getSerial())) {
									if (ValidateUtil.validateFormatSerial(lstSeriNumber.get(i).trim())) {
										String[] spaceVld = lstSeriNumber.get(i).trim().split(" ");
										if (spaceVld.length > 1) {
											result.put(ERROR, true);
											result.put("errMsg", "Số seri không hợp lệ!");
											return JSON;
										}
										equipment.setSerial(lstSeriNumber.get(i));
										equipment.setFirstDateInUse(sysDateIns);
										equipmentManagerMgr.updateEquipment(equipment);
									} else {
										result.put(ERROR, true);
										result.put("errMsg", "Số seri không hợp lệ!");
										return JSON;
									}
								}
							}
							// kiem tra kho dich
							EquipStock equipStock = null;
							if (!StringUtil.isNullOrEmpty(lstToStockCode.get(i))) {
								equipStock = equipmentManagerMgr.getEquipStockbyCode(lstToStockCode.get(i));
							}

							if (equipStock != null) {
								if (!StatusType.ACTIVE.getValue().equals(equipStock.getStatus().getValue())) {
									errMsg += " Kho " + lstToStockCode.get(i) + " đang ở trạng thái không hoạt động";
								}
								equipmentStockTransFDtl.setToStockId(equipStock.getId());
								equipmentStockTransFDtl.setToStockName(equipStock.getName());
								equipmentStockTransFDtl.setToShopName(equipStock.getShop().getShopName());
							} else {
								result.put(ERROR, true);
								result.put("errMsg", "Kho nhận nhập vào không đúng");
								return JSON;
							}

							if (errMsg.length() > 0) {
								result.put(ERROR, true);
								result.put("errMsg", errMsg);
								return JSON;
							}
							equipmentStockTransFDtl.setEquip(equipment);
							equipmentStockTransFDtl.setSerial(equipment.getSerial());
							equipmentStockTransFDtl.setHealthStatus(equipment.getHealthStatus());
							equipmentStockTransFDtl.setCapacityFrom(equipment.getEquipGroup().getCapacityFrom());
							equipmentStockTransFDtl.setCapacityTo(equipment.getEquipGroup().getCapacityTo());
							equipmentStockTransFDtl.setEquipGroup(equipment.getEquipGroup());
							equipmentStockTransFDtl.setEquipGroupName(equipment.getEquipGroup().getName());
							if (equipment.getStockId() != null) {
								equipStock = equipmentManagerMgr.getEquipStockById(equipment.getStockId());
								if (equipStock != null) {
									equipmentStockTransFDtl.setFromStockId(equipment.getStockId());
									equipmentStockTransFDtl.setFromStockName(equipStock.getName());
									equipmentStockTransFDtl.setFromShopName(equipStock.getShop().getShopName());
								}
							}

						} else {
							result.put(ERROR, true);
							result.put("errMsg", "Không tìm thấy thiết bị có mã " + lstEquipCode.get(i));
							return JSON;
						}
						equipmentStockTransFDtl.setEquipStockTransForm(equipStockTransForm);
						equipmentStockTransFDtl.setCreateDate(sysDateIns);
						equipmentStockTransFDtl.setCreateUser(currentUser.getUserName());
						equipmentStockTransFDtl.setPerformStatus(perFormStatus);
						equipmentStockTransFormDtl.add(equipmentStockTransFDtl);
					}
					EquipRecordFilter<EquipLostRecord> filter = new EquipRecordFilter<EquipLostRecord>();
					filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));
					equipStockTransForm = equipmentManagerMgr.createStockTransFormByImportExcel(equipStockTransForm, equipmentStockTransFormDtl, filter, getLogInfoVO());
				} else {
					result.put(ERROR, true);
					result.put("errMsg", "Bạn chưa chọn thiết bị để chuyển kho!");
					return JSON;
				}
			}
		} catch (Exception e) {
			error = true;
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentStockAction.createRecordStockChange"), createLogErrorStandard(startLogDate));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Upload File
	 * 
	 * @author hoanv25
	 * @since January 10/2/2015
	 * */
	private List<FileVO> saveUploadFileByEquip(List<File> lstFile, List<String> lstFileFileName, List<String> lstFileContentType) {
		try {
			if (lstFile != null && lstFile.size() > 0) {
				// String perfix =
				// Configuration.getPrefixFileEquipServerDocPath();
				List<FileVO> lstFileVO = FileUtility.storeFileOnDisk(lstFile, lstFileFileName, lstFileContentType, null, EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				if (lstFileVO != null && !lstFileVO.isEmpty()) {
					String thumbUrl = Configuration.getEquipThumbUrlAttachFile();
					for (FileVO voFile : lstFileVO) {
						voFile.setThumbUrl(thumbUrl);
					}
				}
				return lstFileVO;
			}
			System.gc();
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.saveUploadFileByEquip()" + e.getMessage());
		}
		return new ArrayList<FileVO>();
	}

	/**
	 * In bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 14/01/2015
	 * @throws BusinessException
	 */

	public String printRecodeStockTran() throws Exception {
		try {
			// Shop shop = null;
			if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null) {
				List<Rpt_TB_QLCK> lstData = new ArrayList<Rpt_TB_QLCK>();
				HashMap<String, Object> parametersReport = new HashMap<String, Object>();
				parametersReport.put("tenKenh", "");
				parametersReport.put("tenMien", "");
				if (lstPrintRecodeStockTran != null && !lstPrintRecodeStockTran.isEmpty()) {
					EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
					EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
					for (int i = 0; i < lstPrintRecodeStockTran.size(); i++) {
						Rpt_TB_QLCK vo = new Rpt_TB_QLCK();
						EquipStockTransForm equipStockTransForm = equipmentManagerMgr.getEquipStockTransFormById(lstPrintRecodeStockTran.get(i));
						if (equipStockTransForm != null) {
							/*
							 * if(equipStockTransForm.getFromStockId() != null){
							 * Shop shop =
							 * shopMgr.getShopById(equipStockTransForm
							 * .getFromStockId()); if(shop != null){
							 * vo.setShopFromName(shop.getShopName());
							 * vo.setShopFromAddress(shop.getAddress());
							 * vo.setShopFromPhone(shop.getPhone());
							 * vo.setShopFromMobiPhone(shop.getMobiphone());
							 * vo.setShopFax(shop.getFax()); } }
							 * if(equipStockTransForm.getToStockId() != null){
							 * Shop shop =
							 * shopMgr.getShopById(equipStockTransForm
							 * .getToStockId()); if(shop != null){
							 * vo.setShopToName(shop.getShopName());
							 * vo.setShopToAddress(shop.getAddress());
							 * vo.setShopToPhone(shop.getPhone());
							 * vo.setShopToMobiPhone(shop.getMobiphone()); } }
							 */
							filter.setId(equipStockTransForm.getId());
							ObjectVO<EquipmentVO> equipmentStockTransFDtl = null;
							equipmentStockTransFDtl = equipmentManagerMgr.getEquipStockTransFormDtlByFilter(filter);
							Equipment equipment = null;
							List<EquipmentDeliveryVO> lstDetailEquipment = new ArrayList<EquipmentDeliveryVO>();
							for (int j = 0; j < equipmentStockTransFDtl.getLstObject().size(); j++) {
								equipment = equipmentManagerMgr.getEquipmentByEquipId(equipmentStockTransFDtl.getLstObject().get(j).getId());
								if (equipment != null) {
									equipmentFilter.setEquipId(equipment.getId());
									ObjectVO<EquipmentDeliveryVO> lstGroupEquipment = null;
									lstGroupEquipment = equipmentManagerMgr.searchRecordStockChangeByEquipment(equipmentFilter);
									if (lstGroupEquipment != null && lstGroupEquipment.getLstObject() != null && !lstGroupEquipment.getLstObject().isEmpty()) {
										lstDetailEquipment.add(lstGroupEquipment.getLstObject().get(0));
									}
								}
							}
							vo.setListDetail(lstDetailEquipment);
						}
						lstData.add(vo);
					}
				}
				if (lstData.size() > 0) {
					JRDataSource dataSource = new JRBeanCollectionDataSource(lstData);
					downloadPath = ReportUtils.exportFromFormat(FileExtension.PDF, parametersReport, dataSource, ShopReportTemplate.EQUIPMENT_STOCK_TRANS_REPORT);
					// downloadPath = outputPath;
					session.setAttribute("downloadPath", downloadPath);
					result.put(ERROR, false);
					return JSON;
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		System.gc();
		return JSON;
	}

	/**
	 * Mo trang applet
	 * 
	 * @author hoanv25
	 * @since 15/01/2015
	 */
	public String openTaxView() {
		downloadPath = (String) session.getAttribute("downloadPath");
		session.removeAttribute("downloadPath");
		return SUCCESS;
	}

	/**
	 * get list equip stock vo by filter
	 * 
	 * @author phuongvm
	 * @since 30/03/2015
	 * @description Chuc nang lay danh sach kho F9
	 * */
	public String getListEquipStockVOByFilter() {
		result.put("rows", new ArrayList<EquipStockVO>());
		result.put("total", 0);
		try {
			EquipStockFilter filter = new EquipStockFilter();
			// set cac tham so tim kiem
			KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			// goi ham thuc thi tim kiem
			if (currentUser.getShopRoot() != null) {
				filter.setShopRoot(currentUser.getShopRoot().getShopId());
			}
			if (currentUser.getStaffRoot() != null) {
				filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setShopCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setShopName(name);
			}
			ObjectVO<EquipStockVO> objVO = equipmentManagerMgr.getListEquipStockVOByFilterForStockTrans(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentStockAction.getListEquipStockVOByFilter()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * import bien ban chuyen kho
	 * 
	 * @author phuongvm
	 * @since 06/04/2015
	 * @return
	 */
	public String importExcel() throws Exception {
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		boolean isNewRecord = true;
		boolean isValid = true;
		Date now = commonMgr.getSysDate();
		String username = getCurrentUser().getUserName();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<CellBean> lstFailsInForm = new ArrayList<CellBean>();
		boolean isValidForm = true; //bien ban hop le (bao gom tat ca cac dong chi tiet)
		List<EquipStockTransFormDtl> lstEquipStockTransFormDtl = new ArrayList<EquipStockTransFormDtl>();
		EquipStockTransForm equipStockTransForm = null;
		List<Equipment> lstEquipment = new ArrayList<Equipment>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 4);
		Long shopRoot = null;
		Long staffRoot = null;
		if (currentUser.getShopRoot() != null) {
			shopRoot = currentUser.getShopRoot().getShopId();
		}
		if (currentUser.getStaffRoot() != null) {
			staffRoot = currentUser.getStaffRoot().getStaffId();
		}
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
//				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
//					isError = true;
//					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//					return SUCCESS;
//				}
//				EquipPeriod equipPeriod = null;
				EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
				equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
				equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
				if (currentUser.getStaffRoot() != null) {
					equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
				}
				if (currentUser.getShopRoot() != null) {
					equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
				}

				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						totalItem++;
						List<String> row = lstData.get(i);
						List<String> rowNext = null;
						if (i != lstData.size() - 1) {
							rowNext = lstData.get(i + 1);
						}
						if (row != null && isEmtyRow(row)) {
							isValid = false;
						}
						if (isValid) {
							if (i == 0 || isANewRecord(row)) { // check xem co phai la bien ban mới
								isNewRecord = true;
								if (rowNext != null && isEmtyRow(rowNext)) {
									isValid = false;
								}
								//								lstFailsInForm.add(StringUtil.addFailBean(row, ""));
								if (isValidForm && equipStockTransForm != null && equipStockTransForm.getStatus() != null && lstEquipStockTransFormDtl != null && lstEquipStockTransFormDtl.size() > 0) { // them moi phieu chuyen kho
									//									equipmentManagerMgr.createEquipRepairFormByExcel(equipRepairForm, lstEquipRepairFormDtl);
									equipStockTransForm = equipmentManagerMgr.createStockTransFormByImportExcel(equipStockTransForm, lstEquipStockTransFormDtl, null, getLogInfoVO());
									lstFailsInForm = new ArrayList<CellBean>();
								} else {
									lstFails.addAll(lstFailsInForm);
									lstFailsInForm = new ArrayList<CellBean>();
									isValidForm = true;
								}
								lstEquipment = new ArrayList<Equipment>();
								lstEquipStockTransFormDtl = new ArrayList<EquipStockTransFormDtl>();
								equipStockTransForm = new EquipStockTransForm();
								//								continue;
							} else {
								if (rowNext != null && isEmtyRow(rowNext)) {
									isValid = false;
								}
								isNewRecord = false;
							}
						} else {
							if (isValidForm && equipStockTransForm != null && equipStockTransForm.getStatus() != null && lstEquipStockTransFormDtl != null && lstEquipStockTransFormDtl.size() > 0) { // them moi phieu chuyen kho
								//								equipmentManagerMgr.createEquipRepairFormByExcel(equipRepairForm, lstEquipRepairFormDtl);
								equipStockTransForm = equipmentManagerMgr.createStockTransFormByImportExcel(equipStockTransForm, lstEquipStockTransFormDtl, null, getLogInfoVO());
								lstFailsInForm = new ArrayList<CellBean>();
							} else {
								lstFails.addAll(lstFailsInForm);
								lstFailsInForm = new ArrayList<CellBean>();
								isValidForm = true;
							}
							lstEquipment = new ArrayList<Equipment>();
							lstEquipStockTransFormDtl = new ArrayList<EquipStockTransFormDtl>();
							equipStockTransForm = new EquipStockTransForm();
							lstFailsInForm.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
							continue;
						}
						StatusRecordsEquip statusRecord = null;
						PerFormStatus perFormStatus = null;
						EquipStock equipStock = null;
						EquipStockTransFormDtl equipStockTransFormDtl = null;
						Equipment equipment = null;
						Date ngayLap = null;
						String noteStr = null;
//						equipPeriod = null;
						message = "";
						if (isNewRecord) { //Neu la bien ban moi 
							// ** Mã thiết bị */
							if (row.size() > 0) {
								String value = row.get(0);
								String msg = ValidateUtil.validateField(value, "device.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + "\n";
								} else {
									value = value.trim().toUpperCase();
									equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
									if (equipment != null) {
										equipmentFilter.setCode(equipment.getCode());
										ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentStockVOByFilter(equipmentFilter);
										if (equipmentDeliverys == null || equipmentDeliverys.getLstObject().isEmpty()) {
											equipment = null;
										}
									}

									if (equipment == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code");
									} else if (!ActiveType.RUNNING.getValue().equals(equipment.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code");
									} else if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) { //trade_status = 0 thi moi cho lap bien ban
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.psc2");
									} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.o.kho");
									}
								}
							}
							// ** Kho */
							if (row.size() > 1) {
								String value = row.get(1);
								String msg = ValidateUtil.validateField(value, "device.code.stock", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + "\n";
								} else {
									value = value.trim().toUpperCase();
									equipStock = equipmentManagerMgr.getEquipStockbyCode(value);
									if (equipStock == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code.stock");
									} else if (!ActiveType.RUNNING.getValue().equals(equipStock.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code.stock");
									}
								}
							}

							// ** Trang thai bien ban*/
							statusRecord = StatusRecordsEquip.DRAFT;
							perFormStatus = PerFormStatus.ONGOING;
//							if (row.size() > 2) {
//								String value = row.get(2).trim().toUpperCase();
//								String msg = ValidateUtil.validateField(value, "report.status.label", 100, ConstantManager.ERR_REQUIRE);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + "\n";
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duthao").toUpperCase())) {
//									statusRecord = StatusRecordsEquip.DRAFT;
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.duyet").toUpperCase())) {
//									statusRecord = StatusRecordsEquip.APPROVED;
//								} else {
//									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.phieu.sua.chua.trangthai.ex");
//								}
//							}
//
//							// ** Trang thai thực hiện*/
//							if (row.size() > 3) {
//								String value = row.get(3).trim().toUpperCase();
//								String msg = ValidateUtil.validateField(value, "action.status.perform", 100, ConstantManager.ERR_REQUIRE);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg + "\n";
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.perform.ongoing").toUpperCase())) {
//									perFormStatus = PerFormStatus.ONGOING;
//								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.perform.success").toUpperCase())) {
//									perFormStatus = PerFormStatus.SUCCESS;
//								} else {
//									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.phieu.sua.chua.trangthai.ex.2");
//								}
//							}
							if (StringUtil.isNullOrEmpty(message)) {
								if (StatusRecordsEquip.DRAFT.equals(statusRecord) && PerFormStatus.SUCCESS.equals(perFormStatus)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err4");
								}
							}
							// ** Ngay lap */
							if (row.size() > 2) {
								String value = row.get(2);
								String msg = ValidateUtil.validateField(value, "equipment.create.form.date", 50, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date",null);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										ngayLap = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
										if (ngayLap == null) {
											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
										} else {
											if (DateUtil.compareTwoDate(ngayLap, now) == 1) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.big") + "\n";
											} 
//											else {
//												//tamvnm: update dac ta moi. 30/06/2015
//												List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(ngayLap);
//												if (lstPeriod == null || lstPeriod.size() == 0) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//												} else if (lstPeriod.size() > 0) {
//													// gan gia tri ky dau tien.
//													equipPeriod = lstPeriod.get(0);
//												}
//											}
										}
									}
								}							
							}
							// ** Ghi chu */
							if (row.size() > 3) {
								String value = row.get(3);
								String msg = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									noteStr = value;
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								if (equipment.getStockId().equals(equipStock.getId())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err3");
								}
							}
							lstFailsInForm.add(StringUtil.addFailBean(row, message));
						} else {//nguoc lai no la tung dong chi tiet cua bien ban
							if (row.size() > 0) {
								String value = row.get(0);
								String msg = ValidateUtil.validateField(value, "device.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + "\n";
								} else {
									value = value.trim().toUpperCase();
									equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
									if (equipment != null) {
										equipmentFilter.setCode(equipment.getCode());
										ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentStockVOByFilter(equipmentFilter);
										if (equipmentDeliverys == null || equipmentDeliverys.getLstObject().isEmpty()) {
											equipment = null;
										}
									}

									if (equipment == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code");
									} else if (!ActiveType.RUNNING.getValue().equals(equipment.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code");
									} else if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) { //trade_status = 0 thi moi cho lap bien ban
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2.psc");
									} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.o.kho");
									} else {
										for (int k = 0; k < lstEquipment.size(); k++) {
											if (equipment.getCode().equals(lstEquipment.get(k).getCode())) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err1");
												break;
											}
										}
									}
								}
							}
							// ** Kho */
							if (row.size() > 1) {
								String value = row.get(1);
								String msg = ValidateUtil.validateField(value, "device.code.stock", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg + "\n";
								} else {
									value = value.trim().toUpperCase();
									equipStock = equipmentManagerMgr.getEquipStockbyCode(value);
									if (equipStock == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "device.code.stock");
									} else if (!ActiveType.RUNNING.getValue().equals(equipStock.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "device.code.stock");
									}
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								if (equipment.getStockId().equals(equipStock.getId())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err3");
								}
							}
//							lstFailsInForm.add(StringUtil.addFailBean(row, message));
//							if (!StringUtil.isNullOrEmpty(message)) {
//								isValidForm = false;
//							}
						}
						if (StringUtil.isNullOrEmpty(message)) {
							if (isNewRecord) { //la bien ban moi
								lstEquipment = new ArrayList<Equipment>();
								lstEquipStockTransFormDtl = new ArrayList<EquipStockTransFormDtl>();
								equipStockTransForm = new EquipStockTransForm();
//								equipStockTransForm.setEquipPeriod(equipPeriod);
								equipStockTransForm.setCreateFormDate(ngayLap);
								equipStockTransForm.setNote(noteStr);
								if (statusRecord.equals(StatusRecordsEquip.APPROVED)) {
									equipStockTransForm.setApproveDate(now);
								}
								equipStockTransForm.setPerformStatus(perFormStatus);
								equipStockTransForm.setStatus(statusRecord.getValue());
								equipStockTransForm.setCreateDate(now);
								equipStockTransForm.setCreateUser(username);
							}
							equipStockTransFormDtl = new EquipStockTransFormDtl();
							equipStockTransFormDtl.setCreateDate(now);
							equipStockTransFormDtl.setCreateUser(username);
							equipStockTransFormDtl.setEquip(equipment);
							if (equipment.getStockId() != null) {
								EquipStock equipStockFrom = equipmentManagerMgr.getEquipStockById(equipment.getStockId());
								if (equipStockFrom != null) {
									equipStockTransFormDtl.setFromStockId(equipStockFrom.getId());
									equipStockTransFormDtl.setFromStockName(equipStockFrom.getName());
									equipStockTransFormDtl.setFromShopName(equipStockFrom.getShop().getShopName());
								}
							}
							equipStockTransFormDtl.setEquipGroup(equipment.getEquipGroup());
							equipStockTransFormDtl.setEquipGroupName(equipment.getEquipGroup().getName());
							equipStockTransFormDtl.setCapacityFrom(equipment.getEquipGroup().getCapacityFrom());
							equipStockTransFormDtl.setCapacityTo(equipment.getEquipGroup().getCapacityTo());
							equipStockTransFormDtl.setHealthStatus(equipment.getHealthStatus());
							equipStockTransFormDtl.setPerformStatus(equipStockTransForm.getPerformStatus());
							equipStockTransFormDtl.setSerial(equipment.getSerial());
							equipStockTransFormDtl.setToStockId(equipStock.getId());
							equipStockTransFormDtl.setToShopName(equipStock.getShop().getShopName());
							equipStockTransFormDtl.setToStockName(equipStock.getName());
							lstEquipment.add(equipment);
							lstEquipStockTransFormDtl.add(equipStockTransFormDtl);
						} else {
							isValidForm = false;
						}
					}
				}
				if (isValidForm && equipStockTransForm != null && equipStockTransForm.getStatus() != null && lstEquipStockTransFormDtl != null && lstEquipStockTransFormDtl.size() > 0) { // them moi phieu sua chua
					//					equipmentManagerMgr.createEquipRepairFormByExcel(equipRepairForm, lstEquipRepairFormDtl);
					equipStockTransForm = equipmentManagerMgr.createStockTransFormByImportExcel(equipStockTransForm, lstEquipStockTransFormDtl, null, getLogInfoVO());
					lstFailsInForm = new ArrayList<CellBean>();
				} else {
					lstFails.addAll(lstFailsInForm);
					lstFailsInForm = new ArrayList<CellBean>();
					isValidForm = true;
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_STOCK_TRAN_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	public boolean isEmtyRow(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3))
				|| !StringUtil.isNullOrEmpty(row.get(4)) || !StringUtil.isNullOrEmpty(row.get(5))) {
			return false;
		}
		return true;
	}

	public boolean isANewRecord(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4)) || !StringUtil.isNullOrEmpty(row.get(5))) {
			return true;
		}
		return false;
	}

	public EquipmentVO getEquipmentStockVO() {
		return equipmentStockVO;
	}

	public void setEquipmentStockVO(EquipmentVO equipmentStockVO) {
		this.equipmentStockVO = equipmentStockVO;
	}

	public EquipmentVO getEquipmentShopVO() {
		return equipmentShopVO;
	}

	public void setEquipmentShopVO(EquipmentVO equipmentShopVO) {
		this.equipmentShopVO = equipmentShopVO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(Integer statusStr) {
		this.statusStr = statusStr;
	}

	public Integer getStatusRecord() {
		return statusRecord;
	}

	public Integer getPerfromStatusRecord() {
		return perfromStatusRecord;
	}

	public void setPerfromStatusRecord(Integer perfromStatusRecord) {
		this.perfromStatusRecord = perfromStatusRecord;
	}

	public void setStatusRecord(Integer statusRecord) {
		this.statusRecord = statusRecord;
	}

	public Integer getStatusStock() {
		return statusStock;
	}

	public void setStatusStock(Integer statusStock) {
		this.statusStock = statusStock;
	}

	public List<EquipmentVO> getListEquipBrand() {
		return listEquipBrand;
	}

	public void setListEquipBrand(List<EquipmentVO> listEquipBrand) {
		this.listEquipBrand = listEquipBrand;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public List<EquipmentVO> getListEquipCategory() {
		return listEquipCategory;
	}

	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}

	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}

	public void setListEquipCategory(List<EquipmentVO> listEquipCategory) {
		this.listEquipCategory = listEquipCategory;
	}

	public List<EquipmentVO> getListEquipProvider() {
		return listEquipProvider;
	}

	public void setListEquipProvider(List<EquipmentVO> listEquipProvider) {
		this.listEquipProvider = listEquipProvider;
	}

	public List<EquipmentVO> getListChangeEquipGroupManager() {
		return listChangeEquipGroupManager;
	}

	public void setListChangeEquipGroupManager(List<EquipmentVO> listChangeEquipGroupManager) {
		this.listChangeEquipGroupManager = listChangeEquipGroupManager;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public Integer getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(Integer fromMonth) {
		this.fromMonth = fromMonth;
	}

	public Integer getToMonth() {
		return toMonth;
	}

	public void setToMonth(Integer toMonth) {
		this.toMonth = toMonth;
	}

	public String getFromStock() {
		return fromStock;
	}

	public void setFromStock(String fromStock) {
		this.fromStock = fromStock;
	}

	public String getToStock() {
		return toStock;
	}

	public void setToStock(String toStock) {
		this.toStock = toStock;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getLstEquipDelete() {
		return lstEquipDelete;
	}

	public void setLstEquipDelete(String lstEquipDelete) {
		this.lstEquipDelete = lstEquipDelete;
	}

	public String getLstEquipAdd() {
		return lstEquipAdd;
	}

	public void setLstEquipAdd(String lstEquipAdd) {
		this.lstEquipAdd = lstEquipAdd;
	}

	public List<EquipmentDeliveryVO> getEquipments() {
		return equipments;
	}

	public void setEquipments(List<EquipmentDeliveryVO> equipments) {
		this.equipments = equipments;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<Long> getLstIdEquip() {
		return lstIdEquip;
	}

	public void setLstIdEquip(List<Long> lstIdEquip) {
		this.lstIdEquip = lstIdEquip;
	}

	public List<String> getLstEquipCode() {
		return lstEquipCode;
	}

	public void setLstEquipCode(List<String> lstEquipCode) {
		this.lstEquipCode = lstEquipCode;
	}

	public List<String> getLstSeriNumber() {
		return lstSeriNumber;
	}

	public void setLstSeriNumber(List<String> lstSeriNumber) {
		this.lstSeriNumber = lstSeriNumber;
	}

	public String getLstDell() {
		return lstDell;
	}

	public void setLstDell(String lstDell) {
		this.lstDell = lstDell;
	}

	public String getLstEquipId() {
		return lstEquipId;
	}

	public void setLstEquipId(String lstEquipId) {
		this.lstEquipId = lstEquipId;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public List<Long> getLstPrintRecodeStockTran() {
		return lstPrintRecodeStockTran;
	}

	public void setLstPrintRecodeStockTran(List<Long> lstPrintRecodeStockTran) {
		this.lstPrintRecodeStockTran = lstPrintRecodeStockTran;
	}

	public Integer getWindowWidth() {
		return windowWidth;
	}

	public void setWindowWidth(Integer windowWidth) {
		this.windowWidth = windowWidth;
	}

	public Integer getWindowHeight() {
		return windowHeight;
	}

	public void setWindowHeight(Integer windowHeight) {
		this.windowHeight = windowHeight;
	}

	public Long getFromStockId() {
		return fromStockId;
	}

	public void setFromStockId(Long fromStockId) {
		this.fromStockId = fromStockId;
	}

	public List<String> getDeletedEquipmentCodes() {
		return deletedEquipmentCodes;
	}

	public void setDeletedEquipmentCodes(List<String> deletedEquipmentCodes) {
		this.deletedEquipmentCodes = deletedEquipmentCodes;
	}

	public List<File> getLstFile() {
		return lstFile;
	}

	public List<String> getLstFileContentType() {
		return lstFileContentType;
	}

	public List<String> getLstFileFileName() {
		return lstFileFileName;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFile(List<File> lstFile) {
		this.lstFile = lstFile;
	}

	public void setLstFileContentType(List<String> lstFileContentType) {
		this.lstFileContentType = lstFileContentType;
	}

	public void setLstFileFileName(List<String> lstFileFileName) {
		this.lstFileFileName = lstFileFileName;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public String getEquipAttachFileStr() {
		return equipAttachFileStr;
	}

	public void setEquipAttachFileStr(String equipAttachFileStr) {
		this.equipAttachFileStr = equipAttachFileStr;
	}

	public Integer getStatusPerform() {
		return statusPerform;
	}

	public void setStatusPerform(Integer statusPerform) {
		this.statusPerform = statusPerform;
	}

	public List<String> getLstToStockCode() {
		return lstToStockCode;
	}

	public void setLstToStockCode(List<String> lstToStockCode) {
		this.lstToStockCode = lstToStockCode;
	}

	public List<Integer> getLstPerformStatus() {
		return lstPerformStatus;
	}

	public void setLstPerformStatus(List<Integer> lstPerformStatus) {
		this.lstPerformStatus = lstPerformStatus;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCreateFromDate() {
		return createFromDate;
	}

	public void setCreateFromDate(String createFromDate) {
		this.createFromDate = createFromDate;
	}

	public String getCreateToDate() {
		return createToDate;
	}

	public void setCreateToDate(String createToDate) {
		this.createToDate = createToDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getStartLogDate() {
		return startLogDate;
	}

	public void setStartLogDate(Date startLogDate) {
		this.startLogDate = startLogDate;
	}
	
	

}

package ths.dms.web.action.equipment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;
import ths.dms.web.utils.report.excel.SXSSFReportHelper;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.AreaMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipProposalBorrowMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.Area;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopDecentralizationSTT;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StatusDeliveryEquip;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.TypeStockEquipLend;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.AreaVO;
import ths.dms.core.entities.vo.EquipBorrowVO;
import ths.dms.core.entities.vo.EquipLendDetailVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;
//import ths.dms.core.entities.enumtype.ShopDecentralizationSTT;
//import ths.dms.core.entities.enumtype.TypeStockEquipLend;

/**
 * Action Quan ly De nghi muon thiet bi
 * 
 * @author hoanv25
 * @since March 03,2015
 */
public class EquipmentManagementProposalBorrowAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Khai bao cac bien toan cuc
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	private EquipProposalBorrowMgr equipProposalBorrowMgr;
	private CustomerMgr customerMgr;
	private ShopMgr shopMgr;
	private AreaMgr areaMgr;
	private ApParamMgr apParamMgr;
	private StaffMgr staffMgr;

	private List<AreaVO> lstPrecinct;
	private List<EquipLendDetailVO> lstDetails;
	private List<StaffVO> lstStaffVOs;

	private List<Long> lstIdRecord;
	private List<Long> lstCustomerId;
		
	private Long id;
	private String curShopCode;
	private Integer statusDelivery;
	private Integer statusRecord;
	private String code;
	private String lstShop;
	private String superviseCode;
	private String staffCode;
	private String fromDate;
	private String toDate;
	private String nameFile;
	private String createFormDate;
	private String nganhHangStr;
	private Integer isLevelShop; 
	private String note;
	
	private File excelFile;
	private String excelFileContentType;

	private Long areaId;
	private Integer objectType;
	private Integer flagRecordStatus;
	
	/**
	 * Xu ly cac ham thuc thi ket noi
	 * 
	 * @author hoanv25
	 * @since March 02,2014
	 * */
	@Override
	public void prepare() throws Exception {
		equipProposalBorrowMgr = (EquipProposalBorrowMgr) context.getBean("equipProposalBorrowMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		areaMgr = (AreaMgr) context.getBean("areaMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		super.prepare();
	}

	/**
	 * Khai bao cac du lieu dung chung khong khai bao method
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		if (shop != null) {
			shopId = shop.getId();
			curShopCode = shop.getShopCode();
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem Danh sach de nghi muon tu
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since March 09,2015
	 * 
	 * @author update nhutnn
	 * @since update 02/07/2015
	 */
	public String search() {
		actionStartTime = DateUtil.now();
		result.put("rows", new ArrayList<EquipRecordVO>());
		result.put("total", 0);
		try {
			EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
			//set cac tham so tim kiem
			KPaging<EquipBorrowVO> kPaging = new KPaging<EquipBorrowVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			List<Long> lstShopId = new ArrayList<Long>();
			if (StringUtil.isNullOrEmpty(lstShop)) {
				lstShopId.add(currentUser.getShopRoot().getShopId());
			} else {
				String[] temp = lstShop.split(",");
				for (int i = 0, sz = temp.length; i < sz; i++) {
					if (!checkShopInOrgAccessByCode(temp[i].trim().toUpperCase())) {
						result.put("errMsg", R.getResource("common.shop.not.belong.user", temp[i].trim().toUpperCase()));
						result.put(ERROR, true);
						return JSON;
					} else {
						Shop shop = shopMgr.getShopByCode(temp[i].trim().toUpperCase());
						if (shop == null) {
							result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("common.unit.name.err") + " " + temp[i].trim().toUpperCase()));
							result.put(ERROR, true);
							return JSON;
						} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
							result.put("errMsg", R.getResource("common.catalog.shop.status.not.active", temp[i].trim().toUpperCase()));
							result.put(ERROR, true);
							return JSON;
						}
						lstShopId.add(shop.getId());
					}
				}
			}
			filter.setLstShopId(lstShopId);

			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}

			if (!StringUtil.isNullOrEmpty(superviseCode)) {
				filter.setSuperviseCode(superviseCode);
			}

			if (statusRecord != null) {
				if (flagRecordStatus == null) {
					if (StatusRecordsEquip.DRAFT.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.APPROVED.getValue().equals(statusRecord)
						|| StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.COMPLETION_REPAIRS.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord)) {
						filter.setStatus(statusRecord);
					} else {
						filter.setStatus(StatusRecordsEquip.DRAFT.getValue());
					}
				} else {
					if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.APPROVED.getValue().equals(statusRecord) 
						|| StatusRecordsEquip.COMPLETION_REPAIRS.getValue().equals(statusRecord)) {
						filter.setStatus(statusRecord);
					} else {
						filter.setStatus(StatusRecordsEquip.WAITING_APPROVAL.getValue());
					}
				}				
			} else {
				if (flagRecordStatus == null) {
					List<Integer> lstStatus = new ArrayList<Integer>();
					lstStatus.add(StatusRecordsEquip.DRAFT.getValue());
					lstStatus.add(StatusRecordsEquip.WAITING_APPROVAL.getValue());
					lstStatus.add(StatusRecordsEquip.APPROVED.getValue());
					lstStatus.add(StatusRecordsEquip.NO_APPROVAL.getValue());
					lstStatus.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
					lstStatus.add(StatusRecordsEquip.CANCELLATION.getValue());
					filter.setLstStatus(lstStatus);
				} else {
					List<Integer> lstStatus = new ArrayList<Integer>();
					lstStatus.add(StatusRecordsEquip.WAITING_APPROVAL.getValue());
					lstStatus.add(StatusRecordsEquip.APPROVED.getValue());
					lstStatus.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
					filter.setLstStatus(lstStatus);
				} 
			}

			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setToDate(tDate);
			}

			if (statusDelivery != null) {
				filter.setDeliveryStatus(statusDelivery);
			}

			//goi ham thuc thi tim kiem
			ObjectVO<EquipBorrowVO> objVO = equipProposalBorrowMgr.searchListProposalBorrowVOByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.search()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Xu ly Luu chuyen trang thai tren trang tim kiem
	 * 
	 * @author hoanv25
	 * @since March 10,2015
	 * 
	 * @author update nhutnn
	 * @since 03/07/2015
	 */
	public String saveStatusRecord() {
		actionStartTime = DateUtil.now();
		final int CHECK_ERROR = 1;
		resetToken(result);
		try {
			List<FormErrVO> lstError = new ArrayList<FormErrVO>();
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				for (int i = 0; i < lstIdRecord.size(); i++) {
					EquipLend equipLend = equipProposalBorrowMgr.getEquipLendById(lstIdRecord.get(i));
					FormErrVO formErrVO = new FormErrVO();
					if (equipLend != null && statusRecord != null && !statusRecord.equals(equipLend.getStatus())) {						
						if (equipLend.getStatus() != null
							&& !((StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus()))
							|| (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus()))
							|| (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipLend.getStatus())) 
							|| (StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipLend.getStatus()))
							|| (StatusRecordsEquip.APPROVED.getValue().equals(statusRecord)&& StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipLend.getStatus())) 
							|| (StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusRecord)&& StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipLend.getStatus())))) {
							formErrVO.setFormStatusErr(CHECK_ERROR);
						}
						if (formErrVO.getFormStatusErr() == null) {
							if (statusRecord != null && !statusRecord.equals(equipLend.getStatus())) {
								equipLend.setUpdateUser(currentUser.getUserName());
								equipLend.setStatus(statusRecord);
								equipProposalBorrowMgr.updateEquipLend(equipLend);
							}
						} else {
							formErrVO.setFormCode(equipLend.getCode());
							lstError.add(formErrVO);
						}
					}
				}
				if (lstError.size() > 0) {
					result.put("lstRecordError", lstError);
				}
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.saveStatusRecord()" + e.getMessage(), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * In bien ban de nghi muon tu
	 * 
	 * @author hoanv25
	 * @since March 10,2015
	 * @throws BusinessException
	 */
	public String exportEquipLend() {
		SXSSFWorkbook workbook = null;
		try {
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
				filter.setLstId(lstIdRecord);
				List<EquipBorrowVO> data = equipProposalBorrowMgr.getListEquipLendDetailVOForExportByFilter(filter);
				if (data == null || data.size() == 0) {
					result.put("errMsg", R.getResource("common.export.excel.null"));
					result.put(ERROR, true);
					return JSON;
				}
				/** Bo sung ATTT */
				String reportToken = retrieveReportToken(reportCode);
				if (StringUtil.isNullOrEmpty(reportToken)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
					return JSON;
				}
				/** workbook */
				workbook = new SXSSFWorkbook(200);
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				/** style */
				// HeaderLeft
				XSSFCellStyle headerStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK).clone();
				XSSFFont fontHeader = (XSSFFont) workbook.createFont();
				fontHeader.setFontHeight(10);
				fontHeader.setFontName("Arial");
				fontHeader.setBold(true);
				headerStyle.setFont(fontHeader);
				headerStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
				// HeaderLeftI
				XSSFCellStyle headerStyleI = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK).clone();
				XSSFFont fontHeaderI = (XSSFFont) workbook.createFont();
				fontHeaderI.setFontHeight(10);
				fontHeaderI.setItalic(true);
				fontHeaderI.setFontName("Arial");
				headerStyleI.setFont(fontHeaderI);
				headerStyleI.setAlignment(XSSFCellStyle.ALIGN_LEFT);
				// boldLeftI
				XSSFCellStyle boldLeftI = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.BOLD_LEFT).clone();
				XSSFFont fontBoldLeftI = (XSSFFont) workbook.createFont();
				fontBoldLeftI.setFontHeight(9);
				fontBoldLeftI.setBold(true);
				fontBoldLeftI.setItalic(true);
				fontBoldLeftI.setFontName("Arial");
				boldLeftI.setFont(fontBoldLeftI);
				// header
				XSSFCellStyle headerWrapText = (XSSFCellStyle)style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN).clone();
				headerWrapText.setWrapText(true);
				// normalCenterLeftI
				XSSFCellStyle normalCenterLeftI = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK).clone();
				XSSFFont normalCenterLeftIfont = (XSSFFont) workbook.createFont();
				normalCenterLeftIfont.setFontHeight(9);
				normalCenterLeftIfont.setItalic(true);
				normalCenterLeftIfont.setFontName("Arial");
				normalCenterLeftI.setFont(normalCenterLeftIfont);
				normalCenterLeftI.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				/** sheet */
				/*ApParamEquip nganhHangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_NGANH_HANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
				String nganhHangStr= "";
				if (nganhHangParam != null && !StringUtil.isNullOrEmpty(nganhHangParam.getValue())) {
					nganhHangStr = "(" + nganhHangParam.getValue() + ")";
				}*/ 
				for (int j = 0, sz = data.size(); j < sz; j++) {
					EquipBorrowVO equipBorrowVO = data.get(j);
					List<EquipLendDetailVO> lstDetailVOs = equipBorrowVO.getLstEquipLendDetailVOs();
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(equipBorrowVO.getCode());
					sheet.setDisplayGridlines(false);
					SXSSFReportHelper.setColumnWidths(sheet, 0, (short) 5, (short) 10, (short) 10, (short) 15, (short) 20, (short) 18, (short) 15);
					SXSSFReportHelper.setColumnWidths(sheet, 7, (short) 15, (short) 15, (short) 15, (short) 15, (short) 15, (short) 15);
					SXSSFReportHelper.setColumnWidths(sheet, 13, (short) 12, (short) 15, (short) 12, (short) 12, (short) 15, (short) 12);
					SXSSFReportHelper.setColumnWidths(sheet, 19, (short) 20, (short) 12, (short) 12, (short) 20, (short) 18, (short) 12, (short) 12);
					SXSSFReportHelper.setColumnWidths(sheet, 25, (short) 15, (short) 15, (short) 12, (short) 20, (short) 50);
					
					// header
					int colIdx = 0;
					int rowIdx = 0;
					// textbox
					/*XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, 24, 1, 27, 4);
					XSSFTextBox textbox = ((XSSFDrawing) sheet.createDrawingPatriarch()).createTextbox(anchor);
					textbox.setText(R.getResource("equip.proposal.export.sheet1.textbox1") + "\n" + R.getResource("equip.proposal.export.sheet1.textbox2") + "\n" + R.getResource("equip.proposal.export.sheet1.textbox3"));
					textbox.setLineWidth(1);
					textbox.setLineStyle(0);
					textbox.setLineStyleColor(0, 0, 0);
					textbox.setFillColor(255, 255, 255);*/
					// title
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.title"), headerStyle);
					/*if (currentUser != null && currentUser.getShopRoot() != null) {
						String shopCodeView = currentUser.getShopRoot().getShopCode() + " - " + currentUser.getShopRoot().getShopName();
						SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.title1") + " " + shopCodeView, headerStyleI);
					} else {
						SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.title1"), headerStyleI);
					}*/
					String printDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR);
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.title2") + " " + printDate, headerStyleI);
					SXSSFReportHelper.addCell(sheet, colIdx + 10, rowIdx++, R.getResource("equip.proposal.export.sheet1.title3"), style.get(ExcelPOIProcessUtils.TITLE_VNM_BLACK));
					rowIdx++;
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.title4"), headerStyleI);
					colIdx = 0;
					rowIdx += 2;
					SXSSFReportHelper.setRowHeights(sheet, rowIdx, (short) 25, (short) 30);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.stt"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.mien"), headerWrapText);
//					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.kenh"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.makhachhang"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.tencuahang"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.nguoidungten"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.quanhe"), headerWrapText);
					//
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx + 5, rowIdx, R.getResource("equip.proposal.export.sheet1.diachi"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.dienthoai"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.sonha"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.tenduong"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.phuong"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.quan"), headerWrapText);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.tinh"), headerWrapText);
					//
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.cmnd"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.noicap"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.ngaycap"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.sodkkd"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.noicapdkkd"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.ngaycapdkd"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.diachithuongtru"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.cogiaythuematbang"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.soluongtu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.loaitu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.tinhtrangtu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.thoigianmongnhantu"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.khoxuat"), headerWrapText);
//					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.doanhsobathang") + nganhHangStr, headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.hotengiamsat"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.dienthoaigs"), headerWrapText);
					SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, R.getResource("equip.proposal.export.sheet1.ghichu"), headerWrapText);
					// dong header cuoi
					colIdx = 0;
					rowIdx += 2;
					String[] numberTemp = R.getResource("equip.proposal.export.sheet1.hearder.number").split(",");
					for (int k = 0, szk = numberTemp.length; k < szk; k++) {
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, numberTemp[k], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					}
					// data
					rowIdx++;
					for (int i = 0, szi = lstDetailVOs.size(); i < szi; i++) {
						colIdx = 0;
						EquipLendDetailVO vo = lstDetailVOs.get(i);
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, String.valueOf(i + 1), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getShopCode() + " - " + vo.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getShopCodeKenh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getCustomerCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getCustomerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getRepresentative(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getRelation(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						//						
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getPhone(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getAddress(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getStreet(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getWard(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getDistrict(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getProvince(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						//
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getIdNo(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getIdNoPlace(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getIdNoDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getBusinessNo(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getBusinessNoPlace(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getBusinessNoDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getShopAddress(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, vo.getQuantity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getEquipCategory() + " " + vo.getCapacity() + "L", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getHealthStatus(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getTimeLend(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						if (TypeStockEquipLend.NCC.getValue().equals(vo.getStockTypeValue())) {
							SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, R.getResource("equip.proposal.import.ncc"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						} else {
							SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, R.getResource("equip.proposal.import.congty"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
//						SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, vo.getAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getStaffCode() + " - " + vo.getStaffName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						String t = "";
						if(!StringUtil.isNullOrEmpty(vo.getStaffPhone())){
							if(!StringUtil.isNullOrEmpty(vo.getStaffMobile())){
								t = vo.getStaffPhone() + " / " +vo.getStaffMobile();
							} else {
								t = vo.getStaffPhone();
							}
						}else {
							if(!StringUtil.isNullOrEmpty(vo.getStaffMobile())){
								t = vo.getStaffMobile();
							}
						}
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, t, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getNote(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						rowIdx++;
					}
					
					// dong tong
					colIdx = 4;
					int SUM_NUMBER = 20;
					for (int h = 0; h < 28; h++) {
						if (h == SUM_NUMBER) {
							SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, equipBorrowVO.getTotalEquip(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
						} else if (h > 3) {
							SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
					}
					SXSSFReportHelper.addMergeCells(sheet, 0, rowIdx, 3, rowIdx, R.getResource("equip.proposal.export.sheet1.sum"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
					
					rowIdx++;
					colIdx = 0;
					// chu ky + ghi chu bien ban
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.note"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.note1"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.note2"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.note3"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx, rowIdx++, R.getResource("equip.proposal.export.sheet1.note4"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx + 26, rowIdx++, R.getResource("equip.proposal.export.sheet1.ngay.thang.nam"), normalCenterLeftI);
					rowIdx++;
					SXSSFReportHelper.addCell(sheet, colIdx + 7, rowIdx, R.getResource("equip.proposal.export.sheet1.gd"), style.get(ExcelPOIProcessUtils.BOLD_CENTER));
					SXSSFReportHelper.addCell(sheet, colIdx + 12, rowIdx, R.getResource("equip.proposal.export.sheet1.tnkd"), style.get(ExcelPOIProcessUtils.BOLD_CENTER));
					SXSSFReportHelper.addCell(sheet, colIdx + 26, rowIdx, R.getResource("equip.proposal.export.sheet1.tbbh"), style.get(ExcelPOIProcessUtils.BOLD_CENTER));
				}
				/** Ghi file va tra ve ket qua */
				String outputPath = SXSSFReportHelper.exportXLSX(workbook, null, "equip.proposal.export.sheet1.name.file");
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				data = null;
			} else {
				result.put("errMsg", R.getResource("equip.proposal.export.not.record.export"));
				result.put(ERROR, true);
				return JSON;
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.exportEquipLend()" + ex.getMessage(), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * Xuat file template import
	 * 
	 * @author nhutnn
	 * @since 04/07/2015
	 */
	public String getFileTemplate() {
		actionStartTime = DateUtil.now();
		try {
			/** ATTT duong dan */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			List<EquipmentVO> lstEquipCategory = equipmentManagerMgr.getListEquipmentCategory(new EquipmentFilter<EquipmentVO>());
			List<ApParam> lstHealthy = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
			List<Area> lstArea = areaMgr.getListAreaFull();
			if (lstArea == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("err.msg.not.get.all"));
			} else {
				//Put du lieu
				List<CellBean> lstFails = new ArrayList<CellBean>();
				List<List<CellBean>> listFill = new ArrayList<List<CellBean>>();
				if (lstArea != null) {
					int size = lstArea.size();
					if (size > 65000)
						size = 65000;
					String provinceCode = "";
					String districtCode = "";
					for (int i = 0; i < size; i++) {
						CellBean cellBean = new CellBean();
						if (provinceCode.equals(lstArea.get(i).getProvince()) == false) {
							cellBean.setContent1(lstArea.get(i).getProvince() + " (" + lstArea.get(i).getProvinceName() + ")");
							cellBean.setContent2(lstArea.get(i).getProvinceName());
							provinceCode = lstArea.get(i).getProvince();
						}

						if (districtCode.equals(lstArea.get(i).getDistrict()) == false) {
							cellBean.setContent3(lstArea.get(i).getDistrict() + " (" + lstArea.get(i).getDistrictName() + ")");
							cellBean.setContent4(lstArea.get(i).getDistrictName());
							districtCode = lstArea.get(i).getDistrict();
						}

						cellBean.setContent5(lstArea.get(i).getPrecinct() + " (" + lstArea.get(i).getPrecinctName() + ")");
						cellBean.setContent6(lstArea.get(i).getPrecinctName());
						lstFails.add(cellBean);
					}
				}
				listFill.add(lstFails);
				//Put du lieu
				List<CellBean> lstEquip = new ArrayList<CellBean>();
				if (lstEquipCategory != null) {
					int size = lstEquipCategory.size();
					for (int i = 0; i < size; i++) {
						CellBean cellBean = new CellBean();
						cellBean.setContent1(lstEquipCategory.get(i).getCode() + " (" + lstEquipCategory.get(i).getName() + ")");
						lstEquip.add(cellBean);
					}
				}
				listFill.add(lstEquip);
				//Put du lieu
				List<CellBean> lstHeal = new ArrayList<CellBean>();
				if (lstHealthy != null) {
					int size = lstHealthy.size();
					for (int i = 0; i < size; i++) {
						CellBean cellBean = new CellBean();
						cellBean.setContent1(lstHealthy.get(i).getApParamCode() + " (" + lstHealthy.get(i).getApParamName() + ")");
						lstHeal.add(cellBean);
					}
				}
				listFill.add(lstHeal);
				String filePath = exportExcelDataTemplate(listFill, ConstantManager.TEMPLATE_IMPORT_RECORD_LEND);
				if (!StringUtil.isNullOrEmpty(filePath)) {
					MemcachedUtils.putValueToMemcached(reportToken, filePath, retrieveReportMemcachedTimeout());
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.getFileTemplate()" + e.getMessage(), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Xuat file excel template
	 * 
	 * @author nhutnn
	 * @since 06/07/2015
	 * @param lstData
	 * @param tempFileName
	 * @return
	 * @throws Exception
	 */
	private String exportExcelDataTemplate(List<List<CellBean>> lstData, String tempFileName) throws Exception {
		String outputPath = "";
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			if (lstData != null && lstData.size() > 0) {
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathEquipment() + tempFileName;
				templateFileName = templateFileName.replace('/', File.separatorChar);
				String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + tempFileName;
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("report", lstData.get(0));
				params.put("lstEquipCategory", lstData.get(1));
				params.put("lstHealthy", lstData.get(2));
				//params.put("lstTitle", lstTitle);
				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(LIST, outputPath);
			} else {
				result.put("hasData", false);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (os != null) {
				os.flush();
				os.close();
			}
		}
		return outputPath;
	}

	/**
	 * Kiem tra dong moi
	 * 
	 * @author nhutnn
	 * @param row
	 * @return
	 * @since 06/07/2015
	 */
	private boolean isANewRecord(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(22)) || !StringUtil.isNullOrEmpty(row.get(23))
				|| !StringUtil.isNullOrEmpty(row.get(24))) {
			return true;
		}
		return false;
	}

	/**
	 * Ham cat chuoi s tu vi tri 0 den vi tri dau tien cua chuoi cSub
	 * 
	 * @author nhutnn
	 * @param s
	 * @param cSub
	 * @return
	 * @since 30/07/2015
	 */
	private String subStringByString(String s, String cSub) {
		if (!StringUtil.isNullOrEmpty(s) && !StringUtil.isNullOrEmpty(cSub)) {
			int idx = s.indexOf(cSub);
			if (idx > 0) {
				s = s.substring(0, idx - 1);
			}
		}
		return s;
	}

	/**
	 * Kiem tra giam sat co quan ly shop ko
	 * 
	 * @author nhutnn
	 * @param staffCode
	 * @param shopId
	 * @return Boolean
	 * @since 12/08/2015
	 * 
	 * @modified vuongmq 07/03/2016
	 */
	private Boolean checkNVGS(String staffCode, Long shopId) throws Exception {
		boolean flag = false;
		if (!StringUtil.isNullOrEmpty(staffCode) && shopId != null) {
			List<StaffVO> lstStaffGS = new ArrayList<StaffVO>();
			// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
			if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null) {
				StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
				filter.setUserId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				filter.setStatus(ActiveType.RUNNING.getValue());
				List<Integer> lstObjectType = new ArrayList<Integer>();
				lstObjectType.add(StaffSpecificType.GSMT.getValue());
				lstObjectType.add(StaffSpecificType.SUPERVISOR.getValue());
				filter.setLstObjectType(lstObjectType);
				filter.setShopId(shopId);
				Staff staff = staffMgr.getStaffByCode(staffCode);
				if (staff != null) {
					filter.setStaffId(staff.getId());
					lstStaffGS = staffMgr.getListGSByFilter(filter);
					if (lstStaffGS != null) {
						flag = true;
					}
				}
			}
		}
		return flag;
	}
	/**
	 * Import excel nhan hang
	 * 
	 * @author nhutnn
	 * @since 08/06/2015
	 */
	public String importExcelRecord() {
		actionStartTime = DateUtil.now();
		final String CHAR_SUB = "(";
		try {
			resetToken(result);
			isError = true;
			errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
			totalItem = 0;
			String msg = "";
			// bien ban hop le (bao gom tat ca cac dong chi tiet)
			boolean isValidForm = true;
			boolean isNewRecord = true;
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<CellBean> lstFailsInForm = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 25);

			List<EquipLendDetail> lstEquipLendDetails = null;
			EquipLend equipLend = null;
			//			if(!StaffObjectType.NVGS.getValue().equals(currentUser.getStaffRoot().getObjectType())){
			//				isError = true;
			//				errMsg = R.getResource("equip.proposal.not.permission.import.excel");
			//				return SUCCESS;
			//			}
//			EquipPeriod equipPeriod = null;
			//			equipmentManagerMgr.getEquipPeriodCurrent();
			//			if (equipPeriod == null) {
			//				result.put(ERROR, true);
			//				result.put("errMsg", "Kỳ hiện tại không MỞ!");
			//				return JSON;
			//			}
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 1) {
				Date sysDate = commonMgr.getSysDate();
				for (int i = 1, sz = lstData.size(); i < sz; i++) {
					Date createFormDate = null;
					Shop shop = null;
					Customer customer = null;
					String representative = null;
					String relation = null;
					String address = null;
					String street = null;
					Area ward = null;
					Area district = null;
					Area province = null;
					String idNo = null;
					String idNoPlace = null;
					Date idNoDate = null;
					String businessNo = null;
					String businessNoPlace = null;
					Date businessNoDate = null;
					Long quantity = null;
					EquipCategory equipCategory = null;
					String capacity = null;
					String healthStatus = null;
					Date timeLend = null;
					Integer objectType = null;
					Integer status = null;
					Staff staff = null;
					String noteStr = null;
					msg = "";
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						String value = "";
						totalItem++;
						List<String> row = lstData.get(i);
						if (i == 1 || isANewRecord(row)) {
							isNewRecord = true;
							if (isValidForm && lstEquipLendDetails != null && lstEquipLendDetails.size() > 0 && equipLend != null) {
								equipLend = equipProposalBorrowMgr.createEquipLendWithListDetail(equipLend, lstEquipLendDetails);
								lstFailsInForm = new ArrayList<CellBean>();
								lstEquipLendDetails = null;
								equipLend = null;
							} else {
								lstFails.addAll(lstFailsInForm);
								lstFailsInForm = new ArrayList<CellBean>();
								isValidForm = true;
								lstEquipLendDetails = null;
								equipLend = null;
							}
						} else {
							isNewRecord = false;
						}
						if (isNewRecord) {
							// dong muon thiet bi chinh
							if (row.size() > 0) {
								// ngay lap
								value = row.get(0).trim();
								String msgTemp = ValidateUtil.validateField(value, "equip.proposal.create.date", 8, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
								if (StringUtil.isNullOrEmpty(msgTemp)) {
									createFormDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
									if (createFormDate == null || DateUtil.compareDateWithoutTime(createFormDate, sysDate) == 1) {
										msg += R.getResource("equip.proposal.import.ngaylap.not.more.now") + "\n";
									} else {
//										List<EquipPeriod> lstEquipPeriods = equipmentManagerMgr.getListEquipPeriodByDate(createFormDate);
//										if (lstEquipPeriods == null || lstEquipPeriods.size() == 0) {
//											msg += R.getResource("equip.proposal.import.ngaylap.not.in.period") + "\n";
//										} else {
//											equipPeriod = lstEquipPeriods.get(0);
//										}
									}
								} else {
									msg += msgTemp;
								}
							}
						}
						if (row.size() > 1) {
							// ma NPP
							value = row.get(1).trim().toUpperCase();
							String msgTemp = ValidateUtil.validateField(value, "customer.display.program.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								if (!checkShopInOrgAccessByCode(value)) {
									msg += R.getResource("common.shop.not.belong.user", value) + "\n";
								} else {
									shop = shopMgr.getShopByCode(value);
									if (shop == null) {
										msg += R.getResource("common.not.exist.in.db", R.getResource("customer.display.program.npp.code")) + "\n";
									} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
										msg += R.getResource("common.catalog.shop.status.not.active", value) + "\n";
									} else if (shop.getType() == null 
											|| !(ShopSpecificType.NPP.equals(shop.getType().getSpecificType())
											|| ShopSpecificType.NPP_MT.equals(shop.getType().getSpecificType()))) {
										msg += R.getResource("customer.shop.not.npp") + "\n";
									}
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 2 && shop != null) {
							// ma KH
							value = row.get(2).trim().toUpperCase();
							String msgTemp = ValidateUtil.validateField(value, "khmuontu.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								value = StringUtil.getFullCode(shop.getShopCode(), value);
								customer = customerMgr.getCustomerByCode(value);
								if (customer == null) {
									msg += R.getResource("common.not.exist.in.db", R.getResource("khmuontu.customer.code")) + "\n";
								} else if (!ActiveType.RUNNING.equals(customer.getStatus())) {
									msg += R.getResource("common.catalog.customer.status.not.active", value) + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 3) {
							// nguoi dai dien
							representative = row.get(3).trim();
							msg += ValidateUtil.validateField(representative, "equip.proposal.export.sheet1.nguoidungten", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						}
						if (row.size() > 4) {
							// quan he
							relation = row.get(4).trim();
							msg += ValidateUtil.validateField(relation, "equip.proposal.import.quanhe", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						}
						if (row.size() > 5) {
							// so nha
							address = row.get(5).trim();
							msg += ValidateUtil.validateField(address, "equip.proposal.export.sheet1.sonha", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
						}
						if (row.size() > 6) {
							// ten duong
							street = row.get(6).trim();
							msg += ValidateUtil.validateField(street, "equip.proposal.export.sheet1.tenduong", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						}
						if (row.size() > 7) {
							// tinh
							value = this.subStringByString(row.get(7), CHAR_SUB);
							String msgTemp = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.tinh", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								value = value.trim().toUpperCase();
								province = areaMgr.getAreaByCode(value);
								if (province == null) {
									msg += R.getResource("common.not.exist.in.db", R.getResource("equip.proposal.export.sheet1.tinh")) + "\n";
								} else if (!ActiveType.RUNNING.equals(province.getStatus())) {
									msg += R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.tinh")) + "\n";
								} else if (!AreaType.PROVINCE.equals(province.getType())) {
									msg += R.getResource("equip.proposal.import.not.type.tinh", R.getResource("equip.proposal.export.sheet1.tinh")) + "\n";
								}
							} else {
								msg += msgTemp;
							}
							if (province != null && StringUtil.isNullOrEmpty(msgTemp)) {
								// quan, huyen
								value = this.subStringByString(row.get(8), CHAR_SUB);
								msgTemp += ValidateUtil.validateField(value, "equip.proposal.export.sheet1.quan", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
								if (StringUtil.isNullOrEmpty(msgTemp)) {
									value = value.trim().toUpperCase();
									district = areaMgr.getAreaByCode(value);
									if (district == null) {
										msg += R.getResource("common.not.exist.in.db", R.getResource("equip.proposal.export.sheet1.quan")) + "\n";
									} else if (!ActiveType.RUNNING.equals(district.getStatus())) {
										msg += R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.quan")) + "\n";
									} else if (!AreaType.DISTRICT.equals(district.getType())) {
										msg += R.getResource("equip.proposal.import.not.type.quan", R.getResource("equip.proposal.export.sheet1.quan")) + "\n";
									} else if (district.getParentArea() == null || !province.getId().equals(district.getParentArea().getId())) {
										msg += R.getResource("customer.district") + "\n";
									}
								} else {
									msg += msgTemp;
								}
							}
							if (district != null && StringUtil.isNullOrEmpty(msgTemp)) {
								// phuong, xa
								value = this.subStringByString(row.get(9), CHAR_SUB);
								msgTemp += ValidateUtil.validateField(value, "equip.proposal.export.sheet1.phuong", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
								if (StringUtil.isNullOrEmpty(msgTemp)) {
									value = value.trim().toUpperCase();
									ward = areaMgr.getAreaByCode(value);
									if (ward == null) {
										msg += R.getResource("common.not.exist.in.db", R.getResource("equip.proposal.export.sheet1.phuong")) + "\n";
									} else if (!ActiveType.RUNNING.equals(ward.getStatus())) {
										msg += R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.phuong")) + "\n";
									} else if (!AreaType.WARD.equals(ward.getType())) {
										msg += R.getResource("equip.proposal.import.not.type.phuong", R.getResource("equip.proposal.export.sheet1.phuong")) + "\n";
									} else if (ward.getParentArea() == null || !district.getId().equals(ward.getParentArea().getId())) {
										msg += R.getResource("equip.proposal.import.phuong.not.in.quan") + "\n";
									}
								} else {
									msg += msgTemp;
								}
							}
						}
						if (row.size() > 10) {
							// CMND
							idNo = row.get(10).trim();
							msg += ValidateUtil.validateField(idNo, "equip.proposal.export.sheet1.cmnd", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);							
						}
						if (row.size() > 11) {
							// noi lap CMND
							idNoPlace = row.get(11).trim();
							msg += ValidateUtil.validateField(idNoPlace, "equip.proposal.export.sheet1.noicap", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						}
						if (row.size() > 12) {
							// ngay lap CMND
							value = row.get(12).trim();
							String msgTemp = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ngaycap", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								idNoDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								if (idNoDate == null || DateUtil.compareDateWithoutTime(idNoDate, sysDate) != -1) {
									msg += R.getResource("equip.proposal.import.ngaycap.id.not.more.now") + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 13) {
							// so DKKD
							businessNo = row.get(13).trim();
							if (!StringUtil.isNullOrEmpty(businessNo)) {
								msg += ValidateUtil.validateField(businessNo, "equip.proposal.export.sheet1.sodkkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
							}
						}
						if (row.size() > 14) {
							// noi lap so DKKD
							businessNoPlace = row.get(14).trim();
							if (!StringUtil.isNullOrEmpty(businessNoPlace)) {
								msg += ValidateUtil.validateField(businessNoPlace, "equip.proposal.export.sheet1.noicapdkkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
							}
						}
						if (row.size() > 15) {
							// ngay lap so DKKD
							value = row.get(15).trim();
							if (!StringUtil.isNullOrEmpty(value)) {
								String msgTemp = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ngaycapdkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
								if (StringUtil.isNullOrEmpty(msgTemp)) {
									businessNoDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
									if (businessNoDate == null || DateUtil.compareDateWithoutTime(businessNoDate, sysDate) != -1) {
										msg += R.getResource("equip.proposal.import.ngaydkkd.id.not.more.now") + "\n";
									}
								} else {
									msg += msgTemp;
								}
							}
						}

						if (row.size() > 16) {
							// loai tu
							value = this.subStringByString(row.get(16), CHAR_SUB);
							String msgTemp = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.loaitu", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								value = value.trim().toUpperCase();
								equipCategory = equipmentManagerMgr.getEquipCategoryByCode(value, null);
								if (equipCategory == null) {
									msg += R.getResource("common.not.exist.in.db", R.getResource("equip.proposal.export.sheet1.loaitu")) + "\n";
								} else if (!ActiveType.RUNNING.equals(equipCategory.getStatus())) {
									msg += R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.loaitu")) + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 17) {
							// dungtich
							capacity = row.get(17).trim();
							msg += ValidateUtil.validateField(capacity, "equip.proposal.import.dungtich", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						}
						if (row.size() > 18) {
							// tinh trang tu
							healthStatus = this.subStringByString(row.get(18), CHAR_SUB);
							String msgTemp = ValidateUtil.validateField(healthStatus, "equip.proposal.export.sheet1.tinhtrangtu", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								healthStatus = healthStatus.trim().toUpperCase();
								ApParam apParam = apParamMgr.getApParamByCode(healthStatus, ApParamType.EQUIP_CONDITION);
								if (apParam == null) {
									msg += R.getResource("common.not.date", R.getResource("equip.proposal.export.sheet1.tinhtrangtu")) + "\n";
								} else if (!ActiveType.RUNNING.equals(apParam.getStatus())) {
									msg += R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.tinhtrangtu")) + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 19) {
							// thoi gian muon nhan tu
							value = row.get(19).trim();
							String msgTemp = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.thoigianmongnhantu", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								timeLend = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								if (DateUtil.compareDateWithoutTime(timeLend, sysDate) == -1) {
									msg += R.getResource("equip.proposal.import.ngaymuon.not.before.now") + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 20) {
							// so luong
							value = row.get(20).trim();
							String msgTemp = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.soluongtu", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								quantity = Long.valueOf(value);
								if (quantity <= 0) {
									msg += R.getResource("common.possitive.integer", R.getResource("equip.proposal.export.sheet1.soluongtu")) + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (row.size() > 21) {
							// kho xuat
							value = row.get(21).trim();
							String msgTemp = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.khoxuat", 250, ConstantManager.ERR_REQUIRE);
							if (StringUtil.isNullOrEmpty(msgTemp)) {
								if (value.trim().toUpperCase().equals(R.getResource("equip.proposal.import.ncc").trim().toUpperCase())) {
									objectType = TypeStockEquipLend.NCC.getValue();
								} else if (value.trim().toUpperCase().equals(R.getResource("equip.proposal.import.congty").trim().toUpperCase())) {
									objectType = TypeStockEquipLend.COMPANY.getValue();
								} else {
									msg += R.getResource("common.not.date", R.getResource("equip.proposal.export.sheet1.khoxuat")) + "\n";
								}
							} else {
								msg += msgTemp;
							}
						}
						if (isNewRecord) {
							if (row.size() > 22) {
								// ma giam sat
								value = row.get(22).trim().toUpperCase();
								if (!StringUtil.isNullOrEmpty(value)) {
									String msgTemp = ValidateUtil.validateField(value, "equip.suggest.import.giamsat", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (StringUtil.isNullOrEmpty(msgTemp)) {
										staff = staffMgr.getStaffByCode(value);
										if (staff == null) {
											msg += R.getResource("common.not.exist.in.db", R.getResource("equip.suggest.import.magiamsat", value)) + "\n";
										} else if (!ActiveType.RUNNING.equals(staff.getStatus())) {
											msg += R.getResource("common.catalog.status.in.active", R.getResource("equip.suggest.import.magiamsat", value)) + "\n";
										} else if (!(StaffSpecificType.SUPERVISOR.equals(staff.getStaffType().getSpecificType()) 
												|| StaffSpecificType.GSMT.equals(staff.getStaffType().getSpecificType()))) {
											msg += R.getResource("equipment.invalid.mgs") + "\n";
										} else if (!checkNVGS(value, currentUser.getShopRoot().getShopId())) {
											msg += R.getResource("equipment.manager.mgs.role.invalid") + "\n";
										}
									} else {
										msg += msgTemp;
									}
								}
							}
							if (row.size() > 23) {
								// trang thai
								value = row.get(23).trim();
								String msgTemp = ValidateUtil.validateField(value, "equip.proposal.import.status", 250, ConstantManager.ERR_REQUIRE);
								if (StringUtil.isNullOrEmpty(msgTemp)) {
									if (value.trim().toUpperCase().equals(R.getResource("equip.proposal.import.du.thao").trim().toUpperCase())) {
										status = StatusRecordsEquip.DRAFT.getValue();
									} else if (value.trim().toUpperCase().equals(R.getResource("equip.proposal.import.cho.duyet").trim().toUpperCase())) {
										status = StatusRecordsEquip.WAITING_APPROVAL.getValue();
									} else {
										msg += R.getResource("common.not.date", R.getResource("equip.proposal.import.status")) + "\n";
									}
								} else {
									msg += msgTemp;
								}
							}
							// ** Ghi chu */
							if (row.size() > 24) {
								value = row.get(24);
								String msgTemp = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
								if (!StringUtil.isNullOrEmpty(msgTemp)) {
									msg += msgTemp;
								} else {
									noteStr = value;
								}
							}
						}

						if (StringUtil.isNullOrEmpty(msg)) {
							if (isNewRecord) {
								equipLend = new EquipLend();
								equipLend.setCreateFormDate(createFormDate);
								equipLend.setCreateUser(currentUser.getStaffRoot().getStaffCode());
								equipLend.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());
//								equipLend.setEquipPeriod(equipPeriod);
								shop = shopMgr.getShopByCode(currentUser.getShopRoot().getShopCode());
								equipLend.setShop(shop);
								equipLend.setStatus(status);								
								equipLend.setStaff(staff);
								equipLend.setNote(noteStr);
							}
							if (lstEquipLendDetails == null) {
								lstEquipLendDetails = new ArrayList<EquipLendDetail>();
							}
							EquipLendDetail equipLendDetail = new EquipLendDetail();
							equipLendDetail.setAddress(address);
							equipLendDetail.setBusinessNo(businessNo);
							equipLendDetail.setBusinessNoDate(businessNoDate);
							equipLendDetail.setBusinessNoPlace(businessNoPlace);
							equipLendDetail.setCapacity(capacity);
							equipLendDetail.setCreateUser(currentUser.getStaffRoot().getStaffCode());
							equipLendDetail.setCustomer(customer);
							equipLendDetail.setCustomerAddress(customer.getAddress());
							equipLendDetail.setCustomerName(customer.getCustomerName());
							equipLendDetail.setDistrictId(district.getId());
							equipLendDetail.setDistrictName(district.getAreaName());
							equipLendDetail.setEquipCategory(equipCategory);
							equipLendDetail.setHealthStatus(healthStatus);
							equipLendDetail.setIdNo(idNo);
							equipLendDetail.setIdNoDate(idNoDate);
							equipLendDetail.setIdNoPlace(idNoPlace);
							equipLendDetail.setObjectType(objectType);
							if(!StringUtil.isNullOrEmpty(customer.getPhone()) && !StringUtil.isNullOrEmpty(customer.getMobiphone())){
								equipLendDetail.setPhone(customer.getPhone() +" / "+ customer.getMobiphone());
							} else if(!StringUtil.isNullOrEmpty(customer.getPhone())){
								equipLendDetail.setPhone(customer.getPhone());
							} else if(!StringUtil.isNullOrEmpty(customer.getMobiphone())){
								equipLendDetail.setPhone(customer.getMobiphone());
							} 
							equipLendDetail.setProvinceId(province.getId());
							equipLendDetail.setProvinceName(province.getAreaName());
							equipLendDetail.setRelation(relation);
							equipLendDetail.setRepresentative(representative);
							equipLendDetail.setStatus(ActiveType.RUNNING.getValue());
							equipLendDetail.setStreet(street);
							equipLendDetail.setTimeLend(timeLend);
							equipLendDetail.setWardId(ward.getId());
							equipLendDetail.setWardName(ward.getAreaName());
							equipLendDetail.setQuantity(new BigDecimal(quantity));
							//lay doanh so
							/*BigDecimal amount = getAmountTB(customer);
							equipLendDetail.setAmount(amount);*/

							lstEquipLendDetails.add(equipLendDetail);
						} else {
							// co loi trong nhan hang import
							isValidForm = false;
						}
						lstFailsInForm.add(StringUtil.addFailBean(row, msg));
					}
				}
				if (isValidForm && lstEquipLendDetails != null && lstEquipLendDetails.size() > 0 && equipLend != null) {
					equipLend = equipProposalBorrowMgr.createEquipLendWithListDetail(equipLend, lstEquipLendDetails);
					lstFailsInForm = new ArrayList<CellBean>();
					lstEquipLendDetails = null;
					equipLend = null;
				} else {
					lstFails.addAll(lstFailsInForm);
					lstFailsInForm = new ArrayList<CellBean>();
					isValidForm = true;
					lstEquipLendDetails = null;
					equipLend = null;
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_RECORD_LEND_FAIL);
			} else {
				isError = true;
				errMsg = R.getResource("import.excel.empty.content");
				return SUCCESS;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.importExcelRecord()"), createLogErrorStandard(actionStartTime));
			isError = true;
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}

		return SUCCESS;
	}

	/**
	 * @author nhutnn
	 * @date 07/07/2015 Ham lay doanh so TB theo cau hinh: nganh hang va so
	 *       thang TB, theo Khach hang
	 */
	private BigDecimal getAmountTB(Customer customer) {
		/**
		 * doanh so trung binh cua cau hinh so thang va nganh hang; chi ap dung
		 * cho Khach hang
		 */
		actionStartTime = DateUtil.now();
		BigDecimal amount = BigDecimal.ZERO;
		try {
			ApParamEquip soThangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_THANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
			/** Truong hop khong cau hinh so thang thi setAmount = 0 */
			if (soThangParam != null && soThangParam.getValue() != null) {
				EquipRepairFilter filter = new EquipRepairFilter();
				filter.setCustomerId(customer.getId());
				Integer soThang = null; // gan bang null, ben DAOImpl kiem tra khac null moi co dieu kien nay
				BigDecimal soThangBigDecimal = BigDecimal.ONE;
				if (soThangParam != null && soThangParam.getValue() != null) {
					soThang = Integer.valueOf(soThangParam.getValue());
					soThangBigDecimal = BigDecimal.valueOf(soThang);
				}
				filter.setSoThang(soThang);
				ApParamEquip nganhHangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_NGANH_HANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
				String nganhHangStr = "";
				if (nganhHangParam != null && nganhHangParam.getValue() != null) {
					nganhHangStr = nganhHangParam.getValue();
				}
				filter.setNganhHangStr(nganhHangStr);
				amount = (BigDecimal) equipmentManagerMgr.getDoanhSoEquipRepair(filter);
				if (!BigDecimal.ZERO.equals(soThangBigDecimal)) {
					amount = amount.divide(soThangBigDecimal, RoundingMode.FLOOR);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.getAmountTB()"), createLogErrorStandard(actionStartTime));
		}
		return amount;
	}

//	/**
//	 * Lay danh sach nhan vien GS
//	 * 
//	 * @author nhutnn
//	 * @since 12/08/2015
//	 * @return
//	 */
//	private List<StaffVO> getListGSNPP(String shopCode) throws Exception {
//		ObjectVO<StaffVO> objVO = null;
//		// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
//		StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
//		filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
//		filter.setStatus(ActiveType.RUNNING.getValue());
//		ShopToken shopVo = null;
//		if (!StringUtil.isNullOrEmpty(shopCode)) {
//			if (checkShopInOrgAccessByCode(shopCode)) {
//				shopVo = getShopTockenInOrgAccessByCode(shopCode);
//				filter.setShopId(shopVo.getShopId());
//			} else {
//				return null;
//			}
//		} else {
//			return null;
//		}
//		//			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
//		//				filter.setIsLstChildStaffRoot(true);
//		//				objVO = staffMgr.getListGSByStaffRootWithInParentStaffMap(filter);
//		//			} else	
//		Shop toShop = commonMgr.getEntityById(Shop.class, shopVo.getShopId());
//		if (toShop != null && toShop.getType() != null) {
//			filter.setIsLstChildStaffRoot(false);
//			if (ShopSpecificType.NPP_MT.equals(toShop.getType().getSpecificType())) {
//				List<Integer> lstObjectType = new ArrayList<Integer>();
//				lstObjectType.add(StaffSpecificType.GSMT.getValue());
//				lstObjectType.add(StaffSpecificType.SUPERVISOR.getValue());
//				filter.setLstObjectType(lstObjectType);
//
//				objVO = staffMgr.getListGSByStaffRootKAMT(filter);
//			} else {
//				filter.setGsmt(StaffSpecificType.GSMT.getValue());
////				filter.setGska(StaffObjectType.GSKA.getValue());
//				objVO = staffMgr.getListGSByStaffRootWithNVBH(filter);
//			}
//		}
//		if (objVO != null) {
//			return objVO.getLstObject();
//		}
//		return null;
//	}

	/**
	 * 
	 * Xu ly getListGSNPP
	 * @author vuongmq
	 * @return List<StaffVO>
	 * @throws Exception
	 * @since Feb 24, 2016
	 */
	private List<StaffVO> getListGSNPP() throws Exception {
		List<StaffVO> lstStaffGS = new ArrayList<StaffVO>();
		// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
		if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null) {
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setUserId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			List<Integer> lstObjectType = new ArrayList<Integer>();
			lstObjectType.add(StaffSpecificType.GSMT.getValue());
			lstObjectType.add(StaffSpecificType.SUPERVISOR.getValue());
			filter.setLstObjectType(lstObjectType);
			Long shopId = currentUser.getShopRoot().getShopId();
			if (shopId != null) {
				if (checkShopInOrgAccessById(shopId)) {
					filter.setShopId(shopId);
				}
			}
			lstStaffGS = staffMgr.getListGSByFilter(filter);
			if (lstStaffGS == null) {
				lstStaffGS = new ArrayList<StaffVO>();
			}
		}
		return lstStaffGS;
	}
	
	/**
	 * Chuyen trang tao moi hay chinh sua bien ban
	 * 
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	public String changeRecord() {
		actionStartTime = DateUtil.now();
		generateToken();
		try {
			result.put(ERROR, false);
			/*ApParamEquip nganhHangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_NGANH_HANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
			if (nganhHangParam != null && !StringUtil.isNullOrEmpty(nganhHangParam.getValue())) {
				nganhHangStr = "(" + nganhHangParam.getValue() + ")";
			} else {
				nganhHangStr = "";
			}*/
			lstStaffVOs = this.getListGSNPP();
			if (lstStaffVOs == null) {
				lstStaffVOs = new ArrayList<StaffVO>();
			}
			if (id != null) {
				EquipLend equipLend = equipProposalBorrowMgr.getEquipLendById(id);
				if (equipLend != null) {
					if (equipLend.getShop() != null && !checkShopInOrgAccessById(equipLend.getShop().getId())) {
						return PAGE_NOT_PERMISSION;
					}
					code = equipLend.getCode();
					note = equipLend.getNote();
					createFormDate = DateUtil.toDateString(equipLend.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					if (equipLend.getStaff() != null) {
						superviseCode = equipLend.getStaff().getStaffCode();
						// gan danh sach ma GS neu GS co trong bien ban nhung khong thuoc phan quyen					
						boolean isExist = false;
						for (int i = 0, sz = lstStaffVOs.size(); i < sz; i++) {
							if (equipLend.getStaff().getId().equals(lstStaffVOs.get(i).getId())) {
								isExist = true;
								break;
							}
						}
						if (!isExist) {
							StaffVO staffVO = new StaffVO();
							staffVO.setStaffCode(equipLend.getStaff().getStaffCode());
							staffVO.setStaffName(equipLend.getStaff().getStaffName());
							lstStaffVOs.add(staffVO);
						}
					} else {
						StaffVO staffVO = new StaffVO();
						staffVO.setStaffCode("");
						staffVO.setStaffName("");
						lstStaffVOs.add(0, staffVO);
					}
					statusRecord = equipLend.getStatus();
				} else {
					id = null;
					createFormDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
					superviseCode = currentUser.getStaffRoot().getStaffCode();
					statusRecord = StatusRecordsEquip.DRAFT.getValue();
				}
			} else {
				createFormDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
				superviseCode = currentUser.getStaffRoot().getStaffCode();
				statusRecord = StatusRecordsEquip.DRAFT.getValue();
			}
			shopCode = currentUser.getShopRoot().getShopCode();
			if (ShopObjectType.MIEN_ST.getValue().equals(currentUser.getShopRoot().getObjectType())) {
				isLevelShop = ShopDecentralizationSTT.NPP.getValue();
			} else {
				isLevelShop = currentUser.getShopRoot().getIsLevel();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.changeRecord()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * Validate cac truong trong chi tiet bien ban
	 * 
	 * @author nhutnn
	 * @since 08/07/2015
	 * @param lstDetails
	 * @param lstEquipLendDetails
	 * @return
	 * @throws BusinessException
	 */
	private String validateListDetail(List<EquipLendDetailVO> lstDetails, List<EquipLendDetail> lstEquipLendDetails) throws BusinessException {
		String msg = "";
		String value = "";
		for (int i = 0, sz = lstDetails.size(); i < sz; i++) {
			// lay chi tiet tu giao dien
			EquipLendDetailVO equipLendDetailVO = lstDetails.get(i);
			// chi tiet them vao danh sach dua len Database
			EquipLendDetail equipLendDetail = new EquipLendDetail();
			equipLendDetail.setId(equipLendDetailVO.getId());
			lstEquipLendDetails.add(equipLendDetail);
			// kiem tra neu id==null la tu giao dien truyen xuong

			Customer customer = null;
			String representative = null;
			String relation = null;
			String address = null;
			String street = null;
			Area ward = null;
			Area district = null;
			Area province = null;
			String idNo = null;
			String idNoPlace = null;
			Date idNoDate = null;
			String businessNo = null;
			String businessNoPlace = null;
			Date businessNoDate = null;
			BigDecimal quantity = null;
			EquipCategory equipCategory = null;
			String capacity = null;
			String healthStatus = null;
			Date timeLend = null;
			Integer objectType = null;
			Shop shop = null;
			// ma NPP
			value = equipLendDetailVO.getShopCode();
			msg = ValidateUtil.validateField(value, "customer.display.program.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
			if (StringUtil.isNullOrEmpty(msg)) {
				value = value.trim().toUpperCase();
				if (!checkShopInOrgAccessByCode(value)) {
					msg = R.getResource("common.shop.not.belong.user", value) + "\n";
				} else {
					shop = shopMgr.getShopByCode(value);
					if (shop == null) {
						msg = R.getResource("common.not.exist.in.db", R.getResource("customer.display.program.npp.code")) + "\n";
					} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
						msg = R.getResource("common.catalog.shop.status.not.active", value) + "\n";
					} else if (shop.getType() == null 
							|| !(ShopSpecificType.NPP.equals(shop.getType().getSpecificType())
							|| ShopSpecificType.NPP_MT.equals(shop.getType().getSpecificType())
							/*|| ShopObjectType.MIEN_ST.getValue().equals(shop.getType().getObjectType())*/)) {
						msg += R.getResource("customer.shop.not.npp") + "\n";
					}
				}
			}
			if (StringUtil.isNullOrEmpty(msg) && shop != null) {
				// ma KH
				value = equipLendDetailVO.getCustomerCode();
				msg = ValidateUtil.validateField(value, "khmuontu.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				if (StringUtil.isNullOrEmpty(msg)) {
					value = value.trim().toUpperCase();
					value = StringUtil.getFullCode(shop.getShopCode(), value);
					customer = customerMgr.getCustomerByCode(value);
					if (customer == null) {
						msg = R.getResource("common.not.exist.in.db", R.getResource("khmuontu.customer.code")) + "\n";
					} else if (!ActiveType.RUNNING.equals(customer.getStatus())) {
						msg = R.getResource("common.catalog.customer.status.not.active", value) + "\n";
					}
				} 
			}
			// nguoi dai dien
			if (StringUtil.isNullOrEmpty(msg)) {
				representative = equipLendDetailVO.getRepresentative().trim();
				msg = ValidateUtil.validateField(representative, "equip.proposal.export.sheet1.nguoidungten", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
			}
			// quan he
			if (StringUtil.isNullOrEmpty(msg)) {
				relation = equipLendDetailVO.getRelation().trim();
				msg = ValidateUtil.validateField(relation, "equip.proposal.import.quanhe", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
			}
			// so nha
			if (StringUtil.isNullOrEmpty(msg)) {
				address = equipLendDetailVO.getAddress().trim();
				msg = ValidateUtil.validateField(address, "equip.proposal.export.sheet1.sonha", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
			}
			// ten duong
			if (StringUtil.isNullOrEmpty(msg)) {
				street = equipLendDetailVO.getStreet().trim();
				msg = ValidateUtil.validateField(street, "equip.proposal.export.sheet1.tenduong", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
			}
			// tinh
			if (StringUtil.isNullOrEmpty(msg)) {
				value = equipLendDetailVO.getProvinceCode().trim().toUpperCase();
				msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.tinh", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				province = areaMgr.getAreaByCode(value);
				if (province == null) {
					msg = R.getResource("common.not.exist.in.db", R.getResource("equip.proposal.export.sheet1.tinh")) + "\n";
				} else if (!ActiveType.RUNNING.equals(province.getStatus())) {
					msg = R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.tinh")) + "\n";
				}
			}
			if (province != null && StringUtil.isNullOrEmpty(msg)) {
				// quan, huyen
				value = equipLendDetailVO.getDistrictCode().trim().toUpperCase();
				msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.quan", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				if (StringUtil.isNullOrEmpty(msg)) {
					district = areaMgr.getAreaByCode(value);
					if (district == null) {
						msg = R.getResource("common.not.exist.in.db", R.getResource("equip.proposal.export.sheet1.quan")) + "\n";
					} else if (!ActiveType.RUNNING.equals(district.getStatus())) {
						msg = R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.quan")) + "\n";
					} else if (district.getParentArea() == null || !province.getId().equals(district.getParentArea().getId())) {
						msg = R.getResource("customer.district") + "\n";
					}
				}
			}
			if (district != null && StringUtil.isNullOrEmpty(msg)) {
				// phuong, xa
				value = equipLendDetailVO.getWardCode().trim().toUpperCase();
				msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.phuong", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				if (StringUtil.isNullOrEmpty(msg)) {
					ward = areaMgr.getAreaByCode(value);
					if (ward == null) {
						msg += R.getResource("common.not.exist.in.db", R.getResource("equip.proposal.export.sheet1.phuong")) + "\n";
					} else if (!ActiveType.RUNNING.equals(ward.getStatus())) {
						msg += R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.phuong")) + "\n";
					} else if (ward.getParentArea() == null || !district.getId().equals(ward.getParentArea().getId())) {
						msg += R.getResource("equip.proposal.import.phuong.not.in.quan") + "\n";
					}
				}
			}
			// CMND
			if (StringUtil.isNullOrEmpty(msg)) {
				idNo = equipLendDetailVO.getIdNo().trim();
				msg = ValidateUtil.validateField(idNo, "equip.proposal.export.sheet1.cmnd", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
			}
			// noi lap CMND
			if (StringUtil.isNullOrEmpty(msg)) {
				idNoPlace = equipLendDetailVO.getIdNoPlace().trim();
				msg = ValidateUtil.validateField(idNoPlace, "equip.proposal.export.sheet1.noicap", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
			}
			// ngay lap CMND
			if (StringUtil.isNullOrEmpty(msg)) {
				value = equipLendDetailVO.getIdNoDate().trim();
				msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ngaycap", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
				if (StringUtil.isNullOrEmpty(msg)) {
					idNoDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
					if (DateUtil.compareDateWithoutTime(idNoDate, DateUtil.now()) != -1) {
						msg = R.getResource("equip.proposal.import.ngaycap.id.not.more.now") + "\n";
					}
				}
			}
			// so DKKD
			if (StringUtil.isNullOrEmpty(msg)) {
				businessNo = equipLendDetailVO.getBusinessNo();
				if (!StringUtil.isNullOrEmpty(businessNo)) {
					businessNo = businessNo.trim();
					msg = ValidateUtil.validateField(businessNo, "equip.proposal.export.sheet1.sodkkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
				}
			}
			// noi lap so DKKD
			if (StringUtil.isNullOrEmpty(msg)) {
				businessNoPlace = equipLendDetailVO.getBusinessNoPlace();
				if (!StringUtil.isNullOrEmpty(businessNoPlace)) {
					businessNoPlace = businessNoPlace.trim();
					msg = ValidateUtil.validateField(businessNoPlace, "equip.proposal.export.sheet1.noicapdkkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
				}
			}
			// ngay lap so DKKD
			if (StringUtil.isNullOrEmpty(msg)) {
				value = equipLendDetailVO.getBusinessNoDate();
				if (!StringUtil.isNullOrEmpty(value)) {
					value = value.trim();
					msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ngaycapdkd", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
					if (StringUtil.isNullOrEmpty(msg)) {
						businessNoDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
						if (DateUtil.compareDateWithoutTime(businessNoDate, DateUtil.now()) != -1) {
							msg = R.getResource("equip.proposal.import.ngaydkkd.id.not.more.now") + "\n";
						}
					}
				}
			}
			// loai tu
			if (StringUtil.isNullOrEmpty(msg)) {
				value = equipLendDetailVO.getEquipCategoryCode().trim().toUpperCase();
				msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.loaitu", 50, ConstantManager.ERR_REQUIRE);
				if (StringUtil.isNullOrEmpty(msg)) {
					equipCategory = equipmentManagerMgr.getEquipCategoryByCode(value, null);
					if (equipCategory == null) {
						msg = R.getResource("common.not.exist.in.db", R.getResource("equip.proposal.export.sheet1.loaitu")) + "\n";
					} else if (!ActiveType.RUNNING.equals(equipCategory.getStatus())) {
						msg = R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.loaitu")) + "\n";
					}
				}
			}
			// dungtich
			if (StringUtil.isNullOrEmpty(msg)) {
				capacity = equipLendDetailVO.getCapacity().trim();
				msg = ValidateUtil.validateField(capacity, "equip.proposal.import.dungtich", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
			}
			// tinh trang tu
			if (StringUtil.isNullOrEmpty(msg)) {
				healthStatus = equipLendDetailVO.getHealthStatusCode().trim().toUpperCase();
				msg = ValidateUtil.validateField(healthStatus, "equip.proposal.export.sheet1.tinhtrangtu", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				if (StringUtil.isNullOrEmpty(msg)) {
					ApParam apParam = apParamMgr.getApParamByCode(healthStatus, ApParamType.EQUIP_CONDITION);
					if (apParam == null) {
						msg = R.getResource("common.not.date", R.getResource("equip.proposal.export.sheet1.tinhtrangtu")) + "\n";
					} else if (!ActiveType.RUNNING.equals(apParam.getStatus())) {
						msg = R.getResource("common.catalog.status.in.active", R.getResource("equip.proposal.export.sheet1.tinhtrangtu")) + "\n";
					}
				}
			}
			// thoi gian muon nhan tu
			if (StringUtil.isNullOrEmpty(msg)) {
				value = equipLendDetailVO.getTimeLend().trim();
				msg = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.thoigianmongnhantu", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
				if (StringUtil.isNullOrEmpty(msg)) {
					timeLend = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
					if (DateUtil.compareDateWithoutTime(timeLend, DateUtil.now()) == -1) {
						msg = R.getResource("equip.proposal.import.ngaymuon.not.before.now");
					}
				}
			}
			// so luong
			if (StringUtil.isNullOrEmpty(msg)) {
				if (equipLendDetailVO.getQuantity() != null) {
					quantity = equipLendDetailVO.getQuantity();
					msg = ValidateUtil.validateField(quantity.toString(), "equip.proposal.export.sheet1.soluongtu", 10, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
					if (StringUtil.isNullOrEmpty(msg)) {
						if (quantity.compareTo(BigDecimal.ZERO) <= 0) {
							msg = R.getResource("common.possitive.integer", R.getResource("equip.proposal.export.sheet1.soluongtu"));
						}
					}
				} else {
					msg = R.getResource("common.required.field", R.getResource("equip.proposal.export.sheet1.soluongtu"));
				}
			}
			// kho xuat
			if (StringUtil.isNullOrEmpty(msg)) {
				if (equipLendDetailVO.getStockTypeValue() != null) {
					objectType = equipLendDetailVO.getStockTypeValue();
					if (!TypeStockEquipLend.NCC.getValue().equals(objectType) && !TypeStockEquipLend.COMPANY.getValue().equals(objectType)) {
						msg = R.getResource("common.not.date", R.getResource("equip.proposal.export.sheet1.khoxuat"));
					}
				} else {
					msg = R.getResource("common.required.field", R.getResource("equip.proposal.export.sheet1.khoxuat"));
				}
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				msg = R.getResource("equip.proposal.update.dong", (i + 1)) + " " + msg;
				break;
			} else {
				equipLendDetail.setAddress(address);
				equipLendDetail.setBusinessNo(businessNo);
				equipLendDetail.setBusinessNoDate(businessNoDate);
				equipLendDetail.setBusinessNoPlace(businessNoPlace);
				equipLendDetail.setCapacity(capacity);
				if(equipLendDetail.getId() == null){
					equipLendDetail.setCreateUser(currentUser.getStaffRoot().getStaffCode());
				}else {
					equipLendDetail.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
				}
				equipLendDetail.setCustomer(customer);
				equipLendDetail.setCustomerAddress(customer.getAddress());
				equipLendDetail.setCustomerName(customer.getCustomerName());
				equipLendDetail.setDistrictId(district.getId());
				equipLendDetail.setDistrictName(district.getAreaName());
				equipLendDetail.setEquipCategory(equipCategory);
				equipLendDetail.setHealthStatus(healthStatus);
				equipLendDetail.setIdNo(idNo);
				equipLendDetail.setIdNoDate(idNoDate);
				equipLendDetail.setIdNoPlace(idNoPlace);
				equipLendDetail.setObjectType(objectType);
				if(!StringUtil.isNullOrEmpty(customer.getPhone()) && !StringUtil.isNullOrEmpty(customer.getMobiphone())){
					equipLendDetail.setPhone(customer.getPhone() +" / "+ customer.getMobiphone());
				} else if(!StringUtil.isNullOrEmpty(customer.getPhone())){
					equipLendDetail.setPhone(customer.getPhone());
				} else if(!StringUtil.isNullOrEmpty(customer.getMobiphone())){
					equipLendDetail.setPhone(customer.getMobiphone());
				} 
				equipLendDetail.setProvinceId(province.getId());
				equipLendDetail.setProvinceName(province.getAreaName());
				equipLendDetail.setRelation(relation);
				equipLendDetail.setRepresentative(representative);
				equipLendDetail.setStatus(ActiveType.RUNNING.getValue());
				equipLendDetail.setStreet(street);
				equipLendDetail.setTimeLend(timeLend);
				equipLendDetail.setWardId(ward.getId());
				equipLendDetail.setWardName(ward.getAreaName());
				equipLendDetail.setQuantity(quantity);
				//lay doanh so
				/*BigDecimal amount = getAmountTB(customer);
				equipLendDetail.setAmount(amount);*/
			}
		}

		return msg;
	}

	/**
	 * Kiem tra nhan vien giam sat
	 * 
	 * @author nhutnn
	 * @param staff
	 * @return
	 * @throws Exception
	 * @since 13/08/2015
	 */
	private String validateStaffCode(Staff staff) throws Exception {
		String msg = "";
		if (staff == null) {
			msg = R.getResource("common.not.exist.in.db", R.getResource("equip.suggest.import.giamsat")) + "\n";
		} else if (!ActiveType.RUNNING.equals(staff.getStatus())) {
			msg = R.getResource("common.catalog.status.in.active", R.getResource("equip.suggest.import.giamsat")) + "\n";
//		} else if (!(StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType()) || StaffObjectType.GSKA.getValue().equals(staff.getStaffType().getObjectType()) || StaffObjectType.GSMT.getValue().equals(
//				staff.getStaffType().getObjectType()))) {
		} else if (!(StaffSpecificType.SUPERVISOR.equals(staff.getStaffType().getSpecificType()) || StaffSpecificType.GSMT.equals(staff.getStaffType().getSpecificType()))) {
			msg = R.getResource("equipment.invalid.mgs") + "\n";
		} else if (!this.checkNVGS(staffCode, currentUser.getShopRoot().getShopId())) {
			msg = R.getResource("equipment.manager.mgs.role.invalid") + "\n";
		}
		return msg;
	}
	
	/**
	 * Tao moi hay chinh sua bien ban
	 * 
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	public String updateRecord() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			result.put(ERROR, false);
			String msg = "";
			if (lstDetails != null && lstDetails.size() > 0) {
				Date dateForm = null;
//				EquipPeriod equipPeriod = null;
				Staff staff = null;
				if (id != null) {
					// chinh sua bien ban
					EquipLend equipLend = equipProposalBorrowMgr.getEquipLendById(id);
					if (equipLend != null) {
						// ngay lap
						if (createFormDate != null && !createFormDate.equals(equipLend.getCreateFormDate()) 
							&& !StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) 
							&& StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus())) {
							dateForm = DateUtil.parse(createFormDate, DateUtil.DATE_FORMAT_DDMMYYYY);
							if (dateForm == null || DateUtil.compareDateWithoutTime(dateForm, DateUtil.now()) == 1) {
								msg = R.getResource("equip.proposal.import.ngaylap.not.more.now");
							} 
//							else {
//								List<EquipPeriod> lstEquipPeriods = equipmentManagerMgr.getListEquipPeriodByDate(dateForm);
//								if (lstEquipPeriods == null || lstEquipPeriods.size() == 0) {
//									msg = R.getResource("equip.proposal.import.ngaylap.not.in.period");
//								} else {
//									equipPeriod = lstEquipPeriods.get(0);
//								}
//							}
						} else if (createFormDate == null) {
							msg = R.getResource("common.required.field", R.getResource("equip.proposal.create.date"));
						}
						// ma giam sat
						if (StringUtil.isNullOrEmpty(msg) && StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus()) && !StringUtil.isNullOrEmpty(staffCode) 
							&& (equipLend.getStaff() == null || (equipLend.getStaff() != null && !staffCode.equals(equipLend.getStaff().getStaffCode())))) {
							msg = ValidateUtil.validateField(staffCode, "equip.suggest.import.giamsat", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(msg)) {
								staff = staffMgr.getStaffByCode(staffCode);
								msg = this.validateStaffCode(staff);
							}
						}
						// trang thai
						if (statusRecord != null && !statusRecord.equals(equipLend.getStatus()) && StringUtil.isNullOrEmpty(msg)) {
							if (!((StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus()))
								|| (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus()))
								|| (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipLend.getStatus()))
								|| (StatusRecordsEquip.CANCELLATION.getValue().equals(statusRecord) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipLend.getStatus()))
								|| (StatusRecordsEquip.APPROVED.getValue().equals(statusRecord) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipLend.getStatus())) 
								|| (StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusRecord) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipLend.getStatus())))) {
								msg = R.getResource("common.not.date", R.getResource("equip.proposal.import.status"));
							}
						}
						// danh sach chi tiet
						List<EquipLendDetail> lstEquipLendDetails = new ArrayList<EquipLendDetail>();
						if ((StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus()) || StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipLend.getStatus())) 
							&& StringUtil.isNullOrEmpty(msg)) {
							msg = this.validateListDetail(lstDetails, lstEquipLendDetails);
						}
						//ghi chu
						equipLend.setNote(note);
						// thong bao loi 
						if (!StringUtil.isNullOrEmpty(msg)) {
							result.put(ERROR, true);
							result.put("errMsg", msg);
							return JSON;
						}
						// cap nhat du lieu
						if (dateForm != null) {
							equipLend.setCreateFormDate(dateForm);
//							equipLend.setEquipPeriod(equipPeriod);
						}
						if (staff != null) {
							equipLend.setStaff(staff);
						}

						equipLend.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
						EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
						filter.setStatus(statusRecord);
						equipProposalBorrowMgr.updateEquipLendWithListDetail(equipLend, lstEquipLendDetails, filter);
					}
				} else {
					// tao moi bien ban					
					// ngay lap
					msg = ValidateUtil.validateField(createFormDate, "equip.proposal.create.date", 8, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
					if (StringUtil.isNullOrEmpty(msg)) {
						dateForm = DateUtil.parse(createFormDate, DateUtil.DATE_FORMAT_DDMMYYYY);
						if (dateForm == null || DateUtil.compareDateWithoutTime(dateForm, DateUtil.now()) == 1) {
							msg = R.getResource("equip.proposal.import.ngaylap.not.more.now");
						} 
//						else {
//							List<EquipPeriod> lstEquipPeriods = equipmentManagerMgr.getListEquipPeriodByDate(dateForm);
//							if (lstEquipPeriods == null || lstEquipPeriods.size() == 0) {
//								msg = R.getResource("equip.proposal.import.ngaylap.not.in.period");
//							} else {
//								equipPeriod = lstEquipPeriods.get(0);
//							}
//						}
					}
					// ma giam sat
					if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(staffCode)) {
						msg = ValidateUtil.validateField(staffCode, "equip.suggest.import.giamsat", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(msg)) {
							staff = staffMgr.getStaffByCode(staffCode);
							msg = validateStaffCode(staff);
						}
					}
					// trang thai
					if (!StatusRecordsEquip.DRAFT.getValue().equals(statusRecord)) {
						msg = R.getResource("common.not.date", R.getResource("equip.proposal.import.status"));
					}
					// danh sach chi tiet
					List<EquipLendDetail> lstEquipLendDetails = new ArrayList<EquipLendDetail>();
					if (StringUtil.isNullOrEmpty(msg)) {
						msg = validateListDetail(lstDetails, lstEquipLendDetails);
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						result.put(ERROR, true);
						result.put("errMsg", msg);
						return JSON;
					}

					// tao moi du lieu
					EquipLend equipLend = new EquipLend();
					equipLend.setCreateFormDate(dateForm);
					equipLend.setCreateUser(currentUser.getStaffRoot().getStaffCode());
					equipLend.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());
//					equipLend.setEquipPeriod(equipPeriod);
					Shop shop = shopMgr.getShopByCode(currentUser.getShopRoot().getShopCode());
					equipLend.setShop(shop);
					equipLend.setStatus(statusRecord);
					equipLend.setStaff(staff);
					//ghi chu
					equipLend.setNote(note);
					equipLend = equipProposalBorrowMgr.createEquipLendWithListDetail(equipLend, lstEquipLendDetails);
					result.put("id", equipLend.getEquipLendId());
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("equip.proposal.update.not.detail"));
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.updateRecord()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Lay chi tiet bien ban
	 * 
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	public String getListDetailInRecord() {
		actionStartTime = DateUtil.now();
		try {
			result.put("rows", new ArrayList<EquipRecordVO>());
			result.put("total", 0);
			if(id != null) {
				EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
				List<Long> lstId = new ArrayList<Long>();
				lstId.add(id);
				filter.setLstId(lstId);
				List<EquipBorrowVO> lstEquipBorrowVOs = equipProposalBorrowMgr.getListEquipLendDetailVOForExportByFilter(filter);
	
				if (lstEquipBorrowVOs != null && lstEquipBorrowVOs.size() > 0) {
					EquipBorrowVO equipBorrowVO = lstEquipBorrowVOs.get(0);
					if (equipBorrowVO.getLstEquipLendDetailVOs() != null && !equipBorrowVO.getLstEquipLendDetailVOs().isEmpty()) {
						result.put("rows", equipBorrowVO.getLstEquipLendDetailVOs());
						result.put("total", equipBorrowVO.getLstEquipLendDetailVOs().size());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.getListDetailInRecord()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * Lay thong tin: loai, tinh trang thiet bi, tinh
	 * 
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	public String getLstInfo() {
		actionStartTime = DateUtil.now();
		try {
			List<EquipmentVO> lstEquipCategory = equipmentManagerMgr.getListEquipmentCategory(new EquipmentFilter<EquipmentVO>());
			if (lstEquipCategory != null && !lstEquipCategory.isEmpty()) {
				result.put("lstEquipCategory", lstEquipCategory);
			}
			List<ApParam> lstHealthy = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
			if (lstHealthy != null && !lstHealthy.isEmpty()) {
				result.put("lstHealthy", lstHealthy);
			}
			List<AreaVO> lstProvince = areaMgr.getListAreaByType(null, AreaType.PROVINCE.getValue(), ActiveType.RUNNING.getValue());
			if (lstProvince != null && !lstProvince.isEmpty()) {
				result.put("lstProvince", lstProvince);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.getLstInfo()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Lay danh sach dia ban
	 * 
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	public String getListSubArea() throws Exception {
		actionStartTime = DateUtil.now();
		try {
			if (areaId == null || objectType == 0) {
				return JSON;
			}
			lstPrecinct = areaMgr.getListAreaByType(areaId, objectType, ActiveType.RUNNING.getValue());
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.getListSubArea()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Xuat bien ban hop dong giao nhan
	 *  
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	public String exportDeliveryRecord() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			result.put(ERROR, false);
			if (lstIdRecord != null && lstIdRecord.size() > 0) {
				List<FormErrVO> lstError = new ArrayList<FormErrVO>();				
				for(int i=0, sz = lstIdRecord.size(); i<sz; i++){
					FormErrVO formErrVO = new FormErrVO();
					EquipLend equipLend = equipProposalBorrowMgr.getEquipLendById(lstIdRecord.get(i));
					if (equipLend != null) {
						if (!StatusRecordsEquip.APPROVED.getValue().equals(equipLend.getStatus())) {
							formErrVO.setFormCode(equipLend.getCode());
							formErrVO.setFormStatusErr(1);
						} 
						if (StatusDeliveryEquip.EXPORTED.getValue().equals(equipLend.getDeliveryStatus())) {
							formErrVO.setFormCode(equipLend.getCode());
							formErrVO.setDeliveryStatusErr(1);
						}
					}
					if(formErrVO.getFormStatusErr() != null || formErrVO.getDeliveryStatusErr() != null){
						lstError.add(formErrVO);
						lstIdRecord.remove(i);
						sz--;
						i--;
					}
				}
				if(lstIdRecord.size() > 0){
					EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
					filter.setLstId(lstIdRecord);
					filter.setRecordStatus(StatusRecordsEquip.APPROVED.getValue());
					filter.setUserName(currentUser.getStaffRoot().getStaffCode());
					equipmentManagerMgr.exportListEquipDeliveryRecordExportByEquipLend(filter);
				}
				if (lstError.size() > 0) {
					result.put("lstRecordError", lstError);
				}				
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.exportDeliveryRecord()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Lay chi tiet bien ban hop dong giao nhan giao nhan
	 * 
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	public String getListDeliveryExportAgain() {
		actionStartTime = DateUtil.now();
		try {
			result.put(ERROR, false);
			result.put("rows", new ArrayList<EquipRecordVO>());
			result.put("total", 0);
			if(id != null) {				
				EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentRecordDeliveryVO>();
				equipmentFilter.setCurShopCode(currentUser.getShopRoot().getShopCode());
				equipmentFilter.setEquipLendId(id);
				List<EquipmentRecordDeliveryVO> equipmentRecordDeliverys = equipmentManagerMgr.getListDeliveryRecordVOForEquipLendByFilter(equipmentFilter);
				if (equipmentRecordDeliverys != null && equipmentRecordDeliverys.size() > 0) {
					result.put("total", equipmentRecordDeliverys.size());
					result.put("rows", equipmentRecordDeliverys);
				} else {
					result.put("errMsg", R.getResource("equip.proposal.export.delivery.not.record.cancel"));
					result.put(ERROR, true);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.getListDeliveryExportAgain()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *  
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	public String exportDeliveryRecordAgain() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			result.put(ERROR, false);
			if(id != null && lstCustomerId != null) {
				EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
				List<Long> lstId = new ArrayList<Long>();
				lstId.add(id);
				filter.setLstId(lstId);
				filter.setLstCustomerId(lstCustomerId);
				filter.setRecordStatus(StatusRecordsEquip.APPROVED.getValue());
				filter.setUserName(currentUser.getStaffRoot().getStaffCode());
				equipmentManagerMgr.exportListEquipDeliveryRecordExportByEquipLend(filter);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.exportDeliveryRecordAgain()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * In bien ban de nghi muon
	 * 
	 * @author nhutnn
	 * @since 12/07/2015
	 * @return
	 */
	public String viewPrintEquiplend() {
		actionStartTime = DateUtil.now();
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("errMsg", R.getResource("equip.proposal.export.not.record.export"));
				result.put(ERROR, true);
				return JSON;
			}
			EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
			filter.setLstId(lstIdRecord);
			List<EquipBorrowVO> data = equipProposalBorrowMgr.getListEquipLendDetailVOForExportByFilter(filter);
			if (data == null || data.size() == 0) {
				result.put("errMsg", R.getResource("equip.err.excell"));
				result.put(ERROR, true);
				return JSON;
			}
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			// file in						
			String templatePath = ServletActionContext.getServletContext().getRealPath("/") + File.separatorChar + Configuration.getEquipmentPrintTemplatePath() + File.separator + ConstantManager.TEMPLATE_RECORD_LEND_PRINT
					+ FileExtension.JASPER.getValue();
			File fileReport = new File(templatePath);
			if (!fileReport.exists()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.respon.not.template"));
				return JSON;
			}
			// dat Parameters
			Map<String, Object> parameters = new HashMap<String, Object>();
			/*ApParamEquip nganhHangParam = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.CODE_NGANH_HANG_DOANH_SO.getValue(), ApParamEquipType.DOANH_SO_PHIEU_SUA_CHUA, ActiveType.RUNNING);
			if (nganhHangParam != null && !StringUtil.isNullOrEmpty(nganhHangParam.getValue())) {
				parameters.put("productInfo", "(" + nganhHangParam.getValue() + ")");
			} else {
				parameters.put("productInfo", "");
			}*/
			
			Date sys = DateUtil.now();
			parameters.put("printDate", DateUtil.toDateString(sys, DateUtil.DATETIME_FORMAT_STR));
			JRDataSource dataSource = new JRBeanCollectionDataSource(data);
			// file output
			String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(sys, DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(ConstantManager.TEMPLATE_RECORD_LEND_PRINT + FileExtension.PDF.getValue())).toString());
			String outputPath = Configuration.getStoreRealPath() + outputFile;
			String downloadPath = Configuration.getExportExcelPath() + outputFile;
			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);

			JRAbstractExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
			exporter.exportReport();

			result.put(ERROR, false);
			String downloadUrl = ReportUtils.addReportTokenToDownloadFileUrl(downloadPath, reportToken);
			result.put(REPORT_PATH, downloadUrl);
			// set link
			session.setAttribute("downloadPath", downloadUrl);
			/** ATTT duong dan */
			MemcachedUtils.putValueToMemcached(reportToken, downloadPath, retrieveReportMemcachedTimeout());

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentManagementProposalBorrowAction.viewPrintEquiplend()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	// TODO Khai bai cac thuoc tinh GETTER/SETTER
	public Long getId() {
		return id;
	}

	public Integer getStatusDelivery() {
		return statusDelivery;
	}

	public String getCode() {
		return code;
	}

	public String getShopCode() {
		return shopCode;
	}

	public String getSuperviseCode() {
		return superviseCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setStatusDelivery(Integer statusDelivery) {
		this.statusDelivery = statusDelivery;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public void setSuperviseCode(String superviseCode) {
		this.superviseCode = superviseCode;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}

	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}

	public Integer getStatusRecord() {
		return statusRecord;
	}

	public void setStatusRecord(Integer statusRecord) {
		this.statusRecord = statusRecord;
	}

	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	public String getCurShopCode() {
		return curShopCode;
	}

	public void setCurShopCode(String curShopCode) {
		this.curShopCode = curShopCode;
	}

	public String getLstShop() {
		return lstShop;
	}

	public void setLstShop(String lstShop) {
		this.lstShop = lstShop;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<AreaVO> getLstPrecinct() {
		return lstPrecinct;
	}

	public void setLstPrecinct(List<AreaVO> lstPrecinct) {
		this.lstPrecinct = lstPrecinct;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public String getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(String createFormDate) {
		this.createFormDate = createFormDate;
	}

	public List<EquipLendDetailVO> getLstDetails() {
		return lstDetails;
	}

	public void setLstDetails(List<EquipLendDetailVO> lstDetails) {
		this.lstDetails = lstDetails;
	}

	public String getNganhHangStr() {
		return nganhHangStr;
	}

	public void setNganhHangStr(String nganhHangStr) {
		this.nganhHangStr = nganhHangStr;
	}

	public Integer getFlagRecordStatus() {
		return flagRecordStatus;
	}

	public void setFlagRecordStatus(Integer flagRecordStatus) {
		this.flagRecordStatus = flagRecordStatus;
	}

	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}

	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

	public List<StaffVO> getLstStaffVOs() {
		return lstStaffVOs;
	}

	public void setLstStaffVOs(List<StaffVO> lstStaffVOs) {
		this.lstStaffVOs = lstStaffVOs;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Integer getIsLevelShop() {
		return isLevelShop;
	}

	public void setIsLevelShop(Integer isLevelShop) {
		this.isLevelShop = isLevelShop;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}

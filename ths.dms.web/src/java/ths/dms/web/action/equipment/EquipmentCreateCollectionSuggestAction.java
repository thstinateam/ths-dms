package ths.dms.web.action.equipment;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.vo.EquipmentSuggestEvictionVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.utils.LogUtility;

import ths.dms.core.entities.vo.EquipmentCreateCollectionSuggestVO;

public class EquipmentCreateCollectionSuggestAction extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CustomerMgr customerMgr;
	
	
	
	@Override
	public void prepare() throws Exception {
//		customerMgr = (CustomerMgr) context.getBean("customerMgr");
//		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}

		if (shop != null) {
			shopId = shop.getId();
		}
		return SUCCESS;
	}

//	public String search(){
//		result.put("page", page);
//		result.put("max", max);
//		try{
//		List<EquipmentCreateCollectionSuggestVO> ls = new ArrayList<EquipmentCreateCollectionSuggestVO>();
//		
//		EquipmentCreateCollectionSuggestVO vo1 = new EquipmentCreateCollectionSuggestVO();
//		vo1.setMien("Tủ mát");
//		vo1.setKenh("TT");
//		vo1.setNPP("TL40041 - Tài Lộc");
//		vo1.setMaKhachHang("OCX");
//		vo1.setTenKhachHang("Chị Lý");
//		vo1.setMaThietBi("001");
//		vo1.setSoSeri("123456");
//		vo1.setThoiGian("01/01/2015");
//		vo1.setNguoiDungTen("Nguyễn Văn Hóa");
//		vo1.setDienThoai("0909010234");
//		vo1.setSoNha("01");
//		vo1.setTinh("HCM");
//		vo1.setHuyen("Q.1");
//		vo1.setXa("VNM-VNM");
//		vo1.setDuong("Nguyễn Kim");
//		vo1.setSoLuong("1");
//		vo1.setNhomThietBi("Mới 100%");
//		vo1.setTinhTrang("Mới 100%");
//		vo1.setGiamSat("001 - Nguyễn Văn A");
//		vo1.setDtGiamSat("0123654789");
//		ls.add(vo1);
//		
//		EquipmentCreateCollectionSuggestVO vo2 = new EquipmentCreateCollectionSuggestVO();
//		vo2.setMien("Tủ mát");
//		vo2.setKenh("TT");
//		vo2.setNPP("TL40041 - Tài Lộc");
//		vo2.setMaKhachHang("OR1");
//		vo2.setTenKhachHang("Chị Hải");
//		vo2.setMaThietBi("002");
//		vo2.setSoSeri("123456");
//		vo2.setThoiGian("01/01/2015");
//		vo2.setNguoiDungTen("Trần Thị Hải");
//		vo2.setDienThoai("0900922145");
//		vo2.setSoNha("01");
//		vo2.setTinh("HCM");
//		vo2.setHuyen("Q.1");
//		vo2.setXa("TL40041 - Tài Lộc");
//		vo2.setDuong("Nguyễn Kim");
//		vo2.setSoLuong("1");
//		vo2.setNhomThietBi("Đã qua sử dụng");
//		vo2.setTinhTrang("Đã qua sử dụng");
//		vo2.setGiamSat("001 - Nguyễn Văn A");
//		vo2.setDtGiamSat("0123654789");
//		ls.add(vo2);
//		
//		
//		if (ls != null) {
//			result.put("rows", ls);
//		}
//		}catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
//		return JSON;
//	}
//	
}

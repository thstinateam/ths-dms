package ths.dms.web.action.equipment;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import net.sf.jxls.transformer.XLSTransformer;

import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import sun.misc.BASE64Encoder;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.TreeGridNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.ImageUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.Unicode2English;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;
import viettel.passport.client.ShopToken;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.common.utils.StringFormatter;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipStatisticCustomer;
import ths.dms.core.entities.EquipStatisticGroup;
import ths.dms.core.entities.EquipStatisticRecDtl;
import ths.dms.core.entities.EquipStatisticRecord;
import ths.dms.core.entities.EquipStatisticStaff;
import ths.dms.core.entities.EquipStatisticUnit;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipObjectType;
import ths.dms.core.entities.enumtype.EquipStatisticStatus;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUnitTypeEnum;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentStatisticRecordStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.ShopDecentralizationSTT;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.EquipRecordShopVO;
import ths.dms.core.entities.vo.EquipStatisticCustomerVO;
import ths.dms.core.entities.vo.EquipStatisticRecordDetailVO;
import ths.dms.core.entities.vo.EquipmentManagerVO;
import ths.dms.core.entities.vo.EquipmentRecordDetailVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;
//import ths.dms.core.common.utils.StringFormatter;
//import ths.dms.core.entities.enumtype.ShopDecentralizationSTT;

/**
 * kiem ke thiet bi
 * 
 * @author phut
 * 
 */
public class EquipStatisticCheckingAction extends AbstractAction {
	/** field serialVersionUID field long */
	private static final long serialVersionUID = 1L;
	private static final int EQUIPGROUP = 1;
	private static final int SHELF = 2;
	private static final Integer KK_TUYEN = 1;
	private static final Integer KK_SOLAN = 2;
//	private static final Integer CTHTTT = 1;
	private static final Integer MAX_LENGTH_CODE = 50;
	private static final Integer MAX_LENGTH_NAME = 200;
	final String DASH = " - ";
	List<EquipStatisticRecord> listRecord;
	List<EquipGroup> listEquipGroup;
	List<Equipment> listEquipment;
	List<EquipmentVO> listEquip;
	/**
	 * list u ke return when search
	 */
	List<Product> listShelf;
	String fromDate;
	String toDate;
	String checkingCode;
	String checkingName;
	Integer status;
	Long id;
	Long recordId;
	Long detailId;
	private Long equipId;
	EquipStatisticRecord instance;
	List<Long> listId;
	private List<TreeGridNode<EquipRecordShopVO>> lstTree;
	String shopSearchCode;
	String shopSearchName;
	List<EquipmentRecordDetailVO> listDetail;
	String customerCode;
	String customerName;
	String deviceCode;
	String seri;
	String dateInfo;
	String dateInWeek;
	Boolean overwrite;
	String promotionCode;
	String equipGroupName;
	private List<Long> lstIdRecord;
	Integer isCheck;
	String staffCode;
	String staffName;
	private Integer resultImg;
	private Integer statusCus;
	private Integer isInspeck;
	// so mat hang
	private String numberProduct;
	// POSM
	private String strPOSM;
	// so mat hang
	private String strNoted;
	private String strLstShopId;
	/**
	 * type of record
	 */
	Integer type;
	/**
	 * checking time/step
	 */
	String quantity;

	/**
	 * checkbox for shop load on customer tab
	 */
	Boolean ckShop;
	/**
	 * checkbox for promotion load on customer tab
	 */
	Boolean ckPromotion;

	/**
	 * delete shop child?
	 */
	Boolean deleteChild;
	/**
	 * list shop
	 */
	List<EquipRecordShopVO> lstShop;

	/**
	 * checking time get from detail
	 */
	Integer soLan;
	/**
	 * equip_statistic_group_id
	 */
	Long equipGroupId;
	/**
	 * equipment code
	 */
	String equipCode;
	/**
	 * equip_stock_id or warehouse_id
	 */
	Long stockId;
	/**
	 * step checking
	 */
	Integer stepCheck;
	/**
	 * checking date
	 */
	String checkDate;
	/**
	 * product_id
	 */
	Long shelfId;

	/**
	 * is first load => return nothing. on second load => get query from DB
	 */
	Boolean isFirstLoad;

	/**
	 * list detail for save
	 */
	List<Long> listDetailId;
	/**
	 * list exists / not checkbox fro save
	 */
	List<Integer> listExists;
	/**
	 * list input quantity
	 */
	List<Integer> listActQty;

	/** file excel import */
	private File excelFile;
	private String excelFileContentType;

	private Integer equipmentStatisticRecordStatus;

	EquipRecordMgr equipRecordMgr;
	EquipmentManagerMgr equipmentManagerMgr;
	ProductMgr productMgr;
	CustomerMgr customerMgr;
	PromotionProgramMgr promotionProgramMgr;
	StockMgr stockMgr;

	@Override
	public void prepare() throws Exception {
		stockMgr = (StockMgr) context.getBean("stockMgr");
		equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}

		if (shop != null) {
			shopId = shop.getId();
		}
		return SUCCESS;
	}

	/**
	 * tim kiem
	 * 
	 * @author phuongvm
	 * @return
	 * 
	 * @author hunglm16
	 * @since May 31,2015
	 * @description Ra soat va hotfix
	 */
	public String search() {
		try {
			KPaging<EquipStatisticRecord> kPaging = new KPaging<EquipStatisticRecord>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.toDate(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.toDate(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (!StringUtil.isNullOrEmpty(strLstShopId)) {
				String[] lstStr = strLstShopId.split(",");
				listId = new ArrayList<Long>();
				for (int i = 0; i < lstStr.length; i++) {
					Long value = Long.valueOf(lstStr[i]);
					listId.add(value);
				}
			} else {
				listId = new ArrayList<Long>();
				listId.add(currentUser.getShopRoot().getShopId());
			}
			EquipRecordFilter<EquipStatisticRecord> filter = new EquipRecordFilter<EquipStatisticRecord>();
			filter.setkPaging(kPaging);
			filter.setFromDate(fDate);
			filter.setToDate(tDate);
			filter.setCode(checkingCode);
			filter.setName(checkingName);
			filter.setStatus(status);
			filter.setLstShopId(listId);
			filter.setStrListShopId(getStrListShopId());
			ObjectVO<EquipStatisticRecord> vo = equipRecordMgr.getListEquipStatisticRecord(filter);
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipStatisticRecord>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.search"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * edit hoac create bien ban kiem ke
	 * 
	 * @return
	 * @throws Exception
	 */
	public String edit() throws Exception {
		try {
			if (id != null && id > 0) {
				instance = equipRecordMgr.getEquipStatisticRecordById(id);
				if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
					return PAGE_NOT_PERMISSION;
				}
				if (instance.getFromDate() != null) {
					fromDate = DateUtil.toDateString(instance.getFromDate());
				}
				if (instance.getToDate() != null) {
					toDate = DateUtil.toDateString(instance.getToDate());
				}
				type = instance.getType();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.edit"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * load detail for record of group and checking step
	 * 
	 * @author phut
	 */
	public String inputGroupSoLanLoad() {
		if (isFirstLoad) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			return SUCCESS;
		}
		try {
			KPaging<StatisticCheckingVO> kPaging = new KPaging<StatisticCheckingVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroupSoLan(kPaging, id, shopId, equipGroupId, equipCode, seri, stockId, stepCheck, getLogInfoVO());
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.inputGroupSoLanLoad"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * load detail for record of group and checking routing
	 * 
	 * @author phut
	 */
	public String inputGroupTuyenLoad() {
		if (isFirstLoad) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			return SUCCESS;
		}
		try {
			KPaging<StatisticCheckingVO> kPaging = new KPaging<StatisticCheckingVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date __checkDate = null;
			if (!StringUtil.isNullOrEmpty(checkDate)) {
				__checkDate = DateUtil.parse(checkDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroupTuyen(kPaging, id, shopId, equipGroupId, equipCode, seri, stockId, __checkDate, getLogInfoVO());
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.inputGroupTuyenLoad"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * load detail for record of shelf and checking step
	 * 
	 * @author phut
	 */
	public String inputShelfSoLanLoad() {
		if (isFirstLoad) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			return SUCCESS;
		}
		try {
			KPaging<StatisticCheckingVO> kPaging = new KPaging<StatisticCheckingVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailShelfSoLan(kPaging, id, shopId, shelfId, stockId, stepCheck, getLogInfoVO());
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.inputShelfSoLanLoad"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * load detail for record of shelf and checking routing
	 * 
	 * @author phut
	 */
	public String inputShelfTuyenLoad() {
		if (isFirstLoad) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			return SUCCESS;
		}
		try {
			KPaging<StatisticCheckingVO> kPaging = new KPaging<StatisticCheckingVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date __checkDate = null;
			if (!StringUtil.isNullOrEmpty(checkDate)) {
				__checkDate = DateUtil.parse(checkDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailShelfTuyen(kPaging, id, shopId, shelfId, stockId, __checkDate, getLogInfoVO());
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.inputShelfTuyenLoad"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * return page input
	 * 
	 * @author phut
	 */
	public String input() throws Exception {
		try {
			instance = equipRecordMgr.getEquipStatisticRecordById(id);
			if (instance == null || EquipmentStatisticRecordStatus.RUNNING.getValue() != instance.getRecordStatus()) {
				return PAGE_NOT_PERMISSION;
			}
			listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(id);
			listEquipment = equipRecordMgr.getListEquipByRecordId(id);
			listShelf = equipRecordMgr.getListShelfByRecordId(id);
			List<EquipRecordShopVO> __lstShop = equipRecordMgr.getListShopOfRecord(id, null, null, null);
			lstShop = new ArrayList<EquipRecordShopVO>();
			for (int i = 0; i < __lstShop.size(); i++) {
				if (__lstShop.get(i).getIsCheck() == 1) {
					lstShop.add(__lstShop.get(i));
				}
			}
			fromDate = instance.getFromDateStr();
			toDate = instance.getToDateStr();
			if ((!listEquipGroup.isEmpty() || !listEquipment.isEmpty()) && instance.getType() == 2) {//nhom sp + so lan
				soLan = Integer.parseInt(instance.getQuantity());
				return "thietbi-solan";
			} else if ((!listEquipGroup.isEmpty() || !listEquipment.isEmpty()) && instance.getType() == 1) {//nhom sp + tuyen
				return "thietbi-tuyen";
			} else if (!listShelf.isEmpty() && instance.getType() == 2) {//u ke + so lan
				soLan = Integer.parseInt(instance.getQuantity());
				return "uke-solan";
			} else if (!listShelf.isEmpty() && instance.getType() == 1) {//u ke + tuyen
				return "uke-tuyen";
			} else {
				return PAGE_NOT_PERMISSION;
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.input"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return PAGE_NOT_PERMISSION;
	}

	/**
	 * save bien ban kiem ke
	 * 
	 * @author phut
	 */
	public String save() {
		resetToken(result);
		try {
			Shop shop = null;
			if (!EquipmentStatisticRecordStatus.DRAFT.getValue().equals(status) && !EquipmentStatisticRecordStatus.RUNNING.getValue().equals(status) && !EquipmentStatisticRecordStatus.DELETED.getValue().equals(status)
					&& !EquipmentStatisticRecordStatus.DONE.getValue().equals(status)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.status.invalid"));
				return SUCCESS;
			}
			if (id != null && id > 0) {
				instance = equipRecordMgr.getEquipStatisticRecordById(id);
				if (instance == null || instance.getShop() == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.not.date", R.getResource("equipment.statistic.record")));
					return SUCCESS;
				}

				if (!EquipmentStatisticRecordStatus.DRAFT.getValue().equals(instance.getRecordStatus()) && !EquipmentStatisticRecordStatus.RUNNING.getValue().equals(instance.getRecordStatus())) {
					return PAGE_NOT_FOUND;
				}

				if (EquipmentStatisticRecordStatus.DRAFT.getValue().equals(instance.getRecordStatus())) {
					Date tDate = null;
					if (!StringUtil.isNullOrEmpty(toDate)) {
						tDate = DateUtil.toDate(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
						if (tDate != null) {
							String checkMessage = ValidateUtil.getErrorMsgForInvalidToDate(instance.getFromDate(), tDate);
							if (!StringUtil.isNullOrEmpty(checkMessage)) {
								result.put(ERROR, true);
								result.put("errMsg", checkMessage);
								return SUCCESS;
							}
							instance.setToDate(tDate);
						}
					}
					//instance.setType(type);
					errMsg = ValidateUtil.validateField(checkingName, "equipment.name.statistic", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
					if (StringUtil.isNullOrEmpty(errMsg)) {
						checkingName = checkingName.trim();
						instance.setName(checkingName);
						instance.setNameText(Unicode2English.codau2khongdau(checkingName));
					} else {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return SUCCESS;
					}
					if (EquipmentStatisticRecordStatus.RUNNING.getValue().equals(status)) {
						// lay danh sach nhom
						List<EquipGroup> lstEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(id);
//						List<Product> listShelf = equipRecordMgr.getListShelfByRecordId(id);
						List<Equipment> lstEquip = equipRecordMgr.getListEquipByRecordId(id);
						List<EquipStatisticUnit> lstUnit = equipRecordMgr.getListStatisticUnitChild(id, null);
						if (lstUnit.isEmpty()) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.not.have.group.or.shop"));
							return SUCCESS;
						}
//						if (instance.getTypeCustomer() != null && instance.getTypeCustomer() == CTHTTT) {//ct httm
//							if (listShelf.isEmpty()) {
//								result.put(ERROR, true);
//								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.customer.not.has.shelf"));
//								return SUCCESS;
//							}
//							List<Customer> listCustomer = equipRecordMgr.getListCustomerBuTCHTTM(instance.getId());
//							if (listCustomer == null || listCustomer.isEmpty()) {
//								result.put(ERROR, false);
//								result.put("confirmCus", true);
//								return SUCCESS;
//							}
//						}

//						if (!lstEquipGroup.isEmpty() && !listShelf.isEmpty()) {
//							result.put(ERROR, true);
//							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.can.not.have.group.and.shelf"));
//							return SUCCESS;
//						}
						if (!lstEquipGroup.isEmpty() && !lstEquip.isEmpty()) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.can.not.have.group.and.shelf.equipment"));
							return SUCCESS;
						}
//						if (listShelf != null && !listShelf.isEmpty() && StringUtil.isNullOrEmpty(instance.getPromotionCode())) {
//							result.put(ERROR, true);
//							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.have.program"));
//							return SUCCESS;
//						}
						// lay danh sach don vi thuoc bien ban kiem ke
						//					List<ShopObjectType> lstShopObjectType = new ArrayList<ShopObjectType>();
						//					lstShopObjectType.add(ShopObjectType.NPP);
						//					lstShopObjectType.add(ShopObjectType.NPP_KA);
						//					List<EquipRecordShopVO> lstShop = equipRecordMgr.getListShopOfRecord(id, lstShopObjectType, null, null);
						if (type != null && (type.equals(EquipObjectType.GROUP.getValue()) && (lstEquipGroup == null || lstEquipGroup.size() == 0)) 
								&& (type.equals(EquipObjectType.EQUIP.getValue()) && (lstEquip == null || lstEquip.size() == 0))) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.not.have.group.or.shelf"));
							return SUCCESS;
						}
						//					if (lstShop == null || lstShop.size() == 0) {
						//						result.put(ERROR, true);
						//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.not.have.group.or.shop"));
						//						return SUCCESS;
						//					}
//						if (!listShelf.isEmpty()) { // u ke
//							//chay tien trinh tong hop cho u ke
//							equipRecordMgr.runProcessForEquipShelfTotal(instance.getId(), null, null);
//						} else { //nhom thiet bi hoac thiet bi
//							equipRecordMgr.deleteEquipStatisticStaffNotBelong(instance.getId());
//						}
						equipRecordMgr.deleteEquipStatisticStaffNotBelong(instance.getId());
					}
				} else if (EquipmentStatisticRecordStatus.RUNNING.getValue().equals(instance.getRecordStatus())) {
					if (!EquipmentStatisticRecordStatus.RUNNING.getValue().equals(status) && !EquipmentStatisticRecordStatus.DONE.getValue().equals(status)) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("common.status.invalid"));
						return SUCCESS;
					}
				}
				equipmentManagerMgr.updateEquipStatisticRecord(instance, status, type, getLogInfoVO());
				result.put("instance", instance);
			} else {
				if (currentUser.getShopRoot() != null) {
					shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				}
				if (shop == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.status.invalid"));
					return SUCCESS;
				}
//				EquipPeriod equipPriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPriod == null) {
//					result.put(ERROR, true);
//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.create.record.statistic.not.in.period"));
//					return SUCCESS;
//				}
				if (!EquipmentStatisticRecordStatus.DRAFT.getValue().equals(status)) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.status.invalid"));
					return SUCCESS;
				}
				errMsg = ValidateUtil.validateField(checkingCode, "equipment.code.statistic", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				errMsg += ValidateUtil.validateField(checkingName, "equipment.name.statistic", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (fDate == null) {
					errMsg += R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg) && tDate == null) {
					errMsg += R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					errMsg += R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return SUCCESS;
				}
				checkingCode = checkingCode.trim();
				checkingName = checkingName.trim();
				if (equipRecordMgr.checkEquipStatisticRecordByCode(checkingCode)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.exist.code", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.code.statistic")));
					return SUCCESS;
				}
				instance = new EquipStatisticRecord();
				instance.setCode(checkingCode.toUpperCase());
				instance.setName(checkingName);
				instance.setNameText(Unicode2English.codau2khongdau(checkingName));
				instance.setTypeCustomer(0);
				instance.setShop(shop);
				String checkMessage = ValidateUtil.getErrorMsgForInvalidToDate(fDate, tDate);
				if (!StringUtil.isNullOrEmpty(checkMessage)) {
					result.put(ERROR, true);
					result.put("errMsg", checkMessage);
					return SUCCESS;
				}
				instance.setFromDate(fDate);
				instance.setToDate(tDate);
				instance.setType(type);
				instance.setRecordStatus(EquipmentStatisticRecordStatus.DRAFT.getValue());
//				instance.setEquipPriod(equipPriod);
				instance = equipRecordMgr.createEquipStatisticRecord(instance, getLogInfoVO());

				//them don vi tham gia
				List<EquipStatisticUnit> lst = new ArrayList<EquipStatisticUnit>();
				EquipStatisticUnit equipSattisticUnit = null;
				equipSattisticUnit = new EquipStatisticUnit();
				equipSattisticUnit.setEquipStatisticRecord(instance);
				equipSattisticUnit.setUnitId(shop.getId());
				if (shop.getType() != null && shop.getType().getSpecificType() != null) {
					if (ShopSpecificType.NPP.getValue().equals(shop.getType().getSpecificType().getValue())) {
						equipSattisticUnit.setUnitType(EquipUnitTypeEnum.NPP.getValue());
					} else if (ShopSpecificType.NPP_MT.getValue().equals(shop.getType().getSpecificType().getValue())) {
						equipSattisticUnit.setUnitType(EquipUnitTypeEnum.MIEN.getValue());
					}
				}
				lst.add(equipSattisticUnit);
				equipRecordMgr.createListEquipStatisticUnit(lst, getLogInfoVO());

				result.put("instance", instance);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.save"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * Cap nhat bien ban kiem ke sang hoat dong ma chap nhan ma cthtm khong co
	 * khach hang
	 * 
	 * @author phuongvm
	 * @since 18/05/2015
	 * @return
	 */
	public String saveEx() {
		resetToken(result);
		try {
			if (id != null && id > 0) {
				instance = equipRecordMgr.getEquipStatisticRecordById(id);

				if (instance == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record")));
					return SUCCESS;
				}

				if (EquipmentStatisticRecordStatus.DRAFT.getValue().equals(instance.getRecordStatus()) && EquipmentStatisticRecordStatus.RUNNING.getValue().equals(status)) {

					// lay danh sach nhom
					List<EquipGroup> lstEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(id);
					List<Product> listShelf = equipRecordMgr.getListShelfByRecordId(id);
					List<Equipment> lstEquip = equipRecordMgr.getListEquipByRecordId(id);
					List<EquipStatisticUnit> lstUnit = equipRecordMgr.getListStatisticUnitChild(id, null);
					if (lstUnit.isEmpty()) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.not.have.group.or.shop"));
						return SUCCESS;
					}
					if (instance.getTypeCustomer() != null && instance.getTypeCustomer() == 1) {//ct httm
						if (listShelf.isEmpty()) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.customer.not.has.shelf"));
							return SUCCESS;
						}
					}

					if (!lstEquipGroup.isEmpty() && !listShelf.isEmpty()) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.can.not.have.group.and.shelf"));
						return SUCCESS;
					}
					if (!lstEquipGroup.isEmpty() && !lstEquip.isEmpty()) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.can.not.have.group.and.shelf.equipment"));
						return SUCCESS;
					}
					// lay danh sach don vi thuoc bien ban kiem ke
					//					List<ShopObjectType> lstShopObjectType = new ArrayList<ShopObjectType>();
					//					lstShopObjectType.add(ShopObjectType.NPP);
					//					lstShopObjectType.add(ShopObjectType.NPP_KA);
					//					List<EquipRecordShopVO> lstShop = equipRecordMgr.getListShopOfRecord(id, lstShopObjectType, null, null);
					if ((lstEquipGroup == null || lstEquipGroup.size() == 0) && (listShelf == null || listShelf.size() == 0) && (lstEquip == null || lstEquip.size() == 0)) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.not.have.group.or.shelf"));
						return SUCCESS;
					}
					//					if (lstShop == null || lstShop.size() == 0) {
					//						result.put(ERROR, true);
					//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.not.have.group.or.shop"));
					//						return SUCCESS;
					//					}
					if (!listShelf.isEmpty()) { // u ke
						//chay tien trinh tong hop cho u ke
						equipRecordMgr.runProcessForEquipShelfTotal(instance.getId(), null, null);
					} else { //nhom thiet bi hoac thiet bi
						equipRecordMgr.deleteEquipStatisticStaffNotBelong(instance.getId());
					}
				}

				Date fDate = null;
				Date tDate = null;
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					fDate = DateUtil.toDate(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					tDate = DateUtil.toDate(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				instance.setName(checkingName);
				instance.setNameText(Unicode2English.codau2khongdau(checkingName));
				instance.setFromDate(fDate);
				instance.setToDate(tDate);
				equipmentManagerMgr.updateEquipStatisticRecord(instance, status, type, getLogInfoVO());
				result.put("instance", instance);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.saveEx"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * save of grid checking group
	 * 
	 * @author phut
	 */
	public String saveGroupCheking() {
		resetToken(result);
		try {
			instance = equipRecordMgr.getEquipStatisticRecordById(id);

			if (instance == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record")));
				return SUCCESS;
			}

			if (!EquipmentStatisticRecordStatus.RUNNING.getValue().equals(instance.getRecordStatus())) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.not.have.group.or.shop.err.running"));
				return SUCCESS;
			}
			if (!listDetailId.isEmpty() && !listExists.isEmpty() && listDetailId.size() == listExists.size()) {
				equipRecordMgr.saveGroupCheking(listDetailId, listExists, getLogInfoVO());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.saveGroupCheking"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * save shelf checking
	 * 
	 * @author phut
	 * 
	 * @author update nhutnn, validate so luong
	 * @since update 05/08/2015
	 */
	public String saveShelfCheking() {
		resetToken(result);
		String msg = "";
		try {
			instance = equipRecordMgr.getEquipStatisticRecordById(id);

			if (instance == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record")));
				return SUCCESS;
			}

			if (!EquipmentStatisticRecordStatus.RUNNING.getValue().equals(instance.getRecordStatus())) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.not.have.group.or.shop.err.running"));
				return SUCCESS;
			}
			if (!listDetailId.isEmpty() && !listActQty.isEmpty() && listDetailId.size() == listActQty.size()) {
				for (int i = 0, sz = listActQty.size(); i < sz; i++) {
					if (listActQty.get(i) != null) {
						msg = ValidateUtil.validateField(listActQty.get(i).toString(), "equipment.statistic.input.quantity", 17, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
						if (!StringUtil.isNullOrEmpty(msg)) {
							msg = R.getResource("equip.proposal.update.dong", (i + 1)) + " " + msg;
							break;
						}
					}
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return SUCCESS;
				}
				equipRecordMgr.saveShelfCheking(listDetailId, listActQty, getLogInfoVO());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.saveShelfCheking"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * Kiem tra thiet bi trong nhom co thuoc don vi cua bien ban kiem ke khong
	 * 
	 * @author nhutnn
	 * @since 29/01/2015
	 */
	//	private String checkEquipInGroup(EquipGroup eqGroup, List<EquipRecordShopVO> lstShop) throws BusinessException {
	//		// check nhom thiet bi
	//		EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
	//		filter.setEquipGroupId(eqGroup.getId());
	//		filter.setStockType(EquipStockTotalType.KHO.getValue());
	//		filter.setStatus(StatusType.ACTIVE.getValue());
	//		filter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
	//		filter.setStaffRoot(getCurrentUser().getStaffRoot().getStaffId());
	//		// lay danh sach thiet bi trong mot nhom
	//		ObjectVO<EquipmentVO> equipVO = equipmentManagerMgr.getListEquipmentByFilter(filter);
	//
	//		String errMsg = "";
	//		// kiem tra thiet bi co thuoc don vi kiem ke hay khong
	//		if (lstShop != null && lstShop.size() > 0) {
	//			if (equipVO.getLstObject() != null && equipVO.getLstObject().size() > 0) {
	//				Integer sizeEquip = equipVO.getLstObject().size();
	//				for (int i = 0, size = equipVO.getLstObject().size(); i < size; i++) {
	//					EquipmentVO equipmentVO = equipVO.getLstObject().get(i);
	//					Long shopIdCheck = -1L;
	//					if (equipmentVO.getShopId() != null) {
	//						shopIdCheck = equipmentVO.getShopId();
	//					}
	//					//					if(EquipStockTotalType.KHO_KH.getValue().equals(equipmentVO.getStockType())){
	//					//						Customer cus = customerMgr.getCustomerById(shopIdCheck);
	//					//						if(cus!=null){
	//					//							shopIdCheck = cus.getShop().getId();
	//					//						}
	//					//					}
	//					for (int j = 0, sizej = lstShop.size(); j < sizej; j++) {
	//						if ((EquipUsageStatus.IS_USED.getValue().equals(equipmentVO.getUsageStatus()) || EquipUsageStatus.SHOWING_REPAIR.getValue().equals(equipmentVO.getUsageStatus())) && shopIdCheck.equals(lstShop.get(j).getId())) {
	//							sizeEquip--;
	//							break;
	//						}
	//					}
	//				}
	//				if (sizeEquip == equipVO.getLstObject().size()) {
	//					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.equip.in.group.not.in.stock.customer", eqGroup.getCode());
	//				}
	//			} else {
	//				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.not.equip.in.group", eqGroup.getCode());
	//			}
	//		} else {
	//			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.not.shop.in.record");
	//		}
	//		return errMsg;
	//	}

	/**
	 * list shop stock
	 * 
	 * @author phut
	 */
	public String listShopStock() {
		try {
			List<EquipStock> listStock = equipmentManagerMgr.getEquipStockByShopAndOrdinal(shopId, null);
			result.put("listStock", listStock);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.listShopStock"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * Lay danh sach kho - u ke
	 * 
	 * @author phuongvm
	 * @since 26/05/2015
	 */
	public String listShopStockUke() {
		try {
			//			List<EquipStock> listStock = equipmentManagerMgr.getEquipStockByShopAndOrdinal(shopId, null);
			StockTotalFilter filter = new StockTotalFilter();
			if (shopId != null) {
				filter.setOwnerId(shopId);
			}
			if (id != null) {
				filter.setIdChecking(id);
			}
			List<Warehouse> lstWarehouses = stockMgr.getListWarehouseByFilterChecking(filter);
			result.put("listStock", lstWarehouses);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.listShopStockUke"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * tra ve ajax page tab nhom san pham thiet bi
	 * 
	 * @return
	 */
	public String editGroupProduct() {
		try {
			listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(id);
			instance = equipRecordMgr.getEquipStatisticRecordById(id);
			if (instance != null) {
				isCheck = instance.getType();
			}
		} catch (Exception e) {
			listEquipGroup = new ArrayList<EquipGroup>();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editGroupProduct"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * tra ve danh sach san pham thiet bi
	 * 
	 * @return
	 */
	public String editGroupProductSearch() {
		try {
			listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(id);
		} catch (Exception e) {
			listEquipGroup = new ArrayList<EquipGroup>();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editGroupProductSearch"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * tra ve danh sach thiet bi
	 * 
	 * @author phuongvm
	 * @since 06/07/2015
	 * @return
	 */
	public String editEquipSearch() {
		try {
			KPaging<EquipmentVO> kPaging = new KPaging<EquipmentVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setId(id);
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				filter.setEquipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(equipGroupName)) {
				filter.setEquipGroupName(equipGroupName);
			}
			if (statusCus != null) {
				filter.setStatusRecord(statusCus);
			}
			ObjectVO<EquipmentVO> vo = equipRecordMgr.getListEquipmentByStatisticFilter(filter);
			if (vo != null && vo.getLstObject().size() > 0) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipmentVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editEquipSearch"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * save group equip
	 * 
	 * @return
	 */
	public String editGroupProductSave() {
		resetToken(result);
		try {
			listShelf = equipRecordMgr.getListShelfByRecordId(id);
			if (!listShelf.isEmpty()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.has.shelf"));
				return SUCCESS;
			}
			EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
			if (record == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record")));
				return SUCCESS;
			}
			if (record.getRecordStatus() != 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect"));
				return SUCCESS;
			}

			List<EquipStatisticGroup> listNew = new ArrayList<EquipStatisticGroup>();
			for (Long __id : listId) {
				EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupById(__id);
				if (equipGroup == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined"));
					return SUCCESS;
				}
				if (!ActiveType.RUNNING.getValue().equals(equipGroup.getStatus().getValue())) {
					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.in.active", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.code.group") + equipGroup.getCode());
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return SUCCESS;
				}
				List<ShopObjectType> lstShopObjectType = new ArrayList<ShopObjectType>();
				lstShopObjectType.add(ShopObjectType.NPP);
				lstShopObjectType.add(ShopObjectType.NPP_KA);
				//				List<EquipRecordShopVO> lstShop = equipRecordMgr.getListShopOfRecord(id, lstShopObjectType, null, null);
				//				String errMsg = checkEquipInGroup(equipGroup, lstShop);
				//				if (errMsg.length() > 0) {
				//					result.put(ERROR, true);
				//					result.put("errMsg", errMsg);
				//					return SUCCESS;
				//				}
				EquipStatisticGroup __new = new EquipStatisticGroup();
				__new.setEquipStatisticRecord(record);
				__new.setObjectId(equipGroup.getId());
				__new.setObjectType(EquipObjectType.GROUP);
				listNew.add(__new);
			}

			equipRecordMgr.createListEquipStatisticGroup(listNew, getLogInfoVO());

			listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(id);
			result.put("listEquipGroup", listEquipGroup);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editGroupProductSave"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * save group equip
	 * 
	 * @return
	 */
	public String editEquipSave() {
		resetToken(result);
		try {
			listShelf = equipRecordMgr.getListShelfByRecordId(id);
			if (!listShelf.isEmpty()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.has.shelf"));
				return SUCCESS;
			}
			EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
			if (record == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record")));
				return SUCCESS;
			}
			//			if (record.getRecordStatus() != 0) {
			//				result.put(ERROR, true);
			//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect"));
			//				return SUCCESS;
			//			}

			List<EquipStatisticGroup> listNew = new ArrayList<EquipStatisticGroup>();
			for (Long __id : listId) {
				Equipment equipGroup = equipmentManagerMgr.getEquipmentById(__id);
				if (equipGroup == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined.ex"));
					return SUCCESS;
				}
				if (!ActiveType.RUNNING.getValue().equals(equipGroup.getStatus().getValue())) {
					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.in.active", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.code.group") + equipGroup.getCode());
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return SUCCESS;
				}
				EquipStatisticGroup __new = new EquipStatisticGroup();
				__new.setEquipStatisticRecord(record);
				__new.setObjectId(equipGroup.getId());
				__new.setObjectType(EquipObjectType.EQUIP);
				listNew.add(__new);
			}

			equipRecordMgr.createListEquipStatisticGroup(listNew, getLogInfoVO());

			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editEquipSave"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * delete group from record
	 * 
	 * @author phut
	 */
	public String editGroupProductDelete() {
		resetToken(result);
		try {
			equipRecordMgr.deleteEquipStatisticGroup(recordId, id, EquipObjectType.GROUP);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editGroupProductDelete"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * delete group from record
	 * 
	 * @author phuongvm
	 */
	public String editEquipDelete() {
		resetToken(result);
		try {
			equipRecordMgr.deleteEquipStatisticGroup(recordId, id, EquipObjectType.EQUIP);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editEquipDelete"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	String brand;
	Float fromCapacity;
	Float toCapacity;
	String code;
	String name;
	Long equipCategory;

	/**
	 * tra ve danh sach san pham thiet bi
	 * 
	 * @return
	 */
	public String editGroupProductDialog() {
		try {
			KPaging<EquipGroup> kPaging = new KPaging<EquipGroup>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			ObjectVO<EquipGroup> vo = equipRecordMgr.getListEquipGroupExceptInRecord(kPaging, id, code, equipCategory, fromCapacity, toCapacity, brand);
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipGroup>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editGroupProductDialog"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	public String getMetaData() {
		try {
			EquipmentFilter<EquipCategory> categoryFilter = new EquipmentFilter<EquipCategory>();
			categoryFilter.setStatus(1);
			List<EquipCategory> listEquipCategory = equipmentManagerMgr.getListEquipmentCategoryByFilter(categoryFilter);
			result.put("listEquipCategory", listEquipCategory);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.getMetaData"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * tra ve ajax page tab u, ke
	 * 
	 * @return
	 */
	public String editShelfProduct() {
		return SUCCESS;
	}

	/**
	 * tra ve danh sach san pham thiet bi
	 * 
	 * @return
	 */
	public String editShelfProductSearch() {
		try {
			listShelf = equipRecordMgr.getListShelfByRecordId(id);
		} catch (Exception e) {
			listShelf = new ArrayList<Product>();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editShelfProductSearch"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * save u, ke
	 * 
	 * @return
	 */
	public String editShelfProductSave() {
		resetToken(result);
		try {
			listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(id);
			if (!listEquipGroup.isEmpty()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.has.group"));
				return SUCCESS;
			}
			listEquipment = equipRecordMgr.getListEquipByRecordId(id);
			if (!listEquipment.isEmpty()) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.has.group"));
				return SUCCESS;
			}
			EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
			if (record == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record")));
				return SUCCESS;
			}
			if (record.getRecordStatus() != 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect"));
				return SUCCESS;
			}

			List<EquipStatisticGroup> listNew = new ArrayList<EquipStatisticGroup>();
			for (Long __id : listId) {
				Product product = productMgr.getProductById(__id);
				if (product == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.shelf.undefined"));
					return SUCCESS;
				}
				if (!ActiveType.RUNNING.getValue().equals(product.getStatus().getValue())) {
					errMsg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.in.active", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.code.group") + product.getProductCode());
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return SUCCESS;
				}
				List<ShopObjectType> lstShopObjectType = new ArrayList<ShopObjectType>();
				lstShopObjectType.add(ShopObjectType.NPP);
				lstShopObjectType.add(ShopObjectType.NPP_KA);

				EquipStatisticGroup __new = new EquipStatisticGroup();
				__new.setEquipStatisticRecord(record);
				__new.setObjectId(product.getId());
				__new.setObjectType(EquipObjectType.SHELF);
				listNew.add(__new);
			}

			equipRecordMgr.createListEquipStatisticGroup(listNew, getLogInfoVO());

			listShelf = equipRecordMgr.getListShelfByRecordId(id);
			result.put("listShelf", listShelf);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editShelfProductSave"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * delete u, ke
	 * 
	 * @return
	 */
	public String editShelfProductDelete() {
		resetToken(result);
		try {
			equipRecordMgr.deleteEquipStatisticGroup(recordId, id, EquipObjectType.SHELF);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editShelfProductDelete"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * tra ve danh sach san pham
	 * 
	 * @return
	 */
	public String editShelfProductDialog() {
		try {
			KPaging<Product> kPaging = new KPaging<Product>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (!StringUtil.isNullOrEmpty(code)) {
				code = code.trim();
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				name = name.trim();
			}
			ObjectVO<Product> vo = equipRecordMgr.getListShelfExceptInRecord(kPaging, id, code, name);
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipGroup>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editShelfProductDialog"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * tra ve ajax page tab nhom
	 * 
	 * @return
	 */
	public String editShop() {
		try {
			instance = equipRecordMgr.getEquipStatisticRecordById(id);
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipGroup>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editShop"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * tra ve danh sach shop
	 * 
	 * @return
	 */
	public String editShopSearch() {
		try {
			List<EquipRecordShopVO> lst = equipRecordMgr.getListShopOfRecord(recordId, null, shopSearchCode, shopSearchName);

			List<TreeGridNode<EquipRecordShopVO>> tree = new ArrayList<TreeGridNode<EquipRecordShopVO>>();
			if (lst == null || lst.size() == 0) {
				result.put("rows", tree);
				return SUCCESS;
			}

			// Tao cay
			int i = 0, sz = lst.size();
			EquipRecordShopVO vo = null;
			Long shId = currentUser.getShopRoot().getShopId();
			for (i = 0; i < sz; i++) {
				vo = lst.get(i);
				if (shId.equals(vo.getId())) {
					i++;
					break;
				}
			}
			//EquipRecordShopVO vo = lst.get(0);
			TreeGridNode<EquipRecordShopVO> node = new TreeGridNode<EquipRecordShopVO>();
			node.setNodeId(vo.getId().toString());
			node.setAttr(vo);
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
			node.setText(vo.getShopCode() + " - " + vo.getShopName());
			List<TreeGridNode<EquipRecordShopVO>> chidren = new ArrayList<TreeGridNode<EquipRecordShopVO>>();
			node.setChildren(chidren);
			tree.add(node);

			TreeGridNode<EquipRecordShopVO> tmp = null;
			TreeGridNode<EquipRecordShopVO> tmp2 = null;
			for (; i < sz; i++) {
				vo = lst.get(i);

				if (vo.getParentId() != null) {
					tmp2 = getNodeFromTree(tree, vo.getParentId().toString());
					if (tmp2 != null) {
						tmp = new TreeGridNode<EquipRecordShopVO>();
						tmp.setNodeId(vo.getId().toString());
						tmp.setAttr(vo);
						if (0 == vo.getIsNPP()) {
							tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
						} else {
							tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
						}
						tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

						if (tmp2.getChildren() == null) {
							tmp2.setChildren(new ArrayList<TreeGridNode<EquipRecordShopVO>>());
						}
						tmp2.getChildren().add(tmp);
					}
				}
			}

			result.put("rows", tree);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editShopSearch"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	private <T> TreeGridNode<T> getNodeFromTree(List<TreeGridNode<T>> treeTmp, String nodeId) throws Exception {
		if (treeTmp == null) {
			return null;
		}
		TreeGridNode<T> node = null;
		TreeGridNode<T> tmp = null;
		for (int i = 0, sz = treeTmp.size(); i < sz; i++) {
			node = treeTmp.get(i);
			if (node.getNodeId().equals(nodeId)) {
				return node;
			}
			tmp = getNodeFromTree(node.getChildren(), nodeId);
			if (tmp != null) {
				return tmp;
			}
		}
		return null;
	}

	/**
	 * search shop
	 * 
	 * @return
	 * @throws Exception
	 */
	public String searchShopOnDlg() throws Exception {
		try {
			Shop sh = null;
			if (id == null || id == 0) {
				shopId = currentUser.getShopRoot().getShopId();
			} else {
				shopId = id;
			}

			ShopToken shopTk = getShopTockenInOrgAccessById(shopId);
			if (shopTk == null) {
				return SUCCESS;
			}

			sh = shopMgr.getShopById(shopId);
			//			Integer isLevel=2;
			//			
			////			if(ShopObjectType.KA.getValue().equals(sh.getType().getObjectType())
			////					|| ShopObjectType.GT.getValue().equals(sh.getType().getObjectType())
			////					|| ShopObjectType.ST.getValue().equals(sh.getType().getObjectType())){
			////					isLevel = 2;
			////			}else 
			//			if(ShopObjectType.MIEN.getValue().equals(sh.getType().getObjectType())
			//					|| ShopObjectType.MIEN_KA.getValue().equals(sh.getType().getObjectType())
			//					|| ShopObjectType.MIEN_ST.getValue().equals(sh.getType().getObjectType())){
			//					isLevel = 3;
			//			}else if(ShopObjectType.VUNG.getValue().equals(sh.getType().getObjectType())
			//					|| ShopObjectType.VUNG_KA.getValue().equals(sh.getType().getObjectType())){
			//					isLevel = 4;
			//			}
			if (StringUtil.isNullOrEmpty(shopSearchCode) && StringUtil.isNullOrEmpty(shopSearchName) && ShopDecentralizationSTT.NPP.getValue().equals(shopTk.getIsLevel())) {
				shopSearchCode = shopTk.getShopCode();
			}
			List<EquipRecordShopVO> lst = equipRecordMgr.searchShopForRecord(recordId, shopTk.getShopId(), shopSearchCode, shopSearchName, shopTk.getIsLevel());
			lstTree = new ArrayList<TreeGridNode<EquipRecordShopVO>>();
			if (lst == null || lst.size() == 0) {
				return SUCCESS;
			}

			if (id == null || id == 0 || !StringUtil.isNullOrEmpty(shopSearchCode) || !StringUtil.isNullOrEmpty(shopSearchName)) {
				EquipRecordShopVO vo = null;
				int i = 0;
				int sz = lst.size();
				String state = null;

				if (StringUtil.isNullOrEmpty(shopSearchCode) && StringUtil.isNullOrEmpty(shopSearchName)) {
					vo = new EquipRecordShopVO();
					vo.setId(sh.getId());
					vo.setShopCode(sh.getShopCode());
					vo.setShopName(sh.getShopName());
					vo.setIsNPP(0);
					vo.setIsCheck(equipRecordMgr.checkShopInRecordUnit(recordId, sh.getId()));
					state = ConstantManager.JSTREE_STATE_CLOSE;
					i = 0;
				} else {
					vo = lst.get(0);
					state = ConstantManager.JSTREE_STATE_OPEN;
					i = 1;
				}

				TreeGridNode<EquipRecordShopVO> node = new TreeGridNode<EquipRecordShopVO>();
				node.setNodeId(vo.getId().toString());
				node.setAttr(vo);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setText(vo.getShopCode() + " - " + vo.getShopName());
				List<TreeGridNode<EquipRecordShopVO>> chidren = new ArrayList<TreeGridNode<EquipRecordShopVO>>();
				node.setChildren(chidren);
				lstTree.add(node);

				if (lst == null || lst.size() == 0) {
					return SUCCESS;
				}

				// Tao cay			
				TreeGridNode<EquipRecordShopVO> tmp = null;
				TreeGridNode<EquipRecordShopVO> tmp2 = null;
				for (; i < sz; i++) {
					vo = lst.get(i);

					tmp2 = getNodeFromTree(lstTree, vo.getParentId().toString());
					if (tmp2 != null) {
						tmp = new TreeGridNode<EquipRecordShopVO>();
						tmp.setNodeId(vo.getId().toString());
						tmp.setAttr(vo);
						if (0 == vo.getIsNPP()) {
							tmp.setState(state);
						} else {
							tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
						}
						tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

						if (tmp2.getChildren() == null) {
							tmp2.setChildren(new ArrayList<TreeGridNode<EquipRecordShopVO>>());
						}
						tmp2.getChildren().add(tmp);
					}
				}
			} else {
				// Tao cay			
				TreeGridNode<EquipRecordShopVO> tmp = null;
				EquipRecordShopVO vo = null;
				for (int i = 0, sz = lst.size(); i < sz; i++) {
					vo = lst.get(i);

					tmp = new TreeGridNode<EquipRecordShopVO>();
					tmp.setNodeId(vo.getId().toString());
					tmp.setAttr(vo);
					if (0 == vo.getIsNPP()) {
						tmp.setState(ConstantManager.JSTREE_STATE_CLOSE);
						//tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					} else {
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getShopCode() + " - " + vo.getShopName());

					lstTree.add(tmp);
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.searchShopOnDlg"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * add list shop
	 * 
	 * @return
	 * @throws Exception
	 * 
	 * @author hunglm16
	 * @since June 14,2015
	 * @description Ra soat va phan quyen CMS
	 */
	public String addShop() throws Exception {
		resetToken(result);
		result.put(ERROR, true);
		if (id == null || id < 1 || listId == null || listId.isEmpty()) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return SUCCESS;
		}
		try {
			EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
			if (record == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record")));
				return SUCCESS;
			}
			if (record.getRecordStatus() != 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect"));
				return SUCCESS;
			}

			List<EquipStatisticUnit> lst = new ArrayList<EquipStatisticUnit>();
			EquipStatisticUnit equipSattisticUnit = null;
			List<Shop> allShopChildrens = equipRecordMgr.getAllShopByLstId(record.getId(), listId);
			if (allShopChildrens != null) {
				for (Shop shop : allShopChildrens) {
					if (ActiveType.RUNNING != shop.getStatus()) {
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, shop.getShopCode()));
						return SUCCESS;
					}
					equipSattisticUnit = new EquipStatisticUnit();
					equipSattisticUnit.setEquipStatisticRecord(record);
					equipSattisticUnit.setUnitId(shop.getId());
					if (!checkShopInOrgAccessById(shop.getId())) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.permission.error.paramater"));
						return SUCCESS;
					}
					if (shop.getType() != null && shop.getType().getSpecificType() != null) {
						if (ShopSpecificType.NPP.getValue().equals(shop.getType().getSpecificType().getValue())) {
							equipSattisticUnit.setUnitType(EquipUnitTypeEnum.NPP.getValue());
						} else if (ShopSpecificType.NPP_MT.getValue().equals(shop.getType().getSpecificType().getValue())) {
							equipSattisticUnit.setUnitType(EquipUnitTypeEnum.MIEN.getValue());
						}
					}
					lst.add(equipSattisticUnit);
				}
			}
			equipRecordMgr.addShopForEquipStatistic(id, listId, lst, getLogInfoVO());
			
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.addShop"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * delete shop
	 * 
	 * @return
	 * @throws Exception
	 */
	public String deleteShop() {
		resetToken(result);
		result.put(ERROR, true);
		if (id == null || id < 1 || shopId == null || shopId < 1) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
			return SUCCESS;
		}
		try {
			EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
			if (record == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record")));
				return SUCCESS;
			}
			if (record.getRecordStatus() != 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect"));
				return SUCCESS;
			}

			Shop sh = shopMgr.getShopById(shopId);
			if (sh == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.focus.program.shop.code"));
				return SUCCESS;
			}

			equipRecordMgr.deleteEquipStatisticUnit(id, shopId, getLogInfoVO(), deleteChild != null ? deleteChild : false);
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.deleteShop"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * check shop has child
	 * 
	 * @author phut
	 */
	public String checkChildShop() {
		try {
			deleteChild = equipRecordMgr.checkHashChild(id, shopId);
			result.put(ERROR, false);
			result.put("deleteChild", deleteChild);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.checkChildShop"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * return customer tab
	 */
	public String editCustomer() {
		try {
			ckShop = false;
			ckPromotion = false;
			instance = equipRecordMgr.getEquipStatisticRecordById(id);
			if (instance.getTypeCustomer() != null && instance.getTypeCustomer() == 0) {
				ckShop = true;
			} else if (instance.getTypeCustomer() != null && instance.getTypeCustomer() == 1) {
				ckPromotion = true;
				promotionCode = instance.getPromotionCode();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.editCustomer"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * search customer on tab customer
	 * 
	 * @author phut
	 */
	public String searchCustomer() {
		try {
			KPaging<EquipStatisticCustomerVO> kPaging = new KPaging<EquipStatisticCustomerVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<EquipStatisticCustomerVO> vo = equipRecordMgr.searchCustomer4Record(kPaging, recordId, customerCode, customerName, statusCus);
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipStatisticCustomer>());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.searchCustomer"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return SUCCESS;
	}

	/**
	 * import customer
	 * 
	 * @author phut
	 */
	public String importCustomer() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 2);
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
			return SUCCESS;
		}
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				if (id == null || id <= 0) {
					isError = true;
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
					return SUCCESS;
				}
				EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
				if (record == null) {
					message = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
				}
				if (record.getRecordStatus() != 0) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect");
				}
				if (message.length() > 0) {
					isError = true;
					errMsg = message;
					return SUCCESS;
				}

				lstView = new ArrayList<CellBean>();
				List<EquipStatisticCustomer> lst = new ArrayList<EquipStatisticCustomer>();
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						Shop shop = null;
						Customer customer = null;
						boolean isMienMT = false;
						// ** Check trung du lieu cac record */
						message = "";
						message = checkDupRow(lstData, i);
						if (!StringUtil.isNullOrEmpty(message)) {
							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
							continue;
						}

						// ** Ma don vi */
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "ss.traningplan.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								if (message.length() == 0) {
									value = value.trim().toUpperCase();
									shop = shopMgr.getShopByCode(value);
									if (shop == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "ss.traningplan.npp.code");
									} else if (!checkShopInOrgAccessByCode(value)) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "ss.traningplan.npp.code");
										shop = null;
									} else if (!shop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.npp.code");
										shop = null;
									} else {
										if (shop.getType() != null && shop.getType().getSpecificType() != null && ShopSpecificType.NPP_MT.getValue().equals(shop.getType().getSpecificType().getValue())) {
											isMienMT = true;
										} else {
											isMienMT = false;
										}
									}
								}
							}
						}
						/* ma kh */
						if (row.size() > 1) {
							String value = row.get(1);
							String msg = ValidateUtil.validateField(value, "contract.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								if (message.length() == 0) {
									value = value.trim().toUpperCase();
									if (isMienMT) {
										customer = customerMgr.getCustomerByCode(value);
									} else {
										customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), value));
									}
									if (customer == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "contract.customer.code");
									}
									//									else if (!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
									//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "contract.customer.code");
									//										shop = null;
									//									}
								}
							}
						}

						if (StringUtil.isNullOrEmpty(message) && shop != null) {
							if (equipRecordMgr.checkCustomerInEquipStatisCus(record.getId(), customer.getId())) {
								message += R.getResource("equipment.statistic.record.cus.has.group");
								lstFails.add(StringUtil.addFailBean(row, message));
							} else {
								EquipStatisticCustomer entity = new EquipStatisticCustomer();
								entity.setCustomer(customer);
								entity.setEquipStatisticRecord(record);
								entity.setType(2);
								entity.setStatus(0);
								lst.add(entity);
							}
						} else {
							//message += msgEquip;
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				if (!lst.isEmpty()) {
					equipRecordMgr.createListEquipStatisticCustomer(lst, getLogInfoVO());
				}

				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CUSTOMER_CHECK_STATISTIC_LOI);
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.importCustomer"), createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return SUCCESS;
			}
		} else {
			errMsg = R.getResource("customer.display.program.nodata");
			return SUCCESS;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * import nvbh
	 * 
	 * @author phuongvm
	 * @since 15/07/2015
	 * @return
	 */
	public String importStaff() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 2);
		String username = currentUser.getUserName();
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				if (id == null || id <= 0) {
					isError = true;
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
					return SUCCESS;
				}
				EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
				if (record == null) {
					message = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
				}
				if (record.getRecordStatus() != 0) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect");
				}
				if (message.length() > 0) {
					isError = true;
					errMsg = message;
					return SUCCESS;
				}
				Date now = commonMgr.getSysDate();
				lstView = new ArrayList<CellBean>();
				//List<EquipStatisticCustomer> lst = new ArrayList<EquipStatisticCustomer>();
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						Equipment equipment = null;
						Staff staff = null;
						// ** Check trung du lieu cac record */
						message = "";
						message = checkDupRow(lstData, i);
						if (!StringUtil.isNullOrEmpty(message)) {
							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
							continue;
						}

						// ** Ma thiết bị */
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "device.code", MAX_LENGTH_NAME, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim().toUpperCase();
								equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
								if (equipment == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.equipment.code");
								} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
								} else if (EquipUsageStatus.LOST.getValue().equals(equipment.getUsageStatus().getValue()) || EquipUsageStatus.LIQUIDATED.getValue().equals(equipment.getUsageStatus().getValue())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.dangsudung");
								} else if (!equipRecordMgr.checkEquipInUnit(record.getId(), equipment.getId())) {
									message += R.getResource("equipment.statistic.record.has.group.mathietbi.kiemke.err");
								} else {
									EquipmentFilter<EquipmentManagerVO> equipmentFilter = new EquipmentFilter<EquipmentManagerVO>();
									equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
									if (!StringUtil.isNullOrEmpty(equipment.getCode())) {
										equipmentFilter.setCode(equipment.getCode());
									}
									ObjectVO<EquipmentManagerVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentVOByShopInCMSEx(equipmentFilter);
									if (equipmentDeliverys == null || equipmentDeliverys.getLstObject().isEmpty()) {
										message += R.getResource("equipment.statistic.equip.permission");
									}
								}
							}
						}
						/* ma nvbh */
						if (StringUtil.isNullOrEmpty(message)) {
							if (row.size() > 1) {
								String value = row.get(1);
								String msg = ValidateUtil.validateField(value, "ss.traningplan.salestaff.code", MAX_LENGTH_CODE, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									value = value.trim().toUpperCase();
									staff = staffMgr.getStaffByCode(value);
									if (staff != null) {
										if (!ActiveType.RUNNING.equals(staff.getStatus())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "imp.tuyen.clmn.error.maNVBH.stop") + "\n";
										} else if (staff.getStaffType() != null
												&& (staff.getStaffType().getSpecificType() == null || (!StaffSpecificType.STAFF.getValue().equals(staff.getStaffType().getSpecificType().getValue()) && !StaffSpecificType.GSMT.getValue().equals(
														staff.getStaffType().getSpecificType().getValue())))) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "imp.tuyen.clmn.error.maNVBH.nvbh.gsmt.gska");
										} else {
											Long equipShopId = -1L;
											if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
												Customer c = customerMgr.getCustomerById(equipment.getStockId());
												if (c != null && c.getShop() != null) {
													equipShopId = c.getShop().getId();
												}
											} else if (EquipStockTotalType.KHO.equals(equipment.getStockType())) {
												EquipStock equipStock = equipmentManagerMgr.getEquipStockById(equipment.getStockId());
												if (equipStock != null && equipStock.getShop() != null) {
													equipShopId = equipStock.getShop().getId();
												}
											}
											if (!equipShopId.equals(staff.getShop().getId())) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.staff.equip.err.shop") + "\n";
											}
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "aclong.im.staff.code.not.avai") + "\n";
									}
								}
							}
						}

						if (StringUtil.isNullOrEmpty(message)) {
							if (equipRecordMgr.checkStaffInEquipStatisStaff(record.getId(), staff.getId(), equipment.getId())) {
								message += R.getResource("equipment.statistic.record.cus.has.group.err.staff");
								lstFails.add(StringUtil.addFailBean(row, message));
							} else {
								EquipStatisticStaff entity = new EquipStatisticStaff();
								entity.setStaff(staff);
								entity.setEquipment(equipment);
								//luu them stock_id, stock_code
								entity.setStockId(equipment.getStockId());
								entity.setStockType(equipment.getStockType());
								entity.setEquipStatisticRecord(record);
								entity.setCreateDate(now);
								entity.setCreateUser(username);
								entity = equipRecordMgr.createEquipStatisticStaff(entity);
							}
						} else {
							// message += msgEquip;
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}

				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_STAFF_CHECK_STATISTIC_LOI);
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.importStaff"), createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return SUCCESS;
			}
		} else {
			errMsg = R.getResource("customer.display.program.nodata");
			return SUCCESS;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * export customer
	 * 
	 * @author phut
	 * @return
	 */
	public String exportCustomer() {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			List<EquipStatisticCustomerVO> listExport = null;
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			EquipStatisticRecord instance = equipRecordMgr.getEquipStatisticRecordById(recordId);
			if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.date", R.getResource("common.invalid.record")));
				return JSON;
			}
			listExport = equipRecordMgr.searchCustomer4Record(null, recordId, customerCode, customerName, statusCus).getLstObject();
			if (listExport != null && !listExport.isEmpty()) {
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("rows", listExport);
				String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
				String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_CUSTOMER_RECORD_EXPORT;
				templateFileName = templateFileName.replace('/', File.separatorChar);
				String outputName = ConstantManager.EQUIP_CUSTOMER_RECORD_EXPORT + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
				InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				inputStream.close();
				OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				os.close();
				result.put(ERROR, false);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put("path", outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.err.excell"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportCustomer"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
				}
			}
		}
		System.gc();
		return SUCCESS;
	}

	/**
	 * export equip
	 * 
	 * @author phuongvm
	 * @since 08/07/2015
	 * @return
	 */
	public String exportEquip() {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			List<EquipmentVO> listExport = null;
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			EquipStatisticRecord instance = equipRecordMgr.getEquipStatisticRecordById(id);
			if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.date", R.getResource("common.invalid.record")));
				return JSON;
			}
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setId(id);
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				filter.setEquipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(equipGroupName)) {
				filter.setEquipGroupName(equipGroupName);
			}
			listExport = equipRecordMgr.getListEquipmentByStatisticFilter(filter).getLstObject();
			if (listExport != null && !listExport.isEmpty()) {
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("rows", listExport);
				String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
				String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_RECORD_EXPORT;
				templateFileName = templateFileName.replace('/', File.separatorChar);
				String outputName = ConstantManager.EQUIP_EQUIP_RECORD_EXPORT + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
				InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				inputStream.close();
				OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				os.close();
				result.put(ERROR, false);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put("path", outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.err.excell"));
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportEquip"), createLogErrorStandard(actionStartTime));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
				}
			}
		}
		System.gc();
		return SUCCESS;
	}

	/**
	 * export staff
	 * 
	 * @author phuongvm
	 * @since 20/07/2015
	 * @return
	 */
	public String exportStaff() {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		OutputStream os = null;
		try {
			List<EquipmentVO> listExport = null;
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			EquipStatisticRecord instance = equipRecordMgr.getEquipStatisticRecordById(id);
			if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.date", R.getResource("common.invalid.record")));
				return JSON;
			}
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setId(id);
			filter.setShopRoot(currentUser.getShopRoot().getShopId() != null ? currentUser.getShopRoot().getShopId() : -1L);
			List<EquipmentVO> vo = equipRecordMgr.getListEquipmentStaffByStatisticFilter(filter);
			if (vo != null && vo.size() > 0) {
				listExport = vo;
			}
			if (listExport != null && !listExport.isEmpty()) {
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("rows", listExport);
				String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
				String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_STAFF_RECORD_EXPORT;
				templateFileName = templateFileName.replace('/', File.separatorChar);
				String outputName = ConstantManager.EQUIP_EQUIP_STAFF_RECORD_EXPORT + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
				InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				inputStream.close();
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				result.put(ERROR, false);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put("path", outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.err.excell"));
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportStaff"), createLogErrorStandard(actionStartTime));
		} finally {
			if (os != null) {
				try {
					os.flush();
					os.close();
				} catch (IOException e) {
					LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportStaff"), createLogErrorStandard(actionStartTime));
				}
			}
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
				}
			}
		}
		return SUCCESS;
	}

	/**
	 * delete customer
	 * 
	 * @author phut
	 * @return
	 */
	public String deleteCustomer() {
		resetToken(result);
		try {
			if (id != null) {
				equipRecordMgr.deleteCustomer(id);
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.deleteCustomer"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * xoa tat ca khach hang trong bang equip_statistic_customer co status = 0
	 * 
	 * @author phuongvm
	 * @since 20/05/2015
	 */
	public String deleteAllCustomer() {
		resetToken(result);
		try {
			if (recordId != null) {
				equipRecordMgr.deleteAllCustomer(recordId);
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.deleteAllCustomer"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * save on tab customer
	 * 
	 * @author phut
	 * @return
	 */
	public String saveAndLoadCustomer() {
		final String SPACE = " ";
		resetToken(result);
		try {
			if (ckShop != null && ckShop) {//load theo shop
				listShelf = equipRecordMgr.getListShelfByRecordId(recordId);
				if (listShelf != null && !listShelf.isEmpty()) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("equipment.statistic.record.has.shelf") + SPACE + R.getResource("equipment.statistic.record.has.shelf.get.customer.in.program"));
					return SUCCESS;
				}
				instance = equipRecordMgr.getEquipStatisticRecordById(recordId);
				instance.setTypeCustomer(0);
				equipmentManagerMgr.updateEquipStatisticRecord(instance, instance.getRecordStatus(), null, getLogInfoVO());
			} else if (ckPromotion != null && ckPromotion) {//load theo CT HTTM
				listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(recordId);
				listEquipment = equipRecordMgr.getListEquipByRecordId(recordId);
				if ((listEquipGroup != null && !listEquipGroup.isEmpty()) || (listEquipment != null && !listEquipment.isEmpty())) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("equipment.statistic.record.has.group") + SPACE + R.getResource("equipment.statistic.record.has.shelf.get.customer.in.shop"));
					return SUCCESS;
				}

				instance = equipRecordMgr.getEquipStatisticRecordById(recordId);
				instance.setTypeCustomer(1);
				if (!StringUtil.isNullOrEmpty(promotionCode)) {
					promotionCode = promotionCode.toUpperCase().trim();
				}
				PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramByCode(promotionCode);
				if (promotionProgram == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.group.not.exists"));
					return SUCCESS;
				}
				//				if (promotionProgram.getType().startsWith("ZV") && (promotionProgram.getStatus().equals(ActiveType.RUNNING) || promotionProgram.getStatus().equals(ActiveType.STOPPED))) {
				//					result.put(ERROR, true);
				//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.group.not.exists"));
				//					return SUCCESS;
				//				}
				listShelf = equipRecordMgr.getListShelfByRecordId(recordId);
				if (listShelf == null || listShelf.isEmpty()) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.customer.not.has.shelf"));
					return SUCCESS;
				}
				instance.setPromotionCode(promotionCode);
				equipmentManagerMgr.updateEquipStatisticRecord(instance, instance.getRecordStatus(), null, getLogInfoVO());
			}
			result.put(ERROR, false);

		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.saveAndLoadCustomer"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * return page statistic of record
	 * 
	 * @author phut
	 * @return
	 */
	public String pageListStatistic() {
		try {
			instance = equipRecordMgr.getEquipStatisticRecordById(id);
			if (instance == null) {
				return PAGE_NOT_PERMISSION;
			}
			listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(id);
			listEquipment = equipRecordMgr.getListEquipByRecordId(id);
//			listShelf = equipRecordMgr.getListShelfByRecordId(id);
			return "thietbi";

		} catch (BusinessException e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.pageListStatistic"), createLogErrorStandard(actionStartTime));
		}
		return PAGE_NOT_PERMISSION;
	}

	//	/**
	//	 * tim kiem kiem ke
	//	 * 
	//	 * @return
	//	 */
	//	public String listStatisticSearch() {
	//		result.put("total", 0);
	//		result.put("rows", new ArrayList<StatisticCheckingVO>());
	//		try {
	//			KPaging<StatisticCheckingVO> kPaging = new KPaging<StatisticCheckingVO>();
	//			kPaging.setPageSize(max);
	//			kPaging.setPage(page - 1);
	//			ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListStatisticCheckingVOByRecordId(kPaging, id, shopCode, customerCode, customerName, deviceCode, seri, status);
	//			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
	//				String t = vo.getLstObject().get(0).getShopCode() + vo.getLstObject().get(0).getShortCode() + vo.getLstObject().get(0).getCustomerName() + vo.getLstObject().get(0).getAddress();
	//				for (int i = 1, size = vo.getLstObject().size(); i < size; i++) {
	//					if (t.equals(vo.getLstObject().get(i).getShopCode() + vo.getLstObject().get(i).getShortCode() + vo.getLstObject().get(i).getCustomerName() + vo.getLstObject().get(i).getAddress())) {
	//						vo.getLstObject().get(i).setAddress(null);
	//						vo.getLstObject().get(i).setCustomerName(null);
	//						vo.getLstObject().get(i).setShopCode(null);
	//						vo.getLstObject().get(i).setShortCode(null);
	//						vo.getLstObject().get(i).setShopName(null);
	//					} else {
	//						t = vo.getLstObject().get(i).getShopCode() + vo.getLstObject().get(i).getShortCode() + vo.getLstObject().get(i).getCustomerName() + vo.getLstObject().get(i).getAddress();
	//					}
	//				}
	//			}
	//			result.put("total", vo.getkPaging().getTotalRows());
	//			result.put("rows", vo.getLstObject());
	//		} catch (Exception e) {
	//			result.put("total", 0);
	//			result.put("rows", new ArrayList<StatisticCheckingVO>());
	//			LogUtility.logError(e, "EquipStatisticCheckingAction.listStatisticSearch - " + e.getMessage());
	//		}
	//		return SUCCESS;
	//	}
	/**
	 * tim kiem kiem ke
	 * 
	 * @author phuongvm
	 * @since 07/05/2015
	 * 
	 * @author hunglm16
	 * @since May 31,2015
	 * @description Ra soat va update
	 */
	public String listStatisticSearch() {
		result.put("total", 0);
		result.put("rows", new ArrayList<StatisticCheckingVO>());
		try {
			KPaging<StatisticCheckingVO> kPaging = new KPaging<StatisticCheckingVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipStatisticFilter filter = new EquipStatisticFilter();
			filter.setRecordId(id);
			filter.setkPaging(kPaging);
			filter.setShopCode(shopCode);
			filter.setCustomerCode(customerCode);
			filter.setStaffCode(staffCode);
			filter.setEquipCode(deviceCode);
			filter.setSeri(seri);
			filter.setStatus(status);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
//			if (stepCheck != null && stepCheck > -1) {
//				filter.setStepCheck(stepCheck);
//			}
//			if (!StringUtil.isNullOrEmpty(dateInWeek)) {
//				filter.setDateInWeek(dateInWeek);
//			}
			ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroup(filter);

			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
				StatisticCheckingVO statisticCheckingVO = null;
				for (int i = 0, size = vo.getLstObject().size(); i < size; i++) {
					statisticCheckingVO = vo.getLstObject().get(i);
					if (statisticCheckingVO.getCustomerCode() != null && statisticCheckingVO.getCustomerName() != null) {
						statisticCheckingVO.setCusCodeAndName(statisticCheckingVO.getCustomerCode() + DASH + statisticCheckingVO.getCustomerName() 
								+ (statisticCheckingVO.getAddress() == null ? "" : (" (" + statisticCheckingVO.getAddress() + ")")));
					}
				}
			}
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.listStatisticSearch"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * tim kiem kiem ke uke
	 * 
	 * @author phuongvm
	 * @since 08/05/2015
	 */
	public String listStatisticUkeSearch() {
		result.put("total", 0);
		result.put("rows", new ArrayList<StatisticCheckingVO>());
		try {
			KPaging<StatisticCheckingVO> kPaging = new KPaging<StatisticCheckingVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipStatisticFilter filter = new EquipStatisticFilter();
			filter.setRecordId(id);
			filter.setkPaging(kPaging);
			filter.setShopCode(shopCode);
			filter.setCustomerCode(customerCode);
			filter.setCustomerNameAddress(customerName);
			filter.setStaffCode(staffCode);
			filter.setStaffName(staffName);
			filter.setEquipCode(deviceCode);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (stepCheck != null && stepCheck > -1) {
				filter.setStepCheck(stepCheck);
			}
			if (!StringUtil.isNullOrEmpty(dateInWeek)) {
				filter.setDateInWeek(dateInWeek);
			}
			ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailShelf(filter);

			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
				StatisticCheckingVO statisticCheckingVO = vo.getLstObject().get(0);
				String t = statisticCheckingVO.getShopCode() + statisticCheckingVO.getShortCode() + statisticCheckingVO.getCustomerName() + statisticCheckingVO.getAddress();
				if (statisticCheckingVO.getCustomerCode() != null && statisticCheckingVO.getCustomerName() != null) {
					statisticCheckingVO.setCusCodeAndName(statisticCheckingVO.getCustomerCode() + DASH + statisticCheckingVO.getCustomerName());
				}
				for (int i = 1, size = vo.getLstObject().size(); i < size; i++) {
					statisticCheckingVO = vo.getLstObject().get(i);
					if (statisticCheckingVO.getCustomerCode() != null && statisticCheckingVO.getCustomerName() != null) {
						statisticCheckingVO.setCusCodeAndName(statisticCheckingVO.getCustomerCode() + DASH + statisticCheckingVO.getCustomerName());
					}
					if (t.equals(statisticCheckingVO.getShopCode() + statisticCheckingVO.getShortCode() + statisticCheckingVO.getCustomerName() + statisticCheckingVO.getAddress())) {
						statisticCheckingVO.setAddress(null);
						statisticCheckingVO.setCustomerName(null);
						statisticCheckingVO.setShopCode(null);
						statisticCheckingVO.setStaffCode(null);
						statisticCheckingVO.setShortCode(null);
						statisticCheckingVO.setShopName(null);
					} else {
						t = statisticCheckingVO.getShopCode() + statisticCheckingVO.getShortCode() + statisticCheckingVO.getCustomerName() + statisticCheckingVO.getAddress();
					}
				}
			}
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<StatisticCheckingVO>());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.listStatisticUkeSearch"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	public String rotateImage() {
		try {
			if (id != null && id != 0) {
				EquipAttachFile item = equipRecordMgr.getEquipAttachFileById(id);
				String realPathItem = Configuration.getImageRealSOPath() + item.getUrl();
				String realPathThumbnail = Configuration.getImageRealSOPath() + item.getThumbUrl();
				ImageUtility.rotateImage(realPathItem);
				ImageUtility.rotateImage(realPathThumbnail);
				String serverPathItem = Configuration.getImgServerSOPath(item.getUrl());
				String serverPathThumbnail = Configuration.getImgServerSOPath(item.getThumbUrl());
				result.put(ERROR, false);
				result.put("url", serverPathItem);
				result.put("thumbnailUrl", serverPathThumbnail);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.rotateImage"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	@SuppressWarnings("deprecation")
	public static String encodeToString(BufferedImage image, String type) {
		String imageString = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, type, bos);
			byte[] imageBytes = bos.toByteArray();
			BASE64Encoder encoder = new BASE64Encoder();
			imageString = encoder.encode(imageBytes);
			bos.close();
		} catch (IOException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return imageString;
	}

	public String downloadImage() {
		try {
			BufferedImage img;
			String type = "jpeg";
			EquipAttachFile attachFile = null;
			String titleInfo = "";
			String shopInfo = "";
			Boolean isAddInfo = false;
			EquipStatisticFilter fileFilter = null;
			if (id != null) {
				attachFile = equipRecordMgr.getEquipAttachFileById(id);
				if (null != attachFile) {
					if (null != attachFile.getObjectId() && attachFile.getObjectType() == EquipTradeType.INVENTORY.getValue()) {
						EquipStatisticRecDtl recordDetail = equipRecordMgr.geEquipStatisticRecDtlById(attachFile.getObjectId());
						if (recordDetail.getObjectStockType() == 2) {
							Customer cust = customerMgr.getCustomerById(recordDetail.getObjectStockId());
							if (cust != null) {
								titleInfo = cust.getCustomerCode().substring(0, 3) + " - " + cust.getCustomerName() + " - " + cust.getHousenumber() + "," + cust.getStreet();
							}
						} else {
							Shop sh = shopMgr.getShopById(recordDetail.getObjectStockId());
							if (sh != null) {
								titleInfo = sh.getShopCode() + " - " + sh.getShopName();
							}
						}
						shopInfo = codeNameDisplay(recordDetail.getShop().getShopCode(), recordDetail.getShop().getShopName());
					}
					if (EquipTradeType.INVENTORY.getValue().equals(attachFile.getObjectType())) {
						fileFilter = new EquipStatisticFilter();
						EquipStatisticRecDtl equipStatisticRecDtl = commonMgr.getEntityById(EquipStatisticRecDtl.class, attachFile.getObjectId());
						if (equipStatisticRecDtl != null && equipStatisticRecDtl.getStaff() != null && equipStatisticRecDtl.getEquipStatisticRecord() != null) {
							EquipStatisticRecord equipStatisticRecord = equipStatisticRecDtl.getEquipStatisticRecord();
							fileFilter.setProgramCode(equipStatisticRecord.getCode());
							fileFilter.setProgramName(equipStatisticRecord.getName());

							Staff staff = equipStatisticRecDtl.getStaff();
							if (staff != null && staff.getStaffType() != null && staff.getStaffType().getSpecificType() != null && StaffSpecificType.STAFF.getValue().equals(staff.getStaffType().getSpecificType().getValue())) {
								fileFilter.setStaffCode(staff.getStaffCode());
								fileFilter.setStaffName(staff.getStaffName());
							}

							if (EquipStockTotalType.KHO_KH.getValue().equals(equipStatisticRecDtl.getObjectStockType())) {
								isAddInfo = true;
							}
						}
					}
				}

				String imgDes = addLabelToImage(attachFile.getUrl(), titleInfo, shopInfo, dateInfo, fileFilter, overwrite, isAddInfo);
				if (!StringUtil.isNullOrEmpty(imgDes)) {
					File fileImageDes = new File(Configuration.getImageRealSOPath() + imgDes);
					img = ImageIO.read(fileImageDes);
					if (FileUtility.getMime(fileImageDes) != null) {
						type = FileUtility.getMime(fileImageDes);
					}
					String imgstr;
					if (type.equals("image/png")) {
						imgstr = encodeToString(img, "png");
						result.put("errMsg", "data:image/png;base64," + imgstr);
					} else {
						imgstr = encodeToString(img, "jpeg");
						result.put("errMsg", "data:image/jpeg;base64," + imgstr);
					}
					result.put(ERROR, false);
				} else {
					result.put(ERROR, true);
				}
			}
		} catch (IOException e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.downloadImage"), createLogErrorStandard(actionStartTime));
		} catch (BusinessException e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.downloadImage"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	private String addLabelToImage(String imgSrc, String customerInfo, String shopInfo, String dateInfo, EquipStatisticFilter fileFilter, boolean overwrite, boolean isAddInfo) {
		final int DISTANCE_X = 10;
		final int DISTANCE_Y = 17;
		String imgDes = "";
		int dotIndex = imgSrc.lastIndexOf(".");
		imgDes = imgSrc.substring(0, dotIndex - 1) + "_info" + imgSrc.substring(dotIndex);
		File fileImage = new File(Configuration.getImageRealSOPath() + imgSrc);
		File outputFile = new File(Configuration.getImageRealSOPath() + imgDes);
		if (fileImage.exists() && (!outputFile.exists() || overwrite)) {
			try {
				BufferedImage imageSrc = ImageIO.read(fileImage);
				BufferedImage newImage = new BufferedImage(imageSrc.getWidth(), imageSrc.getHeight() + 87, BufferedImage.OPAQUE);

				Graphics g = newImage.getGraphics();
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, newImage.getWidth(), newImage.getHeight());
				g.drawImage(imageSrc, 0, 0, imageSrc.getWidth(), imageSrc.getHeight(), null);
				Font font = new Font("Times New Roman", Font.PLAIN, 15);
				g.setFont(font);
				g.setColor(Color.BLACK);
				g.drawString(customerInfo, DISTANCE_X, imageSrc.getHeight() + DISTANCE_Y);
				//				System.out.println(customerInfo);
				g.drawString(R.getResource("equipment.statistic.npp.text") + " " + shopInfo, DISTANCE_X, imageSrc.getHeight() + DISTANCE_Y * 2);
				//				System.out.println(shopInfo);
				g.drawString(R.getResource("equipment.statistic.tg.text") + " " + dateInfo, DISTANCE_X, imageSrc.getHeight() + DISTANCE_Y * 3);

				if (isAddInfo && fileFilter != null) {
					String programText = StringFormatter.formatTextCodeAndName(fileFilter.getProgramCode(), fileFilter.getProgramName());
					g.drawString(R.getResource("equipment.statistic.program.text") + " " + (StringUtil.isNullOrEmpty(programText) ? "" : programText), DISTANCE_X, imageSrc.getHeight() + DISTANCE_Y * 4);

					String staffText = StringFormatter.formatTextCodeAndName(fileFilter.getStaffCode(), fileFilter.getStaffName());
					g.drawString(R.getResource("equipment.statistic.staff.nvbh.text") + " " + (StringUtil.isNullOrEmpty(staffText) ? "" : staffText), DISTANCE_X, imageSrc.getHeight() + DISTANCE_Y * 5);
				}
				g.dispose();
				OutputStream os = new FileOutputStream(outputFile);

				Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
				ImageWriter writer = (ImageWriter) writers.next();
				ImageOutputStream ios = ImageIO.createImageOutputStream(os);
				writer.setOutput(ios);

				ImageWriteParam param = writer.getDefaultWriteParam();

				// compress to a given quality
				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				param.setCompressionQuality(0.6f);
				writer.write(null, new IIOImage(newImage, null, null), param);
				os.close();
				ios.close();
				writer.dispose();
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.addLabelToImage"), createLogErrorStandard(actionStartTime));
				return "";
			}
		}
		return imgDes;
	}

	public String getImagesForPopup() {
		try {
			ObjectVO<ImageVO> imageVO = null;
			KPaging<ImageVO> kPaging = new KPaging<ImageVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipStatisticFilter filter = new EquipStatisticFilter();
			filter.setRecordId(id);
			filter.setEquipId(equipId);
			filter.setkPagingImageVO(kPaging);
			filter.setShopCode(shopCode);
			filter.setCustomerCode(customerCode);
			filter.setStaffCode(staffCode);
			filter.setEquipCode(deviceCode);
			filter.setSeri(seri);
			filter.setStatus(status);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			imageVO = equipRecordMgr.getListImageVO(filter);

			if (imageVO != null) {
				List<ImageVO> lstImage = imageVO.getLstObject();
				if (lstImage != null && lstImage.size() > 0) {
					for (ImageVO temp : lstImage) {
						if (temp.getCreateDate() != null) {
							temp.setHhmmDate(new SimpleDateFormat("HH:mm:ss").format(temp.getCreateDate()));
							temp.setDmyDate(new SimpleDateFormat("dd/MM/yyyy").format(temp.getCreateDate()));
						}
					}
				}
				result.put("lstImage", lstImage);
				result.put(ERROR, false);
			}
		} catch (BusinessException e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.getImagesForPopup"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * chi tiet kiem ke
	 * 
	 * @return
	 */
	public String statisticDetail() {
		try {
			listDetail = equipRecordMgr.getListEquipmentRecordDetailVOByRecordId(id);
		} catch (Exception e) {
			listDetail = new ArrayList<EquipmentRecordDetailVO>();
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.statisticDetail"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * 
	 * lay danh sach cac lan kiem ke
	 * @author trietptm
	 * @return String
	 * @since Mar 11, 2016
	 */
	public String getListStatisticTime() {
		try {
			KPaging<EquipStatisticRecordDetailVO> kPaging = new KPaging<EquipStatisticRecordDetailVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipStatisticFilter filter = new EquipStatisticFilter();
			filter.setRecordId(id);
			filter.setEquipId(equipId);
			filter.setkPagingESRD(kPaging);
			filter.setShopCode(shopCode);
			filter.setCustomerCode(customerCode);
			filter.setStaffCode(staffCode);
			filter.setEquipCode(deviceCode);
			filter.setSeri(seri);
			filter.setStatus(status);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			ObjectVO<EquipStatisticRecordDetailVO> vo = equipRecordMgr.getListStatisticTime(filter);
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipStatisticRecordDetailVO>());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.getListStatisticTime"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * cap nhat trang thai kiem ke
	 * 
	 * @author tuannd20
	 * @return trang thai cap nhat thanh cong/that bai & chi tiet loi trong bien
	 *         result
	 */
	public String updateEquipmentStatisticStatus() {
		resetToken(result);
		if (listId != null && listId.size() > 0 && getEquipmentStatisticRecordStatus() != null) {
			try {
				List<FormErrVO> updateFailedRecords = equipmentManagerMgr.updateEquipmentStatisticStatus(listId, EquipmentStatisticRecordStatus.parseValue(equipmentStatisticRecordStatus), getLogInfoVO());
				result.put("updateFailedRecords", updateFailedRecords);
				result.put(ERROR, false);
			} catch (Exception e) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.updateEquipmentStatisticStatus"), createLogErrorStandard(actionStartTime));
			}
		}
		return JSON;
	}

	/**
	 * Xuat excel kiem ke
	 * 
	 * @author phuongvm
	 * @since 24/04/2015
	 * @return
	 */
	public String exportExcel() {
		final int MAX_LENGTH_HEADER = 9;
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			// Init XSSF workboook
			String outputName = ConstantManager.TEMPLATE_EQUIP_LIST_STATISTIC_CHECKING_EXPORT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			workbook = new SXSSFWorkbook(200);
			workbook.setCompressTempFiles(true);
			// Tao shett
			SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

			// Set Getting Defaul
			sheetData.setDefaultRowHeight((short) (15 * 20));
			sheetData.setDefaultColumnWidth(18);
			// Size Row
			ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 20);
			sheetData.setDisplayGridlines(true);
			int iRow = 0, iColumn = 0;
			for (int i = 0; i < lstIdRecord.size(); i++) {

				// set static Column width
				EquipStatisticRecord instance = equipRecordMgr.getEquipStatisticRecordById(lstIdRecord.get(i));
				if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
					continue;
				}
				listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(lstIdRecord.get(i));
				listShelf = equipRecordMgr.getListShelfByRecordId(lstIdRecord.get(i));
				listEquipment = equipRecordMgr.getListEquipByRecordId(lstIdRecord.get(i));
				String headers = "";
				iColumn = 0;
				iRow += 3;
				String maKiemKe = instance.getCode();
				String fromDate = DateUtil.toDateString(instance.getFromDate(), DateUtil.DATE_DD_MM_YY);
				String toDate = DateUtil.toDateString(instance.getToDate(), DateUtil.DATE_DD_MM_YY);

				if ((!listEquipGroup.isEmpty() || !listEquipment.isEmpty()) && instance.getType() == 2) {//nhom thiet bi + so lan
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 250, 150, 220, 220, 200, 120, 120, 120, 120);
					headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.checking.group.solan");
					String[] lstHeaders = headers.split(";");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, iRow, lstHeaders.length - 1, iRow, R.getResource("equipment.manager.kiemke.tieude"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, lstHeaders.length - 1, iRow, R.getResource("baocao.dktt.sheet1.maKiemKe") + maKiemKe, style.get(ExcelPOIProcessUtils.HEADER));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, lstHeaders.length - 1, iRow, R.getResource("baocao.dtbh.general.tuNgay") + fromDate + " " + R.getResource("baocao.dtbh.dt3.sheet1.general.denNgay") + toDate,
							style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
					iRow += 3; // xuong hang 3 dong
					//Bang
					for (int j = 0; j < lstHeaders.length; j++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[j], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					EquipStatisticFilter filter = new EquipStatisticFilter();
					filter.setRecordId(instance.getId());
					//tamvnm: Them dieu kien ko lay CHUA KIEM KE
					filter.setNotStatus(ActiveType.STOPPED.getValue());
					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroup(filter);
					List<StatisticCheckingVO> lstDetails = vo.getLstObject();
					if (lstDetails != null) {
						for (int count = 0; count < lstDetails.size(); count++) {
							String buffer = "";
							iColumn = 0;
							iRow++;
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, count + 1, style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							//get shopcode - shopname
							buffer = lstDetails.get(count).getShopCode() + "-" + lstDetails.get(count).getShopName();
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));

							//get shortcode-customername
							if (!StringUtil.isNullOrEmpty(lstDetails.get(count).getCustomerCode()) && !StringUtil.isNullOrEmpty(lstDetails.get(count).getCustomerName())) {
								buffer = lstDetails.get(count).getCustomerCode() + "-" + lstDetails.get(count).getCustomerName();
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_LEFT));
							}

							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getAddress(), style.get(ExcelPOIProcessUtils.ROW_LEFT));

							//get equipgroupcode-equipgroupname
							buffer = lstDetails.get(count).getEquipCodeGroup() + "-" + lstDetails.get(count).getEquipNameGroup();
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getSeri(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getStepCheck(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							// status
							String status = "";
							if (EquipStatisticStatus.NOT_INVENTORY.getValue().equals(lstDetails.get(count).getStatus())) {
								status = R.getResource("equipment.manager.chuakiemke");
							} else if (EquipStatisticStatus.INVENTORIED.getValue().equals(lstDetails.get(count).getStatus())) {
								status = R.getResource("equipment.manager.dakiemke");
							} else if (EquipStatisticStatus.STILL.getValue().equals(lstDetails.get(count).getStatus())) {
								status = R.getResource("equipment.manager.con");
							} else if (EquipStatisticStatus.LOST.getValue().equals(lstDetails.get(count).getStatus())) {
								status = R.getResource("equipment.manager.mat");
							}
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, status, style.get(ExcelPOIProcessUtils.ROW_LEFT));
						}
					}
				} else if ((!listEquipGroup.isEmpty() || !listEquipment.isEmpty()) && instance.getType() == 1) {//nhom thiet bi + tuyen
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 250, 150, 220, 220, 200, 120, 120, 120, 120);
					headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.checking.group.tuyen");
					String[] lstHeaders = headers.split(";");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, iRow, lstHeaders.length - 1, iRow, R.getResource("equipment.manager.kiemke.tieude"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, lstHeaders.length - 1, iRow, R.getResource("baocao.dktt.sheet1.maKiemKe") + maKiemKe, style.get(ExcelPOIProcessUtils.HEADER));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, lstHeaders.length - 1, iRow, R.getResource("baocao.dtbh.general.tuNgay") + fromDate + " " + R.getResource("baocao.dtbh.dt3.sheet1.general.denNgay") + toDate,
							style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
					iRow += 3; // xuong hang 3 dong
					//Bang
					for (int j = 0; j < lstHeaders.length; j++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[j], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					EquipStatisticFilter filter = new EquipStatisticFilter();
					filter.setRecordId(instance.getId());
					//tamvnm: Them dieu kien ko lay CHUA KIEM KE
					filter.setNotStatus(ActiveType.STOPPED.getValue());
					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroup(filter);
					List<StatisticCheckingVO> lstDetails = vo.getLstObject();
					if (lstDetails != null) {
						for (int count = 0; count < lstDetails.size(); count++) {
							String buffer = "";
							iColumn = 0;
							iRow++;
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, count + 1, style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							//get shopcode - shopname
							buffer = lstDetails.get(count).getShopCode() + "-" + lstDetails.get(count).getShopName();
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));

							//get shortcode-customername
							if (!StringUtil.isNullOrEmpty(lstDetails.get(count).getCustomerCode()) && !StringUtil.isNullOrEmpty(lstDetails.get(count).getCustomerName())) {
								buffer = lstDetails.get(count).getCustomerCode() + "-" + lstDetails.get(count).getCustomerName();
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_LEFT));
							}

							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getAddress(), style.get(ExcelPOIProcessUtils.ROW_LEFT));

							//get equipgroupcode-equipgroupname
							buffer = lstDetails.get(count).getEquipCodeGroup() + "-" + lstDetails.get(count).getEquipNameGroup();
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getSeri(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getDateInWeek(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getStatisticDate(), style.get(ExcelPOIProcessUtils.ROW_CENTER));
							// status
							String status = "";
							if (EquipStatisticStatus.NOT_INVENTORY.getValue().equals(lstDetails.get(count).getStatus())) {
								status = R.getResource("equipment.manager.chuakiemke");
							} else if (EquipStatisticStatus.INVENTORIED.getValue().equals(lstDetails.get(count).getStatus())) {
								status = R.getResource("equipment.manager.dakiemke");
							} else if (EquipStatisticStatus.STILL.getValue().equals(lstDetails.get(count).getStatus())) {
								status = R.getResource("equipment.manager.con");
							} else if (EquipStatisticStatus.LOST.getValue().equals(lstDetails.get(count).getStatus())) {
								status = R.getResource("equipment.manager.mat");
							}
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, status, style.get(ExcelPOIProcessUtils.ROW_LEFT));
						}
					}
				} else if (!listShelf.isEmpty() && instance.getType() == 2) {//u ke + so lan
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 250, 150, 220, 250, 100, 120, 120);
					headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.checking.shelf.solan");
					String[] lstHeaders = headers.split(";");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, iRow, lstHeaders.length - 1, iRow, R.getResource("equipment.manager.kiemke.tieude"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, lstHeaders.length - 1, iRow, R.getResource("baocao.dktt.sheet1.maKiemKe") + maKiemKe, style.get(ExcelPOIProcessUtils.HEADER));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, lstHeaders.length - 1, iRow, R.getResource("baocao.dtbh.general.tuNgay") + fromDate + " " + R.getResource("baocao.dtbh.dt3.sheet1.general.denNgay") + toDate,
							style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
					iRow += 3; // xuong hang 3 dong
					//Bang
					for (int j = 0; j < lstHeaders.length; j++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[j], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					EquipStatisticFilter filter = new EquipStatisticFilter();
					filter.setRecordId(instance.getId());
					//tamvnm: Them dieu kien ko lay CHUA KIEM KE
					filter.setNotStatus(ActiveType.STOPPED.getValue());
					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailShelf(filter);
					List<StatisticCheckingVO> lstDetails = vo.getLstObject();
					if (lstDetails != null) {
						for (int count = 0; count < lstDetails.size(); count++) {
							String buffer = "";
							iColumn = 0;
							iRow++;
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, count + 1, style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							//get shopcode - shopname
							buffer = lstDetails.get(count).getShopCode() + "-" + lstDetails.get(count).getShopName();
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));

							//get shortcode-customername
							if (!StringUtil.isNullOrEmpty(lstDetails.get(count).getCustomerCode()) && !StringUtil.isNullOrEmpty(lstDetails.get(count).getCustomerName())) {
								buffer = lstDetails.get(count).getCustomerCode() + "-" + lstDetails.get(count).getCustomerName();
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_LEFT));
							}

							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getAddress(), style.get(ExcelPOIProcessUtils.ROW_LEFT));

							//get equipcode-equipname
							buffer = lstDetails.get(count).getEquipCode() + "-" + lstDetails.get(count).getEquipName();
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getStepCheck(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getQuantity(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getActualQuantity(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));

						}
					}
				} else if (!listShelf.isEmpty() && instance.getType() == 1) {//u ke + tuyen
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 250, 150, 220, 250, 100, 120, 120, 120);
					headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.checking.shelf.tuyen");
					String[] lstHeaders = headers.split(";");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, iRow, lstHeaders.length - 1, iRow, R.getResource("equipment.manager.kiemke.tieude"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, lstHeaders.length - 1, iRow, R.getResource("baocao.dktt.sheet1.maKiemKe") + maKiemKe, style.get(ExcelPOIProcessUtils.HEADER));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, lstHeaders.length - 1, iRow, R.getResource("baocao.dtbh.general.tuNgay") + fromDate + " " + R.getResource("baocao.dtbh.dt3.sheet1.general.denNgay") + toDate,
							style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
					iRow += 3; // xuong hang 3 dong
					//Bang
					for (int j = 0; j < lstHeaders.length; j++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[j], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					EquipStatisticFilter filter = new EquipStatisticFilter();
					filter.setRecordId(instance.getId());
					//tamvnm: Them dieu kien ko lay CHUA KIEM KE
					filter.setNotStatus(ActiveType.STOPPED.getValue());
					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailShelf(filter);
					List<StatisticCheckingVO> lstDetails = vo.getLstObject();
					if (lstDetails != null) {
						for (int count = 0; count < lstDetails.size(); count++) {
							String buffer = "";
							iColumn = 0;
							iRow++;
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, count + 1, style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							//get shopcode - shopname
							buffer = lstDetails.get(count).getShopCode() + "-" + lstDetails.get(count).getShopName();
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));

							//get shortcode-customername
							if (!StringUtil.isNullOrEmpty(lstDetails.get(count).getCustomerCode()) && !StringUtil.isNullOrEmpty(lstDetails.get(count).getCustomerName())) {
								buffer = lstDetails.get(count).getCustomerCode() + "-" + lstDetails.get(count).getCustomerName();
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_LEFT));
							}

							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getAddress(), style.get(ExcelPOIProcessUtils.ROW_LEFT));

							//get equipcode-equipname
							buffer = lstDetails.get(count).getEquipCode() + "-" + lstDetails.get(count).getEquipName();
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, buffer, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getDateInWeek(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getStatisticDate(), style.get(ExcelPOIProcessUtils.ROW_CENTER));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getQuantity(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getActualQuantity(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));

						}
					}
				} else {
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, iRow, MAX_LENGTH_HEADER, iRow, R.getResource("equipment.manager.kiemke.tieude"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, MAX_LENGTH_HEADER, iRow, R.getResource("baocao.dktt.sheet1.maKiemKe") + maKiemKe, style.get(ExcelPOIProcessUtils.HEADER));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iColumn, ++iRow, MAX_LENGTH_HEADER, iRow, R.getResource("baocao.dtbh.general.tuNgay") + fromDate + " " + R.getResource("baocao.dtbh.dt3.sheet1.general.denNgay") + toDate, style
							.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				}
			}
			out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());

		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportExcel"), createLogErrorStandard(actionStartTime));
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			workbook.dispose();
			System.gc();
		}
		return JSON;
	}

	/**
	 * Xuat excel kiem ke nhom thiet bi so lan
	 * 
	 * @author phuongvm
	 * @since 21/05/2015
	 * @return
	 */
	public String exportExcelGroupSoLan() {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			// Init XSSF workboook
			String outputName = ConstantManager.TEMPLATE_EQUIP_LIST_STATISTIC_CHECKING_EXPORT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			workbook = new SXSSFWorkbook(200);
			workbook.setCompressTempFiles(true);
			// Tao shett
			SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

			// Set Getting Defaul
			sheetData.setDefaultRowHeight((short) (15 * 20));
			sheetData.setDefaultColumnWidth(18);
			// Size Row
			ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 20);
			sheetData.setDisplayGridlines(true);
			int iRow = 0, iColumn = 0;
			for (int i = 0; i < lstIdRecord.size(); i++) {

				// set static Column width
				EquipStatisticRecord instance = equipRecordMgr.getEquipStatisticRecordById(lstIdRecord.get(i));
				if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
					continue;
				}
				listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(lstIdRecord.get(i));
				listShelf = equipRecordMgr.getListShelfByRecordId(lstIdRecord.get(i));
				listEquipment = equipRecordMgr.getListEquipByRecordId(lstIdRecord.get(i));
				String headers = "";
				iColumn = 0;
				if ((!listEquipGroup.isEmpty() || !listEquipment.isEmpty()) && KK_SOLAN.equals(instance.getType())) {//nhom thiet bi + so lan
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 250, 70, 70, 220, 220, 200, 120, 120, 120, 120);
					headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.checking.group.solan2");
					String[] lstHeaders = headers.split(";");

					//Bang
					for (int j = 0; j < lstHeaders.length; j++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[j], style.get(ExcelPOIProcessUtils.HEADER_GRAY_TOP_BOTTOM_MEDIUM));
					}
					EquipStatisticFilter filter = new EquipStatisticFilter();
					filter.setRecordId(instance.getId());
					//					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroup(filter);
					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroupSoLan(null, instance.getId(), shopId, equipGroupId, equipCode, seri, stockId, stepCheck, getLogInfoVO());

					List<StatisticCheckingVO> lstDetails = vo.getLstObject();
					if (lstDetails != null && lstDetails.size() > 0) {
						for (int count = 0; count < lstDetails.size(); count++) {
							iColumn = 0;
							iRow++;
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							// status
							String status = "";
							//							if (EquipStatisticStatus.NOT_INVENTORY.getValue().equals(lstDetails.get(count).getStatus())) {
							//								status = "";
							//							} else if (EquipStatisticStatus.INVENTORIED.getValue().equals(lstDetails.get(count).getStatus())) {
							//								status = "";
							//							} else if (EquipStatisticStatus.STILL.getValue().equals(lstDetails.get(count).getStatus())) {
							//								status = "X";
							//							} else if (EquipStatisticStatus.LOST.getValue().equals(lstDetails.get(count).getStatus())) {
							//								status = "";
							//							}
							if (lstDetails.get(count).getIsBelong() != null && lstDetails.get(count).getIsBelong() > 0) {
								status = "X";
							}
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, status, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getStepCheck(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
						}
						out = new FileOutputStream(exportFileName);
						workbook.write(out);
						out.close();
						String outputPath = Configuration.getExportExcelPath() + outputName;
						result.put(REPORT_PATH, outputPath);
						result.put(ERROR, false);
						result.put("hasData", true);
						MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());

					} else {
						result.put(ERROR, true);
						result.put("hasData", false);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.err.excell"));
						return JSON;
					}
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportExcelGroupSoLan"), createLogErrorStandard(actionStartTime));
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			workbook.dispose();
			System.gc();
		}
		return JSON;
	}

	/**
	 * Xuat excel kiem ke u ke so lan
	 * 
	 * @author phuongvm
	 * @since 21/05/2015
	 * @return
	 */
	public String exportExcelUKeSoLan() {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			// Init XSSF workboook
			String outputName = ConstantManager.TEMPLATE_EQUIP_LIST_STATISTIC_CHECKING_EXPORT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			workbook = new SXSSFWorkbook(200);
			workbook.setCompressTempFiles(true);
			// Tao shett
			SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

			// Set Getting Defaul
			sheetData.setDefaultRowHeight((short) (15 * 20));
			sheetData.setDefaultColumnWidth(18);
			// Size Row
			ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 20);
			sheetData.setDisplayGridlines(true);
			int iRow = 0, iColumn = 0;
			for (int i = 0; i < lstIdRecord.size(); i++) {

				// set static Column width
				EquipStatisticRecord instance = equipRecordMgr.getEquipStatisticRecordById(lstIdRecord.get(i));
				if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
					continue;
				}
				listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(lstIdRecord.get(i));
				listShelf = equipRecordMgr.getListShelfByRecordId(lstIdRecord.get(i));
				String headers = "";
				iColumn = 0;
				if (!listShelf.isEmpty() && KK_SOLAN.equals(instance.getType())) {//u ke + so lan
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 250, 150, 90, 90, 220, 200, 120, 120, 120, 120);
					headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.checking.uke.solan2");
					String[] lstHeaders = headers.split(";");

					//Bang
					for (int j = 0; j < lstHeaders.length; j++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[j], style.get(ExcelPOIProcessUtils.HEADER_GRAY_TOP_BOTTOM_MEDIUM));
					}
					EquipStatisticFilter filter = new EquipStatisticFilter();
					filter.setRecordId(instance.getId());
					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailShelfSoLan(null, instance.getId(), shopId, shelfId, stockId, stepCheck, getLogInfoVO());
					List<StatisticCheckingVO> lstDetails = vo.getLstObject();
					if (lstDetails != null && lstDetails.size() > 0) {
						for (int count = 0; count < lstDetails.size(); count++) {
							iColumn = 0;
							iRow++;
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getShopCode(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getActualQuantity(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getStepCheck(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
						}
						out = new FileOutputStream(exportFileName);
						workbook.write(out);
						out.close();
						String outputPath = Configuration.getExportExcelPath() + outputName;
						result.put(REPORT_PATH, outputPath);
						result.put(ERROR, false);
						result.put("hasData", true);
						MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());

					} else {
						result.put(ERROR, true);
						result.put("hasData", false);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.err.excell"));
						return JSON;
					}
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportExcelUKeSoLan"), createLogErrorStandard(actionStartTime));
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			workbook.dispose();
			System.gc();
		}
		return JSON;
	}

	/**
	 * Xuat excel kiem ke nhom thiet bi tuyen
	 * 
	 * @author phuongvm
	 * @since 22/05/2015
	 * @return
	 */
	public String exportExcelGroupTuyen() {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			// Init XSSF workboook
			String outputName = ConstantManager.TEMPLATE_EQUIP_LIST_STATISTIC_CHECKING_EXPORT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			workbook = new SXSSFWorkbook(200);
			workbook.setCompressTempFiles(true);
			// Tao shett
			SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

			// Set Getting Defaul
			sheetData.setDefaultRowHeight((short) (15 * 20));
			sheetData.setDefaultColumnWidth(18);
			// Size Row
			ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 20);
			sheetData.setDisplayGridlines(true);
			int iRow = 0, iColumn = 0;
			for (int i = 0; i < lstIdRecord.size(); i++) {

				// set static Column width
				EquipStatisticRecord instance = equipRecordMgr.getEquipStatisticRecordById(lstIdRecord.get(i));
				if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
					continue;
				}
				listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(lstIdRecord.get(i));
				listShelf = equipRecordMgr.getListShelfByRecordId(lstIdRecord.get(i));
				listEquipment = equipRecordMgr.getListEquipByRecordId(lstIdRecord.get(i));
				String headers = "";
				iColumn = 0;
				if ((!listEquipGroup.isEmpty() || !listEquipment.isEmpty()) && KK_TUYEN.equals(instance.getType())) {//nhom thiet bi + tuyen
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 250, 70, 120, 220, 220, 200, 120, 120, 120, 120);
					headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.checking.group.tuyen2");
					String[] lstHeaders = headers.split(";");

					//Bang
					for (int j = 0; j < lstHeaders.length; j++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[j], style.get(ExcelPOIProcessUtils.HEADER_GRAY_TOP_BOTTOM_MEDIUM));
					}
					EquipStatisticFilter filter = new EquipStatisticFilter();
					filter.setRecordId(instance.getId());
					//					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroup(filter);
					Date __checkDate = null;
					if (!StringUtil.isNullOrEmpty(checkDate)) {
						__checkDate = DateUtil.parse(checkDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailGroupTuyen(null, instance.getId(), shopId, equipGroupId, equipCode, seri, stockId, __checkDate, getLogInfoVO());
					List<StatisticCheckingVO> lstDetails = vo.getLstObject();
					if (lstDetails != null && lstDetails.size() > 0) {
						for (int count = 0; count < lstDetails.size(); count++) {
							iColumn = 0;
							iRow++;
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							// status
							String status = "";
							//							if (EquipStatisticStatus.NOT_INVENTORY.getValue().equals(lstDetails.get(count).getStatus())) {
							//								status = "";
							//							} else if (EquipStatisticStatus.INVENTORIED.getValue().equals(lstDetails.get(count).getStatus())) {
							//								status = "";
							//							} else if (EquipStatisticStatus.STILL.getValue().equals(lstDetails.get(count).getStatus())) {
							//								status = "X";
							//							} else if (EquipStatisticStatus.LOST.getValue().equals(lstDetails.get(count).getStatus())) {
							//								status = "";
							//							}
							if (lstDetails.get(count).getIsBelong() != null && lstDetails.get(count).getIsBelong() > 0) {
								status = "X";
							}
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, status, style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getCheckDate(), style.get(ExcelPOIProcessUtils.ROW_CENTER));
						}
						out = new FileOutputStream(exportFileName);
						workbook.write(out);
						out.close();
						String outputPath = Configuration.getExportExcelPath() + outputName;
						result.put(REPORT_PATH, outputPath);
						result.put(ERROR, false);
						result.put("hasData", true);
						MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
					} else {
						result.put(ERROR, true);
						result.put("hasData", false);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.err.excell"));
						return JSON;
					}
				}
			}

		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportExcelGroupTuyen"), createLogErrorStandard(actionStartTime));
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			workbook.dispose();
			System.gc();
		}
		return JSON;
	}

	/**
	 * Xuat excel kiem ke u ke Tuyen
	 * 
	 * @author phuongvm
	 * @since 22/05/2015
	 * @return
	 */
	public String exportExcelUKeTuyen() {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			// Init XSSF workboook
			String outputName = ConstantManager.TEMPLATE_EQUIP_LIST_STATISTIC_CHECKING_EXPORT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			workbook = new SXSSFWorkbook(200);
			workbook.setCompressTempFiles(true);
			// Tao shett
			SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

			// Set Getting Defaul
			sheetData.setDefaultRowHeight((short) (15 * 20));
			sheetData.setDefaultColumnWidth(18);
			// Size Row
			ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 20);
			sheetData.setDisplayGridlines(true);
			int iRow = 0, iColumn = 0;
			for (int i = 0; i < lstIdRecord.size(); i++) {

				// set static Column width
				EquipStatisticRecord instance = equipRecordMgr.getEquipStatisticRecordById(lstIdRecord.get(i));
				if (instance == null || instance.getShop() == null || !checkShopPermission(instance.getShop().getShopCode())) {
					continue;
				}
				listEquipGroup = equipRecordMgr.getListEquipGroupByRecordId(lstIdRecord.get(i));
				listShelf = equipRecordMgr.getListShelfByRecordId(lstIdRecord.get(i));
				String headers = "";
				iColumn = 0;
				if (!listShelf.isEmpty() && KK_TUYEN.equals(instance.getType())) {//u ke + tuyen
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 250, 150, 90, 120, 220, 200, 120, 120, 120, 120);
					headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.checking.uke.tuyen2");
					String[] lstHeaders = headers.split(";");

					//Bang
					for (int j = 0; j < lstHeaders.length; j++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[j], style.get(ExcelPOIProcessUtils.HEADER_GRAY_TOP_BOTTOM_MEDIUM));
					}
					EquipStatisticFilter filter = new EquipStatisticFilter();
					filter.setRecordId(instance.getId());
					Date __checkDate = null;
					if (!StringUtil.isNullOrEmpty(checkDate)) {
						__checkDate = DateUtil.parse(checkDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					ObjectVO<StatisticCheckingVO> vo = equipRecordMgr.getListDetailShelfTuyen(null, instance.getId(), shopId, shelfId, stockId, __checkDate, getLogInfoVO());
					List<StatisticCheckingVO> lstDetails = vo.getLstObject();
					if (lstDetails != null && lstDetails.size() > 0) {
						for (int count = 0; count < lstDetails.size(); count++) {
							iColumn = 0;
							iRow++;
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getShopCode(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getActualQuantity(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstDetails.get(count).getCheckDate(), style.get(ExcelPOIProcessUtils.ROW_CENTER));
						}

						out = new FileOutputStream(exportFileName);
						workbook.write(out);
						out.close();
						String outputPath = Configuration.getExportExcelPath() + outputName;
						result.put(REPORT_PATH, outputPath);
						result.put(ERROR, false);
						result.put("hasData", true);
						MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());

					} else {
						result.put(ERROR, true);
						result.put("hasData", false);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.err.excell"));
						return JSON;
					}
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportExcelUKeTuyen"), createLogErrorStandard(actionStartTime));
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			workbook.dispose();
			System.gc();
		}
		return JSON;
	}

	/**
	 * Xuat excel bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since 17/12/2014
	 * @return
	 */
	public String exportShop() {
		try {
			if (id == null || id <= 0) {
				result.put("hasData", false);
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			//			List<ShopObjectType> lstShopObjectType = new ArrayList<ShopObjectType>();
			//			lstShopObjectType.add(ShopObjectType.NPP);
			//			lstShopObjectType.add(ShopObjectType.NPP_KA);
			/*
			 * List<EquipRecordShopVO> lstEquipRecordShopVO =
			 * equipRecordMgr.getListShopOfRecord(id, lstShopObjectType, null,
			 * null);
			 */
			/* sua lại ham xuat file excel - khong su dung ham chung - hoanv25 */
			List<EquipRecordShopVO> lstEquipRecordShopVO = equipRecordMgr.getListShopOfRecordExport(id, null, shopSearchCode, shopSearchName);
			SXSSFWorkbook workbook = null;
			FileOutputStream out = null;
			try {
				if (null != lstEquipRecordShopVO && lstEquipRecordShopVO.size() > 0) {

					// Init XSSF workboook
					String outputName = ConstantManager.TEMPLATE_SHOP_CHECK_STATISTIC + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX;
					String exportFileName = Configuration.getStoreRealPath() + outputName;

					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					// Tao shett
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

					// Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(18);
					// set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 120);
					// Size Row
					ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 20);
					sheetData.setDisplayGridlines(true);
					// text style
					XSSFCellStyle textStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.NORMAL).clone();
					DataFormat fmt = workbook.createDataFormat();
					textStyle.setDataFormat(fmt.getFormat("@"));

					int iRow = 0, iColumn = 0;
					// tao column header
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code");
					ExcelPOIProcessUtils.addCell(sheetData, iColumn, iRow, headers, style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					for (int i = 0; i < lstEquipRecordShopVO.size(); i++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn, ++iRow, lstEquipRecordShopVO.get(i).getShopCode(), textStyle);
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.err.excell");
					result.put(ERROR, true);
					result.put("data.hasData", true);
					result.put("errMsg", errMsg);
				}
			} catch (Exception e) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportShop"), createLogErrorStandard(actionStartTime));
				if (workbook != null) {
					workbook.dispose();
				}
				if (out != null) {
					out.close();
				}
			}
			System.gc();
			return JSON;
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.exportShop"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	public String check1(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).trim().toUpperCase().equals(row2.get(0).trim().toUpperCase())) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
						} else {
							message += ", " + (k + 2);
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
			}
		}
		return message;
	}

	/**
	 * import bien ban bang excel
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 */
	public String importShop() throws Exception {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 2);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				if (id == null || id <= 0) {
					isError = true;
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
					return SUCCESS;
				}
				EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
				if (record == null) {
					message = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
				}
				if (record.getRecordStatus() != 0) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect");
				}
				if (message.length() > 0) {
					isError = true;
					errMsg = message;
					return SUCCESS;
				}

				lstView = new ArrayList<CellBean>();

				List<EquipStatisticUnit> lst = null;
				boolean isHasChild = false;
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					isHasChild = false;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						lst = new ArrayList<EquipStatisticUnit>();
						Shop shop = null;
						// ** Check trung du lieu cac record */
						message = "";
						//						String checkMessage1 = check1(lstData, i);
						String checkMessage1 = "";
						if (!StringUtil.isNullOrEmpty(checkMessage1)) {
							lstFails.add(StringUtil.addFailBean(row, checkMessage1));
						} else {
							// ** Ma don vi */
							if (row.size() > 0) {
								String value = row.get(0);
								String msg = ValidateUtil.validateField(value, "contract.shop.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									//									for (int j = 0, sizej = lstData.size(); j < sizej; j++) {
									//										if (!StringUtil.isNullOrEmpty(value) && j != i && value.trim().toUpperCase().equals(lstData.get(j).get(0).trim().toUpperCase())) {
									//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.code.shop.is.exist", value) + "\n";
									//											break;
									//										}
									//									}

									if (message.length() == 0) {
										value = value.trim().toUpperCase();
										shop = shopMgr.getShopByCode(value);
										if (shop == null) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "contract.shop.code");
										} else if (!checkShopInOrgAccessByCode(value)) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "contract.shop.code");
											shop = null;
										} else if (!shop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "contract.shop.code");
											shop = null;
										} /*
										 * else { // kiem tra don vi, don vi con
										 * da thuoc bien ban kiem ke List<Long>
										 * lstShopId = new ArrayList<Long>();
										 * lstShopId.add(shop.getId());
										 * List<EquipRecordShopVO> lstTmp =
										 * equipRecordMgr
										 * .getListShopInRecord(id, lstShopId,
										 * false); if (lstTmp != null &&
										 * lstTmp.size() > 0) { message +=
										 * Configuration
										 * .getResourceString(ConstantManager
										 * .VI_LANGUAGE,
										 * "equipment.statistic.record.shop.map.exists"
										 * , lstTmp.get(0).getShopCode()) +
										 * "\n"; shop = null; } }
										 */
									}
								}
							}

							// ** có chọn đơn vị cấp dưới hay ko */
							if (StringUtil.isNullOrEmpty(message)) {
								if (row.size() > 1) {
									String value = row.get(1);
									if (!StringUtil.isNullOrEmpty(value)) {
										if ("X".equals(value.trim().toUpperCase())) {
											isHasChild = true;
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.shop.map.exists.shopchild.errr");
										}
									}
								}
							}

							if (StringUtil.isNullOrEmpty(message) && shop != null) {
								/** bo doan nay. ko import con chau nua */
								//List<Long> lstShopId = new ArrayList<Long>();
								//lstShopId.add(shop.getId());
								//List<Shop> allShopChildrens = shopMgr.getAllShopChildrenByLstId(lstShopId);
								//List<EquipStatisticUnit> lst = null;
								//if (allShopChildrens != null) {
								//lst = new ArrayList<EquipStatisticUnit>();
								//final BigDecimal UNIT_TYPE_SHOP = BigDecimal.valueOf(2l);
								//for (Shop sh: allShopChildrens) {
								//if (sh == null || ShopType.parseValue(sh.getType().getObjectType()) != ShopType.SHOP) {
								//continue;
								//}
								//if (ActiveType.RUNNING != shop.getStatus()) {
								//continue;
								//}
								//EquipStatisticUnit equipSattisticUnit = new EquipStatisticUnit();
								//equipSattisticUnit.setEquipStatisticRecord(record);
								//equipSattisticUnit.setUnitId(sh.getId());
								//equipSattisticUnit.setUnitType(UNIT_TYPE_SHOP);

								//lst.add(equipSattisticUnit);									
								//}
								//if(lst.size()>0){

								//}
								//}
								List<EquipStatisticUnit> lstChild = equipRecordMgr.getListStatisticUnitChild(record.getId(), shop.getId());
								if (isHasChild) {
									List<Long> lstShopIdExcept = new ArrayList<Long>();
									if (lstChild != null && !lstChild.isEmpty()) {
										for (EquipStatisticUnit unit : lstChild) {
											lstShopIdExcept.add(unit.getUnitId());
										}
									}
									List<Shop> lstShopAdd = equipRecordMgr.getListShopForEquipStatisticUnit(shop.getId(), lstShopIdExcept);
									if (lstShopAdd != null && !lstShopAdd.isEmpty()) {
										for (Shop shopAdd : lstShopAdd) {
											EquipStatisticUnit equipSattisticUnit = new EquipStatisticUnit();
											equipSattisticUnit.setEquipStatisticRecord(record);
											equipSattisticUnit.setUnitId(shopAdd.getId());
											if (shopAdd.getType() != null && shopAdd.getType().getSpecificType() != null) {
												if (ShopSpecificType.NPP.getValue().equals(shopAdd.getType().getSpecificType().getValue())) {
													equipSattisticUnit.setUnitType(EquipUnitTypeEnum.NPP.getValue());
												} else if (ShopSpecificType.NPP_MT.getValue().equals(shopAdd.getType().getSpecificType().getValue())) {
													equipSattisticUnit.setUnitType(EquipUnitTypeEnum.MIEN.getValue());
												}
											}
											lst.add(equipSattisticUnit);
										}
										equipRecordMgr.createListEquipStatisticUnit(lst, getLogInfoVO());
									}
								} else {
									if (lstChild != null && !lstChild.isEmpty()) {
										equipRecordMgr.deleteListEquipStatisticUnit(lstChild);
									}
									EquipStatisticUnit equipSattisticUnit = new EquipStatisticUnit();
									equipSattisticUnit.setEquipStatisticRecord(record);
									equipSattisticUnit.setUnitId(shop.getId());
									if (shop.getType() != null && shop.getType().getSpecificType() != null) {
										if (ShopSpecificType.NPP.getValue().equals(shop.getType().getSpecificType().getValue())) {
											equipSattisticUnit.setUnitType(EquipUnitTypeEnum.NPP.getValue());
										} else if (ShopSpecificType.NPP_MT.getValue().equals(shop.getType().getSpecificType().getValue())) {
											equipSattisticUnit.setUnitType(EquipUnitTypeEnum.MIEN.getValue());
										}
									}
									lst.add(equipSattisticUnit);
									equipRecordMgr.createListEquipStatisticUnit(lst, getLogInfoVO());
								}
							} else {
								//message += msgEquip;
								lstFails.add(StringUtil.addFailBean(row, message));
							}
						}
					}
				}

				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_SHOP_CHECK_STATISTIC_LOI);
			} catch (Exception e) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.importShop"), createLogErrorStandard(actionStartTime));
				return SUCCESS;
			}
		} else {
			errMsg = R.getResource("customer.display.program.nodata");
			return SUCCESS;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * import equip
	 * 
	 * @author phuongvm
	 * @since 08/07/2015
	 */
	public String importEquip() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		final String CREATE = "1";
		final String DELETE = "0";
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		final Integer MAX_COL = 3;
		Date startLogDate = DateUtil.now();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, MAX_COL);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				if (id == null || id <= 0) {
					isError = true;
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
					return SUCCESS;
				}
				EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
				if (record == null) {
					message = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
				}
				if (record.getRecordStatus() != 0) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect");
				}
				if (message.length() > 0) {
					isError = true;
					errMsg = message;
					return SUCCESS;
				}
				listShelf = equipRecordMgr.getListShelfByRecordId(id);
				if (!listShelf.isEmpty()) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.has.shelf");
					isError = true;
					return SUCCESS;
				}
				boolean isAdd = true;
				lstView = new ArrayList<CellBean>();
				List<EquipStatisticGroup> lst = new ArrayList<EquipStatisticGroup>();
//				Date now = commonMgr.getSysDate();
//				String username = currentUser.getUserName();
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						int rowSize = row.size();
						Equipment equipment = null;
//						Staff staff = null;
						isAdd = true;
						lst = new ArrayList<EquipStatisticGroup>();
						message = "";
						// ** Ma thiết bị */
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "device.code", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim().toUpperCase();
								equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
								if (equipment == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.equipment.code");
								} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
								} else if (equipment.getUsageStatus() == null || EquipUsageStatus.LOST.getValue().equals(equipment.getUsageStatus().getValue()) || EquipUsageStatus.LIQUIDATED.getValue().equals(equipment.getUsageStatus().getValue())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.kiemke.ex");
								} else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
									message += R.getResource("equipment.manager.equipment.code") + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.vnm") + "\n";
								} else if (!equipRecordMgr.checkEquipInUnitAndGroup(record.getId(), equipment.getId())) {
									message += R.getResource("equipment.statistic.record.has.group.mathietbi.kiemke");
								} else {
									if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
										Customer customer = customerMgr.getCustomerById(equipment.getStockId());
										if (customer == null || !ActiveType.RUNNING.equals(customer.getStatus())) {
											msg += R.getResource("customer.display.program.customer.CusNotRunning");
											msg += ConstantManager.NEW_LINE_CHARACTER;
										}
									}
									if (StringUtil.isNullOrEmpty(msg)) {
										EquipmentFilter<EquipmentManagerVO> equipmentFilter = new EquipmentFilter<EquipmentManagerVO>();
										equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
										if (!StringUtil.isNullOrEmpty(equipment.getCode())) {
											equipmentFilter.setCode(equipment.getCode());
										}
										ObjectVO<EquipmentManagerVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentVOByShopInCMSEx(equipmentFilter);
										if (equipmentDeliverys == null || equipmentDeliverys.getLstObject().isEmpty()) {
											message += R.getResource("equipment.statistic.equip.permission");
										}
									} else {
										message += msg;
									}
								}
							}
						}
						/* ma nvbh */
//						if (StringUtil.isNullOrEmpty(message)) {
//							if (rowSize > 1) {
//								String value = row.get(1);
//								if (!StringUtil.isNullOrEmpty(value)) {
//									String msg = ValidateUtil.validateField(value, "ss.traningplan.salestaff.code", MAX_LENGTH_CODE, ConstantManager.ERR_MAX_LENGTH);
//									if (!StringUtil.isNullOrEmpty(msg)) {
//										message += msg;
//									} else {
//										value = value.trim().toUpperCase();
//										staff = staffMgr.getStaffByCode(value);
//										if (staff != null) {
//											if (!ActiveType.RUNNING.equals(staff.getStatus())) {
//												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "imp.tuyen.clmn.error.maNVBH.stop") + "\n";
//											} else if (staff.getStaffType() != null
//													&& (staff.getStaffType().getSpecificType() == null || (!StaffSpecificType.STAFF.getValue().equals(staff.getStaffType().getSpecificType().getValue()) && !StaffSpecificType.GSMT.getValue().equals(
//															staff.getStaffType().getSpecificType().getValue())))) {
//												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "imp.tuyen.clmn.error.maNVBH.nvbh.gsmt.gska");
//											} else {
//												Long equipShopId = -1L;
//												if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
//													Customer c = customerMgr.getCustomerById(equipment.getStockId());
//													if (c != null && c.getShop() != null) {
//														equipShopId = c.getShop().getId();
//													}
//												} else if (EquipStockTotalType.KHO.equals(equipment.getStockType())) {
//													EquipStock equipStock = equipmentManagerMgr.getEquipStockById(equipment.getStockId());
//													if (equipStock != null && equipStock.getShop() != null) {
//														equipShopId = equipStock.getShop().getId();
//													}
//												}
//												if (!equipRecordMgr.isStaffBelongShop(staff.getId(), equipShopId)) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.staff.equip.err.shop") + "\n";
//												}
//											}
//										} else {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "aclong.im.staff.code.not.avai") + "\n";
//										}
//									}
//								}
//							}
//						}
						// ** Trạng thái : 0: xóa,    1 : thệm mới */
						if (StringUtil.isNullOrEmpty(message)) {
							if (rowSize > 6) {
								String value = row.get(6);
								String msg = ValidateUtil.validateField(value, "equipment.statistic.import.equip.action", 200, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									if (CREATE.equals(value.trim())) {
										isAdd = true;
									} else if (DELETE.equals(value.trim())) {
										isAdd = false;
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.shop.map.exists.shopchild.err.trangthai");
									}
								}
							}
						}

						if (StringUtil.isNullOrEmpty(message)) {
							EquipStatisticGroup temp = equipRecordMgr.getEquipStatisticGroupById(record.getId(), equipment.getId(), EquipObjectType.EQUIP);
							if (isAdd) {
								if (temp == null) {
									EquipStatisticGroup __new = new EquipStatisticGroup();
									__new.setEquipStatisticRecord(record);
									__new.setObjectId(equipment.getId());
									__new.setObjectType(EquipObjectType.EQUIP);
									lst.add(__new);
									equipRecordMgr.createListEquipStatisticGroup(lst, getLogInfoVO());
								}
							} else {
								if (temp != null) {
									equipRecordMgr.deleteEquipStatisticGroup(record.getId(), equipment.getId(), EquipObjectType.EQUIP);
								}
							}
//							if (staff != null) {
//								EquipStatisticStaff essCheck = equipRecordMgr.getEquipStatisticStaff(record.getId(), null, equipment.getId());
//								if (isAdd) {
//									if (essCheck == null) {
//										EquipStatisticStaff entity = new EquipStatisticStaff();
//										entity.setStaff(staff);
//										entity.setEquipment(equipment);
//										entity.setStockId(equipment.getStockId());
//										entity.setStockType(equipment.getStockType());
//										entity.setEquipStatisticRecord(record);
//										entity.setCreateDate(now);
//										entity.setCreateUser(username);
//										entity = equipRecordMgr.createEquipStatisticStaff(entity);
//									} else {
//										essCheck.setUpdateDate(now);
//										essCheck.setUpdateUser(username);
//										essCheck.setStaff(staff);
//										commonMgr.updateEntity(essCheck);
//									}
//
//								} else if (!isAdd && essCheck != null) {
//									equipRecordMgr.deleteEquipStatisticStaff(record.getId(), staff.getId(), equipment.getId());
//								}
//							} else {
//								equipRecordMgr.deleteEquipStatisticStaff(record.getId(), null, equipment.getId());
//							}
						} else {
							//message += msgEquip;
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_EQUIP_CHECK_STATISTIC_LOI);
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.importEquip"), createLogErrorStandard(startLogDate));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return SUCCESS;
			}
		} else {
			errMsg = R.getResource("customer.display.program.nodata");
			return SUCCESS;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	public String checkLikeRow(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).trim().toUpperCase().equals(row2.get(0).trim().toUpperCase())) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
						} else {
							message += ", " + (k + 2);
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau.thiet.bi");
			}
		}
		return message;
	}

	/**
	 * check trung khi import kiemke thiet bi so lan
	 * 
	 * @author phuongvm
	 * @since 20/05/2015
	 */
	public String checkLikeRowEx(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).trim().toUpperCase().equals(row2.get(0).trim().toUpperCase()) && row1.get(1).trim().toUpperCase().equals(row2.get(1).trim().toUpperCase())
							&& row1.get(2).trim().toUpperCase().equals(row2.get(2).trim().toUpperCase())) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
						} else {
							message += ", " + (k + 2);
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
			}
		}
		return message;
	}

	/**
	 * check trung khi import kiemke uke
	 * 
	 * @author phuongvm
	 * @since 25/05/2015
	 */
	public String checkLikeRowExUke(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).trim().toUpperCase().equals(row2.get(0).trim().toUpperCase()) && row1.get(1).trim().toUpperCase().equals(row2.get(1).trim().toUpperCase())
							&& row1.get(3).trim().toUpperCase().equals(row2.get(3).trim().toUpperCase()) && row1.get(2).trim().toUpperCase().equals(row2.get(2).trim().toUpperCase())) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
						} else {
							message += ", " + (k + 2);
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
			}
		}
		return message;
	}

	/**
	 * check trung khi import khach hang
	 * 
	 * @author phuongvm
	 * @since 20/05/2015
	 */
	public String checkDupRow(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).trim().toUpperCase().equals(row2.get(0).trim().toUpperCase()) && row1.get(1).trim().toUpperCase().equals(row2.get(1).trim().toUpperCase())) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
						} else {
							message += ", " + (k + 2);
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
			}
		}
		return message;
	}

	/**
	 * import group so lan
	 * 
	 * @author phuongvm
	 * @since 24/04/2015
	 */
	public String importGroupSoLan() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 3);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				if (id == null || id <= 0) {
					isError = true;
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
					return SUCCESS;
				}
				EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
				if (record == null) {
					message = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
				}
				if (!EquipmentStatisticRecordStatus.RUNNING.getValue().equals(record.getRecordStatus())) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect");
				}
				if (message.length() > 0) {
					isError = true;
					errMsg = message;
					return SUCCESS;
				}
				Date now = commonMgr.getSysDate();
				String username = currentUser.getUserName();
				Staff staff = staffMgr.getStaffByCode(username);
				lstView = new ArrayList<CellBean>();
				List<EquipStatisticRecDtl> lst = new ArrayList<EquipStatisticRecDtl>();
				EquipStatisticFilter filter = new EquipStatisticFilter();
				filter.setRecordId(record.getId());
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						Equipment equipment = null;
						Integer status = EquipStatisticStatus.LOST.getValue();
						Integer quantity = null;
						message = "";
						message = checkLikeRowEx(lstData, i);
						if (!StringUtil.isNullOrEmpty(message)) {
							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
							continue;
						}
						// ** Ma thiết bị */
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "device.code", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim().toUpperCase();
								equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
								if (equipment == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.equipment.code");
								} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
								} /*
								 * else if
								 * (!EquipTradeStatus.NOT_TRADE.getValue(
								 * ).equals(equipment.getTradeStatus())) {
								 * //trade_status = 0 thi moi cho lap bien ban
								 * message +=
								 * Configuration.getResourceString(ConstantManager
								 * .VI_LANGUAGE,
								 * "equipment.row.invalid.makh.not.in.kho.manpp.err2.psc.kk"
								 * ); }
								 */else if (!EquipUsageStatus.SHOWING_REPAIR.getValue().equals(equipment.getUsageStatus().getValue()) && !EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.kiemke");
								} else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
									message += R.getResource("equipment.manager.equipment.code") + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.vnm") + "\n";
								} else if (!equipRecordMgr.checkEquipInUnitAndGroup(record.getId(), equipment.getId())) {
									message += R.getResource("equipment.statistic.record.has.group.mathietbi.kiemke");
								}
							}
						}
						// ** Hien con */
						if (row.size() > 1) {
							String value = row.get(1);
							value = value.trim().toUpperCase();
							if (!StringUtil.isNullOrEmpty(value)) {
								if ("X".equals(value)) {
									status = EquipStatisticStatus.STILL.getValue();
								} else {
									message += R.getResource("equipment.manager.equipment.statistic.hiencon.err");
								}
							}
						}
						/* lan kiem ke */
						if (row.size() > 2) {
							String value = row.get(2);
							String msg = ValidateUtil.validateField(value, "equipment.manager.customer.lankiemke", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								Integer __value, _quan = null;
								try {
									__value = Integer.parseInt(value.trim());
									_quan = Integer.parseInt(record.getQuantity());
									if (__value != null) {
										if (__value < 1 || __value > _quan) {
											message += R.getResource("equipment.statistic.solan.integer2");
										} else {
											quantity = __value;
										}
									}
								} catch (Exception e) {
									message += R.getResource("equipment.statistic.solan.integer");
								}
							}
						}

						if (StringUtil.isNullOrEmpty(message)) {
							EquipStatisticRecDtl equipStatisticRecDtl = new EquipStatisticRecDtl();
							equipStatisticRecDtl.setCreateDate(now);
							equipStatisticRecDtl.setCreateUser(username);
							equipStatisticRecDtl.setEquipStatisticRecord(record);
							equipStatisticRecDtl.setObjectId(equipment.getId());
							equipStatisticRecDtl.setObjectStockId(equipment.getStockId());
							equipStatisticRecDtl.setObjectStockType(equipment.getStockType().getValue());
							equipStatisticRecDtl.setObjectType(EQUIPGROUP);
							Shop shop = equipRecordMgr.getShopInEquip(equipment.getStockId());
							if (shop != null) {
								equipStatisticRecDtl.setShop(shop);
							}
							equipStatisticRecDtl.setStaff(staff);
							equipStatisticRecDtl.setStatisticTime(quantity);
							equipStatisticRecDtl.setStatus(status);
							if (EquipStatisticStatus.STILL.getValue().equals(equipStatisticRecDtl.getStatus())) {
								equipStatisticRecDtl.setQuantity(1);
								equipStatisticRecDtl.setQuantityActually(1);
							} else {
								equipStatisticRecDtl.setQuantity(1);
								equipStatisticRecDtl.setQuantityActually(0);
							}
							equipStatisticRecDtl.setStatisticDate(now);
							filter.setEquipId(equipment.getId());
							filter.setStepCheck(quantity);
							EquipStatisticRecDtl equipStatisticRecDtlTemp = equipRecordMgr.geEquipStatisticRecDtlByFilter(filter);
							if (equipStatisticRecDtlTemp != null) {
								equipStatisticRecDtlTemp.setUpdateDate(now);
								equipStatisticRecDtlTemp.setUpdateUser(username);
								equipStatisticRecDtlTemp.setStaff(staff);
								equipStatisticRecDtlTemp.setStatus(status);
								equipStatisticRecDtlTemp.setStatisticDate(now);
								if (EquipStatisticStatus.STILL.getValue().equals(equipStatisticRecDtlTemp.getStatus())) {
									equipStatisticRecDtlTemp.setQuantity(1);
									equipStatisticRecDtlTemp.setQuantityActually(1);
								} else {
									equipStatisticRecDtlTemp.setQuantity(1);
									equipStatisticRecDtlTemp.setQuantityActually(0);
								}
								equipRecordMgr.updateEquipStatisticRecDetail(equipStatisticRecDtlTemp);
							} else {
								lst.add(equipStatisticRecDtl);
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				if (!lst.isEmpty()) {
					equipRecordMgr.createListEquipStatisticRecDetail(lst);
				}

				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_EQUIP_STATISTIC_GROUP_TIME_FAIL);
			} catch (Exception e) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.importGroupSoLan"), createLogErrorStandard(actionStartTime));
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * import group tuyen
	 * 
	 * @author phuongvm
	 * @since 27/04/2015
	 */
	public String importGroupTuyen() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 3);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				if (id == null || id <= 0) {
					isError = true;
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
					return SUCCESS;
				}
				EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
				if (record == null) {
					message = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
				}
				if (!EquipmentStatisticRecordStatus.RUNNING.getValue().equals(record.getRecordStatus())) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect");
				}
				if (message.length() > 0) {
					isError = true;
					errMsg = message;
					return SUCCESS;
				}
				Date now = commonMgr.getSysDate();
				String username = currentUser.getUserName();
				Staff staff = staffMgr.getStaffByCode(username);
				lstView = new ArrayList<CellBean>();
				List<EquipStatisticRecDtl> lst = new ArrayList<EquipStatisticRecDtl>();
				Calendar cal = Calendar.getInstance();
				EquipStatisticFilter filter = new EquipStatisticFilter();
				filter.setRecordId(record.getId());
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						Equipment equipment = null;
						Integer status = EquipStatisticStatus.LOST.getValue();
						Date date = null;
						String dayOfWeek = "";
						message = "";
						message = checkLikeRow(lstData, i);
						if (!StringUtil.isNullOrEmpty(message)) {
							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
							continue;
						}
						// ** Ma thiết bị */
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "device.code", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
								value = value.trim().toUpperCase();
								equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
								if (equipment == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.equipment.code");
								} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
								} /*
								 * else if
								 * (!EquipTradeStatus.NOT_TRADE.getValue(
								 * ).equals(equipment.getTradeStatus())) {
								 * //trade_status = 0 thi moi cho lap bien ban
								 * message +=
								 * Configuration.getResourceString(ConstantManager
								 * .VI_LANGUAGE,
								 * "equipment.row.invalid.makh.not.in.kho.manpp.err2.psc.kk"
								 * ); }
								 */else if (!EquipUsageStatus.SHOWING_REPAIR.getValue().equals(equipment.getUsageStatus().getValue()) && !EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.kiemke");
								} else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
									message += R.getResource("equipment.manager.equipment.code") + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.vnm") + "\n";
								} else if (!equipRecordMgr.checkEquipInUnitAndGroup(record.getId(), equipment.getId())) {
									message += R.getResource("equipment.statistic.record.has.group.mathietbi.kiemke");
								}
							} else {
								value = value.trim().toUpperCase();
								equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
								if (equipment == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.manager.equipment.code");
								} else if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
								} /*
								 * else if
								 * (!EquipTradeStatus.NOT_TRADE.getValue(
								 * ).equals(equipment.getTradeStatus())) {
								 * //trade_status = 0 thi moi cho lap bien ban
								 * message +=
								 * Configuration.getResourceString(ConstantManager
								 * .VI_LANGUAGE,
								 * "equipment.row.invalid.makh.not.in.kho.manpp.err2.psc.kk"
								 * ); }
								 */else if (!EquipUsageStatus.SHOWING_REPAIR.getValue().equals(equipment.getUsageStatus().getValue()) && !EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.kiemke");
								} else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
									message += R.getResource("equipment.manager.equipment.code") + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.vnm") + "\n";
								} else if (!equipRecordMgr.checkEquipInUnitAndGroup(record.getId(), equipment.getId())) {
									message += R.getResource("equipment.statistic.record.has.group.mathietbi.kiemke");
								}
							}
						}
						// ** Hien con */
						if (row.size() > 1) {
							String value = row.get(1);
							value = value.trim().toUpperCase();
							if (!StringUtil.isNullOrEmpty(value)) {
								if ("X".equals(value)) {
									status = EquipStatisticStatus.STILL.getValue();
								} else {
									message += R.getResource("equipment.manager.equipment.statistic.hiencon.err");
								}
							}
						}
						/* ng?�y kiem ke */
						if (row.size() > 2) {
							String value = row.get(2);
							value = value.trim();
							String msg = ValidateUtil.validateField(value, "equipment.manager.customer.ngaykiemke", 12, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								date = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								if (date != null) {
									if (date != null && record.getToDate() != null && record.getFromDate() != null
											&& (DateUtil.compareDateWithoutTime(date, record.getToDate()) == 1 || DateUtil.compareDateWithoutTime(date, record.getFromDate()) == -1)) {
										message += R.getResource("equipment.statistic.record.has.group.mathietbi.kiemke.ngaykiemke.err");
									} else {
										cal.setTime(date);
										if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
											dayOfWeek = "CN";
										} else {
											dayOfWeek = "T" + cal.get(Calendar.DAY_OF_WEEK);
										}
									}
								}
							}
						}

						if (StringUtil.isNullOrEmpty(message) && equipment != null) {
							EquipStatisticRecDtl equipStatisticRecDtl = new EquipStatisticRecDtl();
							equipStatisticRecDtl.setCreateDate(now);
							equipStatisticRecDtl.setCreateUser(username);
							equipStatisticRecDtl.setEquipStatisticRecord(record);
							equipStatisticRecDtl.setObjectId(equipment.getId());
							equipStatisticRecDtl.setObjectStockId(equipment.getStockId());
							equipStatisticRecDtl.setObjectStockType(equipment.getStockType().getValue());
							equipStatisticRecDtl.setObjectType(EQUIPGROUP);
							Shop shop = equipRecordMgr.getShopInEquip(equipment.getStockId());
							if (shop != null) {
								equipStatisticRecDtl.setShop(shop);
							}
							equipStatisticRecDtl.setStaff(staff);
							equipStatisticRecDtl.setStatisticDate(date);
							equipStatisticRecDtl.setDateInWeek(dayOfWeek);
							equipStatisticRecDtl.setStatus(status);
							if (EquipStatisticStatus.STILL.getValue().equals(equipStatisticRecDtl.getStatus())) {
								equipStatisticRecDtl.setQuantity(1);
								equipStatisticRecDtl.setQuantityActually(1);
							} else {
								equipStatisticRecDtl.setQuantity(1);
								equipStatisticRecDtl.setQuantityActually(0);
							}
							filter.setEquipId(equipment.getId());
							filter.setStatisticDate(date);
							EquipStatisticRecDtl equipStatisticRecDtlTemp = equipRecordMgr.geEquipStatisticRecDtlByFilter(filter);
							if (equipStatisticRecDtlTemp != null) {
								equipStatisticRecDtlTemp.setUpdateDate(now);
								equipStatisticRecDtlTemp.setUpdateUser(username);
								equipStatisticRecDtlTemp.setStaff(staff);
								equipStatisticRecDtlTemp.setStatisticDate(date);
								equipStatisticRecDtlTemp.setDateInWeek(dayOfWeek);
								equipStatisticRecDtlTemp.setStatus(status);
								if (EquipStatisticStatus.STILL.getValue().equals(equipStatisticRecDtlTemp.getStatus())) {
									equipStatisticRecDtlTemp.setQuantity(1);
									equipStatisticRecDtlTemp.setQuantityActually(1);
								} else {
									equipStatisticRecDtlTemp.setQuantity(1);
									equipStatisticRecDtlTemp.setQuantityActually(0);
								}
								equipRecordMgr.updateEquipStatisticRecDetail(equipStatisticRecDtlTemp);
							} else {
								lst.add(equipStatisticRecDtl);
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				if (!lst.isEmpty()) {
					equipRecordMgr.createListEquipStatisticRecDetail(lst);
				}

				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_EQUIP_STATISTIC_GROUP_ROUTE_FAIL);
			} catch (Exception e) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.importGroupTuyen"), createLogErrorStandard(actionStartTime));
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * import u ke so lan
	 * 
	 * @author phuongvm
	 * @since 27/04/2015
	 */
	public String importShelfSoLan() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 4);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				if (id == null || id <= 0) {
					isError = true;
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
					return SUCCESS;
				}
				EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
				if (record == null) {
					message = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
				}
				if (!EquipmentStatisticRecordStatus.RUNNING.getValue().equals(record.getRecordStatus())) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect");
				}
				if (message.length() > 0) {
					isError = true;
					errMsg = message;
					return SUCCESS;
				}
				Date now = commonMgr.getSysDate();
				String username = currentUser.getUserName();
				Staff staff = staffMgr.getStaffByCode(username);
				lstView = new ArrayList<CellBean>();
				List<EquipStatisticRecDtl> lst = new ArrayList<EquipStatisticRecDtl>();
				EquipStatisticFilter filter = new EquipStatisticFilter();
				ProductFilter filterPro = new ProductFilter();
				filter.setRecordId(record.getId());
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						Shop shop = null;
						Integer status = EquipStatisticStatus.INVENTORIED.getValue();
						Integer stepCheck = null;
						Integer quantity = null;
						Product product = null;
						message = "";
						message = checkLikeRowExUke(lstData, i);
						if (!StringUtil.isNullOrEmpty(message)) {
							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
							continue;
						}
						// ** Ma don vi */
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "catalog.unit.tree.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg + "\n";
							} else {
								value = value.trim().toUpperCase();
								shop = shopMgr.getShopByCode(value);
								if (shop == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.unit.tree.code");
								} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.unit.tree.code");
								} else if (!equipRecordMgr.checkExistInEquipStatisticUnit(record.getId(), shop.getId())) {
									message += R.getResource("equipment.statistic.unit.err");
								}
							}
						}
						// ** Ma u ke */
						if (row.size() > 1) {
							String value = row.get(1);
							String msg = ValidateUtil.validateField(value, "equipment.statistic.ma.u.ke", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim().toUpperCase();
								filterPro.setProductCode(value);
								product = productMgr.getProductByFilter(filterPro);
								if (product == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.statistic.ma.u.ke");
								} else if (!StatusType.ACTIVE.getValue().equals(product.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.statistic.ma.u.ke");
								} else if (!equipRecordMgr.checkExistInEquipStatisticGroup(record.getId(), product.getId())) {
									message += R.getResource("equipment.statistic.u.ke.err");
								}
							}
						}
						/* So luong */
						if (row.size() > 2) {
							String value = row.get(2);
							String msg = ValidateUtil.validateField(value, "equipment.so.luong", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								Integer __value = null;
								try {
									__value = Integer.parseInt(value.trim());
									if (__value != null) {
										if (__value < 0) {
											message += R.getResource("common.integer.zero", R.getResource("equipment.so.luong"));
										} else {
											quantity = __value;
										}
									}
								} catch (Exception e) {
									message += R.getResource("common.integer.zero", R.getResource("equipment.so.luong"));
								}
							}
						}
						/* lan kiem ke */
						if (row.size() > 3) {
							String value = row.get(3);
							String msg = ValidateUtil.validateField(value, "equipment.manager.customer.lankiemke", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								Integer __value, _quan = null;
								try {
									__value = Integer.parseInt(value.trim());
									_quan = Integer.parseInt(record.getQuantity());
									if (__value != null) {
										if (__value < 1 || __value > _quan) {
											message += R.getResource("equipment.statistic.solan.integer2");
										} else {
											stepCheck = __value;
										}
									}
								} catch (Exception e) {
									message += R.getResource("equipment.statistic.solan.integer");
								}
							}
						}

						if (StringUtil.isNullOrEmpty(message)) {
							EquipStatisticRecDtl equipStatisticRecDtl = new EquipStatisticRecDtl();
							equipStatisticRecDtl.setCreateDate(now);
							equipStatisticRecDtl.setCreateUser(username);
							equipStatisticRecDtl.setEquipStatisticRecord(record);
							equipStatisticRecDtl.setObjectId(product.getId());
							equipStatisticRecDtl.setObjectType(SHELF);
							equipStatisticRecDtl.setShop(shop);
							equipStatisticRecDtl.setStaff(staff);
							equipStatisticRecDtl.setStatisticDate(now);
							equipStatisticRecDtl.setStatisticTime(stepCheck);
							equipStatisticRecDtl.setStatus(status);
							StockTotal stockTotal = stockMgr.getStockTotalByProductAndOwner(product.getId(), StockObjectType.SHOP, shop.getId(), null);
							if (stockTotal != null) {
								equipStatisticRecDtl.setObjectStockId(stockTotal.getWarehouse().getId());
								equipStatisticRecDtl.setObjectStockType(StockObjectType.SHOP.getValue());
								equipStatisticRecDtl.setQuantity(stockTotal.getQuantity());
								equipStatisticRecDtl.setQuantityActually(quantity);
								filter.setEquipId(product.getId());
								filter.setStepCheck(stepCheck);
								if (shop != null) {
									filter.setShopId(shop.getId());
								}
								EquipStatisticRecDtl equipStatisticRecDtlTemp = equipRecordMgr.geEquipStatisticRecDtlByFilter(filter);
								if (equipStatisticRecDtlTemp != null) {
									equipStatisticRecDtlTemp.setUpdateDate(now);
									equipStatisticRecDtlTemp.setUpdateUser(username);
									equipStatisticRecDtlTemp.setStaff(staff);
									equipStatisticRecDtlTemp.setStatus(status);
									equipStatisticRecDtlTemp.setStatisticDate(now);
									equipStatisticRecDtlTemp.setQuantityActually(quantity);
									equipStatisticRecDtlTemp.setQuantity(stockTotal.getQuantity());
									equipRecordMgr.updateEquipStatisticRecDetail(equipStatisticRecDtlTemp);
								} else {
									lst.add(equipStatisticRecDtl);
								}
							} else {
								lstFails.add(StringUtil.addFailBean(row, R.getResource("equipment.statistic.uke.is.not.shop")));
							}

						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				if (!lst.isEmpty()) {
					equipRecordMgr.createListEquipStatisticRecDetail(lst);
				}

				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_EQUIP_STATISTIC_SHELF_TIME_FAIL);
			} catch (Exception e) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.importShelfSoLan"), createLogErrorStandard(actionStartTime));
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * import u ke tuyen
	 * 
	 * @author phuongvm
	 * @since 04/05/2015
	 */
	public String importShelfTuyen() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 4);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				if (id == null || id <= 0) {
					isError = true;
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
					return SUCCESS;
				}
				EquipStatisticRecord record = equipRecordMgr.getEquipStatisticRecordById(id);
				if (record == null) {
					message = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record"));
				}
				if (!EquipmentStatisticRecordStatus.RUNNING.getValue().equals(record.getRecordStatus())) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.statistic.record.incorrect");
				}
				if (message.length() > 0) {
					isError = true;
					errMsg = message;
					return SUCCESS;
				}
				Date now = commonMgr.getSysDate();
				String username = currentUser.getUserName();
				Staff staff = staffMgr.getStaffByCode(username);
				lstView = new ArrayList<CellBean>();
				List<EquipStatisticRecDtl> lst = new ArrayList<EquipStatisticRecDtl>();
				Calendar cal = Calendar.getInstance();
				EquipStatisticFilter filter = new EquipStatisticFilter();
				ProductFilter filterPro = new ProductFilter();
				filter.setRecordId(record.getId());
				for (int i = 0, size = lstData.size(); i < size; i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						Shop shop = null;
						Integer status = EquipStatisticStatus.INVENTORIED.getValue();
						Integer quantity = null;
						Product product = null;
						Date date = null;
						String dayOfWeek = "";
						message = "";
						message = checkLikeRowExUke(lstData, i);
						if (!StringUtil.isNullOrEmpty(message)) {
							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
							continue;
						}
						// ** Ma don vi */
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "catalog.unit.tree.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg + "\n";
							} else {
								value = value.trim().toUpperCase();
								shop = shopMgr.getShopByCode(value);
								if (shop == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.unit.tree.code");
								} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.unit.tree.code");
								} else if (!equipRecordMgr.checkExistInEquipStatisticUnit(record.getId(), shop.getId())) {
									message += R.getResource("equipment.statistic.unit.err");
								}
							}
						}
						// ** Ma u ke */
						if (row.size() > 1) {
							String value = row.get(1);
							String msg = ValidateUtil.validateField(value, "equipment.statistic.ma.u.ke", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim().toUpperCase();
								filterPro.setProductCode(value);
								product = productMgr.getProductByFilter(filterPro);
								if (product == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equipment.statistic.ma.u.ke");
								} else if (!StatusType.ACTIVE.getValue().equals(product.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.statistic.ma.u.ke");
								} else if (!equipRecordMgr.checkExistInEquipStatisticGroup(record.getId(), product.getId())) {
									message += R.getResource("equipment.statistic.u.ke.err");
								}
							}
						}
						/* So luong */
						if (row.size() > 2) {
							String value = row.get(2);
							String msg = ValidateUtil.validateField(value, "equipment.so.luong", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								Integer __value = null;
								try {
									__value = Integer.parseInt(value.trim());
									if (__value != null) {
										if (__value < 0) {
											message += R.getResource("common.integer.zero", R.getResource("equipment.so.luong"));
										} else {
											quantity = __value;
										}
									}
								} catch (Exception e) {
									message += R.getResource("common.integer.zero", R.getResource("equipment.so.luong"));
								}
							}
						}
						/* ngay kiem ke */
						if (row.size() > 3) {
							String value = row.get(3);
							value = value.trim();
							String msg = ValidateUtil.validateField(value, "equipment.manager.customer.ngaykiemke", 12, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INVALID_DATE);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								date = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								if (date != null) {
									if (date != null && record.getToDate() != null && record.getFromDate() != null
											&& (DateUtil.compareDateWithoutTime(date, record.getToDate()) == 1 || DateUtil.compareDateWithoutTime(date, record.getFromDate()) == -1)) {
										message += R.getResource("equipment.statistic.record.has.group.mathietbi.kiemke.ngaykiemke.err");
									} else {
										cal.setTime(date);
										if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
											dayOfWeek = "CN";
										} else {
											dayOfWeek = "T" + cal.get(Calendar.DAY_OF_WEEK);
										}
									}
								}
							}
						}

						if (StringUtil.isNullOrEmpty(message)) {
							EquipStatisticRecDtl equipStatisticRecDtl = new EquipStatisticRecDtl();
							equipStatisticRecDtl.setCreateDate(now);
							equipStatisticRecDtl.setCreateUser(username);
							equipStatisticRecDtl.setEquipStatisticRecord(record);
							equipStatisticRecDtl.setObjectId(product.getId());
							equipStatisticRecDtl.setObjectType(SHELF);
							equipStatisticRecDtl.setShop(shop);
							equipStatisticRecDtl.setStaff(staff);
							equipStatisticRecDtl.setStatisticDate(date);
							equipStatisticRecDtl.setDateInWeek(dayOfWeek);
							equipStatisticRecDtl.setStatus(status);
							StockTotal stockTotal = stockMgr.getStockTotalByProductAndOwner(product.getId(), StockObjectType.SHOP, shop.getId(), null);
							if (stockTotal != null) {
								equipStatisticRecDtl.setObjectStockId(stockTotal.getWarehouse().getId());
								equipStatisticRecDtl.setObjectStockType(StockObjectType.SHOP.getValue());
								equipStatisticRecDtl.setQuantity(stockTotal.getQuantity());
								equipStatisticRecDtl.setQuantityActually(quantity);
								filter.setEquipId(product.getId());
								if (shop != null) {
									filter.setShopId(shop.getId());
								}
								filter.setStatisticDate(date);
								EquipStatisticRecDtl equipStatisticRecDtlTemp = equipRecordMgr.geEquipStatisticRecDtlByFilter(filter);
								if (equipStatisticRecDtlTemp != null) {
									equipStatisticRecDtlTemp.setUpdateDate(now);
									equipStatisticRecDtlTemp.setUpdateUser(username);
									equipStatisticRecDtlTemp.setStaff(staff);
									equipStatisticRecDtlTemp.setStatisticDate(date);
									equipStatisticRecDtlTemp.setDateInWeek(dayOfWeek);
									equipStatisticRecDtlTemp.setStatus(status);
									equipStatisticRecDtlTemp.setQuantityActually(quantity);
									equipStatisticRecDtlTemp.setQuantity(stockTotal.getQuantity());
									equipRecordMgr.updateEquipStatisticRecDetail(equipStatisticRecDtlTemp);
								} else {
									lst.add(equipStatisticRecDtl);
								}
							} else {
								lstFails.add(StringUtil.addFailBean(row, R.getResource("equipment.statistic.uke.is.not.shop")));
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				if (!lst.isEmpty()) {
					equipRecordMgr.createListEquipStatisticRecDetail(lst);
				}

				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_EQUIP_STATISTIC_SHELF_ROUTE_FAIL);
			} catch (Exception e) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.importShelfTuyen"), createLogErrorStandard(actionStartTime));
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * update hinh anh kiem ke
	 * 
	 * @author phuongvm
	 * @since 16/09/2015
	 */
	public String updateResult() {
		resetToken(result);
		result.put(ERROR, true);
		try {
			final int MAX_LENGTH_NUM_PRODUCT = 3;
			final int MAX_LENGTH_POSM = 6;
			final int MAX_LENGTH_NOTE = 10;
			final int NOT_CHANGE_RESULT_DISPLAY = -1;
			final int RESULT_DISPLAY = 1;
			final int NOT_RESULT_DISPLAY = 0;

			if (id != null) {
				String username = currentUser.getStaffRoot() != null ? currentUser.getStaffRoot().getStaffCode() : null;
				Date now = commonMgr.getSysDate();
				EquipAttachFile img = equipRecordMgr.getEquipAttachFileById(id);
				if (img != null) {
					boolean isChange = false;
					if (resultImg != null && resultImg != NOT_CHANGE_RESULT_DISPLAY && resultImg != img.getResult() && (resultImg == NOT_RESULT_DISPLAY || resultImg == RESULT_DISPLAY)) {
						img.setResult(resultImg);
						isChange = true;
					} else if (resultImg == null) {
						img.setResult(resultImg);
						isChange = true;
					}
					if (isInspeck != null) {
						if (img.getInspectingUser() == null) {
							img.setInspectingDate(now);
							img.setInspectingUser(username);
						}
						img.setIsInspected(isInspeck);
						isChange = true;
					}
					if (numberProduct != null) {
						if (numberProduct.trim().equals("") && img.getNumberProduct() != null) {
							img.setNumberProduct(null);
							isChange = true;
						} else if (numberProduct.trim().length() > 0) {
							String msg = ValidateUtil.validateField(numberProduct, "images.number.product", MAX_LENGTH_NUM_PRODUCT, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								result.put("errMsg", msg);
								return JSON;
							} else if (Integer.valueOf(numberProduct.trim()) > 0) {
								img.setNumberProduct(Integer.valueOf(numberProduct.trim()));
								isChange = true;
							} else {
								result.put("errMsg", R.getResource("common.not.int", R.getResource("images.number.product")));
								return JSON;
							}
						}
					}
					if (strPOSM != null && !strPOSM.equals(img.getPOSM())) {
						if (strPOSM.trim().length() > MAX_LENGTH_POSM) {
							result.put("errMsg", R.getResource("common.maxlength", R.getResource("images.POSM"), MAX_LENGTH_POSM));
							return JSON;
						}
						img.setPOSM(strPOSM.trim());
						isChange = true;
					}
					if (strNoted != null && !strNoted.equals(img.getNote())) {
						if (strNoted.trim().length() > MAX_LENGTH_NOTE) {
							result.put("errMsg", R.getResource("common.maxlength", R.getResource("images.noted"), MAX_LENGTH_NOTE));
							return JSON;
						}
						img.setNote(strNoted.trim());
						isChange = true;
					}
					if (isChange) {
						img.setUpdateUser(username);
						img.setUpdateDate(now);
						equipRecordMgr.updateEquipAttachFile(img);
					}
					result.put(ERROR, false);
				}
			}
		} catch (BusinessException e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipStatisticCheckingAction.updateResult"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	public List<EquipStatisticRecord> getListRecord() {
		return listRecord;
	}

	public void setListRecord(List<EquipStatisticRecord> listRecord) {
		this.listRecord = listRecord;
	}

	public List<EquipGroup> getListEquipGroup() {
		return listEquipGroup;
	}

	public void setListEquipGroup(List<EquipGroup> listEquipGroup) {
		this.listEquipGroup = listEquipGroup;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCheckingCode() {
		return checkingCode;
	}

	public void setCheckingCode(String checkingCode) {
		this.checkingCode = checkingCode;
	}

	public String getCheckingName() {
		return checkingName;
	}

	public void setCheckingName(String checkingName) {
		this.checkingName = checkingName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public List<TreeGridNode<EquipRecordShopVO>> getLstTree() {
		return lstTree;
	}

	public void setLstTree(List<TreeGridNode<EquipRecordShopVO>> lstTree) {
		this.lstTree = lstTree;
	}

	public String getShopSearchCode() {
		return shopSearchCode;
	}

	public void setShopSearchCode(String shopSearchCode) {
		this.shopSearchCode = shopSearchCode;
	}

	public String getShopSearchName() {
		return shopSearchName;
	}

	public void setShopSearchName(String shopSearchName) {
		this.shopSearchName = shopSearchName;
	}

	public EquipStatisticRecord getInstance() {
		return instance;
	}

	public void setInstance(EquipStatisticRecord instance) {
		this.instance = instance;
	}

	public List<Long> getListId() {
		return listId;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public List<EquipmentRecordDetailVO> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<EquipmentRecordDetailVO> listDetail) {
		this.listDetail = listDetail;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDeviceCode() {
		return deviceCode;
	}

	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public Integer getEquipmentStatisticRecordStatus() {
		return equipmentStatisticRecordStatus;
	}

	public void setEquipmentStatisticRecordStatus(Integer equipmentStatisticRecordStatus) {
		this.equipmentStatisticRecordStatus = equipmentStatisticRecordStatus;
	}

	public Long getDetailId() {
		return detailId;
	}

	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}

	public String getDateInfo() {
		return dateInfo;
	}

	public void setDateInfo(String dateInfo) {
		this.dateInfo = dateInfo;
	}

	public Boolean getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(Boolean overwrite) {
		this.overwrite = overwrite;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getEquipCategory() {
		return equipCategory;
	}

	public void setEquipCategory(Long equipCategory) {
		this.equipCategory = equipCategory;
	}

	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}

	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}

	public Float getFromCapacity() {
		return fromCapacity;
	}

	public void setFromCapacity(Float fromCapacity) {
		this.fromCapacity = fromCapacity;
	}

	public Float getToCapacity() {
		return toCapacity;
	}

	public void setToCapacity(Float toCapacity) {
		this.toCapacity = toCapacity;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public List<Product> getListShelf() {
		return listShelf;
	}

	public void setListShelf(List<Product> listShelf) {
		this.listShelf = listShelf;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getCkShop() {
		return ckShop;
	}

	public void setCkShop(Boolean ckShop) {
		this.ckShop = ckShop;
	}

	public Boolean getCkPromotion() {
		return ckPromotion;
	}

	public void setCkPromotion(Boolean ckPromotion) {
		this.ckPromotion = ckPromotion;
	}

	public Boolean getDeleteChild() {
		return deleteChild;
	}

	public void setDeleteChild(Boolean deleteChild) {
		this.deleteChild = deleteChild;
	}

	public List<EquipRecordShopVO> getLstShop() {
		return lstShop;
	}

	public void setLstShop(List<EquipRecordShopVO> lstShop) {
		this.lstShop = lstShop;
	}

	public Integer getSoLan() {
		return soLan;
	}

	public void setSoLan(Integer soLan) {
		this.soLan = soLan;
	}

	public ProductMgr getProductMgr() {
		return productMgr;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Integer getStepCheck() {
		return stepCheck;
	}

	public void setStepCheck(Integer stepCheck) {
		this.stepCheck = stepCheck;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public Long getShelfId() {
		return shelfId;
	}

	public void setShelfId(Long shelfId) {
		this.shelfId = shelfId;
	}

	public Boolean getIsFirstLoad() {
		return isFirstLoad;
	}

	public void setIsFirstLoad(Boolean isFirstLoad) {
		this.isFirstLoad = isFirstLoad;
	}

	public List<Long> getListDetailId() {
		return listDetailId;
	}

	public void setListDetailId(List<Long> listDetailId) {
		this.listDetailId = listDetailId;
	}

	public List<Integer> getListExists() {
		return listExists;
	}

	public void setListExists(List<Integer> listExists) {
		this.listExists = listExists;
	}

	public List<Integer> getListActQty() {
		return listActQty;
	}

	public void setListActQty(List<Integer> listActQty) {
		this.listActQty = listActQty;
	}

	public String getDateInWeek() {
		return dateInWeek;
	}

	public void setDateInWeek(String dateInWeek) {
		this.dateInWeek = dateInWeek;
	}

	public List<EquipmentVO> getListEquip() {
		return listEquip;
	}

	public void setListEquip(List<EquipmentVO> listEquip) {
		this.listEquip = listEquip;
	}

	public String getEquipGroupName() {
		return equipGroupName;
	}

	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}

	public List<Equipment> getListEquipment() {
		return listEquipment;
	}

	public void setListEquipment(List<Equipment> listEquipment) {
		this.listEquipment = listEquipment;
	}

	public Integer getIsCheck() {
		return isCheck;
	}

	public void setIsCheck(Integer isCheck) {
		this.isCheck = isCheck;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Integer getResultImg() {
		return resultImg;
	}

	public void setResultImg(Integer resultImg) {
		this.resultImg = resultImg;
	}

	public Integer getIsInspeck() {
		return isInspeck;
	}

	public void setIsInspeck(Integer isInspeck) {
		this.isInspeck = isInspeck;
	}

	public String getNumberProduct() {
		return numberProduct;
	}

	public void setNumberProduct(String numberProduct) {
		this.numberProduct = numberProduct;
	}

	public String getStrPOSM() {
		return strPOSM;
	}

	public void setStrPOSM(String strPOSM) {
		this.strPOSM = strPOSM;
	}

	public String getStrNoted() {
		return strNoted;
	}

	public void setStrNoted(String strNoted) {
		this.strNoted = strNoted;
	}

	public Integer getStatusCus() {
		return statusCus;
	}

	public void setStatusCus(Integer statusCus) {
		this.statusCus = statusCus;
	}

	public String getStrLstShopId() {
		return strLstShopId;
	}

	public void setStrLstShopId(String strLstShopId) {
		this.strLstShopId = strLstShopId;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

}

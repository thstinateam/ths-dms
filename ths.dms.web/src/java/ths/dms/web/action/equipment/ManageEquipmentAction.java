package ths.dms.web.action.equipment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.TreeGridNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentRepairFormFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentRepairFormVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopViewParentVO;
import ths.dms.core.memcached.MemcachedUtils;

/**
 * Action Quan ly thiet bi
 * 
 * @author hunglm16
 * @since December 14,2014
 */
public class ManageEquipmentAction extends AbstractAction {

	private static final long serialVersionUID = 5330840248556870465L;
	private static final String PAGING = "PAGING";
	private static final String NO_PAGING = "NO_PAGING";
	private static final int FOR_CMS = 1;
	private static final int UN_FOR_CMS = 0;
	
	CommonMgr commonMgr;
	StaffMgr staffMgr;
	ShopMgr shopMgr;
	CustomerMgr customerMgr;
	EquipmentManagerMgr equipmentManagerMgr;

	private String arrShop;
	
	
	@Override
	public void prepare() throws Exception {
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		return SUCCESS;
	}

	/**
	 * get list equip stock vo by filter
	 * 
	 * @author liemtpt
	 * @since 23/03/2015
	 * @description Chuc nang lay danh sach kho F9
	 * 
	 * @author hunglm16
	 * @since May 26, 2015
	 * @description ra soat va update
	 * */
	public String getListEquipStockVOByFilter(){
		result.put("rows", new ArrayList<EquipStockVO>());
		result.put("total", 0);
		try {
			EquipStockFilter filter = new EquipStockFilter();
			//set cac tham so tim kiem
			KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			if (currentUser.getStaffRoot() != null && flagForStaffRoot != null && flagForStaffRoot == FOR_CMS) {
				filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			}
			if (currentUser.getShopRoot() != null) {
				filter.setShopRoot(currentUser.getShopRoot().getShopId());
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setShopCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setShopName(name);
			}
			ObjectVO<EquipStockVO> objVO = equipmentManagerMgr.getListEquipStockVOByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ManageEquipmentAction.getListEquipStockVOByFilter()" + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Tim kiem thiet bi
	 * 
	 * @author hunglm16
	 * @since December 17,2014
	 * @description Chuc nang Danh Sach Thiet Bi
	 * */
	public String searchListEquipmentVO() {
		result.put("rows", new ArrayList<EquipmentVO>());
		result.put("total", 0);
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			//set cac tham so tim kiem
			KPaging<EquipmentVO> kPaging = new KPaging<EquipmentVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			//Xu ly cho danh sach don vi
			List<Long> arrShopId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(arrShop)) {
				for (String valShopId: arrShop.split(",")) {
					if (StringUtil.isNumberInt(valShopId) && checkShopInOrgAccessById(Long.valueOf(valShopId))) {
						arrShopId.add(Long.valueOf(valShopId));
					}
				}
			}
			if (arrShopId == null || arrShopId.isEmpty()) {
				arrShopId.add(currentUser.getShopRoot().getShopId());
			}
			filter.setLstShopId(arrShopId);
			//goi ham thuc thi tim kiem
			if (currentUser.getStaffRoot() != null) {
				filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			}
			if (status != null) {
				filter.setStatus(status);
			} else {
				filter.setStatus(StatusType.ACTIVE.getValue());
			}
			if (equipGroupId != null && (equipGroupId > 0 || equipGroupId.longValue() == - 2)) {
				filter.setEquipGroupId(equipGroupId);
			}
			if (tradeType != null && tradeType > -1) {
				filter.setTradeType(tradeType);
			}
			if (tradeStatus != null && tradeStatus > -1) {
				filter.setTradeStatus(tradeStatus);
			}
			if (usageStatus != null && usageStatus > -1) {
				filter.setUsageStatus(usageStatus);
			}
			
			if (equipProviderId != null && (equipProviderId > 0 || equipProviderId.longValue() == -2 )) {
				filter.setEquipProviderId(equipProviderId);
			}
			if (equipCategoryId != null && equipCategoryId > 0) {
				filter.setEquipCategoryId(equipCategoryId);
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				filter.setSeriNumber(seriNumber);
			}
			if (!StringUtil.isNullOrEmpty(healthStatus)) {
				filter.setHealthStatus(healthStatus);
			}
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if(!StringUtil.isNullOrEmpty(equipImportRecordCode)){
				filter.setEquipImportRecordCode(equipImportRecordCode);
			}
			if(!StringUtil.isNullOrEmpty(stockCode)){
				filter.setStockCode(stockCode);
			}
			if(!StringUtil.isNullOrEmpty(stockName)){
				filter.setStockName(stockName);
			}
			if(!StringUtil.isNullOrEmpty(customerCode)){
				filter.setCustomerCode(customerCode);
			}
			if(!StringUtil.isNullOrEmpty(customerName)){
				filter.setCustomerName(customerName);
			}
			if(!StringUtil.isNullOrEmpty(customerAddress)){
				filter.setCustomerAddress(customerAddress);
			}
			
			filter.setShopRootId(currentUser.getShopRoot() == null ? null : currentUser.getShopRoot().getShopId());
			filter.setStaffRootId(currentUser.getStaffRoot() == null ? null :currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken() == null ? null : currentUser.getRoleToken().getRoleId());
			
			ObjectVO<EquipmentVO> objVO = equipmentManagerMgr.getListEquipmentByFilterNew(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ManageEquipmentAction.search()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay danh sach nhom thiet bi theo loai thiet bi
	 * 
	 * @author hunglm16
	 * @since December 17,2014
	 * @description Chuc nang Danh Sach Thiet Bi
	 * */
	public String getListEquipGroupVOByCategry() {
		result.put("rows", new ArrayList<EquipmentVO>());
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setStatus(StatusType.ACTIVE.getValue());
			if (equipCategoryId != null && equipCategoryId > 0) {
				filter.setEquipCategoryId(equipCategoryId);
			}
			ObjectVO<EquipmentVO> obj = equipmentManagerMgr.searchEquipmentGroupVObyFilter(filter);
			if (obj != null && obj.getLstObject() != null && !obj.getLstObject().isEmpty()) {
				result.put("rows", obj.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "getListEquipGroupVOByCategry()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem lich thiet bi
	 * 
	 * @author hunglm16
	 * @since December 17,2014
	 * @description Chuc nang Danh Sach Thiet Bi
	 * */
	public String getEquipRFVOByListEquipment() {
		result.put("rows", new ArrayList<EquipmentRepairFormVO>());
		result.put("total", 0);
		try {
			EquipmentRepairFormFilter<EquipmentRepairFormVO> filter = new EquipmentRepairFormFilter<EquipmentRepairFormVO>();
			//set cac tham so tim kiem
			KPaging<EquipmentRepairFormVO> kPaging = new KPaging<EquipmentRepairFormVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			if (id == null || id <= 0) {
				return JSON;
			}
			filter.setEquipId(id);
			filter.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
			//goi ham thuc thi tim kiem
			ObjectVO<EquipmentRepairFormVO> objVO = equipmentManagerMgr.searchListEquipmentRepairFormVOByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.search()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Xuat Excel theo Tim kiem thiet bi
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * @return Excel
	 * */
	public String exportBySearchEquipmentVO() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			//Xu ly cho danh sach don vi
			List<Long> arrShopId = new ArrayList<Long>();
			if (!StringUtil.isNullOrEmpty(arrShop)) {
				for (String valShopId: arrShop.split(",")) {
					if (StringUtil.isNumberInt(valShopId) && checkShopInOrgAccessById(Long.valueOf(valShopId))) {
						arrShopId.add(Long.valueOf(valShopId));
					}
				}
			}
			if (arrShopId == null || arrShopId.isEmpty()) {
				arrShopId.add(currentUser.getShopRoot().getShopId());
			}
			filter.setLstShopId(arrShopId);
			//set cac tham so tim kiem
			if (currentUser.getStaffRoot()!=null) {
				filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			}
			if (status != null) {
				filter.setStatus(status);
			} else {
				filter.setStatus(StatusType.ACTIVE.getValue());
			}
			if (equipGroupId != null && equipGroupId > 0) {
				filter.setEquipGroupId(equipGroupId);
			}
			if (tradeType != null && tradeType > -1) {
				filter.setTradeType(tradeType);
			}
			if (tradeStatus != null && tradeStatus > -1) {
				filter.setTradeStatus(tradeStatus);
			}
			if (usageStatus != null && usageStatus > -1) {
				filter.setUsageStatus(usageStatus);
			}
			if (equipProviderId != null && equipProviderId > 0) {
				filter.setEquipProviderId(equipProviderId);
			}
			if (equipCategoryId != null && equipCategoryId > 0) {
				filter.setEquipCategoryId(equipCategoryId);
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				filter.setSeriNumber(seriNumber);
			}
			if (!StringUtil.isNullOrEmpty(healthStatus)) {
				filter.setHealthStatus(healthStatus);
			}
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if(!StringUtil.isNullOrEmpty(equipImportRecordCode)){
				filter.setEquipImportRecordCode(equipImportRecordCode);
			}
			if(!StringUtil.isNullOrEmpty(stockCode)){
				filter.setStockCode(stockCode);
			}
			if(!StringUtil.isNullOrEmpty(stockName)){
				filter.setStockName(stockName);
			}
			if(!StringUtil.isNullOrEmpty(customerCode)){
				filter.setCustomerCode(customerCode);
			}
			if(!StringUtil.isNullOrEmpty(customerName)){
				filter.setCustomerName(customerName);
			}
			if(!StringUtil.isNullOrEmpty(customerAddress)){
				filter.setCustomerAddress(customerAddress);
			}
			filter.setShopRootId(currentUser.getShopRoot() == null ? null : currentUser.getShopRoot().getShopId());
			filter.setStaffRootId(currentUser.getStaffRoot() == null ? null :currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken() == null ? null : currentUser.getRoleToken().getRoleId());
			ObjectVO<EquipmentVO> objVO = equipmentManagerMgr.getListEquipmentByFilterNew(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				String reportToken = retrieveReportToken(reportCode);
				if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
					return JSON;
				}
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("rows", objVO.getLstObject());
				String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
				String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_LIST_EQUIPMENT_EXPORT;
				templateFileName = templateFileName.replace('/', File.separatorChar);
				String outputName = ConstantManager.EQUIP_LIST_EQUIPMENT_EXPORT + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
				
				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				result.put(ERROR, false);
				
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put("path", outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.err.export.list.equip.isnull"));
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Tai tap tin Excel Import Danh sach thiet bi
	 * 
	 * @author hunglm16
	 * @since December 22,2014
	 * @return Excel
	 * */
	public String downloadTemplateImportListEquip() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			EquipmentFilter<EquipProvider> filterEP = new EquipmentFilter<EquipProvider>();
			filterEP.setStatus(StatusType.ACTIVE.getValue());
			lstEquipProvider = equipmentManagerMgr.getListEquipProviderByFilter(filterEP);
			if (lstEquipProvider == null || lstEquipProvider.isEmpty()) {
				lstEquipProvider = new ArrayList<EquipProvider>();
			} else {
				for (int i = 0, size = lstEquipProvider.size(); i < size; i++) {
					lstEquipProvider.get(i).setCode(lstEquipProvider.get(i).getCodeName());
				}
			}

			List<ApParam> lstApp = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
			if (lstApp == null || lstApp.isEmpty()) {
				lstApp = new ArrayList<ApParam>();
			} else {
				for (int i = 0, size = lstApp.size(); i < size; i++) {
					lstApp.get(i).setApParamCode(lstApp.get(i).getApParamCodeValue());
				}
			}

			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("rows", lstEquipProvider);
			beans.put("lstHealth", lstApp);
			String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
			String templateFileName = folder + ConstantManager.TEMPLATE_IMPORT_EQUIP_LIST_EQUIPMENT_EXPORT;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = "Bieu_Mau_Nhap_Danh_Sach_Thiet_Bi" + ConstantManager.EXPORT_FILE_EXTENSION_EX;
			String exportFileName = (Configuration.getStoreImportRealPath() + outputName).replace('/', File.separatorChar);
			
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			
			result.put(ERROR, false);
			String outputPath = Configuration.getStoreImportDownloadPath() + outputName;
			result.put("path", outputPath);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Nhap Excel Trong Danh sach thiet bi
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * @description Import Excel
	 * */
	public String importEquipListEquip() {
		resetToken(result);
		isError = true;
		totalItem = 0;
		try {
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			if (equipPeriod == null) {
//				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//				return SUCCESS;
//			}
			//Xu ly nghiep vu import file
			lstView = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 8);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				//TODO khoi tao nhung du lieu de validate
				EquipmentFilter<EquipProvider> filterEP = new EquipmentFilter<EquipProvider>();
				filterEP.setStatus(StatusType.ACTIVE.getValue());
				List<EquipProvider> lstEquipProvider = equipmentManagerMgr.getListEquipProviderByFilter(filterEP);
				//Danh sach Tinh Trang Thiet Bi
				List<ApParam> lstHealthStatus = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);

				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(currentUser.getUserName());
				String message = "";
				String msg = "";
				String value = "";
				boolean flag = false;
				EquipmentFilter<BasicVO> filter = new EquipmentFilter<BasicVO>();
				Equipment equipIst = new Equipment();
				for (int i = 0; i < lstData.size(); i++) {
					message = "";
					msg = "";
					totalItem++;
					List<String> row = lstData.get(i);
					flag = false;
					filter = new EquipmentFilter<BasicVO>();
					equipIst = new Equipment();

					if (row != null && row.size() > 5) {
						for (int j = 0; j < row.size(); j++) {
							if (!StringUtil.isNullOrEmpty(row.get(j))) {
								flag = true;
								break;
							}
						}
					}
					if (flag) {
						//Ma Nhom Thiet Bi - equipGroupCode
						value = row.get(0);
						value = value.trim();
						msg = ValidateUtil.validateField(value, "equipment.group.product.equipment.group.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
						} else {
							EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupByCode(value);
							if (equipGroup != null) {
								if (!ActiveType.RUNNING.getValue().equals(equipGroup.getStatus().getValue())) {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon");
								} else {
									filter.setEquipGroupId(equipGroup.getId());
									equipIst.setEquipGroup(equipGroup);
								}
							} else {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined");
							}
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
								message += "\n";
							}
						}
//						if (!StringUtil.isNullOrEmpty(value)) {
//						}
						//So luong - So luong quyet sinh cho so dong tao moi
						value = row.get(1);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.trim();
							if (!StringUtil.isNumberInt(value) || value.length() > 5) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.quantity");
							} else {
								Integer quantity = Integer.valueOf(value);
								if (quantity < 1 || quantity > 9999) {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.quantity");
								} else {
									filter.setQuantity(quantity);
								}
							}
						} else {
							//Bat rong
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.quantity.null");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
							message += "\n";
						}
						//Nguyen Gia - price
						value = row.get(2);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.trim();
							value = value.replaceAll(",", "").trim();
							if (value.length() > 15 || !StringUtil.isNumberInt(value)) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.price.isprice.err");
							} else {
								BigDecimal price = new BigDecimal(value);
								if (price.doubleValue() < 100) {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.price.isprice.err");
								} else {
									filter.setPrice(price);
									equipIst.setPrice(price);
								}
							}
						} else {
							//Bat rong
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.price.null");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
							message += "\n";
						}
						boolean flagDate = true;
						//Ngày het han bao hanh - warrantyExpiredDate
						value = row.get(3);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.trim();
							if (DateUtil.isThisDateValidate(value, DateUtil.DATE_FORMAT_DDMMYYYY)) {
								filter.setWarrantyExpiredDateStr(value);
								equipIst.setWarrantyExpiredDate(DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY));
							} else {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDateStr.format.err");
							}
						} else {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDateStr.format.err.null");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							flagDate = false;
							message += msg;
							message += "\n";
						}
						//Nam san xuat - manufacturingYear
						value = row.get(4);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.trim();
							if (!StringUtil.isNumberInt(value) || value.length() != 4 || Integer.valueOf(value) < 1000 || Integer.valueOf(value) > 9999) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.manufacturingYearStr.format.err");
							} else {
								filter.setYearManufacture(Integer.valueOf(value));
								equipIst.setManufacturingYear(filter.getYearManufacture());
							}
						} else {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.manufacturingYearStr.format.err.null");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							flagDate = false;
							message += msg;
							message += "\n";
						}

						if (flagDate) {
							Integer yearWrran = Integer.valueOf(row.get(3).trim().split("/")[2]);
							if (yearWrran < equipIst.getManufacturingYear()) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDate.manufacturingYear.err");
								message += "\n";
							}
						}
						//NCC (Nha cung cap) - equipProviderCode
						value = row.get(5);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.trim();
							String[] arrValue5 = value.split(" - ");
							if (arrValue5.length > 1 && !StringUtil.isNullOrEmpty(arrValue5[0]) && !StringUtil.isNullOrEmpty(arrValue5[1])) {
								value = arrValue5[0];
							}
							String equipProviderCode = value.trim().toUpperCase();
							boolean flagProvider = false;
							for (EquipProvider equipProvider : lstEquipProvider) {
								if (equipProviderCode.equals(equipProvider.getCode().trim().toUpperCase())) {
									flagProvider = true;
									equipIst.setEquipProvider(equipProvider);
									break;
								}
							}
							if (!flagProvider) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.running");
							}
						} else {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.equipprovider.null");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
							message += "\n";
						}

						//Tinh trang thiet bi - healthStatus
						value = row.get(6);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.trim();
							String[] arrValue6 = value.split(" - ");
							if (arrValue6.length > 1 && !StringUtil.isNullOrEmpty(arrValue6[0]) && !StringUtil.isNullOrEmpty(arrValue6[1])) {
								value = arrValue6[0];
							}
							String healthStatusCode = value.trim().toUpperCase();
							boolean flagHealthStatus = false;
							for (ApParam healthStatus : lstHealthStatus) {
								if (healthStatusCode.equals(healthStatus.getApParamCode().toUpperCase().trim())) {
									flagHealthStatus = true;
									equipIst.setHealthStatus(healthStatusCode);
									break;
								}
							}
							if (!flagHealthStatus) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.running");
							}
						} else {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.import.err.healthstatus.null");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
							message += "\n";
						}
						
						//Kho nhap - stockCode
						value = row.get(7);
						msg = ValidateUtil.validateField(value, "equipment.update.err.stock.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(msg)) {
							value = value.trim();
							EquipStock equipStock = equipmentManagerMgr.getEquipStockbyCode(value);
							if (equipStock != null) {
								if (ActiveType.RUNNING.equals(equipStock.getStatus())) {
									if (equipmentManagerMgr.countStockEquipForPermisionCSM(currentUser.getStaffRoot().getStaffId(), null, equipStock.getCode()) == 0) {
										msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.code.undefined.permision");
									} else {
										equipIst.setStockCode(value.trim().toUpperCase());
										equipIst.setStockId(equipStock.getId());
										equipIst.setStockName(equipStock.getName());
									}
								} else {
									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.code.is.not.running");
								}
							} else {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.code.undefined");
							}
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
							message += "\n";
						}
					}
					//XU LY CAP NHAT VAO DB
					if (StringUtil.isNullOrEmpty(message)) {
						//TODO thuc hien them moi thiet bi
						equipIst.setTradeStatus(StatusType.INACTIVE.getValue());
						equipIst.setCreateUser(currentUser.getUserName());
						equipmentManagerMgr.createEquipmentInListEquip(equipIst, filter);
					} else {
						lstView.add(StringUtil.addFailBean(row, message));
					}
				}
				//Export error
				getOutputFailExcelFile(lstView, ConstantManager.TEMPLATE_EQUIP_LIST_EQUIPMENT_FAIL);

			} else {
				isError = true;
				errMsg = "Tập tin Excel chưa nhập dữ liệu Import";
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Load trang Danh sach Thiet bi
	 * 
	 * @author hunglm16
	 * @since December 17,2014
	 * @description Chuc nang Danh Sach Thiet Bi
	 * */
	public String viewlListEquipmentJSP() {
		try {
			shopId = currentUser.getShopRoot().getShopId();
			//lstChannelType = commonMgr.getListChannelTypeByTypeAndArrObjType(ChannelTypeType.EQUIPMENT_TYPE_GENERAL.getValue(), null);
			EquipmentFilter<EquipCategory> filterEC = new EquipmentFilter<EquipCategory>();
			filterEC.setStatus(StatusType.ACTIVE.getValue());
			lstEquipCategory = equipmentManagerMgr.getListEquipmentCategoryByFilter(filterEC);
			EquipmentFilter<EquipProvider> filterEP = new EquipmentFilter<EquipProvider>();
			filterEP.setStatus(StatusType.ACTIVE.getValue());
			lstEquipProvider = equipmentManagerMgr.getListEquipProviderByFilter(filterEP);
			EquipmentFilter<EquipmentVO> filterG = new EquipmentFilter<EquipmentVO>();
			filterG.setStatus(StatusType.ACTIVE.getValue());
			result.put("lstEquipGroup", equipmentManagerMgr.searchEquipmentGroupVObyFilter(filterG).getLstObject());

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Load trang Chi tiet Danh sach Thiet bi
	 * 
	 * @author hunglm16
	 * @since December 17,2014
	 * @description Chuc nang Danh Sach Thiet Bi
	 * */
	public String viewDetailListEquipmentJSP() {
		try {
			//lstChannelType = commonMgr.getListChannelTypeByTypeAndArrObjType(ChannelTypeType.EQUIPMENT_TYPE_GENERAL.getValue(), null);
			EquipmentFilter<EquipCategory> filterEC = new EquipmentFilter<EquipCategory>();
			filterEC.setStatus(StatusType.ACTIVE.getValue());
			lstEquipCategory = equipmentManagerMgr.getListEquipmentCategoryByFilter(filterEC);
			EquipmentFilter<EquipProvider> filterEP = new EquipmentFilter<EquipProvider>();
			filterEP.setStatus(StatusType.ACTIVE.getValue());
			lstEquipProvider = equipmentManagerMgr.getListEquipProviderByFilter(filterEP);
			EquipmentFilter<EquipmentVO> filterG = new EquipmentFilter<EquipmentVO>();
			filterG.setStatus(StatusType.ACTIVE.getValue());
			result.put("lstEquipGroup", equipmentManagerMgr.searchEquipmentGroupVObyFilter(filterG).getLstObject());
			result.put("lstHealthStatus", apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING));
			if (id != null) {
				equip = equipmentManagerMgr.getEquipmentById(id);
				if (equip == null || equip.getId() == null) {
					return PAGE_NOT_FOUND;
				}
				if (equip.getManufacturingYear() != null) {
					manufacturingYearStr = String.valueOf(equip.getManufacturingYear()).trim();
				}
				warrantyExpiredDateStr = "";
				if (equip.getWarrantyExpiredDate() != null) {
					warrantyExpiredDateStr = DateUtil.convertDateByString(equip.getWarrantyExpiredDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
				}
			} else {
				equip = new Equipment();
				//equipImportRecordCode = equipmentManagerMgr.getEquipImportRecordCodeGenetated();
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Upload File
	 * 
	 * @author liemtpt
	 * @since 25/03/2015
	 * */
	private List<FileVO> saveUploadFileByEquip(List<File> lstFile, List<String> lstFileFileName, List<String> lstFileContentType) {
		try {
			if (lstFile != null && lstFile.size() > 0) {
				//String perfix = Configuration.getPrefixFileEquipServerDocPath();
				List<FileVO> lstFileVO = FileUtility.storeFileOnDisk(lstFile, lstFileFileName, lstFileContentType, null, EquipTradeType.EQUIP_ALLOCATION.getValue());
				if (lstFileVO != null && !lstFileVO.isEmpty()) {
					String thumbUrl = Configuration.getEquipThumbUrlAttachFile();
					for (FileVO voFile : lstFileVO) {
						voFile.setThumbUrl(thumbUrl);
					}
				}
				return lstFileVO;
			}
			System.gc();
		} catch (Exception e) {
			LogUtility.logError(e, "ManageEquipmentAction.saveUploadFileByEquip()" + e.getMessage());
		}
		return new ArrayList<FileVO>();
	}
	
	/**
	 * Them moi hoac cap nhat thong tin Thiet bị
	 * 
	 * @author hunglm16
	 * @since December 17,2014
	 * @description change in equipment
	 * */
//	public String addOrUpdateEquipment() {
//		//generateToken();
//		errMsg = "";
//		String msg = "";
//		try {
//			Equipment equipIst = new Equipment();
//			EquipmentFilter<BasicVO> filter = new EquipmentFilter<BasicVO>();
//			//TODO Validate Chung cho ca them moi va cap nhat
//			if (StringUtil.isNullOrEmpty(msg) && equipGroupId != null) {//Nhom san pham
//				EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupById(equipGroupId);
//				if (equipGroup != null) {
//					if (!ActiveType.RUNNING.getValue().equals(equipGroup.getStatus().getValue())) {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.unon");
//					} else {
//						filter.setEquipGroupId(equipGroupId);
//						equipIst.setEquipGroup(equipGroup);
//					}
//				} else {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined");
//				}
//			} else {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipgroup.undefined");
//			}
//			//Nha cung cap
//			if (StringUtil.isNullOrEmpty(msg) && equipProviderId != null) {
//				EquipProvider equipProvider = equipmentManagerMgr.getEquipProviderById(equipProviderId);
//				if (equipProvider != null) {
//					if (!ActiveType.RUNNING.getValue().equals(equipProvider.getStatus().getValue())) {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.unon");
//					} else {
//						filter.setEquipProviderId(equipProviderId);
//						equipIst.setEquipProvider(equipProvider);
//					}
//				} else {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.undefined");
//				}
//			} else {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.unon");
//			}
//			//Gia san pham
//			if (StringUtil.isNullOrEmpty(msg) && (price == null || price.doubleValue() < 100)) {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.price");
//			} else {
//				filter.setPrice(price);
//				equipIst.setPrice(price);
//			}
//			//Ngay bao hanh
//			if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(warrantyExpiredDateStr)) {
//				if (!DateUtil.isThisDateValidate(warrantyExpiredDateStr, DateUtil.DATE_FORMAT_STR)) {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDateStr.format.err");
//				} else {
//					filter.setWarrantyExpiredDateStr(warrantyExpiredDateStr);
//					equipIst.setWarrantyExpiredDate(DateUtil.parse(warrantyExpiredDateStr, DateUtil.DATE_FORMAT_STR));
//				}
//			} else if (StringUtil.isNullOrEmpty(msg)) {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDateStr.format.err.null");
//			}
//			//Nam san xuat
//			if (StringUtil.isNullOrEmpty(msg) && yearManufacture != null) {
//				Integer yearWrran = Integer.valueOf(warrantyExpiredDateStr.trim().split("/")[2]);
//				if (yearWrran < yearManufacture) {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDate.manufacturingYear.err");
//				} else {
//					filter.setManufacturingYearStr(manufacturingYearStr);
//					equipIst.setManufacturingYear(yearManufacture);
//				}
//			} else if (StringUtil.isNullOrEmpty(msg)) {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.manufacturingYearStr.format.err");
//			}
//
//			//Trang thai su dung
//			if (StringUtil.isNullOrEmpty(msg) && (usageStatus == null || !EquipUsageStatus.checkIsRecode(usageStatus))) {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.usagestatus.undefinde");
//			} else {
//				filter.setUsageStatus(usageStatus);
//			}
//			//Trang thai thiet bi
//			if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(healthStatus)) {
//				List<ApParam> lstAppram = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
//				boolean flagHs = false;
//				for (ApParam row : lstAppram) {
//					if (healthStatus.equals(row.getApParamCode())) {
//						flagHs = true;
//						break;
//					}
//				}
//				if (!flagHs) {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde");
//				} else {
//					filter.setHealthStatusNew(healthStatus);
//					equipIst.setHealthStatus(healthStatus);
//				}
//			} else {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde");
//			}
//			//Kho(F9)
//			if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(stockCode)) {
//				//Kiem tra thuoc thuoc phan quyen kho theo user dang nhap
//				if (equipmentManagerMgr.countStockEquipForPermisionCSM(currentUser.getStaffRoot().getStaffId(), null, stockCode) == 0) {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.code.undefined.permision");
//				} else {
//					EquipStock equipStock = equipmentManagerMgr.getEquipStockbyCode(stockCode);
//					if (ActiveType.RUNNING.equals(equipStock.getStatus())) {
//						equipIst.setStockCode(stockCode);
//						equipIst.setStockId(equipStock.getId());
//						equipIst.setStockName(equipStock.getName());
//					} else {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.code.is.not.running");
//					}
//				}
//			} else {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.stock.code.undefined");
//			}
//			if (id != null && id > 0) {
//				//Cap nhat thong tin Thiet bi
//				Equipment equipUdt = equipmentManagerMgr.getEquipmentById(id);
//				if (equipUdt == null) {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.undefined");
//				}
//				if (!StringUtil.isNullOrEmpty(seriNumber)) {
//					String[] arrSeriNumber = seriNumber.split(" ");
//					if (arrSeriNumber.length > 1) {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial");
//					} else {
//						equipUdt.setSerial(seriNumber);
//					}
//				}
//				if (!StringUtil.isNullOrEmpty(msg)) {
//					result.put(ERROR, true);
//					result.put("errMsg", msg);
//					return JSON;
//				}
//				//TODO Thuc hien cap nhat thiet bi
//				if (!equipUdt.getHealthStatus().equals(healthStatus)) {
//					equipUdt.setHealthStatus(equipIst.getHealthStatus());
//					filter.setHealthStatusNew(equipIst.getHealthStatus());
//					equipUdt.setStockId(equipIst.getStockId());
//					equipUdt.setStockCode(equipIst.getStockCode());
//					equipUdt.setStockName(equipIst.getStockName());
//				} else {
//					filter.setHealthStatusNew(null);
//				}
//				//equipUdt.setEquipGroup(equipIst.getEquipGroup());
//				equipUdt.setEquipProvider(equipIst.getEquipProvider());
//				equipUdt.setPrice(equipIst.getPrice());
//				equipUdt.setWarrantyExpiredDate(equipIst.getWarrantyExpiredDate());
//				equipUdt.setManufacturingYear(equipIst.getManufacturingYear());
//				equipUdt.setUpdateUser(currentUser.getUserName());
//				equipmentManagerMgr.updateEquipmentInListEquip(equipUdt, filter);
//			} else {
//				//Them moi thiet bi
//				if (StringUtil.isNullOrEmpty(msg) && (quantity == null || quantity < 0 || quantity > 10000)) {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.quantity");
//				} else {
//					filter.setQuantity(quantity);
//				}
//				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPeriod == null) {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//				}
//				
//				if (!StringUtil.isNullOrEmpty(msg)) {
//					result.put(ERROR, true);
//					result.put("errMsg", msg);
//					return JSON;
//				}
//				//TODO thuc hien them moi thiet bi
//				// xu ly attach file
//				filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));
//				
////				equipIst.setTradeStatus(StatusType.INACTIVE.getValue());
//				equipIst.setCreateUser(currentUser.getUserName());
//				equipmentManagerMgr.createEquipmentInListEquip(equipIst, filter);
//			}
//		} catch (Exception e) {
//			result.put(ERROR, true);
//			LogUtility.logError(e, e.getMessage());
//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			return JSON;
//		}
//		result.put(ERROR, false);
//		return JSON;
//	}
	
	/**
	 * Them moi hoac cap nhat thong tin Thiet bị
	 * 
	 * @author hunglm16
	 * @since December 17,2014
	 * @description change in equipment. Update by tamvnm: chi dung de cap nhat thiet bi
	 * */
	public String addOrUpdateEquipment() {
		//generateToken();
		resetToken(result);
		//tamvnm: ham chi cap nhat thiet bi
		errMsg = "";
		String msg = "";
		try {
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
				return SUCCESS;
			}
			if (id != null && id > 0) {
				//Cap nhat thong tin Thiet bi
				Equipment equipUdt = equipmentManagerMgr.getEquipmentById(id, currentUser.getShopRoot().getShopId());
				Date sysDate = commonMgr.getSysDate();
//				EquipmentFilter<BasicVO> filter = new EquipmentFilter<BasicVO>();
				if (equipUdt == null) {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.undefined");
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.undefined"));
					return JSON;
				} else {
					//Nha cung cap
					if (StringUtil.isNullOrEmpty(msg) && equipProviderId != null) {
						EquipProvider equipProvider = equipmentManagerMgr.getEquipProviderById(equipProviderId);
						if (equipProvider != null) {
							if (!ActiveType.RUNNING.getValue().equals(equipProvider.getStatus().getValue())) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.unon");
							} else {
//								filter.setEquipProviderId(equipProviderId);
								equipUdt.setEquipProvider(equipProvider);
							}
						} else {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.undefined");
						}
					} else {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.unon");
					}
					//Gia san pham
					if (StringUtil.isNullOrEmpty(msg) && (price == null || price.doubleValue() < 100)) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.price");
					} else {
//						filter.setPrice(price);
						equipUdt.setPrice(price);
					}
					//Ngay bao hanh
					if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(warrantyExpiredDateStr)) {
						if (!DateUtil.isThisDateValidate(warrantyExpiredDateStr, DateUtil.DATE_FORMAT_DDMMYYYY)) {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDateStr.format.err");
						} else {
//							filter.setWarrantyExpiredDateStr(warrantyExpiredDateStr);
							equipUdt.setWarrantyExpiredDate(DateUtil.parse(warrantyExpiredDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
						}
					} else if (StringUtil.isNullOrEmpty(msg)) {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDateStr.format.err.null");
						equipUdt.setWarrantyExpiredDate(null);
					}
					//Nam san xuat
					if (StringUtil.isNullOrEmpty(msg) && yearManufacture != null) {
						if (!StringUtil.isNullOrEmpty(warrantyExpiredDateStr)) {
							Integer yearWrran = Integer.valueOf(warrantyExpiredDateStr.trim().split("/")[2]);
							if (yearWrran < yearManufacture) {
								msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.warrantyExpiredDate.manufacturingYear.err");
							} else {
								//							filter.setManufacturingYearStr(manufacturingYearStr);
								equipUdt.setManufacturingYear(yearManufacture);
							}
						} else {
							equipUdt.setManufacturingYear(yearManufacture);
						}
					} else if (StringUtil.isNullOrEmpty(msg)) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lst.equip.manufacturingYearStr.format.err");
					}
					
					//Trang thai thiet bi
					if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(healthStatus)) {
						List<ApParam> lstAppram = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
						boolean flagHs = false;
						for (ApParam row : lstAppram) {
							if (healthStatus.equals(row.getApParamCode())) {
								flagHs = true;
								break;
							}
						}
						if (!flagHs) {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde");
						} else {
//							filter.setHealthStatusNew(healthStatus);
							equipUdt.setHealthStatus(healthStatus);
						}
					} else {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.healthstatus.undefinde");
					}
					
					if (!StringUtil.isNullOrEmpty(seriNumber)) {
						String[] arrSeriNumber = seriNumber.split(" ");
						if (arrSeriNumber.length > 1) {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.is.serial");
						} else {
							equipUdt.setSerial(seriNumber);
						}
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						result.put(ERROR, true);
						result.put("errMsg", msg);
						return JSON;
					}
					//Thuc hien cap nhat thiet bi
//					if (!equipUdt.getHealthStatus().equals(healthStatus)) {
//						equipUdt.setHealthStatus(equipIst.getHealthStatus());
//						filter.setHealthStatusNew(equipIst.getHealthStatus());
//						equipUdt.setStockId(equipIst.getStockId());
//						equipUdt.setStockCode(equipIst.getStockCode());
//						equipUdt.setStockName(equipIst.getStockName());
//					} else {
//						filter.setHealthStatusNew(null);
//					}
//					//equipUdt.setEquipGroup(equipIst.getEquipGroup());
//
//					equipUdt.setPrice(equipIst.getPrice());
//					equipUdt.setWarrantyExpiredDate(equipIst.getWarrantyExpiredDate());
//					equipUdt.setManufacturingYear(equipIst.getManufacturingYear());
					equipUdt.setUpdateUser(currentUser.getUserName());
					equipUdt.setUpdateDate(sysDate);
//					equipmentManagerMgr.updateEquipmentInListEquip(equipUdt, filter);
					commonMgr.updateEntity(equipUdt);
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.undefined"));
				return JSON;
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	
	/**
	 * @author vuongmq
	 * @date Mar 13,2015
	 * @description search  cay don vi
	 * */
	public String searchShopDialog() throws Exception {
		try {
			staff = getStaffByCurrentUser();
			if (staff == null || staff.getShop() == null || staff.getStaffType() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			if (id != null) {
				shopId = id;
			}
			Shop sh = null;
			if (shopId == null || shopId == 0) {
				//sh = staff.getShop();
				//shopId = sh.getId();
				shopId = currentUser.getShopRoot().getShopId();
			}/* else {
				sh = shopMgr.getShopById(shopId);
			}*/
			sh = shopMgr.getShopById(shopId);
			
			EquipmentFilter<ShopViewParentVO> filter = new EquipmentFilter<ShopViewParentVO>();
			filter.setShopRootId(currentUser.getShopRoot() == null ? null : currentUser.getShopRoot().getShopId());
			filter.setStaffRootId(currentUser.getStaffRoot() == null ? null :currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken() == null ? null : currentUser.getRoleToken().getRoleId());
			
			//List<ShopViewParentVO> lst = promotionProgramMgr.searchShopForPromotionProgram(promotionId, shopId, code, name);
			List<ShopViewParentVO> lst = equipmentManagerMgr.searchShopTreeView(shopId, code, name, filter);
			lstTree = new ArrayList<TreeGridNode<ShopViewParentVO>>();
			if (lst == null || lst.size() == 0) {
				return JSON;
			}
			
			if (id == null) {				
				ShopViewParentVO vo = null;
				int i = 0;
				int sz = lst.size();
				String state = null;
				
				if (StringUtil.isNullOrEmpty(code) && StringUtil.isNullOrEmpty(name)) {
					vo = new ShopViewParentVO();
					vo.setId(sh.getId());
					vo.setShopCode(sh.getShopCode());
					vo.setShopName(sh.getShopName());
					//vo.setIsExists(1);
					vo.setIsNPP(0);
					
					state = ConstantManager.JSTREE_STATE_CLOSE;
					i = 0;
				} else {
					vo = lst.get(0);
					state = ConstantManager.JSTREE_STATE_OPEN;
					i = 1;
				}
				
				TreeGridNode<ShopViewParentVO> node = new TreeGridNode<ShopViewParentVO>();
				node.setNodeId(vo.getId().toString());
				node.setAttr(vo);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setText(vo.getShopCode() + " - " + vo.getShopName());
				List<TreeGridNode<ShopViewParentVO>> chidren = new ArrayList<TreeGridNode<ShopViewParentVO>>();
				node.setChildren(chidren);
				lstTree.add(node);
				
				if (lst == null || lst.size() == 0) {
					//vo.setIsExists(0);
					return JSON;
				}
	
				// Tao cay			
				TreeGridNode<ShopViewParentVO> tmp = null;
				TreeGridNode<ShopViewParentVO> tmp2 = null;
				for (; i < sz; i++) {
					vo = lst.get(i);
					
					tmp2 = this.getNodeFromTree(lstTree, vo.getParentId().toString());
					if (tmp2 != null) {
						tmp = new TreeGridNode<ShopViewParentVO>();
						tmp.setNodeId(vo.getId().toString());
						tmp.setAttr(vo);
						if (0 == vo.getIsNPP()) {
							tmp.setState(state);
						} else {
							tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
						}
						tmp.setText(vo.getShopCode() + " - " + vo.getShopName());
						
						if (tmp2.getChildren() == null) {
							tmp2.setChildren(new ArrayList<TreeGridNode<ShopViewParentVO>>());
						}
						tmp2.getChildren().add(tmp);
					}
				}
			} else {
				// Tao cay			
				TreeGridNode<ShopViewParentVO> tmp = null;
				ShopViewParentVO vo = null;
				for (int i = 0, sz = lst.size(); i < sz; i++) {
					vo = lst.get(i);
					
					tmp = new TreeGridNode<ShopViewParentVO>();
					tmp.setNodeId(vo.getId().toString());
					tmp.setAttr(vo);
					if (0 == vo.getIsNPP()) {
						tmp.setState(ConstantManager.JSTREE_STATE_CLOSE);
					} else {
						tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
					}
					tmp.setText(vo.getShopCode() + " - " + vo.getShopName());
					
					lstTree.add(tmp);
				}
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "ManageEquipmentStockAction.searchShopDialog - " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay node trong cay
	 * 
	 * @author vuongmq
	 * @date Mar 19, 2015
	 */
	private <T> TreeGridNode<T> getNodeFromTree(List<TreeGridNode<T>> treeTmp, String nodeId) throws Exception {
		if (treeTmp == null) {
			return null;
		}
		TreeGridNode<T> node = null;
		TreeGridNode<T> tmp = null;
		for (int i = 0, sz = treeTmp.size(); i < sz; i++) {
			node = treeTmp.get(i);
			if (node.getNodeId().equals(nodeId)) {
				return node;
			}
			tmp = getNodeFromTree(node.getChildren(), nodeId);
			if (tmp != null) {
				return tmp;
			}
		}
		return null;
	}
	
	/**
	 * @author vuongmq
	 * @date Mar 13,2015
	 * @description filter kho thiet bi: EquipmentStock
	 * */
	private EquipmentFilter<EquipStock> getFilterSearchEquipmentStock(String viewPaging){
		EquipmentFilter<EquipStock> filter = new EquipmentFilter<EquipStock>();
		//set cac tham so tim kiem
		if(PAGING.equals(viewPaging)){
			KPaging<EquipStock> kPaging = new KPaging<EquipStock>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
		}
		if (status != null) {
			filter.setStatus(status);
		}
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			if (!checkShopInOrgAccessByCode(shopCode)) {
				return null;
			}
			filter.setShopCode(shopCode);
		}
		if (!StringUtil.isNullOrEmpty(code)) {
			filter.setCode(code);
		}
		if (!StringUtil.isNullOrEmpty(name)) {
			filter.setName(name);
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			filter.setFromDate(fDate);;
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			Date fDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			filter.setToDate(fDate);;
		}
		return filter;
	}
	/**
	 * @author vuongmq
	 * @date Mar 13,2015
	 * @description search kho thiet bi: EquipmentStock
	 * */
	public String searchEquipmentStock() {
		//generateToken();
		result.put("rows", new ArrayList<EquipStock>());
		result.put("total", 0);
		try {
			EquipmentFilter<EquipStock> filter = this.getFilterSearchEquipmentStock(PAGING);
			if (filter == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.shop.not.belong.area"));
				return JSON;
			}
			
			filter.setShopRootId(currentUser.getShopRoot() == null ? null : currentUser.getShopRoot().getShopId());
			filter.setStaffRootId(currentUser.getStaffRoot() == null ? null :currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken() == null ? null : currentUser.getRoleToken().getRoleId());
			
			/** goi ham thuc thi tim kiem */
			ObjectVO<EquipStock> objVO = equipmentManagerMgr.getListEquipmentStockByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	/**
	 * Luu thong tin kho thiet bị
	 * @author tientv11
	 * @since 18/12/2014
	 * @return
	 */
	public String saveOrUpdateStock(){
		resetToken(result);
		try {
			result.put(ERROR, true);
			if(currentUser != null && currentUser.getUserName() != null ){
				Shop sh = null;
				if(!StringUtil.isNullOrEmpty(shopCode)){
					sh = shopMgr.getShopByCode(shopCode);
					if(sh == null){
						result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("catalog.unit.tree.code"))); // ma kho khong ton tai
						return JSON;
					} else {
						if(!ActiveType.RUNNING.equals(sh.getStatus())){
							//common.catalog.shop.status.not.active = Mã đơn vị {0} đang ở trạng thái không hoạt động.
							result.put("errMsg", R.getResource("common.catalog.shop.status.not.active", shopCode));
							return JSON;
						}
						if (!checkShopInOrgAccessByCode(shopCode)) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("common.shop.not.belong.area"));
							return JSON;
						}
					}
				} else {
					result.put("errMsg", R.getResource("common.required.field", R.getResource("catalog.unit.tree.code"))); // ma don vi khong duoc de trong
					return JSON;
				}
				if(StringUtil.isNullOrEmpty(code)){
					result.put("errMsg", R.getResource("common.required.field", R.getResource("stock.update.stock.warehouse.code"))); // ma kho khong duoc de trong
					return JSON;
				}
				/** cập nhật EquipStock */
				if (id != null && id != 0L) {
					EquipStock eStockUpdate = commonMgr.getEntityById(EquipStock.class, id);
					if(eStockUpdate == null){
						return JSON;
					}
					
					//Them kiem tra ATTT, equipstock phai duoc phan quyen shop dang nhap
					if (eStockUpdate.getShop() == null || !checkShopInOrgAccessById(eStockUpdate.getShop().getId())) {
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
						result.put(ERROR, true);
						return JSON;
					}
					
					/** truong hop so thu tu cap nhat khac record cu thi moi kiem tra so thu tu*/
					if(eStockUpdate.getOrdinal() != null && !eStockUpdate.getOrdinal().equals(ordinal)){
						/** cap nhat kiem tra so thu tu cua kho co trong shop hay khong*/
						List<EquipStock> lstStockOrdinal = equipmentManagerMgr.getEquipStockByShopAndOrdinal(sh.getId(), ordinal);
						if(lstStockOrdinal != null && lstStockOrdinal.size() > 0){
							result.put("errMsg", R.getResource("equip.manager.stock.ordinal.err", sh.getShopCode())); //Thứ tự không trùng trong cùng hệ thống kho của đơn vị {0}
							return JSON;
						}
					}
					eStockUpdate.setOrdinal(ordinal);
					eStockUpdate.setName(name);
					eStockUpdate.setStatus(ActiveType.parseValue(status));
					eStockUpdate.setDescription(description);
					eStockUpdate.setUpdateDate(DateUtil.now());
					eStockUpdate.setUpdateUser(currentUser.getUserName());
					commonMgr.updateEntity(eStockUpdate);
					/** cap nhat truong Object_id; la shop khong can cap nhat lai*/
				}else{
					/** thêm moi EquipStock */
					if(!StringUtil.isNullOrEmpty(code)){
						EquipStock eStock = equipmentManagerMgr.getEquipStockbyCode(code);
						if(eStock != null){
							result.put("errMsg", R.getResource("common.exist.code", R.getResource("stock.update.stock.warehouse.code"))); // ma kho da ton tai
							return JSON;
						}
					}
					EquipStock equipStock = new EquipStock();
					/** them moi: kiem tra so thu tu cua kho co trong shop hay khong*/
					List<EquipStock> lstStockOrdinal = equipmentManagerMgr.getEquipStockByShopAndOrdinal(sh.getId(), ordinal);
					if(lstStockOrdinal != null && lstStockOrdinal.size() > 0){
						result.put("errMsg", R.getResource("equip.manager.stock.ordinal.err", sh.getShopCode())); //Thứ tự không trùng trong cùng hệ thống kho của đơn vị {0}
						return JSON;
					}
					equipStock.setOrdinal(ordinal);
					equipStock.setCode(code.toUpperCase());
					equipStock.setName(name);
					equipStock.setStatus(ActiveType.parseValue(status));
					equipStock.setDescription(description);
					equipStock.setCreateDate(DateUtil.now());
					equipStock.setCreateUser(currentUser.getUserName());
					/** them moi truong Object_id*/
					equipStock.setShop(sh);
					equipStock = commonMgr.createEntity(equipStock);
				}
				result.put(ERROR, false);
			} else {
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
		} catch (Exception e) {	
			LogUtility.logError(e, "ManageEquipmentStock.saveOrUpdateStock() "+e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Xuat Excel theo Tim kiem kho thiet bi
	 * @author vuongmq
	 * @since Mar 20,2015
	 * @return Excel
	 * */
	public String exportExcelBySearchEquipmentStock() {
		try {
			/*begin vuongmq; 11/05/2015; xuat Excel ATTT*/
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			/*end vuongmq; 11/05/2015; xuat Excel ATTT*/
			EquipmentFilter<EquipStock> filter = this.getFilterSearchEquipmentStock(NO_PAGING);
			
			filter.setShopRootId(currentUser.getShopRoot() == null ? null : currentUser.getShopRoot().getShopId());
			filter.setStaffRootId(currentUser.getStaffRoot() == null ? null :currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken() == null ? null : currentUser.getRoleToken().getRoleId());
			
			/** goi ham thuc thi tim kiem */
			ObjectVO<EquipStock> objVO = equipmentManagerMgr.getListEquipmentStockByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				HashMap<String, Object> beans = new HashMap<String, Object>();
				beans.put("lst", objVO.getLstObject());
				String outputPath = ReportUtils.exportExcelJxlsx(beans, ShopReportTemplate.QLTB_QLKHOTHIETBI, FileExtension.XLSX);
				result.put(ERROR, false);
				result.put(REPORT_PATH, outputPath);
				/*vuongmq; 11/05/2015; xuat Excel ATTT*/
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				//common.export.excel.null = Không có dữ liệu để xuất file Excel
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.export.excel.null"));
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ManagementEquipmentAction.exportExcelBySearchEquipmentStock()" +  e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Nhap Excel kho thiet bi
	 * @author vuongmq
	 * @since Mar 20,2015
	 * @description Import Excel
	 * */
	public String importEquipStock() {
		resetToken(result);
		isError = true;
		totalItem = 0;
		try {
			//Xu ly nghiep vu import file
			lstView = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 5);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				String message = "";
				String msg = "";
				String value = "";
				//boolean flag = false;
				Date sys = commonMgr.getSysDate();
				EquipStock equipStockNew = new EquipStock();
				for (int i = 0; i < lstData.size(); i++) {
					message = "";
					msg = "";
					totalItem++;
					List<String> row = lstData.get(i);
					//Ma don vi
					Shop shopExcel = null;
					value = row.get(0);
					if (!StringUtil.isNullOrEmpty(value)) {
						value = value.trim();
						shopExcel = shopMgr.getShopByCode(value);
						if (shopExcel != null) {
							if (!ActiveType.RUNNING.equals(shopExcel.getStatus())) {
								//common.catalog.status.in.active = {0} đang ở trạng thái không hoạt động.
								msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, value);
							} else if (!checkShopInOrgAccessByCode(value)) {
								msg = R.getResource("common.shop.not.belong.area");
							}
						} else {
							// {0} khong co trong he thong
							msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, value);
						}
					} else {
						// Ban chua nhap gia tri cho truong ma don vi
						msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("catalog.unit.tree.code"));
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						message += msg + "\n";
					} else {
						equipStockNew.setShop(shopExcel);
					}
					//Ma kho : stock.update.stock.warehouse.code
					value = row.get(1);
					msg = "";
					if (!StringUtil.isNullOrEmpty(value)) {
						value = value.trim();
						String errMakho = ValidateUtil.validateField(value, "stock.update.stock.warehouse.code", 50, ConstantManager.ERR_MAX_LENGTH_EQUALS);
						if(!StringUtil.isNullOrEmpty(errMakho)){
							msg += errMakho;
						}
						errMakho = "";
						errMakho = ValidateUtil.getErrorMsgOfSpecialCharInCode(value, "stock.update.stock.warehouse.code");
						if(!StringUtil.isNullOrEmpty(errMakho)){
							msg += errMakho;
						}
						EquipStock equipStock = equipmentManagerMgr.getEquipStockbyCode(value);
						if(equipStock != null){
							//common.exist.code = {0} đã tồn tại trong hệ thống.
							msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, value);
						}
					} else {
						// Ban chua nhap gia tri cho truong ma kho
						msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("stock.update.stock.warehouse.code"));
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						message += msg + "\n";
					} else {
						equipStockNew.setCode(value.toUpperCase());
					}
					//Ten kho
					value = row.get(2);
					msg = "";
					if (!StringUtil.isNullOrEmpty(value)) {
						value = value.trim();
						String errTenkho = ValidateUtil.validateField(value, "equip.manager.stock.name", 250, ConstantManager.ERR_MAX_LENGTH_EQUALS);
						if(!StringUtil.isNullOrEmpty(errTenkho)){
							msg += errTenkho;
						}
					} else {
						// Ban chua nhap gia tri cho truong ten kho
						msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("equip.manager.stock.name"));
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						message += msg+ "\n";
					} else {
						equipStockNew.setName(value);
					}
					//Thu tu
					value = row.get(3);
					msg = "";
					if (!StringUtil.isNullOrEmpty(value)) {
						value = value.trim();
						String errThuTu = ValidateUtil.validateField(value, "equip.manager.stock.ordinal.name", 2, ConstantManager.ERR_MAX_LENGTH_EQUALS, ConstantManager.ERR_INTEGER);
						if(!StringUtil.isNullOrEmpty(errThuTu)){
							msg += errThuTu;
						}
						if(shopExcel != null && StringUtil.isNullOrEmpty(msg)){
							List<EquipStock> lstStockOrdinal = equipmentManagerMgr.getEquipStockByShopAndOrdinal(shopExcel.getId(), Integer.parseInt(value));
							if(lstStockOrdinal != null && lstStockOrdinal.size() > 0){
								msg += R.getResource("equip.manager.stock.ordinal.err", shopExcel.getShopCode());
							}
						}
						//errThuTu = ValidateUtil.validateField(value, "", null, ConstantManager.ERR_INTEGER);
					} else {
						// Ban chua nhap gia tri cho truong thu tu
						msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("equip.manager.stock.ordinal.name"));
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						message += msg+ "\n";
					} else {
						equipStockNew.setOrdinal(Integer.parseInt(value));
					}
					//Ghi chu
					value = row.get(4);
					msg = "";
					if (!StringUtil.isNullOrEmpty(value)) {
						value = value.trim();
						String errGhiChu = ValidateUtil.validateField(value, "equip.proposal.export.sheet1.ghichu", 250, ConstantManager.ERR_MAX_LENGTH_EQUALS);
						if(!StringUtil.isNullOrEmpty(errGhiChu)){
							msg += errGhiChu;
						}
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						message += msg+ "\n";
					} else {
						equipStockNew.setDescription(value);
					}
					//XU LY CAP NHAT VAO DB
					if (StringUtil.isNullOrEmpty(message)) {
						/*** thuc hien them moi thiet bi */
						equipStockNew.setStatus(ActiveType.RUNNING);
						equipStockNew.setCreateDate(sys);
						equipStockNew.setCreateUser(currentUser.getUserName());
						commonMgr.createEntity(equipStockNew);
					} else {
						lstView.add(StringUtil.addFailBean(row, message));
					}
				}
				//Export error
				getOutputFailExcelFile(lstView, ConstantManager.EQUIP_STOCK_IMPORT_FAIL);
			} else {
				isError = true;
				// errMsg != null thi luc nay thi errMsg đã có giá trị lỗi
				if(StringUtil.isNullOrEmpty(errMsg)){
					//customer.display.program.nodata = Không có dữ liệu import. 
					errMsg = R.getResource("customer.display.program.nodata");
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ManagementEquipmentAction.importEquipStock()" + e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	//TODO Khai bai cac thuoc tinh GETTER/SETTER
	/**
	 * Xu ly khai bao cac ham getter/ setter
	 * 
	 * @author hunglm16
	 * @since December 14,2014
	 * */
	private File excelFile;

	private List<ChannelType> lstChannelType = new ArrayList<ChannelType>();
	private List<EquipCategory> lstEquipCategory = new ArrayList<EquipCategory>();
	private List<EquipProvider> lstEquipProvider = new ArrayList<EquipProvider>();
	private List<File> lstFile;
	private List<String> lstFileContentType;
	private List<String> lstFileFileName;
	private List<FileVO> lstFileVo;

	private Equipment equip;

	private Long id;
	private Long shopId;
	private Long stockId;
	private Long staffId;
	private Long equipId;
	private Long equipGroupId;
	private Long equipCategoryId;
	private Long equipProviderId;

	private Integer type;
	private Integer status;
	private Integer quantity;
	private Integer stockType;
	private Integer tradeType;
	private Integer tradeStatus;
	private Integer usageStatus;
	private Integer yearManufacture;
	private Integer flagForStaffRoot;
	private Integer ordinal; /** so thu tu*/

	private BigDecimal price;

	private String code;
	private String stockCode;
	private String staffCode;
	private String seriNumber;
	private String healthStatus;
	private String excelFileContentType;
	private String equipImportRecordCode;
	private String manufacturingYearStr;
	private String warrantyExpiredDateStr;
	private String fromDateStr;
	private String toDateStr;
	private String equipAttachFileStr;
	private String name;
	private String shopCode;
	private String fromDate;
	private String toDate;
	private String description;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String stockName;	
	
	
	private List<TreeGridNode<ShopViewParentVO>> lstTree;
	/**END vuongmq;16/03/2015*/

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getManufacturingYearStr() {
		return manufacturingYearStr;
	}

	public void setManufacturingYearStr(String manufacturingYearStr) {
		this.manufacturingYearStr = manufacturingYearStr;
	}

	public String getWarrantyExpiredDateStr() {
		return warrantyExpiredDateStr;
	}

	public void setWarrantyExpiredDateStr(String warrantyExpiredDateStr) {
		this.warrantyExpiredDateStr = warrantyExpiredDateStr;
	}

	public String getEquipImportRecordCode() {
		return equipImportRecordCode;
	}

	public void setEquipImportRecordCode(String equipImportRecordCode) {
		this.equipImportRecordCode = equipImportRecordCode;
	}

	public Equipment getEquip() {
		return equip;
	}

	public void setEquip(Equipment equip) {
		this.equip = equip;
	}

	public Long getEquipCategoryId() {
		return equipCategoryId;
	}

	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}

	public List<EquipProvider> getLstEquipProvider() {
		return lstEquipProvider;
	}

	public void setLstEquipProvider(List<EquipProvider> lstEquipProvider) {
		this.lstEquipProvider = lstEquipProvider;
	}

	public List<EquipCategory> getLstEquipCategory() {
		return lstEquipCategory;
	}

	public void setLstEquipCategory(List<EquipCategory> lstEquipCategory) {
		this.lstEquipCategory = lstEquipCategory;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public Long getEquipProviderId() {
		return equipProviderId;
	}

	public void setEquipProviderId(Long equipProviderId) {
		this.equipProviderId = equipProviderId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Integer getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(Integer usageStatus) {
		this.usageStatus = usageStatus;
	}

	public List<ChannelType> getLstChannelType() {
		return lstChannelType;
	}

	public void setLstChannelType(List<ChannelType> lstChannelType) {
		this.lstChannelType = lstChannelType;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public List<TreeGridNode<ShopViewParentVO>> getLstTree() {
		return lstTree;
	}

	public void setLstTree(List<TreeGridNode<ShopViewParentVO>> lstTree) {
		this.lstTree = lstTree;
	}

	public String getFromDateStr() {
		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}

	public String getEquipAttachFileStr() {
		return equipAttachFileStr;
	}

	public void setEquipAttachFileStr(String equipAttachFileStr) {
		this.equipAttachFileStr = equipAttachFileStr;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public List<File> getLstFile() {
		return lstFile;
	}

	public void setLstFile(List<File> lstFile) {
		this.lstFile = lstFile;
	}

	public List<String> getLstFileContentType() {
		return lstFileContentType;
	}

	public void setLstFileContentType(List<String> lstFileContentType) {
		this.lstFileContentType = lstFileContentType;
	}

	public List<String> getLstFileFileName() {
		return lstFileFileName;
	}

	public void setLstFileFileName(List<String> lstFileFileName) {
		this.lstFileFileName = lstFileFileName;
	}

	public Integer getFlagForStaffRoot() {
		return flagForStaffRoot;
	}

	public void setFlagForStaffRoot(Integer flagForStaffRoot) {
		this.flagForStaffRoot = flagForStaffRoot;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getArrShop() {
		return arrShop;
	}

	public void setArrShop(String arrShop) {
		this.arrShop = arrShop;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

}

package ths.dms.web.action.equipment;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.EquipmentRepairPayFormMgr;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipRepairPayFormDtl;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PaymentStatusRepair;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairPayFormVO;
import ths.dms.core.entities.vo.EquipmentRepairPaymentRecord;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

public class EquipmentRepairPayFormAction extends AbstractAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	EquipmentRepairPayFormMgr equipmentRepairPayFormMgr;
	EquipmentManagerMgr equipmentManagerMgr;

	private Long id;
	private String customerName;
	private String customerCode;
	private String seri;
	private String equipCode;
	private String fromDate;
	private String toDate;
	private Integer statusAction;
	private Integer statusForm;
	private String strListShopId;
	private String shopCode;
	private Long equipRepairId;
	private String formCode;
	private Integer status;
	private Integer statusPayment;
	private Long equipCategoryId;
	private Long equipGroupId;
	/** so seri */
	private String seriNumber;

	/** loai thiet bi */
	private Long categoryId;

	/** nhom thiet bi */
	private Long groupId;

	/** nha cung cap thiet bi */
	private Long providerId;

	/** nam san xuat thiet bi */
	private Integer yearManufacture;

	// vi dung fromDate co kiem tra <interceptor-ref name="securityInterceptStack"></interceptor-ref>; se lay lai gia tri fromDate o ham prepare()
	private String paymentDate; 
	
	private List<Map<String, Object>> listObjectBeans;
	private List<Long> lstShopId;
	private List<Long> lstId;
	private String lstIdStr; // vi truyen duoi datagrid, khong tryen duoc kieu mang [];

	private List<EquipCategory> lstEquipCategory = new ArrayList<EquipCategory>();
	private List<EquipProvider> lstEquipProvider = new ArrayList<EquipProvider>();

	private EquipmentRepairPaymentRecord equipmentRepairPaymentRecord; // phieu thanh toan cho phieu sua chua thiet bi
	private EquipRepairPayForm equipmentRepairPayment; // phieu thanh toan

	@Override
	public void prepare() throws Exception {
		super.prepare();
		equipmentRepairPayFormMgr = (EquipmentRepairPayFormMgr) context.getBean("equipmentRepairPayFormMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		Date fDate = commonMgr.getDateBySysdateForNumberDay(-7, false);
		Date tDate = commonMgr.getSysDate();
		fromDate = DateUtil.toDateSimpleFormatString(fDate);
		toDate = DateUtil.toDateSimpleFormatString(tDate);
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}
		if (shop != null) {
			shopId = shop.getId();
		}
		
		return SUCCESS;
	}
	
	/**
	 * search QL sua chua
	 * cap nhat lay filter theo ham getFilterRepairSeach
	 * @author vuongmq
	 * @since 22/04/2015
	 */
	public String searchRepairPayForm() {
		result.put("page", page);
		result.put("max", max);
		try {
			EquipRepairFilter filter = new EquipRepairFilter();
			if (currentUser == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			} else {
				if (currentUser.getShopRoot() == null) {
					// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				} else {
					filter.setShopRoot(currentUser.getShopRoot().getShopId());
				}
			}
			// lay filter Repair
			//EquipRepairFilter filter = this.getFilterRepairPayFormSeach(PAGING);
			KPaging<EquipRepairPayFormVO> kPaging = new KPaging<EquipRepairPayFormVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPagingPayFormVO(kPaging);
			filter.setFormCode(formCode);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			filter.setStatus(status);
			filter.setStrListShopId(getStrListShopId());
			ObjectVO<EquipRepairPayFormVO> temp = equipmentRepairPayFormMgr.getListEquipRepairPayForm(filter);
			if (temp != null && temp.getLstObject().size() > 0) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipRepairFormVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairPayFormAction.searchRepairPayForm()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * Lay danh sach chi tiet phieu thanh toan tren grid (icon_view)
	 * @author vuongmq
	 * @since 22/06/2015
	 * @return
	 */
	public String searchRepairPayDetail() {
		try {
			/** truong hop them moi van cho ve rows danh sach new ArrayList<EquipRepairPayFormVO>() */
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipRepairPayFormVO>());
			if (id != null && id > ActiveType.DELETED.getValue()) {
				/** truong hop thong tin chi tiet icon_view */
				equipmentRepairPayment = equipmentRepairPayFormMgr.retrieveEquipmentRepairPayment(id);
				if (equipmentRepairPayment != null) {
					if (equipmentRepairPayment.getShop() != null && super.checkShopInOrgAccessById(equipmentRepairPayment.getShop().getId())) {
						EquipRepairFilter filter = new EquipRepairFilter();
						if (page != 0 && max != 0) {
							KPaging<EquipRepairPayFormVO> kPagingDetail = new KPaging<EquipRepairPayFormVO>();
							kPagingDetail.setPageSize(max);
							kPagingDetail.setPage(page - 1);
							filter.setkPagingPayFormVO(kPagingDetail);
						}
						filter.setId(id);
						// danh sach phieu sua chua cua phieu thanh toan Detail; id của danh sach la id phieu sua chua
						ObjectVO<EquipRepairPayFormVO> temp = equipmentRepairPayFormMgr.getListEquipRepairPayFormDtlById(filter);
						if (temp != null && temp.getLstObject() != null && temp.getLstObject().size() > 0) {
							if (temp.getkPaging() != null) {
								result.put("total", temp.getkPaging().getTotalRows());
							}
							result.put("rows", temp.getLstObject());
						}
					} else {
						return PAGE_NOT_PERMISSION;
					}
				} else {
					return PAGE_NOT_FOUND;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairPayFormAction.searchRepairPayDetail()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach phieu sua chua man hinh them moi QL thanh toan
	 * @author vuongmq
	 * @since 23/06/2015
	 * @return
	 */
	public String searchRepairPopup() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<EquipRepairFormVO> kPaging = new KPaging<EquipRepairFormVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipRepairFilter filter = new EquipRepairFilter();
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipCode = equipCode.trim();
				filter.setEuqipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seri)) {
				seri = seri.trim();
				filter.setSeri(seri);
			}
			if (!StringUtil.isNullOrEmpty(formCode)) {
				formCode = formCode.trim();
				filter.setFormCode(formCode);
			}
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				customerCode = customerCode.trim();
				filter.setCustomerCode(customerCode);
			}
			if (!StringUtil.isNullOrEmpty(customerName)) {
				customerName = customerName.trim();
				filter.setCustomerName(customerName);
			}
			if (currentUser.getShopRoot() != null) {
				filter.setShopRoot(currentUser.getShopRoot().getShopId());
			}
			filter.setStrListShopId(getStrListShopId());
			/** trang thai: hoan tat (6); thanh toan: chua thanh toan (0)*/
			filter.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
			filter.setStatusPayment(PaymentStatusRepair.NOT_PAID_YET.getValue());
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shopCode = shopCode.trim();
				filter.setShopCode(shopCode);
			}
			// lay danh sach phieu sua chua not in ds getLstId duoi gridRepairItem
			if (!StringUtil.isNullOrEmpty(lstIdStr)) {
				String [] arrId  = lstIdStr.split(",");
				lstId = new ArrayList<Long>();
				for (int i = 0, sz = arrId.length; i < sz; i++) {
					if (!StringUtil.isNullOrEmpty(arrId[i])) {
						lstId.add(Long.parseLong(arrId[i]));
					}
				}
				if (lstId != null && lstId.size() > 0) {
					filter.setLstId(lstId);
				}
			}
			ObjectVO<EquipRepairFormVO> temp = equipmentRepairPayFormMgr.getListEquipRepairPopup(filter);

			if (temp != null && temp.getLstObject() != null && temp.getLstObject().size() > 0) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipRepairFormVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairPayFormAction.searchRepairPopup()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	/**
	 * Tra ve tra lap phieu thanh toan sua chua hay cap nhat phieu thanh toan sua chua
	 * @author vuongmq
	 * @since 22/06/2015
	 * @return
	 */
	public String getChangeForm() {
		try {
			if (id != null && id > 0) {
				/** truong hop thong tin chi tiet icon_view */
				equipmentRepairPayment = equipmentRepairPayFormMgr.retrieveEquipmentRepairPayment(id);
				if (equipmentRepairPayment == null) {
					return PAGE_NOT_FOUND;
				}
				if (equipmentRepairPayment.getShop() == null || !checkShopInOrgAccessById(equipmentRepairPayment.getShop().getId())) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
					result.put(ERROR, true);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairPayFormAction.getChangeForm()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * cap nhat phieu thanh toan cho phieu sua chua thiet bi; 
	 * @author vuongmq
	 * @date 26/06/2015
	 * @return ket qua cap nhat thanh cong/that bai, luu trong bien result
	 * 
	 */
	public String updateEquipmentRepairPaymentRecord() {
		resetToken(result);
		try {
			equipmentRepairPaymentRecord = new EquipmentRepairPaymentRecord();
			if (currentUser == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			} else {
				if (currentUser.getShopRoot() == null) {
					// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				} else {
					equipmentRepairPaymentRecord.setShopRoot(currentUser.getShopRoot().getShopId());
				}
			}
			if (!StringUtil.isNullOrEmpty(paymentDate)) {
				Date pDate = DateUtil.parse(paymentDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				equipmentRepairPaymentRecord.setPaymentDate(pDate);
			} else {
				result.put("errMsg", R.getResource("report.action.no.allow.fromdate.null"));
				result.put(ERROR, true);
				return JSON;
			}
			if (status != null && status > ActiveType.DELETED.getValue()) {
				equipmentRepairPaymentRecord.setStatus(StatusRecordsEquip.parseValue(status));
			} else {
				result.put("errMsg", R.getResource("equipment.repair.payment.record.status"));
				result.put(ERROR, true);
				return JSON;
			}
			List<FormErrVO> lstErr = new ArrayList<FormErrVO>();
			int DUTHAO_CHODUYET = -1; // tra ve gia tri duoi client
			if (lstId != null && lstId.size() > 0) {	
				EquipRepairFilter filter = new EquipRepairFilter();
				filter.setLstId(lstId);
				List<EquipRepairForm> lstRepair = equipmentManagerMgr.getListEquipRepairFormFilter(filter);
				if (lstRepair != null && lstRepair.size() > 0) {
					int KHONGDUYET_CHODUYET = -1;
					int DUTHAO_DUTHAO = -1;
					List<Long> lstPayDtlRepairId = null;
					if (id != null && id > ActiveType.DELETED.getValue()) {
						EquipRepairPayForm equipRepairPayForm = equipmentRepairPayFormMgr.retrieveEquipmentRepairPayment(id);
						if (equipRepairPayForm.getShop() == null || !checkShopInOrgAccessById(equipRepairPayForm.getShop().getId())) {
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
							result.put(ERROR, true);
							return JSON;
						}
						// kiem tra Không duyệt - >Chờ duyệt:
						if (StatusRecordsEquip.WAITING_APPROVAL.equals(equipmentRepairPaymentRecord.getStatus())) {
							if (equipRepairPayForm != null && StatusRecordsEquip.NO_APPROVAL.equals(equipRepairPayForm.getStatus())) {
								KHONGDUYET_CHODUYET = StatusRecordsEquip.WAITING_APPROVAL.getValue();
							}
						}
						// kiem tra Du thao - > Du thao:
						if (StatusRecordsEquip.DRAFT.equals(equipmentRepairPaymentRecord.getStatus())) {
							EquipRepairFilter filterPayDtl = new EquipRepairFilter();
							filterPayDtl.setId(id);
							List<EquipRepairPayFormDtl> lstDtl =  equipmentRepairPayFormMgr.getEquipmentRepairPaymentDtlFilter(filterPayDtl);
							if (lstDtl != null && lstDtl.size() > 0) {
								lstPayDtlRepairId = new ArrayList<Long>();
								for (EquipRepairPayFormDtl equipRepairPayFormDtl : lstDtl) {
									if (equipRepairPayFormDtl != null && equipRepairPayFormDtl.getEquipRepairForm() != null) {
										lstPayDtlRepairId.add(equipRepairPayFormDtl.getEquipRepairForm().getId());
									}
								}
							}
							DUTHAO_DUTHAO = StatusRecordsEquip.DRAFT.getValue();
						}
						if (StatusRecordsEquip.WAITING_APPROVAL.equals(equipmentRepairPaymentRecord.getStatus())) {
							if (equipRepairPayForm != null && StatusRecordsEquip.DRAFT.equals(equipRepairPayForm.getStatus())) {
								DUTHAO_CHODUYET = StatusRecordsEquip.WAITING_APPROVAL.getValue();
							}
						}
					}
					for (int i = 0, sz = lstRepair.size(); i < sz; i++) {
						EquipRepairForm equipRepairForm = lstRepair.get(i);
						/** xu ly loai bo cac dong phieu sua chua co trang thai: ! hoan thanh hoac thanh toan: ! chua thanh toan */
						// truong hop: chon du thao, tao moi
						if (StatusRecordsEquip.DRAFT.equals(equipmentRepairPaymentRecord.getStatus())) {
							// truong hop cap nhat du thao thi ko kiem tra: hoan tat va chua thanh toan  nhung payment o detail da ton tai 
							if (DUTHAO_DUTHAO == StatusRecordsEquip.DRAFT.getValue() && lstPayDtlRepairId != null && lstPayDtlRepairId.size() > 0) {
								if (!lstPayDtlRepairId.contains(equipRepairForm.getId())) {
									if (!StatusRecordsEquip.COMPLETION_REPAIRS.equals(equipRepairForm.getStatus()) 
										|| !PaymentStatusRepair.NOT_PAID_YET.equals(equipRepairForm.getStatusPayment()) ) {
										FormErrVO errVO = new FormErrVO();
										errVO.setFormCode(equipRepairForm.getEquipRepairFormCode());
										errVO.setRecordNotComplete(R.getResource("equipment.repair.payment.record.status.and.paystatus.err"));
										errVO.setFormStatusErr(1);
										lstErr.add(errVO);
										lstId.remove(lstId.indexOf(equipRepairForm.getId()));
									}
								}
							} else {
								if (!StatusRecordsEquip.COMPLETION_REPAIRS.equals(equipRepairForm.getStatus()) 
									|| !PaymentStatusRepair.NOT_PAID_YET.equals(equipRepairForm.getStatusPayment()) ) {
									FormErrVO errVO = new FormErrVO();
									errVO.setFormCode(equipRepairForm.getEquipRepairFormCode());
									errVO.setRecordNotComplete(R.getResource("equipment.repair.payment.record.status.and.paystatus.err"));
									errVO.setFormStatusErr(1);
									lstErr.add(errVO);
									lstId.remove(lstId.indexOf(equipRepairForm.getId()));
								}
							}
						}
						/** xu ly loai bo cac dong phieu sua chua co trang thai: 
						 * Không duyệt - >Chờ duyệt:
						 * a.	Phải kiểm tra là các phiếu sửa chữa phải có trạng thái thanh toán = {Chưa thanh toán} mới cho chuyển.*/
						if (KHONGDUYET_CHODUYET == StatusRecordsEquip.WAITING_APPROVAL.getValue()) {
							if (!PaymentStatusRepair.NOT_PAID_YET.equals(equipRepairForm.getStatusPayment()) ) {
									FormErrVO errVO = new FormErrVO();
									errVO.setFormCode(equipRepairForm.getEquipRepairFormCode());
									errVO.setRecordNotComplete(R.getResource("equipment.repair.payment.record.paystatus.not.pay.err"));
									errVO.setFormStatusErr(1);
									lstErr.add(errVO);
									lstId.remove(lstId.indexOf(equipRepairForm.getId()));
							}
						}
					}
				}
			} else {
				result.put("errMsg", R.getResource("equipment.repair.payment.record.equipment.repair.form.not.exists"));
				result.put(ERROR, true);
				return JSON;
			}
			if (lstErr != null && lstErr.size() > 0) {
				result.put(ERROR, false);
				result.put("lstErr", lstErr);
				return JSON;
			}
			// khong co dong detail phieu sua chua nao loi thi luu thong tin tao moi or cap nhat
			String updateMessage = equipmentRepairPayFormMgr.updateEquipmentRepairPaymentRecord(id, lstId, equipmentRepairPaymentRecord, getLogInfoVO());
			if (StringUtil.isNullOrEmpty(updateMessage)) { // update success
				result.put(ERROR, false);
				//result.put("lstErr", lstErr);
			} else { // update error
				if (updateMessage.startsWith("TT")) {
					// truong hop nay ko loi: tao moi luu xuong ma phieu de view thong tin
					result.put(ERROR, false);
					String[] arrMaVaId = updateMessage.split(";");
					result.put("maPhieu", arrMaVaId[0]); // ma phieu
					result.put("idPhieu", arrMaVaId[1]); // id phieu
					//result.put("lstErr", lstErr);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, updateMessage));
				}
			}
			//update thi se co: update ben duyet va ben quan ly tao moi; tr/hop du thao -> cho duyet ben Qly tao se load lai grid khong cho chinh sua va disable ngay thanh toan
			if (DUTHAO_CHODUYET == StatusRecordsEquip.WAITING_APPROVAL.getValue()) {
				result.put("status", DUTHAO_CHODUYET); // neu = cho duyet(1) se xu ly
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairPayFormAction.updateEquipmentRepairPaymentRecord()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Cap nhat danh sach phieu thanh toan
	 * @author vuongmq
	 * @since 25/06/2015
	 * */
	public String updateStatus() {
		resetToken(result);
		errMsg = "";
		try {
			List<FormErrVO> lstErr = new ArrayList<FormErrVO>();
			StatusRecordsEquip status = null;
			PaymentStatusRepair paymentStatusRepair = null;
			if (statusForm == null || (statusForm != null && statusForm < ActiveType.DELETED.getValue())) {
				//errMsg = "Chưa có trạng thái nào được chọn";
				errMsg = R.getResource("equip.lost.recorde.err.chua.chon.trang.thai");
			} else {
				status = StatusRecordsEquip.parseValue(statusForm);
				if (lstId != null && lstId.size() > 0) {
					EquipRepairFilter filter = new EquipRepairFilter();
					filter.setLstId(lstId);
					List<EquipRepairPayForm> lstEquipRepairPayForms = equipmentRepairPayFormMgr.retrieveListEquipmentRepairPayment(filter);
					if (lstEquipRepairPayForms != null && lstEquipRepairPayForms.size() > 0) {
						for (EquipRepairPayForm equipRepairPayForm : lstEquipRepairPayForms) {
							//check quyen thao tac
							if (equipRepairPayForm.getShop() == null || !checkShopInOrgAccessById(equipRepairPayForm.getShop().getId())) {
								lstEquipRepairPayForms.remove(equipRepairPayForm);
							}
						}
						if (StatusRecordsEquip.WAITING_APPROVAL.equals(status)) {
							// chon cho duyet: thi phai la Du thao, khong duyet
							paymentStatusRepair = PaymentStatusRepair.PAID_TRANSACTION;
							for (EquipRepairPayForm equipRepairPayForm : lstEquipRepairPayForms) {
								if (!StatusRecordsEquip.DRAFT.equals(equipRepairPayForm.getStatus()) && !StatusRecordsEquip.NO_APPROVAL.equals(equipRepairPayForm.getStatus())) {
									FormErrVO errVO = new FormErrVO();
									errVO.setFormCode(equipRepairPayForm.getCode());
									errVO.setRecordNotComplete(R.getResource("equipment.repair.payment.record.status.draff.no.approved.err"));
									errVO.setFormStatusErr(1);
									lstErr.add(errVO);
									lstId.remove(lstId.indexOf(equipRepairPayForm.getId()));
								} else if (StatusRecordsEquip.NO_APPROVAL.equals(equipRepairPayForm.getStatus())) {
									// if la khong duyet: a.	Phải kiểm tra là các phiếu sửa chữa phải có trạng thái thanh toán = {Chưa thanh toán} mới cho chuyển.
									EquipRepairFilter filterD = new EquipRepairFilter();
									filterD.setId(equipRepairPayForm.getId());
									List<EquipRepairPayFormDtl> lstEquipRepairPayFormDtls = equipmentRepairPayFormMgr.getEquipmentRepairPaymentDtlFilter(filterD);
									if (lstEquipRepairPayFormDtls != null && lstEquipRepairPayFormDtls.size() > 0) {
										boolean PTT = false; 
										// PTT: ton tai mot phieu sua chua ma khac trang thai chua thanh toan: bao loi tung phieu; sau do se remove Id trong lstID
										StringBuilder errPSC = new StringBuilder();
										for (EquipRepairPayFormDtl equipRepairPayFormDtl : lstEquipRepairPayFormDtls) {
											if (equipRepairPayFormDtl != null && equipRepairPayFormDtl.getEquipRepairForm() != null) {
												EquipRepairForm equipRepairForm = equipRepairPayFormDtl.getEquipRepairForm();
												if (!PaymentStatusRepair.NOT_PAID_YET.equals(equipRepairForm.getStatusPayment()) ) {
													errPSC.append(", ").append(equipRepairForm.getEquipRepairFormCode());
													PTT = true;
												}
											}
										}
										if (PTT) {
											FormErrVO errVO = new FormErrVO();
											String viewErr = errPSC.toString();
											errVO.setFormCode(equipRepairPayForm.getCode() + "; PSC(" + viewErr.replaceFirst(", ",  "") + ")");
											errVO.setRecordNotComplete(R.getResource("equipment.repair.payment.record.paystatus.not.pay.err"));
											errVO.setFormStatusErr(1);
											lstErr.add(errVO);
											lstId.remove(lstId.indexOf(equipRepairPayForm.getId()));
										}
									}
									
								}
							}
						}
						if (StatusRecordsEquip.CANCELLATION.equals(status)) {
							// chon huy: thi phai la Du thao, khong duyet, cho duyet
							paymentStatusRepair = PaymentStatusRepair.NOT_PAID_YET;
							for (EquipRepairPayForm equipRepairPayForm : lstEquipRepairPayForms) {
								if (!StatusRecordsEquip.DRAFT.equals(equipRepairPayForm.getStatus()) && !StatusRecordsEquip.NO_APPROVAL.equals(equipRepairPayForm.getStatus()) && !StatusRecordsEquip.WAITING_APPROVAL.equals(equipRepairPayForm.getStatus())) {
									FormErrVO errVO = new FormErrVO();
									errVO.setFormCode(equipRepairPayForm.getCode());
									errVO.setRecordNotComplete(R.getResource("equipment.repair.payment.record.status.draff.no.approved.waiting.approved.err"));
									errVO.setFormStatusErr(1);
									lstErr.add(errVO);
									lstId.remove(lstId.indexOf(equipRepairPayForm.getId()));
								}
							}
						}
						if (StatusRecordsEquip.NO_APPROVAL.equals(status) || StatusRecordsEquip.APPROVED.equals(status)) {
							// chon khong duyet or duyet: thi phai la Du thao, khong duyet, cho duyet
							if (StatusRecordsEquip.NO_APPROVAL.equals(status)) {
								// khong duyet
								paymentStatusRepair = PaymentStatusRepair.NOT_PAID_YET;
							} else {
								// duyet
								paymentStatusRepair = PaymentStatusRepair.PAID;
							}
							for (EquipRepairPayForm equipRepairPayForm : lstEquipRepairPayForms) {
								if (!StatusRecordsEquip.WAITING_APPROVAL.equals(equipRepairPayForm.getStatus())) {
									FormErrVO errVO = new FormErrVO();
									errVO.setFormCode(equipRepairPayForm.getCode());
									errVO.setRecordNotComplete(R.getResource("equipment.repair.payment.record.status.waiting.approved.err"));
									errVO.setFormStatusErr(1);
									lstErr.add(errVO);
									lstId.remove(lstId.indexOf(equipRepairPayForm.getId()));
								}
							}
						}
					} else {
						errMsg = R.getResource("equipment.repair.payment.record.choose");
					}
				} else {
					errMsg = R.getResource("equipment.repair.payment.record.choose");
				}
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
			// xu ly cap nhat cac id trong list OK, con co lstErr thi view len popup
			EquipRepairFilter filterAdd = new EquipRepairFilter();
			filterAdd.setLstId(lstId);
			filterAdd.setStatusRecordsEquip(status);
			filterAdd.setPaymentStatusRepair(paymentStatusRepair);
			equipmentRepairPayFormMgr.updateStatus(filterAdd, getLogInfoVO());
			result.put(ERROR, false);
			result.put("lstErr", lstErr);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairPayFormAction.updateStatus()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	
	/**
	 * @author vuongmq
	 * @date 29/06/2015
	 * Xuat file excel danh sach phieu thanh toan theo check chon
	 * @return
	 * @throws IOException 
	 */
	public String exportRepairPayForm() throws IOException {
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			EquipRepairFilter filter = new EquipRepairFilter();
			if (currentUser == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			} else {
				if (currentUser.getShopRoot() == null) {
					// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				} else {
					filter.setShopRoot(currentUser.getShopRoot().getShopId());
				}
			}
			if (lstId == null || lstId.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			filter.setLstId(lstId);
			List<EquipRepairPayFormVO> lstPayForm = equipmentRepairPayFormMgr.exportRepairPayForm(filter);
			if (null != lstPayForm && lstPayForm.size() > 0) {
				// Init XSSF workboook
				String outputName = ConstantManager.TEMPLATE_EQUIP_LIST_REPAIR_PAYFORM_EXPORT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
				String exportFileName = Configuration.getStoreRealPath() + outputName;

				workbook = new SXSSFWorkbook(200);
				workbook.setCompressTempFiles(true);
				// Tao shett
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("equipment.repair.payment.record.export.sheet"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

				// Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(18);
				// set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 225, 100, 160, 200, 225, 100, 160);
				// Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 20);
				//sheetData.setDisplayGridlines(true);

				int iRow = 0;
				int iColumn = 0;
				// tao column header
				String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.repair.payment.record.header.export");
				String[] lstHeaders = headers.split(";");
				for (int i = 0, szHeader = lstHeaders.length; i < szHeader; i++) {
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				}
				iRow++;
				// xuat danh sach du lieu
				String maPhieu = "";
				for (int i = 0, size = lstPayForm.size(); i < size; i++) {
					EquipRepairPayFormVO item = lstPayForm.get(i);
					if (!maPhieu.equals(item.getMaPhieu())) {
						maPhieu = item.getMaPhieu();
					} else {
						item.setMaPhieu("");
						item.setNgayLap("");
						item.setTrangThai(-1);
						item.setTongTien(null);
					}
					iColumn = 0;
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getMaPhieu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getNgayLap(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getTrangThaiStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getTongTien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_NOT_ZERO));
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getMaPhieuSuaChua(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getNgayTao(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getTongTienSuaChua(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					iRow++;
				}
				out = new FileOutputStream(exportFileName);
				workbook.write(out);
				out.close();
				workbook.dispose();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(REPORT_PATH, outputPath);
				result.put(ERROR, false);
				result.put("hasData", true);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
				result.put(ERROR, true);
				result.put("data.hasData", true);
				result.put("errMsg", errMsg);
			}
		} catch (Exception e) {
			//errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentRepairPayFormAction.exportRepairPayForm()"), createLogErrorStandard(actionStartTime));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getStatusAction() {
		return statusAction;
	}

	public void setStatusAction(Integer statusAction) {
		this.statusAction = statusAction;
	}

	public Integer getStatusForm() {
		return statusForm;
	}

	public void setStatusForm(Integer statusForm) {
		this.statusForm = statusForm;
	}

	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getEquipCategoryId() {
		return equipCategoryId;
	}

	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}

	public List<EquipCategory> getLstEquipCategory() {
		return lstEquipCategory;
	}

	public void setLstEquipCategory(List<EquipCategory> lstEquipCategory) {
		this.lstEquipCategory = lstEquipCategory;
	}

	public List<EquipProvider> getLstEquipProvider() {
		return lstEquipProvider;
	}

	public void setLstEquipProvider(List<EquipProvider> lstEquipProvider) {
		this.lstEquipProvider = lstEquipProvider;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public Integer getStatusPayment() {
		return statusPayment;
	}

	public void setStatusPayment(Integer statusPayment) {
		this.statusPayment = statusPayment;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public Long getEquipRepairId() {
		return equipRepairId;
	}

	public void setEquipRepairId(Long equipRepairId) {
		this.equipRepairId = equipRepairId;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public EquipmentRepairPaymentRecord getEquipmentRepairPaymentRecord() {
		return equipmentRepairPaymentRecord;
	}

	public void setEquipmentRepairPaymentRecord(EquipmentRepairPaymentRecord equipmentRepairPaymentRecord) {
		this.equipmentRepairPaymentRecord = equipmentRepairPaymentRecord;
	}

	public EquipRepairPayForm getEquipmentRepairPayment() {
		return equipmentRepairPayment;
	}

	public void setEquipmentRepairPayment(EquipRepairPayForm equipmentRepairPayment) {
		this.equipmentRepairPayment = equipmentRepairPayment;
	}

	public List<Map<String, Object>> getListObjectBeans() {
		return listObjectBeans;
	}

	public void setListObjectBeans(List<Map<String, Object>> listObjectBeans) {
		this.listObjectBeans = listObjectBeans;
	}

	public String getLstIdStr() {
		return lstIdStr;
	}

	public void setLstIdStr(String lstIdStr) {
		this.lstIdStr = lstIdStr;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

}

package ths.dms.web.action.equipment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ths.dms.core.business.EquipStockPermissionMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleUser;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipPermissionFilter;
import ths.dms.core.entities.filter.EquipStaffFilter;
import ths.dms.core.entities.vo.EquipmentPermissionVO;
import ths.dms.core.entities.vo.EquipmentStaffVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class EquipmentManagePermissionAction extends AbstractAction {

	/** field BREAK_LINE field String */
	private static final String BREAK_LINE = "\n";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String PAGING = "PAGING";
	private static final String NO_PAGING = "NO_PAGING";
	private String personalCode;
	private String personalName;
	private Integer personalStatus;
	private String permissionCode;
	private String permissionName;
	private String listShopId;
	private Integer permissionStatus;
	private Long staffId;
	private List<String> lstRoleId;
	private List<String> lstRoleUserId;
	private Long permissionId;
	private Integer status;

	/** vuongmq; import excel */
	private File excelFile;
	private String excelFileContentType;

	private EquipmentManagerMgr equipmentManagerMgr;
	private EquipStockPermissionMgr equipStockPermissionMgr;

	@Override
	public void prepare() throws Exception {
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		equipStockPermissionMgr = (EquipStockPermissionMgr) context.getBean("equipStockPermissionMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}

		if (shop != null) {
			shopId = shop.getId();
		}
		return SUCCESS;
	}

	/**
	 * Filter de lay param sear danh sach nguoi dung, dung chung cho searchStaff
	 * va export Excel Tim kiem nguoi dung
	 * 
	 * @author vuongmq
	 * @date 26/03/2015
	 */
	private EquipStaffFilter getFilterStaffPermission(String paging) {
		EquipStaffFilter filter = new EquipStaffFilter();
		try {
			if (PAGING.equals(paging)) {
				result.put("page", page);
				result.put("max", max);
				KPaging<EquipmentStaffVO> kPaging = new KPaging<EquipmentStaffVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);

				filter.setkPaging(kPaging);
			}
			if (personalCode != null) {
				if (!personalCode.isEmpty()) {
					filter.setPersonalCode(personalCode);
				}
			}
			if (personalName != null) {
				if (!personalName.isEmpty()) {
					filter.setPersonalName(personalName);
				}
			}
			if (personalStatus != null) {
				filter.setPersonalStatus(personalStatus);
			}

			Shop shopStaff = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			if (shopStaff != null) {
				filter.setShopId(shopStaff.getId());
			}
			if (currentUser.getStaffRoot().getStaffId() != null) {
				filter.setStaffId(currentUser.getStaffRoot().getStaffId());
			}

			Integer flagCMS = ActiveType.RUNNING.getValue();
			filter.setFlagCMS(flagCMS);
		} catch (BusinessException e) {
			LogUtility.logError(e, "EquipmentManagePermissionAction.getFilterStaffPermission()" + e.getMessage());
		}
		return filter;
	}

	/**
	 * Tim kiem nguoi dung
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param
	 * @throws
	 */
	public String searchStaff() {
		try {
			/*
			 * result.put("page", page); result.put("max", max);
			 * KPaging<EquipmentStaffVO> kPaging = new
			 * KPaging<EquipmentStaffVO>(); kPaging.setPageSize(max);
			 * kPaging.setPage(page - 1); EquipStaffFilter filter = new
			 * EquipStaffFilter();
			 * 
			 * filter.setkPaging(kPaging); if(personalCode != null) {
			 * if(!personalCode.isEmpty()) {
			 * filter.setPersonalCode(personalCode); } } if(personalName !=
			 * null) { if(!personalName.isEmpty()) {
			 * filter.setPersonalName(personalName); } } if(personalStatus !=
			 * null) { filter.setPersonalStatus(personalStatus); }
			 * 
			 * Shop shopStaff =
			 * shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			 * if(shopStaff != null){ filter.setShopId(shopStaff.getId()); }
			 * if(currentUser.getStaffRoot().getStaffId() != null) {
			 * filter.setStaffId(currentUser.getStaffRoot().getStaffId()); }
			 * 
			 * Integer flagCMS = 1; filter.setFlagCMS(flagCMS);
			 */
			/**
			 * dung chung getFilterStaffPermission cho search Staff va
			 * exporExcel
			 */
			EquipStaffFilter filter = this.getFilterStaffPermission(PAGING);
			StringBuilder sb = new StringBuilder(super.getStrListUserId());
			sb.append(", ");
			sb.append(currentUser.getUserId());
			filter.setStrListUserId(sb.toString());
			filter.setLstShopId(listShopId);

			ObjectVO<EquipmentStaffVO> lsEquipmentStaffVO = equipmentManagerMgr.getStaffForPermission(filter);
			if (lsEquipmentStaffVO != null && lsEquipmentStaffVO.getLstObject() != null && lsEquipmentStaffVO.getLstObject().size() > 0) {
				result.put("total", lsEquipmentStaffVO.getkPaging().getTotalRows());
				result.put("rows", lsEquipmentStaffVO.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipmentStaffVO>());
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem quyen cua nguoi dung
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param
	 * @throws
	 */
	public String searchPermission() {
		result.put("page", page);
		result.put("max", max);
		
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentPermissionVO>());
		
		KPaging<EquipmentPermissionVO> kPaging = new KPaging<EquipmentPermissionVO>();
		kPaging.setPageSize(10);
		kPaging.setPage(page - 1);
		EquipPermissionFilter filter = new EquipPermissionFilter();

		filter.setkPaging(kPaging);
		if (permissionCode != null) {
			if (!permissionCode.isEmpty()) {
				filter.setPermissionCode(permissionCode);
			}
		}
		if (permissionName != null) {
			if (!permissionName.isEmpty()) {
				filter.setPermissionName(permissionName);
			}
		}
		if (permissionStatus != null) {
			filter.setPermissionStatus(permissionStatus);
		}
		if (staffId != null) {
			if (super.getMapUserId().get(staffId) == null) {
				return JSON;
			}
			filter.setStaffId(staffId);
		} else {
			return JSON;
		}

		try {
			ObjectVO<EquipmentPermissionVO> lsEquipRole = equipmentManagerMgr.getPermissionByFilter(filter);
			if (lsEquipRole != null && lsEquipRole.getLstObject() != null && lsEquipRole.getLstObject().size() > 0) {
				result.put("total", lsEquipRole.getkPaging().getTotalRows());
				result.put("rows", lsEquipRole.getLstObject());
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}

		return JSON;
	}

	public List<EquipmentPermissionVO> getListPermissionForUser(Long id) {
		EquipPermissionFilter filter = new EquipPermissionFilter();
		List<EquipmentPermissionVO> lst = new ArrayList<EquipmentPermissionVO>();
		filter.setkPaging(null);
		if (id != null) {
			filter.setStaffId(id);
		} else {
			return null;
		}
		try {
			ObjectVO<EquipmentPermissionVO> lsEquipRole = equipmentManagerMgr.getPermissionByFilter(filter);
			if (lsEquipRole != null) {
				lst = lsEquipRole.getLstObject();
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lst;
	}

	/**
	 * them quyen cho nguoi dung
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param list
	 *            quyen, staffId
	 * @throws
	 */
	public String addPermission() throws IOException {
		Date sysDateCrt;
		resetToken(result);
		try {
			sysDateCrt = commonMgr.getSysDate();
			if (super.getMapUserId().get(staffId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("catalog.staff.code.permission"));
				return JSON;
			}
			Staff staff = staffMgr.getStaffById(staffId);
			if (staff == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("comon.not.exist", R.getResource("common.label.staff")));
				return JSON;
			}
			if (lstRoleId != null && lstRoleId.size() > 0) {
				for (int i = 0, sz = lstRoleId.size(); i < sz; i++) {
					EquipRoleUser equipRoleUser = new EquipRoleUser();
					equipRoleUser.setCreateDate(sysDateCrt);
					equipRoleUser.setCreateUser(currentUser.getUserName());

					equipRoleUser.setStaff(staff);
					List<EquipmentPermissionVO> lst = this.getListPermissionForUser(staffId);
					if (lst != null && lst.size() == 0) {
						equipRoleUser.setStatus(ActiveType.RUNNING);
					} else {
						equipRoleUser.setStatus(ActiveType.STOPPED);
					}

					EquipRole equipRole = equipmentManagerMgr.getEquipRoleById(Long.parseLong(lstRoleId.get(i)));
					if (equipRole != null) {
						equipRoleUser.setEquipRole(equipRole);
					}
					equipRoleUser = equipmentManagerMgr.createEquipRoleUser(equipRoleUser);
				}
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}

		return JSON;
	}

	/**
	 * cap nhat quyen cho nguoi dung
	 * 
	 * @author tamvnm
	 * @since 26/03/2015
	 * @param staffId
	 *            , permissionId, status;
	 * @throws
	 */
	public String updatePermission() {
		resetToken(result);
		String roleCode = "";
		try {
			if (lstRoleUserId != null && lstRoleUserId.size() > 0) {
				int szRole = lstRoleUserId.size();
				errMsg = "";
				// check quyen co thuoc cua user phan quyen 
				for (int i = 0; i < szRole; i++) {
					EquipRoleUser equipRoleUser = commonMgr.getEntityById(EquipRoleUser.class, Long.parseLong(lstRoleUserId.get(i)));
					if (equipRoleUser == null) {
						errMsg = R.getResource("comon.not.exist", R.getResource("equipment.role.code"));
						break;
					}
					if (equipRoleUser.getStaff() != null) {
						if (super.getMapUserId().get(equipRoleUser.getStaff().getId()) == null) {
							errMsg = R.getResource("equip.manager.staff.permission.role.user.not.err");
							break;
						}
					} else {
						errMsg = R.getResource("equip.manager.staff.permission.role.not.user");
						break;
					}
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put(ERR_MSG, errMsg);
					return JSON;
				}
				// cap nhat trang thai
				for (int i = 0; i < szRole; i++) {
					EquipRoleUser equipRoleUser = commonMgr.getEntityById(EquipRoleUser.class, Long.parseLong(lstRoleUserId.get(i)));

					if (equipRoleUser != null && equipRoleUser.getStaff() != null && equipRoleUser.getStaff().getId() != null) {
						List<EquipmentPermissionVO> lstRole = this.getListPermissionForUser(equipRoleUser.getStaff().getId());
						if (status != null) {
							switch (status) {
							case -1:
								commonMgr.deleteEntity(equipRoleUser);
								break;
							case 0:
								equipRoleUser.setStatus(ActiveType.STOPPED);
								equipRoleUser.setUpdateUser(currentUser.getUserName());
								equipRoleUser.setUpdateDate(new Date());
								commonMgr.updateEntity(equipRoleUser);
								break;
							case 1:
								Boolean isTrue = true;
								if (lstRole != null && lstRole.size() > 0) {
									for (int j = 0, jsize = lstRole.size(); j < jsize; j++) {
										if (lstRole.get(j).getStatus().equals(ActiveType.RUNNING.getValue())) {
											roleCode = lstRole.get(j).getRoleCode();
											isTrue = false;
											break;
										}
									}
								}
								if (isTrue) {
									equipRoleUser.setStatus(ActiveType.RUNNING);
									equipRoleUser.setUpdateUser(currentUser.getUserName());
									equipRoleUser.setUpdateDate(new Date());
									commonMgr.updateEntity(equipRoleUser);
									break;
								} else {
									result.put(ERROR, true);
									String code = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.role.code");
									String run = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.running");
									result.put("errMsg", code + roleCode + run);
								}
							default:
								break;
							}
						}
					}
				}
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * Xuat Excel theo Tim kiem gan quyen nguoi dung
	 * 
	 * @author vuongmq
	 * @since Mar 26,2015
	 * @return Excel
	 * */
	public String exportExcelBySearchEquipStaffPermission() {
		try {
			/* begin vuongmq; 11/05/2015; xuat Excel ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			/* end vuongmq; 11/05/2015; xuat Excel ATTT */
			/** goi ham thuc thi tim kiem */
			EquipStaffFilter filter = this.getFilterStaffPermission(NO_PAGING);
			filter.setStrListUserId(super.getStrListUserId());
			ObjectVO<EquipmentStaffVO> lsEquipmentStaffVO = equipmentManagerMgr.getStaffForPermissionExportExcel(filter);
			if (lsEquipmentStaffVO != null && lsEquipmentStaffVO.getLstObject() != null && lsEquipmentStaffVO.getLstObject().size() > 0) {
				HashMap<String, Object> beans = new HashMap<String, Object>();
				beans.put("lst", lsEquipmentStaffVO.getLstObject());
				String outputPath = ReportUtils.exportExcelJxlsx(beans, ShopReportTemplate.QLTB_GANQUYENNGUOIDUNG, FileExtension.XLSX);
				result.put(ERROR, false);
				result.put(REPORT_PATH, outputPath);
				/* vuongmq; 11/05/2015; xuat Excel ATTT */
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				//common.export.excel.null = Không có dữ liệu để xuất file Excel
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.export.excel.null"));
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagePermissionAction.exportExcelBySearchEquipStaffPermission()" + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Nhap Excel gan quyen nguoi dung
	 * 
	 * @author vuongmq
	 * @since Mar 26,2015
	 * @description Import Excel
	 * */
	public String importEquipStaffPermission() {
		generateToken();
		isError = true;
		totalItem = 0;
		try {
			//Xu ly nghiep vu import file
			lstView = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 3);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				String message = "";
				String msg = "";
				List<Long> lstId = this.getListUserInherit();
				//boolean flag = false;
				Date sys = commonMgr.getSysDate();
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					message = "";
					msg = "";
					totalItem++;
					List<String> row = lstData.get(i);
					Staff staff = null;
					EquipRole equipRole = null;
					//Ma nguoi dung
					String valueStaffCode = row.get(0);
					if (!StringUtil.isNullOrEmpty(valueStaffCode)) {
						valueStaffCode = valueStaffCode.trim();
						/** ma nguoi dung khong duoc nhap ky tu dac biet */
						String errStaffRole = ValidateUtil.getErrorMsgOfSpecialCharInCode(valueStaffCode, "equip.manager.staff.permission.staff.name");
						if (!StringUtil.isNullOrEmpty(errStaffRole)) {
							msg += errStaffRole;
						}
						staff = staffMgr.getStaffByCode(valueStaffCode);
						if (staff != null) {
							if (!ActiveType.RUNNING.equals(staff.getStatus())) {
								//common.catalog.status.in.active = {0} đang ở trạng thái không hoạt động.
								msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, valueStaffCode) + BREAK_LINE;
							} else if (lstId.indexOf(staff.getId()) == -1) {
								msg += R.getResource("common.not.belong.userlogin", valueStaffCode) + BREAK_LINE;
							}
						} else {
							// {0} khong co trong he thong
							msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, valueStaffCode) + BREAK_LINE;
						}
					} else {
						// Ban chua nhap gia tri cho truong ma nguoi dung
						msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("equip.manager.staff.permission.staff.name")) + BREAK_LINE;
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						message += msg;
					}
					//Ma quyen: equipment.role.code
					String valueRoleCode = row.get(1);
					msg = "";
					if (!StringUtil.isNullOrEmpty(valueRoleCode)) {
						valueRoleCode = valueRoleCode.trim();
						String errCodeRole = ValidateUtil.validateField(valueRoleCode, "equipment.role.code", 50, ConstantManager.ERR_MAX_LENGTH_EQUALS);
						if (!StringUtil.isNullOrEmpty(errCodeRole)) {
							msg += errCodeRole;
						}
						errCodeRole = "";
						errCodeRole = ValidateUtil.getErrorMsgOfSpecialCharInCode(valueRoleCode, "equipment.role.code");
						if (!StringUtil.isNullOrEmpty(errCodeRole)) {
							msg += errCodeRole;
						}
						equipRole = equipStockPermissionMgr.getEquipRoleByCodeEx(valueRoleCode, null);
						if (equipRole != null) {
							if (!ActiveType.RUNNING.equals(equipRole.getStatus())) {
								//common.catalog.status.in.active = {0} đang ở trạng thái không hoạt động.
								msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, valueRoleCode) + BREAK_LINE;
							} else {
								// quyen nay da gan cho nguoi dung
								if (staff != null) {
									EquipPermissionFilter filter = new EquipPermissionFilter();
									filter.setRoleId(equipRole.getId());
									filter.setStaffId(staff.getId());
									EquipRoleUser equipRoleUser = equipmentManagerMgr.getEquipEquipRoleUserByFilter(filter);
									if (equipRoleUser != null) {
										msg += R.getResource("equip.manager.staff.permission.role.assign.staff.err", equipRole.getCode(), staff.getStaffCode()) + BREAK_LINE;
									}
								}
							}
						} else {
							// {0} khong co trong he thong
							msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, valueRoleCode) + BREAK_LINE;
						}
					} else {
						// Ban chua nhap gia tri cho truong ma quyen
						msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("equipment.role.code")) + BREAK_LINE;
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						message += msg;
					}
					// trang thai: equipment.manager.group.status
					String valueStatus = row.get(2);
					ActiveType statusRole = null;
					msg = "";
					if (!StringUtil.isNullOrEmpty(valueStatus)) {
						valueStatus = valueStatus.trim();
						if (valueStatus.toUpperCase().equals(R.getResource("equipment.manager.status.running").trim().toUpperCase()) || valueStatus.toUpperCase().equals(R.getResource("equipment.manager.status.stopped").trim().toUpperCase())) {
							if (valueStatus.toUpperCase().equals(R.getResource("equipment.manager.status.running").trim().toUpperCase())) {
								statusRole = ActiveType.RUNNING;
							} else if (valueStatus.toUpperCase().equals(R.getResource("equipment.manager.status.stopped").trim().toUpperCase())) {
								statusRole = ActiveType.STOPPED;
							}
						} else {
							// contract.invalid = {0} không hợp lệ
							msg = R.getResource("contract.invalid", R.getResource("equipment.manager.group.status")) + "\n ";
						}
					} else {
						// Ban chua nhap gia tri cho truong trang thai
						msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, R.getResource("equipment.manager.group.status")) + "\n ";
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						message += msg;
					}
					//XU LY CAP NHAT VAO DB
					if (StringUtil.isNullOrEmpty(message)) {
						/*** thuc hien them moi quyen vao user */
						EquipRoleUser equipRoleUserNew = new EquipRoleUser();
						equipRoleUserNew.setStatus(statusRole);
						equipRoleUserNew.setEquipRole(equipRole);
						equipRoleUserNew.setStaff(staff);
						equipRoleUserNew.setCreateDate(sys);
						equipRoleUserNew.setCreateUser(currentUser.getUserName());
						commonMgr.createEntity(equipRoleUserNew);
					} else {
						lstView.add(StringUtil.addFailBean(row, message));
					}
				}
				//Export error lstview
				getOutputFailExcelFile(lstView, ConstantManager.EQUIP_ROLE_ASSIGN_STAFF_IMPORT_FAIL);
			} else {
				isError = true;
				if (StringUtil.isNullOrEmpty(errMsg)) {
					//customer.display.program.nodata = Không có dữ liệu import. 
					errMsg = R.getResource("customer.display.program.nodata");
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipmentManagePermissionAction.importEquipStaffPermission()" + e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	public String getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}

	public String getPersonalName() {
		return personalName;
	}

	public void setPersonalName(String personalName) {
		this.personalName = personalName;
	}

	public Integer getPersonalStatus() {
		return personalStatus;
	}

	public void setPersonalStatus(Integer personalStatus) {
		this.personalStatus = personalStatus;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public Integer getPermissionStatus() {
		return permissionStatus;
	}

	public void setPermissionStatus(Integer permissionStatus) {
		this.permissionStatus = permissionStatus;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<String> getLstRoleId() {
		return lstRoleId;
	}

	public void setLstRoleId(List<String> lstRoleId) {
		this.lstRoleId = lstRoleId;
	}

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<String> getLstRoleUserId() {
		return lstRoleUserId;
	}

	public void setLstRoleUserId(List<String> lstRoleUserId) {
		this.lstRoleUserId = lstRoleUserId;
	}

	public String getListShopId() {
		return listShopId;
	}

	public void setListShopId(String listShopId) {
		this.listShopId = listShopId;
	}

}

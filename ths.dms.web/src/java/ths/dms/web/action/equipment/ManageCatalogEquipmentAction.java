package ths.dms.web.action.equipment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipItem;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipSalePlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.EquipmentChannelTypeObjectType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipmentSalePlaneFilter;
import ths.dms.core.entities.vo.EquipSalePlanVO;
import ths.dms.core.entities.vo.EquipmentGroupVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.memcached.MemcachedUtils;

/**
 * Action Quan ly thiet bi tai san - Quan ly danh muc
 * 
 * @author hoanv25
 * @since December 14,2014
 */
public class ManageCatalogEquipmentAction extends AbstractAction {

	/** field BREAK_LINE  field String */
	private static final String BREAK_LINE = "\n";

	private static final long serialVersionUID = 5330840248556870465L;

	EquipmentManagerMgr equipmentManagerMgr;
	CommonMgr commonMgr;
	StaffMgr staffMgr;
	ShopMgr shopMgr;

	@Override
	public void prepare() throws Exception {
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");

		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			listEquipCategory = equipmentManagerMgr.getListEquipmentCategory(filter);
			listEquipProvider = equipmentManagerMgr.getListEquipmentProvider(filter);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}

		return SUCCESS;
	}

	/**
	 * Search danh muc thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 10/12/2014
	 */
	public String search() {
		result.put("rows", new ArrayList<EquipmentVO>());
		result.put("total", 0);
		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			KPaging<EquipmentVO> kPaging = new KPaging<EquipmentVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
			if (status != null) {
				filter.setStatus(status);
			}
			if (type != null) {
				filter.setType(type);
			}
			ObjectVO<EquipmentVO> lstEquipment = null;
			lstEquipment = equipmentManagerMgr.getListEquipmentGeneral(filter);
			if (lstEquipment != null && lstEquipment.getLstObject() != null && !lstEquipment.getLstObject().isEmpty()) {
				result.put("total", lstEquipment.getkPaging().getTotalRows());
				result.put("rows", lstEquipment.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return JSON;
	}

	/**
	 * Luu thong tin danh muc thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 15/12/2014
	 * @description Update equipment
	 */
	public String saveInfo() throws Exception {
		resetToken(result);
		String errMsg = "";
		try {
			if (id != null && id > 0) {
				if (type != null && type == 1) {
					EquipCategory equipCategory = equipmentManagerMgr.getEquipCategoryById(id);
					// tam ngung moi kiem tra
					if (status != null && ActiveType.STOPPED.getValue().equals(status)) {
						// boolean flagStatus = true;
						Integer flagStatus = 1;
						EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
						filter.setId(id);
						ObjectVO<EquipmentVO> listEquipGroup = equipmentManagerMgr.getListEquipGroupById(filter);
						if (listEquipGroup != null && listEquipGroup.getLstObject() != null && listEquipGroup.getLstObject().size() > 0) {
							int sz = listEquipGroup.getLstObject().size();
							for (int i = 0; i < sz; i++) {
								EquipmentVO eqVO = listEquipGroup.getLstObject().get(i);
								if (eqVO != null && !eqVO.getStatus().equals(status)) {
									flagStatus = 0;
									continue;
								}
							}
						}
						if (flagStatus == 0) {
							result.put(ERROR, true);
							errMsg = R.getResource("equipment.row.muc.doi.tuong.khac.dang.su.dung");
							result.put("errMsg", errMsg);
							return JSON;
						}
					}
					//tamvnm: update ma loai chi gioi han 2 ky tu
					if (!StringUtil.isNullOrEmpty(code)) {
						String msg = ValidateUtil.validateField(code, "equipment.catalog.manage.code.loaitb", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						if (msg.length() > 0) {
							result.put(ERROR, true);
							result.put("errMsg", msg);
							return JSON;
						}
					}
					
					equipCategory.setCode(code);
					equipCategory.setName(name);
					equipCategory.setStatus(ActiveType.parseValue(status));
					equipCategory.setUpdateDate(DateUtil.now());
					equipCategory.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
					equipCategory = equipmentManagerMgr.updateEquipCategoryManagerCatalog(equipCategory, getLogInfoVO());
				} else if (type != null && type == 2) {
					EquipProvider equipProvider = equipmentManagerMgr.getEquipProviderById(id);
					// tam ngung moi kiem tra
					if (status != null && ActiveType.STOPPED.getValue().equals(status)) {
						Integer flagStatus = 1;
						EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
						filter.setId(id);
						ObjectVO<EquipmentVO> listEquipGroup = equipmentManagerMgr.getListEquipGroupByIdEquipProvider(filter);
						if (listEquipGroup != null && listEquipGroup.getLstObject() != null && listEquipGroup.getLstObject().size() > 0) {
							int sz = listEquipGroup.getLstObject().size();
							for (int i = 0; i < sz; i++) {
								EquipmentVO eqVO = listEquipGroup.getLstObject().get(i);
								if (eqVO != null && !eqVO.getStatus().equals(status)) {
									flagStatus = 0;
									continue;
								}
							}
						}
						if (flagStatus == 0) {
							result.put(ERROR, true);
							errMsg = R.getResource("equipment.row.muc.doi.tuong.khac.dang.su.dung");
							result.put("errMsg", errMsg);
							return JSON;
						}
					}
					equipProvider.setCode(code);
					equipProvider.setName(name);
					equipProvider.setStatus(ActiveType.parseValue(status));
					equipProvider.setUpdateDate(DateUtil.now());
					equipProvider.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
					equipProvider = equipmentManagerMgr.updateEquipProviderManagerCatalog(equipProvider, getLogInfoVO());

				} else if (type != null && type == 4) {
					EquipItem equipItem = equipmentManagerMgr.getEquipItemById(id);
					// tam ngung moi kiem tra
					EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
					filter.setId(id);
					;
					ObjectVO<EquipmentVO> listEquipItem = equipmentManagerMgr.listEquipItemById(filter);
					if (listEquipItem != null && listEquipItem.getLstObject() != null && !listEquipItem.getLstObject().isEmpty()) {
						result.put(ERROR, true);
						errMsg = R.getResource("equipment.row.muc.doi.tuong.khac.dang.su.dung");
						result.put("errMsg", errMsg);
						return JSON;
					} else {
						equipItem.setCode(code);
						equipItem.setName(name);
						equipItem.setType(typeItem);
						equipItem.setWarranty(warranty);
						equipItem.setStatus(ActiveType.parseValue(status));
						equipItem.setUpdateDate(DateUtil.now());
						equipItem.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
						equipItem = equipmentManagerMgr.updateEquipItemManagerCatalog(equipItem, getLogInfoVO());
					}
				}
			} else {
				if (type != null && type == 1) {
					//tamvnm: update ma loai chi gioi han 2 ky tu
					if (!StringUtil.isNullOrEmpty(code)) {
						String msg = ValidateUtil.validateField(code, "equipment.catalog.manage.code.loaitb", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
						if (msg.length() > 0) {
							result.put(ERROR, true);
							result.put("errMsg", msg);
							return JSON;
						}
					}
					
					
					EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
					filter.setCode(code);
					ObjectVO<EquipmentVO> listEquipCategoryCode = equipmentManagerMgr.listEquipCategoryCode(filter);
					if (listEquipCategoryCode != null && listEquipCategoryCode.getLstObject() != null && !listEquipCategoryCode.getLstObject().isEmpty()) {
						result.put(ERROR, true);
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.err.code.exist");
						result.put("errMsg", errMsg);
						return JSON;
					}
					EquipCategory equipCategory = null;
					equipCategory = new EquipCategory();
					equipCategory.setCode(code);
					equipCategory.setName(name);
					equipCategory.setStatus(ActiveType.parseValue(status));
					equipCategory.setCreateDate(DateUtil.now());
					equipCategory.setCreateUser(currentUser.getStaffRoot().getStaffCode());
					equipCategory = equipmentManagerMgr.createEquipCategoryManagerCatalog(equipCategory, getLogInfoVO());
				} else if (type != null && type == 2) {
					EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
					filter.setCode(code);
					ObjectVO<EquipmentVO> listEquipProvider = equipmentManagerMgr.listEquipEquipProvider(filter);
					if (listEquipProvider != null && listEquipProvider.getLstObject() != null && !listEquipProvider.getLstObject().isEmpty()) {
						result.put(ERROR, true);
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.err.code.exist");
						result.put("errMsg", errMsg);
						return JSON;
					}
					EquipProvider equipProvider = null;
					equipProvider = new EquipProvider();
					equipProvider.setCode(code);
					equipProvider.setName(name);
					equipProvider.setStatus(ActiveType.parseValue(status));
					equipProvider.setCreateDate(DateUtil.now());
					equipProvider.setCreateUser(currentUser.getStaffRoot().getStaffCode());
					equipProvider = equipmentManagerMgr.createEquipProviderManagerCatalog(equipProvider, getLogInfoVO());
				} else if (type != null && type == 3) {

					EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
					filter.setCode(code);
					ObjectVO<EquipmentVO> listEquipBrand = equipmentManagerMgr.listEquipBrand(filter);
					if (listEquipBrand != null && listEquipBrand.getLstObject() != null && !listEquipBrand.getLstObject().isEmpty()) {
						result.put(ERROR, true);
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.err.code.exist");
						result.put("errMsg", errMsg);
						return JSON;
					}
				} else if (type != null && type == 4) {
					EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
					filter.setCode(code);
					ObjectVO<EquipmentVO> listEquipItem = equipmentManagerMgr.listEquipItem(filter);
					if (listEquipItem != null && listEquipItem.getLstObject() != null && !listEquipItem.getLstObject().isEmpty()) {
						result.put(ERROR, true);
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.err.code.exist");
						result.put("errMsg", errMsg);
						return JSON;
					}
					EquipItem equipItem = null;
					equipItem = new EquipItem();
					equipItem.setCode(code);
					equipItem.setName(name);
					equipItem.setType(typeItem);
					equipItem.setWarranty(warranty);
					equipItem.setStatus(ActiveType.parseValue(status));
					equipItem.setCreateDate(DateUtil.now());
					equipItem.setCreateUser(currentUser.getStaffRoot().getStaffCode());
					equipItem = equipmentManagerMgr.createEquipItemManagerCatalog(equipItem, getLogInfoVO());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Chuyen sang trang them moi hoac chinh sua thiet bi
	 * 
	 * @return the changed form
	 * @throws Exception
	 * 
	 * @author hoanv25
	 * @since Dec 16, 2014
	 * @return JSP
	 * @description Update equipment manager
	 */
	public String equipmentEditManageJSP() throws Exception {
		generateToken();
		if (id != null && id > 0) {
			// Chinh sua
			if (type != null) {
				if (EquipmentChannelTypeObjectType.EQM_LTB.getValue().equals(type)) {
					EquipCategory detailEtt = equipmentManagerMgr.getEquipCategoryById(id);
					equipmentVO.setCode(detailEtt.getCode());
					equipmentVO.setName(detailEtt.getName());
					equipmentVO.setStatus(detailEtt.getStatus().getValue());
				} else if (EquipmentChannelTypeObjectType.EQM_NCC.getValue().equals(type)) {
					EquipProvider detailEtt = equipmentManagerMgr.getEquipProviderById(id);
					equipmentVO.setCode(detailEtt.getCode());
					equipmentVO.setName(detailEtt.getName());
					equipmentVO.setStatus(detailEtt.getStatus().getValue());
				} else if (EquipmentChannelTypeObjectType.EQM_HMSC.getValue().equals(type)) {
					EquipItem detailEtt = equipmentManagerMgr.getEquipItemById(id);
					equipmentVO.setCode(detailEtt.getCode());
					equipmentVO.setName(detailEtt.getName());
					equipmentVO.setStatus(detailEtt.getStatus().getValue());
					equipmentVO.setType(detailEtt.getType());
					equipmentVO.setWarranty(detailEtt.getWarranty());
				} else {
					return PAGE_NOT_FOUND;
				}
			} else {
				return PAGE_NOT_FOUND;
			}
		}
		// Them moi
		return SUCCESS;
	}

	/**
	 * Load gird thong so nhom thiet bi
	 * 
	 * @throws Exception
	 * 
	 * @author hoanv25
	 * @since Dec 17, 2014
	 */
	public String viewEquipGroup() throws Exception {
		result.put("rows", new ArrayList<EquipmentVO>());
		result.put("total", 0);
		try {
			if (id != null && id > 0) {
				KPaging<EquipmentVO> kPaging = new KPaging<EquipmentVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
				filter.setkPaging(kPaging);
				if (id != null && id > 0) {
					filter.setId(id);
				}
				ObjectVO<EquipmentVO> lstEquipment = null;
				lstEquipment = equipmentManagerMgr.getEquipFlan(filter);
				if (lstEquipment != null && lstEquipment.getLstObject() != null && !lstEquipment.getLstObject().isEmpty()) {
					result.put("total", lstEquipment.getkPaging().getTotalRows());
					result.put("rows", lstEquipment.getLstObject());
				}

			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return JSON;
	}

	/**
	 * Search danh muc nhom thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 17/12/2014
	 */
	public String searchEquipmentGroup() {
		try {
			result.put("rows", new ArrayList<EquipmentVO>());
			result.put("total", 0);
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			KPaging<EquipmentVO> kPaging = new KPaging<EquipmentVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
			if (!StringUtil.isNullOrEmpty(equipBrandName)) {
				filter.setBrand(equipBrandName);
			}
			filter.setToCapacity(toCapacity);
			filter.setFromCapacity(fromCapacity);
			if (type != null) {
				filter.setType(type);
			}
			filter.setStatus(status);
			filter.setFlagStockCom(ActiveType.RUNNING.getValue());

			ObjectVO<EquipmentVO> lstGroupEquipment = null;
			lstGroupEquipment = equipmentManagerMgr.searchEquipmentGroup(filter);
			if (lstGroupEquipment != null && lstGroupEquipment.getLstObject() != null && !lstGroupEquipment.getLstObject().isEmpty()) {
				result.put("total", lstGroupEquipment.getkPaging().getTotalRows());
				result.put("rows", lstGroupEquipment.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return JSON;
	}

	/**
	 * Chuyen sang trang them moi hoac chinh sua nhom thiet bi
	 * 
	 * @return the changed form
	 * @throws Exception
	 * 
	 * @author hoanv25
	 * @since Dec 17, 2014
	 * @return JSP
	 * @description Update group equipment
	 */
	public String changeEquipmentGroupJSP() throws Exception {
		resetToken(result);
		try {
			if (id != null && id > 0) {
				EquipGroup detailGroup = equipmentManagerMgr.getEquipGroupById(id);
				if (detailGroup != null) {
					equipmentVO.setCode(detailGroup.getCode());
					equipmentVO.setName(detailGroup.getName());
					equipmentVO.setToCapacity(detailGroup.getCapacityTo());
					equipmentVO.setFromCapacity(detailGroup.getCapacityFrom());
					equipmentVO.setStatus(detailGroup.getStatus().getValue());
					equipmentVO.setBrandName(detailGroup.getEquipBrandName());
					if (detailGroup.getEquipCategory() != null) {
						equipCategoryVO.setId(detailGroup.getEquipCategory().getId());
						equipCategoryVO.setName(detailGroup.getEquipCategory().getName());
					}
				}
				EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
				listEquipCategory = equipmentManagerMgr.getListEquipmentCategory(filter);
			} else {
				EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
				listEquipCategory = equipmentManagerMgr.getListEquipmentCategory(filter);
			}
			
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.CUSTOMER);
			filter.setStatus(ActiveType.RUNNING);
			lstCustomerType = getlistCustomerTypeByFilter(filter);
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}

	private List<ChannelType> getlistCustomerTypeByFilter(ChannelTypeFilter filter) {
		List<ChannelType> lstCustomerType = new ArrayList<>(); 
		try {
			ObjectVO<ChannelType> lstCustomerTypeVO = channelTypeMgr.getListChannelType(filter, null);
			if (lstCustomerTypeVO != null && !lstCustomerTypeVO.getLstObject().isEmpty()) {
				lstCustomerType = lstCustomerTypeVO.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstCustomerType;
	}
	
	/**
	 * Save nhom danh muc thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 18/12/2014
	 * @description Update equipment group
	 * 
	 * @author hunglm16
	 * @since May 2,2015
	 * @description ra soat va thay doi cach tuong tac he thong
	 * EquipSalePlan: xoa tat ca cac dong cu de them cac dong moi
	 * @param equipGroupId
	 */
	public String saveEquipGroup() throws Exception {
		resetToken(result);
		String errMsg = "";
		result.put(ERROR, false);
		try {
			List<Long> lstDelete = new ArrayList<Long>();
			// Xu ly cho cac dong cu 
			String[] arrDeleteStr = arrDelStr.split(",");
			for (String value : arrDeleteStr) {
				if (StringUtil.isNumberInt(value)) {
					lstDelete.add(Long.valueOf(value));
				}
			}
			EquipGroup equipGroup = new EquipGroup();
			if (id != null && id > 0) {
				equipGroup = equipmentManagerMgr.getEquipGroupById(id);
				if (status != null && ActiveType.STOPPED.getValue().equals(status)) {
					EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
					filter.setId(id);
					ObjectVO<EquipmentVO> listEquipGroup = equipmentManagerMgr.getListCheckEquipGroupById(filter);
					ObjectVO<EquipmentVO> listEquipGroupProduct = equipmentManagerMgr.getListCheckEquipGroupProductByIdEquipGroup(filter);
					if ((listEquipGroup != null && listEquipGroup.getLstObject() != null && listEquipGroup.getLstObject().size() > 0)
							|| (listEquipGroupProduct != null && listEquipGroupProduct.getLstObject() != null && listEquipGroupProduct.getLstObject().size() > 0)) {
						result.put(ERROR, true);
						//errMsg = "Nhóm thiết bị dang có đối tượng khác sử dụng";
						errMsg = R.getResource("equipment.manager.group.nhom.co.tb.dang.hoat.dong");
						result.put("errMsg", errMsg);
						return JSON;
					}

				}
				equipGroup.setName(name);
				equipGroup.setCapacityFrom(fromCapacity);
				equipGroup.setCapacityTo(toCapacity);
				if (type != null && type > 0) {
					EquipCategory eCategory = equipmentManagerMgr.getEquipCategoryById(new Long(type));
					if (eCategory != null) {
						equipGroup.setEquipCategory(eCategory);
						codeCtg = eCategory.getCode();
					}
				}
				equipGroup.setEquipBrandName(equipBrandName);
				equipGroup.setStatus(ActiveType.parseValue(status));
				equipGroup.setUpdateDate(DateUtil.now());
				equipGroup.setUpdateUser(currentUser.getStaffRoot().getStaffCode());
				//equipGroup = equipmentManagerMgr.updateEquipGroup(equipGroup, getLogInfoVO());
			} else {
				
				equipGroup.setName(name);
				equipGroup.setCapacityFrom(fromCapacity);
				equipGroup.setCapacityTo(toCapacity);
				equipGroup.setEquipBrandName(equipBrandName);
				if (type != null && type > 0) {
					EquipCategory eCategory = equipmentManagerMgr.getEquipCategoryById(new Long(type));
					if (eCategory != null) {
						equipGroup.setEquipCategory(eCategory);
						codeCtg = eCategory.getCode();
					}
				}
				EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
				filter.setCode(codeCtg + code);
				ObjectVO<EquipmentVO> listEquipGroup = equipmentManagerMgr.getlistEquipGroup(filter);
				if (listEquipGroup != null && listEquipGroup.getLstObject() != null && !listEquipGroup.getLstObject().isEmpty()) {
					result.put(ERROR, true);
					errMsg = R.getResource("equipment.manager.group.code.is.exist.db");
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipGroup.setCode(codeCtg + code);
				equipGroup.setStatus(ActiveType.parseValue(status));
				equipGroup.setCreateDate(DateUtil.now());
				equipGroup.setCreateUser(currentUser.getStaffRoot().getStaffCode());
				//equipGroup = equipmentManagerMgr.createEquipGroup(equipGroup, getLogInfoVO());
			}
			// Luu gird doanh so
			EquipmentSalePlaneFilter<EquipGroup> filter = new EquipmentSalePlaneFilter<EquipGroup>();
			filter.setArrFromMonthStr(arrFromMonthStr);
			filter.setArrToMonthStr(arrToMonthStr);
			filter.setArrCustomerTypeStr(arrCustomerTypeStr);
			filter.setArrAmountStr(arrAmountStr);
			filter.setLstDelete(lstDelete);
			filter.setEquipGroupId(id);
			filter.setLogInfoVO(getLogInfoVO());
			filter.setUserName(currentUser.getUserName());
			filter.setAttribute(equipGroup);
			equipmentManagerMgr.changeEquipSalePlanByFilter(filter);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Tai tap tin excel import Danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * @return Excel
	 * */
	public String downloadTemplateImportEquipGroup() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			listEquipCategory = equipmentManagerMgr.getListEquipmentCategory(filter);
			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("rows", listEquipCategory);
			ChannelTypeFilter filterCustomerType = new ChannelTypeFilter();
			filterCustomerType.setType(ChannelTypeType.CUSTOMER);
			filterCustomerType.setStatus(ActiveType.RUNNING);
			lstCustomerType = getlistCustomerTypeByFilter(filterCustomerType);
			beans.put("lstCustomerType", lstCustomerType);
			
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
			String templateFileName = folder + ConstantManager.TEMPLATE_IMPORT_EQUIP_GROUP_EQUIPMENT_EXPORT;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			//String outputName = "Bieu_Mau_Danh_Muc_Nhom_Thiet_Bi" + ConstantManager.EXPORT_FILE_EXTENSION_EX;
			String outputName = ConstantManager.TEMPLATE_EQUIP_GROUP_EQUIPMENT_IMPORT;
			String exportFileName = (Configuration.getStoreImportDownloadPath() + outputName).replace('/', File.separatorChar);
			
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			
			result.put(ERROR, false);
			String outputPath = Configuration.getStoreImportFailDownloadPath() + outputName;
			result.put("path", outputPath);
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Import excel danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * @description Import Excel
	 * */
	public String importEquipGroup() {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		String msg = "";
		EquipmentGroupVO equipVO = new EquipmentGroupVO();
		EquipSalePlanVO spVO = new EquipSalePlanVO();
		List<EquipmentGroupVO> lstEquipVO = new ArrayList<EquipmentGroupVO>();
		List<EquipSalePlanVO> lstSalePlanVO = new ArrayList<EquipSalePlanVO>();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		String username = currentUser.getStaffRoot().getStaffCode();
		
		//tamvnm: Lay danh sach loai khach hang.
		ChannelTypeFilter filter = new ChannelTypeFilter();
		filter.setType(ChannelTypeType.CUSTOMER);
		filter.setStatus(ActiveType.RUNNING);
		lstCustomerType = getlistCustomerTypeByFilter(filter);
		
		List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, 11);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				Date now = commonMgr.getSysDate();
				lstEquipVO = new ArrayList<EquipmentGroupVO>();
				int sizeData = lstData.size();
				for (int i = 1; i < sizeData; i++) {
					List<String> row = lstData.get(i);
					List<String> rowNext = null;
					Boolean flagCheckRowNextNotEmpty = false;
					if(lstData.get(1)!=null && lstData.get(1).size()>0 && this.isEmtyRow(lstData.get(1))){
						totalItem++;
						lstFails.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.dong1.sai.dinh.dang.khong.thuc.hien.cac.dong.ben.duoi")));
						break;
					}
					if (row != null && row.size() > 0) {
						if (this.isRowKeyGroup(row) || lstEquipVO.size() == 0) {
							equipVO = new EquipmentGroupVO();
							// index row
							equipVO.setIndexRow(i);
							equipVO.setRow(row);
							// Ma nhom
							equipVO.setCode(row.get(0));
							// Ten nhom
							equipVO.setName(row.get(1));
							// Loai
							equipVO.setTypeGroup(row.get(2));
							// Hieu
							equipVO.setBrandName(row.get(3));
							// Dung tich tu
							equipVO.setCapacityFrom(row.get(4));
							// Dung tich den
							equipVO.setCapacityTo(row.get(5));
							// Trang thai
							equipVO.setStatusStr(row.get(10));
							lstEquipVO.add(equipVO);
						}
						/*** neu dong null thi thong bao loi va khong thuc hien nhung dong ben duoi */
						if ((!this.isRowKeyGroup(row) && !this.isRowKeySalePlan(row))||this.isEmtyRow(row)) { 
							for(int j=i+1 ; j < sizeData -1; j++){
								rowNext = lstData.get(i+1);
								if ((!this.isRowKeyGroup(rowNext) && !this.isRowKeySalePlan(rowNext))||this.isEmtyRow(rowNext)) {
									flagCheckRowNextNotEmpty = false;
								}else{
									flagCheckRowNextNotEmpty = true;
									break;
								}
							}
							if(flagCheckRowNextNotEmpty){
								spVO = new EquipSalePlanVO();
								spVO.setRow(row);
								spVO.setIndexRow(i);
								spVO.setIsRowEmpty(true);
								if (equipVO.getLstEquipSalePlanVO() != null && equipVO.getLstEquipSalePlanVO().size() > 0) {
									equipVO.getLstEquipSalePlanVO().add(spVO);
								} else {
									lstSalePlanVO = new ArrayList<EquipSalePlanVO>();
									lstSalePlanVO.add(spVO);
									equipVO.setLstEquipSalePlanVO(lstSalePlanVO);
								}
							}
							break;
						} else {
							// list doanh so detail
							spVO = new EquipSalePlanVO();
							spVO.setRow(row);
							spVO.setIndexRow(i);
							spVO.setIsRowEmpty(false);
							// Tu thang
							spVO.setFromMonth(row.get(6));
							// Den thang
							spVO.setToMonth(row.get(7));
							// Loai khach hang
							spVO.setCustomerTypeStr(row.get(8));
							// Doanh so
							spVO.setAmount(row.get(9));
							if (equipVO.getLstEquipSalePlanVO() != null && equipVO.getLstEquipSalePlanVO().size() > 0) {
								equipVO.getLstEquipSalePlanVO().add(spVO);
							} else {
								lstSalePlanVO = new ArrayList<EquipSalePlanVO>();
								lstSalePlanVO.add(spVO);
								equipVO.setLstEquipSalePlanVO(lstSalePlanVO);
							}
						}
					}
				}
				if (lstEquipVO != null && lstEquipVO.size() > 0) {
					List<String> row = null;
					int sizeFails = lstFails.size() - 1;
					for (int i = 0, size = lstEquipVO.size(); i < size; i++) {
						// message = "";
						msg = "";
						EquipmentGroupVO vo = lstEquipVO.get(i);
						if (vo != null) {
							// check validate nhom thiet bi
							message = this.validateRowInGroup(vo);
							if (!StringUtil.isNullOrEmpty(message)) { // Neu dong nhom thiet bi loi -> khong validate nhom muc doanh so
								int sizeSP = vo.getLstEquipSalePlanVO().size();
								if (sizeSP > 1) {
									/*** Thong bao loi tai dong cha va khong quan tam loi cua cac dong muc doanh so */
									row = vo.getRow();
									// Add them vi tri index cua lstFails
									lstFails.add(StringUtil.addFailBean(row, message));
									sizeFails = lstFails.size() - 1;
									lstFails.get(sizeFails).setContent33(vo.getIndexRow().toString());
									List<String> rowSP = null;
									EquipSalePlanVO sp = new EquipSalePlanVO();
									for (int j = 0; j < sizeSP; j++) {
										totalItem++;
										sp = vo.getLstEquipSalePlanVO().get(j);
										if (j > 0 && sp!= null) {
											rowSP = sp.getRow();
											/*** Neu co dong rong thi khong thuc hien nhung dong ben duoi va thong bao loi */
											if (sp.getIsRowEmpty()) { 
												lstFails.add(StringUtil.addFailBean(rowSP, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.khong.thuc.hien.cac.dong.ben.duoi")));
												sizeFails = lstFails.size() - 1;
												lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
											} else {/*** De null cot thong bao loi cho dong con muc doanh so */
												lstFails.add(StringUtil.addFailBean(rowSP, ""));
												sizeFails = lstFails.size() - 1;
												lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
											}
										}
									}
								} else {
									totalItem++;
									/*** Thong bao loi tai nhom thiet bi va khong quan tam loi cua cac dong muc doanh so */
									row = vo.getRow();
									lstFails.add(StringUtil.addFailBean(row, message));
									sizeFails = lstFails.size() - 1;
									lstFails.get(sizeFails).setContent33(vo.getIndexRow().toString());
								}
							} else { /*** Neu dong nhom thiet bi khong loi -> validate muc doanh so */ 
								EquipSalePlanVO sp = new EquipSalePlanVO();
								int sizeSP = vo.getLstEquipSalePlanVO().size();
								Boolean flagCheckRowSPError = false;
								int countRowSPNotFails = 0;
								Boolean flagCheckRowExistEmpty = false;
								if (sizeSP > 1) { /*** Nhom thiet bi co muc doanh so */
									for (int j = 0; j < sizeSP; j++) {
										sp = vo.getLstEquipSalePlanVO().get(j);
										row = sp.getRow();
										totalItem++;
										if (sp!= null && sp.getIsRowEmpty()) {/*** Neu co dong rong thi khong thuc hien nhung dong ben duoi va thong bao loi */
//										totalItem++;
											lstFails.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.khong.thuc.hien.cac.dong.ben.duoi")));
											sizeFails = lstFails.size() - 1;
											lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
											flagCheckRowExistEmpty = true;
										} else {
											if(!this.isRowKeySalePlan(row)){
												lstFails.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.nhom.thiet.bi.khong.hop.le")));
												sizeFails = lstFails.size() - 1;
												lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
											}else{
												//TH1: neu co loi trong moi dong muc doanh so thi thong bao loi tung dong 
												msg = this.validateRowInSalePlan(row, sp);
												if (!StringUtil.isNullOrEmpty(msg)) { /*** Neu cac dong muc doanh so loi*/
													lstFails.add(StringUtil.addFailBean(row, msg));
													sizeFails = lstFails.size() - 1;
													lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
													flagCheckRowSPError = true;
												} else { 
													/*** Dem xem co bao nhieu dong muc doanh so hop le */
													countRowSPNotFails++;
													lstFails.add(StringUtil.addFailBean(row, ""));
													sizeFails = lstFails.size() - 1;
													lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
												}
											}
										}
									}
									
									if (countRowSPNotFails == sizeSP) {/**Neu nhom thiet bi co nhieu muc doanh so hop le */
										int sizeRowsFails = lstFails.size();
										/** --> removed nhung thang muc doanh so hop le add tam vao truoc do trong listFails */
										for (int k = sizeRowsFails - 1; k >= sizeRowsFails - sizeSP; k--) {
											lstFails.remove(k);
										}
										/** Neu nhom thiet bi khong co dong muc doanh so nao bi loi validate  */
										if(!flagCheckRowSPError){
											row = vo.getRow();
											if (row != null){
												/**validate rang buoc cac dong muc doanh so trong nhom thiet bi */
												message = this.validateGroupSalePlan(row, vo);
												if (StringUtil.isNullOrEmpty(message)) {
													/** Tien hanh set du lieu va insert xuong DB du lieu ko co muc doanh so*/
													vo.setCreateDate(now);
													vo.setCreateUser(username);
													vo.setFlagCheckExistSP(true);
													equipmentManagerMgr.createEquipGroupByExcel(vo);
												} else {
													lstFails.add(StringUtil.addFailBean(row, message));
													sizeFails = lstFails.size() - 1;
													lstFails.get(sizeFails).setContent33(vo.getIndexRow().toString());
													int nTmp = vo.getLstEquipSalePlanVO().size();
													if (nTmp > 1) {
														List<String> rowTmp = null;
														EquipSalePlanVO spTmp = null;
														for (int j = 0; j < nTmp; j++) {
//													totalItem++;
															spTmp = vo.getLstEquipSalePlanVO().get(j);
															if (j > 0 && spTmp!=null) { 
																rowTmp = spTmp.getRow();
																if (spTmp.getIsRowEmpty()) { /*** Neu co dong rong thi khong thuc hien nhung dong ben duoi va thong bao loi */ 
																	lstFails.add(StringUtil.addFailBean(rowTmp, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.khong.thuc.hien.cac.dong.ben.duoi")));
																	sizeFails = lstFails.size() - 1;
																	lstFails.get(sizeFails).setContent33(spTmp.getIndexRow().toString());
																} else {/***De null cac dong thong bao neu dong nhom loi */
																	lstFails.add(StringUtil.addFailBean(rowTmp, ""));
																	sizeFails = lstFails.size() - 1;
																	lstFails.get(sizeFails).setContent33(spTmp.getIndexRow().toString());
																}
															}
														}
														
													}
												}
											}
										}
									}
									/** Neu nhom thiet bi co muc doanh so hop le (hoac khong co muc doanh so) va dong tiep theo cua no la dong rong  */
									if (countRowSPNotFails == (sizeSP - 1) && flagCheckRowExistEmpty) {
										int sizeSPTmp = vo.getLstEquipSalePlanVO().size() - 1;
										vo.getLstEquipSalePlanVO().remove(sizeSPTmp);
										/** Neu so dong cua countRowSPNotFails ton tai bao nhieu thi remove dung voi so luong phan tu dau tien trong lstFail  */
										for (int k = 0; k < countRowSPNotFails; k++) {
											lstFails.remove(0);
											totalItem--;
										}
										row = vo.getRow();
										if (row != null){
											if (!this.isRowKeySalePlan(row) && vo.getLstEquipSalePlanVO().size() == 1) { /*** Khong co muc doanh so*/
												totalItem++;
												/** Tien hanh set du lieu va insert xuong DB du lieu ko co muc doanh so */
												vo.setCreateDate(now);
												vo.setCreateUser(username);
												vo.setFlagCheckExistSP(false);
												equipmentManagerMgr.createEquipGroupByExcel(vo);
											} else { /*** Neu nhom co muc doanh so hop le */
												if (vo.getLstEquipSalePlanVO().size() > 1 ) { 
													if(!this.isRowKeySalePlan(vo.getRow())){  /*** Neu co dong muc doanh so nao rong */
														if(vo.getLstEquipSalePlanVO().get(0)!= null){
															lstFails.add(StringUtil.addFailBean(vo.getLstEquipSalePlanVO().get(0).getRow(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.customer.type.cung.loai")));
															sizeFails = lstFails.size() - 1;
															lstFails.get(sizeFails).setContent33(vo.getLstEquipSalePlanVO().get(0).getIndexRow().toString());
														}
														for (int k = 0, sizeTmp = vo.getLstEquipSalePlanVO().size(); k < sizeTmp; k++) {
															totalItem++;
															if (k > 0 && vo.getLstEquipSalePlanVO().get(k)!= null) {
																lstFails.add(StringUtil.addFailBean(vo.getLstEquipSalePlanVO().get(k).getRow(), ""));
																sizeFails = lstFails.size() - 1;
																lstFails.get(sizeFails).setContent33(vo.getLstEquipSalePlanVO().get(k).getIndexRow().toString());
															}
														}
													}else { /*** Neu khong co dong muc doanh so nao rong */
														message = this.validateGroupSalePlan(row, vo);
														if (StringUtil.isNullOrEmpty(message)) {/*** Muc doanh so khong hop le*/
															int sizeTmp = vo.getLstEquipSalePlanVO().size();
															if (sizeTmp > 1) {
																for (int k = 0; k < sizeTmp; k++) {
																	totalItem++;
																}
															} else {
																totalItem++;
															}
															/**Tien hanh set du lieu va insert xuong DB du lieu ko co muc doanh so*/
															vo.setCreateDate(now);
															vo.setCreateUser(username);
															vo.setFlagCheckExistSP(true);
															equipmentManagerMgr.createEquipGroupByExcel(vo);
														} else {/*** Neu rang buoc giua cac muc doanh so khong hop le */
															if (countRowSPNotFails > 1) {
																lstFails.add(StringUtil.addFailBean(row, message));
																sizeFails = lstFails.size() - 1;
																lstFails.get(sizeFails).setContent33(vo.getIndexRow().toString());
																int nTmp = vo.getLstEquipSalePlanVO().size();
																if (nTmp > 1) {
																	List<String> rowTmp = null;
																	EquipSalePlanVO spTmp = null;
																	for (int j = 0; j < nTmp; j++) {
																		totalItem++;
																		spTmp = vo.getLstEquipSalePlanVO().get(j);
																		if (j > 0 && spTmp!= null) { 
																			rowTmp = spTmp.getRow();
																			if (spTmp.getIsRowEmpty()) { /*** Neu co dong rong thi khong thuc hien nhung dong ben duoi va thong bao loi */ 
																				lstFails.add(StringUtil.addFailBean(rowTmp, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.khong.thuc.hien.cac.dong.ben.duoi")));
																				sizeFails = lstFails.size() - 1;
																				lstFails.get(sizeFails).setContent33(spTmp.getIndexRow().toString());
																			} else {/***De null cac dong thong bao neu dong nhom loi */
																				lstFails.add(StringUtil.addFailBean(rowTmp, ""));
																				sizeFails = lstFails.size() - 1;
																				lstFails.get(sizeFails).setContent33(spTmp.getIndexRow().toString());
																			}
																		}
																	}
																	
																}
															} else {
																totalItem++;
																lstFails.add(StringUtil.addFailBean(row, message));
																sizeFails = lstFails.size() - 1;
																lstFails.get(sizeFails).setContent33(vo.getIndexRow().toString());
															}
															
														}
													}
												} else { /**Neu khong co muc doanh so hoac nhom thiet bi co 1 muc doanh so */
													sp = vo.getLstEquipSalePlanVO().get(0);
													if (sp != null) {
														row = sp.getRow();
														totalItem++;
														if (!this.isRowKeySalePlan(row)) { /**Neu dong khong co muc doanh so */
															/** Tien hanh set du lieu va insert xuong DB du lieu ko co muc doanh so */
															vo.setCreateDate(now);
															vo.setCreateUser(username);
															vo.setFlagCheckExistSP(false);
															equipmentManagerMgr.createEquipGroupByExcel(vo);
														} else {/**dong muc doanh so */
															/** check validate dong muc doanh so */
															msg = this.validateRowInSalePlan(row, sp);
															if (!StringUtil.isNullOrEmpty(msg)) {
																lstFails.add(StringUtil.addFailBean(row, msg));
																sizeFails = lstFails.size() - 1;
																lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
															} else {/** Tung dong muc doanh so khong loi */ 
																/** check validate rang buoc cac dong muc doanh so */
																msg = this.validateGroupSalePlan(row, vo);
																if (!StringUtil.isNullOrEmpty(msg)) { 
																	lstFails.add(StringUtil.addFailBean(row, msg));
																	sizeFails = lstFails.size() - 1;
																	lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
																} else {
																	/** Tien hanh set du lieu va insert xuong DB du lieu ko co muc doanh so */
																	vo.setCreateDate(now);
																	vo.setCreateUser(username);
																	vo.setFlagCheckExistSP(true);
																	equipmentManagerMgr.createEquipGroupByExcel(vo);
																}
															}
														}
													}
												}
											}
										}
									}
								} else { /**Neu khong co muc doanh so hoac nhom thiet bi co 1 muc doanh so */
									sp = vo.getLstEquipSalePlanVO().get(0);
									if (sp != null) {
										row = sp.getRow();
										totalItem++;
										if (!this.isRowKeySalePlan(row)) { /**Neu dong khong co muc doanh so */
											/** Tien hanh set du lieu va insert xuong DB du lieu ko co muc doanh so */
											vo.setCreateDate(now);
											vo.setCreateUser(username);
											vo.setFlagCheckExistSP(false);
											equipmentManagerMgr.createEquipGroupByExcel(vo);
										} else {/**dong muc doanh so */
											/** check validate dong muc doanh so */
											msg = this.validateRowInSalePlan(row, sp);
											if (!StringUtil.isNullOrEmpty(msg)) {
												lstFails.add(StringUtil.addFailBean(row, msg));
												sizeFails = lstFails.size() - 1;
												lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
											} else {/** Tung dong muc doanh so khong loi */ 
												/** check validate rang buoc cac dong muc doanh so */
												msg = validateGroupSalePlan(row, vo);
												if (!StringUtil.isNullOrEmpty(msg)) { 
													lstFails.add(StringUtil.addFailBean(row, msg));
													sizeFails = lstFails.size() - 1;
													lstFails.get(sizeFails).setContent33(sp.getIndexRow().toString());
												} else {
													/** Tien hanh set du lieu va insert xuong DB du lieu ko co muc doanh so */
													vo.setCreateDate(now);
													vo.setCreateUser(username);
													vo.setFlagCheckExistSP(true);
													equipmentManagerMgr.createEquipGroupByExcel(vo);
												}
											}
										}
									}
								}
							}
						}
					}
					/** sap xep lai thu tu lstFail theo index row */
					if(lstFails!= null && lstFails.size() > 0){
						int sizeFailsTmp = lstFails.size();
						if (sizeFailsTmp > 0) {
							for (int i = 0; i < sizeFailsTmp - 1; i++) {
								for (int j = i + 1; j < sizeFailsTmp; j++) {
									if(lstFails.get(i)!= null && lstFails.get(j)!= null){
										if(lstFails.get(i).getContent33()!= null && lstFails.get(j).getContent33()!= null){
											int content1 = Integer.valueOf(lstFails.get(i).getContent33());
											int content2 = Integer.valueOf(lstFails.get(j).getContent33());
											if (content1 > content2) {
												CellBean tmp = lstFails.get(i);
												lstFails.set(i, lstFails.get(j));
												lstFails.set(j, tmp);
											}
										}
									}
								}
							}
						}
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_EQUIP_GROUP_EQUIPMENT_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * validate row in group
	 * 
	 * @author liemtpt
	 * @throws Exception 
	 * @since 04/04/2015
	 * @discription validate du lieu tung dong cua nhom thiet bi
	 */
	private String validateRowInGroup(EquipmentGroupVO equipVO) throws Exception {
		/** Ma nhom **/
		String msg = "";
		String message = "";
		String STATUS = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.status.type");
		String[] LIST_STATUS = STATUS.split(";");
		EquipGroup equipGroup = null;
		if (StringUtil.isNullOrEmpty(message)) {
			String value = equipVO.getCode();
			msg = ValidateUtil.validateField(value, "equipment.manager.group.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
			if (!StringUtil.isNullOrEmpty(msg)) {
				message += msg;
				message += BREAK_LINE;
			} else {
				/** gio neu ma nhom da ton tai thi cap nhat thong tin va insert dong moi du lieu doanh so */
				//vuongmq; validate ma nhom Thiet bi
				String codeGroup = equipVO.getTypeGroup() + equipVO.getCode();
				equipGroup = equipmentManagerMgr.getEquipGroupByCode(codeGroup);
				if (equipGroup != null && !ActiveType.RUNNING.equals(equipGroup.getStatus())) {
					message += R.getResource("common.catalog.status.in.active", R.getResource("equipment.group.product.equipment.group.code") + " " + equipGroup.getCode());
					message += BREAK_LINE;
				}
				/*value = value.trim().toUpperCase();
				EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
				if (equipVO.getTypeGroup() != null) {// neu loai khac null
					filter.setCode(equipVO.getTypeGroup() + value);
					ObjectVO<EquipmentVO> listEquipGroup = null;
					try {
						// kiem tra ma nhom co ton tai duy nhat trong he thong
						// khong
						listEquipGroup = equipmentManagerMgr.getlistEquipGroup(filter);
						if (listEquipGroup != null && listEquipGroup.getLstObject() != null && !listEquipGroup.getLstObject().isEmpty()) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.code.is.exist.db");
							message += "\n";
						}
					} catch (BusinessException e) {

					}
				}*/
			}

		}
		/** Ten nhom **/
		msg = "";
		String tennhom = equipVO.getName();
		msg = ValidateUtil.validateField(tennhom, "equipment.manager.group.name", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
		if (!StringUtil.isNullOrEmpty(msg)) {
			message += msg;
			message += BREAK_LINE;
		}
		/** Loai **/
		msg = "";
		String loai = equipVO.getTypeGroup();
		msg = ValidateUtil.validateField(loai, "equipment.manager.group.type", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
		if (!StringUtil.isNullOrEmpty(msg)) {
			message += msg;
			message += BREAK_LINE;
		} else {
			if (!StringUtil.isNullOrEmpty(loai)) {
				/*EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
				filter.setEquipCategoryCode(loai);
				listEquipCategory = equipmentManagerMgr.getListEquipmentCategory(filter);
				if(listEquipCategory == null || listEquipCategory.size() <= 0){
					message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.type.not.exist");;
					message += "\n";
				}*/
				EquipCategory equipCategory = equipmentManagerMgr.getEquipCategoryByCode(loai, null);
				if (equipCategory != null) {
					if (!ActiveType.RUNNING.equals(equipCategory.getStatus())) {
						message += R.getResource("common.catalog.status.in.active", R.getResource("equipment.manager.group.type"));
						message += BREAK_LINE;
					}
				} else {
					message += R.getResource("equipment.manager.group.type.not.exist");
					message += BREAK_LINE;
				}
			}
		}
		/** Hieu **/
		msg = "";
		String hieu = equipVO.getBrandName();
		msg = ValidateUtil.validateField(hieu, "equipment.manager.group.brand", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
		if (!StringUtil.isNullOrEmpty(msg)) {
			message += msg;
			message += BREAK_LINE;
		}

		/** Dung tich tu kieu so <= 10 ký tự **/
		msg = "";
		String dungtichtu = equipVO.getCapacityFrom();
		if (!StringUtil.isNullOrEmpty(dungtichtu)) {
			msg = ValidateUtil.validateField(dungtichtu, "equipment.manager.group.capacity.from", 10, ConstantManager.ERR_MAX_LENGTH);
			if (!StringUtil.isNullOrEmpty(msg)) {
				message += msg;
				message += BREAK_LINE;
			} else {
				dungtichtu = dungtichtu.trim();
				try {
					BigDecimal capacity = BigDecimal.valueOf(Double.valueOf(dungtichtu));
					if (BigDecimal.ZERO.compareTo(capacity)>=0) {
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.capacity.from.is.kieu.so.lon.hon.zero");
						message += BREAK_LINE;
					} else {
						equipVO.setCapacityFrom(dungtichtu);
					}
				} catch (Exception e) {
					// khong hop le
					message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.capacity.from.is.kieu.so");
					message += BREAK_LINE;
				}
			}
		}
		/**
		 * Dung tích đến: kiểu số, <=10 ký tự, >= đến. Dung tích từ và đến phải
		 * tồn tại ít nhất 1 trong 2.
		 **/
		msg = "";
		String dungtichden = equipVO.getCapacityTo();
		if (!StringUtil.isNullOrEmpty(dungtichden)) {
			msg = ValidateUtil.validateField(dungtichden, "equipment.manager.group.capacity.to", 10, ConstantManager.ERR_MAX_LENGTH);
			if (!StringUtil.isNullOrEmpty(msg)) {
				message += msg;
				message += BREAK_LINE;
			} else {
				dungtichden = dungtichden.trim();
				try {
					BigDecimal capacity = BigDecimal.valueOf(Double.valueOf(dungtichden));
					if (BigDecimal.ZERO.compareTo(capacity)>=0) {
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.capacity.to.is.kieu.so.lon.hon.zero");
						message += BREAK_LINE;
					}
				} catch (Exception e) {
					// khong hop le
					message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.capacity.to.is.kieu.so");
					message += BREAK_LINE;
				}
			}
		}

		/**
		 * Dung tích từ và đến phải tồn tại ít nhất 1 trong 2
		 * 
		 **/
		msg = "";
		//if (StringUtil.isNullOrEmpty(message)) {
			if (StringUtil.isNullOrEmpty(equipVO.getCapacityFrom()) && StringUtil.isNullOrEmpty(equipVO.getCapacityTo())) {
				message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.capacity.is.exists.than.one");
				message += BREAK_LINE;
			}
		//}

		/**
		 * Dung tích từ <= dung tich den
		 * 
		 **/
		msg = "";
		//if (StringUtil.isNullOrEmpty(message)) {
			if (!StringUtil.isNullOrEmpty(equipVO.getCapacityFrom()) && !StringUtil.isNullOrEmpty(equipVO.getCapacityTo()) 
				&& ValidateUtil.validateNumber(equipVO.getCapacityFrom()) && ValidateUtil.validateNumber(equipVO.getCapacityTo())) {
				BigDecimal capFrom = new BigDecimal(equipVO.getCapacityFrom());
				BigDecimal capTo = new BigDecimal(equipVO.getCapacityTo());
				if(capFrom.compareTo(capTo)> 0){
					message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.capacity.from.smaller.than.capacity.to");
					message += BREAK_LINE;
				}
			}
		//}
		
		/** Trang thai 1: Hoạt động,0 :Tạm ngưng **/
		//if (StringUtil.isNullOrEmpty(message)) {
			String value = equipVO.getStatusStr();
			msg = ValidateUtil.validateField(value, "equipment.manager.group.status", 10, ConstantManager.ERR_REQUIRE);
			if (!StringUtil.isNullOrEmpty(msg)) {
				message += msg;
			} else {
				value = value.trim();
				if (!StringUtil.isNullOrEmpty(value)) {
					try {
						if (LIST_STATUS[0].toUpperCase().trim().equalsIgnoreCase(value.toUpperCase().trim())) {
							equipVO.setStatus(ActiveType.RUNNING.getValue());
						} else if (LIST_STATUS[1].toUpperCase().trim().equalsIgnoreCase(value.toUpperCase().trim())) {
							equipVO.setStatus(ActiveType.STOPPED.getValue());
							// validate nhom thiet bi khi co trang thai import: inactive
							if (equipGroup != null) {
								EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
								filter.setId(equipGroup.getId());
								ObjectVO<EquipmentVO> listEquipGroupProduct = equipmentManagerMgr.getListCheckEquipGroupProductByIdEquipGroup(filter);
								if (listEquipGroupProduct != null && listEquipGroupProduct.getLstObject() != null && listEquipGroupProduct.getLstObject().size() > 0) {
									message += R.getResource("equipment.manager.group.nhom.co.tb.dang.hoat.dong");
									message += BREAK_LINE;
								}
							}
						} else {
							// khong hop le
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.status.is.not.exist");
							message += BREAK_LINE;
						}
					} catch (Exception e) {
						// khong hop le
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.status.is.not.exist");
					}
				}
			}
		//}
		return message;
	}

	/**
	 * validate row in sale plan
	 * 
	 * @author liemtpt
	 * @since 04/04/2015
	 * @discription validate du lieu tung dong cua dinh muc doanh so
	 */
	private String validateRowInSalePlan(List<String> row, EquipSalePlanVO sp) {
		String CUSTYPE = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.cus.type.type");
		String[] LIST_CUS_TYPE = CUSTYPE.split(";");
		String message = "";
		// Tu Thang
		String msg = "";
		// Co muc doanh so moi validate tung dong nguoc lai ko validate tung
		// dong cho muc doanh so
		if (this.isRowKeySalePlan(row)) { // Co muc doanh so
			if (StringUtil.isNullOrEmpty(message)) {
				String value = sp.getFromMonth();
				// if( !StringUtil.isNullOrEmpty(value)){
				msg = ValidateUtil.validateField(value, "equipment.manager.group.from.month", 3, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
				if (!StringUtil.isNullOrEmpty(msg)) {
					message += msg;
					message += BREAK_LINE;
				} else {
					value = value.trim();
					try {
						int fromMonth = Integer.valueOf(value);
						if (fromMonth <= 0) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.from.month.is.Int");
							message += BREAK_LINE;
						}
					} catch (Exception e) {
						// khong hop le
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.from.month.is.Int");
						message += BREAK_LINE;
					}
				}
				// }
			}
			/**
			 * Den thang kieu so nguyen duong va >= tu thang
			 **/
			msg = "";
			if (StringUtil.isNullOrEmpty(message)) {
				String value = sp.getToMonth();
				value = value.trim();
				if (!StringUtil.isNullOrEmpty(sp.getFromMonth())) {
					if (!StringUtil.isNullOrEmpty(value)) {
						msg = ValidateUtil.validateField(value, "equipment.manager.group.to.month", 3, ConstantManager.ERR_MAX_LENGTH);
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg;
							message += BREAK_LINE;
						} else {
							try {
								int fromMonth = Integer.valueOf(sp.getFromMonth());
								int toMonth = Integer.valueOf(value);
								if (toMonth <= 0) {
//									msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.to.month.is.Int");
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.to.month.is.Int");
									message += BREAK_LINE;
								} else {
									// kiem tra den thang >= tu thang
									if (fromMonth > toMonth) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.toMonth.greater.than.fromMonth");
										message += BREAK_LINE;
									}
								}
							} catch (Exception e) {
								// khong hop le
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.to.month.is.Int");
								message += BREAK_LINE;
							}
						}
					}
				}
				/*
				 * else{ message += ValidateUtil.validateField(value,
				 * "equipment.manager.group.from.month",
				 * 3,ConstantManager.ERR_REQUIRE
				 * ,ConstantManager.ERR_MAX_LENGTH); message +="\n"; }
				 */
			}

			/** Loai khach hang NULL,1 Thành thị,2 Nông thôn **/
			msg = "";
			if (StringUtil.isNullOrEmpty(message)) {
				String value = sp.getCustomerTypeStr();
				if (!StringUtil.isNullOrEmpty(value)) {
					value = value.trim();
					try {
						Boolean isTrueCustomerType = false;
						if (lstCustomerType == null || !lstCustomerType.isEmpty()) {
							for (ChannelType customerType : lstCustomerType) {
								if (customerType.getChannelTypeCode().equals(value.toUpperCase())) {
									isTrueCustomerType = true;
									break;
								}
							}
							if (!isTrueCustomerType) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.customer.type.is.not.exits");
								message += BREAK_LINE;
							}
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.customer.type.is.not.exits");
							message += BREAK_LINE;
						}
						
						
//						if (LIST_CUS_TYPE[0].trim().toUpperCase().equals(value.toUpperCase())) {
//							sp.setCustomerType(EquipSalePlanCustomerType.CITY.getValue());
//						} else if (LIST_CUS_TYPE[1].trim().toUpperCase().equals(value.toUpperCase())) {
//							sp.setCustomerType(EquipSalePlanCustomerType.GARDEN_CITY.getValue());
//						} else {
//							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.customer.type.is.not.exits");
//							message += "\n";
//						}
					} catch (Exception e) {
						// khong hop le
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.customer.type.is.not.exits");
						message += BREAK_LINE;
					}
				} else {
					sp.setCustomerTypeId(null);
				}
			}

			/** Doanh so **/
			// kiem tra them muc doanh so
			msg = "";
			if (StringUtil.isNullOrEmpty(message)) {
				String value = sp.getAmount();
				msg = ValidateUtil.validateField(value, "equipment.manager.group.amount", 17, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
				if (!StringUtil.isNullOrEmpty(msg)) {
					message += msg;
					message += BREAK_LINE;
				} else {
					if (!StringUtil.isNullOrEmpty(value) && !StringUtil.isNullOrEmpty(row.get(6))) {
						value = value.trim();
						try {
							BigDecimal amount = BigDecimal.valueOf(Double.valueOf(value));
							if (BigDecimal.ZERO.compareTo(amount)>=0) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.amount.is.Int");
								message += BREAK_LINE;
							}
						} catch (Exception e) {
							// khong hop le
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.amount.is.Int");
							message += BREAK_LINE;
						}
					}
				}
			}
		}
		return message;
	}

	/**
	 * validate group sale plan
	 * 
	 * @author liemtpt
	 * @since 04/04/2015
	 * @discription validate du lieu nhom cua dinh muc doanh so
	 */
	private String validateGroupSalePlan(List<String> row, EquipmentGroupVO equipVO) {
		String message = "";
		/** Success ==> Validate quan he giua cac dong deail voi nhau */
		Integer DEFAULT_MONTH = 1;
		// check co ton tai tu thang = 1 khong
		Integer flagTonTaiThangMot = 0;
		Integer flagTrungChom = 0;
		// dem check gia tri null
		int COUNT_EMPTY = 0;
		int NULL_LIMIT = 2;
		// dem check lien ke
		int count = 0;
		Integer minFrom = null;
		Integer maxTo = null;
		Boolean flagFirtToMonthIsEmpty = false;
		Integer indexFirst = -1;
		if (equipVO != null) {
			// Integer cusType = lstEquipSalePlans.get(0).getCustomerType();
			List<EquipSalePlanVO> lstEquipSalePlans = equipVO.getLstEquipSalePlanVO();
			if (lstEquipSalePlans != null && lstEquipSalePlans.size() > 0) {
				EquipSalePlanVO salePlan = new EquipSalePlanVO();
				Map<Long, String> mapTypeCustomer = new HashMap<Long, String>();
				int sizeTmp = lstEquipSalePlans.size();
				String cusType = null;
				if (lstEquipSalePlans.get(0) != null) {
					/** Loai khach hang cua row dau tien */
					cusType = lstEquipSalePlans.get(0).getCustomerTypeStr();
				}
				for (int i = 0; i < sizeTmp; i++) {
					salePlan = lstEquipSalePlans.get(i);
					/**Loai khach hang khong cung luc la null hoac khong cung luc khac null*/
					if (StringUtil.isNullOrEmpty(cusType)) {
						if (!StringUtil.isNullOrEmpty(salePlan.getCustomerTypeStr())) {
							/** equipment.manager.group.customer.type.cung.loai = loai khach hang la thanh thi hoac nong thon thi khong chung nhom voi loai khach hang la rong va nguoc lai */
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.customer.type.cung.loai");
							message += BREAK_LINE;
							return message;
						}
					} else {
						if (StringUtil.isNullOrEmpty(salePlan.getCustomerTypeStr())) {
							/** equipment.manager.group.customer.type.cung.loai = loai khach hang la thanh thi hoac nong thon thi khong chung nhom voi loai khach hang la rong va nguoc lai */
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.customer.type.cung.loai");
							message += BREAK_LINE;
							return message;
						}
					}
					
					for (ChannelType type : lstCustomerType) {
						if (type.getChannelTypeCode().equals(salePlan.getCustomerTypeStr())) {
							salePlan.setCustomerTypeId(type.getId());
							lstEquipSalePlans.get(i).setCustomerTypeId(type.getId());
							break;
						}
					}
					
					/** cho tat ca các loai type Customer vao map type **/
					if (!mapTypeCustomer.containsKey(salePlan.getCustomerTypeStr())) {
						if (salePlan.getCustomerTypeId() == null) {
							salePlan.setCustomerTypeId(0L);
						}
						mapTypeCustomer.put(salePlan.getCustomerTypeId(), salePlan.getCustomerTypeStr());
					}
				}
				/** kiem tra tung loai typeCustomer */
				for (Long key : mapTypeCustomer.keySet()) {
					/** moi typeCustomer gan gia tri mac dinh ban dau*/
					DEFAULT_MONTH = 1;
					// check co ton tai tu thang = 1 khong
					flagTonTaiThangMot = 0;
					flagTrungChom = 0;
					// dem check gia tri null
					COUNT_EMPTY = 0;
					NULL_LIMIT = 2;
					// dem check lien ke
					count = 0;
					minFrom = null;
					maxTo = null;
					flagFirtToMonthIsEmpty = false;
					indexFirst = -1;
					/** gan tmp gia tri dau tien cho tung loai customerType */
					for (int k = 0; k < sizeTmp; k++) {
						if (lstEquipSalePlans.get(k) != null && lstEquipSalePlans.get(k).getCustomerTypeId().equals(key)) {
							if (!StringUtil.isNullOrEmpty(lstEquipSalePlans.get(k).getFromMonth())) {
								minFrom = Integer.valueOf(lstEquipSalePlans.get(k).getFromMonth());
								if (!StringUtil.isNullOrEmpty(lstEquipSalePlans.get(k).getToMonth())) {
									maxTo = Integer.valueOf(lstEquipSalePlans.get(k).getToMonth());
								} else {
									flagFirtToMonthIsEmpty = true;
									maxTo = Integer.valueOf(lstEquipSalePlans.get(k).getFromMonth()) + 1;
								}
							}
							/**
							 * dem doan thoi gian lien ke tu thang, den thang cua row
							 * dau tien
							 */
							if (minFrom != null && maxTo != null && minFrom <= maxTo) {
								count += (maxTo - minFrom) + 1;
							}
							indexFirst = k;
							break; // gan dau tien: ra khoi dong for k: for (int k = 0; k < sizeTmp; k++)
						}
					} // end for (int k = 0; k < sizeTmp; k++)
					
					for (int i = 0; i < sizeTmp; i++) {
						salePlan = lstEquipSalePlans.get(i);
						if (salePlan.getCustomerTypeId().equals(key)) {
							/** neu tu thang co ton tai thang = 1 thi bat flag = 1 */
							if (DEFAULT_MONTH.equals(Integer.valueOf(salePlan.getFromMonth()))) {
								flagTonTaiThangMot = 1;
							}
							/** neu den thang la null thi tang dem */
							if (StringUtil.isNullOrEmpty(salePlan.getToMonth())) {
								COUNT_EMPTY++; //
							}
							/** kiem tra doan lien ke hop le */
							int fMonth = 0;
							int tMonth = 0;
							if (i > indexFirst) {
								if (flagFirtToMonthIsEmpty) { //Neu dong den thang dau tien null
									if (!StringUtil.isNullOrEmpty(salePlan.getToMonth())) {
										// Thong bao loi tu thang co lon hon 2 dong null
										fMonth = Integer.valueOf(salePlan.getFromMonth());
										tMonth = Integer.valueOf(salePlan.getToMonth());
										if (fMonth <= tMonth) {
											count += (tMonth - fMonth) + 1;
										}
										if (fMonth < minFrom) {
											minFrom = fMonth;
										}
										if (tMonth > maxTo) {
											maxTo = fMonth;
										}
									}
								} else { // nguoc lai neu dong den thang dau tien khac null
									if (!StringUtil.isNullOrEmpty(salePlan.getFromMonth())) {
										fMonth = Integer.valueOf(salePlan.getFromMonth());
										if (!StringUtil.isNullOrEmpty(salePlan.getToMonth())) {
											tMonth = Integer.valueOf(salePlan.getToMonth());
										} else {
											tMonth = Integer.valueOf(salePlan.getFromMonth()) + 1;
										}
										if (fMonth <= tMonth) {
											count += (tMonth - fMonth) + 1;
										}
										if (fMonth < minFrom) {
											minFrom = fMonth;
										}
										if (tMonth > maxTo) {
											maxTo = tMonth;
										}
									}
								}
								/** HUI TRUOC kt CAP GIA TRI LOAI kh*/
							}
							for (int j = i + 1; j < sizeTmp; j++) {
								EquipSalePlanVO spVO = lstEquipSalePlans.get(j);
								if (spVO.getCustomerTypeId().equals(key)) {
									int spfMonth1 = 0;
									int sptMonth1 = 0;
									int spfMonth2 = 0;
									int sptMonth2 = 0;
									if (!StringUtil.isNullOrEmpty(salePlan.getFromMonth())) {
										spfMonth1 = Integer.valueOf(salePlan.getFromMonth());
										if (!StringUtil.isNullOrEmpty(salePlan.getToMonth())) {
											sptMonth1 = Integer.valueOf(salePlan.getToMonth());
										} else {
											sptMonth1 = Integer.valueOf(salePlan.getFromMonth()) + 1;
										}
									}
									if (!StringUtil.isNullOrEmpty(spVO.getFromMonth())) {
										spfMonth2 = Integer.valueOf(spVO.getFromMonth());
										if (!StringUtil.isNullOrEmpty(spVO.getToMonth())) {
											sptMonth2 = Integer.valueOf(spVO.getToMonth());
										} else {
											sptMonth2 = Integer.valueOf(spVO.getFromMonth()) + 1;
										}
									}
									if (spfMonth2 <= sptMonth1 && sptMonth2 >= spfMonth1) {
										// trung chom
										flagTrungChom = 1;
			
									}
								} // end if (spVO.getCustomerType().equals(key))
							} // end for (int j = i + 1; j < sizeTmp; j++) 
						} // end if (salePlan.getCustomerType().equals(key))
					} // for (int i = 0; i < sizeTmp; i++)
					/**
					 * dem doan thoi gian lien ke tu thang, den thang cua tat ca cac
					 * dong detail Neu doan thoi gian thuc te = voi doan thoi gian
					 * check thi no lien ke nguoc lai khong lien ke
					 */
					if ((maxTo - minFrom + 1) != count) {
						//equipment.manager.group.tomonth.frommonth.lien.ke = khoang tu thang, den thang co cac giai doan phai lien ke nhau 
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.toMonth.fromMonth.lien.ke");
						message += " " + R.getResource("equipment.manager.group.voi.loai.kh") + " " + mapTypeCustomer.get(key);
						message += BREAK_LINE;
					}
					/** flagTrungChom = 1: Trung chom 0: khong trung chom */
					if (flagTrungChom == 1) {
						//equipment.manager.group.tomonth.frommonth.trung.chom = khoang tu thang, den thang khong the trung chom
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.toMonth.fromMonth.trung.chom");
						message += " " + R.getResource("equipment.manager.group.voi.loai.kh") + " " + mapTypeCustomer.get(key);
						message += BREAK_LINE;
					}
					/**
					 * flagTonTaiThangMot = 1: co ton tai nguoc lai 0: khong co ton
					 * tai tu thang = 1
					 */
					if (flagTonTaiThangMot != null && flagTonTaiThangMot == 0) {
						//equipment.manager.group.frommonth.ton.tai.thang.mot = phai ton tai 1 dong co tu thang = 1 
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.fromMonth.ton.tai.thang.mot");
						message += " " + R.getResource("equipment.manager.group.voi.loai.kh") + " " + mapTypeCustomer.get(key);
						message += BREAK_LINE;
					}
					/**
					 * Neu demNull >= 2 thi ton tai 2 dong null nguoc lai thi khogn
					 * ton tai 2 dong null
					 */
					if (COUNT_EMPTY >= NULL_LIMIT) {
						//equipment.manager.group.tomonth.khong.ton.tai.hai.dong.null = khong ton tai 2 dong den thang la rong
						message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.toMonth.khong.ton.tai.hai.dong.null");
						message += " " + R.getResource("equipment.manager.group.voi.loai.kh") + " " + mapTypeCustomer.get(key);
						message += BREAK_LINE;
					}
				} // end for (Integer key : mapTypeCustomer.keySet())
			}
		}
		return message;
	}

	// public String importEquipGroup1() {
	// resetToken(result);
	// isError = true;
	// errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
	// totalItem = 0;
	// String message = "";
	// String msg = "";
	// EquipmentVO equipVO = new EquipmentVO();
	// EquipSalePlan sp = new EquipSalePlan();
	// // List<EquipmentVO> lstEquipVO = new ArrayList<EquipmentVO>();
	// // List<EquipSalePlan> lstEquipSalePlan = new ArrayList<EquipSalePlan>();
	// // Map<String, EquipmentVO> map = new HashMap<String, EquipmentVO>();
	// EquipmentFilter<EquipmentVO> filter;
	// List<CellBean> lstFails = new ArrayList<CellBean>();
	// // Map<Integer, CellBean> mapCellBeanFails = new HashMap<Integer,
	// CellBean>();
	// String STATUS =
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.status.type");
	// String[] LIST_STATUS = STATUS.split(";");
	// String username = currentUser.getStaffRoot().getStaffCode();
	// List<List<String>> lstData = getExcelDataEx(excelFile,
	// excelFileContentType, errMsg, 11);
	// int iFailDt = 0;
	// if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size()
	// > 0) {
	// try {
	// Date now = commonMgr.getSysDate();
	// Boolean flagCheckEmpty = false;
	// // do 2 dong dau la merge thanh dong menu nen se hieu dong thu 2 la dong
	// du lieu nen fix bang cach duyet tu dong dau t1
	// for (int i = 1; i < lstData.size(); i++) {
	// totalItem++;
	// List<String> row = lstData.get(i);
	// message = "";
	// if(flagCheckEmpty){
	// // --totalItem;
	// // --i;
	// break;
	// }
	// /** Khong co bat cu du lieu nao tren dong nay */
	// if (isEmtyRow(row)) {
	// lstFails.add(StringUtil.addFailBean(row,
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.invalid.khong.thuc.hien.cac.dong.ben.duoi")));
	// break;
	// }
	// if (lstData.get(i) != null && lstData.get(i).size() > 0) {
	// /** Neu la dong cha, thi tien hanh validate dong cha */
	// equipVO = new EquipmentVO();
	// equipVO.setIndexRow(i);
	//
	// /** Ma nhom **/
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 0) {
	// String value = row.get(0);
	// msg = ValidateUtil.validateField(value, "equipment.manager.group.code",
	// 50, ConstantManager.ERR_REQUIRE,
	// ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,
	// ConstantManager.ERR_MAX_LENGTH);
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// } else {
	// value = value.trim().toUpperCase();
	// filter = new EquipmentFilter<EquipmentVO>();
	// if (row.get(2) != null) {// neu loai khac null
	// filter.setCode(row.get(2) + value);
	// ObjectVO<EquipmentVO> listEquipGroup =
	// equipmentManagerMgr.getlistEquipGroup(filter);
	// // kiem tra ma nhom co ton tai duy nhat trong he thong khong
	// if (listEquipGroup != null && listEquipGroup.getLstObject() != null &&
	// !listEquipGroup.getLstObject().isEmpty()) {
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.code.is.exist.db");
	// } else {
	// // set code group
	// equipVO.setCode(row.get(2) + value);
	// }
	// }
	// }
	// }
	// }
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	//
	// /** Ten nhom **/
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 1) {
	// String value = row.get(1);
	// msg = ValidateUtil.validateField(value, "equipment.manager.group.name",
	// 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// } else {
	// equipVO.setName(value);
	// }
	// }
	// }
	// /** Loai **/
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 2) {
	// String value = row.get(2);
	// msg = ValidateUtil.validateField(value, "equipment.manager.group.type",
	// 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// } else {
	// value = value.trim();
	// equipVO.setTypeGroup(value);
	// }
	// }
	// }
	// /** Hieu **/
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 3) {
	// String value = row.get(3);
	// msg = ValidateUtil.validateField(value, "equipment.manager.group.brand",
	// 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// } else {
	// value = value.trim();
	// equipVO.setBrandName(value);
	// }
	// }
	// }
	//
	// /** Dung tich tu kieu so <= 10 ký tự **/
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 4) {
	// String value = row.get(4);
	// if (!StringUtil.isNullOrEmpty(value)) {
	// msg = ValidateUtil.validateField(value,
	// "equipment.manager.group.capacity.from", 10,
	// ConstantManager.ERR_MAX_LENGTH);
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// } else {
	// value = value.trim();
	// try{
	// Integer capacity = new Integer(value);
	// if (capacity.intValue() <= 0) {
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.capacity.from.is.Int");
	// } else {
	// equipVO.setCapacityFrom(value);
	// }
	// }catch(Exception e){
	// // khong hop le
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.capacity.from.is.Int");
	// }
	// }
	// }
	// }
	// }
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	//
	// /**
	// * Dung tích đến: kiểu số, <=10 ký tự, >= đến. Dung
	// * tích từ và đến phải tồn tại ít nhất 1 trong 2.
	// **/
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 5) {
	// String value = row.get(5);
	// if (!StringUtil.isNullOrEmpty(value)) {
	// msg = ValidateUtil.validateField(value,
	// "equipment.manager.group.capacity.to", 10,
	// ConstantManager.ERR_MAX_LENGTH);
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// } else {
	// value = value.trim();
	// try{
	// Integer capacity = new Integer(value);
	// if (capacity.intValue() <= 0) {
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.capacity.to.is.Int");
	// } else {
	// equipVO.setCapacityTo(value);
	// }
	// }catch(Exception e){
	// // khong hop le
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.capacity.to.is.Int");
	// }
	// }
	// }
	// }
	// }
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	//
	// /**
	// * Dung tích từ và đến phải tồn tại ít nhất 1 trong 2
	// *
	// **/
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (StringUtil.isNullOrEmpty(equipVO.getCapacityFrom()) &&
	// StringUtil.isNullOrEmpty(equipVO.getCapacityTo())) {
	// msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.capacity.is.exists.than.one");
	// }
	// }
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	// /** Trang thai 1: Hoạt động,0 :Tạm ngưng **/
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 10) {
	// String value = row.get(10);
	// msg = ValidateUtil.validateField(value, "equipment.manager.group.status",
	// 10, ConstantManager.ERR_REQUIRE);
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// } else {
	// value = value.trim();
	// if (!StringUtil.isNullOrEmpty(value)) {
	// try{
	// if (LIST_STATUS[0].toUpperCase().equalsIgnoreCase(value.toUpperCase())) {
	// equipVO.setStatus(ActiveType.RUNNING.getValue());
	// } else if
	// (LIST_STATUS[1].toUpperCase().equalsIgnoreCase(value.toUpperCase())) {
	// equipVO.setStatus(ActiveType.STOPPED.getValue());
	//
	// } else {
	// // khong hop le
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.status.is.not.exist");
	// }
	// }catch(Exception e){
	// // khong hop le
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.status.is.not.exist");
	// }
	// }
	// }
	// }
	// }
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	//
	// /** Neu dong cha bi loi,
	// * thi khong quan tam toi cac dong detail cua no
	// *
	// */
	// int indexRow = i;
	//
	// if(!StringUtil.isNullOrEmpty(message)){ /**Neu dong cha loi*/
	// lstFails.add(StringUtil.addFailBean(row, message)); /** Thong bao loi
	// dong cha */
	//
	// /** Duyet cac dong detail , bo vao list Fails */
	// for (i = i + 1; i < lstData.size(); i++) {
	// List<String> rowDetail = lstData.get(i);
	// if (isEmtyRow(rowDetail)) {
	// iFailDt = i;
	// flagCheckEmpty = true;
	// // --totalItem;
	// // --i;
	// // lstFails.add(StringUtil.addFailBean(rowDetail,
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.invalid")));
	// break;
	// }else{
	// /**Tang tong de cap nhat dung so dong du lieu thoi diem hien tai*/
	// ++totalItem;
	// if (!isRowKeyGroup(rowDetail)) {
	// lstFails.add(StringUtil.addFailBean(rowDetail, ""));
	// }else{
	// /**Nhay lai vi tri dong hien hanh. cap nhat la vi tri va so dong du lieu
	// o vi tri hien tai*/
	// --totalItem;
	// --i;
	// break;
	// }
	// }
	// }
	// continue; /** Tiep tuc voi cac dong tiep theo */
	//
	// }else{
	//
	// /** Xu ly cac dong detail */
	// List<EquipSalePlan> lstEquipSalePlans = new ArrayList<EquipSalePlan>();
	//
	// List<CellBean> lstDetails = new ArrayList<CellBean>();
	// Boolean flagValidate = false;
	// Boolean flagMucDoanhSo = false;
	// // Boolean flagSPFail = false;
	// int demSPEmpty = 0;
	// for (; i < lstData.size(); i++) {
	// List<String> rowDetail = lstData.get(i);
	// // /**Tang tong de cap nhat dung so dong du lieu thoi diem hien tai*/
	// // ++totalItem;
	// /** Khong co bat cu du lieu nao tren dong nay */
	// if (isEmtyRow(rowDetail)) {
	// iFailDt =i;
	// flagCheckEmpty = true;
	// // lstFails.add(StringUtil.addFailBean(rowDetail,
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.invalid")));
	// --totalItem;
	// --i;
	// break;
	// }else{
	// if (!isRowKeyGroup(rowDetail) || indexRow == i) {
	// // iFailDt =i;
	// sp = new EquipSalePlan();
	// /**Tang tong de cap nhat dung so dong du lieu thoi diem hien tai*/
	// ++totalItem;
	// String messageValidateDetailError = this.validateDetail(rowDetail,
	// equipVO, sp);
	//
	// lstDetails.add(StringUtil.addFailBean(rowDetail,
	// messageValidateDetailError)); /** Thong bao loi detail */
	// if(!isRowKeySalePlan(rowDetail)){
	// demSPEmpty++;
	// }
	// /** Detail thanh cong */
	// if(StringUtil.isNullOrEmpty(messageValidateDetailError)){
	// // Neu co muc doanh so moi them vao list
	// if(sp.getFromMonth()!= null && sp.getCustomerType()!=null &&
	// sp.getAmount()!=null){
	// lstEquipSalePlans.add(sp);
	// }else{
	// flagMucDoanhSo = true;
	// }
	// }else{
	// // dong chi tiet loi
	// // flagMucDoanhSo = true;
	// flagValidate = true;
	// }
	// }else{
	// /**Nhay lai vi tri dong hien hanh. cap nhat la vi tri va so dong du lieu
	// o vi tri hien tai*/
	// --totalItem;
	// --i;
	// break;
	// }
	// }
	//
	// }
	//
	// /** Cac dong detail khong co loi * Thoa man tat ca cac dieu kien */
	// if(lstEquipSalePlans.size() > 0 ){
	// if(lstDetails.size() == lstEquipSalePlans.size()){
	// String detailErrors = "";
	// /**Success ==> Validate quan he giua cac dong deail voi nhau */
	// Integer DEFAULT_MONTH = 1;
	// // check co ton tai tu thang = 1 khong
	// int flag = 0;
	// // dem check gia tri null
	// int demNULL = 0;
	// int NULL_LIMIT = 2;
	// // dem check lien ke
	// int count = 0;
	// Integer minFrom = null;
	// Integer maxTo = null;
	// Integer cusType = lstEquipSalePlans.get(0).getCustomerType();
	// int sizeTmp = lstEquipSalePlans.size();
	// EquipSalePlan salePlan = new EquipSalePlan();
	// for(int j =0 ; j < sizeTmp; j++){
	// salePlan = lstEquipSalePlans.get(j);
	// /** neu tu thang co ton tai thang = 1 thi bat flag = 1 */
	// if (salePlan.getFromMonth().equals(DEFAULT_MONTH)) {
	// flag = 1;
	// }
	// /** neu den thang la null thi tang dem*/
	// if (salePlan.getToMonth() == null) {
	// demNULL++; //
	// }
	// /** dem doan thoi gian lien ke tu thang, den thang cua tat ca cac dong
	// detail*/
	// if (salePlan.getToMonth() != null && salePlan.getFromMonth() != null &&
	// salePlan.getFromMonth() <= salePlan.getToMonth()) {
	// count += (salePlan.getToMonth() - salePlan.getFromMonth()) + 1;
	// }
	//
	// minFrom = lstEquipSalePlans.get(0).getFromMonth();
	// maxTo = lstEquipSalePlans.get(0).getToMonth();
	// if (salePlan.getFromMonth() != null && salePlan.getFromMonth() > 0) {
	// if (minFrom > salePlan.getFromMonth()) {
	// minFrom = salePlan.getFromMonth();
	// }
	// }
	// if (salePlan.getToMonth() != null && salePlan.getToMonth() > 0) {
	// if (maxTo < salePlan.getToMonth()) {
	// maxTo = salePlan.getToMonth();
	// }
	// }
	// if (j > 0) {
	// if (cusType.equals(EquipSalePlanCustomerType.UNDEFINED.getValue())) {
	// if
	// (!EquipSalePlanCustomerType.UNDEFINED.getValue().equals(lstEquipSalePlans.get(j).getCustomerType()))
	// {
	// // thong bao loi khong cung loai NULL
	// detailErrors +=
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.customer.type.cung.loai");
	// }
	// } else {
	// if
	// (EquipSalePlanCustomerType.UNDEFINED.getValue().equals(lstEquipSalePlans.get(j).getCustomerType()))
	// {
	// // thong bao loi
	// detailErrors +=
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.customer.type.cung.loai");
	// }
	// }
	// }
	//
	// for (int k = j + 1; k < sizeTmp; k++) {
	// EquipSalePlan salePlan2 = lstEquipSalePlans.get(k);
	// if ((salePlan2.getFromMonth() <= salePlan.getToMonth() &&
	// salePlan2.getToMonth() >= salePlan.getFromMonth())) {
	// // trung chom
	// detailErrors +=
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.toMonth.fromMonth.trung.chom");
	// }
	// }
	//
	// }
	// /** dem doan thoi gian lien ke tu thang, den thang cua tat ca cac dong
	// detail
	// * Neu doan thoi gian thuc te = voi doan thoi gian check thi no lien ke
	// * nguoc lai khong lien ke
	// */
	// if ((maxTo - minFrom + 1) != count) {
	// detailErrors +=
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.toMonth.fromMonth.lien.ke");
	// }
	// /** flag = 1: co ton tai nguoc lai 0: khong co ton tai tu thang = 1 */
	// if (flag == 0) {
	// detailErrors +=
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.fromMonth.ton.tai.thang.mot");
	// }
	// /** Neu demNull >= 2 thi ton tai 2 dong null nguoc lai thi khogn ton tai
	// 2 dong null*/
	// if (demNULL >= NULL_LIMIT) {
	// detailErrors +=
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.toMonth.khong.ton.tai.hai.dong.null");
	// }
	// if(StringUtil.isNullOrEmpty(detailErrors)){
	// /** Tien hanh set du lieu va insert xuong DB du lieu co muc doanh so*/
	// equipVO.setCreateDate(now);
	// equipVO.setCreateUser(username);
	// equipVO.setLstEquipSalePlan(lstEquipSalePlans);
	// equipmentManagerMgr.createEquipGroupByExcel(equipVO);
	// }else{
	// CellBean cellBean = lstDetails.get(0);
	// cellBean.setErrMsg(cellBean.getErrMsg() + detailErrors);
	// lstFails.addAll(lstDetails); /** Cac dong con bi loi */
	// /*if(flagCheckEmpty && iFailDt > 0){
	// List<String> rowDetail = lstData.get(iFailDt);
	// lstFails.add(StringUtil.addFailBean(rowDetail,
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.invalid")));
	// }*/
	// }
	// }else{
	// if(demSPEmpty >=1){
	// CellBean cellBean = lstDetails.get(0);
	// cellBean.setErrMsg(cellBean.getErrMsg() +
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.muc.doanh.so.khong.hop.le"));
	// lstFails.addAll(lstDetails); /** Cac dong con bi loi */
	// // lstFails.add(StringUtil.addFailBean(rowDetail,
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.muc.doanh.so.khong.hop.le")));
	// }else if(flagMucDoanhSo){
	// CellBean cellBean = lstDetails.get(0);
	// cellBean.setErrMsg(cellBean.getErrMsg() +
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.muc.doanh.so.khong.hop.le"));
	// lstFails.addAll(lstDetails); /** Cac dong con bi loi */
	// }else{
	// lstFails.addAll(lstDetails); /** Rang buoc giua cac dong con bi loi */
	// }
	// /*if(flagCheckEmpty && iFailDt > 0){
	// List<String> rowDetail = lstData.get(iFailDt);
	// lstFails.add(StringUtil.addFailBean(rowDetail,
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.invalid")));
	// }*/
	// }
	// }else{
	// if(flagValidate){
	// lstFails.addAll(lstDetails); /** Cac dong con bi loi */
	// /*if(flagCheckEmpty && iFailDt > 0){
	// List<String> rowDetail = lstData.get(iFailDt);
	// lstFails.add(StringUtil.addFailBean(rowDetail,
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.invalid")));
	// }*/
	// }else if(flagMucDoanhSo){ // chu y truong hop nay co the gay anh huogn
	// den nhung truong hop khac
	// CellBean cellBean = lstDetails.get(0);
	// cellBean.setErrMsg(cellBean.getErrMsg() +
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.muc.doanh.so.khong.hop.le"));
	// lstFails.addAll(lstDetails); /** Cac dong con bi loi */
	// }else{
	// /** Tien hanh set du lieu va insert xuong DB du lieu ko co muc doanh so*/
	// equipVO.setCreateDate(now);
	// equipVO.setCreateUser(username);
	// equipmentManagerMgr.createEquipGroupByExcel(equipVO);
	// }
	// }
	//
	// //** End */
	// }
	// }
	// }
	// if(flagCheckEmpty && iFailDt > 0){
	// List<String> rowDetail = lstData.get(iFailDt);
	// lstFails.add(StringUtil.addFailBean(rowDetail,
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.row.invalid.khong.thuc.hien.cac.dong.ben.duoi")));
	// }
	// getOutputFailExcelFile(lstFails,
	// ConstantManager.TEMPLATE_EQUIP_GROUP_EQUIPMENT_FAIL);
	// } catch (Exception e) {
	// LogUtility.logError(e, e.getMessage());
	// errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	// return SUCCESS;
	// }
	// }
	// if (StringUtil.isNullOrEmpty(errMsg)) {
	// isError = false;
	// }
	// isError = false;
	// return SUCCESS;
	// }

	/**
	 * Validate dong du lieu detail
	 * 
	 * @author liemtpt
	 * @param lstSP
	 */
	// private String validateDetail(List<String> row,EquipmentVO equipVO,
	// EquipSalePlan sp){
	// String CUSTYPE =
	// Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.cus.type.type");
	// String[] LIST_CUS_TYPE = CUSTYPE.split(";");
	// String message = "";
	// String msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 6) {
	// String value = row.get(6);
	// if( !StringUtil.isNullOrEmpty(value)){
	// msg = ValidateUtil.validateField(value,
	// "equipment.manager.group.from.month", 10, ConstantManager.ERR_REQUIRE);
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// } else {
	// value = value.trim();
	// try{
	// Integer fromMonth = new Integer(value);
	// if (fromMonth.intValue() <= 0) {
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.from.month.is.Int");
	// } else {
	// sp.setFromMonth(fromMonth);
	// }
	// }catch(Exception e){
	// // khong hop le
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.from.month.is.Int");
	// }
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	//
	// }
	//
	// }
	//
	// }
	// }
	//
	// /**
	// * Den thang kieu so nguyen duong va >= tu thang
	// **/
	// msg = "";
	// if (row.size() > 7) {
	// String value = row.get(7);
	// // if( !StringUtil.isNullOrEmpty(value)){
	// value = value.trim();
	// if (sp.getFromMonth() != null){
	// if( !StringUtil.isNullOrEmpty(value)){
	// try{
	// Integer toMonth = new Integer(value);
	// if (toMonth.intValue() <= 0) {
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.to.month.is.Int");
	// } else {
	// // kiem tra den thang >= tu thang
	// if (sp.getFromMonth() <= toMonth) {
	// //set gia tri
	// sp.setToMonth(toMonth);
	// } else {
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.toMonth.greater.than.fromMonth");
	// }
	// }
	// }catch(Exception e){
	// // khong hop le
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.to.month.is.Int");
	// }
	// }
	// }
	//
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	// // }
	// }
	//
	// /** Loai khach hang NULL,1 Thành thị,2 Nông thôn **/
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 8) {
	// String value = row.get(8);
	// if (!StringUtil.isNullOrEmpty(value)) {
	// value = value.trim();
	// try{
	// if (LIST_CUS_TYPE[0].trim().toUpperCase().equals(value.toUpperCase())) {
	// sp.setCustomerType(EquipSalePlanCustomerType.CITY.getValue());
	// } else if
	// (LIST_CUS_TYPE[1].trim().toUpperCase().equals(value.toUpperCase())) {
	// sp.setCustomerType(EquipSalePlanCustomerType.GARDEN_CITY.getValue());
	// } else {
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.customer.type.is.not.exits");
	// }
	// }catch(Exception e){
	// // khong hop le
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.customer.type.is.not.exits");
	// }
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	// } else {
	// sp.setCustomerType(EquipSalePlanCustomerType.UNDEFINED.getValue());
	// }
	// }
	// }
	//
	// /** Doanh so **/
	// //kiem tra them muc doanh so
	// msg = "";
	// if (StringUtil.isNullOrEmpty(message)) {
	// if (row.size() > 9) {
	// String value = row.get(9);
	// if (!StringUtil.isNullOrEmpty(value)) {
	// msg = ValidateUtil.validateField(value, "equipment.manager.group.amount",
	// 17, ConstantManager.ERR_REQUIRE);
	// if(StringUtil.isNullOrEmpty(msg)){
	// if (!StringUtil.isNullOrEmpty(value) &&
	// !StringUtil.isNullOrEmpty(row.get(6))) {
	// value = value.trim();
	// try{
	// Integer amount = new Integer(value);
	// if (amount.intValue() <= 0) {
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.amount.is.Int");
	// } else {
	// sp.setAmount(amount);
	// }
	// }catch(Exception e){
	// // khong hop le
	// msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
	// "equipment.manager.group.amount.is.Int");
	// }
	// }
	// }
	// }
	// }
	// }
	// if (!StringUtil.isNullOrEmpty(msg)) {
	// message += msg;
	// message += "\n";
	// }
	// return message;
	// }

	/**
	 * Xuat excel theo tim kiem danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * @return Excel
	 * */
	public String exportBySearchEquipmentVO() {
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
			if (!StringUtil.isNullOrEmpty(equipBrandName)) {
				filter.setBrand(equipBrandName);
			}
			filter.setToCapacity(toCapacity);
			filter.setFromCapacity(fromCapacity);
			if (type != null) {
				filter.setType(type);
			}
			filter.setStatus(status);

			ObjectVO<EquipmentVO> lstEquipmentVO = equipmentManagerMgr.searchEquipmentGroup(filter);
			EquipmentSalePlaneFilter<EquipSalePlan> filterSP = null;
			// List<EquipmentVO> lstTmpVO = new ArrayList<EquipmentVO>();
			List<EquipSalePlan> lstSP = new ArrayList<EquipSalePlan>();
			if (lstEquipmentVO != null && lstEquipmentVO.getLstObject() != null && lstEquipmentVO.getLstObject().size() > 0) {
				String reportToken = retrieveReportToken(reportCode);
				if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
					return JSON;
				}
				
				SXSSFWorkbook workbook = null;
				FileOutputStream out = null;
				try {
					// Init XSSF workboook
					String outputName = ConstantManager.EQUIP_GROUP_EQUIPMENT_EXPORT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ConstantManager.EXPORT_FILE_EXTENSION_EX;
					String exportFileName = Configuration.getStoreRealPath() + outputName;

					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					// Tao shett
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

					// Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(18);
					// set static Column width
					mySheet.setColumnsWidth(sheetData, 0, 180, 150, 150, 120, 120, 120, 100, 100, 150, 150, 150);
					// Size Row
					mySheet.setRowsHeight(sheetData, 0, 20);
					mySheet.setRowsHeight(sheetData, 1, 20);
					sheetData.setDisplayGridlines(true);

					int iRow = 2;
					// tao column header
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.group.header.export");
					String[] lstHeaders = headers.split(";");
					mySheet.addCellsAndMerged(sheetData, 0, 0, 0, 1, lstHeaders[0], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCellsAndMerged(sheetData, 1, 0, 1, 1, lstHeaders[1], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCellsAndMerged(sheetData, 2, 0, 2, 1, lstHeaders[2], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCellsAndMerged(sheetData, 3, 0, 3, 1, lstHeaders[3], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCellsAndMerged(sheetData, 4, 0, 4, 1, lstHeaders[4], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCellsAndMerged(sheetData, 5, 0, 5, 1, lstHeaders[5], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCellsAndMerged(sheetData, 6, 0, 9, 0, lstHeaders[6], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCell(sheetData, 6, 1, lstHeaders[7], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCell(sheetData, 7, 1, lstHeaders[8], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCell(sheetData, 8, 1, lstHeaders[9], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCell(sheetData, 9, 1, lstHeaders[10], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					mySheet.addCellsAndMerged(sheetData, 10, 0, 10, 1, lstHeaders[11], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));

					XSSFCellStyle textStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.NORMAL).clone();
					DataFormat fmt = workbook.createDataFormat();
					textStyle.setDataFormat(fmt.getFormat("@"));
					// XSSFCellStyle numberStyle = (XSSFCellStyle)
					// style.get(ExcelPOIProcessUtils.NORMAL).clone();
					// set data
					for (EquipmentVO vo : lstEquipmentVO.getLstObject()) {
						// int size = lstEquipmentVO.getLstObject().size();
						// for(; iColumn < size ; iColumn++){
						// EquipmentVO vo =
						// lstEquipmentVO.getLstObject().get(iColumn);
						// iColumn = 0;
						// iRow++;
						if (vo != null) {
							if (vo.getCode() != null) {
								EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupByCode(vo.getCode());
								if (equipGroup != null) {
									Long idGroup = equipGroup.getId();
									if (idGroup != null) {
										filterSP = new EquipmentSalePlaneFilter<EquipSalePlan>();
										filterSP.setEquipGroupId(idGroup);
										lstSP = equipmentManagerMgr.getListEquipSalePlanByFilter(filterSP);
										if (lstSP != null && lstSP.size() > 0) {
											vo.setLstEquipSalePlan(lstSP);
										}
									}
								}
							}

							if (ActiveType.RUNNING.getValue().equals(vo.getStatus())) {
								vo.setStatusStr(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.status.running"));
							} else if (ActiveType.STOPPED.getValue().equals(vo.getStatus())) {
								vo.setStatusStr(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.status.stopped"));
							}
							// lstTmpVO.add(vo);
							// set thong tin chung
							mySheet.addCell(sheetData, 0, iRow, vo.getCode(), textStyle);
							mySheet.addCell(sheetData, 1, iRow, vo.getName(), textStyle);
							mySheet.addCell(sheetData, 2, iRow, vo.getTypeGroup(), textStyle);
							mySheet.addCell(sheetData, 3, iRow, vo.getBrandName(), textStyle);
							mySheet.addCell(sheetData, 4, iRow, vo.getCapacityFrom(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER));
							mySheet.addCell(sheetData, 5, iRow, vo.getCapacityTo(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER));
							mySheet.addCell(sheetData, 10, iRow, vo.getStatusStr(), textStyle);
							String customerTypeStr = "";
							if (lstSP != null && lstSP.size() > 0) {
								// int iRowSP = iRow;
								for (EquipSalePlan e : lstSP) {
//									if (EquipSalePlanCustomerType.CITY.getValue().equals(e.getCustomerTypeId())) {
//										customerTypeStr = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.err.customer.type.is.city");
//									} else if (EquipSalePlanCustomerType.GARDEN_CITY.getValue().equals(e.getCustomerType())) {
//										customerTypeStr = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.err.customer.type.is.garden.city");
//									} else {
//										customerTypeStr = "";
//									}
									customerTypeStr = "";
									if (e.getCustomerTypeId() != null) {
										ChannelType type = commonMgr.getEntityById(ChannelType.class, e.getCustomerTypeId());
										if (type != null) {
											customerTypeStr = type.getChannelTypeName();
										}
										
									}
									
									mySheet.addCell(sheetData, 6, iRow, e.getFromMonth() == null ? "" : e.getFromMonth(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER));
									mySheet.addCell(sheetData, 7, iRow, e.getToMonth() == null ? "" : e.getToMonth(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER));
									mySheet.addCell(sheetData, 8, iRow, customerTypeStr, textStyle);
									mySheet.addCell(sheetData, 9, iRow, e.getAmount(), style.get(ExcelPOIProcessUtils.NORMAL_NUMBER));
									iRow++;
								}
							} else {
								mySheet.addCell(sheetData, 6, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_NUMBER));
								mySheet.addCell(sheetData, 7, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_NUMBER));
								mySheet.addCell(sheetData, 8, iRow, "", textStyle);
								mySheet.addCell(sheetData, 9, iRow, "", style.get(ExcelPOIProcessUtils.NORMAL_NUMBER));
								iRow++;
							}
							//mySheet.addCell(sheetData, 10, iRow, vo.getStatusStr(), textStyle); // chuyen len tren
						}
					}

					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} catch (Exception e) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					LogUtility.logError(e, "CustomerReportAction.exportBBGNExcel" + e.getMessage());
				} finally {
					if (workbook != null) {
						workbook.dispose();
					}
					if (out != null) {
						out.close();
					}
				}
				return JSON;
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Check dong trong cua file import du lieu nhom thiet bi
	 * @param row
	 * @return
	 */
	public boolean isEmtyRow(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4))
				|| !StringUtil.isNullOrEmpty(row.get(5)) || !StringUtil.isNullOrEmpty(row.get(6)) || !StringUtil.isNullOrEmpty(row.get(7)) || !StringUtil.isNullOrEmpty(row.get(8)) || !StringUtil.isNullOrEmpty(row.get(9))
				|| !StringUtil.isNullOrEmpty(row.get(10))) {
			return false;
		}
		return true;
	}

//	public boolean isRowValid(List<String> row) {
//		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4))
//				|| !StringUtil.isNullOrEmpty(row.get(5))) {
//			return false;
//		}
//		return true;
//	}

	/***
	 * Check row cua nhom thiet bi la dong chinh
	 * @param row
	 * @return
	 */
	public boolean isRowKeyGroup(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3))
				|| !StringUtil.isNullOrEmpty(row.get(4)) || !StringUtil.isNullOrEmpty(row.get(5))|| !StringUtil.isNullOrEmpty(row.get(10))) {
			return true;
		}
		return false;
	}

	public boolean isRowKeySalePlan(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(6)) || !StringUtil.isNullOrEmpty(row.get(7)) || !StringUtil.isNullOrEmpty(row.get(8)) || !StringUtil.isNullOrEmpty(row.get(9))) {
			return true;
		}
		return false;
	}

	public String checkLikeRow(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).equals(row2.get(0)) && row1.get(1).equals(row2.get(1))) {
						if (row1.get(2).equals(row2.get(2))) {
							if (StringUtil.isNullOrEmpty(message)) {
								message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
							} else {
								message += ", " + (k + 2);
							}
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
			}
		}
		return message;
	}

	/**
	 * import danh muc thiet bi
	 * 
	 * @author phuongvm
	 * @since 16/03/2015
	 */
	public String importExcel() throws Exception {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		EquipmentFilter<EquipmentVO> filter;
		String username = currentUser.getStaffRoot().getStaffCode();
		Date now = commonMgr.getSysDate();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 5);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				for (int i = 0; i < lstData.size(); i++) {
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						Integer type = null;
						Integer typeHMSC = null;
						String code = "";
						String name = "";
						Integer warranty = null;
						// ** Check trung du lieu cac record */
						message = "";
						message = this.checkLikeRow(lstData, i);
						if (!StringUtil.isNullOrEmpty(message)) {
							lstFails.add(StringUtil.addFailBean(lstData.get(i), message));
							continue;
						}
						// ** Loai */
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "equipment.catalog.manage.type", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim();
								value = value.toUpperCase();
								if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.NCC").toUpperCase())) {
									type = EquipmentChannelTypeObjectType.EQM_NCC.getValue();
								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.hmsc").toUpperCase())) {
									type = EquipmentChannelTypeObjectType.EQM_HMSC.getValue();
								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.loaitb").toUpperCase())) {
									type = EquipmentChannelTypeObjectType.EQM_LTB.getValue();
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.loai.err");
								}
							}
						}

						// ** Ma */
						if (StringUtil.isNullOrEmpty(message)) {
							if (row.size() > 1) {
								String value = row.get(1);
								//tamvnm: Loai thiet bi chi 2 ky tu
								String msg = "";
								if (type != null && EquipmentChannelTypeObjectType.EQM_LTB.getValue().equals(type)) {
									msg = ValidateUtil.validateField(value, "equipment.catalog.manage.code", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								} else {
									msg = ValidateUtil.validateField(value, "equipment.catalog.manage.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								}
								
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									value = value.trim().toUpperCase();
									filter = new EquipmentFilter<EquipmentVO>();
									filter.setCode(value);
									ObjectVO<EquipmentVO> listEquipCategoryCode = null;
									if (EquipmentChannelTypeObjectType.EQM_LTB.getValue().equals(type)) {
										listEquipCategoryCode = equipmentManagerMgr.listEquipCategoryCode(filter);
									} else if (EquipmentChannelTypeObjectType.EQM_NCC.getValue().equals(type)) {
										listEquipCategoryCode = equipmentManagerMgr.listEquipEquipProvider(filter);
									} else if (EquipmentChannelTypeObjectType.EQM_HMSC.getValue().equals(type)) {
										listEquipCategoryCode = equipmentManagerMgr.listEquipItem(filter);
									}
									if (listEquipCategoryCode != null && listEquipCategoryCode.getLstObject() != null && !listEquipCategoryCode.getLstObject().isEmpty()) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.err.code.exist") + BREAK_LINE;
									} else {
										code = value;
									}
								}
							}
						}
						// ** Ten */
						if (row.size() > 2) {
							String value = row.get(2);
							String msg = ValidateUtil.validateField(value, "equipment.catalog.manage.name", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								name = value;
							}
						}
						// ** loai hang muc - khi nhap Loai: hang muc sua chua
						// (type = 4)*/

						if (EquipmentChannelTypeObjectType.EQM_HMSC.getValue().equals(type)) {
							if (row.size() > 3) {
								String value = row.get(3);
								String msg = ValidateUtil.validateField(value, "equipment.catalog.manage.loaihm", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									value = value.trim();
									value = value.toUpperCase();
									if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.ko.cho.thay.doi").toUpperCase())) {
										typeHMSC = 0;
									} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.cho.thay.doi").toUpperCase())) {
										typeHMSC = 1;
									} else {

										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.loai.err2");
									}
								}
							}

							if (row.size() > 4) {
								String value = row.get(4);
								String msg = ValidateUtil.validateField(value, "equipment.catalog.manage.warranty", 5, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									value = value.trim();
									if (!StringUtil.isNullOrEmpty(value)) {
										try {
											warranty = Integer.valueOf(value);
										} catch (Exception e) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.integer.zero", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.warranty"));
										}
										if (warranty != null && warranty < 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.integer.zero", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.warranty"));
										}
									}
								}
							}
						} else {
							if (row.size() > 3) {
								String value = row.get(3);
								if (!StringUtil.isNullOrEmpty(value)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.ko.cho.thay.doi.err");
								}
							}

							if (row.size() > 4) {
								String value = row.get(4);
								if (!StringUtil.isNullOrEmpty(value)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.ko.cho.thay.doi.err1");
								}
							}
						}
						if (StringUtil.isNullOrEmpty(message)) {
							if (EquipmentChannelTypeObjectType.EQM_LTB.getValue().equals(type)) {
								EquipCategory equipCategory = new EquipCategory();
								equipCategory.setCode(code);
								equipCategory.setName(name);
								equipCategory.setStatus(ActiveType.RUNNING);
								equipCategory.setCreateDate(now);
								equipCategory.setCreateUser(username);
								equipCategory = equipmentManagerMgr.createEquipCategoryManagerCatalog(equipCategory, getLogInfoVO());
							} else if (EquipmentChannelTypeObjectType.EQM_NCC.getValue().equals(type)) {
								EquipProvider equipProvider = new EquipProvider();
								equipProvider.setCode(code);
								equipProvider.setName(name);
								equipProvider.setStatus(ActiveType.RUNNING);
								equipProvider.setCreateDate(now);
								equipProvider.setCreateUser(username);
								equipProvider = equipmentManagerMgr.createEquipProviderManagerCatalog(equipProvider, getLogInfoVO());
							} else if (EquipmentChannelTypeObjectType.EQM_HMSC.getValue().equals(type)) {
								EquipItem equipItem = new EquipItem();
								equipItem.setCode(code);
								equipItem.setName(name);
								equipItem.setWarranty(warranty);
								equipItem.setStatus(ActiveType.RUNNING);
								equipItem.setType(typeHMSC);
								equipItem.setCreateDate(now);
								equipItem.setCreateUser(username);
								equipItem = equipmentManagerMgr.createEquipItemManagerCatalog(equipItem, getLogInfoVO());
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_EQUIPMENT_CATALOG_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return SUCCESS;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * Xuat excel danh muc thiet bi
	 * 
	 * @author phuongvm
	 * @since 16/03/2015
	 * @return
	 */
	public String exportExcel() {
		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			filter.setkPaging(null);
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
			if (status != null) {
				filter.setStatus(status);
			}
			if (type != null) {
				filter.setType(type);
			}
			ObjectVO<EquipmentVO> lstEquipment = null;
			lstEquipment = equipmentManagerMgr.getListEquipmentGeneral(filter);
			SXSSFWorkbook workbook = null;
			FileOutputStream out = null;
			try {
				if (lstEquipment != null && lstEquipment.getLstObject() != null && !lstEquipment.getLstObject().isEmpty()) {

					// Init XSSF workboook
					String outputName = ConstantManager.TEMPLATE_IMPORT_EQUIP_CATALOG + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "." + FileExtension.XLSX;
					String exportFileName = Configuration.getStoreRealPath() + outputName;

					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					// Tao shett
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("Sheet1");
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

					// Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(18);
					// set static Column width
					mySheet.setColumnsWidth(sheetData, 0, 150, 100, 270, 270);
					// Size Row
					mySheet.setRowsHeight(sheetData, 0, 20);
					sheetData.setDisplayGridlines(true);
					// text style
					XSSFCellStyle textStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.NORMAL).clone();
					DataFormat fmt = workbook.createDataFormat();
					textStyle.setDataFormat(fmt.getFormat("@"));

					XSSFCellStyle numberStyle = (XSSFCellStyle) style.get(ExcelPOIProcessUtils.NORMAL_NUMBER).clone();

					int iRow = 0, iColumn = 0;
					// tao column header
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.catalog");
					String[] lstHeaders = headers.split(";");
					for (int i = 0; i < lstHeaders.length; i++) {
						mySheet.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN));
					}
					iRow++;
					for (int i = 0, size = lstEquipment.getLstObject().size(); i < size; i++) {
						iColumn = 0;
						String type = "";
						String typeHMSC = "";
						if (EquipmentChannelTypeObjectType.EQM_NCC.getValue().equals(lstEquipment.getLstObject().get(i).getType())) {
							type = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.NCC");
						} else if (EquipmentChannelTypeObjectType.EQM_LTB.getValue().equals(lstEquipment.getLstObject().get(i).getType())) {
							type = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.loaitb");
						} else if (EquipmentChannelTypeObjectType.EQM_HMSC.getValue().equals(lstEquipment.getLstObject().get(i).getType())) {
							type = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.hmsc");
						}
						if (lstEquipment.getLstObject().get(i).getTypeHMSC() != null) {
							if (lstEquipment.getLstObject().get(i).getTypeHMSC() > 0) {
								typeHMSC = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.cho.thay.doi");
							} else {
								typeHMSC = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.catalog.manage.ko.cho.thay.doi");
							}
						}
						mySheet.addCell(sheetData, iColumn++, iRow, type, textStyle);
						mySheet.addCell(sheetData, iColumn++, iRow, lstEquipment.getLstObject().get(i).getCode(), textStyle);
						mySheet.addCell(sheetData, iColumn++, iRow, lstEquipment.getLstObject().get(i).getName(), textStyle);
						mySheet.addCell(sheetData, iColumn++, iRow, typeHMSC, textStyle);
						if (lstEquipment.getLstObject().get(i).getWarranty() != null) {
							mySheet.addCell(sheetData, iColumn++, iRow, lstEquipment.getLstObject().get(i).getWarranty(), numberStyle);
						}
						iRow++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
					result.put(ERROR, true);
					result.put("data.hasData", true);
					result.put("errMsg", errMsg);
				}
			} catch (Exception e) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.error.system");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				LogUtility.logError(e, e.getMessage());
			} finally {
				if (workbook != null) {
					workbook.dispose();
				}
				if (out != null) {
					out.close();
				}
			}
			return JSON;
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	// TODO Khai bai cac thuoc tinh GETTER/SETTER
	/**
	 * Xu ly khai bao cac ham getter/ setter
	 * 
	 * @author hoanv25
	 * @since December 14,2014
	 * */
	private EquipmentVO equipmentVO = new EquipmentVO();
	private EquipmentVO equipBrandVO = new EquipmentVO();
	private EquipmentVO equipCategoryVO = new EquipmentVO();

	private Long id;

	private String code;
	private String name;
	private String customerTypeName;
	private String typeGroup;
	private String codeCtg;
	private String codeEquipGroup;
	private String equipBrandName;
	private String arrDelStr;
	private String arrFromMonthStr;
	private String arrToMonthStr;
	private String arrCustomerTypeStr;
	private String arrAmountStr;

	private Integer type;
	private Integer typeItem;
	private Integer status;
	private Integer statusStr;
	private Integer fromMonth;
	private Integer toMonth;
	private Integer customerType;
	private Integer amount;
	private Integer warranty;

	private BigDecimal toCapacity;
	private BigDecimal fromCapacity;
	private String excelFileContentType;
	private File excelFile;

	private List<EquipmentVO> listEquipBrand;
	private List<EquipmentVO> listEquipProvider;
	private List<EquipmentVO> listEquipCategory;
	private List<EquipmentVO> listChangeEquipGroupManager;
	private List<ChannelType> lstCustomerType;

	public String getArrDelStr() {
		return arrDelStr;
	}

	public void setArrDelStr(String arrDelStr) {
		this.arrDelStr = arrDelStr;
	}

	public String getArrFromMonthStr() {
		return arrFromMonthStr;
	}

	public void setArrFromMonthStr(String arrFromMonthStr) {
		this.arrFromMonthStr = arrFromMonthStr;
	}

	public String getArrToMonthStr() {
		return arrToMonthStr;
	}

	public void setArrToMonthStr(String arrToMonthStr) {
		this.arrToMonthStr = arrToMonthStr;
	}

	public String getArrCustomerTypeStr() {
		return arrCustomerTypeStr;
	}

	public void setArrCustomerTypeStr(String arrCustomerTypeStr) {
		this.arrCustomerTypeStr = arrCustomerTypeStr;
	}

	public String getArrAmountStr() {
		return arrAmountStr;
	}

	public void setArrAmountStr(String arrAmountStr) {
		this.arrAmountStr = arrAmountStr;
	}

	public EquipmentVO getEquipmentVO() {
		return equipmentVO;
	}

	public void setEquipmentVO(EquipmentVO equipmentVO) {
		this.equipmentVO = equipmentVO;
	}

	public EquipmentVO getEquipBrandVO() {
		return equipBrandVO;
	}

	public void setEquipBrandVO(EquipmentVO equipBrandVO) {
		this.equipBrandVO = equipBrandVO;
	}

	public EquipmentVO getEquipCategoryVO() {
		return equipCategoryVO;
	}

	public void setEquipCategoryVO(EquipmentVO equipCategoryVO) {
		this.equipCategoryVO = equipCategoryVO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public BigDecimal getToCapacity() {
		return toCapacity;
	}

	public BigDecimal getFromCapacity() {
		return fromCapacity;
	}

	public void setToCapacity(BigDecimal toCapacity) {
		this.toCapacity = toCapacity;
	}

	public void setFromCapacity(BigDecimal fromCapacity) {
		this.fromCapacity = fromCapacity;
	}

	public String getEquipBrandName() {
		return equipBrandName;
	}

	public void setEquipBrandName(String equipBrandName) {
		this.equipBrandName = equipBrandName;
	}

	public Integer getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(Integer statusStr) {
		this.statusStr = statusStr;
	}

	public List<EquipmentVO> getListEquipBrand() {
		return listEquipBrand;
	}

	public void setListEquipBrand(List<EquipmentVO> listEquipBrand) {
		this.listEquipBrand = listEquipBrand;
	}

	public List<EquipmentVO> getListEquipCategory() {
		return listEquipCategory;
	}

	public String getCodeCtg() {
		return codeCtg;
	}

	public void setCodeCtg(String codeCtg) {
		this.codeCtg = codeCtg;
	}

	public void setListEquipCategory(List<EquipmentVO> listEquipCategory) {
		this.listEquipCategory = listEquipCategory;
	}

	public String getTypeGroup() {
		return typeGroup;
	}

	public void setTypeGroup(String typeGroup) {
		this.typeGroup = typeGroup;
	}

	public List<EquipmentVO> getListEquipProvider() {
		return listEquipProvider;
	}

	public void setListEquipProvider(List<EquipmentVO> listEquipProvider) {
		this.listEquipProvider = listEquipProvider;
	}

	public List<EquipmentVO> getListChangeEquipGroupManager() {
		return listChangeEquipGroupManager;
	}

	public void setListChangeEquipGroupManager(List<EquipmentVO> listChangeEquipGroupManager) {
		this.listChangeEquipGroupManager = listChangeEquipGroupManager;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public Integer getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(Integer fromMonth) {
		this.fromMonth = fromMonth;
	}

	public Integer getToMonth() {
		return toMonth;
	}

	public void setToMonth(Integer toMonth) {
		this.toMonth = toMonth;
	}

	public Integer getCustomerType() {
		return customerType;
	}

	public void setCustomerType(Integer customerType) {
		this.customerType = customerType;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getCustomerTypeName() {
		return customerTypeName;
	}

	public void setCustomerTypeName(String customerTypeName) {
		this.customerTypeName = customerTypeName;
	}

	public String getCodeEquipGroup() {
		return codeEquipGroup;
	}

	public void setCodeEquipGroup(String codeEquipGroup) {
		this.codeEquipGroup = codeEquipGroup;
	}

	public Integer getTypeItem() {
		return typeItem;
	}

	public void setTypeItem(Integer typeItem) {
		this.typeItem = typeItem;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Integer getWarranty() {
		return warranty;
	}

	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}

	/** @return the lstCustomerType */
	public List<ChannelType> getLstCustomerType() {
		return lstCustomerType;
	}

	/** @param lstCustomerType the lstCustomerType to set */
	public void setLstCustomerType(List<ChannelType> lstCustomerType) {
		this.lstCustomerType = lstCustomerType;
	}

}

package ths.dms.web.action.equipment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.EquipItem;
import ths.dms.core.entities.EquipItemConfig;
import ths.dms.core.entities.EquipItemConfigDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipItemConfigDtlFilter;
import ths.dms.core.entities.filter.EquipItemConfigFilter;
import ths.dms.core.entities.vo.EquipItemConfigDtlVO;
import ths.dms.core.entities.vo.EquipItemConfigVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.memcached.MemcachedUtils;

///**
// * Thiet lap dinh muc hang muc
// * 
// * @author liemtpt
// * @since 30/03/2015
// * 
// * @author hunglm16
// * @since May 05,2015
// * @description ra soat va hotfix
// */
public class EquipItemConfigAction extends AbstractAction {

	private static final long serialVersionUID = 5330840248556870465L;

	EquipmentManagerMgr equipmentManagerMgr;
	CommonMgr commonMgr;
	StaffMgr staffMgr;
	ShopMgr shopMgr;

	@Override
	public void prepare() throws Exception {
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");

		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		return SUCCESS;
	}

	/**
	 * Search
	 * 
	 * @return the string
	 * @author liemtpt
	 * @since 07/04/2015
	 * @description tim kiem danh sach thiet lap
	 */
	public String search() {
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			KPaging<EquipItemConfigVO> kPaging = new KPaging<EquipItemConfigVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipItemConfigFilter filter = new EquipItemConfigFilter();
			filter.setkPaging(kPaging);
			if (fromCapacity != null) {
				filter.setFromCapacity(fromCapacity);
			}
			if (toCapacity != null) {
				filter.setToCapacity(toCapacity);
			}
			if (status != null && !ActiveType.DELETED.getValue().equals(status)) {
				filter.setStatus(status);
			}
			ObjectVO<EquipItemConfigVO> lsVO = equipmentManagerMgr.getListEquipItemConfigByFilter(filter);
			if (lsVO != null) {
				result.put("total", lsVO.getkPaging().getTotalRows());
				result.put("rows", lsVO.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipItemConfigVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return JSON;
	}

	/**
	 * Lay danh sach hang muc theo Dinh muc
	 * Left Join
	 * 
	 * @return the string
	 * @author hunglm16
	 * @since 07/05/2015
	 */
	public String getListEquipItemConfigDetail() {
		try {
			result.put("rows", new ArrayList<EquipItemConfigDtlVO>());
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			List<EquipItemConfigDtlVO> lsVO = equipmentManagerMgr.getEquipItemInDetailByEquipItemConfig(id);
			if (lsVO != null) {
				result.put("rows", lsVO);
			} else {
				result.put("rows", new ArrayList<EquipItemConfigDtlVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return JSON;
	}

	/**
	 * Luu thong tin danh muc thiet bi
	 * 
	 * @return the string
	 * @author liemtpt
	 * @since 15/12/2014
	 * @description Update equipment
	 * 
	 * @author hunglm16
	 * @since May 06,2015
	 * @description Ra soat va bo sung cac truong mac dinh
	 */
	public String saveOrUpdateEquipItemConfig() {
		resetToken(result);
		try {
			String errMsg = "";
			result.put(ERROR, true);
			if (currentUser != null && currentUser.getUserName() != null) {
				if (StringUtil.isNullOrEmpty(errMsg) && fromCapacity == null && toCapacity == null) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.from.to.ton.tai.mot.trong.hai");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && fromCapacity != null && fromCapacity < 1) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.from.is.Int");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && toCapacity != null && toCapacity < 1) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.to.is.Int");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && fromCapacity != null && toCapacity != null && toCapacity < fromCapacity) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.dung.tich.den.phai.lon.hon.dung.tich.tu");
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					return JSON;
				}
				//Lay ngay hien tai
				Date sysDateC = commonMgr.getSysDate();
				if (id != null && id != 0L) {
					EquipItemConfig equipItemConfig = commonMgr.getEntityById(EquipItemConfig.class, id);
					if (equipItemConfig == null) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.error.undefined"));
						return JSON;
					} else if (equipItemConfig.getStatus() == null || !ActiveType.RUNNING.getValue().equals(equipItemConfig.getStatus().getValue())) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.error.running.undefined"));
						return JSON;
					} else {
						Long[] arrException = new Long[1];
						arrException[0] = equipItemConfig.getId();
						if (StringUtil.isNullOrEmpty(errMsg) && equipmentManagerMgr.countIntersectionCapacity(fromCapacity, toCapacity, ActiveType.RUNNING.getValue(), arrException) > 0) {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.check.trung.chom.dung.tich.tu.den"));
							return JSON;
						}
					}
					equipItemConfig.setFromCapacity(fromCapacity);
					equipItemConfig.setToCapacity(toCapacity);
					equipItemConfig.setStatus(ActiveType.RUNNING);
					equipItemConfig.setUpdateDate(sysDateC);
					equipItemConfig.setUpdateUser(currentUser.getUserName());
					commonMgr.updateEntity(equipItemConfig);
				} else {
					if (StringUtil.isNullOrEmpty(errMsg) && equipmentManagerMgr.countIntersectionCapacity(fromCapacity, toCapacity, ActiveType.RUNNING.getValue(), null) > 0) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.check.trung.chom.dung.tich.tu.den"));
						return JSON;
					}
					EquipItemConfig equipItemConfig = new EquipItemConfig();
					String code = equipmentManagerMgr.getCodeEquipItemConfig();
					equipItemConfig.setCode(code);
					equipItemConfig.setFromCapacity(fromCapacity);
					equipItemConfig.setToCapacity(toCapacity);
					equipItemConfig.setStatus(ActiveType.RUNNING);
					equipItemConfig.setCreateDate(sysDateC);
					equipItemConfig.setCreateUser(currentUser.getUserName());
					equipItemConfig = commonMgr.createEntity(equipItemConfig);
				}
				result.put(ERROR, false);
			} else {
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipItemConfigAction.saveOrUpdateEquipItemConfig() " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * removed equip item config
	 * 
	 * @return the string
	 * @author liemtpt
	 * @since 13/04/2015
	 * @description Xoa thong tin dinh muc
	 * 
	 * @author hunglm16
	 * @since May 07,2015
	 * @description Ra soat va Hotfix
	 */
	public String removedEquipItemConfig() {
		resetToken(result);
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				if (id != null && id != 0L) {
					EquipItemConfig equipItemConfig = commonMgr.getEntityById(EquipItemConfig.class, id);
					if (equipItemConfig == null) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.error.undefined"));
						return JSON;
					}
					equipmentManagerMgr.updateEquipItemConfigByStatus(equipItemConfig, ActiveType.DELETED.getValue(), currentUser.getUserName());
				}
				result.put(ERROR, false);
			} else {
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * tam ngung Dinh muc Hang muc
	 * 
	 * @author hunglm16
	 * @since May 07,2015
	 */
	public String stopEquipItemConfig() {
		resetToken(result);
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				if (id != null && id != 0L) {
					EquipItemConfig equipItemConfig = commonMgr.getEntityById(EquipItemConfig.class, id);
					if (equipItemConfig == null) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.error.undefined"));
						return JSON;
					} else if (equipItemConfig.getStatus() == null || !ActiveType.RUNNING.getValue().equals(equipItemConfig.getStatus().getValue())) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.error.running.undefined"));
						return JSON;
					}
					equipmentManagerMgr.updateEquipItemConfigByStatus(equipItemConfig, ActiveType.STOPPED.getValue(), currentUser.getUserName());
				}
				result.put(ERROR, false);
			} else {
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * save equip item config dtl
	 * 
	 * @return the string
	 * @author liemtpt
	 * @since 15/12/2014
	 * @description Luu thong tin dinh muc hang muc nhap vao
	 */
	public String saveEquipItemConfigDtl() {
		resetToken(result);
		try {
			result.put(ERROR, true);
			if (currentUser != null && currentUser.getUserName() != null) {
				if (equipItemConfigVO == null) {
					return JSON;
				}
				EquipItemConfig equipItemConfig = new EquipItemConfig();
				EquipItemConfigDtl equipItemConfigDtl = new EquipItemConfigDtl();
				if (equipItemConfigVO.getId() != null) {
					equipItemConfig = commonMgr.getEntityById(EquipItemConfig.class, equipItemConfigVO.getId());
					if (equipItemConfig == null) {
						return JSON;
					}
					EquipItem equipItem = new EquipItem();
					Date changeDate = commonMgr.getSysDate();
					equipItemConfig.setUpdateDate(changeDate);
					equipItemConfig.setUpdateUser(currentUser.getUserName());
					if (equipItemConfigVO.getLstEquipItemConfigDtlVO() != null && equipItemConfigVO.getLstEquipItemConfigDtlVO().size() > 0) {
						List<EquipItemConfigDtl> lstEquipItemConfigDtl = new ArrayList<EquipItemConfigDtl>();
						for (EquipItemConfigDtlVO vo : equipItemConfigVO.getLstEquipItemConfigDtlVO()) {
							if (vo != null && vo.getEquipItemId() > 0) {
								equipItem = new EquipItem();
								equipItem = commonMgr.getEntityById(EquipItem.class, vo.getEquipItemId());
								if (equipItem != null) {
									if (vo.getId() != null && vo.getId() > 0) {
										equipItemConfigDtl = commonMgr.getEntityById(EquipItemConfigDtl.class, vo.getId());
										equipItemConfigDtl.setUpdateDate(changeDate);
										equipItemConfigDtl.setUpdateUser(currentUser.getUserName());
									} else {
										equipItemConfigDtl = new EquipItemConfigDtl();
										equipItemConfigDtl.setCreateDate(changeDate);
										equipItemConfigDtl.setCreateUser(currentUser.getUserName());
									}
									equipItemConfigDtl.setEquipItemConfig(equipItemConfig);
									equipItemConfigDtl.setEquipItem(equipItem);
									equipItemConfigDtl.setFromMaterialPrice(vo.getFromMaterialPrice());
									equipItemConfigDtl.setToMaterialPrice(vo.getToMaterialPrice());
									equipItemConfigDtl.setFromWorkerPrice(vo.getFromWorkerPrice());
									equipItemConfigDtl.setToWorkerPrice(vo.getToWorkerPrice());
									equipItemConfigDtl.setStatus(ActiveType.RUNNING);
									lstEquipItemConfigDtl.add(equipItemConfigDtl);						
								}
							}
						}
						if (lstEquipItemConfigDtl != null && lstEquipItemConfigDtl.size() > 0) {
							equipmentManagerMgr.createOrUpdateEquipItemConfigByExcel(equipItemConfig, lstEquipItemConfigDtl);
						}
					}
				}
				result.put(ERROR, false);
			} else {
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipItemConfigAction.saveAllEquipItemConfigDtl() " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xuat excel theo tim kiem danh muc thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * @return Excel
	 * */
	public String exportBySearchEquipmentVO1() {
		/*
		 * try { EquipmentFilter<EquipmentVO> filter = new
		 * EquipmentFilter<EquipmentVO>(); if (!StringUtil.isNullOrEmpty(code))
		 * { filter.setCode(code); } if (!StringUtil.isNullOrEmpty(name)) {
		 * filter.setName(name); } if
		 * (!StringUtil.isNullOrEmpty(equipBrandName)) {
		 * filter.setBrand(equipBrandName); } filter.setToCapacity(toCapacity);
		 * filter.setFromCapacity(fromCapacity); if (type != null) {
		 * filter.setType(type); } filter.setStatus(status);
		 * 
		 * ObjectVO<EquipmentVO> lstEquipmentVO =
		 * equipmentManagerMgr.searchEquipmentGroup(filter); List<EquipmentVO>
		 * lstTmpVO = new ArrayList<EquipmentVO>(); if (lstEquipmentVO != null
		 * && lstEquipmentVO.getLstObject() != null &&
		 * !lstEquipmentVO.getLstObject().isEmpty()) { for (EquipmentVO vo :
		 * lstEquipmentVO.getLstObject()) { if
		 * (ActiveType.RUNNING.getValue().equals(vo.getStatus())) {
		 * vo.setStatusStr
		 * (Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
		 * "equipment.manager.status.running")); } else if
		 * (ActiveType.STOPPED.getValue().equals(vo.getStatus())) {
		 * vo.setStatusStr
		 * (Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
		 * "equipment.manager.status.stopped")); } if
		 * (EquipSalePlanCustomerType.
		 * CITY.getValue().equals(vo.getCustomerType())) {
		 * vo.setCustomerTypeStr(
		 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
		 * "equipment.manager.err.customer.type.is.city")); } else if
		 * (EquipSalePlanCustomerType
		 * .GARDEN_CITY.getValue().equals(vo.getCustomerType())) {
		 * vo.setCustomerTypeStr
		 * (Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
		 * "equipment.manager.err.customer.type.is.garden.city")); } else {
		 * vo.setCustomerTypeStr(""); } lstTmpVO.add(vo); } Map<String, Object>
		 * beans = new HashMap<String, Object>(); beans.put("rows", lstTmpVO);
		 * String folder =
		 * ServletActionContext.getServletContext().getRealPath("/") +
		 * "/resources/templates/equipment/"; String templateFileName = folder +
		 * ConstantManager.TEMPLATE_EQUIP_GROUP_EQUIPMENT_EXPORT;
		 * templateFileName = templateFileName.replace('/', File.separatorChar);
		 * String outputName = ConstantManager.EQUIP_GROUP_EQUIPMENT_EXPORT +
		 * genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
		 * String exportFileName = (Configuration.getStoreRealPath() +
		 * outputName).replace('/', File.separatorChar); InputStream inputStream
		 * = new BufferedInputStream(new FileInputStream(templateFileName));
		 * XLSTransformer transformer = new XLSTransformer(); Workbook
		 * resultWorkbook = transformer.transformXLS(inputStream, beans);
		 * inputStream.close(); OutputStream os = new BufferedOutputStream(new
		 * FileOutputStream(exportFileName)); resultWorkbook.write(os);
		 * os.flush(); os.close(); result.put(ERROR, false); String outputPath =
		 * Configuration.getExportExcelPath() + outputName; result.put("path",
		 * outputPath); } else { result.put(ERROR, true); result.put("errMsg",
		 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
		 * "equipment.manager.err.isEmpty")); } } catch (Exception e) {
		 * LogUtility.logError(e, e.getMessage()); result.put(ERROR, true);
		 * result.put("errMsg",
		 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM)); }
		 */
		return JSON;
	}

	/**
	 * Tai tap tin excel import thiet lap dinh muc hang muc
	 * 
	 * @author liemtpt
	 * @since 16/04/2015
	 * @return Excel
	 * 
	 * @author hunglm16
	 * @since May 05,2015
	 * @description Bo sung Check ATTT
	 * */
	public String downloadTemplateImportEquipItemConfig() {
		try {
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			Map<String, Object> beans = new HashMap<String, Object>();
			lstEquipItemConfDtl = equipmentManagerMgr.getEquipItemInDetailByEquipItemConfig(-1l);
			if (lstEquipItemConfDtl != null && lstEquipItemConfDtl.size() > 0) {
				beans.put("rows", lstEquipItemConfDtl);
			}
			String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
			String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_ITEM_CONFIG_EXPORT;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = "Bieu_Mau_Thiet_Lap_Dinh_Muc_Hang_Muc" + ConstantManager.EXPORT_FILE_EXTENSION_EX;
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put("path", outputPath);
			//Bo sung ATTT
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			//Set mac dinh he thong truoc khi tao cac du lieu tam
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Import excel thiet lap dinh muc hang muc
	 * 
	 * @author liemtpt
	 * @since 17/04/2015
	 * @description Import Excel
	 * 
	 * @author hunglm16
	 * @since May 05,2015
	 * @description Ra soat theo luong dac ta moi Lam lai moi
	 * */
	public String importEquipItemConfig() throws Exception {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		//Khai bao cac tham bien dung chung
		totalItem = 0;
		numFail = 0;
		numFinish = 0;
		String message = "";
		//Khai bao cac co hieu
		boolean isValid = true;
		//Khai bao cac gia tri tuong tac voi file import
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<CellBean> lstFailsInItem = new ArrayList<CellBean>();
		List<EquipItem> lstEquipItem = new ArrayList<EquipItem>();
		EquipItemConfigDtl equipItemConfigDtl = null;
		List<EquipItemConfigDtl> lstEquipItemConfigDtl = new ArrayList<EquipItemConfigDtl>();
		//Khai bao cac gia tri mac dinh cho cac cot trong File Import
		String value = null;
		String msg = null;
		Double donGiaVatTuTu = null;
		Double donGiaVatTuDen = null;
		Double donGiaNhanCongTu = null;
		Double donGiaNhanCongDen = null;
		Integer fromCapacity = null;
		Integer toCapacity = null;
		//Khai bao cac doi tuong luu tru
		EquipItem equipItem = null;
		EquipItemConfig equipItemConfig = null;
		Date nowDate = commonMgr.getSysDate();
		String username = currentUser.getUserName();
		try {
			//Thuc hien doc file excel
			List<List<String>> rows = getExcelDataEx(excelFile, excelFileContentType, errMsg, 7);
			if (rows != null && rows.size() > 0) {
				int dem = 0;
				int size = rows.size();
				int maxIndex = size - 1;
				int flagFirt = 0;
				int countMax = 0;
				while (dem < size && countMax < size) {
					countMax ++;
					if (!isValid) {
						break;
					}
					flagFirt = dem;
					List<List<String>> lstGroup = new ArrayList<List<String>>();
					lstFailsInItem = new ArrayList<CellBean>();
					for (int i = dem; i < size; i++) {
						if (!StringUtil.isEmtyRow(rows.get(i), 7)) {
							lstGroup.add(rows.get(i));
							if (i == maxIndex) {
								//Xu ly cho dieu kien dung
								dem = size;
							}
						} else {
							if (i < maxIndex) {
								if (StringUtil.isEmtyRow(rows.get(i + 1), 7)) {
									isValid = false;
								}
								//Xu ly cho group tiep theo
								dem = i + 1;
							} else {
								dem = maxIndex;
							}
							break;
						}
					}
					if (!isValid) {
						if (lstGroup.size() == 0 && dem == 0) {
							//Loi 2 dong lien tiep dau tien de rong
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.import.row.null.two");
							return SUCCESS;
						}
					}
					if (lstGroup.size() > 0) {
						lstEquipItem = new ArrayList<EquipItem>();
						//Thuc hien validate va import voi nhom du lieu duoc tac
						for (int d = 0, sized = lstGroup.size(); d < sized; d++) {
							totalItem++;
							value = null;
							message = "";
							donGiaVatTuTu = null;
							donGiaVatTuDen = null;
							donGiaNhanCongTu = null;
							donGiaNhanCongDen = null;
							fromCapacity = null;
							toCapacity = null;

							List<String> row = lstGroup.get(d);
							//Kiem tra tinh hop le cua Dung tich (tu, den)
							if (d == 0) {
								if (StringUtil.isRowValidSingleIndexNotNull(row, 0, 1)) {//Kiem tra voi 2 cot dau
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.from.to.ton.tai.mot.trong.hai") + "\n";
								} else {
									/**
									 * Kiem tra tinh hop le
									 * 
									 * Dung tich TO de trong hoac phai lon hon
									 * Dung tich FROM
									 * */
									//Cot: Dung tich tu
									value = row.get(0);
									msg = "";
									if (!StringUtil.isNullOrEmpty(value)) {
										msg = ValidateUtil.validateField(value, "equip.item.config.capacity.from", 10, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
										if (!StringUtil.isNullOrEmpty(msg)) {
											message += msg;
										} else {
											fromCapacity = new Integer(value);
											if (fromCapacity <= 0) {
												fromCapacity = null;
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.from.is.Int") + "\n";
											}
										}
									}
									//Cot: Dung tich den
									value = row.get(1);
									msg = "";
									if (!StringUtil.isNullOrEmpty(value)) {
										msg = ValidateUtil.validateField(value, "equip.item.config.capacity.to", 10, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
										if (!StringUtil.isNullOrEmpty(msg)) {
											message += msg;
										} else {
											toCapacity = new Integer(value);
											if (toCapacity <= 0) {
												toCapacity = null;
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.to.is.Int") + "\n";
											}
										}
									}
									/**
									 * @description Dung tich den phai lon hon
									 *              dung tich tu
									 */
									if (toCapacity != null && fromCapacity != null && toCapacity < fromCapacity) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.dung.tich.den.phai.lon.hon.dung.tich.tu") + "\n";
									}
									if (StringUtil.isNullOrEmpty(message)) {
										/**
										 * Neu Hang muc da duoc thiet lap thi se
										 * cap nhat lai Nguoc lai, se lam thao
										 * tac them moi
										 * */
										equipItemConfig = new EquipItemConfig();
										equipItemConfig = equipmentManagerMgr.getEquipItemConfigByFromAndToCapacity(fromCapacity, toCapacity, ActiveType.RUNNING.getValue());
										if (equipItemConfig == null) {
											/**
											 * check dung tich tu va dung tich
											 * den
											 * 
											 * @description Khong trung chom
											 *              khoang doan da dinh
											 *              nghia dung tich
											 *              truoc do con hoat
											 *              dong
											 */
											if (equipmentManagerMgr.countIntersectionCapacity(fromCapacity, toCapacity, ActiveType.RUNNING.getValue(), null) > 0) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.check.trung.chom.dung.tich.tu.den") + "\n";
											}
										} else if (equipItemConfig.getStatus() == null || !ActiveType.RUNNING.getValue().equals(equipItemConfig.getStatus().getValue())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.error.running.undefined") + "\n";
										}
									}
								}
							} else {
								if (!StringUtil.isRowValidSingleIndexNotNull(row, 0, 1)) {//Kiem tra voi 2 cot dau
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.capacity.from.to.two.isnull.undefined") + "\n";
								}
							}
							//Cot: Ma hang muc
							value = row.get(2);
							msg = ValidateUtil.validateField(value, "equip.item.config.item.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								value = value.trim().toUpperCase();
								equipItem = equipmentManagerMgr.getEquipItemByCodeEx(value, null);
								if (equipItem == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "equip.item.config.item.code");
								} else if (!ActiveType.RUNNING.getValue().equals(equipItem.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equip.item.config.item.code");
								} else {
									if (lstEquipItem != null && !lstEquipItem.isEmpty()) {
										for (int k = 0; k < lstEquipItem.size(); k++) {
											if (equipItem.getCode().equals(lstEquipItem.get(k).getCode())) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.item.code.khong.trung.trong.mot.khai.bao.dinh.muc");
												break;
											}
										}
									}
								}
							}
							//Cot: Don gia vat tu FROM
							value = row.get(3);
							if (!StringUtil.isNullOrEmpty(value.trim())) {
								msg = ValidateUtil.validateField(value, "equip.item.config.from.material.price", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									donGiaVatTuTu = new Double(value.trim());
									if (donGiaVatTuTu <= 0) {
										donGiaVatTuTu = null;
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.from.material.price.is.Int") + "\n";
									} else if (donGiaVatTuTu > Double.parseDouble("99999999999999999")) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.price.maxlength", Configuration
												.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.from.material.price"))
												+ "\n";
									}
								}
							}
							//Cot: Don gia vat tu TO
							value = row.get(4);
							if (!StringUtil.isNullOrEmpty(value.trim())) {
								msg = ValidateUtil.validateField(value, "equip.item.config.to.material.price", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									donGiaVatTuDen = new Double(value.trim());
									if (donGiaVatTuDen <= 0) {
										donGiaVatTuDen = null;
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.to.material.price.is.Int") + "\n";
									} else if (donGiaVatTuDen > Double.parseDouble("99999999999999999")) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.price.maxlength", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.to.material.price"))
												+ "\n";
									}
								}
							}
							/**
							 * Don gia vat tu tu va den bat buoc it nhat 1
							 * truong khong duoc trong
							 */
							if (donGiaVatTuTu == null && donGiaVatTuDen == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.material.from.to.ton.tai.mot.trong.hai") + "\n";
							}
							/**
							 * Don gia vat tu tu va den bat buoc it nhat 1
							 * truong khong duoc trong
							 */
							if (donGiaVatTuTu != null && donGiaVatTuDen != null && donGiaVatTuDen < donGiaVatTuTu) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.material.den.phai.lon.hon.tu") + "\n";
							}

							//Cot: Don gia nhan cong FROM
							value = row.get(5);
							if (!StringUtil.isNullOrEmpty(value.trim())) {
								msg = ValidateUtil.validateField(value, "equip.item.config.from.worker.price", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									donGiaNhanCongTu = new Double(value.trim());
									if (donGiaNhanCongTu <= 0) {
										donGiaNhanCongTu = null;
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.from.worker.price.is.Int") + "\n";
									} else if (donGiaNhanCongTu > Double.parseDouble("99999999999999999")) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.price.maxlength", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.from.worker.price"))
												+ "\n";
									}
								}
							}
							//Cot: Don gia nhan cong TO
							value = row.get(6);
							if (!StringUtil.isNullOrEmpty(value.trim())) {
								msg = ValidateUtil.validateField(value, "equip.item.config.to.worker.price", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									donGiaNhanCongDen = new Double(value.trim());
									if (donGiaNhanCongDen <= 0) {
										donGiaNhanCongDen = null;
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.to.worker.price.is.Int") + "\n";
									} else if (donGiaNhanCongDen > Double.parseDouble("99999999999999999")) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.price.maxlength", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.to.worker.price"))
												+ "\n";
									}
								}
							}
							
							/**
							 * Don gia nhan cong tu nho hon hoac bang Den
							 */
							if (donGiaNhanCongTu == null && donGiaNhanCongDen == null) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.worker.from.to.ton.tai.mot.trong.hai") + "\n";
							}
							/**
							 * Don gia nhan cong tu nho hon hoac bang Den
							 */
							if (donGiaNhanCongTu != null && donGiaNhanCongDen != null && donGiaNhanCongDen < donGiaNhanCongTu) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.worker.den.phai.lon.hon.tu") + "\n";
							}

							//Kiem tra tih hop le tren tung dong
							if (!StringUtil.isNullOrEmpty(message)) {
								//Thuc hien add vao danh sach loi
								lstFailsInItem.add(StringUtil.addFailBean(row, message));
							} else if (lstFailsInItem != null && lstFailsInItem.size() > 0) {
								lstFailsInItem.add(StringUtil.addFailBean(row, ""));
							} else {
								if (equipItemConfig != null && equipItemConfig.getId() != null) {
									equipItemConfig.setUpdateDate(nowDate);
									equipItemConfig.setUpdateUser(username);
								} else if (equipItemConfig == null) {
									equipItemConfig = new EquipItemConfig();
									equipItemConfig.setFromCapacity(fromCapacity);
									equipItemConfig.setToCapacity(toCapacity);
									equipItemConfig.setStatus(ActiveType.RUNNING);
									equipItemConfig.setCreateDate(nowDate);
									equipItemConfig.setCreateUser(username);
								}
								equipItemConfigDtl = new EquipItemConfigDtl();
								if (donGiaVatTuTu != null) {
									equipItemConfigDtl.setFromMaterialPrice(BigDecimal.valueOf(donGiaVatTuTu));
								}
								if (donGiaVatTuDen != null) {
									equipItemConfigDtl.setToMaterialPrice(BigDecimal.valueOf(donGiaVatTuDen));
								}
								if (donGiaNhanCongTu != null) {
									equipItemConfigDtl.setFromWorkerPrice(BigDecimal.valueOf(donGiaNhanCongTu));
								}
								if (donGiaNhanCongDen != null) {
									equipItemConfigDtl.setToWorkerPrice(BigDecimal.valueOf(donGiaNhanCongDen));
								}
								equipItemConfigDtl.setEquipItem(equipItem);
								equipItemConfigDtl.setStatus(ActiveType.RUNNING);
								equipItemConfigDtl.setCreateDate(nowDate);
								equipItemConfigDtl.setCreateUser(username);
								lstEquipItem.add(equipItem);
								lstEquipItemConfigDtl.add(equipItemConfigDtl);
							}
						}
						//Thuc hien kiem tra va Xu ly Group
						if (lstFailsInItem != null && lstFailsInItem.size() > 0) {
							if (flagFirt > 0) {
								lstFails.add(new CellBean());
							}
							numFail = numFail + lstFailsInItem.size();
							lstFails.addAll(lstFailsInItem);
							lstFailsInItem = new ArrayList<CellBean>();
							lstEquipItemConfigDtl = new ArrayList<EquipItemConfigDtl>();
						} else {
							numFinish = numFinish + lstGroup.size();
							//Them moi hoac Cap nhat
							equipmentManagerMgr.createOrUpdateEquipItemConfigByExcel(equipItemConfig, lstEquipItemConfigDtl);
							lstFailsInItem = new ArrayList<CellBean>();
							lstEquipItemConfigDtl = new ArrayList<EquipItemConfigDtl>();
						}
					}
				}
				if (totalItem > 0) {
					getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_EQUIP_ITEM_CONFIG_FAIL);
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.item.config.import.row.null.two");
				}
				if (numFinish == 0) {
					numFail = totalItem;
				}
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				isError = false;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			return SUCCESS;
		}
		return SUCCESS;
	}

	/**
	 * Xuat excel thiet lap dinh muc hang muc
	 * 
	 * @author hunglm16
	 * @since May 05,2015
	 * @description Ra soat va hotfix
	 * */
	public String exportBySearchEquipItemConfig() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			EquipItemConfigFilter filter = new EquipItemConfigFilter();
			if (fromCapacity != null) {
				filter.setFromCapacity(fromCapacity);
			}
			if (toCapacity != null) {
				filter.setToCapacity(toCapacity);
			}
			if (status != null && !ActiveType.DELETED.getValue().equals(status)) {
				filter.setStatus(status);
			}

			List<EquipItemConfigVO> lstData = equipmentManagerMgr.exportListEquipItemConfigByFilter(filter);

			if (lstData != null && !lstData.isEmpty()) {
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstData", lstData);

				String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
				String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_ITEM_CONFIG_EXPORT_NEW;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = ConstantManager.EQUIP_ITEMCONFIG_EXPORT_EXCEL + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				result.put(ERROR, false);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put("path", outputPath);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.export.isnull"));
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Khai bao cac phuong thuc getter/ setter
	 * 
	 * */

	private Long id;
	private EquipItemConfigVO equipItemConfigVO;

	private String equipItemConfigCode;
	private String capacity;
	private String statusStr;
	private String equipItemCode;
	private String equipItemName;
	private String excelFileContentType;

	private Integer status;
	private Integer fromCapacity;
	private Integer toCapacity;
	private Integer fromMaterialPrice;
	private Integer toMaterialPrice;
	private Integer fromWorkerPrice;
	private Integer toWorkerPrice;

	private File excelFile;

	List<EquipItemConfigDtlVO> lstEquipItemConfDtl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getFromCapacity() {
		return fromCapacity;
	}

	public void setFromCapacity(Integer fromCapacity) {
		this.fromCapacity = fromCapacity;
	}

	public Integer getToCapacity() {
		return toCapacity;
	}

	public void setToCapacity(Integer toCapacity) {
		this.toCapacity = toCapacity;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public Integer getFromMaterialPrice() {
		return fromMaterialPrice;
	}

	public void setFromMaterialPrice(Integer fromMaterialPrice) {
		this.fromMaterialPrice = fromMaterialPrice;
	}

	public Integer getToMaterialPrice() {
		return toMaterialPrice;
	}

	public void setToMaterialPrice(Integer toMaterialPrice) {
		this.toMaterialPrice = toMaterialPrice;
	}

	public Integer getFromWorkerPrice() {
		return fromWorkerPrice;
	}

	public void setFromWorkerPrice(Integer fromWorkerPrice) {
		this.fromWorkerPrice = fromWorkerPrice;
	}

	public Integer getToWorkerPrice() {
		return toWorkerPrice;
	}

	public void setToWorkerPrice(Integer toWorkerPrice) {
		this.toWorkerPrice = toWorkerPrice;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getEquipItemConfigCode() {
		return equipItemConfigCode;
	}

	public void setEquipItemConfigCode(String equipItemConfigCode) {
		this.equipItemConfigCode = equipItemConfigCode;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getEquipItemCode() {
		return equipItemCode;
	}

	public void setEquipItemCode(String equipItemCode) {
		this.equipItemCode = equipItemCode;
	}

	public String getEquipItemName() {
		return equipItemName;
	}

	public void setEquipItemName(String equipItemName) {
		this.equipItemName = equipItemName;
	}

	public EquipItemConfigVO getEquipItemConfigVO() {
		return equipItemConfigVO;
	}

	public void setEquipItemConfigVO(EquipItemConfigVO equipItemConfigVO) {
		this.equipItemConfigVO = equipItemConfigVO;
	}

	public List<EquipItemConfigDtlVO> getLstEquipItemConfDtl() {
		return lstEquipItemConfDtl;
	}

	public void setLstEquipItemConfDtl(List<EquipItemConfigDtlVO> lstEquipItemConfDtl) {
		this.lstEquipItemConfDtl = lstEquipItemConfDtl;
	}
}

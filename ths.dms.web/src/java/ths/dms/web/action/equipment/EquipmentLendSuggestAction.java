package ths.dms.web.action.equipment;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.vo.EquipmentLendSuggestVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.utils.LogUtility;

public class EquipmentLendSuggestAction extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CustomerMgr customerMgr;
	
	
	
	@Override
	public void prepare() throws Exception {
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
//		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}

		if (shop != null) {
			shopId = shop.getId();
		}
		return SUCCESS;
	}

//	public String search(){
//		result.put("page", page);
//		result.put("max", max);
//		try{
//		List<EquipmentLendSuggestVO> ls = new ArrayList<EquipmentLendSuggestVO>();
//		
//		EquipmentLendSuggestVO vo1 = new EquipmentLendSuggestVO();
//		vo1.setDonVi("TL40041 - Tài Lộc");
//		vo1.setGiamSat("001 - Nguyễn Quang Diệu");
//		vo1.setMaBienBan("NB001");
//		vo1.setNgay("01/01/2014");
//		vo1.setTrangThaiBienBan("Dự thảo");
//		vo1.setTrangThaiGiaoNhan("Chưa xuất");
//		ls.add(vo1);
//		
//		EquipmentLendSuggestVO vo2 = new EquipmentLendSuggestVO();
//		vo2.setDonVi("TL40041 - Tài Lộc");
//		vo2.setGiamSat("001 - Nguyễn Quang Diệu");
//		vo2.setMaBienBan("NB002");
//		vo2.setNgay("01/01/2014");
//		vo2.setTrangThaiBienBan("Đã duyệt");
//		vo2.setTrangThaiGiaoNhan("Chưa xuất");
//		ls.add(vo2);
//		
//		EquipmentLendSuggestVO vo3 = new EquipmentLendSuggestVO();
//		vo3.setDonVi("TL40041 - Tài Lộc");
//		vo3.setGiamSat("001 - Nguyễn Quang Diệu");
//		vo3.setMaBienBan("NB003");
//		vo3.setNgay("01/01/2014");
//		vo3.setTrangThaiBienBan("Đã hủy");
//		vo3.setTrangThaiGiaoNhan("Chưa xuất");
//		ls.add(vo3);
//		
//		if (ls != null) {
//			result.put("rows", ls);
//		}
//		}catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
//		return JSON;
//	}
	
}

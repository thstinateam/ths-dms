package ths.dms.web.action.equipment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ApParamEquipMgr;
import ths.dms.core.business.EquipStockAdjustFormMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipStockAdjustForm;
import ths.dms.core.entities.EquipStockAdjustFormDtl;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.filter.EquipStockAdjustFormFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipStockAdjustFormDtlVO;
import ths.dms.core.entities.vo.EquipStockAdjustFormRecord;
import ths.dms.core.entities.vo.EquipStockAdjustFormVO;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;
/**
 * @author Datpv4
 * @since July 02,2015
 * @description class action quan ly cap nhat kho dieu chinh
 */
public class EquipStockAdjustFormAction extends AbstractAction {

	private static final String PAGING = "PAGING";
	private static final String PAGING_NO = "PAGING_NO";
	
	private static final long serialVersionUID = 1L;

	EquipStockAdjustFormMgr equipStockAdjustFormMgr;
	ApParamEquipMgr apParamEquipMgr;
	private String fromDate;
	private String toDate;
	private Integer statusAction;
	private List<Long> lstId = new ArrayList<Long>();
	private Long id;
	private Long equipStockAdjustFormId;	
	private Integer isEdit;
	private Long equipCategoryId;
	private Long equipGroupId;	
	private List<EquipCategory> lstEquipCategory = new ArrayList<EquipCategory>();
	private List<EquipProvider> lstEquipProvider = new ArrayList<EquipProvider>();
	private EquipStockAdjustForm equipStockAdjustForm; // phieu 
	private List<EquipStockAdjustFormDtlVO> equipStockAdjustFormDtlVO =new ArrayList<EquipStockAdjustFormDtlVO>();
	
	private String code;// Ma phieu
	private Integer status;
	private String description; // Ly do
	//Cap nhat danh sach thiet bi 
	private Integer rowsLength;
	private List<Long> 	 lstEquipStockAdjustDtlId = new ArrayList<Long>();
	private List<String> lstSerial = new ArrayList<String>();
	private List<String> lstHealthStatus = new ArrayList<String>();
	private List<Long> 	 lstEquipGroupId = new ArrayList<Long>();
	private List<String> lstEquipGroupName = new ArrayList<String>();
	private List<String> lstEquipCateGoryName = new ArrayList<String>();
	private List<String> lstCapacity = new ArrayList<String>();
	private List<Long> 	 lstEquipProviderId = new ArrayList<Long>();
	private List<String> lstEquipProviderName = new ArrayList<String>();
	private List<Integer> lstManufacturingYear = new ArrayList<Integer>();
	private List<String> lstWarrantyExpiredDate = new ArrayList<String>();
	private List<Long> 	 lstStockId = new ArrayList<Long>();
	private List<String> lstStockType = new ArrayList<String>();
	private List<String> lstStockCode = new ArrayList<String>();
	private List<String> lstStockName = new ArrayList<String>();
	private List<BigDecimal> lstPrice = new ArrayList<BigDecimal>();
	private Integer statusForm;
	//Quyen nguoi dung isCreate =1 quyen tao, isCreate=0 quyen dueyt
	private Integer isCreate;
	private String note;
	
	//Import 
	private File excelFile;
	private String excelFileContentType;

	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		equipStockAdjustFormMgr = (EquipStockAdjustFormMgr) context.getBean("equipStockAdjustFormMgr");
		apParamEquipMgr = (ApParamEquipMgr) context.getBean("apParamEquipMgr");
	}

	@Override
	public String execute() throws Exception {
		generateToken();
		Shop shop = null;
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		}
		if (shop != null) {
			shopId = shop.getId();
		}
		return SUCCESS;
	}
	
	/**
	 * @author Datpv4
	 * @since July 02,2015
	 * @description action tim kiem phieu cap nhat kho dieu chinh
	 */
	 
	public String searchEquipStockAdjust() {
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			} else {
				if (currentUser.getShopRoot() == null) {
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			}
			EquipStockAdjustFormFilter filter = this.getFilterEquipStockAdjustFormSeach(PAGING);
			filter.setShopId(currentUser.getShopRoot().getShopId());
			filter.setIsCreate(isCreate);
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			ObjectVO<EquipStockAdjustFormVO> temp = equipStockAdjustFormMgr.getListEquipStockAdjustFormByFilter(filter);
			if (temp != null && temp.getLstObject().size() > 0) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipRepairFormVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipStockAdjustFormAction.searchEquipStockAdjust" + e.getMessage());
		}
		return JSON;
	}
//	
//
	/**
	 * @author Datpv4
	 * @since July 02,2015
	 * @description action chuyen trang xem chi tiet phieu
	 */
	public String getChangeForm() {
		generateToken();
		try {
			if (id != null) {
				EquipStockAdjustFormFilter filter = new EquipStockAdjustFormFilter();
				filter.setId(id);
				EquipStockAdjustForm temp = equipStockAdjustFormMgr.getEquipStockAdjustFormById(id);
				if (temp != null ) {
					code = temp.getCode();
					status = temp.getStatus().getValue();
					description = temp.getDescription();
					note = temp.getNote();
				}
				
			}
			
			EquipmentFilter<EquipProvider> filter = new EquipmentFilter<EquipProvider>();
			filter.setStatus(StatusType.ACTIVE.getValue());
			lstEquipProvider = equipmentManagerMgr.getListEquipProviderByFilter(filter);
			result.put("lstEquipProvider", lstEquipProvider);
			EquipmentFilter<EquipmentVO> filterG = new EquipmentFilter<EquipmentVO>();
			filterG.setStatus(StatusType.ACTIVE.getValue());
			result.put("lstEquipGroup", equipmentManagerMgr.searchEquipmentGroupVObyFilter(filterG).getLstObject());
			result.put("lstHealthStatus", apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING));
		    //Lay kho
			
			EquipStockFilter stockFilter = new EquipStockFilter();
			stockFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			stockFilter.setShopRoot(currentUser.getShopRoot().getShopId());
			ObjectVO<EquipStockVO> objVO = equipmentManagerMgr.getListEquipStockVOByRole(stockFilter);
			result.put("lstStock", objVO.getLstObject());
			
		} catch (Exception e) {
			LogUtility.logError(e, "EquipStockAdjustFormAction.getChangeForm" + e.getMessage());
		}
		return SUCCESS;
	}
//
	/**
	 * @author Datpv4
	 * @since July 02,2015
	 * @description action chi tiet phieu
	 */
	public String searchEquipStockAdjustFormDtl() {
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipStockAdjustFormVO>());
			if (id != null) {
				equipStockAdjustForm = equipStockAdjustFormMgr.getEquipStockAdjustFormById(id);
				if (equipStockAdjustForm != null) {
					EquipStockAdjustFormFilter filter = new EquipStockAdjustFormFilter();
					filter.setId(id);
					filter.setShopId(currentUser.getShopRoot().getShopId());
					filter.setIsCreate(isCreate);
					/*KPaging<EquipStockAdjustFormDtlVO> kPaging = new KPaging<EquipStockAdjustFormDtlVO>();
					kPaging.setPageSize(max);
					kPaging.setPage(page - 1);
					filter.setkPagingDtl(kPaging);*/
					ObjectVO<EquipStockAdjustFormDtlVO> temp = equipStockAdjustFormMgr.getListEquipStockAdjustFormDtlFilter(filter);
					if (temp != null && temp.getLstObject() != null && temp.getLstObject().size() > 0) {
						result.put("total", temp.getLstObject().size());
						result.put("rows", temp.getLstObject());
						
					}
				} else {
					return PAGE_NOT_FOUND;
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipStockAdjustFormAction.searchEquipStockAdjustFormDtl" + e.getMessage());
		}
		return JSON;
	}
//	
//
	/**
	 * @author Datpv4
	 * @since July 03,2015
	 * @description them moi va cap nhat phieu
	 */
	public String updateStockAdjustRecord() {
		Date startLogDate = DateUtil.now();
		try {
			resetToken(result);
			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(currentUser.getUserName());

			final int IS_ERROR = 1;
			// Truong hop o man hinh chi tiet chuyen sang trang thai duyet
			if (status.equals(StatusRecordsEquip.APPROVED.getValue())) {
				EquipStockAdjustFormFilter filter = new EquipStockAdjustFormFilter();
				List<Long> listID = new ArrayList<Long>();
				listID.add(id);
				filter.setLstId(listID);
				filter.setStatus(status);
				equipStockAdjustFormMgr.updateStatus(filter, logInfo);
				result.put(ERROR, false);
				return JSON;
			}

			List<FormErrVO> lstErr = new ArrayList<FormErrVO>();
			EquipStockAdjustFormRecord record = new EquipStockAdjustFormRecord();
			record.setId(id);
			record.setStatus(StatusRecordsEquip.parseValue(status));
			record.setDescription(description);
			record.setNote(note);

			List<EquipStockAdjustFormDtl> lstRecord = new ArrayList<EquipStockAdjustFormDtl>();

			for (int i = 0; i < rowsLength; i++) {
				EquipStockAdjustFormDtl equipStockAdjustFormDtl = new EquipStockAdjustFormDtl();

				if (lstEquipStockAdjustDtlId.size() > i) {
					lstId.add(Long.valueOf(lstEquipStockAdjustDtlId.get(i)));
					equipStockAdjustFormDtl.setId(lstEquipStockAdjustDtlId.get(i));
				}

				equipStockAdjustFormDtl.setSerial(lstSerial.get(i));
				equipStockAdjustFormDtl.setHealthStatus(lstHealthStatus.get(i).trim());

				EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupById(lstEquipGroupId.get(i));
				if (equipGroup != null) {
					equipStockAdjustFormDtl.setEquipGroup(equipGroup);
					equipStockAdjustFormDtl.setEquipGroupName(equipGroup.getName());
				} else {
					FormErrVO errVO = new FormErrVO();
					errVO.setFormCode("" + i + 1);
					errVO.setRecordNotComplete(R.getResource("equip.stock.adjust.import.equipgroup.not.exits.err"));
					errVO.setFormStatusErr(IS_ERROR);
					lstErr.add(errVO);
				}
				equipStockAdjustFormDtl.setCapacityFrom(equipGroup.getCapacityFrom() == null ? null : equipGroup.getCapacityFrom());
				equipStockAdjustFormDtl.setCapacityTo(equipGroup.getCapacityTo() == null ? null : equipGroup.getCapacityTo());

				EquipProvider equipProvider = equipmentManagerMgr.getEquipProviderById(lstEquipProviderId.get(i));
				if (equipProvider != null) {
					equipStockAdjustFormDtl.setEquipProvider(equipProvider);
					equipStockAdjustFormDtl.setEquipProviderName(equipProvider.getName());
				} else {
					FormErrVO errVO = new FormErrVO();
					errVO.setFormCode("" + i + 1);
					errVO.setRecordNotComplete(R.getResource("equip.stock.adjust.import.equipprovider.not.exits.err"));
					errVO.setFormStatusErr(IS_ERROR);
					lstErr.add(errVO);
				}

				equipStockAdjustFormDtl.setManufacturingYear(lstManufacturingYear.get(i));
				String msg = ValidateUtil.validateField(lstWarrantyExpiredDate.get(i), "equip.stock.adjust.import.warrantyExpiredDate.format.error", null, ConstantManager.ERR_INVALID_DATE);
				if (StringUtil.isNullOrEmpty(msg)) {
					equipStockAdjustFormDtl.setWarrantyExpiredDate(DateUtil.parse(lstWarrantyExpiredDate.get(i), DateUtil.DATE_FORMAT_STR_PRO));
				} else {
					FormErrVO errVO = new FormErrVO();
					errVO.setFormCode("" + i + 1);
					errVO.setRecordNotComplete(msg);
					errVO.setFormStatusErr(IS_ERROR);
					lstErr.add(errVO);
				}

				if (lstStockCode.size() > i) {
					EquipStock equipStock = equipmentManagerMgr.getEquipStockbyCode(lstStockCode.get(i));
					if (equipStock != null) {
						equipStockAdjustFormDtl.setStockId(equipStock.getId());
						equipStockAdjustFormDtl.setStockType(1);
						equipStockAdjustFormDtl.setStockName(equipStock.getName());
					}
				} else {
					FormErrVO errVO = new FormErrVO();
					errVO.setFormCode("" + i + 1);
					errVO.setRecordNotComplete(R.getResource("equip.stock.adjust.import.stock.not.exist.err"));
					errVO.setFormStatusErr(IS_ERROR);
					lstErr.add(errVO);
				}

				if (lstPrice.size() > 0) {
					equipStockAdjustFormDtl.setPrice(lstPrice.get(i));
				}
				if (lstErr.size() >= 0) {
					lstRecord.add(equipStockAdjustFormDtl);
				} else {
					result.put(ERROR, false);
					result.put("lstErr", lstErr);
					return JSON;
				}
			}

			record.setEquipStockAdjustFormDtl(lstRecord);
			String updateMessage = equipStockAdjustFormMgr.createOrUpdateEquipStockAdjustForm(record, logInfo);

			if (StringUtil.isNullOrEmpty(updateMessage)) { // update success
				result.put(ERROR, false);
			}
			
			if (updateMessage != null && updateMessage.startsWith(OrderType.DCT.getValue())) {
				// truong hop nay ko loi: tao moi luu xuong ma phieu de view thong tin
				result.put(ERROR, false);
				String[] arrCodeAndId = updateMessage.split(";");
				result.put("equipStockAdjustCode", arrCodeAndId[0]); // ma phieu
				result.put("equipStockAdjustId", arrCodeAndId[1]); // id phieu
			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, updateMessage));
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipStockAdjustFormAction"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
//	
//
	/**
	 * @author Datpv4
	 * @since July 05,2015
	 * @description action cap nhat trang thai 
	 */
	public String updateStatus() {
		resetToken(result);		
		try {
			List<FormErrVO> lstErr = new ArrayList<FormErrVO>();
			StatusRecordsEquip status = null;
			
			if (statusForm == null || (statusForm != null && statusForm < ActiveType.DELETED.getValue())) {
				//errMsg = "Chưa có trạng thái nào được chọn";
				errMsg = R.getResource("equip.lost.recorde.err.chua.chon.trang.thai");
			} else {
				status = StatusRecordsEquip.parseValue(statusForm);
				if (lstId != null && lstId.size() > 0) {
					EquipRepairFilter filter = new EquipRepairFilter();
					filter.setLstId(lstId);
					List<EquipStockAdjustForm> lstEquipStockAdjustForms = equipStockAdjustFormMgr.getListEquipStockAdjustFormByListId(lstId);
					if (lstEquipStockAdjustForms != null && lstEquipStockAdjustForms.size() > 0) {
						
						if (StatusRecordsEquip.WAITING_APPROVAL.equals(status)) {
							// chon cho duyet: thi phai la Du thao, khong duyet
							for (EquipStockAdjustForm equipStockAdjustForm : lstEquipStockAdjustForms) {
								if (!StatusRecordsEquip.DRAFT.equals(equipStockAdjustForm.getStatus()) && !StatusRecordsEquip.NO_APPROVAL.equals(equipStockAdjustForm.getStatus())) {
									FormErrVO errVO = new FormErrVO();
									errVO.setFormCode(equipStockAdjustForm.getCode());
									errVO.setRecordNotComplete(R.getResource("equipment.stock.adjust.record.status.waiting.err"));
									errVO.setFormStatusErr(1);
									lstErr.add(errVO);
									lstId.remove(lstId.indexOf(equipStockAdjustForm.getId()));
						
								}
							}
						}
						
						if (StatusRecordsEquip.CANCELLATION.equals(status)) {
							// chon huy: thi phai la Du thao, khong duyet, cho duyet
							for (EquipStockAdjustForm equipStockAdjustForm : lstEquipStockAdjustForms) {
								if (!StatusRecordsEquip.DRAFT.equals(equipStockAdjustForm.getStatus()) && !StatusRecordsEquip.NO_APPROVAL.equals(equipStockAdjustForm.getStatus()) && !StatusRecordsEquip.WAITING_APPROVAL.equals(equipStockAdjustForm.getStatus())) {
									FormErrVO errVO = new FormErrVO();
									errVO.setFormCode(equipStockAdjustForm.getCode());
									errVO.setRecordNotComplete(R.getResource("equipment.stock.adjust.record.status.cancelation.err"));
									errVO.setFormStatusErr(1);
									lstErr.add(errVO);
									lstId.remove(lstId.indexOf(equipStockAdjustForm.getId()));
								}
							}
						}								
						if (StatusRecordsEquip.NO_APPROVAL.equals(status) || StatusRecordsEquip.APPROVED.equals(status)) {
							// chon khong duyet, duyet thi phai la cho duyet
							for (EquipStockAdjustForm equipStockAdjustForm : lstEquipStockAdjustForms) {
								if (!StatusRecordsEquip.WAITING_APPROVAL.equals(equipStockAdjustForm.getStatus())) {
									FormErrVO errVO = new FormErrVO();
									errVO.setFormCode(equipStockAdjustForm.getCode());
									errVO.setRecordNotComplete(R.getResource("equipment.stock.adjust.record.status.reject.or.approved.err"));
									errVO.setFormStatusErr(1);
									lstErr.add(errVO);
									lstId.remove(lstId.indexOf(equipStockAdjustForm.getId()));
								}
							}
						}
					} 
				} else {
					errMsg = R.getResource("equipment.stock.adjust.record.choose");
				}
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
			// xu ly cap nhat cac id trong list OK, con co lstErr thi view len popup
			EquipStockAdjustFormFilter filter = new EquipStockAdjustFormFilter();
			filter.setLstId(lstId);
			filter.setStatus(statusForm);
			equipStockAdjustFormMgr.updateStatus(filter, getLogInfoVO());
			
			result.put(ERROR, false);
			result.put("lstErr", lstErr);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return JSON;
	}
//	
	private EquipStockAdjustFormFilter getFilterEquipStockAdjustFormSeach (String paging) {
		EquipStockAdjustFormFilter filter = new EquipStockAdjustFormFilter();
		try {
			if (PAGING.equals(paging)) {
				KPaging<EquipStockAdjustFormVO> kPaging = new KPaging<EquipStockAdjustFormVO>();
				
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				filter.setkPaging(kPaging);
			}
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_STR_PRO));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_STR_PRO));
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				code = code.trim();
				filter.setCode(code);
			}
			if (status != null && status > -1) {
				filter.setStatus(status);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Seach Filter"+ e.getMessage());
		}
		return filter;
	}
//	
//
	/**
	 * @author Datpv4
	 * @since July 05,2015
	 * @description action export
	 */
	public String exportEquipStockAdjust(){
		try {
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			EquipStockAdjustFormFilter filter = this.getFilterEquipStockAdjustFormSeach(PAGING_NO);
			
			filter.setIsCreate(isCreate);
			filter.setLstId(lstId);
			filter.setShopId(currentUser.getShopRoot().getShopId());
			ObjectVO<EquipStockAdjustFormDtlVO> temp = equipStockAdjustFormMgr.getListEquipStockAdjustFormDtlFilter(filter);
			if (temp != null && temp.getLstObject().size() > 0) {
				SXSSFWorkbook workbook = null;
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao shett
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("TB");
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

				//Set Getting Defaul
//				sheetData.setDefaultRowHeight((short) (15 * 25));
//				sheetData.setDefaultColumnWidth(13);
				//Size Row
				mySheet.setColumnsWidth(sheetData, 0, 175, 175, 175, 175, 175, 175, 100, 300, 200, 200, 500);
				sheetData.setDisplayGridlines(false);
				int row=1;
				String[] lstHeader = R.getResource("equipment.stock.adjust.record.export.header").split(";");
				int column = 0;
				for(String header : lstHeader){
					mySheet.addCell(sheetData, column, row,header, style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					column++;
				}

				row =row+1;
				List<EquipStockAdjustFormDtlVO> listRecord = temp.getLstObject();
				for(EquipStockAdjustFormDtlVO  record : listRecord){
					mySheet.addCell(sheetData, 0, row, record.getCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 1, row, record.getEquipCode(),style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 2, row, record.getSerial(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 3, row, record.getEquipGroupCode()+"-"+record.getEquipGroupName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 4, row, record.getEquipProviderCode()+"-"+record.getEquipProviderName(),style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 5, row, record.getPrice(),style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					mySheet.addCell(sheetData, 6, row, record.getManufacturingYear(),style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					mySheet.addCell(sheetData, 7, row,record.getHealthStatus() +"-"+ record.getHealthStatusStr(),style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 8, row, record.getStockCode()+"-"+record.getStockName(),style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 9, row, record.getWarrantyExpiredDate(),style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					mySheet.addCell(sheetData, 10, row, record.getNote(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					row++;
				}
				
				FileOutputStream out = null;
				String outputName = "TB" + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
				String exportFileName = Configuration.getStoreRealPath() + outputName;	
				out = new FileOutputStream(exportFileName);
				workbook.write(out);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(ERROR, false);
				result.put(REPORT_PATH, outputPath);
				result.put("hasData", true);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else{
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
		 }catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
//
	/**
	 * @author Datpv4
	 * @since July 06,2015
	 * @description action download file template
	 */
	public String downloadTemplateExcel(){
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			EquipmentFilter<EquipProvider> filterEP = new EquipmentFilter<EquipProvider>();
			filterEP.setStatus(StatusType.ACTIVE.getValue());
			lstEquipProvider = equipmentManagerMgr.getListEquipProviderByFilter(filterEP);
			EquipmentFilter<EquipmentVO> filterG = new EquipmentFilter<EquipmentVO>();
			filterG.setStatus(StatusType.ACTIVE.getValue());
			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("listProvider", lstEquipProvider);
			beans.put("listEquipGroup", equipmentManagerMgr.searchEquipmentGroupVObyFilter(filterG).getLstObject());
			beans.put("listHealthStatus", apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING));
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
			String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_STOCK_ADJUST_FORM;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_Bieu_Mau_import_cap_nhat_kho_dieu_chinh" + ConstantManager.EXPORT_FILE_EXTENSION_EX;
			String exportFileName = (Configuration.getStoreImportRealPath() + outputName).replace('/', File.separatorChar);
			
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			
			result.put(ERROR, false);
			String outputPath = Configuration.getStoreImportEquipRealPath() + outputName;
			result.put("path", outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
//	
	/**
	 * Kiem tra dong bien ban import nhap kho dieu chinh
	 * @author tamvnm
	 * @param row
	 * @return true: la dong bien ban - false: dong  chi tiet
	 * @since Oct 09 2015
	 */
	private boolean isANewRecord(List<String> row) {
		return !StringUtil.isNullOrEmpty(row.get(8));
	}
//	
	/**
	 * @author Datpv4
	 * @since July 08,2015
	 * @description action import excel
	 */
	public String importExcel() {
		isError = true;
		totalItem = 0;
		int maxCol = 9;
		try {
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
//				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//				result.put(ERROR, true);
//				result.put("errMsg", errMsg);
//				return SUCCESS;
//			}

			EquipStockAdjustFormRecord record = new EquipStockAdjustFormRecord();
			record.setStatus(StatusRecordsEquip.DRAFT);//Du thao
			
			List<EquipStockAdjustFormDtl> lstStockAdjustFormDtl = new ArrayList<EquipStockAdjustFormDtl>();		
			
			/////Xu ly nghiep vu import file
			lstView = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, maxCol);
			
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(currentUser.getUserName());
				String messageImport = "";
				String msgErrorCell = "";
				String value = "";
				boolean flag = false;
				int rowSize = 0;
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					totalItem++;
					messageImport = "";
					 msgErrorCell = "";
					 value = "";
					EquipStockAdjustFormDtl equipStockAdjustFormDtl = new EquipStockAdjustFormDtl();
					List<String> row = lstData.get(i);
					rowSize = row.size();
					if (i > 0 && isANewRecord(row)) {
						messageImport += R.getResource("equip.stock.adjust.import.record.invalid") +"\n";
						lstView.add(StringUtil.addFailBean(row, messageImport));
						continue;
					}
					
					if (rowSize > 0) {
						//So seri 
						value = row.get(0);					
						msgErrorCell = ValidateUtil.validateField(value,"equip.stock.adjust.import.serial.maxlength.or.fotmat.errr", 100, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(msgErrorCell)) {
							equipStockAdjustFormDtl.setSerial(value.trim());
						} else {
							messageImport += msgErrorCell +"\n";
						}
					}
					
					if (rowSize > 1) {
					//Nhom thiet bi
						value = row.get(1);
						if (!StringUtil.isNullOrEmpty(value)) {
							String[] valueStr = value.split("-");
							EquipGroup equipGroup = equipmentManagerMgr.getEquipGroupByCode(valueStr[0].trim());
							if(equipGroup!=null){
								if (!ActiveType.RUNNING.equals(equipGroup.getStatus())) {
									messageImport += R.getResource("equip.group.product.stop") +"\n";
								} else {
									equipStockAdjustFormDtl.setCapacityFrom(equipGroup.getCapacityFrom());
									equipStockAdjustFormDtl.setCapacityTo(equipGroup.getCapacityTo());
									equipStockAdjustFormDtl.setEquipGroup(equipGroup);
									equipStockAdjustFormDtl.setEquipGroupName(equipGroup.getName());
								}
							} else {
								//bao loi ko co equip group
								messageImport += R.getResource("equip.stock.adjust.import.equipgroup.not.exits.err") +"\n";
							}
							
						} else {
							messageImport += R.getResource("equip.stock.adjust.import.equipgroup.empty.err") +"\n";
						}
					}
					if (rowSize > 2) {
					//NCC
						value = row.get(2);
						if (!StringUtil.isNullOrEmpty(value)) {
							String[] valueStr = value.split("-");
							EquipProvider equipProvider = equipmentManagerMgr.getEquipProviderByCode(valueStr[0].trim());
							if(equipProvider!=null){
								if (!ActiveType.RUNNING.equals(equipProvider.getStatus())) {
									messageImport = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.upadte.err.equipprovider.running") + "\n";
								} else {
									equipStockAdjustFormDtl.setEquipProvider(equipProvider);
									equipStockAdjustFormDtl.setEquipProviderName(equipProvider.getName());
								}
							} else {
								//bao loi ko co equip provider
								messageImport += R.getResource("equip.stock.adjust.import.equipprovider.not.exits.err") + "\n";
							}
						} else {
						  //Khong dc de trong	
							messageImport += R.getResource("equip.stock.adjust.import.equiprovider.empty.errr") +"\n";
						} 
					}
					
					if (rowSize > 3) {
						//Nguyen gia
						value = row.get(3);
						if (!StringUtil.isNullOrEmpty(value)) {
							msgErrorCell = ValidateUtil.validateField(value, "equip.stock.adjust.import.price.maxlengh.or.format.err", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msgErrorCell)) {
								messageImport += msgErrorCell;
							} else{
								equipStockAdjustFormDtl.setPrice(BigDecimal.valueOf(Long.valueOf(value.trim())));
							}
	
						} else {
							messageImport += R.getResource("equip.stock.adjust.import.price.empty.err")  +"\n";
						}
					}
					
					if (rowSize > 4) {
						//Nam san xuat
						value = row.get(4);
						if (!StringUtil.isNullOrEmpty(value)) {
							msgErrorCell = ValidateUtil.validateField(value, "equip.stock.adjust.import.manufacturingYear.maxlength.or.format.err", 4, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msgErrorCell)) {
								messageImport += msgErrorCell;
							} else {
								int yearNow = DateUtil.getYear(DateUtil.now());
								Integer manufacturingYear = Integer.valueOf(value.trim());
								if(manufacturingYear.compareTo(yearNow) > 0) {
									messageImport += R.getResource("equipment.upadte.err.manufacturingYear")  +"\n";
								}
								equipStockAdjustFormDtl.setManufacturingYear(Integer.valueOf(value.trim()));
							}
						} else {
							messageImport += R.getResource("equip.stock.adjust.import.manufacturingYear.empty.err")  +"\n";
						}
					}
					if (rowSize > 5) {
						//trang thai thiet bi
						value = row.get(5);
						String[] valueStr = null;
						if (!StringUtil.isNullOrEmpty(value)) {
							valueStr = value.split("-");
							List<ApParam> listApParam = apParamMgr.getListApParam(ApParamType.EQUIP_CONDITION, ActiveType.RUNNING);
							flag = false;
							if(listApParam!=null && listApParam.size()>0){
								for(ApParam apParam : listApParam){
									if(valueStr[0].trim().equals(apParam.getApParamCode().trim())){
										flag = true;
									}
								}
							} else {
								messageImport += R.getResource("equip.stock.adjust.import.healthStatus.empty.err")  +"\n";
							}
						}
						if(flag && valueStr!=null){
							equipStockAdjustFormDtl.setHealthStatus(valueStr[0]);
						} else{
							messageImport += R.getResource("equip.stock.adjust.import.healthStatus.not.exits.err")  +"\n";
						}
					}
						
					if (rowSize > 6) {
						//kho
						value = row.get(6);
						if (!StringUtil.isNullOrEmpty(value)) {
							EquipStockFilter filter = new EquipStockFilter();
							filter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
							filter.setShopRoot(currentUser.getShopRoot().getShopId());
							filter.setEquipStockCode(value.trim());
							ObjectVO<EquipStockVO> objVO = equipmentManagerMgr.getListEquipStockVOByRole(filter);
							if(objVO!=null && objVO.getLstObject()!=null && objVO.getLstObject().size()>0){
								equipStockAdjustFormDtl.setStockId(objVO.getLstObject().get(0).getEquipStockId());
								equipStockAdjustFormDtl.setStockName(objVO.getLstObject().get(0).getEquipStockName());
								equipStockAdjustFormDtl.setStockType(1);//Ma don vi luon la 1
							} else {
								//bao loi ko ton tai hoac khong dc phan quyen 
								messageImport += R.getResource("equip.stock.adjust.import.stock.not.exist.err")  +"\n";
							}
						} else {
						  //Khong dc de trong	
							messageImport += R.getResource("equip.stock.adjust.import.stock.empty.err")  +"\n";
						}
					}
					if (rowSize > 7) {
						//ngay het han bao hanh
						value = row.get(7);
						if (!StringUtil.isNullOrEmpty(value)) {
							msgErrorCell = ValidateUtil.validateField(value,"equip.stock.adjust.import.warrantyExpiredDate.format.error", null, ConstantManager.ERR_INVALID_DATE);
							if (!StringUtil.isNullOrEmpty(msgErrorCell)) {
								messageImport += msgErrorCell;
							} else {
								equipStockAdjustFormDtl.setWarrantyExpiredDate(DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_STR_PRO));
								if (equipStockAdjustFormDtl.getManufacturingYear() != null) {
									int year = DateUtil.getYear(equipStockAdjustFormDtl.getWarrantyExpiredDate());
									if (year < equipStockAdjustFormDtl.getManufacturingYear()) {
										messageImport += R.getResource("equip.lst.equip.manufacturingYear.warrantyExpiredDate.err");
									}
								}
//								if(DateUtil.compareDateWithoutTime(date1, equipStockAdjustFormDtl.getWarrantyExpiredDate()))
							}
						}
					}
					// ** Ghi chu */
					if (rowSize > 8) {
						value = row.get(8);
						msgErrorCell = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
						if (!StringUtil.isNullOrEmpty(msgErrorCell)) {
							messageImport += msgErrorCell;
						} else if (i == 0) {
							record.setNote(value);
						}
					}
				//XU LY CAP NHAT VAO DB
				if (StringUtil.isNullOrEmpty(messageImport)) {
					lstStockAdjustFormDtl.add(equipStockAdjustFormDtl);
					
				} else {
					lstView.add(StringUtil.addFailBean(row, messageImport));
				}
				
			}
				
			if (lstStockAdjustFormDtl.size()>0 && lstStockAdjustFormDtl!=null) {
				record.setEquipStockAdjustFormDtl(lstStockAdjustFormDtl);
				equipStockAdjustFormMgr.createOrUpdateEquipStockAdjustForm(record,logInfo);
			}
			//Export error
			getOutputFailExcelFile(lstView, ConstantManager.TEMPLATE_EQUIP_STOCK_ADJUST_FORM);

			} else {
				isError = true;
				errMsg = R.getResource("import.error.not.data");
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
//	
//	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getStatusAction() {
		return statusAction;
	}

	public void setStatusAction(Integer statusAction) {
		this.statusAction = statusAction;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEquipStockAdjustFormId() {
		return equipStockAdjustFormId;
	}

	public void setEquipStockAdjustFormId(Long equipStockAdjustFormId) {
		this.equipStockAdjustFormId = equipStockAdjustFormId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}

	public Long getEquipCategoryId() {
		return equipCategoryId;
	}

	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public List<EquipCategory> getLstEquipCategory() {
		return lstEquipCategory;
	}

	public void setLstEquipCategory(List<EquipCategory> lstEquipCategory) {
		this.lstEquipCategory = lstEquipCategory;
	}

	public List<EquipProvider> getLstEquipProvider() {
		return lstEquipProvider;
	}

	public void setLstEquipProvider(List<EquipProvider> lstEquipProvider) {
		this.lstEquipProvider = lstEquipProvider;
	}

	public EquipStockAdjustForm getEquipStockAdjustForm() {
		return equipStockAdjustForm;
	}

	public void setEquipStockAdjustForm(EquipStockAdjustForm equipStockAdjustForm) {
		this.equipStockAdjustForm = equipStockAdjustForm;
	}

	public List<EquipStockAdjustFormDtlVO> getEquipStockAdjustFormDtlVO() {
		return equipStockAdjustFormDtlVO;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Long> getLstEquipStockAdjustDtlId() {
		return lstEquipStockAdjustDtlId;
	}

	public void setLstEquipStockAdjustDtlId(List<Long> lstEquipStockAdjustDtlId) {
		this.lstEquipStockAdjustDtlId = lstEquipStockAdjustDtlId;
	}

	public List<String> getLstSerial() {
		return lstSerial;
	}

	public void setLstSerial(List<String> lstSerial) {
		this.lstSerial = lstSerial;
	}

	public List<String> getLstHealthStatus() {
		return lstHealthStatus;
	}

	public void setLstHealthStatus(List<String> lstHealthStatus) {
		this.lstHealthStatus = lstHealthStatus;
	}

	public List<Long> getLstEquipGroupId() {
		return lstEquipGroupId;
	}

	public void setLstEquipGroupId(List<Long> lstEquipGroupId) {
		this.lstEquipGroupId = lstEquipGroupId;
	}

	public List<String> getLstEquipGroupName() {
		return lstEquipGroupName;
	}

	public void setLstEquipGroupName(List<String> lstEquipGroupName) {
		this.lstEquipGroupName = lstEquipGroupName;
	}

	public List<String> getLstEquipCateGoryName() {
		return lstEquipCateGoryName;
	}

	public void setLstEquipCateGoryName(List<String> lstEquipCateGoryName) {
		this.lstEquipCateGoryName = lstEquipCateGoryName;
	}

	public List<String> getLstCapacity() {
		return lstCapacity;
	}

	public void setLstCapacity(List<String> lstCapacity) {
		this.lstCapacity = lstCapacity;
	}

	public List<Long> getLstEquipProviderId() {
		return lstEquipProviderId;
	}

	public void setLstEquipProviderId(List<Long> lstEquipProviderId) {
		this.lstEquipProviderId = lstEquipProviderId;
	}

	public List<String> getLstEquipProviderName() {
		return lstEquipProviderName;
	}

	public void setLstEquipProviderName(List<String> lstEquipProviderName) {
		this.lstEquipProviderName = lstEquipProviderName;
	}

	public List<Integer> getLstManufacturingYear() {
		return lstManufacturingYear;
	}

	public void setLstManufacturingYear(List<Integer> lstManufacturingYear) {
		this.lstManufacturingYear = lstManufacturingYear;
	}

	public List<String> getLstWarrantyExpiredDate() {
		return lstWarrantyExpiredDate;
	}

	public void setLstWarrantyExpiredDate(List<String> lstWarrantyExpiredDate) {
		this.lstWarrantyExpiredDate = lstWarrantyExpiredDate;
	}

	public List<Long> getLstStockId() {
		return lstStockId;
	}

	public void setLstStockId(List<Long> lstStockId) {
		this.lstStockId = lstStockId;
	}

	public List<String> getLstStockType() {
		return lstStockType;
	}

	public void setLstStockType(List<String> lstStockType) {
		this.lstStockType = lstStockType;
	}

	public List<String> getLstStockName() {
		return lstStockName;
	}

	public void setLstStockName(List<String> lstStockName) {
		this.lstStockName = lstStockName;
	}

	public List<BigDecimal> getLstPrice() {
		return lstPrice;
	}

	public void setLstPrice(List<BigDecimal> lstPrice) {
		this.lstPrice = lstPrice;
	}

	public Integer getRowsLength() {
		return rowsLength;
	}

	public void setRowsLength(Integer rowsLength) {
		this.rowsLength = rowsLength;
	}

	public Integer getStatusForm() {
		return statusForm;
	}

	public void setStatusForm(Integer statusForm) {
		this.statusForm = statusForm;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Integer getIsCreate() {
		return isCreate;
	}

	public void setIsCreate(Integer isCreate) {
		this.isCreate = isCreate;
	}

	public List<String> getLstStockCode() {
		return lstStockCode;
	}

	public void setLstStockCode(List<String> lstStockCode) {
		this.lstStockCode = lstStockCode;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	
}

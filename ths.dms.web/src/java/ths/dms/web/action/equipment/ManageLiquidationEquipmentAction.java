package ths.dms.web.action.equipment;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipLiquidationForm;
import ths.dms.core.entities.EquipLiquidationFormDtl;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.IsCheckType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.LinquidationStatus;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentLiquidationFormVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.memcached.MemcachedUtils;
//import ths.dms.web.utils.SendMailBySite;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

/**
 * 
 */
public class ManageLiquidationEquipmentAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	final int MAX_LENGTH_VALUE = 17;

	CommonMgr commonMgr;
	StaffMgr staffMgr;
	ShopMgr shopMgr;
	CustomerMgr customerMgr;
	EquipmentManagerMgr equipmentManagerMgr;
	EquipRecordMgr equipRecordMgr;

	/** ma bien ban */
	private String code;

	/** ma nhan vien */
	private String docNumber;

	/** ma nhan vien */
	private Long staffId;

	/** ma khach hang */
	private String buyerAddress;

	/** ten khach hang */
	private String buyerName;

	/** dien thoai khach hang */
	private String buyerPhone;

	/** dien thoai khach hang */
	private String reason;

	/** ngay tao tu */
	private String fromDate;

	/** ngay tao den */
	private String toDate;

	/** trang thai bien ban */
	private Integer status;

	/** trang thai thanh ly */
	private Long idLiquidation;
	private Long id;

	/** ma thiet bi */
	private String equipCode;

	/** so seri */
	private String seriNumber;

	/** loai thiet bi */
	private Long categoryId;

	/** nhom thiet bi */
	private Long groupId;

	/** nha cung cap thiet bi */
	private Long providerId;

	/** nam san xuat thiet bi */
	private Integer yearManufacture;

	/** ma kho thiet bi */
	private String stockCode;

	/** checkLot: cho thanh ly tai san */
	private Integer checkLot;
	
	/** gia tri thanh ly*/
	private BigDecimal liquidationValue;
	
	/** gia tri thu hoi*/
	private BigDecimal evictionValue;
	
	private String shopCode;
	private String shortCode;
	private String customerName;
	private String address;
	private String note;
	
	private Integer isSearch;
	/** file excel import */
	private File excelFile;
	private String excelFileContentType;

	/** danh sach bien ban */
	private List<Long> lstIdForm;

	/** danh sach thong tin thiet bi */
	private String lstEquipDelete;
	private String lstEquipAdd;
	
	private String lstEquipmentCode;
	private String equipLendCode;
	
	private List<String> lstEquipCode;
	private List<BigDecimal> lstValueEquip; // ko chua xai, 07/07/2015
	private List<BigDecimal> lstLiquidationValue; // 07/07/2015, gia tri thanh ly
	private List<BigDecimal> lstEvictionValue; // 07/07/2015, gia tri thu hoi
	private List<Long> lstIdRecord;
	private String createDate;
	private String lstRecordNotSendMail;
	/** danh sach thiet bi */
	private List<EquipmentDeliveryVO> equipments;

	// mot so thong tin cho upload file
	private List<File> lstFile;
	private List<String> lstFileContentType;
	private List<String> lstFileFileName;
	private List<FileVO> lstFileVo;
	// danh sach file xoa
	private String equipAttachFileStr;
	
	@Override
	public void prepare() throws Exception {
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
		super.prepare();
		lstRecordNotSendMail = "";
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);

		return SUCCESS;
	}

	/**
	 * Tim kiem danh sach bien ban thanh ly
	 * 
	 * @author nhutnn
	 * @since December 11,2014
	 */
	public String searchLiquidation() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentLiquidationFormVO>());
		try {
			KPaging<EquipmentLiquidationFormVO> kPaging = new KPaging<EquipmentLiquidationFormVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentFilter<EquipmentLiquidationFormVO> equipmentFilter = new EquipmentFilter<EquipmentLiquidationFormVO>();
			equipmentFilter.setkPaging(kPaging);
			Date fDate = null;
			Date tDate = null;

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				equipmentFilter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				equipmentFilter.setToDate(tDate);
			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setEquipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(docNumber)) {
				equipmentFilter.setDocNumber(docNumber);
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				equipmentFilter.setCode(code);
			}
			if (status != null) {
				equipmentFilter.setStatus(status);
			}
			if (currentUser.getShopRoot() != null) {
				equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
			}
			ObjectVO<EquipmentLiquidationFormVO> equipmentLiquidationFormVOs = equipmentManagerMgr.getListEquipmentLiquidationFormVOByFilter(equipmentFilter);
			if (equipmentLiquidationFormVOs.getLstObject() != null) {
				result.put("total", equipmentLiquidationFormVOs.getkPaging().getTotalRows());
				result.put("rows", equipmentLiquidationFormVOs.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.searchLiquidation"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Luu bien ban
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	
	/**
	 * chuyen trang thai bien ban thanh ly tai san
	 * @author vuongmq
	 * @since 09/07/2015
	 */
	public String saveStatusForm() {
		resetToken(result);
		result.put(ERROR, false);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
				return SUCCESS;
			}
			List<FormErrVO> lstError = new ArrayList<FormErrVO>();
			if (lstIdForm != null && lstIdForm.size() > 0) {
				for (int i = 0; i < lstIdForm.size(); i++) {
					List<EquipLiquidationFormDtl> lstDetailSendMail = new ArrayList<EquipLiquidationFormDtl>();
					EquipLiquidationForm e = equipmentManagerMgr.getEquipLiquidationFormById(lstIdForm.get(i), currentUser.getShopRoot().getShopId());
					if (e != null) {
						FormErrVO formErrVO = new FormErrVO();
						if (e.getStatus().equals(status)) {
							formErrVO.setFormStatusErr(1);
						}
						if (StatusRecordsEquip.CANCELLATION.getValue().equals(e.getStatus()) && (StatusRecordsEquip.APPROVED.getValue().equals(status) || StatusRecordsEquip.LIQUIDATED.getValue().equals(status))) {
							formErrVO.setFormStatusErr(1);
						}
						if (StatusRecordsEquip.APPROVED.getValue().equals(e.getStatus()) && StatusRecordsEquip.CANCELLATION.getValue().equals(status)) {
							formErrVO.setFormStatusErr(1);
						}
						if (StatusRecordsEquip.DRAFT.getValue().equals(e.getStatus()) && StatusRecordsEquip.LIQUIDATED.getValue().equals(status)) {
							formErrVO.setFormStatusErr(1);
						}
						if (StatusRecordsEquip.LIQUIDATED.getValue().equals(e.getStatus()) && (StatusRecordsEquip.APPROVED.getValue().equals(status) || StatusRecordsEquip.CANCELLATION.getValue().equals(status))) {
							formErrVO.setFormStatusErr(1);
						}
						if (formErrVO.getFormStatusErr() == null) {
							// status = 0 thi ko cap nhat
							Integer statusRe = 0;
							if (e != null && status != null) {
								// du thao
								if (e.getStatus() != null && e.getStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
									// duyet
									if (StatusRecordsEquip.APPROVED.getValue().equals(status)) {
										// trang thai tu du thao -> duyet
										statusRe = StatusRecordsEquip.APPROVED.getValue();
										e.setStatus(status);
									} else if (StatusRecordsEquip.CANCELLATION.getValue().equals(status)) {
										// trang thai tu du thao -> huy
										statusRe = StatusRecordsEquip.CANCELLATION.getValue();
										e.setStatus(status);
									}
								} else if (e.getStatus() != null && e.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
									if (StatusRecordsEquip.LIQUIDATED.getValue().equals(status)) {
										List<EquipLiquidationFormDtl> lstDetail = equipRecordMgr.getListEquipLiquidationFormDtlByRecordId(e.getId());
										if (lstDetail != null && !lstDetail.isEmpty()) {
											for (EquipLiquidationFormDtl detail : lstDetail) {
												Equipment equip = detail.getEquip();
												if (EquipTradeType.NOTICE_LOST.equals(equip.getTradeType())) {
													lstDetailSendMail.add(detail);
												}
											}
										}
										// trang thai tu duyet -> da thanh ly
										statusRe = StatusRecordsEquip.LIQUIDATED.getValue();
										e.setStatus(status);
									}
								}
								if (statusRe != 0) {
									e.setUpdateUser(currentUser.getUserName());
									equipRecordMgr.saveEquipLiquidationForm(e, statusRe);
//									if (!lstDetailSendMail.isEmpty()) {
//										this.sendEmail(lstDetailSendMail);
//									}
								}
							}
						} else {
							formErrVO.setFormCode(e.getEquipLiquidationFormCode());
							lstError.add(formErrVO);
						}
					}
				}
				if (lstError.size() > 0) {
					result.put("lstRecordError", lstError);
				}
			}
			result.put("lstRecordNotSendMail", lstRecordNotSendMail);

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.saveStatusForm"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}
	
	/**
	 * Send email cho phieu thanh ly cho thiet bi bao mat trang thai thanh ly
	 * 
	 * @author phuongvm
	 * @since 20/11/2015
	 * */
//	private void sendEmail(List<EquipLiquidationFormDtl> lstLiquidDetail) {
//		errMsg = "";
//		Date startLogDate = DateUtil.now();
//		try {
//			ApParamEquip emailSend = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.EMAIL_SEND_LOST_RECORD.getValue(), ApParamEquipType.EMAIL_SEND_LOST_RECORD, ActiveType.RUNNING);
//			if (emailSend != null && !StringUtil.isNullOrEmpty(emailSend.getApParamEquipName()) && !StringUtil.isNullOrEmpty(emailSend.getValue()) && lstLiquidDetail != null) {
//				for (EquipLiquidationFormDtl detail : lstLiquidDetail) {
//					errMsg = "";
//					Long shopId = null;
//					Customer customer = null;
//					String formCode = "";
//					Equipment equip = null;
//					if (detail.getEquip() != null) {
//						equip = detail.getEquip();
//						EquipRecordVO vo = equipRecordMgr.getLostRecordNewestByEquipId(detail.getEquip().getId());
//						if (vo != null) {
//							formCode = vo.getCode();
//						}
//						if (detail != null && EquipStockTotalType.KHO.equals(equip.getStockType())) {
//							EquipStock equipStock = equipmentManagerMgr.getEquipStockById(equip.getStockId());
//							if (equipStock != null && equipStock.getShop() != null) {
//								shopId = equipStock.getShop().getId();
//							}
//						} else if (detail != null && EquipStockTotalType.KHO_KH.equals(equip.getStockType())) {
//							customer = customerMgr.getCustomerById(equip.getStockId());
//							if (customer != null && customer.getShop() != null) {
//								shopId = customer.getShop().getId();
//							}
//						}
//					}
//
//					// lay danh sach mail can gui uu tien tu shop -> parent len
//					if (shopId != null) {
//						ApParamFilter paramFilter = new ApParamFilter();
//						paramFilter.setShopId(shopId);
//						paramFilter.setTypeStr(ApParamEquipType.EMAIL_TO_LOST_RECORD.getValue());
//						List<ApParamEquipVO> lstEmailToVO = apParamEquipMgr.getListEmailToParentShopByFilter(paramFilter);
//						if (lstEmailToVO != null && lstEmailToVO.size() > 0) {
//							//lay mail
//							String lstEmailToStr = "";
//							for (ApParamEquipVO apParamEquipVO : lstEmailToVO) {
//								if (apParamEquipVO != null && !StringUtil.isNullOrEmpty(apParamEquipVO.getApParamName())) {
//									lstEmailToStr = apParamEquipVO.getApParamName().trim();
//									break;
//								}
//							}
//							if (!StringUtil.isNullOrEmpty(lstEmailToStr)) {
//								String[] emailArr = lstEmailToStr.split(ConstantManager.SEPARATOR_CHARACTER);
//								StringBuilder thongTin = null;
//								for (String emailTo : emailArr) {
//									if (StringUtil.isNullOrEmpty(emailTo.trim())) {
//										continue;
//									}
//									// luc nay lay thong tin send Email
//									thongTin = new StringBuilder();
//									Shop shop = shopMgr.getShopById(shopId);
//									String title = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.don.vi", shop.getShopCode() + " - " + shop.getShopName());
//									thongTin.append(title);
//									if (detail != null && detail.getEquip() != null && detail.getEquip().getEquipGroup() != null) {
//										thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.matb", detail.getEquip().getCode()));
//										thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.name.group.code.eqip", detail.getEquip().getEquipGroup().getCode() + " - " + detail.getEquip()
//												.getEquipGroup().getName()));
//									}
//									if (customer != null) {
//										thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.kh", customer.getShortCode() + " - " + customer.getCustomerName()));
//									}
//									thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.gui.ma.phieu.tb", formCode));
//									SendMailBySite emailer = new SendMailBySite();
//									StringBuilder body = new StringBuilder();
//									body.append(R.getResource("equipment.lost.rec.send.mail.gui.nguoi.phe.duyet"));
//									body.append(ConstantManager.NEW_LINE_CHARACTER);
//									body.append(thongTin);
//									emailer.sendEmail(emailSend.getApParamEquipName(), emailSend.getValue(), emailTo.trim(), title, body.toString());
//								}
//							} else {
//								//errMsg = "Tham so cau hinh Emqil nhan khong ton tai.";
//								errMsg = R.getResource("equipment.lost.rec.send.mail.nhan.khong.ton.tai");
//							}
//						} else {
//							//errMsg ="Tham so cau hinh khong ton tai Email nhan.";
//							errMsg = R.getResource("equipment.lost.rec.send.mail.nhan.khong.ton.tai.trong.cau.hinh");
//						}
//					} else {
//						//errMsg = "Don vi khong ton tai de gui Email phieu cho duyet.";
//						errMsg = R.getResource("equipment.lost.rec.send.mail.don.vi.khong.ton.tai.de.send.email.cho.duyet");
//					}
//					if (!StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(formCode)) {
//						if (!StringUtil.isNullOrEmpty(lstRecordNotSendMail)) {
//							lstRecordNotSendMail += ConstantManager.COMMA + formCode;
//						} else {
//							lstRecordNotSendMail += formCode;
//						}
//					}
//				}
//			} else {
//				//errMsg = "Eqmil gui khong ton tai hoac khong dung Mat khau.";
//				errMsg = R.getResource("equipment.lost.rec.send.mail.gui.khong.ton.tai.hoac.pass.khong.dung");
//			}
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.sendEmail"), createLogErrorStandard(startLogDate));
//			errMsg = R.getResource("equipment.lost.rec.send.mail.khong.thanh.cong");
//		}
//	}
	
	/**
	 * Kiem tra dong bien ban
	 * 
	 * @author phuongvm
	 * @return true: la dong bien , false: la dong chi tiet
	 * @since 16/11/2015
	 */
	public boolean isANewRecord(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4)) || !StringUtil.isNullOrEmpty(row.get(7)) || !StringUtil.isNullOrEmpty(row
				.get(8)) || !StringUtil.isNullOrEmpty(row.get(9)) || !StringUtil.isNullOrEmpty(row.get(10)) || !StringUtil.isNullOrEmpty(row.get(11)) || !StringUtil.isNullOrEmpty(row.get(12))) {
			return true;
		}
		return false;
	}

	/**
	 * Kiem tra dong trong
	 * 
	 * @author nhutnn
	 * @since 05/01/2015
	 */
	public boolean isEmtyRow(List<String> row) {
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(2)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4)) || !StringUtil.isNullOrEmpty(row.get(5)) || !StringUtil.isNullOrEmpty(row.get(6)) || !StringUtil.isNullOrEmpty(row.get(7))
		   || !StringUtil.isNullOrEmpty(row.get(8)) || !StringUtil.isNullOrEmpty(row.get(9)) || !StringUtil.isNullOrEmpty(row.get(10)) || !StringUtil.isNullOrEmpty(row.get(11))) {
			return false;
		}
		return true;
	}

	/**
	 * Kiem tra dong hop le
	 * 
	 * @author nhutnn
	 * @since 05/01/2015
	 * @Description, nhung dung detail: co dong chinh != null thi false
	 */
	public boolean isRowValid(List<String> row) {
		/*if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(3)) || !StringUtil.isNullOrEmpty(row.get(4)) || !StringUtil.isNullOrEmpty(row.get(5)) || !StringUtil.isNullOrEmpty(row.get(6)) || !StringUtil.isNullOrEmpty(row.get(7))) {
			return false;
		}*/
		if (!StringUtil.isNullOrEmpty(row.get(0)) || !StringUtil.isNullOrEmpty(row.get(1)) || !StringUtil.isNullOrEmpty(row.get(8))) {
			return false;
		}
		return true;
	}

	/**
	 * import bien ban bang excel
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 */
	public String importLiquidation() throws Exception {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		boolean isNewRecord = true;
		boolean isValid = true;
		int numImportColumn = 13;
		Date sysDate = commonMgr.getSysDate();
		List<EquipLiquidationFormDtl> lstEquipDtl = new ArrayList<EquipLiquidationFormDtl>();
		List<Equipment> lstEquipCheck = new ArrayList<Equipment>();
		EquipLiquidationForm equipLiquidationForm = null;
		String username = getCurrentUser().getUserName();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<CellBean> lstFailsInForm = new ArrayList<CellBean>();
//		List<CellBean> lstFailsRowInvalid = new ArrayList<CellBean>();
		// bien ban hop le (bao gom tat ca cac dong chi tiet)
		boolean isValidForm = true;
		// so dong thanh cong
//		Integer numberSuccess=0;
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, numImportColumn);
		EquipRecordFilter<EquipmentLiquidationFormVO> filter = new EquipRecordFilter<EquipmentLiquidationFormVO>();
//		Long shopRoot = null;
//		Long staffRoot = null;
//		if (currentUser.getShopRoot() != null) {
//			shopRoot = currentUser.getShopRoot().getShopId();
//		}
//		if (currentUser.getStaffRoot() != null) {
//			staffRoot = currentUser.getStaffRoot().getStaffId();
//		}
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				//tamvnm: bo check ky hien tai
//				EquipPeriod equipPeriod = null;
//				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPeriod == null ||!equipPeriod.getStatus().equals(EquipPeriodType.OPENED) || equipPeriod.getFromDate().after(DateUtil.now()) || equipPeriod.getToDate().before(DateUtil.now())) {
//					isError = true;
//					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//					return SUCCESS;
//				}
				/** flag cho checkLot, Tong gia tri thanh ly, thu hoi; Gia tri thanh ly, thu hoi */
				int FLAG_SUM_VALUE = 0;
				int FLAG_VALUE = 0;
				// kiem tra dong rong
				BigDecimal sumEquipPrice = BigDecimal.ZERO;
				BigDecimal valEquipLiquidationSum = null;
				BigDecimal valEquipEvictionSum = null;
				for (int i = 0, size = lstData.size(); i < size; i++) {
					message = "";
					totalItem++;
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						List<String> rowNext = null;
						if (i != lstData.size() - 1) {
							rowNext = lstData.get(i + 1);
						}
						if (isValid) {
							if (i == 0 || isANewRecord(row)) { // check xem co phai la bien ban mới
								isNewRecord = true;
								if (rowNext != null && isEmtyRow(rowNext)) {
									isValid = false;
								}
								if (isValidForm && !lstEquipDtl.isEmpty() && equipLiquidationForm != null) { // them moi
									List<EquipLiquidationFormDtl> lstDetailSendMail = new ArrayList<EquipLiquidationFormDtl>();
									if (lstEquipDtl != null) {
										for (EquipLiquidationFormDtl detail : lstEquipDtl) {
											Equipment equip = detail.getEquip();
											if (EquipTradeType.NOTICE_LOST.equals(equip.getTradeType())) {
												lstDetailSendMail.add(detail);
											}
										}
									}
									equipRecordMgr.createLiquidationForm(equipLiquidationForm, lstEquipDtl, filter);
//									if (StatusRecordsEquip.LIQUIDATED.getValue().equals(equipLiquidationForm.getStatus())) {
//										if (!lstDetailSendMail.isEmpty()) {
//											this.sendEmail(lstDetailSendMail);
//										}
//									}
									lstFailsInForm = new ArrayList<CellBean>();
								} else {
									lstFails.addAll(lstFailsInForm);
									lstFailsInForm = new ArrayList<CellBean>();
									isValidForm = true;
								}
								lstEquipDtl = new ArrayList<EquipLiquidationFormDtl>();
								equipLiquidationForm = new EquipLiquidationForm();
								FLAG_SUM_VALUE = 0;
								FLAG_VALUE = 0;
								sumEquipPrice = BigDecimal.ZERO;
								valEquipLiquidationSum = null;
								valEquipEvictionSum = null;
							} else {
								if (rowNext != null && isEmtyRow(rowNext)) {
									isValid = false;
								}
								isNewRecord = false;
							}
						} else {
							if (isValidForm && !lstEquipDtl.isEmpty() && equipLiquidationForm != null) { // them moi 
								List<EquipLiquidationFormDtl> lstDetailSendMail = new ArrayList<EquipLiquidationFormDtl>();
								if (lstEquipDtl != null) {
									for (EquipLiquidationFormDtl detail : lstEquipDtl) {
										Equipment equip = detail.getEquip();
										if (EquipTradeType.NOTICE_LOST.equals(equip.getTradeType())) {
											lstDetailSendMail.add(detail);
										}
									}
								}
								equipRecordMgr.createLiquidationForm(equipLiquidationForm, lstEquipDtl, filter);
//								if (StatusRecordsEquip.LIQUIDATED.getValue().equals(equipLiquidationForm.getStatus())) {
//									if (!lstDetailSendMail.isEmpty()) {
//										this.sendEmail(lstDetailSendMail);
//									}
//								}
								lstFailsInForm = new ArrayList<CellBean>();
							} else {
								lstFails.addAll(lstFailsInForm);
								lstFailsInForm = new ArrayList<CellBean>();
								isValidForm = true;
							}
							lstEquipDtl = new ArrayList<EquipLiquidationFormDtl>();
							equipLiquidationForm = new EquipLiquidationForm();
							lstFailsInForm.add(StringUtil.addFailBean(row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid")));
							FLAG_SUM_VALUE = 0;
							FLAG_VALUE = 0;
							sumEquipPrice = BigDecimal.ZERO;
							valEquipLiquidationSum = null;
							valEquipEvictionSum = null;
							continue;
						}
						
						String docNumber = null;
						StatusRecordsEquip statusRecord = null;
						Equipment equipment = null;						
						BigDecimal valLiquidation = null;
						BigDecimal valEviction = null;
						String reason = null;
						String buyerName = null;
						String buyerAddress = null;
						String buyerPhone = null;
						Date ngayLap = null;
						String noteStr = null;
						// ** Check trung du lieu cac record */
						// Neu la bien ban moi
						if (isNewRecord) {
							// Ma cong van/ to trinh thanh ly
							if (row.size() > 0) {
								String value = row.get(0);
								String msg = ValidateUtil.validateField(value.trim(), "equipment.manager.doc.number", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									//isValidForm = false;
								} else {
									docNumber = value;
								}
							}
							
							// ** Ngay lap */
							if (row.size() > 1) {
								String value = row.get(1);
								String msg = ValidateUtil.validateField(value, "equipment.create.form.date", 50, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}else{
									msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date",null);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										ngayLap = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
										if (ngayLap == null) {
											message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
										} else {
											if (DateUtil.compareTwoDate(ngayLap, sysDate) == 1) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.big") + "\n";
//												
											} 
//											else {
//												//tamvnm: update dac ta moi. 30/06/2015
//												List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(ngayLap);
//												if (lstPeriod == null || lstPeriod.size() == 0) {
//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//												} else if (lstPeriod.size() > 0) {
//													// gan gia tri ky dau tien.
//													equipPeriod = lstPeriod.get(0);
//												}
//											}
										}
									}
								}							
							}
							
							
							// Ma thiet bi
							if (row.size() > 2) {
								String value = row.get(2);
								String msg = ValidateUtil.validateField(value, "device.code", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									value = value.trim().toUpperCase();
									EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
									equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
									equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
									// trang thai se su dung flagNotUsageStatus: null se la: 2: dang o kho va 3: dang su dung
									// tim kiem thi flagNotUsageStatus: true
//									equipmentFilter.setFlagNotUsageStatus(true);
									equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
									equipmentFilter.setFlagNotUsageStatus(false);
									equipmentFilter.setEquipCode(value);
									ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipRecordMgr.getListEquipmentLiquidationPopup(equipmentFilter);
									if (equipmentDeliverys != null && equipmentDeliverys.getLstObject() != null && equipmentDeliverys.getLstObject().size() > 0) {
										equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
										if (equipment != null) {
											if (!StatusType.ACTIVE.equals(equipment.getStatus())) {
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
											}
											// 2 tr/hop: 1: tu bao mat sang va thiet bi dang o kho, su dung
											if (!EquipTradeType.NOTICE_LOST.equals(equipment.getTradeType()) && !EquipTradeType.NOTICE_LOSS_MOBILE.equals(equipment.getTradeType())) {
												// dang o kho, dang su dung
												if (!EquipUsageStatus.SHOWING_WAREHOUSE.equals(equipment.getUsageStatus()) && !EquipUsageStatus.IS_USED.equals(equipment.getUsageStatus())) {
													message += R.getResource("equipment.liquidation.row.invalid.not.in.kho.dangsudung", equipment.getCode()) + "\n";
												}
												if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) {
													message += R.getResource("equipment.liquidation.tb.phai.giao.dich.bt") + "\n";
												}
											} else {
												// Tb bao mat
												if (!LinquidationStatus.WAITING_LIQUIDATION.getValue().equals(equipment.getLinquidationStatus())) {
													message += R.getResource("equipment.liquidation.tb.phai.cho.thanh.ly");
												}
											}
											for (int k = 0, sz = lstEquipCheck.size(); k < sz; k++) {
												if (equipment.getCode().equals(lstEquipCheck.get(k).getCode())) {
													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err1");
													break;
												}
											}
											if (equipment.getPrice() != null) {
												sumEquipPrice = sumEquipPrice.add(equipment.getPrice());
											}
										} else {
											message += R.getResource("common.catalog.code.not.exist",  R.getResource("device.code") + " " + value);
										}
										
									} else {
										message += R.getResource("equipment.liquidation.tb.khong.ton.tai.hoac.khong.phan.quyen");
									}
								}
							}
							// lay tong nguyen gia thiet bi
							for (int j = i + 1; j < size; j++) {
								List<String> rowTmp = lstData.get(j);
								if (rowTmp != null && (!StringUtil.isNullOrEmpty(rowTmp.get(0)) || !StringUtil.isNullOrEmpty(rowTmp.get(1)) || !StringUtil.isNullOrEmpty(rowTmp.get(8)))) {
									break;
								}
								if (StringUtil.isNullOrEmpty(rowTmp.get(2))) {
									continue;
								}
								Equipment equipTmp = equipmentManagerMgr.getEquipmentByCodeAndStatus(rowTmp.get(2), null);
								if (equipTmp != null && equipTmp.getPrice() != null) {
									sumEquipPrice = sumEquipPrice.add(equipTmp.getPrice());
								}
							}
							// Tong Gia tri thanh ly
							if (row.size() > 3) {
								String value = row.get(3);
								if (!StringUtil.isNullOrEmpty(value)) {
									FLAG_SUM_VALUE = 1;
									String msg = ValidateUtil.validateField(value, "equipment.liquidation.gt.thanh.ly.tong", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										BigDecimal val = BigDecimal.valueOf(Long.parseLong(value));
										if (val.compareTo(BigDecimal.ZERO) <= 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.liquidation.value.sum.more") + ConstantManager.NEW_LINE_CHARACTER;
										} else if (val.compareTo(sumEquipPrice) > 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.liquidation.value.sum.less.price") + ConstantManager.NEW_LINE_CHARACTER;
										} else {
											valEquipLiquidationSum = val;
										}
									}
								}
							}
							// Tong Gia tri thu hoi
							if (row.size() > 4) {
								String value = row.get(4);
								if (!StringUtil.isNullOrEmpty(value)) {
									FLAG_SUM_VALUE = 1;
									String msg = ValidateUtil.validateField(value, "equipment.liquidation.gt.thu.hoi.tong", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										BigDecimal val = BigDecimal.valueOf(Long.parseLong(value));//										
										if (val.compareTo(BigDecimal.ZERO) <= 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.value.sum.more") + ConstantManager.NEW_LINE_CHARACTER;
										} else if (val.compareTo(sumEquipPrice) > 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.value.sum.less.price") + ConstantManager.NEW_LINE_CHARACTER;
										} else {
											valEquipEvictionSum = val;
										}
									}
								}
//								else {
//									if (FLAG_SUM_VALUE == 1) {
//										message += R.getResource("equipment.liquidation.nhap.them.tong.gia.tri.thu.hoi") + "\n";
//									}
//								}
							}
							// Gia tri thanh ly
							if (row.size() > 5) {
								String value = row.get(5);
								if (!StringUtil.isNullOrEmpty(value)) {
									if (FLAG_SUM_VALUE == 1) {
										message += R.getResource("equipment.liquidation.nhap.thanh.ly.thu.hoi.tat.ca.loi") + "\n";
									}
									FLAG_VALUE = 2;
									String msg = ValidateUtil.validateField(value, "equipment.liquidation.gt.thanh.ly", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										BigDecimal val = BigDecimal.valueOf(Long.parseLong(value));
										if (equipment != null && val.compareTo(equipment.getPrice()) > 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.liquidation.value.less.price") + "\n";
										} else if (val.compareTo(BigDecimal.ZERO) <= 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.liquidation.value.more") + "\n";
										} else {
											valLiquidation = val;
										}
									}
								}
							}
							// Gia tri thu hoi
							if (row.size() > 6) {
								String value = row.get(6);
								if (!StringUtil.isNullOrEmpty(value)) {
									if (FLAG_SUM_VALUE == 1) {
										message += R.getResource("equipment.liquidation.nhap.thanh.ly.thu.hoi.tat.ca.loi") + "\n";
									}
//									if (FLAG_VALUE == 0) {
//										message += R.getResource("equipment.liquidation.nhap.them.gia.tri.thanh.ly") + "\n";
//									}
									FLAG_VALUE = 2;
									String msg = ValidateUtil.validateField(value, "equipment.liquidation.gt.thu.hoi", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										BigDecimal val = BigDecimal.valueOf(Long.parseLong(value));
										if (equipment != null && val.compareTo(equipment.getPrice()) > 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.value.less.price") + "\n";
										} else if (val.compareTo(BigDecimal.ZERO) <= 0) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.value.more") + "\n";
										} else {
											valEviction = val;
										}
									}
								} 
//								else {
//									if (FLAG_VALUE == 2) {
//										message += R.getResource("equipment.liquidation.nhap.them.gia.tri.thu.hoi") + "\n";
//									}
//								}
							}
							// Ly do
							if (row.size() > 7) {
								String value = row.get(7);
								String msg = ValidateUtil.validateField(value, "equipment.manager.reason", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									//isValidForm = false;
								} else {
									reason = value;
								}
							}
							// Trang thai
							if (row.size() > 8) {
								String value = row.get(8);
								String msg = ValidateUtil.validateField(value, "equipment.manager.status.record", 100, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									isValidForm = false;
								} else if (value.toUpperCase().trim().equals(R.getResource("action.status.duthao").toUpperCase().trim())) {
									statusRecord = StatusRecordsEquip.DRAFT;
								} else if (value.toUpperCase().trim().equals(R.getResource("action.status.duyet").toUpperCase().trim())) {
									statusRecord = StatusRecordsEquip.APPROVED;
								} else if (value.toUpperCase().trim().equals(R.getResource("action.status.dathanhly").toUpperCase().trim())) {
									statusRecord = StatusRecordsEquip.LIQUIDATED;
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.status.record.invalid") + "\n";
									//isValidForm = false;
								}
							}
							// Ten nguoi mua
							if (row.size() > 9) {
								String value = row.get(9);
								String msg = ValidateUtil.validateField(value, "equipment.manager.buyer.name", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									//isValidForm = false;
								} else {
									buyerName = value;
								}
							}
							// Dien thoai nguoi mua
							if (row.size() > 10) {
								String value = row.get(10);
								String msg = ValidateUtil.validateField(value, "equipment.manager.buyer.phone", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									//isValidForm = false;
								} else {
									buyerPhone = value;
								}
							}
							// Dia chi nguoi mua
							if (row.size() > 11) {
								String value = row.get(11);
								String msg = ValidateUtil.validateField(value, "equipment.manager.buyer.address", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									//isValidForm = false;
								} else {
									buyerAddress = value;
								}
							}
							
							// ** Ghi chu */
							if (row.size() > 12) {
								String value = row.get(12);
								String msg = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									noteStr = value;
								}
							}
							lstFailsInForm.add(StringUtil.addFailBean(row, message));
						} else {
							// nguoc lai no la tung dong chi tiet cua bien ban
//							if (isValidForm) {
								// Ma thiet bi
								if (row.size() > 2) {
									String value = row.get(2);
									String msg = ValidateUtil.validateField(value, "device.code", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										value = value.trim().toUpperCase();
										EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
										equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
										equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
										// trang thai se su dung flagNotUsageStatus: null se la: 2: dang o kho va 3: dang su dung
										// tim kiem thi flagNotUsageStatus: true
//										equipmentFilter.setFlagNotUsageStatus(true);
										equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
										equipmentFilter.setFlagNotUsageStatus(false);
										equipmentFilter.setEquipCode(value);
										ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipRecordMgr.getListEquipmentLiquidationPopup(equipmentFilter);
										if (equipmentDeliverys != null && equipmentDeliverys.getLstObject() != null && equipmentDeliverys.getLstObject().size() > 0) {
											equipment = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
											if (equipment != null) {
												if (!StatusType.ACTIVE.equals(equipment.getStatus())) {
													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "equipment.manager.equipment.code");
												}
												// 2 tr/hop: 1: tu bao mat sang va thiet bi dang o kho, su dung
												if (!EquipTradeType.NOTICE_LOST.equals(equipment.getTradeType())) {
													// dang o kho, dang su dung
													if (!EquipUsageStatus.SHOWING_WAREHOUSE.equals(equipment.getUsageStatus()) && !EquipUsageStatus.IS_USED.equals(equipment.getUsageStatus())) {
														message += R.getResource("equipment.liquidation.row.invalid.not.in.kho.dangsudung", equipment.getCode()) + "\n";
													}
													if (!EquipTradeStatus.NOT_TRADE.getValue().equals(equipment.getTradeStatus())) {
														message += R.getResource("equipment.liquidation.tb.phai.giao.dich.bt") + "\n";
													}
												} else {
													// Tb bao mat
													if (!LinquidationStatus.WAITING_LIQUIDATION.getValue().equals(equipment.getLinquidationStatus())) {
														message += R.getResource("equipment.liquidation.tb.phai.cho.thanh.ly");
													}
												}
												for (int k = 0, sz = lstEquipCheck.size(); k < sz; k++) {
													if (equipment.getCode().equals(lstEquipCheck.get(k).getCode())) {
														message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err1");
														break;
													}
												}
											} else {
												message += R.getResource("common.catalog.code.not.exist",  R.getResource("device.code") + " " + value);
											}
										} else {
											message += R.getResource("equipment.liquidation.tb.khong.ton.tai.hoac.khong.phan.quyen");
										}
									}
								}
								
								// Gia tri thanh ly
								if (row.size() > 5) {
									String value = row.get(5);
									if (!StringUtil.isNullOrEmpty(value)) {
										if (FLAG_SUM_VALUE == 1) {
											message += R.getResource("equipment.liquidation.nhap.thanh.ly.thu.hoi.tat.ca.loi") + "\n";
										}
										FLAG_VALUE = 2;
										String msg = ValidateUtil.validateField(value, "equipment.liquidation.gt.thanh.ly", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
										if (!StringUtil.isNullOrEmpty(msg)) {
											message += msg;
										} else {
											BigDecimal val = BigDecimal.valueOf(Long.parseLong(value));
											if (equipment != null && val.compareTo(equipment.getPrice()) > 0) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.liquidation.value.less.price") + "\n";
											} else if (val.compareTo(BigDecimal.ZERO) <= 0) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.liquidation.value.more") + "\n";
											} else {
												valLiquidation = val;
											}
										}
									}
								}
								// Gia tri thu hoi
								if (row.size() > 6) {
									String value = row.get(6);
									if (!StringUtil.isNullOrEmpty(value)) {
										if (FLAG_SUM_VALUE == 1) {
											message += R.getResource("equipment.liquidation.nhap.thanh.ly.thu.hoi.tat.ca.loi") + "\n";
										}
//										if (FLAG_VALUE == 0) {
//											message += R.getResource("equipment.liquidation.nhap.them.gia.tri.thanh.ly") + "\n";
//										}
										FLAG_VALUE = 2;
										String msg = ValidateUtil.validateField(value, "equipment.liquidation.gt.thu.hoi", 17, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
										if (!StringUtil.isNullOrEmpty(msg)) {
											message += msg;
										} else {
											BigDecimal val = BigDecimal.valueOf(Long.parseLong(value));
											if (equipment != null && val.compareTo(equipment.getPrice()) > 0) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.value.less.price") + "\n";
											} else if (val.compareTo(BigDecimal.ZERO) <= 0) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.eviction.value.more") + "\n";
											} else {
												valEviction = val;
											}
										}
									} 
//									else {
//										if (FLAG_VALUE == 2) {
//											message += R.getResource("equipment.liquidation.nhap.them.gia.tri.thu.hoi") + "\n";
//										}
//									}
								}
//							} else if (!isValidForm) {
//								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.row.invalid.makh.not.in.kho.manpp.err2");
//							}
							lstFailsInForm.add(StringUtil.addFailBean(row, message));
//							if (!StringUtil.isNullOrEmpty(message)) {
//								isValidForm = false;
//							}
						}
						if (StringUtil.isNullOrEmpty(message)) {
							if (isNewRecord) {
								lstEquipDtl = new ArrayList<EquipLiquidationFormDtl>();
								equipLiquidationForm = new EquipLiquidationForm();
								equipLiquidationForm.setBuyerAddress(buyerAddress);
								equipLiquidationForm.setBuyerName(buyerName);
								equipLiquidationForm.setBuyerPhoneNumber(buyerPhone);
								equipLiquidationForm.setCreateUser(username);
//								equipLiquidationForm.setEquipPeriod(equipPeriod);
								equipLiquidationForm.setCreateFormDate(ngayLap);
								equipLiquidationForm.setNote(noteStr);
								equipLiquidationForm.setReason(reason);
								equipLiquidationForm.setStatus(statusRecord.getValue());
								equipLiquidationForm.setDocNumber(docNumber);
								if (FLAG_SUM_VALUE == 1 && FLAG_VALUE == 0) {
									// co tong Gia tri thanh ly,thu hoi; khong co gia tri thanh ly thu hoi detail
									equipLiquidationForm.setCheckLot(IsCheckType.YES);
									equipLiquidationForm.setLiquidationValue(valEquipLiquidationSum);
									equipLiquidationForm.setEvictionValue(valEquipEvictionSum);
								} else {
									equipLiquidationForm.setCheckLot(IsCheckType.NO);
									equipLiquidationForm.setLiquidationValue(null);
									equipLiquidationForm.setEvictionValue(null);
								}
							}
							if (equipment != null) {
								EquipLiquidationFormDtl liquidationFormDtl = new EquipLiquidationFormDtl();
								liquidationFormDtl.setCreateUser(username);
								liquidationFormDtl.setEquip(equipment);
								liquidationFormDtl.setEquipCategory(equipment.getEquipGroup().getEquipCategory());
								liquidationFormDtl.setEquipGroup(equipment.getEquipGroup());
								//liquidationFormDtl.setEquipValue(valEquip);
								if (FLAG_SUM_VALUE == 0 && FLAG_VALUE == 2) {
									// Ko co tong Gia tri thanh ly,thu hoi; co gia tri thanh ly thu hoi Detail
									liquidationFormDtl.setLiquidationValue(valLiquidation);
									liquidationFormDtl.setEvictionValue(valEviction);
								} else {
									liquidationFormDtl.setLiquidationValue(null);
									liquidationFormDtl.setEvictionValue(null);
								}

								lstEquipDtl.add(liquidationFormDtl);
								lstEquipCheck.add(equipment);
							}						
						} else {
							isValidForm = false;
						}
					}
				}
				// het file excel
				if (isValidForm && !lstEquipDtl.isEmpty() && equipLiquidationForm != null) { // them moi 
					List<EquipLiquidationFormDtl> lstDetailSendMail = new ArrayList<EquipLiquidationFormDtl>();
					if (lstEquipDtl != null) {
						for (EquipLiquidationFormDtl detail : lstEquipDtl) {
							Equipment equip = detail.getEquip();
							if (EquipTradeType.NOTICE_LOST.equals(equip.getTradeType())) {
								lstDetailSendMail.add(detail);
							}
						}
					}
					equipRecordMgr.createLiquidationForm(equipLiquidationForm, lstEquipDtl, filter);
//					if (StatusRecordsEquip.LIQUIDATED.getValue().equals(equipLiquidationForm.getStatus())) {
//						if (!lstDetailSendMail.isEmpty()) {
//							this.sendEmail(lstDetailSendMail);
//						}
//					}
					lstFailsInForm = new ArrayList<CellBean>();
				} else {
					lstFails.addAll(lstFailsInForm);
					lstFailsInForm = new ArrayList<CellBean>();
					isValidForm = true;
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_LIQUIDATION_FAIL);
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.importLiquidation"), createLogErrorStandard(startLogDate));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			isError = true;
		}
		return SUCCESS;
	}

	/**
	 * Chuyen trang tao moi hay chinh sua bien ban thanh ly
	 * 
	 * @author nhutnn
	 * @since 05/01/2015
	 */
	public String changeLiquidation() {
		generateToken();
		result.put(ERROR, false);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
				return SUCCESS;
			}
			if (id != null) {
				EquipmentLiquidationFormVO equipmentLiquidationFormVO = equipRecordMgr.getEquipmentLiquidationVOById(id, currentUser.getShopRoot().getShopId());
				if (equipmentLiquidationFormVO != null) {
					// kiem tra phan quyen
					EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
					equipmentFilter.setkPaging(null);
					equipmentFilter.setIdLiquidation(id);
					ObjectVO<EquipmentDeliveryVO> objectVO = equipRecordMgr.getListEquipmentLiquidationVOByFilter(equipmentFilter);
					if (objectVO != null && objectVO.getLstObject() != null) {
						boolean isPermission = false;
						List<EquipmentDeliveryVO> lstEquipVOs = objectVO.getLstObject();
						for (int i = 0, sz = lstEquipVOs.size(); i < sz; i++) {
							EquipmentDeliveryVO equipmentVO = lstEquipVOs.get(i);
							if (equipmentVO.getStockId() == null) {
								continue;
							}
							if (EquipStockTotalType.KHO.getValue().equals(equipmentVO.getStockType())) {
								EquipStock eqStock = commonMgr.getEntityById(EquipStock.class, equipmentVO.getStockId());
								if (eqStock == null || eqStock.getShop() == null || checkShopInOrgAccessById(eqStock.getShop().getId())) {
									isPermission = true;
									break;
								}
							} else if (EquipStockTotalType.KHO_KH.getValue().equals(equipmentVO.getStockType())) {
								Customer eqCus = commonMgr.getEntityById(Customer.class, equipmentVO.getStockId());
								if (eqCus == null || eqCus.getShop() == null || checkShopInOrgAccessById(eqCus.getShop().getId())) {
									isPermission = true;
									break;
								}
							}
						}
						if (!isPermission) {
							return PAGE_NOT_PERMISSION;
						}
					}
					
					
					idLiquidation = id;
					docNumber = equipmentLiquidationFormVO.getDocNumber();
					buyerName = equipmentLiquidationFormVO.getBuyerName();
					buyerPhone = equipmentLiquidationFormVO.getBuyerPhoneNumber();
					buyerAddress = equipmentLiquidationFormVO.getBuyerAddress();
					reason = equipmentLiquidationFormVO.getReasonLiquidation();
					note = equipmentLiquidationFormVO.getNote();
					status = equipmentLiquidationFormVO.getStatus();
					if (equipmentLiquidationFormVO.getCreateFormDate() != null) {
						createDate = DateUtil.convertDateByString(equipmentLiquidationFormVO.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY) ;
					}
					checkLot = equipmentLiquidationFormVO.getCheckLot();
					liquidationValue = equipmentLiquidationFormVO.getLiquidationValue();
					evictionValue = equipmentLiquidationFormVO.getEvictionValue();
				}
				//Lay danh sach tap tin dinh kem neu co
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(id);
				filter.setObjectType(EquipTradeType.LIQUIDATION.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
			}
			
			// xu ly popup chi tiet
			if (isView != null && isView == 1) {
				return LIST;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.changeLiquidation"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return SUCCESS;
	}

	/**
	 * Chinh sua bien ban thnh ly tai san
	 * @author nhutnn
	 * @since December 11,2014
	 */
	
	/**
	 * chinh sua bien ban thanh ly tai san
	 * @author vuongmq
	 * @since 08/07/2015
	 * @return
	 */
	public String updateLiquidation() {
		resetToken(result);
		result.put(ERROR, false);
		String errMsg = "";
		try {
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
				return SUCCESS;
			}
			if (idLiquidation != null) {
				EquipLiquidationForm e = equipmentManagerMgr.getEquipLiquidationFormById(idLiquidation, currentUser.getShopRoot().getShopId());
				Integer statusRe = StatusRecordsEquip.DRAFT.getValue();
				// e: nay la pheiu thanh toan
				if (e != null) {
					if (status != null) {
						// du thao
						if (e.getStatus() != null && e.getStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
							// duyet
							if (StatusRecordsEquip.APPROVED.getValue().equals(status)) {
								// trang thai tu du thao -> duyet
								statusRe = StatusRecordsEquip.APPROVED.getValue();
	//							e.setStatus(status);
							} else if (StatusRecordsEquip.CANCELLATION.getValue().equals(status)) {
								// trang thai tu du thao -> huy
								statusRe = StatusRecordsEquip.CANCELLATION.getValue();
								e.setStatus(status);
							}
						} else if (e.getStatus() != null && e.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
							statusRe = StatusRecordsEquip.APPROVED.getValue();
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("equipment.manager.group.status.is.not.exist"));
						return JSON;
					}
				} else {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("equipment.liquidation.not.exist"));
					return JSON;
				}
				if (!StringUtil.isNullOrEmpty(docNumber)) {
					errMsg = ValidateUtil.validateField(docNumber.trim(), "equipment.manager.doc.number", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT, ConstantManager.ERR_MAX_LENGTH);
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					e.setDocNumber(docNumber);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.doc.number")));
					return JSON;
				}
				
				Date cDate = null;
				if (!StringUtil.isNullOrEmpty(createDate)) {
					cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					e.setCreateFormDate(cDate);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null"));
					return JSON;
				}
				if (!StringUtil.isNullOrEmpty(buyerName)) {
					errMsg = ValidateUtil.validateField(buyerName, "equipment.manager.buyer.name", 250, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					e.setBuyerName(buyerName);
				}
				if (!StringUtil.isNullOrEmpty(buyerPhone)) {
					errMsg = ValidateUtil.validateField(buyerPhone, "equipment.manager.buyer.phone", 50, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					e.setBuyerPhoneNumber(buyerPhone);
				}
				if (!StringUtil.isNullOrEmpty(buyerAddress)) {
					errMsg = ValidateUtil.validateField(buyerAddress, "equipment.manager.buyer.address", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					e.setBuyerAddress(buyerAddress);
				}
				if (!StringUtil.isNullOrEmpty(reason)) {
					errMsg = ValidateUtil.validateField(reason, "equipment.manager.reason", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					e.setReason(reason);
				}
				if (!StringUtil.isNullOrEmpty(note)) {
					errMsg = ValidateUtil.validateField(note, "common.description.lable.gc", 500, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					e.setNote(note);
				}
				
				//tamvnm: update dac ta moi. 30/06/2015
//				List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//				EquipPeriod equipPeriod = null;
//				if (lstPeriod == null || lstPeriod.size() == 0) {
//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//					result.put(ERROR, true);
//					return JSON;
//				} else if (lstPeriod.size() > 0) {
//					// gan gia tri ky dau tien.
//					equipPeriod = lstPeriod.get(0);
//				}
//				e.setEquipPeriod(equipPeriod);
				
				// thanh ly
				if (e.getStatus() != null && e.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
					List<EquipLiquidationFormDtl> lstDetailSendMail = new ArrayList<EquipLiquidationFormDtl>();
					// thanh ly
					if (StatusRecordsEquip.LIQUIDATED.getValue().equals(status)) {
						List<EquipLiquidationFormDtl> lstDetail = equipRecordMgr.getListEquipLiquidationFormDtlByRecordId(e.getId());
						if (lstDetail != null && !lstDetail.isEmpty()) {
							for (EquipLiquidationFormDtl detail : lstDetail) {
								Equipment equip = detail.getEquip();
								if (EquipTradeType.NOTICE_LOST.equals(equip.getTradeType())) {
									lstDetailSendMail.add(detail);
								}
							}
						}
						// trang thai tu duyet -> thanh ly
						statusRe = StatusRecordsEquip.LIQUIDATED.getValue();
						e.setStatus(status);
					}
					equipRecordMgr.saveEquipLiquidationForm(e, statusRe);
//					if (!lstDetailSendMail.isEmpty()) {
//						this.sendEmail(lstDetailSendMail);
//					}
				} else {
					// kiem tra chi tiet bien ban
					List<EquipLiquidationFormDtl> lstLiquidationFormDtls = new ArrayList<EquipLiquidationFormDtl>();
					BigDecimal sumPriceEquip = BigDecimal.ZERO;
					if (lstEquipCode != null && lstEquipCode.size() > 0) {					
						for (int i = 0, sz = lstEquipCode.size(); i < sz; i++) {
							EquipLiquidationFormDtl liquidationFormDtl = new EquipLiquidationFormDtl();
							Equipment equipment = null;
							if (!StringUtil.isNullOrEmpty(lstEquipCode.get(i))) {
								if(lstEquipCode.lastIndexOf(lstEquipCode.get(i)) != i) {
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("device.code") + " " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau") + "\n");
									return JSON;
								}
								equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
							}
							// kiem tra thiet bi
							if (equipment != null) {
								if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
									errMsg += R.getResource("common.catalog.status.in.active", R.getResource("device.code") + " " + equipment.getCode());
								} else {
									// 2 tr/hop: 1: tu bao mat sang va thiet bi dang o kho, su dung
									if (!(LinquidationStatus.WAITING_LIQUIDATION.getValue().equals(equipment.getLinquidationStatus()) && EquipTradeType.NOTICE_LOST.equals(equipment.getTradeType()))) {
										// dang o kho, dang su dung
										if (!EquipUsageStatus.SHOWING_WAREHOUSE.equals(equipment.getUsageStatus()) && !EquipUsageStatus.IS_USED.equals(equipment.getUsageStatus())) {
											errMsg += R.getResource("equipment.liquidation.row.invalid.not.in.kho.dangsudung", equipment.getCode()) + "\n";
										}
										/*
										 else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
											errMsg += R.getResource("device.code") + " " + equipment.getCode() + " " + R.getResource("equipment.manager.equipment.not.in.stock.vnm") + "\n";
										} else if (EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue()) && !equipment.getStockType().getValue().equals(EquipStockTotalType.KHO_VNM.getValue())) {
											errMsg += "Mã TB " + equipment.getCode() + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.vnm") + "\n";
										}*/ /*else if (StringUtil.isNullOrEmpty(equipment.getSerial())) {
											errMsg += "Mã TB " + equipment.getCode() + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.not.seri") + "\n";
										}*/
									}
								}
								if (!StringUtil.isNullOrEmpty(errMsg)) {
									result.put(ERROR, true);
									result.put("errMsg", errMsg);
									return JSON;
								}
								if(equipment.getPrice() != null){
									sumPriceEquip = sumPriceEquip.add(equipment.getPrice());
								}
							} else {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("common.catalog.code.not.exist",  R.getResource("device.code") + " " + lstEquipCode.get(i)));
								return JSON;
							}
							// vuongmq; 08/07/2015; neu ko co check Lot: 0 thi moi lay gia tri thanh ly, thu hoi o detail.
							// update by nhutnn, since 24/11/2015
							errMsg = "";
							if (checkLot != null) {
								if (IsCheckType.NO.getValue().equals(checkLot)) {
									// gia tri thanh ly
									if (lstLiquidationValue != null && !lstLiquidationValue.isEmpty()) {
										errMsg = validateLiquidationValue(i, liquidationFormDtl, equipment);
									}
									// gia tri thu hoi
									if (lstEvictionValue != null && !lstEvictionValue.isEmpty() && StringUtil.isNullOrEmpty(errMsg)) {
										errMsg = validateEvictionValue(i, liquidationFormDtl, equipment);
									}
								} else {
									// co check Lo thi gia tri Detail is null
									liquidationFormDtl.setLiquidationValue(null);
									liquidationFormDtl.setEvictionValue(null);
								}
							}
							if (errMsg.length() > 0) {
								result.put(ERROR, true);
								result.put("errMsg", errMsg);
								return JSON;
							}
							liquidationFormDtl.setEquip(equipment);
							liquidationFormDtl.setEquipCategory(equipment.getEquipGroup().getEquipCategory());
							liquidationFormDtl.setEquipGroup(equipment.getEquipGroup());
							// thong tin chi tiet bien ban
							liquidationFormDtl.setCreateUser(currentUser.getUserName());
							lstLiquidationFormDtls.add(liquidationFormDtl);
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("equipment.liquidation.ds.thong.tin.tb.k.ton.tai"));
						return JSON;
					}
					
					// vuongmq; 08/07/2015; them checkLot, gia tri thanh ly, thu hoi cua dong chinh
					// update by nhutnn, since 24/11/2015
					errMsg = "";
					if (checkLot != null && IsCheckType.YES.getValue().equals(checkLot) || IsCheckType.NO.getValue().equals(checkLot)) {
						e.setCheckLot(IsCheckType.parseValue(checkLot));
						// vuongmq; 08/07/2015; neu co check Lot: 1 thi moi lay gia tri thanh ly, thu hoi o detail.
						if (IsCheckType.YES.getValue().equals(checkLot)) {
							errMsg = validateSumLiquidationValue(e, sumPriceEquip);
							if (StringUtil.isNullOrEmpty(errMsg)) {
								errMsg = validateSumEvictionValue(e, sumPriceEquip);
							}
						} else {
							// khong co check Lo thi gia tri Chinh is null
							e.setLiquidationValue(null);
							e.setEvictionValue(null);
						}
					} else {
						errMsg = R.getResource("equipment.liquidation.check.lot");
					}
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					
					e.setUpdateUser(currentUser.getUserName());					
					// Xu ly file dinh kem
					List<Long> lstFileIdDelete = new ArrayList<Long>();
					Integer maxLength = 0;				
					EquipRecordFilter<FileVO> filterFile = new EquipRecordFilter<FileVO>();
					filterFile.setObjectId(idLiquidation);
					filterFile.setObjectType(EquipTradeType.LIQUIDATION.getValue());
					lstFileVo = equipRecordMgr.searchListFileVOByFilter(filterFile).getLstObject();
					if (lstFileVo == null || lstFileVo.isEmpty()) {
						lstFileVo = new ArrayList<FileVO>();
					}
					// Xu ly Xoa file dinh kem
					if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
						String[] arrEquipAttachFile = equipAttachFileStr.split(",");
						for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
							String value = arrEquipAttachFile[i];
							if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
								lstFileIdDelete.add(Long.parseLong(value));
							}
						}
					}
					maxLength = lstFileVo.size() - lstFileIdDelete.size();
					if (maxLength < 0) {
						maxLength = 0;
					}						
					errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					// luu file tam de xoa tren server
					ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
					if (lstFileIdDelete != null && lstFileIdDelete.size() > 0) {
						BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
						filterF.setLstId(lstFileIdDelete);
						filterF.setObjectId(idLiquidation);
						filterF.setObjectType(EquipTradeType.LIQUIDATION.getValue());
						objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
					}
					List<FileVO> lstfileVOs = saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType);
					EquipRecordFilter<EquipmentLiquidationFormVO> filter = new EquipRecordFilter<EquipmentLiquidationFormVO>();
					filter.setLstFileVo(lstfileVOs);
					filter.setLstEquipAttachFileId(lstFileIdDelete);
					equipRecordMgr.updateLiquidationForm(e, lstLiquidationFormDtls, statusRe, filter);
					
					// xoa file tren server
					if (lstFileIdDelete != null && lstFileIdDelete.size() > 0 && objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
						// Xu ly xoa cung file tren storeFileOnDisk
						List<String> lstFileName = new ArrayList<String>();
						for (EquipAttachFile itemFile : objVO.getLstObject()) {
							if (!StringUtil.isNullOrEmpty(itemFile.getUrl())) {
								lstFileName.add(itemFile.getUrl());
							}
						}
						FileUtility.deleteStoreFileOnDisk(lstFileName, Configuration.getFileEquipServerDocPath());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.ManageliquidationEquipmentAction.updateLiquidation"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		result.put("errMsg", errMsg);
		result.put("lstRecordNotSendMail", lstRecordNotSendMail);
		return JSON;
	}

	/**
	 * Kiem tra gia tri thu hoi
	 * 
	 * @author nhutnn
	 * @param i vi tri gia tri thu hoi
	 * @param liquidationFormDtl danh sach chi tiet 
	 * @param equipment thiet bi kiem tra
	 * @return thong bao loi
	 * @since 24/11/2015
	 */
	private String validateEvictionValue(int i, EquipLiquidationFormDtl liquidationFormDtl, Equipment equipment) {
		String errMsg = "";
		if (lstEvictionValue.get(i) != null && lstEvictionValue.get(i).compareTo(BigDecimal.ZERO) > 0) {
			if (equipment.getPrice() != null && lstEvictionValue.get(i).compareTo(equipment.getPrice()) > 0) {
				errMsg += R.getResource("device.code") + " " + lstEquipCode.get(i) + " " + R.getResource("equipment.eviction.value.less.price");
			} else if (lstEvictionValue.get(i).toString().length() > MAX_LENGTH_VALUE) {
				errMsg += R.getResource("device.code") + " " + lstEquipCode.get(i) + " " + R.getResource("equipment.eviction.value.length.err");
			} else {
				liquidationFormDtl.setEvictionValue(lstEvictionValue.get(i));
			}
		} else {
			liquidationFormDtl.setEvictionValue(null);
		}
		return errMsg;
	}

	/**
	 * Kiem tra gia tri thanh ly
	 * 
	 * @author nhutnn
	 * @param i vi tri gia tri thanh ly
	 * @param liquidationFormDtl danh sach chi tiet 
	 * @param equipment thiet bi kiem tra
	 * @return thong bao loi
	 * @since 24/11/2015
	 */
	private String validateLiquidationValue(int i, EquipLiquidationFormDtl liquidationFormDtl, Equipment equipment) {
		String errMsg = "";
		if (lstLiquidationValue.get(i) != null && lstLiquidationValue.get(i).compareTo(BigDecimal.ZERO) > 0) {
			if (equipment.getPrice() != null && lstLiquidationValue.get(i).compareTo(equipment.getPrice()) > 0) {
				errMsg += R.getResource("device.code") + " " + lstEquipCode.get(i) + " " + R.getResource("equipment.liquidation.value.less.price");
			} else if (lstLiquidationValue.get(i).toString().length() > MAX_LENGTH_VALUE) {
				errMsg += R.getResource("device.code") + " " + lstEquipCode.get(i) + " " + R.getResource("equipment.liquidation.value.length.err");
			} else {
				liquidationFormDtl.setLiquidationValue(lstLiquidationValue.get(i));
			}
		} else {
			liquidationFormDtl.setLiquidationValue(null);
		}
		return errMsg;
	}

	/**
	 * Kiem tra tong gia tri thu hoi
	 * 
	 * @author nhutnn
	 * @param equipLiquidation bien ban thu hoi
	 * @param sumPriceEquip tong gia thiet bi
	 * @return thong bao loi
	 * @since 24/11/2015
	 */
	private String validateSumEvictionValue(EquipLiquidationForm equipLiquidation, BigDecimal sumPriceEquip) {
		String errMsg = "";
		if (evictionValue != null && evictionValue.compareTo(BigDecimal.ZERO) > 0 && sumPriceEquip.compareTo(BigDecimal.ZERO) > 0) {
			if (evictionValue.compareTo(sumPriceEquip) > 0) {
				errMsg = R.getResource("equipment.eviction.value.sum.less.price");
			} else if (evictionValue.toString().length() > MAX_LENGTH_VALUE) {
				errMsg = R.getResource("equipment.eviction.value.sum.length.err");
			} else {
				equipLiquidation.setEvictionValue(evictionValue);
			}
		} else {
			equipLiquidation.setEvictionValue(null);
		}
		return errMsg;
	}

	/**
	 * Kiem tra Tong gia tri thanh ly
	 * 
	 * @author nhutnn
	 * @param equipLiquidation bien ban thanh ly
	 * @param sumPriceEquip tong gia thiet bi
	 * @return thong bao loi
	 * @since 24/11/2015
	 */
	private String validateSumLiquidationValue(EquipLiquidationForm equipLiquidation, BigDecimal sumPriceEquip) {
		String errMsg = "";
		if (liquidationValue != null && liquidationValue.compareTo(BigDecimal.ZERO) > 0 && sumPriceEquip.compareTo(BigDecimal.ZERO) > 0) {
			if (liquidationValue.compareTo(sumPriceEquip) > 0) {
				errMsg = R.getResource("equipment.liquidation.value.sum.less.price");
			} else if (liquidationValue.toString().length() > MAX_LENGTH_VALUE) {
				errMsg = R.getResource("equipment.liquidation.value.sum.length.err");
			} else {
				equipLiquidation.setLiquidationValue(liquidationValue);
			}
		} else {
			equipLiquidation.setLiquidationValue(null);
		}
		return errMsg;
	}

	/**
	 * Tao moi bien ban thanh ly tai san
	 * @author nhutnn
	 * @since 07/01/2015
	 * @return
	 */
	
	/**
	 * Tao moi bien ban thanh ly tai san
	 * @author vuongmq
	 * @since 08/07/2015
	 * @return
	 */
	public String createLiquidation() {
		resetToken(result);
		actionStartTime = DateUtil.now();
		boolean error = true;
		String errMsg = "";
		try {
			EquipLiquidationForm equipLiquidationForm = new EquipLiquidationForm();
			if (!StringUtil.isNullOrEmpty(docNumber)) {
				errMsg = ValidateUtil.validateField(docNumber.trim(), "equipment.manager.doc.number", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT, ConstantManager.ERR_MAX_LENGTH);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipLiquidationForm.setDocNumber(docNumber);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.doc.number")));
				return JSON;
			}
			
			Date cDate = null;
			if (!StringUtil.isNullOrEmpty(createDate)) {
				cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				equipLiquidationForm.setCreateFormDate(cDate);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(buyerName)) {
				errMsg = ValidateUtil.validateField(buyerName, "equipment.manager.buyer.name", 250, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipLiquidationForm.setBuyerName(buyerName);
			}
			if (!StringUtil.isNullOrEmpty(buyerPhone)) {
				errMsg = ValidateUtil.validateField(buyerPhone, "equipment.manager.buyer.phone", 50, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER, ConstantManager.ERR_MAX_LENGTH);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipLiquidationForm.setBuyerPhoneNumber(buyerPhone);
			}
			if (!StringUtil.isNullOrEmpty(buyerAddress)) {
				errMsg = ValidateUtil.validateField(buyerAddress, "equipment.manager.buyer.address", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipLiquidationForm.setBuyerAddress(buyerAddress);
			}
			if (!StringUtil.isNullOrEmpty(reason)) {
				errMsg = ValidateUtil.validateField(reason, "equipment.manager.reason", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipLiquidationForm.setReason(reason);
			}
			if (!StringUtil.isNullOrEmpty(note)) {
				errMsg = ValidateUtil.validateField(note, "common.description.lable.gc", 500, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				equipLiquidationForm.setNote(note);
			}
			if (status != null && (StatusRecordsEquip.APPROVED.getValue().equals(status) || StatusRecordsEquip.DRAFT.getValue().equals(status) || StatusRecordsEquip.LIQUIDATED.getValue().equals(status))) {
				equipLiquidationForm.setStatus(status);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.status.record")));
				return JSON;
			}			
			
			// kiem tra chi tiet bien ban			
			List<EquipLiquidationFormDtl> lstLiquidationFormDtls = new ArrayList<EquipLiquidationFormDtl>();
			BigDecimal sumPriceEquip = BigDecimal.ZERO;
			if (lstEquipCode != null && lstEquipCode.size() > 0) {	
				for (int i = 0, sz = lstEquipCode.size(); i < sz; i++) {
					EquipLiquidationFormDtl liquidationFormDtl = new EquipLiquidationFormDtl();
					Equipment equipment = null;
					if (!StringUtil.isNullOrEmpty(lstEquipCode.get(i))) {
						if(lstEquipCode.lastIndexOf(lstEquipCode.get(i)) != i) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("device.code") + " " + lstEquipCode.get(i) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau") + "\n");
							return JSON;
						}
						equipment = equipmentManagerMgr.getEquipmentEntityByCode(lstEquipCode.get(i));
					}
					// kiem tra thiet bi
					if (equipment != null) {
						if (!StatusType.ACTIVE.getValue().equals(equipment.getStatus().getValue())) {
							errMsg += R.getResource("common.catalog.status.in.active", R.getResource("device.code") + " " + equipment.getCode());
						} else {
							// 2 tr/hop: 1: tu bao mat sang va thiet bi dang o kho, su dung
							if (!(LinquidationStatus.WAITING_LIQUIDATION.getValue().equals(equipment.getLinquidationStatus()) && (EquipTradeType.NOTICE_LOST.equals(equipment.getTradeType()) || 
									EquipTradeType.NOTICE_LOSS_MOBILE.equals(equipment.getTradeType())))) {
								// dang o kho, dang su dung
								if (EquipTradeStatus.TRADING.getValue().equals(equipment.getTradeStatus())) {
									errMsg += R.getResource("device.code") + " " + equipment.getCode() + " " + R.getResource("equipment.manager.equipment.trade.status") + "\n";
								} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.equals(equipment.getUsageStatus()) && !EquipUsageStatus.IS_USED.equals(equipment.getUsageStatus())) {
									errMsg += R.getResource("equipment.liquidation.row.invalid.not.in.kho.dangsudung", equipment.getCode()) + "\n";
								}
								/*
								 else if (EquipStockTotalType.KHO_KH.equals(equipment.getStockType())) {
									errMsg += R.getResource("device.code") + " " + equipment.getCode() + " " + R.getResource("equipment.manager.equipment.not.in.stock.vnm") + "\n";
								} else if (EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(equipment.getUsageStatus().getValue()) && !equipment.getStockType().getValue().equals(EquipStockTotalType.KHO_VNM.getValue())) {
									errMsg += "Mã TB " + equipment.getCode() + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.not.in.stock.vnm") + "\n";
								}*/ /*else if (StringUtil.isNullOrEmpty(equipment.getSerial())) {
									errMsg += "Mã TB " + equipment.getCode() + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.not.seri") + "\n";
								}*/
							}
						}
						if (!StringUtil.isNullOrEmpty(errMsg)) {
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						if(equipment.getPrice() != null){
							sumPriceEquip = sumPriceEquip.add(equipment.getPrice());
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("common.catalog.code.not.exist",  R.getResource("device.code") + " " + lstEquipCode.get(i)));
						return JSON;
					}
					// vuongmq; 08/07/2015; neu ko co check Lot: 0 thi moi lay gia tri thanh ly, thu hoi o detail.
					// update by nhutnn, since 24/11/2015
					errMsg = "";
					if (checkLot != null ) {
						if (IsCheckType.NO.getValue().equals(checkLot)) {
							// gia tri thanh ly
							if (lstLiquidationValue != null && !lstLiquidationValue.isEmpty()) {
								errMsg = validateLiquidationValue(i, liquidationFormDtl, equipment);
							}
							// gia tri thu hoi
							if (lstEvictionValue != null && !lstEvictionValue.isEmpty() && StringUtil.isNullOrEmpty(errMsg)) {
								errMsg = validateEvictionValue(i, liquidationFormDtl, equipment);
							}
						} else {
							// co check Lo thi gia tri Detail is null
							liquidationFormDtl.setLiquidationValue(null);
							liquidationFormDtl.setEvictionValue(null);
						}
					}
					if (errMsg.length() > 0) {
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					liquidationFormDtl.setEquip(equipment);
					liquidationFormDtl.setEquipCategory(equipment.getEquipGroup().getEquipCategory());
					liquidationFormDtl.setEquipGroup(equipment.getEquipGroup());
					// thong tin chi tiet bien ban
					liquidationFormDtl.setCreateUser(currentUser.getUserName());
					lstLiquidationFormDtls.add(liquidationFormDtl);
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("equipment.liquidation.ds.thong.tin.tb.k.ton.tai"));
				return JSON;
			}
						
			// vuongmq; 08/07/2015; them checkLot, gia tri thanh ly, thu hoi cua dong chinh
			// update by nhutnn, since 24/11/2015
			errMsg = "";
			if (checkLot != null && IsCheckType.YES.getValue().equals(checkLot) || IsCheckType.NO.getValue().equals(checkLot)) {
				equipLiquidationForm.setCheckLot(IsCheckType.parseValue(checkLot));
				// vuongmq; 08/07/2015; neu co check Lot: 1 thi moi lay gia tri thanh ly, thu hoi o detail.
				if (IsCheckType.YES.getValue().equals(checkLot)) {
					errMsg = validateSumLiquidationValue(equipLiquidationForm, sumPriceEquip);
					if (StringUtil.isNullOrEmpty(errMsg)) {
						errMsg = validateSumEvictionValue(equipLiquidationForm, sumPriceEquip);
					}
				} else {
					// khong co check Lo thi gia tri Chinh is null
					equipLiquidationForm.setLiquidationValue(null);
					equipLiquidationForm.setEvictionValue(null);
				}
			} else {
				errMsg = R.getResource("equipment.liquidation.check.lot");
			}
			if (errMsg.length() > 0) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
			
			equipLiquidationForm.setCreateUser(currentUser.getUserName());
			//tamvnm: update dac ta moi. 30/06/2015
//			List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//			EquipPeriod equipPeriod = null;
//			if (lstPeriod == null || lstPeriod.size() == 0) {
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//				result.put(ERROR, error);
//				return JSON;
//			} else if (lstPeriod.size() > 0) {
//				// gan gia tri ky dau tien.
//				equipPeriod = lstPeriod.get(0);
//			}
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			equipLiquidationForm.setEquipPeriod(equipPeriod);
			
			if (lstFile!=null && lstFile.size() > 0) {
				errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, 0);
				if (errMsg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
			}
			
			List<FileVO> lstfileVOs = saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType);
			EquipRecordFilter<EquipmentLiquidationFormVO> filter = new EquipRecordFilter<EquipmentLiquidationFormVO>();
			filter.setLstFileVo(lstfileVOs);
			List<EquipLiquidationFormDtl> lstDetailSendMail = new ArrayList<EquipLiquidationFormDtl>();
			if (lstLiquidationFormDtls != null) {
				for (EquipLiquidationFormDtl detail : lstLiquidationFormDtls) {
					Equipment equip = detail.getEquip();
					if (EquipTradeType.NOTICE_LOST.equals(equip.getTradeType())) {
						lstDetailSendMail.add(detail);
					}
				}
			}
			equipLiquidationForm = equipRecordMgr.createLiquidationForm(equipLiquidationForm, lstLiquidationFormDtls, filter);
			if (equipLiquidationForm != null) {
//				if (StatusRecordsEquip.LIQUIDATED.getValue().equals(equipLiquidationForm.getStatus())) {
//					if (!lstDetailSendMail.isEmpty()) {
//						this.sendEmail(lstDetailSendMail);
//					}
//				}
				error = false;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.ManageliquidationEquipmentAction.createLiquidation"), createLogErrorStandard(actionStartTime));
		}
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		result.put("lstRecordNotSendMail", lstRecordNotSendMail);
		result.put("errMsg", errMsg);
		result.put(ERROR, error);
		return JSON;
	}

	/**
	 * Lay danh sach thiet bi trong 1 bien ban thanh ly
	 * @author nhutnn
	 * @since 07/01/2015
	 * @return
	 */
	public String searchEquipmentInForm() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(null);
			if (idLiquidation != null) {
				equipmentFilter.setIdLiquidation(idLiquidation);
			}
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipRecordMgr.getListEquipmentLiquidationVOByFilter(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getLstObject().size());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.searchEquipmentInForm"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Tim kiem danh sach thiet bi bien ban thanh ly
	 * @author nhutnn
	 * @since December 15,2014
	 * vuongmq; kiem tra 07/08/2015; khong xai ac tion này
	 */
	/*public String searchEquipmentLiquidation() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			KPaging<EquipmentDeliveryVO> kPaging = new KPaging<EquipmentDeliveryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (categoryId != null) {
				equipmentFilter.setEquipCategoryId(categoryId);
			}
			if (groupId != null) {
				equipmentFilter.setEquipGroupId(groupId);
			}
			if (providerId != null) {
				equipmentFilter.setEquipProviderId(providerId);
			}
			if (yearManufacture != null) {
				equipmentFilter.setYearManufacture(yearManufacture);
			}
			if (!StringUtil.isNullOrEmpty(stockCode)) {
				equipmentFilter.setStockCode(stockCode);
			}
			List<String> lstEDelete = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
				String[] lst = lstEquipDelete.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEDelete.add(lst[i]);
				}
				equipmentFilter.setLstEquipDelete(lstEDelete);
			}
			List<String> lstEAdd = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipAdd))) {
				String[] lst = lstEquipAdd.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				equipmentFilter.setLstEquipAdd(lstEAdd);
			}
			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			//equipmentFilter.setStockType(EquipStockTotalType.KHO_VNM.getValue());
			equipmentFilter.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipRecordMgr.getListEquipmentLiquidationVOByFilter(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getkPaging().getTotalRows());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return JSON;
	}*/

	
	/**
	 * Tim kiem danh sach thiet bi popup thanh ly tai san
	 * lay thiet bi: NPP, KH, va thiet bi giao dich la bao mat, cho thanh ly
	 * @author vuongmq
	 * @since 08/07/2015
	 */
	public String searchEquipmentLiquidationPopup() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			KPaging<EquipmentDeliveryVO> kPaging = new KPaging<EquipmentDeliveryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (categoryId != null) {
				equipmentFilter.setEquipCategoryId(categoryId);
			}
			if (groupId != null) {
				equipmentFilter.setEquipGroupId(groupId);
			}
			if (providerId != null) {
				equipmentFilter.setEquipProviderId(providerId);
			}
			if (yearManufacture != null) {
				equipmentFilter.setYearManufacture(yearManufacture);
			}
			if (!StringUtil.isNullOrEmpty(stockCode)) {
				equipmentFilter.setStockCode(stockCode);
			}
			if (!StringUtil.isNullOrEmpty(equipLendCode)) {
				equipmentFilter.setEquipLendCode(equipLendCode);
			} 
//			List<String> lstEDelete = new ArrayList<String>();
//			if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
//				String[] lst = lstEquipDelete.toString().split(",");
//				for (int i = 0; i < lst.length; i++) {
//					lstEDelete.add(lst[i]); 
//				}
//				equipmentFilter.setLstEquipDelete(lstEDelete);
//			}
			List<String> lstECode = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipmentCode))) {
				String[] lst = lstEquipmentCode.toString().split(", ");
				for (int i = 0; i < lst.length; i++) {
					lstECode.add(lst[i].trim());
				}
				equipmentFilter.setLstEquipmentCode(lstECode);
			}
			List<String> lstEAdd = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipAdd))) {
				String[] lst = lstEquipAdd.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				if (equipmentFilter.getLstEquipmentCode() != null && equipmentFilter.getLstEquipmentCode().size() > 0) {
					lstEAdd.addAll(lstECode);
				}
				equipmentFilter.setLstEquipAdd(lstEAdd);
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				equipmentFilter.setShopCode(shopCode);
			}
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				// filter CustomerCode la MaKH
				equipmentFilter.setCustomerCode(shortCode);
			}
			equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			// trang thai se su dung flagNotUsageStatus: null se la: 2: dang o kho va 3: dang su dung
			if (isSearch != null && ActiveType.RUNNING.getValue().equals(isSearch)) {
				// tim kiem thi flagNotUsageStatus: true
				equipmentFilter.setFlagNotUsageStatus(true);
			} else {
				equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
				equipmentFilter.setFlagNotUsageStatus(false);
			}
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipRecordMgr.getListEquipmentLiquidationPopup(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getkPaging().getTotalRows());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.searchEquipmentLiquidationPopup"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}
	/**
	 * Xuat excel bien ban giao nhan
	 * @author tamvnm
	 * @since 13/01/2015
	 * @return
	 */
	public String exportRecordLiquiDation() {
		try {
			if (lstIdRecord == null || lstIdRecord.size() == 0) {
				result.put("hasData", false);
				return JSON;
			}
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			List<EquipmentLiquidationFormVO> equipmentLiquidationFormVOs = equipmentManagerMgr.getListEquipmentLiquidationFormVOForExport(lstIdRecord);

			SXSSFWorkbook workbook = null;
			FileOutputStream out = null;
			try {

				if (null != equipmentLiquidationFormVOs && equipmentLiquidationFormVOs.size() > 0) {
					// Init XSSF workboook
					String outputName = ConstantManager.TEMPLATE_EQUIP_LIST_LIQUIDATION_EXPORT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;

					workbook = new SXSSFWorkbook(200);
					workbook.setCompressTempFiles(true);
					// Tao shett
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("equipment.liquidation.thanh_ly_tai_san_sheet"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

					// Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(18);
					// set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 150, 200, 100, 200);
					// Size Row
					ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 25);
					sheetData.setDisplayGridlines(true);

					int iRow = 0, iColumn = 0;
					// tao column header
					String headers = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.equipment.header.export.liquidation");
					String[] lstHeaders = headers.split(";");
					for (int i = 0; i < lstHeaders.length; i++) {
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, lstHeaders[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					String maPhieu = "";
					iRow++;
					for (int i = 0; i < equipmentLiquidationFormVOs.size(); i++) {
						EquipmentLiquidationFormVO item = equipmentLiquidationFormVOs.get(i);
						/*if(i>0){
							EquipmentLiquidationFormVO rowPrev = equipmentLiquidationFormVOs.get(i-1);
							EquipmentLiquidationFormVO row = equipmentLiquidationFormVOs.get(i);
							if(rowPrev.getLiquidationCode()!=null && rowPrev.getLiquidationCode().equals(row.getLiquidationCode()) 
									&& rowPrev.getEquip_liquidation_doc()!=null && rowPrev.getEquip_liquidation_doc().equals(row.getEquip_liquidation_doc())){
								equipmentLiquidationFormVOs.get(i).setLiquidationCode("");
								equipmentLiquidationFormVOs.get(i).setEquip_liquidation_doc("");
								equipmentLiquidationFormVOs.get(i).setStatus(null);
								equipmentLiquidationFormVOs.get(i).setBuyerAddress(null);
								equipmentLiquidationFormVOs.get(i).setBuyerName(null);
								equipmentLiquidationFormVOs.get(i).setBuyerPhoneNumber(null);
							}
						}*/
						if (!maPhieu.equals(item.getLiquidationCode())) {
							maPhieu = item.getLiquidationCode();
//							iRow++; // de cach 1 dong trong
						} else {
							item.setLiquidationCode("");
							item.setEquip_liquidation_doc("");
							item.setCreateDate(null); // cot nay lay export la: create_form_date
							item.setLiquidationValueSum(null);
							item.setEvictionValueSum(null);
							item.setStatus(null);
							item.setReasonLiquidation(null);
							item.setBuyerAddress(null);
							item.setBuyerName(null);
							item.setBuyerPhoneNumber(null);
							item.setNote(null);
						}
						iColumn = 0;
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getLiquidationCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getEquip_liquidation_doc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getCreateDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						/*if (item.getEquipValue() != null) {
							mySheet.addCell(sheetData, iColumn++, iRow, Double.parseDouble(item.getEquipValue().toString()), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						} else {
							mySheet.addCell(sheetData, iColumn++, iRow, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						}*/
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getLiquidationValueSum(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_NOT_ZERO));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getEvictionValueSum(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_NOT_ZERO));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getLiquidationValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_NOT_ZERO));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getEvictionValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_NOT_ZERO));
						// String date =
						// DateUtil.toDateString(equipmentRecordDeliveryVOs.get(i).getDateContract(),
						// DateUtil.DATE_FORMAT_DDMMYYYY);
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getReasonLiquidation(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						String status = "";
						if (item.getStatus() != null) {
							if (item.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
								status = R.getResource("action.status.duyet");
							} else if (item.getStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
								status = R.getResource("action.status.duthao");
							} else if (item.getStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
								status = R.getResource("action.status.huy");
							} else if (item.getStatus().equals(StatusRecordsEquip.LIQUIDATED.getValue())) {
								status = R.getResource("action.status.dathanhly");
							}
						}
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, status, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getBuyerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getBuyerPhoneNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getBuyerAddress(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, iColumn++, iRow, item.getNote(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						iRow++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null");
					result.put(ERROR, true);
					result.put("data.hasData", true);
					result.put("errMsg", errMsg);
				}
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.exportRecordLiquiDation"), createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				if (workbook != null) {
					workbook.dispose();
				}
				if (out != null) {
					out.close();
				}
			}
			System.gc();
			return JSON;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.exportRecordLiquiDation"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Lay mot thiet bi
	 * 
	 * @author nhutnn
	 * @since 19/12/2014
	 * @return
	 * Lay thiet bi khi nhap vao textbox nhan Enter
	 * Lay ma thiet bi giong nhu search popup thiet bi them moi
	 */
	public String getEquipmentBindTexbox() {
		try {
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(null);
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			/*if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}*/
			/*List<String> lstEDelete = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipDelete))) {
				String[] lst = lstEquipDelete.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEDelete.add(lst[i]);
				}
				equipmentFilter.setLstEquipDelete(lstEDelete);
			}*/
			List<String> lstEAdd = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty((lstEquipAdd))) {
				String[] lst = lstEquipAdd.toString().split(",");
				for (int i = 0; i < lst.length; i++) {
					lstEAdd.add(lst[i]);
				}
				equipmentFilter.setLstEquipAdd(lstEAdd);
			}
			equipmentFilter.setStaffRoot(currentUser.getStaffRoot().getStaffId());
			equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			// trang thai se su dung flagNotUsageStatus: null se la: 2: dang o kho va 3: dang su dung
			if (isSearch != null && ActiveType.RUNNING.getValue().equals(isSearch)) {
				// tim kiem thi flagNotUsageStatus: true
				equipmentFilter.setFlagNotUsageStatus(true);
			} else {
				equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
				equipmentFilter.setFlagNotUsageStatus(false);
			}
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipRecordMgr.getListEquipmentLiquidationPopup(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				equipments = equipmentDeliverys.getLstObject();
			}
			/*ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentByRole(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				equipments = equipmentDeliverys.getLstObject();
			}*/
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.getEquipmentBindTexbox"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Lay mot vai danh muc cua thiet bi: nhom, ncc, loai
	 * 
	 * @author nhutnn
	 * @since 20/12/2014
	 * @return
	 */
	public String getEquipmentCatalog() {
		try {
			EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
			equipmentFilter.setkPaging(null);
			List<EquipmentVO> equipsCategory = new ArrayList<EquipmentVO>();
			List<EquipmentVO> equipProviders = new ArrayList<EquipmentVO>();
			equipsCategory = equipmentManagerMgr.getListEquipmentCategory(equipmentFilter);
			equipProviders = equipmentManagerMgr.getListEquipmentProvider(equipmentFilter);

			result.put("equipsCategory", equipsCategory);
			result.put("equipProviders", equipProviders);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.getEquipmentCatalog"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Lay nhom thiet bi
	 * 
	 * @author nhutnn
	 * @since 20/12/2014
	 * @return
	 */
	public String getEquipmentGroup() {
		try {
			EquipmentFilter<EquipmentVO> equipmentFilter = new EquipmentFilter<EquipmentVO>();
			equipmentFilter.setkPaging(null);
			List<EquipmentVO> equipGroups = new ArrayList<EquipmentVO>();
			if (id != null) {
				equipmentFilter.setEquipCategoryId(id);
			}
			equipmentFilter.setStatus(ActiveType.RUNNING.getValue());
			ObjectVO<EquipmentVO> eGroup = equipmentManagerMgr.searchEquipmentGroupVObyFilter(equipmentFilter);
			if (eGroup.getLstObject() != null) {
				equipGroups = eGroup.getLstObject();
			}
			result.put("equipGroups", equipGroups);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.getEquipmentGroup"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Kiem tra ki
	 * 
	 * @author nhutnn
	 * @since 22/12/2014
	 * @return
	 */
//	public String checkEquipPeriod() {
//		try {
//			result.put(ERROR, false);
//			//if (id == null || id <= 0) {
//				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPeriod == null) {
//					result.put(ERROR, true);
//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.liquidation.not.open.period"));
//				}
////			} else {
////				EquipLiquidationForm equipLiquidationForm = equipmentManagerMgr.getEquipLiquidationFormById(id);
////				if (equipLiquidationForm.getEquipPeriod() == null || !equipLiquidationForm.getEquipPeriod().getStatus().getValue().equals(EquipPeriodType.OPENED.getValue()) 
////					|| equipLiquidationForm.getEquipPeriod().getFromDate().after(DateUtil.now()) || equipLiquidationForm.getEquipPeriod().getToDate().before(DateUtil.now())) {
////					result.put(ERROR, true);
////					result.put("errMsg", "Kỳ của biên bản không hợp lệ để cho chỉnh sửa!");
////				}
////			}
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.checkEquipPeriod"), createLogErrorStandard(actionStartTime));
//			result.put(ERROR, true);
//			result.put("errMsg", "Kỳ của biên bản không hợp lệ để cho chỉnh sửa!");
//		}
//		return JSON;
//	}

	/**
	 * Upload File
	 * 
	 * @author nhutnn
	 * @since 11/02/2015
	 * */
	private List<FileVO> saveUploadFileByEquip(List<File> lstFile, List<String> lstFileFileName, List<String> lstFileContentType) {
		try {
			if (lstFile != null && lstFile.size() > 0) {
				//String perfix = Configuration.getPrefixFileEquipServerDocPath();
				List<FileVO> lstFileVO = FileUtility.storeFileOnDisk(lstFile, lstFileFileName, lstFileContentType, null, EquipTradeType.LIQUIDATION.getValue());
				if (lstFileVO != null && !lstFileVO.isEmpty()) {
					String thumbUrl = Configuration.getEquipThumbUrlAttachFile();
					for (FileVO voFile : lstFileVO) {
						voFile.setThumbUrl(thumbUrl);
					}
				}
				return lstFileVO;
			}
			System.gc();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.ManageLiquidationEquipmentAction.saveUploadFileByEquip"), createLogErrorStandard(actionStartTime));
		}
		return new ArrayList<FileVO>();
	}
	
	// TODO Xu ly GETTER/SETTER
	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<Long> getLstIdForm() {
		return lstIdForm;
	}

	public void setLstIdForm(List<Long> lstIdForm) {
		this.lstIdForm = lstIdForm;
	}

	public List<EquipmentDeliveryVO> getEquipments() {
		return equipments;
	}

	public void setEquipments(List<EquipmentDeliveryVO> equipments) {
		this.equipments = equipments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public List<String> getLstEquipCode() {
		return lstEquipCode;
	}

	public void setLstEquipCode(List<String> lstEquipCode) {
		this.lstEquipCode = lstEquipCode;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getLstEquipAdd() {
		return lstEquipAdd;
	}

	public void setLstEquipAdd(String lstEquipAdd) {
		this.lstEquipAdd = lstEquipAdd;
	}

	public String getLstEquipDelete() {
		return lstEquipDelete;
	}

	public void setLstEquipDelete(String lstEquipDelete) {
		this.lstEquipDelete = lstEquipDelete;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public Long getIdLiquidation() {
		return idLiquidation;
	}

	public void setIdLiquidation(Long idLiquidation) {
		this.idLiquidation = idLiquidation;
	}

	public List<BigDecimal> getLstValueEquip() {
		return lstValueEquip;
	}

	public void setLstValueEquip(List<BigDecimal> lstValueEquip) {
		this.lstValueEquip = lstValueEquip;
	}

	public String getBuyerAddress() {
		return buyerAddress;
	}

	public void setBuyerAddress(String buyerAddress) {
		this.buyerAddress = buyerAddress;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerPhone() {
		return buyerPhone;
	}

	public void setBuyerPhone(String buyerPhone) {
		this.buyerPhone = buyerPhone;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}

	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}

	public List<File> getLstFile() {
		return lstFile;
	}

	public void setLstFile(List<File> lstFile) {
		this.lstFile = lstFile;
	}

	public List<String> getLstFileContentType() {
		return lstFileContentType;
	}

	public void setLstFileContentType(List<String> lstFileContentType) {
		this.lstFileContentType = lstFileContentType;
	}

	public List<String> getLstFileFileName() {
		return lstFileFileName;
	}

	public void setLstFileFileName(List<String> lstFileFileName) {
		this.lstFileFileName = lstFileFileName;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public String getEquipAttachFileStr() {
		return equipAttachFileStr;
	}

	public void setEquipAttachFileStr(String equipAttachFileStr) {
		this.equipAttachFileStr = equipAttachFileStr;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public List<BigDecimal> getLstLiquidationValue() {
		return lstLiquidationValue;
	}

	public void setLstLiquidationValue(List<BigDecimal> lstLiquidationValue) {
		this.lstLiquidationValue = lstLiquidationValue;
	}

	public List<BigDecimal> getLstEvictionValue() {
		return lstEvictionValue;
	}

	public void setLstEvictionValue(List<BigDecimal> lstEvictionValue) {
		this.lstEvictionValue = lstEvictionValue;
	}

	public BigDecimal getLiquidationValue() {
		return liquidationValue;
	}

	public void setLiquidationValue(BigDecimal liquidationValue) {
		this.liquidationValue = liquidationValue;
	}

	public BigDecimal getEvictionValue() {
		return evictionValue;
	}

	public void setEvictionValue(BigDecimal evictionValue) {
		this.evictionValue = evictionValue;
	}

	public String getLstEquipmentCode() {
		return lstEquipmentCode;
	}

	public void setLstEquipmentCode(String lstEquipmentCode) {
		this.lstEquipmentCode = lstEquipmentCode;
	}

	public String getEquipLendCode() {
		return equipLendCode;
	}

	public void setEquipLendCode(String equipLendCode) {
		this.equipLendCode = equipLendCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getIsSearch() {
		return isSearch;
	}

	public void setIsSearch(Integer isSearch) {
		this.isSearch = isSearch;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getLstRecordNotSendMail() {
		return lstRecordNotSendMail;
	}

	public void setLstRecordNotSendMail(String lstRecordNotSendMail) {
		this.lstRecordNotSendMail = lstRecordNotSendMail;
	}
	
}

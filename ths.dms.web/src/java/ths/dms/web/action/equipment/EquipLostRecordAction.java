package ths.dms.web.action.equipment;

/**
 * Import thu vien
 * */
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.SendMailBySite;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipLender;
import ths.dms.core.entities.EquipLostMobileRec;
import ths.dms.core.entities.EquipLostRecord;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.DeliveryType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.LinquidationStatus;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.ApParamEquipVO;
import ths.dms.core.entities.vo.EquipLostMobileRecVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.EquipmentLostRecordVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;
//import ths.dms.web.utils.SendMailBySite;

/**
 * Action Bao Mat
 * 
 * @author hunglm16
 * @since December 24,2014
 */
public class EquipLostRecordAction extends AbstractAction {

	private static final long serialVersionUID = 5330840248556870465L;

	/**
	 * Khai bao cac bien toan cuc
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * */
	CommonMgr commonMgr;
	CustomerMgr customerMgr;
	EquipRecordMgr equipRecordMgr;
	EquipmentManagerMgr equipmentManagerMgr;

	/**
	 * Xu ly cac ham thuc thi ket noi
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * */
	@Override
	public void prepare() throws Exception {
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");

		super.prepare();
	}

	/**
	 * Khai bao cac du lieu dung chung khong khai bao method
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * */
	@Override
	public String execute() throws Exception {
		generateToken();
		isLevel = currentUser.getShopRoot().getIsLevel();
		shopId = currentUser.getShopRoot().getShopId();
		/** vuongmq; 28/05/2015; dung cho bao mat tu mobile*/
		/** vuongmq; 28/05/2015; truong hop co shopCode la user Chi quan ly 1 shop; khi them moi thi disabled ma shopCode luon*/
		if (currentUser != null && currentUser.getListShop() != null && currentUser.getListShop().size() == 1) {
			shopCode = currentUser.getListShop().get(0).getShopCode();
		}
		return SUCCESS;
	}
//
//	/**
//	 * Tim kiem Bien Ban Bao Mat
//	 * 
//	 * @author hunglm16
//	 * @since December 31,2014
//	 * */
//	
	/**
	 * Tim kiem Bien Ban Bao Mat
	 * @author vuongmq
	 * @since Apr 25,2015
	 * cap nhat: lay them loai cua kho: 1: NPP, 2: KH
	 * dung chung cho Ql bao mat va phe duyet bao mat
	 * */
	public String searchEquipLostRecordVO() {
		result.put("rows", new ArrayList<EquipRecordVO>());
		result.put("total", 0);
		try {
			EquipRecordFilter<EquipRecordVO> filter = new EquipRecordFilter<EquipRecordVO>();
			//set cac tham so tim kiem
			KPaging<EquipRecordVO> kPaging = new KPaging<EquipRecordVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			//filter.setShopRootId(currentUser.getShopRoot().getShopId());
			/** vuongmq; 14/05/2015; them ma NPP; va staff dang nhap */
			if (currentUser != null) {
				filter.setStaffRoot(currentUser.getUserId());
			} else {
				// loi khong ton tai user dang nhap
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(arrShopTxt)) {
				String[] arrShopIdTxt = arrShopTxt.split(",");
				List<Long> listShopId = new ArrayList<Long>();
				for (String value : arrShopIdTxt) {
					if (StringUtil.isNumberInt(value)) {
						listShopId.add(Long.valueOf(value));
					}
				}
				if (listShopId != null && !listShopId.isEmpty()) {
					filter.setLstShopId(listShopId);
				}
			} else {
				//po.adjustment.message.not.user = Lỗi không tồn tại user đăng nhập; kiem tra hơ
				if (currentUser != null && currentUser.getShopRoot() != null) {
					List<Long> listShopId = new ArrayList<Long>();
					listShopId.add(currentUser.getShopRoot().getShopId());
					filter.setLstShopId(listShopId);
				} else {
					result.put("errMsg", R.getResource("po.adjustment.message.not.user"));
					return JSON;
				}
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				filter.setShortCode(shortCode);
			}
//			if (!StringUtil.isNullOrEmpty(customerName)) {
//				filter.setCustomerName(customerName);
//			}
//			if (!StringUtil.isNullOrEmpty(address)) {
//				filter.setAddress(address);
//			}
			if (!StringUtil.isNullOrEmpty(customerSearch)) {
				filter.setCustomerSearch(customerSearch);
			}
			
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				filter.setEquipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(serial)) {
				filter.setSerial(serial);
			}
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (recordStatus != null && recordStatus > ActiveType.DELETED.getValue()) {
				filter.setRecordStatus(recordStatus);
			}
			if (flagRecordStatus != null) {
				filter.setFlagRecordStatus(StatusRecordsEquip.WAITING_APPROVAL.getValue());
			}
			if (deliveryStatus != null && deliveryStatus > ActiveType.DELETED.getValue()) {
				filter.setDeliveryStatus(deliveryStatus);
			}
			if (stockType != null && stockType > ActiveType.DELETED.getValue()) {
				filter.setStockType(stockType);
			}
			/*filter.setIsLevel(currentUser.getShopRoot().getIsLevel());*/
			//goi ham thuc thi tim kiem
			ObjectVO<EquipRecordVO> objVO = equipRecordMgr.searchListEquipLostRecordVOByFilter(filter);
			if (objVO != null &&  !objVO.getLstObject().isEmpty()) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.searchEquipLostRecordVO()" + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Lay danh sach thiet bi voi ma hop dong neu co la moi nhat
	 * 
	 * @author hunglm16
	 * @since January 06, 2015
	 */
	/**
	 * Lay danh sach thiet bi co ban hop dong moi nhat; luc tao moi
	 * @author vuongmq
	 * @since 06/05,2015
	 * cap nhat: lay theo bao mat cua NPP or KH, chon thiet bi cua NPP or KH
	 * */
	public String searchEquipByEquipDeliveryRecordNew() {
		result.put("rows", new ArrayList<EquipRecordVO>());
		result.put("total", 0);
		try {
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			//set cac tham so tim kiem
			if (flagPagination != null && flagPagination) {
				KPaging<EquipmentVO> kPaging = new KPaging<EquipmentVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				filter.setkPaging(kPaging);
			}
			/** vuongmq; 14/05/2015; them ma NPP; va staff dang nhap */
			if (currentUser != null) {
				filter.setStaffRoot(currentUser.getUserId());
			} else {
				// loi khong ton tai user dang nhap
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
			Shop shopSearch = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(shopCode);
				shopSearch = shopMgr.getShopByCode(shopCode);
				if (shopSearch != null) {
					filter.setShopId(shopSearch.getId());
				} else {
					// đơn vị không tồn tại
					result.put("errMsg", R.getResource("po.conf.group.err.imp.isShop.isUnDefined"));
					return JSON;
				}
			} else {
				// Mã NPP không được để trống
				result.put("errMsg", R.getResource("imp.tuyen.clmn.error.maNPP.null"));
				return JSON;
			}
			if (equipId != null && equipId > ActiveType.STOPPED.getValue()) {
				filter.setEquipId(equipId);
			}
			if (eqDeliveryRecId != null && eqDeliveryRecId > ActiveType.STOPPED.getValue()) {
				filter.setEqDeliveryRecId(eqDeliveryRecId);
			}
			if (eqLostRecId != null && eqLostRecId > ActiveType.STOPPED.getValue()) {
				filter.setEquipLostId(eqLostRecId);
			}
			if (stockType != null) {
				filter.setStockType(stockType);
			}
			if (recordStatus != null) {
				filter.setRecordStatus(recordStatus);
			}
			if (tradeStatus != null) {
				filter.setTradeStatus(tradeStatus);
			}
			filter.setStatus(StatusType.ACTIVE.getValue());
			/** vuongmq; 06/05/2015; them ma kho, ma Thiet bi bao mat: 1: NPP, 2: KH*/
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				// co kho KH
				if (shopSearch != null) {
					String code = shortCode + "_" + shopSearch.getShopCode();
					Customer customer = customerMgr.getCustomerByCode(code);
					if (customer != null) {
						filter.setStockId(customer.getId());
					} else {
						//"Khách hàng không tồn tại."
						result.put("errMsg", R.getResource("equip.lost.recorde.err.kh.ko.ton.tai"));
						return JSON;
					}
				}
			}
			/** vuongmq; 1/05/2015; them ma Thiet bi de lay thiet bi*/
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				filter.setEquipCode(equipCode);
			}
			if (flagUpdate != null) {
				filter.setFlagUpdate(flagUpdate);
			}
			//goi ham thuc thi tim kiem
			ObjectVO<EquipmentVO> objVO = equipmentManagerMgr.searchEquipByEquipDeliveryRecordNew(filter);
			if (objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
				if (flagPagination != null && flagPagination) {
					result.put("total", objVO.getkPaging().getTotalRows());
				}
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.searchEquipByEquipDeliveryRecordNew()" + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * In phieu bao mat
	 * 
	 * @author hunglm16
	 * @since January 14, 2015
	 */
	public String printfEquiLostRec() {
		try {
			/**ATTT duong dan*/
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			result.put("error", true);
			if (StringUtil.isNullOrEmpty(arrEquipLostRec)) {
				errMsg = "Chưa có biên bản báo mất nào được chọn";
				result.put("errMsg", errMsg);
				return JSON;
			}
			String[] arrEquipLostRecId = arrEquipLostRec.split(",");
			List<Long> lstEquipLostRecId = new ArrayList<Long>();
			for (String value : arrEquipLostRecId) {
				if (ValidateUtil.validateNumber(value)) {
					lstEquipLostRecId.add(Long.valueOf(value));
				}
			}
			if (lstEquipLostRecId == null || lstEquipLostRecId.isEmpty()) {
				errMsg = "Biên bản báo mất được chọn không hợp lệ";
				result.put("errMsg", errMsg);
				return JSON;
			}

			EquipRecordFilter<EquipRecordVO> filter = new EquipRecordFilter<EquipRecordVO>();
			filter.setLstId(lstEquipLostRecId);
			//goi ham thuc thi tim kiem
			ObjectVO<EquipRecordVO> objVO = equipRecordMgr.printfEquipLostRecordVOByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				List<EquipRecordVO> lstData = objVO.getLstObject();
				//Shop shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				listObjectBeans = new ArrayList<Map<String, Object>>();
				for (EquipRecordVO row : lstData) {
					Map<String, Object> beans = new HashMap<String, Object>();
					beans.put("shopIsLevel5Code", row.getShopIsLevel5Code()); // vuongmq; 12/05/2015; npp
					beans.put("shopIsLevel3Code", row.getShopIsLevel3Code()); // vuongmq; 12/05/2015; mien
					//Ben VNM
					//beans.put("customerName", row.getShortCode() + " - " + row.getCustomerName());
					if (!StringUtil.isNullOrEmpty(row.getShortCode()) && !StringUtil.isNullOrEmpty(row.getCustomerName())) {
						beans.put("customerName", row.getShortCode() + " - " + row.getCustomerName());
					}
					beans.put("shortCode", row.getShortCode());
					beans.put("customerAddress", row.getCustomerAddress());
					beans.put("customerPhone", row.getCustomerPhone());
					beans.put("representor", row.getRepresentor());
					beans.put("chucVuBenMuon", row.getChucVuBenMuon());
					//Thiet Bi
					beans.put("contractNumber", row.getContractNumber());
					beans.put("manufacturingYear", row.getFirstDateInUseStr());
					beans.put("equipCategoryName", row.getEquipCategoryName());
					beans.put("equipGroupName", row.getEquipGroupName());
					//Thong tin bien ban bao mat
					beans.put("lostDateStr", row.getLostDateStr());
					beans.put("lastArisingSalesDateStr", row.getLastArisingSalesDateStr());
					beans.put("tracingPlace", row.getTracingPlace());
					beans.put("tracingResult", row.getTracingResult());
					beans.put("conclusion", row.getConclusion());
					beans.put("recommendedTreatment", row.getRecommendedTreatment());
					//Them hinh check, uncheck
					beans.put("urlUnChecked", ReportUtils.getVinamilkUncheckedSmall(request));
					beans.put("urlChecked", ReportUtils.getVinamilkCheckedSmall(request));
					addLoseEquipmentRecordHeader(beans);
					beans.put("equipments", getEquipmentLostRecordVO(row));
					listObjectBeans.add(beans);
				}
				// file in
				String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/print-template/";
				String templatePath = folder + ConstantManager.TEMPLATE_PRINT_EQUIP_LOST_RECORD + ".jasper";
				templatePath = templatePath.replace('/', File.separatorChar);
				File fileReport = new File(templatePath);
				if (!fileReport.exists()) {
					result.put(ERROR, true);
					result.put("errMsg", "Lỗi không tồn tại file template In, cập nhật mẫu bên chức năng Quản lý giao nhận !");
					return JSON;
				}
				// dat Parameters
				Map<String, Object> parameters = new HashMap<String, Object>();

				JRDataSource dataSource = new JRBeanCollectionDataSource(listObjectBeans);

				// file output
				String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(ConstantManager.TEMPLATE_PRINT_LOST_RECORD + ".pdf")).toString());

				//String outputPath = Configuration.getStoreRealPath() + outputFile;
				String outputPath = Configuration.getStoreRealPath() + outputFile;
				String downloadPath = Configuration.getExportExcelPath() + outputFile;

				JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);

				JRAbstractExporter exporter = new JRPdfExporter();

				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
				exporter.exportReport();

				// set link
				session.setAttribute("downloadPath", downloadPath);

				result.put(ERROR, false);
				result.put("hasData", true);
				//result.put(REPORT_PATH, outputPath);
				result.put(REPORT_PATH, downloadPath);
				/**ATTT duong dan*/
				MemcachedUtils.putValueToMemcached(reportToken, downloadPath, retrieveReportMemcachedTimeout());
			} else {
				errMsg = "Biên bản báo mất được chọn không hợp lệ";
				result.put("errMsg", errMsg);
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.printfEquiLostRec()" + e.getMessage());
			return JSON;
		}
		result.put("error", false);
		return JSON;
	}

	private List<EquipmentLostRecordVO> getEquipmentLostRecordVO(EquipRecordVO row) {
		List<EquipmentLostRecordVO> vos = new ArrayList<EquipmentLostRecordVO>();
		EquipmentLostRecordVO vo = new EquipmentLostRecordVO();
		final String DEFAULT_QUANTITY = "01";
		vo.setEquipmentQuantity(DEFAULT_QUANTITY);
		vo.setContractNumber(row.getContractNumber());
		vo.setProvideYear(row.getFirstDateInUseStr());
		vo.setEquipmentType(row.getEquipCategoryName());
		vo.setEquipmentModel(row.getEquipGroupName());
		vos.add(vo);
		return vos;
	}

	private void addLoseEquipmentRecordHeader(Map<String, Object> beans) throws BusinessException {
		EquipLender equipLender = commonMgr.getFirtInformationEquipLender (currentUser.getShopRoot().getShopId());
		String headerCompany = "", lender = "", lenderAddress = "",
				lenderRepresentation = "", lenderPosition = "", lenderRole = "",
				lenderPhone = "";
		if (equipLender != null) {
			headerCompany = equipLender.getName();
			lender = equipLender.getName();
			lenderAddress = equipLender.getAddress();
			lenderRepresentation = equipLender.getRepressentative();
			lenderPosition = equipLender.getPosition();
			//lenderRole = "Phu trach";
			lenderPhone = equipLender.getPhone();
		}
		beans.put("headerCompany", headerCompany);
		beans.put("lender", lender);
		beans.put("lenderAddress", lenderAddress);
		beans.put("lenderRepresentation", lenderRepresentation);
		beans.put("lenderPosition", lenderPosition);
		beans.put("lenderRole", lenderRole);
		beans.put("lenderPhone", lenderPhone);
	}

	/**
	 * Xuat Excel theo Tim kiem bao mat thiet bi
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * @return Excel
	 * @throws IOException 
	 * */
	public String exportBySearchEquipLostRecordVO() throws IOException {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			/*begin vuongmq; 12/05/2015; xuat Excel ATTT*/
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			/*end vuongmq; 11/05/2015; xuat Excel ATTT*/
			EquipRecordFilter<EquipRecordVO> filter = new EquipRecordFilter<EquipRecordVO>();
			//set cac tham so tim kiem
			/** vuongmq; 14/05/2015; them ma NPP; va staff dang nhap */
			if (currentUser != null) {
				filter.setStaffRoot(currentUser.getUserId());
			} else {
				// loi khong ton tai user dang nhap
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(arrShopTxt)) {
				String[] arrShopIdTxt = arrShopTxt.split(",");
				List<Long> listShopId = new ArrayList<Long>();
				for (String value : arrShopIdTxt) {
					if (StringUtil.isNumberInt(value)) {
						listShopId.add(Long.valueOf(value));
					}
				}
				if (listShopId != null && !listShopId.isEmpty()) {
					filter.setLstShopId(listShopId);
				}
			} else {
				//po.adjustment.message.not.user = Lỗi không tồn tại user đăng nhập; kiem tra hơ
				if (currentUser != null && currentUser.getShopRoot() != null) {
					List<Long> listShopId = new ArrayList<Long>();
					listShopId.add(currentUser.getShopRoot().getShopId());
					filter.setLstShopId(listShopId);
				} else {
					result.put("errMsg", R.getResource("po.adjustment.message.not.user"));
					return JSON;
				}
			}
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				filter.setShortCode(shortCode);
			}
//			if (!StringUtil.isNullOrEmpty(customerName)) {
//				filter.setCustomerName(customerName);
//			}
//			if (!StringUtil.isNullOrEmpty(address)) {
//				filter.setAddress(address);
//			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				filter.setEquipCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(serial)) {
				filter.setSerial(serial);
			}
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (recordStatus != null && recordStatus > ActiveType.DELETED.getValue()) {
				filter.setRecordStatus(recordStatus);
			}
			if (deliveryStatus != null && deliveryStatus > ActiveType.DELETED.getValue()) {
				filter.setDeliveryStatus(deliveryStatus);
			}
			if (stockType != null && stockType > ActiveType.DELETED.getValue()) {
				filter.setStockType(stockType);
			}
			//filter.setIsLevel(currentUser.getShopRoot().getIsLevel());
			//goi ham thuc thi tim kiem
			ObjectVO<EquipRecordVO> objVO = equipRecordMgr.searchListEquipLostRecordVOByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				
				//Datpv4 26/06/2015 them trang thai bien ban va trang thai giao nhan
				
				for(EquipRecordVO equipRecordVO: objVO.getLstObject()){
					if(equipRecordVO.getDeliveryStatus()==DeliveryType.NOTSEND.getValue()){
						equipRecordVO.setDeliveryStatusStr(R.getResource("action.status.chuagui"));
					} else if(equipRecordVO.getDeliveryStatus()==DeliveryType.SENT.getValue()){
						equipRecordVO.setDeliveryStatusStr(R.getResource("action.status.dagui"));
					} else if(equipRecordVO.getDeliveryStatus()==DeliveryType.RECEIVED.getValue()){
						equipRecordVO.setDeliveryStatusStr(R.getResource("action.status.danhan"));
					}
					
					if(equipRecordVO.getRecordStatus()==StatusRecordsEquip.DRAFT.getValue()){
						equipRecordVO.setRecordStatusStr(R.getResource("action.status.duthao"));
					} else if(equipRecordVO.getRecordStatus()==StatusRecordsEquip.WAITING_APPROVAL.getValue()){
						equipRecordVO.setRecordStatusStr(R.getResource("action.status.choduyet"));
					} else if(equipRecordVO.getRecordStatus()==StatusRecordsEquip.APPROVED.getValue()){
						equipRecordVO.setRecordStatusStr(R.getResource("action.status.duyet"));
					} else if(equipRecordVO.getRecordStatus()==StatusRecordsEquip.NO_APPROVAL.getValue()){
						equipRecordVO.setRecordStatusStr(R.getResource("action.status.khongduyet"));
					} else if(equipRecordVO.getRecordStatus()==StatusRecordsEquip.CANCELLATION.getValue()){
						equipRecordVO.setRecordStatusStr(R.getResource("action.status.huy"));
					} 
				}
				
						
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("rows", objVO.getLstObject());
				//String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/";
				String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathEquipment();
				String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_LOST_RECORD_EXPORT;
				templateFileName = templateFileName.replace('/', File.separatorChar);
				String outputName = ConstantManager.EQUIP_LOST_RECORD_EXPORT + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				inputStream.close();
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				os.close();
				result.put(ERROR, false);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put("path", outputPath);
				/*vuongmq; 12/05/2015; xuat Excel ATTT*/
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.err.export.list.equip.isnull"));
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (os != null) {
				os.close();
			}
		}
		return JSON;
	}

	/**
	 * Cap nhat danh sach Bien ban bao mat
	 * 
	 * @author hunglm16
	 * @since January 01,2014
	 * */
	public String changeByListEquipLoadRecord() {
		resetToken(result);
		errMsg = "";
		boolean flagErrorB = false;
		try {
			if (recordStatus == null && deliveryStatus == null) {
				//errMsg = "Chưa có trạng thái nào được chọn";
				errMsg = R.getResource("equip.lost.recorde.err.chua.chon.trang.thai");
				flagErrorB = true;
			} else if (recordStatus != null && deliveryStatus != null && recordStatus < 0 && deliveryStatus < 0) {
				//errMsg = "Chỉ được phép chọn 1 trong 2 trạng thái";
				errMsg = R.getResource("equip.lost.recorde.err.chi.chon.1.trong.2.trang.thai");
				flagErrorB = true;
			} else {
				if (currentUser == null || currentUser.getShopRoot() == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
					return SUCCESS;
				}
				
				if (StringUtil.isNullOrEmpty(arrEquipLostRecordTxt)) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.arr.id.null");
					flagErrorB = true;
				} else {
					String[] arrIdStr = arrEquipLostRecordTxt.split(",");
					List<Long> lstEquipLostRecordId = new ArrayList<Long>();
					for (String value : arrIdStr) {
						if (StringUtil.isNumberInt(value)) {
							lstEquipLostRecordId.add(Long.valueOf(value));
						}
					}
					if (lstEquipLostRecordId == null || (lstEquipLostRecordId != null && lstEquipLostRecordId.isEmpty())) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.arr.id.null");
						flagErrorB = true;
					}
					EquipRecordFilter<EquipLostRecord> filterL = new EquipRecordFilter<EquipLostRecord>();
					filterL.setLstId(lstEquipLostRecordId);
					filterL.setShopId(currentUser.getShopRoot().getShopId());
					List<EquipLostRecord> lstEqLostRecord = equipRecordMgr.searchEquipLostRecordByFilter(filterL).getLstObject();
					List<EquipLostRecord> lstEror = new ArrayList<EquipLostRecord>();
					/** kiem tra cac trang thai de dua ra lstEror; */
					/** vuongmq; 20/05/2015; truong hop tnkd chon t.thai giao nhan: da gui: 1 thi kiem tra; neu truong hop Duyet;  moi cho cap nhat, con lai la ko cho cap nhat */
					if (deliveryStatus != null && DeliveryType.SENT.getValue().equals(deliveryStatus) ) {
						for (EquipLostRecord eq : lstEqLostRecord) {
							if (!StatusRecordsEquip.APPROVED.equals(eq.getRecordStatus())) {
								lstEror.add(eq);
								lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
							} else {
								/** vuongmq; 29/05/2015; truong hop tnkd chon t.thai giao nhan: da gui: 1 thi kiem tra; neu truong hop Duyet/ da nhan (ok) -> da gui ko cho cap nhat */
								if (DeliveryType.RECEIVED.equals(eq.getDeliveryStatus())) {
									lstEror.add(eq);
									lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
								}
							}
						}
					}
					/** vuongmq; 18/05/2015; truong hop tnkd chon t.thai giao nhan: da nhan: 2 thi kiem tra; neu truong hop Duyet va da gui;  moi cho cap nhat, con lai la ko cho cap nhat */
					if (deliveryStatus != null && DeliveryType.RECEIVED.getValue().equals(deliveryStatus) ) {
						for (EquipLostRecord eq : lstEqLostRecord) {
							if (!StatusRecordsEquip.APPROVED.equals(eq.getRecordStatus()) || !DeliveryType.SENT.equals(eq.getDeliveryStatus())) {
								lstEror.add(eq);
								lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
							}
						}
					}
					/** vuongmq; 20/05/2015; truong hop tnkd chon t.thai bien ban: cho duyet: 1 thi kiem tra; neu truong hop Du thao, ko duyet;  moi cho cap nhat, con lai la ko cho cap nhat */
					if (recordStatus != null && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(recordStatus) ) {
						for (EquipLostRecord eq : lstEqLostRecord) {
							if (!StatusRecordsEquip.DRAFT.equals(eq.getRecordStatus()) && !StatusRecordsEquip.NO_APPROVAL.equals(eq.getRecordStatus())) {
								lstEror.add(eq);
								lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
							}
						}
					}
					/** vuongmq; 20/05/2015; truong hop tnkd chon t.thai bien ban: huy: 4 thi kiem tra; neu truong hop Du thao, ko duyet, cho duyet;  moi cho cap nhat, con lai la ko cho cap nhat */
					if (recordStatus != null && StatusRecordsEquip.CANCELLATION.getValue().equals(recordStatus) ) {
						for (EquipLostRecord eq : lstEqLostRecord) {
							if (!StatusRecordsEquip.DRAFT.equals(eq.getRecordStatus()) && !StatusRecordsEquip.NO_APPROVAL.equals(eq.getRecordStatus()) && !StatusRecordsEquip.WAITING_APPROVAL.equals(eq.getRecordStatus())) {
								lstEror.add(eq);
								lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
							}
						}
					}
					//Kiem tra cho truong hop duyet -> Trang thai thanh ly phai Da Thanh Ly
					if (recordStatus != null && (lstEror == null || lstEror.isEmpty()) && StatusRecordsEquip.APPROVED.getValue().equals(recordStatus) ) {
						for (EquipLostRecord eq : lstEqLostRecord) {
							if (eq.getEquip() == null || !LinquidationStatus.FINISH_LIQUIDATION.equals(eq.getEquip().getLinquidationStatus())) {
								lstEror.add(eq);
								lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
							}
						}
					}
					//Kiem tra cho truong hop Cho Duyet chuyen sang Khong duyet -> Trang thai thanh ly phai Khong thanh ly
					if (recordStatus != null && (lstEror == null || lstEror.isEmpty()) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(recordStatus) ) {
						for (EquipLostRecord eq : lstEqLostRecord) {
							if (eq.getEquip() == null || !LinquidationStatus.NO_LIQUIDATION.equals(eq.getEquip().getLinquidationStatus())) {
								lstEror.add(eq);
								lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
							}
						}
					}
					result.put("rows", lstEror);
					if (lstEquipLostRecordId == null || (lstEquipLostRecordId != null && lstEquipLostRecordId.isEmpty())) {
						flagErrorB = false;
						if (flagError != null && flagError == 1) {
							flagErrorB = true;
						}
						result.put(ERROR, flagErrorB);
						result.put("errMsg", errMsg);
						return JSON;
					}

					EquipRecordFilter<EquipLostRecord> filter = new EquipRecordFilter<EquipLostRecord>();
					filter.setLstId(lstEquipLostRecordId);
					filter.setUserName(currentUser.getUserName());
					if (recordStatus != null && recordStatus > ActiveType.DELETED.getValue()) {
						filter.setRecordStatus(recordStatus);
					}
					if (deliveryStatus != null && deliveryStatus > ActiveType.DELETED.getValue()) {
						filter.setDeliveryStatus(deliveryStatus);
					}
					//Cap nhat tuong tac Database
					equipRecordMgr.changeByListEquipLostRecord(filter);
					//Gui mail - Cho duyet
					if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(recordStatus)) {
						List<EquipLostRecord> lstLostRecord = new ArrayList<EquipLostRecord>();
						if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
							EquipRecordFilter<EquipLostRecord> filterLR = new EquipRecordFilter<EquipLostRecord>();
							filterLR.setLstId(filter.getLstId());
							filterLR.setStockType(filter.getStockType()); // truong hop nay bao mat tu mobile co stock type
							lstLostRecord = equipRecordMgr.searchEquipLostRecordByFilter(filterLR).getLstObject();
						}
						if (lstLostRecord == null || lstLostRecord.isEmpty()) {
							lstLostRecord = new ArrayList<EquipLostRecord>();
							lstLostRecord.add(filter.getAttribute());
						}
						String errMsgMail = "";
//						for (EquipLostRecord lostRecord : lstLostRecord) {
//							errMsgMail = this.sendEmailEquipLostRecord(lostRecord);
//						}
						result.put("errMsgMail", errMsgMail);
					}
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, flagError);
		result.put("errMsg", errMsg);
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	
	/**
	 * Cap nhat danh sach Bien ban bao mat; phe duyet Approve
	 * @author vuongmq
	 * @since 15/05/2015
	 * 
	 * @author hunglm16
	 * @since 02/07/2015
	 * @description Ra soat va update
	 * */
	public String changeByListEquipLostRecordApprove() {
		//generateToken();
		errMsg = "";
		boolean flagErrorB = false;
		try {
			if (recordStatus == null) {
				//errMsg = "Chưa có trạng thái nào được chọn";
				errMsg = R.getResource("equip.lost.recorde.err.chua.chon.trang.thai");
				flagErrorB = true;
			} else {
				if (currentUser == null || currentUser.getShopRoot() == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
					return SUCCESS;
				}
				if (!StatusRecordsEquip.NO_APPROVAL.getValue().equals(recordStatus) && !StatusRecordsEquip.APPROVED.getValue().equals(recordStatus) ) {
					//errMsg = "Chỉ được phép chọn Duyệt hoặc Không duyệt";
					errMsg = R.getResource("equip.lost.recorde.err.chon.duyet.or.khong.duyet");
					flagErrorB = true;
				}
				if (StringUtil.isNullOrEmpty(arrEquipLostRecordTxt)) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.arr.id.null");
					flagErrorB = true;
				} else {
					String[] arrIdStr = arrEquipLostRecordTxt.split(",");
					List<Long> lstEquipLostRecordId = new ArrayList<Long>();
					for (String value : arrIdStr) {
						if (StringUtil.isNumberInt(value)) {
							lstEquipLostRecordId.add(Long.valueOf(value));
						}
					}
					if (lstEquipLostRecordId == null || (lstEquipLostRecordId != null && lstEquipLostRecordId.isEmpty())) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.arr.id.null");
						flagErrorB = true;
					}
					EquipRecordFilter<EquipLostRecord> filterL = new EquipRecordFilter<EquipLostRecord>();
					filterL.setLstId(lstEquipLostRecordId);
					filterL.setShopId(currentUser.getShopRoot().getShopId());
					List<EquipLostRecord> lstEqLostRecord = equipRecordMgr.searchEquipLostRecordByFilter(filterL).getLstObject();
					List<EquipLostRecord> lstEror = new ArrayList<EquipLostRecord>();
					/** kiem tra cac trang thai de dua ra lstEror; */
					/** vuongmq; 18/05/2015; truong hop record != cho duyet -> ko cho cap nhat */
					for (EquipLostRecord eq : lstEqLostRecord) {
						if (!StatusRecordsEquip.WAITING_APPROVAL.equals(eq.getRecordStatus())){
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.update.page.update.waiting.approval");
							lstEror.add(eq);
							lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
						}
					}
					//TH: Duyet -> Thiet bi Da Thanh Ly
					if (StatusRecordsEquip.APPROVED.getValue().equals(recordStatus) && (lstEror == null || lstEror.isEmpty())) {
						for (EquipLostRecord eq : lstEqLostRecord) {
							if (eq.getEquip() == null || !LinquidationStatus.FINISH_LIQUIDATION.getValue().equals(eq.getEquip().getLinquidationStatus())){
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.update.page.update.approval");
								lstEror.add(eq);
								lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
							}
						}
					}
					//TH: Khong duyet -> Thiet bi Khong Thanh Ly
					if (StatusRecordsEquip.NO_APPROVAL.getValue().equals(recordStatus) && (lstEror == null || lstEror.isEmpty())) {
						for (EquipLostRecord eq : lstEqLostRecord) {
							if (eq.getEquip() == null || !LinquidationStatus.NO_LIQUIDATION.getValue().equals(eq.getEquip().getLinquidationStatus())){
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.update.page.update.not.approval");
								lstEror.add(eq);
								lstEquipLostRecordId.remove(lstEquipLostRecordId.indexOf(eq.getId()));
							}
						}
					}
					result.put("rows", lstEror);
					if (lstEquipLostRecordId == null || lstEror == null || !lstEror.isEmpty()) {
						flagErrorB = false;
						if (flagError != null && flagError == 1) {
							flagErrorB = true;
						}
						result.put(ERROR, flagErrorB);
						result.put("errMsg", errMsg);
						return JSON;
					}
					EquipRecordFilter<EquipLostRecord> filter = new EquipRecordFilter<EquipLostRecord>();
					filter.setLstId(lstEquipLostRecordId);
					filter.setUserName(currentUser.getUserName());
					if (recordStatus != null && recordStatus > ActiveType.DELETED.getValue()) {
						filter.setRecordStatus(recordStatus);
					}
					//Cap nhat tuong tac Database
					equipRecordMgr.changeByListEquipLostRecord(filter);
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, flagErrorB);
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Them moi hoac cap nhat Bien ban bao mat
	 * 
	 * @author hunglm16
	 * @since January 08,2014
	 * */
	
	/**
	 * @author vuongmq
	 * @since 27/04/2015
	 * @description Them moi hoac cap nhat Bien ban bao mat
	 * */
	public String insertOrUpdateLoadRecord() {
		resetToken(result);
		errMsg = "";
		boolean flagError = false;
		if (eqLostRecId != null && eqLostRecId < 0) {
			eqLostRecId = null;
		}
		try {
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
				return SUCCESS;
			}
			
			List<Long> lstEquipAttachFileId = new ArrayList<Long>();
			Integer maxLength = 0;
			if (eqLostRecId != null && eqLostRecId > 0) {
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(eqLostRecId);
				filter.setObjectType(EquipTradeType.NOTICE_LOST.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
				//Xu ly Xoa file dinh kem
				if (!StringUtil.isNullOrEmpty(equipAttachFileStr)) {
					String[] arrEquipAttachFile = equipAttachFileStr.split(",");
					for (int i = 0, size = arrEquipAttachFile.length; i < size; i++) {
						String value = arrEquipAttachFile[i];
						if (!StringUtil.isNullOrEmpty(value) && ValidateUtil.validateNumber(value)) {
							lstEquipAttachFileId.add(Long.parseLong(value));
						}
					}
				}
				maxLength = lstFileVo.size() - lstEquipAttachFileId.size();
				if (maxLength < 0) {
					maxLength = 0;
				}
			}
			if (eqLostRecId != null && eqLostRecId > 0 && recordStatus != null && StatusRecordsEquip.CANCELLATION.getValue().equals(recordStatus)) {
				equipLostRecord = equipRecordMgr.getEquipLostRecordById(eqLostRecId, currentUser.getShopRoot().getShopId());
				if (equipLostRecord == null) {
					//errMsg = "Biên bản không tồn tại";
					errMsg = R.getResource("equip.lost.recorde.err.ma.bb.ko.ton.tai");
				} else if (!StatusRecordsEquip.DRAFT.equals(equipLostRecord.getRecordStatus()) && !StatusRecordsEquip.NO_APPROVAL.equals(equipLostRecord.getRecordStatus())) {
					//errMsg = "Trạng thái Biên bản không hợp lệ";
					errMsg = R.getResource("equipment.status.record.invalid");
				} else {
					errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				//Neu trang thai van ban la huy
				List<Long> lstEquipLostRecordId = new ArrayList<Long>();
				lstEquipLostRecordId.add(eqLostRecId);
				EquipRecordFilter<EquipLostRecord> filter = new EquipRecordFilter<EquipLostRecord>();
				filter.setLstId(lstEquipLostRecordId);
				//Luu file dinh kem
				filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));
				filter.setUserName(currentUser.getUserName());
				filter.setRecordStatus(recordStatus);
				//filter.setStockType(EquipStockTotalType.KHO_KH.getValue());
				EquipLostRecord equipLostRecordTmp = equipRecordMgr.getEquipLostRecordById(eqLostRecId, currentUser.getShopRoot().getShopId());
				if (equipLostRecordTmp != null) {
					filter.setStockType(equipLostRecordTmp.getStockType().getValue());
				}
				//Cap nhat tuong tac Database
				equipRecordMgr.changeByListEquipLostRecord(filter);

			}

			Date sysDateDb = commonMgr.getSysDate();
			if (eqLostMobiRecId == null || eqLostMobiRecId < 1) {
				eqLostMobiRecId = Long.valueOf(0);
			}
			//Shop shop = shopMgr.getShopByCode(shopCode.trim().toUpperCase());
			//Shop shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			//shopCode = shop.getShopCode();
			Shop shopIn = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shopIn = shopMgr.getShopByCode(shopCode.trim().toUpperCase());
				if (shopIn != null) {
					shopCode = shopIn.getShopCode();
				} else {
					// đơn vị không tồn tại
					errMsg = R.getResource("po.conf.group.err.imp.isShop.isUnDefined");
				}
			} else {
				// Mã NPP không được để trống
				errMsg = R.getResource("imp.tuyen.clmn.error.maNPP.null");
			}
			if (EquipStockTotalType.KHO.getValue().equals(flagLostRecord)) {
				if (StringUtil.isNullOrEmpty(shopCode)) {
					//errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.null.stockcode");
					// Mã NPP không được để trống
					errMsg = R.getResource("imp.tuyen.clmn.error.maNPP.null");
				}
			} else {
				if (StringUtil.isNullOrEmpty(shortCode)) {
					//errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.null.stockcode");
					//equip.lost.recorde.err.customer.not.require = Mã khách hàng không được để trống.
					errMsg = R.getResource("equip.lost.recorde.err.customer.not.require");
				}
			}
			/*stockCode = shopCode.trim().toUpperCase() + shortCode.trim().toUpperCase();
			stockCode = stockCode.trim();*/
			List<Long> lstEquipId = new ArrayList<Long>();
			Map<Long, Long> mapEquipDeliveryRec = new HashMap<Long, Long>();
			/**vuongmq; 06/05/2015; add them map thiet bi, cua gia tri con lai; arrEquip va arrPriceActually luon co gia tri la 1 thiet bi */
			Map<Long, Long> mapPriceActually = new HashMap<Long, Long>();
			if (errMsg.length() == 0) {
				if (!StringUtil.isNullOrEmpty(arrEquip)) {
					String[] arrEquipStr = arrEquip.split(",");
					String[] arrPriceActuallyStr = arrPriceActually.split(",");
					for (int i = 0, len = arrEquipStr.length; i < len; i++) {
						if (ValidateUtil.validateNumber(arrEquipStr[i])) {
							lstEquipId.add(Long.valueOf(arrEquipStr[i]));
							if (!mapPriceActually.containsKey(Long.valueOf(arrEquipStr[i]))) {
								mapPriceActually.put(Long.valueOf(arrEquipStr[i]), Long.valueOf(arrPriceActuallyStr[i]));
							}
						}
					}
				}
				if (lstEquipId.size() == 0) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.arr.equip.null");
				}
			}
			if (errMsg.length() == 0) {
				/** lay KH hay la NPP*/
				/** vuongmq; 06/05/2015; them bao mat NPP: 1; KH: 2*/
				if (EquipStockTotalType.KHO.getValue().equals(flagLostRecord)) {
					stockId = shopIn.getId();
					stockType = EquipStockTotalType.KHO.getValue();
				} else {
					String codeCustomer = shortCode.trim().toUpperCase() + "_" + shopIn.getShopCode();
					customer = customerMgr.getCustomerByCode(codeCustomer);
					if (customer == null) {
						//errMsg = "Khách hàng không hợp lệ";
						errMsg = R.getResource("equip.lost.recorde.err.khach.hang");
					} else {
						stockId = customer.getId();
						stockType = EquipStockTotalType.KHO_KH.getValue();
					}
				}
				if (errMsg.length() == 0) {
					EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
					filter.setStockId(stockId);
					filter.setStockType(stockType);
					filter.setLstEquipId(lstEquipId);
					filter.setShopId(shopIn.getId());
					filter.setStaffRoot(currentUser.getUserId());
					/*if (eqLostRecId != null && eqLostRecId > 0) {
						// truong hop cap nhat thi truyen them eqLostRecId
						filter.setEquipLostId(eqLostRecId);
					}*/
					//goi ham thuc thi tim kiem; ham nay phai co staffRoot va shopId
					ObjectVO<EquipmentVO> objVO = equipmentManagerMgr.searchEquipByEquipDeliveryRecordNew(filter);
					if (objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
						List<EquipmentVO> rows = objVO.getLstObject();
						boolean flagEq = false;
						for (int i = 0, size = rows.size(); i < size; i++) {
							Long eqDelvRecId = rows.get(i).getEquipDeliveryRecordId();
							if (eqDelvRecId == null || eqDelvRecId < 0) {
								eqDelvRecId = 0l;
							}
							mapEquipDeliveryRec.put(rows.get(i).getEquipId(), eqDelvRecId);
							flagEq = true;
						}
						if (!flagEq) {
							//equip.lost.recorde.err.arr.equip.null
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.arr.equip.dieu.kien");
						}
					} else {
						//equip.lost.recorde.err.arr.equip.null
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.arr.equip.dieu.kien");
					}
				}
			}
			//Thoi gian bao mat
			if (errMsg.length() == 0 && StringUtil.isNullOrEmpty(lostDateStr)) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.null.lockDate");
			}
			if (errMsg.length() == 0 && DateUtil.compareDateWithoutTime(DateUtil.parse(lostDateStr, DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.parse(DateUtil.convertDateByString(sysDateDb, DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.DATE_FORMAT_DDMMYYYY)) > 0) {
				//errMsg = "Thời gian báo mất phải nhỏ hơn hoặc bằng ngày hiện tại";
				errMsg = R.getResource("equip.lost.recorde.err.time.bao.mat.nho.hon.bang.time.sysdate");
			}
			//Ngay het phat sinh doanh so
			if (errMsg.length() == 0 && StringUtil.isNullOrEmpty(lastArisingSalesDateStr)) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.null.lastArisingSalesDate");
			}
			if (errMsg.length() == 0
					&& DateUtil.compareDateWithoutTime(DateUtil.parse(lastArisingSalesDateStr, DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.parse(DateUtil.convertDateByString(sysDateDb, DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.DATE_FORMAT_DDMMYYYY)) > 0) {
				//errMsg = "Ngày hết phát sinh doanh số phải nhỏ hơn hoặc bằng ngày hiện tại";
				errMsg = R.getResource("equip.lost.recorde.err.time.het.phat.sinh.nho.hon.bang.time.sysdate");
			}
			//So sanh hai ngay
			if (errMsg.length() == 0 && DateUtil.compareDateWithoutTime(DateUtil.parse(lastArisingSalesDateStr, DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.parse(lostDateStr, DateUtil.DATE_FORMAT_DDMMYYYY)) > 0) {
				//errMsg = "Ngày hết phát sinh doanh số phải nhỏ hơn hoặc bằng Thời gian báo mất";
				errMsg = R.getResource("equip.lost.recorde.err.time.het.phat.sinh.nho.hon.bang.time.bao.mat");
			}
			//GS va NPP truy tim tu mat
			if (errMsg.length() == 0 && (tracingPlace == null || tracingPlace < 0 || tracingPlace > 1)) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.tracingplace.undefined");
			}
			//Ket qua truy tim
			if (errMsg.length() == 0 && (tracingResult == null || tracingResult < 0 || tracingResult > 1)) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.lost.recorde.err.tracingresult.undefined");
			}
			if (errMsg.length() == 0) {
				errMsg = FileUtility.checkEquipAttachFileReturnMsg(lstFile, lstFileFileName, lstFileContentType, maxLength);
			}
			
			Date cDate = null;
			if (errMsg.length() == 0 && !StringUtil.isNullOrEmpty(createDate)) {
				cDate = DateUtil.parse(createDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.null");
			}
			
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
			EquipRecordFilter<EquipLostRecord> filter = new EquipRecordFilter<EquipLostRecord>();
			filter.setEqLostMobiRecId(eqLostMobiRecId);
			filter.setShopCode(shopCode);
			filter.setShortCode(shortCode);
			filter.setLstLong(lstEquipId);
			filter.setUserName(currentUser.getUserName());
			filter.setMapLongLong(mapEquipDeliveryRec);
			filter.setMapPriceActually(mapPriceActually); // vuongmq; 06/05/2015
			//filter.setStockCode(stockCode);
			filter.setLostDate(DateUtil.parse(lostDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			filter.setLastArisingSalesDate(DateUtil.parse(lastArisingSalesDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			filter.setTracingPlace(tracingPlace);
			filter.setTracingResult(tracingResult);
			filter.setConclusion(conclusion);
			filter.setRecommendedTreatment(recommendedTreatment);
			filter.setRecordStatus(recordStatus);
			filter.setDeliveryStatus(deliveryStatus);
			filter.setAttribute(equipLostRecord);
			filter.setShopId(shopIn.getId());
			//Luu file dinh kem

			Equipment eqChange = new Equipment();
			if (lstEquipId != null && !lstEquipId.isEmpty()) {
				eqChange = equipmentManagerMgr.getEquipmentById(lstEquipId.get(0));
				if (eqChange == null) {
					//errMsg = "Thiết bị không tồn tại";
					errMsg = R.getResource("equipment.upadte.err.undefined");
				} else {
					if (!StatusType.ACTIVE.equals(eqChange.getStatus())) {
						//errMsg = "Thiết bị không còn hoạt động";
						errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.ko.hoat.dong");
					}
					//Kiem tra cho truong hop duyet -> Trang thai thanh ly phai Da Thanh Ly
					if (errMsg.length() == 0 && recordStatus != null && StatusRecordsEquip.APPROVED.getValue().equals(recordStatus) && !LinquidationStatus.FINISH_LIQUIDATION.equals(eqChange.getLinquidationStatus())) {
						//Loi
						errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.duyet");
					}
					//Kiem tra cho truong hop Cho Duyet chuyen sang Khong duyet -> Trang thai thanh ly phai Khong thanh ly
					if (errMsg.length() == 0 && recordStatus != null && StatusRecordsEquip.NO_APPROVAL.getValue().equals(recordStatus) && !LinquidationStatus.NO_LIQUIDATION.equals(eqChange.getLinquidationStatus())) {
						//Loi
						errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.khong.duyet");
					}
					/** vuongmq; 06/05/2015; them bao mat NPP: 1; KH: 2*/
					if (EquipStockTotalType.KHO.getValue().equals(flagLostRecord)) {
						if (errMsg.length() == 0 && !EquipStockTotalType.KHO.equals(eqChange.getStockType())) {
							//errMsg = "Thiết bị không thuộc loại kho NPP";
							errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.ko.thuoc.loai.npp");
						} /*else if (errMsg.length() == 0 && !shopIn.getId().equals(eqChange.getStockId())) {
							//errMsg = "Thiết bị không thuộc loại kho NPP";
							errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.ko.thuoc.loai.kho.npp");
						}*/
						/** vuongmq; 08/05/2015; chi lay TB dang o kho: 2 voi loai kho: NPP; dang su dung: 3 voi loai kho la KH
						* va giao dich Binh thuong: 0
						* da kiem tra eqChange 
						*/
						if (errMsg.length() == 0 
								&& eqChange.getUsageStatus() != null && !EquipUsageStatus.SHOWING_WAREHOUSE.equals(eqChange.getUsageStatus())
								&& eqChange.getTradeStatus() != null && !EquipTradeStatus.NOT_TRADE.getValue().equals(eqChange.getTradeStatus())) {
							//errMsg = "Thiết bị phải đang ở kho NPP";
							errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.phai.o.kho.npp");
						} 
					} else {
						if (errMsg.length() == 0 && !EquipStockTotalType.KHO_KH.getValue().equals(eqChange.getStockType().getValue())) {
							//errMsg = "Thiết bị không thuộc loại kho khách hàng";
							errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.ko.thuoc.loai.khach.hang");
						} else if (errMsg.length() == 0 && !customer.getId().equals(eqChange.getStockId())) {
							//errMsg = "Thiết bị không thuộc loại kho khách hàng";
							errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.ko.thuoc.loai.kho.khach.hang");
						}
						/** vuongmq; 08/05/2015; chi lay TB dang o kho: 2 voi loai kho: NPP; dang su dung: 3 voi loai kho la KH
						* va giao dich Binh thuong: 0
						* da kiem tra eqChange 
						*/
						//vuongmq; 27/05/2015; 0: bao mat wweb; 1: bao mat mobile khong can kiem tra usage_status
						if (errMsg.length() == 0 
								&& eqChange.getUsageStatus() != null && !EquipUsageStatus.IS_USED.equals(eqChange.getUsageStatus()) 
								&& eqChange.getTradeStatus() != null && !EquipTradeStatus.NOT_TRADE.getValue().equals(eqChange.getTradeStatus())) {
								//errMsg = "Thiết bị phải đang sử dụng";
							errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.phai.dang.su.dung");
						} 
					}
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
			}
			/** Cap nhat bien ban bao mat */
			EquipLostRecord equipLostRecord = new EquipLostRecord();
			if (eqLostRecId != null && eqLostRecId > 0) {
				equipLostRecord = equipRecordMgr.getEquipLostRecordById(eqLostRecId, currentUser.getShopRoot().getShopId());
				if (equipLostRecord == null) {
					//errMsg = "Biên bản không tồn tại";
					errMsg = R.getResource("equip.lost.recorde.err.ma.bb.ko.ton.tai");
				}
				if (errMsg.length() == 0 && equipLostRecord.getEquip() != null && !equipLostRecord.getEquip().getId().equals(lstEquipId.get(0))) {
					//Kiem tra Bao mat tu Mobile
					EquipRecordFilter<EquipRecordVO> filterCMb = new EquipRecordFilter<EquipRecordVO>();
					filterCMb.setEqLostRecId(eqLostRecId);
					Integer countEqLostMobiRec = equipRecordMgr.countEquipLostMobileRecByFilter(filterCMb);
					if (countEqLostMobiRec != null && countEqLostMobiRec > 0) {
						//errMsg = "Biên bản được lập từ phiếu báo mất mobile nên không được thay đổi thiết bị";
						errMsg = R.getResource("equip.lost.recorde.err.bao.mat.tu.mobile.ko.doi.thiet.bi");
					}
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				/** Kiem tra neu la  */
				
				EquipLostRecord equipLostRecordOld = equipLostRecord.clone();
				//equipLostRecord.setCustomer(customer); // vuongmq; lay id cua NPP hoac KH
				//equipLostRecord.setStockType(EquipStockTotalType.KHO_KH.getValue());
				//equipLostRecord.setRecordStatus(StatusRecordsEquip.APPROVED.getValue());
				//equipLostRecord.setStockType(EquipStockTotalType.KHO_KH);
				//equipLostRecord.setStockCode(stockCode);
				/** vuongmq; 06/05/2015; them bao mat NPP: 1; KH: 2*/
				if (EquipStockTotalType.KHO.getValue().equals(flagLostRecord)) {
					// bao mat NPP
					Equipment equipmentTmp = equipmentManagerMgr.getEquipmentById(lstEquipId.get(0));
					if (equipmentTmp != null) {
						equipLostRecord.setStockId(equipmentTmp.getStockId());
						EquipStock equipStock = equipmentManagerMgr.getEquipStockById(equipmentTmp.getStockId());
						if (equipStock != null) {
							stockCode = equipStock.getCode();
							stockCode = stockCode.trim();
						}
					}
					equipLostRecord.setStockType(EquipStockTotalType.KHO);
				} else {
					if (customer != null) {
						equipLostRecord.setStockId(customer.getId());
					}
					stockCode = shopCode.trim().toUpperCase() + shortCode.trim().toUpperCase();
					stockCode = stockCode.trim();
					equipLostRecord.setStockType(EquipStockTotalType.KHO_KH);
				}
				filter.setStockCode(stockCode);
				equipLostRecord.setStockCode(stockCode);
				//equipLostRecord.setRecordStatus(StatusRecordsEquip.APPROVED); // vuongmq; 08/05/2015 
				equipLostRecord.setEquip(eqChange);
				/** da kiem tra eqChange o tren*/
				/*if (lstEquipId != null && !lstEquipId.isEmpty()) {
					if (!equipLostRecordOld.getEquip().getId().equals(eqChange.getId())) {
						if (eqChange.getUsageStatus() == null || !EquipUsageStatus.IS_USED.getValue().equals(eqChange.getUsageStatus().getValue()) || !StatusType.INACTIVE.getValue().equals(eqChange.getTradeStatus())) {
							//errMsg = "Thiết bị không hợp lệ";
							errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.ko.hop.le");
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
					}
					equipLostRecord.setEquip(eqChange);
				}*/
				//tamvnm: update dac ta moi. 30/06/2015
				if (cDate != null) {
//					List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//					EquipPeriod equipPeriod = null;
//					if (lstPeriod == null || lstPeriod.size() == 0) {
//						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//						result.put(ERROR, true);
//						result.put("errMsg", errMsg);
//						return JSON;
//					} else if (lstPeriod.size() > 0) {
//						// gan gia tri ky dau tien.
//						equipPeriod = lstPeriod.get(0);
//					}
//					equipLostRecord.setEquipPeriod(equipPeriod);
					equipLostRecord.setCreateFormDate(cDate);
					equipLostRecord.setNote(note);
				}
				
				//tamvnm: cap nhat ky theo ngay mat - Dec 29, 2015
//				Date lostDate = DateUtil.parse(lostDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
//				if (lostDate != null) {
//					List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(lostDate);
//					EquipPeriod equipPeriod = null;
//					if (lstPeriod == null || lstPeriod.size() == 0) {
//						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.lost.not.in");
//						result.put(ERROR, true);
//						result.put("errMsg", errMsg);
//						return JSON;
//					} else if (lstPeriod.size() > 0) {
//						// gan gia tri ky dau tien.
//						equipPeriod = lstPeriod.get(0);
//					}
//					equipLostRecord.setEquipPeriod(equipPeriod);
//				}
				equipLostRecord.setLostDate(DateUtil.parse(lostDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
				equipLostRecord.setLastArisingSalesDate(DateUtil.parse(lastArisingSalesDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
				equipLostRecord.setTracingPlace(tracingPlace);
				equipLostRecord.setTracingResult(tracingResult);
				equipLostRecord.setConclusion(conclusion);
				equipLostRecord.setRepresentor(representor);
				equipLostRecord.setRecommendedTreatment(recommendedTreatment);
				if (recordStatus != null && StatusRecordsEquip.checkIsRecode(recordStatus)) {
					equipLostRecord.setRecordStatus(StatusRecordsEquip.parseValue(recordStatus));
				}
				if (deliveryStatus != null && DeliveryType.checkIsRecode(deliveryStatus)) {
					equipLostRecord.setDeliveryStatus(DeliveryType.parseValue(deliveryStatus));
				}
				equipLostRecord.setUpdateUser(currentUser.getUserName());
				/** begin  vuongmq; 26/05/2015; cho nay cap nhat gia tri con lai cho equipment */
				BigDecimal priceActual = BigDecimal.ZERO;
				if (filter.getMapPriceActually() != null && !filter.getMapPriceActually().isEmpty()) {
					if (lstEquipId != null && lstEquipId.size() > 0) {
						Long pActually = filter.getMapPriceActually().get(lstEquipId.get(0));
						if (pActually != null) {
							priceActual = BigDecimal.valueOf(pActually);
						}
					}
				}
				equipLostRecord.setPriceActually(priceActual);
				/** end  vuongmq; 26/05/2015; cho nay cap nhat gia tri con lai cho equipment */
				filter.setAttribute(equipLostRecord);
				
				ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
				if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0) {
					BasicFilter<EquipAttachFile> filterF = new BasicFilter<EquipAttachFile>();
					filterF.setLstId(lstEquipAttachFileId);
					filterF.setObjectId(id);
					filterF.setObjectType(EquipTradeType.NOTICE_LOST.getValue());
					objVO = equipRecordMgr.searchEquipAttachFileByFilter(filterF);
				}
				//Luu file dinh kem
				filter.setLstEquipAttachFileId(lstEquipAttachFileId);
				filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));
				equipRecordMgr.changeEquipLostRecord(filter, equipLostRecordOld);
				if (lstEquipAttachFileId != null && lstEquipAttachFileId.size() > 0 && objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
					//Xu ly xoa cung file tren storeFileOnDisk
					List<String> lstFileName = new ArrayList<String>();
					for (EquipAttachFile itemFile : objVO.getLstObject()) {
						if (!StringUtil.isNullOrEmpty(itemFile.getUrl())) {
							lstFileName.add(itemFile.getUrl());
						}
					}
					FileUtility.deleteStoreFileOnDisk(lstFileName, Configuration.getFileEquipServerDocPath());
				}
				//Neu la trang thai cho duyet se send mail
//				if (StatusRecordsEquip.WAITING_APPROVAL.equals(filter.getAttribute().getRecordStatus())) {
//					String errMsgMail = this.sendEmailEquipLostRecord(filter.getAttribute());
//					result.put("errMsgMail", errMsgMail);
//				}
			} else {
				/** Them moi bien ban bao mat */
//				EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//				if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
//					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//					result.put(ERROR, true);
//					result.put("errMsg", errMsg);
//					return JSON;
//				}
				//tamvnm: update dac ta moi. 30/06/2015
//				EquipPeriod equipPeriod = null;
//				if (cDate != null) {
//					List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(cDate);
//					
//					if (lstPeriod == null || lstPeriod.size() == 0) {
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null"));
//						result.put(ERROR, true);
//						return JSON;
//					} else if (lstPeriod.size() > 0) {
//						// gan gia tri ky dau tien.
//						equipPeriod = lstPeriod.get(0);
//					}
//					
//				}
				//tamvnm: cap nhat ky theo ngay mat - Dec 29, 2015
//				EquipPeriod equipPeriod = null;
//				Date lostDate = DateUtil.parse(lostDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
//				if (lostDate != null) {
//					List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(lostDate);
//					
//					if (lstPeriod == null || lstPeriod.size() == 0) {
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.lost.not.in"));
//						result.put(ERROR, true);
//						return JSON;
//					} else if (lstPeriod.size() > 0) {
//						// gan gia tri ky dau tien.
//						equipPeriod = lstPeriod.get(0);
//					}
//				}

//				filter.setEquipPeriod(equipPeriod);
				equipLostRecord = new EquipLostRecord();
//				equipLostRecord.setEquipPeriod(equipPeriod);
				equipLostRecord.setCreateFormDate(cDate);
				equipLostRecord.setNote(note);
				if (lstEquipId != null && !lstEquipId.isEmpty()) {
					if (eqLostMobiRecId > 0) {
						EquipLostMobileRec eqLostMobiRec = equipRecordMgr.getEquipLostMobileRecById(eqLostMobiRecId, currentUser.getShopRoot().getShopId());
						if (eqLostMobiRec.getEquip() == null) {
							//errMsg = "Thiết bị không tồn tại";
							errMsg = R.getResource("equipment.upadte.err.undefined");
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						equipLostRecord.setEquip(eqLostMobiRec.getEquip());
					} else {
						/** vuongmq; 08/05/2015; chi lay TB dang o kho: 2 voi loai kho: NPP; dang su dung: 3 voi loai kho la KH
						* va giao dich Binh thuong: 0
						* da kiem tra eqChange o tren
						*/
						equipLostRecord.setEquip(eqChange);
						 /*if (eqChange.getUsageStatus() != null && EquipUsageStatus.IS_USED.equals(eqChange.getUsageStatus()) 
								&& eqChange.getTradeStatus() != null && EquipTradeStatus.NOT_TRADE.getValue().equals(eqChange.getTradeStatus())) {
							
						} else if (eqChange.getTradeStatus() == null || !EquipTradeStatus.TRADING.getValue().equals(eqChange.getTradeStatus()) 
								|| eqChange.getTradeType() == null || !EquipTradeType.NOTICE_LOSS_MOBILE.equals(eqChange.getTradeType())) {
							//errMsg = "Thiết bị không hợp lệ";
							errMsg = R.getResource("equip.lost.recorde.err.thiet.bi.ko.hop.le");
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}*/
					}
				}
				filter.setLstFileVo(saveUploadFileByEquip(lstFile, lstFileFileName, lstFileContentType));
				// equipLostRecord.setCustomer(customer); //// vuongmq; lay id cua NPP hoac KH
				//equipLostRecord.setStockType(EquipStockTotalType.KHO_KH.getValue());
				/** vuongmq; 06/05/2015; them bao mat NPP: 1; KH: 2*/
				Equipment equipmentTmp = equipmentManagerMgr.getEquipmentById(lstEquipId.get(0));
				if (EquipStockTotalType.KHO.getValue().equals(flagLostRecord)) {
					// bao mat NPP
					if (equipmentTmp != null) {
						/*EquipStock equipStock = equipmentManagerMgr.getEquipStockById(equipmentTmp.getStockId());
						if (equipStock != null) {
							stockCode = equipStock.getCode();
							stockCode = stockCode.trim();
						}*/
						equipLostRecord.setStockId(equipmentTmp.getStockId());
						stockCode = equipmentTmp.getStockCode();
						stockCode = stockCode.trim();
						equipLostRecord.setStockName(equipmentTmp.getStockName());
						equipLostRecord.setAddress(shopIn.getAddress());
						equipLostRecord.setPhone(this.viewPhoneShop(shopIn));
						
					}
					equipLostRecord.setStockType(EquipStockTotalType.KHO);
				} else {
					if (customer != null) {
						equipLostRecord.setStockId(customer.getId());
						equipLostRecord.setStockName(equipmentTmp.getStockName());
						equipLostRecord.setAddress(customer.getAddress());
						equipLostRecord.setPhone(this.viewPhoneCustomer(customer));
					}
					stockCode = shopCode.trim().toUpperCase() + shortCode.trim().toUpperCase();
					stockCode = stockCode.trim();
					equipLostRecord.setStockType(EquipStockTotalType.KHO_KH);
				}
				filter.setStockCode(stockCode);
				equipLostRecord.setStockCode(stockCode);
				equipLostRecord.setLostDate(DateUtil.parse(lostDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
				equipLostRecord.setLastArisingSalesDate(DateUtil.parse(lastArisingSalesDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
				equipLostRecord.setTracingPlace(tracingPlace);
				equipLostRecord.setTracingResult(tracingResult);
				equipLostRecord.setConclusion(conclusion);
				equipLostRecord.setRecommendedTreatment(recommendedTreatment);
				/*equipLostRecord.setRecordStatus(recordStatus);
				equipLostRecord.setDeliveryStatus(deliveryStatus);*/
				if (recordStatus != null && StatusRecordsEquip.checkIsRecode(recordStatus)) {
					equipLostRecord.setRecordStatus(StatusRecordsEquip.parseValue(recordStatus));
				}
				if (deliveryStatus != null && DeliveryType.checkIsRecode(deliveryStatus)) {
					equipLostRecord.setDeliveryStatus(DeliveryType.parseValue(deliveryStatus));
				}
				equipLostRecord.setCreateUser(currentUser.getUserName());
				equipLostRecord.setRepresentor(representor);
				/** begin  vuongmq; 26/05/2015; cho nay cap nhat gia tri con lai cho equipment */
				BigDecimal priceActual = BigDecimal.ZERO;
				if (filter.getMapPriceActually() != null && !filter.getMapPriceActually().isEmpty()) {
					if (lstEquipId != null && lstEquipId.size() > 0) {
						Long pActually = filter.getMapPriceActually().get(lstEquipId.get(0));
						if (pActually != null) {
							priceActual = BigDecimal.valueOf(pActually);
						}
					}
				}
				equipLostRecord.setPriceActually(priceActual);
				/** end  vuongmq; 26/05/2015; cho nay cap nhat gia tri con lai cho equipment */
				filter.setAttribute(equipLostRecord);
				List<Long> lstIdRes = equipRecordMgr.insertLoadRecordByListEquip(filter);
				
				//Neu la trang thai cho duyet se send mail
//				if (lstIdRes != null && !lstIdRes.isEmpty() && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(recordStatus)) {
//					String errMsgMail = "";
//					for (Long lostRecId:lstIdRes) {
//						errMsgMail = this.sendEmailEquipLostRecord(commonMgr.getEntityById(EquipLostRecord.class, lostRecId));
//					}
//					result.put("errMsgMail", errMsgMail);
//				}
			}

		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			//result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			//result.put("errMsg", "Luồng xử lý các tham số đầu vào không hợp lệ");
			result.put("errMsg", R.getResource("equip.lost.recorde.err.dau.vao.tham.so.ko.hop.le"));
			return JSON;
		}
		result.put(ERROR, flagError);
		result.put("errMsg", errMsg);
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Send email cho phieu bao mat o truong hop cho duyet
	 * 
	 * @author hunglm16
	 * @since 22/05/2015
	 * */
	private String sendEmailEquipLostRecord(EquipLostRecord lostRecord) {
		errMsg = "";
		try {
			ApParamEquip emailSend = apParamEquipMgr.getApParamEquipByCodeX(ApParamEquipType.EMAIL_SEND_LOST_RECORD.getValue(), ApParamEquipType.EMAIL_SEND_LOST_RECORD, ActiveType.RUNNING);
			if (emailSend != null && !StringUtil.isNullOrEmpty(emailSend.getApParamEquipName()) && !StringUtil.isNullOrEmpty(emailSend.getValue())) {
				//co name: ten email khac null va value: mat khau khac null moi send email
				/** lay mail nhan tu equip_param */
				Long shopId = null;
				Customer customer = null;
				if (lostRecord != null && EquipStockTotalType.KHO.equals(lostRecord.getStockType())) {
					EquipStock equipStock = equipmentManagerMgr.getEquipStockById(lostRecord.getStockId());
					if (equipStock != null && equipStock.getShop() != null) {
						shopId = equipStock.getShop().getId();
					}
				} else if (lostRecord != null && EquipStockTotalType.KHO_KH.equals(lostRecord.getStockType())) {
					customer = customerMgr.getCustomerById(lostRecord.getStockId());
					if (customer != null && customer.getShop() != null) {
						shopId = customer.getShop().getId();
					}
				}
				// lay danh sach mail can gui uu tien tu shop -> parent len
				if (shopId != null) {
					ApParamFilter paramFilter = new ApParamFilter();
					paramFilter.setShopId(shopId);
					paramFilter.setTypeStr(ApParamEquipType.EMAIL_TO_LOST_RECORD.getValue());
					List<ApParamEquipVO> lstEmailToVO = apParamEquipMgr.getListEmailToParentShopByFilter(paramFilter);
					if (lstEmailToVO != null && lstEmailToVO.size() > 0) {
						//lay mail
						String emailTo = "";
						for (ApParamEquipVO apParamEquipVO : lstEmailToVO) {
							if (apParamEquipVO != null && !StringUtil.isNullOrEmpty(apParamEquipVO.getApParamName())) {
								emailTo = apParamEquipVO.getApParamName().trim();
								break;
							}
						}
						if (!StringUtil.isNullOrEmpty(emailTo)) {
							String errMailTo = ValidateUtil.validateField(emailTo, "common.email", null, ConstantManager.ERR_INVALID_EMAIL);
							if (StringUtil.isNullOrEmpty(errMailTo)) {
								// luc nay lay thong tin send Email
								StringBuilder thongTin = new StringBuilder();
								Shop shop = shopMgr.getShopById(shopId);
								//[--Khoi tao noi dung Email bao mat--]
								thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.don.vi", "[" +shop.getShopCode() + " - " + shop.getShopName() + "] "));
								thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.gui.tieu.de.bao.mat"));
								if (customer != null) {
									thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.kh", "[" +customer.getShortCode() + " - " + customer.getCustomerName() + "] "));
								}
								if (lostRecord != null && lostRecord.getEquip() != null && lostRecord.getEquip().getEquipGroup() != null) {
									thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.name.group.code.eqip","[" +lostRecord.getEquip().getEquipGroup().getName() + " - " + lostRecord.getEquip().getCode() + "] "));
								} else if (lostRecord != null && lostRecord.getEquip() != null ) {
									thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.name.group.code.eqip","[" + lostRecord.getEquip().getCode() + "] "));
								}
								thongTin.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.lost.rec.send.mail.gui.ma.phieu.tb","[" + lostRecord.getCode() + "] "));
								//[--Thuc thi khoi tao Email hoan chinh--]
								//Emailer emailer = new Emailer();
								SendMailBySite emailer = new SendMailBySite();
								StringBuilder body = new StringBuilder();
								//body.append("Kinh gui nguoi phe duyet, \n ");
								body.append(R.getResource("equipment.lost.rec.send.mail.gui.nguoi.phe.duyet"));
								body.append("\n");
								body.append(thongTin);
								emailer.sendEmail(emailSend.getApParamEquipName(), emailSend.getValue(), emailTo, thongTin.toString(), body.toString());
							} else {
								//errMsg = "Tham so caui hinh Email nhan khong dung dinh dang.";
								errMsg = R.getResource("equipment.lost.rec.send.mail.nhan.khong.dung.dinh.dang");
							}
						} else {
							//errMsg = "Tham so cau hinh Emqil nhan khong ton tai.";
							errMsg = R.getResource("equipment.lost.rec.send.mail.nhan.khong.ton.tai");
						}
					} else {
						//errMsg ="Tham so cau hinh khong ton tai Email nhan.";
						errMsg = R.getResource("equipment.lost.rec.send.mail.nhan.khong.ton.tai.trong.cau.hinh");
					}
				} else {
					//errMsg = "Don vi khong ton tai de gui Email phieu cho duyet.";
					errMsg = R.getResource("equipment.lost.rec.send.mail.don.vi.khong.ton.tai.de.send.email.cho.duyet");
				}
			} else {
				//errMsg = "Eqmil gui khong ton tai hoac khong dung Mat khau.";
				errMsg = R.getResource("equipment.lost.rec.send.mail.gui.khong.ton.tai.hoac.pass.khong.dung");
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage()+" Equipmentlost.recManageAction.sendEmaillost.rec()");
			errMsg = R.getResource("equipment.lost.rec.send.mail.khong.thanh.cong");
		}
		return errMsg;
	}
	
	/**
	 * Upload File
	 * 
	 * @author hunglm16
	 * @since January 30,2015
	 * */
	private List<FileVO> saveUploadFileByEquip(List<File> lstFile, List<String> lstFileFileName, List<String> lstFileContentType) {
		try {
			if (lstFile != null && lstFile.size() > 0) {
				//String perfix = Configuration.getPrefixFileEquipServerDocPath();
				List<FileVO> lstFileVO = FileUtility.storeFileOnDisk(lstFile, lstFileFileName, lstFileContentType, null, EquipTradeType.NOTICE_LOST.getValue());
				if (lstFileVO != null && !lstFileVO.isEmpty()) {
					String thumbUrl = Configuration.getEquipThumbUrlAttachFile();
					for (FileVO voFile : lstFileVO) {
						voFile.setThumbUrl(thumbUrl);
					}
				}
				return lstFileVO;
			}
			System.gc();
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.saveUploadFileByEquip()" + e.getMessage());
		}
		return new ArrayList<FileVO>();
	}

	/**
	 * Load trang Chi tiet Danh sach Thiet bi
	 * 
	 * @author hunglm16
	 * @since December 17,2014
	 * @description Chuc nang Danh Sach Thiet Bi
	 * */
	
	/**
	 * Load trang Chi tiet bao mat
	 * @author vuongmq
	 * @since 27/04/2015
	 * @description Chi tiet bao mat, tao moi or cap nhat
	 * */
	public String viewDetailEquipLostRecordJSP() {
		try {
			//if (!ShopDecentralizationSTT.NPP.getValue().equals(currentUser.getShopRoot().getIsLevel())) {
			//		return PAGE_NOT_PERMISSION;
			//}
			flagMobile = 0;
			if (id != null) {
				/** cap nhat phieu bao mat */
				equipLostRecord = equipRecordMgr.getEquipLostRecordById(id);
				if (equipLostRecord == null || equipLostRecord.getId() == null) {
					return PAGE_NOT_FOUND;
				}
				equipId = equipLostRecord.getEquip().getId();
				/*if (!checkShopInOrgAccessByCode(shopCode)) {
					return PAGE_NOT_PERMISSION;
				}*/
				if (equipLostRecord != null && EquipStockTotalType.KHO_KH.equals(equipLostRecord.getStockType())) {
					flagLostRecord = EquipStockTotalType.KHO_KH.getValue(); // gan co: 2 bao mat kh
					if (equipLostRecord.getEquip() != null) {
						customer = customerMgr.getCustomerById(equipLostRecord.getEquip().getStockId());
						if (customer != null && customer.getShop() != null) {
							shopCode = customer.getShop().getShopCode();
						}
					}
					if (equipLostRecord.getCreateFormDate() != null) {
						createDate = DateUtil.convertDateByString(equipLostRecord.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY) ;
					}
				} else if (equipLostRecord != null && EquipStockTotalType.KHO.equals(equipLostRecord.getStockType())) {
					flagLostRecord = EquipStockTotalType.KHO.getValue(); // gan co: 1 bao mat npp
					EquipStock equipStock = equipmentManagerMgr.getEquipStockById(equipLostRecord.getStockId());
					if (equipStock != null && equipStock.getShop() != null) {
						shop = equipStock.getShop();
						shopCode = shop.getShopCode();
					}
					if (equipLostRecord.getCreateFormDate() != null) {
						createDate = DateUtil.convertDateByString(equipLostRecord.getCreateFormDate(), DateUtil.DATE_FORMAT_DDMMYYYY) ;
					}
				}
				if (!checkShopInOrgAccessByCode(shopCode)) {
					return PAGE_NOT_PERMISSION;
				}
				//Lay danh sach tap tin dinh kem neu co
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(id);
				filter.setObjectType(EquipTradeType.NOTICE_LOST.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
			} else {
				/** tao moi phieu bao mat */
				equipLostRecord = new EquipLostRecord();
				if (customerId != null || eqLostMobiRecId != null || equipId != null) {
					if (customerId == null || eqLostMobiRecId == null || equipId == null) {
						return PAGE_NOT_FOUND;
					}
					Equipment equpMb = equipmentManagerMgr.getEquipmentById(equipId);
					if (equpMb == null || !StatusType.ACTIVE.equals(equpMb.getStatus())) {
						return PAGE_NOT_FOUND;
					}
					customer = customerMgr.getCustomerById(customerId);
					if (customer == null) {
						return PAGE_NOT_FOUND;
					}
					EquipLostMobileRec eqLostMobiRec = equipRecordMgr.getEquipLostMobileRecById(eqLostMobiRecId);
					if (eqLostMobiRec == null || (eqLostMobiRec.getEquipLostRecId() != null && eqLostMobiRec.getEquipLostRecId() > 0)) {
						return PAGE_NOT_FOUND;
					}
					shopCode = customer.getShop().getShopCode();
					if (!checkShopInOrgAccessByCode(shopCode)) {
						return PAGE_NOT_PERMISSION;
					}
					flagMobile = 1;
				}
				flagLostRecord = EquipStockTotalType.KHO_KH.getValue(); // gan co: 2 bao mat KH lam mac dinh
				//shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				/** vuongmq; 19/05/2015 lay shop cua NPP; truong hop user quan ly duy nhat 1 shop; thi disable luon txtShopCode*/
				if (currentUser != null && currentUser.getListShop() != null && currentUser.getListShop().size() == 1) {
					shopCode = currentUser.getListShop().get(0).getShopCode();
					shop = shopMgr.getShopByCode(shopCode); // vuongmq; 20/05/2015; de lay so dien thoai va  phone duoi Js
				}
				
			}
			phoneStr = this.viewPhoneCustomer(customer);
			sysDateStr = DateUtil.convertDateByString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			isLevel = currentUser.getShopRoot().getIsLevel();
			//ShopDecentralizationSTT
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.viewDetailEquipLostRecordJSP()" + e.getMessage());
		}
		return SUCCESS;
	}
	
	
	/**
	 * Load trang Chi tiet bao mat; phe duyet Approve
	 * @author vuongmq
	 * @since 15/04/2015
	 * @description Chi tiet bao mat cap nhat phe duyet Approve
	 * */
	public String viewDetailEquipLostRecordJSPApprove() {
		try {
			//if (!ShopDecentralizationSTT.NPP.getValue().equals(currentUser.getShopRoot().getIsLevel())) {
			//		return PAGE_NOT_PERMISSION;
			//}
			flagMobile = 0;
			if (id != null) {
				/** cap nhat phieu bao mat */
				equipLostRecord = equipRecordMgr.getEquipLostRecordById(id);
				if (equipLostRecord == null || equipLostRecord.getId() == null) {
					return PAGE_NOT_FOUND;
				}
				equipId = equipLostRecord.getEquip().getId();
				/*if (!checkShopInOrgAccessByCode(shopCode)) {
					return PAGE_NOT_PERMISSION;
				}*/
				if (equipLostRecord != null && EquipStockTotalType.KHO_KH.equals(equipLostRecord.getStockType())) {
					flagLostRecord = EquipStockTotalType.KHO_KH.getValue(); // gan co: 2 bao mat kh
					if (equipLostRecord.getEquip() != null) {
						customer = customerMgr.getCustomerById(equipLostRecord.getEquip().getStockId());
						if (customer != null && customer.getShop() != null) {
							shopCode = customer.getShop().getShopCode();
						}
					}
				} else if (equipLostRecord != null && EquipStockTotalType.KHO.equals(equipLostRecord.getStockType())) {
					flagLostRecord = EquipStockTotalType.KHO.getValue(); // gan co: 1 bao mat npp
					EquipStock equipStock = equipmentManagerMgr.getEquipStockById(equipLostRecord.getStockId());
					if (equipStock != null && equipStock.getShop() != null) {
						shop = equipStock.getShop();
						shopCode = shop.getShopCode();
					}
				}
				if (!checkShopInOrgAccessByCode(shopCode)) {
					return PAGE_NOT_PERMISSION;
				}
				//Lay danh sach tap tin dinh kem neu co
				EquipRecordFilter<FileVO> filter = new EquipRecordFilter<FileVO>();
				filter.setObjectId(id);
				filter.setObjectType(EquipTradeType.NOTICE_LOST.getValue());
				lstFileVo = equipRecordMgr.searchListFileVOByFilter(filter).getLstObject();
				if (lstFileVo == null || lstFileVo.isEmpty()) {
					lstFileVo = new ArrayList<FileVO>();
				}
			} else {
				/** tao moi phieu bao mat */
				equipLostRecord = new EquipLostRecord();
				if (customerId != null || eqLostMobiRecId != null || equipId != null) {
					if (customerId == null || eqLostMobiRecId == null || equipId == null) {
						return PAGE_NOT_FOUND;
					}
					Equipment equpMb = equipmentManagerMgr.getEquipmentById(equipId);
					if (equpMb == null || !StatusType.ACTIVE.equals(equpMb.getStatus())) {
						return PAGE_NOT_FOUND;
					}
					customer = customerMgr.getCustomerById(customerId);
					if (customer == null) {
						return PAGE_NOT_FOUND;
					}
					EquipLostMobileRec eqLostMobiRec = equipRecordMgr.getEquipLostMobileRecById(eqLostMobiRecId);
					if (eqLostMobiRec == null || (eqLostMobiRec.getEquipLostRecId() != null && eqLostMobiRec.getEquipLostRecId() > 0)) {
						return PAGE_NOT_FOUND;
					}
					shopCode = customer.getShop().getShopCode();
					if (!checkShopInOrgAccessByCode(shopCode)) {
						return PAGE_NOT_PERMISSION;
					}
					flagMobile = 1;
				}
				/** lay shop cua NPP*/
				flagLostRecord = EquipStockTotalType.KHO_KH.getValue(); // gan co: 2 bao mat KH lam mac dinh
				//shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			phoneStr = this.viewPhoneCustomer(customer);
			sysDateStr = DateUtil.convertDateByString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			isLevel = currentUser.getShopRoot().getIsLevel();
			//ShopDecentralizationSTT
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.viewDetailEquipLostRecordJSP()" + e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * @author vuongmq
	 * @since 15/04/2015
	 * @description lay thong tin phone, address shop khi change textbox Shop
	 * */
	public String searchCustomerLost() {
		try {
			Customer customer = null;
			if (shopId != null) {
				customer = customerMgr.getCustomerById(customerId);
				if (customer != null) {
					result.put("rows", customer);
					result.put("nameCustomer", customer.getCustomerName());
					result.put("phoneCustomer", this.viewPhoneCustomer(customer));
					result.put("addressCustomer", customer.getAddress());
				}
			} 
			if (customer == null && !StringUtil.isNullOrEmpty(shopCode) && !StringUtil.isNullOrEmpty(shortCode)) {
				String code = shortCode.toUpperCase().trim() + "_" + shopCode.toUpperCase().trim();
				customer = customerMgr.getCustomerByCode(code);
				if (customer != null) {
					result.put("rows", customer);
					result.put("nameCustomer", customer.getCustomerName());
					result.put("phoneCustomer", this.viewPhoneCustomer(customer));
					result.put("addressCustomer", customer.getAddress());
				}
			}
			if (customer == null) {
				result.put("rows", "");
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.viewDetailEquipLostRecordJSP()" + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * @author vuongmq
	 * @since 15/04/2015
	 * @description lay thong tin phone, address shop khi change textbox Shop
	 * */
	public String searchShopLost() {
		try {
			Shop shopView = null;
			if (shopId != null) {
				shopView = shopMgr.getShopById(shopId);
				if (shopView != null) {
					result.put("rows", shopView);
					result.put("phoneShop", this.viewPhoneShop(shopView));
					result.put("addressShop", shopView.getAddress());
				}
			} 
			if (shopView == null && !StringUtil.isNullOrEmpty(shopCode)) {
				shopView = shopMgr.getShopByCode(shopCode.trim().toUpperCase());
				if (shopView != null) {
					result.put("rows", shopView);
					result.put("phoneShop", this.viewPhoneShop(shopView));
					result.put("addressShop", shopView.getAddress());
				}
			}
			if (shopView == null) {
				result.put("rows", "");
			}
		} catch (Exception e) {
			LogUtility.logError(e, "EquipLostRecordAction.viewDetailEquipLostRecordJSP()" + e.getMessage());
		}
		return JSON;
	}
	/**
	 * @author vuongmq
	 * @date 07/05/2015
	 * Lay phone cho Customer
	 */
	private String viewPhoneCustomer(Customer customer) {
		phoneStr = "";
		if (customer != null) {
			if (!StringUtil.isNullOrEmpty(customer.getPhone()) && !StringUtil.isNullOrEmpty(customer.getMobiphone())) {
				phoneStr = customer.getPhone().trim() + " - " + customer.getMobiphone().trim();
			} else if (!StringUtil.isNullOrEmpty(customer.getPhone())) {
				phoneStr = customer.getPhone().trim();
			} else if (!StringUtil.isNullOrEmpty(customer.getMobiphone())) {
				phoneStr = customer.getMobiphone().trim();
			}

		}
		return phoneStr;
	}
	
	/**
	 * @author vuongmq
	 * @date 07/05/2015
	 * Lay phone cho shop
	 */
	private String viewPhoneShop(Shop shop) {
		phoneStr = "";
		if (shop != null) {
			if (!StringUtil.isNullOrEmpty(shop.getPhone()) && !StringUtil.isNullOrEmpty(shop.getMobiphone())) {
				phoneStr = shop.getPhone().trim() + " - " + shop.getMobiphone().trim();
			} else if (!StringUtil.isNullOrEmpty(shop.getPhone())) {
				phoneStr = shop.getPhone().trim();
			} else if (!StringUtil.isNullOrEmpty(shop.getMobiphone())) {
				phoneStr = shop.getMobiphone().trim();
			}

		}
		return phoneStr;
	}
	
	/**
	 * Chi tiet thiet bi
	 * 
	 * @author tientv11
	 */
	public String euipDetailLostMobile() {
		try {
			List<EquipmentVO> lst = equipRecordMgr.getListEquipmentVOByEquiId(id);
			if (lst != null && lst.size() > 0) {
				result.put("rows", lst);
			} else {
				result.put("rows", new ArrayList<EquipmentVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem Danh sach bao mat tu may tinh bang
	 * 
	 * @author tientv11
	 */
	public String searchLostRecordMobile() {
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipLostMobileRecVO>());

			EquipmentFilter<EquipLostMobileRecVO> filter = new EquipmentFilter<EquipLostMobileRecVO>();

			KPaging<EquipLostMobileRecVO> kPaging = new KPaging<EquipLostMobileRecVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);

			if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
				filter.setFlagCms(1);
			}
			//filter.setShopRoot(currentUser.getShopRoot().getShopId());
			/** vuongmq; 28/05/2015; cap nhat them shopCode */
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(shopCode);
				Shop shopSearch = shopMgr.getShopByCode(shopCode);
				if (shopSearch != null) {
					//filter.setShopId(shopSearch.getId());
					filter.setShopRoot(shopSearch.getId());
				} else {
					// đơn vị không tồn tại
					result.put("errMsg", R.getResource("po.conf.group.err.imp.isShop.isUnDefined"));
					return JSON;
				}
			} else {
				filter.setShopRoot(currentUser.getShopRoot().getShopId());
				// Mã NPP không được để trống
				//result.put("errMsg", R.getResource("imp.tuyen.clmn.error.maNPP.null"));
				//return JSON;
			}
			filter.setCustomerCode(stockCode);
			filter.setCustomerName(customerName);
			filter.setCustomerAddress(address);

			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}

			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}

			if (recordStatus != null && recordStatus >= 0) {
				filter.setRecordStatus(recordStatus);
			}

			if (processingStatus != null && processingStatus > 0) {
				filter.setEquipLostId(processingStatus.longValue());
			}

			ObjectVO<EquipLostMobileRecVO> objectVO = equipRecordMgr.getListEquipLostMobileRecByFilter(filter);
			if (objectVO != null) {
				result.put("total", objectVO.getkPaging().getTotalRows());
				result.put("rows", objectVO.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Cap nhat trang thai
	 * 
	 * @author tientv11
	 */
	public String updateLostRecordMobile() {
		try {
			errMsg = "";
			result.put(ERROR, false);
			EquipRecordFilter<EquipLostMobileRec> filter = new EquipRecordFilter<EquipLostMobileRec>();
			if (listLostRecordMobile == null || listLostRecordMobile.size() == 0) {
				errMsg = "Chưa biên bản nào được chọn";
			} else {
				if (currentUser == null || currentUser.getShopRoot() == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.undefined"));
					return SUCCESS;
				}
				filter.setLstId(listLostRecordMobile);
				filter.setShopId(currentUser.getShopRoot().getShopId());
				List<EquipLostMobileRec> lstData = equipRecordMgr.searchEquipRecordFilter(filter).getLstObject();
				if (lstData == null || lstData.isEmpty()) {
					errMsg = "Danh sách báo mất không hợp lệ";
				} else {
					listLostRecordMobile = new ArrayList<Long>();
					for (EquipLostMobileRec recItem : lstData) {
						if (recItem.getEquipLostRecId() == null || recItem.getEquipLostRecId() <= 0) {
							listLostRecordMobile.add(recItem.getId());
						}
					}
					if (listLostRecordMobile == null || (listLostRecordMobile != null && listLostRecordMobile.size() == 0)) {
						errMsg = "Danh sách báo mất không có trường hợp nào hợp lệ";
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
			filter = new EquipRecordFilter<EquipLostMobileRec>();
			filter.setLstId(listLostRecordMobile);
			filter.setUserName(currentUser.getUserName());
			equipRecordMgr.updateByListRecordMobile(filter);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Import Excel Price
	 * 
	 * @author hunglm16
	 * @since SEPTEMBER 3, 2014
	 * @description Import Excel Price
	 * */
	public String importEquipLostRecord() {
		//IMPORT EXCEL - THEM MOI BAO MAT
		isError = true;
		totalItem = 0;
		try {
			//tamvnm: cap nhat dac ta moi
//			EquipPeriod equipPeriod = equipmentManagerMgr.getEquipPeriodCurrent();
//			if (equipPeriod == null || !equipPeriod.getStatus().getValue().equals(EquipPeriodType.OPENED.getValue())) {
//				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.null");
//				result.put(ERROR, true);
//				result.put("errMsg", errMsg);
//				return SUCCESS;
//			}
			resetToken(result);
//			EquipPeriod equipPeriod = null;
			/////Xu ly nghiep vu import file
			lstView = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 14);
			Date sysDateDb = commonMgr.getSysDate();
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(currentUser.getUserName());
				EquipRecordFilter<EquipLostRecord> filter = new EquipRecordFilter<EquipLostRecord>();
				//Shop shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				//shopCode = shop.getShopCode();

				//Lay tat ca cac kenh thoa man
				Customer customer = new Customer();
				Shop shopImport = null;
				EquipLostRecord equipLostRec = new EquipLostRecord();
				List<Long> lstEquipId = new ArrayList<Long>();
				Map<Long, Long> mapEquipDeliveryRec = new HashMap<Long, Long>();
				Equipment equipImport = new Equipment();
				Integer tracingResult = null;
				Integer tracingPlace = null;
				String customerCode = "";
				String message = "";
				String msg = "";
				String value = "";
				boolean flag = false;
				Integer BAO_MAT_NPP = 1;
				Integer BAO_MAT_KH = 2;
				Integer viewBaoMat = BAO_MAT_NPP;
				Date ngayLap = null;
				String noteStr = null;
				Date now = commonMgr.getSysDate();
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					msg = "";
					value = "";
					message = "";
					totalItem++;
					customer = new Customer();
					shopImport = null;
					equipLostRec = new EquipLostRecord();
					filter = new EquipRecordFilter<EquipLostRecord>();
					lstEquipId = new ArrayList<Long>();
					mapEquipDeliveryRec = new HashMap<Long, Long>();
					equipImport = new Equipment();
					tracingPlace = null;
					tracingResult = null;
					customerCode = "";
					flag = false;
					ngayLap = null;
					viewBaoMat = BAO_MAT_NPP; // 1: bao mat NPP; 2: bao mat KH
					List<String> row = lstData.get(i);
					//Ap dung cho lay gia la duy nhat
					if (row != null && row.size() > 10) {
						for (int j = 0, size = row.size(); j < size; j++) {
							if (!StringUtil.isNullOrEmpty(row.get(j))) {
								flag = true;
								break;
							}
						}
					}
					if (flag) {
						// Ma NPP; vuongmq 11/05/2015
						value = row.get(0);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							shopImport = shopMgr.getShopByCode(value.toUpperCase().trim());
							if (shopImport != null) {
								//msg = "Mã NPP phải thuộc NPP đăng nhập.";
								//msg = R.getResource("equip.lost.recorde.err.npp.thuoc.dang.nhap");
								if (ActiveType.RUNNING.equals(shopImport.getStatus())) {
									shopCode = shopImport.getShopCode();
								} else {
									//imp.tuyen.clmn.error.maNPP.stop = Mã NPP không còn hoạt động
									msg = R.getResource("imp.tuyen.clmn.error.maNPP.stop");
								}
							} else {
								//imp.tuyen.clmn.error.maNPP.undefined = Mã NPP không có trong hệ thống
								msg = R.getResource("imp.tuyen.clmn.error.maNPP.undefined");
							}
						} else {
							//imp.tuyen.clmn.error.maNPP.null = Mã NPP không được để trống
							msg = R.getResource("imp.tuyen.clmn.error.maNPP.null");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						///Ma khach hang
						value = row.get(1);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							/** ma khach hang co the de trong*/
							viewBaoMat = BAO_MAT_KH;
							if (!StringUtil.isNullOrEmpty(shopCode)) {
								customerCode = value.toUpperCase().trim() + "_" + shopCode.toUpperCase().trim();
								customer = customerMgr.getCustomerByCode(customerCode);
								if (customer != null) {
//									if (!ActiveType.RUNNING.equals(customer.getStatus())) {
										//common.catalog.customer.status.not.active = Mã khách hàng {0} đang ở trạng thái không hoạt động.
//										msg = R.getResource("common.catalog.customer.status.not.active", value);
//									} else {
										stockCode = shopCode.trim().toUpperCase() + customer.getShortCode().trim().toUpperCase();
										stockCode = stockCode.trim();
//									}
								} else {
									//msg = "Mã khách hàng không tồn tại";
									msg = R.getResource("equip.lost.recorde.err.customer.not.exists");
								}
							}
						} else {
							viewBaoMat = BAO_MAT_NPP;
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						
						
						// ** Ngay lap */
						if (row.size() > 2) {
							value = row.get(2);
							msg = ValidateUtil.validateField(value, "equipment.create.form.date", 50, ConstantManager.ERR_REQUIRE);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}else{
								msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date",null);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									ngayLap = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
									if (ngayLap == null) {
										message += ValidateUtil.getErrorMsgForInvalidFormatDate(value, "equipment.create.form.date", null);
									} else {
										if (DateUtil.compareTwoDate(ngayLap, now) == 1) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equipment.manager.create.form.date.big") + "\n";
//											
										} else {
//											//tamvnm: update dac ta moi. 30/06/2015
//											List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(ngayLap);
//											if (lstPeriod == null || lstPeriod.size() == 0) {
//												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.null");
//											} else if (lstPeriod.size() > 0) {
//												// gan gia tri ky dau tien.
//												equipPeriod = lstPeriod.get(0);
//											}
										}
									}
								}
							}							
						}
						
						///Nguoi dai dien
						value = row.get(3);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "equip.lost.rec.impl.clm.representor", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
						} else {
							//msg = "Người đại diện không được để trống";
							msg = R.getResource("equip.lost.recorde.err.representor.not.require");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						} else {
							filter.setRepresentor(value.trim());
						}
						///Ma thiet bi
						value = row.get(4);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							equipImport = equipmentManagerMgr.getEquipmentByCodeAndStatus(value, null);
							if (equipImport != null) {
								if (StatusType.ACTIVE.equals(equipImport.getStatus())) {
									if (EquipTradeStatus.NOT_TRADE.getValue().equals(equipImport.getTradeStatus())) {
										if (BAO_MAT_NPP.equals(viewBaoMat)) {
											if (!EquipStockTotalType.KHO.equals(equipImport.getStockType())) {
												//msg = "Mã thiết bị không phải loại kho NPP.";
												msg = R.getResource("equip.lost.recorde.err.ma.thiet.bi.ko.phai.kho.npp");
											} else if (!EquipUsageStatus.SHOWING_WAREHOUSE.equals(equipImport.getUsageStatus())) {
												//msg = "Mã thiết bị thuộc NPP phải đang ở kho.";
												msg = R.getResource("equip.lost.recorde.err.ma.thiet.kho.npp.phai.o.kho");
											} else {
												// kiem tra 
												if (shopImport != null) {
													EquipmentFilter<EquipmentVO> filterEquip = new EquipmentFilter<EquipmentVO>();
													filterEquip.setShopId(shopImport.getId()); // NPP la shopId cua cot MaNPP
													filterEquip.setStockType(EquipStockTotalType.KHO.getValue());
													filterEquip.setEquipId(equipImport.getId());
													filterEquip.setStaffRoot(currentUser.getUserId());
													ObjectVO<EquipmentVO> voEquip = equipmentManagerMgr.searchEquipByEquipDeliveryRecordNew(filterEquip);
													if ((voEquip != null && voEquip.getLstObject() == null) || (voEquip.getLstObject() != null && voEquip.getLstObject().size() <= 0)) {
														//msg = "Mã thiết bị phải đang thuộc kho NPP mà người dùng được phân quyền.";
														msg = R.getResource("equip.lost.recorde.err.ma.thiet.kho.npp.ma.nguoi.dung.duoc.phan.quyen");
													}
												}
											}
										} else {
											if (!EquipStockTotalType.KHO_KH.equals(equipImport.getStockType())) {
												//msg = "Mã thiết bị không phải loại kho Khách hàng.";
												msg = R.getResource("equip.lost.recorde.err.ma.thiet.bi.ko.phai.kho.kh");
											} else if (customer != null && !equipImport.getStockId().equals(customer.getId())) {
												//msg = "Mã thiết bị không thuộc Khách hàng.";
												msg = R.getResource("equip.lost.recorde.err.ma.thiet.bi.ko.thuoc.kh");
											} else if (!EquipUsageStatus.IS_USED.equals(equipImport.getUsageStatus())) {
												//msg = "Mã thiết bị thuộc khách hàng phải đang sử dụng.";
												msg = R.getResource("equip.lost.recorde.err.ma.thiet.bi.thuoc.kh.phai.dang.su.dung");
											} else {
												// kiem tra 
												/*EquipmentFilter<EquipmentVO> filterEquip = new EquipmentFilter<EquipmentVO>();
												filterEquip.setStockId(customer.getId()); // KH la id cua customer
												filterEquip.setStockType(EquipStockTotalType.KHO_KH.getValue());
												filterEquip.setEquipId(equipImport.getId());
												ObjectVO<EquipmentVO> voEquip = equipmentManagerMgr.searchEquipByEquipDeliveryRecordNew(filterEquip);
												if (voEquip != null && voEquip.getLstObject() == null || (voEquip.getLstObject() != null && voEquip.getLstObject().size() <= 0)) {
													msg = "Thiết bị phải thuộc người dùng.";
												}*/
											}
										}
									} else {
										//msg = "Mã thiết bị đang giao dịch.";
										msg = R.getResource("equip.lost.recorde.err.ma.thiet.bi.dang.giao.dich");
									}
								} else {
									//msg = "Mã thiết bị không còn Hoạt động.";
									msg = R.getResource("equip.lost.recorde.err.ma.thiet.bi.ko.hoat.dong");
								}
							} else {
								//msg = "Mã thiết bị không tồn tại.";
								msg = R.getResource("equip.lost.recorde.err.ma.thiet.bi.ko.ton.tai");
							}
						} else {
							//msg = "Mã thiết bị không được để trống.";
							msg = R.getResource("equip.lost.recorde.err.ma.thiet.bi.ko.require");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						} else {
							lstEquipId.add(equipImport.getId());
							filter.setLstLong(lstEquipId);
						}
						boolean flagSSDate = true;
						// Gia tri con lai; vuongmq 11/05/2015
						value = row.get(5);
						msg = "";
						BigDecimal priceActually = BigDecimal.ZERO;
						if (!StringUtil.isNullOrEmpty(value)) {
							String errPriceActually = ValidateUtil.validateField(value, "equip.lost.recorde.price.actually", 17, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(errPriceActually)) {
								msg = errPriceActually;
							} else {
								BigDecimal tmp = BigDecimal.valueOf(Long.valueOf(value));
								// update nhutnn, 05/08/2015, kiem tra neu lon hon nguyen gia 
								if (equipImport != null && equipImport.getPrice() != null && equipImport.getPrice().compareTo(tmp) == -1) {
									msg = R.getResource("equip.lost.value.less");
								}
								if (StringUtil.isNullOrEmpty(msg)) {
									priceActually = tmp;
									filter.setPriceActually(priceActually);
								}								
							}
						} else {
							//msg = "Giá trị còn lại không được để trống.";
							msg = R.getResource("equip.lost.recorde.price.actually.not.require");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						///Thoi gian mat
						value = row.get(6);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "equip.lost.rec.impl.clm.lostDate", 10, ConstantManager.ERR_INVALID_DATE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)
									&& DateUtil.compareDateWithoutTime(DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.parse(DateUtil.convertDateByString(sysDateDb, DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.DATE_FORMAT_DDMMYYYY)) > 0) {
								//msg = "Thời gian mất phải nhỏ hơn hoặc bằng ngày hiện tại.";
								msg = R.getResource("equip.lost.rec.impl.clm.lostDate.nho.hon.bang.hien.tai");
							}
						} else {
							//msg = "Thời gian mất không được để trống.";
							msg = R.getResource("equip.lost.rec.impl.clm.lostDate.not.require");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							flagSSDate = false;
							message += msg + "\n";
						} else {
							Date lostDate = DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
							filter.setLostDate(lostDate);
							//tamvnm: update dac ta moi. 30/06/2015
//							List<EquipPeriod> lstPeriod = equipmentManagerMgr.getListEquipPeriodByDate(lostDate);
//							if (lstPeriod == null || lstPeriod.size() == 0) {
//								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.period.date.lost.not.in") + ConstantManager.NEW_LINE_CHARACTER;
//							} else if (lstPeriod.size() > 0) {
//								// gan gia tri ky dau tien.
//								equipPeriod = lstPeriod.get(0);
//							}
						}
						///Ngay het phat sinh doanh so
						value = row.get(7);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "equip.lost.rec.impl.clm.last.arising.sales.sate", 10, ConstantManager.ERR_INVALID_DATE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)
									&& DateUtil.compareDateWithoutTime(DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.parse(DateUtil.convertDateByString(sysDateDb, DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.DATE_FORMAT_DDMMYYYY)) > 0) {
								//msg = "Ngày hết phát sinh doanh số phải nhỏ hơn hoặc bằng ngày hiện tại.";
								msg = R.getResource("equip.lost.rec.impl.clm.last.arising.sales.sate.nho.hon.bang.hien.tai");
							}
						} else {
							//msg = "Ngày hết phát sinh doanh số không được để trống.";
							msg = R.getResource("equip.lost.rec.impl.clm.last.arising.sales.sate.not.require");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							flagSSDate = false;
							message += msg + "\n";
						} else {
							filter.setLastArisingSalesDate(DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY));
						}
						msg = "";
						if (flagSSDate && DateUtil.compareDateWithoutTime(DateUtil.parse(row.get(7).trim(), DateUtil.DATE_FORMAT_DDMMYYYY), DateUtil.parse(row.get(6).trim(), DateUtil.DATE_FORMAT_DDMMYYYY)) > 0) {
							//msg = "Ngày hết phát sinh doanh số phải nhỏ hơn hoặc bằng Thời gian báo mất.";
							msg = R.getResource("equip.lost.rec.impl.clm.last.arising.sales.sate.nho.hon.bang.lostDate");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						///Giam sat va NPP truy tim tu mat
						value = row.get(8);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							String[] arrGsNpp = value.split("-");
							if (StringUtil.isNullOrEmpty(arrGsNpp[0])) {
								//msg = "Giám sát và NPP truy tìm tủ mất chỉ nhận 0-Tại địa chỉ kinh doanh hoặc 1-Tại địa chỉ thường trú của Khách hàng.";
								msg = R.getResource("equip.lost.rec.impl.clm.tracingPlace.quy.dinh.nhap.gia.tri");
							} else {
								msg = ValidateUtil.validateField(arrGsNpp[0].trim(), "equip.lost.rec.impl.clm.tracingPlace", 1, ConstantManager.ERR_INTEGER, ConstantManager.ERR_INVALID_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									//msg = "Giám sát và NPP truy tìm tủ mất chỉ nhận 0-Tại địa chỉ kinh doanh hoặc 1-Tại địa chỉ thường trú của Khách hàng.";
									msg = R.getResource("equip.lost.rec.impl.clm.tracingPlace.quy.dinh.nhap.gia.tri");
								} else {
									tracingPlace = Integer.valueOf(arrGsNpp[0].toString().trim());
									if (tracingPlace != 0 && tracingPlace != 1) {
										//msg = "Giám sát và NPP truy tìm tủ mất chỉ nhận 0-Tại địa chỉ kinh doanh hoặc 1-Tại địa chỉ thường trú của Khách hàng.";
										msg = R.getResource("equip.lost.rec.impl.clm.tracingPlace.quy.dinh.nhap.gia.tri");
									} else {
										filter.setTracingPlace(tracingPlace);
									}
								}
							}
						} else {
							//msg = "Giám sát và NPP truy tìm tủ mất không được để trống.";
							msg = R.getResource("equip.lost.rec.impl.clm.tracingPlace.not.require");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						///Ket qua truy tim
						value = row.get(9);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							String[] arrKqtt = value.split("-");
							if (StringUtil.isNullOrEmpty(arrKqtt[0])) {
								//msg = "Kết quả truy tìm chỉ nhận 0-Tìm được khách hàng nhưng không thấy tủ hoặc 1-Không tìm thấy khách hàng.";
								msg = R.getResource("equip.lost.rec.impl.clm.tracingResult.quy.dinh.nhap.gia.tri");
							} else {
								msg = ValidateUtil.validateField(arrKqtt[0].trim(), "equip.lost.rec.impl.clm.tracingResult", 1, ConstantManager.ERR_INTEGER, ConstantManager.ERR_INVALID_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									//msg = "Kết quả truy tìm chỉ nhận 0-Tìm được khách hàng nhưng không thấy tủ hoặc 1-Không tìm thấy khách hàng.";
									msg = R.getResource("equip.lost.rec.impl.clm.tracingResult.quy.dinh.nhap.gia.tri");
								} else {
									tracingResult = Integer.valueOf(arrKqtt[0].toString().trim());
									if (tracingResult != 0 && tracingResult != 1) {
										//msg = "Kết quả truy tìm chỉ nhận 0-Tìm được khách hàng nhưng không thấy tủ hoặc 1-Không tìm thấy khách hàng.";
										msg = R.getResource("equip.lost.rec.impl.clm.tracingResult.quy.dinh.nhap.gia.tri");
									} else {
										filter.setTracingResult(tracingResult);
									}
								}
							}
						} else {
							//msg = "Kết quả truy tìm không được để trống.";
							msg = R.getResource("equip.lost.rec.impl.clm.tracingResult.not.require");
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						}
						///Ket luan
						value = row.get(10);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "equip.lost.rec.impl.clm.conclusion", 500, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						} else {
							filter.setConclusion(value.trim());
						}
						///Huong de xuat xu ly
						value = row.get(11);
						msg = "";
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "equip.lost.rec.impl.recommended.treatment", 500, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
						}
						if (!StringUtil.isNullOrEmpty(msg)) {
							message += msg + "\n";
						} else {
							filter.setRecommendedTreatment(value.trim());
						}
						// ** Ghi chu */
						if (row.size() > 12) {
							value = row.get(12);
							msg = ValidateUtil.validateField(value, "equipment.delivery.note", 500, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								noteStr = value;
							}
						}
					}
					//XU LY CAP NHAT VAO DB
					if (StringUtil.isNullOrEmpty(message)) {
						EquipmentFilter<EquipmentVO> equipFilter = new EquipmentFilter<EquipmentVO>();
						//set cac tham so tim kiem
						if (flagPagination != null && flagPagination) {
							KPaging<EquipmentVO> kPaging = new KPaging<EquipmentVO>();
							kPaging.setPageSize(max);
							kPaging.setPage(page - 1);
							equipFilter.setkPaging(kPaging);
						}
						/** vuongmq; 14/05/2015; them ma NPP; va staff dang nhap */
						if (currentUser != null) {
							equipFilter.setStaffRoot(currentUser.getUserId());
						} else {
							// loi khong ton tai user dang nhap
							result.put("errMsg", R.getResource("common.not.login"));
							return JSON;
						}
//						Shop shopSearch = null;
						if (!StringUtil.isNullOrEmpty(shopCode)) {
							equipFilter.setShopCode(shopCode);
//							shopSearch = shopMgr.getShopByCode(shopCode);
							if (shopImport != null) {
								equipFilter.setShopId(shopImport.getId());
							} else {
								// đơn vị không tồn tại
								result.put("errMsg", R.getResource("po.conf.group.err.imp.isShop.isUnDefined"));
								return JSON;
							}
						} else {
							// Mã NPP không được để trống
							result.put("errMsg", R.getResource("imp.tuyen.clmn.error.maNPP.null"));
							return JSON;
						}
						if (equipId != null && equipId > ActiveType.STOPPED.getValue()) {
							equipFilter.setEquipId(equipId);
						}
						if (eqDeliveryRecId != null && eqDeliveryRecId > ActiveType.STOPPED.getValue()) {
							equipFilter.setEqDeliveryRecId(eqDeliveryRecId);
						}
						if (eqLostRecId != null && eqLostRecId > ActiveType.STOPPED.getValue()) {
							equipFilter.setEquipLostId(eqLostRecId);
						}
						if (equipImport.getStockType() != null) {
							equipFilter.setStockType(equipImport.getStockType().getValue());
						}
						if (recordStatus != null) {
							equipFilter.setRecordStatus(recordStatus);
						}
						if (tradeStatus != null) {
							equipFilter.setTradeStatus(tradeStatus);
						}
						equipFilter.setStatus(StatusType.ACTIVE.getValue());
						/** vuongmq; 06/05/2015; them ma kho, ma Thiet bi bao mat: 1: NPP, 2: KH*/
						if (shopImport != null) {
							if (customer != null) {
								equipFilter.setStockId(customer.getId());
							} else {
								//"Khách hàng không tồn tại."
								result.put("errMsg", R.getResource("equip.lost.recorde.err.kh.ko.ton.tai"));
								return JSON;
							}
						}
						
						/** vuongmq; 1/05/2015; them ma Thiet bi de lay thiet bi*/
						if (!StringUtil.isNullOrEmpty(equipCode)) {
							equipFilter.setEquipCode(equipCode);
						}
						if (flagUpdate != null) {
							equipFilter.setFlagUpdate(flagUpdate);
						}
						//goi ham thuc thi tim kiem
						ObjectVO<EquipmentVO> objVO = equipmentManagerMgr.searchEquipByEquipDeliveryRecordNew(equipFilter);
						if (objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
							for(EquipmentVO equip : objVO.getLstObject()) {
								mapEquipDeliveryRec.put(equip.getEquipId(), equip.getEquipDeliveryRecordId());
							}
						}
						
						filter.setEqLostMobiRecId(null);
						filter.setShopCode(shopCode);
						filter.setShopCode(shortCode);
						//filter.setStockCode(stockCode); // filter set vao o duoi ung voi tung loai 1:NPP; 2: KH
						filter.setLstLong(lstEquipId);
						filter.setMapLongLong(mapEquipDeliveryRec);
//						filter.setEquipPeriod(equipPeriod);
						filter.setRecordStatus(StatusRecordsEquip.DRAFT.getValue());
						filter.setDeliveryStatus(DeliveryType.NOTSEND.getValue());
						filter.setUserName(currentUser.getUserName());

//						equipLostRec.setEquipPeriod(equipPeriod);
						equipLostRec.setCreateFormDate(ngayLap);
						equipLostRec.setNote(noteStr);
						equipLostRec.setEquip(equipImport);
						// equipLostRec.setCustomer(customer); //// vuongmq; lay id cua NPP hoac KH
						//equipLostRec.setStockType(EquipStockTotalType.KHO_KH.getValue());
						if (BAO_MAT_NPP.equals(viewBaoMat)) {
							equipLostRec.setStockType(EquipStockTotalType.KHO);
							equipLostRec.setStockId(equipImport.getStockId());
							equipLostRec.setStockCode(equipImport.getStockCode());
							equipLostRec.setStockName(equipImport.getStockName());
							equipLostRec.setAddress(shopImport.getAddress());
							equipLostRec.setPhone(this.viewPhoneShop(shopImport));
							
							filter.setStockCode(equipImport.getStockCode());
						} else {
							equipLostRec.setStockType(EquipStockTotalType.KHO_KH);
							equipLostRec.setStockId(customer.getId());
							equipLostRec.setStockCode(stockCode); // stockCode= maNPP+ customer short
							equipLostRec.setStockName(equipImport.getStockName());
							equipLostRec.setAddress(customer.getAddress());
							equipLostRec.setPhone(this.viewPhoneCustomer(customer));
							
							filter.setStockCode(stockCode);
						}
						equipLostRec.setLostDate(filter.getLostDate());
						equipLostRec.setLastArisingSalesDate(filter.getLastArisingSalesDate());
						equipLostRec.setTracingPlace(tracingPlace);
						equipLostRec.setTracingResult(tracingResult);
						equipLostRec.setConclusion(filter.getConclusion());
						equipLostRec.setRecommendedTreatment(filter.getRecommendedTreatment());
						//equipLostRec.setRecordStatus(StatusRecordsEquip.DRAFT.getValue());
						equipLostRec.setRecordStatus(StatusRecordsEquip.DRAFT);
						//equipLostRec.setDeliveryStatus(DeliveryType.NOTSEND.getValue());
						equipLostRec.setDeliveryStatus(DeliveryType.NOTSEND);
						equipLostRec.setPriceActually(filter.getPriceActually()); // them PriceActually vao equipLostRec
						equipLostRec.setCreateUser(currentUser.getUserName());
						equipLostRec.setRepresentor(filter.getRepresentor());
						filter.setAttribute(equipLostRec);
						equipRecordMgr.insertLoadRecordByListEquip(filter);
					} else {
						lstView.add(StringUtil.addFailBean(row, message));
					}
				}
				//Export error
				getOutputFailExcelFile(lstView, ConstantManager.TEMPLATE_EQUIP_LOST_RECORD_FAIL);

			} else {
				isError = true;
				//errMsg = "Tập tin Excel chưa nhập dữ liệu Import";
				errMsg = R.getResource("import.error.not.data");
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua, 
	 * lay theo phan quyen; popup thiet bi
	 * equipmentManagerMgr.getListEquipmentLostPopUp(equipmentFilter);
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	public String searchEquipmentLost() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentDeliveryVO>());
		try {
			KPaging<EquipmentDeliveryVO> kPaging = new KPaging<EquipmentDeliveryVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter = new EquipmentFilter<EquipmentDeliveryVO>();
			equipmentFilter.setkPaging(kPaging);
			/** vuongmq; 14/05/2015; them ma NPP; va staff dang nhap */
			if (currentUser != null) {
				equipmentFilter.setStaffRoot(currentUser.getUserId());
			} else {
				// loi khong ton tai user dang nhap
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
			Shop shopSearch = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				equipmentFilter.setShopCode(shopCode);
				shopSearch = shopMgr.getShopByCode(shopCode);
				if (shopSearch != null) {
					equipmentFilter.setShopId(shopSearch.getId());
				} else {
					// đơn vị không tồn tại
					result.put("errMsg", R.getResource("po.conf.group.err.imp.isShop.isUnDefined"));
					return JSON;
				}
			} else {
				// Mã NPP không được để trống
				result.put("errMsg", R.getResource("imp.tuyen.clmn.error.maNPP.null"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (categoryId != null) {
				equipmentFilter.setEquipCategoryId(categoryId);
			}
			if (groupId != null) {
				equipmentFilter.setEquipGroupId(groupId);
			}
			if (providerId != null) {
				equipmentFilter.setEquipProviderId(providerId);
			}
			if (yearManufacture != null) {
				equipmentFilter.setYearManufacture(yearManufacture);
			}
			if (!StringUtil.isNullOrEmpty(stockCode)) {
				equipmentFilter.setStockCode(stockCode);
			}
			equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			//equipmentFilter.setUsageStatus(usageStatus);//2,3
			/** vuongmq; 06/05/2015; them stockId va stockType*/
			if (stockType != null) {
				equipmentFilter.setStockType(stockType);
			}
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				// co kho KH
				if (shopSearch != null) {
					String code = shortCode + "_" + shopSearch.getShopCode();
					Customer customer = customerMgr.getCustomerByCode(code);
					if (customer != null) {
						equipmentFilter.setStockId(customer.getId());
					} else {
						//"Khách hàng không tồn tại."
						result.put("errMsg", R.getResource("equip.lost.recorde.err.kh.ko.ton.tai"));
						return JSON;
					}
				}
			}
			// truyen len tradeStatus: 0, de lay thiet bi co trang thai giao dich: 0 binh thuong cho chinh xac
			if (tradeStatus != null) {
				equipmentFilter.setTradeStatus(tradeStatus);
			}
			if (flagUpdate != null) {
				equipmentFilter.setFlagUpdate(flagUpdate);
			}
			//ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentDeliveryVOByFilterEx(equipmentFilter);
			//ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentPopUp(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> equipmentDeliverys = equipRecordMgr.getListEquipmentLostPopUp(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getkPaging().getTotalRows());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Tim kiem kho tren popup thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 * 	Nếu là báo mất NPP: thì phải đang thuộc kho NPP mà người dùng được phân quyền và phải thuộc kho NPP đăng nhập.
	 * @return
	 */
	public String searchStockLost() {
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			KPaging<EquipStockVO> kPaging = new KPaging<EquipStockVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipStockFilter filter = new EquipStockFilter();
			filter.setkPaging(kPaging);
			/** vuongmq; 14/05/2015; them ma NPP; va staff dang nhap */
			if (currentUser != null) {
				filter.setStaffRoot(currentUser.getUserId());
			} else {
				// loi khong ton tai user dang nhap
				result.put("errMsg", R.getResource("common.not.login"));
				return JSON;
			}
			// arrShopTxt la shop chon can them bao mat
			if (!StringUtil.isNullOrEmpty(arrShopTxt)) {
				filter.setShopCode(arrShopTxt);
				Shop shopSearch = shopMgr.getShopByCode(arrShopTxt);
				if (shopSearch != null) {
					filter.setShopId(shopSearch.getId());
				} else {
					// đơn vị không tồn tại
					result.put("errMsg", R.getResource("po.conf.group.err.imp.isShop.isUnDefined"));
					return JSON;
				}
			} else {
				// Mã NPP không được để trống
				result.put("errMsg", R.getResource("imp.tuyen.clmn.error.maNPP.null"));
				return JSON;
			}
			
			/*** cai nay tim kiem tren popup*/
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shopCode = shopCode.trim();
				filter.setShopCode(shopCode);
			}
			if (!StringUtil.isNullOrEmpty(shopName)) {
				shopName = shopName.trim();
				filter.setShopName(shopName);
			}
			//ObjectVO<EquipStockVO> temp = equipmentManagerMgr.getListStockByFilter(filter);
			ObjectVO<EquipStockVO> temp = equipRecordMgr.getListStockLostByFilter(filter);

			if (temp != null && temp.getLstObject().size() > 0) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<EquipRepairFormVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	/**
	 * Khai bao cac tham bien
	 * */
	private File excelFile;

	private List<Map<String, Object>> listObjectBeans;
	private List<File> lstFile;
	private List<String> lstFileContentType;
	private List<String> lstFileFileName;
	private List<FileVO> lstFileVo;

	private EquipLostRecord equipLostRecord;
	private Customer customer;

	private Long id;
	private Long equipId;
	private Long shopId;
	private Long stockId;
	private Long customerId;
	private Long eqLostRecMobileId;
	private Long eqLostRecId;
	private Long eqDeliveryRecId;
	private Long eqLostMobiRecId;

	private Boolean flagPagination;

	private Integer isLevel;
	private Integer flagMobile;
	private Integer recordStatus;
	private Integer deliveryStatus;
	private Integer stockType;
	private Integer usageStatus;
	private Integer tradeStatus;
	private Integer statusEquipRecord;
	private Integer processingStatus;
	private Integer tracingResult;
	private Integer tracingPlace;
	private Integer flagRecordStatus;
	private Integer flagUpdate;

	private String code;
	private String phoneStr;
	private String stockCode;
	private String lostDateStr;
	private String shortCode;
	private String customerName;
	private String customerSearch;
	private String address;
	private String shopCode;
	private String shopName;
	private String shopCodeName;
	private String serial;
	private String equipCode;
	private String fromDateStr;
	private String toDateStr;
	private String arrShopTxt;
	private String arrEquipLostRec;
	private String arrEquip;
	private String arrPriceActually;
	private String arrEquipLostRecordTxt;
	private String lastArisingSalesDateStr;
	private String conclusion;
	private String recommendedTreatment;
	private String representor;
	private String excelFileContentType;
	private String sysDateStr;
	private String equipAttachFileStr;
	private String createDate;
	private String note;
	//Begin vuongmq 04/05/2015 khai bao
	/** so seri */
	private String seriNumber;

	/** loai thiet bi */
	private Long categoryId;

	/** nhom thiet bi */
	private Long groupId;

	/** nha cung cap thiet bi */
	private Long providerId;

	/** nam san xuat thiet bi */
	private Integer yearManufacture;
	
	/** ds shopId */
	private String strListShopId;
	
	private Shop shop; // tra ve shop khi chi sua hoac them moi bao mat
	
	private Integer flagLostRecord; // co bao mat NPP hay Customer
	// End vuongmq 04/05/2015 khai bao
	
	private Integer flagError;
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */

	public File getExcelFile() {
		return excelFile;
	}

	public String getEquipAttachFileStr() {
		return equipAttachFileStr;
	}

	public void setEquipAttachFileStr(String equipAttachFileStr) {
		this.equipAttachFileStr = equipAttachFileStr;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public List<String> getLstFileContentType() {
		return lstFileContentType;
	}

	public void setLstFileContentType(List<String> lstFileContentType) {
		this.lstFileContentType = lstFileContentType;
	}

	public List<String> getLstFileFileName() {
		return lstFileFileName;
	}

	public void setLstFileFileName(List<String> lstFileFileName) {
		this.lstFileFileName = lstFileFileName;
	}

	public List<File> getLstFile() {
		return lstFile;
	}

	public void setLstFile(List<File> lstFile) {
		this.lstFile = lstFile;
	}

	public String getSysDateStr() {
		return sysDateStr;
	}

	public void setSysDateStr(String sysDateStr) {
		this.sysDateStr = sysDateStr;
	}

	public String getPhoneStr() {
		return phoneStr;
	}

	public void setPhoneStr(String phoneStr) {
		this.phoneStr = phoneStr;
	}

	public List<Map<String, Object>> getListObjectBeans() {
		return listObjectBeans;
	}

	public void setListObjectBeans(List<Map<String, Object>> listObjectBeans) {
		this.listObjectBeans = listObjectBeans;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	private List<Long> listLostRecordMobile;

	public Long getEqDeliveryRecId() {
		return eqDeliveryRecId;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Integer getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(Integer usageStatus) {
		this.usageStatus = usageStatus;
	}

	public Long getEqLostMobiRecId() {
		return eqLostMobiRecId;
	}

	public void setEqLostMobiRecId(Long eqLostMobiRecId) {
		this.eqLostMobiRecId = eqLostMobiRecId;
	}

	public Integer getFlagMobile() {
		return flagMobile;
	}

	public void setFlagMobile(Integer flagMobile) {
		this.flagMobile = flagMobile;
	}

	public void setEqDeliveryRecId(Long eqDeliveryRecId) {
		this.eqDeliveryRecId = eqDeliveryRecId;
	}

	public String getRepresentor() {
		return representor;
	}

	public void setRepresentor(String representor) {
		this.representor = representor;
	}

	public Long getId() {
		return id;
	}

	public Integer getIsLevel() {
		return isLevel;
	}

	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getEqLostRecId() {
		return eqLostRecId;
	}

	public void setEqLostRecId(Long eqLostRecId) {
		this.eqLostRecId = eqLostRecId;
	}

	public Long getEqLostRecMobileId() {
		return eqLostRecMobileId;
	}

	public void setEqLostRecMobileId(Long eqLostRecMobileId) {
		this.eqLostRecMobileId = eqLostRecMobileId;
	}

	public String getArrEquip() {
		return arrEquip;
	}

	public void setArrEquip(String arrEquip) {
		this.arrEquip = arrEquip;
	}

	public String getArrPriceActually() {
		return arrPriceActually;
	}

	public void setArrPriceActually(String arrPriceActually) {
		this.arrPriceActually = arrPriceActually;
	}

	public Boolean getFlagPagination() {
		return flagPagination;
	}

	public void setFlagPagination(Boolean flagPagination) {
		this.flagPagination = flagPagination;
	}

	public EquipLostRecord getEquipLostRecord() {
		return equipLostRecord;
	}

	public void setEquipLostRecord(EquipLostRecord equipLostRecord) {
		this.equipLostRecord = equipLostRecord;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public String getArrEquipLostRecordTxt() {
		return arrEquipLostRecordTxt;
	}

	public void setArrEquipLostRecordTxt(String arrEquipLostRecordTxt) {
		this.arrEquipLostRecordTxt = arrEquipLostRecordTxt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArrShopTxt() {
		return arrShopTxt;
	}

	public void setArrShopTxt(String arrShopTxt) {
		this.arrShopTxt = arrShopTxt;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getLostDateStr() {
		return lostDateStr;
	}

	public void setLostDateStr(String lostDateStr) {
		this.lostDateStr = lostDateStr;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopCodeName() {
		return shopCodeName;
	}

	public void setShopCodeName(String shopCodeName) {
		this.shopCodeName = shopCodeName;
	}

	public String getFromDateStr() {
		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}

	public String getLastArisingSalesDateStr() {
		return lastArisingSalesDateStr;
	}

	public void setLastArisingSalesDateStr(String lastArisingSalesDateStr) {
		this.lastArisingSalesDateStr = lastArisingSalesDateStr;
	}

	public Integer getTracingResult() {
		return tracingResult;
	}

	public void setTracingResult(Integer tracingResult) {
		this.tracingResult = tracingResult;
	}

	public Integer getTracingPlace() {
		return tracingPlace;
	}

	public void setTracingPlace(Integer tracingPlace) {
		this.tracingPlace = tracingPlace;
	}

	public String getConclusion() {
		return conclusion;
	}

	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	public String getRecommendedTreatment() {
		return recommendedTreatment;
	}

	public void setRecommendedTreatment(String recommendedTreatment) {
		this.recommendedTreatment = recommendedTreatment;
	}

	public Integer getStatusEquipRecord() {
		return statusEquipRecord;
	}

	public void setStatusEquipRecord(Integer statusEquipRecord) {
		this.statusEquipRecord = statusEquipRecord;
	}

	public Integer getProcessingStatus() {
		return processingStatus;
	}

	public void setProcessingStatus(Integer processingStatus) {
		this.processingStatus = processingStatus;
	}

	public List<Long> getListLostRecordMobile() {
		return listLostRecordMobile;
	}

	public void setListLostRecordMobile(List<Long> listLostRecordMobile) {
		this.listLostRecordMobile = listLostRecordMobile;
	}

	public String getArrEquipLostRec() {
		return arrEquipLostRec;
	}

	public void setArrEquipLostRec(String arrEquipLostRec) {
		this.arrEquipLostRec = arrEquipLostRec;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Integer getFlagLostRecord() {
		return flagLostRecord;
	}

	public void setFlagLostRecord(Integer flagLostRecord) {
		this.flagLostRecord = flagLostRecord;
	}

	public Integer getFlagRecordStatus() {
		return flagRecordStatus;
	}

	public void setFlagRecordStatus(Integer flagRecordStatus) {
		this.flagRecordStatus = flagRecordStatus;
	}

	public Integer getFlagError() {
		return flagError;
	}

	public void setFlagError(Integer flagError) {
		this.flagError = flagError;
	}

	public Integer getFlagUpdate() {
		return flagUpdate;
	}

	public void setFlagUpdate(Integer flagUpdate) {
		this.flagUpdate = flagUpdate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCustomerSearch() {
		return customerSearch;
	}

	public void setCustomerSearch(String customerSearch) {
		this.customerSearch = customerSearch;
	}
	
}
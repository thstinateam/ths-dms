package ths.dms.web.action.equipment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;
import ths.dms.web.utils.report.excel.SXSSFReportHelper;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipStatisticRecord;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.EquipObjectType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentStatisticRecordStatus;
import ths.dms.core.entities.enumtype.ShopDecentralizationSTT;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.core.entities.vo.rpt.DynamicVO;
import ths.core.entities.vo.rpt.RptBCBAOMAT1_5VO;
import ths.core.entities.vo.rpt.RptBCTDTMTHVO;
import ths.core.entities.vo.rpt.RptEqImExVO;
import ths.core.entities.vo.rpt.RptKK1_1VO;
import ths.core.entities.vo.rpt.Rpt_3116_WVKD_03F5SC1_1VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_10_BCTHTDTM_F12_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_3_BCDSTL11_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_4_SC1_1VODetail;
import ths.core.entities.vo.rpt.Rpt_3_1_1_8_BCTDTMSC_F10_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_9_DSKHMT_F11_VO;
import ths.dms.core.memcached.MemcachedUtils;
import ths.dms.core.report.HoReportMgr;
//import ths.dms.core.entities.salemt.vo.rpt.DynamicVO;
//import ths.core.entities.vo.rpt.Rpt_XNT1_1_EquipVO;
//import ths.core.entities.vo.rpt.Rpt_XNT1_2_EquipVO;
//import ths.dms.core.report.EquipmentReportMgr;
//import ths.core.entities.vo.rpt.RptBCBAOMAT1_5VO;
//import ths.core.entities.vo.rpt.RptBCTDTMTHVO;
//import ths.core.entities.vo.rpt.RptKK1_2;
//import ths.core.entities.vo.rpt.RptKK1_3;
//import ths.core.entities.vo.rpt.RptPSDS1_1Sheet1VO;
//import ths.core.entities.vo.rpt.RptPSDS1_1Sheet2VO;
//import ths.core.entities.vo.rpt.Rpt_3116_WVKD_03F5SC1_1VO;
//import ths.core.entities.vo.rpt.Rpt_3_1_1_10_BCTHTDTM_F12_VO;
//import ths.core.entities.vo.rpt.Rpt_3_1_1_3_BCDSTL11_VO;

/**
 * Action bao cao danh muc cho HO
 * 
 * @author lacnv1
 * @since Mar 19, 2014
 */
public class EquipmentReportAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
//	private static final int ALL_INT = -2;
//	private static final String QUANTITYCOMPANY = "COMPANYQ";
//	private static final String QUANTITYOVERSIGHT = "GSNPPQ";
//	private static final String DEVIANT = "CHENHLECH";
//	private final String DELIMITER = ",";

	private Long id;
	private Long shopId;
	private Long equipPeriodId;
	private Long periodId;
	private Long statisticRecord;

	private Integer year;
	private Integer year1;
	private Integer year2;
	private Integer fromPeriodId;
	private Integer toPeriodId;
	private Integer typeGG;
	
	private String period;
	private String shopCode;
	private String NPP;
	private String fromDate;
	private String toDate;
	private String lstShopId;
	private String lstEquipId;
	private String strStatus;
	private String periodName;
	private String stockMultil;
	private String groupMultil;
	private String categoryIdMultil;
	private String reportDate;
	private String strListShopId;
	private String lstCheck;
	private String staffCode;
	private String statisticCode;
	private String yesterdayStr;
	
	private int startNpp;
	
	/** The end npp. */
	private int endNpp;

	/** The start vung. */
	private int startVung;

	/** The end vung. */
	private int endVung;

	/** The start mien. */
	private int startMien;

	/** The end mien. */
	private int endMien;

	/** The count npp. */
	private int countNpp;

	/** The count vung. */
	private int countVung;

	/** The count mien. */
	private int countMien;

	/** The count other. */
	private int countOther;

	/** The start other. */
	private int startOther;

	/** The end other. */
	private int endOther;

	/** The start4. */
	private int start4;

	/** The start5. */
	private int start5;

	/** The start6. */
	private int start6;

	/** The start7. */
	private int start7;

	/** The start8. */
	private int start8;

	/** The end4. */
	private int end4;

	/** The end5. */
	private int end5;

	/** The end6. */
	private int end6;

	/** The end7. */
	private int end7;

	/** The end8. */
	private int end8;

	/** The count4. */
	private int count4;

	/** The count5. */
	private int count5;

	/** The count6. */
	private int count6;

	/** The count7. */
	private int count7;

	/** The count8. */
	private int count8;

	/** List Work_State */
	private List<ApParamEquip> lstReasonEviction;
	private List<EquipmentVO> listEquipCategory;
	private List<EquipmentVO> listVO;
	private HoReportMgr hoReportMgr;
	private EquipRecordMgr equipRecordMgr;

	List<EquipPeriod> listEquipPeriod;
	private HashMap<String, Object> parametersReport;


	@Override
	public void prepare() throws Exception {
		super.prepare();
		hoReportMgr = (HoReportMgr) context.getBean("hoReportMgr");
		equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
		startNpp = -1;
		startVung = -1;
		startMien = -1;
		endNpp = -1;
		endVung = -1;
		endMien = -1;
		startOther = -1;
		endOther = -1;
		start4 = -1;
		start5 = -1;
		start6 = -1;
		start7 = -1;
		start8 = -1;
		end4 = -1;
		end5 = -1;
		end6 = -1;
		end7 = -1;
		end8 = -1;
		count4 = -1;
		count5 = -1;
		count6 = -1;
		count7 = -1;
		count8 = -1;
	}

	@Override
	public String execute() throws Exception {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shopId = currentUser.getShopRoot().getShopId();
				shopCode = currentUser.getShopRoot().getShopCode();
				if (ShopDecentralizationSTT.NPP.getValue().equals(currentUser.getShopRoot().getIsLevel())) {
					NPP = "NPP";
				} else {
					NPP = "NOTNPP";
				}
			}
			Date comnSys = commonMgr.getSysDate();
			Date yesterday = commonMgr.getYesterDay();
			if (yesterday != null) {
				yesterdayStr = DateUtil.convertDateByString(yesterday, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			reportDate = DateUtil.convertDateByString(comnSys, DateUtil.DATE_FORMAT_DDMMYYYY);
			year = DateUtil.getYear(comnSys);
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			if (year != null) {
				filter.setYearManufacture(year);
			}
			listEquipCategory = equipmentManagerMgr.getListEquipmentPeriod(filter);
			Cycle cycleSys = cycleMgr.getCycleByCurrentSysdate();
			Date dateSys = commonMgr.getSysDate();
			//Lay mac dinh cho tu ngay dn ngay
			if (cycleSys != null && cycleSys.getBeginDate() != null) {
				fromDate = DateUtil.toDateString(cycleSys.getBeginDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				fromDate = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			toDate = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.execute"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	/**
	 * Bao cao Xuat nhap ton thiet bi
	 * @author ThangNV31
	 * @return String
	 * @since 2015-03-15
	 */
	public String exportXnttb() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				List<RptEqImExVO> lstData = hoReportMgr.exportEQImEx(fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					beans.put("lstData", lstData);
					beans.put("fromDate", fromDate);
					beans.put("toDate", toDate);
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.RPT_EQ_IM_EX_TEMPLATE;
					String fileName = ConstantManager.RPT_EQ_IM_EX_FILE_NAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getTemplatePathEquipmentReport();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
	
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
	
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.EquipmentReportAction.exportXnttb()", createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	

	public String getReportNameFormat(String name, String fDate, String tDate, String fileExtension) {
//		String nameRpt = "Viettel_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_";
		String nameRpt = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_";
		if (fDate.isEmpty() && tDate.isEmpty()) {
			nameRpt = name + nameRpt;
		} else {
			Date fD = DateUtil.parse(fDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tD = DateUtil.parse(tDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			fDate = DateUtil.toDateString(fD, DateUtil.DATE_FORMAT_CSV);
			tDate = DateUtil.toDateString(tD, DateUtil.DATE_FORMAT_CSV);
			nameRpt = name + "_" + fDate + "-" + tDate + nameRpt;
		}
		Random rand = new Random();
		Integer n = rand.nextInt(100) + 1;
		return nameRpt + "_" + n.toString() + fileExtension;
	}

//	/**
//	 * Xuat ten sheet cho bao cao
//	 * 
//	 * @author hoanv25
//	 * @since April 08, 2015
//	 * 
//	 * @description Xuất sheet bao cao
//	 */
//	public String getReportFormat(String name, String fDate, String tDate, String fileExtension) {
//		String nameRpt = "";
//		if (fDate.isEmpty() && tDate.isEmpty()) {
//			nameRpt = nameRpt + name;
//		} else {
//			if (tDate == null || tDate != "") {
//				String tDStr = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_CSV);
//				fDate = DateUtil.toDateString(DateUtil.parse(fDate, DateUtil.DATE_FORMAT_STR), DateUtil.DATE_DD_MM_RP);
//				nameRpt = nameRpt + name + "_" + fDate + "_" + tDStr;
//			} else {
//				Date tD = DateUtil.parse(tDate, DateUtil.DATE_FORMAT_STR);
//				fDate = DateUtil.toDateString(DateUtil.parse(fDate, DateUtil.DATE_FORMAT_STR), DateUtil.DATE_DD_MM_RP);
//				tDate = DateUtil.toDateString(tD, DateUtil.DATE_FORMAT_CSV);
//				nameRpt = nameRpt + name + "_" + fDate + "_" + tDate;
//			}
//		}
//		Random rand = new Random();
//		Integer n = rand.nextInt(100) + 1;
//		return nameRpt + "_" + n.toString() + fileExtension;
//	}
//
//	/**
//	 * Search danh sach ky thiet bi
//	 * 
//	 * @return the string
//	 * @author hoanv25
//	 * @since May 05,2014
//	 * 
//	 */
//	public String getListPeriod() {
//		result.put("rows", new ArrayList<EquipmentVO>());
//		result.put("total", 0);
//		try {
//			if (currentUser == null) {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
//				return JSON;
//			}
//			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
//			if (year != null) {
//				filter.setYearManufacture(year);
//			}
//			ObjectVO<EquipmentVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentPeriodChangeYear(filter);
//			if (equipmentDeliverys.getLstObject() != null) {
//				result.put("rows", equipmentDeliverys.getLstObject());
//			}
//			//Current period
//			EquipPeriod equipPeriodCurrent = equipmentManagerMgr.getEquipPeriodCurrent();
//			result.put("equipPeriodCurrent", equipPeriodCurrent );
//
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//		}
//		return JSON;
//	}
//	
//	/**
//	 * Search danh sach kiem ke
//	 * 
//	 * @return the string
//	 * @author hoanv25
//	 * @since May 20,2014
//	 * 
//	 */
//	public String getStatistic() {
//		result.put("rows", new ArrayList<EquipmentVO>());
//		result.put("total", 0);
//		try {
//			if (currentUser == null) {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
//				return JSON;
//			}
//			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
//			if (periodId != null) {
//				filter.setPeriodId(periodId);
//			}
//			if (currentUser != null && currentUser.getShopRoot() != null) {
//				filter.setShopRoot(currentUser.getShopRoot().getShopId());
//			}
//			ObjectVO<EquipmentVO> listEquipPeriod = equipmentManagerMgr.getListEquipmentStatisticPeriod(filter);
//			if (listEquipPeriod.getLstObject() != null) {
//				result.put("rows", listEquipPeriod.getLstObject());
//			}
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//		}
//		return JSON;
//	}

	/**
	 * [TL1.1] Danh sach thiet bi thanh ly
	 * 
	 * @author tamvnm
	 * @since 18/05/2015
	 * @return String
	 * @throws IOException
	 */
	public String exportBCTL11() throws IOException {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				Shop shop = null;
				if (shopId != null) {
					shop = shopMgr.getShopById(shopId);
				}
				if (shop == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("catalog.unit.tree")));
					return JSON;
				}
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
					return JSON;
				}
				if (StringUtil.isNullOrEmpty(fromDate)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.9.from.date"));
					return JSON;
				} 
				if (StringUtil.isNullOrEmpty(toDate)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.9.to.date"));
					return JSON;
				}
				String msg = "";
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportMedium(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				List<Rpt_3_1_1_3_BCDSTL11_VO> lstData = hoReportMgr.getRptBCDSTL11(shopId, lstEquipId, categoryIdMultil, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
				if (lstData != null && lstData.size() > 0) {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("data", lstData);
					params.put("fromDate", fromDate);
					params.put("toDate", toDate);
					params.put("currentDate", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					params.put("shopReportText", shop.getShopCode() + " - " + shop.getShopName());
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathEquipment(); //Configuration.getStoreRealPath();
					String templateFileName = folder + ConstantManager.TEMPLATE_BCDSTMTDTK;
					templateFileName = templateFileName.replace('/', File.separatorChar);
					String outputName = ConstantManager.EXPORT_BCDSTDTMTK + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put("hasData", true);
					result.put(REPORT_PATH, outputPath);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportBCTL11"), createLogErrorStandard(startDate));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
//	/**
//	 * Xuat excel danh sach khach hang muon tu
//	 * @author tamvnm
//	 * @version 1.0
//	 * @param lstData, report date
//	 * @return url file excel
//	 * @exception Exception
//	 * @since 20/05/2015
//	 */
//	private String createExcelWVKD03F11(List<Rpt_3_1_1_9_DSKHMT_F11_VO> lstData, String reportDate) {
//		String outputPath  = "";
//		SXSSFWorkbook workbook = null;
//		
//		try {
//			List <EquipCategory> lstCategory = equipmentManagerMgr.getListCategory();
//			int dynamicSize  = 0;
//			if (lstCategory == null) {
//				return "";
//			} else {
//				dynamicSize = lstCategory.size();
//			}
//			
//			/** workbook */
//			workbook = new SXSSFWorkbook(200);
//			
//			
//			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
//	
//			/** sheet */
//			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(R.getResource("equip.report.f11.sheet.name"));
//			sheet.setDisplayGridlines(true);
//			SXSSFReportHelper.setColumnWidths(sheet, 0, 
//					(short) 15, (short) 15, (short) 15, (short) 15, (short) 15, 
//					(short) 20, (short) 15, (short) 20, (short) 20, (short) 30,
//					(short) 15, (short) 15, (short) 15, (short) 15, (short) 15,
//					(short) 15, (short) 15, (short) 15, (short) 15, (short) 15,
//					(short) 15, (short) 15, (short) 15, (short) 15, (short) 15,
//					(short) 15, (short) 15, (short) 15, (short) 15, (short) 15,
//					(short) 15, (short) 15, (short) 15, (short) 15, (short) 15,
//					(short) 15, (short) 15, (short) 15, (short) 15, (short) 15,
//					(short) 15, (short) 15, (short) 15, (short) 15, (short) 15,
//					(short) 50
//
//					);
//			
//			int colIdx = 5;
//			int rowIdx = 0;
//			// titile
//			SXSSFReportHelper.addCell(sheet, colIdx, rowIdx, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.header") +  " " + reportDate, style.get(ExcelPOIProcessUtils.TIMES_BOLD_20));
//			// header
//			colIdx = 10;
//			rowIdx = 4;
//			//thong tin khach hang
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx + 7, rowIdx, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.group1"), style.get(ExcelPOIProcessUtils.MENU_BLUE));
//			
//			//thong tin tai san
//			colIdx = 18;
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx + 11 + dynamicSize - 1, rowIdx, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.group2"), style.get(ExcelPOIProcessUtils.MENU_PINK));
//			
//			//table header
//			colIdx = 0;
//			rowIdx = 5;
//			
//			// textbox
//			XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, 26, 0, 29, 3);
//			XSSFTextBox textbox = ((XSSFDrawing) sheet.createDrawingPatriarch()).createTextbox(anchor);
//			textbox.setText(R.getResource("equip.report.textbox.1") + "\n" + R.getResource("equip.report.textbox.2") + "\n" + R.getResource("equip.report.textbox.3"));
//			textbox.setLineWidth(1);
//			textbox.setLineStyle(0);
//			textbox.setLineStyleColor(0, 0, 0);
//			textbox.setFillColor(255, 255, 255);
//			
//			//ngay cap nhat, stt hop dong, so hd, mien, kenh
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu3"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu4"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu5"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			
//			//tbhv, ma npp, ten npp, giam sat, ma tai san/seri
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu6"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu7"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu8"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu9"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu10"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			
//			//ma kh, ten kh, dt, so, duong
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu11"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu12"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu13"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu14"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu15"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			
//			//phuong, quan, tp
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu16"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu17"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu18"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			
//			for (int i = 0, isize = lstCategory.size(); i < isize; i++) {
//				SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, lstCategory.get(i).getName(), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			}
//			//so luong, loai tu, hieu tu, dung tich, nam cap lan dau
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu19"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu20"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu21"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu22"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu23"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			
//			//Nam ky HD, tai diem ban, tai kho npp
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu24"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu25"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu26"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			
//			//doi chieu, tinh trang ho so
//			SXSSFReportHelper.addMergeCells(sheet, colIdx, rowIdx, colIdx + 2, rowIdx, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu27"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addMergeCells(sheet, colIdx + 3, rowIdx, colIdx + 8, rowIdx, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu31"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_ALL_THIN_WRAP2));
//			
//			//so luong thu te, chenh lech, giai trinh chenh lech
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu28"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu29"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu30"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM_WRAPTEXT));
//			
//			//Co HD tai Vp, ko co HD, Da gui HD, ngay gui HD, ngay nhan HD tu GS
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu32"), style.get(ExcelPOIProcessUtils.HEADER_YELLOW_ALl_THIN_WRAPTEXT_RED));
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu33"), style.get(ExcelPOIProcessUtils.HEADER_YELLOW_ALl_THIN_WRAPTEXT_RED));
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu34"), style.get(ExcelPOIProcessUtils.HEADER_YELLOW_ALl_THIN_WRAPTEXT_RED));
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu35"), style.get(ExcelPOIProcessUtils.HEADER_YELLOW_ALl_THIN_WRAPTEXT_RED));
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu36"), style.get(ExcelPOIProcessUtils.HEADER_YELLOW_ALl_THIN_WRAPTEXT_RED));
//			
//			//ghi chu
//			SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.f11.menu37"), style.get(ExcelPOIProcessUtils.HEADER_YELLOW_ALl_THIN_WRAPTEXT_RED));
//		
//			
//			/**ghi phan body */
//			rowIdx = 7;
////			colIdx = 0;
//			int stt = 0;
//			for (int i = 0, isize = lstData.size(); i < isize; i++) {
//				Rpt_3_1_1_9_DSKHMT_F11_VO row = lstData.get(i);
//				String status = "";
//				colIdx = 0;
//				Boolean isTotal = false;
//				//ngay cap nhat, stt, so HD, mien, kenh
//				if (row.getId() == null && row.getChangeDate() == null && row.getFlag().equals(0)) {
//					status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.total1");
//					isTotal = true;
//				} else if (row.getId() == null && row.getChangeDate() == null && row.getFlag().equals(1)) {
//					status = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.total2");
//					isTotal = true;
//				} else {
//					status = row.getChangeDate();
//					stt++;
//				}
//				
//				if (!isTotal) {
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, status, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, Double.parseDouble(stt + ""), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getContractNumber(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getMien(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getKenh(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					
//					//tbhv, ma npp, ten npp, giam sat, ma tb-serial
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getTbhv(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getShopCode(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getShopName(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getGsnpp(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getEquipCode(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					
//					//Ma Kh, ten KH, dt, so, duong, phuong, quan, tp
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getCustomerCode(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getCustomerName(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getPhone(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getHouseNumber(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getStreet(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getWard(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getDistrict(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getCity(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					
//					for (int j = 0, jsize = lstCategory.size(); j < jsize; j++) {
//						if (lstCategory.get(j).getCode().equals(row.getCategoryCode())) {
//							SXSSFReportHelper.addNumberCell(sheet, colIdx, rowIdx, Double.parseDouble( 1 + ""), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						}
//						colIdx++;
//					}
//					
//					//tc so luong, loai tu, hieu tu, dung tich, nam cap tu lan dau
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, Double.parseDouble( 1 + ""), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getCategoryName(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getBrandName(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getCapacity(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getFirstYear() == null ? null : Double.parseDouble(row.getFirstYear() + "")
//							, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					
//					//Nam ky HD muon tu, tai diem ban, tai kho npp, so luong thuc te, chenh lech, giai trinh chenh lech
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getYearContract() == null ? null : row.getYearContract()
//							, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getKhoKH() == null ? null : Double.parseDouble(row.getKhoKH() + "")
//							, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getKho() == null ? null : Double.parseDouble(row.getKho() + "")
//							, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getQuantityActually() == null ? null : Double.parseDouble(row.getQuantityActually() + "")
//							, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					if (row.getQuantityActually() == null && row.getDifference() == 0) {
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx,  null
//								, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					} else {
//						SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getDifference() == null ? null : Double.parseDouble(row.getDifference() + "")
//								, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					}
//					
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getBecause(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					
//					//co tai vp, ko co HD, da gui GS, ngay gui GS, ngay nhan HD
//					if (row.getDeliveryStatus() != null && row.getDeliveryStatus().equals(DeliveryType.NOTSEND.getValue())) {
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "X", style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					} else if (row.getDeliveryStatus() != null && row.getDeliveryStatus().equals(DeliveryType.SENT.getValue())) {
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "X", style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getSentDate(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					} else if (row.getDeliveryStatus() != null && row.getDeliveryStatus().equals(DeliveryType.RECEIVED.getValue())) {
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "X", style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, row.getReceivedDate(), style.get(ExcelPOIProcessUtils.TIMES_NORMAL_12));
//					}
//					rowIdx++;
//				} else {
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, status, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_BOLD_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					
//					//tbhv, ma npp, ten npp, giam sat, ma tb-serial
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					
//					//Ma Kh, ten KH, dt, so, duong, phuong, quan, tp
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					
//					for (int j = 0, jsize = lstCategory.size(); j < jsize; j++) {
//						SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					}
//					
//					//tc so luong, loai tu, hieu tu, dung tich, nam cap tu lan dau
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					
//					//Nam ky HD muon tu, tai diem ban, tai kho npp, so luong thuc te, chenh lech, giai trinh chenh lech
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getKho() == null ? null : Double.parseDouble(row.getKho() + "")
//							, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getKhoKH() == null ? null : Double.parseDouble(row.getKhoKH() + "")
//							, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getQuantityActually() == null ? null : Double.parseDouble(row.getQuantityActually() + "")
//							, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, row.getDifference() == null ? null : Double.parseDouble(row.getDifference() + "")
//							, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					
//					//co tai vp, ko co HD, da gui GS, ngay gui GS, ngay nhan HD
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, null, style.get(ExcelPOIProcessUtils.YELLOW_TIMES_NORMAL_12));
//					rowIdx++;
//				}
//			}
//			//duyet, nguoi lap bao cao
//			colIdx = 5;
//			SXSSFReportHelper.addCell(sheet, colIdx, rowIdx, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.duyet"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_13));
//			colIdx = 14;
//			SXSSFReportHelper.addCell(sheet, colIdx, rowIdx, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "equip.report.nguoi.lap"), style.get(ExcelPOIProcessUtils.TIMES_BOLD_13));
//
//			/** Ghi file va tra ve ket qua */
//			outputPath = SXSSFReportHelper.exportXLSX(workbook, null, "equip.report.f11.file.name");
//			result.put(ERROR, false);
//			result.put("hasData", true);
//			result.put(REPORT_PATH, outputPath);
//		
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.createExcelWVKD03F11()"), createLogErrorStandard(actionStartTime));
//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			result.put(ERROR, true);
//		}
//		
//		return outputPath;
//
//	}
	
	/**
	 * 3.1.1.9	[WV-KD-03-F11] Danh sach khach hang muon tu - ham xuat file excel bao cao F11
	 * @author tamvnm
	 * @version 1.0
	 * @return String
	 * @exception Exception
	 * @since 20/05/2015
	 */
	public String exportWVKD03F11() {
		actionStartTime = DateUtil.now();
		try {
			result.put(ERROR, true);
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null || currentUser.getShopRoot() == null) {
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(lstShopId)) {
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.9.shop"));
				return JSON;
			}
			if (typeGG == null || (!ActiveType.STOPPED.getValue().equals(typeGG) && !ActiveType.RUNNING.getValue().equals(typeGG))) {
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.9.info.export.err"));
				return JSON;
			}
			Date fDate = null;
			Date tDate = null;
			if (ActiveType.RUNNING.getValue().equals(typeGG)) {
				if (StringUtil.isNullOrEmpty(fromDate)) {
					result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.9.from.date"));
					return JSON;
				} 
				if (StringUtil.isNullOrEmpty(toDate)) {
					result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.9.to.date"));
					return JSON;
				}
				fDate = DateUtil.toDate(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				tDate = DateUtil.toDate(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				String errDistinctDate = super.checkMaxNumberDayReportLong(fDate, tDate);
				if (!StringUtil.isNullOrEmpty(errDistinctDate)) {
					result.put(ERR_MSG, errDistinctDate);
					return JSON;
				}
			}
			Long staffRootId = currentUser.getStaffRoot().getStaffId();
			Long roleId = currentUser.getRoleToken().getRoleId();
			Long shopRootId = currentUser.getShopRoot().getShopId();
			List<Rpt_3_1_1_9_DSKHMT_F11_VO> lstData = hoReportMgr.getRptWVKD03F11_DSKHMTB(staffRootId, roleId, shopRootId, lstShopId, typeGG, fDate, tDate);
			if (lstData != null && lstData.size() > 0) {
				String outputPath = "";
				SXSSFWorkbook workbook = null;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("equip.report.f11.sheet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				sheetData.setDisplayGridlines(false);
				sheetData.setDefaultColumnWidth(23);
				ExcelPOIProcessUtils.setRowHeight(sheetData, 0, 25);
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 120, 200, 120, 200, 250, 140, 100, 200, 140, 140);
				//outputPath = this.createExcelWVKD03F11(lstData, reportDate);
				ExcelPOIProcessUtils.addCell(sheetData, 7, 0, R.getResource("equip.report.f11.header"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
				//header
				if (ActiveType.STOPPED.getValue().equals(typeGG)) {
					Date sysDate = commonMgr.getSysDate();
//					ExcelPOIProcessUtils.addCell(sheetData, 5, 1, R.getResource("equip.report.f11.tinh.den.ngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					ExcelPOIProcessUtils.addCell(sheetData, 6, 1, DateUtil.toDateString(sysDate, DateUtil.DATE_FORMAT_DDMMYYYY), style.get(ExcelPOIProcessUtils.NORMAL));
//					ExcelPOIProcessUtils.addCell(sheetData, 8, 1, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					ExcelPOIProcessUtils.addCell(sheetData, 9, 1, DateUtil.toDateString(sysDate, DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 1, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 7, 1, DateUtil.toDateString(sysDate, DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
				} else {
					ExcelPOIProcessUtils.addCell(sheetData, 3, 1, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 1, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 1, R.getResource("common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 7, 1, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 9, 1, R.getResource("common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 10, 1, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
				}
				int rowStartHeader = 3;
				ExcelPOIProcessUtils.setRowHeight(sheetData, rowStartHeader, 25);
				for (int i = 0; i < 21; i++) {
					ExcelPOIProcessUtils.addCell(sheetData, i, rowStartHeader, R.getResource("equip.report.f11.menu" + i), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				}
				int d = 4;
				for (int i = 0, size = lstData.size(); i < size; i++) {
					Rpt_3_1_1_9_DSKHMT_F11_VO item = lstData.get(i);
					ExcelPOIProcessUtils.addCell(sheetData, 0, d, (i + 1), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, 1, d, item.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 2, d, item.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 3, d, item.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 4, d, item.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, d, item.getShortCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 6, d, item.getCustomerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 7, d, item.getAddress(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 8, d, item.getContractNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 9, d, item.getDateContract(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, 10, d, item.getMaGSNPP() + " - " + item.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 11, d, item.getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 12, d, item.getSerialNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 13, d, item.getEquipGroupName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 14, d, item.getCategoryName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 15, d, item.getBrandName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 16, d, item.getCapacity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 17, d, item.getHealthStatusStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 18, d, item.getDateFirstUse(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, 19, d, item.getEndDateContract(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, 20, d, item.getReasonEndContract(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					d++;
				}
				outputPath = SXSSFReportHelper.exportXLSX(workbook, null, "equip.report.f11.file.name");
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, false);
				result.put("hasData", false);
			}
			lstData = null;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportWVKD03F11()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Bao cao Danh sách thiết bị thu hồi
	 * 
	 * @author dungnt19
	 * @return file XLS
	 * @throws IOException
	 */
	public String exportBCTDTMTH() throws IOException {
		actionStartTime = DateUtil.now();
		SXSSFWorkbook workbook = null;
		parametersReport = new HashMap<String, Object>();
		parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
		try {
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			if (!StringUtil.isNullOrEmpty(lstShopId)) {
				shopId = Long.valueOf(lstShopId);
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			}
			Shop shop = shopMgr.getShopById(shopId);
			if (shop == null) {
				result.put(ERR_MSG, R.getResource("catalog.unit.tree.not.exist"));
				return JSON;
			}
			shopName = shop.getShopCode() + " - " + shop.getShopName();
			
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (fDate == null) {
				result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.fromdate")));
				return JSON;
			}
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (tDate == null) {
				result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.todate")));
				return JSON;
			}
			if (DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
				result.put(ERR_MSG, R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate")));
				return JSON;
			}
			String msgDate = checkMaxNumberDayReportLong(fDate, tDate);
			if (!StringUtil.isNullOrEmpty(msgDate)) {
				result.put(ERR_MSG, msgDate);
				return JSON;
			}
			
			Long staffRootId = currentUser.getStaffRoot().getStaffId();
			Long roleId = currentUser.getRoleToken().getRoleId();
			Long shopRootId = currentUser.getShopRoot().getShopId();
			List<RptBCTDTMTHVO> lstData = hoReportMgr.getRptBCDSTDTMTH(staffRootId, roleId, shopRootId, lstShopId, lstEquipId, categoryIdMultil, fDate, tDate);
			if (lstData != null && lstData.size() > 0) {
				String outputPath = "";
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(200);
				workbook.setCompressTempFiles(true);
				//Tao shett
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet("NKMT");
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 25));
				sheetData.setDefaultColumnWidth(13);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 
																50, 90, 90, 90, 120, 
																90, 90, 90, 150, 90, 
																90, 150, 150, 150, 90, 
																90, 90, 90, 90, 90, 
																90, 90, 150);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 25, 15, 20, 15, 25, 25);
				sheetData.setDisplayGridlines(false);

//					ExcelPOIProcessUtils.addCell(sheetData, 0, 0, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.sheet.label"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					ExcelPOIProcessUtils.addCell(sheetData, 0, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.sheet.label2"), style.get(ExcelPOIProcessUtils.BOLD));
				int rowId = 0;
				ExcelPOIProcessUtils.addCell(sheetData, 8, rowId,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.sheet.label3"),style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
				
				rowId += 2;
				ExcelPOIProcessUtils.addCell(sheetData, 3, rowId, R.getResource("common.shop.name.lable"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, rowId, shopName, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
				
				rowId++;
				ExcelPOIProcessUtils.addCell(sheetData, 3, rowId, R.getResource("common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, rowId, fromDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, rowId, R.getResource("common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 6, rowId, toDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, rowId, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 8, rowId, DateUtil.toDateString(actionStartTime, DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));

				//header
				rowId += 2;
				int colId = 0;
				
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, rowId, 0, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
//				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, rowId, 2, rowId, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
//				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, rowId + 1, 2, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu2.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				

				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu4.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu4"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu5"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu6"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu2.2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu3.2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu7"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu8"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu15"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu15.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu11"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu11.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu12"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu10"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu13"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu14"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu16"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				//ExcelPOIProcessUtils.addCell(sheetData, 18, rowId, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu18"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				
				lstReasonEviction = apParamEquipMgr.getListApParamEquipByType(ApParamEquipType.EQUIP_EVICTION_REASON_TYPE, ActiveType.RUNNING);
				int lstReasonEvictionSize = 0;
				if(lstReasonEviction != null) {
					lstReasonEvictionSize = lstReasonEviction.size();
				}
				
				if (lstReasonEviction != null && lstReasonEvictionSize > 0) {
					for (int i = 0; i < lstReasonEvictionSize; i++) {
						ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + i + 1, rowId + 1, colId + i + 1, rowId + 1, lstReasonEviction.get(i).getValue(), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
//						++colId;
					}
				}
				
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, colId + lstReasonEvictionSize, rowId, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu17"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + lstReasonEvictionSize + 1, rowId, colId + lstReasonEvictionSize + 1, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu18"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				
				//Xu l?� do du lieu Detail
				rowId += 2;
				int x = 0;
				for (int i = 0; i < lstData.size(); i++) {
					int c = -1;
					RptBCTDTMTHVO record = lstData.get(i);
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getNumberCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getDateEviction(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getCustomerCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getCustomerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getSerialNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getCategoryName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getGroupName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getBrandName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getTotal(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getCapacity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getManufacturingYear(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, record.getFirstDateUse(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++c, rowId, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					for (int j = 0; j < lstReasonEviction.size(); j++) {
						if (record.getReason() != null && record.getReason().equals(lstReasonEviction.get(j).getApParamEquipCode())) {
							ExcelPOIProcessUtils.addCell(sheetData, c + j, rowId, "X", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, c + j, rowId, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						}
						x = c + j;
					}
					ExcelPOIProcessUtils.addCell(sheetData, x + 1, rowId, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					rowId++;
				}
//				r += 5;
//				ExcelPOIProcessUtils.addCell(sheetData, 2, r, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu19"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//				ExcelPOIProcessUtils.addCell(sheetData, 11, r++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu20"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

				String fileName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu21");
				fileName = getReportNameFormat(fileName, "", "", FileExtension.XLSX.getValue());
				outputPath = SXSSFReportHelper.exportXLSX(workbook, null, null, fileName);
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());

					result.put(ERROR, false);
				result.put(REPORT_PATH, outputPath);
				result.put("hasData", true);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, false);
				result.put("hasData", false);
			}
			lstData = null;
		} catch (Exception e) {
//			System.out.print(e.getMessage());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportBCTDTMTH"), createLogErrorStandard(actionStartTime));
			if (workbook != null) {
				workbook.dispose();
			}
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao 3.1.1.8
	 * 
	 * @author hoanv25
	 * @return file XLS
	 * @exception IOException
	 * @since 14/08/2015
	 * @deprecation Thiet bi - Xuat BC F10 update by tamvnm
	 * @modified tamvnm
	 */
	/**
	 * Xu ly exportBCTDTMSC Modified
	 * @author vuongmq
	 * @return String
	 * @throws IOException
	 * @since Apr 7, 2016
	 */
	public String exportBCTDTMSC() throws IOException {
		SXSSFWorkbook workbook = null;
		actionStartTime = DateUtil.now();
		try {
			result.put(ERROR, true);
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(lstShopId)) {
				shopId = Long.valueOf(lstShopId);
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			}
			Shop shop = shopMgr.getShopById(shopId);
			if (shop == null) {
				result.put(ERR_MSG, R.getResource("catalog.unit.tree.not.exist"));
				return JSON;
			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (fDate == null) {
				result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.fromdate")));
				return JSON;
			}
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (tDate == null) {
				result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.todate")));
				return JSON;
			}
			if (DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
				result.put(ERR_MSG, R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate")));
				return JSON;
			}
			String msgDate = checkMaxNumberDayReportLong(fDate, tDate);
			if (!StringUtil.isNullOrEmpty(msgDate)) {
				result.put(ERR_MSG, msgDate);
				return JSON;
			}
			Long staffRootId = currentUser.getStaffRoot().getStaffId();
			Long roleId = currentUser.getRoleToken().getRoleId();
			Long shopRootId = currentUser.getShopRoot().getShopId();
			List<Rpt_3_1_1_8_BCTDTMSC_F10_VO> lstData = hoReportMgr.getRptBCTDTMSC(staffRootId, roleId, shopRootId, lstShopId, groupMultil, categoryIdMultil, fDate, tDate);
			if (lstData != null && lstData.size() > 0) {
				String outputPath = "";
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(200);
				workbook.setCompressTempFiles(true);
				//Tao shett
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.sheet.name"));

				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 25));
				sheetData.setDefaultColumnWidth(13);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 120, 120, 120, 200, 120, 120, 120, 180, 120, 180, 150, 150, 
																200, 200, 200, 120, 100, 160, 250, 120, 120, 120, 
																120, 120, 120, 250, 250, 250, 250, 250, 250);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 15, 15, 15);
				sheetData.setDisplayGridlines(true);

				int iCol = 0;
				int iRow = 0;
				String dateNow = DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR);
				ExcelPOIProcessUtils.addCell(sheetData, iCol + 7, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.label1"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 1, R.getResource("common.shop.name.lable"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 1, R.getResource("common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 1, R.getResource("common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 1, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 1, shop.getShopCode() + " - " + shop.getShopName(), style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 1, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 1, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 1, dateNow, style.get(ExcelPOIProcessUtils.NORMAL));
				iCol = 0;
				iRow = 3;
				
				//header
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.0"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.1.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.4"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.5"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol + 1, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.2.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.3"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.6"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.6.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.7"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.8"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.8.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.8.2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.8.3"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.9"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.10"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.11"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.12"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.14"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.15"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.16"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.17"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.18"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow,  iCol + 2, iRow, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.19"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.19.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.19.2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.19.3"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				
				//header cot dong
				List<DynamicVO> lstDynamic = lstData.get(0).getLstDynamic();
				if (lstDynamic != null && lstDynamic.size() > 0) {
					for (int i = 0, isize = lstDynamic.size(); i < isize; i++) {
						ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.dynamic") + " " + (i + 1), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
				}
				
				//cot cuoi: ghi chu
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, iCol, iRow, iCol++, iRow + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.col.22"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				
				//Xu ly do du lieu Detail
				iCol = 0;
				iRow = 5;
				for (int i = 0; i < lstData.size(); i++) {
					Rpt_3_1_1_8_BCTDTMSC_F10_VO record = lstData.get(i);
					if (record == null) {
						continue;
					}
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getRepairCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getCreateFormDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getStaffCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getStaffName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getShortCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getCustomerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getSerialNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getCategoryName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getGroupName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getBrandName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getCapacity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getManufacturingYear(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getFirstDayInUse(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getItemName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getRepairTotalAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getRepairCount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getRepairedInMonth(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getRepairPayCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getRepairPayDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getRepairPayTotalAmuont(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					
					if (record.getLstDynamic() != null && record.getLstDynamic().size() > 0) {
						for (int j = 0, jsize = record.getLstDynamic().size(); j < jsize; j++) {
							ExcelPOIProcessUtils.addCell(sheetData, iCol++, iRow, record.getLstDynamic().get(j) == null ? null : record.getLstDynamic().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
					}
					
					//ghi chu
					ExcelPOIProcessUtils.addCell(sheetData, iCol, iRow++, record.getNote(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					iCol = 0;
				}
				String fileName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.8.label.filename");
				fileName = fileName + genFileSuffix() + FileExtension.XLSX.getValue();
				outputPath = SXSSFReportHelper.exportXLSX(workbook, null, null, fileName);
				result.put(ERROR, false);
				result.put(REPORT_PATH, outputPath);
				result.put("hasData", true);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, false);
				result.put("hasData", false);
			}
			lstData = null;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportBCTDTMSC"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 *	Bao cao F12 Xuat nhap ton thiet bi tai NPP 
	 * @author trietptm
	 * @return String
	 * @throws IOException
	 * @since Apr 12, 2016
	 */
	public String exportBCF12() throws IOException {
		actionStartTime = DateUtil.now();
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				Shop shop = null;
				if (shopId != null) {
					shop = shopMgr.getShopById(shopId);
				}
				if (shop == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("catalog.unit.tree")));
					return JSON;
				}
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
					return JSON;
				}
				if (StringUtil.isNullOrEmpty(fromDate)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.9.from.date"));
					return JSON;
				} 
				if (StringUtil.isNullOrEmpty(toDate)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.9.to.date"));
					return JSON;
				}
				String msg = "";
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportMedium(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				List<Rpt_3_1_1_10_BCTHTDTM_F12_VO> lstData = hoReportMgr.getRptF12(shopId, lstEquipId, categoryIdMultil, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
				if (lstData != null && lstData.size() > 0) {
					String outputPath = "";
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.10.sheet"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					
					//Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 25));
					sheetData.setDefaultColumnWidth(10);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 40, 80, 80, 80, 120, 120, 120, 120, 100);
					//Size Row
					ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 20, 20, 15, 15);
					sheetData.setDisplayGridlines(false);
					
					int col = 0;
					int row = 0;
					ExcelPOIProcessUtils.addCell(sheetData, col + 10, row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.10.title"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					// info
					row++;
					ExcelPOIProcessUtils.addCell(sheetData, 3, row, R.getResource("common.shop.name.lable"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 4, row, shop.getShopCode() + " - " + shop.getShopName(), style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
					col = col + 3;
					row++;
					ExcelPOIProcessUtils.addCell(sheetData, col++, row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, col++, row, fromDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, col++, row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, col++, row, toDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, col++, row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					String dateNow = DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR);
					ExcelPOIProcessUtils.addCell(sheetData, col++, row, dateNow, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
					//header
					col = 0;
					row += 2;
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, col + 9, row, col + 16, row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.10.header.stock.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, col + 17, row, col + 22, row, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.10.header.stock.customer"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					row++;
					String strHeader = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.10.header");
					String[] listHeader = strHeader.split(",");
					for (int i = 0, n = listHeader.length; i < n; i++) {
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, listHeader[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					//detail
					for (int i = 0, n = lstData.size(); i < n; i++) {
						col = 0;
						row++;
						Rpt_3_1_1_10_BCTHTDTM_F12_VO record = lstData.get(i);
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getRegionCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getAreaCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getCategoryName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getGroupName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getBrandName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getCapacity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getOpenShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getImportShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getTransferInShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getEvictionShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getLostShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getLiquidationShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getTransferOutShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getCloseShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getOpenCus(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getImportCus(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getEvictionCus(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getLostCus(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getLiquidationCus(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, col++, row, record.getCloseCus(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));						
					}
					String fileName = getReportNameFormat(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.10.file.name"), "", "", FileExtension.XLSX.getValue());
					outputPath = SXSSFReportHelper.exportXLSX(workbook, null, null, fileName);
					result.put(ERROR, false);
					result.put("hasData", true);
					result.put(REPORT_PATH, outputPath);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportBCTL11"), createLogErrorStandard(actionStartTime));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}
	
//	/**
//	 * Bao cao 3.1.1.1; Bao cao tong hop tu kiem ke
//	 * 
//	 * @author hoanv25
//	 * @date May 20,2015
//	 * @return file XLS
//	 * @throws IOException
//	 */
//	/*public String exportBCTHKKT() throws IOException {
//		SXSSFWorkbook workbook = null;
//		parametersReport = new HashMap<String, Object>();
//		parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
//		try {
//			*//** begin Bo sung ATTT *//*
//			String reportToken = retrieveReportToken(reportCode);
//			if (StringUtil.isNullOrEmpty(reportToken)) {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
//				return JSON;
//			}
//			*//** end Bo sung ATTT *//*
//			Shop shop = null;
//			if (shopId != null) {
//				shop = shopMgr.getShopById(shopId);
//				if (shop != null) {
//					parametersReport.put("sName", shop.getShopName());
//					if (!StringUtil.isNullOrEmpty(shop.getAddress())) {
//						parametersReport.put("sAddress", shop.getAddress());
//					} else {
//						parametersReport.put("sAddress", "");
//					}
//				} else {
//					//common.not.exist.in.db = {0} hiện kh?�ng c?� trong hệ thống.
//					result.put(ERROR, true);
//					result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("catalog.unit.tree")));
//					return JSON;
//				}
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("catalog.unit.tree")));
//				return JSON;
//			}
//			String text = "";
//			if (period != null) {
//				EquipPeriod equipPeriod = equipmentManagerMgr.getListEquipPeriodById(new Long(period));
//				if (equipPeriod != null) {
//					text = equipPeriod.getName();
//				}
//			}
//			List<RptBCTDTMTHVO> lstData = hoReportMgr.getRptBCTHKKT(shopId, lstShopId, statisticRecord, period);
//			if (lstData != null && lstData.size() > 0) {
//				String outputPath = "";
//				if ("PDF".equals(formatType)) {
//				} else {
//					*//** xuat file excel *//*
//					//Init XSSF workboook
//					workbook = new SXSSFWorkbook(200);
//					workbook.setCompressTempFiles(true);
//					//Tao shett
//					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
//					ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();
//					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("report.equip.3.1.1.5.bao.mat.sheet.label"));
//					//Set Getting Defaul
//					sheetData.setDefaultRowHeight((short) (15 * 25));
//					sheetData.setDefaultColumnWidth(13);
//					//set static Column width
//					mySheet.setColumnsWidth(sheetData, 0, 50, 50, 60, 60, 60, 210, 230, 47, 70, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65);
//					//Size Row
//					mySheet.setRowHeight(sheetData, 5, 25);
//					mySheet.addCell(sheetData, 0, 0, R.getResource("report.equip.sheet.label"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 0, 1, R.getResource("report.equip.sheet.label2"), style.get(ExcelPOIProcessUtils.BOLD));
//					mySheet.addCell(sheetData, 7, 3, R.getResource("report.equip.3.1.1.1.label1") + text, style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 0, 5, R.getResource("report.equip.3.1.1.1.title"), style.get(ExcelPOIProcessUtils.BOLD));
//					mySheet.addCell(sheetData, 0, 6, R.getResource("report.equip.3.1.1.1.title1"), style.get(ExcelPOIProcessUtils.BOLD));
//					listVO = equipmentManagerMgr.getListCategoryCount(statisticRecord);
//					//header
//					mySheet.addCellsAndMerged(sheetData, 0, 7, 0, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu1"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 1, 7, 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label2"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 2, 7, 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.10.label2"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 3, 7, 3, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label3"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 4, 7, 4, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu5"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 5, 7, 5, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu6"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 6, 7, 6, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label4"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 7, 7, 7, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label5"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 8, 7, 8, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label6"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 9, 8, 9, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label8"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					int c = 10;
//					int col = 0;
//					int colgs = 0;
//					int colcl = 0;
//					if (listVO != null && listVO.size() > 0) {
//						for (int i = 0, sz = listVO.size(); i < sz; i++) {
//							mySheet.setColumnsWidth(sheetData, c + i,65);
//							mySheet.addCell(sheetData, c + i, 9, listVO.get(i).getName(), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//							col = c + i;
//						}
//					}
//					mySheet.addCell(sheetData, col + 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label12"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 9, 7, col + 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label7"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 10, 8, col + 1, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label9"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					c = col + 3;
//					if (listVO != null && listVO.size() > 0) {
//						for (int i = 0, sz = listVO.size(); i < sz; i++) {
//							mySheet.setColumnsWidth(sheetData, c + i,65);
//							mySheet.addCell(sheetData, c + i, 9, listVO.get(i).getName(), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//							colgs = c + i;
//						}
//					}
//					mySheet.addCellsAndMerged(sheetData, col + 2, 8, col + 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label8"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, col + 2, 7, colgs + 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label13"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, col + 3, 8, colgs + 1, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label9"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCell(sheetData, colgs + 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label12"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//
//					mySheet.addCellsAndMerged(sheetData, colgs + 2, 8, colgs + 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label15"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, colgs + 3, 8, colgs + 3, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label16"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					c = colgs + 4;
//					if (listVO != null && listVO.size() > 0) {
//						for (int i = 0, sz = listVO.size(); i < sz; i++) {
//							mySheet.setColumnsWidth(sheetData, c + i,65);
//							mySheet.addCell(sheetData, c + i, 9, listVO.get(i).getName(), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//							colcl = c + i;
//						}
//					}
//					mySheet.addCell(sheetData, colcl + 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label12"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, colgs + 2, 7, colcl + 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label14"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, colgs + 4, 8, colcl + 1, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label9"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, colcl + 2, 7, colcl + 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu18"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//
//					// xu ly danh sach detail			
//					int r = 10;
//					int cd = 10;
//					int colum = 0;
//					int colw = 0;
//					int colcheck = 0;
//					int cell = 0;
//					int totalCompanyAll = 0;
//					int totalGSNPPAll = 0;
//					int totalOversightCompany = 0;
//					int totalOversightGSNPP = 0;
//					int totalQuantityCompany = 0;
//					int totalQuantityGSNPP = 0;
//					int differenceShort = 0; //thieu
//					int differenexcess = 0; //thua
//
//					int coltotalGSNPPAll = 0;
//					int coltotalOversightCompany = 0;
//					int coltotalOversightGSNPP = 0;
//					int coltotalQuantityGSNPP = 0;
//					int coltotalCheck = 0;
//					int differenCheck = 0;
//					for (int i = 0, size = lstData.size(); i < size; i++) {
//						RptBCTDTMTHVO record = lstData.get(i);
//						int totalCompany = 0;
//						int totalOversight = 0;
//						int check = 0;
//						int checkOversight = 0;
//						int checkAdd = 0;					
//						 tinh tong kiem ke 
//						if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//								|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//										.get(i - 1).getParentShop() != null)))) {
//							mySheet.addCellsAndMerged(sheetData, 0, r, 8, r, R.getResource("report.equip.3.1.1.1.label20") + lstData.get(i - 1).getParentShop(), style.get(ExcelPOIProcessUtils.BOLD));
//						}
//						 end 
//						if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//								|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//										.get(i - 1).getParentShop() != null)))) {
//						} else {
//							mySheet.addCell(sheetData, 0, r, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//							mySheet.addCell(sheetData, 1, r, record.getParentShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//							mySheet.addCell(sheetData, 2, r, record.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//							mySheet.addCell(sheetData, 3, r, record.getArea(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//							mySheet.addCell(sheetData, 4, r, record.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//							mySheet.addCell(sheetData, 5, r, record.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, 6, r, record.getGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, 7, r, R.getResource("report.equip.3.1.1.1.label21") + record.getStatisticTime(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, 8, r, record.getStatisticDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//						}
//						if (record.getShelvesCompany() != null) {
//							if (record.getQuantityCompany() != null) {
//								totalCompany = record.getShelvesCompany() + record.getQuantityCompany();
//							} else {
//								totalCompany = record.getShelvesCompany();
//							}
//						} else if (record.getQuantityCompany() != null) {
//							totalCompany = record.getQuantityCompany();
//						} else {
//							totalCompany = 0;
//						}
//						totalCompanyAll = totalCompanyAll + totalCompany;
//						if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//								|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//										.get(i - 1).getParentShop() != null)))) {
//							mySheet.addCell(sheetData, 9, r, totalCompanyAll, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							totalCompanyAll = 0;
//						} else {
//							mySheet.addCell(sheetData, 9, r, totalCompany, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//						}
//						if (listVO != null && listVO.size() > 0) {
//							for (int j = 0, sz = listVO.size(); j < sz; j++) {
//								if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//										|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i)
//												.getParentShop() == null && lstData.get(i - 1).getParentShop() != null)))) {
//									 tinh tong theo cac loai thiet bi 
//									for (int i2 = 0, sz2 = lstData.size(); i2 < sz2; i2++) {
//										if (listVO.get(j).getId().equals(lstData.get(i2).getId())) {
//											if (lstData.get(i2).getQuantityCompany() != null) {
//												totalQuantityCompany = totalQuantityCompany + lstData.get(i2).getQuantityCompany();
//											} else {
//												totalQuantityCompany = totalQuantityCompany + 0;
//											}
//										}
//									}
//									mySheet.addCell(sheetData, cd + j, r, totalQuantityCompany, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									check = 1;
//									totalQuantityCompany = 0;
//								} else {
//									if (listVO.get(j).getId().equals(record.getId())) {
//										mySheet.addCell(sheetData, cd + j, r, record.getQuantityCompany(), style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//										check = 1;
//									} else {
//										mySheet.addCell(sheetData, cd + j, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									}
//									if (check == 0) {
//										mySheet.addCell(sheetData, cd + j, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									}
//								}
//								colum = cd + j;
//							}
//						}
//						 u ke cong ty theo doi 
//						if (record.getShelvesCompany() != null) {
//							totalOversightCompany = totalOversightCompany + record.getShelvesCompany();
//						} else {
//							totalOversightCompany = totalOversightCompany + 0;
//						}
//						if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//								|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//										.get(i - 1).getParentShop() != null)))) {
//							mySheet.addCell(sheetData, colum + 1, r, totalOversightCompany, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							totalOversightCompany = 0;
//							coltotalOversightCompany = colum + 1;
//						} else {
//							mySheet.addCell(sheetData, colum + 1, r, record.getShelvesCompany(), style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							coltotalOversightCompany = 0;
//							coltotalOversightCompany = colum + 1;
//						}
//						if (record.getShelvesOversight() != null) {
//							if (record.getQuantityOversight() != null) {
//								totalOversight = record.getShelvesOversight() + record.getQuantityOversight();
//							} else {
//								totalOversight = record.getShelvesOversight();
//							}
//						} else if (record.getQuantityOversight() != null) {
//							totalOversight = record.getQuantityOversight();
//						} else {
//							totalOversight = 0;
//						}
//						 tong cong GSNPP kiem ke 
//						totalGSNPPAll = totalGSNPPAll + totalOversight;
//						if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//								|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//										.get(i - 1).getParentShop() != null)))) {
//							mySheet.addCell(sheetData, colum + 2, r, totalGSNPPAll, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							totalGSNPPAll = 0;
//							coltotalGSNPPAll = colum + 2;
//						} else {
//							mySheet.addCell(sheetData, colum + 2, r, totalOversight, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							coltotalGSNPPAll = 0;
//							coltotalGSNPPAll = colum + 2;
//						}
//						colum = colum + 3;
//						if (listVO != null && listVO.size() > 0) {
//							for (int j = 0, sz = listVO.size(); j < sz; j++) {
//								if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//										|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i)
//												.getParentShop() == null && lstData.get(i - 1).getParentShop() != null)))) {
//									 tinh tong theo cac loai thiet bi 
//									for (int i2 = 0, sz2 = lstData.size(); i2 < sz2; i2++) {
//										if (listVO.get(j).getId().equals(lstData.get(i2).getId())) {
//											if (lstData.get(i2).getQuantityOversight() != null) {
//												totalQuantityGSNPP = totalQuantityGSNPP + lstData.get(i2).getQuantityOversight();
//											} else {
//												totalQuantityGSNPP = totalQuantityGSNPP + 0;
//											}
//										}
//									}
//									mySheet.addCell(sheetData, colum + j, r, totalQuantityGSNPP, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									checkOversight = 1;
//									totalQuantityGSNPP = 0;
//									coltotalQuantityGSNPP = colum + j;
//									if (listVO.get(j).getId().equals(record.getId())) {
//									if (record.getQuantityOversight() != null) {
//										totalQuantityGSNPP = totalQuantityGSNPP + record.getQuantityOversight();
//									} else {
//										totalQuantityGSNPP = totalQuantityGSNPP + 0;
//									}
//									mySheet.addCell(sheetData, colum + j, r, totalQuantityGSNPP, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									checkOversight = 1;
//									totalQuantityGSNPP = 0;
//									coltotalQuantityGSNPP = colum + j;
//									} else {
//									mySheet.addCell(sheetData, colum + j, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									coltotalQuantityGSNPP = 0;
//									coltotalQuantityGSNPP = colum + j;
//									}
//									if (checkOversight == 0) {
//									mySheet.addCell(sheetData, colum + j, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									}
//								
//								} else {
//									if (listVO.get(j).getId().equals(record.getId())) {
//										mySheet.addCell(sheetData, colum + j, r, record.getQuantityOversight(), style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//										checkOversight = 1;
//									} else {
//										mySheet.addCell(sheetData, colum + j, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									}
//									if (checkOversight == 0) {
//										mySheet.addCell(sheetData, colum + j, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									}
//									coltotalQuantityGSNPP = colum;
//								}
//								colw = colum + j;
//							}
//						}
//						 u ke GSNPP 
//						if (record.getShelvesOversight() != null) {
//							totalOversightGSNPP = totalOversightGSNPP + record.getShelvesOversight();
//						} else {
//							totalOversightGSNPP = totalOversightGSNPP + 0;
//						}
//						if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//								|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//										.get(i - 1).getParentShop() != null)))) {
//							mySheet.addCell(sheetData, colw + 1, r, totalOversightGSNPP, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							totalOversightGSNPP = 0;
//							coltotalOversightGSNPP = colw + 1;
//						} else {
//							mySheet.addCell(sheetData, colw + 1, r, record.getShelvesOversight(), style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							coltotalOversightGSNPP = 0;
//							coltotalOversightGSNPP = colw + 1;
//						}
//						chenh lech thieu - thua
//						int total = totalCompany - totalOversight;
//						if (total >= 0) {
//							differenceShort = differenceShort + total;
//							mySheet.addCell(sheetData, colw + 2, r, total, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, colw + 3, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//						} else {
//							differenexcess = differenexcess + total;
//							mySheet.addCell(sheetData, colw + 2, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, colw + 3, r, total, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//						}
//						if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//								|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//										.get(i - 1).getParentShop() != null)))) {
//							mySheet.addCell(sheetData, colw + 2, r, differenceShort, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, colw + 3, r, differenexcess, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							differenceShort = 0;
//							differenexcess = 0;
//						}
//						chenh lech loai thiet bi
//						colcheck = colw + 4;
//						coltotalCheck =  colw + 4;
//						if (listVO != null && listVO.size() > 0) {
//							for (int j = 0, sz = listVO.size(); j < sz; j++) {
//								int totalCheck = 0;
//								if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//										|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//												.get(i - 1).getParentShop() != null)))) {
//									 tinh tong theo cac loai thiet bi 
//									for (int i2 = 0, sz2 = lstData.size(); i2 < sz2; i2++) {
//										if (listVO.get(j).getId().equals(lstData.get(i2).getId())) {
//											if (lstData.get(i2).getQuantityOversight() != null) {
//												if (lstData.get(i2).getQuantityCompany() != null) {
//													totalCheck = totalCheck + lstData.get(i2).getQuantityCompany() - lstData.get(i2).getQuantityOversight();
//												} else {
//													totalCheck = totalCheck - lstData.get(i2).getQuantityOversight();
//												}
//											} else if (lstData.get(i2).getQuantityCompany() != null) {
//												totalCheck = totalCheck + lstData.get(i2).getQuantityCompany();
//											} else {
//												totalCheck = totalCheck + 0;
//											}
//										}
//									}
//									mySheet.addCell(sheetData, colcheck + j, r, totalCheck, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));								
//								} else {
//									if (listVO.get(j).getId().equals(record.getId())) {
//										if (record.getQuantityOversight() != null) {
//											if (record.getQuantityCompany() != null) {
//												totalCheck = record.getQuantityCompany() - record.getQuantityOversight();
//											} else {
//												totalCheck = -record.getQuantityOversight();
//											}
//										} else if (record.getQuantityCompany() != null) {
//											totalCheck = record.getQuantityCompany();
//										} else {
//											totalCheck = 0;
//										}
//									}									
//									mySheet.addCell(sheetData, colcheck + j, r, totalCheck, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));									
//								}								
//								cell = colcheck + j;
//							}
//						}
//						//u ke chenh lech
//						int totalShelves = 0;
//						if (record.getShelvesOversight() != null) {
//							if (record.getShelvesCompany() != null) {
//								totalShelves = record.getShelvesCompany() - record.getShelvesOversight();
//							} else {
//								totalShelves = -record.getShelvesOversight();
//							}
//						} else if (record.getShelvesCompany() != null) {
//							totalShelves = record.getShelvesCompany();
//						} else {
//							totalShelves = 0;
//						}
//						differenCheck = differenCheck + totalShelves;
//						if ((i != 0 && lstData.get(i).getParentShop() != null && lstData.get(i - 1).getParentShop() != null && !lstData.get(i).getParentShop().equals(lstData.get(i - 1).getParentShop()))
//								|| (i != 0 && !lstData.get(i).getShopCode().equals(lstData.get(i - 1).getShopCode()) && ((lstData.get(i).getParentShop() != null || lstData.get(i - 1).getParentShop() == null) || (lstData.get(i).getParentShop() == null && lstData
//										.get(i - 1).getParentShop() != null)))) {
//							mySheet.addCell(sheetData, cell + 1, r, differenCheck, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, cell + 2, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							differenCheck = 0;
//						} else {
//							mySheet.addCell(sheetData, cell + 1, r, totalShelves, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, cell + 2, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//						}
//						r++;
//						 tinh tong kiem ke 
//						if (i + 1 == size) {
//							mySheet.addCellsAndMerged(sheetData, 0, r, 8, r, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label20") + lstData.get(i).getParentShop(), style.get(ExcelPOIProcessUtils.BOLD));
//							for (int k = 0, siz = lstData.size(); k < siz; k++) {
//								if ((lstData.get(i).getParentShop() == null && lstData.get(k).getParentShop() == null && lstData.get(i).getShopCode().equals(lstData.get(k).getShopCode()))
//										|| (lstData.get(i).getParentShop() != null && lstData.get(i).getParentShop().equals(lstData.get(k).getParentShop()))) {
//									if (totalCompanyAll == 0) {
//										if (lstData.get(k).getShelvesCompany() != null) {
//											if (lstData.get(k).getQuantityCompany() != null) {
//												totalCompany = lstData.get(k).getShelvesCompany() + lstData.get(k).getQuantityCompany();
//											} else {
//												totalCompany = lstData.get(k).getShelvesCompany();
//											}
//										} else if (lstData.get(k).getQuantityCompany() != null) {
//											totalCompany = lstData.get(k).getQuantityCompany();
//										} else {
//											totalCompany = 0;
//										}
//										totalCompanyAll = totalCompanyAll + totalCompany;
//									} else {
//										totalCompanyAll = totalCompanyAll + 0;
//									}
//									if (totalGSNPPAll == 0) {
//										if (lstData.get(k).getShelvesOversight() != null) {
//											if (lstData.get(k).getQuantityOversight() != null) {
//												totalOversight = lstData.get(k).getShelvesOversight() + lstData.get(k).getQuantityOversight();
//											} else {
//												totalOversight = lstData.get(k).getShelvesOversight();
//											}
//										} else if (lstData.get(k).getQuantityOversight() != null) {
//											totalOversight = lstData.get(k).getQuantityOversight();
//										} else {
//											totalOversight = 0;
//										}
//										totalGSNPPAll = totalGSNPPAll + totalOversight;
//									} else {
//										totalGSNPPAll = totalGSNPPAll + 0;
//									}
//									if (totalOversightCompany == 0) {
//										if (lstData.get(k).getShelvesCompany() != null) {
//											totalOversightCompany = totalOversightCompany + lstData.get(k).getShelvesCompany();
//										} else {
//											totalOversightCompany = totalOversightCompany + 0;
//										}
//									} else {
//										totalOversightCompany = totalOversightCompany + 0;
//									}
//									if (totalOversightGSNPP == 0) {
//										if (lstData.get(k).getShelvesOversight() != null) {
//											totalOversightGSNPP = totalOversightGSNPP + lstData.get(k).getShelvesOversight();
//										} else {
//											totalOversightGSNPP = totalOversightGSNPP + 0;
//										}
//									} else {
//										totalOversightGSNPP = totalOversightGSNPP + 0;
//									}
//									if (totalQuantityCompany == 0) {
//										if (lstData.get(k).getQuantityCompany() != null) {
//											totalQuantityCompany = totalQuantityCompany + lstData.get(k).getQuantityCompany();
//										} else {
//											totalQuantityCompany = totalQuantityCompany + 0;
//										}
//									} else {
//										totalQuantityCompany = totalQuantityCompany + 0;
//									}
//									if (totalQuantityGSNPP == 0) {
//										if (lstData.get(k).getQuantityOversight() != null) {
//											totalQuantityGSNPP = totalQuantityGSNPP + lstData.get(k).getQuantityOversight();
//										} else {
//											totalQuantityGSNPP = totalQuantityGSNPP + 0;
//										}
//									} else {
//										totalQuantityGSNPP = totalQuantityGSNPP + 0;
//									}
//								}
//								 xuat xls 
//								if (listVO != null && listVO.size() > 0) {
//									for (int j = 0, sz = listVO.size(); j < sz; j++) {
//										for (int i2 = 0, sz2 = lstData.size(); i2 < sz2; i2++) {
//											if (listVO.get(j).getId().equals(lstData.get(i2).getId())) {
//												if (lstData.get(i2).getQuantityCompany() != null) {
//													totalQuantityCompany = totalQuantityCompany + lstData.get(i2).getQuantityCompany();
//												} else {
//													totalQuantityCompany = totalQuantityCompany + 0;
//												}
//											}
//										}
//										mySheet.addCell(sheetData, cd + j, r, totalQuantityCompany, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//										check = 1;
//										totalQuantityCompany = 0;
//									}
//								}
//								if (listVO != null && listVO.size() > 0) {
//									for (int j = 0, sz = listVO.size(); j < sz; j++) {
//										int totalCheck = 0;
//										for (int i2 = 0, sz2 = lstData.size(); i2 < sz2; i2++) {
//											if (listVO.get(j).getId().equals(lstData.get(i2).getId())) {
//												if (lstData.get(i2).getQuantityOversight() != null) {
//													totalQuantityGSNPP = totalQuantityGSNPP + lstData.get(i2).getQuantityOversight();
//												} else {
//													totalQuantityGSNPP = totalQuantityGSNPP + 0;
//												}
//												chenh lech loai thiet bi
//												if (lstData.get(i2).getQuantityOversight() != null) {
//													if (lstData.get(i2).getQuantityCompany() != null) {
//														totalCheck = totalCheck + lstData.get(i2).getQuantityCompany() - lstData.get(i2).getQuantityOversight();
//													} else {
//														totalCheck = totalCheck - lstData.get(i2).getQuantityOversight();
//													}
//												} else if (lstData.get(i2).getQuantityCompany() != null) {
//													totalCheck = totalCheck + lstData.get(i2).getQuantityCompany();
//												} else {
//													totalCheck = totalCheck + 0;
//												}
//											}
//										}
//										mySheet.addCell(sheetData, coltotalQuantityGSNPP + j, r, totalQuantityGSNPP, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//										checkOversight = 1;
//										totalQuantityGSNPP = 0;			
//										mySheet.addCell(sheetData, coltotalCheck + j, r, totalCheck, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//									}
//								}
//							}
//							mySheet.addCell(sheetData, 9, r, totalCompanyAll, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							totalCompanyAll = 0;
//							mySheet.addCell(sheetData, coltotalGSNPPAll, r, totalGSNPPAll, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							totalGSNPPAll = 0;
//							mySheet.addCell(sheetData, coltotalOversightCompany, r, totalOversightCompany, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							totalOversightCompany = 0;
//							mySheet.addCell(sheetData, coltotalOversightGSNPP, r, totalOversightGSNPP, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							totalOversightGSNPP = 0;							
//							mySheet.addCell(sheetData, coltotalOversightGSNPP + 1, r, differenceShort, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, coltotalOversightGSNPP + 2, r, differenexcess, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, cell + 1, r, differenCheck, style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, cell + 2, r, "", style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
//							r++;
//						}
//						 end 
//					}
//					r += 3;
//					mySheet.addCell(sheetData, 0, r, R.getResource("report.equip.3.1.1.1.label17"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 2, r + 2, R.getResource("report.equip.3.1.1.1.label18"), style.get(ExcelPOIProcessUtils.NORMAL));
//					r += 5;
//					mySheet.addCell(sheetData, 0, r, R.getResource("report.equip.3.1.1.1.label19"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 10, r, R.getResource("report.equip.gdtnkd"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 15, r++, R.getResource("report.equip.nguoi.lap.bao.cao"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//
//					String fileName = R.getResource("report.equip.3.1.1.1.bao.mat.sheet.label"); // ten file: bao_mat_ky
//					fileName = getReportNameFormat(fileName, "", "", FileExtension.XLSX.getValue());
//					outputPath = SXSSFReportHelper.exportXLSX(workbook, null, null, fileName);
//					result.put(ERROR, false);
//					result.put("hasData", true);
//					result.put(REPORT_PATH, outputPath);
//					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//				}
//				result.put(ERROR, false);
//				result.put("hasData", true);
//				result.put(REPORT_PATH, outputPath);
//				*//** begin Bo sung ATTT *//*
//				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//			} else {
//				result.put(ERROR, false);
//				result.put("hasData", false);
//			}
//			lstData = null;
//		} catch (Exception e) {
//			System.out.print(e.getMessage());
//			LogUtility.logError(e, "EquipmentReportAction.exportBCTHKKT" + e.getMessage());
//			if (workbook != null) {
//				workbook.dispose();
//			}
//		} finally {
//			if (workbook != null) {
//				workbook.dispose();
//			}
//		}
//		return JSON;
//	}*/
//	
//	/**
//	 * Bao cao kiem ke KK1.1
//	 * 
//	 * @author phuongvm
//	 * @return file XLS
//	 * @since 24/08/2015
//	 */
//	public String exportBCTHKKT() throws IOException {
//		SXSSFWorkbook workbook = null;
//		parametersReport = new HashMap<String, Object>();
//		parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
//		Date startLogDate = DateUtil.now();
//		try {
//			/** begin Bo sung ATTT */
//			String reportToken = retrieveReportToken(reportCode);
//			if (StringUtil.isNullOrEmpty(reportToken)) {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
//				return JSON;
//			}
//			/** end Bo sung ATTT */
//			Shop shop = null;
//			if (shopId != null) {
//				shop = shopMgr.getShopById(shopId);
//				if (shop != null) {
//					parametersReport.put("sName", shop.getShopName());
//					if (!StringUtil.isNullOrEmpty(shop.getAddress())) {
//						parametersReport.put("sAddress", shop.getAddress());
//					} else {
//						parametersReport.put("sAddress", "");
//					}
//				} else {
//					//common.not.exist.in.db = {0} hiện kh?�ng c?� trong hệ thống.
//					result.put(ERROR, true);
//					result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("catalog.unit.tree")));
//					return JSON;
//				}
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", R.getResource("common.not.exist.in.db", R.getResource("catalog.unit.tree")));
//				return JSON;
//			}
//			String text = "";
//			Date fromDate = commonMgr.getSysDate();
//			Date toDate = fromDate;
//			if (period != null) {
//				EquipPeriod equipPeriod = equipmentManagerMgr.getListEquipPeriodById(new Long(period));
//				if (equipPeriod != null) {
//					text = equipPeriod.getName();
//					fromDate = equipPeriod.getFromDate();
//					toDate = equipPeriod.getToDate();
//				}
//			}
//			List<RptBCTDTMTHVO> lstData = hoReportMgr.getRptKK_1_1(lstShopId, statisticRecord, period, fromDate, toDate);
//			if (lstData != null && lstData.size() > 0) {
//				String outputPath = "";
//				if ("PDF".equals(formatType)) {
//					// xuat PDF
//				} else {
//					/** xuat file excel */
//					//Init XSSF workboook
//					workbook = new SXSSFWorkbook(200);
//					workbook.setCompressTempFiles(true);
//					//Tao shett
//					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
//					ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();
//					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("report.equip.3.1.1.1.sheet.name"));
//					//Set Getting Defaul
//					sheetData.setDefaultRowHeight((short) (15 * 25));sheetData.createFreezePane(7, 0);
//					sheetData.setDefaultColumnWidth(13);
//					//set static Column width
//					mySheet.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 100, 250, 300, 60, 80, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65);
//					//Size Row
//					mySheet.setRowHeight(sheetData, 5, 25);
//					mySheet.addCell(sheetData, 0, 0, R.getResource("report.equip.sheet.label"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 0, 1, R.getResource("report.equip.sheet.label2"), style.get(ExcelPOIProcessUtils.BOLD));
//					mySheet.addCell(sheetData, 7, 3, R.getResource("report.equip.3.1.1.1.label1") + text, style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 0, 5, R.getResource("report.equip.3.1.1.1.title"), style.get(ExcelPOIProcessUtils.BOLD));
//					mySheet.addCell(sheetData, 0, 6, R.getResource("report.equip.3.1.1.1.title1"), style.get(ExcelPOIProcessUtils.BOLD));
//					listVO = equipmentManagerMgr.getListCategoryCount(statisticRecord); 
//					//header
//					mySheet.addCellsAndMerged(sheetData, 0, 7, 0, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu1"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 1, 7, 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label2"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 2, 7, 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.10.label2"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 3, 7, 3, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label3"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 4, 7, 4, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu5"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 5, 7, 5, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu6"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 6, 7, 6, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label4"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 7, 7, 7, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label5"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 8, 7, 8, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label6"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 9, 8, 9, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label8"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					int c = 10;
//					int col = 0;
//					int colgs = 0;
//					int colcl = 0;
//					if (listVO != null && listVO.size() > 0) {
//						for (int i = 0, sz = listVO.size(); i < sz; i++) {
//							mySheet.setColumnsWidth(sheetData, c + i, 65);
//							mySheet.addCell(sheetData, c + i, 9, listVO.get(i).getName(), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//							col = c + i;
//						}
//					}
//					mySheet.addCell(sheetData, col + 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label12"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 9, 7, col + 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label7"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, 10, 8, col + 1, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label9"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					c = col + 3;
//					if (listVO != null && listVO.size() > 0) {
//						for (int i = 0, sz = listVO.size(); i < sz; i++) {
//							mySheet.setColumnsWidth(sheetData, c + i, 65);
//							mySheet.addCell(sheetData, c + i, 9, listVO.get(i).getName(), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//							colgs = c + i;
//						}
//					}
//					mySheet.addCellsAndMerged(sheetData, col + 2, 8, col + 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label8"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, col + 2, 7, colgs + 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label13"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, col + 3, 8, colgs + 1, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label9"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCell(sheetData, colgs + 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label12"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//
//					mySheet.addCellsAndMerged(sheetData, colgs + 2, 8, colgs + 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label15"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, colgs + 3, 8, colgs + 3, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label16"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					c = colgs + 4;
//					if (listVO != null && listVO.size() > 0) {
//						for (int i = 0, sz = listVO.size(); i < sz; i++) {
//							mySheet.setColumnsWidth(sheetData, c + i, 65);
//							mySheet.addCell(sheetData, c + i, 9, listVO.get(i).getName(), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//							colcl = c + i;
//						}
//					}
//					mySheet.addCell(sheetData, colcl + 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label12"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, colgs + 2, 7, colcl + 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label14"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, colgs + 4, 8, colcl + 1, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.1.label9"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//					mySheet.addCellsAndMerged(sheetData, colcl + 2, 7, colcl + 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu18"), style.get(ExcelPOIProcessUtils.HEADER_YEALLOW_ALL_THIN_BOLD));
//
//					// xu ly danh sach detail			
//					int r = 10;
//
//					Integer totalColCompany = 0;
//					Integer totalColGSNPP = 0;
//					Integer totalColDiff = 0;
//					Integer totalThieu = 0;
//					Integer totalThua = 0;
//					int colCount = 0;
//					//					int startIndex = r;
//					//					int endIndex = r;
//					if (listVO != null) {
//						EquipmentVO equipVO = new EquipmentVO();
//						equipVO.setCode("ShelvesCode");
//						listVO.add(equipVO);
//						colCount = listVO.size();
//					}
//					int stt = 1;
//					for (int i = 0, size = lstData.size(); i < size; i++) {
//						String titleStatisticTime = "";
//						RptBCTDTMTHVO record = lstData.get(i);
//						int colDetailBegin = 9;
//						int colDetail = colDetailBegin + 1;
//						int colDetail2 = colDetail + colCount + 1;
//						int colDetail3 = colDetail2 + colCount + 2;
//						int colDetailBegin2 = colDetailBegin + colCount + 1;
//						int colDetailBegin3 = colDetailBegin2 + colCount + 1;
//						int colDetailBegin4 = colDetailBegin3 + 1;
//						/* tinh tong kiem ke */
////						if (lstData.get(i).getMien() == null && lstData.get(i).getArea() == null && lstData.get(i).getShopCode() == null) {
//						if (lstData.get(i).getArea() == null && lstData.get(i).getShopCode() == null && lstData.get(i).getStatisticDate() == null) {
//							if (lstData.get(i).getParentShop() != null && lstData.get(i).getMien() != null) {
//								mySheet.addCellsAndMerged(sheetData, 0, r, 8, r, R.getResource("report.equip.3.1.1.1.total.mien") + lstData.get(i).getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							} else if (lstData.get(i).getParentShop() != null && lstData.get(i).getMien() == null) {
//								mySheet.addCellsAndMerged(sheetData, 0, r, 8, r, R.getResource("report.equip.3.1.1.1.label20") + lstData.get(i).getParentShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE02));
//							} else {
//								mySheet.addCellsAndMerged(sheetData, 0, r, 8, r, R.getResource("salemt.report.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_YELLOW_01));
//							}
//							// so luong cty theo doi
//							if (listVO != null && listVO.size() > 0) {
//								if (lstData.get(i).getParentShop() != null && lstData.get(i).getMien() != null) {
//									//color orange
//									List<DynamicVO> lstDetail = record.getLstDynamic();
//									if (lstDetail != null && lstDetail.size() > 0) {
//										for (int j = 0, sz = listVO.size(); j < sz; j++) {
//											for (int k = 0, sz1 = lstDetail.size(); k < sz1; k++) {
//												DynamicVO vo = lstDetail.get(k);
//												if (vo.getKey() != null && vo.getKey().contains(QUANTITYCOMPANY)) {
//													String[] arrStr = vo.getKey().split(QUANTITYCOMPANY);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															totalColCompany += value;
//															mySheet.addCell(sheetData, colDetail++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//														}
//													}
//												} else if (vo.getKey() != null && vo.getKey().contains(QUANTITYOVERSIGHT)) {
//													String[] arrStr = vo.getKey().split(QUANTITYOVERSIGHT);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															totalColGSNPP += value;
//															mySheet.addCell(sheetData, colDetail2++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//														}
//													}
//												} else if (vo.getKey() != null && vo.getKey().contains(DEVIANT)) {
//													String[] arrStr = vo.getKey().split(DEVIANT);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															mySheet.addCell(sheetData, colDetail3++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//															break;
//														}
//													}
//												}
//											}
//										}
//										//cot tong cong nhom thiet bi + u ke
//										mySheet.addCell(sheetData, colDetailBegin, r, totalColCompany, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//										mySheet.addCell(sheetData, colDetailBegin2, r, totalColGSNPP, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//										totalColDiff = totalColCompany - totalColGSNPP;
//										if (totalColDiff > 0) {
//											mySheet.addCell(sheetData, colDetailBegin3, r, totalColDiff, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//											mySheet.addCell(sheetData, colDetailBegin4, r, 0, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//											totalThieu += totalColDiff;
//										} else {
//											mySheet.addCell(sheetData, colDetailBegin3, r, 0, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//											mySheet.addCell(sheetData, colDetailBegin4, r, totalColDiff, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//											totalThua += totalColDiff;
//										}
//										mySheet.addCell(sheetData, colDetailBegin4 + colCount + 1, r, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//										totalColCompany = 0;
//										totalColGSNPP = 0;
//										totalColDiff = 0;
//									}
//								} else if (lstData.get(i).getParentShop() != null && lstData.get(i).getMien() == null) {//color orange
//									List<DynamicVO> lstDetail = record.getLstDynamic();
//									if (lstDetail != null && lstDetail.size() > 0) {
//										for (int j = 0, sz = listVO.size(); j < sz; j++) {
//											for (int k = 0, sz1 = lstDetail.size(); k < sz1; k++) {
//												DynamicVO vo = lstDetail.get(k);
//												if (vo.getKey() != null && vo.getKey().contains(QUANTITYCOMPANY)) {
//													String[] arrStr = vo.getKey().split(QUANTITYCOMPANY);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															totalColCompany += value;
//															mySheet.addCell(sheetData, colDetail++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//														}
//													}
//												} else if (vo.getKey() != null && vo.getKey().contains(QUANTITYOVERSIGHT)) {
//													String[] arrStr = vo.getKey().split(QUANTITYOVERSIGHT);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															totalColGSNPP += value;
//															mySheet.addCell(sheetData, colDetail2++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//														}
//													}
//												} else if (vo.getKey() != null && vo.getKey().contains(DEVIANT)) {
//													String[] arrStr = vo.getKey().split(DEVIANT);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															mySheet.addCell(sheetData, colDetail3++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//															break;
//														}
//													}
//												}
//											}
//										}
//										//cot tong cong nhom thiet bi + u ke
//										mySheet.addCell(sheetData, colDetailBegin, r, totalColCompany, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//										mySheet.addCell(sheetData, colDetailBegin2, r, totalColGSNPP, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//										totalColDiff = totalColCompany - totalColGSNPP;
//										if (totalColDiff > 0) {
//											mySheet.addCell(sheetData, colDetailBegin3, r, totalColDiff, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//											mySheet.addCell(sheetData, colDetailBegin4, r, 0, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//											totalThieu += totalColDiff;
//										} else {
//											mySheet.addCell(sheetData, colDetailBegin3, r, 0, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//											mySheet.addCell(sheetData, colDetailBegin4, r, totalColDiff, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//											totalThua += totalColDiff;
//										}
//										mySheet.addCell(sheetData, colDetailBegin4 + colCount + 1, r, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE02));
//										totalColCompany = 0;
//										totalColGSNPP = 0;
//										totalColDiff = 0;
//									}
//								} else { //color yellow
//									List<DynamicVO> lstDetail = record.getLstDynamic();
//									if (lstDetail != null && lstDetail.size() > 0) {
//										for (int j = 0, sz = listVO.size(); j < sz; j++) {
//											for (int k = 0, sz1 = lstDetail.size(); k < sz1; k++) {
//												DynamicVO vo = lstDetail.get(k);
//												if (vo.getKey() != null && vo.getKey().contains(QUANTITYCOMPANY)) {
//													String[] arrStr = vo.getKey().split(QUANTITYCOMPANY);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															totalColCompany += value;
//															mySheet.addCell(sheetData, colDetail++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//														}
//													}
//												} else if (vo.getKey() != null && vo.getKey().contains(QUANTITYOVERSIGHT)) {
//													String[] arrStr = vo.getKey().split(QUANTITYOVERSIGHT);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															totalColGSNPP += value;
//															mySheet.addCell(sheetData, colDetail2++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//														}
//													}
//												} else if (vo.getKey() != null && vo.getKey().contains(DEVIANT)) {
//													String[] arrStr = vo.getKey().split(DEVIANT);
//													if (arrStr != null && arrStr.length > 0) {
//														String code = arrStr[0];
//														code = code.replace("'", "");
//														if (code.equals(listVO.get(j).getCode())) {
//															Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//															mySheet.addCell(sheetData, colDetail3++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//															break;
//														}
//													}
//												}
//											}
//										}
//										//cot tong cong nhom thiet bi + u ke
//										mySheet.addCell(sheetData, colDetailBegin, r, totalColCompany, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//										mySheet.addCell(sheetData, colDetailBegin2, r, totalColGSNPP, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//										totalColDiff = totalColCompany - totalColGSNPP;
//										if (totalColDiff > 0) {
//											mySheet.addCell(sheetData, colDetailBegin3, r, totalColDiff, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//											mySheet.addCell(sheetData, colDetailBegin4, r, 0, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//											totalThieu += totalColDiff;
//										} else {
//											mySheet.addCell(sheetData, colDetailBegin3, r, 0, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//											mySheet.addCell(sheetData, colDetailBegin4, r, totalColDiff, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//											totalThua += totalColDiff;
//										}
//										mySheet.addCell(sheetData, colDetailBegin4 + colCount + 1, r, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_YELLOW_01));
//										totalColCompany = 0;
//										totalColGSNPP = 0;
//										totalColDiff = 0;
//									}
//								}
//							}
//							//							endIndex = i + 10; 
//							//							if(startIndex!=endIndex){
//							//								mySheet.groupRows(sheetData, startIndex, endIndex, null, null, null);
//							//							}
//							//							startIndex =  endIndex;
//						} else {
//							mySheet.addCell(sheetData, 0, r, stt++, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
//							mySheet.addCell(sheetData, 1, r, record.getParentShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//							mySheet.addCell(sheetData, 2, r, record.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//							mySheet.addCell(sheetData, 3, r, record.getArea(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//							mySheet.addCell(sheetData, 4, r, record.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//							mySheet.addCell(sheetData, 5, r, record.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//							mySheet.addCell(sheetData, 6, r, record.getGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//							if (record.getStatisticTime() != null) {
//								titleStatisticTime = R.getResource("report.equip.3.1.1.1.label21") + record.getStatisticTime();
//							}
//							mySheet.addCell(sheetData, 7, r, titleStatisticTime, style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//							mySheet.addCell(sheetData, 8, r, record.getStatisticDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER_BOLD));
//							// so luong cty theo doi
//							if (listVO != null && listVO.size() > 0) {
//								List<DynamicVO> lstDetail = record.getLstDynamic();
//								if (lstDetail != null && lstDetail.size() > 0) {
//									for (int j = 0, sz = listVO.size(); j < sz; j++) {
//										for (int k = 0, sz1 = lstDetail.size(); k < sz1; k++) {
//											DynamicVO vo = lstDetail.get(k);
//											if (vo.getKey() != null && vo.getKey().contains(QUANTITYCOMPANY)) {
//												String[] arrStr = vo.getKey().split(QUANTITYCOMPANY);
//												if (arrStr != null && arrStr.length > 0) {
//													String code = arrStr[0];
//													code = code.replace("'", "");
//													if (code.equals(listVO.get(j).getCode())) {
//														Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//														totalColCompany += value;
//														mySheet.addCell(sheetData, colDetail++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//													}
//												}
//											} else if (vo.getKey() != null && vo.getKey().contains(QUANTITYOVERSIGHT)) {
//												String[] arrStr = vo.getKey().split(QUANTITYOVERSIGHT);
//												if (arrStr != null && arrStr.length > 0) {
//													String code = arrStr[0];
//													code = code.replace("'", "");
//													if (code.equals(listVO.get(j).getCode())) {
//														Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//														totalColGSNPP += value;
//														mySheet.addCell(sheetData, colDetail2++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//													}
//												}
//											} else if (vo.getKey() != null && vo.getKey().contains(DEVIANT)) {
//												String[] arrStr = vo.getKey().split(DEVIANT);
//												if (arrStr != null && arrStr.length > 0) {
//													String code = arrStr[0];
//													code = code.replace("'", "");
//													if (code.equals(listVO.get(j).getCode())) {
//														Integer value = Integer.valueOf(vo.getValue() != null ? vo.getValue().toString() : "0");
//														mySheet.addCell(sheetData, colDetail3++, r, value, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//														break;
//													}
//												}
//											}
//										}
//									}
//									//cot tong cong nhom thiet bi + u ke
//									mySheet.addCell(sheetData, colDetailBegin, r, totalColCompany, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//									mySheet.addCell(sheetData, colDetailBegin2, r, totalColGSNPP, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//									totalColDiff = totalColCompany - totalColGSNPP;
//									if (totalColDiff > 0) {
//										mySheet.addCell(sheetData, colDetailBegin3, r, totalColDiff, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//										mySheet.addCell(sheetData, colDetailBegin4, r, 0, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//										totalThieu += totalColDiff;
//									} else {
//										mySheet.addCell(sheetData, colDetailBegin3, r, 0, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//										mySheet.addCell(sheetData, colDetailBegin4, r, totalColDiff, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//										totalThua += totalColDiff;
//									}
//									mySheet.addCell(sheetData, colDetailBegin4 + colCount + 1, r, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//									totalColCompany = 0;
//									totalColGSNPP = 0;
//									totalColDiff = 0;
//								}
//							}
//						}
//						r++;
//					}
//
//					r += 3;
//					mySheet.addCell(sheetData, 0, r, R.getResource("report.equip.3.1.1.1.label17"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 2, r + 2, R.getResource("report.equip.3.1.1.1.label18"), style.get(ExcelPOIProcessUtils.NORMAL));
//					r += 5;
//					mySheet.addCell(sheetData, 0, r, R.getResource("report.equip.3.1.1.1.label19"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 10, r, R.getResource("report.equip.gdtnkd"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 15, r++, R.getResource("report.equip.nguoi.lap.bao.cao"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//
//					String fileName = R.getResource("report.equip.3.1.1.1.file.name"); // ten file: bao_mat_ky
//					fileName = getReportNameFormat(fileName, "", "", FileExtension.XLSX.getValue());
//					outputPath = SXSSFReportHelper.exportXLSX(workbook, null, null, fileName);
//					result.put(ERROR, false);
//					result.put("hasData", true);
//					result.put(REPORT_PATH, outputPath);
//					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//				}
//				result.put(ERROR, false);
//				result.put("hasData", true);
//				result.put(REPORT_PATH, outputPath);
//				/** begin Bo sung ATTT */
//				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//			} else {
//				result.put(ERROR, false);
//				result.put("hasData", false);
//			}
//			lstData = null;
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportBCTHKKT"), createLogErrorStandard(startLogDate));
//		} finally {
//			if (workbook != null) {
//				workbook.dispose();
//			}
//		}
//		return JSON;
//	}
//	
//	/**
//	 * Bao cao hinh anh kiem ke KK1.2
//	 * 
//	 * @author phuongvm
//	 * @return file XLS
//	 * @since 18/09/2015
//	 */
//	public String exportKK1_2() {
//		Date startLogDate = DateUtil.now();
//		SXSSFWorkbook workbook = null;
//		FileOutputStream out = null;
//		try {
//			/** Bo sung ATTT */
//			String reportToken = retrieveReportToken(reportCode);
//			if (StringUtil.isNullOrEmpty(reportToken)) {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
//				return JSON;
//			}
//			Date fDate = DateUtil.parse(fromDate, DateUtil.DATETIME_FORMAT_STR);
//			Date tDate = DateUtil.parse(toDate, DateUtil.DATETIME_FORMAT_STR);
//
//			String strListStaffId = null;
//			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
//				// Don vi
//				if (!StringUtil.isNullOrEmpty(strListShopId)) {
//					final Boolean isIncludeStoppedShop = true;
//					String[] arrShop = strListShopId.split(",");
//					int len = arrShop.length;
//					for (int i = 0; i < len; i++) {
//						String value = arrShop[i];
//						Long shopIdTmp = Long.valueOf(value);
//						if (!checkShopInOrgAccessNotShopRootById(shopIdTmp, isIncludeStoppedShop)) {
//							result.put(ERROR, true);
//							result.put(errMsg, ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, false, "catalog.unit.tree"));
//							return JSON;
//						}
//					}
//				} else {
//					strListShopId = String.valueOf(currentUser.getShopRoot().getShopId());
//				}
//
//				if (!StringUtil.isNullOrEmpty(staffCode)) {
//					strListStaffId = "";
//					for (String str : staffCode.split(",")) {
//						Staff saleStaff = staffMgr.getStaffByCode(str);
//						if (saleStaff == null) {
//							continue;
//						} else {
//							if (strListStaffId.length() > 0) {
//								strListStaffId += ",";
//							}
//							strListStaffId += saleStaff.getId();
//						}
//					}
//
//				}
////				if (!DateUtil.check2MonthAfter(fromDate)) {
////					result.put(ERROR, true);
////					result.put("errMsg", R.getResource("images.check.date"));
////					return JSON;
////				}
//				List<RptKK1_2> lstData = hoReportMgr.getRptKK_1_2(strListShopId, strListStaffId, fDate, tDate, lstCheck);
//				if (lstData != null && lstData.size() > 0) {
//					workbook = new SXSSFWorkbook(-1);
//					workbook.setCompressTempFiles(true);
//					//Tao shett
//					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.dituyen.vt9.sheetName.kk12"));
//					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
//					ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();
//					CreationHelper createHelper = workbook.getCreationHelper();
//
//					String outputName = ConstantManager.EXPORT_KK1_2 + "_" + DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
//					String exportFileName = Configuration.getStoreRealPath() + outputName;
//					//Chieu cao mac dinh cua row
//					sheetData.setDefaultRowHeight((short) (15 * 20));
//					//set header & menu Row width
//					mySheet.setRowsHeight(sheetData, 0, 15, 25, 15, 15, 28);
//					//set static Col width
//					mySheet.setColumnsWidth(sheetData, 0, 75, 75, 110, 250, 110, 170, 110, 170, 150, 190, 110, 110, 145, 130, 145, 178, 140, 140, 140, 160);
//					//Bo grid line
//					sheetData.setDisplayGridlines(false);
//					String ngayInTmp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.general.ngayIn") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR);
//					mySheet.addCellsAndMerged(sheetData, 0, 0, 3, 0, ngayInTmp, style.get(ExcelPOIProcessUtils.BOLD));
//					//Tieu de bao cao
//					mySheet.addCellsAndMerged(sheetData, 0, 1, 9, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.dituyen.vt9.content.title.kk12"), style.get(ExcelPOIProcessUtils.TITLE_BLUE));
//					//Khoang ngay bao cao
//					mySheet.addCellsAndMerged(sheetData, 0, 2, 9, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.fdat.tdate.show.times", fromDate, toDate), style.get(ExcelPOIProcessUtils.TIMES_TITLE));
//
//					int rowCol = 4;
//					int clCol = 0;
//					//HEADER - CO DINH
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.mien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.title.nvcode"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.title.nvname"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.kh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.tenkh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.program.code.kk12"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.program.name.kk12"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.program.name.kk12.equip"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.count.photograph"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.nice.photograph"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					// so hinh khong dat,dakiemtra,nguoikiemtra,thoigiankiemtra, so mat, POSM, ghi chu
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.bad.photograph"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.bad.photograph.kk12.dakiemtra"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.bad.photograph.kk12.nguoikiemtra"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.bad.photograph.kk12.thoigian"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.number.product"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.POSM"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.display.note"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					// url hinh header
//					int maxNumberUrl = 0;
//					for (int i = 0, size = lstData.size(); i < size; i++) {
//						RptKK1_2 salesVO = lstData.get(i);
//						if (!StringUtil.isNullOrEmpty(salesVO.getUrl())) {
//							String[] tUrl = salesVO.getUrl().split(",");
//							if (tUrl.length > maxNumberUrl) {
//								maxNumberUrl = tUrl.length;
//							}
//						}
//					}
//					for (int j = 0; j < maxNumberUrl; j++) {
//						mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.content.commons.url.file.new", (j + 1)), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//					}
//					RptKK1_2 salesVO = null;
//					RptKK1_2 salesVONext = null;
//					rowCol++;
//					for (int i = 0, size = lstData.size(); i < size; i++) {
//						clCol = 0;
//						salesVO = lstData.get(i);
//						salesVO.safeSetNull();
//						count4Group(5);
////						mySheet.setRowsHeight(sheetData, rowCol, 15);
//						if (i < size - 1) {
//							salesVONext = lstData.get(i + 1);
//							salesVONext.safeSetNull();
//							if (StringUtil.isNullOrEmpty(salesVO.getVung()) && StringUtil.isNullOrEmpty(salesVO.getNpp()) && StringUtil.isNullOrEmpty(salesVO.getMaNV()) && salesVONext.getMien().equals(salesVO.getMien()) && salesVONext.getVung().equals(salesVO.getVung())
//									&& salesVONext.getNpp().equals(salesVO.getNpp())) {
//								continue;
//							}
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getNpp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMaNV(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getTenNV(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getTenKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMaKiemKe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getTenKiemKe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMaThietBi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getSoLanChup(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getSoHinhDat(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//							// so hinh khong dat, so mat, POSM, ghi chu
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getSoHinhKhongDat(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getDakiemTra(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getNguoiKiemTra(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getNgayKiemTra(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getSoMat(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getPOSM(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getGhiChu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							// url hinh
//							if (!StringUtil.isNullOrEmpty(salesVO.getUrl())) {
//								String[] tUrl = salesVO.getUrl().split(",");
//								int j = 0;
//								for (int szj = tUrl.length; j < szj; j++) {
//									int idx = tUrl[j].lastIndexOf("/");
//									String t = tUrl[j].substring(idx + 1);
//									Hyperlink hyperlink = createHelper.createHyperlink(Hyperlink.LINK_URL);
//									hyperlink.setAddress(Configuration.getImgServerSOPath() + tUrl[j] + "?v=" + DateUtil.getCurrentGMTDate().getTime());
//
//									mySheet.addCellHyperlink(sheetData, clCol++, rowCol, t, hyperlink, style.get(ExcelPOIProcessUtils.HYPERLINK_BLUE_LEFT_DOTTED_LEFT));
//								}
//								for (; j < maxNumberUrl; j++) {
//									mySheet.addCell(sheetData, clCol++, rowCol, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//								}
//							} else {
//								for (int j = 0; j < maxNumberUrl; j++) {
//									mySheet.addCell(sheetData, clCol++, rowCol, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//								}
//							}
//							if (StringUtil.isNullOrEmpty(salesVO.getVung())) {
//								main4GroupPOI(sheetData, 3);
//							} else if (StringUtil.isNullOrEmpty(salesVO.getNpp())) {
//								main4GroupPOI(sheetData, 2);
//							} else if (StringUtil.isNullOrEmpty(salesVO.getMaNV())) {
//								main4GroupPOI(sheetData, 1);
//							}
//						} else {
//							// to mau dong tong
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getNpp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMaNV(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getTenNV(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getTenKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMaKiemKe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getTenKiemKe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getMaThietBi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getSoLanChup(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getSoHinhDat(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//							// so hinh khong dat, so mat, POSM, ghi chu
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getSoHinhKhongDat(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getDakiemTra(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getNguoiKiemTra(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getNgayKiemTra(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getSoMat(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getPOSM(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							mySheet.addCell(sheetData, clCol++, rowCol, salesVO.getGhiChu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							// url hinh
//							for (int j = 0; j < maxNumberUrl; j++) {
//								mySheet.setColumnsWidth(sheetData, clCol, 200);
//								mySheet.addCell(sheetData, clCol++, rowCol, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE01));
//							}
//						}
//						rowCol++;
//					}
//					out = new FileOutputStream(exportFileName);
//					workbook.write(out);
//					String outputPath = Configuration.getExportExcelPath() + outputName;
//					result.put(REPORT_PATH, outputPath);
//					result.put(LIST, outputPath);
//					result.put(ERROR, false);
//					result.put("hasData", true);
//
//					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//				} else {
//					result.put("hasData", false);
//				}
//				lstData = null;
//			}
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportKK1_2"), createLogErrorStandard(startLogDate));
//			result.put(ERROR, true);
//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			return JSON;
//		} finally {
//			if (workbook != null) {
//				workbook.dispose();
//			}
//			if (out != null) {
//				try {
//					out.close();
//				} catch (IOException e) {
//					LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportKK1_2"), createLogErrorStandard(startLogDate));
//				}
//			}
//		}
//		return JSON;
//	}
//	
//	/**
//	 * Bao cao kiem ke KK1.3
//	 * 
//	 * @author tamvnm
//	 * @return file XLS
//	 * @since Sep 29 2015
//	 */
//	public String exportKK1_3() {
//		Date startLogDate = DateUtil.now();
//		SXSSFWorkbook workbook = null;
//		FileOutputStream out = null;
//		try {
//			/** Bo sung ATTT */
//			String reportToken = retrieveReportToken(reportCode);
//			if (StringUtil.isNullOrEmpty(reportToken)) {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
//				return JSON;
//			}
//			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_STR);
//			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_STR);
//
//			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getStaffRoot() == null) {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.current.staff.invalid"));
//				return JSON;
//			}
//
//			// Don vi
//			if (!StringUtil.isNullOrEmpty(strListShopId)) {
//				final Boolean isIncludeStoppedShop = true;
//				String[] arrShop = strListShopId.split(",");
//				int len = arrShop.length;
//				for (int i = 0; i < len; i++) {
//					String value = arrShop[i];
//					Long shopIdTmp = Long.valueOf(value);
//					if (!checkShopInOrgAccessNotShopRootById(shopIdTmp, isIncludeStoppedShop)) {
//						result.put(ERROR, true);
//						result.put(errMsg, ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, false, "catalog.unit.tree"));
//						return JSON;
//					}
//				}
//			} else {
//				strListShopId = String.valueOf(currentUser.getShopRoot().getShopId());
//			}
//
//			if (!StringUtil.isNullOrEmpty(staffCode)) {
//				staffCode = staffCode.replaceAll("\\s", "");
//			}
//			if (!StringUtil.isNullOrEmpty(statisticCode)) {
//				statisticCode = statisticCode.replaceAll("\\s", "");
//			}
//
//			List<RptKK1_3> lstData = hoReportMgr.getRptKK_1_3(strListShopId, staffCode, statisticCode, fDate, tDate);
//			if (lstData != null && lstData.size() > 0) {
//				workbook = new SXSSFWorkbook(-1);
//				workbook.setCompressTempFiles(true);
//				// Tao shett
//				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.sheet.name"));
//				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
//				ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();
//
//				String outputName = ConstantManager.EXPORT_KK1_3 + "_" + DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
//				String exportFileName = Configuration.getStoreRealPath() + outputName;
//				// Chieu cao mac dinh cua row
//				// sheetData.setDefaultRowHeight((short) (15 * 20));
//				// set static Col width
//				mySheet.setColumnsWidth(sheetData, 0, 150, 150, 150, 250, 150, 250, 100, 300, 300, 150, 150);
//				// Bo grid line
//				sheetData.setDisplayGridlines(false);
//
//				// Tieu de bao cao
//				mySheet.addCellsAndMerged(sheetData, 0, 1, 10, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.title"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
//				// Khoang ngay bao cao
//				mySheet.addCellsAndMerged(sheetData, 0, 2, 10, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.fdat.tdate.show.times", fromDate, toDate), style.get(ExcelPOIProcessUtils.TIMES_TITLE));
//
//				int rowCol = 4;
//				int clCol = 0;
//
//				// HEADER - CO DINH
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.mien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.shop.code"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.shop.name"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.staff.code"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.staff.name"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.month"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.statistic.code"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.statistic.name"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.sltg"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//				mySheet.addCell(sheetData, clCol++, rowCol++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "bc.kk.1.3.sl.tb.chup"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//
//				for (int i = 0, isize = lstData.size(); i < isize; i++) {
//					clCol = 0;
//					RptKK1_3 rpt = lstData.get(i);
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getStaffCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getStaffName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getMonth(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getStatisticCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getStatisticName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_WRAP_LEFT));
//					mySheet.addCell(sheetData, clCol++, rowCol, rpt.getJoinNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
//					mySheet.addCell(sheetData, clCol++, rowCol++, rpt.getPhotoNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
//				}
//
//				out = new FileOutputStream(exportFileName);
//				workbook.write(out);
//				String outputPath = Configuration.getExportExcelPath() + outputName;
//				result.put(REPORT_PATH, outputPath);
//				result.put(LIST, outputPath);
//				result.put(ERROR, false);
//				result.put("hasData", true);
//
//				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//			} else {
//				result.put("hasData", false);
//			}
//			lstData = null;
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportKK1_3"), createLogErrorStandard(startLogDate));
//			result.put(ERROR, true);
//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			return JSON;
//		} finally {
//			if (workbook != null) {
//				workbook.dispose();
//			}
//			if (out != null) {
//				try {
//					out.close();
//				} catch (IOException e) {
//					LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportKK1_3"), createLogErrorStandard(startLogDate));
//				}
//			}
//		}
//		return JSON;
//	}
//
	/**
	 * Bao cao 3.1.1.5; Bao cao mat tu
	 * @author dungnt19
	 * @date 8/04/2016
	 * @return file XLS
	 * @throws IOException
	 */
	public String exportBCMATTU1_5() throws IOException {
		actionStartTime = DateUtil.now();
		SXSSFWorkbook workbook = null;
		parametersReport = new HashMap<String, Object>();
		parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
		try {
			/** begin Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			/** end Bo sung ATTT */
			if (!StringUtil.isNullOrEmpty(lstShopId)) {
				shopId = Long.valueOf(lstShopId);
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			}
			Shop shop = shopMgr.getShopById(shopId);
			if (shop == null) {
				result.put(ERR_MSG, R.getResource("catalog.unit.tree.not.exist"));
				return JSON;
			}
			shopName = shop.getShopCode() + " - " + shop.getShopName();
			
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (fDate == null) {
				result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.fromdate")));
				return JSON;
			}
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (tDate == null) {
				result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.todate")));
				return JSON;
			}
			if (DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
				result.put(ERR_MSG, R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate")));
				return JSON;
			}
			String msgDate = checkMaxNumberDayReportLong(fDate, tDate);
			if (!StringUtil.isNullOrEmpty(msgDate)) {
				result.put(ERR_MSG, msgDate);
				return JSON;
			}
			
			//List<RptBCTDTMTHVO> lstData = hoReportMgr.getRptBCDSTDTMTH(shopId, lstShopId, lstEquipId, period);
			Long staffRootId = currentUser.getStaffRoot().getStaffId();
			Long roleId = currentUser.getRoleToken().getRoleId();
			Long shopRootId = currentUser.getShopRoot().getShopId();
			List<RptBCBAOMAT1_5VO> lstData = hoReportMgr.getRptBCBAOMAT1_5(staffRootId, roleId, shopRootId, lstShopId, lstEquipId, categoryIdMultil, fDate, tDate);
			if (lstData != null && lstData.size() > 0) {
				String outputPath = "";
				/** xuat file excel */
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(200);
				workbook.setCompressTempFiles(true);
				//Tao shett
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("report.equip.3.1.1.5.bao.mat.sheet.label"));
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 25));
				sheetData.setDefaultColumnWidth(13);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 
													50, 90, 90, 90, 120, 
													120, 90, 90, 120, 120, 90, 
													150, 150, 150, 90, 90, 
													90, 90, 90, 90, 150, 
													150, 150, 150);
				//Size Row
//				ExcelPOIProcessUtils.setRowHeight(sheetData, 0, 25); // header
//				ExcelPOIProcessUtils.setRowHeight(sheetData, 2, 20); // header
				//ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 15, 25, 15, 15, 35);
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 25, 15, 20, 15, 25);
				sheetData.setDisplayGridlines(false);

//				ExcelPOIProcessUtils.addCell(sheetData, 0, 0, R.getResource("report.equip.sheet.label"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//				ExcelPOIProcessUtils.addCell(sheetData, 0, 1, R.getResource("report.equip.sheet.label2"), style.get(ExcelPOIProcessUtils.BOLD));
				int r = 0;
				ExcelPOIProcessUtils.addCell(sheetData, 8, r,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.3.1.1.5.bao.mat.title"),style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
				
				r += 2;
				ExcelPOIProcessUtils.addCell(sheetData, 3, r, R.getResource("common.shop.name.lable"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, r, shopName, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
				
				r++;
				ExcelPOIProcessUtils.addCell(sheetData, 3, r, R.getResource("common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, r, fromDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, r, R.getResource("common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 6, r, toDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, r, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 8, r, DateUtil.toDateString(actionStartTime, DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
				
//				ExcelPOIProcessUtils.addCell(sheetData, 4, 3, R.getResource("report.equip.3.1.1.5.bao.mat.equip.category"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//				ExcelPOIProcessUtils.addCell(sheetData, 5, 3, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
//				ExcelPOIProcessUtils.addCell(sheetData, 6, 3, R.getResource("report.equip.3.1.1.5.bao.mat.equip.group"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//				ExcelPOIProcessUtils.addCell(sheetData, 7, 3, toDate, style.get(ExcelPOIProcessUtils.NORMAL));

				//header
				r += 2;
				for (int i = 0; i < 24; i++) {
					ExcelPOIProcessUtils.addCell(sheetData, i, r, R.getResource("report.equip.3.1.1.5.bao.mat.menu" + (i + 1)), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				}
				// xu ly danh sach detail
				r++;
				for (int i = 0, size = lstData.size() ; i < size; i++) {
					int column = -1;
					RptBCBAOMAT1_5VO record = lstData.get(i);
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getNumberCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getCreateFormDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getCustomerCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getCustomerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//					ExcelPOIProcessUtils.addCell(sheetData, 7, r, record.getPeriod(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getSerialNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getCategoryName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getGroupName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getBrandName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					//ExcelPOIProcessUtils.addCell(sheetData, 9, r, record.getBrandName(), style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getCapacity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getManufacturingYear(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getPrice(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getPriceActually(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getLostDateStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getLastSaleDateStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getGsAndNPPsearchStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getResultSearchStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getConclusion(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, ++column, r, record.getRecommendedTreatment(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					//mySheet.addCell(sheetData, x + 1, r, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					r++;
				}
				r += 3;
//					mySheet.addCell(sheetData, 2, r, R.getResource("report.equip.gdtnkd"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
//					mySheet.addCell(sheetData, 11, r++, R.getResource("report.equip.nguoi.lap.bao.cao"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

				String fileName = R.getResource("report.equip.3.1.1.5.bao.mat.sheet.label"); // ten file: bao_mat_ky
				fileName = getReportNameFormat(fileName, "", "", FileExtension.XLSX.getValue());
				outputPath = SXSSFReportHelper.exportXLSX(workbook, null, null, fileName);
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
					
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputPath);
				/** begin Bo sung ATTT */
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, false);
				result.put("hasData", false);
			}
			lstData = null;
		} catch (Exception e) {
//			System.out.print(e.getMessage());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportBCMATTU1_5"), createLogErrorStandard(actionStartTime));
			if (workbook != null) {
				workbook.dispose();
			}
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao 3.1.1.4
	 * 
	 * @author Datpv4
	 * @return file XLS | PDF
	 * @throws IOException
	 */
	public String exportBCPSCCHT() throws IOException {
		actionStartTime = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		Workbook resultWorkbook = null;
		try {
			result.put(ERROR, true);
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null || currentUser.getShopRoot() == null) {
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(lstShopId)) {
				result.put(ERR_MSG,  R.getResource("report.action.no.allow.shop.null"));
				return JSON;
			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (fDate == null) {
				result.put(ERR_MSG, R.getResource("msg.common.err.1"));
				return JSON;
			}
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (tDate == null) {
				result.put(ERR_MSG, R.getResource("msg.common.err.2"));
				return JSON;
			}
			Long staffRootId = currentUser.getStaffRoot().getStaffId();
			Long roleId = currentUser.getRoleToken().getRoleId();
			Long shopRootId = currentUser.getShopRoot().getShopId();
			List<Rpt_3_1_1_4_SC1_1VODetail> lstData = hoReportMgr.exportBCPSCCTH(staffRootId, roleId, shopRootId, lstShopId, groupMultil, categoryIdMultil, fDate, tDate);

			if (lstData == null || lstData.isEmpty()) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("lstData", lstData);
			beans.put("shopCurrent", currentUser.getShopRoot().getShopCode() + " - " + currentUser.getShopRoot().getShopName());
			beans.put("fromDate", fromDate);
			beans.put("toDate", toDate);
			beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
			String outputPath = "";
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathEquipment(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.TEMPLATE_EQUIP_BCPSCCTH_EXPORT;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = ConstantManager.EXPORT_BCPSCCTH + genFileSuffix() + FileExtension.XLSX.getValue();
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			resultWorkbook = transformer.transformXLS(inputStream, beans);
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			result.put("hasData", true);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportBCPSCCHT()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (os != null) {
				os.close();
			}
			if (resultWorkbook != null) {
				resultWorkbook.close();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao 3.1.1.6: Báo cáo Thông tin mượn thiết bị
	 * 
	 * @author vuongmq
	 * @since May 13, 2015
	 * @return file XLS | PDF
	 * @throws IOException
	 */
	public String exportBCDSMTDTMTK() throws IOException {
		try {
			actionStartTime = DateUtil.now();
			parametersReport = new HashMap<String, Object>();
			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			if (!StringUtil.isNullOrEmpty(lstShopId)) {
				shopId = Long.valueOf(lstShopId);
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			}
			Shop shop = shopMgr.getShopById(shopId);
			if (shop == null) {
				result.put(ERR_MSG, R.getResource("catalog.unit.tree.not.exist"));
				return JSON;
			}
			shopName = shop.getShopCode() + " - " + shop.getShopName();
			
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (fDate == null) {
				result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.fromdate")));
				return JSON;
			}
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (tDate == null) {
				result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.todate")));
				return JSON;
			}
			if (DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
				result.put(ERR_MSG, R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate")));
				return JSON;
			}
			String msgDate = checkMaxNumberDayReportLong(fDate, tDate);
			if (!StringUtil.isNullOrEmpty(msgDate)) {
				result.put(ERR_MSG, msgDate);
				return JSON;
			}
			
			Long staffRootId = currentUser.getStaffRoot().getStaffId();
			Long roleId = currentUser.getRoleToken().getRoleId();
			Long shopRootId = currentUser.getShopRoot().getShopId();
			List<Rpt_3116_WVKD_03F5SC1_1VO> lstBCData = hoReportMgr.getRptBCDSMTDTMTK(staffRootId, roleId, shopRootId, lstShopId, lstEquipId, categoryIdMultil, fDate, tDate);				
			if (lstBCData == null || lstBCData.isEmpty()) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			parametersReport = new HashMap<String, Object>();
			String outputPath = "";
			
			FileOutputStream out = null;
			SXSSFWorkbook workbook = null;
			//Init XSSF workboook
			String outputName = ConstantManager.EQUIP_REPORT_BSDSMTTK + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			workbook = new SXSSFWorkbook(-1);
			workbook.setCompressTempFiles(true);

			//Tao shett
			SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.sheetname"));
			Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook); 
			
			//Set Getting Defaul
			sheetData.setDefaultRowHeight((short) (15 * 20));
			sheetData.setDefaultColumnWidth(13);

			//set static Column width
			ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 
														50, 90, 90, 90, 120, 
														90, 90, 90, 90, 90, 
														120, 90, 90, 150, 150, 
														150, 90, 90, 90, 90, 
														90, 80, 80, 80, 150);

			//Size Row
			ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 25, 15, 20, 15, 25, 25);

			sheetData.setDisplayGridlines(false);

			int rowId = 0;
			int colId = -1;
			
//				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, rowId, 23, rowId, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.common.company.name"), style.get(ExcelPOIProcessUtils.BOLD));
//				rowId = rowId +1;
//				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, rowId, 23, rowId,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.sheet.label2"), style.get(ExcelPOIProcessUtils.BOLD));
//				rowId = rowId + 3;
			//Tittle
			ExcelPOIProcessUtils.addCell(sheetData, 8, rowId,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.title"),style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
			
			rowId += 2;
			ExcelPOIProcessUtils.addCell(sheetData, 3, rowId, R.getResource("common.shop.name.lable"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
			ExcelPOIProcessUtils.addCell(sheetData, 4, rowId, shopName, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
			
			rowId++;
			ExcelPOIProcessUtils.addCell(sheetData, 3, rowId, R.getResource("common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
			ExcelPOIProcessUtils.addCell(sheetData, 4, rowId, fromDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
			ExcelPOIProcessUtils.addCell(sheetData, 5, rowId, R.getResource("common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
			ExcelPOIProcessUtils.addCell(sheetData, 6, rowId, toDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
			ExcelPOIProcessUtils.addCell(sheetData, 7, rowId, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
			ExcelPOIProcessUtils.addCell(sheetData, 8, rowId, DateUtil.toDateString(actionStartTime, DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL_CENTER_LEFT));
			
			rowId += 2;

			//header
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
//			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 2, rowId, 5, rowId,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu2") , style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
//			rowId += 1;
//			colId += 4;
			
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu4.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu4"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu5"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu6"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, colId + 2, rowId, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.header.contract") , style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 3, rowId, colId + 4, rowId,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.header.equip_delivery_record") , style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			
			ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.number"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.date"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.number"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.date"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu7"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu8"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu15"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu15.1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu11") , style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu11.1") , style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu12"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.header.createDate"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu10"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu13") , style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu14") , style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+1, rowId,++colId, rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.header.equipmentdateUse"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
			
			//Danh sach Noi Dung Muon Tu
			List<ApParamEquip> lstDeliveryContent = apParamEquipMgr.getListApParamEquipByType(ApParamEquipType.DELIVERY_CONTENT, null);
			int lstContentSize = lstDeliveryContent.size();
			if (lstContentSize > 0) {
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + 1, rowId, colId + lstContentSize, rowId , Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.3116.header.content"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				for (int i = 0; i < lstContentSize; i++) {
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId + i + 1, rowId + 1, colId + i + 1, rowId + 1, lstDeliveryContent.get(i).getValue(), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				}
			}
			ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colId+lstContentSize+1, rowId,colId+lstContentSize+1 ,rowId + 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.equip.header.menu18"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
		
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 0, 50);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 2, 105);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 3, 85);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 4, 105);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 5, 85);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 9, 180);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 10, 95);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 12, 85);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 20, 200);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 15, 105);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 19, 105);
//			ExcelPOIProcessUtils.setColumnWidth(sheetData, 20, 100);
			
			//Xu ly do du lieu Detail
			rowId += 2;
			
			for (int i = 0, size1 = lstBCData.size(); i < size1; i++) {
				Rpt_3116_WVKD_03F5SC1_1VO row = lstBCData.get(i);
				colId = -1;
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getManpp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getTennpp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getSohd() , style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getNgayhd(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getSobbgn(),style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getNgaybbgn(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getMakh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getTenkh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getMataisan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getSoserial(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getLoaitu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getNhomtu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getHieutu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getNgaymuon(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getSoluong(), style.get(ExcelPOIProcessUtils.ROW_RIGHT));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getDungtich(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getNamsx(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, ++colId, rowId, row.getNgaysudung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				if(lstContentSize>0){
					for(int j=0;j<lstContentSize;j++){
						if(row.getNoidungmuontu()!=null && row.getNoidungmuontu().equals(lstDeliveryContent.get(j).getApParamEquipCode())){
							ExcelPOIProcessUtils.addCell(sheetData, colId+j+1, rowId, "X", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						} else{
							ExcelPOIProcessUtils.addCell(sheetData, colId+j+1, rowId, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						}
					}
				}
				
				ExcelPOIProcessUtils.addCell(sheetData, colId+lstContentSize+1,rowId, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				rowId++;
			}
			out = new FileOutputStream(exportFileName);
			workbook.write(out);
			outputPath = Configuration.getExportExcelPath() + outputName;
				
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			result.put("hasData", true);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportBCDSMTDTMTK"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return JSON;
	}
//
//	
//	/**
//	 * Bao cao nhap xuat ton tu mat, tu dong ky
//	 * 
//	 * @author hunglm16
//	 * @since May 16, 2015
//	 * */
//	public String reportXNT1Dot1Equip() {
//		/**
//		 * @param flagSys -> 1: Ky o thoi diem hien tai; 0: Qua khu; 2: Tuong lai
//		 * 
//		 * */
//		//parametersReport = new HashMap<String, Object>();
//		InputStream inputStream = null;
//		OutputStream os = null;
//		String outputPath = "";
//		try {
//			//Xu ly Validate
//			String msg = "";
//			int flagSys = 1;
//			EquipPeriod equipPeriod = null;
//			if (currentUser == null) {
//				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
//			}
//			/** Bo sung ATTT */
//			String reportToken = retrieveReportToken(reportCode);
//			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token");
//			}
//			if (StringUtil.isNullOrEmpty(msg) && (equipPeriodId == null || equipPeriodId < 0)) {
//				msg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "xnt.1.1.equip.err.priod.isnull");
//			}
//			if (StringUtil.isNullOrEmpty(msg)) {
//				equipPeriod = commonMgr.getEntityById(EquipPeriod.class, equipPeriodId);
//				if (equipPeriod == null) {
//					msg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "xnt.1.1.equip.err.priod.isnull");					
//				} else if (equipPeriod.getStatus() == null || (!EquipPeriodType.OPENED.getValue().equals(equipPeriod.getStatus().getValue())) && !EquipPeriodType.CLOSED.getValue().equals(equipPeriod.getStatus().getValue())) {
//					msg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "xnt.1.1.equip.err.priod.exits.not");
//				} else {
//					if (equipPeriod.getFromDate() == null || equipPeriod.getToDate() == null || !DateUtil.compareDate(equipPeriod.getFromDate(), equipPeriod.getToDate())) {
//						msg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "xnt.1.1.equip.err.priod.exits.not");
//					} else if (DateUtil.compareDate(equipPeriod.getToDate(), commonMgr.getSysDate())) {
//						flagSys = 0;
//					}
//				}
//			}
//			if (!StringUtil.isNullOrEmpty(msg)) {
//				result.put(ERROR, true);
//				result.put("errMsg", msg);
//				return JSON;
//			}
//			//Xu ly goi Core lay du lieu
//			List<Rpt_XNT1_1_EquipVO> lstData = hoReportMgr.rptXNT1Dot1Equip(stockMultil, groupMultil,  categoryIdMultil, equipPeriodId, currentUser.getStaffRoot().getStaffId(), currentUser.getShopRoot().getShopId(), flagSys);
//
//			if (lstData != null && !lstData.isEmpty()) {
//				/*if ("XLS".equals(formatType)) {
//				} else {
//					JRDataSource dataSource = new JRBeanCollectionDataSource(lstData);
//					outputPath = ReportUtils.exportFromFormat(FileExtension.PDF, parametersReport, dataSource, ShopReportTemplate.RPORT_EQUIP_XNT1_1);
//					result.put(ERROR, false);
//					result.put("hasData", true);
//				}*/
//				//Xu ly xuat file excel
//				Map<String, Object> beans = new HashMap<String, Object>();
//				beans.put("lstData", lstData);
//				if (!StringUtil.isNullOrEmpty(equipPeriod.getName())) {
//					beans.put("tenKy", equipPeriod.getName().trim());					
//				} else {
//					beans.put("tenKy", "");
//				}
//				String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/equip-report/";
//				String templateFileName = folder + ConstantManager.RPT_EQUIP_XNT1_1;
//				templateFileName = templateFileName.replace('/', File.separatorChar);
//				
//				String outputName = ConstantManager.RPT_EQUIP_XNT1_1_Name_File + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
//				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
//				
//				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
//				XLSTransformer transformer = new XLSTransformer();
//				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
//				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
//				resultWorkbook.write(os);
//				os.flush();
//				outputPath = Configuration.getExportExcelPath() + outputName;
//				result.put(ERROR, false);
//				result.put("path", outputPath);
//				//Bo sung ATTT
//				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null"));
//			}
//			//Xoa cac du lieu tham bien
//			inputStream = null;
//			os = null;     
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//			result.put(ERROR, true);
//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//		} finally {
//			if (inputStream != null) {
//				IOUtils.closeQuietly(inputStream);
//			}
//			if (os != null) {
//				IOUtils.closeQuietly(os);
//			}
//		}
//		return JSON;
//	}
//	
//	/**
//	 * Bao cao nhap xuat ton tu mat, tu dong ky
//	 * 
//	 * @author hunglm16
//	 * @since May 16, 2015
//	 * */
//	public String reportXNT1Dot2Equip() {
//		/**
//		 * @param flagSys -> 1: Ky o thoi diem hien tai; 0: Qua khu; 2: Tuong lai
//		 * 
//		 * */
//		InputStream inputStream = null;
//		OutputStream os = null;
//		String outputPath = "";
//		try {
//			//Xu ly Validate
//			String msg = "";
//			int flagSys = 1;
//			EquipPeriod equipPeriod = null;
//			if (currentUser == null) {
//				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
//			}
//			/** Bo sung ATTT */
//			String reportToken = retrieveReportToken(reportCode);
//			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token");
//			}
//			if (StringUtil.isNullOrEmpty(msg) && (equipPeriodId == null || equipPeriodId < 0)) {
//				msg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "xnt.1.2.equip.err.priod.isnull");
//			}
//			if (StringUtil.isNullOrEmpty(msg)) {
//				equipPeriod = commonMgr.getEntityById(EquipPeriod.class, equipPeriodId);
//				if (equipPeriod == null) {
//					msg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "xnt.1.2.equip.err.priod.isnull");					
//				} else if (equipPeriod.getStatus() == null || (!EquipPeriodType.OPENED.getValue().equals(equipPeriod.getStatus().getValue())) && !EquipPeriodType.CLOSED.getValue().equals(equipPeriod.getStatus().getValue())) {
//					msg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "xnt.1.2.equip.err.priod.exits.not");
//				} else {
//					if (equipPeriod.getFromDate() == null || equipPeriod.getToDate() == null || !DateUtil.compareDate(equipPeriod.getFromDate(), equipPeriod.getToDate())) {
//						msg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "xnt.1.2.equip.err.priod.exits.not");
//					} else if (DateUtil.compareDate(equipPeriod.getToDate(), commonMgr.getSysDate())) {
//						flagSys = 0;
//					}
//				}
//			}
//			if (!StringUtil.isNullOrEmpty(msg)) {
//				result.put(ERROR, true);
//				result.put("errMsg", msg);
//				return JSON;
//			}
//			if (status == null) {
//				status = ALL_INT;
//			}
//			//Xu ly goi Core lay du lieu
//			List<Rpt_XNT1_2_EquipVO> lstData = hoReportMgr.rptXNT1Dot2Equip(stockMultil, groupMultil, categoryIdMultil, equipPeriodId, status, currentUser.getStaffRoot().getStaffId(), currentUser.getShopRoot().getShopId(), flagSys);
//
//			if (lstData != null && !lstData.isEmpty()) {
//				//Xu ly xuat file excel
//				Map<String, Object> beans = new HashMap<String, Object>();
//				beans.put("lstData", lstData);
//				if (!StringUtil.isNullOrEmpty(equipPeriod.getName())) {
//					beans.put("tenKy", equipPeriod.getName().trim());					
//				} else {
//					beans.put("tenKy", "");
//				}
//				String folder = ServletActionContext.getServletContext().getRealPath("/") + "/resources/templates/equipment/equip-report/";
//				String templateFileName = folder + ConstantManager.RPT_EQUIP_XNT1_2;
//				templateFileName = templateFileName.replace('/', File.separatorChar);
//				
//				String outputName = ConstantManager.RPT_EQUIP_XNT1_2_Name_File + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
//				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
//				
//				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
//				XLSTransformer transformer = new XLSTransformer();
//				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
//				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
//				resultWorkbook.write(os);
//				os.flush();
//				outputPath = Configuration.getExportExcelPath() + outputName;
//				result.put(ERROR, false);
//				result.put("path", outputPath);
//				//Bo sung ATTT
//				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null"));
//			}
//			//Xoa cac du lieu tham bien
//			inputStream = null;
//			os = null;     
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//			result.put(ERROR, true);
//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//		} finally {
//			if (inputStream != null) {
//				IOUtils.closeQuietly(inputStream);
//			}
//			if (os != null) {
//				IOUtils.closeQuietly(os);
//			}
//		}
//		return JSON;
//	}
//	
//	/**
//	 * kiem tra list shop co thuoc danh sach shop cua user dang nhap
//	 * 
//	 * @author tamvnm
//	 * @param lstShopId - chuoi shop id cach nhau ','
//	 * @return thong bao loi
//	 * @since Nov 12 2015
//	 */
//	private String checkShopInOrgAccess(String lstShopId) {
//		String msg = "";
//		if (StringUtil.isNullOrEmpty(lstShopId)) {
//			msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "com.list.shop.id.null");
//		} else {
//			String [] lstShop = lstShopId.split(DELIMITER);
//			
//			if (lstShop != null && lstShop.length > 0) {
//				for (int i = 0, isize = lstShop.length; i < isize; i++) {
//					Long shopId = Long.parseLong(lstShop[i]);
//					if (!checkShopInOrgAccessById(shopId)) {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "com.shop.not.org.access");
//						break;
//					}
//				}
//			}
//		}
//		return msg;
//	}
//	
//	/**
//	 * GD 1.1 Bao cao cac giao dich phat sinh
//	 * 
//	 * @author hunglm16
//	 * @since August 24, 2015
//	 * */
//	public String reportGD1D1BCCGDPS() {
//		try {
//			//Xu ly Validate
//			String msg = "";
//			if (currentUser == null) {
//				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
//			}
//			/** Bo sung ATTT */
//			String reportToken = retrieveReportToken(reportCode);
//			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
//				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token");
//			}
//			
//			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_STR);
//			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_STR);
//			Date nowDate = DateUtil.now();
//			if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
//				fDate = nowDate;
//			}
//			if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
//				tDate = nowDate;
//			}
//			if (typeGG == null) {
//				status = ALL_INT;
//			}
//			if (StringUtil.isNullOrEmpty(lstShopId)) {
//				lstShopId = currentUser.getShopRoot().getShopId().toString();
//			} else if (StringUtil.isNullOrEmpty(msg)) {
//				msg = checkShopInOrgAccess(lstShopId);
//			}
//			
//			if (!StringUtil.isNullOrEmpty(msg)) {
//				result.put(ERROR, true);
//				result.put("errMsg", msg);
//				return JSON;
//			}
//			
//			EquipmentReportMgr equipReportMgr = (EquipmentReportMgr) context.getBean("equipmentReportMgr");
//			if (equipReportMgr == null) {
//				result.put(ERROR, true);
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//				return JSON;
//			}
//			String outputPath = equipReportMgr.exportGD1_1(lstShopId, typeGG, fDate, tDate, null);
//
//			if (outputPath == null) {
//				result.put(ERROR, false);
//				result.put("hasData", false);
//				return JSON;
//			}
//
//			result.put(REPORT_PATH, outputPath);
//			result.put(LIST, outputPath);
//			result.put(ERROR, false);
//			result.put("hasData", true);
//			//Bo sung ATTT
//			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.reportGD1D1BCCGDPS"), createLogErrorStandard(actionStartTime));
//			result.put(ERROR, true);
//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//		}
//		return JSON;
//	}
//	
//	/**
//	 * Bao cao phat sinh doanh so tu mat tu dong ds1.1
//	 * 
//	 * @author phuongvm
//	 * @return File XLSX
//	 * @throws Exception
//	 * @since 16/12/2015
//	 */
//	public String exportDS1_1() throws Exception {
//		InputStream inputStream = null;
//		OutputStream os = null;
//		String pathTemplate = Configuration.getTemplatePathEquipmentReport();
//		String outputPath = "";
//		try {
//			/** Bo sung ATTT */
//			String reportToken = retrieveReportToken(reportCode);
//			if (StringUtil.isNullOrEmpty(reportToken)) {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
//				return JSON;
//			}
//			errMsg = "";
//			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_STR);
//			Date sysDate = commonMgr.getSysDate();
//			Shop shop = null;
//
//			if (lstShopId != null && lstShopId.trim().length() > 0) {
//				final Boolean isIncludeStoppedShop = true;
//				for (String str : lstShopId.split(DELIMITER)) {
//					shop = shopMgr.getShopById(new Long(str));
//					if (shop == null) {
//						result.put(ERROR, true);
//						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "catalog.unit.tree");
//						return JSON;
//					} else if (!checkShopInOrgAccessNotShopRootById(shop.getId(), isIncludeStoppedShop)) {
//						result.put(ERROR, true);
//						result.put(errMsg, ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, false, "catalog.unit.tree"));
//						return JSON;
//					}
//				}
//			} else {
//				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_REQUIRE, false, "catalog.unit.tree");
//			}
//
//			if (StringUtil.isNullOrEmpty(toDate) && errMsg.length() == 0) {
//				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.toDate");
//			}
//			if (errMsg.length() == 0 && DateUtil.compareDateWithoutTime(tDate, sysDate) > -1) {
//				errMsg = R.getResource("ss.assign.route.set.order.error.todate.less.curentdate");
//			}
//			if (errMsg.length() > 0) {
//				result.put(ERROR, true);
//				result.put("errMsg", errMsg);
//				return JSON;
//			}
//			
//			List<RptPSDS1_1Sheet1VO> lstDataSheet1 = hoReportMgr.getBCDS1_1Sheet1(lstShopId, tDate);
//			List<RptPSDS1_1Sheet2VO> lstDataSheet2 = hoReportMgr.getBCDS1_1Sheet2(lstShopId, tDate);
//
//			if ((lstDataSheet1 != null && !lstDataSheet1.isEmpty()) || (lstDataSheet2 != null && !lstDataSheet2.isEmpty())) {
//				//Xu ly xuat file excel
//				Map<String, Object> beans = new HashMap<String, Object>();
//				beans.put("lstData", lstDataSheet1);
//				beans.put("lstDataEx", lstDataSheet2);
//				String folder = ServletActionContext.getServletContext().getRealPath("/") + pathTemplate;
//				String templateFileName = folder + ConstantManager.RPT_EQUIP_DS1_1;
//				templateFileName = templateFileName.replace('/', File.separatorChar);
//				
//				String outputName = ConstantManager.RPT_EQUIP_DS1_1_NAME_FILE + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
//				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
//				
//				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
//				XLSTransformer transformer = new XLSTransformer();
//				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
//				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
//				resultWorkbook.write(os);
//				os.flush();
//				outputPath = Configuration.getExportExcelPath() + outputName;
//				result.put(ERROR, false);
//				result.put("path", outputPath);
//				//Bo sung ATTT
//				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
//			} else {
//				result.put(ERROR, true);
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null"));
//			}
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportDS1_1"), createLogErrorStandard(actionStartTime));
//			result.put(ERROR, true);
//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//		} finally {
//			if (inputStream != null) {
//				IOUtils.closeQuietly(inputStream);
//			}
//			if (os != null) {
//				IOUtils.closeQuietly(os);
//			}
//		}
//		return JSON;
//	}
//	
//	/**
//	 * Ham dung de gom nhom trong excel
//	 * 
//	 * @author hunglm16
//	 * @since August 24, 2014
//	 * */
//	public void count4Group(int initParam) {
//		if (startNpp == -1 && startVung == -1 && startMien == -1 && startOther == -1) {
//			startNpp = initParam;
//			startVung = initParam;
//			startMien = initParam;
//			startOther = initParam;
//			start4 = initParam;
//			start5 = initParam;
//			start6 = initParam;
//			start7 = initParam;
//			start8 = initParam;
//			countNpp = 0;
//			countVung = 0;
//			countMien = 0;
//			countOther = 0;
//			count4 = 0;
//			count5 = 0;
//			count6 = 0;
//			count7 = 0;
//			count8 = 0;
//		}
//		/**
//		 * 
//		 */
//		countNpp++;
//		countVung++;
//		countMien++;
//		countOther++;
//		count4++;
//		count5++;
//		count6++;
//		count7++;
//		count8++;
//	}
//	
//	/**
//	 * Ham dung de gom nhom trong excel
//	 * 
//	 * @author hunglm16
//	 * @since August 24, 2014
//	 * */
//	public void main4GroupPOI(SXSSFSheet sheet, int type) {
//		/**
//		 * type = 1: Group Npp type = 2: Group Vung type = 3: Group Mien
//		 */
//		try {
//			switch (type) {
//			case 0: {
//				endOther = startOther + countOther - 1;
//				sheet.groupRow(startOther, endOther - 1);
//				startOther = endOther + 1;
//				countOther = 0;
//			}
//				break;
//			case 1: {
//				endNpp = startNpp + countNpp - 1;
//				sheet.groupRow(startNpp, endNpp - 1);
//				startNpp = endNpp + 1;
//				countNpp = 0;
//				countOther = 0;
//				startOther++;
//			}
//				break;
//			case 2: {
//				endVung = startVung + countVung - 1;
//				sheet.groupRow(startVung, endVung - 1);
//				startVung = endVung + 1;
//				countNpp = 0;
//				countVung = 0;
//				startNpp++;
//				countOther = 0;
//				startOther++;
//			}
//				break;
//			case 3: {
//				endMien = startMien + countMien - 1;
//				sheet.groupRow(startMien, endMien - 1);
//				startMien = endMien + 1;
//				countOther = 0;
//				countNpp = 0;
//				countVung = 0;
//				countMien = 0;
//				startOther++;
//				startVung++;
//				startNpp++;
//			}
//				break;
//			case 4: {
//				end4 = start4 + count4 - 1;
//				sheet.groupRow(start4, end4 - 1);
//				start4 = end4 + 1;
//				countOther = 0;
//				countNpp = 0;
//				countVung = 0;
//				countMien = 0;
//				count4 = 0;
//				startOther++;
//				startVung++;
//				startNpp++;
//				startMien++;
//			}
//				break;
//			case 5: {
//				end5 = start5 + count5 - 1;
//				sheet.groupRow(start5, end5 - 1);
//				start5 = end5 + 1;
//				countOther = 0;
//				countNpp = 0;
//				countVung = 0;
//				countMien = 0;
//				count4 = 0;
//				count5 = 0;
//				startOther++;
//				startVung++;
//				startNpp++;
//				startMien++;
//				start4++;
//			}
//				break;
//			case 6: {
//				end6 = start6 + count6 - 1;
//				sheet.groupRow(start6, end6 - 1);
//				start6 = end6 + 1;
//				countOther = 0;
//				countNpp = 0;
//				countVung = 0;
//				countMien = 0;
//				count4 = 0;
//				count5 = 0;
//				count6 = 0;
//				startOther++;
//				startVung++;
//				startNpp++;
//				startMien++;
//				start4++;
//				start5++;
//			}
//				break;
//			case 7: {
//				end7 = start7 + count7 - 1;
//				sheet.groupRow(start7, end7 - 1);
//				start7 = end7 + 1;
//				countOther = 0;
//				countNpp = 0;
//				countVung = 0;
//				countMien = 0;
//				count4 = 0;
//				count5 = 0;
//				count6 = 0;
//				count7 = 0;
//				startOther++;
//				startVung++;
//				startNpp++;
//				startMien++;
//				start4++;
//				start5++;
//				start6++;
//			}
//				break;
//			case 8: {
//				end8 = start8 + count8 - 1;
//				sheet.groupRow(start8, end8 - 1);
//				start8 = end8 + 1;
//				countOther = 0;
//				countNpp = 0;
//				countVung = 0;
//				countMien = 0;
//				count4 = 0;
//				count5 = 0;
//				count6 = 0;
//				count7 = 0;
//				count8 = 0;
//				startOther++;
//				startVung++;
//				startNpp++;
//				startMien++;
//				start4++;
//				start5++;
//				start6++;
//				start7++;
//			}
//				break;
//			}
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
//	}
	
	/**
	 * Xu ly exportKKTB11
	 * @author vuongmq
	 * @return String
	 * @since Mar 29, 2016
	 */
	public String exportKKTB11() {
		actionStartTime = DateUtil.now();
		try {
			result.put(ERROR, true);
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null || currentUser.getShopRoot() == null) {
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			/** Bo sung ATTT */
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(lstShopId)) {
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.shop.null"));
				return JSON;
			}
			if (!ValidateUtil.validateNumber(lstShopId)) {
				result.put(ERR_MSG, R.getResource("common.possitive.integer", lstShopId));
				return JSON;
			}
			Long shopId = Long.valueOf(lstShopId);
			if (!checkShopInOrgAccessById(shopId)) {
				result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
				return JSON;
			}
			EquipStatisticRecord equipStatisticRecord = equipRecordMgr.getEquipStatisticRecordByCode(statisticCode);
			if (equipStatisticRecord != null) {
				if (!EquipmentStatisticRecordStatus.RUNNING.getValue().equals(equipStatisticRecord.getRecordStatus()) && !EquipmentStatisticRecordStatus.DONE.getValue().equals(equipStatisticRecord.getRecordStatus())) {
					result.put(ERR_MSG, R.getResource("equip.statistic.program.running.done.err"));
					return JSON;
				}
			} else {
				result.put(ERR_MSG, R.getResource("equip.statistic.program.err"));
				return JSON;
			}
			Long staffRootId = currentUser.getStaffRoot().getStaffId();
			Long roleId = currentUser.getRoleToken().getRoleId();
			Long shopRootId = currentUser.getShopRoot().getShopId();
			List<RptKK1_1VO> lstData = hoReportMgr.getExportKKTB11(staffRootId, roleId, shopRootId, lstShopId, equipStatisticRecord.getId(), null, null, null);
			if (lstData != null && lstData.size() > 0) {
				String outputPath = "";
				SXSSFWorkbook workbook = null;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("equip.report.kk11.sheet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				sheetData.setDisplayGridlines(false);
				sheetData.setDefaultColumnWidth(18);
				ExcelPOIProcessUtils.setRowHeight(sheetData, 0, 25);
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 120, 200, 120, 200, 140, 200, 200, 120, 120, 200, 120, 200);
				//outputPath = this.createExcelWVKD03F11(lstData, reportDate);
				ExcelPOIProcessUtils.addCell(sheetData, 5, 0, R.getResource("equip.report.kk11.header"), style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
				//header
				Date sysDate = commonMgr.getSysDate();
				Shop shop = shopMgr.getShopById(shopId);
				String typeStr = null;
				String statisticStr = null;
				if (equipStatisticRecord != null) {
					statisticStr = equipStatisticRecord.getCode() + " - " + equipStatisticRecord.getName();
					if (EquipObjectType.ALL.getValue().equals(equipStatisticRecord.getType())) {
						typeStr = R.getResource("equip.statistic.type.all");
					} else if (EquipObjectType.GROUP.getValue().equals(equipStatisticRecord.getType())) {
						typeStr = R.getResource("equip.statistic.type.group");
					} else if (EquipObjectType.EQUIP.getValue().equals(equipStatisticRecord.getType())) {
						typeStr = R.getResource("equip.statistic.type.list");
					}
				}
				ExcelPOIProcessUtils.addCell(sheetData, 1, 1, R.getResource("common.unit"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 1, (shop != null) ? shop.getShopCode() + " - " + shop.getShopName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 1, R.getResource("equip.report.kk11.chuong.trinh"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 1, statisticStr, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 1, R.getResource("equip.report.kk11.loai"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 1, typeStr, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 1, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 1, DateUtil.toDateString(sysDate, DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 3, 4, 3, R.getResource("equip.report.kk11.group1"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 5, 3, 16, 3, R.getResource("equip.report.kk11.group2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				int rowStartHeader = 4;
				ExcelPOIProcessUtils.setRowHeight(sheetData, rowStartHeader, 25);
				for (int i = 0; i < 17; i++) {
					ExcelPOIProcessUtils.addCell(sheetData, i, rowStartHeader, R.getResource("equip.report.kk11.menu" + i), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				}
				int d = 5;
				for (int i = 0, size = lstData.size(); i < size; i++) {
					RptKK1_1VO item = lstData.get(i);
					ExcelPOIProcessUtils.addCell(sheetData, 0, d, (i + 1), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, 1, d, item.getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 2, d, item.getShopName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 3, d, item.getShortCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 4, d, item.getCustomerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, d, item.getUserStatisticCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 6, d, item.getUserStatisticName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 7, d, item.getEquipCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 8, d, item.getSerial(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 9, d, item.getEquipGroupCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 10, d, item.getEquipGroupName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 11, d, item.getCategoryCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 12, d, item.getCategoryName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 13, d, item.getNumberStatistic(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					ExcelPOIProcessUtils.addCell(sheetData, 14, d, item.getStatisticDateEnd(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					ExcelPOIProcessUtils.addCell(sheetData, 15, d, item.getNumberTotalImg(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					ExcelPOIProcessUtils.addCell(sheetData, 16, d, item.getStatusCurrent(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					d++;
				}
				outputPath = SXSSFReportHelper.exportXLSX(workbook, null, "equip.report.kk11.file.name");
				result.put(ERROR, false);
				result.put("hasData", true);
				result.put(REPORT_PATH, outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, false);
				result.put("hasData", false);
			}
			lstData = null;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.equipment.EquipmentReportAction.exportKKTB11()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	private Integer status;
	
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getNPP() {
		return NPP;
	}

	public void setNPP(String nPP) {
		NPP = nPP;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getStrStatus() {
		return strStatus;
	}

	public void setStrStatus(String strStatus) {
		this.strStatus = strStatus;
	}

	public List<ApParamEquip> getLstReasonEviction() {
		return lstReasonEviction;
	}

	public void setLstReasonEviction(List<ApParamEquip> lstReasonEviction) {
		this.lstReasonEviction = lstReasonEviction;
	}

	public String getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(String lstShopId) {
		this.lstShopId = lstShopId;
	}

	public String getLstEquipId() {
		return lstEquipId;
	}

	public void setLstEquipId(String lstEquipId) {
		this.lstEquipId = lstEquipId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public List<EquipmentVO> getListEquipCategory() {
		return listEquipCategory;
	}

	public void setListEquipCategory(List<EquipmentVO> listEquipCategory) {
		this.listEquipCategory = listEquipCategory;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<EquipPeriod> getListEquipPeriod() {
		return listEquipPeriod;
	}

	public void setListEquipPeriod(List<EquipPeriod> listEquipPeriod) {
		this.listEquipPeriod = listEquipPeriod;
	}

	public Integer getYear1() {
		return year1;
	}

	public void setYear1(Integer year1) {
		this.year1 = year1;
	}

	public Integer getYear2() {
		return year2;
	}

	public void setYear2(Integer year2) {
		this.year2 = year2;
	}

	public Integer getFromPeriodId() {
		return fromPeriodId;
	}

	public void setFromPeriodId(Integer fromPeriodId) {
		this.fromPeriodId = fromPeriodId;
	}

	public Integer getToPeriodId() {
		return toPeriodId;
	}

	public void setToPeriodId(Integer toPeriodId) {
		this.toPeriodId = toPeriodId;
	}

	public List<EquipmentVO> getListVO() {
		return listVO;
	}

	public void setListVO(List<EquipmentVO> listVO) {
		this.listVO = listVO;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public Long getEquipPeriodId() {
		return equipPeriodId;
	}

	public void setEquipPeriodId(Long equipPeriodId) {
		this.equipPeriodId = equipPeriodId;
	}

	public String getStockMultil() {
		return stockMultil;
	}

	public void setStockMultil(String stockMultil) {
		this.stockMultil = stockMultil;
	}

	public String getGroupMultil() {
		return groupMultil;
	}

	public void setGroupMultil(String groupMultil) {
		this.groupMultil = groupMultil;
	}

	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}

	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	public Long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	public Long getStatisticRecord() {
		return statisticRecord;
	}

	public void setStatisticRecord(Long statisticRecord) {
		this.statisticRecord = statisticRecord;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTypeGG() {
		return typeGG;
	}

	public void setTypeGG(Integer typeGG) {
		this.typeGG = typeGG;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getCategoryIdMultil() {
		return categoryIdMultil;
	}

	public void setCategoryIdMultil(String categoryIdMultil) {
		this.categoryIdMultil = categoryIdMultil;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public String getLstCheck() {
		return lstCheck;
	}

	public void setLstCheck(String lstCheck) {
		this.lstCheck = lstCheck;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public int getEndNpp() {
		return endNpp;
	}

	public void setEndNpp(int endNpp) {
		this.endNpp = endNpp;
	}

	public int getStartVung() {
		return startVung;
	}

	public void setStartVung(int startVung) {
		this.startVung = startVung;
	}

	public int getEndVung() {
		return endVung;
	}

	public void setEndVung(int endVung) {
		this.endVung = endVung;
	}

	public int getStartMien() {
		return startMien;
	}

	public void setStartMien(int startMien) {
		this.startMien = startMien;
	}

	public int getEndMien() {
		return endMien;
	}

	public void setEndMien(int endMien) {
		this.endMien = endMien;
	}

	public int getCountNpp() {
		return countNpp;
	}

	public void setCountNpp(int countNpp) {
		this.countNpp = countNpp;
	}

	public int getCountVung() {
		return countVung;
	}

	public void setCountVung(int countVung) {
		this.countVung = countVung;
	}

	public int getCountMien() {
		return countMien;
	}

	public void setCountMien(int countMien) {
		this.countMien = countMien;
	}

	public int getCountOther() {
		return countOther;
	}

	public void setCountOther(int countOther) {
		this.countOther = countOther;
	}

	public int getStartOther() {
		return startOther;
	}

	public void setStartOther(int startOther) {
		this.startOther = startOther;
	}

	public int getEndOther() {
		return endOther;
	}

	public void setEndOther(int endOther) {
		this.endOther = endOther;
	}

	public int getStart4() {
		return start4;
	}

	public void setStart4(int start4) {
		this.start4 = start4;
	}

	public int getStart5() {
		return start5;
	}

	public void setStart5(int start5) {
		this.start5 = start5;
	}

	public int getStart6() {
		return start6;
	}

	public void setStart6(int start6) {
		this.start6 = start6;
	}

	public int getStart7() {
		return start7;
	}

	public void setStart7(int start7) {
		this.start7 = start7;
	}

	public int getStart8() {
		return start8;
	}

	public void setStart8(int start8) {
		this.start8 = start8;
	}

	public int getEnd4() {
		return end4;
	}

	public void setEnd4(int end4) {
		this.end4 = end4;
	}

	public int getEnd5() {
		return end5;
	}

	public void setEnd5(int end5) {
		this.end5 = end5;
	}

	public int getEnd6() {
		return end6;
	}

	public void setEnd6(int end6) {
		this.end6 = end6;
	}

	public int getEnd7() {
		return end7;
	}

	public void setEnd7(int end7) {
		this.end7 = end7;
	}

	public int getEnd8() {
		return end8;
	}

	public void setEnd8(int end8) {
		this.end8 = end8;
	}

	public int getCount4() {
		return count4;
	}

	public void setCount4(int count4) {
		this.count4 = count4;
	}

	public int getCount5() {
		return count5;
	}

	public void setCount5(int count5) {
		this.count5 = count5;
	}

	public int getCount6() {
		return count6;
	}

	public void setCount6(int count6) {
		this.count6 = count6;
	}

	public int getCount7() {
		return count7;
	}

	public void setCount7(int count7) {
		this.count7 = count7;
	}

	public int getCount8() {
		return count8;
	}

	public void setCount8(int count8) {
		this.count8 = count8;
	}

	public int getStartNpp() {
		return startNpp;
	}

	public void setStartNpp(int startNpp) {
		this.startNpp = startNpp;
	}

	public String getStatisticCode() {
		return statisticCode;
	}

	public void setStatisticCode(String statisticCode) {
		this.statisticCode = statisticCode;
	}

	public String getYesterdayStr() {
		return yesterdayStr;
	}

	public void setYesterdayStr(String yesterdayStr) {
		this.yesterdayStr = yesterdayStr;
	}
	
}
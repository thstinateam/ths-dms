package ths.dms.web.action.kpi;

import java.io.File;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
import ths.dms.core.business.SalePlanMgr;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.Kpi;
import ths.dms.core.entities.KpiImplement;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.PlanTypeKpi;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.DynamicStaffKpiSaveVO;
import ths.dms.core.entities.vo.DynamicStaffKpiVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.DynamicVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.filter.CommonFilter;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelProcessUtils;
import ths.dms.web.utils.report.excel.ExcelReporter;
import ths.dms.web.utils.report.excel.SpreadsheetWriter;

/**
 * Set kpi target for staff of shop
 * 
 * @author trungtm6
 * @since 08/07/2015
 */
public class KpiTargetAction extends AbstractAction {

	private Shop shop;

	private Long curShopId;
	private Integer yearPeriod;
	private Integer numPeriod;

	private File excelFile;
	private String excelFileContentType;

	List<Kpi> lstKpi;
	List<DynamicStaffKpiVO> lstKpiInfo;
	List<DynamicStaffKpiSaveVO> lstKpiSaveInfo;

	private List<Integer> lstYear;

	private List<Integer> lstPeriod;

	private List<CycleVO> lstCycleVo;

	private SalePlanMgr salePlanMgr;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		salePlanMgr = (SalePlanMgr) context.getBean("salePlanMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			lstYear = cycleMgr.getListYearOfAllCycle();
			Date curdate = commonMgr.getSysDate();
			yearPeriod = DateUtil.getYear(curdate);
			Cycle curCycle = cycleMgr.getCycleByDate(curdate);
			if (curCycle != null) {
				numPeriod = curCycle.getNum();
			} else {
				numPeriod = 1;
			}

			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			shopId = shop.getId();
			curShopId = shop.getId();
		} catch (BusinessException e) {
			LogUtility.logError(e, "KpiTargetAction.execute" + e.getMessage());
		}

		return SUCCESS;
	}

	/**
	 * Search dynamic KPI list
	 * 
	 * @author trungtm6
	 * @since 11/07/2015
	 * @return
	 */
	public String search() {
		try {
			if (shopCode != null && yearPeriod != null && numPeriod != null) {
				Shop sh = shopMgr.getShopByCode(shopCode);
				Cycle c = cycleMgr.getCycleByYearAndNum(yearPeriod, numPeriod);
				if (sh != null && c != null) {
					CommonFilter fi = new CommonFilter();
					fi.setPlanType(PlanTypeKpi.APPLY.getValue());
					fi.setStatus(ActiveType.RUNNING);
					ObjectVO<Kpi> vo = salePlanMgr.getListKpiByFilter(null, fi);
					lstKpi = vo.getLstObject();
					lstKpiInfo = salePlanMgr.getListDynamicStaffKpi(sh.getId(), c.getId());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "KpiTargetAction.search" + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Save dynamic KPI list
	 * 
	 * @author trungtm6
	 * @since 13/07/2015
	 * @return
	 */
	public String saveInfo() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			if (lstKpiSaveInfo != null && lstKpiSaveInfo.size() > 0) {
				DynamicStaffKpiSaveVO row = lstKpiSaveInfo.get(0);
				Date curdate = commonMgr.getSysDate();
				yearPeriod = DateUtil.getYear(curdate);
				Cycle curCycle = cycleMgr.getCycleByDate(curdate);
				if (row.getYear() < yearPeriod) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("kpi_year_must_greater_or_equal_sysyear"));
					return JSON;
				}
				if (row.getYear().equals(yearPeriod) && curCycle != null && row.getPeriod() != null && row.getPeriod().compareTo(curCycle.getNum()) < 0) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("kpi_period_must_greater_or_equal_curperiod"));
					return JSON;
				}
				salePlanMgr.createOrUpdateListKi(lstKpiSaveInfo, getLogInfoVO());
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, R.getResource("system.error"));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.KpiTargetAction.saveInfo()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * download template import
	 * 
	 * @author trungtm6
	 * @since 13/07/2015
	 * @return
	 */
	public String downLoadTemplate() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			CommonFilter filter = new CommonFilter();
			filter.setPlanType(PlanTypeKpi.APPLY.getValue());

			ObjectVO<Kpi> vo = salePlanMgr.getListKpiByFilter(null, filter);
			if (vo.getLstObject() == null || vo.getLstObject().size() == 0) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("kpi_list_is_empty"));
			} else {
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstKpi", vo.getLstObject());
				String reportPath = exportExcelJxls(beans, Configuration.getExcelTemplatePathCatalog(), ConstantManager.TEMPLATE_IMPORT_KPI_TARGET);
				result.put(LIST, reportPath);
				MemcachedUtils.putValueToMemcached(reportToken, reportPath, retrieveReportMemcachedTimeout());
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, R.getResource("system.error"));
			LogUtility.logError(e, "KpiTargetAction.downLoadTemplate" + e.getMessage());
		}

		return JSON;
	}

	/**
	 * Import dynamic KPI list
	 * 
	 * @author trungtm6
	 * @since 13/07/2015
	 * @return
	 */
	public String importKpi() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		CommonFilter filter = new CommonFilter();
		filter.setPlanType(PlanTypeKpi.APPLY.getValue());
		try {
			ObjectVO<Kpi> vo = salePlanMgr.getListKpiByFilter(null, filter);
			if (vo.getLstObject() == null || vo.getLstObject().size() == 0) {
				isError = true;
				errMsg = R.getResource("kpi_list_is_empty");
			} else {
				List<Kpi> lstKpi = vo.getLstObject();
				final int STATIC_COL = 6;
				int numCol = STATIC_COL + lstKpi.size();
				List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, numCol);
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = checkMaxRecordImportPermiss(lstData.size());
					if (!StringUtil.isNullOrEmpty(errMsg)) {
						isError = true;
					}
				}
				if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
					Date sysdate = commonMgr.getSysDate();
					List<KpiImplement> lstCreate = new ArrayList<KpiImplement>();
					List<KpiImplement> lstUpdate = new ArrayList<KpiImplement>();
					List<Long> lstKiId = new ArrayList<Long>();
					LogInfoVO logVo = getLogInfoVO();
					for (int i = 0; i < lstData.size(); i++) {
						List<String> row = lstData.get(i);
						//						KpiImplement ki = new KpiImplement();
						if (row == null || row.size() == 0) {
							continue;
						}
						message = "";
						totalItem++;

						//shop_code
						Shop sh = null;
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "key.shop.import.npp.code", null, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								sh = shopMgr.getShopByCode(value);
								if (sh == null) {
									message += R.getResource("common.param.not.exist", R.getResource("customer.display.program.npp.code"));
								} else if (!sh.getStatus().equals(ActiveType.RUNNING)) {
									message += R.getResource("common.param.not.in.running", R.getResource("customer.display.program.npp.code"));
								} else {
									//									ki.setShop(sh);
								}
							}
						}

						//staff_code
						Staff st = null;
						if (row.size() > 2) {
							String value = row.get(2);
							String msg = ValidateUtil.validateField(value, "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								st = staffMgr.getStaffByCode(value);
								if (st == null) {
									message += R.getResource("common.param.not.exist", R.getResource("ss.traningplan.salestaff.code"));
								} else if (!st.getStatus().equals(ActiveType.RUNNING)) {
									message += R.getResource("common.param.not.in.running", R.getResource("ss.traningplan.salestaff.code"));
								} else if (sh != null && !st.getShop().getId().equals(sh.getId())) {
									message += R.getResource("common.not.belong.to.npp", R.getResource("catalog.staff.code.param", st.getStaffCode()), sh.getShopCode());
								} else {
									//									ki.setStaff(st);
								}
							}
						}
						//year
						Integer year = null;
						if (row.size() > 4) {
							String value = row.get(4);
							String msg = ValidateUtil.validateField(value, "jsp.common.nam.n", 4, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msg) || value.length() != 4 || !StringUtil.isNumberInt(value) || Integer.valueOf(value) < 0) {
								msg = R.getResource("jsp.common.nam.n.ex");
								message += msg;
							} else {
								try {
									year = Integer.valueOf(value);
									if (year < DateUtil.getYear(sysdate)) {
										message += R.getResource("kpi_year_must_greater_or_equal_sysyear");
									}
								} catch (ParseException e) {
									message += R.getResource("kpi.year.not.valid");
								}
							}
						}
						//period
						Integer period = null;
						Cycle cycle = null;
						if (row.size() > 5) {
							String value = row.get(5);
							String msg = ValidateUtil.validateField(value, "baocao.1.3.kqdt.header.chuky", 9, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER);
							;
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								try {
									period = Integer.valueOf(value);
									Cycle curCycle = cycleMgr.getCycleByDate(sysdate);
									if (period < 0) {
										message += R.getResource("kpi.period.not.valid");
									} else if (year != null && StringUtil.isNullOrEmpty(message)) {
										cycle = cycleMgr.getCycleByYearAndNum(year, period);
										if (cycle == null) {
											message += R.getResource("kpi.cycle.not.exist");
										} else if (cycle.getNum() < curCycle.getNum()) {
											message += R.getResource("kpi_period_must_greater_or_equal_curperiod");
										}
									}
								} catch (ParseException e) {
									message += R.getResource("kpi.period.not.valid");
								}
							}
						}

						if (StringUtil.isNullOrEmpty(message)) {
							if (row.size() > STATIC_COL) {
								//for lst Dynamic kpi
								int nKpi = lstKpi.size();
								if (nKpi > row.size() - STATIC_COL) {//min dynamic columns
									nKpi = row.size() - STATIC_COL;
								}
								for (int j = 0; j < nKpi; j++) {//j = STATIC_COL
									if (StringUtil.isNullOrEmpty(message)) {
										String value = row.get(STATIC_COL + j);
										BigDecimal kpiValue = null;
										String msg = ValidateUtil.validateField(value, "kpi.target", null, ConstantManager.ERR_FLOAT_FORMAT);
										if (!StringUtil.isNullOrEmpty(msg)) {
											message += msg;
										} else if (!StringUtil.isNullOrEmpty(value)) {
											try {
												kpiValue = new BigDecimal(Float.valueOf(value));
											} catch (ParseException e) {
												message += R.getResource("kpi.target.not.valid");
											}
										}
										if (StringUtil.isNullOrEmpty(message)) {
											KpiImplement kiCheck = salePlanMgr.getKpiImplement(st.getId(), lstKpi.get(j).getId(), cycle.getId());

											if (kiCheck != null) {//update
												kiCheck.setPlan(kpiValue);
												kiCheck.setUpdateDate(sysdate);
												kiCheck.setUpdateUser(logVo.getStaffCode());
												if (lstKiId.indexOf(kiCheck.getId()) < 0) {
													lstKiId.add(kiCheck.getId());
													lstUpdate.add(kiCheck);
												} else {
													message += R.getResource("kpi.duplicate.import");
													lstFails.add(StringUtil.addDynamicFailBean(row, STATIC_COL + 1, message));
												}

											} else {//create
												KpiImplement ki = new KpiImplement();
												ki.setShop(sh);
												ki.setStaff(st);
												ki.setCycle(cycle);
												ki.setKpi(lstKpi.get(j));
												ki.setPlan(kpiValue);
												ki.setCreateDate(sysdate);
												ki.setCreateUser(logVo.getStaffCode());
												//Check duplicate
												for (KpiImplement createKi : lstCreate) {
													if (createKi.getStaff().getId().equals(st.getId()) && createKi.getCycle().getId().equals(cycle.getId()) && createKi.getKpi().getId().equals(lstKpi.get(j).getId())) {
														message += R.getResource("kpi.duplicate.import");
														break;
													}
												}
												if (StringUtil.isNullOrEmpty(message)) {
													lstCreate.add(ki);
												} else {
													lstFails.add(StringUtil.addDynamicFailBean(row, STATIC_COL + 1, message));
												}
											}
										} else {
											lstFails.add(StringUtil.addDynamicFailBean(row, STATIC_COL + 1, message));
										}
									}
									//									else { //message is not empty
									//										lstFails.add(StringUtil.addDynamicFailBean(row, STATIC_COL + 1, message));
									//									}

								}
							}
						} else {//message is not empty
							lstFails.add(StringUtil.addDynamicFailBean(row, STATIC_COL + 1, message));
						}

					}

					if (lstCreate != null && lstCreate.size() > 0) {
						commonMgr.creatListEntity(lstCreate);
					}
					if (lstUpdate != null && lstUpdate.size() > 0) {
						commonMgr.updateListEntity(lstUpdate);
					}
					// Export error
					getOutputFailFile(lstKpi, lstFails);
					//					getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_IMPORT_KPI_TARGET_ERR);

				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.KpiTargetAction.saveInfo()"), createLogErrorStandard(startLogDate));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	public String exportExcel() {
		try {
			if (shopCode != null && yearPeriod != null && numPeriod != null) {
				String reportToken = retrieveReportToken(reportCode);
				if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
					return JSON;
				}
				Shop sh = shopMgr.getShopByCode(shopCode);
				Cycle c = cycleMgr.getCycleByYearAndNum(yearPeriod, numPeriod);
				if (sh != null && c != null) {
					CommonFilter fi = new CommonFilter();
					fi.setPlanType(PlanTypeKpi.APPLY.getValue());
					fi.setStatus(ActiveType.RUNNING);
					ObjectVO<Kpi> vo = salePlanMgr.getListKpiByFilter(null, fi);
					lstKpi = vo.getLstObject();
					lstKpiInfo = salePlanMgr.getListDynamicStaffKpi(sh.getId(), c.getId());

					if (lstKpiInfo != null && lstKpiInfo.size() > 0) {

						XSSFWorkbook wb = new XSSFWorkbook();
						//						Map<String, XSSFCellStyle> styles = ExcelProcessUtils.createStyles(wb);
						Map<String, XSSFCellStyle> styles = ExcelProcessUtils.createStylesWithFont(wb, "Times New Roman");

						File tmp = File.createTempFile("sheet_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE), ".xml");
						Writer fw = new FileWriterWithEncoding(tmp, Charset.forName("UTF-8"));

						SpreadsheetWriter sw = new SpreadsheetWriter(fw);
						sw.beginSheet(12, 30, 12, 30, 12, 12, 20, 20, 20, 20, 20, 20, 20, 20);//set width for columns	
						int nRow = 0;
						//						sw.createEmptyRow(nRow++);	
						sw.insertRow(nRow++);
						int col = 0;
						int styleIndex = styles.get(ExcelProcessUtils.STYLE_HEADER_GREEN_HO_2_3).getIndex();
						int styleText = styles.get(ExcelProcessUtils.STYLE_TEXT_ALIGN_LEFT).getIndex();
						int styleCen = styles.get(ExcelProcessUtils.STYLE_TEXT_ALIGN_CENTER).getIndex();
						//						int styleTextB = styles.get(ExcelProcessUtils.style_B).getIndex();
						int styleNumber = styles.get(ExcelProcessUtils.STYLE_CURRENCY).getIndex();

						sw.createCell(col++, R.getResource("customer.display.program.npp.code"), styleIndex);
						sw.createCell(col++, R.getResource("ss.ds3.sheet1.header.tenNPP"), styleIndex);
						sw.createCell(col++, R.getResource("customer.display.program.staff.code"), styleIndex);
						sw.createCell(col++, R.getResource("ss.ds3.sheet1.header.tenNVBH"), styleIndex);
						sw.createCell(col++, R.getResource("jsp.common.nam.n"), styleIndex);
						sw.createCell(col++, R.getResource("sale_plan_period"), styleIndex);
						for (Kpi k : lstKpi) {
							sw.createCell(col++, k.getCriteria(), styleIndex);
						}
						sw.endRow();
						DynamicStaffKpiVO kpiVo = null;
						for (int i = 0, size = lstKpiInfo.size(); i < size - 1; i++) {//Not show avg row 
							kpiVo = lstKpiInfo.get(i);
							sw.insertRow(nRow++);
							//							kpiVo.safeSetNull();
							col = 0;
							sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getShopCode()), styleText);
							sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getShopName()), styleText);
							sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getStaffCode()), styleText);
							sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getStaffName()), styleText);
							//							sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getCycleText()), styleCen);
							sw.createCell(col++, StringEscapeUtils.escapeXml(yearPeriod.toString()), styleText);
							sw.createCell(col++, StringEscapeUtils.escapeXml(numPeriod.toString()), styleText);
							for (DynamicVO dy : kpiVo.getLstKpiData()) {
								sw.createCell(col++, dy.getValue() != null ? new BigDecimal(dy.getValue().toString()) : BigDecimal.ZERO, styleNumber);
							}
							sw.endRow();
						}
						sw.endSheet();
						String reportPath = ExcelReporter.exportXLSX("List_Kpi_Target", wb, fw, tmp);
						result.put(LIST, reportPath);

						/*
						 * Map<String, Object> beans = new HashMap<String,
						 * Object>(); beans.put("lstKpi", vo.getLstObject());
						 * beans.put("lstKpiInfo", vo.getLstObject());
						 * result.put(LIST, exportExcelJxls(beans,
						 * Configuration.getExcelTemplatePathCatalog(),
						 * ConstantManager.TEMPLATE_IMPORT_KPI_TARGET));
						 */

						MemcachedUtils.putValueToMemcached(reportToken, reportPath, retrieveReportMemcachedTimeout());
						result.put("hasData", true);
					} else {
						result.put("hasData", false);
					}
				}
			}

		} catch (Exception e) {
			LogUtility.logError(e, "KpiTargetAction.exportExcel(): " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	private void getOutputFailFile(List<Kpi> lstKpi, List<CellBean> lstFails) throws Exception {
		if (lstFails != null && lstFails.size() > 0) {
			numFail = lstFails.size();
			XSSFWorkbook wb = new XSSFWorkbook();
			Map<String, XSSFCellStyle> styles = ExcelProcessUtils.createStylesWithFont(wb, "Times New Roman");

			File tmp = File.createTempFile("sheet_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE), ".xml");
			Writer fw = new FileWriterWithEncoding(tmp, Charset.forName("UTF-8"));

			SpreadsheetWriter sw = new SpreadsheetWriter(fw);
			sw.beginSheet(12, 30, 12, 30, 12, 12, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20);//set width for columns	
			int nRow = 0;
			//			sw.createEmptyRow(nRow++);	
			sw.insertRow(nRow++);
			int col = 0;
			int styleIndex = styles.get(ExcelProcessUtils.STYLE_HEADER_GREEN_HO_2_3).getIndex();
			int styleText = styles.get(ExcelProcessUtils.STYLE_TEXT_ALIGN_LEFT).getIndex();
			//			int styleCen = styles.get(ExcelProcessUtils.STYLE_TEXT_ALIGN_CENTER).getIndex();
			int styleErr = styles.get(ExcelProcessUtils.STYLE_TEXT_RED_LEFT).getIndex();

			sw.createCell(col++, R.getResource("customer.display.program.npp.code"), styleIndex);
			sw.createCell(col++, R.getResource("ss.ds3.sheet1.header.tenNPP"), styleIndex);
			sw.createCell(col++, R.getResource("customer.display.program.staff.code"), styleIndex);
			sw.createCell(col++, R.getResource("ss.ds3.sheet1.header.tenNVBH"), styleIndex);
			sw.createCell(col++, R.getResource("sale_plan_year"), styleIndex);
			sw.createCell(col++, R.getResource("sale_plan_period"), styleIndex);
			for (Kpi k : lstKpi) {
				sw.createCell(col++, k.getCriteria(), styleIndex);
			}
			sw.createCell(col++, R.getResource("catalog.product.note"), styleIndex);
			sw.endRow();
			CellBean kpiVo = null;
			for (int i = 0, size = lstFails.size(); i < size; i++) {
				kpiVo = lstFails.get(i);
				sw.insertRow(nRow++);
				col = 0;
				sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getContent1()), styleText);
				sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getContent2()), styleText);
				sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getContent3()), styleText);
				sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getContent4()), styleText);
				sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getContent5()), styleText);
				sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getContent6()), styleText);
				for (String str : kpiVo.getLstDynamic()) {
					sw.createCell(col++, StringEscapeUtils.escapeXml(str), styleText);
				}
				sw.createCell(col++, StringEscapeUtils.escapeXml(kpiVo.getErrMsg()), styleErr);
				sw.endRow();
			}
			sw.endSheet();
			//			result.put(LIST, ExcelReporter.exportXLSX("List_Kpi_Target", wb, fw, tmp));
			fileNameFail = ExcelReporter.exportXLSXNotAuthentication("Import_KPI_Error", wb, fw, tmp);
		}
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Long getCurShopId() {
		return curShopId;
	}

	public void setCurShopId(Long curShopId) {
		this.curShopId = curShopId;
	}

	public Integer getYearPeriod() {
		return yearPeriod;
	}

	public void setYearPeriod(Integer yearPeriod) {
		this.yearPeriod = yearPeriod;
	}

	public Integer getNumPeriod() {
		return numPeriod;
	}

	public void setNumPeriod(Integer numPeriod) {
		this.numPeriod = numPeriod;
	}

	public List<Integer> getLstYear() {
		return lstYear;
	}

	public void setLstYear(List<Integer> lstYear) {
		this.lstYear = lstYear;
	}

	public List<Kpi> getLstKpi() {
		return lstKpi;
	}

	public void setLstKpi(List<Kpi> lstKpi) {
		this.lstKpi = lstKpi;
	}

	public List<DynamicStaffKpiVO> getLstKpiInfo() {
		return lstKpiInfo;
	}

	public void setLstKpiInfo(List<DynamicStaffKpiVO> lstKpiInfo) {
		this.lstKpiInfo = lstKpiInfo;
	}

	public List<DynamicStaffKpiSaveVO> getLstKpiSaveInfo() {
		return lstKpiSaveInfo;
	}

	public void setLstKpiSaveInfo(List<DynamicStaffKpiSaveVO> lstKpiSaveInfo) {
		this.lstKpiSaveInfo = lstKpiSaveInfo;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<Integer> getLstPeriod() {
		return lstPeriod;
	}

	public void setLstPeriod(List<Integer> lstPeriod) {
		this.lstPeriod = lstPeriod;
	}

	public List<CycleVO> getLstCycleVo() {
		return lstCycleVo;
	}

	public void setLstCycleVo(List<CycleVO> lstCycleVo) {
		this.lstCycleVo = lstCycleVo;
	}

}

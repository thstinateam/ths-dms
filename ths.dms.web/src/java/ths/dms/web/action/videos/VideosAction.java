package ths.dms.web.action.videos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DisplayProgrameMgr;
import ths.dms.core.business.ImageMgr;
import ths.dms.core.business.MediaMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Media;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.MediaMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.MediaType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.MediaFilter;
import ths.dms.core.entities.filter.ProductFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.ProductVOEx;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class VideosAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5330840248556870465L;

	ProductMgr productMgr;
	DisplayProgrameMgr displayProgrameMgr;
	CommonMgr commonMgr;
	StaffMgr staffMgr;
	ShopMgr shopMgr;
	CustomerMgr customerMgr;
	ImageMgr imageMgr;
	MediaMgr mediaMgr;
	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;
	Staff staffSign;
	
	String fromDate;
	String toDate;
	
	List<Media> listVideo;
	List<Product> listProduct;
	List<ProductVOEx> listProductVOabc;
	List<ProductVOEx> listCategoryPro;
	//List<String> listProductCodeAddForVideo;
	//private List<ProductCategory> lstCategory;
	private List<ProductInfo> lstCategory;
	List<Long> listChooseProductId;
	
	private String mediaCode;
	private String mediaName;
	
	private String productCode;
	private String productName;
	private String productCategory;
	
	private Integer type;
	private Integer status;
	private Date createFromDate;
	private Date createToDate;	
	private Date updateFromDate;
	private Date updateToDate;
	private File videoFile;	
	private String videoFileContentType;
	
	private MediaItem mediaItemForVideo;
	
	/** The err msg. */
	protected String errMsg;
	
	/** The is error. */
	protected boolean isError;
	
	private Integer checkAll;
	
	private static final String _SEPARATOR = "/"; 
	
	@Override
	public void prepare() throws Exception {
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		displayProgrameMgr = (DisplayProgrameMgr) context.getBean("displayProgrameMgr");
		productMgr = (ProductMgr)context.getBean("productMgr");
		staffMgr = (StaffMgr)context.getBean("staffMgr");
		shopMgr = (ShopMgr)context.getBean("shopMgr");
		customerMgr = (CustomerMgr)context.getBean("customerMgr");
		mediaMgr = (MediaMgr)context.getBean("mediaMgr");
		productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
		staffSign = getStaffByCurrentUser();
		
		super.prepare();
	}


	@Override
	public String execute() throws Exception {
		resetToken(result);
		lstCategory = new ArrayList<ProductInfo>();
		return SUCCESS;
	}
		
	public String searchVideos(){
		try{		
			listVideo = new ArrayList<Media>();	
			MediaFilter filter = setFilter();
			ObjectVO<Media> listVo = mediaMgr.getListMedia(filter);
			if(null != listVo && null != listVo.getLstObject()){
				listVideo = listVo.getLstObject();
			}			
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	public String searchProductByVideo(){
		try{			
			listProduct = new ArrayList<Product>();			
			if(StringUtil.isNullOrEmpty(mediaCode)){
				return JSON;
			}else{
				Media media = mediaMgr.getMediaByCode(mediaCode);
				if(null == media){
					return JSON;
				}
				ObjectVO<Product> listVo = mediaMgr.getListProductByMedia(null, media.getMediaId(), 1);				
				if(null != listVo && null != listVo.getLstObject()){
					listProduct = listVo.getLstObject();
				}				
			}						
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public String searchProductForVideo(){
		try{
			ProductFilter filter = new ProductFilter();
			result.put("page", page);
		    result.put("rows", rows);
		    KPaging<ProductVOEx> kPaging = new KPaging<ProductVOEx>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);	   
			filter.setkPaging(kPaging);
			
			if(!StringUtil.isNullOrEmpty(productCode)){
				filter.setCodePro(productCode);
			}
			if(!StringUtil.isNullOrEmpty(productName)){
				filter.setNamePro(productName);				
			}
			if(!StringUtil.isNullOrEmpty(productCategory)){
				filter.setCatNamePro(productCategory);				
			}
			
			Long mediaId = null;
			if(null != mediaCode){
				Media media = mediaMgr.getMediaByCode(mediaCode);
				if(null != media){
					mediaId = media.getMediaId();
				}
			}
			
			listProductVOabc = new ArrayList<ProductVOEx>();
			List<Product> lstProCurForVideo = null;			
			ObjectVO<Product> listVOProductCurrentForVideo = mediaMgr.getListProductByMedia(null, mediaId, 1);
			if(null != listVOProductCurrentForVideo && null != listVOProductCurrentForVideo.getLstObject()){
				lstProCurForVideo = listVOProductCurrentForVideo.getLstObject();
			}			
//			ObjectVO<ProductVO> listVo = productMgr.getListProductVOForVideo(filter, lstProCurForVideo);
			ObjectVO<ProductVOEx> listVo = productMgr.getListProductVOForVideo(filter, lstProCurForVideo);
			if(null != listVo && null != listVo.getLstObject()){
				listProductVOabc = listVo.getLstObject();
				result.put("total", listVo.getkPaging().getTotalRows());
			    result.put("rows", listVo.getLstObject());
			}	
						
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public List<ProductVOEx> getListProductForVideo(){
		try{
			ProductFilter filter = new ProductFilter();			
			if(!StringUtil.isNullOrEmpty(productCode)){
				filter.setCodePro(productCode);
			}
			if(!StringUtil.isNullOrEmpty(productName)){
				filter.setNamePro(productName);				
			}
			if(!StringUtil.isNullOrEmpty(productCategory)){
				filter.setCatNamePro(productCategory);				
			}
			
			Long mediaId = null;
			if(null != mediaCode){
				Media media = mediaMgr.getMediaByCode(mediaCode);
				if(null != media){
					mediaId = media.getMediaId();
				}
			}
						
			List<Product> lstProCurForVideo = new ArrayList<Product>();			
			ObjectVO<Product> listVOProductCurrentForVideo = mediaMgr.getListProductByMedia(null, mediaId, 1);
			if(null != listVOProductCurrentForVideo && null != listVOProductCurrentForVideo.getLstObject()
					&& listVOProductCurrentForVideo.getLstObject().size()>0){
				lstProCurForVideo = listVOProductCurrentForVideo.getLstObject();
			}			
			ObjectVO<ProductVOEx> listVo = productMgr.getListProductVOForVideo(filter, lstProCurForVideo);			
			if(null != listVo && null != listVo.getLstObject()){
				return listVo.getLstObject();
			}						
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return null;
	}
	
	public String getListCategory(){
		try{
			listCategoryPro = new ArrayList<ProductVOEx>();
//			listCategoryPro = productMgr.getListCategory("Z");	
			listCategoryPro = productMgr.getListCategory("Z");
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public void copyInputStreamToOutputStream(final InputStream in,
	        final OutputStream out) throws IOException
	{
	    try{
	        try{
	            final byte[] buffer = new byte[1024];
	            int n;
	            while ((n = in.read(buffer)) != -1)
	                out.write(buffer, 0, n);
	        }
	        finally{
	            out.close();
	        }
	    }
	    finally{
	        in.close();
	    }
	}
	
	public String addVideo() throws IOException{
	    resetToken(result);
		//String realPath = Configuration.getImageRealProdcutPath();
	    String realPath = Configuration.getRealVideoPath();
		String pathImageThumbTemp = "resources/images/thumbnailaudiovideo.png";
		//String pathImageThumbFull = ServletActionContext.getServletContext().getRealPath("/") + pathImageThumbTemp;			
		//File imageThumb = new File(pathImageThumbFull);
		
		mediaCode = mediaCode.trim().toUpperCase();
		mediaName = mediaName.trim();
		Media mediaCheck = null;
		try {
			mediaCheck = mediaMgr.getMediaByCode(mediaCode);
			if(null != mediaCheck && null != mediaCheck.getMediaId()){
				isError = true;
				errMsg =  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.video.code.existed");			
				return SUCCESS;
			}
			//Kiem tra duoi file hop le
			if (!FileUtility.isValidExtension(FileUtility.getFileExtension(videoFile), FileUtility.validVideoExtension)) {
				result.put(ERROR, true);
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.validate.file.error", videoFile.getName());
				return SUCCESS;
			}
			errMsg = ValidateUtil.validateVideoFile(videoFile, videoFileContentType);
			if(!StringUtil.isNullOrEmpty(errMsg)){
				isError = true;
				return SUCCESS;
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());			
		}
		
		
		if(null != mediaCode && null != mediaName && null != status && null != videoFile){
			Calendar calendar = Calendar.getInstance();		
			String format = "." + videoFileContentType.substring(videoFileContentType.length() - 3);
			String pathFolderTemp = createDetailUploadPath4MT(calendar, 0);
			String pathFolderVideo = realPath + "/" + pathFolderTemp;
			//String videoFileNameTemp = videoFile.getName().substring(0, videoFile.getName().length() - 4);
			String videoFileNameTemp = getNameVideoFile(mediaCode);
			String videoFileNameExtension = videoFileNameTemp + format;
			String pathFileVideo = pathFolderVideo + videoFileNameExtension;			
			
			File folder = new File(pathFolderVideo);
			if(!folder.exists()){
				folder.mkdirs();
			}
			File videoFileToCreate = new File(pathFileVideo);
			if(!videoFileToCreate.exists()){							
				
				FileOutputStream fileOutStream = new FileOutputStream(videoFileToCreate);
				FileInputStream fileInputStream = new FileInputStream(videoFile);
				copyInputStreamToOutputStream(fileInputStream, fileOutStream);
			}
								
			try{
				//Luu media item vao db
				MediaItem mediaItem = new MediaItem();
		        mediaItem.setCreateDate(DateUtil.now());
		        mediaItem.setThumbUrl("/" + pathImageThumbTemp);
		        mediaItem.setUrl(pathFolderTemp + "/" + videoFileNameExtension);        
//		        mediaItem.setObjectType(7);
		        mediaItem.setObjectType(MediaObjectType.VIDEO_UPLOAD);
		        mediaItem.setObjectId(0L);
//		        mediaItem.setMediaType(1);//0:image, 1:video
		        mediaItem.setMediaType(MediaType.VIDEO);//0:image, 1:video
		        mediaItem.setCreateUser(currentUser.getUserName());
				mediaItem.setCreateDate(DateUtil.now());
		        mediaItem = mediaMgr.createMediaItem(mediaItem);
		        
		        
		        
		        //Luu media vao db
				Media media = new Media();
				media.setMediaItemId(mediaItem);
				media.setMediaCode(mediaCode);
				media.setMediaName(mediaName);
				media.setStatus(status);
				media.setCreateUser(currentUser.getUserName());
				media.setCreateDate(DateUtil.now());				
				media = mediaMgr.createMedia(media);
				
				mediaItem.setObjectId(media.getMediaId());
				mediaMgr.updateMediaItem(mediaItem);
		        
			}catch(Exception e){
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}		   
		}
		
		if(null != errMsg && errMsg.length() > 0){
			isError = true;
		}else{
			isError = false;
		}
		return SUCCESS;
	}
	
	private String getNameVideoFile(String mediaCode){
		Calendar cal = Calendar.getInstance();
		Integer tmpM = cal.get(Calendar.MONTH) +1;
		String dd = String.valueOf(cal.get(Calendar.DATE)>10? cal.get(Calendar.DATE):"0"+cal.get(Calendar.DATE));
		String MM = String.valueOf(tmpM>10?tmpM:"0"+tmpM);
		String hh = String.valueOf(cal.get(Calendar.HOUR_OF_DAY)>10?cal.get(Calendar.HOUR_OF_DAY):"0"+cal.get(Calendar.HOUR_OF_DAY));
		String mm = String.valueOf(cal.get(Calendar.MINUTE)>10?cal.get(Calendar.MINUTE):"0"+cal.get(Calendar.MINUTE));
		String ss = String.valueOf(cal.get(Calendar.SECOND)>10?cal.get(Calendar.SECOND):"0"+cal.get(Calendar.SECOND));
        String fileNameMedia = mediaCode+dd+MM+cal.get(Calendar.YEAR)+hh+mm+ss + Long.toString(cal.getTimeInMillis()); 
        return fileNameMedia;
	}
	
	public String updateVideo(){
	    resetToken(result);
	    mediaName = mediaName.trim();
		try{			
			if(!StringUtil.isNullOrEmpty(mediaName)  && null != status && !StringUtil.isNullOrEmpty(mediaCode)){
				Media media = mediaMgr.getMediaByCode(mediaCode);
				if(null != media){
					media.setMediaName(mediaName);
					media.setStatus(status);
					media.setUpdateUser(currentUser.getUserName());
					media.setUpdateDate(DateUtil.now());
					mediaMgr.updateMedia(media);
				}
			}else{
				if(StringUtil.isNullOrEmpty(mediaCode)){
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.video.code.is.null");					
				}else{
					if(StringUtil.isNullOrEmpty(mediaName)){
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.video.name.is.null");
					}else{
						if(null == status){
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.video.status.is.null");
						}
					}
				}
				if(null != errMsg && errMsg.length() > 0){
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}				
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	
	public String addProductForVideo(){
	    resetToken(result);
		try{
			if(null == checkAll || checkAll == 0){			
				if(null != listChooseProductId && listChooseProductId.size() > 0 && null != mediaCode){
					Media media = mediaMgr.getMediaByCode(mediaCode);
					if(null == media){
						return JSON;
					}				
					for(int i = 0; i<listChooseProductId.size(); i++){
						Product pr = productMgr.getProductById(listChooseProductId.get(i));					
						if(null != pr){				
							MediaMap mediaMap = new MediaMap();
							mediaMap.setCreateDate(DateUtil.now());
							mediaMap.setCreateUser(currentUser.getUserName());
							mediaMap.setMedia(media);
							mediaMap.setObjectId(pr.getId());
							mediaMap.setObjectType(1);
							mediaMap.setStatus(1);
							mediaMgr.createMediaMap(mediaMap);						
						}				
					}							
				}
			}else{
				if(checkAll == 1 && null != mediaCode){
					Media media = mediaMgr.getMediaByCode(mediaCode);
					if(null == media){
						return JSON;
					}						
					List<ProductVOEx> lstProductForVideo = getListProductForVideo();
					for(int i = 0; i<lstProductForVideo.size(); i++){
						Product pr = productMgr.getProductById(lstProductForVideo.get(i).getId());					
						if(null != pr){				
							MediaMap mediaMap = new MediaMap();
							mediaMap.setCreateDate(DateUtil.now());
							mediaMap.setCreateUser(currentUser.getUserName());
							mediaMap.setMedia(media);
							mediaMap.setObjectId(pr.getId());
							mediaMap.setObjectType(1);
							mediaMap.setStatus(1);
							mediaMgr.createMediaMap(mediaMap);						
						}				
					}	
				}
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	
	public String deleteProductForVideo(){
	    resetToken(result);
		try{
			Long mediaId = null;
			Long productId = null;
			if(null != mediaCode && null != productCode){
				Media media = mediaMgr.getMediaByCode(mediaCode);
				if(null != media){
					mediaId = media.getMediaId();
				}
				Product product = productMgr.getProductByCode(productCode);
				if(null != product){
					productId = product.getId();
				}
				MediaMap mediaMap = mediaMgr.getMediaMapByMediaAndObject(mediaId, productId, 1l);
				if(null != mediaMap){
					mediaMap.setStatus(-1);
					mediaMap.setUpdateUser(currentUser.getUserName());
					mediaMap.setUpdateDate(DateUtil.now());
					mediaMgr.updateMediaMap(mediaMap);
				}
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	
	public MediaFilter setFilter() throws BusinessException{
		MediaFilter filter = new MediaFilter();
		KPaging<Media> kPaging = null;	
	
		if(0 != max && 0 != page){
			kPaging = new KPaging<Media>();
			kPaging.setPageSize(max);
			kPaging.setPage(page);			
		}			
				
		filter.setkPaging(kPaging);
		
		if(!StringUtil.isNullOrEmpty(mediaCode)){
			filter.setMediaCode(mediaCode);
		}
		
		if(!StringUtil.isNullOrEmpty(mediaName)){
			filter.setMediaName(mediaName);
		}
		
		if(null != type){
			filter.setType(type);			
		}
		
		if(null != status){
			filter.setStatus(status);
		}
		
		if(null != createFromDate){
			filter.setCreateFromDate(createFromDate);
		}
		
		if(null != createToDate){
			filter.setCreateToDate(createToDate);
		}
		
		if(null != updateFromDate){
			filter.setUpdateFromDate(updateFromDate);
		}
		
		if(null != updateToDate){
			filter.setUpdateToDate(updateToDate);
		}
		
		return filter;
	}

    public String createDetailUploadPath4MT(Calendar calendar, int type) {
        StringBuffer folder = new StringBuffer();
        //folder.append(ConstantManager.PATH_TO_FORDER);
        folder.append(calendar.get(Calendar.YEAR));
        folder.append(_SEPARATOR);
        folder.append(calendar.get(Calendar.MONTH) + 1);
        if(type == 0){
        	folder.append(ConstantManager.PATH_TO_VIDEO);
        }else{
        	folder.append(ConstantManager.PATH_TO_GALLERY_THUMBNAIL);
        }                      
        return folder.toString();
    }
    
    public List<String> getListCategoryByListPro(List<ProductVO> listProduct){
    	List<String> result = new ArrayList<String>();
    	for(int i = 0; i<listProduct.size(); i++){
    		if(!result.contains(listProduct.get(i).getCatCode())){
    			result.add(listProduct.get(i).getCatCode());
    		}
    	}
    	return result;
    }
    
    public String getMediaForVideo(){
    	try{ 
    		mediaItemForVideo = new MediaItem();    	
	    	if(null != mediaCode){
	    		Media media = mediaMgr.getMediaByCode(mediaCode);
	    		if(null != media && null != media.getMediaItemId()){
	    			mediaItemForVideo = media.getMediaItemId();	    			
	    		}
	    	}
    	}catch(Exception e){
    		LogUtility.logError(e, e.getMessage());
    	}
    	return JSON;
    }
    
	public ProductMgr getProductMgr() {
		return productMgr;
	}


	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}


	public DisplayProgrameMgr getDisplayProgrameMgr() {
		return displayProgrameMgr;
	}


	public void setDisplayProgrameMgr(DisplayProgrameMgr displayProgrameMgr) {
		this.displayProgrameMgr = displayProgrameMgr;
	}


	public CommonMgr getCommonMgr() {
		return commonMgr;
	}


	public void setCommonMgr(CommonMgr commonMgr) {
		this.commonMgr = commonMgr;
	}


	public StaffMgr getStaffMgr() {
		return staffMgr;
	}


	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}


	public ShopMgr getShopMgr() {
		return shopMgr;
	}


	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}


	public CustomerMgr getCustomerMgr() {
		return customerMgr;
	}


	public void setCustomerMgr(CustomerMgr customerMgr) {
		this.customerMgr = customerMgr;
	}


	public ImageMgr getImageMgr() {
		return imageMgr;
	}


	public void setImageMgr(ImageMgr imageMgr) {
		this.imageMgr = imageMgr;
	}


	public Staff getStaffSign() {
		return staffSign;
	}


	public void setStaffSign(Staff staffSign) {
		this.staffSign = staffSign;
	}


	public String getFromDate() {
		return fromDate;
	}


	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	public String getToDate() {
		return toDate;
	}


	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	
	public List<Product> getListProduct() {
		return listProduct;
	}


	public void setListProduct(List<Product> listProduct) {
		this.listProduct = listProduct;
	}


	public String getMediaCode() {
		return mediaCode;
	}

	
	public void setMediaCode(String mediaCode) {
		this.mediaCode = mediaCode;
	}

	

	public String getMediaName() {
		return mediaName;
	}


	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}


	public Integer getType() {
		return type;
	}


	public void setType(Integer type) {
		this.type = type;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public Date getCreateFromDate() {
		return createFromDate;
	}


	public void setCreateFromDate(Date createFromDate) {
		this.createFromDate = createFromDate;
	}


	public Date getCreateToDate() {
		return createToDate;
	}


	public void setCreateToDate(Date createToDate) {
		this.createToDate = createToDate;
	}


	public Date getUpdateFromDate() {
		return updateFromDate;
	}


	public void setUpdateFromDate(Date updateFromDate) {
		this.updateFromDate = updateFromDate;
	}


	public Date getUpdateToDate() {
		return updateToDate;
	}


	public void setUpdateToDate(Date updateToDate) {
		this.updateToDate = updateToDate;
	}


	public List<Media> getListVideo() {
		return listVideo;
	}


	public void setListVideo(List<Media> listVideo) {
		this.listVideo = listVideo;
	}


	public File getVideoFile() {
		return videoFile;
	}


	public void setVideoFile(File videoFile) {
		this.videoFile = videoFile;
	}
	
	public String getErrMsg() {
		return errMsg;
	}


	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	

	public boolean isError() {
		return isError;
	}


	public void setError(boolean isError) {
		this.isError = isError;
	}


	public String getVideoFileContentType() {
		return videoFileContentType;
	}


	public void setVideoFileContentType(String videoFileContentType) {
		this.videoFileContentType = videoFileContentType;
	}


	public String getProductCode() {
		return productCode;
	}


	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getProductCategory() {
		return productCategory;
	}


	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}


	public List<ProductVOEx> getListProductVOabc() {
		return listProductVOabc;
	}


	public void setListProductVOabc(List<ProductVOEx> listProductVOabc) {
		this.listProductVOabc = listProductVOabc;
	}


	public List<ProductVOEx> getListCategoryPro() {
		return listCategoryPro;
	}


	public void setListCategoryPro(List<ProductVOEx> listCategoryPro) {
		this.listCategoryPro = listCategoryPro;
	}

	
	public List<Long> getListChooseProductId() {
		return listChooseProductId;
	}


	public void setListChooseProductId(List<Long> listChooseProductId) {
		this.listChooseProductId = listChooseProductId;
	}


	public MediaItem getMediaItemForVideo() {
		return mediaItemForVideo;
	}


	public void setMediaItemForVideo(MediaItem mediaItemForVideo) {
		this.mediaItemForVideo = mediaItemForVideo;
	}


//	public List<ProductCategory> getLstCategory() {
//		return lstCategory;
//	}
//
//
//	public void setLstCategory(List<ProductCategory> lstCategory) {
//		this.lstCategory = lstCategory;
//	}

	

	public Integer getCheckAll() {
		return checkAll;
	}


	public MediaMgr getMediaMgr() {
		return mediaMgr;
	}


	public void setMediaMgr(MediaMgr mediaMgr) {
		this.mediaMgr = mediaMgr;
	}


	public ProductInfoMgr getProductInfoMgr() {
		return productInfoMgr;
	}


	public void setProductInfoMgr(ProductInfoMgr productInfoMgr) {
		this.productInfoMgr = productInfoMgr;
	}


	public List<ProductInfo> getLstCategory() {
		return lstCategory;
	}


	public void setLstCategory(List<ProductInfo> lstCategory) {
		this.lstCategory = lstCategory;
	}


	public void setCheckAll(Integer checkAll) {
		this.checkAll = checkAll;
	}
	
	
}

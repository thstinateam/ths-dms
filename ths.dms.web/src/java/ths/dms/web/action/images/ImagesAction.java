package ths.dms.web.action.images;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.VSARole;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.ImageUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DisplayProgrameMgr;
import ths.dms.core.business.ImageMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.MediaItemCustomerVO;
import ths.dms.core.entities.vo.MediaItemDetailVO;
import ths.dms.core.entities.vo.MediaItemProgramVO;
import ths.dms.core.entities.vo.MediaItemShopVO;
import ths.dms.core.entities.vo.MediaItemVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class ImagesAction.
 */
public class ImagesAction extends AbstractAction {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1668928173835297739L;
	
	/** The product mgr. */
	ProductMgr productMgr;
	
	/** The common mgr. */
	CommonMgr commonMgr;
	
	/** The staff mgr. */
	StaffMgr staffMgr;
	
	/** The shop mgr. */
	ShopMgr shopMgr;
	
	/** The customer mgr. */
	CustomerMgr customerMgr;
	
	/** The image mgr. */
	ImageMgr imageMgr;
	
	/** The display programe mgr. */
	DisplayProgrameMgr displayProgrameMgr;
	
	/** The max. */
	int max;
	
	/** The page. */
	int page;
	
	/** The shop. */
	private Shop shop;
	
	/** The shop id. */
	private Long shopId;
	
	/** The gsnpp id. */
	private Long gsnppId;
	
	/** The lst staff. */
	private List<StaffSimpleVO> lstStaff;
	
	/** The lst nvbh. */
	private List<StaffSimpleVO> lstNVBH;
	
	/** The from date. */
	private String fromDate;
	
	/** The to date. */
	private String toDate;
	
	/** The tuyen. */
	private Integer tuyen;
	
	/** The customer id. */
	private Long customerId;
	
	/** The customer code. */
	private String customerCode;
	
	/** The customer name or address. */
	private String customerNameOrAddress;
	
	/** The nvbh id. */
	private Long nvbhId;
	
	/** The lst album. */
	private List<ImageVO> lstAlbum;
	
	/** The lst image of album. */
	private List<ImageVO> lstImageOfAlbum;
	
	/** The lst display programe id. */
	private List<Long>  lstDisplayProgrameId;
	
	/** The display programe id. */
	private Long displayProgrameId;
	
	/** The is group. */
	private Boolean isGroup;
	
	/** The id. */
	private Long id;
	
	/** The lst cttb. */
	private List<Long> lstCTTB;
	
	/** The url image. */
	private String urlImage;
	
	/** The staff_role. */
	private Integer staff_role;
	
	/** The staff code search. */
	private String staffCodeSearch;
	
	/** The customer info. */
	private String customerInfo;
	
	/** The shop info. */
	private String shopInfo;
	
	/** The staff info. */
	private String staffInfo;
	
	/** The date info. */
	private String dateInfo;
	
	/** The overwrite. */
	private boolean overwrite;
	
	/** The lst shop. */
	private List<Long> lstShop;
	
	/** The lst staff search. */
	private List<Long> lstStaffSearch;
	
	/** The object type. */
	private Integer objectType;
	
	/** The type group. */
	private Integer typeGroup;
	
	/** The staff id group. */
	private Long staffIdGroup;
	
	/** The customer id group. */
	private Long customerIdGroup;
	
	/** The level. */
	private String level;
	
	/** The month seq. */
	private List<Integer> monthSeq;
	
	/** The last seq. */
	private Boolean lastSeq;
	
	/** The result img. */
	private Integer resultImg;
	
	/** The lst staff for shop. */
	private List<Staff> lstStaffForShop;
	
	/** The staff type. */
	private Integer staffType;
	
	/** The is empty image. */
	private Boolean isEmptyImage;
	
	/** The is ctb. */
	private Boolean isCTB;//cham trung bay
	
	/** The month seq download. */
	private String monthSeqDownload;
	
	/** The cttb order. */
	private String cttbOrder;
	
	/** The customer order. */
	private List<String> customerOrder;
	
	/** The title album. */
	private String titleAlbum;
	
	
	 private List<ImageVO> lstCTTBVOs;
	 
	 private List<Long> lstShopId;
		
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		productMgr = (ProductMgr)context.getBean("productMgr");
		staffMgr = (StaffMgr)context.getBean("staffMgr");
		shopMgr = (ShopMgr)context.getBean("shopMgr");
		customerMgr = (CustomerMgr)context.getBean("customerMgr");
		imageMgr = (ImageMgr)context.getBean("imageMgr");
		displayProgrameMgr = (DisplayProgrameMgr)context.getBean("displayProgrameMgr");
		super.prepare();
	}


	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		fromDate = DateUtil.toDateString(DateUtil.getFirstDateInMonth(DateUtil.now()),DateUtil.DATE_FORMAT_DDMMYYYY);
		toDate = DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_DDMMYYYY);		
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				isCTB=true;
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null && staff.getStaffType() != null && staff.getStaffType().getSpecificType() != null) {
					staffType = staff.getStaffType().getSpecificType().getValue();
					setStaff_role(staff.getStaffType().getSpecificType().getValue());
				}
				if (currentUser.getShopRoot() != null) {
					shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
					if (shop != null) {
						shopId = shop.getId();
						lstShop = new ArrayList<Long>();
						lstShop.add(shop.getId());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Decode to image.
	 *
	 * @param imageString the image string
	 * @return the buffered image
	 */
	public static BufferedImage decodeToImage(String imageString) {
		BufferedImage image = null;
		byte[] imageByte;
		try {
			BASE64Decoder decoder = new BASE64Decoder();
			imageByte = decoder.decodeBuffer(imageString);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return image;
	}
	
	/**
	 * Encode image to string.
	 *
	 * @param image the image
	 * @param type the type
	 * @return encoded string
	 */
	public static String encodeToString(BufferedImage image, String type) {
		String imageString = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, type, bos);
			byte[] imageBytes = bos.toByteArray();
			BASE64Encoder encoder = new BASE64Encoder();
			imageString = encoder.encode(imageBytes);
			bos.close();
		} catch (IOException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return imageString;
	}
	
	/**
	 * Sets the filter for popup.
	 *
	 * @return the image filter
	 * @throws BusinessException the business exception
	 */
	public ImageFilter setFilterForPopup() throws BusinessException {
		ImageFilter filter = new ImageFilter();
		KPaging<ImageVO> kPaging = new KPaging<ImageVO>();
		kPaging.setPageSize(max);
		kPaging.setPage(page);
		filter.setkPaging(kPaging);
		if (null != objectType) {
			filter.setObjectType(objectType);
		}
		filter.setKey(tuyen);
		if (lstShop != null && lstShop.size() > 0) {
			filter.setLstShop(lstShop);
		} else {
			List<Long> fail = new ArrayList<Long>();
			fail.add(-1l);
			filter.setLstShop(fail);
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getShopRoot().getShopId() != null) {
				// tulv2 update 18.10.2014
				lstShop = new ArrayList<Long>();
				lstShop.add(currentUser.getShopRoot().getShopId());
				filter.setLstShop(lstShop);
			}
		}
		filter.setShopIdListStr(getStrListShopId());
		filter.setLstStaff(lstStaffSearch);
		filter.setLevel(level);
		if (Boolean.TRUE.equals(lastSeq)) {
			filter.setLastSeq(true);
		} else if (monthSeq != null) {
			filter.setMonthSeq(monthSeq);
		}
		if (displayProgrameId != null && displayProgrameId != 0) {
			List<Long> arrDP = new ArrayList<Long>();
			arrDP.add(displayProgrameId);
			filter.setLstDisplayPrograme(arrDP);
		} else if (lstCTTB != null && lstCTTB.size() > 0) {
			filter.setLstDisplayPrograme(lstCTTB);
		}
		if (customerId != null && customerId != 0) {//neu truyen vao customerId thi ko can set code name
			filter.setIdCustomer(customerId);
		} else {
			if (!StringUtil.isNullOrEmpty(customerCode)) {
//				filter.setCodeCustomer(customerCode);
				filter.setShortCode(customerCode);
			}
//			ImageFilter fff = getFilterCustomerCode();
//			if (fff.getShopId() != null) {
//				filter.setShopId(fff.getShopId());
//			}
//			filter.setCodeCustomer(fff.getCodeCustomer());
//			filter.setShortCode(fff.getShortCode());
			if (!StringUtil.isNullOrEmpty(customerNameOrAddress)) {
				filter.setNameCustomer(customerNameOrAddress);
			}
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_DD_MM_YY);
			filter.setFromDate(fDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_DD_MM_YY);
			filter.setToDate(tDate);
		}
		if (typeGroup != null) {
			if (typeGroup == 1) {
				filter.setIsGroupByStaff(true);
			} else {
				filter.setIsGroupByStaff(false);
			}
			if (id != null) {
				if (typeGroup == 1) {
					filter.setStaffIdGroup(id);
				} else {
					filter.setIdCustomer(id);
				}
			}
		}
		return filter;
	}
	
	/**
	 * Gets the images for popup.
	 *
	 * @return the images for popup
	 */
	public String getImagesForPopup(){		
		try {
			ImageFilter filter=setFilterForPopup();
			ObjectVO<ImageVO> imageVO=null;
			if(filter.getIdCustomer()!=null){
				imageVO=imageMgr.getListImageVODetail(filter);
			}else{
				imageVO=imageMgr.getListImageVO(filter);
			}
			if(imageVO!=null){
				List<ImageVO> lstImage=imageVO.getLstObject();
				if(lstImage!=null && lstImage.size()>0){
					for(ImageVO temp:lstImage){
						if(temp.getCreateDate()!=null){
							temp.setHhmmDate(new SimpleDateFormat("HH:mm:ss").format(temp.getCreateDate()));
							temp.setDmyDate(new SimpleDateFormat("dd/MM/yyyy").format(temp.getCreateDate()));
						}
					}
				}
				result.put("lstImage", lstImage);
				result.put(ERROR, false);
			}
		} catch (BusinessException e) {
			LogUtility.logError(e,"ImagesAction.getImagesForPopup");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Update result.
	 *
	 * @return the string
	 */
	public String updateResult(){
		resetToken(result);
		result.put(ERROR, true);
		try {
			if (checkRoles(VSARole.VNM_TABLET_PORTAL_CTB)) {
				if (id != null && resultImg != null && (resultImg == 0 || resultImg == 1)) {
					MediaItem img = imageMgr.getMediaItemById(id);
					if (img != null && img.getObjectType().getValue() == MediaObjectType.IMAGE_DISPLAY.getValue()) {
						//img.setResult(resultImg);
						imageMgr.updateMediaItem(img);
						result.put(ERROR, false);
					}
				}
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, "ImagesAction.getImagesForPopup");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Adds the label to image.
	 *
	 * @param imgSrc the img src
	 * @param displayProgram the display program
	 * @param customerInfo the customer info
	 * @param shopInfo the shop info
	 * @param staffInfo the staff info
	 * @param dateInfo the date info
	 * @param overwrite the overwrite
	 * @return the string
	 * @author hungtx
	 * @since Aug 22, 2013
	 */
	private String addLabelToImage(String imgSrc, String displayProgram, String customerInfo, String shopInfo, String staffInfo, String dateInfo, boolean overwrite){
		String imgDes = "";		
		int dotIndex = imgSrc.lastIndexOf(".");
		imgDes = imgSrc.substring(0,dotIndex-1) + "_info" + imgSrc.substring(dotIndex);
		File fileImage=new File(Configuration.getImageRealSOPath()+imgSrc);	
		File outputFile =  new File(Configuration.getImageRealSOPath()+imgDes);
		if(fileImage.exists() && (!outputFile.exists() || overwrite)){			
			try{
				BufferedImage imageSrc = ImageIO.read(fileImage);
				BufferedImage newImage = new BufferedImage(imageSrc.getWidth(),imageSrc.getHeight() + 87, BufferedImage.OPAQUE);
				Graphics g = newImage.getGraphics();
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, newImage.getWidth(), newImage.getHeight());
		        g.drawImage(imageSrc, 0, 0, imageSrc.getWidth(), imageSrc.getHeight(), null);		        
		        Font font = new Font("Times New Roman", Font.PLAIN, 15);
		        g.setFont(font);
		        g.setColor(Color.BLACK);
		        if(displayProgram!=null) System.out.println(displayProgram);
		        if(!StringUtil.isNullOrEmpty(displayProgram))
		        	g.drawString("CT: "+displayProgram, 10, imageSrc.getHeight() + (15+2));
		        g.drawString(customerInfo, 10, imageSrc.getHeight() + (15+2)*2);
		        System.out.println(customerInfo);
		        g.drawString("NPP: " + shopInfo, 10, imageSrc.getHeight() + (15+2)*3);
		        System.out.println(shopInfo);
		        g.drawString("NV: " + staffInfo, 10, imageSrc.getHeight() + (15+2)*4);
		        g.drawString("TG: " + dateInfo, 10, imageSrc.getHeight() + (15+2)*5);
		        g.dispose();		        
		        OutputStream os = new FileOutputStream(outputFile);
		        
		        Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");   	 
		        ImageWriter writer = (ImageWriter) writers.next();
		        ImageOutputStream ios = ImageIO.createImageOutputStream(os);
		        writer.setOutput(ios);
		 
		        ImageWriteParam param = writer.getDefaultWriteParam();
		 
		        // compress to a given quality
		        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		        param.setCompressionQuality(0.6f);
		        writer.write(null, new IIOImage(newImage, null, null), param);    	        
		        os.close();
		        ios.close();
		        writer.dispose();
			}catch(Exception e){
				LogUtility.logError(e, e.getMessage());
				return "";
			}
		}
		return imgDes;
	}
	
	/**
	 * Adds the label to image ex.
	 *
	 * @param item the item
	 * @param imgDes the img des
	 * @param overwrite the overwrite
	 * @return the string
	 */
	private String addLabelToImageEx(MediaItemDetailVO item,String imgDes, boolean overwrite){
		File outputFile =  new File(imgDes);
		try{
			URL url = new URL(Configuration.getStoreStaticPath()+item.getUrl());
			BufferedImage imageSrc = ImageIO.read(url);
			BufferedImage newImage = new BufferedImage(imageSrc.getWidth(),imageSrc.getHeight() + 87, BufferedImage.OPAQUE);
			Graphics g = newImage.getGraphics();
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, newImage.getWidth(), newImage.getHeight());
	        g.drawImage(imageSrc, 0, 0, imageSrc.getWidth(), imageSrc.getHeight(), null);		        
	        Font font = new Font("Times New Roman", Font.PLAIN, 15);
	        g.setFont(font);
	        g.setColor(Color.BLACK);
	        int i=1;
	        if(!StringUtil.isNullOrEmpty(item.getDisplayProgramCode())){
	        	g.drawString("CT: "+item.getDisplayProgramCode(), 10, imageSrc.getHeight() + (15+2)*i);i++;
	        }
	        if(!StringUtil.isNullOrEmpty(item.getCustomerCode())){
	        	g.drawString(item.getCustomerCode(), 10, imageSrc.getHeight() + (15+2)*i);i++;
	        }
	        if(!StringUtil.isNullOrEmpty(item.getShopCode())){
	        	g.drawString("NPP: " + item.getShopCode(), 10, imageSrc.getHeight() + (15+2)*i);i++;
	        }
	        if(!StringUtil.isNullOrEmpty(item.getStaffCode())){
	        	g.drawString("NV: " + item.getStaffCode(), 10, imageSrc.getHeight() + (15+2)*i);i++;
	        }
	        if(item.getStaffCode()!=null){
	        	g.drawString("TG: " + DateUtil.toDateString(item.getCreateDate()), 10, imageSrc.getHeight() + (15+2)*i);i++;
	        }
	        g.dispose();		        
	        OutputStream os = new FileOutputStream(outputFile);
	        
	        Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");   	 
	        ImageWriter writer = (ImageWriter) writers.next();
	        ImageOutputStream ios = ImageIO.createImageOutputStream(os);
	        writer.setOutput(ios);
	 
	        ImageWriteParam param = writer.getDefaultWriteParam();
	 
	        // compress to a given quality
	        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	        param.setCompressionQuality(0.6f);
	        writer.write(null, new IIOImage(newImage, null, null), param);    	        
	        os.close();
	        ios.close();
	        writer.dispose();
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
			return "";
		}
		return imgDes;
	}
	
	/**
	 * Download image.
	 *
	 * @return the string
	 */
	public String downloadImage(){
		try {
			String displayCode="";
			if(displayProgrameId!=null){
				StDisplayProgram stDisplayProgram = displayProgrameMgr.getStDisplayProgramById(displayProgrameId);
				if(stDisplayProgram!=null){
					displayCode=stDisplayProgram.getDisplayProgramCode();
				}
				if(!StringUtil.isNullOrEmpty(displayCode) && !StringUtil.isNullOrEmpty(monthSeqDownload)){
					displayCode+=" ( "+Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"export.album.seq");
					displayCode+=" "+monthSeqDownload+" )";
				}
			}
			BufferedImage img;
			String type="jpeg";			
			MediaItem mi=null;
			if(id!=null){
				 mi= imageMgr.getMediaItemById(id);
				 if(null!=mi){
					 if(null!=mi.getObjectId()){
						 Customer cust = customerMgr.getCustomerById(mi.getObjectId());
						 if(cust!=null) 
							 customerInfo=cust.getShortCode()+" - "+cust.getCustomerName()+" - "+cust.getHousenumber()+","+cust.getStreet();
					 }
					 if(null!=mi.getShop()){
						 shopInfo = codeNameDisplay(mi.getShop().getShopCode(), mi.getShop().getShopName());
					 }
					 if(null!=mi.getStaff()){
						 staffInfo = codeNameDisplay(mi.getStaff().getStaffCode(), mi.getStaff().getStaffName());
					 }
				 }
			}
			String imgDes = addLabelToImage(urlImage,displayCode, customerInfo, shopInfo, staffInfo, dateInfo,overwrite);
			if(!StringUtil.isNullOrEmpty(imgDes)){
				File fileImageDes=new File(Configuration.getImageRealSOPath()+imgDes);
				img = ImageIO.read(fileImageDes);
				if(FileUtility.getMime(fileImageDes)!=null){
					type=FileUtility.getMime(fileImageDes);
				}
			    String imgstr;
			    if(type.equals("image/png")){
			    	imgstr = encodeToString(img, "png");
			    	result.put("errMsg", "data:image/png;base64,"+imgstr);
			    }else{
			    	imgstr = encodeToString(img, "jpeg");
			    	result.put("errMsg", "data:image/jpeg;base64,"+imgstr);
			    }
			    result.put(ERROR, false);
			} else {
				result.put(ERROR, true);
			}			
		} catch (IOException e) {
			result.put(ERROR, true);
		} catch (BusinessException e) {
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Delete image.
	 *
	 * @return the string
	 */
	public String deleteImage(){
		try{
			if(id!=null && id!=0){
				imageMgr.deleteImageVO(id);
				result.put(ERROR, false);
			}else{
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		} catch (BusinessException e) {
			LogUtility.logError(e,"ImagesAction.deleteImage");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Rotate image.
	 *
	 * @return the string
	 */
	public String rotateImage(){
		try{
			if(id!=null && id!=0){
				MediaItemVO item = imageMgr.getMediaItemVOById(id);
				String realPathItem = Configuration.getImageRealSOPath() + item.getUrl();
				String realPathThumbnail = Configuration.getImageRealSOPath() + item.getThumbUrl();
				ImageUtility.rotateImage(realPathItem);
				ImageUtility.rotateImage(realPathThumbnail);
				String serverPathItem = Configuration.getImgServerSOPath(item.getUrl()); //String serverPathItem = Configuration.getImgServerPath(item.getUrl());
				String serverPathThumbnail = Configuration.getImgServerSOPath(item.getThumbUrl()); //String serverPathThumbnail = Configuration.getImgServerPath(item.getThumbUrl());
				result.put(ERROR, false);
				result.put("url", serverPathItem);
				result.put("thumbnailUrl", serverPathThumbnail);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		} catch (Exception e) {
			LogUtility.logError(e,e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Gets the list nvgs by shop.
	 *
	 * @return the list nvgs by shop
	 */
	public String getListNVGSByShop() {		
		try {
			if(shopId == null || shopId == 0) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				if(shop != null) {
					shopId = shop.getId();
				}
			}
			lstStaff = new ArrayList<StaffSimpleVO>();
			//ObjectVO<StaffSimpleVO> listVo = staffMgr.getListNVGSByShopEx(null, null, null, shopId);
			//lstStaff = listVo.getLstObject();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		
		return JSON;
	}
	
	/**
	 * Gets the list nvb hby shop and gsnpp.
	 *
	 * @return the list nvb hby shop and gsnpp
	 * @throws BusinessException the business exception
	 */
	public String getListNVBHbyShopAndGSNPP() throws BusinessException{
		String gsnppCode = null;
		List<String> lstOwnerCode = null;
		if(gsnppId == null || gsnppId==-1) gsnppId = null; //tat ca
		if(gsnppId!=null){
			gsnppCode = staffMgr.getStaffById(gsnppId).getStaffCode();
			lstOwnerCode = new ArrayList<String>();
			lstOwnerCode.add(gsnppCode);
		}
		if(shopId == null || shopId == 0) {
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			if(shop != null) {
				shopId = shop.getId();
			}
		}
		lstNVBH = new ArrayList<StaffSimpleVO>();
		/*ObjectVO<StaffSimpleVO> temp = staffMgr.getListNVBHEx(null, !StringUtil.isNullOrEmpty(staffCodeSearch) ? staffCodeSearch.trim() : staffCodeSearch, null, gsnppCode, shopId);
		if (temp!=null){
			lstNVBH = temp.getLstObject();
		}*/
		return JSON;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 */
	public String search(){
		try{
			ImageFilter filterAlbum = setFilterForPopup();
			filterAlbum.setkPaging(null);
			if(!DateUtil.check2MonthAfter(fromDate)) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_IMAGES_CHECK_DATE);
			} else {
				ObjectVO<ImageVO> temp = imageMgr.getListCTTB(filterAlbum);
				if (temp!=null){
					lstAlbum =temp.getLstObject();
				}
			} 
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Show album.
	 *
	 * @return the string
	 */
	public String showAlbum() {
		try {
			// List Album
			KPaging<ImageVO> kPagingAlbum = new KPaging<ImageVO>();
			kPagingAlbum.setPageSize(max);
			kPagingAlbum.setPage(0);
			ImageFilter filterAlbum = setFilterForPopup();
			filterAlbum.setLstDisplayPrograme(lstCTTB);
			filterAlbum.setkPaging(kPagingAlbum);

			ObjectVO<ImageVO> lstAlbumVO = imageMgr.getListCTTB(filterAlbum);
			if(lstAlbumVO != null) {
				lstAlbum = lstAlbumVO.getLstObject();
			}

			ImageFilter filter = setFilterForPopup();
			filter.setkPaging(kPagingAlbum);
			ObjectVO<ImageVO> lstImageVO = imageMgr.getListImageVO(filter);
			if(lstImageVO != null) {
				lstImageOfAlbum = lstImageVO.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Gets the filter customer code.
	 *
	 * @return the filter customer code
	 * @throws BusinessException the business exception
	 */
	public ImageFilter getFilterCustomerCode() throws BusinessException {
		ImageFilter filter = new ImageFilter();
		if (!StringUtil.isNullOrEmpty(customerCode)) {
			if (customerCode.indexOf("_") >= 0) {
				filter.setCodeCustomer(customerCode);
			} else {
				filter.setShortCode(customerCode);
//				if (customerCode != null && customerCode.trim().length() > 3) {
//					String shopSubtring = customerCode.substring(0, customerCode.length() - 3);
//					if (!StringUtil.isNullOrEmpty(shopSubtring)) {
//						Shop shop = shopMgr.getShopByCode(shopSubtring);
//						if (shop != null) {
//							filter.setShopId(shop.getId());
//							filter.setShortCode(customerCode.substring(customerCode.length() - 3));
//						}
//					}
//				}
			}
		}
		return filter;
	}
	
	/**
	 * Show album detail.
	 *
	 * @return the string
	 */
	public String showAlbumDetail() {
		try {
			ImageFilter filter = setFilterForPopup();
			ObjectVO<ImageVO> lstImageVO = imageMgr.getListImageVO(filter);
			if(lstImageVO != null && lstImageVO.getLstObject()!=null && lstImageVO.getLstObject().size()>0) {
				lstImageOfAlbum = lstImageVO.getLstObject();
				if(filter.getObjectType()== 5 && lstImageOfAlbum!=null && lstImageOfAlbum.size()>0){
					customerCode = lstImageOfAlbum.get(0).getDisplayProgrameCode();
				}
			} else {
				isEmptyImage=true;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Adds the list album select.
	 *
	 * @return the string
	 */
	public String addListAlbumSelect() {
		result.put(ERROR, false);
		try {
			ImageFilter filterGroup = setFilterForPopup();
			ObjectVO<ImageVO> lstImageVO = null;
			lstImageVO = imageMgr.getListImageVOGroup(filterGroup);
			if(lstImageVO!=null) lstAlbum=lstImageVO.getLstObject();
			if(lstAlbum!=null && lstAlbum.size()>0){
				if(typeGroup!=null && typeGroup==1) {
					if(lstAlbum.get(0)!=null){
						id=lstAlbum.get(0).getStaffId();
						customerCode = lstAlbum.get(0).getStaffCode()+" - "+lstAlbum.get(0).getStaffName();
					}
					for(ImageVO temp:lstAlbum){
						temp.setCustomerCode(temp.getStaffCode());
						temp.setCustomerName(temp.getStaffName());
						temp.setCustomerId(temp.getStaffId());
					}
				}else{
					if(lstAlbum.get(0)!=null){
						id=lstAlbum.get(0).getCustomerId();
						customerCode = lstAlbum.get(0).getCustomerCode()+" - "+lstAlbum.get(0).getCustomerName();
					}
					for(ImageVO temp:lstAlbum){
						temp.setCustomerCode(temp.getShopCode()+" - "+temp.getCustomerCode());
					}
				}
			}else isEmptyImage=true;
			result.put("lstAlbum", lstAlbum);
			result.put("typeGroup", typeGroup);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Adds the album select.
	 *
	 * @return the string
	 */
	public String addAlbumSelect() {
		result.put(ERROR, false);
		try {
			ObjectVO<ImageVO> lstImageDetailVO = null;
			ImageFilter filter = setFilterForPopup();
			lstImageDetailVO = imageMgr.getListImageVO(filter);
			if(lstImageDetailVO!=null) lstImageOfAlbum=lstImageDetailVO.getLstObject();
			if(typeGroup!=null && lstImageOfAlbum!=null && lstImageOfAlbum.size()>0) {
				if(typeGroup==1) result.put("title",lstImageOfAlbum.get(0).getStaffCode()+" - "+lstImageOfAlbum.get(0).getStaffName());
				else result.put("title",lstImageOfAlbum.get(0).getShopCode()+" - "+lstImageOfAlbum.get(0).getCustomerCode()+" - "+lstImageOfAlbum.get(0).getCustomerName());
				
			}
			result.put("lstImage", lstImageOfAlbum);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Show album select.
	 *
	 * @return the string
	 */
	public String showAlbumSelect() {
		try {
			ImageFilter filterGroup = setFilterForPopup();
			ObjectVO<ImageVO> lstImageVO = null;
			ObjectVO<ImageVO> lstImageDetailVO = null;
			lstImageVO = imageMgr.getListImageVOGroup(filterGroup);
			if(lstImageVO!=null) lstAlbum=lstImageVO.getLstObject();
			
			if(lstAlbum!=null && lstAlbum.size()>0){
				ImageFilter filter = setFilterForPopup();
				if(typeGroup!=null && typeGroup==1) {
					if(lstAlbum.get(0)!=null){
						filter.setStaffIdGroup(lstAlbum.get(0).getStaffId());
						id=lstAlbum.get(0).getStaffId();
						customerCode = lstAlbum.get(0).getStaffCode()+" - "+lstAlbum.get(0).getStaffName();
					}else filter.setStaffIdGroup(null);
					for(ImageVO temp:lstAlbum){
						temp.setCustomerCode(temp.getStaffCode());
						temp.setCustomerName(temp.getStaffName());
						temp.setCustomerId(temp.getStaffId());
					}
				}else{
					if(lstAlbum.get(0)!=null){
						filter.setIdCustomer(lstAlbum.get(0).getCustomerId());
						id=lstAlbum.get(0).getCustomerId();
						customerCode = lstAlbum.get(0).getCustomerCode()+" - "+lstAlbum.get(0).getCustomerName();
					}else filter.setIdCustomer(null);
					for(ImageVO temp:lstAlbum){
						temp.setCustomerCode(temp.getShopCode()+" - "+temp.getCustomerCode());
					}
				}
				lstImageDetailVO = imageMgr.getListImageVO(filter);
				if(lstImageDetailVO!=null){
					lstImageOfAlbum=lstImageDetailVO.getLstObject();
				}
				if(typeGroup!=null && lstImageOfAlbum!=null && lstImageOfAlbum.size()>0) {
					if(typeGroup==1) {
						titleAlbum = lstImageOfAlbum.get(0).getStaffCode()+" - "+lstImageOfAlbum.get(0).getStaffName();
					}else {
						titleAlbum = lstImageOfAlbum.get(0).getShopCode()+" - "+lstImageOfAlbum.get(0).getCustomerCode()+" - "+lstImageOfAlbum.get(0).getCustomerName();
					}
					
				}
			}else isEmptyImage=true;
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Export album.
	 *
	 * @return the string
	 */
	public String exportAlbum() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			List<String> lstUrlImage = new ArrayList<String>();
			List<String> lstUrlImageInfo = new ArrayList<String>();
			ImageFilter filter =setFilterForPopup();
			filter.setkPaging(null);
			ObjectVO<ImageVO> lstImageOfAlbumVO = imageMgr.getListImageVO(filter);
			String displayCode="";
			if(lstImageOfAlbumVO != null) {
				for(ImageVO imageVO : lstImageOfAlbumVO.getLstObject()) {
					lstUrlImage.add(imageVO.getUrlImage());
					String sbDisplayProgram = new String(displayCode);
					/*if(imageVO.getMonthSeq()!=null && !StringUtil.isNullOrEmpty(displayCode)){
						sbDisplayProgram+=" ( "+Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"export.album.seq");
						sbDisplayProgram+=" "+imageVO.getMonthSeq()+" )";
					}*/
					StringBuilder sbCustomerInfo = new StringBuilder("(")
							.append(imageVO.getCustomerCode())
							.append(" - ").append(imageVO.getCustomerName())
							.append(" - ").append(imageVO.getCustomerAddress())
							.append(")");
					StringBuilder sbShopInfo = new StringBuilder(
							imageVO.getShopCode()).append(" - ").append(
							imageVO.getShopName());
					StringBuilder sbStaffInfo = new StringBuilder(
							imageVO.getStaffCode()).append(" - ").append(
							imageVO.getStaffName());
					String createDateString = "";
					if(imageVO.getCreateDate()!= null){
						createDateString = DateUtil.toDateString(imageVO.getCreateDate(), DateUtil.DATE_FORMAT_NOW);
					}
					String imgInfo = addLabelToImage(imageVO.getUrlImage(),sbDisplayProgram,
							sbCustomerInfo.toString(), sbShopInfo.toString(),
							sbStaffInfo.toString(), createDateString,false);
					if(!StringUtil.isNullOrEmpty(imgInfo)){
						lstUrlImageInfo.add(imgInfo);
					} else {
						lstUrlImageInfo.add(imageVO.getUrlImage());
					}
				}
			}
			if(lstUrlImageInfo.size()> 0) {
				String outputDownload = ImageUtility.exportAlbum(lstUrlImageInfo, displayCode);
				result.put(ERROR, false);
				result.put(LIST, outputDownload);
				result.put("hasData", true);
				MemcachedUtils.putValueToMemcached(reportToken, outputDownload, retrieveReportMemcachedTimeout());
			} else{
				result.put("hasData", false);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ImagesAction.exportAlbum(): " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	
	/**
	 * Gets the time for file.
	 *
	 * @return the time for file
	 */
	public String getTimeForFile(){
		Calendar cal = Calendar.getInstance();
		Integer tmpM = cal.get(Calendar.MONTH) +1;
		String dd = String.valueOf(cal.get(Calendar.DATE)>10? cal.get(Calendar.DATE):"0"+cal.get(Calendar.DATE));
		String MM = String.valueOf(tmpM>10?tmpM:"0"+tmpM);
		String hh = String.valueOf(cal.get(Calendar.HOUR_OF_DAY)>10?cal.get(Calendar.HOUR_OF_DAY):"0"+cal.get(Calendar.HOUR_OF_DAY));
		String mm = String.valueOf(cal.get(Calendar.MINUTE)>10?cal.get(Calendar.MINUTE):"0"+cal.get(Calendar.MINUTE));
		String ss = String.valueOf(cal.get(Calendar.SECOND)>10?cal.get(Calendar.SECOND):"0"+cal.get(Calendar.SECOND));
        String file = ""+dd+MM+cal.get(Calendar.YEAR)+hh+mm+ss + Long.toString(cal.getTimeInMillis());
        return file;
	}
	
	/**
	 * Gets the file name.
	 *
	 * @param item the item
	 * @return the file name
	 */
	public String getFileName(MediaItemDetailVO item){
		String file="";
		if(!StringUtil.isNullOrEmpty(item.getCustomerCode())) file+=item.getCustomerCode();
		if(!StringUtil.isNullOrEmpty(item.getDisplayProgramCode())) file+="_"+item.getDisplayProgramCode();
		if(!StringUtil.isNullOrEmpty(item.getStaffCode())) file+="_"+item.getStaffCode();
		if(item.getCreateDate()!=null) file+="_"+DateUtil.toDateString(item.getCreateDate()).replaceAll("/", "");
		if(!StringUtil.isNullOrEmpty(file)) file+=".jpg";
		return file;
	}
	
	/**
	 * Gets the item detail info.
	 *
	 * @param item the item
	 * @return the item detail info
	 */
	public MediaItemDetailVO getItemDetailInfo(MediaItemDetailVO item){
		if(!StringUtil.isNullOrEmpty(item.getCustomerCode()))
			item.setCustomerCode(item.getCustomerCode()+" - "+item.getCustomerName()+" - "+item.getCustomerAddress());
		else item.setCustomerCode("");
		if(!StringUtil.isNullOrEmpty(item.getShopCode()))
			item.setShopCode(item.getShopCode()+" - "+item.getShopName());
		else item.setShopCode("");
		if(!StringUtil.isNullOrEmpty(item.getStaffCode()))
			item.setStaffCode(item.getStaffCode()+" - "+item.getStaffName());
		else item.setStaffCode("");
		if(!StringUtil.isNullOrEmpty(item.getDisplayProgramCode())){
			if(item.getMonthSeq()!=null){
				item.setDisplayProgramCode(item.getDisplayProgramCode()+" ( "+Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"export.album.seq")
						+" "+item.getMonthSeq()+" )");
			}
		}else item.setDisplayProgramCode("");
		return item;
	}
	
	/**
	 * Export album ex.
	 *
	 * @return the string
	 */
	public String exportAlbumEx() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			List<Long> lstCustomerId = new ArrayList<Long>();
			for(String customerTemp:customerOrder){
				Customer temp = customerMgr.getCustomerByCode(customerTemp);
				if(temp!=null) lstCustomerId.add(temp.getId());
			}
			fromDate = "01/09/2013";
			toDate = "30/09/2013";
			Date fDate = DateUtil.parse(fromDate.trim(), ConstantManager.FULL_DATE_FORMAT);
			Date tDate = DateUtil.parse(toDate.trim(), ConstantManager.FULL_DATE_FORMAT);
			List<MediaItemShopVO> lstImageOfAlbumVO = imageMgr.getListMediaItemShopVO(lstCustomerId,fDate,tDate,null);
			String folder="";
			tuyen = 0;
			if(lstImageOfAlbumVO != null) {
				folder=Configuration.getImageRealSOPath()+"/DSKH_DAT_DS_ROT_HA_MR_T8/092013";
				if (!FileUtility.isFolderExist(folder)) {
		            FileUtility.createDirectory(folder);
		        }
				for(MediaItemShopVO shopVO : lstImageOfAlbumVO) {
					if(!StringUtil.isNullOrEmpty(shopVO.getShopCode())){
						String folderShop = folder +"/"+ shopVO.getShopCode();
						if (!FileUtility.isFolderExist(folderShop)) {
				            FileUtility.createDirectory(folderShop);
				        }
						System.out.println(shopVO.getShopCode());
						for(MediaItemCustomerVO cusVO:shopVO.getLstMediaItemCustomer()){
							if(!StringUtil.isNullOrEmpty(cusVO.getCustomerCode())){
								String folderCus=folderShop+"/"+cusVO.getCustomerCode().substring(0,3);
								if (!FileUtility.isFolderExist(folderCus)) {
						            FileUtility.createDirectory(folderCus);
						        }								
								for(MediaItemProgramVO displayVO:cusVO.getLstMediaItemProgram()){
									if(!StringUtil.isNullOrEmpty(displayVO.getDisplayProgramCode())){
										String folderDP=folderCus+"/"+displayVO.getDisplayProgramCode();
										if (!FileUtility.isFolderExist(folderDP)) {
								            FileUtility.createDirectory(folderDP);
								        }
										for(MediaItemDetailVO itemVO:displayVO.getLstMediaItem()){
											if(!StringUtil.isNullOrEmpty(itemVO.getUrl())){
												String itemImage=folderDP+"/"+getFileName(itemVO);
												itemVO = getItemDetailInfo(itemVO);
												addLabelToImageEx(itemVO,itemImage,false);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if(!StringUtil.isNullOrEmpty(folder)) {
				String outputFile = ImageUtility.getOutputFileZip("Album");
				FileUtility.zipFolder(folder, outputFile);
				result.put(ERROR, false);
				result.put(LIST, outputFile);
				result.put("hasData", true);
				MemcachedUtils.putValueToMemcached(reportToken, outputFile, retrieveReportMemcachedTimeout());
			} else{
				result.put("hasData", false);
			}
			System.out.println("success................");
		} catch (Exception e) {
			LogUtility.logError(e, "ImagesAction.exportAlbum(): " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	
	/**
	 * Lay nhan vien theo acc dang nhap va shop duoc chon luc dang nhap. 
	 * @author tulv2
	 * @since 18.10.2014
	 * */
	public String getListStaffForShop(){
		lstStaff = new ArrayList<StaffSimpleVO>();
		try {
		    if (currentUser != null && currentUser.getUserId() != null && currentUser.getRoleToken() != null 
		    		&& currentUser.getRoleToken().getRoleId() != null && currentUser.getShopRoot() != null && currentUser.getShopRoot().getShopId() != null) {
		    	List<Integer> lstObjectType = new ArrayList<Integer>();
		    	//lstObjectType.add(StaffObjectType.TBHV.getValue());
		    	//lstObjectType.add(StaffObjectType.NVGS.getValue());
		    	//lstObjectType.add(StaffObjectType.NVBH.getValue());
		    	//lstObjectType.add(StaffObjectType.NVVS.getValue());
		    	
		    	if (lstShop == null || lstShop.size() == 0) {
		    		lstShop = new ArrayList<Long>();
		    		lstShop.add(currentUser.getShopRoot().getShopId());
		    	}
		    	StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
		    	filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
		    	filter.setLstShopId(lstShop);
		    	filter.setLstObjectType(lstObjectType);
		    	filter.setIsLstChildStaffRoot(true);
		    	filter.setStaffIdListStr(getStrListUserId());
		    	//ObjectVO<StaffVO> objectStaffVOs = staffMgr.searchListStaffVOByFilter(filter);
		    	//fix loi qua 1000 tham so nv, shop
		    	filter.setUserId(currentUser.getUserId());
		    	filter.setShopId(currentUser.getShopRoot().getShopId());
		    	filter.setRoleId(currentUser.getRoleToken().getRoleId());
		    	
		    	ObjectVO<StaffVO> objectStaffVOs = staffMgr.searchListStaffVOByShop(filter);
		    	if (objectStaffVOs != null && objectStaffVOs.getLstObject() != null && objectStaffVOs.getLstObject().size() > 0) {
		    		List<StaffVO> lstStaffVO = objectStaffVOs.getLstObject(); 
		    		for (int i = 0, size = lstStaffVO.size(); i < size; i++) {
		    			StaffSimpleVO staffSimpleVO = new StaffSimpleVO();
		    			staffSimpleVO.setStaffId(lstStaffVO.get(i).getStaffId());
		    			staffSimpleVO.setStaffCode(lstStaffVO.get(i).getStaffCode());
		    			staffSimpleVO.setStaffName(lstStaffVO.get(i).getStaffName());
		    			lstStaff.add(staffSimpleVO);
		    		}
		    	}
		    }
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	
	
	/**
	 * Ham lay danh sach CTTB
	 * @author tientv11
	 */
	
	public String getListCTTBbyShopId() {
		try {
			lstCTTBVOs = new ArrayList<ImageVO>();
			if (shopId != null) {
				lstCTTBVOs = imageMgr.getListCTTBByNPP(this.shopId);
			} else if (this.currentUser != null) {
				Staff s = this.staffMgr.getStaffByCode(this.currentUser.getUserName());
				if ((s != null) && (s.getShop() != null)) {
					this.shopId = s.getShop().getId();
					this.lstCTTBVOs = imageMgr.getListCTTBByNPP(this.shopId);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	/**
	 * Lay DS CTTB theo list Shop
	 * @author tientv11
	 * @return
	 */
	public String getListCTTBbyListShop() {
		try {
			ImageMgr imageMgr = (ImageMgr) this.context.getBean("imageMgr");
			this.lstCTTBVOs = new ArrayList<ImageVO>();
			if (this.currentUser != null) {
				/** author update hoanv25, since 3/07/2015 */
				if ((this.lstShopId != null) && (this.lstShopId.size() > 0)) {
					this.lstCTTBVOs = imageMgr.getListCTTBByListShop(this.lstShopId);
				} else {
					this.lstCTTBVOs = imageMgr.getListCTTBByNPP(currentUser.getShopRoot().getShopId());
				}
				/*Staff s = this.staffMgr.getStaffByCode(this.currentUser.getUserName());
				if ((s != null) && (s.getShop() != null)) {
					this.shopId = s.getShop().getId();
					if ((this.lstShopId != null) && (this.lstShopId.size() > 0)) {
						this.lstCTTBVOs = imageMgr.getListCTTBByListShop(this.lstShopId);
					} else {
						this.lstCTTBVOs = imageMgr.getListCTTBByNPP(this.shopId);
					}
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}
	
	/**
	 * Gets the gsnpp id.
	 *
	 * @return the gsnpp id
	 */
	public Long getGsnppId() {
		return gsnppId;
	}
	
	/**
	 * Gets the from date.
	 *
	 * @return the from date
	 */
	public String getFromDate() {
		return fromDate;
	}
	
	/**
	 * Sets the from date.
	 *
	 * @param fromDate the new from date
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	
	/**
	 * Gets the to date.
	 *
	 * @return the to date
	 */
	public String getToDate() {
		return toDate;
	}
	
	/**
	 * Sets the to date.
	 *
	 * @param toDate the new to date
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	/**
	 * Gets the tuyen.
	 *
	 * @return the tuyen
	 */
	public Integer getTuyen() {
		return tuyen;
	}
	
	/**
	 * Sets the tuyen.
	 *
	 * @param tuyen the new tuyen
	 */
	public void setTuyen(Integer tuyen) {
		this.tuyen = tuyen;
	}
	
	/**
	 * Gets the customer code.
	 *
	 * @return the customer code
	 */
	public String getCustomerCode() {
		return customerCode;
	}
	
	/**
	 * Sets the customer code.
	 *
	 * @param customerCode the new customer code
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	/**
	 * Gets the customer name or address.
	 *
	 * @return the customer name or address
	 */
	public String getCustomerNameOrAddress() {
		return customerNameOrAddress;
	}
	
	/**
	 * Sets the customer name or address.
	 *
	 * @param customerNameOrAddress the new customer name or address
	 */
	public void setCustomerNameOrAddress(String customerNameOrAddress) {
		this.customerNameOrAddress = customerNameOrAddress;
	}
	
	/**
	 * Gets the nvbh id.
	 *
	 * @return the nvbh id
	 */
	public Long getNvbhId() {
		return nvbhId;
	}
	
	/**
	 * Sets the nvbh id.
	 *
	 * @param nvbhId the new nvbh id
	 */
	public void setNvbhId(Long nvbhId) {
		this.nvbhId = nvbhId;
	}
	
	/**
	 * Sets the gsnpp id.
	 *
	 * @param gsnppId the new gsnpp id
	 */
	public void setGsnppId(Long gsnppId) {
		this.gsnppId = gsnppId;
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#getMax()
	 */
	public int getMax() {
		return max;
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#setMax(int)
	 */
	public void setMax(int max) {
		this.max = max;
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#getPage()
	 */
	public int getPage() {
		return page;
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#setPage(int)
	 */
	public void setPage(int page) {
		this.page = page;
	}
	
	/**
	 * Gets the lst image of album.
	 *
	 * @return the lst image of album
	 */
	public List<ImageVO> getLstImageOfAlbum() {
		return lstImageOfAlbum;
	}
	
	/**
	 * Gets the checks if is group.
	 *
	 * @return the checks if is group
	 */
	public Boolean getIsGroup() {
		return isGroup;
	}
	
	/**
	 * Sets the checks if is group.
	 *
	 * @param isGroup the new checks if is group
	 */
	public void setIsGroup(Boolean isGroup) {
		this.isGroup = isGroup;
	}
	
	/**
	 * Sets the lst image of album.
	 *
	 * @param lstImageOfAlbum the new lst image of album
	 */
	public void setLstImageOfAlbum(List<ImageVO> lstImageOfAlbum) {
		this.lstImageOfAlbum = lstImageOfAlbum;
	}
	
	/**
	 * Gets the lst display programe id.
	 *
	 * @return the lst display programe id
	 */
	public List<Long> getLstDisplayProgrameId() {
		return lstDisplayProgrameId;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the lst display programe id.
	 *
	 * @param lstDisplayProgrameId the new lst display programe id
	 */
	public void setLstDisplayProgrameId(List<Long> lstDisplayProgrameId) {
		this.lstDisplayProgrameId = lstDisplayProgrameId;
	}
	
	/**
	 * Gets the display programe id.
	 *
	 * @return the display programe id
	 */
	public Long getDisplayProgrameId() {
		return displayProgrameId;
	}
	
	/**
	 * Sets the display programe id.
	 *
	 * @param displayProgrameId the new display programe id
	 */
	public void setDisplayProgrameId(Long displayProgrameId) {
		this.displayProgrameId = displayProgrameId;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the lst cttb.
	 *
	 * @return the lst cttb
	 */
	public List<Long> getLstCTTB() {
		return lstCTTB;
	}
	
	/**
	 * Sets the lst cttb.
	 *
	 * @param lstCTTB the new lst cttb
	 */
	public void setLstCTTB(List<Long> lstCTTB) {
		this.lstCTTB = lstCTTB;
	}
	
	/**
	 * Sets the lst album.
	 *
	 * @param lstAlbum the new lst album
	 */
	public void setLstAlbum(List<ImageVO> lstAlbum) {
		this.lstAlbum = lstAlbum;
	}
	
	/**
	 * Gets the lst album.
	 *
	 * @return the lst album
	 */
	public List<ImageVO> getLstAlbum() {
		return lstAlbum;
	}
	
	/**
	 * Gets the customer info.
	 *
	 * @return the customer info
	 */
	public String getCustomerInfo() {
		return customerInfo;
	}


	/**
	 * Sets the customer info.
	 *
	 * @param customerInfo the new customer info
	 */
	public void setCustomerInfo(String customerInfo) {
		this.customerInfo = customerInfo;
	}


	/**
	 * Gets the shop info.
	 *
	 * @return the shop info
	 */
	public String getShopInfo() {
		return shopInfo;
	}


	/**
	 * Sets the shop info.
	 *
	 * @param shopInfo the new shop info
	 */
	public void setShopInfo(String shopInfo) {
		this.shopInfo = shopInfo;
	}


	/**
	 * Gets the staff info.
	 *
	 * @return the staff info
	 */
	public String getStaffInfo() {
		return staffInfo;
	}


	/**
	 * Sets the staff info.
	 *
	 * @param staffInfo the new staff info
	 */
	public void setStaffInfo(String staffInfo) {
		this.staffInfo = staffInfo;
	}


	/**
	 * Gets the date info.
	 *
	 * @return the date info
	 */
	public String getDateInfo() {
		return dateInfo;
	}


	/**
	 * Sets the date info.
	 *
	 * @param dateInfo the new date info
	 */
	public void setDateInfo(String dateInfo) {
		this.dateInfo = dateInfo;
	}


	/**
	 * Gets the url image.
	 *
	 * @return the url image
	 */
	public String getUrlImage() {
		return urlImage;
	}
	
	/**
	 * Sets the url image.
	 *
	 * @param urlImage the new url image
	 */
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	
	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public Long getCustomerId() {
		return customerId;
	}
	
	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
	/**
	 * Sets the staff_role.
	 *
	 * @param staff_role the new staff_role
	 */
	public void setStaff_role(Integer staff_role) {
		this.staff_role = staff_role;
	}
	
	/**
	 * Gets the staff_role.
	 *
	 * @return the staff_role
	 */
	public Integer getStaff_role() {
		return staff_role;
	}


	/**
	 * Checks if is overwrite.
	 *
	 * @return true, if is overwrite
	 */
	public boolean isOverwrite() {
		return overwrite;
	}


	/**
	 * Sets the overwrite.
	 *
	 * @param overwrite the new overwrite
	 */
	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}


	/**
	 * Gets the product mgr.
	 *
	 * @return the product mgr
	 */
	public ProductMgr getProductMgr() {
		return productMgr;
	}


	/**
	 * Sets the product mgr.
	 *
	 * @param productMgr the new product mgr
	 */
	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	/**
	 * Gets the common mgr.
	 *
	 * @return the common mgr
	 */
	public CommonMgr getCommonMgr() {
		return commonMgr;
	}


	/**
	 * Sets the common mgr.
	 *
	 * @param commonMgr the new common mgr
	 */
	public void setCommonMgr(CommonMgr commonMgr) {
		this.commonMgr = commonMgr;
	}


	/**
	 * Gets the staff mgr.
	 *
	 * @return the staff mgr
	 */
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}


	/**
	 * Sets the staff mgr.
	 *
	 * @param staffMgr the new staff mgr
	 */
	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}


	/**
	 * Gets the shop mgr.
	 *
	 * @return the shop mgr
	 */
	public ShopMgr getShopMgr() {
		return shopMgr;
	}


	/**
	 * Sets the shop mgr.
	 *
	 * @param shopMgr the new shop mgr
	 */
	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}


	/**
	 * Gets the customer mgr.
	 *
	 * @return the customer mgr
	 */
	public CustomerMgr getCustomerMgr() {
		return customerMgr;
	}


	/**
	 * Sets the customer mgr.
	 *
	 * @param customerMgr the new customer mgr
	 */
	public void setCustomerMgr(CustomerMgr customerMgr) {
		this.customerMgr = customerMgr;
	}


	/**
	 * Gets the image mgr.
	 *
	 * @return the image mgr
	 */
	public ImageMgr getImageMgr() {
		return imageMgr;
	}


	/**
	 * Sets the image mgr.
	 *
	 * @param imageMgr the new image mgr
	 */
	public void setImageMgr(ImageMgr imageMgr) {
		this.imageMgr = imageMgr;
	}


	/**
	 * Gets the shop.
	 *
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}


	/**
	 * Sets the shop.
	 *
	 * @param shop the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}


	/**
	 * Gets the lst nvbh.
	 *
	 * @return the lst nvbh
	 */
	public List<StaffSimpleVO> getLstNVBH() {
		return lstNVBH;
	}
	

	/**
	 * Gets the lst staff.
	 *
	 * @return the lst staff
	 */
	public List<StaffSimpleVO> getLstStaff() {
		return lstStaff;
	}


	/**
	 * Sets the lst nvbh.
	 *
	 * @param lstNVBH the new lst nvbh
	 */
	public void setLstNVBH(List<StaffSimpleVO> lstNVBH) {
		this.lstNVBH = lstNVBH;
	}


	/**
	 * Gets the staff code search.
	 *
	 * @return the staff code search
	 */
	public String getStaffCodeSearch() {
		return staffCodeSearch;
	}


	/**
	 * Sets the staff code search.
	 *
	 * @param staffCodeSearch the new staff code search
	 */
	public void setStaffCodeSearch(String staffCodeSearch) {
		this.staffCodeSearch = staffCodeSearch;
	}


	/**
	 * Sets the lst staff.
	 *
	 * @param lstStaff the new lst staff
	 */
	public void setLstStaff(List<StaffSimpleVO> lstStaff) {
		this.lstStaff = lstStaff;
	}


	
	/**
	 * Gets the object type.
	 *
	 * @return the object type
	 */
	public Integer getObjectType() {
		return objectType;
	}


	/**
	 * Sets the object type.
	 *
	 * @param objectType the new object type
	 */
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}


	/**
	 * Gets the lst shop.
	 *
	 * @return the lst shop
	 */
	public List<Long> getLstShop() {
		return lstShop;
	}


	/**
	 * Sets the lst shop.
	 *
	 * @param lstShop the new lst shop
	 */
	public void setLstShop(List<Long> lstShop) {
		this.lstShop = lstShop;
	}


	/**
	 * Gets the type group.
	 *
	 * @return the type group
	 */
	public Integer getTypeGroup() {
		return typeGroup;
	}


	/**
	 * Sets the type group.
	 *
	 * @param typeGroup the new type group
	 */
	public void setTypeGroup(Integer typeGroup) {
		this.typeGroup = typeGroup;
	}


	/**
	 * Gets the staff id group.
	 *
	 * @return the staff id group
	 */
	public Long getStaffIdGroup() {
		return staffIdGroup;
	}


	/**
	 * Sets the staff id group.
	 *
	 * @param staffIdGroup the new staff id group
	 */
	public void setStaffIdGroup(Long staffIdGroup) {
		this.staffIdGroup = staffIdGroup;
	}

	/**
	 * Gets the customer id group.
	 *
	 * @return the customer id group
	 */
	public Long getCustomerIdGroup() {
		return customerIdGroup;
	}

	/**
	 * Sets the customer id group.
	 *
	 * @param customerIdGroup the new customer id group
	 */
	public void setCustomerIdGroup(Long customerIdGroup) {
		this.customerIdGroup = customerIdGroup;
	}

	/**
	 * Gets the lst staff search.
	 *
	 * @return the lst staff search
	 */
	public List<Long> getLstStaffSearch() {
		return lstStaffSearch;
	}

	/**
	 * Sets the lst staff search.
	 *
	 * @param lstStaffSearch the new lst staff search
	 */
	public void setLstStaffSearch(List<Long> lstStaffSearch) {
		this.lstStaffSearch = lstStaffSearch;
	}

	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * Sets the level.
	 *
	 * @param level the new level
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	/**
	 * Gets the month seq.
	 *
	 * @return the month seq
	 */
	public List<Integer> getMonthSeq() {
		return monthSeq;
	}


	/**
	 * Sets the month seq.
	 *
	 * @param monthSeq the new month seq
	 */
	public void setMonthSeq(List<Integer> monthSeq) {
		this.monthSeq = monthSeq;
	}

	/**
	 * Gets the last seq.
	 *
	 * @return the last seq
	 */
	public Boolean getLastSeq() {
		return lastSeq;
	}

	/**
	 * Sets the last seq.
	 *
	 * @param lastSeq the new last seq
	 */
	public void setLastSeq(Boolean lastSeq) {
		this.lastSeq = lastSeq;
	}


	/**
	 * Gets the result img.
	 *
	 * @return the result img
	 */
	public Integer getResultImg() {
		return resultImg;
	}


	/**
	 * Sets the result img.
	 *
	 * @param resultImg the new result img
	 */
	public void setResultImg(Integer resultImg) {
		this.resultImg = resultImg;
	}


	/**
	 * Gets the lst staff for shop.
	 *
	 * @return the lst staff for shop
	 */
	public List<Staff> getLstStaffForShop() {
		return lstStaffForShop;
	}


	/**
	 * Sets the lst staff for shop.
	 *
	 * @param lstStaffForShop the new lst staff for shop
	 */
	public void setLstStaffForShop(List<Staff> lstStaffForShop) {
		this.lstStaffForShop = lstStaffForShop;
	}


	/**
	 * Gets the staff type.
	 *
	 * @return the staff type
	 */
	public Integer getStaffType() {
		return staffType;
	}


	/**
	 * Sets the staff type.
	 *
	 * @param staffType the new staff type
	 */
	public void setStaffType(Integer staffType) {
		this.staffType = staffType;
	}


	/**
	 * Gets the checks if is empty image.
	 *
	 * @return the checks if is empty image
	 */
	public Boolean getIsEmptyImage() {
		return isEmptyImage;
	}


	/**
	 * Sets the checks if is empty image.
	 *
	 * @param isEmptyImage the new checks if is empty image
	 */
	public void setIsEmptyImage(Boolean isEmptyImage) {
		this.isEmptyImage = isEmptyImage;
	}

	/**
	 * Gets the checks if is ctb.
	 *
	 * @return the checks if is ctb
	 */
	public Boolean getIsCTB() {
		return isCTB;
	}


	/**
	 * Sets the checks if is ctb.
	 *
	 * @param isCTB the new checks if is ctb
	 */
	public void setIsCTB(Boolean isCTB) {
		this.isCTB = isCTB;
	}


	/**
	 * Gets the month seq download.
	 *
	 * @return the month seq download
	 */
	public String getMonthSeqDownload() {
		return monthSeqDownload;
	}


	/**
	 * Sets the month seq download.
	 *
	 * @param monthSeqDownload the new month seq download
	 */
	public void setMonthSeqDownload(String monthSeqDownload) {
		this.monthSeqDownload = monthSeqDownload;
	}


	/**
	 * Gets the cttb order.
	 *
	 * @return the cttb order
	 */
	public String getCttbOrder() {
		return cttbOrder;
	}


	/**
	 * Sets the cttb order.
	 *
	 * @param cttbOrder the new cttb order
	 */
	public void setCttbOrder(String cttbOrder) {
		this.cttbOrder = cttbOrder;
	}


	/**
	 * Gets the customer order.
	 *
	 * @return the customer order
	 */
	public List<String> getCustomerOrder() {
		return customerOrder;
	}


	/**
	 * Sets the customer order.
	 *
	 * @param customerOrder the new customer order
	 */
	public void setCustomerOrder(List<String> customerOrder) {
		this.customerOrder = customerOrder;
	}


	/**
	 * Gets the title album.
	 *
	 * @return the title album
	 */
	public String getTitleAlbum() {
		return titleAlbum;
	}


	/**
	 * Sets the title album.
	 *
	 * @param titleAlbum the new title album
	 */
	public void setTitleAlbum(String titleAlbum) {
		this.titleAlbum = titleAlbum;
	}


	public List<ImageVO> getLstCTTBVOs() {
		return lstCTTBVOs;
	}


	public void setLstCTTBVOs(List<ImageVO> lstCTTBVOs) {
		this.lstCTTBVOs = lstCTTBVOs;
	}


	public List<Long> getLstShopId() {
		return lstShopId;
	}


	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	
	
}

package ths.dms.web.action.saleplan;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ShopObjectType;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class SaleDayPlanAction extends AbstractAction{
	private static final long serialVersionUID = -2128120975032582873L;
	
	/** The ap param mgr. */
	private ApParamMgr apParamMgr;
	
	/** The exception day mgr. */
	private ExceptionDayMgr exceptionDayMgr;
	
	private ShopMgr shopMgr;
	
	/** The lst reasons. */
	private List<ApParam> lstReasons;
	
	/** The date type. */
	private int dateType;
	
	/** The date. */
	private String dateValue;
	
	/** The reason id. */
	private Long reasonId;
	
	/** The lst year. */
	private List<StatusBean> lstYears;
	
	/** The current date. */
	private int currentYear;
		
	/** The current month. */
	private int currentMonth;
	
	/** The excel file. */
	private File excelFile;
	
	/** The excel file content type. */
	private String excelFileContentType;
	
	private int month;
	private int year;
	
	private String reasonCode;
	private String shopCodeStr;
	
	
	public String getShopCodeStr() {
		return shopCodeStr;
	}

	public void setShopCodeStr(String shopCodeStr) {
		this.shopCodeStr = shopCodeStr;
	}

	/**
	 * @return the lstReasons
	 */
	public List<ApParam> getLstReasons() {
		return lstReasons;
	}

	/**
	 * @param lstReasons the lstReasons to set
	 */
	public void setLstReasons(List<ApParam> lstReasons) {
		this.lstReasons = lstReasons;
	}

	/**
	 * @return the dateType
	 */
	public int getDateType() {
		return dateType;
	}

	/**
	 * @param dateType the dateType to set
	 */
	public void setDateType(int dateType) {
		this.dateType = dateType;
	}
	
	/**
	 * @return the reasonId
	 */
	public Long getReasonId() {
		return reasonId;
	}

	/**
	 * @param reasonId the reasonId to set
	 */
	public void setReasonId(Long reasonId) {
		this.reasonId = reasonId;
	}

	/**
	 * @return the dateValue
	 */
	public String getDateValue() {
		return dateValue;
	}

	/**
	 * @param dateValue the dateValue to set
	 */
	public void setDateValue(String dateValue) {
		this.dateValue = dateValue;
	}

	/**
	 * @return the lstYears
	 */
	public List<StatusBean> getLstYears() {
		return lstYears;
	}

	/**
	 * @param lstYears the lstYears to set
	 */
	public void setLstYears(List<StatusBean> lstYears) {
		this.lstYears = lstYears;
	}
	

	/**
	 * @return the currentYear
	 */
	public int getCurrentYear() {
		return currentYear;
	}

	/**
	 * @param currentYear the currentYear to set
	 */
	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}

	/**
	 * @return the currentMonth
	 */
	public int getCurrentMonth() {
		return currentMonth;
	}

	/**
	 * @param currentMonth the currentMonth to set
	 */
	public void setCurrentMonth(int currentMonth) {
		this.currentMonth = currentMonth;
	}
	
	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();		
		apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
		exceptionDayMgr = (ExceptionDayMgr)context.getBean("exceptionDayMgr");
		shopMgr = (ShopMgr)context.getBean("shopMgr");
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			mapControl = getMapControlToken();
			lstReasons = apParamMgr.getOffDateReason();
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(DateUtil.now());
			dateValue = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			currentYear = calendar.get(Calendar.YEAR);
			currentMonth = calendar.get(Calendar.MONTH) + 1;
			lstYears = new ArrayList<StatusBean>();
			for(int i=(currentYear-10);i<=(currentYear+10);i++){
				StatusBean year = new StatusBean();
				year.setName(String.valueOf(i));
				year.setValue(Long.valueOf(i));
				lstYears.add(year);
			}
			ExceptionDay excDay = exceptionDayMgr.getExceptionDayByDate(DateUtil.now(),-1L);
			if(excDay!= null){
				dateType = 1;
				/*if(excDay.getType()!= null){
					reasonCode = excDay.getType();
				}*/
			}
			ShopToken shToken = currentUser.getShopRoot();
			if (shToken != null
					&& (ShopObjectType.NPP.getValue().equals(shToken.getObjectType())
							|| ShopObjectType.NPP_KA.getValue().equals(shToken.getObjectType()))) {
				shopCodeStr = shToken.getShopCode();
			}
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}	
	
	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author phuongvm
	 * @since 06/10/2014
	 */
	public String saveOrUpdate(){
		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				Long shopId = -1L;
				Date excDate = null;
				Shop shopTemp = null;
				if (!StringUtil.isNullOrEmpty(dateValue)) {
					excDate = DateUtil.parse(dateValue, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (excDate == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
					return JSON;
				}
				if (DateUtil.compareDateWithoutTime(excDate, DateUtil.now()) < 1) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.set.up.day.off.past"));
					return JSON;
				}
				if (!StringUtil.isNullOrEmpty(shopCodeStr)) {
					shopTemp = shopMgr.getShopByCode(shopCodeStr);
					if (shopTemp == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.countinginput.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code")));
						return JSON;
					} else if (!checkShopInOrgAccessByCode(shopCodeStr)) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree")));
						return JSON;
					} else if (ActiveType.STOPPED.equals(shopTemp.getStatus())) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.promotion.effect", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code")));
						return JSON;
					} else {
						shopId = shopTemp.getId();
					}
				}
				Date sysDateC = commonMgr.getSysDate();
				ExceptionDay excDay = exceptionDayMgr.getExceptionDayByDate(excDate, shopId);
				ApParam reason = apParamMgr.getApParamByCode(reasonCode, ApParamType.OFFDATE);
				if (dateType == 0) {
					if (excDay != null) {
						excDay.setUpdateDate(sysDateC);
						excDay.setUpdateUser(currentUser.getUserName());
						exceptionDayMgr.deleteExceptionDay(excDay, getLogInfoVO());
						error = false;
					} else {
						error = false;
					}
				} else if (reason != null) {
					if (ApParamType.OFFDATE.equals(reason.getType())) {
						if (excDay != null) {
							//excDay.setType(reasonCode);
							excDay.setUpdateDate(sysDateC);
							excDay.setUpdateUser(currentUser.getUserName());
							exceptionDayMgr.updateExceptionDay(excDay, getLogInfoVO());
							error = false;
						} else {
							excDay = new ExceptionDay();
							excDay.setDayOff(excDate);
							if (shopTemp != null) {
								excDay.setShop(shopTemp);
							}
							excDay.setCreateDate(sysDateC);
							excDay.setCreateUser(currentUser.getUserName());
							exceptionDayMgr.createExceptionDay(excDay, getLogInfoVO());
							error = false;
						}
					} else {
						result.put(ERROR, true);
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.date", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.reason"));
						result.put("errMsg", errMsg);
						return JSON;
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	public String importExcel() throws Exception {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
		typeView = true;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 3);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						message = "";
						totalItem++;
						List<String> row = lstData.get(i);

						Shop shop = null;
						if (row.size() > 0) {
							String value = row.get(0);
							if (!StringUtil.isNullOrEmpty(value)) {
								shop = shopMgr.getShopByCode(value);
								if (shop == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.shop.code");
								} else if (!checkShopInOrgAccessByCode(value)) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, true, "catalog.unit.tree");
								} else if (!shop.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.shop.not.running");
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.focus.program.shop.code");
							}
						}
						//date
						Date saleDate = null;
						if (row.size() > 1) {
							String value = row.get(1);
							if (!StringUtil.isNullOrEmpty(value)) {
								try {
									if (!StringUtil.isNullOrEmpty(value)) {
										if (DateUtil.checkInvalidFormatDate(value)) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID_DATE, true, "sale.plan.exception.day");
										} else {
											saleDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
											if (saleDate != null && DateUtil.compareDateWithoutTime(DateUtil.now(), saleDate) > -1) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.greater.datenow.general", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.exception.day"));
											} else if (saleDate != null) {
												Calendar calendar = new GregorianCalendar();
												calendar.setTime(saleDate);
												if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.exception.sunday") + "\n";
												}
											}
										}
									}
								} catch (Exception e) {
									LogUtility.logError(e, e.getMessage());
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "sale.plan.exception.day");
							}
						}
						// reason
						ApParam reason = null;
						String reasonCode = "";
						if (row.size() > 2) {
							String value = row.get(2);
							if (!StringUtil.isNullOrEmpty(value)) {
								reason = apParamMgr.getApParamByCodeX(value, ApParamType.OFFDATE, null);
								if (reason == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "catalog.reason");
								} else if (!ActiveType.RUNNING.getValue().equals(reason.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.reason");
								} else {
									reasonCode = value;
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.reason");
							}
						}

						if (StringUtil.isNullOrEmpty(message)) {
							ExceptionDay excDay = exceptionDayMgr.getExceptionDayByDate(saleDate, shop.getId());
							if (excDay != null) {
								//excDay.setType(reasonCode.toUpperCase());
								excDay.setUpdateDate(DateUtil.now());
								excDay.setUpdateUser(currentUser.getUserName());
								exceptionDayMgr.updateExceptionDay(excDay, getLogInfoVO());
							} else {
								excDay = new ExceptionDay();
								excDay.setDayOff(saleDate);
								//excDay.setType(reasonCode.toUpperCase());
								excDay.setShop(shop);
								excDay.setCreateDate(DateUtil.now());
								excDay.setCreateUser(currentUser.getUserName());
								exceptionDayMgr.createExceptionDay(excDay, getLogInfoVO());
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
						typeView = false;
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_SALE_DAY_EXCEPTION_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	public String exportExcel(){
		try{
			List<ExceptionDay> lstDays = null;
			List<CellBean> lstDayOff = new ArrayList<CellBean>();
			Long shopId = -1L;
			if(!StringUtil.isNullOrEmpty(shopCode)){
				if (!checkShopInOrgAccessByCode(shopCode)) {
					result.put(ERROR,true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree")));
					return JSON;
				}
				Shop s = shopMgr.getShopByCode(shopCode);
				if(s!=null){
					shopId = s.getId();
				}
			}
			if(month > 0 && year > 0){
				lstDays = exceptionDayMgr.getExceptionDayByMonth(month, year,shopId);
			} else if(year > 0){
				lstDays = exceptionDayMgr.getExceptionDayByYear(year,shopId);
			}			
			if(lstDays!= null && !lstDays.isEmpty()){
				for (ExceptionDay excDay : lstDays) {
					if(excDay!= null){
						CellBean row = new CellBean();
						if(excDay.getShop() != null){
							row.setContent1(excDay.getShop().getShopCode());
						}
						row.setContent2(DateUtil.toDateString(excDay.getDayOff(), DateUtil.DATE_FORMAT_DDMMYYYY));
						/*if(excDay.getType()!= null){
							row.setContent3(excDay.getType());
						}*/
						lstDayOff.add(row);
					}
				}
				exportExcelDataNotSession(lstDayOff,ConstantManager.TEMPLATE_SALE_DAY_EXCEPTION_EXPORT);
				result.put(ERROR, false);
			}else{
				result.put("hasData", false);
				result.put(ERROR, false);
			}
		}catch(Exception e){
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
}

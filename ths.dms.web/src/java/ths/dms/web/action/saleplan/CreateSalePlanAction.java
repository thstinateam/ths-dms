package ths.dms.web.action.saleplan;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CycleMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.SalePlanMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.SalePlan;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SalePlanVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.SumSalePlanVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateSalePlanAction.
 */
public class CreateSalePlanAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	/** The staff code. */
	private String staffCode;

	/** The product code. */
	private String productCode;

	/** The date search. */
	private String dateSearch;

	private String month;

	/** The sale plan mgr. */
	private SalePlanMgr salePlanMgr;

	/** The staff mgr. */
	private StaffMgr staffMgr;

	/** The lst sale plan. */
	private List<SalePlanVO> lstSalePlan;

	/** The sum sale plan. */
	private SumSalePlanVO sumSalePlan;

	/** The total amount. */
	private Integer totalAmount = 0;

	/** The total money. */
	private BigDecimal totalMoney = BigDecimal.ZERO;

	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;

	/** The lst category. */
	private List<ProductInfo> lstCategory;

	/** The shop mgr. */
	private ShopMgr shopMgr;

	/** The shop. */
	private Shop shop;

	/** The index. */
	private Integer index;

	/** The num. */
	private Integer num;

	/** The category code. */
	private String categoryCode;

	/** List category code. */
	private String lstCategoryCode;

	/** The excel file. */
	private File excelFile;

	/** The excel file content type. */
	private String excelFileContentType;

	/** The product mgr. */
	private ProductMgr productMgr;

	/** The list id. */
	private List<Long> listId;

	private List<StaffVO> lstStaff;

	/** The list quantity. */
	private List<Integer> listQuantity;

	/** The list amount. */
	private List<BigDecimal> listAmount;
	
	/** The list size. */
	private Integer listSize = 0;

	/** The shop code. */
	private String shopCode;

	/** The shop id. */
	private Long shopId;

	/** The lst shop. */
	private List<Shop> lstShop;

	private String code;
	private String name;
	private Integer fromQty;
	private Integer toQty;
	private List<Long> lstDelete;
	private String lstCode;
	
	private CycleMgr cycleMgr;
	private Integer yearPeriod;	
	private Integer numPeriod;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		salePlanMgr = (SalePlanMgr) context.getBean("salePlanMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		cycleMgr = (CycleMgr) context.getBean("cycleMgr");
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null && staff.getShop() != null) {
					shop = staff.getShop();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {
		resetToken(result);
		try {
			lstYear = cycleMgr.getListYearOfAllCycle();			
			Date curdate = DateUtil.now();
			yearPeriod = DateUtil.getYear(curdate);
			Cycle curCycle = cycleMgr.getCycleByDate(curdate);
			if(curCycle != null) {
				numPeriod = curCycle.getNum();
			} else {
				numPeriod = 1;
			}
			
			lstStaff = new ArrayList<StaffVO>();
			shopCode = currentUser.getShopRoot().getShopCode();
			lstCategory = new ArrayList<ProductInfo>();
			lstCategory = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true,null,null).getLstObject();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * get list category.
	 * 
	 * @return the string
	 * @author nhanlt
	 * @since May 22, 2013
	 */
	public String getListCategory() {
		if (currentUser != null) {
			try {
				ObjectVO<ProductInfo> lstCategoryTmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true,null,null);
				lstCategory = lstCategoryTmp.getLstObject();
				if (lstCategory == null) {
					lstCategory = new ArrayList<ProductInfo>();
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return JSON;
	}

	/**
	 * get list staff.
	 * 
	 * @return the string
	 * @author longnh15
	 * @since May 22, 2013
	 */
	/**
	 * Lay danh sach staff theo routing, shopId
	 * @author vuongmq 
	 * @return String
	 * @since 31/08/2015
	 */
	public String getListStaff() {
		if (currentUser != null) {
			try {
				if (shopId != null && shopMgr.getShopById(shopId) != null) {
					StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
					filter.setIsLstChildStaffRoot(false);
					filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
					filter.setStatus(ActiveType.RUNNING.getValue());
					filter.setLstShopId(Arrays.asList(shopId));
					filter.setIsGetChildShop(false);
					filter.setObjectType(StaffSpecificType.STAFF.getValue());
					filter.setStaffIdListStr(getStrListUserId());
					ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
					lstStaff = objVO.getLstObject();
				} else {
					lstStaff = new ArrayList<StaffVO>();
				}
			} 
			catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return JSON;
	}
	
	/**
	 * Search version new.
	 * 
	 * @return the string
	 * @author nhanlt
	 * @since May 22, 2013
	 * 
	 * @author hunglm16
	 * @since October 8,2014
	 * @description Tich hop phan quyen CMS
	 */
	public String searchSalePlan() {
		try {
			if (staffCode == null || "-1".equals(staffCode)) {
				//thong bao
				//result.put(ERROR, true);
				//result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
				//result.put("errMsg","Bạn chưa chọn nhân viên");
				result.put("total", 0);
				result.put("rows", new ArrayList<SalePlanVO>());
				result.put("totalQuantity", 0);
				result.put("totalMoney", 0);
				result.put("sumSalePlan", 0);
				return JSON;
			}
			if(shopId == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.shop")));
				return JSON;
			} 
			shop = shopMgr.getShopById(shopId);
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.shop")));
				return JSON;
			} else if (!getListShopChildId().contains(shop.getId())) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.not.belong.area"));
				return JSON;
			}
			
			if (yearPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.year")));
				return JSON;
			} 
			if (numPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.period")));
				return JSON;
			}
			List<String> lstCodeTmp = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty(lstCode)) {
				String[] lstTmp = lstCode.split(",");
				for (int i = 0; i < lstTmp.length; i++) {
					lstCodeTmp.add(i, lstTmp[i]);
				}
			} else {
				lstCodeTmp = null;
			}

			/*if (staffCode == null || "-1".equals(staffCode)) {
				ObjectVO<SalePlanVO> lstSalePlanVO = salePlanMgr.getListSalePlanVOForLoadPage(null, currentUser.getShopRoot().getShopId(), month, year);
				if (lstSalePlanVO.getLstObject() != null) {
					lstSalePlan = lstSalePlanVO.getLstObject();
				}
			} else {*/
				if (currentUser.getListUser() == null || currentUser.getListUser().isEmpty() || checkStaffByStaffRoot(staffCode)) {
					/*if (ShopDecentralizationSTT.NPP.getValue().equals(currentUser.getShopRoot().getIsLevel())) {
						lstSalePlan = salePlanMgr.getListSalePlanVO4KT(currentUser.getShopRoot().getShopId(), staffCode, lstCodeTmp, productCode, month, year);
					} else {
						lstSalePlan = salePlanMgr.getListSalePlanVO(currentUser.getShopRoot().getShopId(), staffCode, lstCodeTmp, productCode, month, year, staff.getId());
					}*/
					lstSalePlan = salePlanMgr.getListSalePlanVO(shopId, staffCode, lstCodeTmp, productCode, numPeriod, yearPeriod, staff.getId());
				} else {
					lstSalePlan = new ArrayList<SalePlanVO>();
				}
			
			
			if (lstSalePlan != null && lstSalePlan.size() > 0) {
				for (int i = 0; i < lstSalePlan.size(); i++) {
					if (lstSalePlan.get(i).getStaffQuantity() != null) {
						totalAmount += lstSalePlan.get(i).getStaffQuantity();
//						if (lstSalePlan.get(i).getPrice() != null) {
//							BigDecimal tmp = new BigDecimal(String.valueOf(lstSalePlan.get(i).getStaffQuantity()));
//							tmp = tmp.multiply(lstSalePlan.get(i).getPrice());
//							totalMoney = totalMoney.add(tmp);
//						}
					}
					if (lstSalePlan.get(i).getAmount() != null) {
						totalMoney = totalMoney.add(lstSalePlan.get(i).getAmount());
					}
				}

				lstSalePlan.get(0).setTotalQuantity(totalAmount);
				lstSalePlan.get(0).setTotalAmount(totalMoney);
				listSize = lstSalePlan.size();
			}
			sumSalePlan = salePlanMgr.getSumSalePlan(shopId, null, SalePlanOwnerType.SHOP, PlanType.MONTH, numPeriod, yearPeriod);

			result.put("total", listSize);
			result.put("rows", lstSalePlan);
			result.put("totalQuantity", totalAmount);
			result.put("totalMoney", totalMoney);
			result.put("sumSalePlan", sumSalePlan);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "CreateSalePlanAction.searchSalePlan() - " + e.getMessage());
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
		}
		
		return JSON;
	}

	/**
	 * Distribute product.
	 * 
	 * @return the string
	 * @author lamnh,tientv
	 * @since Oct 1, 2012
	 * 
	 * @author hunglm16
	 * @since October 10,2014
	 * @description Phan quyen CMS
	 */
	public String distributeProduct() {
		resetToken(result);
		try {
			Staff staff1 = staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId());
			if (staff1 == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
				return JSON;
			}

			if(shopId == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.shop")));
				return JSON;
			} 
			shop = shopMgr.getShopById(shopId);
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.shop")));
				return JSON;
			} else if (!getListShopChildId().contains(shop.getId())) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.not.belong.area"));
				return JSON;
			}
			if (yearPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.year")));
				return JSON;
			} 
			if (numPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.period")));
				return JSON;
			}
			
			//Chỉ phân bổ cho chu kỳ hiện tại thực tế {0} và các chu kỳ tiếp theo
			Cycle curCycle = cycleMgr.getCycleByDate(DateUtil.getCurrentGMTDate());
			Integer curYear = DateUtil.getYear(curCycle.getYear());
			if(yearPeriod < curYear || (yearPeriod == curYear && numPeriod < curCycle.getNum())){
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_MONTH_CREARE_SALE_PLAN, curCycle.getNum() + "/" + curYear));
				return JSON;
			}
			
			if (StringUtil.isNullOrEmpty(staffCode)) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.staff.name")));
				return JSON;
			} else if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty() && !checkStaffByStaffRoot(staffCode)) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.staff.undefined")));
				return JSON;
			}
			Staff staff = staffMgr.getStaffByCode(staffCode);
			if (staff == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.staff.name")));
				return JSON;
			} else {
				if (!ActiveType.RUNNING.getValue().equals(staff.getStatus().getValue())) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.traningplan.gsnpp.not.manage.salestaffDel"));
					return JSON;
				}
				/*if (!ShopDecentralizationSTT.NPP.getValue().equals(currentUser.getShopRoot().getIsLevel())) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.traningplan.gsnpp.not.manage.shop"));
					return JSON;
				}*/
			}
			if (listId != null && listId.size() > 0) {
				salePlanMgr.createSalePlanForStaff(staff.getId(), numPeriod, yearPeriod, listId, listQuantity, listAmount, currentUser.getUserName(), shopId);
				result.put(ERROR, false);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.list.product"));
			}
		} catch (Exception ex) {
			result.put(ERROR, true);
			LogUtility.logError(ex, "CreateSalePlan.createSalePlan - " + ex.getMessage());
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
		}
		return JSON;
	}

	/**
	 * Search product.
	 * 
	 * @return the string
	 * @author tientv
	 */
	public String searchProduct() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<SalePlanVO> kPaging = new KPaging<SalePlanVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (StringUtil.isNullOrEmpty(month)) {
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(staffCode)) {
				return JSON;
			}
			Integer MM = DateUtil.getMonth(DateUtil.parse(month, ConstantManager.MONTH_YEAR_FORMAT));
			Integer yyyy = DateUtil.getYear(DateUtil.parse(month, ConstantManager.MONTH_YEAR_FORMAT));
			ObjectVO<SalePlanVO> productVO = salePlanMgr.getListSalePlanVOProductOfShop(kPaging, shop.getId(), staffCode, null, code, name, MM, yyyy, fromQty, toQty);
			if (productVO != null) {
				result.put("total", productVO.getkPaging().getTotalRows());
				result.put("rows", productVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	/**
	 * Tao file template dung de import ke hoach thang
	 * 
	 * @author longnh15
	 * @return Doi tuong chua duong dan toi file template
	 * @since 06/05/2015
	 */
	public String downloadTemplateExcelfile() {
		String reportToken = retrieveReportToken(reportCode);
		if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		String outputPath = this.createImportSalePlanTemplateFile();
		//String outputPath =  Configuration.getExportExcelPath() + importTemplateFileName;
		result.put(REPORT_PATH, outputPath);
		result.put(LIST, outputPath);
		result.put(ERROR, false);
		result.put("hasData", true);
		try {
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	/**
	 * Tao file mau import sale plan
	 * 
	 * @author longnh15
	 * @return Ten file template moi duoc tao
	 * @since 06/05/2015
	 */
	private String createImportSalePlanTemplateFile() {
		
		String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathSalePlan() + ConstantManager.TEMPLATE_SALE_PLAN_CREATE;
		templateFileName = templateFileName.replace('/', File.separatorChar);
		String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "template_sale_plan_create");
		String outputFileName = Configuration.getStoreRealPath() + outputName;
		outputFileName = outputFileName.replace('/', File.separatorChar);
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("nvbh", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_staff_code"));
		params.put("msp", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_product"));
		params.put("chuky", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
		params.put("nam", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
		params.put("soluong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_quantity"));
		params.put("doanhso", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_amount"));

		XLSTransformer transformer = new XLSTransformer();
		try {
			transformer.setSpreadsheetToRename("sheetName1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_import_sheet"));
			transformer.transformXLS(templateFileName, params, outputFileName);
		} catch (ParsePropertyException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (InvalidFormatException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (IOException e) {
			//System.out.print(e.getMessage());
			LogUtility.logError(e, e.getMessage());
		}
		return Configuration.getExportExcelPath() + outputName;
	}
		
	/**
	 * Import excel file.
	 * 
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 * 
	 * @author hunglm16
	 * @since October 10,2014
	 * @description Cap nhat phan quyen
	 */
	public String importExcelFile() {
		resetToken(result);
		isError = true;
		totalItem = 0;
		String message = "";
		/*if (!ShopDecentralizationSTT.NPP.getValue().equals(currentUser.getShopRoot().getIsLevel())) {
			return ERROR;
		}*/
		try {
			if(shopId == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.shop")));
				return JSON;
			} 
			shop = shopMgr.getShopById(shopId);
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.shop")));
				return JSON;
			} else if (!getListShopChildId().contains(shop.getId())) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.not.belong.area"));
				return JSON;
			}
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, 6);
			lstView = new ArrayList<CellBean>();
			typeView = true;
			if (isView == null) {
				isView = 0;
			}
			Staff excelStaff = null;
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			
//				Staff staff = staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId());
				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						message = "";
						totalItem++;
						SalePlan temp = new SalePlan();
						List<String> row = lstData.get(i);
						temp.setCreateUser(currentUser.getUserName());
						//Mã NVBH
						if (row.size() > 0) {
							String value = row.get(0);
							if (!StringUtil.isNullOrEmpty(value)) {
								try {
									excelStaff = staffMgr.getStaffByCode(value);
									if (excelStaff != null) {
										if (!ActiveType.RUNNING.equals(excelStaff.getStatus())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.month.distribute.excel.staff", value);
											message += "\n";
										}
										else if (excelStaff.getStaffType().getSpecificType()==null)
										{
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.staff.not.belong.to.nvbh.nvvs", value);
											message += "\n";
										}
										else if (excelStaff.getStaffType().getSpecificType()==null || !StaffSpecificType.STAFF.getValue().equals(excelStaff.getStaffType().getSpecificType().getValue())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.staff.not.belong.to.nvbh.nvvs", value);
											message += "\n";
										} else {
											//Kiem tra phan quyen CMS
											if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty() && !checkStaffByStaffRoot(excelStaff.getStaffCode())) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.staff.undefined");
												message += "\n";
											} else {
												temp.setObjectId(excelStaff.getId());
												//temp.setParent(shop);
												////											if(excelStaff.getStaffType()!= null && excelStaff.getStaffType().getChannelTypeCode().equals("SR")){
												//												StaffFilter filter = new StaffFilter();
												//												filter.setStaffCode(value);
												//												filter.setStaffType(StaffObjectType.NVBH);
												//												filter.setStatus(ActiveType.RUNNING);
												//												ObjectVO<Staff> staffVO = staffMgr.getListStaff(filter);
												//												if(staffVO != null && staffVO.getLstObject().size() == 1){
												//													if(ActiveType.RUNNING.equals(staffVO.getLstObject().get(0).getStatus())){

												//													}
												//												}else{
												//													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.not.belong.staff",value);
												//													message += "\n";
												//												}
												//											}else{
												//												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.date",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.staff.code"));
												//												message += "\n";
												//											}

											}
										}
										if (!shop.getId().equals(excelStaff.getShop().getId())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.shop.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.code"));
											message += "\n";
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.code"));
										message += "\n";
									}
								} catch (Exception e) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.code"));
									message += "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.code"));
								message += "\n";
							}
						}
						//Mã SP
						if (row.size() > 1) {
							String value = row.get(1);
							if (!StringUtil.isNullOrEmpty(value)) {
								Product product = productMgr.getProductByCode(value.trim());
								if (product == null) {
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.product.pause"));
									message += "\n";
								} else {
									if (!product.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.product.pause"));
										message += "\n";
									} else {
										if (productMgr.checkProductInZ(value)) {
											temp.setProduct(product); //success
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.product.in.z");
											message += "\n";
										}

									}
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.code"));
								message += "\n";
							}
						}
						//chu ky
						if (row.size() > 3) {
							String numStr = row.get(2).trim();
							String yearStr = row.get(3).trim();
							if (!StringUtil.isNullOrEmpty(numStr)) {
								try {
									numPeriod = Integer.parseInt(numStr);
//									if (num < 1 || num > 13) {
//										throw new Exception();
//									}									
								} catch (Exception e) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.number", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
									message += "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
								message += "\n";
							}
							
							if (!StringUtil.isNullOrEmpty(yearStr)) {
								try {
									yearPeriod = Integer.parseInt(yearStr);
//									if (year < 1) {
//										throw new Exception();
//									}
								} catch (Exception e) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.number", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
									message += "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
								message += "\n";
							}
							
							if (numPeriod != null && numPeriod > 0 && yearPeriod != null && yearPeriod > 0) {
								Date now = DateUtil.now();
								Cycle curCycle = cycleMgr.getCycleByDate(now);
								if (curCycle != null) {
									if (yearPeriod < DateUtil.getYear(now) || (yearPeriod == DateUtil.getYear(now) && numPeriod < curCycle.getNum())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.allocation.date.allocation");
										message += "\n";
									} 
//									else {
//										Cycle cycle = cycleMgr.getCycleByYearAndNum(year, num);
//										temp.setCycle(cycle);
//									}
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.cycle", numPeriod + "/" + yearPeriod));
									message += "\n";
								}
							}
						}
						//Số lượng
						if (row.size() > 4) {
							String value = row.get(4);
							if (!StringUtil.isNullOrEmpty(value)) {
								try {
									if (value.trim().length() < 10) {
//										if (value==null) temp.setQuantity(0);
//										else 
										if (Integer.parseInt(value) > -1) {
											temp.setQuantity(Integer.parseInt(value));
//											BigDecimal tt = new BigDecimal(String.valueOf(temp.getQuantity()));
//											if (temp.getProduct() != null) {
//												Price price = productMgr.getPriceByProductId(shop.getId(), temp.getProduct().getId());
//												if (price == null) {
//													message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.allocation.price"));
//													message += "\n";
//												} else {
//													tt = tt.multiply(price.getPrice());
//													temp.setAmount(tt);
//												}
//											}
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.possitive.integer", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.quantity.distribute"));
											message += "\n";
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.exceed.length", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.quantity.distribute"));
										message += "\n";
									}
								} catch (Exception e) {
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_FORMAT, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.quantity.distribute"));
									message += "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.possitive.integer", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.quantity.distribute"));
								message += "\n";
							}
						}
						//Doanh số
						if (row.size() > 5) {
							String value = row.get(5);
							if (!StringUtil.isNullOrEmpty(value)) {
								try {
									if (value.trim().length() < 20) {
										if (value.trim()==null && row.get(4).trim()==null)
										{
											message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.quantity.amount.distribute"));
											message += "\n";
										}
										else if (value.trim()==null) temp.setAmount(BigDecimal.ZERO);
										else if (Long.parseLong(value) > -1) {
											BigDecimal tempAmount = new BigDecimal(String.valueOf(value));
											temp.setAmount(tempAmount);
											/*BigDecimal tt = new BigDecimal(String.valueOf(temp.getQuantity()));
											if (temp.getProduct() != null) {
												Price price = productMgr.getPriceByProductId(temp.getProduct().getId());
												if (price == null) {
													message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.allocation.price"));
													message += "\n";
												} else {
													tt = tt.multiply(price.getPrice());
													temp.setAmount(tt);
												}
											}*/
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.possitive.integer", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.amount.distribute"));
											message += "\n";
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.exceed.length", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.amount.distribute"));
										message += "\n";
									}
								} catch (Exception e) {
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_FORMAT, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.amount.distribute"));
									message += "\n";
								}
							}
//							else {
//								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.possitive.integer", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.amount.distribute"));
//								message += "\n";
//							}
						}
						//End input
//						boolean isExists = false;
//						SalePlan sp = null;
//						if (StringUtil.isNullOrEmpty(message)) {
//							SalePlan salesPlans = salePlanMgr.getSalePlan(excelStaff.getStaffCode(), temp.getProduct().getProductCode(), numPeriod, yearPeriod);
//							if (salesPlans == null) {
//								isExists = false;
//							} else {
//								isExists = true;
//								Long salePlanId = salesPlans.getId();
//								if (salePlanId != null) {
//									sp = salePlanMgr.getSalePlanById(salePlanId);
//								} else {
//									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.id.not.correct");
//									message += "\n";
//								}
//							}
//						}
						//						if(temp != null && temp.getProduct() != null && excelStaff != null){
						//							if(salePlanMgr.checkSalePlan(excelStaff.getStaffCode(), SalePlanOwnerType.SALEMAN, temp.getProduct().getProductCode(), temp.getMonth(), temp.getYear())){
						//								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"staff.product.month.exist");
						//								message += "\n";
						//							}
						//						}
						if (isView == 0) {
							if (StringUtil.isNullOrEmpty(message)) {
								try {
									if (temp != null && temp.getProduct() != null) {
//										temp.setObjectType(SalePlanOwnerType.SALEMAN);
//										temp.setCreateUser(currentUser.getUserName());
										//temp.setVersion(1);
//										temp.setPrice(productMgr.getPriceByProductId(temp.getProduct().getId()).getPrice());
//										temp.setType(PlanType.MONTH);
										//										ProductLevel pl = temp.getProduct().getProductLevel();
										//										if(pl!=null){
										//											temp.setCat(pl.getCat());
										//										}
										/*if (isExists) {
											//sp.setVersion(sp.getVersion() + 1);
											sp.setQuantity(temp.getQuantity());
											sp.setAmount(temp.getAmount());
											sp.setCreateUser(currentUser.getUserName());
											salePlanMgr.updateSalePlanAndSalePlanVesion(sp, currentUser.getUserName());
										} else {
											salePlanMgr.createSalePlanForStaff(temp.getObjectId(), DateUtil.getMonth(temp.getMonthDate()), DateUtil.getYear(temp.getMonthDate()), Arrays.asList(temp.getProduct().getId()), Arrays.asList(temp.getQuantity()),Arrays.asList(temp.getAmount()), currentUser.getUserName(), currentUser.getShopRoot().getShopId());
										}*/
										salePlanMgr.createSalePlanForStaff(temp.getObjectId(), numPeriod, yearPeriod, Arrays.asList(temp.getProduct().getId()), Arrays.asList(temp.getQuantity()),Arrays.asList(temp.getAmount()), currentUser.getUserName(), shop.getId());
									}
								} catch (BusinessException be) {
									LogUtility.logError(be, be.getMessage());
								}
							} else {
								lstFails.add(StringUtil.addFailBean(row, message));
							}
							typeView = false;
						} else {
							if (StringUtil.isNullOrEmpty(message)) {
								message = "OK";
							}
							lstView.add(StringUtil.addFailBean(row, message));
							typeView = true;
						}
					}
				}
				//Export error
				getOutputFailSalePlan(lstFails, ConstantManager.TEMPLATE_SALE_PLAN_CREATE_FAIL);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}		
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	public void getOutputFailSalePlan(List<CellBean> lstFails, String templateName) {
		if (lstFails.size() > 0) {
			numFail = lstFails.size();
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + templateName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "template_sale_plan_create_fail");
			String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report", lstFails);
			params.put("stt", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_stt"));
			params.put("nvbh", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_staff_code"));
			params.put("msp", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_product"));
			params.put("chuky", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
			params.put("nam", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
			params.put("soluong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_quantity"));
			params.put("doanhso", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_amount"));
			params.put("error", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_error"));
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.setSpreadsheetToRename("sheetName1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_import_sheet"));
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (InvalidFormatException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (IOException e) {
				//System.out.print(e.getMessage());
				LogUtility.logError(e, e.getMessage());
			}
			fileNameFail = Configuration.getStoreImportFailDownloadPath() + outputName;
		}
	}
	/**
	 * @author nhanlt
	 */
	public String exportExcel() {
		result = new HashMap<String, Object>();
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
//			ObjectVO<SalePlan> salePlan = new ObjectVO<SalePlan>();
			if (StringUtil.isNullOrEmpty(staffCode)) {
				result.put(ERROR, true);
				return JSON;
			}
			Staff staff = staffMgr.getStaffByCode(staffCode);
			if (staff == null) {
				result.put("errMsg", "Nhân viên không tồn tại");
				result.put(ERROR, true);
				return JSON;
			} else if (!staff.getStatus().equals(ActiveType.RUNNING)) {
				result.put("errMsg", "Nhân viên không tồn tại");
				result.put(ERROR, true);
				return JSON;
			} else if (currentUser == null
					|| (currentUser.getListUser() != null && currentUser.getListUser().size() > 0 &&  !checkStaffByStaffRoot(staffCode))) {
				result.put("errMsg", "Nhân viên chưa được cấp quyền quản lý");
				result.put(ERROR, true);
				return JSON;
			}
			if(shopId == null) {
				result.put(ERROR, true);
				return JSON;
			} 
			shop = shopMgr.getShopById(shopId);
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.create.shop")));
				return JSON;
			} else if (!getListShopChildId().contains(shop.getId())) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.not.belong.area"));
				return JSON;
			}
			if (yearPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.year")));
				return JSON;
			} 
			if (numPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.period")));
				return JSON;
			}

			List<String> lstCodeTmp = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty(lstCode)) {
				String[] lstTmp = lstCode.split(",");
				for (int i = 0; i < lstTmp.length; i++) {
					lstCodeTmp.add(i, lstTmp[i]);
				}
			} else {
				lstCodeTmp = null;
			}
//			salePlan = salePlanMgr.getListSalePlan(null, staffCode, lstCodeTmp, productCode, numPeriod, yearPeriod);
			lstSalePlan = salePlanMgr.getListSalePlanVO(shopId, staffCode, lstCodeTmp, productCode, numPeriod, yearPeriod, staff.getId());
			BigDecimal totalAmount = BigDecimal.ZERO;
			Integer totalQuantity = 0;
			for (SalePlanVO sp : lstSalePlan) {
				if (sp.getAmount() != null && sp.getStaffQuantity() != null) {
					totalAmount = totalAmount.add(sp.getAmount());
					totalQuantity += sp.getStaffQuantity();
				}
			}
			Map<String, Object> beans = new HashMap<String, Object>();
			if (lstSalePlan.size() > 0) {
				beans.put("hasData", 1);
				
				//replace
				beans.put("stt", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_stt"));
				beans.put("tensp", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_productname"));
				beans.put("msp", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_product"));
				beans.put("soluong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_quantity"));
				beans.put("doanhso", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_amount"));
				beans.put("ngayin", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_printdate"));
				beans.put("tieude", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_title"));
				beans.put("nhanvien", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_staff"));
				beans.put("donvi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_unit"));
				beans.put("nvgs", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_monitorstaff"));
				beans.put("chuky", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
				beans.put("nam", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
				beans.put("tong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_total"));
				//end replace
				beans.put("staffName", staff.getStaffCode() + " - " + staff.getStaffName());
				beans.put("shopCode", shop.getShopCode() + " - " + shop.getShopName());
				if (staff.getStaffOwner() != null) {
					beans.put("staffOwner", staff.getStaffOwner().getStaffCode() + " - " + staff.getStaffOwner().getStaffName());
				} else {
					beans.put("staffOwner", "");
				}
				beans.put("numPeriod", numPeriod);
				beans.put("yearPeriod", yearPeriod);
				beans.put("printDate", DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY));
				beans.put("salesPlan", lstSalePlan);
				beans.put("totalAmount", convertMoney(totalAmount));
				beans.put("totalQuantity", totalQuantity);
			} else {
				beans.put("hasData", 0);
				result.put(ERROR, true);
				result.put("errMsg", "Không có dữ liệu xuất");
				return JSON;
			}
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathStock(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.TEMPLATE_SALE_PLAN_CREATE_EXPORT;
			templateFileName = templateFileName.replace('/', File.separatorChar);

			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" +Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "template_sale_plan_export");
//			String exportFileName = (folder + outputName).replace('/', File.separatorChar);
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			resultWorkbook.setSheetName(0, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_export_sheet"));
			
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			result.put(ERROR, false);
//			String outputPath = ServletActionContext.getServletContext().getContextPath() + Configuration.getExcelTemplatePathStock() + outputName;
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			result.put(ERROR, true);
			LogUtility.logError(ex, "CreateSalePlanAction.exportExcel - " + ex.getMessage());
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);				
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/////////////////@: GETTER/SETTER :@////////////////////////
	public List<StaffVO> getLstStaff() {
		return lstStaff;
	}

	public void setLstStaff(List<StaffVO> lstStaff) {
		this.lstStaff = lstStaff;
	}

	public String getLstCode() {
		return lstCode;
	}

	public void setLstCode(String lstCode) {
		this.lstCode = lstCode;
	}

	public String getLstCategoryCode() {
		return lstCategoryCode;
	}

	public void setLstCategoryCode(String lstCategoryCode) {
		this.lstCategoryCode = lstCategoryCode;
	}

	public List<Long> getLstDelete() {
		return lstDelete;
	}

	public void setLstDelete(List<Long> lstDelete) {
		this.lstDelete = lstDelete;
	}

	public Integer getFromQty() {
		return fromQty;
	}

	public void setFromQty(Integer fromQty) {
		this.fromQty = fromQty;
	}

	public Integer getToQty() {
		return toQty;
	}

	public void setToQty(Integer toQty) {
		this.toQty = toQty;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the lst shop.
	 * 
	 * @return the lst shop
	 */
	public List<Shop> getLstShop() {
		return lstShop;
	}

	/**
	 * Sets the lst shop.
	 * 
	 * @param lstShop
	 *            the new lst shop
	 */
	public void setLstShop(List<Shop> lstShop) {
		this.lstShop = lstShop;
	}

	/**
	 * Gets the shop id.
	 * 
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 * 
	 * @param shopId
	 *            the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the shop code.
	 * 
	 * @return the shop code
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * Sets the shop code.
	 * 
	 * @param shopCode
	 *            the new shop code
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * Gets the list size.
	 * 
	 * @return the list size
	 */
	public Integer getListSize() {
		return listSize;
	}

	/**
	 * Sets the list size.
	 * 
	 * @param listSize
	 *            the new list size
	 */
	public void setListSize(Integer listSize) {
		this.listSize = listSize;
	}

	/**
	 * Gets the list quantity.
	 * 
	 * @return the list quantity
	 */
	public List<Integer> getListQuantity() {
		return listQuantity;
	}

	/**
	 * Sets the list quantity.
	 * 
	 * @param listQuantity
	 *            the new list quantity
	 */
	public void setListQuantity(List<Integer> listQuantity) {
		this.listQuantity = listQuantity;
	}
	
	/**
	 * Gets the list amount.
	 * 
	 * @return the list amount
	 */
	public List<BigDecimal> getListAmount() {
		return listAmount;
	}

	/**
	 * Sets the list amount.
	 * 
	 * @param listAmount
	 *            the new list amount
	 */
	public void setListAmount(List<BigDecimal> listAmount) {
		this.listAmount = listAmount;
	}

	/**
	 * Gets the list id.
	 * 
	 * @return the list id
	 */
	public List<Long> getListId() {
		return listId;
	}

	/**
	 * Sets the list id.
	 * 
	 * @param listId
	 *            the new list id
	 */
	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	/**
	 * Gets the excel file.
	 * 
	 * @return the excel file
	 */
	public File getExcelFile() {
		return excelFile;
	}

	/**
	 * Sets the excel file.
	 * 
	 * @param excelFile
	 *            the new excel file
	 */
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	/**
	 * Gets the excel file content type.
	 * 
	 * @return the excel file content type
	 */
	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	/**
	 * Sets the excel file content type.
	 * 
	 * @param excelFileContentType
	 *            the new excel file content type
	 */
	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/**
	 * Gets the category code.
	 * 
	 * @return the category code
	 */
	public String getCategoryCode() {
		return categoryCode;
	}

	/**
	 * Sets the category code.
	 * 
	 * @param categoryCode
	 *            the new category code
	 */
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	/**
	 * Gets the index.
	 * 
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * Sets the index.
	 * 
	 * @param index
	 *            the new index
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}

	/**
	 * Gets the num.
	 * 
	 * @return the num
	 */
	public Integer getNum() {
		return num;
	}

	/**
	 * Sets the num.
	 * 
	 * @param num
	 *            the new num
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * Gets the shop.
	 * 
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 * 
	 * @param shop
	 *            the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * Gets the lst category.
	 * 
	 * @return the lst category
	 */
	public List<ProductInfo> getLstCategory() {
		return lstCategory;
	}

	/**
	 * Sets the lst category.
	 * 
	 * @param lstCategory
	 *            the new lst category
	 */
	public void setLstCategory(List<ProductInfo> lstCategory) {
		this.lstCategory = lstCategory;
	}

	/**
	 * Gets the total amount.
	 * 
	 * @return the total amount
	 */
	public Integer getTotalAmount() {
		return totalAmount;
	}

	/**
	 * Sets the total amount.
	 * 
	 * @param totalAmount
	 *            the new total amount
	 */
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * Gets the total money.
	 * 
	 * @return the total money
	 */
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	/**
	 * Sets the total money.
	 * 
	 * @param totalMoney
	 *            the new total money
	 */
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	/**
	 * Gets the sum sale plan.
	 * 
	 * @return the sum sale plan
	 */
	public SumSalePlanVO getSumSalePlan() {
		return sumSalePlan;
	}

	/**
	 * Sets the sum sale plan.
	 * 
	 * @param sumSalePlan
	 *            the new sum sale plan
	 */
	public void setSumSalePlan(SumSalePlanVO sumSalePlan) {
		this.sumSalePlan = sumSalePlan;
	}

	/**
	 * Gets the lst sale plan.
	 * 
	 * @return the lst sale plan
	 */
	public List<SalePlanVO> getLstSalePlan() {
		return lstSalePlan;
	}

	/**
	 * Sets the lst sale plan.
	 * 
	 * @param lstSalePlan
	 *            the new lst sale plan
	 */
	public void setLstSalePlan(List<SalePlanVO> lstSalePlan) {
		this.lstSalePlan = lstSalePlan;
	}

	/**
	 * Gets the staff code.
	 * 
	 * @return the staff code
	 */
	public String getStaffCode() {
		return staffCode;
	}

	/**
	 * Sets the staff code.
	 * 
	 * @param staffCode
	 *            the new staff code
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	/**
	 * Gets the product code.
	 * 
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 * 
	 * @param productCode
	 *            the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the date search.
	 * 
	 * @return the date search
	 */
	public String getDateSearch() {
		return dateSearch;
	}

	/**
	 * Sets the date search.
	 * 
	 * @param dateSearch
	 *            the new date search
	 */
	public void setDateSearch(String dateSearch) {
		this.dateSearch = dateSearch;
	}

	public Integer getYearPeriod() {
		return yearPeriod;
	}

	public void setYearPeriod(Integer yearPeriod) {
		this.yearPeriod = yearPeriod;
	}

	public Integer getNumPeriod() {
		return numPeriod;
	}

	public void setNumPeriod(Integer numPeriod) {
		this.numPeriod = numPeriod;
	}

}

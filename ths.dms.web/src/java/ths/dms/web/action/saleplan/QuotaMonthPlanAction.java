package ths.dms.web.action.saleplan;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.CycleMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.SalePlanMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.SalePlan;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.vo.DistributeSalePlanVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SalePlanVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

/**
 * The Class QuotaMonthPlanAction.
 */
public class QuotaMonthPlanAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	/** The excel file. */
	private File excelFile;
	
	/** The excel file content type. */
	private String excelFileContentType;
	
	/** The lst category type. */
	private List<ProductInfo> lstCategoryType;
	
	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;
	
	/** The sale plan mgr. */
	private SalePlanMgr salePlanMgr;
	
	/** The shop mgr. */
	private ShopMgr shopMgr;
	
	/** The sales plans. */
	private List<DistributeSalePlanVO> salesPlans;
	
	/** The obj sales plans. */
	private ObjectVO<SalePlanVO> objSalesPlans;
	
	/** The shop. */
	private Shop shop;
	
	/** The shop code. */
	private String shopCode;
	
	/** The product code. */
	private String productCode;
	
	/** The category code. */
	private String categoryCode;
	
	/** The search month. */
	private String searchMonth;
	
	/** The month. */
	private String month;

	/** The staff mgr. */
	private StaffMgr staffMgr;
	
	/** The staff. */
	private Staff staff;	
	
	/** The product name. */
	private String productName;
	
	/** The lst product. */
	List<Long> lstProduct;
	
	/** The lst quantity. */
	List<Integer> lstQuantity;
	
	/** The lst amount. */
	List<BigDecimal> lstAmount;
	
	/** The lst delete. */
	List<Integer> lstDelete;
	
	/** The lst sale plan id. */
	List<Long> lstSalePlanId;
	
	/** The product mgr. */
	private ProductMgr productMgr;
	
	/** The shop id. */
	private Long shopId;
	
	/** The from quantity. */
	private Integer fromQuantity;
	
	/** The to quantity. */
	private Integer toQuantity;
	
	/** The staff code under. */
	private String staffCodeUnder;
	
	/** The total item. */
	private int totalItem;
	
	/** The cate file. */
	private Integer excelType;
	
	/** The code. */
	private String code;
	
	/** typeSearch=0: Load trang, typeSearch=1: Tim kiem */
	private Integer typeSearch;
	
	
	/** The name. */
	private String name;
	
	private CycleMgr cycleMgr;
	private Integer yearPeriod;	
	private Integer numPeriod;

	/**
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		salePlanMgr = (SalePlanMgr) context.getBean("salePlanMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		productMgr = (ProductMgr)context.getBean("productMgr");
		try {
			staff = getStaffByCurrentUser();
			if(staff!=null){
				shop = staff.getShop();
			}			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	/**
	 * @author tientv
	 */
	@Override
	public String execute() {		
		resetToken(result);
		try {
			lstYear = cycleMgr.getListYearOfAllCycle();
			Date curdate = DateUtil.now();
			yearPeriod = DateUtil.getYear(curdate);
			Cycle curCycle = cycleMgr.getCycleByDate(curdate);
			if(curCycle != null) {
				numPeriod = curCycle.getNum();
			} else {
				numPeriod = 1;
			}
			
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());	
			shopCode = shop.getShopCode();
			/*ObjectVO<ProductInfo> lstCategoryTmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true,null,null).getLstObject();
			lstCategoryType = lstCategoryTmp.getLstObject();
			if(lstCategoryType==null){
				lstCategoryType = new ArrayList<ProductInfo>();
			}*/
			lstCategoryType = new ArrayList<ProductInfo>();
			lstCategoryType = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true,null,null).getLstObject();
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
//		mapControl = getMapControlToken();

		return SUCCESS;
	}
	
	/**
	 * @author tientv 
	 */
	public String details(){
		try{
			salesPlans = new ArrayList<DistributeSalePlanVO>();
			result.put("rows", salesPlans);
			/*if(typeSearch==null || typeSearch==0){								
				ObjectVO<DistributeSalePlanVO> vos = salePlanMgr.getListSalePlanWithNoCondition(null,null,null);
				if(vos!=null){
					result.put("total", vos.getLstObject().size());
					result.put("rows",vos.getLstObject());
				}else{
					result.put("total", 0);
				}				
				return JSON;
			}	*/		
			if(StringUtil.isNullOrEmpty(shopCode)){
				return JSON;
			}
			Shop sh = shopMgr.getShopByCode(shopCode);
			if(sh==null || !sh.getStatus().equals(ActiveType.RUNNING)){
				return JSON;
			}
			//khong xai chanel type nua
			/*if(!ChannelTypeType.SHOP.equals(sh.getType().getType()) || !sh.getType().getObjectType().equals(ShopType.SHOP.getValue()) ){
					return JSON;
			}*/
//			Integer MM = null, yyyy = null;			
//			if(StringUtil.isNullOrEmpty(searchMonth)){				
//				return JSON;
//			}
//			MM = DateUtil.getMonth(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));
//			yyyy = DateUtil.getYear(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));	
			if (yearPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.year")));
				return JSON;
			} 
			if (numPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.period")));
				return JSON;
			}
			
			List<String> lst = new ArrayList<String>();
			if(!StringUtil.isNullOrEmpty(categoryCode) && categoryCode.indexOf("-1") < 0){
				for(String str:categoryCode.split(",")){				
					lst.add(str);
				}
			}						
			salesPlans = salePlanMgr.getListSalePlanWithCondition(sh.getId(),shop.getId(), productCode, numPeriod, yearPeriod, lst, PlanType.MONTH);
			result.put("rows", salesPlans);
			result.put("total", salesPlans.size());
		}catch(Exception ex){
			LogUtility.logError(ex, "QuotaMonthPlanAction.details - " + ex.getMessage());
		}
		return JSON;
	}
	
	/**
	 * @author tientv
	 */
	public String showFooter(){
		try {
			if (yearPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.year")));
				return JSON;
			} 
			if (numPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.period")));
				return JSON;
			}
//			Integer MM = null, yyyy = null;			
//			if(StringUtil.isNullOrEmpty(searchMonth)){				
//				return JSON;
//			}
			Shop sh = shopMgr.getShopByCode(shopCode);
			if(sh==null || !sh.getStatus().equals(ActiveType.RUNNING)){
				return JSON;
			}
//			MM = DateUtil.getMonth(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));
//			yyyy = DateUtil.getYear(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));
			List<String> lst = new ArrayList<String>();
			if(!StringUtil.isNullOrEmpty(categoryCode)){
				for(String str:categoryCode.split(",")){				
					lst.add(str);
				}
			}	
			salesPlans = salePlanMgr.getListSalePlanWithCondition(sh.getId(),shop.getId(), productCode, numPeriod, yearPeriod, lst, PlanType.MONTH);
			DistributeSalePlanVO vo = new DistributeSalePlanVO();
			BigDecimal totalAmount = BigDecimal.ZERO;
			Integer totalQuantity = 0;
			for(DistributeSalePlanVO planVO:salesPlans){
				totalAmount = totalAmount.add(planVO.getAmount() != null ? planVO.getAmount() : BigDecimal.ZERO);
				totalQuantity += planVO.getQuantity() != null ? planVO.getQuantity() : 0; 
			}
			vo.setAmount(totalAmount);
			vo.setQuantity(totalQuantity);
			result.put("footer", vo);
		} catch (Exception ex) {
			LogUtility.logError(ex, "QuotaMonthPlanAction.showFooter - " + ex.getMessage());
		}
		return JSON;
	}
	
	
	/**
	 * Search shop.
	 *
	 * @return the string
	 * @author tientv
	 */
	public String searchShop(){
		
	    result.put("page", page);
	    result.put("max", max);
		try{			
			KPaging<Shop> kPaging = new KPaging<Shop>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);	
			if(shop==null){
				result.put("rows",new ArrayList<Shop>());
				return JSON;
			}
			ObjectVO<Shop> shopVO = null;	
			Long shopId = shop.getId();			
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.SHOP);							
	    	filter.setStatus(ActiveType.RUNNING);
	    	filter.setObjectType(3);	
	    	ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr)context.getBean("channelTypeMgr");
			ObjectVO<ChannelType> vos = channelTypeMgr.getListChannelType(filter,null);
			Long shopTypeId = null;
			if(vos!=null && vos.getLstObject().size()>0){
				shopTypeId = vos.getLstObject().get(0).getId();
			}
			ShopFilter sf = new ShopFilter();
			sf.setShopCode(code);
			sf.setShopName(name);
			sf.setkPaging(kPaging);
			sf.setLstParentId(shopId==null?null:Arrays.asList(shopId));
			sf.setStatus(ActiveType.RUNNING);
			sf.setShopTypeId(shopTypeId);
			shopVO = shopMgr.getListShop(sf);
										
			if(shopVO!= null){
				result.put("total", shopVO.getkPaging().getTotalRows());
			    result.put("rows", shopVO.getLstObject());		
			}			
			
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
			result.put(ERROR,true);
			result.put("errMsg",errMsg);
		}
		return JSON;
	}
	
	
	
	/**
	 * Sku search.
	 *
	 * @return the string
	 */
	public String skuSearch() {
		
		result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<DistributeSalePlanVO> kPaging = new KPaging<DistributeSalePlanVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);			
			ObjectVO<DistributeSalePlanVO> vos = salePlanMgr.getListSalePlanWithNoCondition(kPaging,code,name);	
			if(vos!= null){
				result.put("total", vos.getkPaging().getTotalRows());
			    result.put("rows", vos.getLstObject());		
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Allocation.
	 *
	 * @return the string
	 * @author tientv
	 */
	public String allocation(){
		
	    resetToken(result);	    
//	    boolean error = true;
		try{	
			Shop shopTemp = shopMgr.getShopByCode(shopCode);
			if(shopTemp == null){
				result.put(ERROR,true);
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.countinginput.exist",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.unit.tree.code")));
				return JSON;
			}else if(ActiveType.STOPPED.equals(shopTemp.getStatus())){
				result.put(ERROR,true);
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.promotion.effect",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.unit.tree.code")));
				return JSON;				
			}
			//khong xai chanel_type nua
			/*else if(!ChannelTypeType.SHOP.equals(shopTemp.getType().getType()) 
					|| !shopTemp.getType().getObjectType().equals(ShopType.SHOP.getValue()) ){
				result.put(ERROR,true);
				result.put("errMsg",Configuration.getResourceString("vi","sale.plan.qm.excel.shop.not.shop"));				
				return JSON;
			}*/
//			Integer MM = null, yyyy = null;
//			MM = DateUtil.getMonth(DateUtil.parse(month, ConstantManager.MONTH_YEAR_FORMAT));
//			yyyy = DateUtil.getYear(DateUtil.parse(month, ConstantManager.MONTH_YEAR_FORMAT));
			if (yearPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.year")));
				return JSON;
			} 
			if (numPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.period")));
				return JSON;
			}
			if(lstProduct!=null && lstProduct.size()>0){		
				salePlanMgr.createSalePlanForShop(shopTemp.getId(), numPeriod, yearPeriod, lstProduct, lstQuantity,lstAmount,currentUser.getUserName());
				result.put(ERROR, false);
			}			
		}catch(Exception ex){
			LogUtility.logError(ex, "QuotaMonthPlanAction.alocation - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error"));
		}
		return JSON;
	}
	
	/**
	 * Excel import.
	 *
	 * @return the string
	 * @author tientv
	 */
	public String excelImport(){
		resetToken(result);
		totalItem = 0;
		int num = 0;
		int year = 0;
		isError = true;
		totalItem = 0;
		String message = "";	   
	    List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 6);	
	    List<CellBean> lstFails = new ArrayList<CellBean>();
	    lstView = new ArrayList<CellBean>();
	    typeView = true;	    
	    SalePlan obj = new SalePlan();	    
	    String value = "";
	    Shop excelShop = null;
	    
	    if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {	    	    	
	    	try {    
	    		String sdup = null;
	    		List<String> row = null;
				for(int i=0;i<lstData.size();i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
						row = lstData.get(i);
						sdup = checkFileImport(lstData, i);
						if (!StringUtil.isNullOrEmpty(sdup)) {
							typeView = false;
							lstFails.add(StringUtil.addFailBean(row, sdup));
							continue;
						}
						//Ma don vi
						if(row.size()>=0){
							value = row.get(0);
							if(StringUtil.isNullOrEmpty(value)){	
								message += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,
									Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sale.plan.qm.excel.shop.pause"));
								message += "\n";
							}else{
								excelShop = shopMgr.getShopByCode(value);
								if(excelShop==null){
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
											Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.unit.tree.code"));
									message += "\n";
								}else{
									if(!ActiveType.RUNNING.equals(excelShop.getStatus())){
										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE,
											Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sale.plan.qm.excel.shop.pause"));
										message += "\n";
									}/*else if(!ChannelTypeType.SHOP.equals(excelShop.getType().getType()) 
											|| !excelShop.getType().getObjectType().equals(ShopType.SHOP.getValue()) ){
											message += Configuration.getResourceString("vi","sale.plan.qm.excel.shop.not.shop");
											message += "\n";
									}*/else{
										obj.setObjectId(excelShop.getId());
										//obj.setParent(excelShop.getParentShop());
									}
								}
								
							}
						}	
						//Ma san pham
						if(row.size()>=1){
							value = row.get(1);
							if(StringUtil.isNullOrEmpty(value)){	
								message += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,
									Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sale.plan.qm.excel.product.pause"));
								message += "\n";
							}else{
								Product product = productMgr.getProductByCode(value.trim());
								if(product==null){
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,
										Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sale.plan.qm.excel.product.pause"));
									message += "\n";									
								}else{
									if(!ActiveType.RUNNING.equals(product.getStatus())){
										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE,
											Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sale.plan.qm.excel.product.pause"));
										message += "\n";
									}else if(!productMgr.checkProductInZ(product.getProductCode())){
											message += Configuration.getResourceString("vi", "sale.plan.qm.excel.product.in.z"); 
											message += "\n";
									}else{
										obj.setProduct(product); 									
									}									
								}
							}
						}
						//chu ky
						if(row.size()>=3){
							String numStr = row.get(2).trim();
							String yearStr = row.get(3).trim();
							if (!StringUtil.isNullOrEmpty(numStr)) {
								try {
									num = Integer.parseInt(numStr);
//									if (num < 1 || num > 13) {
//										throw new Exception();
//									}									
								} catch (Exception e) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.number", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
									message += "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
								message += "\n";
							}
							
							if (!StringUtil.isNullOrEmpty(yearStr)) {
								try {
									year = Integer.parseInt(yearStr);
//									if (year < 1) {
//										throw new Exception();
//									}
								} catch (Exception e) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.number", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
									message += "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
								message += "\n";
							}
							
							if (num > 0 && year > 0) {
								Date now = DateUtil.now();
								Cycle curCycle = cycleMgr.getCycleByDate(now);
								if (curCycle != null) {
									if (year < DateUtil.getYear(now) || (year == DateUtil.getYear(now) && num < curCycle.getNum())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.allocation.date.allocation");
										message += "\n";
									} 
//									else {
//										Cycle cycle = cycleMgr.getCycleByYearAndNum(year, num);
//										obj.setCycle(cycle);
//									}
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.cycle", num + "/" + year));
									message += "\n";
								}
							}
						}
						//So luong
						if (row.size() >= 4) {
							value = row.get(4);
							if (StringUtil.isNullOrEmpty(value.trim())) {
								message += ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.quantity"));
								message += "\n";
							} else {
								try {
									if (value.trim().length() < 10) {
										Integer quantity = Integer.parseInt(value.trim());
										if (quantity <= 0) {
											message += ValidateUtil.getErrorMsg(ConstantManager.ERR_POSSITIVE_INTEGER, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.quantity"));
											message += "\n";
										} else {
											obj.setQuantity(quantity);
//											BigDecimal tmp = new BigDecimal(String.valueOf(obj.getQuantity()));
//											if (obj.getProduct() != null && obj.getObjectId() != null) {
//												Price price = productMgr.getPriceByProductId(obj.getObjectId(), obj.getProduct().getId());
//												if (price == null) {
//													message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.allocation.price"));
//													message += "\n";
//												} else {
//													tmp = tmp.multiply(price.getPrice());
//													obj.setAmount(tmp);
//												}
//											}
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.exceed.length", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.quantity"));
										message += "\n";
									}
								} catch (Exception ex) {
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_FORMAT, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.quantity"));
									message += "\n";
								}
							}
						}
						//Doanh số
						if (row.size() > 5) {
							value = row.get(5);
							if (!StringUtil.isNullOrEmpty(value)) {
								try {
									if (value.trim().length() < 22) {
										if (value.trim() == null && row.get(4).trim() == null) {
											message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.quantity.amount.distribute"));
											message += "\n";
										} else if (value.trim() == null) {
											obj.setAmount(BigDecimal.ZERO);
										} else if (Float.parseFloat(value) > -1) {
											BigDecimal tempAmount = new BigDecimal(String.valueOf(value));
											obj.setAmount(tempAmount);
											/*BigDecimal tt = new BigDecimal(String.valueOf(temp.getQuantity()));
											if (temp.getProduct() != null) {
												Price price = productMgr.getPriceByProductId(temp.getProduct().getId());
												if (price == null) {
													message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.allocation.price"));
													message += "\n";
												} else {
													tt = tt.multiply(price.getPrice());
													temp.setAmount(tt);
												}
											}*/
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.possitive.integer", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.amount.distribute"));
											message += "\n";
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.exceed.length", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.amount.distribute"));
										message += "\n";
									}
								} catch (Exception e) {
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_FORMAT, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.amount.distribute"));
									message += "\n";
								}
							}
//							else {
//								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.possitive.integer", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.amount.distribute"));
//								message += "\n";
//							}
						}
						//End input
//						boolean isExists = false;
//						SalePlan sp = null;
//						if (StringUtil.isNullOrEmpty(message)) {
//							SalePlan salePlanTmp = salePlanMgr.getSalePlanOfVNM(excelShop.getId(), obj.getProduct().getId(), SalePlanOwnerType.SHOP, PlanType.MONTH, num, year);
//							if(salePlanTmp==null){
//								isExists = false;
//							}else{
//								isExists=true;
//								Long salePlanId = salePlanTmp.getId();
//								sp = salePlanMgr.getSalePlanById(salePlanId);
//							}		
//						}						
						if (StringUtil.isNullOrEmpty(message)) {
							try{
								/*obj.setPrice(productMgr.getPriceByProductId(obj.getProduct().getId()).getPrice());
								obj.setObjectType(SalePlanOwnerType.SHOP);
								obj.setCreateUser(currentUser.getUserName());
								obj.setObjectId(excelShop.getId());		
								//obj.setVersion(1);	
								obj.setType(PlanType.MONTH);									
								obj.setCat(obj.getProduct().getCat());
								obj.setShop(excelShop);
								if(isExists){
									//sp.setVersion(sp.getVersion()+1);
									sp.setQuantity(obj.getQuantity());
									sp.setAmount(obj.getAmount());										
									salePlanMgr.updateSalePlanAndSalePlanVesion(sp,currentUser.getUserName());
								}else{
									salePlanMgr.insertSalePlanAndSalePlanVersion(obj);
								}	*/
								salePlanMgr.createSalePlanForShop(obj.getObjectId(), num, year, Arrays.asList(obj.getProduct().getId()), Arrays.asList(obj.getQuantity()), Arrays.asList(obj.getAmount()), currentUser.getUserName());
							} catch (BusinessException be) {
								LogUtility.logError(be, be.getMessage());
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
						
					}
				}
				getOutputFailSalePlan(lstFails, ConstantManager.TEMPLATE_SALE_PLAN_QUOTA_MONTH_FAIL);
	    	}catch(IndexOutOfBoundsException ex){
	    		 LogUtility.logError(ex, ex.getMessage());
	 		     errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.allocation.file.err");
	    	}catch(Exception ex){
	    		 LogUtility.logError(ex, ex.getMessage());
	 		     errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
			}
	    }
	    if(StringUtil.isNullOrEmpty(errMsg)){
	    	isError = false;
	    }
	    return SUCCESS;
	    
	}
	public void getOutputFailSalePlan(List<CellBean> lstFails, String templateName) {
		if (lstFails.size() > 0) {
			numFail = lstFails.size();
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + templateName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "template_sale_plan_create_fail");
			String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report", lstFails);
			params.put("stt", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_stt"));
			params.put("mdv", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_unit_code"));
			params.put("msp", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_product"));
			params.put("chuky", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
			params.put("nam", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
			params.put("soluong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_quantity"));
			params.put("doanhso", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_amount"));
			params.put("error", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_error"));
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.setSpreadsheetToRename("sheetName1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_import_sheet"));
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (InvalidFormatException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (IOException e) {
				//System.out.print(e.getMessage());
				LogUtility.logError(e, e.getMessage());
			}
			fileNameFail = Configuration.getStoreImportFailDownloadPath() + outputName;
		}
	}
	
	/**
	 * Tao file template dung de import ke hoach thang
	 * 
	 * @author longnh15
	 * @return Doi tuong chua duong dan toi file template
	 * @since 06/05/2015
	 */
	public String downloadTemplateExcelfile() {
		String reportToken = retrieveReportToken(reportCode);
		if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		String outputPath = this.createImportSalePlanTemplateFile();
		//String outputPath =  Configuration.getExportExcelPath() + importTemplateFileName;
		result.put(REPORT_PATH, outputPath);
		result.put(LIST, outputPath);
		result.put(ERROR, false);
		result.put("hasData", true);
		try {
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	/**
	 * Tao file mau import sale plan
	 * 
	 * @author longnh15
	 * @return Ten file template moi duoc tao
	 * @since 06/05/2015
	 */
	private String createImportSalePlanTemplateFile() {
		
		String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathSalePlan() + ConstantManager.TEMPLATE_SALE_PLAN_UNIT_CREATE;
		templateFileName = templateFileName.replace('/', File.separatorChar);
		String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "template_sale_plan_unit_create");
		String outputFileName = Configuration.getStoreRealPath() + outputName;
		outputFileName = outputFileName.replace('/', File.separatorChar);
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("mdv", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_unit_code"));
		params.put("msp", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_product"));
		params.put("chuky", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
		params.put("nam", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
		params.put("soluong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_quantity"));
		params.put("doanhso", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_amount"));

		XLSTransformer transformer = new XLSTransformer();
		try {
			transformer.setSpreadsheetToRename("sheetName1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_sale_plan_import_sheet"));
			transformer.transformXLS(templateFileName, params, outputFileName);
		} catch (ParsePropertyException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (InvalidFormatException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (IOException e) {
			//System.out.print(e.getMessage());
			LogUtility.logError(e, e.getMessage());
		}
		return Configuration.getExportExcelPath() + outputName;
	}
	/**
	 * @author tientv
	 */
	public String exportExcel(){
		InputStream inputStream = null;
		OutputStream os = null;
		try{
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			salesPlans = new ArrayList<DistributeSalePlanVO>();
			if(StringUtil.isNullOrEmpty(shopCode)){
				result.put(ERROR, true);
				return JSON;
			}
			Shop sh = shopMgr.getShopByCode(shopCode);
			if(sh==null || !sh.getStatus().equals(ActiveType.RUNNING)){
				result.put(ERROR, true);
				return JSON;
			}
//			Integer MM = null, yyyy = null;
			if(shop==null){
				result.put(ERROR, true);
				return JSON;
			}/*else if(!ChannelTypeType.SHOP.equals(sh.getType().getType()) 
					|| !sh.getType().getObjectType().equals(ShopType.SHOP.getValue()) ){
				result.put(ERROR,true);	
				return JSON;
			}*/
//			if(StringUtil.isNullOrEmpty(searchMonth)){
//				result.put(ERROR, true);
//				return JSON;
//			}
//			MM = DateUtil.getMonth(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));
//			yyyy = DateUtil.getYear(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));
			if (yearPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.year")));
				return JSON;
			} 
			if (numPeriod == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.month.create.period")));
				return JSON;
			}
			List<String> lst = new ArrayList<String>();
			if(!StringUtil.isNullOrEmpty(categoryCode)){
				for(String str:categoryCode.split(",")){				
					lst.add(str);
				}
			}	
			salesPlans = salePlanMgr.getListSalePlanWithCondition(sh.getId(),shop.getId(), productCode, numPeriod, yearPeriod, lst, PlanType.MONTH);
			BigDecimal totalAmount = BigDecimal.ZERO;
			Integer totalQuantity = 0;
			for(DistributeSalePlanVO planVO:salesPlans){
				totalAmount = totalAmount.add(planVO.getAmount()==null?new BigDecimal(0):planVO.getAmount());
				totalQuantity += planVO.getQuantity()==null?0:planVO.getQuantity();
			}
						
			Map<String, Object> beans = new HashMap<String, Object>();
			if(salesPlans.size()>0){				
				beans.put("hasData", 1);
				beans.put("shopName", sh.getShopCode() + " - " + sh.getShopName());
				beans.put("address", sh.getAddress());
//				beans.put("month", MM + "/" + yyyy);
				beans.put("numPeriod", numPeriod);
				beans.put("yearPeriod", yearPeriod);
				beans.put("printDate", DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY));
				beans.put("salesPlans", salesPlans);
				beans.put("totalAmount", StringUtil.convertMoney(totalAmount));
				beans.put("totalQuantity", totalQuantity);
			}else{
				beans.put("hasData", 0);
			}
			//replace
			beans.put("stt", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_stt"));
			beans.put("tensp", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_productname"));
			beans.put("msp", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_product"));
			beans.put("soluong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_quantity"));
			beans.put("doanhso", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_amount"));
			beans.put("ngayin", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_printdate"));
			beans.put("tieude", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_title"));
			beans.put("donvi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_unit"));
			beans.put("diachi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_address"));
			beans.put("chuky", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period"));
			beans.put("nam", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_year"));
			beans.put("tong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_total"));
			//end replace
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathStock(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.TEMPLATE_SALE_PLAN_QUOTA_EXPORT;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			
			String outputName = genExportFileSuffix() + "_" + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_sale_plan_export");
//			String exportFileName = (folder + outputName).replace('/', File.separatorChar);
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream,	beans);
			resultWorkbook.setSheetName(0, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_sale_plan_export_sheet"));
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			result.put(ERROR, false);
//			String outputPath = ServletActionContext.getServletContext().getContextPath() + Configuration.getExcelTemplatePathStock() + outputName;
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch(Exception ex) {
			result.put(ERROR, true);
			LogUtility.logError(ex, "QuotaMonthPlanAction.exportExcel" + ex.getMessage());
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	private String checkFileImport(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			List<String> row2 = null;
			message = "";
			String maDV = row1.get(0).toUpperCase();
			String sp = row1.get(1).toUpperCase();
			String thang = row1.get(2).toUpperCase();
			String slg = row1.get(3).toUpperCase();
			String maDV1 = null;
			String sp1 = null;
			String thang1 = null;
			String slg1 = null;
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					row2 = lstData.get(k);
					maDV1 = row2.get(0).toUpperCase();
					sp1 = row2.get(1).toUpperCase();
					thang1 = row2.get(2).toUpperCase();
					slg1 = row2.get(3).toUpperCase();
					if (maDV.equals(maDV1) && sp.equals(sp1) && thang.equals(thang1) && slg.equals(slg1)) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.import.duplicate", k + 2) ;
						} else {
							message += ", " + (k + 2);
						}
					}
				}
			}
		}
		return message;
	}
	
	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public Integer getTypeSearch() {
		return typeSearch;
	}

	public void setTypeSearch(Integer typeSearch) {
		this.typeSearch = typeSearch;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Integer getExcelType() {
		return excelType;
	}

	public void setExcelType(Integer excelType) {
		this.excelType = excelType;
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public List<DistributeSalePlanVO> getSalesPlans() {
		return salesPlans;
	}

	/**
	 * Sets the sales plans.
	 *
	 * @param salesPlans the new sales plans
	 */
	public void setSalesPlans(List<DistributeSalePlanVO> salesPlans) {
		this.salesPlans = salesPlans;
	}

	/**
	 * Gets the staff code under.
	 *
	 * @return the staff code under
	 */
	public String getStaffCodeUnder() {
		return staffCodeUnder;
	}

	/**
	 * Sets the staff code under.
	 *
	 * @param staffCodeUnder the new staff code under
	 */
	public void setStaffCodeUnder(String staffCodeUnder) {
		this.staffCodeUnder = staffCodeUnder;
	}

	/**
	 * Gets the from quantity.
	 *
	 * @return the from quantity
	 */
	public Integer getFromQuantity() {
		return fromQuantity;
	}

	/**
	 * Sets the from quantity.
	 *
	 * @param fromQuantity the new from quantity
	 */
	public void setFromQuantity(Integer fromQuantity) {
		this.fromQuantity = fromQuantity;
	}

	/**
	 * Gets the to quantity.
	 *
	 * @return the to quantity
	 */
	public Integer getToQuantity() {
		return toQuantity;
	}

	/**
	 * Sets the to quantity.
	 *
	 * @param toQuantity the new to quantity
	 */
	public void setToQuantity(Integer toQuantity) {
		this.toQuantity = toQuantity;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the excel file.
	 *
	 * @return the excel file
	 */
	public File getExcelFile() {
		return excelFile;
	}

	/**
	 * Sets the excel file.
	 *
	 * @param excelFile the new excel file
	 */
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}
	
	/**
	 * Gets the lst delete.
	 *
	 * @return the lst delete
	 */
	public List<Integer> getLstDelete() {
		return lstDelete;
	}

	/**
	 * Sets the lst delete.
	 *
	 * @param lstDelete the new lst delete
	 */
	public void setLstDelete(List<Integer> lstDelete) {
		this.lstDelete = lstDelete;
	}

	/**
	 * Gets the lst sale plan id.
	 *
	 * @return the lst sale plan id
	 */
	public List<Long> getLstSalePlanId() {
		return lstSalePlanId;
	}

	/**
	 * Sets the lst sale plan id.
	 *
	 * @param lstSalePlanId the new lst sale plan id
	 */
	public void setLstSalePlanId(List<Long> lstSalePlanId) {
		this.lstSalePlanId = lstSalePlanId;
	}

	/**
	 * Gets the excel file content type.
	 *
	 * @return the excel file content type
	 */
	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	/**
	 * Sets the excel file content type.
	 *
	 * @param excelFileContentType the new excel file content type
	 */
	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/**
	 * Gets the lst category type.
	 *
	 * @return the lst category type
	 */
	public List<ProductInfo> getLstCategoryType() {
		return lstCategoryType;
	}

	/**
	 * Sets the lst category type.
	 *
	 * @param lstCategoryType the new lst category type
	 */
	public void setLstCategoryType(List<ProductInfo> lstCategoryType) {
		this.lstCategoryType = lstCategoryType;
	}

	/**
	 * Gets the product info mgr.
	 *
	 * @return the product info mgr
	 */
	public ProductInfoMgr getProductInfoMgr() {
		return productInfoMgr;
	}

	/**
	 * Sets the product info mgr.
	 *
	 * @param productInfoMgr the new product info mgr
	 */
	public void setProductInfoMgr(ProductInfoMgr productInfoMgr) {
		this.productInfoMgr = productInfoMgr;
	}

	/**
	 * Gets the shop code.
	 *
	 * @return the shop code
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * Sets the shop code.
	 *
	 * @param shopCode the new shop code
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * Gets the sale plan mgr.
	 *
	 * @return the sale plan mgr
	 */
	public SalePlanMgr getSalePlanMgr() {
		return salePlanMgr;
	}

	/**
	 * Sets the sale plan mgr.
	 *
	 * @param salePlanMgr the new sale plan mgr
	 */
	public void setSalePlanMgr(SalePlanMgr salePlanMgr) {
		this.salePlanMgr = salePlanMgr;
	}

	/**
	 * Gets the shop mgr.
	 *
	 * @return the shop mgr
	 */
	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	/**
	 * Sets the shop mgr.
	 *
	 * @param shopMgr the new shop mgr
	 */
	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the obj sales plans.
	 *
	 * @return the obj sales plans
	 */
	public ObjectVO<SalePlanVO> getObjSalesPlans() {
		return objSalesPlans;
	}

	/**
	 * Sets the obj sales plans.
	 *
	 * @param objSalesPlans the new obj sales plans
	 */
	public void setObjSalesPlans(ObjectVO<SalePlanVO> objSalesPlans) {
		this.objSalesPlans = objSalesPlans;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * Sets the month.
	 *
	 * @param month the new month
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * Gets the staff mgr.
	 *
	 * @return the staff mgr
	 */
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	/**
	 * Sets the staff mgr.
	 *
	 * @param staffMgr the new staff mgr
	 */
	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#getStaff()
	 */
	public Staff getStaff() {
		return staff;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#setStaff(ths.dms.core.entities.Staff)
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	/**
	 * Gets the search month.
	 *
	 * @return the search month
	 */
	public String getSearchMonth() {
		return searchMonth;
	}

	/**
	 * Sets the search month.
	 *
	 * @param searchMonth the new search month
	 */
	public void setSearchMonth(String searchMonth) {
		this.searchMonth = searchMonth;
	}

	

	
	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#getErrMsg()
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#setErrMsg(java.lang.String)
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	/**
	 * Gets the shop.
	 *
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 *
	 * @param shop the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	
	/**
	 * Gets the lst product.
	 *
	 * @return the lst product
	 */
	public List<Long> getLstProduct() {
		return lstProduct;
	}

	/**
	 * Sets the lst product.
	 *
	 * @param lstProduct the new lst product
	 */
	public void setLstProduct(List<Long> lstProduct) {
		this.lstProduct = lstProduct;
	}

	/**
	 * Gets the lst quantity.
	 *
	 * @return the lst quantity
	 */
	public List<Integer> getLstQuantity() {
		return lstQuantity;
	}

	/**
	 * Sets the lst quantity.
	 *
	 * @param lstQuantity the new lst quantity
	 */
	public void setLstQuantity(List<Integer> lstQuantity) {
		this.lstQuantity = lstQuantity;
	}
	
	/**
	 * Gets the list amount.
	 * 
	 * @return the list amount
	 */
	public List<BigDecimal> getLstAmount() {
		return lstAmount;
	}

	/**
	 * Sets the list amount.
	 * 
	 * @param listAmount
	 *            the new list amount
	 */
	public void setLstAmount(List<BigDecimal> lstAmount) {
		this.lstAmount = lstAmount;
	}
	
	public Integer getYearPeriod() {
		return yearPeriod;
	}

	public void setYearPeriod(Integer yearPeriod) {
		this.yearPeriod = yearPeriod;
	}

	public Integer getNumPeriod() {
		return numPeriod;
	}

	public void setNumPeriod(Integer numPeriod) {
		this.numPeriod = numPeriod;
	}

	public CycleMgr getCycleMgr() {
		return cycleMgr;
	}

	public void setCycleMgr(CycleMgr cycleMgr) {
		this.cycleMgr = cycleMgr;
	}
}
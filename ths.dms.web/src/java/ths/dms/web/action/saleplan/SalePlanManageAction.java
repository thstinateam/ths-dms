package ths.dms.web.action.saleplan;

import java.math.BigDecimal;
import java.util.HashMap;

import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.SalePlanMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.SalesPlan;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class SalePlanManageAction.
 */
public class SalePlanManageAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The search shop. */
	private String searchShop;
	
	private String searchShopDisplay;
	
	/** The product code. */
	private String productCode;
	
	/** The search month. */
	private String searchMonth;
	
	/** The sale staff code. */
	private String saleStaffCode;
	
	
	/** The staff mgr. */
	private StaffMgr staffMgr;
	
	/** The shop mgr. */
	private ShopMgr shopMgr;
	
	/** The sale plan mgr. */
	private SalePlanMgr salePlanMgr;
	
	private ProductMgr productMgr;
	
	
	public ProductMgr getProductMgr() {
		return productMgr;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	private Shop shop;
	
	private Integer payment;
	
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * Gets the search shop.
	 *
	 * @return the search shop
	 */
	public String getSearchShop() {
		return searchShop;
	}

	/**
	 * Sets the search shop.
	 *
	 * @param searchShop the new search shop
	 */
	public void setSearchShop(String searchShop) {
		this.searchShop = searchShop;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the search month.
	 *
	 * @return the search month
	 */
	public String getSearchMonth() {
		return searchMonth;
	}

	/**
	 * Sets the search month.
	 *
	 * @param searchMonth the new search month
	 */
	public void setSearchMonth(String searchMonth) {
		this.searchMonth = searchMonth;
	}
	

	public String getSearchShopDisplay() {
		return searchShopDisplay;
	}

	public void setSearchShopDisplay(String searchShopDisplay) {
		this.searchShopDisplay = searchShopDisplay;
	}

	/**
	 * Gets the sale staff code.
	 *
	 * @return the sale staff code
	 */
	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	/**
	 * Sets the sale staff code.
	 *
	 * @param saleStaffCode the new sale staff code
	 */
	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}
	
	
	/**
	 * Gets the staff mgr.
	 *
	 * @return the staff mgr
	 */
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	/**
	 * Sets the staff mgr.
	 *
	 * @param staffMgr the new staff mgr
	 */
	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	/**
	 * Gets the shop mgr.
	 *
	 * @return the shop mgr
	 */
	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	/**
	 * Sets the shop mgr.
	 *
	 * @param shopMgr the new shop mgr
	 */
	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	/**
	 * Gets the sale plan mgr.
	 *
	 * @return the sale plan mgr
	 */
	public SalePlanMgr getSalePlanMgr() {
		return salePlanMgr;
	}

	/**
	 * Sets the sale plan mgr.
	 *
	 * @param salePlanMgr the new sale plan mgr
	 */
	public void setSalePlanMgr(SalePlanMgr salePlanMgr) {
		this.salePlanMgr = salePlanMgr;
	}
	
	@Override
	public void prepare() throws Exception {		
		super.prepare();
		staffMgr = (StaffMgr)context.getBean("staffMgr");
		shopMgr = (ShopMgr)context.getBean("shopMgr");
		salePlanMgr = (SalePlanMgr)context.getBean("salePlanMgr");
		result = new HashMap<String, Object>();
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());	
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();	
				}				
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}				
	}

	/**
	 * Search.
	 *
	 * @return the string
	 * @author tientv
	 * 
	 */
	@Override
	public String execute(){
		return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author tientv
	 */
	public String search() {
		
		result.put("page", page);
	    result.put("max", max);
		try{
			Integer MM = null, yyyy = null;
			KPaging<SalesPlan> kPaging = new KPaging<SalesPlan>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			Long shopId;			
			if(StringUtil.isNullOrEmpty(searchShop)){				
				shopId=null;
			}else{
				Shop shopTemp = shopMgr.getShopByCode(searchShop);
				if(shopTemp==null){
					return JSON;
				}
				shopId = shopTemp.getId();
			}			
			if(StringUtil.isNullOrEmpty(searchMonth)){				
				return JSON;
			}	
			MM = DateUtil.getMonth(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));
			yyyy = DateUtil.getYear(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));	
			if(StringUtil.isNullOrEmpty(saleStaffCode)){				
				return JSON;
			}
			Staff saleStaff = staffMgr.getStaffByCode(saleStaffCode);
			if(saleStaff==null){
				return JSON;
			}
			String productCodeTmp = null;
			if(!StringUtil.isNullOrEmpty(productCode)){
				productCodeTmp = productCode;
			}
			ObjectVO<SalesPlan> objVO = salePlanMgr.getListSalePlanForManagerPlanSearchLike(kPaging, shopId, saleStaff.getId(), productCodeTmp, MM, yyyy);
			if(objVO!=null){
				result.put("total", objVO.getkPaging().getTotalRows());
			    result.put("rows", objVO.getLstObject());		
			}
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
			return JSON;
		}
		return JSON;
	}	
	
	public String payment(){
		
		BigDecimal amount = BigDecimal.ZERO;
		try{
			Integer MM = null, yyyy = null;			
			Long shopId;
			if(StringUtil.isNullOrEmpty(searchShopDisplay)){
				searchShop="";
			}
			if(StringUtil.isNullOrEmpty(searchShop)){				
				shopId=null;
			}else{
				Shop shopTemp = shopMgr.getShopByCode(searchShop);
				if(shopTemp==null){
					return JSON;
				}
				shopId = shopTemp.getId();
			}			
			if(StringUtil.isNullOrEmpty(searchMonth)){				
				return JSON;
			}	
			MM = DateUtil.getMonth(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));
			yyyy = DateUtil.getYear(DateUtil.parse(searchMonth, ConstantManager.MONTH_YEAR_FORMAT));	
			if(StringUtil.isNullOrEmpty(saleStaffCode)){				
				return JSON;
			}
			Staff saleStaff = staffMgr.getStaffByCode(saleStaffCode);
			if(saleStaff==null){
				return JSON;
			}
			Long productId;
			if(StringUtil.isNullOrEmpty(productCode)){
				productId=null;
			}else{
				Product product = productMgr.getProductByCode(productCode);
				if(product==null){
					return JSON;
				}
				productId = product.getId();
			}			
			
			ObjectVO<SalesPlan> objVO = salePlanMgr.getListSalePlanForManagerPlan(null, shopId, saleStaff.getId(), productId, MM, yyyy);
			if(objVO!=null){
				for(SalesPlan obj:objVO.getLstObject()){
					amount = amount.add(obj.getAmount());
				}
			}			
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
		}
		result.put("amount", amount);
		return JSON;
	}
	public String searchShop(){
		try{
			String shopResult = "";
			if(StringUtil.isNullOrEmpty(searchShop)){				
				result.put("shop",shopResult);
			}else{
				Shop shopTemp = shopMgr.getShopByCode(searchShop);
				if(shopTemp==null){
					result.put("shop",shopResult);
					return JSON;
				}
				result.put("shop",shopTemp.getShopCode() + "-" + shopTemp.getShopName());
			}	
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
		}
		return JSON;
	}
	
	
	
}

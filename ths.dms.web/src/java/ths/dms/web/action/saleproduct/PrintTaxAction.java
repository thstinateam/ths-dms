package ths.dms.web.action.saleproduct;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ReportManagerMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.InvoiceFilter;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SaleOrderVOEx;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.VATVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class PrintTaxAction.
 */
public class PrintTaxAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The tax value string. */
	private String taxValueString;

	/** The tax value. */
	private Float taxValue;

	/** The order number. */
	private String orderNumber;

	/** The from date. */
	private String fromDate;

	/** The to date. */
	private String toDate;

	/** The sale staff. */
	private String saleStaff;

	/** The short code. */
	private String shortCode;

	/** The list sale order. */
	private List<SaleOrder> listSaleOrder;

	private List<Invoice> listInvoice;

	private List<SaleOrderVOEx> listSaleOrderVOEx;

	/** The shop. */
	private Shop shop;

	/** The staff. */
	private Staff staff;

	/** The output name. */
	private String outputName;

	/** The checkbox. */
	private List<String> checkbox;

	/** The cust payment. */
	private int custPayment;

	private String buyer;

	private String stockStaff;

	private String selStaff;

	/** The sale order mgr. */
	private SaleOrderMgr saleOrderMgr;

	private CommonMgr commonMgr;

	/** The staff mgr. */
	private StaffMgr staffMgr;

	private ReportManagerMgr reportManagerMgr;

	private List<Staff> listNVBH;
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH
	private List<String> lstShopParamCode;	

	private String addressCus;

	private List<Map<String, Object>> listObjectBeans;

	private String status;
	
	private String numberValue;

	private Date sysDate;

	private Date lockDay;

	private Integer isValueOrder;
	
	private Integer numberValueOrder;

	private String shopCode;
	
	private String code;

	private String pdfBase64;
	private String downloadPath;
	private String outputPath;
	private String shopCodeSearch;

	private File zipFile;

	private String fileName;

	private String day;

	private String month;

	private String year;
	
	private Integer windowWidth;
	
	private Integer windowHeight;

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getOutputPath() {
		return outputPath;
	}

	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File getZipFile() {
		return zipFile;
	}

	public void setZipFile(File zipFile) {
		this.zipFile = zipFile;
	}

	public String getPdfBase64() {
		return pdfBase64;
	}

	public void setPdfBase64(String pdfBase64) {
		this.pdfBase64 = pdfBase64;
	}

	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	public Date getLockDay() {
		return lockDay;
	}

	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getSelStaff() {
		return selStaff;
	}

	public void setSelStaff(String selStaff) {
		this.selStaff = selStaff;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Integer getWindowWidth() {
		return windowWidth;
	}

	public void setWindowWidth(Integer windowWidth) {
		this.windowWidth = windowWidth;
	}

	public Integer getWindowHeight() {
		return windowHeight;
	}

	public void setWindowHeight(Integer windowHeight) {
		this.windowHeight = windowHeight;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		reportManagerMgr = (ReportManagerMgr) context.getBean("reportManagerMgr");
		try {
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			sysDate = commonMgr.getSysDate();
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null && staff.getShop() != null) {
					shopCode = currentUser.getShopRoot().getShopCode();
					lockDay = shopLockMgr.getNextLockedDay(currentUser.getShopRoot().getShopId());
					if (lockDay == null) {
						lockDay = sysDate;
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "AdjustmentOrder.prepare");
		}
	}

	/**
	 * Hien thi giao dien.
	 * 
	 * @return the string
	 * @throws Exception
	 *             the exception
	 * @author tungmt
	 */
	@Override
	public String execute() throws Exception {
		try {
			/*
			 * if (!checkRolesAndGetShop(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) {
			 * result.put(ERROR, true); result.put("errMsg",
			 * ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION,
			 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
			 * "sp.print.tax.search.is.permission"))); }
			 */
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			//			shop = staff.getShop();	
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			Date now = DateUtil.getCurrentGMTDate();
			Date dayLock = shopLockMgr.getNextLockedDay(shop.getId());
			if (dayLock != null) {
				now = dayLock;
			}
			fromDate = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
			toDate = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
			/*
			 * if(!checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)){ return
			 * PAGE_NOT_PERMISSION; }
			 */
			
			//Lay danh sach nhan vien theo phan quyen
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				filter.setIsLstChildStaffRoot(true);
			} else {
				filter.setIsLstChildStaffRoot(false);
			}
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setLstShopId(Arrays.asList(currentUser.getShopRoot().getShopId()));
			//Lay NVBH
			filter.setLstObjectType(Arrays.asList(StaffObjectType.NVBH.getValue(), StaffObjectType.NVVS.getValue()));
			ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
			setLstNVBHVo(objVO.getLstObject());
			/*ObjectVO<Staff> staffVO = staffMgr.getListPreAndVanStaff(null, null, null, shop.getId(), ActiveType.RUNNING, Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
			if (staffVO != null && staffVO.getLstObject().size() > 0) {
				listNVBH = staffVO.getLstObject();
			}*/

			ShopParam buyerParam = commonMgr.getShopParamByType(shop.getId(), "VAT_BUYER");
			if (buyerParam != null) {
				buyer = buyerParam.getDescription();
			} else {
				buyer = "";
			}

			ShopParam stockStaffParam = commonMgr.getShopParamByType(shop.getId(), "VAT_STOCK_STAFF");
			if (stockStaffParam != null) {
				stockStaff = stockStaffParam.getDescription();
			} else {
				stockStaff = "";
			}
			
			ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), "ORDER_SEARCH");
			if (numberValueOrderParam != null) {
				numberValue = numberValueOrderParam.getCode();
			} else {
				numberValue = "";
			}

			ShopParam selStaffParam = commonMgr.getShopParamByType(shop.getId(), "VAT_SALE_STAFF");
			if (selStaffParam != null) {
				selStaff = selStaffParam.getDescription();
			} else {
				selStaff = "";
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Parses the float.
	 * 
	 * @return true, if successful
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public boolean parseFloat() {
		try {
			if (!StringUtil.isNullOrEmpty(taxValueString)) {
				taxValue = Float.parseFloat(taxValueString);
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Parses the long.
	 * 
	 * @param test
	 *            the test
	 * @return true, if successful
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public boolean parseLong(String test) {
		try {
			Long temp;
			if (test != null) {
				temp = Long.parseLong(test);
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Kiem tra quyen.
	 * 
	 * @param expertRole
	 *            the expert role
	 * @return true, if successful
	 * @author tungmt
	 * @since Sep 27, 2012
	 */
	/*
	 * public boolean checkRolesAndGetShop(VSARole... expertRole){ try {
	 * resetToken(result);
	 * 
	 * if(currentUser != null && currentUser.getUserName() != null){ staff =
	 * staffMgr.getStaffByCode(currentUser.getUserName()); if(staff != null &&
	 * staff.getShop() != null) shop = staff.getShop(); } if(shop == null){
	 * return false; } return checkRoles(expertRole); }catch(Exception e){
	 * LogUtility.logError(e, "PrintTax.checkRolesAndGetShop"); return false; }
	 * }
	 */

	/**
	 * Checks if is this date valid.
	 * 
	 * @author tungmt
	 * @return true, if is this date valid
	 */
	public boolean isThisDateValid(String dateToValidate) {
		if (dateToValidate == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		try {
			//if not valid, it will throw ParseException
			sdf.parse(dateToValidate);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	/**
	 * Tim kiem hoa don.
	 * 
	 * @return the string
	 * @author tungmt
	 */
	public String searchTax() {
		result.put("page", page);
		result.put("max", max);
		String returnString = ERROR;
		try {
			KPaging<Invoice> kPaging = new KPaging<Invoice>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			result.put("rows", new ArrayList<Invoice>());
			//			if (!checkRolesAndGetShop(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) {
			//				result.put(ERROR, true);
			//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.search.is.permission")));
			//			}else{				
			errMsg = "";
			if (!parseFloat()) {
				result.put(ERROR, true);
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_FLOAT, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.vat.title"));
				result.put("errMsg", errMsg);
				return ERROR;
			} else {
				Staff staffSign = getStaffByCurrentUser();
				if (staffSign == null)
					return JSON;
				Shop shopTmp = null;
				if (!StringUtil.isNullOrEmpty(shopCodeSearch)) {
					shopTmp = shopMgr.getShopByCode(shopCodeSearch);
				}
				Date startTemp = null;
				Date endTemp = null;
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					startTemp = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					endTemp = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
					if (shopTmp != null) {
						Date lockDate = shopLockMgr.getNextLockedDay(shopTmp.getId());
						if (lockDate != null) {
							if (DateUtil.compareDateWithoutTime(endTemp, lockDate) == 1) {
								endTemp = lockDate;
							}
						}
					}
				}
				if (startTemp != null && endTemp != null && startTemp.compareTo(endTemp) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.fromdate.greater.todate"));
				} else if ((!StringUtil.isNullOrEmpty(fromDate) && !isThisDateValid(fromDate)) || (!StringUtil.isNullOrEmpty(toDate) && !isThisDateValid(toDate))) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.invalid.date"));
				} else {
					ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), "ORDER_SEARCH");
					InvoiceCustPayment payment = null;
					if (custPayment != 0) {
						payment = InvoiceCustPayment.parseValue(custPayment);
					}
					InvoiceFilter invoiceFilter = new InvoiceFilter();
					invoiceFilter.setkPaging(kPaging);
					invoiceFilter.setTaxValue(taxValue);
					invoiceFilter.setOrderNumber(orderNumber);
					invoiceFilter.setFromDate(startTemp);
					invoiceFilter.setToDate(endTemp);
					invoiceFilter.setCustomerAddr(addressCus);
					invoiceFilter.setShortCode(shortCode);
					invoiceFilter.setStaffCode(saleStaff);
					invoiceFilter.setCustPayment(payment);
					invoiceFilter.setStatus(InvoiceStatus.USING);
					if (shopCodeSearch != null) {
						Shop sh = shopMgr.getShopByCode(shopCodeSearch);
						invoiceFilter.setShopId(sh.getId());
					} else {
						Shop sh = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
						invoiceFilter.setShopId(sh.getId());
					}
					invoiceFilter.setIsValueOrder(isValueOrder);

					//	invoiceFilter.setNumberValueOrder(numberValueOrder);
					if (numberValueOrder != null) {
						numberValueOrderParam.setCode(numberValueOrder.toString());
						numberValueOrderParam.setName(numberValueOrder.toString());
						numberValueOrderParam.setUpdateUser(currentUser.getUserName());
						saleOrderMgr.updateShopParamByInvoice(numberValueOrderParam);
					}
					switch (Integer.parseInt(status)) {
					case 0:
						//invoiceFilter.setIsSOPrint(null);
						invoiceFilter.setIsInvoicePrint(null);
						break;
					case 1:
						//invoiceFilter.setIsSOPrint(true);
						invoiceFilter.setIsInvoicePrint(true);
						break;
					case 2:
						//invoiceFilter.setIsSOPrint(false);
						invoiceFilter.setIsInvoicePrint(false);
						break;
					}

					ObjectVO<Invoice> objInvoice = saleOrderMgr.getListInvoice(invoiceFilter);
					listInvoice = new ArrayList<Invoice>();
					if (objInvoice != null) {
						result.put("total", objInvoice.getkPaging().getTotalRows());
						result.put("rows", objInvoice.getLstObject());
					} else {
						result.put("total", 0);
					}
					result.put("page", page);
					returnString = SUCCESS;
				}
			}
			//			}
		} catch (Exception e) {
			LogUtility.logError(e, "PrintTaxAction.searchTax");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * format chuoi abc {0} xyz {1}.
	 * 
	 * @param text
	 *            the text
	 * @param params
	 *            the params
	 * @return the string
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public String stringFormat(String text, Object... params) {
		for (int i = 0, size = params.length; i < size; i++) {
			if (params[i] != null) {
				text = text.replace("{" + i + "}", params[i].toString());
			}
		}
		return text;
	}

	public String moneyString(BigDecimal money) {
		String moneyString = "";
		money = money.round(new MathContext(money.toBigInteger().toString().length(), RoundingMode.HALF_UP));
		moneyString = tranlate(money.toString());
		return moneyString;
	}

	public void calculate() {
		//tinh amount
		for (int i = 0; i < listSaleOrderVOEx.size(); i++) {
			SaleOrderVOEx sove = listSaleOrderVOEx.get(i);
			BigDecimal disAmount = new BigDecimal(0);//tien chiet khau
			BigDecimal amount = new BigDecimal(0);//cong tien hang
			Integer indexMoney = -1;
			//			Boolean flagPromotion= false;
			Boolean isFreeMoney = false;
			if (sove.getLstSaleOrderDetail() != null && sove.getLstSaleOrderDetail().size() > 0) {
				for (int j = 0; j < sove.getLstSaleOrderDetail().size(); j++) {
					SaleOrderDetail sod = sove.getLstSaleOrderDetail().get(j);
					if (sod.getIsFreeItem().equals(1)) {//hang KM
					//					if(flagPromotion) listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(j).setIsFreeItem(0);
					//					else flagPromotion = true;
						if (sod.getProduct() == null) {//KM tien
							disAmount = disAmount.add(sod.getDiscountAmount());
							if (isFreeMoney) {//dong KM tien thu 2 tro di can xoa 
								listSaleOrderVOEx.get(i).getLstSaleOrderDetail().remove(j);
								j--;
							} else {
								indexMoney = j;//luu lai dong KM tien thu 1 de update tong tien KM
								isFreeMoney = true;
							}
						} else {//KM SP
							listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(j).setDiscountAmount(new BigDecimal(0));
						}
						/**
						 * neu la detail KM thu 2 va ko phai la dong KM tien can xoa
						 */

					} else {//hang ban
						BigDecimal disAmountSale = new BigDecimal(0);
						disAmountSale = disAmountSale.add(sod.getPriceNotVat().multiply(new BigDecimal(sod.getQuantity())));//thanh tien
						amount = amount.add(disAmountSale);
						listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(j).setDiscountAmount(disAmountSale);
					}
					listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(j).setCreateUser("");
					if (sod.getProduct() != null && sod.getQuantity() != null) {
						if (sod.getQuantity() >= sod.getProduct().getConvfact()) {
							listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(j).setCreateUser(convertConvfact(sod.getQuantity(), sod.getProduct().getConvfact()) + " " + sod.getProduct().getUom2().substring(0, 1));
						} else {
							listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(j).setCreateUser(convertConvfact(sod.getQuantity(), sod.getProduct().getConvfact()) + " " + sod.getProduct().getUom1().substring(0, 1));

						}
					}
				}
			}
			/** cong tien hang va thue */
			BigDecimal moneyVat = amount.multiply(new BigDecimal(sove.getInvoice().getVat()));// *vat
			moneyVat = moneyVat.divide(new BigDecimal(100));// /100;
			moneyVat = moneyVat.setScale(0, RoundingMode.HALF_UP);
			amount = amount.setScale(0, RoundingMode.HALF_UP);
			amount = amount.add(moneyVat);//
			if (indexMoney != -1) {
				disAmount = disAmount.setScale(0, RoundingMode.HALF_UP);
				listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(indexMoney).setDiscountAmount(disAmount);
				//				//loctt - Oct8,2013-begin
				//				Product product  = new Product();
				//				product.setProductName("Chiết khấu thương mại");
				//				listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(indexMoney).setProduct(product); 
				//				//loctt - Oct8,2013-end
				amount = amount.subtract(disAmount);
			}
			listSaleOrderVOEx.get(i).setAmount(amount);
		}
	}

	public BigDecimal calculateAmount(SaleOrderVOEx sove) {
		BigDecimal result = new BigDecimal(0);
		for (SaleOrderDetail sod : sove.getLstSaleOrderDetail()) {
			if (sod.getIsFreeItem().equals(1))
				break;
			result = result.add(sod.getPriceNotVat().multiply(new BigDecimal(sod.getQuantity())));
		}
		return result;
	}

	/**
	 * in hoa don.
	 * 
	 * @return the string
	 * @author phuongvm
	 */
	public String printTax() {
		//		if(!checkRolesAndGetShop(VSARole.VNM_SALESONLINE_DISTRIBUTOR)){
		//			result.put(ERROR, true);
		//			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.is.permission")));
		//			return ERROR;
		//		}		
		if (checkbox == null || checkbox.size() == 0) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} else {
			try {
				Map<String, Object> listBeans = new HashMap<String, Object>();
				List<Map<String, Object>> listObjectBeans = new ArrayList<Map<String, Object>>();
				List<Long> listInvoiceId = new ArrayList<Long>();
				List<Invoice> listInvoice = new ArrayList<Invoice>();
				boolean flag = true;
				List<String> str = new ArrayList<String>();
				for (int i = 0; i < checkbox.size(); i++) {
					flag = true;
					for (int j = 0; j < i; j++)
						if (checkbox.get(j).equals(checkbox.get(i)))
							flag = false;
					if (flag) {
						listInvoiceId.add(Long.valueOf(checkbox.get(i)));
					}

				}
				listSaleOrderVOEx = saleOrderMgr.getListSaleOrderVOEx(listInvoiceId);
				calculate();
				Date lockDate = shopLockMgr.getNextLockedDay(listSaleOrderVOEx.get(0).getInvoice().getShop().getId());
				if (lockDate == null) {
					lockDate = commonMgr.getSysDate();
				}
				for (int i = 0; i < listSaleOrderVOEx.size(); i++) {
					String currentDay = "";
					Calendar calendar = new GregorianCalendar();
					Date printDay = calendar.getTime();
					Date temp = listSaleOrderVOEx.get(i).getInvoice().getOrderDate();
					;
					String daycustom = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.format.day.custom");
					currentDay = stringFormat(daycustom, String.valueOf(DateUtil.getDay(temp)), String.valueOf(DateUtil.getMonth(temp)), String.valueOf(DateUtil.getYear(temp)));
					Invoice invoiceTemp = listSaleOrderVOEx.get(i).getInvoice();

					Map<String, Object> beans = new HashMap<String, Object>();
					if (!InvoiceStatus.CANCELED.equals(invoiceTemp.getStatus())) {
						beans.put("hasData", 1);
						beans.put("saleOrder", listSaleOrderVOEx.get(i).getSaleOrder());
						beans.put("listSODSale", listSaleOrderVOEx.get(i).getLstSaleOrderDetail());
						List<SaleOrderDetail> listDetail = new ArrayList<SaleOrderDetail>();
						List<SaleOrderDetail> listDetailFree = new ArrayList<SaleOrderDetail>();
						for (int j = 0; j < listSaleOrderVOEx.get(i).getLstSaleOrderDetail().size(); j++) {
							SaleOrderDetail detail = listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(j);
							if (detail.getProduct() != null && detail.getIsFreeItem() == 0) {
								listDetail.add(detail);
							} else if (detail.getProduct() != null && detail.getIsFreeItem() == 1) {
								listDetailFree.add(detail);
							}
						}
						beans.put("listDetail", listDetail);
						beans.put("listDetailFree", listDetailFree);
						beans.put("invoice", listSaleOrderVOEx.get(i).getInvoice());
						BigDecimal money = listSaleOrderVOEx.get(i).getAmount();
						beans.put("moneyString", moneyString(money.subtract(listSaleOrderVOEx.get(i).getInvoice().getDiscount())));
						beans.put("amount", calculateAmount(listSaleOrderVOEx.get(i)));
						beans.put("totalAmount", money);
						beans.put("currentDay", currentDay);
						beans.put("stockStaff", stockStaff);
						beans.put("printDay", DateUtil.toDateString(printDay, DateUtil.DATE_FORMAT_DDMMYYYY));
						if (InvoiceCustPayment.MONEY.equals(invoiceTemp.getCustPayment())) {
							beans.put("custPayment", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "payment.type.money"));
						} else if (InvoiceCustPayment.BANK_TRANSFER.equals(invoiceTemp.getCustPayment())) {
							beans.put("custPayment", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "payment.type.bank.transfer"));
						} else {
							beans.put("custPayment", "");
						}
						listObjectBeans.add(beans);
						//							if(invoiceTemp.getInvoiceDate() == null){
						//								invoiceTemp.setInvoiceDate(lockDate);
						//							}
						invoiceTemp.setInvoiceDate(DateUtil.now());
						listInvoice.add(invoiceTemp);
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
						return ERROR;
					}
				}
				listBeans.put("listBeans", listObjectBeans);
				InputStream inputStream = null;
				OutputStream os = null;
				Workbook resultWorkbook = null;
				try {
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathSaleProduct(); //Configuration.getStoreRealPath();
					String templateFileName = folder + ConstantManager.EXPORT_PRINTTAX_TEMPLATE;
					templateFileName = templateFileName.replace('/', File.separatorChar);

					outputName = ConstantManager.EXPORT_PRINTTAX + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					resultWorkbook = transformer.transformXLS(inputStream, listBeans);

					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();

					result.put(ERROR, false);
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(LIST, outputPath);

					/** Set invoice_date la ngay chot */
					saleOrderMgr.updateListInvoice(listInvoice);

				} catch (Exception e) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
					LogUtility.logError(e, e.getMessage());
				} finally {
					if (inputStream != null) {
						inputStream.close();
					}
					if (os != null) {
						os.close();
					}
				}
			} catch (FileNotFoundException e) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				LogUtility.logError(e, e.getMessage());
				return ERROR;
			} catch (ParsePropertyException e) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				LogUtility.logError(e, e.getMessage());
				return ERROR;
			} catch (IOException e) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				LogUtility.logError(e, e.getMessage());
				return ERROR;
			} catch (BusinessException e) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				LogUtility.logError(e, e.getMessage());
				return ERROR;
			} catch (Exception e) {
				LogUtility.logError(e, "PrintTaxAction.printTax");
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				result.put(ERROR, true);
			}
		}
		return JSON;
	}

	public String viewTax() {
		if (checkbox == null || checkbox.size() == 0) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} else {
			try {
				//List<Invoice> listInvoice = new ArrayList<Invoice>();
				listObjectBeans = new ArrayList<Map<String, Object>>();
				List<Long> listInvoiceId = new ArrayList<Long>();
				boolean flag = true;
				List<String> str = new ArrayList<String>();
				for (int i = 0; i < checkbox.size(); i++) {
					flag = true;
					for (int j = 0; j < i; j++)
						if (checkbox.get(j).equals(checkbox.get(i)))
							flag = false;
					if (flag) {
						listInvoiceId.add(Long.valueOf(checkbox.get(i)));
					}
				}

				listSaleOrderVOEx = saleOrderMgr.getListSaleOrderVOEx(listInvoiceId);
				calculate();
				Date lockDate = shopLockMgr.getNextLockedDay(listSaleOrderVOEx.get(0).getInvoice().getShop().getId());
				if (lockDate == null) {
					lockDate = commonMgr.getSysDate();
				}
				for (int i = 0; i < listSaleOrderVOEx.size(); i++) {
					String currentDay = "";
					Calendar calendar = new GregorianCalendar();
					//Date printDay = calendar.getTime();
					Date temp = listSaleOrderVOEx.get(i).getInvoice().getOrderDate();
					;
					String daycustom = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.format.day.custom");
					currentDay = stringFormat(daycustom, String.valueOf(DateUtil.getDay(temp)), String.valueOf(DateUtil.getMonth(temp)), String.valueOf(DateUtil.getYear(temp)));
					Invoice invoiceTemp = listSaleOrderVOEx.get(i).getInvoice();

					Map<String, Object> beans = new HashMap<String, Object>();
					if (!InvoiceStatus.CANCELED.equals(invoiceTemp.getStatus())) {
						beans.put("hasData", 1);
						beans.put("ngay", day);
						beans.put("thang", month);
						beans.put("nam", year);
						beans.put("saleOrder", listSaleOrderVOEx.get(i).getSaleOrder());
						beans.put("diachiKH", listSaleOrderVOEx.get(i).getDiachiKH());
						beans.put("diachiHD", listSaleOrderVOEx.get(i).getDiachiHD());
						beans.put("listSODSale", listSaleOrderVOEx.get(i).getLstSaleOrderDetail());
						List<SaleOrderDetail> listDetail = new ArrayList<SaleOrderDetail>();
						List<SaleOrderDetail> listDetailFree = new ArrayList<SaleOrderDetail>();
						for (int j = 0; j < listSaleOrderVOEx.get(i).getLstSaleOrderDetail().size(); j++) {
							SaleOrderDetail detail = listSaleOrderVOEx.get(i).getLstSaleOrderDetail().get(j);
							detail.setRowNum(j + 1);
							if (detail.getProduct() != null && detail.getIsFreeItem() == 0) {
								listDetail.add(detail);
							} else if (detail.getProduct() != null && detail.getIsFreeItem() == 1) {
								listDetailFree.add(detail);
							}
						}
						beans.put("listDetail", listDetail);
						beans.put("listDetailFree", listDetailFree);
						beans.put("invoice", listSaleOrderVOEx.get(i).getInvoice());
						BigDecimal money = listSaleOrderVOEx.get(i).getAmount();
						beans.put("moneyString", moneyString(money.subtract(listSaleOrderVOEx.get(i).getInvoice().getDiscount())));
						beans.put("amount", calculateAmount(listSaleOrderVOEx.get(i)));
						BigDecimal amountVAT = ((BigDecimal) beans.get("amount")).multiply(BigDecimal.valueOf(((Invoice) beans.get("invoice")).getVat()));
						amountVAT = amountVAT.divide(BigDecimal.valueOf(100l)).setScale(0, RoundingMode.FLOOR);
						beans.put("amountVAT", amountVAT);
						beans.put("totalAmount", money);
						beans.put("currentDay", currentDay);
						beans.put("stockStaff", stockStaff);
						Calendar cal = new GregorianCalendar();
						cal.setTime(temp);
						Date printDay = cal.getTime();
						beans.put("printDay", DateUtil.toDateString(printDay, DateUtil.DATE_FORMAT_DDMMYYYY));
						if (InvoiceCustPayment.MONEY.equals(invoiceTemp.getCustPayment())) {
							beans.put("custPayment", "Tiền mặt");
						} else if (InvoiceCustPayment.BANK_TRANSFER.equals(invoiceTemp.getCustPayment())) {
							beans.put("custPayment", "Chuyển khoản");
						} else {
							beans.put("custPayment", "");
						}
						listObjectBeans.add(beans);

						//invoiceTemp.setInvoiceDate(lockDate);
						//listInvoice.add(invoiceTemp);
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
						return ERROR;
					}
				}

				//				String templatePath = StringUtil.replaceSeparatorChar((new StringBuilder(ServletActionContext.getServletContext().getRealPath("/"))
				//					.append("/resources/templates/sale-product/vat/template-vat."+shopCode.toLowerCase()+".jasper")
				//				).toString());
				//				
				//				Map<String, Object> vatParameters = new HashMap<String, Object>();
				//				
				//				JRDataSource dataSource = new JRBeanCollectionDataSource(listObjectBeans);
				//				
				//				String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE))
				//				.append("_").append("vat."+shopCode.toLowerCase()+".pdf")).toString());
				//				
				//				String outputPath = Configuration.getStoreRealPath() + outputFile;
				//				
				//				String outputDownload = Configuration.getExportExcelPath() + outputFile;
				//				
				//				JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, vatParameters, dataSource);
				//				
				//				JRAbstractExporter exporter = new JRPdfExporter();
				//				
				//				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				//				
				//				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
				//				exporter.exportReport();
				//				
				//				result.put(REPORT_PATH, outputDownload);
				//				
				//				/**Set invoice_date la ngay chot*/
				//				//saleOrderMgr.updateListInvoice(listInvoice);
				//				
				//				return JSON;
			} catch (Exception e) {
				LogUtility.logError(e, "PrintTaxAction.printTax");
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				result.put(ERROR, true);
				return ERROR;
			}
		}

		shopCode = "/WEB-INF/jsp/sale-product/print-tax/" + shopCode.toLowerCase() + ".jsp";

		File jspFile = new File(getSession().getServletContext().getRealPath("/") + shopCode);

		if (!jspFile.exists()) {
			shopCode = "/WEB-INF/jsp/sale-product/print-tax/viewTax.jsp";
		}

		return SUCCESS;
	}

	public String openTaxView() {
		if (null == session.getAttribute("pdfBase64")) {
			return ConstantManager.ERROR_400_CODE;
		}
		pdfBase64 = (String) session.getAttribute("pdfBase64");
		outputPath = (String) session.getAttribute("outputPath");
		downloadPath = (String) session.getAttribute("downloadPath");
		session.removeAttribute("pdfBase64");
		session.removeAttribute("outputPath");
		session.removeAttribute("outputPath");
		return SUCCESS;
	}

	public String removePdfFile() {
		return SUCCESS;
	}

	public String updatePrintDate() {
		resetToken(result);
		if (checkbox == null || checkbox.size() == 0) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}

		try {
			List<Long> listInvoiceId = new ArrayList<Long>();
			boolean flag = true;
			List<String> str = new ArrayList<String>();
			for (int i = 0; i < checkbox.size(); i++) {
				flag = true;
				for (int j = 0; j < i; j++)
					if (checkbox.get(j).equals(checkbox.get(i)))
						flag = false;
				if (flag) {
					listInvoiceId.add(Long.valueOf(checkbox.get(i)));
				}

			}
			Date sysDate = commonMgr.getSysDate();
			List<Invoice> listInvoice = saleOrderMgr.getListInvoiceByLstInvoiceId(listInvoiceId);
			for (Invoice invoice : listInvoice) {
				if (invoice.getInvoiceDate() == null) {
					invoice.setInvoiceDate(sysDate);
				}
			}
			saleOrderMgr.updateListInvoice(listInvoice);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, "PrintTaxAction.updatePrintDate");
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	public String updatePrintDateNew() {
		resetToken(result);
		if (checkbox == null || checkbox.size() == 0) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}

		try {
			List<Long> listInvoiceId = new ArrayList<Long>();
			boolean flag = true;
			List<String> str = new ArrayList<String>();
			for (int i = 0; i < checkbox.size(); i++) {
				flag = true;
				for (int j = 0; j < i; j++)
					if (checkbox.get(j).equals(checkbox.get(i)))
						flag = false;
				if (flag) {
					listInvoiceId.add(Long.valueOf(checkbox.get(i)));
				}

			}
			Date sysDate = commonMgr.getSysDate();
			List<Invoice> listInvoice = saleOrderMgr.getListInvoiceByLstInvoiceId(listInvoiceId);
			for (Invoice invoice : listInvoice) {
				if (invoice.getInvoiceDate() == null) {
					invoice.setInvoiceDate(lockDay);
				}
			}
			saleOrderMgr.updateListInvoice(listInvoice);

			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, "PrintTaxAction.updatePrintDate");
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	public String downloadFileTemplate() {
		try {
			List<String> lstPath = new ArrayList<String>();
			String outputPath = "";

			Shop curShop = null;
			if (session.getAttribute(ConstantManager.SESSION_SHOP) != null) {
				curShop = (Shop) session.getAttribute(ConstantManager.SESSION_SHOP);
			}

			String path = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getVatTemplatePath();
			File[] files = new File(path).listFiles();

			if (files != null) {
				for (File file : files) {
					if (file.isFile()) {
						if (file.getName().indexOf(curShop.getShopCode().toLowerCase()) >= 0) {
							lstPath.add(path + file.getName());
						}
					}
				}

				if (lstPath.size() > 0) {
					String fileName2 = ConstantManager.EXPORT_VAT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE);
					List<File> lstFile = new ArrayList<File>();
					for (int i = 0; i < lstPath.size(); i++) {
						File f = new File(lstPath.get(i));
						lstFile.add(f);
					}
					fileName2 = fileName2 + FileExtension.ZIP.getValue();
					FileUtility.compress(lstFile, fileName2, Configuration.getStoreRealPath());
					outputPath = Configuration.getExportExcelPath() + fileName2;

					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					return JSON;
				} else {
					result.put(ERROR, true);
					return JSON;
				}
			} else {
				result.put(ERROR, true);
				return JSON;
			}

		} catch (Exception e) {
			LogUtility.logError(e, "PrintTaxAction.downloadFileTemplate");
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	private static byte[] loadFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		byte[] bytes = new byte[(int) length];
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}

	public String uploadFileTemplate() {
		FileOutputStream to = null;
		FileInputStream from = null;
		try {
			Shop curShop = null;
			if (session.getAttribute(ConstantManager.SESSION_SHOP) != null) {
				curShop = (Shop) session.getAttribute(ConstantManager.SESSION_SHOP);
			}
			if (zipFile == null || StringUtil.isNullOrEmpty(fileName)) {
				result.put(ERROR, true);
				result.put("errMsg", "File upload không tồn tại!");
				return JSON;
			}
			//String path = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getVatTemplatePath();
			//String pathFileZip = path + ConstantManager.IMPORT_VAT + curShop.getShopCode().toLowerCase()+FileExtension.ZIP.getValue();
			String path = Configuration.getReportTemplatePath();
			String nameDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_FILE_EXPORT);
			String pathFileZip = path + ConstantManager.IMPORT_VAT + curShop.getShopCode().toLowerCase() + "_" + nameDate + FileExtension.ZIP.getValue();
			//unzip file
			FileUtility.decompress(zipFile, path, ConstantManager.IMPORT_VAT + curShop.getShopCode().toLowerCase());
			//coppy file
			File fileZipUpload = new File(pathFileZip);
			if (!fileZipUpload.createNewFile()) {
				fileZipUpload.delete();
				fileZipUpload.createNewFile();
			}
			to = new FileOutputStream(fileZipUpload);
			from = new FileInputStream(zipFile);
			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = from.read(buffer)) != -1) {
				to.write(buffer, 0, bytesRead); // write
			}
			to.close();
			from.close();
			result.put(ERROR, false);
		} catch (Exception e) {
			try {
				if (to != null) {
					to.close();
				}
				if (from != null) {
					from.close();
				}
			} catch (IOException io) {
				;
			}
			LogUtility.logError(e, "PrintTaxAction.uploadFileTemplate");
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	private static String encodeFileToBase64Binary(String fileName) throws IOException {
		File file = new File(fileName);
		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);

		return encodedString;
	}
	
	/**
	 * In hoa don view Aplet
	 * 
	 * @return 
	 * @author hoanv25
	 * @since Auggust 20, 2015
	 */
	public String printTemplate() {
		try {
			Shop curShop = null;
			if (session.getAttribute(ConstantManager.SESSION_SHOP) != null) {
				curShop = (Shop) session.getAttribute(ConstantManager.SESSION_SHOP);
			}
			if (shopCodeSearch != null) {
				Shop sh = shopMgr.getShopByCode(shopCodeSearch);
				ReportUrl reportUrl = reportManagerMgr.getReportFileName(sh.getId(), "VAT");
				if (reportUrl == null || (reportUrl != null && StringUtil.isNullOrEmpty(reportUrl.getUrl()))) {//Chưa có mẫu hóa đơn GTGT
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				//String path = ServletActionContext.getServletContext().getRealPath("/") + reportUrl.getUrl();
				String path = Configuration.getStoreRealPath() + reportUrl.getUrl();
				File file = new File(path);
				if (!file.isFile()) {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				} else {
					result.put(ERROR, false);
					result.put("hasData", true);
					result.put(REPORT_PATH, path);
					return JSON;
				}
			} else {
				ReportUrl reportUrl = reportManagerMgr.getReportFileName(curShop.getId(), "VAT");
				if (reportUrl == null || (reportUrl != null && StringUtil.isNullOrEmpty(reportUrl.getUrl()))) {//Chưa có mẫu hóa đơn GTGT
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				//String path = ServletActionContext.getServletContext().getRealPath("/") + reportUrl.getUrl();
				String path = Configuration.getStoreRealPath() + reportUrl.getUrl();
				File file = new File(path);
				if (!file.isFile()) {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				} else {
					result.put(ERROR, false);
					result.put("hasData", true);
					result.put(REPORT_PATH, path);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "PrintTaxAction.printTemplate");
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		}
		return JSON;
	}

	/**
	 * Gets the tax value string.
	 * 
	 * @return the tax value string
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public String getTaxValueString() {
		return taxValueString;
	}

	/**
	 * Sets the tax value string.
	 * 
	 * @param taxValueString
	 *            the new tax value string
	 */
	public void setTaxValueString(String taxValueString) {
		this.taxValueString = taxValueString;
	}

	/**
	 * Gets the tax value.
	 * 
	 * @return the tax value
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public Float getTaxValue() {
		return taxValue;
	}

	/**
	 * Sets the tax value.
	 * 
	 * @param taxValue
	 *            the new tax value
	 */
	public void setTaxValue(Float taxValue) {
		this.taxValue = taxValue;
	}

	/**
	 * Gets the order number.
	 * 
	 * @return the order number
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * Sets the order number.
	 * 
	 * @param orderNumber
	 *            the new order number
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * Gets the from date.
	 * 
	 * @return the from date
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * Sets the from date.
	 * 
	 * @param fromDate
	 *            the new from date
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * Gets the to date.
	 * 
	 * @return the to date
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * Sets the to date.
	 * 
	 * @param toDate
	 *            the new to date
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * Gets the sale staff.
	 * 
	 * @return the sale staff
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public String getSaleStaff() {
		return saleStaff;
	}

	/**
	 * Sets the sale staff.
	 * 
	 * @param saleStaff
	 *            the new sale staff
	 */
	public void setSaleStaff(String saleStaff) {
		this.saleStaff = saleStaff;
	}

	/**
	 * Gets the short code.
	 * 
	 * @return the short code
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public String getShortCode() {
		return shortCode;
	}

	/**
	 * Sets the short code.
	 * 
	 * @param shortCode
	 *            the new short code
	 */
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getAddressCus() {
		return addressCus;
	}

	public void setAddressCus(String addressCus) {
		this.addressCus = addressCus;
	}

	/**
	 * Gets the serialversionuid.
	 * 
	 * @return the serialversionuid
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Gets the list sale order.
	 * 
	 * @return the list sale order
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public List<SaleOrder> getListSaleOrder() {
		return listSaleOrder;
	}

	/**
	 * Gets the shop.
	 * 
	 * @return the shop
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 * 
	 * @param shop
	 *            the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * Sets the list sale order.
	 * 
	 * @param listSaleOrder
	 *            the new list sale order
	 */
	public void setListSaleOrder(List<SaleOrder> listSaleOrder) {
		this.listSaleOrder = listSaleOrder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#getStaff()
	 */
	public Staff getStaff() {
		return staff;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.web.action.general.AbstractAction#setStaff(ths.dms.core.entities
	 * .Staff)
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public List<Staff> getListNVBH() {
		return listNVBH;
	}

	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}

	/**
	 * Gets the checkbox.
	 * 
	 * @return the checkbox
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public List<String> getCheckbox() {
		return checkbox;
	}

	/**
	 * Sets the checkbox.
	 * 
	 * @param checkbox
	 *            the new checkbox
	 */
	public void setCheckbox(List<String> checkbox) {
		this.checkbox = checkbox;
	}

	/**
	 * Gets the cust payment.
	 * 
	 * @return the cust payment
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	public int getCustPayment() {
		return custPayment;
	}

	/**
	 * Sets the cust payment.
	 * 
	 * @param custPayment
	 *            the new cust payment
	 */
	public void setCustPayment(int custPayment) {
		this.custPayment = custPayment;
	}

	public List<Invoice> getListInvoice() {
		return listInvoice;
	}

	public void setListInvoice(List<Invoice> listInvoice) {
		this.listInvoice = listInvoice;
	}

	public List<SaleOrderVOEx> getListSaleOrderVOEx() {
		return listSaleOrderVOEx;
	}

	public void setListSaleOrderVOEx(List<SaleOrderVOEx> listSaleOrderVOEx) {
		this.listSaleOrderVOEx = listSaleOrderVOEx;
	}

	public String getOutputName() {
		return outputName;
	}

	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	public String getStockStaff() {
		return stockStaff;
	}

	public void setStockStaff(String stockStaff) {
		this.stockStaff = stockStaff;
	}

	private String numberToTextA(int number) {
		String sR = "";
		switch (number) {
		case 0:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "zero");
			break;
		case 1:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "one");
			break;
		case 2:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "two");
			break;
		case 3:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "three");
			break;
		case 4:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "four");
			break;
		case 5:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "five");
			break;
		case 6:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "six");
			break;
		case 7:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "seven");
			break;
		case 8:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "eight");
			break;
		case 9:
			sR = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "nine");
			break;
		default:
			sR = "";
		}
		return sR;
	}

	public String ChuyenDV(String Number) {
		String sNumber = "";
		int len = Number.length();
		if (len == 1) {
			int iNu = Integer.parseInt("" + Number.charAt(0));
			sNumber += numberToTextA(iNu);
		} else if (len == 2) {
			int iChuc = Integer.parseInt("" + Number.charAt(0));
			int iDV = Integer.parseInt("" + Number.charAt(1));
			if (iChuc == 1) {
				if (iDV > 0) {
					sNumber += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ten") + " " + numberToTextA(iDV);
				} else {
					sNumber += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ten") + " ";
				}
			} else {
				sNumber += numberToTextA(iChuc) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ten.low") + " " + numberToTextA(iDV);
			}
		} else {
			int iTram = Integer.parseInt("" + Number.charAt(0));
			int iChuc = Integer.parseInt("" + Number.charAt(1));
			int iDV = Integer.parseInt("" + Number.charAt(2));

			if (iChuc == 0) {
				if (iDV > 0) {
					sNumber += numberToTextA(iTram) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "hundred.single") + " " + numberToTextA(iDV);
				} else {
					sNumber += numberToTextA(iTram) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "hundred");
				}
			} else if (iChuc == 1) {
				if (iDV > 0) {
					sNumber += numberToTextA(iTram) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "hundred.ten") + " " + numberToTextA(iDV);
				} else {
					sNumber += numberToTextA(iTram) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "hundred.ten") + " ";
				}
			} else {
				if (iDV > 0) {
					sNumber += numberToTextA(iTram) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "hundred") + " " + numberToTextA(iChuc) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ten.low") + " "
							+ numberToTextA(iDV);
				} else {
					sNumber += numberToTextA(iTram) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "hundred") + " " + numberToTextA(iChuc) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ten.low") + " ";
				}
			}
		}
		return sNumber;
	}
	/**
	 * Update khi in view apllet
	 * 
	 * @author hoanv25
	 * @return
	 * @since Auggust 20,2015
	 */
	public String viewTaxNew() {
		if (checkbox == null || checkbox.size() == 0) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} else {
			try {
				String reportToken = retrieveReportToken(reportCode);
				if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
					return JSON;
				}
				if (shopCodeSearch != null) {
					Shop sh = shopMgr.getShopByCode(shopCodeSearch);
					//List<Invoice> listInvoice = new ArrayList<Invoice>();
					listObjectBeans = new ArrayList<Map<String, Object>>();
					List<Long> listInvoiceId = new ArrayList<Long>();
					boolean flag = true;
					//List<String> str = new ArrayList<String>();
					for (int i = 0; i < checkbox.size(); i++) {
						flag = true;
						for (int j = 0; j < i; j++)
							if (checkbox.get(j).equals(checkbox.get(i)))
								flag = false;
						if (flag) {
							listInvoiceId.add(Long.valueOf(checkbox.get(i)));
						}
					}

					listSaleOrderVOEx = saleOrderMgr.getListSaleOrderVOEx(listInvoiceId);
					calculate();
					Date lockDate = shopLockMgr.getNextLockedDay(listSaleOrderVOEx.get(0).getInvoice().getShop().getId());
					if (lockDate == null) {
						lockDate = commonMgr.getSysDate();
					}
					List<SaleOrderDetail> listDetail = null;
					List<SaleOrderDetail> listDetailFree = null;
					SaleOrderDetail detail = null;

					BigDecimal disAmt = null;
					BigDecimal amountVAT = null;
					BigDecimal amount = null;
					BigDecimal money = null;
					SaleOrderVOEx vo = null;
					List<SaleOrderDetail> lstSod = null;
					BigDecimal ONE_DOT_ONE = BigDecimal.valueOf(1.1);
					BigDecimal HUNDRED = BigDecimal.valueOf(100l);

					List<String> lstTmp = new ArrayList<String>();
					lstTmp.add("discount");
					for (int i = 0, sz = listSaleOrderVOEx.size(); i < sz; i++) {
						vo = listSaleOrderVOEx.get(i);
						String currentDay = "";

						Date temp = vo.getInvoice().getOrderDate();
						String daycustom = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.format.day.custom");
						currentDay = stringFormat(daycustom, String.valueOf(DateUtil.getDay(temp)), String.valueOf(DateUtil.getMonth(temp)), String.valueOf(DateUtil.getYear(temp)));
						Invoice invoiceTemp = vo.getInvoice();

						Map<String, Object> beans = new HashMap<String, Object>();
						if (!InvoiceStatus.CANCELED.equals(invoiceTemp.getStatus())) {
							beans.put("hasData", 1);
							if (invoiceTemp.getInvoiceDate() != null) {
								Integer __date = DateUtil.getDay(invoiceTemp.getInvoiceDate());
								Integer __month = DateUtil.getMonth(invoiceTemp.getInvoiceDate());
								Integer __year = DateUtil.getYear(invoiceTemp.getInvoiceDate());
								String _date = "";
								String _month = "";
								String _year = "";
								if (__date < 10) {
									_date = "0" + __date;
								} else {
									_date = __date.toString();
								}
								if (__month < 10) {
									_month = "0" + __month;
								} else {
									_month = __month.toString();
								}
								_year = __year.toString();
								beans.put("ngay", _date);
								beans.put("thang", _month);
								beans.put("nam", _year);
							} else {
								beans.put("ngay", this.day);
								beans.put("thang", this.month);
								beans.put("nam", this.year);
							}
							beans.put("saleOrder", vo.getSaleOrder());
							beans.put("diachiKH", vo.getDiachiKH());
							beans.put("diachiHD", vo.getDiachiHD());
							//beans.put("listSODSale", vo.getLstSaleOrderDetail());                                                                               

							listDetail = new ArrayList<SaleOrderDetail>();
							listDetailFree = new ArrayList<SaleOrderDetail>();
							lstSod = vo.getLstSaleOrderDetail();
							amount = BigDecimal.ZERO;
							List<VATVO> __listDetail = new ArrayList<VATVO>();
							List<VATVO> __listDetailFree = new ArrayList<VATVO>();
							for (int j = 0, szj = lstSod.size(); j < szj; j++) {
								detail = lstSod.get(j);
								detail.setRowNum(j + 1);
								VATVO vatVO = new VATVO();
								vatVO.setRowNUm(j + 1);
								vatVO.setProduct(detail.getProduct() == null ? "" : detail.getProduct().getProductCode() + " - " + detail.getProduct().getProductName());
								vatVO.setUom(detail.getProduct() == null ? "" : detail.getProduct().getUom1());
								vatVO.setQuantity(detail.getQuantity());
								vatVO.setPrice(detail.getPriceNotVat());
								vatVO.setDiscount(detail.getDiscountAmount());
								vatVO.setDescription(detail.getCreateUser());
								if (detail.getProduct() != null && detail.getIsFreeItem() == 0) {
									listDetail.add(detail);
									amount = amount.add(detail.getDiscountAmount()); // ham calculate dang dung truong nay de luu tien chua chiet khau, khong phai chiet khau !!!?
									__listDetail.add(vatVO);
								} else if (detail.getProduct() != null && detail.getIsFreeItem() == 1) {
									listDetailFree.add(detail);
									__listDetailFree.add(vatVO);
								}
							}
							List<VATVO> totalList = new ArrayList<VATVO>();
							beans.put("listDetail", listDetail);
							totalList.addAll(__listDetail);
							beans.put("listDetailFree", listDetailFree);
							if (!__listDetailFree.isEmpty()) {
								VATVO strFreeProduct = new VATVO();
								strFreeProduct.setProduct(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.print.vat.string.free.product"));
								totalList.add(strFreeProduct);
								totalList.addAll(__listDetailFree);
							}
							disAmt = vo.getInvoice().getDiscount();
							if (disAmt != null && disAmt.compareTo(BigDecimal.ZERO) > 0) {
								disAmt = disAmt.divide(ONE_DOT_ONE, 0, RoundingMode.HALF_UP);
								amount = amount.subtract(disAmt);
								beans.put("lstDisAmt", lstTmp);
								beans.put("discountAmount", displayNumberVAT(disAmt));
								VATVO vatDiscount = new VATVO();
								vatDiscount.setDiscount(disAmt);
								vatDiscount.setProduct(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.print.vat.discount.amount.money"));
								totalList.add(vatDiscount);
							}
							beans.put("totalList", totalList);

							beans.put("invoice", vo.getInvoice());

							amountVAT = amount.multiply(BigDecimal.valueOf(((Invoice) beans.get("invoice")).getVat()));
							amountVAT = amountVAT.divide(HUNDRED, 0, RoundingMode.HALF_UP);
							//beans.put("amountVAT", amountVAT);
							beans.put("amountVATStr", displayNumberVAT(amountVAT));

							money = amount.add(amountVAT);
							beans.put("moneyString", moneyString(money));
							//beans.put("amount", calculateAmount(vo));
							beans.put("amountStr", displayNumberVAT(amount));

							//beans.put("totalAmount", money);
							beans.put("totalAmountStr", displayNumberVAT(money));
							beans.put("currentDay", currentDay);
							beans.put("stockStaff", stockStaff);

							Calendar calendar = new GregorianCalendar();
							calendar.setTime(temp);
							Date printDay = calendar.getTime();
							beans.put("printDay", DateUtil.toDateString(printDay, DateUtil.DATE_FORMAT_DDMMYYYY));
							if (InvoiceCustPayment.MONEY.equals(invoiceTemp.getCustPayment())) {
								beans.put("custPayment", "Tiền mặt");
							} else if (InvoiceCustPayment.BANK_TRANSFER.equals(invoiceTemp.getCustPayment())) {
								beans.put("custPayment", "Chuyển khoản");
							} else {
								beans.put("custPayment", "");
							}
							listObjectBeans.add(beans);

							//invoiceTemp.setInvoiceDate(lockDate);
							//listInvoice.add(invoiceTemp);
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return ERROR;
						}
					}

					if (!StringUtil.isNullOrEmpty(buyer)) {
						ShopParam buyerParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_BUYER");
						if (buyerParam != null) {
							buyerParam.setDescription(buyer);
							buyerParam.setUpdateUser(getCurrentUser().getUserName());
							commonMgr.updateShopParam(buyerParam);
						} else {
							buyerParam = new ShopParam();
							buyerParam.setShop(sh);
							buyerParam.setType("VAT_BUYER");
							buyerParam.setDescription(buyer);
							buyerParam.setCreateUser(getCurrentUser().getUserName());
							commonMgr.createShopParam(buyerParam);
						}
					} else {
						ShopParam buyerParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_BUYER");
						if (buyerParam != null) {
							buyer = buyerParam.getDescription();
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return ERROR;
						}
					}

					if (!StringUtil.isNullOrEmpty(stockStaff)) {
						ShopParam stockStaffParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_STOCK_STAFF");
						if (stockStaffParam != null) {
							stockStaffParam.setDescription(stockStaff);
							stockStaffParam.setUpdateUser(getCurrentUser().getUserName());
							commonMgr.updateShopParam(stockStaffParam);
						} else {
							stockStaffParam = new ShopParam();
							stockStaffParam.setShop(sh);
							stockStaffParam.setType("VAT_STOCK_STAFF");
							stockStaffParam.setDescription(stockStaff);
							stockStaffParam.setCreateUser(getCurrentUser().getUserName());
							commonMgr.createShopParam(stockStaffParam);
						}
					} else {
						ShopParam stockStaffParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_STOCK_STAFF");
						if (stockStaffParam != null) {
							stockStaff = stockStaffParam.getDescription();
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return ERROR;
						}
					}

					if (!StringUtil.isNullOrEmpty(selStaff)) {
						ShopParam selStaffParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_SALE_STAFF");
						if (selStaffParam != null) {
							selStaffParam.setDescription(selStaff);
							selStaffParam.setUpdateUser(getCurrentUser().getUserName());
							commonMgr.updateShopParam(selStaffParam);
						} else {
							selStaffParam = new ShopParam();
							selStaffParam.setShop(sh);
							selStaffParam.setType("VAT_SALE_STAFF");
							selStaffParam.setDescription(selStaff);
							selStaffParam.setCreateUser(getCurrentUser().getUserName());
							commonMgr.createShopParam(selStaffParam);
						}
					} else {
						ShopParam selStaffParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_SALE_STAFF");
						if (selStaffParam != null) {
							selStaff = selStaffParam.getDescription();
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return ERROR;
						}
					}

					ReportUrl reportUrl = reportManagerMgr.getReportFileName(sh.getId(), "VAT");
					if (reportUrl == null || (reportUrl != null && StringUtil.isNullOrEmpty(reportUrl.getUrl()))) {//Chưa có mẫu hóa đơn GTGT
						result.put(ERROR, true);
						//result.put("hasData", false);
						result.put("errMsg", "Lỗi không có file template VAT trong Dữ liệu, cập nhật mẫu bên chức năng Danh mục -> Quản lý mẫu báo cáo !");
						return JSON;
					}

					//String templatePath = ServletActionContext.getServletContext().getRealPath("/") + reportUrl.getUrl();
					String nameFile = reportUrl.getUrl();
					int idx = nameFile.lastIndexOf(".jrxml");
					nameFile = nameFile.substring(0, idx);
					nameFile = nameFile + ".jasper";
					String templatePath = Configuration.getReportTemplatePath() + nameFile;
					File fileReport = new File(templatePath);
					if (!fileReport.exists()) {
						result.put(ERROR, true);
						//result.put("hasData", false);
						result.put("errMsg", "Lỗi không tồn tại file template VAT, cập nhật mẫu bên chức năng Danh mục -> Quản lý mẫu báo cáo !");
						return JSON;
					}

					/*
					 * String templatePath =
					 * StringUtil.replaceSeparatorChar((new StringBuilder
					 * (ServletActionContext
					 * .getServletContext().getRealPath("/")) .append
					 * ("/resources/templates/sale-product/vat/template-vat."+
					 * shopCode.toLowerCase()+".jasper") ).toString());
					 */

					Map<String, Object> vatParameters = new HashMap<String, Object>();
					vatParameters.put("buyer", buyer);
					vatParameters.put("stockStaff", stockStaff);
					vatParameters.put("selStaff", selStaff);

					JRDataSource dataSource = new JRBeanCollectionDataSource(listObjectBeans);

					String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append("vat." + shopCode.toLowerCase() + ".pdf")).toString());

					//String outputPath = Configuration.getStoreRealPath() + outputFile;
					String outputPath = Configuration.getStoreRealPath() + outputFile;
					String downloadPath = Configuration.getExportExcelPath() + outputFile;

					JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, vatParameters, dataSource);

					JRAbstractExporter exporter = new JRPdfExporter();

					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

					exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
					exporter.exportReport();

					session.setAttribute("pdfBase64", encodeFileToBase64Binary(outputPath));
					session.setAttribute("outputPath", outputPath);
					session.setAttribute("downloadPath", downloadPath);

					result.put(ERROR, false);
					result.put("pdfBase64", encodeFileToBase64Binary(outputPath));
					result.put("outputPath", outputPath);
					result.put("downloadPath", downloadPath);
					MemcachedUtils.putValueToMemcached(reportToken, downloadPath, retrieveReportMemcachedTimeout());

					/** Set invoice_date la ngay chot */
					//saleOrderMgr.updateListInvoice(listInvoice);

					//Date sysDate = commonMgr.getSysDate();
					Date invoiceDate = DateUtil.toDate(day + "/" + month + "/" + year, DateUtil.DATE_FORMAT_DDMMYYYY);
					List<Invoice> listInvoice = saleOrderMgr.getListInvoiceByLstInvoiceId(listInvoiceId);
					for (Invoice invoice : listInvoice) {
						if (invoice.getInvoiceDate() == null) {
							invoice.setInvoiceDate(invoiceDate);
						}
					}
					saleOrderMgr.updateListInvoice(listInvoice);
					return JSON;
				} else {
					//List<Invoice> listInvoice = new ArrayList<Invoice>();
					listObjectBeans = new ArrayList<Map<String, Object>>();
					List<Long> listInvoiceId = new ArrayList<Long>();
					boolean flag = true;
					//List<String> str = new ArrayList<String>();
					for (int i = 0; i < checkbox.size(); i++) {
						flag = true;
						for (int j = 0; j < i; j++)
							if (checkbox.get(j).equals(checkbox.get(i)))
								flag = false;
						if (flag) {
							listInvoiceId.add(Long.valueOf(checkbox.get(i)));
						}
					}

					listSaleOrderVOEx = saleOrderMgr.getListSaleOrderVOEx(listInvoiceId);
					calculate();
					Date lockDate = shopLockMgr.getNextLockedDay(listSaleOrderVOEx.get(0).getInvoice().getShop().getId());
					if (lockDate == null) {
						lockDate = commonMgr.getSysDate();
					}
					List<SaleOrderDetail> listDetail = null;
					List<SaleOrderDetail> listDetailFree = null;
					SaleOrderDetail detail = null;

					BigDecimal disAmt = null;
					BigDecimal amountVAT = null;
					BigDecimal amount = null;
					BigDecimal money = null;
					SaleOrderVOEx vo = null;
					List<SaleOrderDetail> lstSod = null;
					BigDecimal ONE_DOT_ONE = BigDecimal.valueOf(1.1);
					BigDecimal HUNDRED = BigDecimal.valueOf(100l);

					List<String> lstTmp = new ArrayList<String>();
					lstTmp.add("discount");
					for (int i = 0, sz = listSaleOrderVOEx.size(); i < sz; i++) {
						vo = listSaleOrderVOEx.get(i);
						String currentDay = "";

						Date temp = vo.getInvoice().getOrderDate();
						String daycustom = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.format.day.custom");
						currentDay = stringFormat(daycustom, String.valueOf(DateUtil.getDay(temp)), String.valueOf(DateUtil.getMonth(temp)), String.valueOf(DateUtil.getYear(temp)));
						Invoice invoiceTemp = vo.getInvoice();

						Map<String, Object> beans = new HashMap<String, Object>();
						if (!InvoiceStatus.CANCELED.equals(invoiceTemp.getStatus())) {
							beans.put("hasData", 1);
							if (invoiceTemp.getInvoiceDate() != null) {
								Integer __date = DateUtil.getDay(invoiceTemp.getInvoiceDate());
								Integer __month = DateUtil.getMonth(invoiceTemp.getInvoiceDate());
								Integer __year = DateUtil.getYear(invoiceTemp.getInvoiceDate());
								String _date = "";
								String _month = "";
								String _year = "";
								if (__date < 10) {
									_date = "0" + __date;
								} else {
									_date = __date.toString();
								}
								if (__month < 10) {
									_month = "0" + __month;
								} else {
									_month = __month.toString();
								}
								_year = __year.toString();
								beans.put("ngay", _date);
								beans.put("thang", _month);
								beans.put("nam", _year);
							} else {
								beans.put("ngay", this.day);
								beans.put("thang", this.month);
								beans.put("nam", this.year);
							}
							beans.put("saleOrder", vo.getSaleOrder());
							beans.put("diachiKH", vo.getDiachiKH());
							beans.put("diachiHD", vo.getDiachiHD());
							//beans.put("listSODSale", vo.getLstSaleOrderDetail());                                                                               

							listDetail = new ArrayList<SaleOrderDetail>();
							listDetailFree = new ArrayList<SaleOrderDetail>();
							lstSod = vo.getLstSaleOrderDetail();
							amount = BigDecimal.ZERO;
							List<VATVO> __listDetail = new ArrayList<VATVO>();
							List<VATVO> __listDetailFree = new ArrayList<VATVO>();
							for (int j = 0, szj = lstSod.size(); j < szj; j++) {
								detail = lstSod.get(j);
								detail.setRowNum(j + 1);
								VATVO vatVO = new VATVO();
								vatVO.setRowNUm(j + 1);
								vatVO.setProduct(detail.getProduct() == null ? "" : detail.getProduct().getProductCode() + " - " + detail.getProduct().getProductName());
								vatVO.setUom(detail.getProduct() == null ? "" : detail.getProduct().getUom1());
								vatVO.setQuantity(detail.getQuantity());
								vatVO.setPrice(detail.getPriceNotVat());
								vatVO.setDiscount(detail.getDiscountAmount());
								vatVO.setDescription(detail.getCreateUser());
								if (detail.getProduct() != null && detail.getIsFreeItem() == 0) {
									listDetail.add(detail);
									amount = amount.add(detail.getDiscountAmount()); // ham calculate dang dung truong nay de luu tien chua chiet khau, khong phai chiet khau !!!?
									__listDetail.add(vatVO);
								} else if (detail.getProduct() != null && detail.getIsFreeItem() == 1) {
									listDetailFree.add(detail);
									__listDetailFree.add(vatVO);
								}
							}
							List<VATVO> totalList = new ArrayList<VATVO>();
							beans.put("listDetail", listDetail);
							totalList.addAll(__listDetail);
							beans.put("listDetailFree", listDetailFree);
							if (!__listDetailFree.isEmpty()) {
								VATVO strFreeProduct = new VATVO();
								strFreeProduct.setProduct(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.print.vat.string.free.product"));
								totalList.add(strFreeProduct);
								totalList.addAll(__listDetailFree);
							}
							disAmt = vo.getInvoice().getDiscount();
							if (disAmt != null && disAmt.compareTo(BigDecimal.ZERO) > 0) {
								disAmt = disAmt.divide(ONE_DOT_ONE, 0, RoundingMode.HALF_UP);
								amount = amount.subtract(disAmt);
								beans.put("lstDisAmt", lstTmp);
								beans.put("discountAmount", displayNumberVAT(disAmt));
								VATVO vatDiscount = new VATVO();
								vatDiscount.setDiscount(disAmt);
								vatDiscount.setProduct(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.print.vat.discount.amount.money"));
								totalList.add(vatDiscount);
							}
							beans.put("totalList", totalList);

							beans.put("invoice", vo.getInvoice());

							amountVAT = amount.multiply(BigDecimal.valueOf(((Invoice) beans.get("invoice")).getVat()));
							amountVAT = amountVAT.divide(HUNDRED, 0, RoundingMode.HALF_UP);
							//beans.put("amountVAT", amountVAT);
							beans.put("amountVATStr", displayNumberVAT(amountVAT));

							money = amount.add(amountVAT);
							beans.put("moneyString", moneyString(money));
							//beans.put("amount", calculateAmount(vo));
							beans.put("amountStr", displayNumberVAT(amount));

							//beans.put("totalAmount", money);
							beans.put("totalAmountStr", displayNumberVAT(money));
							beans.put("currentDay", currentDay);
							beans.put("stockStaff", stockStaff);

							Calendar calendar = new GregorianCalendar();
							calendar.setTime(temp);
							Date printDay = calendar.getTime();
							beans.put("printDay", DateUtil.toDateString(printDay, DateUtil.DATE_FORMAT_DDMMYYYY));
							if (InvoiceCustPayment.MONEY.equals(invoiceTemp.getCustPayment())) {
								beans.put("custPayment", "Tiền mặt");
							} else if (InvoiceCustPayment.BANK_TRANSFER.equals(invoiceTemp.getCustPayment())) {
								beans.put("custPayment", "Chuyển khoản");
							} else {
								beans.put("custPayment", "");
							}
							listObjectBeans.add(beans);

							//invoiceTemp.setInvoiceDate(lockDate);
							//listInvoice.add(invoiceTemp);
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return ERROR;
						}
					}

					if (!StringUtil.isNullOrEmpty(buyer)) {
						ShopParam buyerParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_BUYER");
						if (buyerParam != null) {
							buyerParam.setDescription(buyer);
							buyerParam.setUpdateUser(getCurrentUser().getUserName());
							commonMgr.updateShopParam(buyerParam);
						} else {
							buyerParam = new ShopParam();
							buyerParam.setShop(shop);
							buyerParam.setType("VAT_BUYER");
							buyerParam.setDescription(buyer);
							buyerParam.setCreateUser(getCurrentUser().getUserName());
							commonMgr.createShopParam(buyerParam);
						}
					} else {
						ShopParam buyerParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_BUYER");
						if (buyerParam != null) {
							buyer = buyerParam.getDescription();
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return ERROR;
						}
					}

					if (!StringUtil.isNullOrEmpty(stockStaff)) {
						ShopParam stockStaffParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_STOCK_STAFF");
						if (stockStaffParam != null) {
							stockStaffParam.setDescription(stockStaff);
							stockStaffParam.setUpdateUser(getCurrentUser().getUserName());
							commonMgr.updateShopParam(stockStaffParam);
						} else {
							stockStaffParam = new ShopParam();
							stockStaffParam.setShop(shop);
							stockStaffParam.setType("VAT_STOCK_STAFF");
							stockStaffParam.setDescription(stockStaff);
							stockStaffParam.setCreateUser(getCurrentUser().getUserName());
							commonMgr.createShopParam(stockStaffParam);
						}
					} else {
						ShopParam stockStaffParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_STOCK_STAFF");
						if (stockStaffParam != null) {
							stockStaff = stockStaffParam.getDescription();
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return ERROR;
						}
					}

					if (!StringUtil.isNullOrEmpty(selStaff)) {
						ShopParam selStaffParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_SALE_STAFF");
						if (selStaffParam != null) {
							selStaffParam.setDescription(selStaff);
							selStaffParam.setUpdateUser(getCurrentUser().getUserName());
							commonMgr.updateShopParam(selStaffParam);
						} else {
							selStaffParam = new ShopParam();
							selStaffParam.setShop(shop);
							selStaffParam.setType("VAT_SALE_STAFF");
							selStaffParam.setDescription(selStaff);
							selStaffParam.setCreateUser(getCurrentUser().getUserName());
							commonMgr.createShopParam(selStaffParam);
						}
					} else {
						ShopParam selStaffParam = commonMgr.getShopParamByType(staff.getShop().getId(), "VAT_SALE_STAFF");
						if (selStaffParam != null) {
							selStaff = selStaffParam.getDescription();
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							return ERROR;
						}
					}

					ReportUrl reportUrl = reportManagerMgr.getReportFileName(shop.getId(), "VAT");
					if (reportUrl == null || (reportUrl != null && StringUtil.isNullOrEmpty(reportUrl.getUrl()))) {//Chưa có mẫu hóa đơn GTGT
						result.put(ERROR, true);
						//result.put("hasData", false);
						result.put("errMsg", "Lỗi không có file template VAT trong Dữ liệu, cập nhật mẫu bên chức năng Danh mục -> Quản lý mẫu báo cáo !");
						return JSON;
					}

					//String templatePath = ServletActionContext.getServletContext().getRealPath("/") + reportUrl.getUrl();
					String nameFile = reportUrl.getUrl();
					int idx = nameFile.lastIndexOf(".jrxml");
					nameFile = nameFile.substring(0, idx);
					nameFile = nameFile + ".jasper";
					String templatePath = Configuration.getReportTemplatePath() + nameFile;
					File fileReport = new File(templatePath);
					if (!fileReport.exists()) {
						result.put(ERROR, true);
						//result.put("hasData", false);
						result.put("errMsg", "Lỗi không tồn tại file template VAT, cập nhật mẫu bên chức năng Danh mục -> Quản lý mẫu báo cáo !");
						return JSON;
					}

					/*
					 * String templatePath =
					 * StringUtil.replaceSeparatorChar((new StringBuilder
					 * (ServletActionContext
					 * .getServletContext().getRealPath("/")) .append
					 * ("/resources/templates/sale-product/vat/template-vat."+
					 * shopCode.toLowerCase()+".jasper") ).toString());
					 */

					Map<String, Object> vatParameters = new HashMap<String, Object>();
					vatParameters.put("buyer", buyer);
					vatParameters.put("stockStaff", stockStaff);
					vatParameters.put("selStaff", selStaff);

					JRDataSource dataSource = new JRBeanCollectionDataSource(listObjectBeans);

					String outputFile = StringUtil.replaceSeparatorChar((new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append("vat." + shopCode.toLowerCase() + ".pdf")).toString());

					//String outputPath = Configuration.getStoreRealPath() + outputFile;
					String outputPath = Configuration.getStoreRealPath() + outputFile;
					String downloadPath = Configuration.getExportExcelPath() + outputFile;

					JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, vatParameters, dataSource);

					JRAbstractExporter exporter = new JRPdfExporter();

					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

					exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
					exporter.exportReport();

					session.setAttribute("pdfBase64", encodeFileToBase64Binary(outputPath));
					session.setAttribute("outputPath", outputPath);
					session.setAttribute("downloadPath", downloadPath);

					result.put(ERROR, false);
					result.put("pdfBase64", encodeFileToBase64Binary(outputPath));
					result.put("outputPath", outputPath);
					result.put("downloadPath", downloadPath);
					MemcachedUtils.putValueToMemcached(reportToken, downloadPath, retrieveReportMemcachedTimeout());

					/** Set invoice_date la ngay chot */
					//saleOrderMgr.updateListInvoice(listInvoice);

					//Date sysDate = commonMgr.getSysDate();
					Date invoiceDate = DateUtil.toDate(day + "/" + month + "/" + year, DateUtil.DATE_FORMAT_DDMMYYYY);
					List<Invoice> listInvoice = saleOrderMgr.getListInvoiceByLstInvoiceId(listInvoiceId);
					for (Invoice invoice : listInvoice) {
						if (invoice.getInvoiceDate() == null) {
							invoice.setInvoiceDate(invoiceDate);
						}
					}
					saleOrderMgr.updateListInvoice(listInvoice);
					return JSON;
				}
			} catch (Exception e) {
				LogUtility.logError(e, "PrintTaxAction.printTax");
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				result.put(ERROR, true);
				return ERROR;
			}
		}

		shopCode = "/WEB-INF/jsp/sale-product/print-tax/" + shopCode.toLowerCase() + ".jsp";

		File jspFile = new File(getSession().getServletContext().getRealPath("/") + shopCode);

		if (!jspFile.exists()) {
			shopCode = "/WEB-INF/jsp/sale-product/print-tax/viewTax.jsp";
		}

		return SUCCESS;
	}

	public String tranlate(String sNumber) {
		Boolean negative = false;
		if (sNumber.substring(0, 1).equals("-")) {
			negative = true;
			sNumber = sNumber.substring(1);
		}
		String sR = "";
		String sR1 = "";
		String sR2 = "";
		String sR3 = "";
		String sR4 = "";
		String sR5 = "";
		// sR = ChuyenDV(sNumber);

		int seq = 0;
		int k = 1;
		for (int i = sNumber.length(); i >= 0; i--) {
			if (seq == 3) {
				String subStr = sNumber.substring(i, i + seq);
				if (k == 1) {
					sR = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "vnd");
				} else if (k == 2) {
					sR1 = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "thousand") + " ";
				} else if (k == 3) {
					sR2 = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "million") + " ";
				} else if (k == 4) {
					sR3 = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "billion") + " ";
				} else {
					sR4 = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "thousand") + " ";
				}
				seq = 0;
				k++;
			}
			seq++;
		}
		if (seq > 1) {
			String subStr = sNumber.substring(0, seq - 1);
			if (k == 1) {
				sR = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "vnd");
			} else if (k == 2) {
				sR1 = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "thousand") + " ";
			} else if (k == 3) {
				sR2 = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "million") + " ";
			} else if (k == 4) {
				sR3 = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "billion") + " ";
			} else {
				sR4 = ChuyenDV(subStr) + " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "thousand") + " ";
			}
		}
		//seq
		sR5 = sR4 + sR3 + sR2 + sR1 + sR;
		if (negative)
			sR5 = "Âm " + sR5;
		if (sR5.length() > 0) {
			char[] stringArray = sR5.toCharArray();
			stringArray[0] = Character.toUpperCase(stringArray[0]);
			sR5 = new String(stringArray);
		}
		return sR5;
	}

	public List<Map<String, Object>> getListObjectBeans() {
		return listObjectBeans;
	}

	public void setListObjectBeans(List<Map<String, Object>> listObjectBeans) {
		this.listObjectBeans = listObjectBeans;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public Integer getNumberValueOrder() {
		return numberValueOrder;
	}

	public void setNumberValueOrder(Integer numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}

	public String getNumberValue() {
		return numberValue;
	}

	public void setNumberValue(String numberValue) {
		this.numberValue = numberValue;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<String> getLstShopParamCode() {
		return lstShopParamCode;
	}

	public void setLstShopParamCode(List<String> lstShopParamCode) {
		this.lstShopParamCode = lstShopParamCode;
	}

	public String getShopCodeSearch() {
		return shopCodeSearch;
	}

	public void setShopCodeSearch(String shopCodeSearch) {
		this.shopCodeSearch = shopCodeSearch;
	}

}

package ths.dms.web.action.saleproduct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SaleOrderVOEx;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.WarehouseVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class AdjustmentTaxAction.
 */
public class AdjustmentTaxAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Danh sach trang thai. */
	private List<StatusBean> listStatus;
	
	/** Danh sach trang thai. */
	private List<StatusBean> listStatus1;

	public List<StatusBean> getListStatus1() {
		return listStatus1;
	}

	public void setListStatus1(List<StatusBean> listStatus1) {
		this.listStatus1 = listStatus1;
	}

	/** The list payment. */
	private List<StatusBean> listPayment;

	/** The choose status. */
	private Integer chooseStatus;

	/** The sale staff code. */
	private String saleStaffCode;

	/** The delivery staff code. */
	private String deliveryStaffCode;

	/** The tax value. */
	private Float taxValue;

	private String numberValue; 
	private Integer numberValueOrder;
	private Integer numberValueOrder1;
	
	/** The red invoice number. */
	private String redInvoiceNumber;
	
	/** The invoice number. */
	private String invoiceNumber;

	/** The create from date. */
	private String createFromDate;

	/** The create to date. */
	private String createToDate;

	/** The customer code. */
	private String customerCode;
	
	private String shopCodeSearch;
	
	private Boolean isVATGop;

	/** The list sale order vo ex. */
	private List<SaleOrderVOEx> listSaleOrderVOEx;

	/** The list invoice. */
	private List<Invoice> listInvoice;

	/** The list sale order id. */
	private List<String> listSaleOrderId;

	/** The list sale order numer*/
	private List<String> listOrderNumber;
	
	/** The list invoice number. */
	private List<String> listInvoiceNumber;

	/** The list invoice id. */
	private List<String> listInvoiceId;
	
	/** The list invoice id delete. */
	private List<String> listDeleteInvoiceId;

	/** The list payment method. */
	private List<Integer> listPaymentMethod;
	
	private List<Long> listSaleOrderIdx;

	/** The shop. */
	private Shop shop;
	
	/** id don chinh*/
	private Long saleOrderIdPri;
	
	/** The choose payment method. */
	private Integer choosePaymentMethod;

	/** The sale order mgr. */
	private SaleOrderMgr saleOrderMgr;

	/** The staff mgr. */
	private StaffMgr staffMgr;

	/** The list nvbh. */
	private List<Staff> listNVBH;
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH

	/** The list nvgh. */
	private List<Staff> listNVGH;
	private List<StaffVO> lstNVGHVo;//Danh sach NVGH

	/** The type payment all. Chon hinh thuc thanh toan tu dong. */
	private Integer typePaymentAll;

	private Integer isValueOrder;
	
	/**
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		listStatus = new ArrayList<StatusBean>();
		StatusBean withInvoice = new StatusBean(1L,
				Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
						"common.status.invoice"));
		StatusBean noInvoice = new StatusBean(0L,
				Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
						"common.status.no.invoice"));
		listStatus.add(withInvoice);
		listStatus.add(noInvoice);
		listStatus1 = new ArrayList<StatusBean>();
		StatusBean withInvoiceOrCancel = new StatusBean(1L,
				Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
				"common.status.invoice1"));
		listStatus1.add(withInvoiceOrCancel);
		listStatus1.add(noInvoice);
		listPayment = new ArrayList<StatusBean>();
		listPayment.add(new StatusBean((long) InvoiceCustPayment.MONEY
				.getValue(), Configuration.getResourceString(
				ConstantManager.VI_LANGUAGE, "common.payment.method.money")));
		listPayment.add(new StatusBean((long) InvoiceCustPayment.BANK_TRANSFER
				.getValue(), Configuration.getResourceString(
				ConstantManager.VI_LANGUAGE, "common.payment.method.bank")));
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		super.prepare();
	}

	/**
	 * @return String
	 * @author tientv
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		
		Staff staff = getStaffByCurrentUser();
		if (staff == null) {
			return PAGE_NOT_PERMISSION;
		}
		shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
		Date now = DateUtil.getCurrentGMTDate();
		Date dayLock = shopLockMgr.getNextLockedDay(staff.getShop().getId());
		if(dayLock != null){
			now = dayLock;
		}
		createFromDate = displayDate(now);
		createToDate = displayDate(now);
		
		//Lay danh sach nhan vien theo phan quyen
		StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
		if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
			filter.setIsLstChildStaffRoot(true);
		} else {
			filter.setIsLstChildStaffRoot(false);
		}
		filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
		filter.setStatus(ActiveType.RUNNING.getValue());
		filter.setLstShopId(Arrays.asList(currentUser.getShopRoot().getShopId()));
		//Lay NVBH
		filter.setLstObjectType(Arrays.asList(StaffObjectType.NVBH.getValue(), StaffObjectType.NVVS.getValue()));
		ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
		setLstNVBHVo(objVO.getLstObject());

		//Lay nhan vien thu tien & NVGH
		filter.setIsLstChildStaffRoot(false);
		filter.setLstObjectType(Arrays.asList(StaffObjectType.NVGH.getValue(), StaffObjectType.NVTT.getValue()));
		objVO = staffMgr.searchListStaffVOByFilter(filter);
		setLstNVGHVo(objVO.getLstObject());
		
		ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), "ORDER_SEARCH");
		if (numberValueOrderParam != null) {
			numberValue = numberValueOrderParam.getCode();
		} else {
			numberValue = "0";
		}
		/*ObjectVO<Staff> staffVO = staffMgr.getListPreAndVanStaff(null, null,
				null, shop.getId(), ActiveType.RUNNING,
				Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
		if (staffVO != null && staffVO.getLstObject().size() > 0) {
			listNVBH = staffVO.getLstObject();
		}
		StaffFilter filter = new StaffFilter();
		List<Integer> lstS = new ArrayList<Integer>();
		lstS.add(StaffObjectType.NVGH.getValue());
		lstS.add(StaffObjectType.NVTT.getValue());
		filter.setLstStaffType(lstS);
		filter.setStatus(ActiveType.RUNNING);
		filter.setShopCodeStaff(shop.getShopCode());
//		filter.setStaffType(StaffObjectType.NVGH);
		filter.setIsGetShopOnly(true);
		staffVO = staffMgr.getListStaff(filter);
		if (staffVO != null && staffVO.getLstObject().size() > 0) {
			listNVGH = staffVO.getLstObject();
		}*/
		return SUCCESS;
	}

	/**
	 * tim kiem hoa don VAT le 
	 * 
	 * @return the string
	 * @author phuongvm  
	 * @since 24/08/2014
	 */
	public String search() {
		result.put("total", 0);
		result.put("rows", new ArrayList<Invoice>());
		try {
			KPaging<Invoice> kPaging = new KPaging<Invoice>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			KPaging<SaleOrderVOEx> __kPaging = new KPaging<SaleOrderVOEx>();
			__kPaging.setPageSize(max);
			__kPaging.setPage(page - 1);
			Staff staff = getStaffByCurrentUser();
			if (staff == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString("vi", "common.label.staff.login")));
				return ERROR;
			}
			if (!StringUtil.isNullOrEmpty(shopCodeSearch)) {
				shop = shopMgr.getShopByCode(shopCodeSearch);
			} else {
				shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			}
			Date fromDate = DateUtil.parse(createFromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(createToDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Calendar calFrom = Calendar.getInstance();
			Calendar calTo = Calendar.getInstance();
			if (fromDate == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString("vi", "catalog.display.program.fromDate")));
				return ERROR;
			}
			if (toDate == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.toDate"));
				return ERROR;
			}

			calFrom.setTime(fromDate);
			calTo.setTime(toDate);
			if (calFrom.compareTo(calTo) > 0) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.toDate"));
				return ERROR;
			}

			Date lockDate = shopLockMgr.getNextLockedDay(shop.getId());
			if (lockDate == null) {
				lockDate = commonMgr.getSysDate();
			}

			if (chooseStatus == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FROM_DATE_NOT_GREATER_TO_DATE));
				return ERROR;
			} else if (chooseStatus == 1) {
				Boolean isdonGop = false;
				/** Luu gia tri dh khi tim kiem */
				if (numberValueOrder != null) {
					ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), "ORDER_SEARCH");
					if (numberValueOrderParam != null && ActiveType.RUNNING.equals(numberValueOrderParam.getStatus())) {
						numberValueOrderParam.setCode(numberValueOrder.toString());
						numberValueOrderParam.setName(numberValueOrder.toString());
						numberValueOrderParam.setUpdateUser(currentUser.getUserName());
						saleOrderMgr.updateShopParamByInvoice(numberValueOrderParam);
					} else if (numberValueOrderParam == null) {
						ShopParam addNumberValueOrderParam = new ShopParam();
						addNumberValueOrderParam.setShop(shop);
						addNumberValueOrderParam.setType("ORDER_SEARCH");
						addNumberValueOrderParam.setStatus(ActiveType.RUNNING);
						addNumberValueOrderParam.setCode(numberValueOrder.toString());
						addNumberValueOrderParam.setName(numberValueOrder.toString());
						addNumberValueOrderParam.setCreateUser(currentUser.getUserName());
						saleOrderMgr.createShopParamByInvoice(addNumberValueOrderParam);
					}

				}
				if (isVATGop != null && isVATGop) {
					isdonGop = isVATGop;
				}
				/** Da co hoa don */
				listInvoice = saleOrderMgr.getListInvoiceByCondition(null, invoiceNumber, redInvoiceNumber, saleStaffCode, deliveryStaffCode, customerCode, fromDate, toDate, taxValue, InvoiceCustPayment.parseValue(choosePaymentMethod), lockDate,
						shop.getId(), isValueOrder, isdonGop, numberValueOrder);
				if (listInvoice != null) {
					for (Invoice item : listInvoice) {
						item.formatData();
					}
					result.put("total", listInvoice.size());
					result.put("rows", listInvoice);
				} 
			} else {
				/** Luu gia tri dh khi tim kiem */
				if (numberValueOrder != null) {
					ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), "ORDER_SEARCH");
					if (numberValueOrderParam != null && ActiveType.RUNNING.equals(numberValueOrderParam.getStatus())) {
						numberValueOrderParam.setCode(numberValueOrder.toString());
						numberValueOrderParam.setName(numberValueOrder.toString());
						numberValueOrderParam.setUpdateUser(currentUser.getUserName());
						saleOrderMgr.updateShopParamByInvoice(numberValueOrderParam);
					} else if (numberValueOrderParam == null) {
						ShopParam addNumberValueOrderParam = new ShopParam();
						addNumberValueOrderParam.setShop(shop);
						addNumberValueOrderParam.setType("ORDER_SEARCH");
						addNumberValueOrderParam.setStatus(ActiveType.RUNNING);
						addNumberValueOrderParam.setCode(numberValueOrder.toString());
						addNumberValueOrderParam.setName(numberValueOrder.toString());
						addNumberValueOrderParam.setCreateUser(currentUser.getUserName());
						saleOrderMgr.createShopParamByInvoice(addNumberValueOrderParam);
					}
				}
				/** Chua co hoa don */
				listSaleOrderVOEx = new ArrayList<SaleOrderVOEx>();

				List<SaleOrderVOEx> listSaleOrderVOExTotal = saleOrderMgr.getListSaleOrderWithNoInvoiceEx(null, shop.getId(), saleStaffCode, deliveryStaffCode, invoiceNumber, fromDate, toDate, customerCode, lockDate, isValueOrder, numberValueOrder);
				if (!listSaleOrderVOExTotal.isEmpty()) {
					result.put("total", listSaleOrderVOExTotal.size());
					result.put("rows", listSaleOrderVOExTotal);
				} else {
					result.put("total", 0);
					result.put("rows", new ArrayList<Invoice>());
				}
			}
			listPayment = new ArrayList<StatusBean>();
			listPayment.add(new StatusBean((long) InvoiceCustPayment.MONEY.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.payment.method.money")));
			listPayment.add(new StatusBean((long) InvoiceCustPayment.BANK_TRANSFER.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.payment.method.bank")));
			return JSON;
		} catch (BusinessException e) {
			LogUtility.logError(e, "Error AdjustmentTaxAction.search");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}
	
	/**
	 * tim kiem hoa don VAT gop
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 20/08/2014
	 */
	public String searchVATgop() {
		result.put("total", 0);
		result.put("rows", new ArrayList<Invoice>());
		try {
			KPaging<Invoice> kPaging = new KPaging<Invoice>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			KPaging<SaleOrderVOEx> __kPaging = new KPaging<SaleOrderVOEx>();
			__kPaging.setPageSize(max);
			__kPaging.setPage(page - 1);
			Staff staff = getStaffByCurrentUser();
			if (staff == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString("vi", "common.label.staff.login")));
				return ERROR;
			}
			if (!StringUtil.isNullOrEmpty(shopCodeSearch)) {
				shop = shopMgr.getShopByCode(shopCodeSearch);
			} else {
				shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			}
			Date fromDate = DateUtil.parse(createFromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(createToDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Calendar calFrom = Calendar.getInstance();
			Calendar calTo = Calendar.getInstance();
			if (fromDate == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString("vi", "catalog.display.program.fromDate")));
				return ERROR;
			}
			if (toDate == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.toDate"));
				return ERROR;
			}

			calFrom.setTime(fromDate);
			calTo.setTime(toDate);
			if (calFrom.compareTo(calTo) > 0) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.toDate"));
				return ERROR;
			}

			Date lockDate = shopLockMgr.getNextLockedDay(shop.getId());
			if (lockDate == null) {
				lockDate = commonMgr.getSysDate();
			}

			if (chooseStatus == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FROM_DATE_NOT_GREATER_TO_DATE));
				return ERROR;
			} else if (chooseStatus == 0) {
				/** Chua co hoa don */
				listSaleOrderVOEx = new ArrayList<SaleOrderVOEx>();
				if (numberValueOrder1 != null) {
					ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), "ORDER_SEARCH");
					if (numberValueOrderParam != null && ActiveType.RUNNING.equals(numberValueOrderParam.getStatus())) {
						numberValueOrderParam.setCode(numberValueOrder1.toString());
						numberValueOrderParam.setName(numberValueOrder1.toString());
						numberValueOrderParam.setUpdateUser(currentUser.getUserName());
						saleOrderMgr.updateShopParamByInvoice(numberValueOrderParam);
					} else if (numberValueOrderParam == null) {
						ShopParam addNumberValueOrderParam = new ShopParam();
						addNumberValueOrderParam.setShop(shop);
						addNumberValueOrderParam.setType("ORDER_SEARCH");
						addNumberValueOrderParam.setStatus(ActiveType.RUNNING);
						addNumberValueOrderParam.setCode(numberValueOrder1.toString());
						addNumberValueOrderParam.setName(numberValueOrder1.toString());
						addNumberValueOrderParam.setCreateUser(currentUser.getUserName());
						saleOrderMgr.createShopParamByInvoice(addNumberValueOrderParam);
					}
				}
				List<SaleOrder> listSaleOrderVOExTotal = saleOrderMgr.getListSaleOrderWithNoInvoiceGOP(null, shop.getId(), saleStaffCode, deliveryStaffCode, invoiceNumber, fromDate, toDate, customerCode, lockDate, isValueOrder, numberValueOrder1);
				if (!listSaleOrderVOExTotal.isEmpty()) {
					result.put("total", listSaleOrderVOExTotal.size());
					result.put("rows", listSaleOrderVOExTotal);
				}
			}
			listPayment = new ArrayList<StatusBean>();
			listPayment.add(new StatusBean((long) InvoiceCustPayment.MONEY.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.payment.method.money")));
			listPayment.add(new StatusBean((long) InvoiceCustPayment.BANK_TRANSFER.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.payment.method.bank")));
			return JSON;
		} catch (BusinessException e) {
			LogUtility.logError(e, "Error AdjustmentTaxAction.search");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}

	/**
	 * Save.
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 12/09/2014
	 */
	public String save() {
		resetToken(result);
		try {
			List<Long> lstSaleOrderIdChoosen = new ArrayList<Long>();
			Staff staff = getStaffByCurrentUser();
			if (staff == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString("vi", "common.label.staff.login")));
				return ERROR;
			}
			//			shop = staff.getShop();
			//			shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			}
			if (listOrderNumber == null || listInvoiceNumber == null || listOrderNumber.size() != listInvoiceNumber.size() || listOrderNumber.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NO_SELECT_DATA, Configuration.getResourceString("vi", "sp.adjusttax.save.data")));
				return ERROR;
			}

			Map<Long, List<String>> listSOIdInvoice = new HashMap<Long, List<String>>();
			Map<Long, List<InvoiceCustPayment>> listCustPayment = new HashMap<Long, List<InvoiceCustPayment>>();
			for (int i = 0; i < listOrderNumber.size(); i++) {
				SaleOrder saleOrder = saleOrderMgr.getSaleOrderByOrderNumberAndShopId(listOrderNumber.get(i), shop.getId());
				//String __saleOrderId = listOrderNumber.get(i);
				//__saleOrderId = __saleOrderId.split("-")[0];
				//Long saleOrderId = Long.parseLong(__saleOrderId);
				Long saleOrderId = saleOrder.getId();
				if (listSOIdInvoice.get(saleOrderId) == null) {
					List<String> listInvoice = new ArrayList<String>();
					listInvoice.add(listInvoiceNumber.get(i));
					listSOIdInvoice.put(saleOrderId, listInvoice);

					List<InvoiceCustPayment> listCP = new ArrayList<InvoiceCustPayment>();
					if (typePaymentAll != null && InvoiceCustPayment.parseValue(typePaymentAll) != null) {
						listCP.add(InvoiceCustPayment.parseValue(typePaymentAll));
					} else {
						listCP.add(InvoiceCustPayment.parseValue(listPaymentMethod.get(i)));
					}
					listCustPayment.put(saleOrderId, listCP);
				} else {
					listSOIdInvoice.get(saleOrderId).add(listInvoiceNumber.get(i));
					if (typePaymentAll != null && InvoiceCustPayment.parseValue(typePaymentAll) != null) {
						listCustPayment.get(saleOrderId).add(InvoiceCustPayment.parseValue(typePaymentAll));
					} else {
						listCustPayment.get(saleOrderId).add(InvoiceCustPayment.parseValue(listPaymentMethod.get(i)));
					}
				}
			}

			/**
			 * Kiểm tra các số hóa đơn mới có trùng với số hóa đơn bị hủy và đã
			 * sử dụng hay không (kiểm tra trong bảng invoice những hóa đơn có
			 * status = 0 or status = 2, với SHOP_ID = SALE_ORDER.SHOP_ID)
			 */

			List<SaleOrder> listSOError = saleOrderMgr.checkDuplicateInvoiceNumber(shop.getId(), listSOIdInvoice);
			if (listSOError.size() > 0) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_EXIST_INVOICE_NUMBER, implodeSaleOrderForInvoiceNumber(listSOError)));
				return ERROR;
			}
			saleOrderMgr.createInvoiceFromSaleOrder(shop.getId(), currentUser.getUserName(), listSOIdInvoice, listCustPayment);
		} catch (Exception e) {
			if (e.getMessage() != null && e.getMessage().indexOf("the sale order need to update dilivery") != -1) {
				result.put(ERROR, true);
				String orderNumber = e.getMessage().substring(e.getMessage().indexOf("the sale order need to update dilivery") + "the sale order need to update dilivery".length() + 1, e.getMessage().length());
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_SALE_ORDER_NEED_TO_UPDATE_DELIVERY, orderNumber));
				return ERROR;
			}
			LogUtility.logError(e, "Error AdjustmentTaxAction.save");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}
	/**
	 * Save VAT gop chua co hoa don
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 26/08/2014
	 */
	public String saveVATGop() {
		resetToken(result);
		try {
			Staff staff = getStaffByCurrentUser();
			if (staff == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString("vi", "common.label.staff.login")));
				return ERROR;
			}
			//			shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
			}
			if (listOrderNumber == null || listInvoiceNumber == null || listOrderNumber.size() != listInvoiceNumber.size() || listOrderNumber.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NO_SELECT_DATA, Configuration.getResourceString("vi", "sp.adjusttax.save.data")));
				return ERROR;
			}
			Map<Long, List<String>> listSOIdInvoice = new HashMap<Long, List<String>>();
			Map<Long, List<InvoiceCustPayment>> listCustPayment = new HashMap<Long, List<InvoiceCustPayment>>();
			for (int i = 0; i < listOrderNumber.size(); i++) {
				SaleOrder saleOrder = saleOrderMgr.getSaleOrderByOrderNumberAndShopId(listOrderNumber.get(i), shop.getId());
				Long saleOrderId = saleOrder.getId();
				if (listSOIdInvoice.get(saleOrderId) == null) {
					List<String> listInvoice = new ArrayList<String>();
					listInvoice.add(listInvoiceNumber.get(i));
					listSOIdInvoice.put(saleOrderId, listInvoice);

					List<InvoiceCustPayment> listCP = new ArrayList<InvoiceCustPayment>();
					if (typePaymentAll != null && InvoiceCustPayment.parseValue(typePaymentAll) != null) {
						listCP.add(InvoiceCustPayment.parseValue(typePaymentAll));
					} else {
						listCP.add(InvoiceCustPayment.parseValue(listPaymentMethod.get(i)));
					}
					listCustPayment.put(saleOrderId, listCP);
				} else {
					listSOIdInvoice.get(saleOrderId).add(listInvoiceNumber.get(i));
					if (typePaymentAll != null && InvoiceCustPayment.parseValue(typePaymentAll) != null) {
						listCustPayment.get(saleOrderId).add(InvoiceCustPayment.parseValue(typePaymentAll));
					} else {
						listCustPayment.get(saleOrderId).add(InvoiceCustPayment.parseValue(listPaymentMethod.get(i)));
					}
				}
			}

			/**
			 * Kiểm tra các số hóa đơn mới có trùng với số hóa đơn bị hủy và đã
			 * sử dụng hay không (kiểm tra trong bảng invoice những hóa đơn có
			 * status = 0 or status = 2, với SHOP_ID = SALE_ORDER.SHOP_ID)
			 */

			List<SaleOrder> listSOError = saleOrderMgr.checkDuplicateInvoiceNumber(shop.getId(), listSOIdInvoice);
			if (listSOError.size() > 0) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_EXIST_INVOICE_NUMBER, implodeSaleOrderForInvoiceNumber(listSOError)));
				return ERROR;
			}
			if (!listSaleOrderIdx.isEmpty() && saleOrderIdPri != null && saleOrderIdPri > 0) {
				SaleOrder donChinh = saleOrderMgr.getSaleOrderById(saleOrderIdPri);
				List<SaleOrderDetail> lstDetailGop = new ArrayList<SaleOrderDetail>();
				List<SaleOrder> lstSO = new ArrayList<SaleOrder>();
				if (donChinh != null) {
					for (Long id : listSaleOrderIdx) {
						SaleOrder so = saleOrderMgr.getSaleOrderById(id);
						if (so != null) {
							ObjectVO<SaleOrderDetail> lstSOD = saleOrderMgr.getListSaleOrderDetailBySaleOrderId(null, so.getId());
							lstDetailGop.addAll(lstSOD.getLstObject());
							lstSO.add(so);
						}
					}
					if (!lstDetailGop.isEmpty()) {
						lstDetailGop = gopSanPham(lstDetailGop);
					}
					saleOrderMgr.createInvoiceFromSaleOrderEx(shop.getId(), currentUser.getUserName(), listSOIdInvoice, listCustPayment, donChinh, lstSO, lstDetailGop);
				}
			}
		} catch (Exception e) {
			if (e.getMessage() != null && e.getMessage().indexOf("the sale order need to update dilivery") != -1) {
				result.put(ERROR, true);
				String orderNumber = e.getMessage().substring(e.getMessage().indexOf("the sale order need to update dilivery") + "the sale order need to update dilivery".length() + 1, e.getMessage().length());
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_SALE_ORDER_NEED_TO_UPDATE_DELIVERY, orderNumber));
				return ERROR;
			}
			LogUtility.logError(e, "Error AdjustmentTaxAction.saveVATGop");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}

	/**
	 * Save with invoice.
	 * 
	 * @return the string
	 */
	public String saveWithInvoice() {
		resetToken(result);
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null && staff.getShop() != null) {
					shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
				}
			}
			if ((listInvoiceId == null || listInvoiceNumber == null || listInvoiceId.size() != listInvoiceNumber.size()) && listDeleteInvoiceId == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				return ERROR;
			}
			if (listInvoiceId != null && listInvoiceNumber != null && listInvoiceId.size() == listInvoiceNumber.size()) {
				List<String> listDuplicateInv = saleOrderMgr.checkDuplicateInvoiceNumberEx1(shop.getId(), listInvoiceNumber);
				if (listDuplicateInv != null && listDuplicateInv.size() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_DUPLICATE_INVOICE_NUMBER, implodeInvoiceNumber(listDuplicateInv)));
					return ERROR;
				}
				List<Long> __listInvoiceId = new ArrayList<Long>();
				for (String invoiceId : listInvoiceId) {
					try {
						Long __invoiceId = Long.parseLong(invoiceId);
						__listInvoiceId.add(__invoiceId);
					} catch (Exception e) {
						LogUtility.logError(e, "Error saveWithInvoice: Long.parseLong(invoiceId)");
					}
				}
				saleOrderMgr.updateInvoiceFromSaleOrder(shop.getId(), __listInvoiceId, listInvoiceNumber, currentUser.getUserName());
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * Implode sale order for invoice number.
	 * 
	 * @param listSaleOrder
	 *            the list sale order
	 * @return the string
	 */
	private String implodeSaleOrderForInvoiceNumber(
			List<SaleOrder> listSaleOrder) {
		String result = "";
		result += listSaleOrder.get(0).getOrderNumber();
		for (int i = 1; i < listSaleOrder.size(); i++) {
			result += ", " + listSaleOrder.get(i).getOrderNumber();
		}
		return result;
	}

	/**
	 * Implode invoice number.
	 * 
	 * @param listInvoiceNumber
	 *            the list invoice number
	 * @return the string
	 */
	private String implodeInvoiceNumber(List<String> listInvoiceNumber) {
		String result = "";
		result += listInvoiceNumber.get(0);
		for (int i = 1; i < listInvoiceNumber.size(); i++) {
			result += ", " + listInvoiceNumber.get(i);
		}
		return result;
	}

	/**
	 * Cancel.
	 * 
	 * @return the string
	 * @author phut
	 * @since Oct 1, 2012
	 */
	public String cancel() {
		resetToken(result);
		try {
			List<Long> __listInvoiceId = new ArrayList<Long>();
			for (String invoiceId : listDeleteInvoiceId) {
				try {
					Long __invoiceId = Long.parseLong(invoiceId);
					__listInvoiceId.add(__invoiceId);
				} catch (Exception e) {
					LogUtility.logError(e, "Error cancel: Long.parseLong(invoiceId)");
				}
			}
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null && staff.getShop() != null) {
					shop = staff.getShop();
				}
			}
			saleOrderMgr.removeInvoice(__listInvoiceId, shop.getId(), currentUser.getUserName());
		} catch (Exception e) {
			LogUtility.logError(e, "Error AdjustmentTaxAction.cancel");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}
	
	/**
	 * gop cac san pham co cung giaban, VAT
	 * 
	 * @return List<SaleOrderDetail>
	 * @author phuongvm   
	 * @since 21/08/2014
	 */
	public List<SaleOrderDetail> gopSanPham(List<SaleOrderDetail> lst){
		List<SaleOrderDetail> lstKQ = new ArrayList<SaleOrderDetail>();
		do {
			SaleOrderDetail sod = new SaleOrderDetail();
			sod = lst.get(lst.size()-1);
			lstKQ.add(sod);
			lst.remove(lst.size()-1);
			if(sod.getProduct()!=null && sod.getIsFreeItem().equals(0)){
				for(int i=lst.size()-1;i>=0;i--){
					SaleOrderDetail detail = lst.get(i);
					if(detail.getProduct()!=null && detail.getIsFreeItem().equals(0)){
						if(detail.getProduct().getId().equals(sod.getProduct().getId()) && detail.getVat().equals(sod.getVat())){
							sod.setQuantity(sod.getQuantity()+detail.getQuantity());
							sod.setDiscountAmount(sod.getDiscountAmount().add(detail.getDiscountAmount()));
							sod.setAmount(sod.getAmount().add(detail.getAmount()));
							sod.setDiscountPercent(null);
							lst.remove(i);
						}
					}
				}
			}
		} while (lst.size()>0);
		
		return lstKQ;
	}
	/**
	 * gop hoa don
	 * 
	 * @return the string
	 * @author phuongvm   
	 * @since 21/08/2014
	 */
	public String gopHoaDon() {
		result.put("page", page);
		result.put("rows", rows);
		try {
			if(!listSaleOrderIdx.isEmpty() && saleOrderIdPri!=null && saleOrderIdPri>0){
				SaleOrder donChinh = saleOrderMgr.getSaleOrderById(saleOrderIdPri);
				List<SaleOrderDetail> lstDetailGop = new ArrayList<SaleOrderDetail>();
				List<SaleOrder> lstSO = new ArrayList<SaleOrder>();
				if(donChinh!=null){
					for(Long id : listSaleOrderIdx){
						SaleOrder so = saleOrderMgr.getSaleOrderById(id);
						if(so!=null){
							ObjectVO<SaleOrderDetail> lstSOD =  saleOrderMgr.getListSaleOrderDetailBySaleOrderId(null,so.getId());
							lstDetailGop.addAll(lstSOD.getLstObject());
							lstSO.add(so);
						}
					}
					if(!lstDetailGop.isEmpty()){
						lstDetailGop = gopSanPham(lstDetailGop);
					}
					List<SaleOrderVOEx>	listSaleOrderVOExTotal = saleOrderMgr.taoDonGopVaTraVeDanhSachDonTach(donChinh,lstDetailGop,lstSO);
					if(!listSaleOrderVOExTotal.isEmpty()){
						result.put("total", listSaleOrderVOExTotal.size());
						result.put("rows", listSaleOrderVOExTotal);
					}
				}
			}
			listPayment = new ArrayList<StatusBean>();
			listPayment.add(new StatusBean((long) InvoiceCustPayment.MONEY.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.payment.method.money")));
			listPayment.add(new StatusBean((long) InvoiceCustPayment.BANK_TRANSFER.getValue(),Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.payment.method.bank")));
			return JSON;
		} catch (BusinessException e) {
			LogUtility.logError(e, "Error AdjustmentTaxAction.gopHoaDon");
			result.put(ERROR, true);
			result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}

	/**
	 * Gets the list status.
	 * 
	 * @return the list status
	 */
	public List<StatusBean> getListStatus() {
		return listStatus;
	}

	/**
	 * Sets the list status.
	 * 
	 * @param listStatus
	 *            the new list status
	 */
	public void setListStatus(List<StatusBean> listStatus) {
		this.listStatus = listStatus;
	}

	/**
	 * Gets the choose status.
	 * 
	 * @return the choose status
	 */
	public Integer getChooseStatus() {
		return chooseStatus;
	}

	/**
	 * Sets the choose status.
	 * 
	 * @param chooseStatus
	 *            the new choose status
	 */
	public void setChooseStatus(Integer chooseStatus) {
		this.chooseStatus = chooseStatus;
	}

	/**
	 * Gets the sale staff code.
	 * 
	 * @return the sale staff code
	 */
	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	/**
	 * Sets the sale staff code.
	 * 
	 * @param saleStaffCode
	 *            the new sale staff code
	 */
	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}

	/**
	 * Gets the delivery staff code.
	 * 
	 * @return the delivery staff code
	 */
	public String getDeliveryStaffCode() {
		return deliveryStaffCode;
	}

	/**
	 * Sets the delivery staff code.
	 * 
	 * @param deliveryStaffCode
	 *            the new delivery staff code
	 */
	public void setDeliveryStaffCode(String deliveryStaffCode) {
		this.deliveryStaffCode = deliveryStaffCode;
	}

	/**
	 * Gets the tax value.
	 * 
	 * @return the tax value
	 */
	public Float getTaxValue() {
		return taxValue;
	}

	/**
	 * Sets the tax value.
	 * 
	 * @param taxValue
	 *            the new tax value
	 */
	public void setTaxValue(Float taxValue) {
		this.taxValue = taxValue;
	}

	/**
	 * Gets the invoice number.
	 * 
	 * @return the invoice number
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * Sets the invoice number.
	 * 
	 * @param invoiceNumber
	 *            the new invoice number
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * Gets the creates the from date.
	 * 
	 * @return the creates the from date
	 */
	public String getCreateFromDate() {
		return createFromDate;
	}

	/**
	 * Sets the creates the from date.
	 * 
	 * @param createFromDate
	 *            the new creates the from date
	 */
	public void setCreateFromDate(String createFromDate) {
		this.createFromDate = createFromDate;
	}

	/**
	 * Gets the creates the to date.
	 * 
	 * @return the creates the to date
	 */
	public String getCreateToDate() {
		return createToDate;
	}

	/**
	 * Sets the creates the to date.
	 * 
	 * @param createToDate
	 *            the new creates the to date
	 */
	public void setCreateToDate(String createToDate) {
		this.createToDate = createToDate;
	}

	/**
	 * Gets the customer code.
	 * 
	 * @return the customer code
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * Sets the customer code.
	 * 
	 * @param customerCode
	 *            the new customer code
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	/**
	 * Gets the list invoice number.
	 * 
	 * @return the list invoice number
	 */
	public List<String> getListInvoiceNumber() {
		return listInvoiceNumber;
	}

	/**
	 * Sets the list invoice number.
	 * 
	 * @param listInvoiceNumber
	 *            the new list invoice number
	 */
	public void setListInvoiceNumber(List<String> listInvoiceNumber) {
		this.listInvoiceNumber = listInvoiceNumber;
	}

	/**
	 * Gets the shop.
	 * 
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 * 
	 * @param shop
	 *            the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * Gets the list payment.
	 * 
	 * @return the list payment
	 */
	public List<StatusBean> getListPayment() {
		return listPayment;
	}

	/**
	 * Sets the list payment.
	 * 
	 * @param listPayment
	 *            the new list payment
	 */
	public void setListPayment(List<StatusBean> listPayment) {
		this.listPayment = listPayment;
	}

	/**
	 * Gets the list payment method.
	 * 
	 * @return the list payment method
	 */
	public List<Integer> getListPaymentMethod() {
		return listPaymentMethod;
	}

	/**
	 * Sets the list payment method.
	 * 
	 * @param listPaymentMethod
	 *            the new list payment method
	 */
	public void setListPaymentMethod(List<Integer> listPaymentMethod) {
		this.listPaymentMethod = listPaymentMethod;
	}

	/**
	 * Gets the choose payment method.
	 * 
	 * @return the choose payment method
	 */
	public Integer getChoosePaymentMethod() {
		return choosePaymentMethod;
	}

	/**
	 * Sets the choose payment method.
	 * 
	 * @param choosePaymentMethod
	 *            the new choose payment method
	 */
	public void setChoosePaymentMethod(Integer choosePaymentMethod) {
		this.choosePaymentMethod = choosePaymentMethod;
	}

	/**
	 * Sets the list sale order vo ex.
	 * 
	 * @param listSaleOrderVOEx
	 *            the new list sale order vo ex
	 */
	public void setListSaleOrderVOEx(List<SaleOrderVOEx> listSaleOrderVOEx) {
		this.listSaleOrderVOEx = listSaleOrderVOEx;
	}

	/**
	 * Gets the list sale order vo ex.
	 * 
	 * @return the list sale order vo ex
	 */
	public List<SaleOrderVOEx> getListSaleOrderVOEx() {
		return listSaleOrderVOEx;
	}

	/**
	 * Gets the list sale order id.
	 * 
	 * @return the list sale order id
	 */
	public List<String> getListSaleOrderId() {
		return listSaleOrderId;
	}

	/**
	 * Sets the list sale order id.
	 * 
	 * @param listSaleOrderId
	 *            the new list sale order id
	 */
	public void setListSaleOrderId(List<String> listSaleOrderId) {
		this.listSaleOrderId = listSaleOrderId;
	}

	/**
	 * Sets the list invoice.
	 * 
	 * @param listInvoice
	 *            the new list invoice
	 */
	public void setListInvoice(List<Invoice> listInvoice) {
		this.listInvoice = listInvoice;
	}

	/**
	 * Gets the list invoice.
	 * 
	 * @return the list invoice
	 */
	public List<Invoice> getListInvoice() {
		return listInvoice;
	}

	/**
	 * Sets the list invoice id.
	 * 
	 * @param listInvoiceId
	 *            the new list invoice id
	 */
	public void setListInvoiceId(List<String> listInvoiceId) {
		this.listInvoiceId = listInvoiceId;
	}

	/**
	 * Gets the list invoice id.
	 * 
	 * @return the list invoice id
	 */
	public List<String> getListInvoiceId() {
		return listInvoiceId;
	}

	/**
	 * Gets the list nvbh.
	 * 
	 * @return the list nvbh
	 */
	public List<Staff> getListNVBH() {
		return listNVBH;
	}

	/**
	 * Sets the list nvbh.
	 * 
	 * @param listNVBH
	 *            the new list nvbh
	 */
	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}

	/**
	 * Gets the list nvgh.
	 * 
	 * @return the list nvgh
	 */
	public List<Staff> getListNVGH() {
		return listNVGH;
	}

	/**
	 * Sets the list nvgh.
	 * 
	 * @param listNVGH
	 *            the new list nvgh
	 */
	public void setListNVGH(List<Staff> listNVGH) {
		this.listNVGH = listNVGH;
	}

	public Integer getTypePaymentAll() {
		return typePaymentAll;
	}

	public void setTypePaymentAll(Integer typePaymentAll) {
		this.typePaymentAll = typePaymentAll;
	}

	public List<String> getListOrderNumber() {
		return listOrderNumber;
	}

	public void setListOrderNumber(List<String> listOrderNumber) {
		this.listOrderNumber = listOrderNumber;
	}

	public String getRedInvoiceNumber() {
		return redInvoiceNumber;
	}

	public void setRedInvoiceNumber(String redInvoiceNumber) {
		this.redInvoiceNumber = redInvoiceNumber;
	}

	public List<String> getListDeleteInvoiceId() {
		return listDeleteInvoiceId;
	}

	public void setListDeleteInvoiceId(List<String> listDeleteInvoiceId) {
		this.listDeleteInvoiceId = listDeleteInvoiceId;
	}
	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}

	public List<Long> getListSaleOrderIdx() {
		return listSaleOrderIdx;
	}

	public void setListSaleOrderIdx(List<Long> listSaleOrderIdx) {
		this.listSaleOrderIdx = listSaleOrderIdx;
	}

	public Long getSaleOrderIdPri() {
		return saleOrderIdPri;
	}

	public void setSaleOrderIdPri(Long saleOrderIdPri) {
		this.saleOrderIdPri = saleOrderIdPri;
	}

	public Boolean getIsVATGop() {
		return isVATGop;
	}

	public void setIsVATGop(Boolean isVATGop) {
		this.isVATGop = isVATGop;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public String getNumberValue() {
		return numberValue;
	}

	public void setNumberValue(String numberValue) {
		this.numberValue = numberValue;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public Integer getNumberValueOrder() {
		return numberValueOrder;
	}

	public void setNumberValueOrder(Integer numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}

	public Integer getNumberValueOrder1() {
		return numberValueOrder1;
	}

	public void setNumberValueOrder1(Integer numberValueOrder1) {
		this.numberValueOrder1 = numberValueOrder1;
	}

	public String getShopCodeSearch() {
		return shopCodeSearch;
	}

	public void setShopCodeSearch(String shopCodeSearch) {
		this.shopCodeSearch = shopCodeSearch;
	}
	
}

package ths.dms.web.action.saleproduct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.KeyShopMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.OrderXmlObject;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderVOEx2;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.entities.vo.WarehouseVO;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

// TODO: Auto-generated Javadoc
/**
 * The QUAN LY BAN HANG
 * @author tientv
 */
public class SearchSaleTransactionAction extends AbstractAction {
	
	/** The Constant serialVersionUID. */
	
	private static final long serialVersionUID = 1L;
	
	private SaleOrderMgr saleOrderMgr;
	private StockMgr stockMgr;
	private KeyShopMgr keyShopMgr;
	private StockManagerMgr stockManagerMgr;
	
	private List<StaffVO> lstNVBH;
	private List<StaffVO> lstNVGH;
	private List<SaleOrderDetailVOEx> order_detail;//Danh sach SP ban
	private List<SaleOrderDetailVOEx> order_detail_promotion;//Danh sach SPKM
	private List<StockTotalVO> productStocks;// Danh sach san pham co the ban cua NPP
	private List<StockTotalVO> productWarehouses;// Danh sach san pham co the ban cua NPP theo kho
	private List<Product> products;// Danh sach san pham nganh hang Z
	private List<OrderProductVO> orderProductVOs;
	private List<WarehouseVO> lstWarehouse;//Danh sach kho
	private Map<Long,List<StockTotalVO>> mapProductByWarehouse;
	
	private SaleOrder saleOrder;
	private StockTrans stockTrans;
	private Shop currentShop;
	
	private String currentShopCode;
	private String customerCode;
	private String customerName;
	private String orderNumber;
	private String orderStatus;
	private String orderSource;
	private String refOrderNumber;
	private String orderType;
	private String fromDate;
	private String toDate;
	private String saleStaff;
	private String deliveryStaff;
	private String deliveryDate;
	private String fromSaleOrderNumber;
	private List<ApParam> priority;//Do uu tien
	private int priorityId;
	private String priorityCode;
	private String apParamCode;
	private Staff staff;
	private String lnk;
	private Long orderId;
	private String fromOwnerName;
	private String toOwnerName;
	private Date lockDay;
	private Integer approved;
	private Integer approvedStep;
	private Integer saleOrderType;
	private String isShowPrice;
	
	/**
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		stockMgr = (StockMgr) context.getBean("stockMgr");
		keyShopMgr = (KeyShopMgr) context.getBean("keyShopMgr");
		stockManagerMgr = (StockManagerMgr) context.getBean("stockManagerMgr");
		//staffMgr = (StaffMgr) context.getBean("staffMgr");
		//commonMgr = (CommonMgr) context.getBean("commonMgr");
		//shopLockMgr = (ShopLockMgr) context.getBean("shopLockMgr");
		staff = getStaffByCurrentUser();
		if (!StringUtil.isNullOrEmpty(currentShopCode)) {
			currentShop = shopMgr.getShopByCode(currentShopCode);
			shopId = currentShop.getId();
		} else if (currentUser != null && currentUser.getShopRoot() != null) {
			shopId = currentUser.getShopRoot().getShopId();
			currentShop = shopMgr.getShopById(shopId);
		}
		
	}	
	
	/**
	 * @author tientv
	 */
	@Override
	public String execute() {
		resetToken(result);	
        try {					
			//Load list do uu tien
			priority=apParamMgr.getListApParam(ApParamType.ORDER_PIRITY, ActiveType.RUNNING);
			if(priority==null){
				priority = new ArrayList<ApParam>();
			} else {
				ApParam optionAll = new ApParam();
				optionAll.setApParamCode(Constant.VALUE_ALL);
				optionAll.setApParamName("Tất cả");
				priority.add(0, optionAll);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SearchSaleTransactionAction.checkRolesAndGetShop");
			return PAGE_NOT_FOUND;
		}
		return SUCCESS;
	}
	
	/**
	 * Ham load trang chi tiet don hang
	 * @author nhanlt6
	 * @since 17-11-2014
	 * 
	 */
	public String loadPageViewOrder() throws Exception {	
		try{
			if (currentShop == null) {
				return PAGE_NOT_PERMISSION;
			}
			if (orderId == null || orderId == 0) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NO_SELECT,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.order.title"));
				return SUCCESS;
			}
			if(!StringUtil.isNullOrEmpty(orderType) && (OrderType.DP.getValue().equals(orderType) || OrderType.GO.getValue().equals(orderType)
					 || OrderType.DCT.getValue().equals(orderType) || OrderType.DCG.getValue().equals(orderType)
					 || OrderType.DC.getValue().equals(orderType))){
				stockTrans = stockMgr.getStockTransById(orderId);
				if(stockTrans != null){
					if(OrderType.GO.getValue().equals(stockTrans.getTransType())){
						List<StockTrans> lstFromDP = stockMgr.getListStockTransByFromId(stockTrans.getId());
						if(lstFromDP != null && lstFromDP.size()>0){
							fromSaleOrderNumber = lstFromDP.get(0).getStockTransCode();
							for(int i = 1 ; i < lstFromDP.size() ; i++){
								fromSaleOrderNumber += ", " + lstFromDP.get(i).getStockTransCode();
							}
						}
					}
					ObjectVO<StockTransLotVO> obj = stockMgr.getListStockTransLotVOForSearchSale(null, stockTrans.getId());
					if (obj != null && obj.getLstObject() != null && obj.getLstObject().size() > 0) {
						//lay nhan vien
						if (OrderType.GO.getValue().equals(stockTrans.getTransType()) && obj.getLstObject().get(0).getFromOwnerId() != null) {
							staff = staffMgr.getStaffById(obj.getLstObject().get(0).getFromOwnerId());
						} else if (OrderType.DP.getValue().equals(stockTrans.getTransType()) && obj.getLstObject().get(0).getToOwnerId() != null) {
							staff = staffMgr.getStaffById(obj.getLstObject().get(0).getToOwnerId());
						}
						//lay kho nguon kho dich
						if (OrderType.DC.getValue().equals(stockTrans.getTransType())) {
							Warehouse whFrom = null;
							if (obj.getLstObject().get(0).getFromOwnerId() != null) {
								whFrom = stockManagerMgr.getWarehouseById(obj.getLstObject().get(0).getFromOwnerId());
							}
							if (whFrom != null) {
								fromOwnerName = whFrom.getWarehouseName() + " - " + whFrom.getShop().getShopName();
							}
							Warehouse whTo = null;
							if (obj.getLstObject().get(0).getToOwnerId() != null) {
								whTo = stockManagerMgr.getWarehouseById(obj.getLstObject().get(0).getToOwnerId());
							}
							if (whTo != null) {
								toOwnerName = whTo.getWarehouseName() + " - " + whTo.getShop().getShopName();
							}
						}
					}
				}
				if(staff == null){
					staff = new Staff();
				}
			}else{
				saleOrder = saleOrderMgr.getSaleOrderById(orderId);
				if(saleOrder.getFromSaleOrder() != null){
					fromSaleOrderNumber = saleOrder.getFromSaleOrder().getOrderNumber();
				}
			}
			if (saleOrder == null) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.order.title"));
				
			}else if(saleOrder.getShop()==null || !saleOrder.getShop().getId().equals(currentShop.getId())){
				return PAGE_NOT_PERMISSION;
			}
		}catch(Exception ex){
			LogUtility.logError(ex, "SearchSaleTransactionAction.loadPageViewOrder - " + ex.getMessage());
		}
		if(!StringUtil.isNullOrEmpty(orderType)){
			if(OrderType.DP.getValue().equals(orderType)){
				return "dp";
			}else if(OrderType.GO.getValue().equals(orderType)){
				return "go";
			}else if(OrderType.DC.getValue().equals(orderType)){
				return "dc";
			}else if(OrderType.DCT.getValue().equals(orderType) || OrderType.DCG.getValue().equals(orderType)){
				return "dctg";
			}
		}
		return SUCCESS;
	}

	/**
	 * Ham load danh sach don hang
	 * @author nhanlt6
	 * @since 17-11-2014
	 * 
	 */
	public String searchOrder(){
		try {
			if (staff != null) {
				SoFilter soFilter = new SoFilter();
				ObjectVO<SaleOrderVOEx2> lstSaleOrder = null;
				result.put("page", page);
			    result.put("max", max);
				KPaging<SaleOrderVOEx2> kPaging = new KPaging<SaleOrderVOEx2>();
				kPaging.setPageSize(max);
				kPaging.setPage(page-1);
				Date startTemp = null;
				Date endTemp  = null;
				Date deliveryDateTemp  = null;
				
				soFilter.setkPagingVOEx2(kPaging);
				if(!StringUtil.isNullOrEmpty(fromDate)){
					startTemp = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
					soFilter.setFromDate(startTemp);
				}
				if(!StringUtil.isNullOrEmpty(toDate)){
					endTemp = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
					soFilter.setToDate(endTemp);
				}
				if(!StringUtil.isNullOrEmpty(deliveryDate)){
					deliveryDateTemp = DateUtil.parse(deliveryDate, ConstantManager.FULL_DATE_FORMAT);
					soFilter.setDeliveryDate(deliveryDateTemp);
				}
				if(!StringUtil.isNullOrEmpty(orderSource)){
					Integer sourceValue = Integer.valueOf(orderSource);
					if (sourceValue != null) {
						soFilter.setOrderSource(SaleOrderSource.parseValue(sourceValue));
					}
				}
				if (!StringUtil.isNullOrEmpty(orderType)) {
					String[] arr = orderType.split(",");
					if (arr.length == 1) {
						soFilter.setOrderType(OrderType.parseValue(arr[0]));
					} else {
						soFilter.setLstOrderTypeStr(new ArrayList<String>());
						for (int i = 0, sz = arr.length; i < sz; i++) {
							if (arr[i].trim().length() == 0) {
								soFilter.setLstOrderTypeStr(null);
							}
							soFilter.getLstOrderTypeStr().add(arr[i]);
						}
					}
				}
				if(!StringUtil.isNullOrEmpty(priorityCode)){
					if (!priorityCode.equals(Constant.VALUE_ALL)) {
						soFilter.setPriorityCode(priorityCode);
					}
				}
				
//						if(!StringUtil.isNullOrEmpty(orderStatus)){
//							Integer sourceValue = Integer.valueOf(orderStatus);
//							if (sourceValue != null) {
//								if (sourceValue == 2) {
//									soFilter.setSaleOrderType(SaleOrderType.RETURNED);
//								} else if (sourceValue == 0 || sourceValue == 1) {
//									if (soFilter.getOrderType() != null) {
//										if (OrderType.CM.getValue().equals(soFilter.getOrderType().getValue()) || OrderType.CO.getValue().equals(soFilter.getOrderType().getValue())) {
//											soFilter.setSaleOrderType(SaleOrderType.RETURNED_ORDER);
//										} else {
//											soFilter.setSaleOrderType(SaleOrderType.NOT_YET_RETURNED);
//										}
//									} 
//									soFilter.setApproval(SaleOrderStatus.parseValue(sourceValue));
//								}
//							}
//						}
				if(saleOrderType != null && saleOrderType != -1){
					soFilter.setSaleOrderType(SaleOrderType.parseValue(saleOrderType));
				}
				if(approved != null){
					soFilter.setApproval(SaleOrderStatus.parseValue(approved));
				}
				if(approvedStep != null){
					soFilter.setApprovedStep(SaleOrderStep.parseValue(approvedStep));
				}
				soFilter.setShortCode(customerCode);
				soFilter.setCustomeName(customerName);
				soFilter.setOrderNumber(orderNumber);
				soFilter.setRefOrderNumber(refOrderNumber);
				soFilter.setStaffCode(saleStaff);
				soFilter.setDeliveryCode(deliveryStaff);
				soFilter.setShopId(shopId);
				soFilter.setStrListUserId(super.getStrListUserId());
				lstSaleOrder = saleOrderMgr.searchStockTrans(soFilter);
				if (lstSaleOrder.getLstObject().size()>0) {	
					result.put("total", lstSaleOrder.getkPaging().getTotalRows());
				    result.put("rows", lstSaleOrder.getLstObject());
				}else{
					result.put("total", 0);
					result.put("rows", new ArrayList<SaleOrderVOEx2>());
				}
			}
		} catch (Exception e) {
			result.put("total", 0);
			result.put("rows", new ArrayList<SaleOrderVOEx2>());
			LogUtility.logError(e, "SearchSaleTransactionAction.searchOrder");
		}
		return JSON;
	}
	
	/**
	 * Xuat danh sach don hang ra file xml
	 * 
	 * @author lacnv1
	 * @since Mar 14, 2015
	 */
	public String exportXml() throws Exception {
		try {
			if (currentShop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
			SoFilter soFilter = new SoFilter();
			Date startTemp = null;
			Date endTemp  = null;
			Date deliveryDateTemp  = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				startTemp = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
				soFilter.setFromDate(startTemp);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				endTemp = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
				soFilter.setToDate(endTemp);
			}
			if (!StringUtil.isNullOrEmpty(deliveryDate)) {
				deliveryDateTemp = DateUtil.parse(deliveryDate, ConstantManager.FULL_DATE_FORMAT);
				soFilter.setDeliveryDate(deliveryDateTemp);
			}
			if (!StringUtil.isNullOrEmpty(orderSource)) {
				Integer sourceValue = Integer.valueOf(orderSource);
				if (sourceValue != null) {
					soFilter.setOrderSource(SaleOrderSource.parseValue(sourceValue));
				}
			}
			if (!StringUtil.isNullOrEmpty(orderType)) {
				String[] arr = orderType.split(",");
				if (arr.length == 1) {
					soFilter.setOrderType(OrderType.parseValue(arr[0]));
				} else {
					soFilter.setLstOrderTypeStr(new ArrayList<String>());
					for (int i = 0, sz = arr.length; i < sz; i++) {
						if (arr[i].trim().length() == 0) {
							soFilter.setLstOrderTypeStr(null);
						}
						soFilter.getLstOrderTypeStr().add(arr[i]);
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(priorityCode)) {
				if (!priorityCode.equals("-1")) {
					soFilter.setPriorityCode(priorityCode);
				}
			}

			if (saleOrderType != null && saleOrderType != -1){
				soFilter.setSaleOrderType(SaleOrderType.parseValue(saleOrderType));
			}
			if (approved != null) {
				soFilter.setApproval(SaleOrderStatus.parseValue(approved));
			}
			if (approvedStep != null) {
				soFilter.setApprovedStep(SaleOrderStep.parseValue(approvedStep));
			}
			soFilter.setShortCode(customerCode);
			soFilter.setCustomeName(customerName);
			soFilter.setOrderNumber(orderNumber);
			soFilter.setRefOrderNumber(refOrderNumber);
			soFilter.setStaffCode(saleStaff);
			soFilter.setDeliveryCode(deliveryStaff);
			soFilter.setShopId(shopId);
			
			List<OrderXmlObject> lstXmlObj = saleOrderMgr.getSaleOrderXmlObject(soFilter);
			if (lstXmlObj == null || lstXmlObj.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("sale.order.no.row.in.list"));
				return JSON;
			}
			
			if (lstXmlObj != null && lstXmlObj.size() > 0) {
				List<String> sourceFiles = new ArrayList<String>();
				List<String> contentFiles = new ArrayList<String>();
				OrderXmlObject obj = null;
				String fileName = "Exp_SalesOrder_" + currentShop.getShopCode() + "_"
						+ DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_STOCKTRAN);
				for (int i = 0; i < lstXmlObj.size(); i++) {
					obj = lstXmlObj.get(i);
					long milis = System.currentTimeMillis();
					sourceFiles.add(fileName + i + milis + ".xml");
					contentFiles.add(obj.getXmlContent());
				}
				
				String outputFile = new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_FILE_EXPORT)).append("_").append("BCDH").append(".zip").toString();
				
				String storePath = Configuration.getStoreRealPath() + outputFile;
				
				/*ApParam ap = apParamMgr.getApParamByCode("AUTO_PO_ZIP_PASS", ApParamType.AUTO_PO_ZIP_PASS);
				String psswd = null;
				if (ap != null && ActiveType.RUNNING.equals(ap.getStatus()) && ap.getValue() != null) {
					psswd = ap.getValue();
				}*/
				
				FileUtility.createZipFile(sourceFiles, contentFiles, storePath, null);
				
				String outputDownload = Configuration.getExportExcelPath() + outputFile;
				
				result.put(ERROR, false);
				result.put(REPORT_PATH, outputDownload);
				result.put("hasData", true);
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "SearchSaleTransactionAction.exportXml: " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	public String loadStaffByShopCode() {
		try {
			//Lay danh sach nhan vien
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setIsLstChildStaffRoot(false);
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setLstShopId(Arrays.asList(shopId));
			filter.setIsGetChildShop(false);
			//Lay NVBH
			filter.setStaffIdListStr(getStrListUserId());
			filter.setObjectType(StaffSpecificType.STAFF.getValue());
			ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVBH = objVO.getLstObject();

			//Lay nhan vien NVGH
			filter.setStaffIdListStr(null);
			filter.setObjectType(StaffSpecificType.NVGH.getValue());
			objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVGH = objVO.getLstObject();

			//cau hinh show gia hay khong
			List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
			if (lstPr != null && lstPr.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())) {//cho show gia
				result.put("isShowPrice", true);
			} else {
				result.put("isShowPrice", false);
			}
			lockDay = shopLockMgr.getNextLockedDay(currentShop.getId());
			if (lockDay == null) {
				Calendar calendar = new GregorianCalendar();
				Date now = calendar.getTime();
				lockDay = now;
			}
			fromDate = DateUtil.toDateString(lockDay, DateUtil.DATE_FORMAT_DDMMYYYY);
			result.put("shopId", shopId);
			result.put("lstNVBH", lstNVBH);
			result.put("lstNVGH", lstNVGH);
			result.put("lockDate", fromDate);

		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
		}
		return JSON;
	}
	
	public String loadKeyShop() {
		try {
			if (orderId == null) {
				result.put("error", true);
				return JSON;
			}
			KeyShopFilter filter = new KeyShopFilter();
			filter.setSaleOrderId(orderId);
			result.put("lstKeyShop", keyShopMgr.getListKeyShopVOForViewOrderByFilter(filter));

		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
		}
		return JSON;
	}
	
	public List<StaffVO> getLstNVBH() {
		return lstNVBH;
	}

	public void setLstNVBH(List<StaffVO> lstNVBH) {
		this.lstNVBH = lstNVBH;
	}

	public List<StaffVO> getLstNVGH() {
		return lstNVGH;
	}

	public void setLstNVGH(List<StaffVO> lstNVGH) {
		this.lstNVGH = lstNVGH;
	}

	public Shop getCurrentShop() {
		return currentShop;
	}

	public void setCurrentShop(Shop currentShop) {
		this.currentShop = currentShop;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}

	public String getRefOrderNumber() {
		return refOrderNumber;
	}

	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getSaleStaff() {
		return saleStaff;
	}

	public void setSaleStaff(String saleStaff) {
		this.saleStaff = saleStaff;
	}

	public String getDeliveryStaff() {
		return deliveryStaff;
	}

	public void setDeliveryStaff(String deliveryStaff) {
		this.deliveryStaff = deliveryStaff;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Date getLockDay() {
		return lockDay;
	}

	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public List<SaleOrderDetailVOEx> getOrder_detail() {
		return order_detail;
	}

	public void setOrder_detail(List<SaleOrderDetailVOEx> order_detail) {
		this.order_detail = order_detail;
	}

	public List<SaleOrderDetailVOEx> getOrder_detail_promotion() {
		return order_detail_promotion;
	}

	public void setOrder_detail_promotion(
			List<SaleOrderDetailVOEx> order_detail_promotion) {
		this.order_detail_promotion = order_detail_promotion;
	}

	public List<StockTotalVO> getProductStocks() {
		return productStocks;
	}

	public void setProductStocks(List<StockTotalVO> productStocks) {
		this.productStocks = productStocks;
	}

	public List<StockTotalVO> getProductWarehouses() {
		return productWarehouses;
	}

	public void setProductWarehouses(List<StockTotalVO> productWarehouses) {
		this.productWarehouses = productWarehouses;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<OrderProductVO> getOrderProductVOs() {
		return orderProductVOs;
	}

	public void setOrderProductVOs(List<OrderProductVO> orderProductVOs) {
		this.orderProductVOs = orderProductVOs;
	}

	public List<WarehouseVO> getLstWarehouse() {
		return lstWarehouse;
	}

	public void setLstWarehouse(List<WarehouseVO> lstWarehouse) {
		this.lstWarehouse = lstWarehouse;
	}

	public Map<Long, List<StockTotalVO>> getMapProductByWarehouse() {
		return mapProductByWarehouse;
	}

	public void setMapProductByWarehouse(
			Map<Long, List<StockTotalVO>> mapProductByWarehouse) {
		this.mapProductByWarehouse = mapProductByWarehouse;
	}

	public String getFromSaleOrderNumber() {
		return fromSaleOrderNumber;
	}

	public void setFromSaleOrderNumber(String fromSaleOrderNumber) {
		this.fromSaleOrderNumber = fromSaleOrderNumber;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public int getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}

	public List<ApParam> getPriority() {
		return priority;
	}

	public void setPriority(List<ApParam> priority) {
		this.priority = priority;
	}

	public String getApParamCode() {
		return apParamCode;
	}

	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}

	public String getPriorityCode() {
		return priorityCode;
	}

	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	public StockTrans getStockTrans() {
		return stockTrans;
	}

	public void setStockTrans(StockTrans stockTrans) {
		this.stockTrans = stockTrans;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getLnk() {
		return lnk;
	}

	public void setLnk(String lnk) {
		this.lnk = lnk;
	}

	public String getToOwnerName() {
		return toOwnerName;
	}

	public void setToOwnerName(String toOwnerName) {
		this.toOwnerName = toOwnerName;
	}

	public String getFromOwnerName() {
		return fromOwnerName;
	}

	public void setFromOwnerName(String fromOwnerName) {
		this.fromOwnerName = fromOwnerName;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public Integer getApprovedStep() {
		return approvedStep;
	}

	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}

	public Integer getSaleOrderType() {
		return saleOrderType;
	}

	public void setSaleOrderType(Integer saleOrderType) {
		this.saleOrderType = saleOrderType;
	}

	public String getIsShowPrice() {
		return isShowPrice;
	}

	public void setIsShowPrice(String isShowPrice) {
		this.isShowPrice = isShowPrice;
	}

	public String getCurrentShopCode() {
		return currentShopCode;
	}

	public void setCurrentShopCode(String currentShopCode) {
		this.currentShopCode = currentShopCode;
	}

}

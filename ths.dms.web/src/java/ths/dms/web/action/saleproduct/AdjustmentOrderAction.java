package ths.dms.web.action.saleproduct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CarMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CommercialSupportType;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.vo.CommercialSupportCodeVO;
import ths.dms.core.entities.vo.CommercialSupportVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderPromotionVO;
import ths.dms.core.entities.vo.SaleProductVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class AdjustmentOrderAction.
 */
public class AdjustmentOrderAction extends AbstractAction {

	private static final long serialVersionUID = 6365121430359198947L;
	//Mgr
	private StockMgr stockMgr;
	private ProductMgr productMgr;
	private CustomerMgr customerMgr;
	private PromotionProgramMgr promotionProgramMgr;
	private SaleOrderMgr saleOrderMgr;
	//private ApParamMgr apParamMgr;
	///private StockManagerMgr stockManagerMgr;
	//List
	private List<Long> listCustomerForOrderCopy;
	private List<Car> lstCars;
	private List<ApParam> priority;
	private List<Staff> listNVBH;
	private List<Staff> listNVGH;
	private List<Staff> listNVTT;
	private List<SaleOrderDetail> listSaleOrderDetailOld;
	private List<SaleOrderDetail> listSaleOrderDetailNew;
	private List<SaleOrderDetailVOEx> order_detail;
	private List<SaleOrderDetailVOEx> order_detail_promotion;
	private List<SaleOrderLot> sale_order_lot;
	private List<StockTotalVO> productStocks;
	private List<Product> products;
	private List<OrderProductVO> listSaleProduct;
	private List<CommercialSupportVO> listCommercialSupport;
	private List<String> lstBuyProductCommercialSupportProgram;
	private List<String> lstBuyProductCommercialSupportProgramType;
	private List<String> lstProduct;
	private List<String> lstCommercialSupport;
	private List<String> lstPromotionCode;
	private List<String> lstPromotionProductSale;
	private List<String> lstPromotionProduct;
	private List<String> lstCommercialSupportType;
	private List<String> lstProductDestroy;
	private List<String> lstProductSaleLot;
	private List<String> lstPromotionProductSaleLot;
	private List<String> lstDiscount;
	private List<String> lstDisper;
	private List<String> lstPromotionProductQuatity;
	private List<String> lstPromotionProductMaxQuatity;
	private List<String> lstPromotionProductMaxQuatityCk;
	private List<String> lstMyTime;
	private List<Integer> lstPromotionType;
	private List<Integer> lstQuantity;
	//Entity
	private SaleOrder saleOrder;
	private Customer customer;
	private ApParam apParam;
	private Shop currentShop;
	//Integer
	private Integer canChangePromotionQuantity;
	private Integer isAuthorize; 
	private Integer allowEditPromotion;
	private Integer editOrderTablet;
	private Integer isVanSale;
	private Integer isApprovedVanSale;
	private int count;
	private int priorityId;
	//Long
	private Long productId;
	private Long saleOrderDetailId;
	private Long saleOrderId;
	private Long orderId;
	private Long carId;
	//String
	private String currentShopCode;
	private String customerCode;
	private String deliveryCode;
	private String deliveryDate;
	private String productCode;
	private String cashierCode;
	private String salerCode;
	private String totalAmountS;
	private String totalAmountVatS;
	private String totalGrossWeightS;
	private String errMsgToDay;
	private String errMsgLot;
	//Date
	private Date sysDate ;
	private Date lockDay;
	//Other
	private Float amountTotal;
	private BigDecimal amountMoney;
	
	private List<SaleOrderPromotionVO> quantityReceivedList;
	
	/**
	 * 
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockMgr = (StockMgr) context.getBean("stockMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");	
		//stockManagerMgr = (StockManagerMgr)context.getBean("stockManagerMgr");
		try {
			sysDate = commonMgr.getSysDate();
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null && staff.getShop() != null) {
					currentShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
					currentShopCode = currentShop.getShopCode();
					lockDay = shopLockMgr.getNextLockedDay(currentShop.getId());
					if(lockDay == null){
						lockDay = sysDate;
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e,"AdjustmentOrder.prepare");
		}
	}

	
	/**
	 * Recalculate available quantity.
	 */
	public void recalculateAvailableQuantity(){
		SaleOrder so=null;
		try {
			so = saleOrderMgr.getSaleOrderById(orderId);
			if(so==null)
				return;
			if(!SaleOrderStatus.APPROVED.equals(so.getApproved())){
				if(order_detail!=null){
					for(int i=0;i<order_detail.size();i++){
						int saq = order_detail.get(i).getStockQuantity();
						for(int k=0;k<order_detail.size();k++){
							if(order_detail.get(k).getProductId().equals(order_detail.get(i).getProductId())){
								saq+=order_detail.get(k).getQuantity();
							}
						}
						if(order_detail_promotion!=null){
							for(int j=0;j<order_detail_promotion.size();j++){
								if(order_detail_promotion.get(j).getProductId()!=null && order_detail_promotion.get(j).getProductId().equals(order_detail.get(i).getProductId())){
									saq+=order_detail_promotion.get(j).getQuantity();
								}
							}
						}
						order_detail.get(i).setStockQuantity(saq);
					}
				}
				if(order_detail_promotion!=null){
					for(int j=0;j<order_detail_promotion.size();j++){	
						
						
						if(order_detail_promotion.get(j).getProductId()!=null){
							int saq = order_detail_promotion.get(j).getStockQuantity();
							for(int i=0;i<order_detail.size();i++){
								if(order_detail.get(i).getProductId().equals(order_detail_promotion.get(j).getProductId())){
									saq+= order_detail.get(i).getQuantity();
								}
							}
							for(int k=0;k<order_detail_promotion.size();k++){
								if(order_detail_promotion.get(k).getProductId()!=null && order_detail_promotion.get(k).getProductId().equals(order_detail_promotion.get(j).getProductId())){
									saq+= order_detail_promotion.get(k).getQuantity();
								}
							}
							order_detail_promotion.get(j).setStockQuantity(saq);
						}
					}
				}
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, "AdjustmentOrderAction.recalculateAvailableQuantity - " + e.getMessage());
		}
	}

	

	/**
	 * Xoa don hang.
	 *
	 * @return the string
	 * @author tungmt
	 */
	public String removeOrder() {
		resetToken(result);

		errMsg = "";
		try {
			SaleOrder orderTemp = null;

			orderTemp = null;
			orderTemp = saleOrderMgr.getSaleOrderById(orderId);
			List<Long> listSaleOrder = new ArrayList<Long>();
			if (orderTemp != null && (SaleOrderStatus.NOT_YET_APPROVE.equals(orderTemp.getApproved()) || SaleOrderStatus.CANCEL.equals(orderTemp.getApproved())) && orderTemp.getFromSaleOrder() == null
					&& !orderTemp.getOrderType().equals(OrderType.CO) && !orderTemp.getOrderType().equals(OrderType.SO) && !orderTemp.getOrderType().equals(OrderType.TT)) {
				listSaleOrder.add(orderTemp.getId());
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_LAW, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.remove.is.error")));
				result.put(ERROR, true);
				return JSON;
			}
			if (DateUtil.compareDateWithoutTime(orderTemp.getOrderDate(), lockDay) != 0) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.error.not.valid.lockday.remove"));
				result.put(ERROR, true);
				return JSON;
			}
			saleOrderMgr.deleteListSaleOrderByListId(listSaleOrder, currentUser.getUserName(), getLogInfoVO()); //Bo sung ghi log 
			result.put("errMsg", "");
			result.put(ERROR, false);
			return JSON;
		} catch (BusinessException e) {
			LogUtility.logError(e, "AdjustmentOrder.removeOrder");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
	}
	
	/**
	 * Check quantity promotion.
	 *
	 * @param promotionCode the promotion code
	 * @param customer the customer
	 * @return the boolean
	 */
	public Boolean checkQuantityPromotion(String promotionCode, Customer customer, Date lockDay) {
		try {
			PromotionProgram promotion = promotionProgramMgr.getPromotionProgramByCode(promotionCode);
			if (promotion != null) {
				if ((promotion.getFromDate() != null && DateUtil.compareDateWithoutTime(promotion.getFromDate(), lockDay) == 1) || (promotion.getToDate() != null && DateUtil.compareDateWithoutTime(promotion.getToDate(), lockDay) == -1)) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION_SHOP, promotion.getPromotionProgramCode());
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					result.put("flag", true);
					return false;
				}
				PromotionShopMap promotionShopMap = promotionProgramMgr.checkPromotionShopMap(promotion.getId(), currentShop.getShopCode(), ActiveType.RUNNING, lockDay);
				if (promotionShopMap == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION_SHOP, promotion.getPromotionProgramCode());
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					result.put("flag", true);
					return false;
				}
				if (promotionShopMap.getQuantityReceived() != null && promotionShopMap.getQuantityMax() != null) {
					if (promotionShopMap.getQuantityMax() <= promotionShopMap.getQuantityReceived() - 1) {//-1 vi luc luu da tang len		
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION_SHOP_FULL, promotion.getPromotionProgramCode()));
						result.put(ERROR, true);
						return false;
					}
				}
				PromotionCustomerMap promotionCustomerMap = promotionProgramMgr.getPromotionCustomerMap(promotion.getId(), customer.getId());
				if (promotionCustomerMap != null && promotionCustomerMap.getQuantityReceived() != null && promotionCustomerMap.getQuantityMax() != null) {
					if (promotionCustomerMap.getQuantityMax() <= promotionCustomerMap.getQuantityReceived() - 1) {
						errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION_CUS_FULL, promotion.getPromotionProgramCode());
						result.put("errMsg", errMsg);
						result.put("flag", true);
						result.put(ERROR, true);
						return false;
					}
				}
			}

		} catch (BusinessException e) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			return false;
		}
		return true;
	}
	
	/**
	 * Check promotion product for currentShop.
	 *
	 * @param productCode the product code
	 * @param idShop the id currentShop
	 * @param quantity the quantity
	 * @return the boolean
	 */
	public Boolean checkPromotionProductForShop(String productCode,Long idShop,Integer quantity){//Kiem tra neu SP khong thuoc don vi ma quantity != 0 thi loi~
		StockTotal stockTotal;
		try {
			stockTotal = stockMgr.getStockTotalByProductCodeAndOwner(productCode,StockObjectType.SHOP,idShop);
			if(stockTotal!=null && quantity!=null){
				return false;
			}
			if(quantity>0 && quantity>stockTotal.getQuantity()){
				return false;
			}			
		} catch (BusinessException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Check promotion product for currentShop for approved.
	 *
	 * @param productCode the product code
	 * @param idShop the id currentShop
	 * @param quantity the quantity
	 * @return the boolean
	 */
	public Boolean checkPromotionProductForShopForApproved(String productCode,Long idShop,Integer quantity){//Kiem tra neu SP khong thuoc don vi ma quantity != 0 thi loi~
		StockTotal stockTotal;
		try {
			stockTotal = stockMgr.getStockTotalByProductCodeAndOwner(productCode,StockObjectType.SHOP,idShop);
			if(stockTotal==null && quantity!=null && quantity!=0){
				return false;
			}
			if(stockTotal!=null && quantity>stockTotal.getQuantity()){
				return false;
			}
			if(stockTotal==null && quantity==null){
				return false;
			}
		} catch (BusinessException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Check quantity.
	 *
	 * @param productCode the product code
	 * @param idShop the id currentShop
	 * @param quantity the quantity
	 * @return the boolean
	 */
	public Boolean checkQuantity(String productCode,Long idShop,Integer quantity){
		StockTotal stockTotal;
		try {
			stockTotal = stockMgr.getStockTotalByProductCodeAndOwner(productCode,StockObjectType.SHOP,idShop);
			if(stockTotal==null){
				if(quantity!=null && quantity!=0) return false;
				else if(quantity!=null && quantity==0) return true;
				else return false;
			}
			if(stockTotal.getQuantity()!=null && stockTotal.getQuantity()<0){
				return false;
			}
			if(quantity !=null && quantity!=0 && stockTotal.getQuantity()<quantity){
				return false;
			}
		} catch (BusinessException e) {
			return false;
		}
		return true;
	}

	/**
	 * Check order.
	 *
	 * @param idOrder the id order
	 * @return the boolean
	 */
	public Boolean checkOrder(Long idOrder){
		try{
			SaleOrder so=saleOrderMgr.getSaleOrderById(idOrder);
			if(so==null){
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.error.remove"));
				result.put(ERROR, true);
				return false;
			}else if(!SaleOrderStatus.NOT_YET_APPROVE.equals(so.getApproved())
							&& so.getFromSaleOrder() == null
							&& !so.getOrderType().equals(OrderType.CO)
							&& !so.getOrderType().equals(OrderType.SO)
							&& !so.getOrderType().equals(OrderType.TT)){
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.not.yet.approved.error"));
				result.put(ERROR, true);
				return false;
			}
			if(DateUtil.compareDateWithoutTime(so.getOrderDate(), lockDay)!=0){
				errMsgToDay += so.getOrderNumber()+" , ";
			}
			//SP ban
			ObjectVO<SaleOrderDetailVOEx> order_detail_vo = new ObjectVO<SaleOrderDetailVOEx>();
			order_detail_vo = saleOrderMgr.getListSaleOrderDetailVOEx(null, so.getId(), null, null, false, (so.getCustomer().getChannelType() == null ? 0L : so.getCustomer().getChannelType().getId()), currentShop.getId(), lockDay);
			//SP KM
			ObjectVO<SaleOrderDetailVOEx> order_detail_vo_free = new ObjectVO<SaleOrderDetailVOEx>();
			order_detail_vo_free = saleOrderMgr.getListSaleOrderDetailVOEx(null, so.getId(), 1, null, true, (so.getCustomer().getChannelType() == null ? 0L : so.getCustomer().getChannelType().getId()), currentShop.getId(), lockDay);

			//Kiem tra stock total
			for(SaleOrderDetailVOEx temp:order_detail_vo.getLstObject()){
				if(!checkQuantity(temp.getProductCode(), currentShop.getId(), temp.getQuantity())){
					result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.quantity.not.enough"));
					result.put(ERROR, true);
					return false;
				}
			}
			for(SaleOrderDetailVOEx temp:order_detail_vo_free.getLstObject()){
				//Kiem tra con suat KM cua KM hang ko
				if(temp.getProductCode()!=null){
					if(!checkQuantityPromotion(temp.getProgramCode(),so.getCustomer(),lockDay)){
						return false;
					}
					if(!checkQuantity(temp.getProductCode(), currentShop.getId(), temp.getQuantity())){
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.quantity.not.enough"));
						result.put(ERROR, true);
						return false;
					}
				}
			}
			//Kiem tra chon lo = tay va da chon du so luong san pham ban cho lo chua
		}catch(Exception ex){
			LogUtility.logError(ex, "AdjustmentOrderAction.checkorder");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return false;
		}
		return true;
	}
	
	public Boolean checkOrderVanSale(Long idOrder){
		try{
			SaleOrder so=saleOrderMgr.getSaleOrderById(idOrder);
			if(so==null){
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.error.remove"));
				result.put(ERROR, true);
				return false;
			}else if(!SaleOrderStatus.APPROVED.equals(so.getApproved())
							&& so.getApprovedVan() !=null &&  so.getApprovedVan().intValue() == 1
							&& so.getFromSaleOrder() == null
							&& !so.getOrderType().equals(OrderType.CM)
							&& !so.getOrderType().equals(OrderType.IN)
							&& !so.getOrderType().equals(OrderType.TT)){
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.not.yet.approved.error"));
				result.put(ERROR, true);
				return false;
			}
			if(DateUtil.compareDateWithoutTime(so.getOrderDate(), lockDay)!=0){
				errMsgToDay += so.getOrderNumber()+" , ";
			}
		
		}catch(Exception ex){
			LogUtility.logError(ex, "AdjustmentOrderAction.checkorder");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return false;
		}
		return true;
	}
	
	/**
	 * Duyet don hang vansale.
	 * @return the string
	 * @author phuongvm
	 */
	public String approvedOrderVanSale()
	{
		/*if(!checkRolesAndGetShop(VSARole.VNM_SALESONLINE_DISTRIBUTOR)){
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.permission")));
			return JSON;
		}*/
		errMsg = "";
		try {
			errMsgToDay="";
			errMsgLot="";
			List<Long> listSaleOrder=new ArrayList<Long>();
			if(!checkOrderVanSale(orderId)){//check don hang
				return ERROR;
			}else{
				listSaleOrder.add(orderId);
			}
			
			if(!StringUtil.isNullOrEmpty(errMsgToDay)){
				errMsgToDay = errMsgToDay.substring(0, errMsgToDay.length()-3);
				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_ORDER_NOT_IN_TODAY, errMsgToDay));
				result.put(ERROR, true);
				return ERROR;
			}
			
			//duyet don hang
//			saleOrderMgr.approveListSaleOrderByListSaleOrderId(listSaleOrder, "",staff.getStaffCode());
			SaleOrder tmpSaleOrder = saleOrderMgr.getSaleOrderById(listSaleOrder.get(0));
			
			result.put("newOrderNumber", tmpSaleOrder.getOrderNumber());
			result.put("errMsg", "");
			result.put(ERROR, false);
			return JSON;
		}catch(BusinessException be){
			LogUtility.logError(be, "AdjustmentOrder.approvedOrder");
			result.put(ERROR, true);
		}catch (Exception e) {
			LogUtility.logError(e, "AdjustmentOrder.approvedOrder");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return ERROR;
	}
	
	
	/**
	 * Check promotion product when save.
	 *
	 * @param lstPromoProductVO the lst promo product vo
	 * @return the boolean
	 */
	public Boolean checkPromotionProductWhenSave(List<SaleProductVO> lstPromoProductVO){
		String str="";
		for(SaleProductVO temp : lstPromoProductVO){
			if(temp.getSaleOrderDetail().getProduct()!=null){
				if(!checkPromotionProductForShop(temp.getSaleOrderDetail().getProduct().getProductCode(), currentShop.getId(), temp.getSaleOrderDetail().getQuantity())){
					str+= temp.getSaleOrderDetail().getProduct().getProductCode()+" , ";
				}
			}
		}
		if(!StringUtil.isNullOrEmpty(str)){
			str.substring(0, str.length()-3);
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PRODUCT_NOT_IN_STOCK_TOTAL_PROMOTION,str));
			return false;
		}
		return true;
	}
	
	/**
	 * Check promotion product when save for approved.
	 *
	 * @param lstPromoProductVO the lst promo product vo
	 * @return the boolean
	 */
	public Boolean checkPromotionProductWhenSaveForApproved(List<SaleProductVO> lstPromoProductVO){
		String str="";
		for(SaleProductVO temp : lstPromoProductVO){//Kiem tra co SPKM khong thuoc don vi ma co quantity != 0
			if(temp.getSaleOrderDetail().getProduct()!=null){
				if(!checkPromotionProductForShopForApproved(temp.getSaleOrderDetail().getProduct().getProductCode(), currentShop.getId(), temp.getSaleOrderDetail().getQuantity())){
					str+= temp.getSaleOrderDetail().getProduct().getProductCode()+" , ";
				}
			}
		}
		if(!StringUtil.isNullOrEmpty(str)){
			str.substring(0, str.length()-3);
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PRODUCT_NOT_IN_STOCK_TOTAL_PROMOTION,str));
			return false;
		}
		return true;
	}
	
	/**
	 * Creates the sale order.
	 *
	 * @param staff the staff
	 * @param staffObjectType the staff object type
	 * @return the string
	 * @author tungmt
	 */
	public Boolean checkStaff(Staff staff, StaffSpecificType staffType){
		if(staff==null){				
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,staffType.toString()));
			result.put(ERROR, true);
			return false;
		}	
		if (StaffSpecificType.STAFF.equals(staff.getStaffType().getSpecificType())) {
			
		}
		if(!staff.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){				
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS,staffType.toString()));
			result.put(ERROR, true);
			return false;
		}
		if(staff.getShop() == null || !staff.getShop().getId().equals(currentShop.getId())) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, staff.getStaffCode(), currentShop.getShopCode()));
			result.put(ERROR, true);
			return false;
		}
		if(staff.getStaffType() == null || (staff.getStaffType() != null && !staff.getStaffType().getSpecificType().equals(staffType))) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SS_NOT_GENERAL, staff.getStaffCode(), staffType.toString()));
			result.put(ERROR, true);
			return false;
		}
		return true;
	}
	
	
	/**
	 * Creates the sale order.
	 *
	 * @param staff the staff
	 * @param staffObjectType the staff object type
	 * @return the string
	 * @author tungmt
	 */
	public Boolean checkStaffForAdjust(Staff staff, StaffSpecificType staffType){
		if(staff==null){				
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,staffType.toString()));
			result.put(ERROR, true);
			return false;
		}	
		if (StaffSpecificType.STAFF.equals(staff.getStaffType().getSpecificType())) {
			if(!staff.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){				
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS,staffType.toString()));
				result.put(ERROR, true);
				return false;
			}
		}
		if(staff.getShop() == null || !staff.getShop().getId().equals(currentShop.getId())) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, staff.getStaffCode(), currentShop.getShopCode()));
			result.put(ERROR, true);
			return false;
		}
		if(staff.getStaffType() == null || (staff.getStaffType() != null && !staff.getStaffType().getSpecificType().equals(staffType))) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SS_NOT_GENERAL, staff.getStaffCode(), staffType.toString()));
			result.put(ERROR, true);
			return false;
		}
		return true;
	}
	
	/**
	 * Creates the sale order.
	 *
	 * @return the string
	 * @author tungmt
	 */
	public String editOrder(){
		try{			
			if (currentShop == null || currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null");
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}
			if(StringUtil.isNullOrEmpty(customerCode)){
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty");
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}
			customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(currentShop.getShopCode(), customerCode));
			if(customer==null){
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.null");
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}
			if(!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
				result.put(ERROR, true);				
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty")+ " - " + customerCode));
				return JSON;
			}
			
			SaleOrder saleOrder = saleOrderMgr.getSaleOrderById(orderId);
			if(saleOrder==null){
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sp.adjust.order.order.title"));
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}
			if (lockDay != null && saleOrder.getOrderDate()!= null
					&& DateUtil.compareDateWithoutTime(saleOrder.getOrderDate(),lockDay) != 0) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sp.search.sale.order.approved.error.not.valid.lockday.remove"));
				result.put(ERROR, true);
				return JSON;
			}
			CarMgr carMgr = (CarMgr)context.getBean("carMgr");
			Car car = null;
			if (carId != null) {
				car = carMgr.getCarById(carId);
				if(car==null && !SaleOrderSource.TABLET.equals(saleOrder.getOrderSource())){				
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sp.create.order.car.error.not.found")));
					result.put(ERROR, true);
					return JSON;
				}
				if(car!=null && !car.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){				
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sp.create.order.car.error.not.found")));
					result.put(ERROR, true);
					return JSON;
				}
				if(car!=null && (car.getShop() == null || !car.getShop().getId().equals(currentShop.getId()))) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_CAR_NOT_BELONG_SHOP));
					result.put(ERROR, true);
					return JSON;
				}
			}
			
			/** Thong tin nhan vien giao hang */
			Staff deliveryStaff = staffMgr.getStaffByCodeAndShopId(deliveryCode,currentShop.getId());
//			if(!checkStaff(deliveryStaff,StaffObjectType.NVGH)){
//					return JSON;
//			}
			/** Thong tin nhan vien thu tien*/
			Staff cashierStaff = staffMgr.getStaffByCodeAndShopId(cashierCode,currentShop.getId());
//			if(!checkStaff(cashierStaff,StaffObjectType.NVTT)){
//				return JSON;
//			}
			/** Thong tin nhan vien ban hang */
			Staff salerStaff = saleOrder.getStaff();//staffMgr.getStaffByCode(salerCode);
			if(!checkStaffForAdjust(salerStaff,StaffSpecificType.STAFF)){
				return JSON;
			}
			
			saleOrder.setCar(car);
			saleOrder.setDelivery(deliveryStaff);
			saleOrder.setCashier(cashierStaff);
			saleOrder.setUpdateUser(currentUser.getUserName());
			saleOrder.setUpdateDate(DateUtil.now());
			if(priorityId!=0) {
				saleOrder.setPriority(priorityId);
			}
			if(!StringUtil.isNullOrEmpty(deliveryDate)){
				Date dateTemp = ths.dms.web.utils.DateUtil.parse(deliveryDate, ConstantManager.FULL_DATE_FORMAT);
				if(dateTemp!=null)
					saleOrder.setDeliveryDate(dateTemp);
			}
			
			// Kiem tra su ton tai va hieu luc cua chuong trinh ho tro thuong mai	
			//Bao gom CTKM bang tay,huy doi tra hang, ct trung bay
			List<CommercialSupportCodeVO> lstCommercialSupportCodeVOs = new ArrayList<CommercialSupportCodeVO>();
			List<Boolean> isExists = new ArrayList<Boolean>();
			CommercialSupportCodeVO vos;
			if(lstCommercialSupport!=null){
				String TYPE = "";
				for(int i=0;i<lstCommercialSupport.size();++i){
					if(!StringUtil.isNullOrEmpty(lstCommercialSupport.get(i))){
						vos = new CommercialSupportCodeVO();						
						vos.setCode(lstCommercialSupport.get(i));
						vos.setCustomerId(customer.getId());
						vos.setShopId(currentShop.getId());						
						TYPE = lstCommercialSupportType.get(i);							
						if(StringUtil.isNullOrEmpty(TYPE)){
							vos.setType(CommercialSupportType.DISPLAY_PROGRAM);
						}else{
							vos.setType(CommercialSupportType.PROMOTION_PROGRAM);
						}
						Product product = productMgr.getProductByCode(lstProduct.get(i));
						vos.setProductId(product.getId());
						lstCommercialSupportCodeVOs.add(vos);
					}					
				}
				if (lstCommercialSupportCodeVOs.size() > 0) {
					isExists = saleOrderMgr.checkIfCommercialSupportExists(lstCommercialSupportCodeVOs, lockDay,orderId);
				}			
				for (int i=0;i<isExists.size();++i) {
					if (!isExists.get(i)) {						
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION,	lstCommercialSupport.get(i)));			
						return JSON;					
					}
				}
			}
			
			/**Kiem tra don hang trong tuyen va ngoai tuyen
			Tim duoc nhan vien bang hang thi co nghia la trong tuyen IS_VISIT_PLAN =1, 
			nguoc lai thi ngoai tuyen IS_VISIT_PLAN =0*/
			Staff sStaff = null;
			RoutingCustomerFilter filter = new RoutingCustomerFilter();
			filter.setCustomerId(customer.getId());
			filter.setShopId(currentShop.getId());
			filter.setOrderDate(saleOrder.getOrderDate());
			filter.setUserId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			List<Staff> lst = staffMgr.getSaleStaffByDate(filter);
			if (lst != null && lst.size() > 0) {
				sStaff = lst.get(0);
			}
			if(sStaff!=null){
				saleOrder.setIsVisitPlan(1);
			}else{
				saleOrder.setIsVisitPlan(0);
			}			
			//Danh sach san pham ban
			List<SaleProductVO> lstSaleProductVO = new ArrayList<SaleProductVO>();
			Map<String, Integer> indexOfSaleProduct = new HashMap<String, Integer>();
			//Danh sach san pham khuyen mai			
			List<SaleProductVO> lstPromoProductVO = new ArrayList<SaleProductVO>();
			//Danh sach san pham  huy
			List<SaleOrderLot> lstSaleOrderLot = new ArrayList<SaleOrderLot>();
			
			Product product;
			SaleOrderDetail saleOrderDetail = null;			
			SaleProductVO productVO = null;
			SaleOrderLot saleOrderLot = null;
			/**
			Lay thong tin danh sach san pham ban
			Tu do lay danh sach cac san pham khuyen mai duoc ap dung cho don hang
			Luu thong tin bao gom hoa don va cac thong tin ve san pham ban, san pham khuyen mai*/
						
			StockTotal stockTotal = null;	
			PromotionProgram program = null;
			/** Danh sach CT HTTTM cua NPP */
			ObjectVO<CommercialSupportVO> commercials = saleOrderMgr.getListCommercialSupport(null, currentShop.getId(), customer.getId(), lockDay,orderId);
			String commercialType = "";				
			for (int i=0;i<lstProduct.size();++i) {
				productVO = new SaleProductVO();
				lstSaleOrderLot = new ArrayList<SaleOrderLot>();
				product = productMgr.getProductByCode(lstProduct.get(i));				
				saleOrderDetail = new SaleOrderDetail();
				saleOrderDetail.setSaleOrder(saleOrder);
				saleOrderDetail.setProduct(product);//san pham ban
				saleOrderDetail.setPrice(productMgr.getPriceByProductId(product.getId()));//Gia san pham
				saleOrderDetail.setPriceNotVat(saleOrderDetail.getPrice().getPriceNotVat());
				saleOrderDetail.setPriceValue(saleOrderDetail.getPrice().getPrice());
				saleOrderDetail.setShop(currentShop);//Nha phan phoi	
				saleOrderDetail.setVat(saleOrderDetail.getPrice().getVat());
				saleOrderDetail.setStaff(salerStaff);
				
				//+ Kiem tra so luong nhap va so luong ton kho dap ung
				if(lstQuantity.get(i)==null || lstQuantity.get(i)<0){
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
					result.put(ERROR, true);
					return JSON;
				}				
				saleOrderDetail.setQuantity(lstQuantity.get(i));//so luong ban		
				saleOrderDetail.setOrderDate(saleOrder.getOrderDate()); // ngay lap hoa don
				saleOrderDetail.setUpdateDate(DateUtil.now());//ngay cap nhat
				saleOrderDetail.setCreateUser(currentUser.getUserName().toUpperCase());//nguoi tao
				saleOrderDetail.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(lstQuantity.get(i))));
				
				// Danh sach san pham ban
				if(lstCommercialSupport!=null && StringUtil.isNullOrEmpty(lstCommercialSupport.get(i))){	
					saleOrderDetail.setIsFreeItem(0); 
					// 10-03-2014 - nhanlt6: fix bug 0000155 luu cac CTHTTM cua san pham mua
					if (lstBuyProductCommercialSupportProgram!= null && !StringUtil.isNullOrEmpty(lstBuyProductCommercialSupportProgram.get(i))) {
						saleOrderDetail.setProgramCode(lstBuyProductCommercialSupportProgram.get(i));
						if (!StringUtil.isNullOrEmpty(lstBuyProductCommercialSupportProgramType.get(i))) {
							if ("ZV".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								if(saleOrderDetail.getProduct() != null && saleOrderDetail.getProduct().getCat() != null && "Z".equals(saleOrderDetail.getProduct().getCat().getProductInfoCode())) {
									result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.z.product.error", saleOrderDetail.getProduct().getProductCode() + " - " + saleOrderDetail.getProduct().getProductName()));
									result.put(ERROR, true);
									return JSON;
								}
								saleOrderDetail.setProgramType(ProgramType.AUTO_PROM);
							} else if ("ZM".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.MANUAL_PROM);
							} else if ("ZH".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.DESTROY);
							} else if ("ZD".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.PRODUCT_EXCHANGE);
							} else if ("ZT".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.RETURN);
							} else if ("DISPLAY_SCORE".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.DISPLAY_SCORE);
							}
						}
					} else if(saleOrderDetail.getProduct() != null && saleOrderDetail.getProduct().getCat() != null && "Z".equals(saleOrderDetail.getProduct().getCat().getProductInfoCode())) {
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.z.product.error", saleOrderDetail.getProduct().getProductCode() + " - " + saleOrderDetail.getProduct().getProductName()));
						result.put(ERROR, true);
						return JSON;
					}
					String detail = lstProductSaleLot.get(i);
					if(!StringUtil.isNullOrEmpty(detail)){
						String detailProductLot = detail.split(";")[0];
						product = productMgr.getProductByCode(detailProductLot);
						if(product != null && product.getCheckLot()==1){
							Price price = productMgr.getPriceByProductId(product.getId());
							for(int j =1;j<detail.split(";").length;++j){
								saleOrderLot = new SaleOrderLot();
								saleOrderLot.setProduct(product);	
								if(price!=null){
									saleOrderLot.setPriceValue(price.getPrice());																					
								}
								String lotDetail = detail.split(";")[j];
								if(!StringUtil.isNullOrEmpty(lotDetail)){
									String lot = lotDetail.split(",")[0];
									String avaiable_quantity = lotDetail.split(",")[1];
									List<ProductLot> productLots = productMgr.getProductLotByProductAndOwner(product.getId(),currentShop.getId(), StockObjectType.SHOP);
									if(productLots==null || productLots.size()==0){										
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.lot") + "  " + product.getProductCode() + " - " + product.getProductName()));
										result.put(ERROR, true);
										return JSON;										
									}
									for(ProductLot productLot:productLots){
										if(productLot.getLot().equals(lot)){
											if(Integer.valueOf(avaiable_quantity)>productLot.getQuantity()){												
												result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_SALE,product.getProductCode() +" - "+ product.getProductName()));
												result.put(ERROR, true);
												return JSON;
											}
											saleOrderLot.setLot(lot);
											saleOrderLot.setQuantity(Integer.parseInt(avaiable_quantity));
											saleOrderLot.setExpirationDate(productLot.getExpiryDate());
										}
										
									}
									if(saleOrderLot.getLot()==null ||saleOrderLot.getQuantity()==null || saleOrderLot.getExpirationDate()==null ){
										result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_DESTROY2,product.getProductCode() + " - " + product.getProductName()));
										result.put(ERROR, true);
										return JSON;										
									}
									saleOrderLot.setCreateUser(currentUser.getUserName());									
									saleOrderLot.setShop(currentShop);
									saleOrderLot.setStaff(salerStaff);									
									saleOrderLot.setOrderDate(saleOrder.getOrderDate());
									saleOrderLot.setSaleOrder(saleOrder);
									saleOrderLot.setSaleOrderDetail(saleOrderDetail);									
									lstSaleOrderLot.add(saleOrderLot);
								}
							}
							if(lstSaleOrderLot.size()>0){
								productVO.setLstSaleOrderLot(lstSaleOrderLot);									
							}
						}
						
					}
				}else{
					saleOrderDetail.setIsFreeItem(1); 
					saleOrderDetail.setAmount(BigDecimal.ZERO);
					saleOrderDetail.setProgramCode(lstCommercialSupport.get(i));	
					commercialType = lstCommercialSupportType.get(i);	
					ProgramType type = null;
					if(commercialType.startsWith("ZM")){
						type = ProgramType.MANUAL_PROM;
					}else if(commercialType.startsWith("ZD")){
						type = ProgramType.PRODUCT_EXCHANGE;
					}else if(commercialType.startsWith("ZH")){
						type = ProgramType.DESTROY;
					}else if(commercialType.startsWith("ZT")){
						type = ProgramType.RETURN;
					}else{
						type = ProgramType.DISPLAY_SCORE;
					}
					saleOrderDetail.setProgramType(type);
					saleOrderDetail.setProgrameTypeCode(commercialType);
					if(commercials!=null){						
						for(CommercialSupportVO cs :commercials.getLstObject()){
							if(StringUtil.isNullOrEmpty(cs.getCode()) && StringUtil.isNullOrEmpty(cs.getPromotionType())){
								continue;
							}
							if(lstCommercialSupport.get(i).equals(cs.getCode())&& ((cs.getPromotionType() == null && StringUtil.isNullOrEmpty(commercialType)) || cs.getPromotionType().equals(commercialType))){	
								/** HTTM: HUY HANG va HANG co quan ly theo lo. Thuc hien tach lo cho viec huy hang */
								if(cs.getPromotionType() == null || !cs.getPromotionType().startsWith("ZH")){
									String detail = lstProductDestroy.get(i);
									if(!StringUtil.isNullOrEmpty(detail)){
										String detailProductLot = detail.split(";")[0];
										product = productMgr.getProductByCode(detailProductLot);
										if(product != null && product.getCheckLot()==1){
											Price price = productMgr.getPriceByProductId(product.getId());
											for(int j =1;j<detail.split(";").length;++j){
												saleOrderLot = new SaleOrderLot();
												saleOrderLot.setProduct(product);	
												if(price!=null){
													saleOrderLot.setPriceValue(price.getPrice());																					
												}
												String lotDetail = detail.split(";")[j];
												if(!StringUtil.isNullOrEmpty(lotDetail)){
													String lot = lotDetail.split(",")[0];
													String avaiable_quantity = lotDetail.split(",")[1];
													List<ProductLot> productLots = productMgr.getProductLotByProductAndOwner(product.getId(),currentShop.getId(), StockObjectType.SHOP);
													if(productLots==null || productLots.size()==0){														
														result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.lot") + "  " + product.getProductCode() + " - " + product.getProductName()));
														result.put(ERROR, true);
														return JSON;										
													}
													for(ProductLot productLot:productLots){
														if(productLot.getLot().equals(lot)){
															if(Integer.valueOf(avaiable_quantity)>productLot.getQuantity()){																 
																result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_DESTROY,product.getProductCode() +" - "+ product.getProductName()));
																result.put(ERROR, true);
																return JSON;
															}
															saleOrderLot.setLot(lot);
															saleOrderLot.setQuantity(Integer.parseInt(avaiable_quantity));
															saleOrderLot.setExpirationDate(productLot.getExpiryDate());
														}
														
													}
													if(saleOrderLot.getLot()==null ||saleOrderLot.getQuantity()==null || saleOrderLot.getExpirationDate()==null ){
														result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_DESTROY2,product.getProductCode() + " - " + product.getProductName()));
														result.put(ERROR, true);
														return JSON;										
													}
													saleOrderLot.setCreateUser(currentUser.getUserName());									
													saleOrderLot.setShop(currentShop);
													saleOrderLot.setStaff(salerStaff);									
													saleOrderLot.setOrderDate(saleOrder.getOrderDate());
													saleOrderLot.setSaleOrder(saleOrder);
													saleOrderLot.setSaleOrderDetail(saleOrderDetail);									
													lstSaleOrderLot.add(saleOrderLot);
													
												}
											}
											if(lstSaleOrderLot.size()>0){
												productVO.setLstSaleOrderLot(lstSaleOrderLot);									
											}
										}
									}
									continue;
								}
								String detail = lstProductDestroy.get(i);	
								if(StringUtil.isNullOrEmpty(detail)){
									continue;
								}																
								String detailProductLot = detail.split(";")[0];
								product = productMgr.getProductByCode(detailProductLot);
								if(product==null){
									continue;
								}
								if(product.getCheckLot()==0){
									continue;
								}
								//Kiem tra so lo nhap va tong chi tiet lo
								Price price = productMgr.getPriceByProductId(product.getId());
								//Tach lo cho san pham huy
								for(int j =1;j<detail.split(";").length;++j){
									saleOrderLot = new SaleOrderLot();
									saleOrderLot.setProduct(product);											
									if(price!=null){
										saleOrderLot.setPriceValue(price.getPrice());																					
									}
									String lotDetail = detail.split(";")[j];
									if(StringUtil.isNullOrEmpty(lotDetail)){
										continue;
									}
									String lot = lotDetail.split(",")[0];
									String avaiable_quantity = lotDetail.split(",")[1];
									//Kiem tra thong tin lo
									List<ProductLot> productLots = productMgr.getProductLotByProductAndOwnerEx(product.getId(),currentShop.getId(), StockObjectType.SHOP);
									Integer countSOLot = saleOrderMgr.sumQuatitySaleOrderLotByCondition(orderId, saleOrderDetailId, lot);
									if(productLots==null || productLots.size()==0){										
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.lot") + "  " + product.getProductCode() + " - " + product.getProductName()));
										result.put(ERROR, true);
										return JSON;										
									}
									for(ProductLot productLot:productLots){
										if(productLot.getLot().equals(lot)){
											if(Integer.valueOf(avaiable_quantity)>(productLot.getQuantity() + countSOLot)){												
												result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_DESTROY,product.getProductCode() +" - "+ product.getProductName()));
												result.put(ERROR, true);
												return JSON;
											}
											saleOrderLot.setLot(lot);
											saleOrderLot.setQuantity(Integer.parseInt(avaiable_quantity));
											saleOrderLot.setExpirationDate(productLot.getExpiryDate());
										}
									}
									if(saleOrderLot.getLot()==null ||saleOrderLot.getQuantity()==null || saleOrderLot.getExpirationDate()==null ){
										result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_DESTROY2,product.getProductCode() + " - " + product.getProductName()));
										result.put(ERROR, true);
										return JSON;										
									}
									saleOrderLot.setCreateUser(currentUser.getUserName());									
									saleOrderLot.setShop(currentShop);
									saleOrderLot.setStaff(salerStaff);									
									saleOrderLot.setOrderDate(saleOrder.getOrderDate());
									saleOrderLot.setSaleOrder(saleOrder);
									saleOrderLot.setSaleOrderDetail(saleOrderDetail);									
									lstSaleOrderLot.add(saleOrderLot);
								}
								if(lstSaleOrderLot.size()>0){
									productVO.setLstSaleOrderLot(lstSaleOrderLot);									
								}
								break;
							}
						}
					}
				}
				productVO.setSaleOrderDetail(saleOrderDetail);					
				lstSaleProductVO.add(productVO);
				indexOfSaleProduct.put(product.getProductCode(), lstSaleProductVO.indexOf(productVO));
			}			
			/** Danh sach san pham khuyen mai tu dong */			
			if(lstPromotionCode!=null){				
				for(int i=0;i<lstPromotionCode.size();++i){
					saleOrderDetail = new SaleOrderDetail();
					program = promotionProgramMgr.getPromotionProgramByCode(lstPromotionCode.get(i));
					product = productMgr.getProductByCode(lstPromotionProduct.get(i)); 
//					boolean flag = false;
					if(!checkQuantityPromotion(lstPromotionCode.get(i),saleOrder.getCustomer(),lockDay)){
						return JSON;
					}/*else{
						flag=true;
					}*/
//					if(flag){						
						SaleProductVO productPromotionVO = new SaleProductVO();						
						saleOrderDetail = new SaleOrderDetail();
						saleOrderDetail.setStaff(salerStaff);
						saleOrderDetail.setShop(currentShop);
						saleOrderDetail.setOrderDate(saleOrder.getOrderDate()); 
						saleOrderDetail.setCreateDate(sysDate);
						saleOrderDetail.setCreateUser(currentUser.getUserName());
						saleOrderDetail.setIsFreeItem(1);
						saleOrderDetail.setProgramType(ProgramType.AUTO_PROM);
						saleOrderDetail.setProgramCode(program.getPromotionProgramCode());
						saleOrderDetail.setProgrameTypeCode(program.getType());
						/** Khuyen mai san pham */
						if(lstPromotionType.get(i)==SaleOrderDetailVO.FREE_PRODUCT){
							if(!StringUtil.isNullOrEmpty(lstPromotionProduct.get(i))){								
								product = productMgr.getProductByCode(lstPromotionProduct.get(i)); 
								saleOrderDetail.setProduct(product);								
								saleOrderDetail.setPrice(productMgr.getPriceByProductId(product.getId()));
								saleOrderDetail.setPriceNotVat(saleOrderDetail.getPrice().getPriceNotVat());
								saleOrderDetail.setPriceValue(saleOrderDetail.getPrice().getPrice());
								Integer promotionQty = Integer.valueOf(lstPromotionProductQuatity.get(i));																	
								Integer promotionMaxQty = Integer.valueOf(lstPromotionProductMaxQuatity.get(i));
//								Integer promotionMaxQtyCk = Integer.valueOf(lstPromotionProductMaxQuatityCk.get(i));
//								BigDecimal myTime = new BigDecimal(lstMyTime.get(i));
								saleOrderDetail.setMaxQuantityFree(promotionMaxQty);
//								saleOrderDetail.setMaxQuantityFreeCk(promotionMaxQtyCk);
//								saleOrderDetail.setMyTime(myTime);
								/** Kiem tra ton kho */
								stockTotal = stockMgr.getStockTotalByProductCodeAndOwner(product.getProductCode(), StockObjectType.SHOP, currentShop.getId());
								if(promotionQty!=0 && stockTotal==null){
									result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_QUANTITY_STOCK_TT_PROMOTION2,product.getProductCode(),product.getProductName()));
									result.put(ERROR, true);
									return JSON;
								} else if(stockTotal!=null && stockTotal.getQuantity()<promotionQty){					
									result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_QUANTITY_STOCK_TT_PROMOTION,product.getProductCode(),product.getProductName()));
									result.put(ERROR, true);
									return JSON;
								}
								saleOrderDetail.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(promotionMaxQty)));
								saleOrderDetail.setQuantity(promotionQty);
							}
							
							
							/** Lot SPKM */
						}
						/** KM tien */ 
						if(lstPromotionType.get(i)==SaleOrderDetailVO.FREE_PRICE){	
							saleOrderDetail.setDiscountPercent(null);
							saleOrderDetail.setDiscountAmount(new BigDecimal(lstDiscount.get(i)));
						}
						/** KM % tien  */
						if(lstPromotionType.get(i)==SaleOrderDetailVO.FREE_PERCENT){
							saleOrderDetail.setDiscountPercent(Float.valueOf(lstDisper.get(i)));
							saleOrderDetail.setDiscountAmount(new BigDecimal(lstDiscount.get(i)));
						}
						
						/**TUNGTT: Kiem tra neu nhap so luong KM = 0 thi khong luu xuong DB*/
						/**KM Hang*/
						if(lstPromotionType.get(i)==1){
							if(saleOrderDetail.getQuantity() > 0){
								productPromotionVO.setSaleOrderDetail(saleOrderDetail);
								lstPromoProductVO.add(productPromotionVO);
							}
						}else /** KM tien */ 
							if(lstPromotionType.get(i)==SaleOrderDetailVO.FREE_PRICE){
								if(saleOrderDetail.getDiscountAmount().compareTo(BigDecimal.ZERO) > 0){
									productPromotionVO.setSaleOrderDetail(saleOrderDetail);
									lstPromoProductVO.add(productPromotionVO);
								}
							
						}else /** KM % tien  */
							if(lstPromotionType.get(i)==SaleOrderDetailVO.FREE_PERCENT){
								if(saleOrderDetail.getDiscountPercent() > 0){
									productPromotionVO.setSaleOrderDetail(saleOrderDetail);
									lstPromoProductVO.add(productPromotionVO);
								}
						}
						
					/*}else{
						errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION_CUS_FULL,program.getPromotionProgramCode());
						result.put("isRePayment", true);
						result.put("errMsg", errMsg);							
						result.put(ERROR, true);
						return JSON;
					}*/
				}
			}		
			/** Kiem tra co SPKM khong thuoc don vi ma co quantity != 0 
			if(!checkPromotionProductWhenSave(lstPromoProductVO)){
				return JSON;
			}*/
			/*
			 * TODO: comment doan code gay loi compile, chuc nang nao su dung thi vao day chinh sua code
			 * @modified by tuannd20
			 * @date 15/12/2014
			 */
			//saleOrderMgr.modifySaleOrder(saleOrder, lstSaleProductVO, lstPromoProductVO,null,quantityReceivedList);
			result.put(ERROR, false);
			result.put("flag", true);
		}catch(BusinessException be){
			LogUtility.logError(be, "AdjustmentOrder.approvedOrder");
			result.put(ERROR, true);
			if(be.getMessage().equals("LOT_QUANTITY_IS_NOT_ENOUGH")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.quantity.not.enough"));
			}else if(be.getMessage().equals("RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.received.larger.than.max.quantity"));
			}else if(be.getMessage().equals("TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.total.larger.max.debit"));
			}else if(be.getMessage().equals("MAX_DAY_DEBIT")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.max.debit.day"));
			}else if(be.getMessage().equals("QUANTITY_IS_NEGATIVE")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.saved.quantity.not.enough"));
			}else{
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}			
		}catch(Exception ex){
			LogUtility.logError(ex, "AdjustmentOrder.editOrder");
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put("errMsg", errMsg);
			result.put(ERROR, true);			
		}
		return JSON;
	}

	
	/**
	 * Gets the staff.
	 * 
	 * @return the staff
	 */
	public Staff getStaff() {
		return staff;
	}

	/**
	 * Sets the staff.
	 * 
	 * @param staff
	 *            the new staff
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	/**
	 * Gets the currentShop.
	 * 
	 * @return the currentShop
	 */
	public Shop getShop() {
		return currentShop;
	}

	/**
	 * Sets the currentShop.
	 * 
	 * @param currentShop
	 *            the new currentShop
	 */
	public void setShop(Shop currentShop) {
		this.currentShop = currentShop;
	}

	/**
	 * Gets the currentShop code.
	 * 
	 * @return the currentShop code
	 */
	public String getShopCode() {
		return currentShopCode;
	}

	/**
	 * Sets the currentShop code.
	 * 
	 * @param currentShopCode
	 *            the new currentShop code
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * Gets the lst cars.
	 * 
	 * @return the lst cars
	 */
	public List<Car> getLstCars() {
		return lstCars;
	}

	/**
	 * Sets the lst cars.
	 * 
	 * @param lstCars
	 *            the new lst cars
	 */
	public void setLstCars(List<Car> lstCars) {
		this.lstCars = lstCars;
	}

	/**
	 * Gets the order id.
	 * 
	 * @return the order id
	 */
	public Long getOrderId() {
		return orderId;
	}

	/**
	 * Sets the order id.
	 * 
	 * @param orderId
	 *            the new order id
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	/**
	 * Gets the sale order.
	 * 
	 * @return the sale order
	 */
	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	/**
	 * Sets the sale order.
	 * 
	 * @param saleOrder
	 *            the new sale order
	 */
	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	/**
	 * Gets the order_detail.
	 *
	 * @return the order_detail
	 */
	public List<SaleOrderDetailVOEx> getOrder_detail() {
		return order_detail;
	}

	/**
	 * Sets the order_detail.
	 *
	 * @param order_detail the new order_detail
	 */
	public void setOrder_detail(List<SaleOrderDetailVOEx> order_detail) {
		this.order_detail = order_detail;
	}

	/**
	 * Gets the amount total.
	 *
	 * @return the amount total
	 */
	public Float getAmountTotal() {
		return amountTotal;
	}

	/**
	 * Sets the amount total.
	 *
	 * @param amountTotal the new amount total
	 */
	public void setAmountTotal(Float amountTotal) {
		this.amountTotal = amountTotal;
	}

	/**
	 * Gets the amount money.
	 *
	 * @return the amount money
	 */
	public BigDecimal getAmountMoney() {
		return amountMoney;
	}

	/**
	 * Sets the amount money.
	 *
	 * @param amountMoney the new amount money
	 */
	public void setAmountMoney(BigDecimal amountMoney) {
		this.amountMoney = amountMoney;
	}

	/**
	 * Gets the customer code.
	 *
	 * @return the customer code
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * Sets the customer code.
	 *
	 * @param customerCode the new customer code
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer the new customer
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * Gets the car id.
	 *
	 * @return the car id
	 */
	public Long getCarId() {
		return carId;
	}

	/**
	 * Sets the car id.
	 *
	 * @param carId the new car id
	 */
	public void setCarId(Long carId) {
		this.carId = carId;
	}

	/**
	 * Gets the delivery code.
	 *
	 * @return the delivery code
	 */
	public String getDeliveryCode() {
		return deliveryCode;
	}

	/**
	 * Sets the delivery code.
	 *
	 * @param deliveryCode the new delivery code
	 */
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	/**
	 * Gets the cashier code.
	 *
	 * @return the cashier code
	 */
	public String getCashierCode() {
		return cashierCode;
	}

	/**
	 * Sets the cashier code.
	 *
	 * @param cashierCode the new cashier code
	 */
	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}

	/**
	 * Gets the saler code.
	 *
	 * @return the saler code
	 */
	public String getSalerCode() {
		return salerCode;
	}

	/**
	 * Sets the saler code.
	 *
	 * @param salerCode the new saler code
	 */
	public void setSalerCode(String salerCode) {
		this.salerCode = salerCode;
	}

	/**
	 * Gets the lst product.
	 *
	 * @return the lst product
	 */
	public List<String> getLstProduct() {
		return lstProduct;
	}

	/**
	 * Sets the lst product.
	 *
	 * @param lstProduct the new lst product
	 */
	public void setLstProduct(List<String> lstProduct) {
		this.lstProduct = lstProduct;
	}

	/**
	 * Gets the lst quantity.
	 *
	 * @return the lst quantity
	 */
	public List<Integer> getLstQuantity() {
		return lstQuantity;
	}

	/**
	 * Sets the lst quantity.
	 *
	 * @param lstQuantity the new lst quantity
	 */
	public void setLstQuantity(List<Integer> lstQuantity) {
		this.lstQuantity = lstQuantity;
	}

	/**
	 * Gets the lst commercial support.
	 *
	 * @return the lst commercial support
	 */
	public List<String> getLstCommercialSupport() {
		return lstCommercialSupport;
	}

	/**
	 * Sets the lst commercial support.
	 *
	 * @param lstCommercialSupport the new lst commercial support
	 */
	public void setLstCommercialSupport(List<String> lstCommercialSupport) {
		this.lstCommercialSupport = lstCommercialSupport;
	}

	/**
	 * Gets the list sale order detail old.
	 *
	 * @return the list sale order detail old
	 */
	public List<SaleOrderDetail> getListSaleOrderDetailOld() {
		return listSaleOrderDetailOld;
	}

	/**
	 * Sets the list sale order detail old.
	 *
	 * @param listSaleOrderDetailOld the new list sale order detail old
	 */
	public void setListSaleOrderDetailOld(
			List<SaleOrderDetail> listSaleOrderDetailOld) {
		this.listSaleOrderDetailOld = listSaleOrderDetailOld;
	}

	/**
	 * Gets the list sale order detail new.
	 *
	 * @return the list sale order detail new
	 */
	public List<SaleOrderDetail> getListSaleOrderDetailNew() {
		return listSaleOrderDetailNew;
	}

	/**
	 * Sets the list sale order detail new.
	 *
	 * @param listSaleOrderDetailNew the new list sale order detail new
	 */
	public void setListSaleOrderDetailNew(
			List<SaleOrderDetail> listSaleOrderDetailNew) {
		this.listSaleOrderDetailNew = listSaleOrderDetailNew;
	}

	/**
	 * Gets the order_detail_promotion.
	 *
	 * @return the order_detail_promotion
	 */
	public List<SaleOrderDetailVOEx> getOrder_detail_promotion() {
		return order_detail_promotion;
	}

	/**
	 * Sets the order_detail_promotion.
	 *
	 * @param order_detail_promotion the new order_detail_promotion
	 */
	public void setOrder_detail_promotion(
			List<SaleOrderDetailVOEx> order_detail_promotion) {
		this.order_detail_promotion = order_detail_promotion;
	}

	/**
	 * Gets the priority.
	 *
	 * @return the priority
	 */
	public List<ApParam> getPriority() {
		return priority;
	}

	/**
	 * Sets the priority.
	 *
	 * @param priority the new priority
	 */
	public void setPriority(List<ApParam> priority) {
		this.priority = priority;
	}

	/**
	 * Gets the lst promotion code.
	 *
	 * @return the lst promotion code
	 */
	public List<String> getLstPromotionCode() {
		return lstPromotionCode;
	}

	/**
	 * Sets the lst promotion code.
	 *
	 * @param lstPromotionCode the new lst promotion code
	 */
	public void setLstPromotionCode(List<String> lstPromotionCode) {
		this.lstPromotionCode = lstPromotionCode;
	}

	/**
	 * Gets the lst promotion product sale.
	 *
	 * @return the lst promotion product sale
	 */
	public List<String> getLstPromotionProductSale() {
		return lstPromotionProductSale;
	}

	/**
	 * Sets the lst promotion product sale.
	 *
	 * @param lstPromotionProductSale the new lst promotion product sale
	 */
	public void setLstPromotionProductSale(List<String> lstPromotionProductSale) {
		this.lstPromotionProductSale = lstPromotionProductSale;
	}

	/**
	 * Gets the lst promotion product.
	 *
	 * @return the lst promotion product
	 */
	public List<String> getLstPromotionProduct() {
		return lstPromotionProduct;
	}

	/**
	 * Sets the lst promotion product.
	 *
	 * @param lstPromotionProduct the new lst promotion product
	 */
	public void setLstPromotionProduct(List<String> lstPromotionProduct) {
		this.lstPromotionProduct = lstPromotionProduct;
	}

	/**
	 * Gets the priority id.
	 *
	 * @return the priority id
	 */
	public int getPriorityId() {
		return priorityId;
	}

	/**
	 * Sets the priority id.
	 *
	 * @param priorityId the new priority id
	 */
	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}

	/**
	 * Gets the delivery date.
	 *
	 * @return the delivery date
	 */
	public String getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * Sets the delivery date.
	 *
	 * @param deliveryDate the new delivery date
	 */
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * Sets the product id.
	 *
	 * @param productId the new product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * Sets the count.
	 *
	 * @param count the new count
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * Gets the lst commercial support type.
	 *
	 * @return the lst commercial support type
	 */
	public List<String> getLstCommercialSupportType() {
		return lstCommercialSupportType;
	}

	/**
	 * Sets the lst commercial support type.
	 *
	 * @param lstCommercialSupportType the new lst commercial support type
	 */
	public void setLstCommercialSupportType(List<String> lstCommercialSupportType) {
		this.lstCommercialSupportType = lstCommercialSupportType;
	}

	/**
	 * Gets the lst product destroy.
	 *
	 * @return the lst product destroy
	 */
	public List<String> getLstProductDestroy() {
		return lstProductDestroy;
	}

	/**
	 * Sets the lst product destroy.
	 *
	 * @param lstProductDestroy the new lst product destroy
	 */
	public void setLstProductDestroy(List<String> lstProductDestroy) {
		this.lstProductDestroy = lstProductDestroy;
	}

	/**
	 * Gets the sale order detail id.
	 *
	 * @return the sale order detail id
	 */
	public Long getSaleOrderDetailId() {
		return saleOrderDetailId;
	}

	/**
	 * Sets the sale order detail id.
	 *
	 * @param saleOrderDetailId the new sale order detail id
	 */
	public void setSaleOrderDetailId(Long saleOrderDetailId) {
		this.saleOrderDetailId = saleOrderDetailId;
	}

	/**
	 * Gets the lst promotion type.
	 *
	 * @return the lst promotion type
	 */
	public List<Integer> getLstPromotionType() {
		return lstPromotionType;
	}

	/**
	 * Sets the lst promotion type.
	 *
	 * @param lstPromotionType the new lst promotion type
	 */
	public void setLstPromotionType(List<Integer> lstPromotionType) {
		this.lstPromotionType = lstPromotionType;
	}

	/**
	 * Gets the lst discount.
	 *
	 * @return the lst discount
	 */
	public List<String> getLstDiscount() {
		return lstDiscount;
	}

	/**
	 * Sets the lst discount.
	 *
	 * @param lstDiscount the new lst discount
	 */
	public void setLstDiscount(List<String> lstDiscount) {
		this.lstDiscount = lstDiscount;
	}

	/**
	 * Gets the lst disper.
	 *
	 * @return the lst disper
	 */
	public List<String> getLstDisper() {
		return lstDisper;
	}

	/**
	 * Sets the lst disper.
	 *
	 * @param lstDisper the new lst disper
	 */
	public void setLstDisper(List<String> lstDisper) {
		this.lstDisper = lstDisper;
	}

	/**
	 * Gets the lst promotion product quatity.
	 *
	 * @return the lst promotion product quatity
	 */
	public List<String> getLstPromotionProductQuatity() {
		return lstPromotionProductQuatity;
	}

	/**
	 * Sets the lst promotion product quatity.
	 *
	 * @param lstPromotionProductQuatity the new lst promotion product quatity
	 */
	public void setLstPromotionProductQuatity(
			List<String> lstPromotionProductQuatity) {
		this.lstPromotionProductQuatity = lstPromotionProductQuatity;
	}

	/**
	 * Gets the total amount s.
	 *
	 * @return the total amount s
	 */
	public String getTotalAmountS() {
		return totalAmountS;
	}

	/**
	 * Sets the total amount s.
	 *
	 * @param totalAmountS the new total amount s
	 */
	public void setTotalAmountS(String totalAmountS) {
		this.totalAmountS = totalAmountS;
	}

	/**
	 * Gets the total gross weight s.
	 *
	 * @return the total gross weight s
	 */
	public String getTotalGrossWeightS() {
		return totalGrossWeightS;
	}

	/**
	 * Sets the total gross weight s.
	 *
	 * @param totalGrossWeightS the new total gross weight s
	 */
	public void setTotalGrossWeightS(String totalGrossWeightS) {
		this.totalGrossWeightS = totalGrossWeightS;
	}

	/**
	 * Gets the total amount vat s.
	 *
	 * @return the total amount vat s
	 */
	public String getTotalAmountVatS() {
		return totalAmountVatS;
	}

	/**
	 * Sets the total amount vat s.
	 *
	 * @param totalAmountVatS the new total amount vat s
	 */
	public void setTotalAmountVatS(String totalAmountVatS) {
		this.totalAmountVatS = totalAmountVatS;
	}

	/**
	 * Gets the lst product sale lot.
	 *
	 * @return the lst product sale lot
	 */
	public List<String> getLstProductSaleLot() {
		return lstProductSaleLot;
	}

	/**
	 * Sets the lst product sale lot.
	 *
	 * @param lstProductSaleLot the new lst product sale lot
	 */
	public void setLstProductSaleLot(List<String> lstProductSaleLot) {
		this.lstProductSaleLot = lstProductSaleLot;
	}

	/**
	 * Gets the lst promotion product sale lot.
	 *
	 * @return the lst promotion product sale lot
	 */
	public List<String> getLstPromotionProductSaleLot() {
		return lstPromotionProductSaleLot;
	}

	/**
	 * Sets the lst promotion product sale lot.
	 *
	 * @param lstPromotionProductSaleLot the new lst promotion product sale lot
	 */
	public void setLstPromotionProductSaleLot(List<String> lstPromotionProductSaleLot) {
		this.lstPromotionProductSaleLot = lstPromotionProductSaleLot;
	}

	/**
	 * Gets the err msg to day.
	 *
	 * @return the err msg to day
	 */
	public String getErrMsgToDay() {
		return errMsgToDay;
	}

	/**
	 * Sets the err msg to day.
	 *
	 * @param errMsgToDay the new err msg to day
	 */
	public void setErrMsgToDay(String errMsgToDay) {
		this.errMsgToDay = errMsgToDay;
	}

	/**
	 * Gets the err msg lot.
	 *
	 * @return the err msg lot
	 */
	public String getErrMsgLot() {
		return errMsgLot;
	}

	/**
	 * Sets the err msg lot.
	 *
	 * @param errMsgLot the new err msg lot
	 */
	public void setErrMsgLot(String errMsgLot) {
		this.errMsgLot = errMsgLot;
	}

	/**
	 * Gets the ap param.
	 *
	 * @return the ap param
	 */
	public ApParam getApParam() {
		return apParam;
	}

	/**
	 * Sets the ap param.
	 *
	 * @param apParam the new ap param
	 */
	public void setApParam(ApParam apParam) {
		this.apParam = apParam;
	}

	/**
	 * Gets the lst promotion product max quatity.
	 *
	 * @return the lst promotion product max quatity
	 */
	public List<String> getLstPromotionProductMaxQuatity() {
		return lstPromotionProductMaxQuatity;
	}

	/**
	 * Sets the lst promotion product max quatity.
	 *
	 * @param lstPromotionProductMaxQuatity the new lst promotion product max quatity
	 */
	public void setLstPromotionProductMaxQuatity(
			List<String> lstPromotionProductMaxQuatity) {
		this.lstPromotionProductMaxQuatity = lstPromotionProductMaxQuatity;
	}

	/**
	 * Gets the can change promotion quantity.
	 *
	 * @return the can change promotion quantity
	 */
	public Integer getCanChangePromotionQuantity() {
		return canChangePromotionQuantity;
	}

	/**
	 * Sets the can change promotion quantity.
	 *
	 * @param canChangePromotionQuantity the new can change promotion quantity
	 */
	public void setCanChangePromotionQuantity(Integer canChangePromotionQuantity) {
		this.canChangePromotionQuantity = canChangePromotionQuantity;
	}

	/**
	 * Gets the list nvbh.
	 *
	 * @return the list nvbh
	 */
	public List<Staff> getListNVBH() {
		return listNVBH;
	}

	/**
	 * Sets the list nvbh.
	 *
	 * @param listNVBH the new list nvbh
	 */
	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}

	/**
	 * Gets the list nvgh.
	 *
	 * @return the list nvgh
	 */
	public List<Staff> getListNVGH() {
		return listNVGH;
	}

	/**
	 * Sets the list nvgh.
	 *
	 * @param listNVGH the new list nvgh
	 */
	public void setListNVGH(List<Staff> listNVGH) {
		this.listNVGH = listNVGH;
	}

	/**
	 * Gets the list nvtt.
	 *
	 * @return the list nvtt
	 */
	public List<Staff> getListNVTT() {
		return listNVTT;
	}

	/**
	 * Sets the list nvtt.
	 *
	 * @param listNVTT the new list nvtt
	 */
	public void setListNVTT(List<Staff> listNVTT) {
		this.listNVTT = listNVTT;
	}

	/**
	 * Gets the list sale product.
	 *
	 * @return the list sale product
	 */
	public List<OrderProductVO> getListSaleProduct() {
		return listSaleProduct;
	}

	/**
	 * Sets the list sale product.
	 *
	 * @param listSaleProduct the new list sale product
	 */
	public void setListSaleProduct(List<OrderProductVO> listSaleProduct) {
		this.listSaleProduct = listSaleProduct;
	}

	/**
	 * Gets the list commercial support.
	 *
	 * @return the list commercial support
	 */
	public List<CommercialSupportVO> getListCommercialSupport() {
		return listCommercialSupport;
	}

	/**
	 * Sets the list commercial support.
	 *
	 * @param listCommercialSupport the new list commercial support
	 */
	public void setListCommercialSupport(
			List<CommercialSupportVO> listCommercialSupport) {
		this.listCommercialSupport = listCommercialSupport;
	}

	/**
	 * Gets the checks if is authorize.
	 *
	 * @return the checks if is authorize
	 */
	public Integer getIsAuthorize() {
		return isAuthorize;
	}

	/**
	 * Sets the checks if is authorize.
	 *
	 * @param isAuthorize the new checks if is authorize
	 */
	public void setIsAuthorize(Integer isAuthorize) {
		this.isAuthorize = isAuthorize;
	}

	/**
	 * Gets the allow edit promotion.
	 *
	 * @return the allow edit promotion
	 */
	public Integer getAllowEditPromotion() {
		return allowEditPromotion;
	}

	/**
	 * Sets the allow edit promotion.
	 *
	 * @param allowEditPromotion the new allow edit promotion
	 */
	public void setAllowEditPromotion(Integer allowEditPromotion) {
		this.allowEditPromotion = allowEditPromotion;
	}

	/**
	 * Gets the sale_order_lot.
	 *
	 * @return the sale_order_lot
	 */
	public List<SaleOrderLot> getSale_order_lot() {
		return sale_order_lot;
	}

	/**
	 * Sets the sale_order_lot.
	 *
	 * @param sale_order_lot the new sale_order_lot
	 */
	public void setSale_order_lot(List<SaleOrderLot> sale_order_lot) {
		this.sale_order_lot = sale_order_lot;
	}

	public List<StockTotalVO> getProductStocks() {
		return productStocks;
	}

	public void setProductStocks(List<StockTotalVO> productStocks) {
		this.productStocks = productStocks;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Integer getEditOrderTablet() {
		return editOrderTablet;
	}

	public void setEditOrderTablet(Integer editOrderTablet) {
		this.editOrderTablet = editOrderTablet;
	}

	public List<String> getLstBuyProductCommercialSupportProgram() {
		return lstBuyProductCommercialSupportProgram;
	}

	public void setLstBuyProductCommercialSupportProgram(
			List<String> lstBuyProductCommercialSupportProgram) {
		this.lstBuyProductCommercialSupportProgram = lstBuyProductCommercialSupportProgram;
	}

	public List<String> getLstBuyProductCommercialSupportProgramType() {
		return lstBuyProductCommercialSupportProgramType;
	}

	public void setLstBuyProductCommercialSupportProgramType(
			List<String> lstBuyProductCommercialSupportProgramType) {
		this.lstBuyProductCommercialSupportProgramType = lstBuyProductCommercialSupportProgramType;
	}

	public List<Long> getListCustomerForOrderCopy() {
		return listCustomerForOrderCopy;
	}

	public void setListCustomerForOrderCopy(List<Long> listCustomerForOrderCopy) {
		this.listCustomerForOrderCopy = listCustomerForOrderCopy;
	}

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public Date getLockDay() {
		return lockDay;
	}

	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}

	public Integer getIsVanSale() {
		return isVanSale;
	}

	public void setIsVanSale(Integer isVanSale) {
		this.isVanSale = isVanSale;
	}

	public Integer getIsApprovedVanSale() {
		return isApprovedVanSale;
	}

	public void setIsApprovedVanSale(Integer isApprovedVanSale) {
		this.isApprovedVanSale = isApprovedVanSale;
	}
	
	public List<String> getLstPromotionProductMaxQuatityCk() {
		return lstPromotionProductMaxQuatityCk;
	}

	public void setLstPromotionProductMaxQuatityCk(
			List<String> lstPromotionProductMaxQuatityCk) {
		this.lstPromotionProductMaxQuatityCk = lstPromotionProductMaxQuatityCk;
	}

	public List<String> getLstMyTime() {
		return lstMyTime;
	}

	public void setLstMyTime(List<String> lstMyTime) {
		this.lstMyTime = lstMyTime;
	}


	public List<SaleOrderPromotionVO> getQuantityReceivedList() {
		return quantityReceivedList;
	}


	public void setQuantityReceivedList(
			List<SaleOrderPromotionVO> quantityReceivedList) {
		this.quantityReceivedList = quantityReceivedList;
	}
	
}

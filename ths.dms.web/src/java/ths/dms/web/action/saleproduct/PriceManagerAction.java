package ths.dms.web.action.saleproduct;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.NumberUtil;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopTypeMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopType;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.PriceFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PriceVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

public class PriceManagerAction extends AbstractAction {
	private static final long serialVersionUID = -2128120975032582873L;

	private ProductMgr productMgr;
	private ProductInfoMgr productInfoMgr;
	//private ChannelTypeMgr channelTypeMgr;
	private CustomerMgr customerMgr;	
	private ShopTypeMgr shopTypeMgr;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		productMgr = (ProductMgr) context.getBean("productMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		//channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		shopTypeMgr = (ShopTypeMgr) context.getBean("shopTypeMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			shopVO = new ShopVO();
			if (currentUser.getShopRoot() != null) {
				shopVO.setShopId(currentUser.getShopRoot().getShopId());
				shopVO.setShopCode(currentUser.getShopRoot().getShopCode());
				shopVO.setShopName(currentUser.getShopRoot().getShopName());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Trang Quan Ly Gia
	 * 
	 * @author vuongbd
	 * @since 08/05/2015
	 * @return Info HTML
	 * */
	public String infoManager() {
		resetToken(result);
		try {
			lstShopVO = new ArrayList<ShopVO>();
			lstChannelType = new ArrayList<ChannelType>();
			lstShopType = new ArrayList<ShopType>();
			
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.CUSTOMER);
			filter.setStatus(ActiveType.RUNNING);
			filter.setIsOrderByChannelTypeName(true);
			lstChannelType = channelTypeMgr.getListChannelType(filter, null).getLstObject();

			List<Integer> lstStatus = new ArrayList<Integer>();
			lstStatus.add(ActiveType.RUNNING.getValue());
			lstShopType = shopTypeMgr.getListShopTypeByType(lstStatus);
			
			shopChannel = currentUser.getShopRoot().getShopChannel();
			
			shopVO = new ShopVO();
			if (currentUser.getShopRoot() != null) {
				shopVO.setShopId(currentUser.getShopRoot().getShopId());
				shopVO.setShopCode(currentUser.getShopRoot().getShopCode());
				shopVO.setShopName(currentUser.getShopRoot().getShopName());
			}

			//Lay danh sach nganh hang
			lstProductInfo = new ArrayList<ProductInfo>();
			BasicFilter<ProductInfo> filterPi = new BasicFilter<ProductInfo>();
			//filterPi.setTextG(StringUtil.PRODUCT_INFOR_SUBTRACT_Z.trim());
			filterPi.setType(ProductType.CAT.getValue());
			lstProductInfo = productInfoMgr.getListProductInfoByFilter(filterPi).getLstObject();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem gia san pham voi nhieu tham bien
	 * 
	 * @author vuongbd
	 * @since 12/05/2015
	 * @description Search Price by Filter
	 * */
	public String searchPriceProduct() {
		
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<PriceVO>());
			PriceFilter<PriceVO> filter = new PriceFilter<PriceVO>();
			KPaging<PriceVO> kPaging = new KPaging<PriceVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			
			//Ma san pham			
			if (!StringUtil.isNullOrEmpty(productCode)) {
				filter.setProductCode(productCode);
			}
			//Ten san pham
			if (!StringUtil.isNullOrEmpty(productName)) {
				filter.setProductName(productName);
			}
			//Ngach hang
			if (catId != null && catId > 0) {
				filter.setCatId(catId);
			}
			//Loai Don vi
			if (shopTypeId != null && shopTypeId > 0) {
				filter.setShopTypeId(shopTypeId);
			}
			//Ma Don vi
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(shopCode);
			}
			//Loai Khach hang
			if (customerTypeId != null && customerTypeId > 0) {
				filter.setCustomerTypeId(customerTypeId);
			}
			//Ma Khach Hang
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				filter.setCustomerCode(customerCode);
			}
			//From date - to date
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			//Status
			if (status != null && status > -1) {
				filter.setStatus(status);
			}
			
			ObjectVO<PriceVO> data = productMgr.searchPriceVOByFilter(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	private String createTemplateExportFile() {
		String importTemplateFileAbsolutePath = null;
		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		XSSFWorkbook workbook = null;
		OPCPackage opcPackage = null;
		try {			
			final String resourceDirectoryPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathPrice();
			final String sourceTemplateFileName = ConstantManager.TEMPLATE_PRICE_PRODUCT_EXPORT;
			final String sourceTemplateFileAbsoluteName = resourceDirectoryPath + File.separator + sourceTemplateFileName;
			
			String importTemplateFileName = ConstantManager.PRICE_EXPORT_EXCEL + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
			importTemplateFileAbsolutePath = Configuration.getStoreRealPath() + importTemplateFileName;
			FileUtility.copyFile(sourceTemplateFileAbsoluteName, importTemplateFileAbsolutePath);
			
			fileInputStream = new FileInputStream(importTemplateFileAbsolutePath);
			opcPackage = OPCPackage.open(fileInputStream);
			workbook = new XSSFWorkbook(opcPackage);

			int IMPORT_SHEET_INDEX = 0;
			XSSFSheet importSheet = workbook.getSheetAt(IMPORT_SHEET_INDEX);
			CreationHelper factory = workbook.getCreationHelper();
			if (importSheet != null) {
				ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
				
				final int HEADER_ROW_INDEX = 0;
				final int HEADER_ROW_INDEX_1 = 1;
				int writingColumnIndex = 0;
				int writingColumnIndexAt1 = 0;
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.object"));
				writingColumnIndex += 4;
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX_1, writingColumnIndexAt1++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column1"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX_1, writingColumnIndexAt1++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column2"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX_1, writingColumnIndexAt1++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column3"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX_1, writingColumnIndexAt1++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column4"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column5"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column6"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column7"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column8"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column13"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column14"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column9"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column10"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column11"));
				excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price_grid_column12"));
				
				XSSFFont commentFont = (XSSFFont) workbook.createFont();
				ExcelPOIProcessUtils.setFontPOI(commentFont, "Times New Roman", 9, false, ExcelPOIProcessUtils.poiBlack);
				excelProcessUtil.writeCellComment(factory, importSheet, HEADER_ROW_INDEX, writingColumnIndex++, 
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.status.comment"), commentFont);
			}
			fileOutputStream = new FileOutputStream(importTemplateFileAbsolutePath);
			workbook.write(fileOutputStream);
			fileOutputStream.close();

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		} finally {
			IOUtils.closeQuietly(fileInputStream);
			IOUtils.closeQuietly(fileOutputStream);
			IOUtils.closeQuietly(opcPackage);
		}

		return importTemplateFileAbsolutePath;
	}

	/**
	 * Xuat Excel theo Tim kiem gia san pham voi nhieu tham bien
	 * 
	 * @author vuongd
	 * @since 13/05/2015
	 * @description Export Excel By Search Price With Filter
	 * */
	public String exportPriceProduct() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			PriceFilter<PriceVO> filter = new PriceFilter<PriceVO>();
			
			//Ma san pham			
			if (!StringUtil.isNullOrEmpty(productCode)) {
				filter.setProductCode(productCode);
			}
			//Ten san pham
			if (!StringUtil.isNullOrEmpty(productName)) {
				filter.setProductName(productName);
			}
			//Ngach hang
			if (catId != null && catId > 0) {
				filter.setCatId(catId);
			}
			//Loai Don vi
			if (shopTypeId != null && shopTypeId > 0) {
				filter.setShopTypeId(shopTypeId);
			}
			//Ma Don vi
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(shopCode);
			}
			//Loai Khach hang
			if (customerTypeId != null && customerTypeId > 0) {
				filter.setCustomerTypeId(customerTypeId);
			}
			//Ma Khach Hang
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				filter.setCustomerCode(customerCode);
			}
			//From date - to date
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			//Status
			if (status != null && status > -1) {
				filter.setStatus(status);
			}
			
			//Status multi language
			String status_active = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.status.active");
			String status_stopped = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.status.stopped");
			String status_waitting = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"jsp.common.status.draft");
			
			ObjectVO<PriceVO> data = productMgr.searchPriceVOByFilter(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstPriceVO", data.getLstObject());
				
				for(PriceVO priceV : data.getLstObject()) {
					
					priceV.setStatusStr(status_stopped);
					if (priceV.getStatus()!=null && priceV.getStatus()==1) {
						priceV.setStatusStr(status_active);
					} else if (priceV.getStatus()!=null && priceV.getStatus()==2) {
						priceV.setStatusStr(status_waitting);
					}
				}
				String templateFileName = createTemplateExportFile();
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = Paths.get(templateFileName).getFileName().toString(); //ConstantManager.PRICE_EXPORT_EXCEL + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
				
				InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				inputStream.close();
				OutputStream os = new BufferedOutputStream(new FileOutputStream(templateFileName));
				resultWorkbook.write(os);
				os.flush();
				os.close();
				result.put(ERROR, false);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put("path", outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.export.isnull"));
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Tam ngung gia
	 * 
	 * @author vuongbd fix
	 * @since 13/05/2015
	 * @return JSON
	 * @description Ham chi cap nhat : den ngay = ngay hien tai, trang thai tam ngung, UPDATE_DATE, UPDATE_USER
	 * */
	public String stopPrice() {
		resetToken(result);
		errMsg = "";
		String msg = "";
		try {

			if (id == null || id < 0) {
				result.put(ERROR, true);
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "category.not.exist");
				result.put("errMsg", msg);
				return JSON;
			}
			Price price = new Price();
			price = productMgr.getPriceById(id);
			if (price != null && ActiveType.RUNNING.getValue().equals(price.getStatus().getValue())) {
				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(currentUser.getUserName());
				 productMgr.stopPrice(price, logInfo);
				
			} else if (price == null) {
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "category.not.exist");
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}


	/**
	 * Import Excel Price
	 * 
	 * @author vuongbd
	 * @since 06/05/2015
	 * @description Import Excel Price
	 * */
	public String importPriceProduct() {
		//IMPORT EXCEL - THEM MOI GIA
		resetToken(result);
		
		totalItem = 0;
		
		String message = "";
		String value = "";
		Integer status = -1;
		
		List<Integer> statusInsert = new ArrayList<Integer>();
		statusInsert.add(ActiveType.RUNNING.getValue());
		statusInsert.add(ActiveType.WAITING.getValue());
		List<Integer> statusUpdate = new ArrayList<Integer>();
		statusUpdate.add(ActiveType.RUNNING.getValue());
		
		try {			
			lstView = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 9);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(currentUser.getUserName());
				
				for (int i = 1, sz = lstData.size(); i < sz; i++) {	
					ShopType shopType = null;
					Shop shop = null;
					ChannelType channelType = null;
					Customer customer = null;
					Product product = null;
					Price priceInsUpd = null;
					List<String> row = lstData.get(i);	
					
					if (row != null) {
						priceInsUpd = new Price();
						status = -1;		
						message = "";
						totalItem++;
						//Check loai don vi - don vi - loai khach hang - khach hang : NULL					
						if (StringUtil.isNullOrEmpty(row.get(0).trim()) && StringUtil.isNullOrEmpty(row.get(1).trim()) 
								&& StringUtil.isNullOrEmpty(row.get(2).trim()) && StringUtil.isNullOrEmpty(row.get(3).trim())) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.4.isnull");
							message += "\n";
							lstView.add(StringUtil.addFailBean(row, message));
							break;
						}
						
						//Check san pham, vat, gia dong goi vat, gia le vat, trang thai
						if (StringUtil.isNullOrEmpty(row.get(4).trim()) && StringUtil.isNullOrEmpty(row.get(5).trim()) 
								&& StringUtil.isNullOrEmpty(row.get(6).trim()) && StringUtil.isNullOrEmpty(row.get(7).trim()) 
								&& StringUtil.isNullOrEmpty(row.get(8).trim())) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.5.isnull");
							message += "\n";
							lstView.add(StringUtil.addFailBean(row, message));
							break;
						}
						
						//Check Loai Don Vi
						value = row.get(0).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							shopType = shopTypeMgr.getShopTypeByCode(value);
							if (shopType != null) {
								if (!ActiveType.RUNNING.equals(shopType.getStatus())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.shopType.isStop");
									message += "\n";
								} else {
									priceInsUpd.setShopType(shopType);								
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.ShopType.isUnDefined");
								message += "\n";
							}
						}
						
						//Check Ma Don Vi
						value = row.get(1).trim();
						String maDonVi = value;
						if (!StringUtil.isNullOrEmpty(value)) {
							shop = shopMgr.getShopByCode(value);
							if (shop != null && shop.getId() > 0) {
								if (!ActiveType.RUNNING.equals(shop.getStatus())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.isShop.isStop");
									message += "\n";
								} else {
									priceInsUpd.setShop(shop);
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.isShop.isUnDefined");
								message += "\n";
							}
						}
						
						//Check Loai Khach Hang
						value = row.get(2).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							channelType = channelTypeMgr.getChannelTypeByName(value, ChannelTypeType.CUSTOMER);
							if (channelType != null) {
								if (!ActiveType.RUNNING.equals(channelType.getStatus())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.customerType.isStop");
									message += "\n";
								} else {
									priceInsUpd.setCustomerTypeId(channelType);									
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.customerType.isUnDefined");
								message += "\n";
							}
						}
						
							
						//Check Ma Khach Hang
						value = row.get(3).trim();						
						if (!StringUtil.isNullOrEmpty(value)) {
							// vuongmq; 07/09/2015; check them ma KH phai co ma DV
							if (shop != null && shop.getId() > 0) {
								customer = customerMgr.getCustomerByCodeAndShop(value, shop.getId());
								if (customer != null && customer.getId() > 0) {
									if (!ActiveType.RUNNING.equals(customer.getStatus())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.iscustomer.isStop");
										message += "\n";
									} else {
										priceInsUpd.setCustomer(customer); // voi gia cua customer, chi luu Id cua customer
										priceInsUpd.setShop(null); 
									}
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.customer.isUnDefined");
									message += "\n";
								}
							} else {
								if (!StringUtil.isNullOrEmpty(maDonVi)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.isShop.isUnDefined");
									message += "\n";
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.iscustomer.isShop.is.require");
									message += "\n";
								}
							}
						}
						
						//Check Ma San Pham
						value = row.get(4).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							product = productMgr.getProductByCode(value);
							if (product != null && product.getId() > 0) {
								if (!ActiveType.RUNNING.equals(product.getStatus())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.product.isStop");
									message += "\n";
								} else {
									priceInsUpd.setProduct(product);
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.product.isundefined");
								message += "\n";
							}
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.product.isnull");
							message += "\n";
						}
						
						
						//Check Phan Tram VAT
						value = row.get(5).trim();
						if (StringUtil.isNullOrEmpty(value)) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.vat.isNull");
							message += "\n";
						} else {
							if (!StringUtil.isFloat(value)) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.vat.isOk");
								message += "\n";
							} else {
								Float vat = Float.valueOf(value);
								if (vat < 0 || vat > 100) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.vat.isOk");
									message += "\n";
								}
								priceInsUpd.setVat(StringUtil.precisionFloat(2, vat));
							}
						}
												
						//Check Gia dong goi VAT
						value = row.get(6).trim();
						if (StringUtil.isNullOrEmpty(value)) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.pricepackage.isNull");
							message += "\n";
						} else {
							value = value.replaceAll(",", "").trim();
							if (value.length() > 22 || !StringUtil.isNumberInt(value)) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.pricepackage.isOk");
								message += "\n";
							} else {
								BigDecimal price = new BigDecimal(value);
								if (price.compareTo(BigDecimal.ZERO) < 0) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.pricepackage.isOk");
									message += "\n";
								} else {
									//Xu ly tinh toan
									///Phep tinh vat: priceNotVat = price * 100 / (100 + vat)
									priceInsUpd.setPackagePrice(price);
								}
							}
						}
						
						//Check Gia Le VAT
						value = row.get(7).trim();
						if (StringUtil.isNullOrEmpty(value)) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isNull");
							message += "\n";
						} else {
							value = value.replaceAll(",", "").trim();
							if (value.length() > 22 || !StringUtil.isNumberInt(value)) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isOk");
								message += "\n";
							} else {
								BigDecimal price = new BigDecimal(value);
								if (price.compareTo(BigDecimal.ZERO) < 0) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isOk");
									message += "\n";
								} else {
									//Xu ly tinh toan
									///Phep tinh vat: priceNotVat = price * 100 / (100 + vat)
									priceInsUpd.setPrice(price);
								}
							}
						}
						
						//Check Trang Thai
						value = row.get(8).trim();
						if (StringUtil.isNullOrEmpty(value)) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.status.isNull");
							message += "\n";
						} else {
							status = NumberUtil.tryParseInteger(value);
							if(status==null || (status < 0 || status > 1)) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.status.isNumber");
								message += "\n";								
							} else {
								//Trang thai = 0
								if(status == ActiveType.STOPPED.getValue()) {
									priceInsUpd.setStatus(ActiveType.STOPPED);
								} else {
									priceInsUpd.setStatus(ActiveType.RUNNING);
								}
							}
						}
						
						//Xy ly ghi du lieu DB
						if (StringUtil.isNullOrEmpty(message.trim())) {
							if (ActiveType.RUNNING.getValue().equals(priceInsUpd.getStatus().getValue())) {
								
								message += this.isExistsPriceByParametter(priceInsUpd, statusInsert, 1);
								
								if (!StringUtil.isNullOrEmpty(message)) {
									lstView.add(StringUtil.addFailBean(row, message));
								} else {
									calculatPriceNotVAT(priceInsUpd);
									
									priceInsUpd.setFromDate(new Date());								
									priceInsUpd.setStatus(ActiveType.WAITING);
									productMgr.createPrice(priceInsUpd, logInfo);
								}								
							} else if (ActiveType.STOPPED.getValue().equals(priceInsUpd.getStatus().getValue())) {
								//Cap nhat gia san pham
								message += this.isExistsPriceByParametter(priceInsUpd, statusUpdate, 2);
								
								if (!StringUtil.isNullOrEmpty(message)) {
									lstView.add(StringUtil.addFailBean(row, message));
								} else {
									//calculatPriceNotVAT(priceInsUpd);
									//Xu ly cap nhat
									//priceInsUpd.setToDate(new Date());
									//vuongmq; 07/09/2015; off gia (giao dien + import) --> to_date = sysdate ==> sua lai to_date = sysdate - 1
									priceInsUpd.setToDate(DateUtil.addDate(DateUtil.now(), -1));
									priceInsUpd.setStatus(ActiveType.STOPPED);
									productMgr.updatePrice(priceInsUpd, logInfo);
								}
							}
						} else {
							lstView.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				
				if (lstView!=null && lstView.size() > 0) {
					//Export error
					getOutputFailExcelFile(lstView, ConstantManager.TEMPLATE_PRICE_PRODUCT_FAIL);	
				}			
			} else {
				isError = true;
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "not.data.excel");
			}
		}
		catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		
		return SUCCESS;
	}
	
	/**
	 * Them moi hoac cap nhat thong tin Gia cua san pham
	 * 
	 * @author hunglm16
	 * @since August 30,2014
	 * @description update in Price
	 * */
	public String addorUpdatePrice() {
		resetToken(result);
		errMsg = "";
		String msg = "";
		try {
			Price priceUpdate = new Price();
			if (id != null && id > 0) {
				priceUpdate = productMgr.getPriceById(id);
				if (priceUpdate == null) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.cms.isdefined.isnot");
				} else {
					if (!ActiveType.WAITING.getValue().equals(priceUpdate.getStatus().getValue())) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.update.iswarning");
					} else if (priceUpdate.getShop() != null && !checkShopInOrgAccessById(priceUpdate.getShop().getId())) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.cms.isdefined");
					}
				}
				vat = StringUtil.precisionFloat(2, vat);
				vat = vat/100;
				if (StringUtil.isNullOrEmpty(msg) && vat != null && (vat < 0 || vat > 100)) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.vat.isNot");
				}
				if (StringUtil.isNullOrEmpty(msg) && price != null && price.doubleValue() < 0) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.price.isNot");
				}
				if (StringUtil.isNullOrEmpty(msg) && priceNotVat != null && priceNotVat.doubleValue() < 0) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.priceNotVAT.isNot");
				}
				
				if (StringUtil.isNullOrEmpty(msg) && pricePackage != null && pricePackage.doubleValue() < 0) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.pricepackage.isNot");
				}
				if (StringUtil.isNullOrEmpty(msg) && pricePackageNotVat != null && pricePackageNotVat.doubleValue() < 0) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.pricepackageNotVAT.isNot");
				}
				
			} else {
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.cms.isdefined.isnot");
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			
			//Xu ly cap nhat
			priceUpdate.setVat(vat);
			
			if (price.doubleValue() == 0 || priceNotVat.doubleValue() == 0) {
				priceUpdate.setPrice(BigDecimal.ZERO);
				priceUpdate.setPriceNotVat(BigDecimal.ZERO);
			} else {
				priceUpdate.setPrice(price);
				Float priceNotVatIp = priceUpdate.getPrice().floatValue() * Float.valueOf(100);
				priceNotVatIp = priceNotVatIp / Float.valueOf(Float.valueOf(100) + priceUpdate.getVat());				
				priceUpdate.setPriceNotVat(StringUtil.precisionBigDecimal(0, BigDecimal.valueOf(priceNotVatIp)));
			}
			
			if (pricePackage.doubleValue() == 0 || pricePackageNotVat.doubleValue() == 0) {
				priceUpdate.setPackagePrice(BigDecimal.ZERO);
				priceUpdate.setPackagePriceNotVat(BigDecimal.ZERO);
			} else {
				priceUpdate.setPackagePrice(pricePackage);
				
				Float priceNotVatIp = priceUpdate.getPackagePrice().floatValue() * Float.valueOf(100);
				priceNotVatIp = priceNotVatIp / Float.valueOf(Float.valueOf(100) + priceUpdate.getVat());				
				priceUpdate.setPackagePriceNotVat(StringUtil.precisionBigDecimal(0, BigDecimal.valueOf(priceNotVatIp)));
			}
			
			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(currentUser.getUserName());
			productMgr.updatePrice(priceUpdate, logInfo);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	
	/**
	 * Cap nhat dong su kien Duyet hoac xoa Gia
	 * 
	 * @author hunglm16
	 * @since August 26,2014
	 * @return JSON
	 * */
	public String changeWaitingPrice() {
		resetToken(result);
		errMsg = "";
		String msg = "";
		try {
			if (StringUtil.isNullOrEmpty(textG) || isEvent == null || isEvent < 0 || isEvent > 2) {
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.change.waiting.arr.isnull");
			} else {
				String[] arrIdStr = textG.split(",");
				List<Long> lstId = new ArrayList<Long>();
				for (String value : arrIdStr) {
					if (StringUtil.isNumberInt(value)) {
						lstId.add(Long.valueOf(value));
					}
				}
				if (lstId != null && !lstId.isEmpty()) {
					PriceFilter<Price> filter = new PriceFilter<Price>();
					filter.setArrlongG(lstId);
					List<Price> lstData = productMgr.getListPriceByFilter(filter).getLstObject();
					if (lstData != null && !lstData.isEmpty()) {
						LogInfoVO logInfo = new LogInfoVO();
						logInfo.setStaffCode(currentUser.getUserName());
						productMgr.updateWaitingPriceManager(lstData, logInfo, isEvent);												
					}
				} else {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.change.waiting.arr.isnull");
				}
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	
	/**
	 * Ghi header sheet import
	 * 
	 * @author vuongbd
	 * @return String Duong dan tuyet doi toi file template tra ve client
	 * @throws IOException 
	 * @since 06/05/2015
	 */
	private String generatePriceTemplateImportFileFromOriginalFile() throws IOException {
		final String resourceDirectoryPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathPrice();
		final String sourceTemplateFileName = ConstantManager.IMPORT_PRICE_TEMPATE_FILE_NAME + FileExtension.XLSX.getValue();
		final String sourceTemplateFileAbsoluteName = resourceDirectoryPath + File.separator + sourceTemplateFileName;

		String importTemplateFileName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.file_name") + "_"
				+ DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
		final String importTemplateFileAbsolutePath = Configuration.getStoreRealPath() + File.separator + importTemplateFileName;
		FileUtility.copyFile(sourceTemplateFileAbsoluteName, importTemplateFileAbsolutePath);
		return importTemplateFileAbsolutePath;
	}
	
	/**
	 * Tao file template dung de import gia san pham
	 * 
	 * @author vuongbd
	 * @return Doi tuong chua duong dan toi file template
	 * @since 06/05/2015
	 */
	public String downloadImportPriceTemplateFile() {
		String reportToken = retrieveReportToken(reportCode);
		if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		String importTemplateFileName = this.createImportPriceTemplateFile();
		String outputPath = Configuration.getExportExcelPath() + importTemplateFileName;
		result.put(REPORT_PATH, outputPath);
		result.put(LIST, outputPath);
		result.put(ERROR, false);
		result.put("hasData", true);
		try {
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Tao file mau import gia san pham
	 * 
	 * @author vuongbd
	 * @return Ten file template moi duoc tao
	 * @since 06/05/2015
	 */
	private String createImportPriceTemplateFile() {
		String importTemplateFileName = null;

		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		XSSFWorkbook workbook = null;
		OPCPackage opcPackage = null;
		try {
			/*
			 * tao file template moi tu file template mau
			 */
			String importTemplateFileAbsolutePath = this.generatePriceTemplateImportFileFromOriginalFile();
			importTemplateFileName = Paths.get(importTemplateFileAbsolutePath).getFileName().toString();

			/*
			 * ghi du lieu
			 */
			fileInputStream = new FileInputStream(importTemplateFileAbsolutePath);
			opcPackage = OPCPackage.open(fileInputStream);
			workbook = new XSSFWorkbook(opcPackage);

			/*
			 * ghi header sheet import
			 */
			int IMPORT_SHEET_INDEX = 0;
			this.writePriceImportSheetHeader(workbook, IMPORT_SHEET_INDEX);

			/*
			 * khoi tao danh sach loai don vi
			 */
			this.initializeTempDataSheetOfShopTemplateImportFile(workbook);

			/*
			 * khoi tao danh sach loai khach hang
			 */
			this.initializeTempDataSheetOfCustomerTemplateImportFile(workbook);

			fileOutputStream = new FileOutputStream(importTemplateFileAbsolutePath);
			workbook.write(fileOutputStream);
			fileOutputStream.close();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		} finally {
			IOUtils.closeQuietly(fileInputStream);
			IOUtils.closeQuietly(fileOutputStream);
			IOUtils.closeQuietly(opcPackage);
		}

		return importTemplateFileName;
	}
	
	
	/**
	 * Ghi header sheet import
	 * 
	 * @author vuongbd
	 * @param workbook Workbook ghi du lieu
	 * @param importSheetIndex Vi tri sheet ghi du lieu
	 * @since 06-05-2015
	 */
	private void writePriceImportSheetHeader(XSSFWorkbook workbook, int importSheetIndex) {
		XSSFSheet importSheet = workbook.getSheetAt(importSheetIndex);
		CreationHelper factory = workbook.getCreationHelper();
		if (importSheet != null) {
			workbook.setSheetName(importSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.sheet_name"));
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			final int HEADER_ROW_INDEX = 0;
			final int HEADER_ROW_INDEX_1 = 1;
			int writingColumnIndex = 0;
			int writingColumnIndexAt1 = 0;
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.object"));
			writingColumnIndex += 4;
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX_1, writingColumnIndexAt1++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.shop_type"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX_1, writingColumnIndexAt1++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.shop"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX_1, writingColumnIndexAt1++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.customer_type"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX_1, writingColumnIndexAt1++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.customer"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.product"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.ptvat"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.pricevat"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.pricevat_2"));
			
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.status"));
			
			XSSFFont commentFont = (XSSFFont) workbook.createFont();
			ExcelPOIProcessUtils.setFontPOI(commentFont, "Times New Roman", 9, false, ExcelPOIProcessUtils.poiBlack);
			excelProcessUtil.writeCellComment(factory, importSheet, HEADER_ROW_INDEX, writingColumnIndex++, 
					Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_price.template_file.import.sheet.status.comment"), commentFont);
		}
	}
	
	/**
	 * Khoi tao sheet du lieu tam cua file mau dung de import price
	 * 
	 * @author tuannd20
	 * @return XSSFWorkbook File ghi du lieu
	 * @throws BusinessException
	 * @since 30/03/2015
	 */
	private void initializeTempDataSheetOfShopTemplateImportFile(XSSFWorkbook workbook) throws BusinessException {					
		//List<ShopType> shopType = shopTypeMgr.getListShopTypeByType(new ArrayList<Integer>(1));
		List<Integer> lstStatus = new ArrayList<Integer>();
		lstStatus.add(ActiveType.RUNNING.getValue());
		List<ShopType> shopType = shopTypeMgr.getListShopTypeByType(lstStatus);
		if (shopType!= null && shopType.size() > 0) {
			int DATA_SHEET_INDEX = 1;
			this.writeStaffTypeDataTo(workbook, DATA_SHEET_INDEX, shopType);
		}
	}
	
	/**
	 * ghi thong tin "Loai don vi" vao file de hien thi combobox chon tren file
	 * 
	 * @author tuannd20
	 * @param workbook Workbook ghi du lieu
	 * @param dataSheetIndex Vi tri sheet ghi du lieu
	 * @param staffTypeNames Danh sach ten Loai nhan vien se ghi vao file
	 * @since 28/03/2015
	 */
	private void writeStaffTypeDataTo(XSSFWorkbook workbook, int dataSheetIndex, List<ShopType> shopType) {
		XSSFSheet dataSheet = workbook.getSheetAt(dataSheetIndex);
		if (dataSheet != null) {
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			for (int i = 0, size = shopType.size(); i < size; i++) {
				excelProcessUtil.writeCellData(dataSheet, i, 0, shopType.get(i).getName());
			}
		}
	}

	
	/**
	 * Khoi tao sheet du lieu tam cua file mau dung de import price
	 * 
	 * @author vuongbd
	 * @return XSSFWorkbook File ghi du lieu
	 * @throws BusinessException
	 * @since 06/05/2015
	 */
	private void initializeTempDataSheetOfCustomerTemplateImportFile(XSSFWorkbook workbook) throws BusinessException {		
		List<ChannelType> lstCustomerType = new ArrayList<ChannelType>();
		lstCustomerType = channelTypeMgr.getListSubChannelTypeByType(ChannelTypeType.CUSTOMER.getValue());
		
		if (lstCustomerType != null && lstCustomerType.size() > 0) {
			int DATA_SHEET_INDEX = 2;
			this.writeCustomerTypeDataTo(workbook, DATA_SHEET_INDEX, lstCustomerType);
		}	
			
	}
	
	/**
	 * ghi thong tin "Loai khach hang" vao file de hien thi combobox chon tren file
	 * 
	 * @author vuong
	 * @param workbook Workbook ghi du lieu
	 * @param dataSheetIndex Vi tri sheet ghi du lieu
	 * @param lstCustomerType Danh sach ten Loai khach hang se ghi vao file
	 * @since 06/05/2015
	 */
	private void writeCustomerTypeDataTo(XSSFWorkbook workbook, int dataSheetIndex, List<ChannelType> lstCustomerType ) {
		XSSFSheet dataSheet = workbook.getSheetAt(dataSheetIndex);
		if (dataSheet != null) {
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			for (int i = 0, size = lstCustomerType.size(); i < size; i++) {
				excelProcessUtil.writeCellData(dataSheet, i, 0, lstCustomerType.get(i).getChannelTypeName());
			}
		}
	}
	
	
	/**
	 *Check ton tai khach hang voi san pham co ton tai chua
	
	 * @author vuong
	 * @param customer ma khach hang
	 * @param product ma san pham
	 * @since 06/05/2015
	 */
	private Price isExistsPriceByCustomerProduct(Customer customer, Product product, List<Integer> status) {		
		try {			
			Price prTmp = productMgr.getPriceByProductAndCustomer(product.getId(), customer.getId(), status);
			if (prTmp!=null && prTmp.getId() > 0) {
				return prTmp;
			} else {
				return null;
			}		
		} catch (BusinessException e) {
			LogUtility.logError(e, "isExistsPriceByCustomerProduct " + e.getMessage());
		}
		return null;
	}
	
	/**
	 *Check ton tai loai khach hang +  san pham + don vi + ma khach hang null co ton tai chua
	
	 * @author vuong
	 * @param customerType loai khach hang
	 * @param shop don vi
	 * @param product ma san pham
	 * @since 07/05/2015
	 */
	private Price isExistsPriceByChannelTypeProductShop(ChannelType customerType, Shop shop, Product product, List<Integer> status) {		
		try {			
			Price prTmp = productMgr.getPriceByProductChannelTypeShopAndCustomerNull(product.getId(), customerType.getId(), shop.getId(), status);
			if (prTmp!=null && prTmp.getId() > 0) {
				return prTmp;
			} else {
				return null;
			}		
		} catch (BusinessException e) {
			LogUtility.logError(e, "isExistsPriceByChannelTypeProductShop " + e.getMessage());
		}
		return null;
	}
	
	/**
	 *Check ton tai loai khach hang +  loai don vi (neu co) + san pham co ton tai chua
	
	 * @author vuong
	 * @param customerType loai khach hang
	 * @param shopType loai don vi
	 * @param product ma san pham
	 * @since 07/05/2015
	 */
	private Price isExistsPriceByChannelTypeShopTypeProduct(ChannelType customerType, ShopType shopType, Product product, List<Integer> status) {		
		try {
			Price prTmp;
			if (shopType!=null && shopType.getId() > 0) {
				prTmp = productMgr.getPriceByProductChannelTypeShopType(customerType.getId(),  shopType.getId(),product.getId(), status);
			} else {
				prTmp = productMgr.getPriceByProductChannelTypeShopType(customerType.getId(), -1L ,product.getId() , status);
			}
			
			if (prTmp!=null && prTmp.getId() > 0) {
				return prTmp;
			} else {
				return null;
			}		
		} catch (BusinessException e) {
			LogUtility.logError(e, "isExistsPriceByChannelTypeShopTypeProduct " + e.getMessage());
		}
		return null;
	}
	
	/**
	 *Check ton tai don vi  + san pham co ton tai chua
	
	 * @author vuong
	 * @param shop don vi
	 * @param product ma san pham
	 * @since 07/05/2015
	 */
	private Price isExistsPriceByShopProduct (Shop shop, Product product, List<Integer> status) {		
		try {
			Price prTmp = productMgr.getPriceByProductShop(shop.getId(), product.getId(), status);	
			if (prTmp!=null && prTmp.getId() > 0) {
				return prTmp;
			} else {
				return null;
			}		
		} catch (BusinessException e) {
			LogUtility.logError(e, "isExistsPriceByShopProduct " + e.getMessage());
		}
		return null;
	}
	
	/**
	 *Check ton tai loai don vi  + san pham co ton tai chua
	
	 * @author vuong
	 * @param shopType loai don vi
	 * @param product ma san pham
	 * @since 07/05/2015
	 */
	private Price isExistsPriceByShopTypeProduct (ShopType shopType, Product product, List<Integer> status) {		
		try {
			Price prTmp = productMgr.getPriceByProductShopType(shopType.getId(), product.getId(), status);	
			if (prTmp!=null && prTmp.getId() > 0) {
				return prTmp;
			} else {
				return null;
			}		
		} catch (BusinessException e) {
			LogUtility.logError(e, "isExistsPriceByShopTypeProduct " + e.getMessage());
		}
		return null;
	}
	
	/**
	 *Check dieu kien ton tai cua gia san pham
	
	 * @author vuong
	 * @param price gia san pham
	 * @param statusInsert trang thai gia
	 * @param type 1 : insert 2 : update
	 * @since 07/05/2015
	 */
	private String isExistsPriceByParametter (Price priceInsUpd, List<Integer> statusInsert, int type) {
		String message = "";
		Price price = null;
		//Them moi gia san pham
		//1. Co dien Ma KH: Khong Validate theo Loai KH, DV, Loai DV. Neu co dong cung SP trung khach hang tuc la trung.
		if (priceInsUpd.getCustomer() != null && priceInsUpd.getCustomer().getId() > 0) {
			price = this.isExistsPriceByCustomerProduct(priceInsUpd.getCustomer(), priceInsUpd.getProduct(), statusInsert);
			if (type == 1) {
				if (price != null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb");
				}
			} else if (type == 2) {
				if (price !=null && price.getId() > 0 && price.getStatus().getValue()==1) {
					setPriceOldToPriceNew(price, priceInsUpd);		
				}
				else if (price !=null && price.getId() > 0 && price.getStatus().getValue()!=1)
				{
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb.running");
				}
				else if (price ==null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.db.row.undefined");
				}
			}
			
		}
		//2. Khong co Ma KH, co Loai KH + DV: Khong Validate Loai DV. Neu co dong cung SP, trung Ma KH (NULL trong DB TH nay), Loai KH va DV tuc la trung
		else if (priceInsUpd.getCustomer() == null && priceInsUpd.getCustomerTypeId() != null && priceInsUpd.getCustomerTypeId().getId() > 0
				&& priceInsUpd.getShop() != null && priceInsUpd.getShop().getId() > 0) {
			price = this.isExistsPriceByChannelTypeProductShop(priceInsUpd.getCustomerTypeId(), priceInsUpd.getShop(), priceInsUpd.getProduct(), statusInsert);
			if (type == 1) {
				if (price != null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb");
				}	
			} else if (type == 2) {
				if (price !=null && price.getId() > 0 && price.getStatus().getValue()==1) {
					setPriceOldToPriceNew(price, priceInsUpd);		
				}
				else if (price !=null && price.getId() > 0 && price.getStatus().getValue()!=1)
				{
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb.running");
				}
				else if (price ==null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.db.row.undefined");
				}
			}
												
		}
		//3. Khong co Ma KH  + DV, Co Loai KH: Validate het. Neu co dong cung SP, trung het tuc la trung
		else if (priceInsUpd.getCustomer() == null && priceInsUpd.getShop() == null &&
				priceInsUpd.getCustomerTypeId() !=null && priceInsUpd.getCustomerTypeId().getId() > 0) {
			price = this.isExistsPriceByChannelTypeShopTypeProduct(priceInsUpd.getCustomerTypeId(), priceInsUpd.getShopType(), priceInsUpd.getProduct(), statusInsert);
			if (type == 1) {
				if (price != null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb");
				}
			} else if (type == 2) {
				if (price !=null && price.getId() > 0 && price.getStatus().getValue()==1) {
					setPriceOldToPriceNew(price, priceInsUpd);		
				}
				else if (price !=null && price.getId() > 0 && price.getStatus().getValue()!=1)
				{
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb.running");
				}
				else if (price ==null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.db.row.undefined");
				}
			}
												
		}
		//4. Khong co Ma KH + Loai KH, Co Don Vi: Khong validate Loai DV.
		else if (priceInsUpd.getCustomer() == null && priceInsUpd.getCustomerTypeId() == null
				&& priceInsUpd.getShop() != null && priceInsUpd.getShop().getId() > 0) {
			price = this.isExistsPriceByShopProduct(priceInsUpd.getShop(), priceInsUpd.getProduct(), statusInsert); //price.getStatus().getValue()
			if (type == 1) {
				if (price != null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb");
				}
			} else if (type == 2) {
				if (price !=null && price.getId() > 0 && price.getStatus().getValue()==1) {
					setPriceOldToPriceNew(price, priceInsUpd);		
				}
				else if (price !=null && price.getId() > 0 && price.getStatus().getValue()!=1)
				{
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb.running");
				}
				else if (price ==null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.db.row.undefined");
				}
			}
			
		}
		//5. Khong co Ma KH + Loai KH + Don Vi: Validate hết.
		else if (priceInsUpd.getCustomer() == null && priceInsUpd.getCustomerTypeId() == null
				&& priceInsUpd.getShop() == null) {
			price = this.isExistsPriceByShopTypeProduct(priceInsUpd.getShopType(), priceInsUpd.getProduct(), statusInsert);
			if (type == 1) {
				if (price != null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb");
				}
			} else if (type == 2) {
				if (price !=null && price.getId() > 0 && price.getStatus().getValue()==1) {
					setPriceOldToPriceNew(price, priceInsUpd);		
				}
				else if (price !=null && price.getId() > 0 && price.getStatus().getValue()!=1)
				{
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.price.isdefined.indb.running");
				}
				else if (price ==null) {
					message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.db.row.undefined");
				}
			}			
		} 
		return message;
	}
	
	private void setPriceOldToPriceNew(Price priceOld, Price priceNew){
		priceNew.setId(priceOld.getId());
		priceNew.setPackagePrice(priceOld.getPackagePrice());
		priceNew.setPackagePriceNotVat(priceOld.getPackagePriceNotVat());
		priceNew.setPrice(priceOld.getPrice());
		priceNew.setPriceNotVat(priceOld.getPriceNotVat());
		priceNew.setFromDate(priceOld.getFromDate());
		priceNew.setCreateDate(priceOld.getCreateDate());
		priceNew.setCreateUser(priceOld.getCreateUser());
		priceNew.setStatus(ActiveType.STOPPED);
	}
	
	private void calculatPriceNotVAT(Price priceInsUpd) {
		//Gia dong goi chua vat
		Float pricePackageNotVat = priceInsUpd.getPackagePrice().floatValue() * Float.valueOf(100);
		if (pricePackageNotVat > 0) {
			pricePackageNotVat = pricePackageNotVat / Float.valueOf(Float.valueOf(100) + priceInsUpd.getVat());
			priceInsUpd.setPackagePriceNotVat(StringUtil.precisionBigDecimal(0, BigDecimal.valueOf(pricePackageNotVat)));
		} else {
			priceInsUpd.setPackagePrice(BigDecimal.ZERO);
			priceInsUpd.setPackagePriceNotVat(BigDecimal.ZERO);
		}
		//Gia le chua vat
		pricePackageNotVat = priceInsUpd.getPrice().floatValue() * Float.valueOf(100);
		if (pricePackageNotVat > 0) {
			pricePackageNotVat = pricePackageNotVat / Float.valueOf(Float.valueOf(100) + priceInsUpd.getVat());
			priceInsUpd.setPriceNotVat(StringUtil.precisionBigDecimal(0, BigDecimal.valueOf(pricePackageNotVat)));
		} else {
			priceInsUpd.setPrice(BigDecimal.ZERO);
			priceInsUpd.setPriceNotVat(BigDecimal.ZERO);
		}
	}
	

	////////GETTER/SETTER

	private Warehouse warehouse;
	private ShopVO shopVO;

	private File excelFile;

	private List<ShopVO> lstShopVO = new ArrayList<ShopVO>();
	private List<Long> arrlongG;
	private List<ApParam> lstApparam = new ArrayList<ApParam>();
	private List<ChannelType> lstChannelType = new ArrayList<ChannelType>();
	private List<ShopType> lstShopType = new ArrayList<ShopType>();
	private List<ProductInfo> lstProductInfo = new ArrayList<ProductInfo>();

	private Long id;
	private Long longG;
	private Long longType;
	private Long catId;
	private Long shopTypeId;
	private Long customerTypeId;

	private Integer intG;
	private Integer type;
	private Integer status;
	private Integer seq;
	private Integer isEvent;

	private Float vat;

	private Boolean isFlag;

	private String code;
	private String name;
	private String description;
	private String textG;
	private String fromDateStr;
	private String toDateStr;
	private String shopCode;
	private String customerCode;
	private String productCode;
	private String productName;
	
	private String excelFileContentType;
	private BigDecimal price;
	private BigDecimal priceNotVat;
	private BigDecimal pricePackage;
	private BigDecimal pricePackageNotVat;

	private Integer shopChannel;

	public Integer getShopChannel() {
		return shopChannel;
	}

	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public List<ProductInfo> getLstProductInfo() {
		return lstProductInfo;
	}

	public void setLstProductInfo(List<ProductInfo> lstProductInfo) {
		this.lstProductInfo = lstProductInfo;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public Float getVat() {
		return vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public ProductMgr getProductMgr() {
		return productMgr;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getPriceNotVat() {
		return priceNotVat;
	}

	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}	

	public List<ShopVO> getLstShopVO() {
		return lstShopVO;
	}

	public void setLstShopVO(List<ShopVO> lstShopVO) {
		this.lstShopVO = lstShopVO;
	}

	public Long getLongType() {
		return longType;
	}

	public void setLongType(Long longType) {
		this.longType = longType;
	}

	public List<ChannelType> getLstChannelType() {
		return lstChannelType;
	}

	public void setLstChannelType(List<ChannelType> lstChannelType) {
		this.lstChannelType = lstChannelType;
	}

	public List<ShopType> getLstShopType() {
		return lstShopType;
	}

	public void setLstShopType(List<ShopType> lstShopType) {
		this.lstShopType = lstShopType;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public List<Long> getArrlongG() {
		return arrlongG;
	}

	public void setArrlongG(List<Long> arrlongG) {
		this.arrlongG = arrlongG;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public Integer getIsEvent() {
		return isEvent;
	}

	public void setIsEvent(Integer isEvent) {
		this.isEvent = isEvent;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Boolean getIsFlag() {
		return isFlag;
	}

	public void setIsFlag(Boolean isFlag) {
		this.isFlag = isFlag;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIntG() {
		return intG;
	}

	public void setIntG(Integer intG) {
		this.intG = intG;
	}

	public ShopVO getShopVO() {
		return shopVO;
	}

	public void setShopVO(ShopVO shopVO) {
		this.shopVO = shopVO;
	}

	public List<ApParam> getLstApparam() {
		return lstApparam;
	}

	public void setLstApparam(List<ApParam> lstApparam) {
		this.lstApparam = lstApparam;
	}

	public Long getLongG() {
		return longG;
	}

	public void setLongG(Long longG) {
		this.longG = longG;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTextG() {
		return textG;
	}

	public void setTextG(String textG) {
		this.textG = textG;
	}

	public String getFromDateStr() {
		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}


	public Long getShopTypeId() {
		return shopTypeId;
	}

	public void setShopTypeId(Long shopTypeId) {
		this.shopTypeId = shopTypeId;
	}

	public Long getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPricePackage() {
		return pricePackage;
	}

	public void setPricePackage(BigDecimal pricePackage) {
		this.pricePackage = pricePackage;
	}

	public BigDecimal getPricePackageNotVat() {
		return pricePackageNotVat;
	}

	public void setPricePackageNotVat(BigDecimal pricePackageNotVat) {
		this.pricePackageNotVat = pricePackageNotVat;
	}
}

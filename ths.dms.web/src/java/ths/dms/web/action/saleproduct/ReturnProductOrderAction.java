package ths.dms.web.action.saleproduct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DisplayProgrameMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.SaleDayMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.StDisplayProgramVNM;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.PromotionType;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProgramCodeVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderLotVOEx;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class ReturnProductOrderAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	final static String NTKH = "NTKH"; // tham so cau hinh ngay tra hang

	private SaleOrderMgr saleOrderMgr;

	private Customer customer; // khach hang
	private CustomerVO customerVo;

	private String shopCode; // ma npp
	private OrderType orderType;
	private Long code;
	private Long customerId;
	private String customerCode;
	private String customerName; //ten khach hang
	private String customerAddress; // dia chi khach hang
	private String customerChannelType; // loai khach hang
	private String customerCashierStaffCode; // nhan vien thu tien
	private String fromDate; // tu ngay
	private String toDate; // den ngay
	private String staffCode; // NVBH
	private String deliveryCode; // NVGH
	private String deliveryDate; // Ngay giao
	private String phone; //SDT nha kh
	private String mobiphone; //SDT di dong kh
	private String typeReturn;
	private boolean flag;
	private Boolean isReturnKeyShop;
	private Long shopId;

	private List<OrderType> orderTypeList; // loai don hang ( IN, CM , SO ... )
	private List<SaleOrder> listSaleOrder;
	private List<SaleOrderDetailVOEx> listSaleOrderDetailVOEx;
	private List<SaleOrderDetail> listSaleOrderDetail;
	private SaleOrderStatus saleOrderStatus; // trang thai don hang ( -2 , -1 ,
												// 0 , 1 ... )
	private String shortCode; // ma khach hang
	private String orderNumber; // ma don hang
	private Long saleOrderId;
	private Float totalWeight; // tong so luong
	private Float totalPrmWeight;
	private String carNumber; // so xe
	private SaleOrderType saleOrderType; // kieu don hang ( chua tra , da tra )
	private String saleOrderTypeString; // Type
	private String saleOrderStatusString; // ApprovedStatus
	private String orderTypeString;
	private BigDecimal totalAmount;

	private SaleOrder saleOrder;
	private ProgramType programType; // loai khuyen mai
	private String programTypeString;
	private BigDecimal total;
	private String ppCode;
	private int quantity;
	private BigDecimal sumDiscountAmount;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		// Nhan vien dang nhap
		// Xem nhan vien dang nhap do thuoc npp nao
		// lay doi tuong cua npp do..
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		try {
			ShopToken shop = currentUser.getShopRoot();
			shopCode = shop.getShopCode();
			shopId = shop.getShopId();
			ApParam ap = apParamMgr.getApParamByCodeEx(ApParamType.RETURN_KEYSHOP_CONFIG.getValue(), ActiveType.RUNNING);
			isReturnKeyShop = (ap == null || Constant.RETURN_KEYSHOP_CONFIG_YES.equals(ap.getValue()));
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.execute()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	public boolean checkLogin() throws Exception {
		try {
			if (currentUser != null && getSessionToken() != null) {
				//Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
				//Shop shop=(staff != null)? (staff.getShop() != null)? staff.getShop() : null : null;
				ShopToken shop = currentUser.getShopRoot();

				return (shop != null);
			}
			return false;
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.checkLogin()"), createLogErrorStandard(actionStartTime));
			return false;
		}
	}

	// tim kiem don hang

	/**
	 * Search sale order.
	 * 
	 * @return the string
	 * @author hungtt
	 * @since Sep 22, 2012
	 */
	public String searchSaleOrder() throws Exception {
		if (checkLogin()) {
			result.put("page", page);
			result.put("rows", rows);
			try {
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					Shop shop = shopMgr.getShopByCode(shopCode);
					shopId = shop.getId();
					dayLock = shopLockMgr.getNextLockedDay(shopId);
					if (dayLock == null) {
						dayLock = commonMgr.getSysDate();
					}
				} else {
					ShopToken shop = currentUser.getShopRoot();//staff.getShop();
					shopId = (shop != null) ? shop.getShopId() : null;//shop.getId();
				}
				/*
				 * Staff staff =
				 * staffMgr.getStaffByCode(currentUser.getUserName()); if (staff
				 * == null || staff.getShop() == null || staff.getStaffType() ==
				 * null) { return JSON; }
				 */
				ExceptionDayMgr exceptionDayMgr = (ExceptionDayMgr) context.getBean("exceptionDayMgr");

				Date startDate = null;
				Date endDate = null;
				//				KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
				//				kPaging.setPageSize(max);
				//				kPaging.setPage(page-1);
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					startDate = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					endDate = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
				} else {
					endDate = dayLock;
				}
				if (!StringUtil.isNullOrEmpty(saleOrderStatusString) && !saleOrderStatusString.equals("")) {
					saleOrderStatus = SaleOrderStatus.parseValue(Integer.parseInt(saleOrderStatusString));
				}
				orderTypeList = new ArrayList<OrderType>();
				if (!StringUtil.isNullOrEmpty(orderTypeString)) {
					orderType = OrderType.parseValue(orderTypeString);
					orderTypeList.add(orderType);
				}

				if (!StringUtil.isNullOrEmpty(saleOrderTypeString)) {
					saleOrderType = SaleOrderType.parseValue(Integer.parseInt(saleOrderTypeString));
				}

				SoFilter filter = new SoFilter();
				//				filter.setkPaging(kPaging);
				filter.setShortCode(shortCode);
				filter.setCustomeName(customerName);
				filter.setCustomerId(customerId);
				filter.setStaffCode(staffCode);
				filter.setOrderNumber(orderNumber);
				filter.setApproval(SaleOrderStatus.APPROVED);
				filter.setListOrderType(orderTypeList);

				filter.setDeliveryCode(deliveryCode);
				filter.setSaleOrderType(SaleOrderType.NOT_YET_RETURNED);
				//ApParam apParam = apParamMgr.getApParamByCode("NTKH",ApParamType.TH);
				Integer iApParamName = 0;
				Date startDateTemp = null;
				//filter.setFromDate(startDate);
				Date lockDate = dayLock;//shopLockMgr.getApplicationDate(shopId);
				if (flag) {
					List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_MAXDAY_RETURN);
					if (lstPr != null && lstPr.size() > 0) {
						try {
							iApParamName = Integer.valueOf(lstPr.get(0).getValue().trim());
						} catch (Exception e1) {
							LogUtility.logErrorStandard(e1, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.searchSaleOrder()"), createLogErrorStandard(actionStartTime));
						}
					}

					if (iApParamName >= 0) {
						Integer iExcept = 0;
						startDateTemp = DateUtil.moveDate(DateUtil.parseLockDate(lockDate), iApParamName * (-1), 1);
						Date dateTmp = DateUtil.parseLockDate(lockDate);
						do {
							//							Integer iExceptHoliday = exceptionDayMgr.getNumOfHoliday(startDateTemp, dateTmp, shopId);
							//							Integer iExceptSunday = this.getNumOfWeekdays(startDateTemp, dateTmp);
							//							iExcept = iExceptHoliday + iExceptSunday;

							iExcept = exceptionDayMgr.getNumOfHoliday(startDateTemp, dateTmp, shopId);
							dateTmp = DateUtil.moveDate(startDateTemp, -1, 1);
							startDateTemp = DateUtil.moveDate(startDateTemp, iExcept * (-1), 1);
						} while (iExcept > 0);
					}
				}
				if (DateUtil.compareDateWithoutTime(endDate, lockDate) == 1) {
					filter.setToDate(lockDate);
				} else {
					filter.setToDate(endDate);
				}
				if (startDate == null || (startDateTemp != null && DateUtil.compareDateWithoutTime(startDate, startDateTemp) < 0)) {
					filter.setFromDate(startDateTemp);
				} else {
					filter.setFromDate(startDate);
				}
				filter.setShopId(shopId);
				filter.setIsPrint(null);
				filter.setStrListUserId(super.getStrListUserId());
				ObjectVO<SaleOrder> objectVOSaleOrder = saleOrderMgr.getListSaleOrderToReturn(filter);
				if (objectVOSaleOrder != null) {
					listSaleOrder = objectVOSaleOrder.getLstObject();
				} else {
					listSaleOrder = new ArrayList<SaleOrder>();
				}

				result.put("total", listSaleOrder.size());
				result.put("rows", listSaleOrder);
			} catch (BusinessException ex) {
				LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.searchSaleOrder()"), createLogErrorStandard(actionStartTime));
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			return JSON;
		} else {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_LOGIN, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.login")));
			result.put("logOut", true);
			return ERROR;
		}

	}

	//	private int getNumOfWeekdays(Date startDate, Date endDate) {
	//		Calendar startCal = Calendar.getInstance();
	//		startCal.setTime(startDate);
	//		startCal.set(Calendar.HOUR_OF_DAY, 0);
	//		startCal.set(Calendar.MINUTE, 0);
	//		startCal.set(Calendar.SECOND, 0);
	//		startCal.set(Calendar.MILLISECOND, 0);
	//		
	//		Calendar endCal = Calendar.getInstance();
	//		endCal.setTime(endDate);
	//		endCal.set(Calendar.HOUR_OF_DAY, 0);
	//		endCal.set(Calendar.MINUTE, 0);
	//		endCal.set(Calendar.SECOND, 0);
	//		endCal.set(Calendar.MILLISECOND, 0);
	//		int holiDays = 0;
	//		
	//		if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
	//			startCal.setTime(endDate);
	//			endCal.setTime(startDate);
	//		}
	//		
	//		while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()) {
	//			if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
	//				++holiDays;
	//			}
	//			startCal.add(Calendar.DAY_OF_MONTH, 1);
	//		}
	//
	//		return holiDays;
	//	}

	public String getCustomerDetail() throws Exception {
		if (checkLogin()) {
			try {
				CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
				ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
				customer = customerMgr.getCustomerById(customerId);
				setCustomerName(customer.getCustomerName());
				setCustomerCode(customer.getShortCode());
				setCustomerAddress(customer.getAddress());
				setShopCode(customer.getShop().getShopCode());
				List<SaleOrder> lst = saleOrderMgr.getListReturnOrderByOrderId(saleOrderId, SaleOrderStatus.NOT_YET_APPROVE, SaleOrderStep.CONFIRMED);
				if (lst != null && lst.size() > 0) {
					saleOrder = lst.get(0);
				} else {
					saleOrder = saleOrderMgr.getSaleOrderById(saleOrderId);
				}
				carNumber = (saleOrder != null) ? ((saleOrder.getCar() != null) ? saleOrder.getCar().getCarNumber() : null) : null;
				setCustomerId(customer.getId());
				deliveryDate = (saleOrder.getDeliveryDate() != null) ? DateUtil.toDateString(saleOrder.getDeliveryDate(), DateUtil.DATE_FORMAT_DDMMYYYY) : null;
				setDeliveryDate(deliveryDate);
				phone = customer.getPhone();
				setPhone(phone);
				mobiphone = customer.getMobiphone();
				setMobiphone(mobiphone);
				if (customer.getCashierStaff() != null) {
					setCustomerCashierStaffCode(customer.getCashierStaff().getStaffCode());
				} else {
					setCustomerCashierStaffCode(null);
				}

				Long id = (customer.getChannelType() != null) ? customer.getChannelType().getId() : null;
				customerChannelType = (channelTypeMgr.getChannelTypeById(id) != null) ? channelTypeMgr.getChannelTypeById(id).getChannelTypeCode() + "-" + channelTypeMgr.getChannelTypeById(id).getChannelTypeName() : "";
			} catch (BusinessException ex) {
				LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.getCustomerDetail()"), createLogErrorStandard(actionStartTime));
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));

			}
			return SUCCESS;
		}
		return ERROR;
	}

	/**
	 * Kiem tra don hang co so hoa don do hay khong
	 * 
	 * @author tungtt21
	 * @return
	 */
	/*
	 * public String checkInvoice() throws Exception { try { if (saleOrderId !=
	 * null) { List<Invoice> listInvoice =
	 * saleOrderMgr.getListInvoiceBySaleOrderId(saleOrderId); String
	 * invoiceString = ""; if(listInvoice.size() > 0){ invoiceString =
	 * listInvoice.get(0).getInvoiceNumber(); for(int i = 1; i <
	 * listInvoice.size(); i++){ invoiceString += ", " +
	 * listInvoice.get(i).getInvoiceNumber(); } }
	 * if(!StringUtil.isNullOrEmpty(invoiceString)){ result.put("invoiceString",
	 * invoiceString); } } } catch (Exception e) { LogUtility.logError(e,
	 * "Error ReturnProductOrderAction.checkInvoice"); result.put(ERROR, true);
	 * result.put("errMsg",
	 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM)); } return JSON; }
	 */

	// tra hang
	/**
	 * Return product order.
	 * 
	 * @return the string
	 * @author hungtt
	 * @since Sep 25, 2012
	 */
	public String returnProductOrder() throws Exception {
		if (checkLogin()) {
			if (saleOrderId != null) {
				try {
					SaleDayMgr saleDayMgr = (SaleDayMgr) context.getBean("saleDayMgr");
					currentUser = getCurrentUser();
					Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
					staffCode = staff.getStaffCode();
					SaleOrder checkSaleOrder = saleOrderMgr.getSaleOrderById(saleOrderId);
					if (checkSaleOrder != null && SaleOrderStatus.APPROVED.equals(checkSaleOrder.getApproved()) && (OrderType.IN.equals(checkSaleOrder.getOrderType()) || OrderType.SO.equals(checkSaleOrder.getOrderType()))
							&& SaleOrderType.NOT_YET_RETURNED.equals(checkSaleOrder.getType())) {
						Date lockDate;
						if (!StringUtil.isNullOrEmpty(shopCode)) {
							Shop shop = shopMgr.getShopByCode(shopCode);
							shopId = shop.getId();
							lockDate = shopLockMgr.getApplicationDate(shopId);
						} else {
							ShopToken shop = currentUser.getShopRoot();
							shopId = shop.getShopId();
							lockDate = shopLockMgr.getApplicationDate(shop.getShopId());
						}

						if (OrderType.SO.equals(checkSaleOrder.getOrderType())) {
							if (DateUtil.compareTwoDate(lockDate, checkSaleOrder.getOrderDate()) <= 0) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("sp.return.product.order.vansale.invalid.date"));
								return JSON;
							}
						}
						long dayDiff = -1;
						Integer workday = 0;

						List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_MAXDAY_RETURN);
						if (lstPr != null && lstPr.size() > 0) {
							try {
								int limit = Integer.valueOf(lstPr.get(0).getValue().trim());
								if (limit > -1) {
									workday = saleDayMgr.getNumberWorkingDayByFromDateAndToDateEx(checkSaleOrder.getOrderDate(), lockDate);
									dayDiff = workday - limit;
								}
							} catch (Exception e1) {
								// pass through
							}
						}

						SaleOrder saleOrder = null;
						if (dayDiff <= 0) {
							saleOrder = (staffCode != null) ? saleOrderMgr.updateReturnSaleOrder(saleOrderId, staffCode, false) : null;
							if (saleOrder != null) {
								result.put("saleOrderNumber", saleOrder.getOrderNumber());
								result.put("saleOrderId", saleOrder.getId());
							} else {
								result.put(ERROR, true);
								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
							}
						} else {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_ORDER_DATE_INVALID));
						}

					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_SALEORDER_RETURNED, false, "sp.adjust.order.order.title"));
						result.put("returnOrder", true);
					}
				} catch (Exception e) {
					LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.returnProductOrder()"), createLogErrorStandard(actionStartTime));
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				result.put("saleOrderNull", true);
			}
		} else {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_LOGIN, false, "common.not.login"));
			result.put("logOut", true);
		}

		return JSON;
	}

	/**
	 * Promotion program.
	 * 
	 * @return the string
	 * @author hungtt
	 * @since Sep 25, 2012
	 */
	public String promotionProgram() throws Exception {
		try {
			if (typeReturn != null && "2".equals(typeReturn)) {
				DisplayProgrameMgr displayProgrameMgr = (DisplayProgrameMgr) context.getBean("displayProgrameMgr");
				StDisplayProgramVNM promotionProgram = displayProgrameMgr.getDisplayProgramByCode(ppCode);
				if (promotionProgram != null) {
					result.put("ppCode", promotionProgram.getDisplayProgramCode());
					result.put("ppName", promotionProgram.getDisplayProgramName());
					result.put("ppType", promotionProgram.getProgrameType());
					result.put("ppFormat", "");
					result.put("fromDate", DateUtil.toDateString(promotionProgram.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					result.put("toDate", DateUtil.toDateString(promotionProgram.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					result.put("ppDescription", "");
				}
			} else {
				PromotionProgramMgr promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
				PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramByCode(ppCode);
				if (promotionProgram != null) {
					result.put("ppCode", promotionProgram.getPromotionProgramCode());
					result.put("ppName", promotionProgram.getPromotionProgramName());
					result.put("ppType", promotionProgram.getType());
					result.put("ppFormat", promotionProgram.getProFormat());
					result.put("fromDate", DateUtil.toDateString(promotionProgram.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					result.put("toDate", DateUtil.toDateString(promotionProgram.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					result.put("ppDescription", promotionProgram.getDescription());
				}
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.promotionProgram()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Gets the customer info.
	 * 
	 * @return the customer info
	 */
	public String getCustomerInfo() throws Exception {
		try {
			CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
			customerVo = customerMgr.getCustomerVO(customerId);
			if (customerVo != null) {
				result.put("customerCode", customerVo.getCustomerCode());
				result.put("customerShortCode", customerVo.getShortCode());
				result.put("customerName", customerVo.getCustomerName());
				result.put("address", customerVo.getAddress());
				result.put("phone", customerVo.getPhone());
				result.put("mobiphone", customerVo.getMobiphone());
				result.put("shopTypeName", customerVo.getShopTypeName());
				result.put("loyalty", customerVo.getLoyalty());
				result.put("contactName", customerVo.getContactName());
				result.put("totalInDate", customerVo.getTotalInDate());
				result.put("numSkuInDate", customerVo.getNumSkuInDate());
				result.put("numOrderInMonth", customerVo.getNumOrderInMonth());
				result.put("avgTotalInLastTwoMonth", customerVo.getAvgTotalInLastTwoMonth());
				result.put("totalInMonth", customerVo.getTotalInMonth());
			}
			if (!StringUtil.isNullOrEmpty(typeReturn)) {
				return SUCCESS;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.getCustomerInfo()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Lay danh sach don hang cua KH
	 */
	public String getListSaleOrderByCustomer() throws Exception {
		try {
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
			kPaging.setPageSize(5);
			SoFilter soFilter = new SoFilter();
			soFilter.setApproval(SaleOrderStatus.APPROVED);
			soFilter.setCustomerId(customerId);
			soFilter.setkPaging(kPaging);
			soFilter.setIsCheckOrderDateDesc(true);
			soFilter.setToDate(dayLock);
			ObjectVO<SaleOrder> saleOrderVo = saleOrderMgr.getListSaleOrderEx1(soFilter);
			if (saleOrderVo != null) {
				result.put("total", saleOrderVo.getLstObject().size());
				result.put("rows", saleOrderVo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.getListSaleOrderByCustomer()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Huy bo don tra hang
	 * 
	 * @author lacnv1
	 * @since Sep 30, 2014
	 */
	public String cancelReturnOrder() throws Exception {
		resetToken(result);
		try {
			result.put(ERROR, true);
			if (saleOrderId == null || saleOrderId < 0) {
				result.put("errMsg", R.getResource("sp.sale.order.required.to.cancel"));
				return JSON;
			}
			saleOrder = saleOrderMgr.getSaleOrderById(saleOrderId);
			if (saleOrder == null) {
				result.put("errMsg", R.getResource("sp.search.sale.order.approved.error.remove"));
				return JSON;
			}
			if (!SaleOrderType.RETURNED_ORDER.equals(saleOrder.getType()) || !SaleOrderStatus.NOT_YET_APPROVE.equals(saleOrder.getApproved()) || !SaleOrderStep.CONFIRMED.equals(saleOrder.getApprovedStep())) {
				result.put("errMsg", R.getResource("sp.return.order.cancel.invalid"));
				return JSON;
			}

			saleOrderMgr.cancelReturnOrder(saleOrder.getId(), getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.cancelReturnOrder()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Lay thong tin chi tiet don hang
	 * 
	 * @author lacnv1
	 * @since Mar 26, 2014
	 */
	public String getSaleOrderDetail() throws Exception {
		try {
			result.put(ERROR, true);
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			//saleOrderId = 1002652L; //68500000020L //1002903L
			if (saleOrderId != null) {
				List<SaleOrderLotVOEx> lstDetail = new ArrayList<SaleOrderLotVOEx>();
				List<SaleOrderLotVOEx> lstPromotion = new ArrayList<SaleOrderLotVOEx>();
				List<SaleOrderLotVOEx> lstOpenPromotion = new ArrayList<SaleOrderLotVOEx>();
				List<SaleOrderLotVOEx> lstOrderPromotion = new ArrayList<SaleOrderLotVOEx>();
				List<SaleOrderLotVOEx> lstAccumulation = new ArrayList<SaleOrderLotVOEx>();

				List<SaleOrderLotVOEx> lst = saleOrderMgr.getListOrderPromoDetailByOrderId(saleOrderId);
				if (lst != null && lst.size() > 0) {
					for (SaleOrderLotVOEx vo : lst) {
						if (PromotionType.ZV19.getValue().equals(vo.getProgramTypeCode()) || PromotionType.ZV20.getValue().equals(vo.getProgramTypeCode())) {
							lstOrderPromotion.add(vo);
						} else if (PromotionType.ZV23.getValue().equals(vo.getProgramTypeCode())) {
							lstAccumulation.add(vo);
						} else if (PromotionType.ZV24.getValue().equals(vo.getProgramTypeCode())) {
							lstOpenPromotion.add(vo);
						} else if (!PromotionType.KS.getValue().equals(vo.getProgramTypeCode())) {
							lstPromotion.add(vo);
						}
					}
				}

				lst = saleOrderMgr.getListOrderDetailVOByOrderId(saleOrderId);
				if (lst != null && lst.size() > 0) {
					for (SaleOrderLotVOEx vo : lst) {
						if (vo.getIsFreeItem() != null && vo.getIsFreeItem() == 1 && ProgramType.AUTO_PROM.getValue().equals(vo.getProgramType())) {
							if (PromotionType.ZV21.getValue().equals(vo.getProgramTypeCode())) {
								lstOrderPromotion.add(vo);
							} else if (PromotionType.ZV23.getValue().equals(vo.getProgramTypeCode())) {
								lstAccumulation.add(vo);
							} else if (PromotionType.ZV24.getValue().equals(vo.getProgramTypeCode())) {
								lstOpenPromotion.add(vo);
							} else {
								lstPromotion.add(vo);
							}
						} else if (!ProgramType.KEY_SHOP.getValue().equals(vo.getProgramType())) {
							lstDetail.add(vo);
						}
					}
				}

				SaleOrderFilter<SaleOrder> saleOrderFilter = new SaleOrderFilter<SaleOrder>();
				saleOrderFilter.setSaleOrderId(saleOrderId);
				List<ProgramCodeVO> progamCodeVOs = saleOrderMgr.getListProgramCodeVO(saleOrderFilter);
				if (progamCodeVOs != null) {
					for (SaleOrderLotVOEx detail : lstDetail) {
						for (ProgramCodeVO programCode : progamCodeVOs) {
							if (programCode.getSaleOrderDetailId().equals(detail.getSaleOrderDetailId())) {
								detail.setProgramCode(programCode.getStrProgramCode());
								break;
							}
						}
					}
				}

				result.put("lstDetail", lstDetail);
				result.put("lstPromotion", lstPromotion);
				result.put("lstOpenPromotion", lstOpenPromotion);
				result.put("lstOrderPromotion", lstOrderPromotion);
				result.put("lstAccumulation", lstAccumulation);
			}

			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.getSaleOrderDetail()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String onSelectShopCodeCombobox() {
		try {
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				shopId = shop.getId();

				//cau hinh show gia hay khong
				List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
				if (lstPr != null && lstPr.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())) {//cho show gia
					result.put("isAllowShowPrice", true);
				} else {
					result.put("isAllowShowPrice", false);
				}

				Date lockDate = shopLockMgr.getApplicationDate(shopId);
				if (lockDate == null) {
					lockDate = commonMgr.getSysDate();
				}
				fromDate = DateUtil.toDateString(lockDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				result.put("fromDate", fromDate);
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ReturnProductOrderAction.onSelectShopCodeCombobox()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/* GETTERS + SETTERS */

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerChannelType() {
		return customerChannelType;
	}

	public void setCustomerChannelType(String customerChannelType) {
		this.customerChannelType = customerChannelType;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public SaleOrderStatus getSaleOrderStatus() {
		return saleOrderStatus;
	}

	public void setSaleOrderStatus(SaleOrderStatus saleOrderStatus) {
		this.saleOrderStatus = saleOrderStatus;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public List<SaleOrder> getListSaleOrder() {
		return listSaleOrder;
	}

	public void setListSaleOrder(List<SaleOrder> listSaleOrder) {
		this.listSaleOrder = listSaleOrder;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getSaleOrderStatusString() {
		return saleOrderStatusString;
	}

	public void setSaleOrderStatusString(String saleOrderStatusString) {
		this.saleOrderStatusString = saleOrderStatusString;
	}

	public String getOrderTypeString() {
		return orderTypeString;
	}

	public void setOrderTypeString(String orderTypeString) {
		this.orderTypeString = orderTypeString;
	}

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public SaleOrderType getSaleOrderType() {
		return saleOrderType;
	}

	public void setSaleOrderType(SaleOrderType saleOrderType) {
		this.saleOrderType = saleOrderType;
	}

	public String getSaleOrderTypeString() {
		return saleOrderTypeString;
	}

	public void setSaleOrderTypeString(String saleOrderTypeString) {
		this.saleOrderTypeString = saleOrderTypeString;
	}

	public List<OrderType> getOrderTypeList() {
		return orderTypeList;
	}

	public void setOrderTypeList(List<OrderType> orderTypeList) {
		this.orderTypeList = orderTypeList;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public List<SaleOrderDetailVOEx> getListSaleOrderDetailVOEx() {
		return listSaleOrderDetailVOEx;
	}

	public void setListSaleOrderDetailVOEx(List<SaleOrderDetailVOEx> listSaleOrderDetailVOEx) {
		this.listSaleOrderDetailVOEx = listSaleOrderDetailVOEx;
	}

	public String getCustomerCashierStaffCode() {
		return customerCashierStaffCode;
	}

	public void setCustomerCashierStaffCode(String customerCashierStaffCode) {
		this.customerCashierStaffCode = customerCashierStaffCode;
	}

	public ProgramType getProgramType() {
		return programType;
	}

	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}

	public String getProgramTypeString() {
		return programTypeString;
	}

	public void setProgramTypeString(String programTypeString) {
		this.programTypeString = programTypeString;
	}

	public List<SaleOrderDetail> getListSaleOrderDetail() {
		return listSaleOrderDetail;
	}

	public void setListSaleOrderDetail(List<SaleOrderDetail> listSaleOrderDetail) {
		this.listSaleOrderDetail = listSaleOrderDetail;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getPpCode() {
		return ppCode;
	}

	public void setPpCode(String ppCode) {
		this.ppCode = ppCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Float getTotalPrmWeight() {
		return totalPrmWeight;
	}

	public void setTotalPrmWeight(Float totalPrmWeight) {
		this.totalPrmWeight = totalPrmWeight;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getTypeReturn() {
		return typeReturn;
	}

	public void setTypeReturn(String typeReturn) {
		this.typeReturn = typeReturn;
	}

	public CustomerVO getCustomerVo() {
		return customerVo;
	}

	public void setCustomerVo(CustomerVO customerVo) {
		this.customerVo = customerVo;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobiphone() {
		return mobiphone;
	}

	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}

	public BigDecimal getSumDiscountAmount() {
		return sumDiscountAmount;
	}

	public void setSumDiscountAmount(BigDecimal sumDiscountAmount) {
		this.sumDiscountAmount = sumDiscountAmount;
	}

	public void setTotalWeight(Float totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Float getTotalWeight() {
		return totalWeight;
	}

	public Boolean getIsReturnKeyShop() {
		return isReturnKeyShop;
	}

	public void setIsReturnKeyShop(Boolean isReturnKeyShop) {
		this.isReturnKeyShop = isReturnKeyShop;
	}


}
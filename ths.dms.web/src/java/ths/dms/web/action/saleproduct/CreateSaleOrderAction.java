package ths.dms.web.action.saleproduct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CarMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.CommercialSupportType;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.vo.CommercialSupportCodeVO;
import ths.dms.core.entities.vo.CommercialSupportVO;
import ths.dms.core.entities.vo.Customer4SaleVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PromotionProductsVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleProductVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class CreateSaleOrderAction extends AbstractAction {
	private List<Customer4SaleVO> listCustomer;
	private List<Staff> listNVBH;
	private List<Staff> listNVGH;
	private List<Staff> listNVTT;
	private List<Car> listCar;
	private Shop shop;
	private String shortCode;
	private String saleStaffCode;
	private String fsxn;
	private Integer allowEditPromotion;
	private List<ApParam> listPriority;
	private String customerCode;
	private List<String> listCS;
	private List<String> listCSType;
	private List<String> listProductCode;
	private List<String> listQuantityStr;
	private List<Integer> listQuantityInt;
	private String productCode;
	private Map<String, List<ProductLot>> mapProductLot;
	private Date lockDay;
	private Long key;
	private Integer typeSortKey;
	private String shopCode;
	private String deliveryCode;
	private String cashierCode;
	private String salerCode;
	private Customer customer;
	private List<String> lstCommercialSupport;
	private List<String> lstCommercialSupportType;
	private List<String> lstProduct;
	private Car car;
	private int priorityId;
	private String deliveryDate;
	private List<Integer> lstQuantity;
	private Date sysDate;
	private List<String> lstBuyProductCommercialSupportProgram;
	private List<String> lstBuyProductCommercialSupportProgramType;
	private List<String> lstProductSaleLot;
	private List<String> lstProductDestroy;
	private List<String> lstPromotionCode;
	private List<Integer> lstPromotionType;
	private List<String> lstPromotionProduct;
	private List<String> lstPromotionProductQuatity;
	private List<String> lstPromotionProductMaxQuatity;
	private List<String> lstPromotionProductMaxQuatityCk;
	private List<String> lstPromotionProductSaleLot;
	private List<String> lstDiscount;
	private List<String> lstDisper;
	private Long carId;
	private String ppCode;
	
	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}

	public List<Customer4SaleVO> getListCustomer() {
		return listCustomer;
	}

	public void setListCustomer(List<Customer4SaleVO> listCustomer) {
		this.listCustomer = listCustomer;
	}

	public List<Staff> getListNVBH() {
		return listNVBH;
	}

	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}

	public List<Staff> getListNVGH() {
		return listNVGH;
	}

	public void setListNVGH(List<Staff> listNVGH) {
		this.listNVGH = listNVGH;
	}

	public List<Staff> getListNVTT() {
		return listNVTT;
	}

	public void setListNVTT(List<Staff> listNVTT) {
		this.listNVTT = listNVTT;
	}

	public List<Car> getListCar() {
		return listCar;
	}

	public void setListCar(List<Car> listCar) {
		this.listCar = listCar;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getFsxn() {
		return fsxn;
	}

	public void setFsxn(String fsxn) {
		this.fsxn = fsxn;
	}

	public Integer getAllowEditPromotion() {
		return allowEditPromotion;
	}

	public void setAllowEditPromotion(Integer allowEditPromotion) {
		this.allowEditPromotion = allowEditPromotion;
	}

	public List<ApParam> getListPriority() {
		return listPriority;
	}

	public void setListPriority(List<ApParam> listPriority) {
		this.listPriority = listPriority;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public List<String> getListCS() {
		return listCS;
	}

	public void setListCS(List<String> listCS) {
		this.listCS = listCS;
	}

	public List<String> getListCSType() {
		return listCSType;
	}

	public void setListCSType(List<String> listCSType) {
		this.listCSType = listCSType;
	}
	
	public List<String> getListProductCode() {
		return listProductCode;
	}

	public void setListProductCode(List<String> listProductCode) {
		this.listProductCode = listProductCode;
	}

	public List<String> getListQuantityStr() {
		return listQuantityStr;
	}

	public void setListQuantityStr(List<String> listQuantityStr) {
		this.listQuantityStr = listQuantityStr;
	}

	public List<Integer> getListQuantityInt() {
		return listQuantityInt;
	}

	public void setListQuantityInt(List<Integer> listQuantityInt) {
		this.listQuantityInt = listQuantityInt;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	

	public Map<String, List<ProductLot>> getMapProductLot() {
		return mapProductLot;
	}

	public void setMapProductLot(Map<String, List<ProductLot>> mapProductLot) {
		this.mapProductLot = mapProductLot;
	}

	public Date getLockDay() {
		return lockDay;
	}

	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}

	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public Integer getTypeSortKey() {
		return typeSortKey;
	}

	public void setTypeSortKey(Integer typeSortKey) {
		this.typeSortKey = typeSortKey;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	
	public String getCashierCode() {
		return cashierCode;
	}

	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}

	public String getSalerCode() {
		return salerCode;
	}

	public void setSalerCode(String salerCode) {
		this.salerCode = salerCode;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<String> getLstCommercialSupport() {
		return lstCommercialSupport;
	}

	public void setLstCommercialSupport(List<String> lstCommercialSupport) {
		this.lstCommercialSupport = lstCommercialSupport;
	}

	public List<String> getLstCommercialSupportType() {
		return lstCommercialSupportType;
	}

	public void setLstCommercialSupportType(List<String> lstCommercialSupportType) {
		this.lstCommercialSupportType = lstCommercialSupportType;
	}

	public List<String> getLstProduct() {
		return lstProduct;
	}

	public void setLstProduct(List<String> lstProduct) {
		this.lstProduct = lstProduct;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public int getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public List<Integer> getLstQuantity() {
		return lstQuantity;
	}

	public void setLstQuantity(List<Integer> lstQuantity) {
		this.lstQuantity = lstQuantity;
	}

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	public List<String> getLstBuyProductCommercialSupportProgram() {
		return lstBuyProductCommercialSupportProgram;
	}

	public void setLstBuyProductCommercialSupportProgram(
			List<String> lstBuyProductCommercialSupportProgram) {
		this.lstBuyProductCommercialSupportProgram = lstBuyProductCommercialSupportProgram;
	}

	public List<String> getLstBuyProductCommercialSupportProgramType() {
		return lstBuyProductCommercialSupportProgramType;
	}

	public void setLstBuyProductCommercialSupportProgramType(
			List<String> lstBuyProductCommercialSupportProgramType) {
		this.lstBuyProductCommercialSupportProgramType = lstBuyProductCommercialSupportProgramType;
	}

	public List<String> getLstProductSaleLot() {
		return lstProductSaleLot;
	}

	public void setLstProductSaleLot(List<String> lstProductSaleLot) {
		this.lstProductSaleLot = lstProductSaleLot;
	}

	public List<String> getLstProductDestroy() {
		return lstProductDestroy;
	}

	public void setLstProductDestroy(List<String> lstProductDestroy) {
		this.lstProductDestroy = lstProductDestroy;
	}

	public List<String> getLstPromotionCode() {
		return lstPromotionCode;
	}

	public void setLstPromotionCode(List<String> lstPromotionCode) {
		this.lstPromotionCode = lstPromotionCode;
	}

	public List<Integer> getLstPromotionType() {
		return lstPromotionType;
	}

	public void setLstPromotionType(List<Integer> lstPromotionType) {
		this.lstPromotionType = lstPromotionType;
	}

	public List<String> getLstPromotionProduct() {
		return lstPromotionProduct;
	}

	public void setLstPromotionProduct(List<String> lstPromotionProduct) {
		this.lstPromotionProduct = lstPromotionProduct;
	}

	public List<String> getLstPromotionProductQuatity() {
		return lstPromotionProductQuatity;
	}

	public void setLstPromotionProductQuatity(
			List<String> lstPromotionProductQuatity) {
		this.lstPromotionProductQuatity = lstPromotionProductQuatity;
	}

	public List<String> getLstPromotionProductMaxQuatity() {
		return lstPromotionProductMaxQuatity;
	}

	public void setLstPromotionProductMaxQuatity(
			List<String> lstPromotionProductMaxQuatity) {
		this.lstPromotionProductMaxQuatity = lstPromotionProductMaxQuatity;
	}
	
	public List<String> getLstPromotionProductMaxQuatityCk() {
		return lstPromotionProductMaxQuatityCk;
	}

	public void setLstPromotionProductMaxQuatityCk(
			List<String> lstPromotionProductMaxQuatityCk) {
		this.lstPromotionProductMaxQuatityCk = lstPromotionProductMaxQuatityCk;
	}


	public List<String> getLstPromotionProductSaleLot() {
		return lstPromotionProductSaleLot;
	}

	public void setLstPromotionProductSaleLot(
			List<String> lstPromotionProductSaleLot) {
		this.lstPromotionProductSaleLot = lstPromotionProductSaleLot;
	}

	public List<String> getLstDiscount() {
		return lstDiscount;
	}

	public void setLstDiscount(List<String> lstDiscount) {
		this.lstDiscount = lstDiscount;
	}

	public List<String> getLstDisper() {
		return lstDisper;
	}

	public void setLstDisper(List<String> lstDisper) {
		this.lstDisper = lstDisper;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}
	
	public String getPpCode() {
		return ppCode;
	}

	public void setPpCode(String ppCode) {
		this.ppCode = ppCode;
	}

	CustomerMgr customerMgr;
	SaleOrderMgr saleOrderMgr;
	CommonMgr commonMgr;
	ApParamMgr apParamMgr;
	CarMgr carMgr;
	ProductMgr productMgr;
	PromotionProgramMgr promotionProgramMgr;
	StockMgr stockMgr;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockMgr = (StockMgr) context.getBean("stockMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		carMgr = (CarMgr) context.getBean("carMgr");
		productMgr = (ProductMgr)context.getBean("productMgr");
		promotionProgramMgr = (PromotionProgramMgr)context.getBean("promotionProgramMgr");
		shop = getCurrentShop();
		
		if(commonMgr!=null){
			sysDate = commonMgr.getSysDate();
		}
		
		staff = getStaffByCurrentUser();
		if(staff!=null && staff.getShop()!=null){
			shop = staff.getShop();
			shopCode = shop.getShopCode();
		}			
		
		lockDay = shopLockMgr.getNextLockedDay(shop.getId());
		if(lockDay == null){
			lockDay = sysDate;
		}
		
	}


	@Override
	public String execute() throws Exception {
		listCustomer = customerMgr.getListCustomer4CreateOrder(shop.getId());
		
		ObjectVO<Staff> nvbhVO = staffMgr.getListPreAndVanStaff(null, null, null, shop.getId(), ActiveType.RUNNING, Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
		listNVBH = nvbhVO.getLstObject();
		
		StaffFilter staffFilter = new StaffFilter();
		staffFilter.setStatus(ActiveType.RUNNING);
		staffFilter.setShopCode(shop.getShopCode());
		staffFilter.setStaffType(StaffObjectType.NVGH);
		staffFilter.setIsGetShopOnly(true);
		ObjectVO<Staff> nvghVO = staffMgr.getListStaff(staffFilter);
		listNVGH = nvghVO.getLstObject();
		
		staffFilter = new StaffFilter();
		staffFilter.setStatus(ActiveType.RUNNING);
		staffFilter.setShopCode(shop.getShopCode());
		staffFilter.setStaffType(StaffObjectType.NVTT);
		staffFilter.setIsGetShopOnly(true);
		ObjectVO<Staff> nvttVO = staffMgr.getListStaff(staffFilter);
		listNVTT = nvttVO.getLstObject();
		
		
		ObjectVO<Car> lstCarsVO = carMgr.getListCar(null, shop.getId(), null, null, null, null, null, null, ActiveType.RUNNING, false);	
		if(lstCarsVO!=null){
			listCar = lstCarsVO.getLstObject();
		}	
		
		/** ALLOW_EDIT_PROMOTION,AP_PARAM_CODE = 1 : Cau hinh cho phep thay doi trong grid san pham KM */
		List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.ALLOW_EDIT_PROMOTION, ActiveType.RUNNING);
		if(aparams!=null && aparams.size()==1 && aparams.get(0).getApParamCode().equals("1")){
			allowEditPromotion = 1;
		}else{
			allowEditPromotion = 0;
		}
		
		listPriority = apParamMgr.getListApParam(ApParamType.ORDER_PIRITY, ActiveType.RUNNING);	
		if(listPriority==null){
			listPriority = new ArrayList<ApParam>();
		}
		
		/** FSXN=0 : Cau hinh chon lo bang tay */
		ApParam apParam = apParamMgr.getApParamByCode("FSXN",ApParamType.SALE_ORDER_APPROVED);
		if(apParam!=null){
			fsxn=apParam.getValue();
		}
		return SUCCESS;
	}
	public String customerDetails(){
		resetToken(result);
		try{
			if (currentShop == null || currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			/** Thong tin khach hang */
			if(shop==null){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.shop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), customerCode));			
			if(customer==null || customer.getShop()==null || !customer.getShop().getId().equals(shop.getId())){
				result.put(ERROR, true);			
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty")));
				return JSON;
			}else if(!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
				result.put(ERROR, true);				
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty") 
						+ " - " + customerCode));
				return JSON;
			}
			if(StringUtil.isNullOrEmpty(customer.getCustomerName())){
				result.put("customerName","");
			}else{
				result.put("customerName", customer.getCustomerName());
			}				
			if(StringUtil.isNullOrEmpty(customer.getAddress())){
				result.put("address", "");
			}else{
				result.put("address", customer.getAddress());
			}	
			if(StringUtil.isNullOrEmpty(customer.getPhone())) {
				result.put("channelTypeName", "");
			} else {
				result.put("channelTypeName", customer.getPhone());
			}
			if(StringUtil.isNullOrEmpty(customer.getShortCode())){
				result.put("shortCode","");
			}else{
				result.put("shortCode", customer.getShortCode());
			}
			if(customer.getCashierStaff()==null){
				result.put("cashierStaffCode","");
			}else{
				result.put("cashierStaffCode", customer.getCashierStaff().getStaffCode());
			}		
			
			if(customer.getDeliver() != null) {
				result.put("transferStaffCode",customer.getDeliver().getStaffCode());
			} else {
				result.put("transferStaffCode","");
			}
			if(!StringUtil.isNullOrEmpty(customer.getPhone()) && !StringUtil.isNullOrEmpty(customer.getMobiphone())){
				result.put("phone", customer.getPhone() + " / " + customer.getMobiphone());
			}else if(!StringUtil.isNullOrEmpty(customer.getPhone())){
				result.put("phone", customer.getPhone());
			}else if(!StringUtil.isNullOrEmpty(customer.getMobiphone())){
				result.put("phone", customer.getMobiphone());
			}else{
				result.put("phone","");
			}
			Staff saleStaff = null;
			if(StringUtil.isNullOrEmpty(saleStaffCode)) {
				RoutingCustomerFilter filter = new RoutingCustomerFilter();
				filter.setCustomerId(customer.getId());
				filter.setShopId(shop.getId());
				filter.setOrderDate(lockDay);
				filter.setUserId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				List<Staff> lst = staffMgr.getSaleStaffByDate(filter);
				if (lst != null && lst.size() > 0) {
					saleStaff = lst.get(0);
				}
			}else{
				saleStaff = staffMgr.getStaffByCode(saleStaffCode);
			}
			if(saleStaff==null){
				result.put("staffSaleCode","");
			}else{
				result.put("staffSaleCode",saleStaff.getStaffCode());
			}		
			
			/** Thong tin CT HTTM */			
			ObjectVO<CommercialSupportVO> listCommercialSupportVO = saleOrderMgr.getListCommercialSupport(null, shop.getId(), customer.getId(), lockDay, null);
			if(listCommercialSupportVO!=null){
				result.put("commercials", listCommercialSupportVO.getLstObject());
			}else{
				result.put("commercials", new ArrayList<CommercialSupportVO>());
			}
			/** Lay danh sach san pham*/
			/*
			 * comment code loi compile
			 * @modified by tuannd20
			 * @date 20/12/2014
			 */
			/*if(saleStaff != null){
				List<OrderProductVO> listOrderProduct = saleOrderMgr.getListOrderProductVO(null, shop.getId(), saleStaff.getId(), customer.getId(), null, null, null,lockDay).getLstObject();
				if(listOrderProduct != null){
					result.put("listSaleProduct", listOrderProduct);
				}else{
					result.put("listSaleProduct", new ArrayList<OrderProductVO>());
				}
			}*/
			result.put(ERROR, false);
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, false);
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
			result.put("errMsg", errMsg);
		}
		return JSON;
	}
	public String customerSaleData() {
		try {
			if (currentShop == null || currentUser == null|| currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			
			Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), shortCode));
			if(null == customer) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, shortCode));
				return JSON;
			}
			
			List<CommercialSupportVO> listCS = saleOrderMgr.getListCommercialSupport(null, shop.getId(), customer.getId(), lockDay, null).getLstObject();
			
			result.put("listCS", listCS);
			
			Staff saleStaff = null;
			
			if(StringUtil.isNullOrEmpty(saleStaffCode)) {
				RoutingCustomerFilter filter = new RoutingCustomerFilter();
				filter.setCustomerId(customer.getId());
				filter.setShopId(shop.getId());
				filter.setOrderDate(lockDay);
				filter.setUserId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				List<Staff> lst = staffMgr.getSaleStaffByDate(filter);
				if (lst != null && lst.size() > 0) {
					saleStaff = lst.get(0);
				}
				result.put("lstStaffSale",lst);
			} else {
				saleStaff = staffMgr.getStaffByCode(saleStaffCode);
				result.put("lstStaffSale",Arrays.asList(saleStaff));
			}
			
			if(saleStaff != null) {
				result.put("saleStaff", saleStaff);
			}
			
			if(saleStaff != null) {
				/*
				 * comment code loi compile
				 * @modified by tuannd20
				 * @date 20/12/2014
				 */
				/*List<OrderProductVO> listOrderProduct = saleOrderMgr.getListOrderProductVO(null, shop.getId(), saleStaff.getId(), customer.getId(), null, null, null,lockDay).getLstObject();
				if(listOrderProduct != null){
					mapProductLot = new HashMap<String, List<ProductLot>>();
					for(OrderProductVO pro: listOrderProduct){
						List<ProductLot> lstProLot = getProductLotEx(pro.getProductId());
						if(lstProLot != null && lstProLot.size() > 0){
							mapProductLot.put(pro.getProductCode(), getProductLotEx(pro.getProductId()));
						}
					}
				}
				result.put("listSaleProduct", listOrderProduct);*/
				result.put("mapProductLot", mapProductLot);
			}
			
			if(customer.getDeliver() != null) {
				result.put("deliveryStaff", customer.getDeliver());
			}
			
			if(customer.getCashierStaff() != null) {
				result.put("cashierStaff", customer.getCashierStaff());
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
			//LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public String pay() {
		errMsg = "";
		try{
			if(staff==null){				
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.shop.is.null")));
				result.put(ERROR, true);
				return JSON;
			}
			if(StringUtil.isNullOrEmpty(customerCode)){				
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty")));
				result.put(ERROR, true);
				return JSON;
			}
			Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), customerCode));
			if(customer==null){			
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.null")));
				result.put(ERROR, true);
				return JSON;
			}
			if(customer.getStatus()!=ActiveType.RUNNING){
				result.put(ERROR, true);				
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS,
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty") 
						+ " - " + customerCode));
				return JSON;
			}
			List<CommercialSupportCodeVO> lstCommercialSupportCodeVOs = new ArrayList<CommercialSupportCodeVO>();
			List<Boolean> isExists = new ArrayList<Boolean>();
			CommercialSupportCodeVO vos;
			if(listCS!=null){
				String TYPE = "";
				for(int i=0;i<listCS.size();++i){
					if(!StringUtil.isNullOrEmpty(listCS.get(i))){
						vos = new CommercialSupportCodeVO();						
						vos.setCode(listCS.get(i));
						vos.setCustomerId(customer.getId());
						vos.setShopId(shop.getId());
						TYPE = listCSType.get(i);							
						if(StringUtil.isNullOrEmpty(TYPE)){
							vos.setType(CommercialSupportType.DISPLAY_PROGRAM);
						}else{
							vos.setType(CommercialSupportType.PROMOTION_PROGRAM);
						}
						Product product = productMgr.getProductByCode(listProductCode.get(i));
						vos.setProductId(product.getId());
						lstCommercialSupportCodeVOs.add(vos);
					}					
				}
				if (lstCommercialSupportCodeVOs.size() > 0) {
					if (lockDay!= null) {
						isExists = saleOrderMgr.checkIfCommercialSupportExists(lstCommercialSupportCodeVOs, (lockDay),null);
					} else {
						isExists = saleOrderMgr.checkIfCommercialSupportExists(lstCommercialSupportCodeVOs, DateUtil.now(),null);
					}
				}			
				for (int i=0;i<isExists.size();++i) {
					if (!isExists.get(i)) {						
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION,	listCS.get(i)));			
						return JSON;					
					}
				}
			}				
			/** Thong tin san pham ban */			
			List<SaleOrderDetailVO> lstSalesOrderDetailVO = new ArrayList<SaleOrderDetailVO>();
			Product product = null;
			SaleOrderDetail saleOrderDetail = null;
			BigDecimal totalAmount = BigDecimal.ZERO;				
			int MANUAL_PROM = 0,SALE_PRODUCT=0;		
			String commercialType = "";
			
			/** Danh sach san pham ban nhap vao  */
			List<CommercialSupportVO> commercials =new ArrayList<CommercialSupportVO>();
			if (listProductCode == null) {
				listProductCode = new ArrayList<String>();
			}
			for (int i=0;i<listProductCode.size();++i) {
				SaleOrderDetailVO sale_order_vos = new SaleOrderDetailVO();
				product = productMgr.getProductByCode(listProductCode.get(i));
				sale_order_vos.setProduct(product);							
				saleOrderDetail = new SaleOrderDetail();
				saleOrderDetail.setProduct(product);
				saleOrderDetail.setPrice(productMgr.getPriceByProductId(product.getId()));
				saleOrderDetail.setPriceValue(saleOrderDetail.getPrice().getPrice());
				saleOrderDetail.setShop(shop);
				saleOrderDetail.setStaff(staff);
				saleOrderDetail.setOrderDate(lockDay);
				
				if(listQuantityInt.get(i)==null || listQuantityInt.get(i)<0){
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
					result.put(ERROR, true);
					return JSON;
				}			
				saleOrderDetail.setQuantity(listQuantityInt.get(i));
				sale_order_vos.setSaleOrderDetail(saleOrderDetail);								
				/** Khong tin tien voi san pham co kem CTHTTM */
				if(StringUtil.isNullOrEmpty(listCS.get(i))){	
					BigDecimal temp = saleOrderDetail.getPrice().getPrice().multiply(new BigDecimal(listQuantityInt.get(i)));
					totalAmount =  totalAmount.add(temp);					
					sale_order_vos.setProduct(product); 
					++SALE_PRODUCT;
					lstSalesOrderDetailVO.add(sale_order_vos);	
				}else{
					commercialType = listCSType.get(i);
					if(commercials!=null){						
						for(CommercialSupportVO cs :commercials){		
							if(cs.getPromotionType()==null){
								continue;
							}
							if(listCS.get(i).equals(cs.getCode())&& cs.getPromotionType().equals(commercialType)){
								if(cs.getPromotionType().startsWith("ZM")){									
									++MANUAL_PROM;
								}								
							}
						}						
					}	
				}	
			}			
//			if(MANUAL_PROM>0 && SALE_PRODUCT==0){
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FIELD,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.saleproduct.productpromotion")));
//				result.put(ERROR, true);				
//				return JSON;
//			}						
			PromotionProductsVO productsVO = null;
			PromotionProductsVO productsZV19_21 = null;
			/*
			 * comment code loi compile
			 * @modified by tuannd20
			 * @date 20/12/2014
			 */
			/*// KM cho ZV1 - ZV18
			productsVO = promotionProgramMgr.getListPromotionProducts(lstSalesOrderDetailVO,shop.getId(),customer.getId(), lockDay,null);
			// KM cho ZV19 - ZV21		
			productsZV19_21 = promotionProgramMgr.calPromotionForOrder(totalAmount, shop.getId(), customer.getId(), new Date(), null);
			List<SaleOrderDetailVO> list = new ArrayList<SaleOrderDetailVO>();	
			if(productsVO==null){
				productsVO = new PromotionProductsVO();
				productsVO.setListProductPromotionSale(list);						
			}
			calcZV19_21(productsZV19_21);		// Tich hop 2 FUNCTION tinh khuyen mai lai thanh 1
			if(productsVO!=null){
				result.put("pp",productsVO.getListProductPromotionSale());				
				//if(key!=null && key>0){	
					result.put("changePPromotionZV01_18",productsVO.getSortListOutPut());
					result.put("changePPromotionZV21",productsZV19_21.getSortListOutPut());
					boolean canChaneMultiProduct = true;
					String promotionProgramTypeCode = "";
					Integer quantityChange = 0;
					if (changePPromotion != null && changePPromotion.size() > 0) {
						for(SaleOrderDetailVO sodVO : changePPromotion) {
							if("".equals(promotionProgramTypeCode) && quantityChange == 0) {
								promotionProgramTypeCode = sodVO.getSaleOrderDetail().getProgrameTypeCode();
								quantityChange = sodVO.getSaleOrderDetail().getQuantity();
							} else if(!promotionProgramTypeCode.equals(sodVO.getSaleOrderDetail().getProgrameTypeCode())
									||!quantityChange.equals(sodVO.getSaleOrderDetail().getQuantity())) {
								canChaneMultiProduct = false;
							}
						}
					}
					result.put("canChaneMultiProduct", canChaneMultiProduct);
				//}
			}*/		
			result.put(ERROR, false);
		}catch(Exception ex){
			result.put(ERROR, true);
			LogUtility.logError(ex, ex.getMessage());
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
		}	        	    
	    result.put("errMsg", errMsg);
		return JSON;
	}
	
	public void calcZV19_21(PromotionProductsVO productsZV19_21) throws Exception{
		if(productsZV19_21!=null && productsZV19_21.getListProductPromotionSale()!=null){
			for(SaleOrderDetailVO vo:productsZV19_21.getListProductPromotionSale()){
				if(productsZV19_21.getListPromotionForOrderChange()!=null && productsZV19_21.getListPromotionForOrderChange().size()>1){
					vo.setChangeProduct(1);
					if(vo.getPromotionType()==ConstantManager.PROMOTION_FOR_ZV21){						
						vo.setKeyList(-1L);						
						vo.setChangePPromotion(productsZV19_21.getSortListOutPut().get(-1L));
					}else{
						vo.setKeyList(-2L);
						vo.setChangePPromotion(productsZV19_21.getSortListOutPut().get(-2L));
					}
					List<PromotionProgram> pp = new ArrayList<PromotionProgram>();
					List<SaleOrderDetailVO> zv19Zv20 = new ArrayList<SaleOrderDetailVO>();
					for(int i=0;i<productsZV19_21.getListPromotionForOrderChange().size();++i){
						SaleOrderDetailVO order = productsZV19_21.getListPromotionForOrderChange().get(i);
						String ppCode = order.getSaleOrderDetail().getProgramCode();
						PromotionProgram p = promotionProgramMgr.getPromotionProgramByCode(ppCode);						
						if(order.getPromotionType()==ConstantManager.PROMOTION_FOR_ZV21){
							result.put("zV21C", order);							
						}else{
							zv19Zv20.add(order);
							if(order.getPromotionType()==ConstantManager.PROMOTION_FOR_ORDER && order.getType()==ConstantManager.FREE_PERCENT){
								p.setType("ZV19");
							}if(order.getPromotionType()==ConstantManager.PROMOTION_FOR_ORDER && order.getType()==ConstantManager.FREE_PRICE){
								p.setType("ZV20");
							}
						}
						pp.add(p);
					}	
					result.put("programes", pp);
					result.put("zV19zV20C", zv19Zv20);
				}
				if(vo.getPromotionType()==ConstantManager.PROMOTION_FOR_ZV21){
					result.put("zV21", vo);
				}else {
					result.put("zV19zV20", vo);
				}
			}
		}
	}
	
	public String getProductLot(){
		try {
			/** typeView : true : Lay thong tin lo huy	 */
			Product info=productMgr.getProductByCode(productCode);	
			List<ProductLot> listProductLot = new ArrayList<ProductLot>();
			List<ProductLot> __listProductLot = new ArrayList<ProductLot>();
			if(info!=null){
				listProductLot=productMgr.getProductLotByProductAndOwner(info.getId(), shop.getId(), StockObjectType.SHOP);
			}else{
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST);
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}			
			Integer NUM_DAY_LOT_EXPIRE  = 0;				
			ApParam ap = apParamMgr.getApParamByCodeEx(ConstantManager.AP_PARAM_CODE_NUM_DAY_LOT_EXPIRE, ActiveType.RUNNING);
			if(ap!=null){
				NUM_DAY_LOT_EXPIRE = Integer.valueOf(ap.getValue());
			}			
			for(ProductLot pl: listProductLot){				
				if(NUM_DAY_LOT_EXPIRE>=0){
					Date moveDate = DateUtil.moveDate(DateUtil.now(), NUM_DAY_LOT_EXPIRE, DateUtil.DATE_MONTH);
					if(pl.getExpiryDate()!=null && DateUtil.compareDateWithoutTime(pl.getExpiryDate(), moveDate)>=0){
						__listProductLot.add(pl);
					}
				}else{
					__listProductLot.add(pl);
				}
			}
			result.put("list", __listProductLot);
			if(__listProductLot.size()==0){
				result.put("lotExpire", true);
			}else{
				result.put("lotExpire", false);
			}
			result.put("productCode", info.getProductCode());
			result.put("productName", info.getProductName());					
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex,"AdjustmentOrderAction.getOrderDetailLot");
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	public List<ProductLot> getProductLotEx(Long id) throws Exception{
		List<ProductLot> listProductLot = new ArrayList<ProductLot>();
		List<ProductLot> __listProductLot = new ArrayList<ProductLot>();
		listProductLot=productMgr.getProductLotByProductAndOwner(id, shop.getId(), StockObjectType.SHOP);
		Integer NUM_DAY_LOT_EXPIRE  = 0;				
		ApParam ap = apParamMgr.getApParamByCodeEx(ConstantManager.AP_PARAM_CODE_NUM_DAY_LOT_EXPIRE, ActiveType.RUNNING);
		if(ap!=null){
			NUM_DAY_LOT_EXPIRE = Integer.valueOf(ap.getValue());
		}			
		for(ProductLot pl: listProductLot){				
			if(NUM_DAY_LOT_EXPIRE>=0){
				Date moveDate = DateUtil.moveDate(DateUtil.now(), NUM_DAY_LOT_EXPIRE, DateUtil.DATE_MONTH);
				if(pl.getExpiryDate()!=null && DateUtil.compareDateWithoutTime(pl.getExpiryDate(), moveDate)>=0){
					__listProductLot.add(pl);
				}
			}else{
				__listProductLot.add(pl);
			}
		}
		return __listProductLot;
	}
	
	public String createSaleOrder(){
		result.put("flag", false);
		try{	
			if (currentShop == null || currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			if(!validateGeneralInfor()){
				return JSON;
			}
			/** Thong tin nhan vien giao hang */
			Staff deliveryStaff = staffMgr.getStaffByCodeAndShopId(deliveryCode,shop.getId());
//			if(!checkStaff(deliveryStaff,StaffObjectType.NVGH)){
//				return JSON;
//			}
			/** Thong tin nhan vien thu tien */
			Staff cashierStaff = staffMgr.getStaffByCodeAndShopId(cashierCode,shop.getId()); 
//			if(!checkStaff(cashierStaff,StaffObjectType.NVTT)){
//				return JSON;
//			}
			/** Thong tin nhan vien ban hang */
			Staff salerStaff = staffMgr.getStaffByCode(salerCode);
			if(!checkStaff(salerStaff,StaffObjectType.NVBH) && !checkStaff(salerStaff, StaffObjectType.NVVS)){
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SS_NOT_GENERAL, salerStaff.getStaffCode(), StaffObjectType.NVBH.toString()));
				result.put(ERROR, true);
				return JSON;
			}
			
						
			/**  Kiem tra su ton tai va hieu luc cua chuong trinh ho tro thuong mai	
			Bao gom CTKM bang tay,huy doi tra hang, ct trung bay */
			List<CommercialSupportCodeVO> lstCommercialSupportCodeVOs = new ArrayList<CommercialSupportCodeVO>();
			List<Boolean> isExists = new ArrayList<Boolean>();
			CommercialSupportCodeVO vos;
			if(lstCommercialSupport!=null){
				String TYPE = "";
				for(int i=0;i<lstCommercialSupport.size();++i){
					if(!StringUtil.isNullOrEmpty(lstCommercialSupport.get(i))){
						vos = new CommercialSupportCodeVO();						
						vos.setCode(lstCommercialSupport.get(i));
						vos.setCustomerId(customer.getId());
						vos.setShopId(shop.getId());
						TYPE = lstCommercialSupportType.get(i);							
						if(StringUtil.isNullOrEmpty(TYPE)){
							vos.setType(CommercialSupportType.DISPLAY_PROGRAM);
						}else{
							vos.setType(CommercialSupportType.PROMOTION_PROGRAM);
						}
						Product product = productMgr.getProductByCode(lstProduct.get(i));
						vos.setProductId(product.getId());
						lstCommercialSupportCodeVOs.add(vos);
					}					
				}
				if (lstCommercialSupportCodeVOs.size() > 0) {
					isExists = saleOrderMgr.checkIfCommercialSupportExists(lstCommercialSupportCodeVOs, lockDay,null);
				}			
				for (int i=0;i<isExists.size();++i) {
					if (!isExists.get(i)) {						
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION,	lstCommercialSupport.get(i)));			
						return JSON;					
					}
				}
			}		
			/**  Thong tin hoa don ban hang */
			SaleOrder saleOrder = new SaleOrder();			
			saleOrder.setShop(shop);
			saleOrder.setCustomer(customer);
			saleOrder.setCar(car);				
			if(priorityId!=0) {
				saleOrder.setPriority(priorityId);
			}			
			if(!StringUtil.isNullOrEmpty(deliveryDate)){
				Date date = DateUtil.parse(deliveryDate, ConstantManager.FULL_DATE_FORMAT);
				if(date!=null)
					saleOrder.setDeliveryDate(date);
			}
			
			saleOrder.setStaff(salerStaff);
			saleOrder.setDelivery(deliveryStaff);
			saleOrder.setCashier(cashierStaff);
			/** Quy doi ve thong tin nho nhat */
			saleOrder.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
			saleOrder.setOrderType(OrderType.IN);
//			saleOrder.setVat(Float.valueOf("10"));
			saleOrder.setOrderSource(SaleOrderSource.WEB);
			
			/**Kiem tra don hang trong tuyen va ngoai tuyen
			Tim duoc nhan vien ban hang thi co nghia la trong tuyen IS_VISIT_PLAN =1,nguoc lai thi ngoai tuyen IS_VISIT_PLAN =0 */			
			Staff sStaff = null;
			RoutingCustomerFilter filter = new RoutingCustomerFilter();
			filter.setCustomerId(customer.getId());
			filter.setShopId(shop.getId());
			filter.setOrderDate(lockDay);
			filter.setUserId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			List<Staff> lst = staffMgr.getSaleStaffByDate(filter);
			if (lst != null && lst.size() > 0) {
				sStaff = lst.get(0);
			}
			if(sStaff!=null && sStaff.getStaffCode().equals(salerStaff.getStaffCode())){
				saleOrder.setIsVisitPlan(1);
			}else{
				saleOrder.setIsVisitPlan(0);
			}						
			saleOrder.setCreateUser(staff.getStaffCode());
			saleOrder.setOrderDate(lockDay);
			/**Danh sach san pham ban */
			List<SaleProductVO> lstSaleProductVO = new ArrayList<SaleProductVO>();
			Map<String, Integer> indexOfSaleProduct = new HashMap<String, Integer>();
			/** Danh sach san pham khuyen mai */
			List<SaleProductVO> lstPromoProductVO = new ArrayList<SaleProductVO>();
			/** Danh sach san pham HUY */
			List<SaleOrderLot> lstSaleOrderLot = new ArrayList<SaleOrderLot>();
			
			Product product;
			SaleOrderDetail saleOrderDetail = null;			
			SaleProductVO productVO = null;
			SaleOrderLot saleOrderLot = null;
			/**Lay thong tin danh sach san pham ban
			Tu do lay danh sach cac san pham khuyen mai duoc ap dung cho don hang
			luu thong tin bao gom hoa don va cac thong tin ve san pham ban, san pham khuyen mai */
						
			PromotionProgram program = null;			
			ObjectVO<CommercialSupportVO> commercials =saleOrderMgr.getListCommercialSupport(null, shop.getId(), customer.getId(), lockDay,null);			
			
			int MANUAL_PROM = 0,SALE_PRODUCT=0;		
			String commercialType = "";
			BigDecimal totalAmountMoneyBuy = BigDecimal.ZERO;
			for (int i=0;i<lstProduct.size();++i) {
				productVO = new SaleProductVO();
				lstSaleOrderLot = new ArrayList<SaleOrderLot>();
				product = productMgr.getProductByCode(lstProduct.get(i));				
				saleOrderDetail = new SaleOrderDetail();
				saleOrderDetail.setProduct(product);//san pham ban
				saleOrderDetail.setPrice(productMgr.getPriceByProductId(product.getId()));//Gia san pham
				saleOrderDetail.setPriceValue(saleOrderDetail.getPrice().getPrice());
				saleOrderDetail.setPriceNotVat(saleOrderDetail.getPrice().getPriceNotVat());
				saleOrderDetail.setShop(shop);//Nha phan phoi	
				saleOrderDetail.setVat(saleOrderDetail.getPrice().getVat());
				saleOrderDetail.setStaff(salerStaff);
				
				/** Kiem tra so luong nhap va ton kho dap ung */
				if(lstQuantity.get(i)==null || lstQuantity.get(i)<0){
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
					result.put(ERROR, true);					
					return JSON;
				}				
				saleOrderDetail.setQuantity(lstQuantity.get(i));
				/** Kiem tra shop da chot ngay hay chua
				 *  Neu co thi luu ngay don hang la ngay chot
				 * */
				saleOrderDetail.setOrderDate(lockDay);
				saleOrderDetail.setCreateDate(sysDate);
				saleOrderDetail.setCreateUser(staff.getStaffCode());
				saleOrderDetail.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(lstQuantity.get(i))));				
				/** Danh sach san pham ban */
				if(lstCommercialSupport!=null && StringUtil.isNullOrEmpty(lstCommercialSupport.get(i))){	
					saleOrderDetail.setIsFreeItem(0); 
					// 10-03-2014 - nhanlt6: fix bug 0000155 luu cac CTHTTM cua san pham mua
					if (lstBuyProductCommercialSupportProgram!= null && !StringUtil.isNullOrEmpty(lstBuyProductCommercialSupportProgram.get(i))) {
						saleOrderDetail.setProgramCode(lstBuyProductCommercialSupportProgram.get(i));
						if (!StringUtil.isNullOrEmpty(lstBuyProductCommercialSupportProgramType.get(i))) {
							if ("ZV".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.AUTO_PROM);
							} else if ("MANUAL_PROM".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.MANUAL_PROM);
							} else if ("DESTROY".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.DESTROY);
							} else if ("PRODUCT_EXCHANGE".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.PRODUCT_EXCHANGE);
							} else if ("RETURN".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.RETURN);
							} else if ("DISPLAY_SCORE".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
								saleOrderDetail.setProgramType(ProgramType.DISPLAY_SCORE);
							}
						}
					}
					String detail = lstProductSaleLot.get(i);
					if(!StringUtil.isNullOrEmpty(detail)){
						String detailProductLot = detail.split(";")[0];
						product = productMgr.getProductByCode(detailProductLot);
						if(product != null && product.getCheckLot()==1){
							Price price = productMgr.getPriceByProductId(product.getId());
							for(int j =1;j<detail.split(";").length;++j){
								saleOrderLot = new SaleOrderLot();
								saleOrderLot.setProduct(product);	
								if(price!=null){
									saleOrderLot.setPriceValue(price.getPrice());																					
								}
								String lotDetail = detail.split(";")[j];
								if(!StringUtil.isNullOrEmpty(lotDetail)){
									String lot = lotDetail.split(",")[0];
									String avaiable_quantity = lotDetail.split(",")[1];
									List<ProductLot> productLots = productMgr.getProductLotByProductAndOwner(product.getId(),shop.getId(), StockObjectType.SHOP);
									/** Lo san pham khong ton tai */
									if(productLots==null || productLots.size()==0){										
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.lot") + "  " + product.getProductCode() + " - " + product.getProductName()));
										result.put(ERROR, true);
										return JSON;										
									}
									for(ProductLot productLot:productLots){
										if(productLot.getLot().equals(lot)){											
											saleOrderLot.setLot(lot);
											saleOrderLot.setQuantity(Integer.parseInt(avaiable_quantity));
											saleOrderLot.setExpirationDate(productLot.getExpiryDate());
										}
										
									}
									if(saleOrderLot.getLot()==null ||saleOrderLot.getQuantity()==null || saleOrderLot.getExpirationDate()==null ){
										result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_DESTROY2,product.getProductCode() + " - " + product.getProductName()));
										result.put(ERROR, true);
										return JSON;										
									}
									saleOrderLot.setCreateUser(staff.getStaffCode());									
									saleOrderLot.setShop(shop);
									saleOrderLot.setStaff(salerStaff);									
									saleOrderLot.setOrderDate(lockDay);
									saleOrderLot.setSaleOrder(saleOrder);
									saleOrderLot.setSaleOrderDetail(saleOrderDetail);									
									lstSaleOrderLot.add(saleOrderLot);
								}
							}
							if(lstSaleOrderLot.size()>0){
								productVO.setLstSaleOrderLot(lstSaleOrderLot);									
							}
						}
						
					}
					totalAmountMoneyBuy = totalAmountMoneyBuy.add(saleOrderDetail.getPrice().getPrice().multiply(new BigDecimal(lstQuantity.get(i))));
					++SALE_PRODUCT;
				}else{						
					saleOrderDetail.setIsFreeItem(1); 
					saleOrderDetail.setAmount(BigDecimal.ZERO);
					saleOrderDetail.setProgramCode(lstCommercialSupport.get(i));	
					commercialType = lstCommercialSupportType.get(i);	
					ProgramType type = null;
					if(commercialType.startsWith("ZM")){
						type = ProgramType.MANUAL_PROM;
					}else if(commercialType.startsWith("ZD")){
						type = ProgramType.PRODUCT_EXCHANGE;
					}else if(commercialType.startsWith("ZH")){
						type = ProgramType.DESTROY;
					}else if(commercialType.startsWith("ZT")){
						type = ProgramType.RETURN;
					}else{
						type = ProgramType.DISPLAY_SCORE;
					}
					saleOrderDetail.setProgramType(type);
					saleOrderDetail.setProgrameTypeCode(commercialType);
					/** Kiem tra thong tin chuong trinh HTTM */
					if(commercials!=null){						
						for(CommercialSupportVO cs :commercials.getLstObject()){
							if(StringUtil.isNullOrEmpty(cs.getPromotionType())){
								continue;
							}
							if(lstCommercialSupport.get(i).equals(cs.getCode())&& cs.getPromotionType().equals(commercialType)){	
								/** HTTM : Khuyen mai bang tay*/
								if(cs.getPromotionType().startsWith("ZM")){									
									++MANUAL_PROM;
								}								
								/** HTTM: HUY HANG va HANG co quan ly theo lo. Thuc hien tach lo cho viec huy hang */
								if(!cs.getPromotionType().startsWith("ZH")){	
									continue;
								}
								String detail = lstProductDestroy.get(i);	
								if(StringUtil.isNullOrEmpty(detail)){
									continue;
								}																
								String detailProductLot = detail.split(";")[0];
								product = productMgr.getProductByCode(detailProductLot);
								if(product==null){
									continue;
								}
								if(product.getCheckLot()==0){
									continue;
								}
								/** Kiem tra so lo nhap va tong chi tiet lo */
								Price price = productMgr.getPriceByProductId(product.getId());
								/** Tung buoc tach lo cho san pham */
								for(int j =1;j<detail.split(";").length;++j){
									saleOrderLot = new SaleOrderLot();
									saleOrderLot.setProduct(product);											
									if(price!=null){
										saleOrderLot.setPriceValue(price.getPrice());																					
									}
									String lotDetail = detail.split(";")[j];
									if(StringUtil.isNullOrEmpty(lotDetail)){
										continue;
									}
									String lot = lotDetail.split(",")[0];
									String avaiable_quantity = lotDetail.split(",")[1];									
									List<ProductLot> productLots = productMgr.getProductLotByProductAndOwnerEx(product.getId(),shop.getId(), StockObjectType.SHOP);
									if(productLots==null || productLots.size()==0){										
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.lot") + "  " + product.getProductCode() + " - " + product.getProductName()));
										result.put(ERROR, true);
										return JSON;										
									}
									for(ProductLot productLot:productLots){
										if(productLot.getLot().equals(lot)){											
											saleOrderLot.setLot(lot);
											saleOrderLot.setQuantity(Integer.parseInt(avaiable_quantity));
											saleOrderLot.setExpirationDate(productLot.getExpiryDate());
										}
									}
									if(saleOrderLot.getLot()==null ||saleOrderLot.getQuantity()==null){
										result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_DESTROY2,product.getProductCode() + " - " + product.getProductName()));
										result.put(ERROR, true);
										return JSON;										
									}
									saleOrderLot.setCreateUser(staff.getStaffCode());									
									saleOrderLot.setShop(shop);
									saleOrderLot.setStaff(salerStaff);									
									saleOrderLot.setOrderDate(lockDay);
									saleOrderLot.setSaleOrder(saleOrder);
									saleOrderLot.setSaleOrderDetail(saleOrderDetail);									
									lstSaleOrderLot.add(saleOrderLot);
								}
								if(lstSaleOrderLot.size()>0){
									productVO.setLstSaleOrderLot(lstSaleOrderLot);									
								}
								break;
							}
						}						
					}					
					
				}	
				productVO.setSaleOrderDetail(saleOrderDetail);					
				lstSaleProductVO.add(productVO);
				indexOfSaleProduct.put(product.getProductCode(), lstSaleProductVO.indexOf(productVO));
			}			
//			/** Neu khuyen mai bang tay, thi trong don hang phai co it nhat mot san pham ban
//			 Con neu huy doi tra thi van co the chap nhan khi tao don hang */
//			if(MANUAL_PROM>0 && SALE_PRODUCT==0){
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FIELD,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.saleproduct.productpromotion")));
//				result.put(ERROR, true);
//				return JSON;
//			}
			
			/**  Danh sach san pham khuyen mai tu dong */
			if(lstPromotionCode!=null){
				for(int i=0;i<lstPromotionCode.size();++i){
					program = promotionProgramMgr.getPromotionProgramByCode(lstPromotionCode.get(i));					
					PromotionShopMap promotionShopMap = promotionProgramMgr.checkPromotionShopMap(program.getId(), shop.getShopCode(),ActiveType.RUNNING,lockDay);
					/**Kiem tra so xuat phan bo cho nha phan phoi */
					if(promotionShopMap==null){						
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION_SHOP,program.getPromotionProgramCode()));
						result.put(ERROR, true);result.put("flag", true);
						return JSON;						
					}
					if(promotionShopMap.getQuantityReceived()!=null && promotionShopMap.getQuantityMax()!=null){
						if(promotionShopMap.getQuantityMax()<=promotionShopMap.getQuantityReceived()){								
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION_SHOP_FULL,program.getPromotionProgramCode()));
							result.put("isRePayment", true);result.put(ERROR, true);								
							return JSON;
						}
					}
					SaleProductVO productPromotionVO = new SaleProductVO();
					saleOrderDetail = new SaleOrderDetail();						
					/** Thong tin chung cho san pham khuyen mai */
					saleOrderDetail.setStaff(salerStaff);
					saleOrderDetail.setShop(shop);
					saleOrderDetail.setOrderDate(lockDay); 
					saleOrderDetail.setCreateDate(sysDate);
					saleOrderDetail.setCreateUser(staff.getStaffCode());
					saleOrderDetail.setIsFreeItem(1);
					saleOrderDetail.setProgramType(ProgramType.AUTO_PROM);
					saleOrderDetail.setProgramCode(program.getPromotionProgramCode());
					saleOrderDetail.setProgrameTypeCode(program.getType());
					/** KM hang */
					if(lstPromotionType.get(i)==1){
						if(!StringUtil.isNullOrEmpty(lstPromotionProduct.get(i))){								
							product = productMgr.getProductByCode(lstPromotionProduct.get(i)); 							
							saleOrderDetail.setProduct(product);								
							saleOrderDetail.setPrice(productMgr.getPriceByProductId(product.getId()));
							if(saleOrderDetail.getPrice()!=null){
								saleOrderDetail.setPriceValue(saleOrderDetail.getPrice().getPrice());
								saleOrderDetail.setPriceNotVat(saleOrderDetail.getPrice().getPriceNotVat());
							}
							String __promoProductQuatity = lstPromotionProductQuatity.get(i);
							Integer promoProductQuatity = Integer.parseInt(__promoProductQuatity);
							
							String __promoProductMaxQuatity = lstPromotionProductMaxQuatity.get(i);
							String __promoProductMaxQuatityCk = lstPromotionProductMaxQuatityCk.get(i);
							Integer promoProductMaxQuatity = Integer.parseInt(__promoProductMaxQuatity);
							Integer promoProductMaxQuatityCk = Integer.parseInt(__promoProductMaxQuatityCk);
							saleOrderDetail.setMaxQuantityFree(promoProductMaxQuatity);
//							saleOrderDetail.setMaxQuantityFreeCk(promoProductMaxQuatityCk);
							if(promoProductQuatity<0){
								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
								result.put(ERROR, true);					
								return JSON;
							}							
							saleOrderDetail.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(promoProductQuatity)));
							saleOrderDetail.setQuantity(promoProductQuatity);
							saleOrderDetail.setMaxQuantityFree(promoProductMaxQuatity);
						}
						
						/** Lo san pham KM */
						ApParam apParam = apParamMgr.getApParamByCode("FSXN",null);
						if(lstPromotionProductSaleLot!=null && apParam!=null && apParam.getValue().equals("0") && lstPromotionProductSaleLot.size()>0){
							String detail = lstPromotionProductSaleLot.get(i);
							if(!StringUtil.isNullOrEmpty(detail) && detail.split(";").length > 0){
								String detailProductLot = detail.split(";")[0];
								product = productMgr.getProductByCode(lstPromotionProduct.get(i));
								if(product != null && product.getCheckLot()!=null 
										&& product.getCheckLot()==1 && product.getProductCode().equals(detailProductLot)){
									Price price = productMgr.getPriceByProductId(product.getId());
									List<SaleOrderLot> lstSaleOrderPromotionLot = new ArrayList<SaleOrderLot>(); 
									for(int j =1;j<detail.split(";").length;++j){
										saleOrderLot = new SaleOrderLot();
										saleOrderLot.setProduct(product);	
										if(price!=null){
											saleOrderLot.setPriceValue(price.getPrice());																					
										}
										String lotDetail = detail.split(";")[j];
										if(!StringUtil.isNullOrEmpty(lotDetail)){
											String lot = lotDetail.split(",")[0];
											String avaiable_quantity = lotDetail.split(",")[1];
											List<ProductLot> productLots = productMgr.getProductLotByProductAndOwner(product.getId(),shop.getId(), StockObjectType.SHOP);
											if(productLots==null || productLots.size()==0){												
												result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.lot") + "  " + product.getProductCode() + " - " + product.getProductName()));
												result.put(ERROR, true);
												return JSON;										
											}
											for(ProductLot productLot:productLots){
												if(productLot.getLot().equals(lot)){													
													saleOrderLot.setLot(lot);
													saleOrderLot.setQuantity(Integer.parseInt(avaiable_quantity));
													saleOrderLot.setExpirationDate(productLot.getExpiryDate());
												}												
											}
											if(saleOrderLot.getLot()==null ||saleOrderLot.getQuantity()==null || saleOrderLot.getExpirationDate()==null ){
												result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_DESTROY2,product.getProductCode() + " - " + product.getProductName()));
												result.put(ERROR, true);
												return JSON;										
											}
											saleOrderLot.setCreateUser(currentUser.getUserName());									
											saleOrderLot.setShop(shop);
											saleOrderLot.setStaff(salerStaff);									
											saleOrderLot.setOrderDate(lockDay);
											saleOrderLot.setSaleOrder(saleOrder);
											saleOrderLot.setSaleOrderDetail(saleOrderDetail);									
											lstSaleOrderPromotionLot.add(saleOrderLot);
										}
									}
									if(lstSaleOrderPromotionLot.size()>0){
										productPromotionVO.setLstSaleOrderLot(lstSaleOrderPromotionLot);									
									}
								}
								
							}
						}
					}
					/** KM tien */
					if(lstPromotionType.get(i)==SaleOrderDetailVO.FREE_PRICE){	
						BigDecimal discountAmt = new BigDecimal(lstDiscount.get(i));						
						saleOrderDetail.setDiscountPercent(null);
						saleOrderDetail.setDiscountAmount(discountAmt);
						saleOrderDetail.setMaxAmountFree(new BigDecimal(lstPromotionProductMaxQuatity.get(i)));
						totalAmountMoneyBuy.subtract(discountAmt);
					}
					/** KM % tien  */
					if(lstPromotionType.get(i)==SaleOrderDetailVO.FREE_PERCENT){
						BigDecimal discountAmt = new BigDecimal(lstDiscount.get(i));						
						saleOrderDetail.setDiscountPercent(Float.valueOf(lstDisper.get(i)));
						saleOrderDetail.setDiscountAmount(discountAmt);
						saleOrderDetail.setMaxAmountFree(new BigDecimal(lstPromotionProductMaxQuatity.get(i)));
						totalAmountMoneyBuy.subtract(discountAmt);
					}
					productPromotionVO.setSaleOrderDetail(saleOrderDetail);
					lstPromoProductVO.add(productPromotionVO);
				}
			}
			/*
			 * comment code loi compile
			 * @modified by tuannd20
			 * @date 20/12/2014
			 */
			/*SaleOrder newSO = saleOrderMgr.createSaleOrder(saleOrder, lstSaleProductVO, lstPromoProductVO);
			result.put(ERROR, false);
			result.put("flag", true);
			result.put("saleOrder", newSO);*/
		}catch(BusinessException be){
			result.put(ERROR, true);
			LogUtility.logError(be, "CreateProductOrder.createSaleOrder");
			result.put(ERROR, true);
			if(be.getMessage().equals("LOT_QUANTITY_IS_NOT_ENOUGH")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.quantity.not.enough"));
			}else if(be.getMessage().equals("RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.received.larger.than.max.quantity"));
			}else if(be.getMessage().equals("TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.total.larger.max.debit"));
			}else if(be.getMessage().equals("MAX_DAY_DEBIT")){
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.max.debit.day"));
			}else{
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}			
		}catch(Exception ex){
			result.put(ERROR, true);						
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(ex, "CreateProductOrder.createSaleOrder" + ex.getMessage());
			return JSON;
		}
		return JSON;
	}
	
	private boolean validateGeneralInfor() throws BusinessException{
		if(shop==null){				
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.shop.is.null")));
			result.put(ERROR, true);
			return false;
		}
		if(StringUtil.isNullOrEmpty(customerCode)){				
			result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty")));
			result.put(ERROR, true);
			return false;
		}
		customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, customerCode));
		if(customer==null){
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.null")));
			result.put(ERROR, true);
			return false;
		}
		if(!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
			result.put(ERROR, true);				
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty")+ " - " + customerCode));
			return false;
		}
		CarMgr carMgr = (CarMgr)context.getBean("carMgr");
		if (carId != null) {
			car = carMgr.getCarById(carId);
//			if(car==null){				
//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sp.create.order.car.error.not.found")));
//				result.put(ERROR, true);
//				return false;
//			}
			if(car !=null && !car.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){				
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"sp.create.order.car.error.not.found")));
				result.put(ERROR, true);
				return false;
			}
			if(car !=null && (car.getShop() == null || !car.getShop().getId().equals(shop.getId()))) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_CAR_NOT_BELONG_SHOP));
				result.put(ERROR, true);
				return false;
			}
		}
		return true;
	}
	
	public Boolean checkStaff(Staff staff, StaffObjectType staffObjectType){
		/*if(staff==null){				
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB,staffObjectType.toString()));
			result.put(ERROR, true);
			return false;
		}	
		if(!staff.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){				
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS,staffObjectType.toString()));
			result.put(ERROR, true);
			return false;
		}
		if(staff.getShop() == null || !staff.getShop().getId().equals(shop.getId())) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, staff.getStaffCode(), shop.getShopCode()));
			result.put(ERROR, true);
			return false;
		}
		if(staff.getStaffType() == null || (staff.getStaffType() != null && !staff.getStaffType().getObjectType().equals(staffObjectType.getValue()))) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SS_NOT_GENERAL, staff.getStaffCode(), staffObjectType.toString()));
			result.put(ERROR, true);
			return false;
		}*/
		return true;
	}
	
	public String productDetail(){
		boolean error = true;
		try{			
			if(!StringUtil.isNullOrEmpty(shopCode)){
				if(!StringUtil.isNullOrEmpty(customerCode)){
					customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, customerCode));					
					if(customer!=null){
						Product product = productMgr.getProductByCode(productCode);
						if(product == null){
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "catalog.display.product.code");
						} else {
							result.put("product", product);		
							result.put("price", convertMoney(productMgr.getPriceByProductId(product.getId()).getPrice()));
							/*
							 * comment code loi compile
							 * @modified by tuannd20
							 * @date 20/12/2014
							 */
							/*PromotionProgram ppTemp=promotionProgramMgr.getCurrentPromotionProgramForProductAndCustomer(productCode,customer.getShop().getId(),customer.getId());
							if(ppTemp!=null)
								result.put("promotion",ppTemp.getPromotionProgramCode());
							else
								result.put("promotion",null);*/
							StockTotal stocktotal = stockMgr.getStockTotalByProductCodeAndOwner(product.getProductCode(), StockObjectType.SHOP, shop.getId());
							if(stocktotal !=null){								
								result.put("avaiableQuantity",stocktotal.getAvailableQuantity());
							}else{
								result.put("avaiableQuantity", 0);
							}						
							error = false;
						}
					}else{						
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.null");
					}
				}else{					
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty");
				}
			}else{				 
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.shop.is.null");				
			}
		}catch(Exception ex){
			LogUtility.logError(ex, ex.getMessage());
		}
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }	    	    
	    result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Show promotion program detail.
	 *
	 * @return the string
	 */
	public String showPromotionProgramDetail() {
		boolean error = true;
		try {
			if(shop==null){
				result.put(ERROR, error);
				return JSON;
			}
			PromotionProgram program = promotionProgramMgr.getPromotionProgramByCode(ppCode);			
			if (program != null) {
				result.put("program", program);				
				result.put("fromDate", DateUtil.toDateString(program.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				result.put("toDate", DateUtil.toDateString(program.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				error = false;
			} else {
				error = true;
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
		}		
		result.put(ERROR, error);
		return JSON;
	}
}

package ths.dms.web.action.saleproduct;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.CarMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DisplayProgramVNMMgr;
import ths.dms.core.business.KeyShopMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.SaleOrderPromotionMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.DisplayProgramVNM;
import ths.dms.core.entities.GroupLevel;
import ths.dms.core.entities.KS;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductGroup;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.RptAccumulativePromotionProgram;
import ths.dms.core.entities.RptAccumulativePromotionProgramDetail;
import ths.dms.core.entities.RptCTTLPay;
import ths.dms.core.entities.RptCTTLPayDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.CommercialSupportType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.PromotionType;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.ValueType;
import ths.dms.core.entities.enumtype.WarehouseType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.CalculatePromotionFilter;
import ths.dms.core.entities.filter.CarSOFilter;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.SaleOrderPromotionFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.CarSOVO;
import ths.dms.core.entities.vo.CommercialSupportCodeVO;
import ths.dms.core.entities.vo.CommercialSupportVO;
import ths.dms.core.entities.vo.GroupLevelDetailVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.ProgramCodeVO;
import ths.dms.core.entities.vo.PromotionProductsVO;
import ths.dms.core.entities.vo.RptAccumulativePromotionProgramVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderPromoLotVO;
import ths.dms.core.entities.vo.SaleOrderPromotionDetailVO;
import ths.dms.core.entities.vo.SaleOrderPromotionVO;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.SaleProductVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.WarehouseVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * 
 * The Class CreateProductOrderAction. Action xu ly cac thao tac lien quan chuc
 * nang Tao don hang
 * 
 * @author: nhanlt6
 */
public class CreateProductOrderAction extends AbstractAction {

	private static final long serialVersionUID = -4085026807517904760L;
	//Mgr
	private StockMgr stockMgr;
	private ProductMgr productMgr;
	private CustomerMgr customerMgr;
	private PromotionProgramMgr promotionProgramMgr;
	private SaleOrderMgr saleOrderMgr;
	private StockManagerMgr stockManagerMgr;
	private CarMgr carMgr;
	private SaleOrderPromotionMgr saleOrderPromotionMgr;
	private ShopMgr shopMgr;
	private KeyShopMgr keyShopMgr;
	//Object entity
	private Customer customer;
	private Car car;
	private SaleOrder saleOrder;
	private Shop currentShop;
	//File
	private File excelFile;
	private File excelFileDisplay;
	//String
	private String excelFileDisplayContentType;
	private String excelFileContentType;
	private String customerCode;
	private String staffTypeCode;
	private String code;
	private String name;
	private String productCode;
	private String ppCode;
	private String cashierCode;
	private String deliveryCode;
	private String deliveryDate;
	private String salerCode;
	private String errMsgImport;
	private String fromDate;
	private String toDate;
	private String isUsingZVOrder;
	private String isUsingZVCode;
	private String orderDate;
	private String ksCode;
	//Long
	private Long carId;
	private Long productId;
	private Long saleOrderDetailId;
	private Long key;
	private Long orderId;
	private Long groupLevelId;
	//Integer
	private int priorityId;
	private Integer canChangePromotionQuantity;
	private Integer typeSortKey;
	private Integer isAuthorize;
	private Integer allowEditPromotion;
	private Integer editOrderTablet;
	private Integer isVanSale;
	private Integer isApprovedVanSale;
	private Integer isWarningPrice;
	private Integer isSaveNotCheck;
	//Date
	private Date sysDate;
	private Date lockDay;
	//List
	private List<String> lstProduct;//Danh sach SP ban luu sale_order_detail
	private List<String> lstSaleProductProgramCode;//Danh sach ma CTKM luu truoc khi thuc hien Tinh tien
	private List<String> lstSaleProductDiscountPercent;//Danh sach % CKMH
	private List<String> lstSaleProductDiscountAmount;//Danh sach so tien CKMH
	private List<String> lstBuyProductCommercialSupportProgram;//Danh sach Ma CTKM luu sale_order_detail
	private List<String> lstBuyProductCommercialSupportProgramType;//Danh sach loai CTKM luu sale_order_detail
	private List<String> lstCommercialSupport;//Danh sach ma CTHTTM luu sale_order_detail
	private List<String> lstCommercialSupportType;//Danh sach loai CTHTTM luu sale_order_detail
	private List<String> lstWarehouseBuyProductCommercialSupportProgram;//Danh sach CTKM theo Kho
	private List<String> lstWarehouseBuyProductCommercialSupportProgramType;//Danh sach Loai KM theo Kho
	private List<String> lstWarehouseCommercialSupport;//Danh sach CTHTTM theo Kho
	private List<String> lstWarehouseCommercialSupportType;//Danh sach loai CTHTTM theo Kho 
	private List<String> lstPromotionCode;
	private List<String> lstPromotionProduct;
	private List<String> lstPromotionProductQuatity;
	private List<String> lstPromotionProductQuatityPackage;
	private List<String> lstPromotionProductQuatityRetail;
	private List<String> lstPromoLevel;
	private List<String> lstPromotionProductMaxQuatity;
	private List<String> lstPromotionProductMaxQuatityCk;
	private List<String> lstMyTime;
	private List<String> lstDiscount;//Danh sach CK mat hang
	private List<String> lstDisper;//Danh sach % CK mat hang
	private List<String> lstWarehouseSaleProduct;//Danh sach SP ban luu theo Kho
	private List<String> lstWarehouseId;//Danh sach Kho
	private List<String> lstWarehouseQuantity;//Danh sach So luong SP theo Kho
	private List<String> lstWarehouseQuantityPackage;//Danh sach So thung SP theo Kho
	private List<String> lstWarehouseQuantityRetail;//Danh sach So le SP theo Kho
	private List<String> lstPromoWarehouseId;//Danh sach Kho cua SP KM
	private List<String> lstPromoWarehouseQuantity;//Danh sach So luong SP theo Kho SP KM
	private List<String> lstProductGroupAndLevelGroupId;//Danh sach muc cua SPKM
	private List<Integer> lstQuantity;//Danh sach so luong thuc dat
	private List<Integer> lstQuantityPackage;//Danh sach so luong thung
	private List<Integer> lstQuantityRetail;//Danh sach so luong le
	private List<Integer> lstPromotionType;//Danh sach loai KM
	private List<String> lstProductHasPromotion;//Danh sach SP ban dc huong KMs
	private List<Customer> lstKH;//Danh sach Khach hang
	private List<Staff> lstNVBH;//Danh sach NVBH
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH
	private List<Staff> lstNVTT;//Danh sach NVTT
	private List<StaffVO> lstNVTTVo;//Danh sach NVTT
	private List<Staff> lstNVGH;//Danh sach NVGH
	private List<StaffVO> lstNVGHVo;//Danh sach NVGH
	private List<Car> lstCars;//Danh sach xe
	private List<ApParam> priority;//Do uu tien
	private List<SaleOrderDetailVOEx> order_detail;//Danh sach SP ban
	private List<SaleOrderDetailVOEx> order_detail_promotion;//Danh sach SPKM
	private List<StockTotalVO> productStocks;// Danh sach san pham co the ban cua NPP
	private List<StockTotalVO> productWarehouses;// Danh sach san pham co the ban cua NPP theo kho
	private List<Product> products;// Danh sach san pham nganh hang Z
	private List<OrderProductVO> orderProductVOs;
	private List<WarehouseVO> lstWarehouse;//Danh sach kho
	private Map<Long, List<StockTotalVO>> mapProductByWarehouse;
	private List<SaleOrderPromotionVO> quantityReceivedList;
	private List<SaleOrderPromotionDetailVO> lstSOPromoDetail;
	private List<SaleOrderPromotionDetailVO> lstProgramDatSaleProduct;
	private String programCode;
	private Integer promoLevel;
	private List<Shop> lstShop;//danh sach shop
	//luu gia tren giao dien
	private List<BigDecimal> lstpkPriceValue;
	private List<BigDecimal> lstPriceValue;
	private List<KeyShopVO> lstKeyShop;
	private List<SaleOrderPromoLotVO> lstPromoLot;
	/**
	 * Man hinh chi tiet don hang viewOnly = 1
	 */
	private Integer viewOnly;
	private List<GroupLevelDetailVO> lstCTTL;
	private String shopCodeFilter;//shop don vi
	private Boolean isAllowShowPrice;
	private Boolean isDividualWarehouse;
	private String sysConvertQuantityConfig;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockMgr = (StockMgr) context.getBean("stockMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		stockManagerMgr = (StockManagerMgr) context.getBean("stockManagerMgr");
		carMgr = (CarMgr) context.getBean("carMgr");
		saleOrderPromotionMgr = (SaleOrderPromotionMgr) context.getBean("saleOrderPromotionMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		keyShopMgr = (KeyShopMgr) context.getBean("keyShopMgr");
		try {
			if (commonMgr != null) {
				sysDate = commonMgr.getSysDate();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.execute"), createLogErrorStandard(actionStartTime));
		}
		currentShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		// Kiem tra currentShop da chot ngay hay chua
		// Neu co thi luu ngay don hang la ngay chot
		lockDay = shopLockMgr.getNextLockedDay(currentShop.getId());
		if (lockDay == null) {
			lockDay = sysDate;
		}
		Date orderDateTemp = DateUtil.parse(orderDate, DateUtil.DATE_FORMAT_DDMMYYYY);
		lockDay = orderDateTemp != null ? orderDateTemp : lockDay;

		if (orderId != null && orderId > 0) {
			functionCode = LogInfoVO.FC_SALE_ADJUST_ORDER;
			actionTypeCode = LogInfoVO.ACTION_UPDATE;
		} else {
			functionCode = LogInfoVO.FC_SALE_CREATE_ORDER;
			actionTypeCode = LogInfoVO.ACTION_INSERT;
		}
		List<ApParam> aparamSysWarehouseConfigs = apParamMgr.getListApParam(ApParamType.SYS_WAREHOUSE_CONFIG, ActiveType.RUNNING);
		isDividualWarehouse = (aparamSysWarehouseConfigs == null || aparamSysWarehouseConfigs.size() == 0 || Constant.SYS_WAREHOUSE_CONFIG_DIVIDUAL.equals(aparamSysWarehouseConfigs.get(0).getValue()));
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		try {
			lstCars = new ArrayList<Car>();
			if (currentShop == null) {
				return PAGE_NOT_PERMISSION;
			}
			//load danh sach shop
			ShopFilter shopFilter = new ShopFilter();
			ObjectVO<Shop> lstShopVO = shopMgr.getListShopSpectific(shopFilter);
			if (lstShopVO != null) {
				lstShop = lstShopVO.getLstObject();
			}
			//Load list xe
			ObjectVO<Car> lstCarsVO = carMgr.getListCar(null, currentShop.getId(), null, null, null, null, null, null, ActiveType.RUNNING, false);
			if (lstCarsVO != null) {
				lstCars = lstCarsVO.getLstObject();
			}
			//Load list do uu tien
			priority = apParamMgr.getListApParam(ApParamType.ORDER_PIRITY, ActiveType.RUNNING);
			if (priority == null) {
				priority = new ArrayList<ApParam>();
			}
			//Lay danh sach nhan vien
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				filter.setIsLstChildStaffRoot(true);
			} else {
				filter.setIsLstChildStaffRoot(false);
			}
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setLstShopId(Arrays.asList(currentUser.getShopRoot().getShopId()));
			//Lay NVBH
			filter.setLstObjectType(Arrays.asList(StaffObjectType.NVBH.getValue(), StaffObjectType.NVVS.getValue()));
			ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVBHVo = objVO.getLstObject();

			//Lay nhan vien thu tien & NVGH
			filter.setIsLstChildStaffRoot(false);
			filter.setLstObjectType(Arrays.asList(StaffObjectType.NVGH.getValue(), StaffObjectType.NVTT.getValue()));
			objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVTTVo = objVO.getLstObject();
			lstNVGHVo = objVO.getLstObject();

			// load cau hinh convert quantity 
			List<ApParam> aparamConvertQuantity = apParamMgr.getListApParam(ApParamType.SYS_CONVERT_QUANTITY_CONFIG, ActiveType.RUNNING);
			sysConvertQuantityConfig = Constant.SYS_CONVERT_QUANTITY_CONFIG_ZEROQUANTITY;
			if (aparamConvertQuantity != null && aparamConvertQuantity.size() == 1) {
				sysConvertQuantityConfig = aparamConvertQuantity.get(0).getValue();
			}
			
			// ALLOW_EDIT_PROMOTION,AP_PARAM_CODE = 1 : Cau hinh cho phep thay doi trong grid san pham KM 
			List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.ALLOW_EDIT_PROMOTION, ActiveType.RUNNING);
			allowEditPromotion = 0;
			if (aparams != null && aparams.size() == 1 && "1".equals(aparams.get(0).getValue())) {
				allowEditPromotion = 1;
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.execute"), createLogErrorStandard(actionStartTime));
		}
		isAuthorize = 1;
		return SUCCESS;
	}

	/**
	 */
	public String checkOrderDate() {
		String msg = "";
		try {
			Date oDate = DateUtil.parse(orderDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date currentDate = DateUtil.now();
			Shop shop = shopMgr.getShopByCode(shopCode);
			Date appDate = currentDate;
			if (shop != null) {
				appDate = shopLockMgr.getApplicationDate(shop.getId());
				if (appDate == null) {
					appDate = currentDate;
				}
			}
			if (oDate == null) {
				msg = R.getResource("create_order_order_date_null");
			} else if (DateUtil.compareDateWithoutTime(appDate, oDate) > 0) {
				msg = R.getResource("create_order_order_date_lock_date");
			} else if (DateUtil.compareDateWithoutTime(oDate, currentDate) > 0) {
				msg = R.getResource("create_order_order_date_current_date");
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.checkOrderDate"), createLogErrorStandard(actionStartTime));
			msg = R.getResource("system.error");
		}
		return msg;
	}

	/**
	 * lay ds nv theo shop don vi
	 * 
	 * @author cuonglt3
	 * @return
	 * @throws Exception
	 */
	public String loadStaffByShopCode() throws Exception {
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				return PAGE_NOT_PERMISSION;
			}
			Long staffId = currentUser.getStaffRoot().getStaffId();
			//Lay danh sach nhan vien
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setIsLstChildStaffRoot(false);
			filter.setInheritUserPriv(staffId);
			filter.setStatus(ActiveType.RUNNING.getValue());
			Long shopId = currentUser.getShopRoot().getShopId();
			if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
				Shop shopSpectific = shopMgr.getShopByCode(shopCodeFilter);
				if (shopSpectific != null) {
					shopId = shopSpectific.getId();
				}
			}
			filter.setLstShopId(Arrays.asList(shopId));
			//Lay NVBH
			filter.setObjectType(StaffSpecificType.STAFF.getValue());
			filter.setIsGetChildShop(false);
			filter.setUserId(staffId);
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVBHVo = objVO.getLstObject();

			//Lay nhan vien thu tien & NVGH
			filter.setUserId(null);
			filter.setRoleId(null);
			filter.setIsLstChildStaffRoot(false);
			filter.setObjectType(StaffSpecificType.NVTT.getValue());
			objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVTTVo = objVO.getLstObject();
			filter.setObjectType(StaffSpecificType.NVGH.getValue());
			objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVGHVo = objVO.getLstObject();

			// ALLOW_EDIT_PROMOTION,AP_PARAM_CODE = 1 : Cau hinh cho phep thay doi trong grid san pham KM 
			List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.ALLOW_EDIT_PROMOTION, ActiveType.RUNNING);
			if (aparams != null && aparams.size() == 1 && aparams.get(0).getApParamCode().equals("1")) {
				allowEditPromotion = 1;
			} else {
				allowEditPromotion = 0;
			}
			//cau hinh show gia hay khong
			List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
			if (lstPr != null && lstPr.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())) {//cho show gia
				result.put("isAllowShowPrice", true);
			} else {
				result.put("isAllowShowPrice", false);
			}
			//cau hinh cho chinh sua gia hay ko
			List<ShopParam> lstPrEdit = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_MODIFY_PRICE);
			if (lstPrEdit != null && lstPrEdit.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPrEdit.get(0).getValue())) {//cho edit gia
				result.put("isAllowModifyPrice", true);
			} else {
				result.put("isAllowModifyPrice", false);
			}
			CarSOFilter carFilter = new CarSOFilter();
			carFilter.setShopId(shopId);
			carFilter.setStatus(ActiveType.RUNNING);
			ObjectVO<CarSOVO> carVo = carMgr.getListCar(carFilter);

			result.put("lstNVBHVo", lstNVBHVo);
			result.put("lstNVTTVo", lstNVTTVo);
			result.put("lstNVGHVo", lstNVGHVo);
			if (carVo != null && carVo.getLstObject() != null) {
				result.put("lstCar", carVo.getLstObject());
			} else {
				result.put("lstCar", new ArrayList<CarSOVO>());
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.loadStaffByShopCode"), createLogErrorStandard(actionStartTime));
		}
		isAuthorize = 1;
		return JSON;
	}

	/**
	 * Ham load thong tin sp ban , kho ,.. cho trang tao don hang
	 * 
	 * @return JSON
	 * @author nhanlt6
	 */
	public String loadProductForSale() {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			// Danh sach san pham co the ban cua NPP 
			Long shopId = currentShop.getId();
			if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
				Shop sh = shopMgr.getShopByCode(shopCodeFilter);
				if (sh != null) {
					shopId = sh.getId();
				}
			}
			//			List<StockTotalVO> vos = stockMgr.getListProductsCanSalesForShop(currentShop.getId());
			List<StockTotalVO> vos = stockMgr.getListProductsCanSalesForShop(shopId);
			if (vos == null) {
				result.put("productStocks", new ArrayList<StockTotalVO>());
			} else {
				result.put("productStocks", vos);
			}
			// Danh sach san pham thuoc nganh hang Z
			products = productMgr.getListProductEquipmentZ();
			if (products == null) {
				result.put("products", new ArrayList<Product>());
			} else {
				result.put("products", products);
			}
			//Danh sach kho cua NPP
			BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setLongG(currentShop.getId());
			// lay du lieu phan quyen duoi DB
			filter.setStaffRootId(currentUser.getUserId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<WarehouseVO> obj = stockManagerMgr.getListWarehouseVOByFilter(filter);
			if (obj != null && obj.getLstObject().size() > 0) {
				result.put("lstWarehouse", obj.getLstObject());
			} else {
				result.put("lstWarehouse", new ArrayList<Product>());
			}
			lstWarehouse = obj.getLstObject();
			List<StockTotalVO> vosWarehouse = null;
			// Danh sach san pham co the ban cua NPP theo tung Kho
			mapProductByWarehouse = new HashMap<Long, List<StockTotalVO>>();
			for (int i = 0; i < vos.size(); i++) {
				// Danh sach san pham co the ban cua NPP theo kho
				// vosWarehouse = stockMgr.getListProductsCanSalesForShopByWarehouse(currentShop.getId(),vos.get(i).getProductId(), null);
				vosWarehouse = stockMgr.getListProductsCanSalesForShopByWarehouse(shopId, vos.get(i).getProductId(), null);
				if (vosWarehouse != null && vosWarehouse.size() > 0) {
					mapProductByWarehouse.put(vos.get(i).getProductId(), vosWarehouse);
				}
			}
			if (mapProductByWarehouse == null || mapProductByWarehouse.size() == 0) {
				result.put("mapProductByWarehouse", new HashMap<Long, List<StockTotalVO>>());
			} else {
				result.put("mapProductByWarehouse", mapProductByWarehouse);
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.stock.CreateProductOrderAction.loadProductForSale()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Ham load trang chinh sua don hang
	 * 
	 * @return JSON
	 * @author nhanlt6
	 */
	private Integer compareLockDay;
	private String orderType;
	private Integer approved;
	private Integer approvedStep;
	private Integer lockedStock;
	private Integer isLostDetail = 0;

	public String loadPageAdjustOrder() throws Exception {
		try {
			isAuthorize = 1; //Khong cho phep sua thong tin co ban cua don hang
			editOrderTablet = 0; //Cho phep luu don hang tablet 
			isVanSale = 0;
			isApprovedVanSale = 1;
			lockedStock = 1;
			resetToken(result);
			if (currentUser.getShopRoot() == null) {
				return PAGE_NOT_PERMISSION;
			}

			saleOrder = saleOrderMgr.getSaleOrderById(orderId);
			if (saleOrder == null) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.order.title"));

			} else if (saleOrder.getShop() == null) {// || !saleOrder.getShop().getId().equals(currentUser.getShopRoot().getShopId())
				return PAGE_NOT_PERMISSION;
			} else {
				if (OrderType.SO.equals(saleOrder.getOrderType()) || OrderType.CO.equals(saleOrder.getOrderType())) {
					isVanSale = 1;
					if (saleOrder.getApprovedVan() == null || saleOrder.getApprovedVan().intValue() == 0) {
						isApprovedVanSale = 0;
					} else {
						isApprovedVanSale = 1;
					}
					// lacnv1 -  kiem tra NV da chot ngay chua, neu chua thi khong cho duyet don SO, CO
					String s = String.valueOf(saleOrder.getStaff().getId());
					s = shopLockMgr.getListStaffLockStockYet(s, dayLock);
					if (!StringUtil.isNullOrEmpty(s)) {
						lockedStock = 0;
					}
				}
			}
			carId = saleOrder.getCar() != null ? saleOrder.getCar().getId() : null;
			deliveryDate = saleOrder.getDeliveryDate() != null ? DateUtil.toDateString(saleOrder.getDeliveryDate(), DateUtil.DATE_FORMAT_DDMMYYYY) : "";
			priority = apParamMgr.getListApParam(ApParamType.ORDER_PIRITY, ActiveType.RUNNING);
			priorityId = saleOrder.getPriority();
			orderType = saleOrder.getOrderType().getValue();
			approved = saleOrder.getApproved().getValue();
			approvedStep = saleOrder.getApprovedStep().getValue();
			compareLockDay = DateUtil.compareDateWithoutTime(lockDay, saleOrder.getOrderDate());
			shopCode = saleOrder.getShop().getShopCode();
			shopId = saleOrder.getShop().getId();
			//			SoFilter filter = new SoFilter();
			//			filter.setShopId(shopId);
			//			filter.setLockDay(saleOrder.getOrderDate());
			//			filter.setLstSaleOrderId(Arrays.asList(orderId));
			//			filter.setIsFreeItem(0);
			//			List<OrderProductVO> lstPrice = saleOrderMgr.getListProductPriceWarning(filter);
			//			if(lstPrice!=null && lstPrice.size()>0){
			//				isWarningPrice=1;
			//			}

			/**
			 * ALLOW_EDIT_PROMOTION,AP_PARAM_CODE = 1 : Cau hinh cho phep thay
			 * doi trong grid san pham KM
			 */
			List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.ALLOW_EDIT_PROMOTION, ActiveType.RUNNING);
			if (aparams != null && aparams.size() == 1 && aparams.get(0).getApParamCode().equals("1")) {
				allowEditPromotion = 1;
			} else {
				allowEditPromotion = 0;
			}

			CarMgr carMgr = (CarMgr) context.getBean("carMgr");
			ObjectVO<Car> lstCarsVO = carMgr.getListCar(null, shopId, null, null, null, null, null, null, ActiveType.RUNNING, null);
			if (lstCarsVO != null) {
				lstCars = lstCarsVO.getLstObject();
			}
			if (orderId == null || orderId == 0) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NO_SELECT, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.order.title"));
				return SUCCESS;
			}

			StaffPrsmFilter<StaffVO> staffFilter = new StaffPrsmFilter<StaffVO>();
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				staffFilter.setIsLstChildStaffRoot(true);
			} else {
				staffFilter.setIsLstChildStaffRoot(false);
			}
			staffFilter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			staffFilter.setStatus(ActiveType.RUNNING.getValue());
			staffFilter.setLstShopId(Arrays.asList(shopId));
			staffFilter.setIsGetChildShop(false);
			//Lay NVBH
			staffFilter.setObjectType(StaffSpecificType.STAFF.getValue());
			ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(staffFilter);
			lstNVBHVo = objVO.getLstObject();
			Staff nvbh = saleOrder.getStaff();
			StaffVO nvbhVO = new StaffVO();
			nvbhVO.setId(nvbh.getId());
			nvbhVO.setStaffCode(nvbh.getStaffCode());
			nvbhVO.setStaffName(nvbh.getStaffName());
			lstNVBHVo.add(0, nvbhVO);

			staffFilter.setIsLstChildStaffRoot(false);
			staffFilter.setObjectType(StaffSpecificType.NVTT.getValue());
			objVO = staffMgr.searchListStaffVOByFilter(staffFilter);
			lstNVTTVo = objVO.getLstObject();
			staffFilter.setObjectType(StaffSpecificType.NVGH.getValue());
			objVO = staffMgr.searchListStaffVOByFilter(staffFilter);
			lstNVGHVo = objVO.getLstObject();
			if (OrderType.SO.equals(saleOrder.getOrderType()) || OrderType.CO.equals(saleOrder.getOrderType())) {
				if (saleOrder.getDelivery() != null
						&& saleOrder.getCashier() != null
						&& (saleOrder.getDelivery().getStaffType() == null || (!StaffSpecificType.NVGH.equals(saleOrder.getDelivery().getStaffType().getSpecificType()) && !StaffSpecificType.NVTT.equals(saleOrder.getCashier().getStaffType()
								.getSpecificType())))) {
					Staff nvgh = saleOrder.getDelivery();
					StaffVO nvghVO = new StaffVO();
					nvghVO.setId(nvgh.getId());
					nvghVO.setStaffCode(nvgh.getStaffCode());
					nvghVO.setStaffName(nvgh.getStaffName());
					lstNVGHVo.add(0, nvghVO);
				}
			}

			//kiem tra rot chi tiet
			SoFilter fLost = new SoFilter();
			fLost.setShopId(shopId);
			fLost.setLockDay(lockDay);
			fLost.setLstSaleOrderId(Arrays.asList(orderId));
			fLost.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
			fLost.setOrderSource(SaleOrderSource.TABLET);
			String lstLostDetail = saleOrderMgr.getListOrderNumberLostDetail(fLost);
			if (!StringUtil.isNullOrEmpty(lstLostDetail)) {
				isLostDetail = 1;
			}
			List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
			if (lstPr != null && lstPr.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())) {
				isAllowShowPrice = true;
			} else {
				isAllowShowPrice = false;
			}

		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.loadPageAdjustOrder"), createLogErrorStandard(actionStartTime));
		}

		return SUCCESS;
	}

	/**
	 * Ham load thong tin sp ban , spkm... cho man hinh chinh sua don hang
	 * 
	 * @return JSON
	 * @author nhanlt6
	 */
	public String loadProductForAdjustSale() {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			saleOrder = saleOrderMgr.getSaleOrderById(orderId);
			if (saleOrder == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.order.title")));
				result.put("error", true);
				return JSON;
			}
			ObjectVO<SaleOrderDetailVOEx> order_detail_vo;
			ObjectVO<SaleOrderDetailVOEx> order_detail_vo_free;
			Long custTypeId = null;
			if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
				Shop shop = shopMgr.getShopByCode(shopCodeFilter);
				if (shop != null) {//lay shop tren giao dien
					currentShop = shop;
				} else {
					currentShop = saleOrder.getShop();
				}
			} else {//lay shop tren giao dien
				currentShop = saleOrder.getShop();
			}
			if (saleOrder.getCustomer().getChannelType() != null) {
				custTypeId = saleOrder.getCustomer().getChannelType().getId();
			}
			if (SaleOrderSource.TABLET.equals(saleOrder.getOrderSource())) {
				order_detail_vo = saleOrderMgr.getListSaleOrderDetailVOExForTablet(saleOrder, null, null, false, lockDay, getLogInfoVO());
				order_detail_vo_free = saleOrderMgr.getListSaleOrderDetailVOEx(null, orderId, 1, null, true, custTypeId, currentShop.getId(), lockDay);
			} else {
				order_detail_vo = saleOrderMgr.getListSaleOrderDetailVOEx(null, orderId, null, null, false, custTypeId, currentShop.getId(), lockDay);
				order_detail_vo_free = saleOrderMgr.getListSaleOrderDetailVOEx(null, orderId, 1, null, true, custTypeId, currentShop.getId(), lockDay);
			}
			//Load danh sach CKMH ap dung KM don hang theo sale order id
			List<SaleOrderDetailVOEx> lstPromoOrder = saleOrderMgr.getListSaleOrderPromoDetailForOrder(orderId, SaleOrderDetailVO.FREE_PRICE);
			if (order_detail_vo != null) {
				order_detail = order_detail_vo.getLstObject();
				order_detail_promotion = order_detail_vo_free.getLstObject();
				order_detail_promotion.addAll(lstPromoOrder);

				SaleOrderFilter<SaleOrder> saleOrderFilter = new SaleOrderFilter<SaleOrder>();
				saleOrderFilter.setSaleOrderId(saleOrder.getId());
				List<ProgramCodeVO> progamCodeVOs = saleOrderMgr.getListProgramCodeVO(saleOrderFilter);
				Map<String, Integer> mapWh = new HashMap<String, Integer>();
				Integer qtt = null;
				StockTotal stk = null;

				for (SaleOrderDetailVOEx voEx : order_detail) {
					if (voEx.getWarehouseId() != null) {
						qtt = mapWh.get(voEx.getProductId() + "_" + voEx.getWarehouseId());
						if (qtt == null) {
							stk = stockMgr.getStockTotalByProductAndOwner(voEx.getProductId(), StockObjectType.SHOP, saleOrder.getShop().getId(), voEx.getWarehouseId());
							if (stk == null) {
								qtt = 0;
							} else {
								qtt = stk.getAvailableQuantity();
							}
						}
						qtt = qtt + voEx.getQuantity();
						mapWh.put(voEx.getProductId() + "_" + voEx.getWarehouseId(), qtt);
					}
					if (viewOnly != null && 1 == viewOnly) {
						if (voEx.getPriceView() != null) {
							voEx.setPrice(voEx.getPriceView());
						}
						if (voEx.getPkPriceView() != null) {
							voEx.setPkPrice(voEx.getPkPriceView());
						}
					}
					for (int k = 0, n = progamCodeVOs.size(); k < n; k++) {
						ProgramCodeVO programCode = progamCodeVOs.get(k);
						if (programCode.getSaleOrderDetailId().equals(voEx.getId())) {
							voEx.setProgramCode(programCode.getStrProgramCode());
							break;
						}
					}
				}
				if (SaleOrderSource.WEB.equals(saleOrder.getOrderSource())) {
					for (SaleOrderDetailVOEx voEx : order_detail_promotion) {
						if (voEx.getWarehouseId() != null) {
							qtt = mapWh.get(voEx.getProductId() + "_" + voEx.getWarehouseId());
							if (qtt == null) {
								stk = stockMgr.getStockTotalByProductAndOwner(voEx.getProductId(), StockObjectType.SHOP, saleOrder.getShop().getId(), voEx.getWarehouseId());
								if (stk == null) {
									qtt = 0;
								} else {
									qtt = stk.getAvailableQuantity();
								}
							}
							qtt = qtt + voEx.getQuantity();
							mapWh.put(voEx.getProductId() + "_" + voEx.getWarehouseId(), qtt);
						}
					}
				}

				//check san pham km co the doi nhieu sp hay ko?
				Map<String, List<SaleOrderDetailVOEx>> listTmp = new HashMap<String, List<SaleOrderDetailVOEx>>();
				Map<String, Integer> mapHasPortion = new HashMap<String, Integer>();
				List<StockTotal> lst = null;
				for (SaleOrderDetailVOEx voEx : order_detail_promotion) {
					if (voEx.getProductGroupId() != null && voEx.getGroupLevelId() != null) {
						voEx.setProductGroup(commonMgr.getEntityById(ProductGroup.class, voEx.getProductGroupId()));
						voEx.setGroupLevel(commonMgr.getEntityById(GroupLevel.class, voEx.getGroupLevelId()));
					}
					if (listTmp.get(voEx.getProgramCode()) == null) {
						List<SaleOrderDetailVOEx> listVOEX = new ArrayList<SaleOrderDetailVOEx>();
						listVOEX.add(voEx);
						listTmp.put(voEx.getProgramCode(), listVOEX);
					} else {
						listTmp.get(voEx.getProgramCode()).add(voEx);
					}
					if (voEx.getProductId() != null) {
						if (OrderType.SO.equals(saleOrder.getOrderType()) || OrderType.CO.equals(saleOrder.getOrderType())) {
							StockTotalFilter stockTotalFilter = new StockTotalFilter();
							stockTotalFilter.setProductId(voEx.getProductId());
							stockTotalFilter.setOwnerType(StockObjectType.STAFF);
							stockTotalFilter.setOwnerId(saleOrder.getStaff().getId());
							lst = stockMgr.getListStockTotalByProductAndOwner(stockTotalFilter);
							if (lst != null && lst.size() > 0) {
								voEx.setAvailableQuantity(lst.get(0).getApprovedQuantity());
								voEx.setStockQuantity(lst.get(0).getApprovedQuantity());
							}
						} else {
							//Load danh sach SPKM dc huong theo Kho
							StockTotalFilter stockTotalFilter = new StockTotalFilter();
							stockTotalFilter.setProductId(voEx.getProductId());
							stockTotalFilter.setOwnerType(StockObjectType.SHOP);
							stockTotalFilter.setOwnerId(saleOrder.getShop().getId());
							if (isDividualWarehouse == null || isDividualWarehouse) {
								stockTotalFilter.setWarehouseType(WarehouseType.PROMOTION);
							} else {
								stockTotalFilter.setOrderByType(WarehouseType.PROMOTION);
							}
							lst = stockMgr.getListStockTotalByProductAndOwner(stockTotalFilter);
							if (lst != null && lst.size() > 0) {
								StockTotal stt = lst.get(0);
								int i = 0;
								if (voEx.getWarehouseId() != null) {
									for (int size = lst.size(); i < size && lst.get(i).getWarehouse() != null; i++) {
										if (lst.get(i).getWarehouse().getId().equals(voEx.getWarehouseId())) {
											stt = lst.get(i);
											if (lst.get(i).getAvailableQuantity() != null && lst.get(i).getAvailableQuantity() > 0) {
												break;
											}
										}
									}
									qtt = mapWh.get(voEx.getProductId() + "_" + voEx.getWarehouseId());
									if (qtt == null) {
										stt.setAvailableQuantity(stt.getAvailableQuantity() + voEx.getQuantity());
									} else {
										stt.setAvailableQuantity(qtt);
									}
								} else {
									for (int size = lst.size(); i < size && lst.get(i).getWarehouse() != null && lst.get(i).getAvailableQuantity() != null && lst.get(i).getAvailableQuantity() <= 0; i++)
										;
									stt = lst.get(i >= lst.size() ? 0 : i);
								}
								voEx.setWarehouse(stt.getWarehouse());
								voEx.setAvailableQuantity(stt.getAvailableQuantity());
								voEx.setStockQuantity(stt.getQuantity());
								if ((viewOnly == null || viewOnly != 1) && !StringUtil.isNullOrEmpty(voEx.getProgramCode())) { // set gia tri cho biet CTKM co so suat hay khong
									if (mapHasPortion.get(voEx.getProgramCode()) == null) {
										boolean b = promotionProgramMgr.checkPromotionProgramHasPortion(voEx.getProgramCode(), saleOrder.getShop().getId(), saleOrder.getCustomer().getId(), saleOrder.getStaff().getId(), dayLock);
										if (b) {
											voEx.setHasPortion(1);
										} else {
											voEx.setHasPortion(0);
										}
										mapHasPortion.put(voEx.getProgramCode(), voEx.getHasPortion());
									} else {
										voEx.setHasPortion(mapHasPortion.get(voEx.getProgramCode()));
									}
								}
							}
						}
					}
				}
				for (String keyTmp : listTmp.keySet()) {
					if (listTmp.get(keyTmp).size() > 1) {
						for (SaleOrderDetailVOEx voEx : listTmp.get(keyTmp)) {
							if ((voEx.getDiscountAmount() != null && BigDecimal.ZERO.compareTo(voEx.getDiscountAmount()) < 0) || (voEx.getDiscountPercent() != null && voEx.getDiscountPercent() > 0)) {
								voEx.setCanChaneMultiProduct(false);
							} else {
								voEx.setCanChaneMultiProduct(true);
							}
						}
					}
				}
			}
			//Set type cho cac loai KM
			setTypePromotion();

			ObjectVO<OrderProductVO> objVO = saleOrderMgr.getListOrderProductVO(null, currentShop.getId(), saleOrder.getStaff().getId(), saleOrder.getCustomer().getId(), null, null, null, lockDay, custTypeId, currentShop.getType().getId());

			//Danh sach CTHTTM : ap dung NPP va KH /
			ObjectVO<CommercialSupportVO> listCommercialSupportVO = null;
			if (viewOnly != null && 1 == viewOnly) { // lacnv1 - danh cho man hinh XEM chi tiet don hang
				listCommercialSupportVO = saleOrderMgr.getListCommercialSupportOfSaleOrder(saleOrder.getId(), null);
			} else {
				listCommercialSupportVO = saleOrderMgr.getListCommercialSupport(null, currentShop != null ? currentShop.getId() : null, saleOrder.getCustomer().getId(), lockDay, null);
			}

			List<SaleOrderPromoDetail> lstPromoDetails = saleOrderMgr.getListSaleOrderPromoDetailOfOrder(saleOrder.getId(), null);
			if (lstPromoDetails != null && lstPromoDetails.size() > 0) {
				ProductGroup productGroup = null;
				GroupLevel groupLevel = null;
				for (SaleOrderPromoDetail promoDetail : lstPromoDetails) {
					try {
						if (promoDetail.getProductGroupId() != null) {
							productGroup = commonMgr.getEntityById(ProductGroup.class, promoDetail.getProductGroupId());
							promoDetail.setProductGroup(productGroup);
						}
						if (promoDetail.getGroupLevelId() != null) {
							groupLevel = commonMgr.getEntityById(GroupLevel.class, promoDetail.getGroupLevelId());
							promoDetail.setGroupLevel(groupLevel);
						}
					} catch (Exception e1) {
						// pass through
					}
					promoDetail.setShop(null);
					promoDetail.setStaff(null);
					promoDetail.setSaleOrder(null);
				}
			}
			result.put("lstPromoDetails", lstPromoDetails);

			//Danh sach san pham co the ban cua NPP /
			List<StockTotalVO> vos = stockMgr.getListProductsCanSalesForShop(currentShop.getId());
			productStocks = vos;

			//Danh sach sp thuoc nganh hang Z
			products = productMgr.getListProductEquipmentZ();

			//Danh sach kho cua NPP
			BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setLongG(currentShop.getId());
			// lay du lieu phan quyen duoi DB
			filter.setStaffRootId(currentUser.getUserId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<WarehouseVO> obj = stockManagerMgr.getListWarehouseVOByFilter(filter);

			List<StockTotalVO> vosWarehouse = null;
			// Danh sach san pham co the ban cua NPP theo tung Kho
			//load cham
			mapProductByWarehouse = new HashMap<Long, List<StockTotalVO>>();
			//			for (StockTotalVO product : productStocks) {
			// Danh sach san pham co the ban cua NPP theo kho
			vosWarehouse = stockMgr.getListProductsCanSalesForShopByWarehouse(currentShop.getId(), null, orderId);
			if (vosWarehouse != null && vosWarehouse.size() > 0) {
				for (StockTotalVO vo : vosWarehouse) {
					List<StockTotalVO> lst = mapProductByWarehouse.get(vo.getProductId());
					if (lst == null) {
						lst = new ArrayList<StockTotalVO>();
					}
					lst.add(vo);
					mapProductByWarehouse.put(vo.getProductId(), lst);
				}
			}
			//			}

			//put cac list data vao JSON object
			if (obj != null && obj.getLstObject().size() > 0) {
				result.put("lstWarehouse", obj.getLstObject());
			} else {
				result.put("lstWarehouse", new ArrayList<Product>());
			}
			if (mapProductByWarehouse == null || mapProductByWarehouse.size() == 0) {
				result.put("mapProductByWarehouse", new HashMap<Long, List<StockTotalVO>>());
			} else {
				result.put("mapProductByWarehouse", mapProductByWarehouse);
			}
			if (order_detail != null) {
				// tulv2: su dung de check ton kho dap ung o javascript.
				for (SaleOrderDetailVOEx orderProductVO : order_detail) {
					orderProductVO.setOldQuantity(orderProductVO.getQuantity() == null ? 0 : orderProductVO.getQuantity());
				}
				result.put("sale_order_lot", order_detail);
			} else {
				result.put("sale_order_lot", "");
			}
			if (order_detail_promotion != null) {
				/** vuongmq; 11/01/2015; hien thi maxQuantityFreeTotal so luong tinh khuyen mai*/
				for (SaleOrderDetailVOEx voEx: order_detail_promotion) {
					voEx.setMaxQuantityFreeTotal(voEx.getMaxQuantityFree());
				}
				result.put("order_detail_promotion", order_detail_promotion);
			} else {
				result.put("order_detail_promotion", "");
			}
			List<OrderProductVO> listSaleProduct = objVO.getLstObject();
			if (listSaleProduct != null && listSaleProduct.size() > 0) {
				result.put("listSaleProduct", listSaleProduct);
			} else {
				result.put("listSaleProduct", "");
			}
			if (listCommercialSupportVO.getLstObject() != null && listCommercialSupportVO.getLstObject().size() > 0) {
				result.put("listCommercialSupport", listCommercialSupportVO.getLstObject());
			} else {
				result.put("listCommercialSupport", "");
			}
			if (productStocks != null) {
				result.put("productStocks", productStocks);
			} else {
				result.put("productStocks", "");
			}
			if (products != null) {
				result.put("products", products);
			} else {
				result.put("products", "");
			}
			result.put("saleOrder", saleOrder);
			/*
			 * load sale_order_promotion
			 */
			SaleOrderPromotionFilter sopFilter = new SaleOrderPromotionFilter();
			sopFilter.setSaleOrderId(orderId);
			List<SaleOrderPromotion> saleOrderPromotions = saleOrderPromotionMgr.getSaleOrderPromotions(sopFilter);
			if (saleOrderPromotions != null && !saleOrderPromotions.isEmpty()) {
				for (SaleOrderPromotion saleOrderPromotion : saleOrderPromotions) {
					saleOrderPromotion.setMaxQuantityReceivedTotal(saleOrderPromotion.getMaxQuantityReceived());
				}
			}
			result.put("saleOrderQuantityReceivedList", saleOrderPromotions);

			SoFilter filter1 = new SoFilter();
			filter1.setShopId(saleOrder.getShop().getId());
			filter1.setLockDay(lockDay);
			//filter1.setLstSaleOrderId(Arrays.asList(saleOrder.getId()));
			filter1.setSaleOrderId(saleOrder.getId());
			List<OrderProductVO> lstPromotion = saleOrderMgr.getListWarningPromotionOfOrder(filter1);
			if (lstPromotion != null && lstPromotion.size() > 0) {
				String stmp = "";
				for (OrderProductVO vo : lstPromotion) {
					stmp = stmp + "," + vo.getPromotionProgramCode();
				}
				stmp = stmp.replaceFirst(",", "");
				result.put("lstPromotion", stmp);
			}
			
			//Lay danh sach keyshop
			KeyShopFilter filterKS = new KeyShopFilter();
			filterKS.setCustomerId(saleOrder.getCustomer().getId());
			result.put("lstKeyShop", keyShopMgr.getListKSVOForRewardOrderByFilter(filterKS));
			//------------------------------

			/*
			 * tinh KM tich luy
			 */
//			try {
//				List<PromotionItem> accumulativePromotionItems = promotionProgramMgr.getAccumulationInfoOfOrder(orderId);
//				result.put("accumulativePromotionItems", accumulativePromotionItems);
//			} catch (Exception e) {
//				String exceptionMsg = "Fail to calculate accumulative promotion program for sale_order_id = " + orderId + ".\n" + e.getMessage();
//				LogUtility.logError(e, exceptionMsg);
//			}

			/*
			 * check xem CTKM co thay doi co cau hay ko. Neu nhu CTKM cua DH co
			 * su thay doi co cau, thi canh bao nguoi dung CTKM thay doi co cau
			 * & ko cho phep doi SPKM neu nhu dang sua DH
			 */
			/*
			 * Boolean isPromotionProgramStructureHasChanged =
			 * promotionProgramMgr
			 * .isSaleOrderPromotionProgramStructureHasChanged
			 * (saleOrder.getId());
			 * result.put("promotionProgramStructureHasChanged",
			 * isPromotionProgramStructureHasChanged);
			 */
			List<String> changedStructurePromotionProgramsInSaleOrder = promotionProgramMgr.getChangedStructurePromotionProgramInSaleOrder(saleOrder.getId());
			result.put("changedStructurePromotionProgramsInSaleOrder", changedStructurePromotionProgramsInSaleOrder);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.stock.CreateProductOrderAction.loadProductForAdjustSale()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Thong tin chi tiet khach hang va nhan vien ban hang
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public String customerAndSaleStaffDetails() {
		try {
			currentShop = getCurrentShop();
			/*
			 * long shopIdFilter = -1; long shopTypeFilter = -1; long cusTypeId
			 * = -1;
			 */
			if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
				Shop shop = shopMgr.getShopByCode(shopCodeFilter);
				if (shop != null) {
					currentShop = shop;
				}
			}
			if (currentShop == null || currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			Date oDate = DateUtil.parse(orderDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			//Thong tin khach hang 
			customer = customerMgr.getCustomerByCodeEx(StringUtil.getFullCode(currentShop.getShopCode(), customerCode), ActiveType.RUNNING);
			if (customer == null || customer.getShop() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty")));
				return JSON;
			} else if (!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty") + " - " + customerCode));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(customer.getCustomerName())) {
				result.put("customerName", "");
			} else {
				result.put("customerName", customer.getCustomerName());
			}
			if (StringUtil.isNullOrEmpty(customer.getAddress())) {
				result.put("address", "");
			} else {
				result.put("address", customer.getAddress());
			}
			if (StringUtil.isNullOrEmpty(customer.getPhone())) {
				result.put("channelTypeName", "");
			} else {
				result.put("channelTypeName", customer.getPhone());
			}
			if (StringUtil.isNullOrEmpty(customer.getShortCode())) {
				result.put("shortCode", "");
			} else {
				result.put("shortCode", customer.getShortCode());
			}
			if (customer.getCashierStaff() == null) {
				result.put("cashierStaffCode", "");
			} else {
				result.put("cashierStaffCode", customer.getCashierStaff().getStaffCode());
			}

			if (customer.getDeliver() != null) {
				result.put("transferStaffCode", customer.getDeliver().getStaffCode());
			} else {
				result.put("transferStaffCode", "");
			}
			if (!StringUtil.isNullOrEmpty(customer.getPhone()) && !StringUtil.isNullOrEmpty(customer.getMobiphone())) {
				result.put("phone", customer.getPhone() + " / " + customer.getMobiphone());
			} else if (!StringUtil.isNullOrEmpty(customer.getPhone())) {
				result.put("phone", customer.getPhone());
			} else if (!StringUtil.isNullOrEmpty(customer.getMobiphone())) {
				result.put("phone", customer.getMobiphone());
			} else {
				result.put("phone", "");
			}
			//Thong tin NVBH
			Staff saleStaff = null;
			if (StringUtil.isNullOrEmpty(salerCode)) {
				//co the co nhieu nhan vien cung quan ly 1 tuyen tai cung mot thoi diem, nhung lay nv dau tien
				RoutingCustomerFilter filter = new RoutingCustomerFilter();
				filter.setCustomerId(customer.getId());
				filter.setShopId(currentShop.getId());
				filter.setOrderDate(oDate != null ? oDate : lockDay);
				filter.setUserId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				List<Staff> lst = staffMgr.getSaleStaffByDate(filter);
				if (lst != null && lst.size() > 0) {
					saleStaff = lst.get(0);
				}
				result.put("lstStaffSale", lst);
			} else {
				saleStaff = staffMgr.getStaffByCode(salerCode);
				result.put("lstStaffSale", Arrays.asList(saleStaff));
			}
			if (saleStaff == null || super.getMapUserId().get(saleStaff.getId()) == null) {
				result.put("staffSaleCode", "");
			} else {
				result.put("staffSaleCode", saleStaff.getStaffCode());
			}
			// Lay danh sach san pham ban theo nhan vien , khach hang va currentShop
			if (saleStaff != null) {
				//				List<OrderProductVO> listOrderProduct = saleOrderMgr.getListOrderProductVO(null, currentShop.getId(), saleStaff.getId(),
				List<OrderProductVO> listOrderProduct = saleOrderMgr.getListOrderProductVO(null, currentShop.getId(), saleStaff.getId(), customer.getId(), null, null, null, oDate != null ? oDate : lockDay, (customer.getChannelType() == null ? 0L : customer.getChannelType().getId()),
						currentShop.getType().getId()).getLstObject();
				if (listOrderProduct != null) {
					result.put("listSaleProduct", listOrderProduct);
				} else {
					result.put("listSaleProduct", new ArrayList<OrderProductVO>());
				}
			}
			// Lay danh sach CT HTTM 			
			ObjectVO<CommercialSupportVO> listCommercialSupportVO = saleOrderMgr.getListCommercialSupport(null, currentShop.getId(), customer.getId(), oDate != null ? oDate : lockDay, null);
			if (listCommercialSupportVO != null) {
				result.put("commercials", listCommercialSupportVO.getLstObject());
			} else {
				result.put("commercials", new ArrayList<CommercialSupportVO>());
			}

			//Lay danh sach keyshop
			KeyShopFilter filter = new KeyShopFilter();
			filter.setCustomerId(customer.getId());
			result.put("lstKeyShop", keyShopMgr.getListKSVOForRewardOrderByFilter(filter));
			//------------------------------
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.customerAndSaleStaffDetails"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, false);
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Thong tin chi tiet san pham
	 * 
	 * @author nhanlt6
	 * @return the string
	 */
	public String productDetails() {
		boolean error = true;
		try {
			if (!StringUtil.isNullOrEmpty(currentShop.getShopCode())) {
				if (!StringUtil.isNullOrEmpty(customerCode)) {
					Date oDate = DateUtil.parse(orderDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					customer = customerMgr.getCustomerByCodeEx(StringUtil.getFullCode(shopCodeFilter, customerCode), ActiveType.RUNNING);
					if (customer != null) {
						Staff saleStaff = staffMgr.getStaffByCode(salerCode);
						if (saleStaff != null /*
											 * &&
											 * (StaffObjectType.NVBH.getValue(
											 * ).equals
											 * (saleStaff.getStaffType().
											 * getObjectType()) ||
											 * StaffObjectType
											 * .NVVS.getValue().equals
											 * (saleStaff.
											 * getStaffType().getObjectType()))
											 */) {
							Product product = productMgr.getProductByCode(productCode);
							if (product == null) {
								errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "catalog.display.product.code");
							} else {
								result.put("product", product);
								// lay cau hinh 
								// neu co sho gia
								Shop currentShop = shopMgr.getShopByCode(shopCodeFilter);
								SaleOrderFilter<Price> filter = new SaleOrderFilter<Price>();
								filter.setProductId(product.getId());
								filter.setShopId(currentShop.getId());
								filter.setCustomerId(customer.getId());
								filter.setOrderDate(oDate != null ? oDate : lockDay);
								List<Price> lstPr = productMgr.getPriceByRpt(filter);
								Price pri = new Price();
								if (lstPr != null && lstPr.size() > 0) {
									pri = lstPr.get(0);
								}
								if (pri != null) {
									result.put("price", convertMoney(pri.getPrice()));
									result.put("priceThung", convertMoney(pri.getPackagePrice()));
									result.put("priceDGsdgh", convertMoney(pri.getPrice()));
								}
								// khong thi thoi
								List<PromotionProgram> lstPpTemp = promotionProgramMgr.getPromotionProgramByProductAndShopAndCustomer(product.getId(), customer.getShop().getId(), customer.getId(), oDate != null ? oDate : lockDay, null, saleStaff
										.getId());
								String ppCode = "";
								if (lstPpTemp != null && lstPpTemp.size() > 0) {
									for (PromotionProgram ppTemp : lstPpTemp) {
										ppCode += (ppCode.equals("") ? ppTemp.getPromotionProgramCode() : ", " + ppTemp.getPromotionProgramCode());
									}
								}
								result.put("promotion", ppCode);
								SaleOrder so = null;
								if (orderId != null && orderId > 0) {
									so = saleOrderMgr.getSaleOrderById(orderId);
								}
								List<StockTotalVO> stockTotal = null;
								if (so != null && (OrderType.SO.equals(so.getOrderType()) || OrderType.CO.equals(so.getOrderType()))) {
									stockTotal = productMgr.getPromotionProductInfo(product.getId(), so.getStaff().getId(), currentShop.getShopChannel(), customer.getId(), StockObjectType.STAFF.getValue(), oDate != null ? oDate : lockDay);
								} else {
									stockTotal = productMgr.getPromotionProductInfo(product.getId(), currentShop.getId(), currentShop.getShopChannel(), customer.getId(), StockObjectType.SHOP.getValue(), oDate != null ? oDate : lockDay);
								}
								if (stockTotal != null && stockTotal.get(0) != null) {
									StockTotalVO st = stockTotal.get(0);
									if (st.getAvailableQuantity() == null) {
										st.setAvailableQuantity(0);
									}
									if (orderId != null && orderId > 0) {
										if (so != null && (OrderType.SO.equals(so.getOrderType()) || OrderType.CO.equals(so.getOrderType()))) {
											//don SO, CO lay availableQuantity theo goc nhin ke toan la approvedQuantity va khong + them sl don hang hien tai
											st.setAvailableQuantity(st.getApprovedQuantity());
										} else {
											List<SaleOrderLot> lst1 = saleOrderMgr.getListSaleOrderLotBySaleOrderId(orderId);
											if (so != null && lst1 != null && lst1.size() > 0) {
												if (!SaleOrderStatus.APPROVED.equals(so.getApproved())) {
													for (int j = 0, szj = lst1.size(); j < szj; j++) {
														if (lst1.get(j).getProduct() != null && lst1.get(j).getProduct().getId().equals(st.getProductId())) {
															st.setAvailableQuantity(st.getAvailableQuantity() + lst1.get(j).getQuantity());
														}
													}
												}
											}
										}
										result.put("avaiableQuantity", st.getAvailableQuantity());
									} else {
										result.put("avaiableQuantity", st.getAvailableQuantity());
									}
								} else {
									result.put("avaiableQuantity", 0);
								}
								error = false;
							}
						} else {
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.sale.staff.is.null");
						}
					} else {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.not.in.shop");
					}
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty");
				}
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null");
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.productDetails"), createLogErrorStandard(actionStartTime));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Load list chuong trinh HTTM theo currentShop va KH
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public String listCommercialSupport() {
		try {
			//currentShop = getCurrentShop();
			shopCode = currentShop.getShopCode();
			customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, customerCode));
			if (customer == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer"));
				return JSON;
			}
			ObjectVO<CommercialSupportVO> listCommercialSupportVO = saleOrderMgr.getListCommercialSupport(null, currentShop != null ? currentShop.getId() : null, customer.getId(), lockDay, null);
			result.put(ERROR, false);
			result.put("lst", listCommercialSupportVO.getLstObject());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.listCommercialSupport"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * Load danh sach sp ban hien thi tren dialog
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public String searchProduct() {
		result.put("page", page);
		result.put("rows", rows);
		boolean error = true;
		errMsg = "";
		try {
			KPaging<OrderProductVO> kPaging = new KPaging<OrderProductVO>();
			kPaging.setPage(page - 1);
			kPaging.setPageSize(rows);
			result.put("rows", new ArrayList<OrderProductVO>());
			if (StringUtil.isNullOrEmpty(currentShop.getShopCode())) {
				result.put(ERROR, error);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(customerCode)) {
				result.put(ERROR, error);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty"));
				return JSON;
			}
			customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(currentShop.getShopCode(), customerCode));
			if (customer != null) {
				if (customer.getStatus() != ActiveType.RUNNING) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty") + " - " + customerCode));
					return JSON;
				}
				Staff salerStaff = staffMgr.getStaffByCode(salerCode);
				if (salerStaff == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "NVBH"));
					return JSON;
				}
				Date oDate = DateUtil.parse(orderDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				ObjectVO<OrderProductVO> objVO = saleOrderMgr.getListOrderProductVO(kPaging, currentShop.getId(), salerStaff.getId(), customer.getId(), code, name, ppCode, oDate != null ? oDate : lockDay, (customer.getChannelType() == null ? 0L : customer.getChannelType().getId()), null);
				if (objVO != null) {
					result.put("total", objVO.getkPaging().getTotalRows());
					result.put("rows", objVO.getLstObject());
				}
				error = false;
			} else {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.null"));
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.searchProduct"), createLogErrorStandard(actionStartTime));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Load danh sach kho vao ton kho dap ung theo ma sp ban
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public String searchProductByWarehouse() {
		result.put("page", page);
		result.put("rows", rows);
		boolean error = true;
		errMsg = "";
		try {
			KPaging<OrderProductVO> kPaging = new KPaging<OrderProductVO>();
			kPaging.setPage(page - 1);
			kPaging.setPageSize(rows);
			result.put("total", 0);
			result.put("rows", new ArrayList<OrderProductVO>());
			if (productId == null) {
				result.put(ERROR, error);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.id.null"));
				return JSON;
			}
			ObjectVO<OrderProductVO> objVO = saleOrderMgr.getListOrderProductWarehouseVO(kPaging, productId, currentShop.getId());
			if (objVO != null) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}
			error = false;
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.searchProductByWarehouse"), createLogErrorStandard(actionStartTime));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Load thong tin sp
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public String getProductInfo() {
		boolean error = true;
		errMsg = "";
		try {
			if (!StringUtil.isNullOrEmpty(currentShop.getShopCode())) {
				if (!StringUtil.isNullOrEmpty(customerCode)) {
					customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(currentShop.getShopCode(), customerCode));
					if (customer != null) {
						if (customer.getStatus() != ActiveType.RUNNING) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty") + " - " + customerCode));
							return JSON;
						}
						Staff salerStaff = staffMgr.getStaffByCode(salerCode);
						if (salerStaff == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "NVBH"));
							return JSON;
						}
						ObjectVO<OrderProductVO> objVO = saleOrderMgr.getListOrderProductVO(null, currentShop.getId(), salerStaff.getId(), customer.getId(), code, name, ppCode, lockDay, (customer.getChannelType() == null ? 0L : customer.getChannelType().getId()), null);
						//Lay don hang neu la edit
						ObjectVO<SaleOrderDetailVOEx> order_detail_vo = new ObjectVO<SaleOrderDetailVOEx>();
						ObjectVO<SaleOrderDetailVOEx> order_detail_vo_free = new ObjectVO<SaleOrderDetailVOEx>();
						List<SaleOrderDetailVOEx> order_detail = new ArrayList<SaleOrderDetailVOEx>();
						List<SaleOrderDetailVOEx> order_detail_promotion = new ArrayList<SaleOrderDetailVOEx>();
						List<OrderProductVO> lst = objVO.getLstObject();
						if (orderId != null && orderId > 0) {
							order_detail_vo = saleOrderMgr.getListSaleOrderDetailVOEx(null, orderId, null, null, false, (customer.getChannelType() == null ? null : customer.getChannelType().getId()), currentShop.getId(), lockDay);
							order_detail_vo_free = saleOrderMgr.getListSaleOrderDetailVOEx(null, orderId, 1, null, true, (customer.getChannelType() == null ? null : customer.getChannelType().getId()), currentShop.getId(), lockDay);
							if (order_detail_vo != null) {
								order_detail = order_detail_vo.getLstObject();
								order_detail_promotion = order_detail_vo_free.getLstObject();
								for (int i = 0; i < lst.size(); i++) {
									for (int j = 0; j < order_detail.size(); j++) {
										if (order_detail.get(j).getProductId().equals(lst.get(i).getProductId())) {
											lst.get(i).setAvailableQuantity(lst.get(i).getAvailableQuantity() + order_detail.get(j).getQuantity());
										}
									}
									for (int k = 0; k < order_detail_promotion.size(); k++) {
										if (order_detail_promotion.get(k).getProductId() != null && order_detail_promotion.get(k).getProductId().equals(lst.get(i).getProductId())) {
											lst.get(i).setAvailableQuantity(lst.get(i).getAvailableQuantity() + order_detail_promotion.get(k).getQuantity());
										}
									}
								}
							}
						}
						if (objVO != null && objVO.getLstObject() != null && objVO.getLstObject().size() > 0) {
							result.put("product", objVO.getLstObject().get(0));
							error = false;
						} else {
							errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_PRODUCT_FOR_CUSTOMER_AND_STAFF, code, customer.getShortCode(), salerStaff.getStaffCode());
						}
					} else {
						//Xin loi khach hang nay khong ton tai
						errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.null"));
					}
				} else {
					//Ma khach hang khong duoc phep de trong
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty");
				}
			} else {
				//Xin loi, ban khong co quyen thuc hien chuc nang nay 
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null");
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.getProductInfo"), createLogErrorStandard(actionStartTime));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Kiem tra thong tin NV
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public Boolean checkStaff(Staff staff, StaffObjectType staffObjectType) {
		if (staff == null) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, staffObjectType.toString()));
			result.put(ERROR, true);
			return false;
		}
		if (!staff.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS, staffObjectType.toString()));
			result.put(ERROR, true);
			return false;
		}
		if (staff.getShop() == null || !staff.getShop().getId().equals(currentShop.getId())) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, staff.getStaffCode(), currentShop.getShopCode()));
			result.put(ERROR, true);
			return false;
		}
		/*
		 * if(staff.getStaffType() == null || (staff.getStaffType() != null &&
		 * !staff
		 * .getStaffType().getSpecificType().equals(staffObjectType.getValue
		 * ()))) { result.put("errMsg",
		 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SS_NOT_GENERAL,
		 * staff.getStaffCode(), staffObjectType.toString())); result.put(ERROR,
		 * true); return false; }
		 */
		return true;
	}

	/**
	 * Kiem tra cac thong tin co ban cua don hang
	 * 
	 * @return the boolean
	 * @author nhanlt6 trungtm6 modify on 23/04/2015
	 */
	private boolean validateGeneralInfor() throws BusinessException {
		Shop shopFilter = null;
		if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
			shopFilter = shopMgr.getShopByCode(shopCodeFilter);
			if (shopFilter == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.param.not.exist", shopCode));
				result.put(ERROR, true);
				return false;
			}
		}
		currentShop = shopFilter;//lay theo shop chon tren giao dien
		if (currentShop == null) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null")));
			result.put(ERROR, true);
			return false;
		}
		if (StringUtil.isNullOrEmpty(customerCode)) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty")));
			result.put(ERROR, true);
			return false;
		}
		customer = customerMgr.getCustomerByCodeEx(StringUtil.getFullCode(currentShop.getShopCode(), customerCode), ActiveType.RUNNING);
		if (customer == null) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.null")));
			result.put(ERROR, true);
			return false;
		}
		if (!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty") + " - " + customerCode));
			return false;
		}
		CarMgr carMgr = (CarMgr) context.getBean("carMgr");
		if (carId != null) {
			car = carMgr.getCarById(carId);
			if (car == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.car.error.not.found")));
				result.put(ERROR, true);
				return false;
			}
			if (car != null && !car.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.car.error.not.found")));
				result.put(ERROR, true);
				return false;
			}
			if (car != null && (car.getShop() == null || !car.getShop().getId().equals(currentShop.getId()))) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_CAR_NOT_BELONG_SHOP));
				result.put(ERROR, true);
				return false;
			}
		}
		return true;
	}

	/**
	 * Ham set type cho tung loai KM
	 * 
	 * @author nhanlt6
	 */
	public void setTypePromotion() {
		if (order_detail_promotion == null) {
			return;
		}
		SaleOrderDetailVOEx sod = null;
		GroupLevel groupLevel = null;
		for (int i = 0, sz = order_detail_promotion.size(); i < sz; i++) {
			sod = order_detail_promotion.get(i);
			if (PromotionType.ZV01.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PERCENT);
				sod.setType(SaleOrderDetailVO.FREE_PERCENT);
			} else if (PromotionType.ZV02.getValue().equals(sod.getPromotionType())) {
				sod.setProgramType(SaleOrderDetailVO.FREE_PRICE);
				sod.setPpType(SaleOrderDetailVO.FREE_PRICE);
				sod.setType(SaleOrderDetailVO.FREE_PRICE);
			} else if (PromotionType.ZV03.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
				sod.setType(SaleOrderDetailVO.FREE_PRODUCT);

			} else if (PromotionType.ZV04.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PERCENT);
				sod.setType(SaleOrderDetailVO.FREE_PERCENT);
			} else if (PromotionType.ZV05.getValue().equals(sod.getPromotionType())) {

				sod.setPpType(SaleOrderDetailVO.FREE_PRICE);
				sod.setType(SaleOrderDetailVO.FREE_PRICE);
			} else if (PromotionType.ZV06.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
				sod.setType(SaleOrderDetailVO.FREE_PRODUCT);

			} else if (PromotionType.ZV07.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PERCENT);
				sod.setType(SaleOrderDetailVO.FREE_PERCENT);

			} else if (PromotionType.ZV08.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRICE);
				sod.setType(SaleOrderDetailVO.FREE_PRICE);

			} else if (PromotionType.ZV09.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
				sod.setType(SaleOrderDetailVO.FREE_PRODUCT);

			} else if (PromotionType.ZV10.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PERCENT);
				sod.setType(SaleOrderDetailVO.FREE_PERCENT);

			} else if (PromotionType.ZV11.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRICE);
				sod.setType(SaleOrderDetailVO.FREE_PRICE);

			} else if (PromotionType.ZV12.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
				sod.setType(SaleOrderDetailVO.FREE_PRODUCT);

			} else if (PromotionType.ZV13.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PERCENT);
				sod.setType(SaleOrderDetailVO.FREE_PERCENT);

			} else if (PromotionType.ZV14.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRICE);
				sod.setType(SaleOrderDetailVO.FREE_PRICE);

			} else if (PromotionType.ZV15.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
				sod.setType(SaleOrderDetailVO.FREE_PRODUCT);

			} else if (PromotionType.ZV16.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PERCENT);
				sod.setType(SaleOrderDetailVO.FREE_PERCENT);

			} else if (PromotionType.ZV17.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRICE);
				sod.setType(SaleOrderDetailVO.FREE_PRICE);

			} else if (PromotionType.ZV18.getValue().equals(sod.getPromotionType())) {
				sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
				sod.setType(SaleOrderDetailVO.FREE_PRODUCT);

			} else if (PromotionType.ZV19.getValue().equals(sod.getPromotionType())) {// Docmt-Amt-Percent				
				sod.setPpType(SaleOrderDetailVO.PROMOTION_FOR_ORDER);
				sod.setType(SaleOrderDetailVO.FREE_PERCENT);

			} else if (PromotionType.ZV20.getValue().equals(sod.getPromotionType())) {// Docmt-Amt-Amt
				sod.setProgramType(SaleOrderDetailVO.PROMOTION_FOR_ORDER);
				sod.setPpType(SaleOrderDetailVO.PROMOTION_FOR_ORDER);
				sod.setType(SaleOrderDetailVO.FREE_PRICE);

			} else if (PromotionType.ZV21.getValue().equals(sod.getPromotionType())) {// Docmt-Amt-FreeItem
				sod.setProgramType(SaleOrderDetailVO.PROMOTION_FOR_ZV21);
				sod.setPpType(SaleOrderDetailVO.PROMOTION_FOR_ZV21);
				sod.setType(SaleOrderDetailVO.FREE_PRODUCT);

			} else if (PromotionType.ZV24.getValue().equals(sod.getPromotionType())) {
				if (sod.getIsOwner() != null && sod.getIsOwner().equals(2)) {
					if (sod.getGroupLevel() != null) {
						groupLevel = sod.getGroupLevel();
						if (groupLevel.getHasProduct() != null && groupLevel.getHasProduct() == 1) {
							//sod.setPpType(SaleOrderDetailVO.PROMOTION_FOR_ZV21);
							sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
							sod.setType(SaleOrderDetailVO.FREE_PRODUCT);
						} else {
							//sod.setPpType(SaleOrderDetailVO.PROMOTION_FOR_ORDER);
							sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
							sod.setType(SaleOrderDetailVO.FREE_PRICE);
						}
					}
				} else {
					sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
					sod.setType(SaleOrderDetailVO.FREE_PRODUCT);
				}
			} else if (PromotionType.ZV23.getValue().equals(sod.getPromotionType())) {
				if (sod.getIsOwner() != null && sod.getIsOwner().equals(2)) {
					if (sod.getGroupLevel() != null) {
						groupLevel = sod.getGroupLevel();
						if (groupLevel.getHasProduct() != null && groupLevel.getHasProduct() == 1) {
							sod.setPpType(SaleOrderDetailVO.PROMOTION_FOR_ZV21);
							sod.setType(SaleOrderDetailVO.FREE_PRODUCT);
						} else {
							sod.setPpType(SaleOrderDetailVO.PROMOTION_FOR_ORDER);
							sod.setType(SaleOrderDetailVO.FREE_PRICE);
						}
					}
				} else {
					sod.setPpType(SaleOrderDetailVO.FREE_PRODUCT);
					sod.setType(SaleOrderDetailVO.FREE_PRODUCT);
				}
			}

		}
	}

	/**
	 * Tinh tien don hang
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public String payment() {
		errMsg = "";
		String errCodeForProductNotStock = "";
		result.put(ERROR, true);
		try {
			//moi bo sung tren giao dien cho chon shop
			Shop shopFilter = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shopFilter = shopMgr.getShopByCode(shopCode);
			}
			if (shopFilter == null) {
				result.put("errMsg", Configuration.getResourceBundle("catalog.unit.tree.incorrect"));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(salerCode)) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, salerCode));
				return JSON;
			}
			Staff salerStaff = staffMgr.getStaffByCode(salerCode);
			if (salerStaff == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, salerCode));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(customerCode)) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty")));
				return JSON;
			}
			String msg = this.checkOrderDate();
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put("errMsg", msg);
				return JSON;
			}
			Date oDate = DateUtil.parse(orderDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			//			customer = customerMgr.getCustomerByCodeEx(StringUtil.getFullCode(currentShop.getShopCode(), customerCode), ActiveType.RUNNING);
			customer = customerMgr.getCustomerByCodeEx(StringUtil.getFullCode(shopFilter.getShopCode(), customerCode), ActiveType.RUNNING);
			if (customer == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.null")));
				result.put(ERROR, true);
				return JSON;
			}
			if (customer.getStatus() != ActiveType.RUNNING) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty") + " - " + customerCode));
				return JSON;
			}
			List<CommercialSupportCodeVO> lstCommercialSupportCodeVOs = new ArrayList<CommercialSupportCodeVO>();
			List<Boolean> isExists = new ArrayList<Boolean>();
			CommercialSupportCodeVO vos;
			if (lstCommercialSupport != null) {
				String TYPE = "";
				for (int i = 0; i < lstCommercialSupport.size(); ++i) {
					if (!StringUtil.isNullOrEmpty(lstCommercialSupport.get(i))) {
						vos = new CommercialSupportCodeVO();
						vos.setCode(lstCommercialSupport.get(i));
						vos.setCustomerId(customer.getId());
						vos.setShopId(shopFilter.getId());
						TYPE = lstCommercialSupportType.get(i);
						if ("DISPLAY_SCORE".equals(TYPE)) {
							vos.setType(CommercialSupportType.DISPLAY_PROGRAM);
						} else if (StringUtil.isNullOrEmpty(TYPE)) {
							vos.setType(CommercialSupportType.DISPLAY_PROGRAM);
						} else {
							vos.setType(CommercialSupportType.PROMOTION_PROGRAM);
						}
						Product product = productMgr.getProductByCode(lstProduct.get(i));
						vos.setProductId(product.getId());
						lstCommercialSupportCodeVOs.add(vos);
					}
				}
				if (lstCommercialSupportCodeVOs.size() > 0) {
					/** Neu co ngay chot thi kiem tra CTHTTM theo ngay chot */
					if (oDate != null) {
						isExists = saleOrderMgr.checkIfCommercialSupportExists(lstCommercialSupportCodeVOs, (oDate), null);
					} else {
						isExists = saleOrderMgr.checkIfCommercialSupportExists(lstCommercialSupportCodeVOs, DateUtil.now(), null);
					}
				}
				for (int i = 0; i < isExists.size(); ++i) {
					if (!isExists.get(i)) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION, lstCommercialSupport.get(i)));
						return JSON;
					}
				}
			}
			/** Thong tin san pham ban */
			List<SaleOrderDetailVO> lstSalesOrderDetailVO = new ArrayList<SaleOrderDetailVO>();
			Product product = null;
			SaleOrderDetail saleOrderDetail = null;
			SaleOrderLot saleOrderLot = null;
			List<SaleOrderLot> listSaleOrderLot = null;
			Warehouse warehouse = null;
			BigDecimal totalAmount = BigDecimal.ZERO;
			String commercialType = "";
			Map<Long, List<SaleOrderLot>> mapSaleOrderLot = new HashMap<Long, List<SaleOrderLot>>();
			/** Danh sach san pham ban nhap vao */
			List<CommercialSupportVO> commercials = new ArrayList<CommercialSupportVO>();
			if (lstProduct == null) {
				lstProduct = new ArrayList<String>();
			}
			for (int i = 0; i < lstProduct.size(); ++i) {
				SaleOrderDetailVO sale_order_vos = new SaleOrderDetailVO();
				listSaleOrderLot = new ArrayList<SaleOrderLot>();
				product = productMgr.getProductByCode(lstProduct.get(i));
				sale_order_vos.setProduct(product);
				saleOrderDetail = new SaleOrderDetail();
				saleOrderDetail.setProduct(product);
				//saleOrderDetail.setPrice(productMgr.getPriceByProductAndShopId(product.getId(),currentShop.getId()));
				//				List<Price> lstPr = productMgr.getPriceByFullCondition(product.getId(), shopFilter.getId(), lockDate, shopFilter.getType().getId(), customer.getId(), customer.getChannelType() != null ? customer.getChannelType().getId(): null);
				SaleOrderFilter<Price> filter = new SaleOrderFilter<Price>();
				filter.setProductId(product.getId());
				filter.setShopId(shopFilter.getId());
				filter.setCustomerId(customer.getId());
				filter.setOrderDate(oDate);
				List<Price> lstPr = productMgr.getPriceByRpt(filter);
				if (lstPr == null || lstPr.size() == 0 || lstPr.size() > 1) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "create.order.payment.price", product.getProductCode()));
					result.put(ERROR, true);
					return JSON;
				}
				//				saleOrderDetail.setPrice(productMgr.getProductPriceForCustomer(product.getId(), customer));
				saleOrderDetail.setPrice(lstPr.get(0));
				saleOrderDetail.setPriceValue(saleOrderDetail.getPrice().getPrice());
				saleOrderDetail.setPackagePrice(saleOrderDetail.getPrice().getPackagePrice());
				saleOrderDetail.setShop(shopFilter);
				saleOrderDetail.setStaff(salerStaff);
				/** Neu co ngay chot thi luu ngay tao don hang theo ngay chot */
				saleOrderDetail.setOrderDate(oDate);

				if (lstQuantity.get(i) == null || lstQuantity.get(i) < 0) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
					result.put(ERROR, true);
					return JSON;
				}

				saleOrderDetail.setQuantity(lstQuantity.get(i));
				saleOrderDetail.setQuantityPackage(lstQuantityPackage.get(i));
				saleOrderDetail.setQuantityRetail(lstQuantityRetail.get(i));
				BigDecimal thungAmount = saleOrderDetail.getPackagePrice().multiply(new BigDecimal(saleOrderDetail.getQuantityPackage()));
				BigDecimal leAmount = saleOrderDetail.getPriceValue().multiply(new BigDecimal(saleOrderDetail.getQuantityRetail()));
				//				saleOrderDetail.setAmount(saleOrderDetail.getPriceValue().multiply(new BigDecimal(lstQuantity.get(i))));
				saleOrderDetail.setAmount(thungAmount.add(leAmount));
				//Luu danh sach sale_order_lot truong hop tach dong sp theo Kho
				if (!StringUtil.isNullOrEmpty(lstWarehouseId.get(i))) {
					String[] warehouseTmp = lstWarehouseId.get(i).split(",");
					String[] warehouseQuantityTmp = lstWarehouseQuantity.get(i).split(",");
					for (int j = 0; j < warehouseTmp.length; ++j) {
						warehouse = stockManagerMgr.getWarehouseById(Long.valueOf(warehouseTmp[j]));
						if (warehouse == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SP_PROMOTION));
							return JSON;
						}
						if (!ActiveType.RUNNING.equals(warehouse.getStatus())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "baocao.kho"));
							return JSON;
						}
						saleOrderLot = new SaleOrderLot();
						saleOrderLot.setProduct(product);
						saleOrderLot.setWarehouse(warehouse);
						saleOrderLot.setShop(shopFilter);//doi thanh currentShop -> shopFilter
						saleOrderLot.setStaff(salerStaff);
						saleOrderLot.setSaleOrder(saleOrder);
						saleOrderLot.setSaleOrderDetail(saleOrderDetail);
						saleOrderLot.setPrice(saleOrderDetail.getPrice());
						saleOrderLot.setPriceValue(saleOrderDetail.getPriceValue());
						saleOrderLot.setPackagePrice(saleOrderDetail.getPackagePrice());
						Integer quantityTmp = Integer.valueOf(warehouseQuantityTmp[j]);
						//Kiem tra so luong nhap  
						if (quantityTmp < 0) {
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
							result.put(ERROR, true);
							return JSON;
						}
						saleOrderLot.setQuantity(quantityTmp);
						if (saleOrderLot.getProduct().getConvfact() != null && !saleOrderLot.getProduct().getConvfact().equals(0)) {
							saleOrderLot.setQuantityPackage(quantityTmp / saleOrderLot.getProduct().getConvfact());
							saleOrderLot.setQuantityRetail(quantityTmp % saleOrderLot.getProduct().getConvfact());
						}
						saleOrderLot.setOrderDate(oDate);// Kiem tra currentShop da chot ngay hay chua, neu co thi luu ngay don hang la ngay chot
						saleOrderLot.setCreateDate(sysDate);
						saleOrderLot.setCreateUser(currentUser.getUserName());
						listSaleOrderLot.add(saleOrderLot);
					}
					if (listSaleOrderLot.size() > 0) {
						sale_order_vos.setSaleOrderLot(listSaleOrderLot);
						mapSaleOrderLot.put(saleOrderDetail.getProduct().getId(), listSaleOrderLot);
					}
				}

				sale_order_vos.setSaleOrderDetail(saleOrderDetail);
				if (lstCommercialSupport != null && lstCommercialSupport.get(i) != null) {
					/** Khong tinh tien voi san pham co kem CTHTTM */
					String TYPE = lstCommercialSupport.get(i);
					if (StringUtil.isNullOrEmpty(TYPE)) {
						/*
						 * yeu cau nghiep vu moi: chi can san pham co gia la co
						 * the ban, ko quan tam san pham thuoc nganh hang Z hay
						 * co gia khac 0 hay ko
						 * 
						 * @modified by tuannd20
						 * 
						 * @date 17/12/2014
						 */
						/*
						 * if (saleOrderDetail.getPrice() == null ||
						 * (saleOrderDetail.getPrice() != null &&
						 * saleOrderDetail.getPrice().getPrice().signum() <=
						 * 0//saleOrderDetail
						 * .getPrice().getPrice().compareTo(BigDecimal.ZERO) <=
						 * 0 &&
						 * productMgr.checkProductInZ(product.getProductCode
						 * ()))) { result.put("errMsg",
						 * Configuration.getResourceString
						 * (ConstantManager.VI_LANGUAGE,
						 * "saleorder.spkm.err.not.in.z.but.price.null"
						 * ,product.getProductCode())); result.put(ERROR, true);
						 * return JSON; }
						 */
						BigDecimal temp = saleOrderDetail.getPrice().getPrice().multiply(new BigDecimal(lstQuantity.get(i)));
						totalAmount = totalAmount.add(temp);
						sale_order_vos.setProduct(product);
						lstSalesOrderDetailVO.add(sale_order_vos);
					} else {
						commercialType = lstCommercialSupportType.get(i);
						if (commercials != null) {
							for (CommercialSupportVO cs : commercials) {
								if (cs.getPromotionType() == null) {
									continue;
								}
								if (lstCommercialSupport.get(i).equals(cs.getCode()) && cs.getPromotionType().equals(commercialType)) {
								}
							}
						}
					}
				}
			}
			PromotionProductsVO productsVO = null;
			long channelTypeId = 0;
			if (customer.getChannelType() != null) {
				channelTypeId = customer.getChannelType().getId();
			}

			//Tinh KM
			SaleOrderVO saleOrderVO = new SaleOrderVO();
			/*
			 * productsVO =
			 * promotionProgramMgr.calculatePromotionProducts2(mapSaleOrderLot
			 * ,lstSalesOrderDetailVO,
			 * currentShop.getId(),currentShop.getShopChannel
			 * (),salerStaff.getId(),
			 * customer.getId(),channelTypeId,lockDay,null,saleOrderVO);
			 */
			if (orderId != null && orderId <= 0) {
				orderId = null;
			}
			/*
			 * productsVO =
			 * promotionProgramMgr.calculatePromotionProducts2(mapSaleOrderLot
			 * ,lstSalesOrderDetailVO,
			 * currentShop.getId(),currentShop.getShopChannel
			 * (),salerStaff.getId(), customer.getId(),channelTypeId,lockDay,
			 * orderId, saleOrderVO);
			 */
			List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.SYS_WAREHOUSE_CONFIG, ActiveType.RUNNING);
			if (aparams == null || aparams.size() == 0 || Constant.SYS_WAREHOUSE_CONFIG_DIVIDUAL.equals(aparams.get(0).getValue())) {
				isDividualWarehouse = true;
			} else {
				isDividualWarehouse = false;
			}
			CalculatePromotionFilter calculatePromotionFilter = new CalculatePromotionFilter();
			calculatePromotionFilter.setMapSaleOrderLot(mapSaleOrderLot);
			calculatePromotionFilter.setLstSalesOrderDetail(lstSalesOrderDetailVO);
			calculatePromotionFilter.setShopId(shopFilter.getId());
			calculatePromotionFilter.setShopChannel(shopFilter.getShopChannel());
			calculatePromotionFilter.setStaffId(salerStaff.getId());
			calculatePromotionFilter.setCustomerId(customer.getId());
			calculatePromotionFilter.setCustomerTypeId(channelTypeId);
			calculatePromotionFilter.setOrderDate(oDate);
			calculatePromotionFilter.setSaleOrderId(orderId);
			calculatePromotionFilter.setOrderVO(saleOrderVO);
			calculatePromotionFilter.setIsDividualWarehouse(isDividualWarehouse);
			productsVO = promotionProgramMgr.calculatePromotionProducts2(calculatePromotionFilter);
//			productsVO = promotionProgramMgr.calculatePromotionProducts2(mapSaleOrderLot, lstSalesOrderDetailVO, shopFilter.getId(), shopFilter.getShopChannel(), salerStaff.getId(), customer.getId(), channelTypeId, oDate, orderId, saleOrderVO);//lockDay
			/*
			 * if (orderId != null) { List<SaleOrderLot> lst1 =
			 * saleOrderMgr.getListSaleOrderLotBySaleOrderId(orderId); if (lst1
			 * != null && lst1.size() > 0) { SaleOrderLotVO lotVO = null;
			 * List<SaleOrderLotVO> lstLotVO = new ArrayList<SaleOrderLotVO>();
			 * for (SaleOrderLot lot : lst1) { lotVO = new SaleOrderLotVO();
			 * lotVO.setProductId(lot.getProduct().getId());
			 * lotVO.setWarehouseId(lot.getWarehouse().getId());
			 * lotVO.setQuantity(lot.getQuantity());
			 * 
			 * lstLotVO.add(lotVO); } result.put("lstLotVO", lstLotVO); } }
			 */

			List<SaleOrderDetailVO> list = new ArrayList<SaleOrderDetailVO>();
			if (productsVO == null) {
				productsVO = new PromotionProductsVO();
				productsVO.setListProductPromotionSale(list);
			}
			/*
			 * List<SaleOrderDetailVO> lstPromoProductMissing =
			 * productsVO.getSortListOutPut().get(10L); if
			 * (lstPromoProductMissing != null && lstPromoProductMissing.size()
			 * > 0) { String productCode = ""; for (SaleOrderDetailVO sodMissing
			 * : lstPromoProductMissing) { if
			 * (sodMissing.getSaleOrderDetail().getProduct() != null) { if
			 * (lstPromoProductMissing.indexOf(sodMissing) ==
			 * lstPromoProductMissing.size() - 1) { productCode +=
			 * sodMissing.getSaleOrderDetail().getProduct().getProductCode(); }
			 * else { productCode +=
			 * sodMissing.getSaleOrderDetail().getProduct().getProductCode() +
			 * ", "; } } } result.put("errMsgInvalidPrice",
			 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
			 * "saleorder.spkm.err.price.not.valid.or.equal.zero",productCode));
			 * }
			 */

			calcZV19_21(productsVO);
			/** Tich hop 2 FUNCTION tinh khuyen mai lai thanh 1 */
			result.put("pp", new ArrayList<SaleOrderDetailVO>());
			result.put("listBuyProduct", new ArrayList<SaleOrderDetailVO>());
			result.put("saleOrderQuantityReceivedList", new ArrayList<SaleOrderPromotionVO>());
			if (productsVO != null) {
				List<SaleOrderPromotion> listQuantityReceive = new ArrayList<SaleOrderPromotion>();
				listQuantityReceive.addAll(productsVO.orderQuantityReceivedList);
				listQuantityReceive.addAll(productsVO.productQuantityReceivedList);

				result.put("pp", productsVO.getListProductPromotionSale());
				result.put("listBuyProduct", productsVO.getListSaleProductReceivePromo());
				result.put("saleOrderQuantityReceivedList", listQuantityReceive);
				if (key != null && key > 0) {
					List<SaleOrderDetailVO> changePPromotion = null;
					if (typeSortKey == null || typeSortKey == 0) {
						changePPromotion = productsVO.getSortListOutPut().get(key);
					} else {
						changePPromotion = productsVO.getSortListOutPut().get(key);
					}
					boolean canChaneMultiProduct = true;
					boolean isZV21 = false;
					boolean isZV24 = false;
					String promotionProgramTypeCode = "";
					Integer quantityChange = 0;
					List<SaleOrderLot> lstLot;
					int totalAvailableQuantity;
					if (changePPromotion != null && changePPromotion.size() > 0) {
						List<Integer> lstIndexProductDonHaveStockTotal = new ArrayList<Integer>();
						List<String> lstProductCodeDonHaveStockTotal = new ArrayList<String>();
						Integer promotionQuotation = 0;
						for (SaleOrderDetailVO sodVO : changePPromotion) {
							if ("".equals(promotionProgramTypeCode) && quantityChange == 0) {
								promotionProgramTypeCode = sodVO.getSaleOrderDetail().getProgrameTypeCode();
								quantityChange = sodVO.getSaleOrderDetail().getQuantity();
								promotionQuotation = sodVO.getSaleOrderDetail().getPromotionQuotation();
							} else if (!promotionProgramTypeCode.equals(sodVO.getSaleOrderDetail().getProgrameTypeCode()) 
										|| !promotionQuotation.equals(sodVO.getSaleOrderDetail().getPromotionQuotation()) 
										|| !quantityChange.equals(sodVO.getSaleOrderDetail().getQuantity())) {
								canChaneMultiProduct = false;
							}
							if ("ZV21".equals(sodVO.getSaleOrderDetail().getProgrameTypeCode())) {
								isZV21 = true;
							} else if ("ZV24".equals(sodVO.getSaleOrderDetail().getProgrameTypeCode())) {
								/*
								 * if (SaleOrderDetailVO.
								 * PROMOTION_FOR_ORDER_TYPE_PRODUCT ==
								 * sodVO.getPromotionType()) { isZV21 = true; }
								 * else {
								 */
								isZV24 = true;
								//}
							}

							//Tach kho list SPKM
							if (sodVO.getSaleOrderDetail() != null) {
								lstLot = new ArrayList<SaleOrderLot>();
								if (sodVO.getPromoProduct() != null) {
									totalAvailableQuantity = 0;
									//Load danh sach SPKM dc huong theo Kho
									//List<StockTotal> lst = stockMgr.getListStockTotalByProductAndOwner(sodVO.getPromoProduct().getId(),StockObjectType.SHOP, currentShop.getId());
									StockTotalFilter stockTotalFilter = new StockTotalFilter();
									stockTotalFilter.setProductId(sodVO.getPromoProduct().getId());
									stockTotalFilter.setOwnerId(shopFilter.getId());//doi thanh currentShop -> shopFilter
									stockTotalFilter.setOrderId(orderId);
//									stockTotalFilter.setWarehouseType(WarehouseType.PROMOTION);
									if (isDividualWarehouse == null || isDividualWarehouse) {
										stockTotalFilter.setWarehouseType(WarehouseType.PROMOTION);
									} else {
										stockTotalFilter.setOrderByType(WarehouseType.PROMOTION);
									}
									List<StockTotal> lst = stockMgr.getListStockTotalByProductAndShop(stockTotalFilter);
									if (lst != null && lst.size() > 0) {
										totalAvailableQuantity = sodVO.getSaleOrderDetail().getQuantity();
										//Tach kho theo stock_Total va warehouse Id
										for (StockTotal stt : lst) {
											if (totalAvailableQuantity > 0 && stt.getAvailableQuantity() != null && stt.getAvailableQuantity() > totalAvailableQuantity) {
												/*
												 * khi doi san pham khuyen mai
												 * thi hien thi het tat ca cac
												 * kho co the va chi lay kho nao
												 * co ton kho cao hon thuc dat ,
												 * neu duyet tat ca kho ma ko co
												 * thi se lay kho mac dinh dau
												 * tien co seq cao nhat
												 */
												SaleOrderLot lot = new SaleOrderLot();
												lot.setProduct(sodVO.getSaleOrderDetail().getProduct());
												lot.setWarehouse(stt.getWarehouse());
												lot.setStockTotal(stt);
												lot.setQuantity(totalAvailableQuantity);
												lstLot.add(lot);
												break;
											}
										}
										if (lstLot.size() <= 0) {
											int it = 0;
											for (StockTotal stt : lst) {
												if (stt.getAvailableQuantity() != null && stt.getAvailableQuantity() > 0) {
													break;
												}
												it++;
											}
											if (it >= lst.size()) {
												it = 0;
											}
											StockTotal st = lst.get(it);
											SaleOrderLot lot = new SaleOrderLot();
											lot.setProduct(sodVO.getSaleOrderDetail().getProduct());
											lot.setWarehouse(st.getWarehouse());
											lot.setStockTotal(st);
											lot.setQuantity(totalAvailableQuantity);
											lstLot.add(lot);
										}
									} else {
										lstIndexProductDonHaveStockTotal.add(changePPromotion.indexOf(sodVO));
										lstProductCodeDonHaveStockTotal.add(sodVO.getPromoProduct().getProductCode());
									}
								} else {
									SaleOrderLot lot = new SaleOrderLot();
									lot.setSaleOrderDetail(sodVO.getSaleOrderDetail());
									lstLot.add(lot);
								}
								sodVO.setSaleOrderLot(lstLot);
							}
						}
						//Remove nhung SPKM khong co stock_total
						for (Integer index : lstIndexProductDonHaveStockTotal) {
							changePPromotion.remove(index);
						}

						for (String tmpCode : lstProductCodeDonHaveStockTotal) {
							if (lstProductCodeDonHaveStockTotal.indexOf(tmpCode) == lstProductCodeDonHaveStockTotal.size() - 1) {
								errCodeForProductNotStock = tmpCode;
							} else {
								errCodeForProductNotStock = tmpCode + ", ";
							}
						}
					}
					result.put("changePPromotion", changePPromotion);
					result.put("canChaneMultiProduct", canChaneMultiProduct);
					result.put("errCodeForProductNotStock", errCodeForProductNotStock);
					result.put("isZV21", isZV21);
					result.put("isZV24", isZV24);
				}
			}

			if ((key == null || key < 1) && orderId != null && orderId > 0) {
				SoFilter filter1 = new SoFilter();
				filter1.setShopId(shopFilter.getId());//doi thanh currentShop -> shopFilter
				filter1.setLockDay(oDate);
				filter1.setSaleOrderId(orderId);
				List<OrderProductVO> lstWarningPromotion = saleOrderMgr.getListWarningPromotionOfOrder(filter1);
				result.put("lstWarningPromotion", lstWarningPromotion);
			}

			/*
			 * tinh KM tich luy
			 */
			//			try {
			//				List<PromotionItem> accumulativePromotionItems = this.calculatePromotionForZV23(shopFilter.getId(), customer.getId(), salerStaff.getId(), oDate);//doi thanh currentShop -> shopFilter
			//				result.put("accumulativePromotionItems", accumulativePromotionItems);
			//			} catch (Exception e) {
			////				String exceptionMsg = "Fail to calculate accumulative promotion program for customer_id = " + customer.getId() + ".\n" + e.getMessage();
			//				String exceptionMsg = Configuration.getResourceBundle("exception.code.payment")  + e.getMessage();
			//				LogUtility.logError(e, exceptionMsg);
			//			}

			result.put(ERROR, false);
		} catch (Exception ex) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.payment"), createLogErrorStandard(actionStartTime));
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * tinh KM tich luy cho KH
	 * 
	 * @author tuannd20
	 * @param shopId
	 * @param customerId
	 * @param staffId
	 * @param orderDate
	 * @return
	 * @throws BusinessException
	 * @date 12/12/2014
	 */
//	private List<PromotionItem> calculatePromotionForZV23(Long shopId, Long customerId, Long staffId, Date orderDate) throws BusinessException {
//		return promotionProgramMgr.calculatePromotionZV23(shopId, customerId, staffId, orderDate, orderId);
//	}

	/**
	 * Gom nhom KM don hang
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public void calcZV19_21(PromotionProductsVO productsVO) throws Exception {
		if (productsVO != null && productsVO.getListProductPromotionSale() != null) {
			Boolean hasChangeProgram = false;
			List<SaleOrderDetailVO> lstZV21 = new ArrayList<SaleOrderDetailVO>();
			List<SaleOrderDetailVO> lstZV1920 = new ArrayList<SaleOrderDetailVO>();
			for (SaleOrderDetailVO vo : productsVO.getListProductPromotionSale()) {
				if (productsVO.getListPromotionForOrderChange() != null && productsVO.getListPromotionForOrderChange().size() > 1) {
					if (vo.getPromotionType() == ConstantManager.PROMOTION_FOR_ORDER || vo.getPromotionType() == ConstantManager.PROMOTION_FOR_ZV21) {
						if (vo.getPromotionType() == ConstantManager.PROMOTION_FOR_ZV21) {
							vo.setKeyList(-1L);
						} else {
							vo.setKeyList(-2L);
						}
						List<PromotionProgram> pp = new ArrayList<PromotionProgram>();
						List<SaleOrderDetailVO> zv19Zv20 = new ArrayList<SaleOrderDetailVO>();
						List<SaleOrderDetailVO> zv21 = new ArrayList<SaleOrderDetailVO>();
						for (int i = 0, sz = productsVO.getListPromotionForOrderChange().size(); i < sz; ++i) {
							SaleOrderDetailVO order = productsVO.getListPromotionForOrderChange().get(i);
							String ppCode = order.getSaleOrderDetail().getProgramCode();

							PromotionProgram p = promotionProgramMgr.getPromotionProgramByCode(ppCode);
							if (PromotionType.ZV24.getValue().equals(p.getType())) {
								continue;
							}

							if (!hasChangeProgram) {
								for (SaleOrderDetailVO voTmp : productsVO.getListPromotionForOrderChange()) {
									if (!voTmp.getSaleOrderDetail().getProgramCode().equals(ppCode) && productsVO.getListPromotionForOrderChange().indexOf(voTmp) != i) {
										hasChangeProgram = true;
										break;
									}
								}
							}

							if (order.getPromotionType() == ConstantManager.PROMOTION_FOR_ZV21) {
								zv21.add(order);
							} else {
								zv19Zv20.add(order);
								if (order.getPromotionType() == ConstantManager.PROMOTION_FOR_ORDER && order.getType() == ConstantManager.FREE_PERCENT) {
									p.setType("ZV19");
								} else if (order.getPromotionType() == ConstantManager.PROMOTION_FOR_ORDER && order.getType() == ConstantManager.FREE_PRICE) {
									p.setType("ZV20");
								}
							}
							if (pp.size() > 0) {
								boolean isExist = false;
								for (PromotionProgram pTmp : pp) {
									if (pTmp.getId().equals(p.getId())) {
										isExist = true;
									}
								}
								if (!isExist) {
									pp.add(p);
								}
							} else {
								pp.add(p);
							}
						}
						if (hasChangeProgram) {
							vo.setChangeProduct(1);
						}
						result.put("programes", pp);
						result.put("zV19zV20C", zv19Zv20);
						result.put("zV21C", zv21);
					}
				}
				if (vo.getOpenPromo() != 1) {
					if (vo.getPromotionType() == ConstantManager.PROMOTION_FOR_ZV21) {
						lstZV21.add(vo);
					} else if (vo.getPromotionType() == ConstantManager.PROMOTION_FOR_ORDER || vo.getPromotionType() == ConstantManager.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
						lstZV1920.add(vo);
					}
				}
			}
			if (lstZV21.size() > 0) {
				result.put("zV21", lstZV21);
			}
			if (lstZV1920.size() > 0) {
				result.put("zV19zV20", lstZV1920);
			}

			if (productsVO.getListPromotionForOrderChange() != null && productsVO.getListPromotionForOrderChange().size() > 0) {
				List<SaleOrderDetailVO> zv19_20MM = new ArrayList<SaleOrderDetailVO>();
				List<SaleOrderDetailVO> zv21MM = new ArrayList<SaleOrderDetailVO>();
				for (int i = 0, sz = productsVO.getListPromotionForOrderChange().size(); i < sz; ++i) {
					SaleOrderDetailVO order = productsVO.getListPromotionForOrderChange().get(i);
					String ppCode = order.getSaleOrderDetail().getProgramCode();

					PromotionProgram p = promotionProgramMgr.getPromotionProgramByCode(ppCode);
					if (PromotionType.ZV24.getValue().equals(p.getType())) {
						if (order.getPromotionType() == ConstantManager.PROMOTION_FOR_ZV21) {
							zv21MM.add(order);
						} else {
							zv19_20MM.add(order);
						}
					}
				}
				result.put("zV19_20MM", zv19_20MM);
				result.put("zV21MM", zv21MM);
			}
		}
	}

	public void createAccumutive(SaleOrder so) {
		try {
			Date dateNow = commonMgr.getSysDate();
			String userStr = getLogInfoVO().getStaffCode();
			ObjectVO<SaleOrderDetail> objSOD = saleOrderMgr.getListSaleOrderDetailBySaleOrderId(null, so.getId());
			List<SaleOrderDetail> lstSOD = new ArrayList<SaleOrderDetail>();
			if (objSOD != null && objSOD.getLstObject() != null) {
				for (SaleOrderDetail sod : objSOD.getLstObject()) {
					if (sod.getIsFreeItem() == 0) {
						lstSOD.add(sod);
					}
				}
			}
			BigDecimal totalDiscount = BigDecimal.ZERO;
			BigDecimal totalWeight = BigDecimal.ZERO;
			Long totalDetail = so.getTotalDetail();
			for (GroupLevelDetailVO temp : lstCTTL) {
				//lay danh sach tich luy
				PromotionProgram pp = promotionProgramMgr.getPromotionProgramByCode(temp.getProgramCode());
				RptAccumulativePromotionProgramVO accumulativePPVO = promotionProgramMgr.getRptAccumulativePPForCustomer(so.getShop().getId(), so.getCustomer().getId(), pp.getId(), so.getOrderDate());
				RptAccumulativePromotionProgram rpp = accumulativePPVO.getRptAccumulativePP();
				Map<Long, Object> mapRptDetail = new HashMap<Long, Object>();
				if (accumulativePPVO != null && accumulativePPVO.getRptAccumulativePPDetail() != null && accumulativePPVO.getRptAccumulativePPDetail().size() > 0) {
					List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetails = accumulativePPVO.getRptAccumulativePPDetail();
					for (RptAccumulativePromotionProgramDetail rptDetail : rptAccumulativePPDetails) {
						mapRptDetail.put(rptDetail.getProduct().getId(), rptDetail);
					}
				}
				//SALE_ORDER_DETAIL
				if (temp.getProductId() != null) {
					Map<Long, SaleOrderDetail> mapProductId = new HashMap<Long, SaleOrderDetail>();
					for (GroupLevelDetailVO vo : temp.getPromoProducts()) {
						if (vo.getProductId() != null) {
							SaleOrderDetail sod = mapProductId.get(vo.getProductId());
							if (sod == null) {
								sod = new SaleOrderDetail();
								sod.setSaleOrder(so);
								sod.setOrderDate(so.getOrderDate());
								sod.setShop(so.getShop());
								sod.setStaff(so.getStaff());
								sod.setQuantity(vo.getValueActually() != null ? vo.getValueActually().intValue() : vo.getValue().intValue());
								Product product = productMgr.getProductById(vo.getProductId());
								if (product != null) {
									sod.setProduct(product);
									sod.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(sod.getQuantity())));
									totalWeight = totalWeight.add(sod.getTotalWeight());
								}
								sod.setIsFreeItem(1);
								sod.setGroupLevelId(vo.getGroupLevelId());
								sod.setProductGroupId(vo.getProductGroupId());
								//								sod.setJoinProgramCode(vo.getProgramCode());
								sod.setProgramCode(vo.getProgramCode());
								sod.setProgramType(ProgramType.AUTO_PROM);
								sod.setProgrameTypeCode("ZV23");
								sod.setMaxQuantityFree(vo.getMaxQuantity());
								sod.setPrice(productMgr.getProductPriceForCustomer(product.getId(), customer));
								sod.setPriceValue(sod.getPrice().getPrice());
								sod.setPriceNotVat(sod.getPrice().getPriceNotVat());
								sod.setVat(sod.getPrice().getVat());
								sod.setAmount(sod.getPrice().getPrice().multiply(new BigDecimal(sod.getQuantity())));
								sod.setPayingOrder(vo.getPayingOrder());
								sod.setCreateDate(dateNow);
								sod.setCreateUser(userStr);
								sod = saleOrderMgr.createSaleOrderDetail(sod);
								totalDetail++;
								mapProductId.put(vo.getProductId(), sod);
							}

							StockTotal st = saleOrderMgr.getStockTotalByProductAndOwnerForUpdate(sod.getProduct().getId(), StockObjectType.SHOP, so.getShop().getId(), vo.getWarehouseId());
							st.setAvailableQuantity(st.getAvailableQuantity() - sod.getQuantity());
							commonMgr.updateEntity(st);

							SaleOrderLot saleOrderLot = new SaleOrderLot();
							saleOrderLot.setProduct(sod.getProduct());
							saleOrderLot.setWarehouse(stockManagerMgr.getWarehouseById(vo.getWarehouseId()));
							saleOrderLot.setShop(so.getShop());
							saleOrderLot.setStaff(so.getStaff());
							saleOrderLot.setSaleOrder(so);
							saleOrderLot.setSaleOrderDetail(sod);
							saleOrderLot.setPrice(sod.getPrice());
							saleOrderLot.setPriceValue(sod.getPriceValue());
							saleOrderLot.setPackagePrice(sod.getPackagePrice());
							saleOrderLot.setQuantity(sod.getQuantity());
							saleOrderLot.setOrderDate(lockDay);// Kiem tra currentShop da chot ngay hay chua, neu co thi luu ngay don hang la ngay chot
							saleOrderLot.setCreateDate(sysDate);
							saleOrderLot.setCreateUser(currentUser.getUserName());
							saleOrderMgr.createSaleOrderLot(saleOrderLot);
						}
					}
				} else {// KM tien & %
					totalDiscount = totalDiscount.add(temp.getValue());
					//BigDecimal amount = so.getAmount();
					for (int i = 0; i < lstSOD.size(); i++) {
						SaleOrderDetail sod = lstSOD.get(i);
						//						BigDecimal sodDiscount = sod.getAmount().multiply(temp.getValue());
						//						sodDiscount = sodDiscount.divide(amount, 0, RoundingMode.HALF_UP);
						//sod.setDiscountAmount(sod.getDiscountAmount().add(sodDiscount));
						BigDecimal sodDiscount = BigDecimal.ZERO;
						for (GroupLevelDetailVO vo : temp.getShareDiscount()) {
							if (sod.getProduct().getProductCode().equals(vo.getProductCode())) {
								sodDiscount = vo.getValue();
							}
						}
						SaleOrderPromoDetail sop = new SaleOrderPromoDetail();
						sop.setSaleOrderDetail(sod);
						sop.setMaxAmountFree(temp.getValue());
						sop.setDiscountAmount(sodDiscount);
						sop.setGroupLevelId(temp.getGroupLevelId());
						sop.setProductGroupId(temp.getProductGroupId());
						sop.setOrderDate(so.getOrderDate());
						sop.setIsOwner(2);
						sop.setSaleOrder(so);
						sop.setShop(so.getShop());
						sop.setStaff(so.getStaff());
						if (rpp != null && rpp.getTotalAmount() != null && !BigDecimal.ZERO.equals(rpp.getTotalAmount())) {
							sop.setDiscountPercent(temp.getValue().floatValue() * 100 / rpp.getTotalAmount().floatValue());
						}
						if (temp.getPercent() != null) {
							sop.setDiscountPercent(temp.getPercent());
						}
						sop.setProgramCode(temp.getProgramCode());
						sop.setProgrameTypeCode("ZV23");
						sop.setProgramType(ProgramType.AUTO_PROM);
						commonMgr.createEntity(sop);
						//saleOrderMgr.updateSaleOrderDetail(sod);
					}
				}

				//------------------------------------------------------------------------------------------------
				//RPT_CTTL_PAY & RPT_CTTL_PAY_DETAIL
				RptCTTLPay pay = new RptCTTLPay();
				if (pp != null) {
					pay.setPromotionProgram(pp);
					pay.setPromotionFromDate(pp.getFromDate());
					pay.setPromotionToDate(pp.getToDate());
				}
				pay.setCustomer(so.getCustomer());
				pay.setShop(so.getShop());
				pay.setStaff(so.getStaff());
				pay.setPromotion(1);
				if (temp.getPromotion() != null) {
					pay.setPromotion(temp.getPromotion());
				}
				pay.setApproved(so.getApproved().getValue());
				pay.setSaleOrder(so);
				BigDecimal totalAmountPromotion = BigDecimal.ZERO;
				BigDecimal totalQuantityPromotion = BigDecimal.ZERO;
				pay.setTotalAmountPayPromotion(totalAmountPromotion);
				pay.setTotalQuantityPayPromotion(totalQuantityPromotion);
				//				if(!ValueType.QUANTITY.getValue().equals(temp.getValueType())){// KM tien or %
				//					pay.setActuallyPromotion(temp.getValueActually());
				//					pay.setAmountPromotion(temp.getValue());
				//				}
				pay.setCreateDate(so.getOrderDate());

				pay = promotionProgramMgr.createRptCTTLPay(pay, getLogInfoVO());

				for (GroupLevelDetailVO vo : temp.getBuyProducts()) {
					RptCTTLPayDetail rptDetail = new RptCTTLPayDetail();
					rptDetail.setRptCTTLPay(pay);
					rptDetail.setShop(so.getShop());
					rptDetail.setStaff(so.getStaff());
					if (vo.getProductId() != null) {
						Product product = productMgr.getProductById(vo.getProductId());
						rptDetail.setProduct(product);
					}
					if (ValueType.QUANTITY.getValue().equals(vo.getValueType())) {
						rptDetail.setQuantityPromotion(vo.getValue());
						rptDetail.setAmountPromotion(this.getAmountOrQuantityProductAccumulative(mapRptDetail, vo.getProductId(), vo.getValue(), ValueType.AMOUNT));
					} else {
						rptDetail.setAmountPromotion(vo.getValue());
						rptDetail.setQuantityPromotion(this.getAmountOrQuantityProductAccumulative(mapRptDetail, vo.getProductId(), vo.getValue(), ValueType.QUANTITY));
					}
					totalQuantityPromotion = totalQuantityPromotion.add(rptDetail.getQuantityPromotion());
					totalAmountPromotion = totalAmountPromotion.add(rptDetail.getAmountPromotion());
					rptDetail.setCreateDate(so.getOrderDate());
					promotionProgramMgr.createRptCTTLPayDetail(rptDetail, getLogInfoVO());
				}
				if (!BigDecimal.ZERO.equals(totalAmountPromotion)) {
					pay.setTotalAmountPayPromotion(totalAmountPromotion);
					pay.setTotalQuantityPayPromotion(totalQuantityPromotion);
					commonMgr.updateEntity(pay);
				}
			}
			so.setTotalWeight(so.getTotalWeight().add(totalWeight));
			/*
			 * if(so.getDiscount()==null){ so.setDiscount(BigDecimal.ZERO); }
			 * so.setDiscount(so.getDiscount().add(totalDiscount));
			 * so.setTotal(so.getAmount().subtract(so.getDiscount()));
			 */
			so.setTotalDetail(totalDetail);
			saleOrderMgr.updateSaleOrder(so);
			saleOrderMgr.updateListSaleOrderDetail(lstSOD);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.createAccumutive"), createLogErrorStandard(actionStartTime));
		}
	}

	/*
	 * convert Amount va quantity valueType 1: get quantity, 2: get amount
	 * tungmt 24/12/2014
	 */
	public BigDecimal getAmountOrQuantityProductAccumulative(Map<Long, Object> mapRptDetail, Long productId, BigDecimal value, ValueType valueType) {
		BigDecimal result = BigDecimal.ZERO;
		RptAccumulativePromotionProgramDetail temp = (RptAccumulativePromotionProgramDetail) mapRptDetail.get(productId);
		if (temp != null) {
			if (ValueType.QUANTITY.equals(valueType) && !BigDecimal.ZERO.equals(temp.getAmount())) {//quantity
				result = value.multiply(temp.getQuantity()).divide(temp.getAmount(), 2, RoundingMode.UP);
			} else if (!BigDecimal.ZERO.equals(temp.getQuantity())) {//amount
				result = value.multiply(temp.getAmount()).divide(temp.getQuantity(), 0, RoundingMode.UP);
			}
		}
		return result;
	}
	
	/**
	 * Tao don hang + Chinh sua don hang
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public String createSaleOrder() {
		result.put("flag", false);
		if (DEBUG) {
			result.put("quantityReceivedList", quantityReceivedList);
			result.put("lstSOPromoDetail", lstSOPromoDetail);
		}
		try {
			//Validate cac thong tin truyen tu client
			if (!validateGeneralInfor()) {
				return JSON;
			}
			Staff deliveryStaff = null;
			Staff cashierStaff = null;
			if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
				Shop sTmp = shopMgr.getShopByCode(shopCodeFilter);
				if (sTmp != null && super.getMapShopChild().get(sTmp.getId()) != null) {
					currentShop = sTmp;
				}
			}
			if (currentShop != null && currentShop.getId() != null) {
				deliveryStaff = StringUtil.isNullOrEmpty(deliveryCode) ? null : staffMgr.getStaffByCodeAndShopId(deliveryCode, currentShop.getId());
				cashierStaff = StringUtil.isNullOrEmpty(cashierCode) ? null : staffMgr.getStaffByCodeAndShopId(cashierCode, currentShop.getId());
			}
			Staff salerStaff = StringUtil.isNullOrEmpty(salerCode) ? null : staffMgr.getStaffByCodeAndShopId1(salerCode, currentShop.getId());
			if (salerStaff != null && !checkStaff(salerStaff, StaffObjectType.NVBH) && !checkStaff(salerStaff, StaffObjectType.NVVS)) {
				//				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SS_NOT_GENERAL, salerStaff.getStaffCode(), StaffObjectType.NVBH.toString()));
				//				result.put(ERROR, true);
				return JSON;
			}

			/*
			 * check saler, cashier, delivery-staff is managed by login staff
			 */
			/*
			 * if (!checkStaffByStaffRoot(salerStaff.getStaffCode()) ||
			 * (cashierStaff != null &&
			 * !checkShopInOrgAccessById(cashierStaff.getShop().getId())) ||
			 * (deliveryStaff != null &&
			 * !checkShopInOrgAccessById(deliveryStaff.getShop().getId()))) {
			 * result.put(ERROR, true); result.put("errMsg",
			 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
			 * "common.cms.staff.undefined")); return JSON; }
			 */

			// TODO: INSERT INTO SALE_ORDER.
			//  Luu thong tin cua don hang */
			SaleOrder saleOrder;
			if (orderId != null && orderId > 0) {
				saleOrder = saleOrderMgr.getSaleOrderById(orderId);
				if (saleOrder == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.order.title"));
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				if (SaleOrderSource.WEB.equals(saleOrder.getOrderSource()) && (!SaleOrderStatus.NOT_YET_APPROVE.equals(saleOrder.getApproved()) || !SaleOrderStep.CONFIRMED.equals(saleOrder.getApprovedStep()))) {
					result.put("errMsg", R.getResource("sp.search.sale.order.must.confirm"));
					result.put(ERROR, true);
					return JSON;
				}
				if (lockDay != null && SaleOrderSource.TABLET.equals(saleOrder.getOrderSource()) && saleOrder.getOrderDate() != null && DateUtil.compareDateWithoutTime(saleOrder.getOrderDate(), lockDay) != 0) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.error.not.valid.lockday.remove"));
					result.put(ERROR, true);
					return JSON;
				}
				saleOrder.setOrderDate(DateUtil.addTime(lockDay));
				saleOrder.setUpdateUser(currentUser.getUserName());
				saleOrder.setUpdateDate(sysDate);
			} else {
				saleOrder = new SaleOrder();
				saleOrder.setCreateUser(currentUser.getUserName());
				saleOrder.setCreateDate(sysDate);
				saleOrder.setOrderDate(DateUtil.addTime(lockDay));
				saleOrder.setShop(currentShop);
				saleOrder.setCustomer(customer);
				saleOrder.setOrderType(OrderType.IN);
				//				saleOrder.setVat(10f);//trungtm6
				saleOrder.setOrderSource(SaleOrderSource.WEB);
				saleOrder.setApprovedStep(SaleOrderStep.CONFIRMED);
				saleOrder.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
			}
			//set cycle
			saleOrder.setCycle(cycleMgr.getCycleByDate(saleOrder.getOrderDate()));

			if (lstKeyShop != null && lstKeyShop.size() > 0) {
				saleOrder.setIsRewardKs(1);
			} else {
				saleOrder.setIsRewardKs(0);
			}

			saleOrder.setCar(car);
			if (priorityId != 0) {
				saleOrder.setPriority(priorityId);
			}
			if (!StringUtil.isNullOrEmpty(deliveryDate)) {
				Date date = DateUtil.parse(deliveryDate, ConstantManager.FULL_DATE_FORMAT);
				if (date != null)
					saleOrder.setDeliveryDate(date);
			}
			saleOrder.setStaff(salerStaff);
			saleOrder.setDelivery(deliveryStaff);
			saleOrder.setCashier(cashierStaff);

			//Kiem tra don hang trong tuyen va ngoai tuyen
			//Tim duoc nhan vien ban hang thi co nghia la trong tuyen IS_VISIT_PLAN =1,
			//nguoc lai thi ngoai tuyen IS_VISIT_PLAN =0 			
			Staff sStaff = staffMgr.getSaleStaffByDateForCreateOrder(customer.getId(), currentShop.getId(), lockDay, salerStaff.getId());
			if (sStaff != null && sStaff.getStaffCode().equals(salerStaff.getStaffCode())) {
				saleOrder.setIsVisitPlan(1);
			} else {
				saleOrder.setIsVisitPlan(0);
			}

			//Danh sach san pham ban 
			List<SaleProductVO> lstSaleProductVO = new ArrayList<SaleProductVO>();

			//Map luu sp ban sort theo ma sp
			Map<String, Integer> indexOfSaleProduct = new HashMap<String, Integer>();

			//Danh sach san pham khuyen mai 
			List<SaleProductVO> lstPromoProductVO = new ArrayList<SaleProductVO>();

			// Danh sach san pham ban theo Kho 
			List<SaleOrderLot> lstSaleOrderLot;

			// Danh sach san pham ban theo Kho 
			List<SaleOrderLot> lstPromoSaleOrderLot;

			Warehouse warehouse = null;
			Product product;
			SaleOrderDetail saleOrderDetail = null;
			SaleProductVO productVO = null;

			SaleProductVO promoProductVO = null;
			SaleOrderLot saleOrderLot = null;
			//SaleOrderPromoDetail sodPromoDetail = null;
			/*
			 * Lay thong tin danh sach san pham ban Tu do lay danh sach cac san
			 * pham khuyen mai duoc ap dung cho don hang luu thong tin bao gom
			 * hoa don va cac thong tin ve san pham ban, san pham khuyen mai
			 */

			PromotionProgram program = null;
			ObjectVO<CommercialSupportVO> commercials = saleOrderMgr.getListCommercialSupport(null, currentShop.getId(), customer.getId(), lockDay, null);
			String commercialType = "";
			BigDecimal totalAmountMoneyBuy = BigDecimal.ZERO;
			BigDecimal amount = BigDecimal.ZERO;
			BigDecimal discount = BigDecimal.ZERO;
			BigDecimal totalWeight = BigDecimal.ZERO;
			//MathContext mc = new MathContext(2, RoundingMode.UP);

			Map<String, Integer> mapRemainPortion = new HashMap<String, Integer>();

			// TODO: SAVE TO SALE_ORDER_DETAIL.
			//Luu danh sach sale_order_detail
			Long totalSaleQuan = 0l;
			for (int i = 0; i < lstProduct.size(); ++i) {
				productVO = new SaleProductVO();
				//Luu bien dung de check don hang su dung loai KM don hang nao ( 19 , 20 hoac 21)
				productVO.setIsUsingZVOrder(isUsingZVOrder);
				productVO.setIsUsingZVCode(isUsingZVCode);
				lstSaleOrderLot = new ArrayList<SaleOrderLot>();
				product = productMgr.getProductByCode(lstProduct.get(i));
				saleOrderDetail = new SaleOrderDetail();
				saleOrderDetail.setProduct(product);
				saleOrderDetail.setProductInfo(product.getCat());
				saleOrderDetail.setConvfact(product.getConvfact());
				//saleOrderDetail.setPrice(productMgr.getPriceByProductAndShopId(product.getId(),currentShop.getId()));
				//trungtm6 sua gia
				SaleOrderFilter<Price> filter = new SaleOrderFilter<Price>();
				filter.setProductId(product.getId());
				filter.setShopId(currentShop.getId());
				filter.setCustomerId(customer.getId());
				filter.setOrderDate(lockDay);
				List<Price> lstPr = productMgr.getPriceByRpt(filter);
				if (lstPr != null && lstPr.size() > 0) {
					saleOrderDetail.setPrice(lstPr.get(0));
					saleOrderDetail.setPriceValue(lstPr.get(0).getPrice());
					saleOrderDetail.setPriceNotVat(lstPr.get(0).getPriceNotVat());
					saleOrderDetail.setPackagePrice(lstPr.get(0).getPackagePrice());
					saleOrderDetail.setPackagePriceNotVAT(lstPr.get(0).getPackagePriceNotVat());
				}
				//set gia cho orderDetail
				

				//khong cho sua gia nen dong lai
//				if (lstPriceValue != null && lstPriceValue.size() > i && lstPriceValue.get(i) != null) {
//					saleOrderDetail.setPriceValue(lstPriceValue.get(i));
//					if (saleOrderDetail.getPrice() != null && saleOrderDetail.getPrice().getVat() != null) {
//						BigDecimal p = lstPriceValue.get(i).divide(new BigDecimal(1 + saleOrderDetail.getPrice().getVat() / 100), ConstantManager.MAX_DECIMAL_NUMBER_FOR_PRICE, RoundingMode.HALF_UP);
//						saleOrderDetail.setPriceNotVat(p);
//					}
//				}
//				if (lstpkPriceValue != null && lstpkPriceValue.size() > i && lstpkPriceValue.get(i) != null) {
//					saleOrderDetail.setPackagePrice(lstpkPriceValue.get(i));
//					if (saleOrderDetail.getPrice() != null && saleOrderDetail.getPrice().getVat() != null) {
//						BigDecimal p = lstpkPriceValue.get(i).divide(new BigDecimal(1 + saleOrderDetail.getPrice().getVat() / 100), ConstantManager.MAX_DECIMAL_NUMBER_FOR_PRICE, RoundingMode.HALF_UP);
//						saleOrderDetail.setPackagePriceNotVAT(p);
//					}
//				}
				//----------------------------------------------------

				saleOrderDetail.setShop(currentShop);
				if (saleOrderDetail.getPrice() != null) {
					saleOrderDetail.setVat(saleOrderDetail.getPrice().getVat());
				}
				saleOrderDetail.setStaff(salerStaff);

				if (lstQuantity.get(i) == null || lstQuantity.get(i) < 0) {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
					result.put(ERROR, true);
					return JSON;
				}

				//Luu gia tri CKMH && % CK cua tung dong sp ban va tinh tong CKMH cua don hang
				if (lstSaleProductDiscountAmount != null && !StringUtil.isNullOrEmpty(lstSaleProductDiscountAmount.get(i))) {
					BigDecimal discountAmount = BigDecimal.ZERO;
					if (!StringUtil.isNullOrEmpty(lstSaleProductDiscountAmount.get(i))) {
						String[] discountAmounts = lstSaleProductDiscountAmount.get(i).split(",");
						for (int k = 0; k < discountAmounts.length; discountAmount = discountAmount.add(new BigDecimal(discountAmounts[k++])))
							;
					}
					//saleOrderDetail.setDiscountAmount(new BigDecimal(lstSaleProductDiscountAmount.get(i)));
					saleOrderDetail.setDiscountAmount(discountAmount);
					/*
					 * nghiep vu moi: ko luu discount_percent trong
					 * sale_order_detail nua ben duoi co dung truong nay de luu
					 * trong discount_percent nen ben duoi dung xong se set lai
					 * = NULL
					 */
					try {
						saleOrderDetail.setDiscountPercent(Float.valueOf(lstSaleProductDiscountPercent.get(i)));
					} catch (Exception e) {
						// pass through
					}
					discount = discount.add(saleOrderDetail.getDiscountAmount());
				}

				saleOrderDetail.setQuantity(lstQuantity.get(i));
				if (saleOrderDetail.getQuantity() != null) {
					totalSaleQuan += saleOrderDetail.getQuantity();
				}
				saleOrderDetail.setQuantityPackage(lstQuantityPackage.get(i));
				saleOrderDetail.setQuantityRetail(lstQuantityRetail.get(i));

				if (saleOrder != null && saleOrder.getId() != null && saleOrder.getOrderDate() != null) {
					saleOrderDetail.setOrderDate(saleOrder.getOrderDate());
				} else {
					saleOrderDetail.setOrderDate(lockDay);// Kiem tra currentShop da chot ngay hay chua, neu co thi luu ngay don hang la ngay chot					
				}

				saleOrderDetail.setCreateDate(sysDate);
				saleOrderDetail.setCreateUser(currentUser.getUserName());
				if (product != null) {
					//Tinh trong luong tung dong sp ban va tong trong luong don hang
					saleOrderDetail.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(lstQuantity.get(i))));
					totalWeight = totalWeight.add(saleOrderDetail.getTotalWeight());
				}
				//Kiem tra neu sp ban khong co ma CTHTTM tuong ung thi luu isFreeItem la 0
				if (lstCommercialSupport != null && StringUtil.isNullOrEmpty(lstCommercialSupport.get(i))) {
					saleOrderDetail.setIsFreeItem(0);
					//Neu la sp ban thi kiem tra khac Z va co gia bang 0 thi thong bao loi
					/*
					 * yeu cau nghiep vu moi: chi can san pham co gia la co the
					 * ban, ko quan tam san pham thuoc nganh hang Z hay co gia
					 * khac 0 hay ko
					 * 
					 * @modified by tuannd20
					 * 
					 * @date 17/12/2014
					 */
					/*
					 * if (saleOrderDetail.getPrice() == null ||
					 * (saleOrderDetail.getPrice() != null &&
					 * saleOrderDetail.getPrice
					 * ().getPrice().compareTo(BigDecimal.ZERO) <= 0 &&
					 * productMgr.checkProductInZ(product.getProductCode()))) {
					 * result.put("errMsg",
					 * Configuration.getResourceString(ConstantManager
					 * .VI_LANGUAGE,
					 * "saleorder.spkm.err.not.in.z.but.price.null"
					 * ,product.getProductCode())); result.put(ERROR, true);
					 * return JSON; }
					 */

					//tinh tien rieng theo thung/le
					BigDecimal thungAmount = saleOrderDetail.getPackagePrice().multiply(new BigDecimal(saleOrderDetail.getQuantityPackage()));
					BigDecimal leAmount = saleOrderDetail.getPriceValue().multiply(new BigDecimal(saleOrderDetail.getQuantityRetail()));
					amount = thungAmount.add(leAmount);
					saleOrderDetail.setAmount(amount);
					
					//Tinh tien cho tung dong so ban
//					if (saleOrderDetail.getPriceValue() != null) {
//						amount = saleOrderDetail.getPriceValue().multiply(new BigDecimal(lstQuantity.get(i)));
//					} else if (saleOrderDetail.getPrice() != null) {
//						amount = saleOrderDetail.getPrice().getPrice().multiply(new BigDecimal(lstQuantity.get(i)));
//					}
//					saleOrderDetail.setAmount(amount);
					totalAmountMoneyBuy = totalAmountMoneyBuy.add(amount);

					//TODO: Luu ma CTKM vao dong sp ban
					if (lstBuyProductCommercialSupportProgram != null && !StringUtil.isNullOrEmpty(lstBuyProductCommercialSupportProgram.get(i))) {
						//set lstprogramCode la lst cac ZV de luu  promomap
						saleOrderDetail.setLstProgramCode(lstBuyProductCommercialSupportProgram.get(i));
						if (!StringUtil.isNullOrEmpty(lstProductHasPromotion.get(i)) && Integer.valueOf(lstProductHasPromotion.get(i)) > 0) {
							//							saleOrderDetail.setProgramCode(lstBuyProductCommercialSupportProgram.get(i));
							if (!StringUtil.isNullOrEmpty(lstBuyProductCommercialSupportProgramType.get(i))) {
								if ("ZV".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
									/*
									 * yeu cau nghiep vu moi: nganh hang Z ko
									 * can tham gia CTKM van co the tao DH
									 * 
									 * @modified by tuannd20
									 * 
									 * @date 17/12/2014
									 */
									/*
									 * if(saleOrderDetail.getProduct() != null
									 * && saleOrderDetail.getProduct().getCat()
									 * != null &&
									 * "Z".equals(saleOrderDetail.getProduct
									 * ().getCat().getProductInfoCode())) {
									 * result.put("errMsg",
									 * Configuration.getResourceString
									 * (ConstantManager.VI_LANGUAGE,
									 * "sp.create.order.product.z.product.error"
									 * ,
									 * saleOrderDetail.getProduct().getProductCode
									 * () + " - " +
									 * saleOrderDetail.getProduct().
									 * getProductName())); result.put(ERROR,
									 * true); return JSON; }
									 */
									saleOrderDetail.setProgramType(ProgramType.AUTO_PROM);
									//									saleOrderDetail.setJoinProgramCode(lstBuyProductCommercialSupportProgram.get(i));
								} else if ("MANUAL_PROM".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
									saleOrderDetail.setProgramType(ProgramType.MANUAL_PROM);

								} else if ("DESTROY".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
									saleOrderDetail.setProgramType(ProgramType.DESTROY);

								} else if ("PRODUCT_EXCHANGE".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
									saleOrderDetail.setProgramType(ProgramType.PRODUCT_EXCHANGE);

								} else if ("RETURN".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
									saleOrderDetail.setProgramType(ProgramType.RETURN);

								} else if ("DISPLAY_SCORE".equals(lstBuyProductCommercialSupportProgramType.get(i))) {
									saleOrderDetail.setProgramType(ProgramType.DISPLAY_SCORE);
								}
							}
						} else {
							//							saleOrderDetail.setJoinProgramCode(lstBuyProductCommercialSupportProgram.get(i));
						}
					}
					/*
					 * yeu cau nghiep vu moi: nganh hang Z ko can tham gia CTKM
					 * van co the tao DH
					 * 
					 * @modified by tuannd20
					 * 
					 * @date 17/12/2014
					 */
					/*
					 * else if(saleOrderDetail.getProduct() != null &&
					 * saleOrderDetail.getProduct().getCat() != null &&
					 * "Z".equals
					 * (saleOrderDetail.getProduct().getCat().getProductInfoCode
					 * ())) { result.put("errMsg",
					 * Configuration.getResourceString
					 * (ConstantManager.VI_LANGUAGE,
					 * "sp.create.order.product.z.product.error",
					 * saleOrderDetail.getProduct().getProductCode() + " - " +
					 * saleOrderDetail.getProduct().getProductName()));
					 * result.put(ERROR, true); return JSON; }
					 */
					//Xu ly khi co lo
				} else {
					// nguoc lai la 1 va luu loai KM tay la Huy or Doi or Tra
					saleOrderDetail.setIsFreeItem(1);
					saleOrderDetail.setAmount(BigDecimal.ZERO);
					saleOrderDetail.setProgramCode(lstCommercialSupport.get(i));
					//					saleOrderDetail.setJoinProgramCode(lstCommercialSupport.get(i));

					Integer remainPortion = mapRemainPortion.get(lstCommercialSupport.get(i).trim());
					if (remainPortion == null) {
						program = promotionProgramMgr.getPromotionProgramByCode(lstCommercialSupport.get(i).trim());
						if (program != null) {
							remainPortion = promotionProgramMgr.getQuantityRemainOfPromotionProgram(program.getId(), currentShop.getId(), salerStaff.getId(), customer.getId(), lockDay);
							if (remainPortion != null) {
								mapRemainPortion.put(lstCommercialSupport.get(i).trim(), remainPortion - 1);
							}
						}
					} else {
						mapRemainPortion.put(lstCommercialSupport.get(i).trim(), remainPortion - 1);
					}

					//Neu la sp KM tay thi chi check gia bang null thi thong bao loi
//					if (saleOrderDetail.getPrice() == null) {
//						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.spkm.err.not.in.z.but.price.null", product.getProductCode()));
//						result.put(ERROR, true);
//						return JSON;
//					}

					commercialType = lstCommercialSupportType.get(i);
					saleOrderDetail.setProgrameTypeCode(commercialType);
					//Kiem tra thong tin chuong trinh HTTM 
					if (commercials != null) {
						for (CommercialSupportVO cs : commercials.getLstObject()) {
							if (StringUtil.isNullOrEmpty(cs.getPromotionType())) {
								continue;
							}
							if (lstCommercialSupport.get(i).equals(cs.getCode()) && cs.getPromotionType().equals(commercialType)) {
								// HTTM: HUY HANG va HANG co quan ly theo lo. Thuc hien tach lo cho viec huy hang 
								if (!cs.getPromotionType().startsWith("ZH")) {
									continue;
								}
								//Xu ly khi co lo

								//
								break;
							}
						}
					}
				}

				// TODO: SALE_ORDER_LOT
				//Luu danh sach sale_order_lot truong hop tach dong sp theo Kho
				if (!StringUtil.isNullOrEmpty(lstWarehouseId.get(i))) {
					String[] warehouseTmp = lstWarehouseId.get(i).split(",");
					String[] warehouseQuantityTmp = lstWarehouseQuantity.get(i).split(",");
					String[] warehouseQuantityPackageTmp = lstWarehouseQuantityPackage.get(i).split(",");
					String[] warehouseQuantityRetailTmp = lstWarehouseQuantityRetail.get(i).split(",");
					String[] discountAmounts = lstSaleProductDiscountAmount != null && !StringUtil.isNullOrEmpty(lstSaleProductDiscountAmount.get(i)) ? lstSaleProductDiscountAmount.get(i).split(",") : null;
					for (int j = 0; j < warehouseTmp.length; ++j) {
						warehouse = stockManagerMgr.getWarehouseById(Long.valueOf(warehouseTmp[j]));
						if (warehouse == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "baocao.kho"));
							return JSON;
						}
						if (!ActiveType.RUNNING.equals(warehouse.getStatus())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "baocao.kho"));
							return JSON;
						}
						saleOrderLot = new SaleOrderLot();
						saleOrderLot.setProduct(product);
						saleOrderLot.setWarehouse(warehouse);
						saleOrderLot.setShop(currentShop);
						saleOrderLot.setStaff(salerStaff);
						saleOrderLot.setSaleOrder(saleOrder);
						saleOrderLot.setSaleOrderDetail(saleOrderDetail);
						saleOrderLot.setPrice(saleOrderDetail.getPrice());
						saleOrderLot.setPriceValue(saleOrderDetail.getPriceValue());
						saleOrderLot.setPackagePrice(saleOrderDetail.getPackagePrice());
						saleOrderLot.setQuantityRetail(saleOrderDetail.getQuantityRetail());
						saleOrderLot.setQuantityPackage(saleOrderDetail.getQuantityPackage());
						Integer quantityTmp = Integer.valueOf(warehouseQuantityTmp[j]);
						Integer quantityPackageTmp = Integer.valueOf(warehouseQuantityPackageTmp[j]);
						Integer quantityRetailTmp = Integer.valueOf(warehouseQuantityRetailTmp[j]);
						//Kiem tra so luong nhap  
						if (quantityTmp < 0 || quantityRetailTmp < 0 || quantityPackageTmp < 0) {
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
							result.put(ERROR, true);
							return JSON;
						}
						saleOrderLot.setQuantity(quantityTmp);
						saleOrderLot.setQuantityPackage(quantityPackageTmp);
						saleOrderLot.setQuantityRetail(quantityRetailTmp);
						StockTotal st = stockMgr.getStockTotalByProductAndOwner(product.getId(), StockObjectType.SHOP, saleOrder.getShop().getId(), warehouse.getId());
						if (st == null && !(quantityTmp == 0 && Long.valueOf(warehouseTmp[j]).equals(0L))) {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.received.vansale.stock.total.of.shop", product.getProductCode(), saleOrder.getShop().getShopCode()));
							result.put(ERROR, true);
							return JSON;
						}
						saleOrderLot.setStockTotal(st);
						if (saleOrder != null && saleOrder.getId() != null && saleOrder.getOrderDate() != null) {
							saleOrderLot.setOrderDate(saleOrder.getOrderDate());
						} else {
							saleOrderLot.setOrderDate(lockDay);// Kiem tra currentShop da chot ngay hay chua, neu co thi luu ngay don hang la ngay chot
						}
						saleOrderLot.setCreateDate(sysDate);
						saleOrderLot.setCreateUser(currentUser.getUserName());

						if (discountAmounts != null && j < discountAmounts.length && discountAmounts[j] != null) {
							BigDecimal discountAmount = new BigDecimal(discountAmounts[j]);
							saleOrderLot.setDiscountAmount(discountAmount);
						}
						lstSaleOrderLot.add(saleOrderLot);
					}
					// 1
					if (lstSaleOrderLot.size() > 0) {
						productVO.setLstSaleOrderLot(lstSaleOrderLot);
					}
				}
				saleOrderDetail.setDiscountPercent(null);
				productVO.setSaleOrderDetail(saleOrderDetail);

				//set danh sach CTKM dat cua sp ban
				if (productVO.getSaleOrderDetail().getIsFreeItem().equals(0) && lstProgramDatSaleProduct != null) {
					for (SaleOrderPromotionDetailVO vo : lstProgramDatSaleProduct) {
						if (productVO.getSaleOrderDetail().getProduct().getId().equals(vo.getProductId())) {
							if (productVO.getLstProgramDat() == null) {
								productVO.setLstProgramDat(vo.getProgramCode());
							} else {
								productVO.setLstProgramDat(productVO.getLstProgramDat() + "," + vo.getProgramCode());
							}
						}
					}
				}

				lstSaleProductVO.add(productVO);
				indexOfSaleProduct.put(product.getProductCode(), lstSaleProductVO.indexOf(productVO));
			}// ket thuc xu ly truong hop san p-ham ban.

			// kiem tra so suat
			if (quantityReceivedList != null && quantityReceivedList.size() > 0) {
				for (SaleOrderPromotionVO vo : quantityReceivedList) {
					Integer remainPortion = mapRemainPortion.get(vo.getPromotionCode().trim());
					if (remainPortion == null) {
						remainPortion = promotionProgramMgr.getQuantityRemainOfPromotionProgram(vo.getPromotionId(), currentShop.getId(), salerStaff.getId(), customer.getId(), lockDay);
						if (remainPortion != null) {
							mapRemainPortion.put(vo.getPromotionCode().trim(), remainPortion - vo.getQuantityReceived());
						}
					} else {
						mapRemainPortion.put(vo.getPromotionCode().trim(), remainPortion - vo.getQuantityReceived());
					}
				}
			}
			if (mapRemainPortion.size() > 0) {
				String pCodes = "";
				for (String pCode : mapRemainPortion.keySet()) {
					Integer remainPortion = mapRemainPortion.get(pCode);
					if (remainPortion != null && remainPortion < 0) {
						pCodes = pCodes + ", " + pCode;
					}
				}
				if (!StringUtil.isNullOrEmpty(pCodes)) {
					result.put("errMsg", R.getResource("sp.create.order.promotion.not.enough.portion", pCodes.replaceFirst(", ", "")));
					//result.put("isRePayment", true);
					result.put(ERROR, true);
					return JSON;
				}
			}
			// kiem tra max so tien KM
			if (lstSOPromoDetail != null) {
				Map<String, BigDecimal> mapDiscountAmountPromotion = new HashMap<String, BigDecimal>();
				for (int i = 0, n = lstSOPromoDetail.size(); i < n; i++) {
					SaleOrderPromotionDetailVO saleOrderPromotionDetailVO = lstSOPromoDetail.get(i);
					BigDecimal discountAmount = mapDiscountAmountPromotion.get(saleOrderPromotionDetailVO.getProgramCode());
					if (discountAmount != null) {
						mapDiscountAmountPromotion.put(saleOrderPromotionDetailVO.getProgramCode(), discountAmount.add(saleOrderPromotionDetailVO.getDiscountAmount() == null ? BigDecimal.ZERO : saleOrderPromotionDetailVO.getDiscountAmount()));
					} else {
						mapDiscountAmountPromotion.put(saleOrderPromotionDetailVO.getProgramCode(), saleOrderPromotionDetailVO.getDiscountAmount() == null ? BigDecimal.ZERO : saleOrderPromotionDetailVO.getDiscountAmount());
					}
				}
				if (mapDiscountAmountPromotion.size() > 0) {
					String programCode = "";
					for (Map.Entry<String, BigDecimal> entry : mapDiscountAmountPromotion.entrySet()) {
						BigDecimal remain = promotionProgramMgr.getAmountRemainOfPromotionProgram(entry.getKey(), currentShop.getId(), salerStaff.getId(), customer.getId(), lockDay);
						if (remain != null && remain.subtract(entry.getValue()).compareTo(BigDecimal.ZERO) < 0) {
							programCode += ", " + entry.getKey() + "(" + convertMoney(remain.setScale(2, BigDecimal.ROUND_HALF_UP)) + ")";
						}
					}
					if (!StringUtil.isNullOrEmpty(programCode)) {
						result.put("errMsg", R.getResource("sp.create.order.promotion.not.enough.amount", programCode.replaceFirst(", ", "")));
						result.put(ERROR, true);
						return JSON;
					}
				}
			}

			Integer levelPromo = 0;
			// TODO: Danh sach san pham khuyen mai tu dong 
			SaleOrderFilter<Price> filter = new SaleOrderFilter<Price>();
			if (lstPromotionCode != null) {
				for (int i = 0, szi = lstPromotionCode.size(); i < szi; ++i) {
					program = promotionProgramMgr.getPromotionProgramByCode(lstPromotionCode.get(i));
					//PromotionShopMap promotionShopMap = promotionProgramMgr.checkPromotionShopMap(program.getId(), currentShop.getShopCode(),ActiveType.RUNNING,lockDay);

					//Kiem tra so xuat phan bo cho nha phan phoi
					/*
					 * if(promotionShopMap == null){ result.put("errMsg",
					 * ValidateUtil
					 * .getErrorMsg(ConstantManager.ERR_SP_PROMOTION_SHOP
					 * ,program.getPromotionProgramCode())); result.put(ERROR,
					 * true);result.put("flag", true); return JSON; }
					 */
					/*
					 * if(promotionShopMap.getQuantityReceived() != null &&
					 * promotionShopMap.getQuantityMax() != null){
					 * if(promotionShopMap
					 * .getQuantityMax()<=promotionShopMap.getQuantityReceived
					 * ()){ result.put("errMsg",
					 * ValidateUtil.getErrorMsg(ConstantManager
					 * .ERR_SP_PROMOTION_SHOP_FULL
					 * ,program.getPromotionProgramCode()));
					 * result.put("isRePayment", true);result.put(ERROR, true);
					 * return JSON; } }
					 */
					promoProductVO = new SaleProductVO();
					saleOrderDetail = new SaleOrderDetail();
					saleOrderDetail.setStaff(salerStaff);
					saleOrderDetail.setShop(currentShop);
					if (saleOrder != null && saleOrder.getId() != null && saleOrder.getOrderDate() != null) {
						saleOrderDetail.setOrderDate(saleOrder.getOrderDate());
					} else {
						saleOrderDetail.setOrderDate(lockDay);
					}
					saleOrderDetail.setCreateDate(sysDate);
					saleOrderDetail.setCreateUser(currentUser.getUserName());
					saleOrderDetail.setIsFreeItem(1);
					saleOrderDetail.setProgramType(ProgramType.AUTO_PROM);
					saleOrderDetail.setProgramCode(program.getPromotionProgramCode());
					//					saleOrderDetail.setJoinProgramCode(program.getPromotionProgramCode());
					saleOrderDetail.setProgrameTypeCode(program.getType());
					/*
					 * set promotion_order_number
					 */
					try {
						if (lstPromoLevel != null && lstPromoLevel.size() > i) {
							String promoLevels = lstPromoLevel.get(i);
							if (!StringUtil.isNullOrEmpty(promoLevels)) {
								String[] aPromoLevel = promoLevels.split(",");
								if (aPromoLevel != null && aPromoLevel.length > 0) {
									saleOrderDetail.setPromotionOrderNumber(Integer.parseInt(aPromoLevel[0]));
								}
							}
						}
						//Luu nhom va level cua cac dong SPKM phuc vu cho chinh sua so suat KM
						if (lstProductGroupAndLevelGroupId != null && lstProductGroupAndLevelGroupId.size() > i) {
							String productGroupAndLevel = lstProductGroupAndLevelGroupId.get(i);
							if (!StringUtil.isNullOrEmpty(productGroupAndLevel)) {
								String[] groupOrLevel = productGroupAndLevel.split("_");
								if (groupOrLevel != null && groupOrLevel.length > 0) {
									Long groupId = Long.parseLong(groupOrLevel[0]);
									Long levelId = Long.parseLong(groupOrLevel[1]);
									if (groupId > 0 && levelId > 0) {
										//saleOrderDetail.setProductGroup(commonMgr.getEntityById(ProductGroup.class,groupId));
										saleOrderDetail.setProductGroupId(groupId);
										//saleOrderDetail.setGroupLevel(commonMgr.getEntityById(GroupLevel.class,levelId));
										saleOrderDetail.setGroupLevelId(levelId);
									}

								}
							}
						}
					} catch (Exception e) {
						// pass through
					}

					//KM hang
					if (lstPromotionType.get(i) == 1) {
						if (!StringUtil.isNullOrEmpty(lstPromotionProduct.get(i))) {
							product = productMgr.getProductByCode(lstPromotionProduct.get(i));
							saleOrderDetail.setProduct(product);
							saleOrderDetail.setConvfact(product.getConvfact());
							saleOrderDetail.setProductInfo(product.getCat());
							//							saleOrderDetail.setPrice(productMgr.getProductPriceForCustomer(product.getId(), customer));
							filter = new SaleOrderFilter<Price>();
							filter.setProductId(product.getId());
							filter.setShopId(currentShop.getId());
							filter.setCustomerId(customer.getId());
							filter.setOrderDate(lockDay);
							List<Price> lstPr = productMgr.getPriceByRpt(filter);
							if (lstPr != null && lstPr.size() > 0) {
								saleOrderDetail.setPrice(lstPr.get(0));
								saleOrderDetail.setPriceValue(saleOrderDetail.getPrice().getPrice());
								saleOrderDetail.setPriceNotVat(saleOrderDetail.getPrice().getPriceNotVat());
								saleOrderDetail.setPackagePrice(saleOrderDetail.getPrice().getPackagePrice());
								saleOrderDetail.setPackagePriceNotVAT(saleOrderDetail.getPrice().getPackagePriceNotVat());
								saleOrderDetail.setVat(saleOrderDetail.getPrice().getVat());
							}

							String __promoProductQuatity = lstPromotionProductQuatity.get(i);
							String __promoProductQuatityPackage = lstPromotionProductQuatityPackage.get(i);
							String __promoProductQuatityRetail = lstPromotionProductQuatityRetail.get(i);
							Integer promoProductQuatity = Integer.parseInt(__promoProductQuatity);
							Integer promoProductQuatityPackage = Integer.parseInt(__promoProductQuatityPackage);
							Integer promoProductQuatityRetail = Integer.parseInt(__promoProductQuatityRetail);

							String __promoProductMaxQuatity = lstPromotionProductMaxQuatity.get(i);
							//							String __promoProductMaxQuatityCk = lstPromotionProductMaxQuatityCk.get(i);
							//							String __myTime = lstMyTime.get(i);
							Integer promoProductMaxQuatity = Integer.parseInt(__promoProductMaxQuatity);
							//							Integer promoProductMaxQuatityCk = Integer.parseInt(__promoProductMaxQuatityCk);
							//							BigDecimal myTime = new BigDecimal(__myTime);
							saleOrderDetail.setMaxQuantityFree(promoProductMaxQuatity);
							String __promoProductMaxQuatityTotal = lstPromotionProductMaxQuatity.get(i);
							Integer promoProductMaxQuatityTotal = Integer.parseInt(__promoProductMaxQuatityTotal);
							saleOrderDetail.setMaxQuantityFreeTotal(promoProductMaxQuatityTotal);
							//							saleOrderDetail.setMyTime(myTime);
							if (promoProductQuatity < 0) {
								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
								result.put(ERROR, true);
								return JSON;
							}

							//Luu khoi luong tung dong SP ban va tinh tong Khoi luong cua don hang
							saleOrderDetail.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(promoProductQuatity)));
							totalWeight = totalWeight.add(saleOrderDetail.getTotalWeight());

							saleOrderDetail.setQuantity(promoProductQuatity);
							saleOrderDetail.setQuantityPackage(promoProductQuatityPackage);
							saleOrderDetail.setQuantityRetail(promoProductQuatityRetail);
							saleOrderDetail.setMaxQuantityFree(promoProductMaxQuatity);
							//							saleOrderDetail.setMaxQuantityFreeCk(promoProductMaxQuatityCk);

							//Luu danh sach sale_order_lot truong hop tach dong sp theo Kho
							if (!StringUtil.isNullOrEmpty(lstPromoWarehouseId.get(i))) {
								lstPromoSaleOrderLot = new ArrayList<SaleOrderLot>();
								String[] warehouseTmp = lstPromoWarehouseId.get(i).split(",");
								String[] warehouseQuantityTmp = lstPromoWarehouseQuantity.get(i).split(",");
								for (int j = 0; j < warehouseTmp.length; ++j) {
									if (Long.valueOf(warehouseTmp[j]).equals(0L)) {
										warehouse = stockManagerMgr.getMostPriorityWareHouse(currentShop.getId(), null);
									} else {
										warehouse = stockManagerMgr.getWarehouseById(Long.valueOf(warehouseTmp[j]));
									}
									if (warehouse == null) {
										result.put(ERROR, true);
										result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "baocao.kho"));
										return JSON;
									}
									if (!ActiveType.RUNNING.equals(warehouse.getStatus())) {
										result.put(ERROR, true);
										result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "baocao.kho"));
										return JSON;
									}
									saleOrderLot = new SaleOrderLot();
									saleOrderLot.setProduct(product);
									saleOrderLot.setWarehouse(warehouse);
									saleOrderLot.setShop(currentShop);
									saleOrderLot.setStaff(salerStaff);
									saleOrderLot.setSaleOrder(saleOrder);
									saleOrderLot.setSaleOrderDetail(saleOrderDetail);
									saleOrderLot.setPrice(saleOrderDetail.getPrice());
									saleOrderLot.setPriceValue(saleOrderDetail.getPriceValue());
									saleOrderLot.setPackagePrice(saleOrderDetail.getPackagePrice());
									Integer quantityTmp = Integer.valueOf(warehouseQuantityTmp[j]);
									//Kiem tra so luong nhap  
									if (quantityTmp < 0) {
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FORMAT_INVALID, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.import.header.format.invalid.quantity")));
										result.put(ERROR, true);
										return JSON;
									}
									// kiem tra trong moi Kho co con so luong sp khong va thuc hien tru ton kho
									StockTotal st = stockMgr.getStockTotalByProductAndOwner(product.getId(), StockObjectType.SHOP, saleOrder.getShop().getId(), warehouse.getId());
									if (st == null && !(quantityTmp == 0 && Long.valueOf(warehouseTmp[j]).equals(0L))) {
										result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.received.vansale.stock.total.of.shop", product.getProductCode(), saleOrder.getShop().getShopCode()));
										result.put(ERROR, true);
										return JSON;
									}
									if (!(quantityTmp == 0 && Long.valueOf(warehouseTmp[j]).equals(0L)) && (warehouse.getShop() == null || !warehouse.getShop().getId().equals(st.getObjectId()))) {
										result.put(ERROR, true);
										result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_SS_NOT_BELONG_GENERAL, false, "baocao.kho", "import.cttb.header.npp"));
										return JSON;
									}
									/*
									 * if (st.getAvailableQuantity() <
									 * quantityTmp) { result.put("errMsg",
									 * Configuration
									 * .getResourceString(ConstantManager
									 * .VI_LANGUAGE,
									 * "saleorder.spkm.err.not.enough.available.quantity"
									 * ,product.getProductCode()));
									 * result.put(ERROR, true); return JSON; }
									 */
									saleOrderLot.setStockTotal(st);
									saleOrderLot.setQuantity(quantityTmp);
									if (saleOrderLot.getProduct().getConvfact() != null && !saleOrderLot.getProduct().getConvfact().equals(0)) {
										saleOrderLot.setQuantityPackage(quantityTmp / saleOrderLot.getProduct().getConvfact());
										saleOrderLot.setQuantityRetail(quantityTmp % saleOrderLot.getProduct().getConvfact());
									}
									if (saleOrder != null && saleOrder.getId() != null && saleOrder.getOrderDate() != null) {
										saleOrderLot.setOrderDate(saleOrder.getOrderDate());
									} else {
										saleOrderLot.setOrderDate(lockDay);// Kiem tra currentShop da chot ngay hay chua, neu co thi luu ngay don hang la ngay chot
									}
									saleOrderLot.setCreateDate(sysDate);
									saleOrderLot.setCreateUser(currentUser.getUserName());
									lstPromoSaleOrderLot.add(saleOrderLot);
								}
								// 2
								if (lstPromoSaleOrderLot.size() > 0) {
									promoProductVO.setLstSaleOrderLot(lstPromoSaleOrderLot);
								}
							}
						}
						//Xu ly khi co lo SP KM

					}
					//KM tien
					if (lstPromotionType.get(i) == SaleOrderDetailVO.FREE_PRICE) {
						BigDecimal discountAmt = new BigDecimal(lstDiscount.get(i));
						saleOrderDetail.setDiscountPercent(null);
						saleOrderDetail.setDiscountAmount(discountAmt);
						//						saleOrderDetail.setMaxAmountFree(new BigDecimal(lstPromotionProductMaxQuatity.get(i)));
					}
					//KM % tien 
					if (lstPromotionType.get(i) == SaleOrderDetailVO.FREE_PERCENT) {
						BigDecimal discountAmt = new BigDecimal(lstDiscount.get(i));
						saleOrderDetail.setDiscountPercent(Float.valueOf(lstDisper.get(i)));
						saleOrderDetail.setDiscountAmount(discountAmt);
					}
					//					discount = discount.add(saleOrderDetail.getDiscountAmount());
					//Kiem tra neu nhap so luong KM = 0 thi khong luu xuong DB
					//KM Hang
					if (lstPromotionType.get(i) == 1) {
						promoProductVO.setSaleOrderDetail(saleOrderDetail);
						lstPromoProductVO.add(promoProductVO);
					} else {
						if (lstPromoLevel != null && lstPromoLevel.size() > i) {
							String promoLevels = lstPromoLevel.get(i);
							if (!StringUtil.isNullOrEmpty(promoLevels)) {
								String[] aPromoLevel = promoLevels.split(",");
								if (aPromoLevel != null && aPromoLevel.length > 0) {
									levelPromo = Integer.valueOf(lstPromoLevel.get(i));
								}
							}
						}
					}
				}// ket thuc truong hop xu ly khuyen mai
			}

			if (levelPromo > 0) {
				for (SaleProductVO s : lstSaleProductVO) {
					s.getSaleOrderDetail().setPromotionOrderNumber(levelPromo);

				}
			}
			
			// kiem tra max so luong KM 
			Map<String, Integer> mapNumProductPromotion = new HashMap<String, Integer>();
			for (int i = 0, n = lstPromoProductVO.size(); i < n; i++) {
				SaleProductVO saleProductVO = lstPromoProductVO.get(i);
				if (saleProductVO != null && saleProductVO.getSaleOrderDetail() != null) {
					SaleOrderDetail orderDetail = saleProductVO.getSaleOrderDetail();
					Integer quantity = mapNumProductPromotion.get(orderDetail.getProgramCode());
					if (quantity != null) {
						mapNumProductPromotion.put(orderDetail.getProgramCode(), quantity + (orderDetail.getQuantity() == null ? 0 : orderDetail.getQuantity()));
					} else {
						mapNumProductPromotion.put(orderDetail.getProgramCode(), orderDetail.getQuantity() == null ? 0 : orderDetail.getQuantity());
					}
				}
			}
			if (mapNumProductPromotion.size() > 0) {
				String programCode = "";
				for (Map.Entry<String, Integer> entry : mapNumProductPromotion.entrySet()) {
					Integer remain = promotionProgramMgr.getNumRemainOfPromotionProgram(entry.getKey(), currentShop.getId(), salerStaff.getId(), customer.getId(), lockDay);
					if (remain != null && remain - entry.getValue() < 0) {
						programCode += ", " + entry.getKey() + "(" + remain + ")";
					}
				}
				if (!StringUtil.isNullOrEmpty(programCode)) {
					result.put("errMsg", R.getResource("sp.create.order.promotion.not.enough.num", programCode.replaceFirst(", ", "")));
					result.put(ERROR, true);
					return JSON;
				}
			}
			
			//Luu tong tien don hang , tong CKMH va tong KL don hang
			saleOrder.setAmount(totalAmountMoneyBuy);
			saleOrder.setDiscount(discount);
			saleOrder.setTotal(totalAmountMoneyBuy.subtract(discount));
			saleOrder.setTotalWeight(totalWeight);
			saleOrder.setQuantity(totalSaleQuan);
			saleOrder.setAccountDate(saleOrder.getOrderDate());
			saleOrder.setShopCode(saleOrder.getShop().getShopCode());

			SaleOrder newSO = saleOrderMgr.createSaleOrder(saleOrder, lstSaleProductVO, lstPromoProductVO, quantityReceivedList, lstSOPromoDetail, lstCTTL, isSaveNotCheck, lstKeyShop, lstPromoLot, getLogInfoVO());
			result.put("saleOrder", newSO);
			result.put(ERROR, false);
			result.put("flag", true);

			/*
			 * Luu CTKM tich luy
			 */
			//			this.createAccumutive(newSO);
		} catch (BusinessException be) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(be, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.createSaleOrder"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			if (be.getMessage().equals("LOT_QUANTITY_IS_NOT_ENOUGH")) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.approved.quantity.not.enough"));
			} else if (be.getMessage().equals("RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY")) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.adjust.order.received.larger.than.max.quantity"));
			} else if (be.getMessage().equals("TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT")) {
				result.put("errMsg", be.getMessage());// Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.total.larger.max.debit"));
			} else if (be.getMessage().equals("MAX_DAY_DEBIT")) {
				result.put("errMsg", be.getMessage());//Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.max.debit.day"));
			} else if (be.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20005) > -1 || be.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20004) > -1 || be.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20003) > -1
					|| be.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20002) > -1 || be.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20001) > -1) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.saved.promotion.quantity.not.enough"));
			} else {
				if (be.getErrorCodes() != null && be.getErrorCodes().size() != 0) {
					for (String errorCode : be.getErrorCodes()) {
						if (errorCode.contains("_")) {
							String[] errs = errorCode.split("_");
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, errs[0], errs.length > 1 ? errs[1] : ""));
						} else {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, errorCode));
						}
					}
				} else {
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				}
			}
		} catch (Exception ex) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			if (ex.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20005) > -1 || ex.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20004) > -1 || ex.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20003) > -1
					|| ex.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20002) > -1 || ex.getMessage().indexOf(ExceptionCode.ERR_PROMOTION_20001) > -1) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.search.sale.order.saved.promotion.quantity.not.enough"));
			}
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.createSaleOrder"), createLogErrorStandard(actionStartTime));
			return JSON;
		}
		return JSON;
	}

	/**
	 * get promotive product to change when editing sale order
	 * 
	 * @author tuannd20
	 * @param orderId
	 * @param productCode
	 * @param programCode
	 * @param promoLevel
	 * @return
	 * @date 10/10/2014
	 */
	public String changePromotionProduct() {
		try {
			if (!StringUtil.isNullOrEmpty(programCode)) {
				PromotionProgram pp = promotionProgramMgr.getPromotionProgramByCode(programCode.trim());
				if (pp != null) {
					if (PromotionType.ZV24.getValue().equalsIgnoreCase(pp.getType())) {
						result.put("isZV24", true);
					} else if (PromotionType.ZV21.getValue().equalsIgnoreCase(pp.getType())) {
						result.put("isZV21", true);
					}
				}
			}
			List<SaleOrderDetailVO> lst = promotionProgramMgr.getChangePromotionProductForEditingSaleOrder2(currentShop.getId(), orderId, productCode, programCode, groupLevelId, promoLevel);
			if (lst != null && lst.size() > 0) {
				/*
				 * neu nhu so luong KM cua tat ca cac san pham deu bang nhau,
				 * thi duoc phep chon nhieu cresan pham khi doi SPKM. (Voi dieu
				 * kien tong so luong cua cac san pham da chon phai <= so luong
				 * KM toi da duoc nhan)
				 */
				int i = 1;
				for (i = 1; i < lst.size() && lst.get(i - 1).getSaleOrderDetail().getMaxQuantityFree() == lst.get(i).getSaleOrderDetail().getMaxQuantityFree(); i++)
					;
				if (i >= lst.size()) {
					result.put("canChaneMultiProduct", true);
				} else {
					result.put("canChaneMultiProduct", false);
				}
			}

			result.put("changePPromotion", lst);
		} catch (BusinessException ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.changePromotionProduct"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Import excel don hang tra thuong trung bay
	 * 
	 * @author nhanlt6
	 * @return the string
	 */
	public String importOrderForDisplay() {
		resetToken(result);
		if (currentShop == null || currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null");
			return ERROR;
		}
		isError = true;
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFileDisplay, excelFileDisplayContentType, errMsg, 5);
		Map<String, Product> mapProduct = new HashMap<String, Product>();
		Map<String, Customer> mapCustomer = new HashMap<String, Customer>();
		Map<String, Staff> mapStaff = new HashMap<String, Staff>();
		Map<String, ObjectVO<CommercialSupportVO>> mapCommercialSupport = new HashMap<String, ObjectVO<CommercialSupportVO>>();
		if (StringUtil.isNullOrEmpty(errMsg) && (lstData == null || lstData.size() == 0)) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.display.program.nodata");
			return ERROR;
		}
		if (lstData.size() > 5000) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.display.program.max.row");
			return ERROR;
		}
		typeView = false;
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				SaleOrder so = null;
				List<Product> lstProduct = null;
				List<Integer> lstQuantity = null;
				List<String> lstCommercialSupport = null;
				List<String> lstCommercialSupportType = null;
				Staff excelStaff = null;
				Customer excelCustomer = null;
				Product product = null;
				String value = null;
				Integer convfact = null;
				Shop currentShop = staffMgr.getStaffByCode(currentUser.getUserName()).getShop();
				message = "";
				String msg = null;
				for (int i = 0; i < lstData.size(); i++) {
					totalItem++;
					List<String> row = lstData.get(i);
					//Ma KH 
					if (row.size() > 0) {
						value = row.get(0);
						if (!StringUtil.isNullOrEmpty(value)) {
							so = new SaleOrder();
							lstProduct = new ArrayList<Product>();
							lstQuantity = new ArrayList<Integer>();
							lstCommercialSupport = new ArrayList<String>();
							lstCommercialSupportType = new ArrayList<String>();
							//Luu du lieu de xuat khi co loi~
							excelCustomer = mapCustomer.get(value);
							if (excelCustomer == null) {
								excelCustomer = customerMgr.getCustomerByCodeEx(StringUtil.getFullCode(currentShop.getShopCode(), value), ActiveType.RUNNING);
								if (excelCustomer != null) {
									if (ActiveType.RUNNING.equals(excelCustomer.getStatus())) {
										if (excelCustomer.getShop().getId().equals(currentShop.getId())) {
											mapCustomer.put(value, excelCustomer);
											if (so != null) {
												so.setCustomer(excelCustomer);
												if (excelCustomer.getDeliver() != null) {
													so.setDelivery(excelCustomer.getDeliver());
												}
												if (excelCustomer.getCashierStaff() != null) {
													so.setCashier(excelCustomer.getCashierStaff());
												}
											}
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.currentShop.exist1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.code.import",
													value));
											message += "\n";
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.in.active", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.code.import",
												value));
										message += "\n";
									}
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.code.import", value));
									message += "\n";
								}
							} else {
								if (so != null) {
									so.setCustomer(excelCustomer);
									if (excelCustomer.getDeliver() != null) {
										so.setDelivery(excelCustomer.getDeliver());
									}
									if (excelCustomer.getCashierStaff() != null) {
										so.setCashier(excelCustomer.getCashierStaff());
									}
								}
							}
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.sale.staff.code.import", value));
							message += "\n";
						}
					}
					//Ma NVBH 
					if (row.size() > 1) {
						value = row.get(1);
						if (!StringUtil.isNullOrEmpty(value)) {
							excelStaff = mapStaff.get(value);
							if (excelStaff == null) {
								excelStaff = staffMgr.getStaffByCode(value);
								if (excelStaff != null) {
									if (StaffObjectType.NVBH.getValue().equals(excelStaff.getStaffType().getSpecificType()) || StaffObjectType.NVVS.getValue().equals(excelStaff.getStaffType().getSpecificType())) {
										if (ActiveType.RUNNING.equals(excelStaff.getStatus())) {
											if (excelStaff.getShop().getId().equals(currentShop.getId())) {
												mapStaff.put(value, excelStaff);
												so.setStaff(excelStaff);
												so.setShop(excelStaff.getShop());
											} else {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.currentShop.exist1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
														"sp.create.order.sale.staff.code.import", value));
												message += "\n";
											}
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.in.active", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
													"sp.create.order.sale.staff.code.import", value));
											message += "\n";
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.not.nvbh", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.sale.staff.code.import", value));
										message += "\n";
									}
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.sale.staff.code.import", value));
									message += "\n";
								}
							} else {
								so.setStaff(excelStaff);
								so.setShop(excelStaff.getShop());
							}
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.code"));
							message += "\n";
						}
					}

					//Ma SP
					if (row.size() > 2) {
						value = row.get(2);
						if (!StringUtil.isNullOrEmpty(value)) {
							product = mapProduct.get(value);
							if (product == null) {
								product = productMgr.getProductByCode(value);
								if (product != null) {
									if (ActiveType.RUNNING.equals(product.getStatus())) {
										mapProduct.put(value, product);
										convfact = product.getConvfact();
										if (StringUtil.isNullOrEmpty(message)) {
											Price price = productMgr.getProductPriceForCustomer(product.getId(), excelCustomer);
											if (price != null) {
												if (lstProduct == null) {
													lstProduct = new ArrayList<Product>();
												}
												lstProduct.add(product);
											} else {
												lstProduct.add(product);
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.dont.have.price.import", value);
												message += "\n";
											}
										} else {
											if (lstProduct == null) {
												lstProduct = new ArrayList<Product>();
											}
											lstProduct.add(product);
										}
									} else {
										lstProduct.add(product);
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.in.active", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.code.import",
												value));
										message += "\n";
									}
								} else {
									product = new Product();
									lstProduct.add(product);
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.code.import", value));
									message += "\n";
								}
							} else {
								lstProduct.add(product);
							}
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.code.import", value));
							message += "\n";
						}
					}
					if (row.size() > 3) {
						value = row.get(3);
						String productCode = row.get(2);
						if (!StringUtil.isNullOrEmpty(value)) {
							//So luong mua
							if (ValidateUtil.validateFormatNumberConvfact(value)) {
								msg = ValidateUtil.validateField(value, "stock.update.import.header.format.invalid.quantity", 8, ConstantManager.ERR_MAX_LENGTH);
								if (StringUtil.isNullOrEmpty(msg)) {
									if (convfact == null) {
										convfact = 1;
									}
									Integer tmp = getQuantity(value, convfact);
									if (tmp != null) {
										if (tmp > 0) {
											if (!StringUtil.isNullOrEmpty(productCode)) {
												Product p = productMgr.getProductByCode(productCode);
												if (p != null) {
													StockTotalFilter stockTotalFilter = new StockTotalFilter();
													stockTotalFilter.setProductId(product.getId());
													stockTotalFilter.setOwnerType(StockObjectType.SHOP);
													stockTotalFilter.setOwnerId(currentShop.getId());
													List<StockTotal> lst = stockMgr.getListStockTotalByProductAndOwner(stockTotalFilter);
													if (lst != null && lst.size() > 0) {
														Integer totalQuantity = 0;
														for (StockTotal stt : lst) {
															totalQuantity += stt.getAvailableQuantity();
														}
														if (totalQuantity >= tmp.intValue()) {
															if (lstQuantity == null) {
																lstQuantity = new ArrayList<Integer>();
															}
															lstQuantity.add(tmp.intValue());
														} else {
															message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.available.quantity.is.not.enough.for.all.lot", productCode, totalQuantity);
															message += "\n";
														}
													} else {
														message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.not.have.ware.house.stock.total", productCode);
														message += "\n";
													}
												} /*
												 * else { message +=
												 * Configuration
												 * .getResourceString
												 * (ConstantManager.VI_LANGUAGE,
												 * "common.not.exist"
												 * ,Configuration
												 * .getResourceString
												 * (ConstantManager.VI_LANGUAGE,
												 * "sp.create.order.product.code.import"
												 * ,value)); message += "\n"; }
												 */
											} else {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.product.code.import", value));
												message += "\n";
											}
										} else {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.convfact.value.not.equal.zero");
											message += "\n";
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.convfact.value.not.valid");
										message += "\n";
									}
								} else {
									message += msg;
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.convfact.value.not.valid");
								message += "\n";
							}
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.convfact.import", value));
							message += "\n";
						}
					}
					if (row.size() > 4) {
						//CT HTTM
						value = row.get(4);
						if (!StringUtil.isNullOrEmpty(value)) {
							if (so != null && so.getCustomer() != null) {
								ObjectVO<CommercialSupportVO> vo = mapCommercialSupport.get(value);
								if (vo == null) {
									vo = saleOrderMgr.getCommercialSupport(null, currentShop.getId(), so.getCustomer().getId(), lockDay, null, value);
									if (vo != null && vo.getLstObject().size() == 1) {
										mapCommercialSupport.put(value, vo);
										lstCommercialSupport.add(value);
										if (vo.getLstObject().get(0).getPromotionType() == null) {
											vo.getLstObject().get(0).setPromotionType("");
										}
										lstCommercialSupportType.add(vo.getLstObject().get(0).getPromotionType());
									} else {
										message += Configuration
												.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.commercial.support.code.import", value));
										message += "\n";
									}
								} else {
									lstCommercialSupport.add(value);
									if (vo.getLstObject().get(0).getPromotionType() == null) {
										vo.getLstObject().get(0).setPromotionType("");
									}
									lstCommercialSupportType.add(vo.getLstObject().get(0).getPromotionType());
								}
							}
						} else {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.commercial.support.code.import", value));
							message += "\n";
						}
					}
					if (StringUtil.isNullOrEmpty(message)) {
						if (so != null && so.getCustomer() != null) {
							List<CommercialSupportCodeVO> lstCommercialSupportCodeVOs = new ArrayList<CommercialSupportCodeVO>();
							CommercialSupportCodeVO vos;
							if (lstCommercialSupport != null) {
								for (int j = 0; j < lstCommercialSupport.size(); ++j) {
									if (!StringUtil.isNullOrEmpty(lstCommercialSupport.get(j))) {
										vos = new CommercialSupportCodeVO();
										vos.setCode(lstCommercialSupport.get(j));
										vos.setCustomerId(so.getCustomer().getId());
										vos.setShopId(currentShop.getId());
										if (StringUtil.isNullOrEmpty(lstCommercialSupportType.get(j))) {
											vos.setType(CommercialSupportType.DISPLAY_PROGRAM);
										} else {
											vos.setType(CommercialSupportType.PROMOTION_PROGRAM);
										}
										vos.setProductId(lstProduct.get(j).getId());
										lstCommercialSupportCodeVOs.add(vos);
									}
								}
							}
							/** Thong tin san pham ban */
							SaleOrderDetail saleOrderDetail = null;
							/** Tao don hang */
							/** Quy doi ve thong tin nho nhat */
							so.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
							so.setApprovedStep(SaleOrderStep.CONFIRMED);
							so.setOrderType(OrderType.IN);
							//							so.setVat(Float.valueOf("10"));trungtm6
							so.setOrderSource(SaleOrderSource.WEB);
							/**
							 * Kiem tra don hang trong tuyen va ngoai tuyen Tim
							 * duoc nhan vien ban hang thi co nghia la trong
							 * tuyen IS_VISIT_PLAN =1,nguoc lai thi ngoai tuyen
							 * IS_VISIT_PLAN =0
							 */
							Staff sStaff = null;
							RoutingCustomerFilter filter = new RoutingCustomerFilter();
							filter.setCustomerId(so.getCustomer().getId());
							filter.setShopId(currentShop.getId());
							filter.setOrderDate(lockDay);
							filter.setUserId(currentUser.getStaffRoot().getStaffId());
							filter.setRoleId(currentUser.getRoleToken().getRoleId());
							List<Staff> lstS = staffMgr.getSaleStaffByDate(filter);
							if (lstS != null && lstS.size() > 0) {
								sStaff = lstS.get(0);
							}
							if (sStaff != null && sStaff.getStaffCode().equals(so.getStaff().getStaffCode())) {
								so.setIsVisitPlan(1);
							} else {
								so.setIsVisitPlan(0);
							}
							so.setCreateUser(currentUser.getUserName());
							so.setCreateDate(sysDate);
							so.setOrderDate(lockDay);
							/** Set do uu tien mac dinh cho don hang */
							ApParam app = apParamMgr.getApParamByCodeEx("B", ActiveType.RUNNING);
							so.setPriority(Integer.valueOf((app.getId().toString())));
							/** Set ngay giao mac dinh la ngay chot */
							so.setDeliveryDate(lockDay);
							so.setAmount(BigDecimal.ZERO);
							so.setDiscount(BigDecimal.ZERO);
							so.setTotal(BigDecimal.ZERO);
							/** Danh sach san pham ban */
							List<SaleProductVO> lstSaleProductVO = new ArrayList<SaleProductVO>();
							Map<String, Integer> indexOfSaleProduct = new HashMap<String, Integer>();
							List<SaleOrderLot> lstLot = null;
							/** Danh sach san pham khuyen mai */
							List<SaleProductVO> lstPromoProductVO = new ArrayList<SaleProductVO>();
							product = null;
							SaleProductVO productVO = null;
							BigDecimal totalWeight = BigDecimal.ZERO;
							/**
							 * Lay thong tin danh sach san pham ban Tu do lay
							 * danh sach cac san pham khuyen mai duoc ap dung
							 * cho don hang luu thong tin bao gom hoa don va cac
							 * thong tin ve san pham ban, san pham khuyen mai
							 */
							//							ObjectVO<CommercialSupportVO> commercials =saleOrderMgr.getListCommercialSupport(null, currentShop.getId(), so.getCustomer().getId(), lockDay,null);			
							String commercialType = "";
							for (int k = 0; k < lstProduct.size(); ++k) {
								productVO = new SaleProductVO();
								product = lstProduct.get(k);
								saleOrderDetail = new SaleOrderDetail();
								saleOrderDetail.setProduct(product);//san pham ban
								saleOrderDetail.setPrice(productMgr.getProductPriceForCustomer(product.getId(), excelCustomer));//Gia san pham
								saleOrderDetail.setPriceValue(saleOrderDetail.getPrice().getPrice());
								saleOrderDetail.setPriceNotVat(saleOrderDetail.getPrice().getPriceNotVat());
								saleOrderDetail.setShop(currentShop);//Nha phan phoi	
								saleOrderDetail.setVat(saleOrderDetail.getPrice().getVat());
								saleOrderDetail.setStaff(so.getStaff());
								saleOrderDetail.setQuantity(lstQuantity.get(k));
								//								saleOrderDetail.setJoinProgramCode(lstCommercialSupport.get(k));
								saleOrderDetail.setCreateUser(so.getCreateUser());
								if (product.getGrossWeight() != null) {
									saleOrderDetail.setTotalWeight(product.getGrossWeight().multiply(BigDecimal.valueOf(saleOrderDetail.getQuantity())));
									totalWeight = totalWeight.add(saleOrderDetail.getTotalWeight());
								}

								//Tach kho cho sp km
								Integer totalAvailableQuantity = 0;
								StockTotalFilter stockTotalFilter = new StockTotalFilter();
								stockTotalFilter.setProductId(saleOrderDetail.getProduct().getId());
								stockTotalFilter.setOwnerType(StockObjectType.SHOP);
								stockTotalFilter.setOwnerId(currentShop.getId());
								List<StockTotal> lst = stockMgr.getListStockTotalByProductAndOwner(stockTotalFilter);
								if (lst != null && lst.size() > 0) {
									lstLot = new ArrayList<SaleOrderLot>();
									Integer total = 0;
									totalAvailableQuantity = saleOrderDetail.getQuantity();
									for (StockTotal stt : lst) {
										total += stt.getAvailableQuantity();
										SaleOrderLot lot = new SaleOrderLot();
										lot.setProduct(saleOrderDetail.getProduct());
										lot.setWarehouse(stt.getWarehouse());
										lot.setSaleOrderDetail(saleOrderDetail);
										lot.setSaleOrder(so);
										lot.setShop(so.getShop());
										lot.setStaff(so.getStaff());
										lot.setOrderDate(so.getOrderDate());
										lot.setCreateUser(so.getCreateUser());
										lot.setPriceValue(saleOrderDetail.getPriceValue());
										lot.setStockTotal(stt);
										if (stt.getAvailableQuantity() <= 0) {
											continue;
										} else if (saleOrderDetail.getProduct() != null && totalAvailableQuantity > stt.getAvailableQuantity()) {
											lot.setQuantity(stt.getAvailableQuantity());
											totalAvailableQuantity = totalAvailableQuantity - stt.getAvailableQuantity();
											lstLot.add(lot);
										} else if (saleOrderDetail.getProduct() != null && totalAvailableQuantity <= stt.getAvailableQuantity()) {
											lot.setQuantity(totalAvailableQuantity);
											totalAvailableQuantity = 0;
											lstLot.add(lot);
											break;
										}
									}
									if (totalAvailableQuantity > 0) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.available.quantity.is.not.enough.for.all.lot", productCode, total);
										message += "\n";
									}
								}
								if (lstLot.size() > 0) {
									productVO.setLstSaleOrderLot(lstLot);
								}
								/**
								 * Kiem tra currentShop da chot ngay hay chua
								 * Neu co thi luu ngay don hang la ngay chot
								 * */
								saleOrderDetail.setOrderDate(lockDay);
								saleOrderDetail.setCreateDate(sysDate);
								if (product.getGrossWeight() != null) {
									saleOrderDetail.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(lstQuantity.get(k))));
								}
								/** Danh sach san pham ban */
								if (lstCommercialSupport != null && StringUtil.isNullOrEmpty(lstCommercialSupport.get(k))) {
									//Khong xu ly truong hop khac don Tra thuong trung bay

								} else {
									saleOrderDetail.setIsFreeItem(1);
									saleOrderDetail.setAmount(BigDecimal.ZERO);
									saleOrderDetail.setProgramCode(lstCommercialSupport.get(k));
									commercialType = lstCommercialSupportType.get(k);
									ProgramType type = null;
									if (commercialType.startsWith("ZM")) {
										type = ProgramType.MANUAL_PROM;
									} else if (commercialType.startsWith("ZD")) {
										type = ProgramType.PRODUCT_EXCHANGE;
									} else if (commercialType.startsWith("ZH")) {
										type = ProgramType.DESTROY;
									} else if (commercialType.startsWith("ZT")) {
										type = ProgramType.RETURN;
									} else {
										type = ProgramType.DISPLAY_SCORE;
									}
									saleOrderDetail.setProgramType(type);
									saleOrderDetail.setProgrameTypeCode(commercialType);
								}
								productVO.setSaleOrderDetail(saleOrderDetail);
								lstSaleProductVO.add(productVO);
								indexOfSaleProduct.put(product.getProductCode(), lstSaleProductVO.indexOf(productVO));
							}
							so.setTotalWeight(totalWeight);
							if (StringUtil.isNullOrEmpty(message)) {
								saleOrderMgr.createSaleOrder(so, lstSaleProductVO, lstPromoProductVO, null, null, null, null, null, null, getLogInfoVO());
								mapProduct = new HashMap<String, Product>();
							} else {
								lstFails.add(StringUtil.addFailBean(row, message));
								message = "";
								mapProduct = new HashMap<String, Product>();
							}
						}
					} else {
						lstFails.add(StringUtil.addFailBean(row, message));
						message = "";
						mapProduct = new HashMap<String, Product>();
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_SALE_PRODUCT_CREATE_ORDER_DISPLAY_IMPORT_FAIL);

			} catch (BusinessException be) {
				LogUtility.logErrorStandard(be, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.importOrderForDisplay"), createLogErrorStandard(actionStartTime));
				if (be.getMessage().equals("LOT_QUANTITY_IS_NOT_ENOUGH")) {
					errMsgImport = ValidateUtil.getErrorMsg(ConstantManager.ERR_LOT_QUANTITY_IS_NOT_ENOUGH);
				} else if (be.getMessage().equals("RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY")) {
					errMsgImport = ValidateUtil.getErrorMsg(ConstantManager.ERR_RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY);
				} else if (be.getMessage().equals("TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT")) {
					errMsgImport = ValidateUtil.getErrorMsg(ConstantManager.ERR_TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT);
				} else if (be.getMessage().equals("MAX_DAY_DEBIT")) {
					errMsgImport = ValidateUtil.getErrorMsg(ConstantManager.ERR_MAX_DAY_DEBIT);
				} else {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				}
			} catch (Exception ex) {
				LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.importOrderForDisplay"), createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Xem chi tiet CTKM
	 * 
	 * @return the string
	 * @author nhanlt6
	 */
	public String showPromotionProgramDetail() {
		boolean error = true;
		try {
			if (currentShop == null) {
				result.put(ERROR, error);
				return JSON;
			}
			PromotionProgram program = promotionProgramMgr.getPromotionProgramByCode(ppCode);
			if (program != null) {
				result.put("program", program);
				result.put("isPromotion", true);
				result.put("fromDate", DateUtil.toDateString(program.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				result.put("toDate", DateUtil.toDateString(program.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				error = false;
			} else {
				DisplayProgramVNMMgr displayProgramVNMMgr = (DisplayProgramVNMMgr) context.getBean("displayProgramVNMMgr");
				DisplayProgramVNM displayProgramVNM = displayProgramVNMMgr.getDisplayProgramVNMByCode(ppCode);
				if (displayProgramVNM != null) {
					result.put("program", displayProgramVNM);
					result.put("isPromotion", false);
					result.put("fromDate", DateUtil.toDateString(displayProgramVNM.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					result.put("toDate", DateUtil.toDateString(displayProgramVNM.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
					error = false;
				} else {
					error = true;
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.showPromotionProgramDetail"), createLogErrorStandard(actionStartTime));
		}
		result.put(ERROR, error);
		return JSON;
	}

	/**
	 * Lay don hang web de chinh sua
	 * 
	 * @author lacnv1
	 * @since Sep 26, 2014
	 */
	public String searchWebSaleOrder() throws Exception {
		try {
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date now = new Date();
			//			ShopToken shop = currentUser.getShopRoot();
			//			Long shopId = (shop != null) ? shop.getShopId() : null;
			Shop shop = getCurrentShop();
			if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
				Shop sh = shopMgr.getShopByCode(shopCodeFilter);
				if (sh != null) {
					shop = sh;
				}
			}
			SoFilter filter = new SoFilter();
			filter.setkPaging(kPaging);
			filter.setShortCode(customerCode);
			filter.setCustomeName(name);
			filter.setStaffCode(salerCode);
			filter.setOrderNumber(code);
			filter.setApproval(SaleOrderStatus.NOT_YET_APPROVE);
			filter.setApprovedStep(SaleOrderStep.CONFIRMED);
			filter.setOrderSource(SaleOrderSource.WEB);
			filter.setListOrderType(Arrays.asList(OrderType.IN));

			filter.setDeliveryCode(deliveryCode);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				filter.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (DateUtil.compareDateWithoutTime(tDate, now) > 0) {
					tDate = now;
				}
				filter.setToDate(tDate);
			} else {
				filter.setToDate(now);
			}
			filter.setShopId(shop.getId());

			ObjectVO<SaleOrder> objectVOSaleOrder = saleOrderMgr.getListSaleOrderEx1(filter);
			if (objectVOSaleOrder != null) {
				result.put("total", objectVOSaleOrder.getkPaging().getTotalRows());
				result.put("rows", objectVOSaleOrder.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<SaleOrder>());
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.searchWebSaleOrder"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put("total", 0);
			result.put("rows", new ArrayList<SaleOrder>());
		}
		return JSON;
	}

	/**
	 * Lay don hang web de chinh sua
	 * 
	 * @author lacnv1
	 * @since Sep 27, 2014
	 */
	public String getSaleOrderForAdjustment() throws Exception {
		try {
			if (orderId != null && orderId > 0) { // sua don hang tren web
				if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
					Shop sh = shopMgr.getShopByCode(shopCodeFilter);
					if (sh != null) {
						shopId = sh.getId();
					}
				} else {
					shopId = currentUser.getShopRoot().getShopId();
				}
				saleOrder = saleOrderMgr.getSaleOrderById(orderId);

				List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
				if (lstPr != null && lstPr.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())) {//cho show gia
					SoFilter filter = new SoFilter();
					Shop shop = shopMgr.getShopById(shopId);
					if (shop != null) {
						filter.setShopTypeId(shop.getType().getId());
					}
					filter.setShopId(shopId);
					filter.setLockDay(saleOrder.getOrderDate());
					filter.setLstSaleOrderId(Arrays.asList(orderId));
					filter.setIsFreeItem(0);
					List<OrderProductVO> lstPrice = saleOrderMgr.getListProductPriceWarning(filter);
					if (lstPrice != null && lstPrice.size() > 0) {
						result.put("priceWarning", 1);
					}
				}
				
				//lay danh sach key shop 
				if (saleOrder != null) {
					
				}
			}
			result.put("saleOrder", saleOrder);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.getSaleOrderForAdjustment"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Huy don hang tao tren web
	 * 
	 * @author lacnv1
	 * @since Sep 29, 2014
	 */
	public String cancelWebOrder() throws Exception {
		resetToken(result);
		try {
			result.put(ERROR, true);
			if (orderId == null || orderId < 0) {
				result.put("errMsg", R.getResource("sp.sale.order.required.to.cancel"));
				return JSON;
			}
			saleOrder = saleOrderMgr.getSaleOrderById(orderId);
			if (saleOrder == null) {
				result.put("errMsg", R.getResource("sp.search.sale.order.approved.error.remove"));
				return JSON;
			}
			if (!SaleOrderSource.WEB.equals(saleOrder.getOrderSource())) {
				result.put("errMsg", R.getResource("sp.sale.order.not.web", ""));
				return JSON;
			}
			//			if (saleOrder.getShop() == null || !currentShop.getId().equals(saleOrder.getShop().getId())) {
			/*
			 * if (saleOrder.getShop() == null ||
			 * !shopMgr.checkAncestor(currentShop.getShopCode(),
			 * saleOrder.getShop().getShopCode())) { result.put("errMsg",
			 * ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION,
			 * "")); return JSON; }
			 */

			saleOrderMgr.cancelWebOrder(saleOrder.getId(), getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.cancelWebOrder"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @version 1.0
	 * @param
	 * @return Lay thong tin don ks va danh sach level cua ks
	 * @exception BusinessException
	 * @see
	 * @since 12/8/2015
	 * @serial
	 */
	public String getKeyShopDetail() throws Exception {
		try {
			result.put(ERROR, false);
			result.put("ks", null);
			result.put("lstLevel", null);
			KS ks = keyShopMgr.getKSByCode(ksCode);
			if (ks == null) {
				return JSON;
			}
			KeyShopFilter filter = new KeyShopFilter();
			filter.setKsId(ks.getId());
			//Lay thong tin ks va san pham ks
			List<KeyShopVO> lstInfo = keyShopMgr.getInfoKSForOrder(filter);
			if (lstInfo != null && lstInfo.size() > 0) {
				result.put("ksInfo", lstInfo.get(0));
			}
			//Lay danh sach level cua ks
			ObjectVO<KeyShopVO> voLevel = keyShopMgr.getListKSLevelVOByFilter(filter);
			if (voLevel != null && voLevel.getLstObject() != null) {
				result.put("lstLevel", voLevel.getLstObject());
			}

		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.CreateProductOrderAction.getKeyShopDetail"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, false);
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	///==============================================GETTER - SETTER==================================================
	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public File getExcelFileDisplay() {
		return excelFileDisplay;
	}

	public void setExcelFileDisplay(File excelFileDisplay) {
		this.excelFileDisplay = excelFileDisplay;
	}

	public String getExcelFileDisplayContentType() {
		return excelFileDisplayContentType;
	}

	public void setExcelFileDisplayContentType(String excelFileDisplayContentType) {
		this.excelFileDisplayContentType = excelFileDisplayContentType;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getStaffTypeCode() {
		return staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getPpCode() {
		return ppCode;
	}

	public void setPpCode(String ppCode) {
		this.ppCode = ppCode;
	}

	public String getCashierCode() {
		return cashierCode;
	}

	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getSalerCode() {
		return salerCode;
	}

	public void setSalerCode(String salerCode) {
		this.salerCode = salerCode;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getErrMsgImport() {
		return errMsgImport;
	}

	public void setErrMsgImport(String errMsgImport) {
		this.errMsgImport = errMsgImport;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public Long getSaleOrderDetailId() {
		return saleOrderDetailId;
	}

	public void setSaleOrderDetailId(Long saleOrderDetailId) {
		this.saleOrderDetailId = saleOrderDetailId;
	}

	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public int getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}

	public Integer getCanChangePromotionQuantity() {
		return canChangePromotionQuantity;
	}

	public void setCanChangePromotionQuantity(Integer canChangePromotionQuantity) {
		this.canChangePromotionQuantity = canChangePromotionQuantity;
	}

	public Integer getAllowEditPromotion() {
		return allowEditPromotion;
	}

	public void setAllowEditPromotion(Integer allowEditPromotion) {
		this.allowEditPromotion = allowEditPromotion;
	}

	public Integer getTypeSortKey() {
		return typeSortKey;
	}

	public void setTypeSortKey(Integer typeSortKey) {
		this.typeSortKey = typeSortKey;
	}

	public Integer getIsAuthorize() {
		return isAuthorize;
	}

	public void setIsAuthorize(Integer isAuthorize) {
		this.isAuthorize = isAuthorize;
	}

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	public Date getLockDay() {
		return lockDay;
	}

	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}

	public List<String> getLstBuyProductCommercialSupportProgram() {
		return lstBuyProductCommercialSupportProgram;
	}

	public void setLstBuyProductCommercialSupportProgram(List<String> lstBuyProductCommercialSupportProgram) {
		this.lstBuyProductCommercialSupportProgram = lstBuyProductCommercialSupportProgram;
	}

	public List<String> getLstBuyProductCommercialSupportProgramType() {
		return lstBuyProductCommercialSupportProgramType;
	}

	public void setLstBuyProductCommercialSupportProgramType(List<String> lstBuyProductCommercialSupportProgramType) {
		this.lstBuyProductCommercialSupportProgramType = lstBuyProductCommercialSupportProgramType;
	}

	public List<String> getLstCommercialSupport() {
		return lstCommercialSupport;
	}

	public void setLstCommercialSupport(List<String> lstCommercialSupport) {
		this.lstCommercialSupport = lstCommercialSupport;
	}

	public List<String> getLstProduct() {
		return lstProduct;
	}

	public void setLstProduct(List<String> lstProduct) {
		this.lstProduct = lstProduct;
	}

	public List<String> getLstPromotionCode() {
		return lstPromotionCode;
	}

	public void setLstPromotionCode(List<String> lstPromotionCode) {
		this.lstPromotionCode = lstPromotionCode;
	}

	public List<String> getLstPromotionProduct() {
		return lstPromotionProduct;
	}

	public void setLstPromotionProduct(List<String> lstPromotionProduct) {
		this.lstPromotionProduct = lstPromotionProduct;
	}

	public List<String> getLstCommercialSupportType() {
		return lstCommercialSupportType;
	}

	public void setLstCommercialSupportType(List<String> lstCommercialSupportType) {
		this.lstCommercialSupportType = lstCommercialSupportType;
	}

	public List<String> getLstPromotionProductQuatity() {
		return lstPromotionProductQuatity;
	}

	public void setLstPromotionProductQuatity(List<String> lstPromotionProductQuatity) {
		this.lstPromotionProductQuatity = lstPromotionProductQuatity;
	}

	public List<String> getLstPromotionProductMaxQuatity() {
		return lstPromotionProductMaxQuatity;
	}

	public void setLstPromotionProductMaxQuatity(List<String> lstPromotionProductMaxQuatity) {
		this.lstPromotionProductMaxQuatity = lstPromotionProductMaxQuatity;
	}

	public List<String> getLstPromotionProductMaxQuatityCk() {
		return lstPromotionProductMaxQuatityCk;
	}

	public void setLstPromotionProductMaxQuatityCk(List<String> lstPromotionProductMaxQuatityCk) {
		this.lstPromotionProductMaxQuatityCk = lstPromotionProductMaxQuatityCk;
	}

	public List<String> getLstMyTime() {
		return lstMyTime;
	}

	public void setLstMyTime(List<String> lstMyTime) {
		this.lstMyTime = lstMyTime;
	}

	public List<String> getLstDiscount() {
		return lstDiscount;
	}

	public void setLstDiscount(List<String> lstDiscount) {
		this.lstDiscount = lstDiscount;
	}

	public List<String> getLstDisper() {
		return lstDisper;
	}

	public void setLstDisper(List<String> lstDisper) {
		this.lstDisper = lstDisper;
	}

	public List<Customer> getLstKH() {
		return lstKH;
	}

	public void setLstKH(List<Customer> lstKH) {
		this.lstKH = lstKH;
	}

	public List<Staff> getLstNVBH() {
		return lstNVBH;
	}

	public void setLstNVBH(List<Staff> lstNVBH) {
		this.lstNVBH = lstNVBH;
	}

	public List<Staff> getLstNVTT() {
		return lstNVTT;
	}

	public void setLstNVTT(List<Staff> lstNVTT) {
		this.lstNVTT = lstNVTT;
	}

	public List<Staff> getLstNVGH() {
		return lstNVGH;
	}

	public void setLstNVGH(List<Staff> lstNVGH) {
		this.lstNVGH = lstNVGH;
	}

	public List<Integer> getLstQuantity() {
		return lstQuantity;
	}

	public void setLstQuantity(List<Integer> lstQuantity) {
		this.lstQuantity = lstQuantity;
	}

	public List<Integer> getLstPromotionType() {
		return lstPromotionType;
	}

	public void setLstPromotionType(List<Integer> lstPromotionType) {
		this.lstPromotionType = lstPromotionType;
	}

	public List<Car> getLstCars() {
		return lstCars;
	}

	public void setLstCars(List<Car> lstCars) {
		this.lstCars = lstCars;
	}

	public List<ApParam> getPriority() {
		return priority;
	}

	public void setPriority(List<ApParam> priority) {
		this.priority = priority;
	}

	public List<SaleOrderDetailVOEx> getOrder_detail() {
		return order_detail;
	}

	public void setOrder_detail(List<SaleOrderDetailVOEx> order_detail) {
		this.order_detail = order_detail;
	}

	public List<SaleOrderDetailVOEx> getOrder_detail_promotion() {
		return order_detail_promotion;
	}

	public void setOrder_detail_promotion(List<SaleOrderDetailVOEx> order_detail_promotion) {
		this.order_detail_promotion = order_detail_promotion;
	}

	public List<StockTotalVO> getProductStocks() {
		return productStocks;
	}

	public void setProductStocks(List<StockTotalVO> productStocks) {
		this.productStocks = productStocks;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<OrderProductVO> getOrderProductVOs() {
		return orderProductVOs;
	}

	public void setOrderProductVOs(List<OrderProductVO> orderProductVOs) {
		this.orderProductVOs = orderProductVOs;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public List<WarehouseVO> getLstWarehouse() {
		return lstWarehouse;
	}

	public void setLstWarehouse(List<WarehouseVO> lstWarehouse) {
		this.lstWarehouse = lstWarehouse;
	}

	public List<StockTotalVO> getProductWarehouses() {
		return productWarehouses;
	}

	public void setProductWarehouses(List<StockTotalVO> productWarehouses) {
		this.productWarehouses = productWarehouses;
	}

	public Map<Long, List<StockTotalVO>> getMapProductByWarehouse() {
		return mapProductByWarehouse;
	}

	public void setMapProductByWarehouse(Map<Long, List<StockTotalVO>> mapProductByWarehouse) {
		this.mapProductByWarehouse = mapProductByWarehouse;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public List<String> getLstWarehouseSaleProduct() {
		return lstWarehouseSaleProduct;
	}

	public void setLstWarehouseSaleProduct(List<String> lstWarehouseSaleProduct) {
		this.lstWarehouseSaleProduct = lstWarehouseSaleProduct;
	}

	public List<String> getLstWarehouseId() {
		return lstWarehouseId;
	}

	public void setLstWarehouseId(List<String> lstWarehouseId) {
		this.lstWarehouseId = lstWarehouseId;
	}

	public List<String> getLstWarehouseBuyProductCommercialSupportProgram() {
		return lstWarehouseBuyProductCommercialSupportProgram;
	}

	public void setLstWarehouseBuyProductCommercialSupportProgram(List<String> lstWarehouseBuyProductCommercialSupportProgram) {
		this.lstWarehouseBuyProductCommercialSupportProgram = lstWarehouseBuyProductCommercialSupportProgram;
	}

	public List<String> getLstWarehouseBuyProductCommercialSupportProgramType() {
		return lstWarehouseBuyProductCommercialSupportProgramType;
	}

	public void setLstWarehouseBuyProductCommercialSupportProgramType(List<String> lstWarehouseBuyProductCommercialSupportProgramType) {
		this.lstWarehouseBuyProductCommercialSupportProgramType = lstWarehouseBuyProductCommercialSupportProgramType;
	}

	public List<String> getLstWarehouseCommercialSupport() {
		return lstWarehouseCommercialSupport;
	}

	public void setLstWarehouseCommercialSupport(List<String> lstWarehouseCommercialSupport) {
		this.lstWarehouseCommercialSupport = lstWarehouseCommercialSupport;
	}

	public List<String> getLstWarehouseCommercialSupportType() {
		return lstWarehouseCommercialSupportType;
	}

	public void setLstWarehouseCommercialSupportType(List<String> lstWarehouseCommercialSupportType) {
		this.lstWarehouseCommercialSupportType = lstWarehouseCommercialSupportType;
	}

	public List<String> getLstWarehouseQuantity() {
		return lstWarehouseQuantity;
	}

	public void setLstWarehouseQuantity(List<String> lstWarehouseQuantity) {
		this.lstWarehouseQuantity = lstWarehouseQuantity;
	}

	public List<String> getLstSaleProductProgramCode() {
		return lstSaleProductProgramCode;
	}

	public void setLstSaleProductProgramCode(List<String> lstSaleProductProgramCode) {
		this.lstSaleProductProgramCode = lstSaleProductProgramCode;
	}

	public List<String> getLstSaleProductDiscountPercent() {
		return lstSaleProductDiscountPercent;
	}

	public void setLstSaleProductDiscountPercent(List<String> lstSaleProductDiscountPercent) {
		this.lstSaleProductDiscountPercent = lstSaleProductDiscountPercent;
	}

	public List<String> getLstSaleProductDiscountAmount() {
		return lstSaleProductDiscountAmount;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public Integer getEditOrderTablet() {
		return editOrderTablet;
	}

	public void setEditOrderTablet(Integer editOrderTablet) {
		this.editOrderTablet = editOrderTablet;
	}

	public Integer getIsVanSale() {
		return isVanSale;
	}

	public void setIsVanSale(Integer isVanSale) {
		this.isVanSale = isVanSale;
	}

	public Integer getIsApprovedVanSale() {
		return isApprovedVanSale;
	}

	public void setIsApprovedVanSale(Integer isApprovedVanSale) {
		this.isApprovedVanSale = isApprovedVanSale;
	}

	public void setLstSaleProductDiscountAmount(List<String> lstSaleProductDiscountAmount) {
		this.lstSaleProductDiscountAmount = lstSaleProductDiscountAmount;
	}

	public List<String> getLstPromoWarehouseId() {
		return lstPromoWarehouseId;
	}

	public void setLstPromoWarehouseId(List<String> lstPromoWarehouseId) {
		this.lstPromoWarehouseId = lstPromoWarehouseId;
	}

	public List<String> getLstPromoWarehouseQuantity() {
		return lstPromoWarehouseQuantity;
	}

	public void setLstPromoWarehouseQuantity(List<String> lstPromoWarehouseQuantity) {
		this.lstPromoWarehouseQuantity = lstPromoWarehouseQuantity;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Shop getCurrentShop() {
		return currentShop;
	}

	public void setCurrentShop(Shop currentShop) {
		this.currentShop = currentShop;
	}

	public List<SaleOrderPromotionVO> getQuantityReceivedList() {
		return quantityReceivedList;
	}

	public void setQuantityReceivedList(List<SaleOrderPromotionVO> quantityReceivedList) {
		this.quantityReceivedList = quantityReceivedList;
	}

	public List<String> getLstProductHasPromotion() {
		return lstProductHasPromotion;
	}

	public void setLstProductHasPromotion(List<String> lstProductHasPromotion) {
		this.lstProductHasPromotion = lstProductHasPromotion;
	}

	public List<String> getLstPromoLevel() {
		return lstPromoLevel;
	}

	public void setLstPromoLevel(List<String> lstPromoLevel) {
		this.lstPromoLevel = lstPromoLevel;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public Integer getPromoLevel() {
		return promoLevel;
	}

	public void setPromoLevel(Integer promoLevel) {
		this.promoLevel = promoLevel;
	}

	public List<SaleOrderPromotionDetailVO> getLstSOPromoDetail() {
		return lstSOPromoDetail;
	}

	public void setLstSOPromoDetail(List<SaleOrderPromotionDetailVO> lstSOPromoDetail) {
		this.lstSOPromoDetail = lstSOPromoDetail;
	}

	public String getIsUsingZVOrder() {
		return isUsingZVOrder;
	}

	public void setIsUsingZVOrder(String isUsingZVOrder) {
		this.isUsingZVOrder = isUsingZVOrder;
	}

	public Integer getIsWarningPrice() {
		return isWarningPrice;
	}

	public void setIsWarningPrice(Integer isWarningPrice) {
		this.isWarningPrice = isWarningPrice;
	}

	public Integer getCompareLockDay() {
		return compareLockDay;
	}

	public void setCompareLockDay(Integer compareLockDay) {
		this.compareLockDay = compareLockDay;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public Integer getApprovedStep() {
		return approvedStep;
	}

	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVTTVo() {
		return lstNVTTVo;
	}

	public void setLstNVTTVo(List<StaffVO> lstNVTTVo) {
		this.lstNVTTVo = lstNVTTVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public List<String> getLstProductGroupAndLevelGroupId() {
		return lstProductGroupAndLevelGroupId;
	}

	public void setLstProductGroupAndLevelGroupId(List<String> lstProductGroupAndLevelGroupId) {
		this.lstProductGroupAndLevelGroupId = lstProductGroupAndLevelGroupId;
	}

	public String getIsUsingZVCode() {
		return isUsingZVCode;
	}

	public void setIsUsingZVCode(String isUsingZVCode) {
		this.isUsingZVCode = isUsingZVCode;
	}

	public Integer getLockedStock() {
		return lockedStock;
	}

	public void setLockedStock(Integer lockedStock) {
		this.lockedStock = lockedStock;
	}

	public Long getGroupLevelId() {
		return groupLevelId;
	}

	public void setGroupLevelId(Long groupLevelId) {
		this.groupLevelId = groupLevelId;
	}

	public Integer getViewOnly() {
		return viewOnly;
	}

	public void setViewOnly(Integer viewOnly) {
		this.viewOnly = viewOnly;
	}

	public List<GroupLevelDetailVO> getLstCTTL() {
		return lstCTTL;
	}

	public void setLstCTTL(List<GroupLevelDetailVO> lstCTTL) {
		this.lstCTTL = lstCTTL;
	}

	public Integer getIsLostDetail() {
		return isLostDetail;
	}

	public void setIsLostDetail(Integer isLostDetail) {
		this.isLostDetail = isLostDetail;
	}

	public Integer getIsSaveNotCheck() {
		return isSaveNotCheck;
	}

	public List<Shop> getLstShop() {
		return lstShop;
	}

	public void setLstShop(List<Shop> lstShop) {
		this.lstShop = lstShop;
	}

	public String getShopCodeFilter() {
		return shopCodeFilter;
	}

	public void setShopCodeFilter(String shopCodeFilter) {
		this.shopCodeFilter = shopCodeFilter;
	}

	public void setIsSaveNotCheck(Integer isSaveNotCheck) {
		this.isSaveNotCheck = isSaveNotCheck;
	}

	public List<BigDecimal> getLstpkPriceValue() {
		return lstpkPriceValue;
	}

	public void setLstpkPriceValue(List<BigDecimal> lstpkPriceValue) {
		this.lstpkPriceValue = lstpkPriceValue;
	}

	public List<BigDecimal> getLstPriceValue() {
		return lstPriceValue;
	}

	public void setLstPriceValue(List<BigDecimal> lstPriceValue) {
		this.lstPriceValue = lstPriceValue;
	}

	public Boolean getIsAllowShowPrice() {
		return isAllowShowPrice;
	}

	public void setIsAllowShowPrice(Boolean isAllowShowPrice) {
		this.isAllowShowPrice = isAllowShowPrice;
	}

	public List<Integer> getLstQuantityPackage() {
		return lstQuantityPackage;
	}

	public void setLstQuantityPackage(List<Integer> lstQuantityPackage) {
		this.lstQuantityPackage = lstQuantityPackage;
	}

	public List<Integer> getLstQuantityRetail() {
		return lstQuantityRetail;
	}

	public void setLstQuantityRetail(List<Integer> lstQuantityRetail) {
		this.lstQuantityRetail = lstQuantityRetail;
	}

	public List<String> getLstPromotionProductQuatityPackage() {
		return lstPromotionProductQuatityPackage;
	}

	public void setLstPromotionProductQuatityPackage(List<String> lstPromotionProductQuatityPackage) {
		this.lstPromotionProductQuatityPackage = lstPromotionProductQuatityPackage;
	}

	public List<String> getLstPromotionProductQuatityRetail() {
		return lstPromotionProductQuatityRetail;
	}

	public void setLstPromotionProductQuatityRetail(List<String> lstPromotionProductQuatityRetail) {
		this.lstPromotionProductQuatityRetail = lstPromotionProductQuatityRetail;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public List<KeyShopVO> getLstKeyShop() {
		return lstKeyShop;
	}

	public void setLstKeyShop(List<KeyShopVO> lstKeyShop) {
		this.lstKeyShop = lstKeyShop;
	}

	public List<SaleOrderPromoLotVO> getLstPromoLot() {
		return lstPromoLot;
	}

	public void setLstPromoLot(List<SaleOrderPromoLotVO> lstPromoLot) {
		this.lstPromoLot = lstPromoLot;
	}

	public List<SaleOrderPromotionDetailVO> getLstProgramDatSaleProduct() {
		return lstProgramDatSaleProduct;
	}

	public void setLstProgramDatSaleProduct(List<SaleOrderPromotionDetailVO> lstProgramDatSaleProduct) {
		this.lstProgramDatSaleProduct = lstProgramDatSaleProduct;
	}

	public String getKsCode() {
		return ksCode;
	}

	public void setKsCode(String ksCode) {
		this.ksCode = ksCode;
	}

	public Boolean getIsDividualWarehouse() {
		return isDividualWarehouse;
	}

	public void setIsDividualWarehouse(Boolean isDividualWarehouse) {
		this.isDividualWarehouse = isDividualWarehouse;
	}

	public String getSysConvertQuantityConfig() {
		return sysConvertQuantityConfig;
	}

	public void setSysConvertQuantityConfig(String sysConvertQuantityConfig) {
		this.sysConvertQuantityConfig = sysConvertQuantityConfig;
	}

	public List<String> getLstWarehouseQuantityPackage() {
		return lstWarehouseQuantityPackage;
	}

	public void setLstWarehouseQuantityPackage(List<String> lstWarehouseQuantityPackage) {
		this.lstWarehouseQuantityPackage = lstWarehouseQuantityPackage;
	}

	public List<String> getLstWarehouseQuantityRetail() {
		return lstWarehouseQuantityRetail;
	}

	public void setLstWarehouseQuantityRetail(List<String> lstWarehouseQuantityRetail) {
		this.lstWarehouseQuantityRetail = lstWarehouseQuantityRetail;
	}

}
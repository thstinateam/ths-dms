package ths.dms.web.action.saleproduct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SaleOrderVOEx;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class AdjustmentTaxAction.
 */
public class RemoveTaxAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Danh sach trang thai. */
	private List<StatusBean> listStatus;

	/** The list payment. */
	private List<StatusBean> listPayment;

	/** The choose status. */
	private Integer chooseStatus;

	/** The sale staff code. */
	private String saleStaffCode;

	/** The delivery staff code. */
	private String deliveryStaffCode;

	/** The tax value. */
	private Float taxValue;

	/** The red invoice number. */
	private String redInvoiceNumber;

	/** The invoice number. */
	private String invoiceNumber;

	/** The create from date. */
	private String createFromDate;

	/** The create to date. */
	private String createToDate;

	/** The customer code. */
	private String customerCode;

	private String currentShopCode;

	/** The list sale order vo ex. */
	private List<SaleOrderVOEx> listSaleOrderVOEx;

	/** The list invoice. */
	private List<Invoice> listInvoice;

	/** The list sale order id. */
	private List<String> listSaleOrderId;

	/** The list sale order numer */
	private List<String> listOrderNumber;

	/** The list invoice number. */
	private List<String> listInvoiceNumber;

	/** The list invoice id. */
	private List<String> listInvoiceId;

	/** The list invoice id delete. */
	private List<String> listDeleteInvoiceId;

	/** The list payment method. */
	private List<Integer> listPaymentMethod;

	/** The shop. */
	private Shop shop;

	/** The choose payment method. */
	private Integer choosePaymentMethod;

	/** The sale order mgr. */
	private SaleOrderMgr saleOrderMgr;

	/** The staff mgr. */
	private StaffMgr staffMgr;

	/** The list nvbh. */
	private List<Staff> listNVBH;
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH

	/** The list nvgh. */
	private List<Staff> listNVGH;
	private List<StaffVO> lstNVGHVo;//Danh sach NVGH

	/** The type payment all. Chon hinh thuc thanh toan tu dong. */
	private Integer typePaymentAll;

	private Integer isValueOrder;

	/**
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		listStatus = new ArrayList<StatusBean>();
		StatusBean withInvoice = new StatusBean(1L, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.invoice"));
		StatusBean noInvoice = new StatusBean(0L, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.no.invoice"));
		listStatus.add(withInvoice);
		listStatus.add(noInvoice);
		listPayment = new ArrayList<StatusBean>();
		listPayment.add(new StatusBean((long) InvoiceCustPayment.MONEY.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.payment.method.money")));
		listPayment.add(new StatusBean((long) InvoiceCustPayment.BANK_TRANSFER.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.payment.method.bank")));
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");

		if (!StringUtil.isNullOrEmpty(currentShopCode)) {
			shop = shopMgr.getShopByCode(currentShopCode);
		}
		if (shop != null) {
			shopId = shop.getId();
		}
	}

	/**
	 * @return String
	 * @author tientv
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);

		Staff staff = getStaffByCurrentUser();
		if (staff == null) {
			return PAGE_NOT_PERMISSION;
		}
		return SUCCESS;
	}

	/**
	 * Search.
	 * 
	 * @return the string
	 * @author tientv
	 * @since Oct 1, 2012
	 */
	public String search() {
		result.put("page", page);
		result.put("rows", rows);
		if (shop == null) {
			return JSON;
		}
		try {
			KPaging<Invoice> kPaging = new KPaging<Invoice>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			KPaging<SaleOrderVOEx> __kPaging = new KPaging<SaleOrderVOEx>();
			__kPaging.setPageSize(max);
			__kPaging.setPage(page - 1);
			Staff staff = getStaffByCurrentUser();
			if (staff == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString("vi", "common.label.staff.login")));
				return ERROR;
			}
			Date fromDate = DateUtil.parse(createFromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(createToDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Calendar calFrom = Calendar.getInstance();
			Calendar calTo = Calendar.getInstance();
			if (fromDate == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString("vi", "catalog.display.program.fromDate")));
				return ERROR;
			}
			if (toDate == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.toDate"));
				return ERROR;
			}

			calFrom.setTime(fromDate);
			calTo.setTime(toDate);
			if (calFrom.compareTo(calTo) > 0) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.toDate"));
				return ERROR;
			}

			Date lockDate = shopLockMgr.getNextLockedDay(shop.getId());
			if (lockDate == null) {
				lockDate = commonMgr.getSysDate();
			}

			if (chooseStatus == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FROM_DATE_NOT_GREATER_TO_DATE));
				return ERROR;
			} else if (chooseStatus == 1) {
				/** Da co hoa don */
				ObjectVO<Invoice> objInvoice = saleOrderMgr.getListInvoiceByConditionEx(kPaging, invoiceNumber, redInvoiceNumber, saleStaffCode, deliveryStaffCode, customerCode, fromDate, toDate, taxValue, InvoiceCustPayment
						.parseValue(choosePaymentMethod), lockDate, shop.getId(), isValueOrder);

				listInvoice = objInvoice.getLstObject();

				//				listInvoice = saleOrderMgr.getListInvoiceByCondition(kPaging,
				//						invoiceNumber, redInvoiceNumber, saleStaffCode, deliveryStaffCode,
				//						customerCode, fromDate, toDate, taxValue,
				//						InvoiceCustPayment.parseValue(choosePaymentMethod), lockDate, shop.getId(), isValueOrder);

				if (listInvoice != null) {
					for (Invoice item : listInvoice) {
						item.formatData();
					}
					result.put("total", objInvoice.getkPaging().getTotalRows());
					result.put("rows", listInvoice);
				} else {
					result.put("total", 0);
					result.put("rows", new ArrayList<Invoice>());
				}
			}
			listPayment = new ArrayList<StatusBean>();
			listPayment.add(new StatusBean((long) InvoiceCustPayment.MONEY.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.payment.method.money")));
			listPayment.add(new StatusBean((long) InvoiceCustPayment.BANK_TRANSFER.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.payment.method.bank")));
			return JSON;
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.RemoveTaxAction.search"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}

	/**
	 * Cancel.
	 * 
	 * @return the string
	 * @author phut
	 * @since Oct 1, 2012
	 */
	public String cancel() {
		resetToken(result);
		try {
			if (shop == null || currentUser == null) {
				return JSON;
			}
			List<Long> __listInvoiceId = new ArrayList<Long>();
			for (String invoiceId : listDeleteInvoiceId) {
				try {
					Long __invoiceId = Long.parseLong(invoiceId);
					__listInvoiceId.add(__invoiceId);
				} catch (Exception e) {
					LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.RemoveTaxAction.cancel: Long.parseLong(invoiceId)"), createLogErrorStandard(actionStartTime));
				}
			}
			saleOrderMgr.removeInvoice(__listInvoiceId, shop.getId(), currentUser.getUserName());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.RemoveTaxAction.cancel"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString("vi", "catalog.invoice.exist"));
			return ERROR;
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * xu ly thay doi don vi
	 * 
	 * @author trietptm
	 * @return JSON
	 */
	public String changeShop() {
		try {
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			if (shop == null) {
				return JSON;
			}
			Date dayLock = shopLockMgr.getNextLockedDay(shopId);
			if (dayLock == null) {
				dayLock = DateUtil.getCurrentGMTDate();
			}
			createFromDate = displayDate(dayLock);
			result.put("createFromDate", createFromDate);
			//Lay danh sach nhan vien
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setIsLstChildStaffRoot(false);
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setLstShopId(Arrays.asList(shopId));
			filter.setIsGetChildShop(false);
			//Lay NVBH
			filter.setUserId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setObjectType(StaffSpecificType.STAFF.getValue());
			filter.setStaffIdListStr(getStrListUserId());
			ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVBHVo = objVO.getLstObject();
			result.put("lstNVBHVo", lstNVBHVo);
			//Lay nhan vien NVGH
			filter.setUserId(null);
			filter.setRoleId(null);
			filter.setStaffIdListStr(null);
			filter.setObjectType(StaffSpecificType.NVGH.getValue());
			objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVGHVo = objVO.getLstObject();
			result.put("lstNVGHVo", lstNVGHVo);

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.RemoveTaxAction.changeShop"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * Gets the list status.
	 * 
	 * @return the list status
	 */
	public List<StatusBean> getListStatus() {
		return listStatus;
	}

	/**
	 * Sets the list status.
	 * 
	 * @param listStatus
	 *            the new list status
	 */
	public void setListStatus(List<StatusBean> listStatus) {
		this.listStatus = listStatus;
	}

	/**
	 * Gets the choose status.
	 * 
	 * @return the choose status
	 */
	public Integer getChooseStatus() {
		return chooseStatus;
	}

	/**
	 * Sets the choose status.
	 * 
	 * @param chooseStatus
	 *            the new choose status
	 */
	public void setChooseStatus(Integer chooseStatus) {
		this.chooseStatus = chooseStatus;
	}

	/**
	 * Gets the sale staff code.
	 * 
	 * @return the sale staff code
	 */
	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	/**
	 * Sets the sale staff code.
	 * 
	 * @param saleStaffCode
	 *            the new sale staff code
	 */
	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}

	/**
	 * Gets the delivery staff code.
	 * 
	 * @return the delivery staff code
	 */
	public String getDeliveryStaffCode() {
		return deliveryStaffCode;
	}

	/**
	 * Sets the delivery staff code.
	 * 
	 * @param deliveryStaffCode
	 *            the new delivery staff code
	 */
	public void setDeliveryStaffCode(String deliveryStaffCode) {
		this.deliveryStaffCode = deliveryStaffCode;
	}

	/**
	 * Gets the tax value.
	 * 
	 * @return the tax value
	 */
	public Float getTaxValue() {
		return taxValue;
	}

	/**
	 * Sets the tax value.
	 * 
	 * @param taxValue
	 *            the new tax value
	 */
	public void setTaxValue(Float taxValue) {
		this.taxValue = taxValue;
	}

	/**
	 * Gets the invoice number.
	 * 
	 * @return the invoice number
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * Sets the invoice number.
	 * 
	 * @param invoiceNumber
	 *            the new invoice number
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * Gets the creates the from date.
	 * 
	 * @return the creates the from date
	 */
	public String getCreateFromDate() {
		return createFromDate;
	}

	/**
	 * Sets the creates the from date.
	 * 
	 * @param createFromDate
	 *            the new creates the from date
	 */
	public void setCreateFromDate(String createFromDate) {
		this.createFromDate = createFromDate;
	}

	/**
	 * Gets the creates the to date.
	 * 
	 * @return the creates the to date
	 */
	public String getCreateToDate() {
		return createToDate;
	}

	/**
	 * Sets the creates the to date.
	 * 
	 * @param createToDate
	 *            the new creates the to date
	 */
	public void setCreateToDate(String createToDate) {
		this.createToDate = createToDate;
	}

	/**
	 * Gets the customer code.
	 * 
	 * @return the customer code
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * Sets the customer code.
	 * 
	 * @param customerCode
	 *            the new customer code
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	/**
	 * Gets the list invoice number.
	 * 
	 * @return the list invoice number
	 */
	public List<String> getListInvoiceNumber() {
		return listInvoiceNumber;
	}

	/**
	 * Sets the list invoice number.
	 * 
	 * @param listInvoiceNumber
	 *            the new list invoice number
	 */
	public void setListInvoiceNumber(List<String> listInvoiceNumber) {
		this.listInvoiceNumber = listInvoiceNumber;
	}

	/**
	 * Gets the shop.
	 * 
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 * 
	 * @param shop
	 *            the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * Gets the list payment.
	 * 
	 * @return the list payment
	 */
	public List<StatusBean> getListPayment() {
		return listPayment;
	}

	/**
	 * Sets the list payment.
	 * 
	 * @param listPayment
	 *            the new list payment
	 */
	public void setListPayment(List<StatusBean> listPayment) {
		this.listPayment = listPayment;
	}

	/**
	 * Gets the list payment method.
	 * 
	 * @return the list payment method
	 */
	public List<Integer> getListPaymentMethod() {
		return listPaymentMethod;
	}

	/**
	 * Sets the list payment method.
	 * 
	 * @param listPaymentMethod
	 *            the new list payment method
	 */
	public void setListPaymentMethod(List<Integer> listPaymentMethod) {
		this.listPaymentMethod = listPaymentMethod;
	}

	/**
	 * Gets the choose payment method.
	 * 
	 * @return the choose payment method
	 */
	public Integer getChoosePaymentMethod() {
		return choosePaymentMethod;
	}

	/**
	 * Sets the choose payment method.
	 * 
	 * @param choosePaymentMethod
	 *            the new choose payment method
	 */
	public void setChoosePaymentMethod(Integer choosePaymentMethod) {
		this.choosePaymentMethod = choosePaymentMethod;
	}

	/**
	 * Sets the list sale order vo ex.
	 * 
	 * @param listSaleOrderVOEx
	 *            the new list sale order vo ex
	 */
	public void setListSaleOrderVOEx(List<SaleOrderVOEx> listSaleOrderVOEx) {
		this.listSaleOrderVOEx = listSaleOrderVOEx;
	}

	/**
	 * Gets the list sale order vo ex.
	 * 
	 * @return the list sale order vo ex
	 */
	public List<SaleOrderVOEx> getListSaleOrderVOEx() {
		return listSaleOrderVOEx;
	}

	/**
	 * Gets the list sale order id.
	 * 
	 * @return the list sale order id
	 */
	public List<String> getListSaleOrderId() {
		return listSaleOrderId;
	}

	/**
	 * Sets the list sale order id.
	 * 
	 * @param listSaleOrderId
	 *            the new list sale order id
	 */
	public void setListSaleOrderId(List<String> listSaleOrderId) {
		this.listSaleOrderId = listSaleOrderId;
	}

	/**
	 * Sets the list invoice.
	 * 
	 * @param listInvoice
	 *            the new list invoice
	 */
	public void setListInvoice(List<Invoice> listInvoice) {
		this.listInvoice = listInvoice;
	}

	/**
	 * Gets the list invoice.
	 * 
	 * @return the list invoice
	 */
	public List<Invoice> getListInvoice() {
		return listInvoice;
	}

	/**
	 * Sets the list invoice id.
	 * 
	 * @param listInvoiceId
	 *            the new list invoice id
	 */
	public void setListInvoiceId(List<String> listInvoiceId) {
		this.listInvoiceId = listInvoiceId;
	}

	/**
	 * Gets the list invoice id.
	 * 
	 * @return the list invoice id
	 */
	public List<String> getListInvoiceId() {
		return listInvoiceId;
	}

	/**
	 * Gets the list nvbh.
	 * 
	 * @return the list nvbh
	 */
	public List<Staff> getListNVBH() {
		return listNVBH;
	}

	/**
	 * Sets the list nvbh.
	 * 
	 * @param listNVBH
	 *            the new list nvbh
	 */
	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}

	/**
	 * Gets the list nvgh.
	 * 
	 * @return the list nvgh
	 */
	public List<Staff> getListNVGH() {
		return listNVGH;
	}

	/**
	 * Sets the list nvgh.
	 * 
	 * @param listNVGH
	 *            the new list nvgh
	 */
	public void setListNVGH(List<Staff> listNVGH) {
		this.listNVGH = listNVGH;
	}

	public Integer getTypePaymentAll() {
		return typePaymentAll;
	}

	public void setTypePaymentAll(Integer typePaymentAll) {
		this.typePaymentAll = typePaymentAll;
	}

	public List<String> getListOrderNumber() {
		return listOrderNumber;
	}

	public void setListOrderNumber(List<String> listOrderNumber) {
		this.listOrderNumber = listOrderNumber;
	}

	public String getRedInvoiceNumber() {
		return redInvoiceNumber;
	}

	public void setRedInvoiceNumber(String redInvoiceNumber) {
		this.redInvoiceNumber = redInvoiceNumber;
	}

	public List<String> getListDeleteInvoiceId() {
		return listDeleteInvoiceId;
	}

	public void setListDeleteInvoiceId(List<String> listDeleteInvoiceId) {
		this.listDeleteInvoiceId = listDeleteInvoiceId;
	}

	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public String getCurrentShopCode() {
		return currentShopCode;
	}

	public void setCurrentShopCode(String currentShopCode) {
		this.currentShopCode = currentShopCode;
	}

}

package ths.dms.web.action.saleproduct;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.CarMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

public class AssignTransferStaffAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private CustomerMgr customerMgr;
	private ChannelTypeMgr channelTypeMgr;
	private SaleOrderMgr saleOrderMgr;
	private CarMgr carMgr;
	private ShopMgr shopMgr;
	private StaffMgr staffMgr;
	private Staff staff;
	private String shopCode;
	private Long shopId;
	private String orderNumber;
	private String transferStaffCode;
	private String saleStaffCode;
	private String cashierStaffCode;
	private String customerCode;
	private String startDate;
	private String priorityCode;
	private List<SaleOrder> listSaleOrder;
	private BigDecimal amount;
	private Float totalWeight;
	private List<Car> listCar;
	private List<Staff> listStaff;
	private List<Long> listSaleOrderId;
	private Long carId;
	private Long deliveryId;
	private Long cashierId;
	private Date now;
	private List<Staff> listNVBH;
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH
	private Shop shop;
	private List<Staff> listNVGH;
	private List<StaffVO> lstNVGHVo;//Danh sach NVGH
	private List<Staff> listNVTT;
	private List<StaffVO> lstNVTTVo;//Danh sach NVTT
	private File excelFile;
	private String excelFileContentType;
	private List<Map<String, Object>> listBeans;
	private Integer orderSource;
	private List<ApParam> priority;//Do uu tien

	private List<OrderType> orderTypes;

	/** //vuongmq; 04/06/2015; tim kiem theo Gia tri DH */
	private String numberValue;
	private BigDecimal numberValueOrder;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {

		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
		carMgr = (CarMgr) context.getBean("carMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");

		super.prepare();

		ShopToken shToken = currentUser.getShopRoot();
		if (shToken != null) {
			currentShop = shopMgr.getShopById(shToken.getShopId());
		}
	}

	/**
	 * @author vuongmq
	 * @since 25 - August, 2014
	 * 
	 * @desciption: load execute
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		if (currentUser != null) {
			currentUser = getCurrentUser();
		}

		now = DateUtil.getCurrentGMTDate();
		/*
		 * if(currentUser != null && currentUser.getUserName() != null){ staff =
		 * staffMgr.getStaffByCode(currentUser.getUserName()); if(staff != null
		 * && staff.getShop() != null){ shop = staff.getShop(); } Date dayLock =
		 * shopLockMgr.getNextLockedDay(shop.getId()); if(dayLock != null){ now
		 * = dayLock; } }
		 */
		if (currentUser != null && currentUser.getShopRoot().getShopId() != null) {
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			Date dayLock = shopLockMgr.getNextLockedDay(shop.getId());
			if (dayLock != null) {
				now = dayLock;
			}
		}

		try {
			if (shop != null) {
				ObjectVO<Car> object = carMgr.getListCar(null, shop.getId(), null, null, null, null, null, null, ActiveType.RUNNING, null);
				listCar = object.getLstObject();
				StaffFilter staffFilter = new StaffFilter();
				staffFilter.setStatus(ActiveType.RUNNING);
				staffFilter.setShopCode(shop.getShopCode());
				staffFilter.setStaffType(StaffObjectType.NVGH);
				staffFilter.setIsGetShopOnly(false);
				ObjectVO<Staff> objectStaff = staffMgr.getListStaff(staffFilter);
				listStaff = objectStaff.getLstObject();

				/** //vuongmq; 04/06/2015; tim kiem theo Gia tri DH */
				ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getId(), CONFIRM_ORDER_SEARCH);
				if (numberValueOrderParam != null) {
					numberValue = numberValueOrderParam.getCode();
				} else {
					numberValue = "0";
				}

			}
			/*
			 * lay NVBH theo phan quyen
			 */
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				filter.setIsLstChildStaffRoot(true);
			} else {
				filter.setIsLstChildStaffRoot(false);
			}
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setLstShopId(Arrays.asList(currentUser.getShopRoot().getShopId()));
			//Lay NVBH
			filter.setLstObjectType(Arrays.asList(StaffObjectType.NVBH.getValue(), StaffObjectType.NVVS.getValue()));
			ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVBHVo = objVO.getLstObject();

			//Lay nhan vien thu tien & NVGH
			filter.setIsLstChildStaffRoot(false);
			filter.setLstObjectType(Arrays.asList(StaffObjectType.NVGH.getValue(), StaffObjectType.NVTT.getValue()));
			objVO = staffMgr.searchListStaffVOByFilter(filter);
			lstNVTTVo = objVO.getLstObject();
			lstNVGHVo = objVO.getLstObject();

			//Load list do uu tien
			priority = apParamMgr.getListApParam(ApParamType.ORDER_PIRITY, ActiveType.RUNNING);
			if (priority == null) {
				priority = new ArrayList<ApParam>();
			} else {
				ApParam optionAll = new ApParam();
				optionAll.setValue(Constant.VALUE_ALL);
				optionAll.setApParamName(R.getResource("baocao.khtt.header.tatca"));
				priority.add(0, optionAll);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.AssignTransferStaffAction.execute"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/***
	 * Load lst staff for comboboxs
	 * 
	 * @author trungtm6
	 * @since 23/07/2015
	 * @return
	 */
	public String lstStaffCbx() {
		result.put("lstCar", new ArrayList<Car>());
		result.put("lstSale", new ArrayList<StaffVO>());
		result.put("lstCashier", new ArrayList<StaffVO>());
		result.put("lstDeliver", new ArrayList<StaffVO>());
		result.put("lockDate", "");
		try {
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					Date lockD = shopLockMgr.getNextLockedDay(shop.getId());
					if (lockD == null) {
						lockD = commonMgr.getSysDate();
					}
					lockDate = DateUtil.convertDateByString(lockD, DateUtil.DATE_FORMAT_DDMMYYYY);

					ObjectVO<Car> object = carMgr.getListCar(null, shop.getId(), null, null, null, null, null, null, ActiveType.RUNNING, null);
					listCar = object.getLstObject();

					StaffFilter filter = new StaffFilter();
					filter.setShopId(shop.getId());
					filter.setStatus(ActiveType.RUNNING);
					filter.setShopRootId(shop.getId());
					filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
					filter.setRoleId(currentUser.getRoleToken().getRoleId());
					filter.setSpecType(StaffSpecificType.STAFF);

					//Lay NVBH
					ObjectVO<StaffVO> objVO = staffMgr.getListStaffVO(filter);
					lstNVBHVo = objVO.getLstObject();

					//Lay nhan vien thu tien 
					filter.setSpecType(StaffSpecificType.NVTT);
					ObjectVO<StaffVO> objVOCash = staffMgr.getListStaffVO(filter);
					lstNVTTVo = objVOCash.getLstObject();

					//Lay nhan vien NVGH
					filter.setSpecType(StaffSpecificType.NVGH);
					ObjectVO<StaffVO> objVODeliver = staffMgr.getListStaffVO(filter);
					lstNVGHVo = objVODeliver.getLstObject();

					if (listCar != null) {
						result.put("lstCar", listCar);
					}
					if (lstNVBHVo != null) {
						result.put("lstSale", lstNVBHVo);
					}
					if (lstNVTTVo != null) {
						result.put("lstCashier", lstNVTTVo);
					}
					if (lstNVGHVo != null) {
						result.put("lstDeliver", lstNVGHVo);
					}
					result.put("lockDate", lockDate);

				}
			}
		} catch (Exception e) {
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.AssignTransferStaffAction.lstStaffCbx"), createLogErrorStandard(actionStartTime));
		}

		return JSON;
	}

	/**
	 * Search assign transfer staff.
	 * 
	 * @return the string
	 * @author hungtt
	 * @since Sep 26, 2012
	 * 
	 *        Search assign transfer staff.
	 * 
	 * @return the string
	 * @author vuongmq
	 * @since August 25, 2014
	 * @description: lay danh sach don hang de chi dinh nhan vien giao hang
	 * 
	 *               Bo sung dieu kien tim kiem va phan trang du lieu
	 * 
	 * @author hunglm16
	 * @since December 10,2014
	 */
	public String searchAssignTransferStaff() {
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<SaleOrder>());
			/*
			 * Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
			 * shopId = staff.getShop().getId();
			 */
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (!StringUtil.isNullOrEmpty(shopCode)) {//shop from combobox
				shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					shopId = shop.getId();
				} else {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.not.exists.npp"));
					return JSON;
				}
			} else if (currentUser.getShopRoot().getShopId() != null) {
				shopId = currentUser.getShopRoot().getShopId();
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			Date sDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(startDate)) {
				sDate = ths.dms.web.utils.DateUtil.parse(startDate, ConstantManager.FULL_DATE_FORMAT);
			} else {
				Date lockDate = shopLockMgr.getNextLockedDay(shopId);
				if (lockDate == null) {
					lockDate = commonMgr.getSysDate();
				}
				tDate = lockDate;
			}
			/** //vuongmq; 04/06/2015; tim kiem theo Gia tri DH */
			checkCreateShopParamOrderSearch(numberValueOrder, shopId, CONFIRM_ORDER_SEARCH);
			SoFilter filter = new SoFilter();
			/*
			 * // khong su dung SaleOrderSource soSource=null;
			 * if(orderSource!=null && orderSource!=0){
			 * soSource=SaleOrderSource.parseValue(orderSource);
			 * filter.setOrderSource(soSource); }
			 */
			filter.setStrListUserId(super.getStrListUserId());
			filter.setShopId(shopId);
			filter.setStaffCode(saleStaffCode);
			filter.setDeliveryCode(transferStaffCode);
			filter.setCashierCode(cashierStaffCode);
			filter.setCarId(carId);
			filter.setListOrderType(Arrays.asList(OrderType.IN));
			//			filter.setFromDate(sDate);
			filter.setOrderDate(sDate);
			filter.setToDate(tDate);
			filter.setSaleOrderType(SaleOrderType.NOT_YET_RETURNED); // chua bi tra
			filter.setOrderNumber(orderNumber);
			filter.setShortCode(customerCode);
			filter.setApprovedAndStep1(true);
			/*
			 * filter.setApproval(SaleOrderStatus.NOT_YET_APPROVE); // chua
			 * duyet filter.setApprovedStep(SaleOrderStep.CONFIRMED);
			 */// don đã xác nhận
			if (!StringUtil.isNullOrEmpty(priorityCode) && !Constant.VALUE_ALL.equals(priorityCode)) {
				filter.setPriorityCode(priorityCode);
			}
			filter.setValueOrder(isValueOrder); //So tien co gia tri, khong co gia tri
			/** //vuongmq; 04/06/2015; tim kiem theo Gia tri DH */
			filter.setNumberValueOrder(numberValueOrder);
			//filter.setIsPrint(false); // chua in thi hien thi

			if (orderTypes != null && orderTypes.size() > 0) {
				while (orderTypes.size() > 0 && orderTypes.get(0) == null) {
					orderTypes.remove(0);
				}
				filter.setListOrderType(orderTypes);
			}

			filter.setExactStaffSearching(true);

			ObjectVO<SaleOrder> vos = saleOrderMgr.getListSaleOrderForDelivery(kPaging, filter);
			if (vos != null) {
				/*
				 * bo sung dia chi phuong, quan
				 */
				/*
				 * for (SaleOrder saleOrder : vos.getLstObject()) { Customer
				 * customer = saleOrder.getCustomer();
				 * customer.setAddress(getCustomerFullAddress(customer)); }
				 */

				result.put("rows", vos.getLstObject());
				result.put("total", vos.getkPaging().getTotalRows());
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.AssignTransferStaffAction.searchAssignTransferStaff"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Update assign transfer.
	 * 
	 * @return the string
	 * @author hungtt
	 * @since Sep 27, 2012
	 */
	/**
	 * Search assign transfer staff.
	 * 
	 * @return the string
	 * @author vuongmq
	 * @since August 25, 2014
	 * @description: chon don hang cap nhat nvgh
	 */
	public String updateAssignTransfer() {
		try {
			if (StringUtil.isNullOrEmpty(shopCode)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			shop = shopMgr.getShopByCode(shopCode);
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			/*
			 * check privilege
			 */
			Staff deliveryStaff = deliveryId != null ? staffMgr.getStaffById(deliveryId) : null;
			Staff cashierStaff = cashierId != null ? staffMgr.getStaffById(cashierId) : null;

			/*
			 * check cashier, delivery-staff is managed by login staff
			 */
			/*
			 * if ((cashierStaff != null &&
			 * !checkShopInOrgAccessById(cashierStaff.getShop().getId())) ||
			 * (deliveryStaff != null &&
			 * !checkShopInOrgAccessById(deliveryStaff.getShop().getId()))) {
			 * result.put(ERROR, true); result.put("errMsg",
			 * ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,
			 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
			 * "common.cms.staff.undefined"))); return JSON; }
			 */

			//trungtm6 comment
			if (shop != null && shop.getId() != null) {
				if ((cashierStaff != null && cashierStaff.getShop() != null && !cashierStaff.getShop().getId().equals(shop.getId()))
						|| (deliveryStaff != null && deliveryStaff.getShop() != null && !deliveryStaff.getShop().getId().equals(shop.getId()))) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.staff.undefined")));
					return JSON;
				}
			}

			saleOrderMgr.updateSaleOrderForDelivery(listSaleOrderId, deliveryId, cashierId, carId);
			result.put("message", true);

		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.AssignTransferStaffAction.updateAssignTransfer"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String importExcelFile() throws Exception {
		isError = true;
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
		typeView = false;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 10);//List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 2);
		Shop curShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
		String lstShop = this.getStrListShopId();
		/*
		 * if(session.getAttribute(ConstantManager.SESSION_SHOP)!= null){
		 * curShop = (Shop)session.getAttribute(ConstantManager.SESSION_SHOP); }
		 */
		if (StringUtil.isNullOrEmpty(errMsg) && curShop == null) {
			errMsg = "Lỗi không tồn tại shop đăng nhập";
		}
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						message = "";
						totalItem++;
						List<String> row = lstData.get(i);
						SaleOrder saleOrder = new SaleOrder();
						Staff deliveryStaff = new Staff();
						Staff cashierStaff = new Staff();
						Car car = new Car();

						// NVGH
						deliveryStaff = null;
						if (row.size() > 5) {
							String value = row.get(5);
							if (!StringUtil.isNullOrEmpty(value)) {
								deliveryStaff = staffMgr.getStaffByCodeAndListShop(value, lstShop);
								if (deliveryStaff == null) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.delivery"));
									message += "\n";
								} else {
									/*
									 * if(deliveryStaff.getStaffType() != null
									 * &&
									 * !deliveryStaff.getStaffType().getObjectType
									 * (
									 * ).equals(StaffObjectType.NVGH.getValue())
									 * ){
									 */
									// check Dieu kien NVGH va NVTT dieu duoc, neu khac NVGH va NVTT thi bat loi 
									if (deliveryStaff.getStaffType() != null && !deliveryStaff.getStaffType().getSpecificType().equals(StaffSpecificType.NVGH) && !deliveryStaff.getStaffType().getSpecificType().equals(StaffSpecificType.NVTT)) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.not.staff.type", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.delivery"));
										message += "\n";
									}
									/*
									 * check privilege: is delivery-staff
									 * managed by login staff?
									 */
									/*
									 * if
									 * (!checkShopInOrgAccessById(deliveryStaff
									 * .getShop().getId())) { message +=
									 * Configuration
									 * .getResourceString(ConstantManager
									 * .VI_LANGUAGE,
									 * "common.cms.staff.undefined"); message +=
									 * "\n"; }
									 */
								}

							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.delivery"));
								message += "\n";
							}
						}

						// NVTT
						cashierStaff = null;
						if (row.size() > 6) {
							String value = row.get(6);
							if (!StringUtil.isNullOrEmpty(value)) {
								cashierStaff = staffMgr.getStaffByCodeAndListShop(value, lstShop);
								if (cashierStaff == null) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.cashier"));
									message += "\n";
								} else {
									/*
									 * if(cashierStaff.getStaffType() != null &&
									 * !
									 * cashierStaff.getStaffType().getObjectType
									 * (
									 * ).equals(StaffObjectType.NVTT.getValue())
									 * ){
									 */
									if (cashierStaff.getStaffType() != null && !cashierStaff.getStaffType().getSpecificType().equals(StaffSpecificType.NVTT) && !cashierStaff.getStaffType().getSpecificType().equals(StaffSpecificType.NVGH)) {
										// check Dieu kien NVGH va NVTT dieu duoc, neu khac NVGH va NVTT thi bap loi
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.not.staff.type", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.cashier"));
										message += "\n";
									}
									/*
									 * check privilege: is cashier managed by
									 * login staff?
									 */
									/*
									 * if
									 * (!checkShopInOrgAccessById(cashierStaff
									 * .getShop().getId())) { message +=
									 * Configuration
									 * .getResourceString(ConstantManager
									 * .VI_LANGUAGE,
									 * "common.cms.staff.undefined"); message +=
									 * "\n"; }
									 */
								}

							}
						}

						// Xe GH
						car = null;
						if (row.size() > 7) {
							String value = row.get(7);
							if (!StringUtil.isNullOrEmpty(value)) {
								car = carMgr.getCarByNumberAndListShop(value.toUpperCase(), lstShop);
								if (car == null) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.car"));
									message += "\n";
								}
							}
						}

						// So hoa don
						if (row.size() > 8) {
							String value = row.get(8);
							if (!StringUtil.isNullOrEmpty(value)) {
								saleOrder = saleOrderMgr.getSaleOrderByOrderNumberAndListShop(value.toUpperCase(), lstShop);
								if (saleOrder != null) {
									if (deliveryStaff != null) {
										if (!saleOrder.getShop().getId().equals(deliveryStaff.getShop().getId())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.difference.delivery.staff");
											message += "\n";
										}
									}
									if (cashierStaff != null) {
										if (!saleOrder.getShop().getId().equals(cashierStaff.getShop().getId())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.difference.cashier.staff");
											message += "\n";
										}
									}
									if (car != null) {
										if (!saleOrder.getShop().getId().equals(car.getShop().getId())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.difference.car");
											message += "\n";
										}
									}
									/*
									 * if(!saleOrder.getApproved().equals(
									 * SaleOrderStatus.APPROVED)){ message +=
									 * Configuration
									 * .getResourceString(ConstantManager
									 * .VI_LANGUAGE
									 * ,"comon.not.approved",Configuration
									 * .getResourceString
									 * (ConstantManager.VI_LANGUAGE
									 * ,"sale.product.sale.order.number"));
									 * message += "\n"; }
									 */
									//									if (!saleOrder.getApproved().equals(SaleOrderStatus.NOT_YET_APPROVE)) { //don hang chua duyet
									//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.must.not.approved", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.number"));
									//										message += "\n";
									//									}
									/*
									 * if(!saleOrder.getOrderType().equals(OrderType
									 * .IN) &&
									 * !saleOrder.getOrderType().equals(OrderType
									 * .TT)){ message +=
									 * Configuration.getResourceString
									 * (ConstantManager.VI_LANGUAGE,
									 * "comon.not.in.order.type.in.or.tt"
									 * ,Configuration
									 * .getResourceString(ConstantManager
									 * .VI_LANGUAGE
									 * ,"sale.product.sale.order.number"));
									 * message += "\n"; }
									 */
									//									if (!saleOrder.getOrderType().equals(OrderType.IN)) {
									//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.not.in.order.type.in.or.tt", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.number"));
									//										message += "\n";
									//									}
									//									if (!saleOrder.getType().equals(SaleOrderType.NOT_YET_RETURNED)) { // don hang chua bi tra
									//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.returned", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.number"));
									//										message += "\n";
									//									}
									//									if (!saleOrder.getApprovedStep().equals(SaleOrderStep.CONFIRMED)) {
									//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.approved.step.confirm", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.number"));
									//										message += "\n";
									//									}
								} else {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "comon.not.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.number"));
									message += "\n";
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.sale.order.number"));
								message += "\n";
							}
						}

						if (isView == 0) {
							if (StringUtil.isNullOrEmpty(message)) {
								saleOrder.setDelivery(deliveryStaff);
								if (cashierStaff != null && cashierStaff.getId() != null) {
									saleOrder.setCashier(cashierStaff);
								}
								if (car != null && car.getId() != null) {
									saleOrder.setCar(car);
								}
								if (currentUser != null) {
									saleOrder.setUpdateUser(currentUser.getUserName());
								}
								saleOrderMgr.updateSaleOrder(saleOrder);
							} else {
								message = message.substring(0, message.length() - 1);
								lstFails.add(StringUtil.addFailBean(row, message));
							}
						} else {
							message = StringUtil.convertHTMLBreakLine(message);
							lstView.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				// Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_SALEPRODUCT);

			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.AssignTransferStaffAction.importExcelFile"), createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	public String exportExcelFile() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			if (listSaleOrderId == null || listSaleOrderId.size() == 0) {
				return JSON;
			}

			List<SaleOrder> listSaleOrder = saleOrderMgr.getListSaleOrderById(listSaleOrderId);

			listBeans = new ArrayList<Map<String, Object>>();

			for (int i = 0; i < listSaleOrder.size(); i++) {
				SaleOrder item = listSaleOrder.get(i);
				Map<String, Object> bean = new HashMap<String, Object>();
				bean.put("invoiceNumber", item.getOrderNumber());
				if (item.getDelivery() != null) {
					bean.put("staffCode", item.getDelivery().getStaffCode());
				} else {
					bean.put("staffCode", "");
				}
				bean.put("cashierCode", item.getCashier() != null ? item.getCashier().getStaffCode() : "");
				bean.put("carNumber", item.getCar() != null ? item.getCar().getCarNumber() : "");
				bean.put("saleStaffCode", item.getStaff().getStaffCode());
				bean.put("saleStaffName", item.getStaff().getStaffName());
				//				bean.put("customerCode", item.getCustomer().getShortCode() + " - " + item.getCustomer().getCustomerName());
				bean.put("customerCode", item.getCustomer().getShortCode());
				bean.put("customerName", item.getCustomer().getCustomerName());
				bean.put("customerAddress", item.getCustomer().getAddress());
				bean.put("amount", item.getTotal());
				listBeans.add(bean);
			}

			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("listBeans", listBeans);

			if (listBeans != null && listBeans.size() > 0) {
				String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathSaleProduct(); //Configuration.getStoreRealPath();
				String templateFileName = folder + ConstantManager.TEMPLATE_EXPORT_SALEPRODUCT;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = ConstantManager.EXPORT_SALEPRODUCT + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);

				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();

				result.put(ERROR, false);
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(LIST, outputPath);
				result.put(REPORT_PATH, outputPath);
				result.put("hasData", true);

				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, false);
				result.put("hasData", false);
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.AssignTransferStaffAction.exportExcelFile"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	//TODO Xu ly GETTER/SETER
	/**
	 * Phan doan code
	 * */
	public Integer isValueOrder;

	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}

	public CustomerMgr getCustomerMgr() {
		return customerMgr;
	}

	public void setCustomerMgr(CustomerMgr customerMgr) {
		this.customerMgr = customerMgr;
	}

	public ChannelTypeMgr getChannelTypeMgr() {
		return channelTypeMgr;
	}

	public void setChannelTypeMgr(ChannelTypeMgr channelTypeMgr) {
		this.channelTypeMgr = channelTypeMgr;
	}

	public SaleOrderMgr getSaleOrderMgr() {
		return saleOrderMgr;
	}

	public void setSaleOrderMgr(SaleOrderMgr saleOrderMgr) {
		this.saleOrderMgr = saleOrderMgr;
	}

	public CarMgr getCarMgr() {
		return carMgr;
	}

	public void setCarMgr(CarMgr carMgr) {
		this.carMgr = carMgr;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getTransferStaffCode() {
		return transferStaffCode;
	}

	public void setTransferStaffCode(String transferStaffCode) {
		this.transferStaffCode = transferStaffCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getPriorityCode() {
		return priorityCode;
	}

	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public List<SaleOrder> getListSaleOrder() {
		return listSaleOrder;
	}

	public void setListSaleOrder(List<SaleOrder> listSaleOrder) {
		this.listSaleOrder = listSaleOrder;
	}

	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}

	public String getCashierStaffCode() {
		return cashierStaffCode;
	}

	public void setCashierStaffCode(String cashierStaffCode) {
		this.cashierStaffCode = cashierStaffCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Float getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(Float totalWeight) {
		this.totalWeight = totalWeight;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public List<ApParam> getPriority() {
		return priority;
	}

	public void setPriority(List<ApParam> priority) {
		this.priority = priority;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public List<Car> getListCar() {
		return listCar;
	}

	public void setListCar(List<Car> listCar) {
		this.listCar = listCar;
	}

	public List<Staff> getListStaff() {
		return listStaff;
	}

	public void setListStaff(List<Staff> listStaff) {
		this.listStaff = listStaff;
	}

	public List<Long> getListSaleOrderId() {
		return listSaleOrderId;
	}

	public void setListSaleOrderId(List<Long> listSaleOrderId) {
		this.listSaleOrderId = listSaleOrderId;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Date getNow() {
		return now;
	}

	public void setNow(Date now) {
		this.now = now;
	}

	public List<Staff> getListNVBH() {
		return listNVBH;
	}

	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}

	public List<Staff> getListNVGH() {
		return listNVGH;
	}

	public void setListNVGH(List<Staff> listNVGH) {
		this.listNVGH = listNVGH;
	}

	public List<Staff> getListNVTT() {
		return listNVTT;
	}

	public void setListNVTT(List<Staff> listNVTT) {
		this.listNVTT = listNVTT;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<Map<String, Object>> getListBeans() {
		return listBeans;
	}

	public void setListBeans(List<Map<String, Object>> listBeans) {
		this.listBeans = listBeans;
	}

	public Long getCashierId() {
		return cashierId;
	}

	public void setCashierId(Long cashierId) {
		this.cashierId = cashierId;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Integer getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(Integer orderSource) {
		this.orderSource = orderSource;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public List<StaffVO> getLstNVTTVo() {
		return lstNVTTVo;
	}

	public void setLstNVTTVo(List<StaffVO> lstNVTTVo) {
		this.lstNVTTVo = lstNVTTVo;
	}

	public String getNumberValue() {
		return numberValue;
	}

	public void setNumberValue(String numberValue) {
		this.numberValue = numberValue;
	}

	public BigDecimal getNumberValueOrder() {
		return numberValueOrder;
	}

	public void setNumberValueOrder(BigDecimal numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}

	public List<OrderType> getOrderTypes() {
		return orderTypes;
	}

	public void setOrderTypes(List<OrderType> orderTypes) {
		this.orderTypes = orderTypes;
	}

}

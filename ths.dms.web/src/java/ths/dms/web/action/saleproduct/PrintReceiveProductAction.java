package ths.dms.web.action.saleproduct;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder.In;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;

import viettel.passport.client.ShopToken;
import viettel.passport.client.UserToken;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.ReportManagerMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO1;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO2;
import ths.dms.core.entities.vo.PrintDeliveryGroupVO;
import ths.dms.core.entities.vo.PrintDeliveryGroupVO1;
import ths.dms.core.entities.vo.StaffVO;
import ths.core.entities.vo.rpt.RptDeliveryAndPaymentOfStaffVO;
import ths.core.entities.vo.rpt.RptExSaleOrder2VO;
import ths.core.entities.vo.rpt.RptPGHVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_DataConvert;
import ths.core.entities.vo.rpt.RptPXKKVCNB_ProductVansale;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.core.entities.vo.rpt.RptSaleOrderLotVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;
//import ths.dms.core.memcached.MemcachedUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class PrintReceiveProductAction.
 */
public class PrintReceiveProductAction extends AbstractAction {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The sale staff code. */
	private String saleStaffCode;
	
	/** The delivery staff code. */
	private String deliveryStaffCode;
	
	/** The list status. */
	private List<StatusBean> listStatus;
	
	/** The choose statuse. */
	private Integer chooseStatuse;
	
	/** The shop. */
	private Shop shop;
	
	/** The create date from. */
	private String createDateFrom;
	
	/** The create date to. */
	private String createDateTo;
	
	/** The list sale order. */
	private List<SaleOrder> listSaleOrder;
	
	/** The list sale order id. */
	private List<Long> listSaleOrderId;
	
	/** The is print. */
	private Boolean isPrint;
	
	private Integer placeCreaterOn;
	
	/** The list beans. */
	private List<Map<String, Object>> listBeans;
	
	/** The sale order mgr. */
	private SaleOrderMgr saleOrderMgr;

	
	/** The promotion program mgr. */
//	private PromotionProgramMgr promotionProgramMgr;
	
	private ReportManagerMgr reportManagerMgr;
	
	/** The promotion program mgr. */
//	private DisplayProgrameMgr displayProgrameMgr;
	
	/** The parameters report. */
	private HashMap<String, Object> parametersReport;
	/** The list nvbh. */
	private List<Staff> listNVBH;
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH
	
	/** The list nvgh. */
	private List<Staff> listNVGH;
	private List<StaffVO> lstNVGHVo;//Danh sach NVGH
	
	private Integer typePrint;
	
	private Integer valueOrder;
	
	private Integer orderType; //1: NVGH, 2:NVBH
	
	private List<OrderType> orderTypes;
	
	private String printPageSize;

	private Integer printBatch;
	
	private String printTime;
	
	private String priorityCode;
	
	List<RptExSaleOrder2VO> lstXHNVBHData;
	
	List<RptPGHVO> viewGopOrder;
	
	private String shortCode;
	
	private Date now;
	
	private String name;
	private String content;
	
	private int fontSz;
	
	private Integer printVATStatus;//Trang thai in VA
	private BigDecimal orderValue;
	private Integer orderValueCompareType;
	
	/**
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		listStatus = new ArrayList<StatusBean>();
		StatusBean approved = new StatusBean((long)ApprovalStatus.APPROVED.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.approved"));
		StatusBean notApproved = new StatusBean((long)ApprovalStatus.NOT_YET.getValue(), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.not.approved"));
		listStatus.add(approved);
		listStatus.add(notApproved);
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
//		promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
//		displayProgrameMgr = (DisplayProgrameMgr) context.getBean("displayProgrameMgr");
		reportManagerMgr = (ReportManagerMgr) context.getBean("reportManagerMgr");
		
		request = ServletActionContext.getRequest();		
		if(request.getSession().getAttribute("cmsUserToken")!= null){
		    currentUser = (UserToken)request.getSession().getAttribute("cmsUserToken");
		}
		if(currentUser != null && currentUser.getUserName() != null){
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			if(staff != null && staff.getShop() != null){
				shop = staff.getShop();
			}
		}
		now = DateUtil.getCurrentGMTDate();
		Date dayLock = shopLockMgr.getApplicationDate(currentUser.getShopRoot().getShopId());
		if(dayLock != null){
			now = dayLock;
		}
	}

	/**
	 * @author tientv
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		Staff staff = null;
		createDateFrom = displayDate(now);
		createDateTo = displayDate(now);
		
		staff = getStaffByCurrentUser();
		if(staff==null){
			return PAGE_NOT_PERMISSION;
		}
		//shop = staff.getShop();
		//Lay danh sach nhan vien theo phan quyen
		/*StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
		if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
			filter.setIsLstChildStaffRoot(true);
		} else {
			filter.setIsLstChildStaffRoot(false);
		}
		filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
		filter.setStatus(ActiveType.RUNNING.getValue());
		filter.setLstShopId(Arrays.asList(currentUser.getShopRoot().getShopId()));
		//Lay NVBH
		filter.setLstObjectType(Arrays.asList(StaffObjectType.NVBH.getValue(), StaffObjectType.NVVS.getValue()));
		ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
		setLstNVBHVo(objVO.getLstObject());

		//Lay nhan vien thu tien & NVGH
		filter.setIsLstChildStaffRoot(false);
		filter.setLstObjectType(Arrays.asList(StaffObjectType.NVGH.getValue(), StaffObjectType.NVTT.getValue()));
		objVO = staffMgr.searchListStaffVOByFilter(filter);
		setLstNVGHVo(objVO.getLstObject());*/
		
		shopCode = currentUser.getShopRoot().getShopCode();
		
		ShopToken shop = currentUser.getShopRoot();
		if (shop == null) {
			return ERROR;
		}
		
		ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getShopId(), PRINT_DELIVERY_ORDER);
		if (numberValueOrderParam != null) {
			orderValue = new BigDecimal(numberValueOrderParam.getCode());
		} else {
			orderValue = BigDecimal.ZERO;
		}
		return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 */
	public String search() throws Exception {
		try {
			result.put("page", page);
		    result.put("max", max);

			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			
			
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			
			Date fromDate = DateUtil.parse(createDateFrom, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(createDateTo, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date printDate = DateUtil.parse(printTime, DateUtil.DATE_FORMAT_DDMMYYYY);			
			SoFilter filter = new SoFilter();
			filter.setStrListUserId(super.getStrListUserId());
			filter.setShopId(shop.getId());
			filter.setStaffCode(saleStaffCode);
			filter.setDeliveryCode(deliveryStaffCode);
			filter.setListOrderType(Arrays.asList(OrderType.IN, OrderType.TT));
			filter.setFromDate(fromDate);
			filter.setToDate(toDate);
			filter.setSaleOrderType(SaleOrderType.NOT_YET_RETURNED);
			filter.setIsPrint(isPrint);
			//filter.setIsValueOrder(isValueOrder);
			filter.setValueOrder(valueOrder);
			filter.setPrintTime(printDate);
			filter.setShortCode(shortCode);
			filter.setPrintBatch(printBatch);
			filter.setApproval(SaleOrderStatus.APPROVED);
			filter.setPriorityCode(priorityCode);
			if(placeCreaterOn != null && SaleOrderSource.parseValue(placeCreaterOn) != null){
				filter.setOrderSource(SaleOrderSource.parseValue(placeCreaterOn));
			}
			//filter.setPrintVATStatus(printVATStatus);
			
			if (orderTypes != null && orderTypes.size() > 0) {
				while (orderTypes.size() > 0 && orderTypes.get(0) == null) {
					orderTypes.remove(0);
				}
				filter.setListOrderType(orderTypes);
			}
			
			filter.setNumberValueOrder(orderValue);
			filter.setIsValueOrder(orderValueCompareType);
			
			ObjectVO<SaleOrder> vos  = saleOrderMgr.getListSaleOrderForDelivery(kPaging, filter);
			if(vos.getLstObject().size()>0) {
				result.put("total", vos.getkPaging().getTotalRows());
			    result.put("rows", vos.getLstObject());
			}else {
				result.put("total", 0);
				result.put("rows", new ArrayList<SaleOrder>());
			}
			
//			saveSearchCondition(orderValue, shT.getShopId());
			saveSearchCondition(orderValue, shop.getId());
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.search"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * Luu thong tin tim kiem
	 * @author tuannd20 
	 * @param orderValue
	 * @param shopId
	 * @since 30/06/2015
	 */
	private void saveSearchCondition(BigDecimal orderValue, Long shopId) {
		try {
			if (orderValue != null && shopId != null) {
				ShopParam orderValueParam = commonMgr.getShopParamByType(shopId, PRINT_DELIVERY_ORDER);
				if (orderValueParam != null && ActiveType.RUNNING.equals(orderValueParam.getStatus())) {
					orderValueParam.setCode(orderValue.toString());
					orderValueParam.setName(orderValue.toString());
					orderValueParam.setUpdateUser(currentUser.getUserName());
					//saleOrderMgr.updateShopParamByInvoice(orderValueParam);
					commonMgr.updateEntity(orderValueParam);
				} else if (orderValueParam == null) {
					Shop shop = shopMgr.getShopById(shopId);
					orderValueParam = new ShopParam();
					orderValueParam.setShop(shop);
					orderValueParam.setType(PRINT_DELIVERY_ORDER);
					orderValueParam.setStatus(ActiveType.RUNNING);
					orderValueParam.setCode(orderValue.toString());
					orderValueParam.setName(orderValue.toString());
					orderValueParam.setCreateUser(currentUser.getUserName());
					//saleOrderMgr.createShopParamByInvoice(orderValueParam);
					commonMgr.createEntity(orderValueParam);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.saveSearchCondition"), createLogErrorStandard(actionStartTime));
		}
	}

	/**
	 * Prints the order with type.
	 *
	 * @return String
	 * @author tientv
	 * @description : In phieu giao hang theo loai duoc chon
	 * @sess: Theo nhan vien giao hang, khach hang,don hang
	 */
	
	private HashMap<String, Object> getParamsCommons() throws Exception {
		HashMap<String, Object> pramsCommons = new HashMap<String, Object>();
		pramsCommons.put("SUBREPORT_LOT", ShopReportTemplate.SALE_PRODUCT_INPHIEUGH_SR_LOT.getTemplatePath(false, FileExtension.JASPER));
		pramsCommons.put("SUBREPORT_CONVFACT", ShopReportTemplate.SALE_PRODUCT_INPHIEUGH_SR_CONVFACT.getTemplatePath(false, FileExtension.JASPER));		
		pramsCommons.put("SUBREPORT_UOM1", ShopReportTemplate.SALE_PRODUCT_INPHIEUGH_SR_UOM1.getTemplatePath(false, FileExtension.JASPER));
		pramsCommons.put("SUBREPORT_UOM2", ShopReportTemplate.SALE_PRODUCT_INPHIEUGH_SR_UOM2.getTemplatePath(false, FileExtension.JASPER));
		pramsCommons.put("SUBREPORT_PRICE", ShopReportTemplate.SALE_PRODUCT_INPHIEUGH_SR_PRICE.getTemplatePath(false, FileExtension.JASPER));
		pramsCommons.put("SUBREPORT_AMOUNT", ShopReportTemplate.SALE_PRODUCT_INPHIEUGH_SR_AMOUNT.getTemplatePath(false, FileExtension.JASPER));
//		pramsCommons.put("LOGO_PATH", ReportUtils.getVinamilkLogoRealPath(request));
		pramsCommons.put("PRINT_DATE", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_DDMMYYYY));			
		Staff staffSign = getStaffByCurrentUser();
		if(staffSign==null) {
			return null;
		}
		pramsCommons.put("SHOP_NAME", shop.getShopName().toUpperCase());
		pramsCommons.put("SHOP_ADDRESS", "");
		if(!StringUtil.isNullOrEmpty(shop.getAddress())){
			pramsCommons.put("SHOP_ADDRESS", shop.getAddress());
		}
		pramsCommons.put("PHONE_BOOK", StringUtil.CodeAddNameEx(shop.getPhone(),""));
		return pramsCommons;
	}
	
	public String printOrderWithType(){
		result.put(ERROR, true);
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			if (listSaleOrderId == null || listSaleOrderId.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.invoice.is.null"));
				return JSON;
			}
			SaleOrder so = saleOrderMgr.getSaleOrderById(listSaleOrderId.get(0));
			shop = so.getShop();
			
			/**Tham so giao dien */
			parametersReport = getParamsCommons();
			if (parametersReport == null) {
				return JSON;
			}
			lstXHNVBHData = new ArrayList<RptExSaleOrder2VO>();

			List<RptPGHVO> rptPGHVOs = null;
			parametersReport.put("TYPE_PRINT", typePrint);
			if (ConstantManager.PRINT_CUSTOMER == typePrint) {
				rptPGHVOs = saleOrderMgr.getPGHGroupByKH(listSaleOrderId, getStrListShopId());
			} else if (ConstantManager.PRINT_DELIVERY == typePrint) {
				rptPGHVOs = saleOrderMgr.getPGHGroupByNVGH(listSaleOrderId, getStrListShopId());
			} else if (ConstantManager.PRINT_ORDER == typePrint) {
				rptPGHVOs = saleOrderMgr.getPGHGroupByDH(listSaleOrderId, getStrListShopId());
			}
			if (rptPGHVOs != null && rptPGHVOs.size() > 0) {
				for (RptPGHVO rpt : rptPGHVOs) {
					rpt.setTotalString(StringUtil.convertMoneyToString(rpt.getTotal()));
					if (typePrint == ConstantManager.PRINT_CUSTOMER) {
//						StringBuilder otherInformation = new StringBuilder(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.receive.product.phone.number"));
//						otherInformation.append(StringUtil.CodeAddNameEx(rpt.getCustomer().getPhone(), rpt.getCustomer().getMobiphone()));
//						otherInformation.append(StringUtil.CodeAddNameEx(getCustomerFullAddress(rpt.getCustomer()), ""));
//						rpt.setOtherInformation(otherInformation.toString());
						if (rpt.getCustomer() != null && rpt.getCustomer().getPhone() != null) {
							rpt.setPhone(rpt.getCustomer().getPhone());
						} else if (rpt.getCustomer() != null && rpt.getCustomer().getMobiphone() != null) {
							rpt.setPhone( rpt.getCustomer().getMobiphone());
						}
						if (rpt.getCustomer() != null && rpt.getCustomer().getAddress() != null) {
							rpt.setAddress(getCustomerFullAddress(rpt.getCustomer()));
						}
						if (rpt.getCustomer() != null && rpt.getCustomer().getDeliver() != null) {
							rpt.setDeliveryStaffInfo(rpt.getCustomer().getDeliver().getStaffCode() + "-" + rpt.getCustomer().getDeliver().getStaffName());
						}
					}
				}
			}
			
			JRDataSource dataSource = new JRBeanCollectionDataSource(rptPGHVOs);
			String outputPath = ReportUtils.exportFromFormat(FileExtension.PDF, parametersReport, dataSource,ShopReportTemplate.SALE_PRODUCT_INPHIEUGH_NVGH);	
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			
			Date sysdate = commonMgr.getSysDate();
			List<SaleOrder> listUpdateSO = new ArrayList<SaleOrder>();
			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shop.getId(), sysdate);
			printBatch1++;
			
			for (Long saleOrderId : listSaleOrderId) {
				SaleOrder saleOrder = saleOrderMgr.getSaleOrderById(saleOrderId);
				if (saleOrder.getTimePrint() == null) {
					saleOrder.setTimePrint(sysdate);
					saleOrder.setPrintBatch(printBatch1);
				}
				listUpdateSO.add(saleOrder);
			}
			saleOrderMgr.updateListSaleOrder(listUpdateSO);
			return JSON;
			
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.printOrderWithType"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}	
	
	public String viewGop() {
		result.put(ERROR, true);
		try {
			if (listSaleOrderId == null || listSaleOrderId.size() == 0) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.tax.invoice.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			SaleOrder so = saleOrderMgr.getSaleOrderById(listSaleOrderId.get(0));
			shop = so.getShop();
			
			/** Tham so giao dien */
			parametersReport = getParamsCommons();
			if (parametersReport == null) {
				return SUCCESS;
			}
			lstXHNVBHData = new ArrayList<RptExSaleOrder2VO>();
			viewGopOrder = new ArrayList<RptPGHVO>();
			List<RptPGHVO> rptPGHVOs = null;
			if (typePrint == ConstantManager.PRINT_CUSTOMER) {
				rptPGHVOs = saleOrderMgr.getPGHGroupByKH(listSaleOrderId, getStrListShopId());
			} else if (typePrint == ConstantManager.PRINT_DELIVERY) {
				rptPGHVOs = saleOrderMgr.getPGHGroupByNVGH(listSaleOrderId, getStrListShopId());
			} else if (typePrint == ConstantManager.PRINT_ORDER) {
				rptPGHVOs = saleOrderMgr.getPGHGroupByDH(listSaleOrderId, getStrListShopId());
			}
			if (rptPGHVOs != null && rptPGHVOs.size() > 0) {
				for (RptPGHVO rpt : rptPGHVOs) {
					rpt.setTotalString(StringUtil.convertMoneyToString(rpt.getTotal()));
					if (typePrint == ConstantManager.PRINT_CUSTOMER) {
//						StringBuilder otherInformation = new StringBuilder(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "print.receive.product.phone.number"));
//						otherInformation.append(StringUtil.CodeAddNameEx(rpt.getCustomer().getPhone(), rpt.getCustomer().getMobiphone()));
//						otherInformation.append(StringUtil.CodeAddNameEx(getCustomerFullAddress(rpt.getCustomer()), ""));
//						rpt.setOtherInformation(otherInformation.toString());
						if (rpt.getCustomer().getPhone() != null) {
							rpt.setPhone(rpt.getCustomer().getPhone());
						} else if (rpt.getCustomer().getMobiphone() != null) {
							rpt.setPhone(rpt.getCustomer().getMobiphone());
						}
						if (rpt.getCustomer().getAddress() != null) {
							rpt.setAddress(getCustomerFullAddress(rpt.getCustomer()));
						}
						if (rpt.getCustomer().getDeliver() != null) {
							rpt.setDeliveryStaffInfo(rpt.getCustomer().getDeliver().getStaffCode() + "-" + rpt.getCustomer().getDeliver().getStaffName());
						}
					}
				}
			}
			viewGopOrder = rptPGHVOs;
			return SUCCESS;

		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.viewGop"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}
	
	
	public String updateAfterPrintWithType(){
		resetToken(result);
		try{
			
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return JSON;
			}
			Date sysdate = commonMgr.getSysDate();
			List<SaleOrder> listUpdateSO = new ArrayList<SaleOrder>();
			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shop.getId(), sysdate);
			printBatch1++;
			for(Long saleOrderId : listSaleOrderId) {
				SaleOrder saleOrder = saleOrderMgr.getSaleOrderById(saleOrderId);
				if(saleOrder.getTimePrint() == null){
					saleOrder.setTimePrint(sysdate);
					saleOrder.setPrintBatch(printBatch1);
				}

				listUpdateSO.add(saleOrder);
			}
			saleOrderMgr.updateListSaleOrder(listUpdateSO);
		}catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.updateAfterPrintWithType"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return JSON;
	}
	
	/**
	 * Prints the odd order.
	 *
	 * @return the string
	 */
	public String printOddOrder() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}

			listBeans = new ArrayList<Map<String,Object>>();
			List<SaleOrder> listUpdateSO = new ArrayList<SaleOrder>();
			List<SaleOrder> listData = saleOrderMgr.getListSaleOrderById(listSaleOrderId, orderType);
//			List<ProgramCodeVO> progamCodeVOs = saleOrderMgr.getListProgramCodeVO(listSaleOrderId);
			Map<String, Object> bean = null;
			Map<String, Object> __sod = null;
			SaleOrderDetail sod = null;
			List<SaleOrderDetail> listSaleOrderDetail = null;
			List<SaleOrderDetail> listSale = null;
			List<Map<String, Object>> listFree = null;
			BigDecimal discountAmount = null;
			BigDecimal notDiscountAmount = null;
			BigDecimal totalMoney = null;
			Integer sumThung = 0;
			Integer sumLe = 0;
			Map<String,Object> soFree = null;
			String stmp = null;
			String stmp1 = null;
			String stmp2 = null;
			Integer qtt = 0;
			Object otmp = null;
			int i, sz, j, szj;
			
//			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shT.getShopId(), commonMgr.getSysDate());
			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shop.getId(), commonMgr.getSysDate());
			printBatch1++;
			
			for(SaleOrder saleOrder : listData) {
				bean = new HashMap<String, Object>();
				
				bean.put("printDate", DateUtil.toDateString(DateUtil.getCurrentGMTDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				String invoiceNumber = "";				
				listSaleOrderDetail = saleOrderMgr.getListSaleOrderDetailBySaleOrderId(null, saleOrder.getId()).getLstObject();
				
				listSale = new ArrayList<SaleOrderDetail>();
				listFree = new ArrayList<Map<String,Object>>();
				
				discountAmount = BigDecimal.ZERO;
				notDiscountAmount = BigDecimal.ZERO;
				totalMoney = BigDecimal.ZERO;
				sumThung = 0;
				sumLe = 0;
				stmp = null;
				stmp1 = null;
				qtt = 0;
				otmp = null;
				
				for(i = 0, sz = listSaleOrderDetail.size(); i < sz; i++) {
					sod = listSaleOrderDetail.get(i);
					if(sod.getInvoice() != null) {
						invoiceNumber += invoiceNumber.equals("") ? invoiceNumber + sod.getInvoice().getInvoiceNumber() : (invoiceNumber.indexOf(sod.getInvoice().getInvoiceNumber()) == -1 ? ", " + invoiceNumber + sod.getInvoice().getInvoiceNumber() : "");
					}
					
					if(sod.getDiscountAmount() != null && sod.getDiscountAmount().compareTo(BigDecimal.ZERO) > 0) {
						discountAmount = discountAmount.add(sod.getDiscountAmount());
					}
					
					if(sod.getIsFreeItem() == 0) {
						listSale.add(sod);
						notDiscountAmount = notDiscountAmount.add(sod.getAmount());
						sumThung = sumThung + (sod.getQuantity() / sod.getProduct().getConvfact());
						sumLe = sumLe + (sod.getQuantity() % sod.getProduct().getConvfact());
					}
					//TUNGTT
					//} else if(!ProgramType.DESTROY.equals(sod.getProgramType()) && !ProgramType.RETURN.equals(sod.getProgramType()) && !ProgramType.PRODUCT_EXCHANGE.equals(sod.getProgramType()) && sod.getQuantity() != null && sod.getQuantity() > 0) {
					else if (sod.getQuantity() != null && sod.getQuantity() > 0) {	
//						for (int k = 0, n = progamCodeVOs.size(); k < n; k++) {
//							ProgramCodeVO programCode = progamCodeVOs.get(k);
//							if (programCode.getSaleOrderId().equals(saleOrder.getId()) && programCode.getProductId().equals(sod.getProduct().getId())) {
//								sod.setProgramCode(programCode.getStrProgramCode());
//								break;
//							}
//						}						
						__sod = null;
						for (j = 0, szj = listFree.size(); j < szj; j++) {
							soFree = listFree.get(j);
							stmp = sod.getProgramCode();
							stmp1 = null;
							stmp2 = null;
							if (soFree.get("programCode") != null) {
								stmp1 = soFree.get("programCode").toString().trim();
							}
							if (soFree.get("product") != null) {
								stmp2 = ((Product)soFree.get("product")).getProductCode();
							}
							if (stmp1 != null && !StringUtil.isNullOrEmpty(stmp) && stmp1.equalsIgnoreCase(stmp.trim())
									&& stmp2 != null && sod.getProduct() != null
									&& stmp2.trim().equalsIgnoreCase(sod.getProduct().getProductCode().trim())) {
								__sod = soFree;
							}
						}
						
						if (__sod == null) {
							__sod = new HashMap<String, Object>();
							__sod.put("amount", sod.getAmount());
							__sod.put("createDate", sod.getCreateDate());
							__sod.put("createUser", sod.getCreateUser());
							__sod.put("discountAmount", sod.getDiscountAmount());
							__sod.put("discountPercent", sod.getDiscountPercent());
							__sod.put("isFreeItem", sod.getIsFreeItem());
							__sod.put("orderDate", sod.getOrderDate());
							__sod.put("priceValue", sod.getPriceValue());
							__sod.put("price", sod.getPrice());
							__sod.put("product", sod.getProduct());
							__sod.put("programCode", sod.getProgramCode());
							__sod.put("programType", sod.getProgramType());
							__sod.put("quantity", sod.getQuantity());
							__sod.put("id", sod.getId());
							__sod.put("saleOrder", sod.getSaleOrder());
							__sod.put("shop", sod.getShop());
							__sod.put("staff", sod.getStaff());
							__sod.put("totalWeight", sod.getTotalWeight());
							__sod.put("updateDate", sod.getUpdateDate());
							__sod.put("updateUser", sod.getUpdateUser());
							__sod.put("vat", sod.getVat());
							
							listFree.add(__sod);
						} else {
							otmp = __sod.get("quantity");
							if (otmp == null) {
								qtt = 0;
							} else {
								qtt = Integer.valueOf(otmp.toString());
							}
							qtt = qtt + sod.getQuantity();
							__sod.put("quantity", qtt);
						}
					}
				}
				
				bean.put("invoiceNumber", invoiceNumber);				
				totalMoney = notDiscountAmount.subtract(discountAmount);				
				bean.put("notDiscountAmount", convertMoney(notDiscountAmount));
				bean.put("discountAmount", convertMoney(discountAmount));
				bean.put("totalMoney", convertMoney(totalMoney));
				bean.put("moneyString", StringUtil.convertMoneyToString(totalMoney));				
				bean.put("saleOrder", saleOrder);
				String infoDeliveryStaff = "";
				if (saleOrder.getDelivery() != null) {
					Staff deliveryStaff = saleOrder.getDelivery();
					infoDeliveryStaff = deliveryStaff.getStaffName();
					if (deliveryStaff.getMobilephone() != null) {
						infoDeliveryStaff += "(" + deliveryStaff.getMobilephone() + ")";
					} else if (deliveryStaff.getPhone() != null) {
						infoDeliveryStaff += "(" + deliveryStaff.getPhone() + ")";
					}
				}
				bean.put("infoDeliveryStaff", infoDeliveryStaff);
				String infoStaff = "";
				if (saleOrder.getStaff() != null) {
					Staff staff = saleOrder.getStaff();
					infoStaff = staff.getStaffName();
					if (staff.getMobilephone() != null) {
						infoStaff += "(" + staff.getMobilephone() + ")";
					} else if (staff.getPhone() != null) {
						infoStaff += "(" + staff.getPhone() + ")";
					}
				}
				bean.put("infoStaff", infoStaff);
				if (saleOrder.getTimePrint() == null) {
					saleOrder.setTimePrint(commonMgr.getSysDate());
					saleOrder.setPrintBatch(printBatch1);
				}
				
				// set customer address to full address
				Customer customer = saleOrder.getCustomer();
				customer.setAddress(getCustomerFullAddress(customer));
				listUpdateSO.add(saleOrder);				
				bean.put("listSale", listSale);				
				bean.put("listFree", listFree);
				bean.put("sumThung", sumThung);
				bean.put("sumLe", sumLe);
				listBeans.add(bean);
			}			
			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("listBeans", listBeans);
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathSaleProduct(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.EXPORT_PRINT_DELIVERY_ORDER_TEMPLATE;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			
			String outputName = ConstantManager.EXPORT_PRINT_DELIVERY_ORDER + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
			
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream,	beans);
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			saleOrderMgr.updateListSaleOrder(listUpdateSO);
			
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.printOddOrder"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);				
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return SUCCESS;
	}
	
	/**
	 * Prints PDFthe odd order.
	 *
	 * @return the string
	 */
	public String printPDFOddOrder() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			parametersReport = new HashMap<String, Object>();		
//			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			
			shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			}
			if(shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			} else {
				parametersReport.put("sName", shop.getShopName());
				if(!StringUtil.isNullOrEmpty(shop.getAddress())){
					parametersReport.put("sAddress", shop.getAddress());
				} else {
					parametersReport.put("sAddress", "");
				}
				if(!StringUtil.isNullOrEmpty(shop.getPhone())){
					parametersReport.put("sPhone", shop.getPhone());
				} else {
					parametersReport.put("sPhone", "");
				}
				if(!StringUtil.isNullOrEmpty(shop.getTaxNum())) {
					parametersReport.put("mst",shop.getTaxNum());
				} else {
					parametersReport.put("mst","");
				}
			}
			parametersReport.put("cDate", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
			List<SaleOrder> listUpdateSO = new ArrayList<SaleOrder>();
			
			List<RptDeliveryAndPaymentOfStaffVO> listPGNTT = new ArrayList<RptDeliveryAndPaymentOfStaffVO>();
			List<SaleOrder> listData = saleOrderMgr.getListSaleOrderById(listSaleOrderId, orderType);
//			List<ProgramCodeVO> progamCodeVOs = saleOrderMgr.getListProgramCodeVO(listSaleOrderId);
			RptDeliveryAndPaymentOfStaffVO item = null;
			RptSaleOrderLotVO saleDetail = null;
			String invoiceNumber = "";				
			List<SaleOrderDetail> listSaleOrderDetail = null;
			
			List<RptSaleOrderLotVO> listProduct = null;
			List<RptSaleOrderLotVO> listPromotionProduct = null;

			BigDecimal discountAmount = BigDecimal.ZERO;
			BigDecimal notDiscountAmount = BigDecimal.ZERO;
			BigDecimal totalMoney = BigDecimal.ZERO;
			
			SaleOrderDetail sod = null;
			RptSaleOrderLotVO promoDetail = null;
			RptSaleOrderLotVO promoTmp = null;
			
			int i, sz, j, szj;
			ShopParam sp = commonMgr.getShopParamEx(shop.getId(), ConstantManager.ORDERNUMBER_FONT_SIZE);
			if (sp != null && !StringUtil.isNullOrEmpty(sp.getCode())) {
				// kiem tra so
				try {
					Integer.valueOf(sp.getCode().trim()); // parse thanh cong thi moi dung la so
					String fontSize = sp.getCode().trim();
					parametersReport.put("fontSz", fontSize);
				} catch (Exception e1) {
					// pass through
					parametersReport.put("fontSz", ""+ConstantManager.DEFAULT_FONT_SIZE);
				}
			} else {
				parametersReport.put("fontSz", ""+ConstantManager.DEFAULT_FONT_SIZE);
			}
//			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shT.getShopId(), commonMgr.getSysDate());
			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shop.getId(), commonMgr.getSysDate());
			printBatch1++;
			for(SaleOrder saleOrder : listData) {
				item = new RptDeliveryAndPaymentOfStaffVO();
				item.setShortCode(saleOrder.getCustomer().getShortCode());
				item.setCustomerName(saleOrder.getCustomer().getCustomerName());
				item.setCustomerPhoneNumber(saleOrder.getCustomer().getMobiphone());
				//item.setCustomerAddress(saleOrder.getCustomer().getAddress());
				item.setCustomerAddress(getCustomerFullAddress(saleOrder.getCustomer()));
				item.setInvoiceDate(saleOrder.getOrderDate());
				if (saleOrder.getDelivery() != null) {
					Staff deliveryStaff = saleOrder.getDelivery();
					String infoDeliveryStaff = deliveryStaff.getStaffName();
					if (deliveryStaff.getMobilephone() != null) {
						infoDeliveryStaff += "(" + deliveryStaff.getMobilephone() + ")";
					} else if (deliveryStaff.getPhone() != null) {
						infoDeliveryStaff += "(" + deliveryStaff.getPhone() + ")";
					}
					item.setDeliveryStaff(infoDeliveryStaff);
				} else {
					item.setDeliveryName("");
					item.setDeliveryStaff("");
				}
				if (saleOrder.getStaff() != null) {
					Staff staff = saleOrder.getStaff();
					String infoStaff = staff.getStaffName();
					if (staff.getMobilephone() != null) {
						infoStaff += "(" + staff.getMobilephone() + ")";
					} else if (staff.getPhone() != null) {
						infoStaff += "(" + staff.getPhone() + ")";
					}
					item.setSaleStaff(infoStaff);
				}
				invoiceNumber = "";				
				listSaleOrderDetail = saleOrderMgr.getListSaleOrderDetailBySaleOrderId(null, saleOrder.getId()).getLstObject();
				
				listProduct = new ArrayList<RptSaleOrderLotVO>();
				listPromotionProduct = new ArrayList<RptSaleOrderLotVO>();

				discountAmount = BigDecimal.ZERO;
				notDiscountAmount = BigDecimal.ZERO;
				totalMoney = BigDecimal.ZERO;
				for(i = 0, sz = listSaleOrderDetail.size(); i < sz; i++) {
					sod = listSaleOrderDetail.get(i);
					if(sod.getInvoice() != null) {
						invoiceNumber += invoiceNumber.equals("") ? invoiceNumber + sod.getInvoice().getInvoiceNumber() : (invoiceNumber.indexOf(sod.getInvoice().getInvoiceNumber()) == -1 ? ", " + invoiceNumber + sod.getInvoice().getInvoiceNumber() : "");
						item.setInvoice(true);
					}
					
					if (sod.getDiscountAmount() != null && sod.getDiscountAmount().compareTo(BigDecimal.ZERO) > 0) {
						discountAmount = discountAmount.add(sod.getDiscountAmount());
					}
					
					if(sod.getIsFreeItem() == 0) {
						saleDetail = new RptSaleOrderLotVO();
						saleDetail.setProductCode(sod.getProduct().getProductCode());
						saleDetail.setProductName(sod.getProduct().getProductName());
						saleDetail.setConvfact(sod.getProduct().getConvfact());
						saleDetail.setQuantity(sod.getQuantity());
						saleDetail.setPrice(sod.getPriceValue());
						saleDetail.setPkPrice(sod.getPackagePrice());
						listProduct.add(saleDetail);
						
						notDiscountAmount = notDiscountAmount.add(sod.getAmount());
					} else if (sod.getQuantity() != null && sod.getQuantity() > 0) {	
//						for (int k = 0, n = progamCodeVOs.size(); k < n; k++) {
//							ProgramCodeVO programCode = progamCodeVOs.get(k);
//							if (programCode.getSaleOrderId().equals(saleOrder.getId()) && programCode.getProductId().equals(sod.getProduct().getId())) {
//								sod.setProgramCode(programCode.getStrProgramCode());
//								break;
//							}
//						}
						promoDetail = null;
						for (j = 0, szj = listPromotionProduct.size(); j < szj; j++) {
							promoTmp = listPromotionProduct.get(j);
							if (promoTmp.getPromotionCode() != null && promoTmp.getPromotionCode().trim().equalsIgnoreCase(sod.getProgramCode()) && promoTmp.getProductCode() != null && sod.getProduct().getProductCode() != null
									&& promoTmp.getProductCode().trim().equalsIgnoreCase(sod.getProduct().getProductCode().trim())) {
								promoDetail = promoTmp;
							}
						}
						
						if (promoDetail == null) {
							promoDetail = new RptSaleOrderLotVO();
							promoDetail.setProductCode(sod.getProduct().getProductCode());
							promoDetail.setProductName(sod.getProduct().getProductName());
							promoDetail.setConvfact(sod.getProduct().getConvfact());
							promoDetail.setQuantity(sod.getQuantity());
							promoDetail.setPromotionCode(sod.getProgramCode());
							listPromotionProduct.add(promoDetail);
						} else {
							if (promoDetail.getQuantity() == null) {
								promoDetail.setQuantity(0);
							}
							promoDetail.setQuantity(promoDetail.getQuantity() + sod.getQuantity());
						}
					}
				}
				totalMoney = notDiscountAmount.subtract(discountAmount);
				item.setInvoiceNumber(saleOrder.getOrderNumber());
				item.setAmount(notDiscountAmount);
				item.setDiscount(discountAmount);
				item.setTotal(totalMoney);
				item.setTotalString(StringUtil.convertMoneyToString(totalMoney));
				item.setListProduct(listProduct);
				item.setListPromotionProduct(listPromotionProduct);
				
				if(saleOrder.getTimePrint() == null){
					saleOrder.setTimePrint(commonMgr.getSysDate());
					saleOrder.setPrintBatch(printBatch1);
				}
				
				listUpdateSO.add(saleOrder);
				
				if (item.isInvoice()) {
					item.setCheckHD(ReportUtils.getVinamilkChecked(request));
				} else {
					item.setCheckHD(ReportUtils.getVinamilkUnchecked(request));
				}
				
				listPGNTT.add(item);
			}
			
			FileExtension ext = FileExtension.PDF;				
			JRDataSource dataSource = new JRBeanCollectionDataSource(listPGNTT);
			
			ShopReportTemplate printTemplate = ShopReportTemplate.DAILY_FOLLOW_DELIVERY_TICKET;	// default page size is A5
			if (printPageSize != null && ConstantManager.PAGE_FORMAT_A4.equals(printPageSize.trim().toUpperCase())) {
				printTemplate = ShopReportTemplate.DAILY_FOLLOW_DELIVERY_TICKET_A4;
			}
			String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, printTemplate);
			// lacnv1 - tam thoi sua lai de demo applet cho KH nao do
			/*String outputFile = ShopReportTemplate.DAILY_FOLLOW_DELIVERY_TICKET.getOutputPath(ext);
			String outputPath = Configuration.getStoreImportRealPath() + outputFile;
			String outputDownload = Configuration.getStoreImportDownloadPath() + outputFile;
			String templatePath = ShopReportTemplate.DAILY_FOLLOW_DELIVERY_TICKET.getTemplatePath(false, FileExtension.JASPER);
			try {
				JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parametersReport, dataSource);
				JRAbstractExporter exporter = new JRPdfExporter();
				
				if (exporter != null) {
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
					exporter.exportReport();
				}
			} catch (Exception e) {
				LogUtility.logError(e, "Error PrintReceiveProductAction.printOddOrder");
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				return ERROR;
			}*/
			//// lacnv1 - moi nguoi dung log bug nha
//			outputPath = outputDownload;
			result.put(ERROR, false);
			result.put("hasData", true);
			result.put(REPORT_PATH, outputPath);
			result.put(LIST, outputPath);
			session.setAttribute("downloadPath", outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			
			saleOrderMgr.updateListSaleOrder(listUpdateSO);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.printPDFOddOrder"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String viewOdd() {
		try {
			shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			}
			if(shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			listBeans = new ArrayList<Map<String,Object>>();
			List<SaleOrder> listData = saleOrderMgr.getListSaleOrderById(listSaleOrderId, orderType);
//			List<ProgramCodeVO> progamCodeVOs = saleOrderMgr.getListProgramCodeVO(listSaleOrderId);
			Map<String, Object> bean = null;
			Map<String, Object> __sod = null;
			SaleOrderDetail sod = null;
			List<SaleOrderDetail> listSaleOrderDetail = null;
			List<SaleOrderDetail> listSale = null;
			List<Map<String, Object>> listFree = null;
			BigDecimal discountAmount = null;
			BigDecimal notDiscountAmount = null;
			BigDecimal totalMoney = null;
			Integer sumThung = 0;
			Integer sumLe = 0;
			Map<String,Object> soFree = null;
			String stmp = null;
			String stmp1 = null;
			String stmp2 = null;
			Integer qtt = 0;
			Object otmp = null;
			int i, sz, j, szj;
			fontSz = ConstantManager.DEFAULT_FONT_SIZE;
			ShopParam sp = commonMgr.getShopParamEx(shop.getId(), ConstantManager.ORDERNUMBER_FONT_SIZE);
			if (sp != null && !StringUtil.isNullOrEmpty(sp.getCode())) {
				// kiem tra so
				try {
					fontSz = Integer.valueOf(sp.getCode().trim()); // parse thanh cong thi moi dung la so
				} catch (Exception e1) {
					// pass through
					fontSz = ConstantManager.DEFAULT_FONT_SIZE;
				}
			}
			for (SaleOrder saleOrder : listData) {
				bean = new HashMap<String, Object>();
				
				bean.put("printDate", DateUtil.toDateString(DateUtil.getCurrentGMTDate(), DateUtil.DATE_FORMAT_DDMMYYYY));
				String invoiceNumber = "";				
				listSaleOrderDetail = saleOrderMgr.getListSaleOrderDetailBySaleOrderId(null, saleOrder.getId()).getLstObject();
				
				listSale = new ArrayList<SaleOrderDetail>();
				listFree = new ArrayList<Map<String,Object>>();
				
				discountAmount = BigDecimal.ZERO;
				notDiscountAmount = BigDecimal.ZERO;
				totalMoney = BigDecimal.ZERO;
				sumThung = 0;
				sumLe = 0;
				stmp = null;
				stmp1 = null;
				qtt = 0;
				otmp = null;
				
				for(i = 0, sz = listSaleOrderDetail.size(); i < sz; i++) {
					sod = listSaleOrderDetail.get(i);
					if (sod.getInvoice() != null) {
						invoiceNumber += invoiceNumber.equals("") ? invoiceNumber + sod.getInvoice().getInvoiceNumber() : (invoiceNumber.indexOf(sod.getInvoice().getInvoiceNumber()) == -1 ? ", " + invoiceNumber + sod.getInvoice().getInvoiceNumber() : "");
					}
					
					if(sod.getDiscountAmount() != null && sod.getDiscountAmount().compareTo(BigDecimal.ZERO) > 0) {
						discountAmount = discountAmount.add(sod.getDiscountAmount());
					}
					
					if (sod.getIsFreeItem() == 0) {
						listSale.add(sod);
						notDiscountAmount = notDiscountAmount.add(sod.getAmount());
						sumThung = sumThung + (sod.getQuantity() / sod.getProduct().getConvfact());
						sumLe = sumLe + (sod.getQuantity() % sod.getProduct().getConvfact());
					}
					//TUNGTT
//					} else if(!ProgramType.DESTROY.equals(sod.getProgramType()) && 
//							!ProgramType.RETURN.equals(sod.getProgramType()) && 
//							!ProgramType.PRODUCT_EXCHANGE.equals(sod.getProgramType()) && sod.getQuantity() != null && sod.getQuantity() > 0) {
					else if (sod.getQuantity() != null && sod.getQuantity() > 0) {
//						for (int k = 0, n = progamCodeVOs.size(); k < n; k++) {
//							ProgramCodeVO programCode = progamCodeVOs.get(k);
//							if (programCode.getSaleOrderId().equals(saleOrder.getId()) && programCode.getProductId().equals(sod.getProduct().getId())) {
//								sod.setProgramCode(programCode.getStrProgramCode());
//								break;
//							}
//						}						
						__sod = null;
						for (j = 0, szj = listFree.size(); j < szj; j++) {
							soFree = listFree.get(j);
							stmp = sod.getProgramCode();
							stmp1 = null;
							stmp2 = null;
							if (soFree.get("programCode") != null) {
								stmp1 = soFree.get("programCode").toString().trim();
							}
							if (soFree.get("product") != null) {
								stmp2 = ((Product)soFree.get("product")).getProductCode();
							}
							if (stmp1 != null && !StringUtil.isNullOrEmpty(stmp) && stmp1.equalsIgnoreCase(stmp.trim())
									&& stmp2 != null && sod.getProduct() != null
									&& stmp2.trim().equalsIgnoreCase(sod.getProduct().getProductCode().trim())) {
								__sod = soFree;
							}
						}
						
						if (__sod == null) {
							__sod = new HashMap<String, Object>();
							
							__sod.put("amount", sod.getAmount());
							__sod.put("createDate", sod.getCreateDate());
							__sod.put("createUser", sod.getCreateUser());
							__sod.put("discountAmount", sod.getDiscountAmount());
							__sod.put("discountPercent", sod.getDiscountPercent());
							__sod.put("isFreeItem", sod.getIsFreeItem());
							__sod.put("orderDate", sod.getOrderDate());
							__sod.put("priceValue", sod.getPriceValue());
							__sod.put("price", sod.getPrice());
							__sod.put("product", sod.getProduct());
							__sod.put("programCode", sod.getProgramCode());
							__sod.put("programType", sod.getProgramType());
							__sod.put("quantity", sod.getQuantity());
							__sod.put("id", sod.getId());
							__sod.put("saleOrder", sod.getSaleOrder());
							__sod.put("shop", sod.getShop());
							__sod.put("staff", sod.getStaff());
							__sod.put("totalWeight", sod.getTotalWeight());
							__sod.put("updateDate", sod.getUpdateDate());
							__sod.put("updateUser", sod.getUpdateUser());
							__sod.put("vat", sod.getVat());
							
							listFree.add(__sod);
						} else {
							otmp = __sod.get("quantity");
							if (otmp == null) {
								qtt = 0;
							} else {
								qtt = Integer.valueOf(otmp.toString());
							}
							qtt = qtt + sod.getQuantity();
							__sod.put("quantity", qtt);
						}
					}
				}
				
				bean.put("invoiceNumber", invoiceNumber);				
				totalMoney = notDiscountAmount.subtract(discountAmount);				
				bean.put("notDiscountAmount", convertMoney(notDiscountAmount));
				bean.put("discountAmount", convertMoney(discountAmount));
				bean.put("totalMoney", convertMoney(totalMoney));
				bean.put("moneyString", StringUtil.convertMoneyToString(totalMoney));
				bean.put("saleOrder", saleOrder);
				String infoDeliveryStaff = "";
				if (saleOrder.getDelivery() != null) {
					Staff deliveryStaff = saleOrder.getDelivery();
					infoDeliveryStaff = deliveryStaff.getStaffName();
					if (deliveryStaff.getMobilephone() != null) {
						infoDeliveryStaff += "(" + deliveryStaff.getMobilephone() + ")";
					} else if (deliveryStaff.getPhone() != null) {
						infoDeliveryStaff += "(" + deliveryStaff.getPhone() + ")";
					}
				}
				bean.put("infoDeliveryStaff", infoDeliveryStaff);
				String infoStaff = "";
				if (saleOrder.getStaff() != null) {
					Staff staff = saleOrder.getStaff();
					infoStaff = staff.getStaffName();
					if (staff.getMobilephone() != null) {
						infoStaff += "(" + staff.getMobilephone() + ")";
					} else if (staff.getPhone() != null) {
						infoStaff += "(" + staff.getPhone() + ")";
					}
				}
				bean.put("infoStaff", infoStaff);
				bean.put("invoiceDate", DateUtil.toDateSimpleFormatString(saleOrder.getOrderDate()));
				
				bean.put("listSale", listSale);
				bean.put("listFree", listFree);
				
				bean.put("sumThung", sumThung);
				bean.put("sumLe", sumLe);
				
				listBeans.add(bean);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.viewOdd"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String updateAfterPrint(){
		resetToken(result);
		try{
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			
			List<SaleOrder> listUpdateSO = new ArrayList<SaleOrder>();
//			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shT.getShopId(), commonMgr.getSysDate());
			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shop.getId(), commonMgr.getSysDate());
			printBatch1++;
			for(Long saleOrderId : listSaleOrderId) {
				SaleOrder saleOrder = saleOrderMgr.getSaleOrderById(saleOrderId);
				if(saleOrder.getTimePrint() == null){
					saleOrder.setTimePrint(commonMgr.getSysDate());
					saleOrder.setPrintBatch(printBatch1);
				}
				listUpdateSO.add(saleOrder);
			}
			saleOrderMgr.updateListSaleOrder(listUpdateSO);
		}catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.updateAfterPrint"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return JSON;
	}
	/**
	 * Prints the total order.
	 *
	 * @return the string
	 */
	public String printTotalOrder() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return ERROR;
			}

			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			List<PrintDeliveryGroupVO> listPrintDeliveryGroupVo = saleOrderMgr.getListPrintDeliveryGroupVO(listSaleOrderId);
			listBeans = new ArrayList<Map<String,Object>>();
			List<SaleOrder> listUpdateSO = new ArrayList<SaleOrder>();
			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shop.getId(), commonMgr.getSysDate());
			printBatch1++;
			for(Long soId : listSaleOrderId) {
				SaleOrder so = saleOrderMgr.getSaleOrderById(soId);
				if(so != null) {
					if (so.getTimePrint() == null) {
						so.setTimePrint(commonMgr.getSysDate());
						so.setPrintBatch(printBatch1);
					}
					listUpdateSO.add(so);
				}
			}
			saleOrderMgr.updateListSaleOrder(listUpdateSO);
			for (PrintDeliveryGroupVO printDeliveryGroupVO : listPrintDeliveryGroupVo) {
				Map<String, Object> bean = new HashMap<String, Object>();
				bean.put("shop", shop);
				bean.put("printDate", displayDate(DateUtil.getCurrentGMTDate()));
				
				bean.put("deliveryCode", printDeliveryGroupVO.getDeliveryCode());
				bean.put("deliveryName", printDeliveryGroupVO.getDeliveryName());
				bean.put("customerShortCode", printDeliveryGroupVO.getCustomerShortCode());
				bean.put("customerName", printDeliveryGroupVO.getCustomerName());
				bean.put("customerAddress", printDeliveryGroupVO.getCustomerAddress());
				
				bean.put("orderNumber", printDeliveryGroupVO.getOrderNumber());
				String carNumber = null;
				if(!StringUtil.isNullOrEmpty(printDeliveryGroupVO.getOrderNumber())) {
					String __orderNumber = printDeliveryGroupVO.getOrderNumber();
					String[] _orderNumber = __orderNumber.split(",");
					List<String> lstOrderNumber = new ArrayList<String>();
					for(int z = 0; z < _orderNumber.length; z++) {
						lstOrderNumber.add(_orderNumber[z].trim());
					}
					Car lastestCar = saleOrderMgr.getLastestCarFromListSaleOrderNumber(lstOrderNumber);
					if(lastestCar != null) {
						carNumber = lastestCar.getCarNumber();
					}
				}
				bean.put("carNumber", carNumber);
				bean.put("listProduct", printDeliveryGroupVO.getLstDeliveryGroup());
				
				BigDecimal totalMoney = BigDecimal.ZERO;
				
				BigDecimal notDiscountMoney = BigDecimal.ZERO;
				
				BigDecimal discountMoney = BigDecimal.ZERO;
				
				for(PrintDeliveryGroupVO1 product : printDeliveryGroupVO.getLstDeliveryGroup()) {
					notDiscountMoney = notDiscountMoney.add(product.getAmount());
				}
				
				
				bean.put("listFreeProduct", printDeliveryGroupVO.getLstDeliveryGroupFree());
				
				for(PrintDeliveryGroupVO1 freeMoney : printDeliveryGroupVO.getLstDeliveryGroupFreeAmount()) {
					discountMoney = discountMoney.add(freeMoney.getDiscountAmount());
				}
				
				totalMoney = notDiscountMoney.subtract(discountMoney);
				
				bean.put("totalMoney", totalMoney);
				bean.put("moneyString", StringUtil.convertMoneyToString(totalMoney));
				bean.put("notDiscountMoney", notDiscountMoney);
				bean.put("discountMoney", discountMoney);
				listBeans.add(bean);
			}
			
			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("listBeans", listBeans);
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathSaleProduct(); //Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.EXPORT_PRINT_DELIVERY_ORDER_TOTAL_TEMPLATE;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			
			String outputName = ConstantManager.EXPORT_PRINT_DELIVERY_ORDER_TOTAL + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
			
			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream,	beans);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch(Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.printTotalOrder"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * Prints the export order.
	 *
	 * @return string
	 * @see In don gop
	 */
	public String printExportOrder() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return ERROR;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			
			Date fromDate = DateUtil.parse(createDateFrom, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date toDate = DateUtil.parse(createDateTo, DateUtil.DATE_FORMAT_DDMMYYYY);
			
			if(fromDate == null) {
				fromDate = DateUtil.getCurrentGMTDate();
			}
			
			if(toDate == null) {
				toDate = DateUtil.getCurrentGMTDate();
			}
			
			List<PrintDeliveryGroupExportVO> listPrintDeliveryExportGroup = saleOrderMgr.getListPrintDeliveryGroupExportVO(listSaleOrderId, fromDate, toDate);
			
			List<SaleOrder> listUpdateSO = new ArrayList<SaleOrder>();
			
			int printBatch1 = saleOrderMgr.getMaxPrintBatch(shop.getId(), commonMgr.getSysDate());
			printBatch1++;
			for(Long soId : listSaleOrderId) {
				SaleOrder so = saleOrderMgr.getSaleOrderById(soId);
				if(so != null) {
					if (so.getTimePrint() == null) {
						so.setTimePrint(commonMgr.getSysDate());
						so.setPrintBatch(printBatch1);
					}
					listUpdateSO.add(so);
				}
			}
			saleOrderMgr.updateListSaleOrder(listUpdateSO);
			String folder = Configuration.getStoreRealPath(); //Configuration.getStoreRealPath();
			String outputName = ConstantManager.EXPORT_PRINT_EXPORT_DELIVERY_ORDER_TOTAL + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (folder + outputName).replace('/', File.separatorChar);
			exportFileName = exportFile(exportFileName, shop, displayDate(DateUtil.getCurrentGMTDate()), displayDate(fromDate), displayDate(toDate), listPrintDeliveryExportGroup);
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.printExportOrder"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * Export file.
	 *
	 * @param output the output
	 * @param shop the shop
	 * @param printDate the print date
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @param listPrintDeliveryExportGroup the list print delivery export group
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String exportFile(String output, Shop shop, String printDate, String fromDate, String toDate, List<PrintDeliveryGroupExportVO> listPrintDeliveryExportGroup) throws IOException {
//		int iLoop = 0;
		int iRow = 0;
		
		FileOutputStream outFile = new FileOutputStream(new File(output));
		
		
		Workbook wb = new HSSFWorkbook();
		
		FileInputStream isPicture = new FileInputStream(new StringBuilder(ServletActionContext.getServletContext().getRealPath("/")).append("/resources/images/vinamilk.jpg").toString());
		byte[] bytesPicture = IOUtils.toByteArray(isPicture);
		
		int pictureIdx = wb.addPicture(bytesPicture, Workbook.PICTURE_TYPE_JPEG);
		isPicture.close();
		CreationHelper helper = wb.getCreationHelper();
		
		Sheet bmSheet = wb.createSheet();
		bmSheet.setColumnWidth(1, 4000);
		bmSheet.setColumnWidth(2, 4000);
		bmSheet.setColumnWidth(3, 4000);
		bmSheet.setColumnWidth(4, 4000);
		bmSheet.setColumnWidth(5, 4000);
		bmSheet.setColumnWidth(6, 4000);
		bmSheet.setColumnWidth(7, 4000);
		bmSheet.setColumnWidth(8, 4000);
		bmSheet.setColumnWidth(9, 4000);
		
		CellStyle cellStyleTextBoldLeft = wb.createCellStyle();
		cellStyleTextBoldLeft.setAlignment(CellStyle.ALIGN_LEFT);
		cellStyleTextBoldLeft.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		Font fontBold = wb.createFont();
		fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);
		cellStyleTextBoldLeft.setFont(fontBold);
		
		
		CellStyle cellStyleTextBoldCenter = wb.createCellStyle();
		cellStyleTextBoldCenter.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
		cellStyleTextBoldCenter.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTextBoldCenter.setFont(fontBold);
		
		CellStyle cellStyleTextCenter = wb.createCellStyle();
		cellStyleTextCenter.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
		cellStyleTextCenter.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		
		CellStyle titleCellStyle = wb.createCellStyle();
		Font titleCellFont = wb.createFont();
		titleCellFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		titleCellFont.setFontHeightInPoints((short) 24);
		titleCellStyle.setFont(titleCellFont);
		titleCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		
		CellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		
		CellStyle proHeaderCellStyle = wb.createCellStyle();
		proHeaderCellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		proHeaderCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		proHeaderCellStyle.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
		proHeaderCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		CellStyle proCellStyleCenter = wb.createCellStyle();
		proCellStyleCenter.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
		proCellStyleCenter.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		CellStyle proCellStyleLeft = wb.createCellStyle();
		proCellStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		proCellStyleLeft.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		for(int i = 0; i < listPrintDeliveryExportGroup.size(); i++) {
			PrintDeliveryGroupExportVO printDeliverGroupExportVO = listPrintDeliveryExportGroup.get(i);
			
			Row tableHeaderRow1 = bmSheet.createRow(iRow);//row 1
			Cell tableHeaderCell1 = tableHeaderRow1.createCell(1);
			tableHeaderCell1.setCellStyle(cellStyleTextBoldLeft);
			tableHeaderCell1.setCellValue(shop.getShopName());
			
			Drawing drawing = bmSheet.createDrawingPatriarch();
			ClientAnchor anchor = helper.createClientAnchor();
			anchor.setCol1(8);
			anchor.setRow1(iRow);
			Picture pict = drawing.createPicture(anchor, pictureIdx);
			pict.resize();
			
			iRow += 1;//dong 1 ghi ten shop name iRow = 0
			
			Row tableHeaderRow2 = bmSheet.createRow(iRow);//row 2
			Cell tableHeaderCell2 = tableHeaderRow2.createCell(1);
			tableHeaderCell2.setCellStyle(cellStyleTextBoldLeft);
			tableHeaderCell2.setCellValue(shop.getAddress());
			iRow += 1;//dong 2 ghi dia chi shop iRow = 1
			
			Row tableHeaderRow3 = bmSheet.createRow(iRow);//row 3
			Cell tableHeaderCell3 = tableHeaderRow3.createCell(1);
			tableHeaderCell3.setCellStyle(cellStyleTextBoldLeft);
			tableHeaderCell3.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.print.delivery.label.print.date") + " :  " + printDate);
			iRow += 2 ;//dong 3 ghi ngay in iRow = 2
			
			Row tableTitleRow4 = bmSheet.createRow(iRow);//tieu de row 5 iRow = 4
			tableTitleRow4.setHeightInPoints((short) 30);
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 2, 7));
			Cell tableTitleCell = tableTitleRow4.createCell(2);
			tableTitleCell.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export"));
			tableTitleCell.setCellStyle(titleCellStyle);
			iRow += 1;//dong 5 ghi tieu de iRow = 4
			
			Row tableDateRow5 = bmSheet.createRow(iRow);//ngay thang row 6 iRow = 5
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 2, 7));
			Cell tableDateCell = tableDateRow5.createCell(2);
			tableDateCell.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.fromDate") + 
					": " + fromDate + "  " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.toDate") +
					": " + toDate);
			tableDateCell.setCellStyle(dateCellStyle);
			iRow += 2;//dong 6 ghi ngay thang
			
			Row tableStaffRow6 = bmSheet.createRow(iRow);// iRow 7
			Cell tableStaffCell =  tableStaffRow6.createCell(1);
			tableStaffCell.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.staff.transfer.not.found") + 
					": " +printDeliverGroupExportVO.getDeliveryName()+ " - " +printDeliverGroupExportVO.getDeliveryCode());
			iRow += 2;//dong 8 ghi nhan vien giao hang
			
			Row tableProHeaderRow1 = bmSheet.createRow(iRow);//them 2 dong tieu de cho bang iRow = 9
			Row tableProHeaderRow2 = bmSheet.createRow(iRow + 1);//them 2 dong tieu de cho bang
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow + 1, 1, 1));//ma hang
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow + 1, 2, 2));//ten hang
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow + 1, 3, 3));//so lo
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow + 1, 4, 4));//don gia
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 5, 6));//san luong ban
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 7, 8));//san luong khuyen mai
			bmSheet.addMergedRegion(new CellRangeAddress(iRow, iRow + 1, 9, 9));//khuyen mai tien
			
			Cell tableProHeaderCell = tableProHeaderRow1.createCell(1);//ma hang
			tableProHeaderCell.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.product.code"));
			tableProHeaderCell.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 1, 1), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 1, 1), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 1, 1), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 1, 1), bmSheet, wb);
			
			Cell tableProHeaderCell2 = tableProHeaderRow1.createCell(2);//ten hang
			tableProHeaderCell2.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.product.name"));
			tableProHeaderCell2.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 2, 2), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 2, 2), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 2, 2), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 2, 2), bmSheet, wb);
			
			Cell tableProHeaderCell3 = tableProHeaderRow1.createCell(3);//so lo
			tableProHeaderCell3.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.product.lot"));
			tableProHeaderCell3.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 3, 3), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 3, 3), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 3, 3), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 3, 3), bmSheet, wb);
			
			Cell tableProHeaderCell4 = tableProHeaderRow1.createCell(4);//don gia
			tableProHeaderCell4.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.product.price"));
			tableProHeaderCell4.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 4, 4), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 4, 4), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 4, 4), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 4, 4), bmSheet, wb);
			
			Cell tableProHeaderCell5 = tableProHeaderRow1.createCell(5);//san luong ban
			tableProHeaderCell5.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.product.number.sale"));
			tableProHeaderCell5.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 5, 6), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 5, 6), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 5, 6), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 5, 6), bmSheet, wb);
			
			Cell tableProHeaderCell6 = tableProHeaderRow2.createCell(5);//SL
			tableProHeaderCell6.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.product.number.sale.num"));
			tableProHeaderCell6.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 5, 5), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 5, 5), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 5, 5), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 5, 5), bmSheet, wb);
			
			Cell tableProHeaderCell7 = tableProHeaderRow2.createCell(6);//thanh tien
			tableProHeaderCell7.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.product.number.sale.money"));
			tableProHeaderCell7.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 6, 6), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 6, 6), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 6, 6), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 6, 6), bmSheet, wb);
			
			Cell tableProHeaderCell8 = tableProHeaderRow1.createCell(7);//Sáº£n lÆ°á»£ng KM HĂ ng
			tableProHeaderCell8.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.promotion.number"));
			tableProHeaderCell8.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 7, 8), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 7, 8), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 7, 8), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 7, 8), bmSheet, wb);
			
			Cell tableProHeaderCell9 = tableProHeaderRow2.createCell(7);//SL
			tableProHeaderCell9.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.promotion.number.num"));
			tableProHeaderCell9.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 7, 7), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 7, 7), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 7, 7), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 7, 7), bmSheet, wb);
			
			Cell tableProHeaderCell10 = tableProHeaderRow2.createCell(8);//Thanh tien
			tableProHeaderCell10.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.promotion.number.money"));
			tableProHeaderCell10.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 8, 8), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 8, 8), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 8, 8), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow + 1, iRow + 1, 8, 8), bmSheet, wb);
			
			Cell tableProHeaderCell11 = tableProHeaderRow1.createCell(9);
			tableProHeaderCell11.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "title.excel.report.delivery.export.promotion.money"));
			tableProHeaderCell11.setCellStyle(proHeaderCellStyle);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 9, 9), bmSheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 9, 9), bmSheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 9, 9), bmSheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow + 1, 9, 9), bmSheet, wb);
			
			iRow += 1;//vi dong tieu de cua bang sp co 2 dong
			
			int sumQuantityBox = 0;
			int sumQuantityBoxPro = 0;
			int sumQuantityOdd = 0;
			int sumQuantityOddPro = 0;
			BigDecimal sumPrice = BigDecimal.ZERO;
			BigDecimal sumPricePro = BigDecimal.ZERO;
			
			for(int j = 0; j < printDeliverGroupExportVO.getLstDeliveryGroup().size(); j++) {//vong lap 2. cho nay iRow = 10
				PrintDeliveryGroupExportVO1 productExport1 = printDeliverGroupExportVO.getLstDeliveryGroup().get(j);//dong nay in sp
				int proLength = productExport1.getLstDeliveryGroup().size();//so dong ma sp nay chiem tren excel
				List<Row> arrTableProContentRow = new ArrayList<Row>();
				for(int k = 0; k < proLength; k++) {
					iRow += 1;
					Row tableProContentRow = bmSheet.createRow(iRow);//dong dau tien se dc lay de dien ten sp
					arrTableProContentRow.add(k, tableProContentRow);
				}
				
				bmSheet.addMergedRegion(new CellRangeAddress(iRow - proLength + 1, iRow, 1, 1));//product Code
				bmSheet.addMergedRegion(new CellRangeAddress(iRow - proLength + 1, iRow, 2, 2));//product Name
				bmSheet.addMergedRegion(new CellRangeAddress(iRow - proLength + 1, iRow, 4, 4));//price
				bmSheet.addMergedRegion(new CellRangeAddress(iRow - proLength + 1, iRow, 6, 6));//thanh tien
				bmSheet.addMergedRegion(new CellRangeAddress(iRow - proLength + 1, iRow, 8, 8));//thanh tien
				bmSheet.addMergedRegion(new CellRangeAddress(iRow - proLength + 1, iRow, 9, 9));//km tien
				
				Cell proCode = arrTableProContentRow.get(0).createCell(1);//product Code
				proCode.setCellStyle(proCellStyleCenter);
				proCode.setCellValue(productExport1.getProductCode());
				RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 1, 1), bmSheet, wb);
				RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 1, 1), bmSheet, wb);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 1, 1), bmSheet, wb);
				RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 1, 1), bmSheet, wb);
				
				Cell proName = arrTableProContentRow.get(0).createCell(2);//product Name
				proName.setCellStyle(proCellStyleLeft);
				proName.setCellValue(productExport1.getProductName());
				RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 2, 2), bmSheet, wb);
				RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 2, 2), bmSheet, wb);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 2, 2), bmSheet, wb);
				RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 2, 2), bmSheet, wb);
				
				Cell proPrice = arrTableProContentRow.get(0).createCell(4);//price
				proPrice.setCellStyle(proCellStyleCenter);
				proPrice.setCellValue(convertMoney(productExport1.getPrice()));
				RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 4, 4), bmSheet, wb);
				RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 4, 4), bmSheet, wb);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 4, 4), bmSheet, wb);
				RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 4, 4), bmSheet, wb);
				
				Cell proMoney = arrTableProContentRow.get(0).createCell(6);//thanh tien
				proMoney.setCellStyle(proCellStyleCenter);
				BigDecimal totalProMoney = BigDecimal.ZERO;
				for(int k = 0; k < proLength; k++) {//tinh toan tong so tien
					PrintDeliveryGroupExportVO2 productExport2 = productExport1.getLstDeliveryGroup().get(k);
					BigDecimal _totalProMoney = productExport2.getPrice().multiply(new BigDecimal(productExport2.getQuantity()));
					totalProMoney = totalProMoney.add(_totalProMoney);
					sumQuantityBox += productExport2.getQuantity() / productExport2.getConvfact();
					sumQuantityOdd += productExport2.getQuantity() % productExport2.getConvfact();
					sumPrice = sumPrice.add(_totalProMoney);
				}
				proMoney.setCellValue(convertMoney(totalProMoney));
				RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 6, 6), bmSheet, wb);
				RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 6, 6), bmSheet, wb);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 6, 6), bmSheet, wb);
				RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 6, 6), bmSheet, wb);
				
				Cell promoMoney = arrTableProContentRow.get(0).createCell(8);
				promoMoney.setCellStyle(proCellStyleCenter);
				BigDecimal totalPromoMoney = BigDecimal.ZERO;
				for(int k = 0; k < proLength; k++) {
					PrintDeliveryGroupExportVO2 productExport2 = productExport1.getLstDeliveryGroup().get(k);
					BigDecimal _totalPromoMoney = productExport2.getPrice().multiply(new BigDecimal(productExport2.getProQuantity()));
					totalPromoMoney = totalPromoMoney.add(_totalPromoMoney);
					sumQuantityBoxPro += productExport2.getProQuantity() / productExport2.getConvfact();
					sumQuantityOddPro += productExport2.getProQuantity() % productExport2.getConvfact();
					sumPricePro = sumPricePro.add(_totalPromoMoney);
				}
				promoMoney.setCellValue(convertMoney(totalPromoMoney));
				RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 8, 8), bmSheet, wb);
				RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 8, 8), bmSheet, wb);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 8, 8), bmSheet, wb);
				RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 8, 8), bmSheet, wb);
				
				
				Cell promoCash = arrTableProContentRow.get(0).createCell(9);
				promoCash.setCellStyle(proCellStyleCenter);
				promoCash.setCellValue(0);
				RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 9, 9), bmSheet, wb);
				RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 9, 9), bmSheet, wb);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 9, 9), bmSheet, wb);
				RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1, iRow, 9, 9), bmSheet, wb);
				
				for(int k = 0; k < proLength; k++) {
					PrintDeliveryGroupExportVO2 productExport2 = productExport1.getLstDeliveryGroup().get(k);
					Row rowLotProduct = arrTableProContentRow.get(k);
					Cell cellLotProduct = rowLotProduct.createCell(3);
					
					cellLotProduct.setCellStyle(proCellStyleCenter);//so lo
					cellLotProduct.setCellValue(productExport2.getLot());
					RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 3, 3), bmSheet, wb);
					RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 3, 3), bmSheet, wb);
					RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 3, 3), bmSheet, wb);
					RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 3, 3), bmSheet, wb);
					
					Cell cellLotQuatityProduct = rowLotProduct.createCell(5);//so luong ban
					cellLotQuatityProduct.setCellStyle(proCellStyleCenter);
					cellLotQuatityProduct.setCellValue(cellQuantityFormatter(productExport2.getQuantity(), productExport2.getConvfact()));
					RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 5, 5), bmSheet, wb);
					RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 5, 5), bmSheet, wb);
					RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 5, 5), bmSheet, wb);
					RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 5, 5), bmSheet, wb);
					
					Cell cellLotQuatityPromo = rowLotProduct.createCell(7);//so luong khuyen mai
					cellLotQuatityPromo.setCellStyle(proCellStyleCenter);
					cellLotQuatityPromo.setCellValue(cellQuantityFormatter(productExport2.getProQuantity(), productExport2.getConvfact()));
					RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 7, 7), bmSheet, wb);
					RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 7, 7), bmSheet, wb);
					RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 7, 7), bmSheet, wb);
					RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow - proLength + 1 + k, iRow - proLength + 1 + k, 7, 7), bmSheet, wb);
				}
			}
			
			BigDecimal sumPromoMoney = BigDecimal.ZERO;
			if(printDeliverGroupExportVO.getLstDeliveryGroupProMoney().size() > 0) {
				iRow += 1;
				//dong nay la de cho tien khuyen mai
				Row tableRowPromoneyCustGet = bmSheet.createRow(iRow);
				Cell cellPromoneyCustGet = tableRowPromoneyCustGet.createCell(9);
				cellPromoneyCustGet.setCellStyle(proCellStyleCenter);
				cellPromoneyCustGet.setCellValue(convertMoney(printDeliverGroupExportVO.getLstDeliveryGroupProMoney().get(0).getLstDeliveryGroup().get(0).getProMoney()));
				RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 1, 9), bmSheet, wb);
				RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 1, 9), bmSheet, wb);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 1, 9), bmSheet, wb);
				RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 1, 9), bmSheet, wb);
				
				RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 8, 9), bmSheet, wb);
				RegionUtil.setBorderTop(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 8, 9), bmSheet, wb);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 8, 9), bmSheet, wb);
				RegionUtil.setBorderRight(CellStyle.BORDER_THIN, new CellRangeAddress(iRow, iRow, 8, 9), bmSheet, wb);
				
				sumPromoMoney = sumPromoMoney.add(printDeliverGroupExportVO.getLstDeliveryGroupProMoney().get(0).getLstDeliveryGroup().get(0).getProMoney());
			}
			
			iRow += 2;
			Row tableRowSumMoneyBottom = bmSheet.createRow(iRow);//in dong Tong cong:
			Cell cellSumText = tableRowSumMoneyBottom.createCell(2);
			CellStyle cellTextStyleBoldRight = wb.createCellStyle();
			cellTextStyleBoldRight.setAlignment(CellStyle.ALIGN_RIGHT);
			cellTextStyleBoldRight.setVerticalAlignment(CellStyle.ALIGN_CENTER_SELECTION);
			Font fontTextBold = wb.createFont();
			fontTextBold.setBoldweight(Font.BOLDWEIGHT_BOLD);
			cellTextStyleBoldRight.setFont(fontTextBold);
			cellSumText.setCellStyle(cellTextStyleBoldRight);
			cellSumText.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.sum.text") + " :");
			
			Cell cellProductQuatitySum = tableRowSumMoneyBottom.createCell(5);
			cellProductQuatitySum.setCellStyle(proCellStyleCenter);
			cellProductQuatitySum.setCellValue(sumQuantityBox + "/" + sumQuantityOdd);
			
			Cell cellProductQuatityPriceSum = tableRowSumMoneyBottom.createCell(6);
			cellProductQuatityPriceSum.setCellStyle(proCellStyleCenter);
			cellProductQuatityPriceSum.setCellValue(convertMoney(sumPrice));
			
			Cell cellProductPromotionSum = tableRowSumMoneyBottom.createCell(7);
			cellProductPromotionSum.setCellStyle(proCellStyleCenter);
			cellProductPromotionSum.setCellValue(sumQuantityBoxPro + "/" + sumQuantityOddPro);
			
			Cell cellProductPromotionPriceSum = tableRowSumMoneyBottom.createCell(8);
			cellProductPromotionPriceSum.setCellStyle(proCellStyleCenter);
			cellProductPromotionPriceSum.setCellValue(convertMoney(sumPricePro));
			
			Cell cellTotalMoneyPromotionSum = tableRowSumMoneyBottom.createCell(9);
			cellTotalMoneyPromotionSum.setCellStyle(proCellStyleCenter);
			cellTotalMoneyPromotionSum.setCellValue(convertMoney(sumPromoMoney));
			
			iRow += 2;
			
			Row tableRowOrderDeliveryPrint = bmSheet.createRow(iRow);
			Cell cellOrderDeliveryPrintText = tableRowOrderDeliveryPrint.createCell(1);
			cellOrderDeliveryPrintText.setCellStyle(cellStyleTextBoldLeft);
			cellOrderDeliveryPrintText.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.order.export.text"));
			
			List<String> __lstSaleOrder = printDeliverGroupExportVO.getLstSaleOrder();
			
			for(int ii = 0; ii < __lstSaleOrder.size(); ii++) {
				iRow += 1;
				Row tableRowOrderDeliveryExported = bmSheet.createRow(iRow);
				Cell cellOrderDeliveryExported = tableRowOrderDeliveryExported.createCell(1);
				cellOrderDeliveryExported.setCellValue(__lstSaleOrder.get(ii));
			}
			
			/*iRow += 1;
			Row tableRowOrderDeliveryExported = bmSheet.createRow(iRow);
			Cell cellOrderDeliveryExported = tableRowOrderDeliveryExported.createCell(1);
			cellOrderDeliveryExported.setCellValue(printDeliverGroupExportVO.getOrderNumber());*/
			
			iRow += 5;
			Row tableRowStaffSign = bmSheet.createRow(iRow);
			
			Cell cellAuditorStaff = tableRowStaffSign.createCell(2);
			cellAuditorStaff.setCellStyle(cellStyleTextBoldCenter);
			cellAuditorStaff.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.auditor.export.text"));
			
			Cell cellDeliveryStaff = tableRowStaffSign.createCell(5);
			cellDeliveryStaff.setCellStyle(cellStyleTextBoldCenter);
			cellDeliveryStaff.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.delivery.staff.export.text"));
			
			Cell cellStockImportStaff = tableRowStaffSign.createCell(8);
			cellStockImportStaff.setCellStyle(cellStyleTextBoldCenter);
			cellStockImportStaff.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.stock.staff.export.text"));
			
			iRow += 1;
			Row tableRowSign = bmSheet.createRow(iRow);
			
			Cell cellAuditorStaffSign = tableRowSign.createCell(2);
			cellAuditorStaffSign.setCellStyle(cellStyleTextCenter);
			cellAuditorStaffSign.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.staff.sign.and.name"));
			
			Cell cellDeliveryStaffSign = tableRowSign.createCell(5);
			cellDeliveryStaffSign.setCellStyle(cellStyleTextCenter);
			cellDeliveryStaffSign.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.staff.sign.and.name"));
			
			Cell cellStockStaffSign = tableRowSign.createCell(8);
			cellStockStaffSign.setCellStyle(cellStyleTextCenter);
			cellStockStaffSign.setCellValue(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.staff.sign.and.name"));
			
			iRow += 10;
		}
		
		wb.write(outFile);
		outFile.flush();
		outFile.close();
		
		return output;
	}
	
	/**
	 * In phieu xuat kho kiem van chuyen noi bo
	 * 
	 * @author lacnv1
	 * @since Jun 20, 2014
	 */
	public String printTransport() throws Exception {
		try{
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.not.exists.npp"));
				return JSON;
			}
			Calendar cal = Calendar.getInstance();
			parametersReport = new HashMap<String, Object>();
			formatType = FileExtension.PDF.getName();
			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			parametersReport.put("fDate", DateUtil.toDateString(cal.getTime(), DateUtil.DATE_FORMAT_DDMMYYYY));
			
			parametersReport.put("cDate", DateUtil.toDateString(cal.getTime(), DateUtil.DATETIME_FORMAT_STR));
			parametersReport.put("pDay", cal.get(Calendar.DATE));
			parametersReport.put("pMonth", cal.get(Calendar.MONTH) + 1);
			parametersReport.put("pYear", cal.get(Calendar.YEAR));
			
			parametersReport.put("noi_dung", content);
			parametersReport.put("lenh_dieu_dong", name);
			parametersReport.put("don_vi", "");
			Integer transacDay = 0;
			Integer transacMonth = 0;
			Integer transacYear = 0;
			if(!StringUtil.isNullOrEmpty(printTime)){
				Date date = DateUtil.parse(printTime, DateUtil.DATE_FORMAT_DDMMYYYY);
				cal.setTime(date);
				transacDay = cal.get(Calendar.DATE);
				transacMonth = cal.get(Calendar.MONTH) + 1;
				transacYear = cal.get(Calendar.YEAR);
			}
			parametersReport.put("don_vi_ngay", transacDay);
			parametersReport.put("don_vi_thang", transacMonth);
			parametersReport.put("don_vi_nam", transacYear);
			List<RptPXKKVCNB_DataConvert> lst = saleOrderMgr.getPhieuXKVCNB(listSaleOrderId);
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			ApParamMgr apMgr = (ApParamMgr)context.getBean("apParamMgr");
			ApParam ap = apMgr.getApParamByCodeEx("PXK_VCNB", ActiveType.RUNNING);
			Integer p = 10;
			if (ap != null) {
				p = Integer.valueOf(ap.getValue());
			}
			
			List<RptPXKKVCNB_Stock> lstDataStock = new ArrayList<RptPXKKVCNB_Stock>();
			RptPXKKVCNB_Stock dataStock = null;
			RptPXKKVCNB_ProductVansale productItem = null;
			RptPXKKVCNB_DataConvert item = null;
			int i = 0, sz = lst.size(), k = 0;
			while (k < sz) {
				dataStock = new RptPXKKVCNB_Stock();
				dataStock.setDon_vi_ban_hang(shop.getShopName());
				dataStock.setMa_so_thue(shop.getTaxNum());
				dataStock.setSo_dien_thoai(shop.getPhone());	
				dataStock.setDia_chi_kho(shop.getAddress());
				String s = shop.getInvoiceNumberAccount();
				if (!StringUtil.isNullOrEmpty(s)) {
					s += shop.getInvoiceBankName();
				} else {
					s = shop.getInvoiceBankName();
				}
				dataStock.setSo_tai_khoan(s);
				dataStock.setLenh_dieu_dong(name);
				dataStock.setNoi_dung(content);
				dataStock.setDanh_sach_san_pham(new ArrayList<RptPXKKVCNB_ProductVansale>());
				
				for (i = 0; k < sz && i < p; i++, k++)  {
					item = lst.get(k);
					productItem = new RptPXKKVCNB_ProductVansale();
					productItem.setDon_gia(item.getDon_gia());
					productItem.setDon_vi_tinh("ThĂ¹ng/Láº»");
					productItem.setMa_san_pham(item.getMa_san_pham());
					productItem.setSo_le(item.getSo_le());
					productItem.setSo_luong_thuc_xuat(item.getSo_thung() + "/" + item.getSo_le());
					productItem.setThanh_tien(item.getThanh_tien());
					productItem.setSo_thung(item.getSo_thung());
					productItem.setTen_san_pham(item.getTen_san_pham());
	
					dataStock.getDanh_sach_san_pham().add(productItem);
				}
				lstDataStock.add(dataStock);
			}
			JRDataSource dataSource = new JRBeanCollectionDataSource(lstDataStock);
			
			
			ReportUrl reportUrl = reportManagerMgr.getReportFileName(shop.getId(), "XNB");
			if(reportUrl == null || (reportUrl != null && StringUtil.isNullOrEmpty(reportUrl.getUrl()))){//ChÆ°a cĂ³ máº«u hĂ³a Ä‘Æ¡n GTGT
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.template.not.found"));
				return JSON;
			}
			
			String nameFile = reportUrl.getUrl();
			int idx = nameFile.lastIndexOf(".jrxml");
			nameFile = nameFile.substring(0, idx);
			nameFile = nameFile + ".jasper";
			String templatePath = Configuration.getReportTemplatePath() + nameFile;
			File fileReport = new File(templatePath);
			if(!fileReport.exists()){
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.template.not.found"));
				return JSON;
			}
			
			StringBuilder sb = new StringBuilder("PhieuXNK_VCNB")
				.append(DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_EXCEL_FILE))
				.append(".pdf");
			String outputPath = Configuration.getExportExcelPath() + sb.toString();
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parametersReport, dataSource);
			JRAbstractExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, Configuration.getStoreRealPath() + sb.toString());
			exporter.exportReport();
			
			result.put(ERROR, false);
			result.put("hasData", true);
			result.put(REPORT_PATH, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		}catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintReceiveProductAction.printTransport"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Gets the sale staff code.
	 *
	 * @return the sale staff code
	 */
	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	/**
	 * Sets the sale staff code.
	 *
	 * @param saleStaffCode the new sale staff code
	 */
	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}

	/**
	 * Gets the delivery staff code.
	 *
	 * @return the delivery staff code
	 */
	public String getDeliveryStaffCode() {
		return deliveryStaffCode;
	}

	/**
	 * Sets the delivery staff code.
	 *
	 * @param deliveryStaffCode the new delivery staff code
	 */
	public void setDeliveryStaffCode(String deliveryStaffCode) {
		this.deliveryStaffCode = deliveryStaffCode;
	}

	/**
	 * Gets the list status.
	 *
	 * @return the list status
	 */
	public List<StatusBean> getListStatus() {
		return listStatus;
	}

	/**
	 * Sets the list status.
	 *
	 * @param listStatus the new list status
	 */
	public void setListStatus(List<StatusBean> listStatus) {
		this.listStatus = listStatus;
	}

	/**
	 * Gets the choose statuse.
	 *
	 * @return the choose statuse
	 */
	public Integer getChooseStatuse() {
		return chooseStatuse;
	}

	/**
	 * Sets the choose statuse.
	 *
	 * @param chooseStatuse the new choose statuse
	 */
	public void setChooseStatuse(Integer chooseStatuse) {
		this.chooseStatuse = chooseStatuse;
	}

	/**
	 * Gets the shop.
	 *
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 *
	 * @param shop the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * Gets the creates the date from.
	 *
	 * @return the creates the date from
	 */
	public String getCreateDateFrom() {
		return createDateFrom;
	}

	/**
	 * Sets the creates the date from.
	 *
	 * @param createDateFrom the new creates the date from
	 */
	public void setCreateDateFrom(String createDateFrom) {
		this.createDateFrom = createDateFrom;
	}

	/**
	 * Gets the creates the date to.
	 *
	 * @return the creates the date to
	 */
	public String getCreateDateTo() {
		return createDateTo;
	}

	/**
	 * Sets the creates the date to.
	 *
	 * @param createDateTo the new creates the date to
	 */
	public void setCreateDateTo(String createDateTo) {
		this.createDateTo = createDateTo;
	}

	/**
	 * Gets the list sale order.
	 *
	 * @return the list sale order
	 */
	public List<SaleOrder> getListSaleOrder() {
		return listSaleOrder;
	}

	/**
	 * Sets the list sale order.
	 *
	 * @param listSaleOrder the new list sale order
	 */
	public void setListSaleOrder(List<SaleOrder> listSaleOrder) {
		this.listSaleOrder = listSaleOrder;
	}

	/**
	 * Gets the list sale order id.
	 *
	 * @return the list sale order id
	 */
	public List<Long> getListSaleOrderId() {
		return listSaleOrderId;
	}

	/**
	 * Sets the list sale order id.
	 *
	 * @param listSaleOrderId the new list sale order id
	 */
	public void setListSaleOrderId(List<Long> listSaleOrderId) {
		this.listSaleOrderId = listSaleOrderId;
	}

	/**
	 * Gets the list beans.
	 *
	 * @return the list beans
	 */
	public List<Map<String, Object>> getListBeans() {
		return listBeans;
	}

	/**
	 * Sets the list beans.
	 *
	 * @param listBeans the list beans
	 */
	public void setListBeans(List<Map<String, Object>> listBeans) {
		this.listBeans = listBeans;
	}
	
	/**
	 * Gets the checks if is print.
	 *
	 * @return the checks if is print
	 */
	public Boolean getIsPrint() {
		return isPrint;
	}

	/**
	 * Sets the checks if is print.
	 *
	 * @param isPrint the new checks if is print
	 */
	public void setIsPrint(Boolean isPrint) {
		this.isPrint = isPrint;
	}
	
	/**
	 * Gets the list nvbh.
	 *
	 * @return the list nvbh
	 */
	public List<Staff> getListNVBH() {
		return listNVBH;
	}

	/**
	 * Sets the list nvbh.
	 *
	 * @param listNVBH the new list nvbh
	 */
	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}

	/**
	 * Gets the list nvgh.
	 *
	 * @return the list nvgh
	 */
	public List<Staff> getListNVGH() {
		return listNVGH;
	}

	/**
	 * Sets the list nvgh.
	 *
	 * @param listNVGH the new list nvgh
	 */
	public void setListNVGH(List<Staff> listNVGH) {
		this.listNVGH = listNVGH;
	}

	public Integer getTypePrint() {
		return typePrint;
	}

	public void setTypePrint(Integer typePrint) {
		this.typePrint = typePrint;
	}

	public List<RptExSaleOrder2VO> getLstXHNVBHData() {
		return lstXHNVBHData;
	}

	public void setLstXHNVBHData(List<RptExSaleOrder2VO> lstXHNVBHData) {
		this.lstXHNVBHData = lstXHNVBHData;
	}

	public Integer getPrintBatch() {
		return printBatch;
	}

	public void setPrintBatch(Integer printBatch) {
		this.printBatch = printBatch;
	}

	public String getPrintTime() {
		return printTime;
	}

	public void setPrintTime(String printTime) {
		this.printTime = printTime;
	}

	public String getPriorityCode() {
		return priorityCode;
	}

	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public List<RptPGHVO> getViewGopOrder() {
		return viewGopOrder;
	}

	public void setViewGopOrder(List<RptPGHVO> viewGopOrder) {
		this.viewGopOrder = viewGopOrder;
	}

	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}

	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	public Date getNow() {
		return now;
	}

	public void setNow(Date now) {
		this.now = now;
	}

	public Integer getPlaceCreaterOn() {
		return placeCreaterOn;
	}

	public void setPlaceCreaterOn(Integer placeCreaterOn) {
		this.placeCreaterOn = placeCreaterOn;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public Integer getValueOrder() {
		return valueOrder;
	}

	public void setValueOrder(Integer valueOrder) {
		this.valueOrder = valueOrder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public int getFontSz() {
		return fontSz;
	}

	public void setFontSz(int fontSz) {
		this.fontSz = fontSz;
	}

	public Integer getPrintVATStatus() {
		return printVATStatus;
	}

	public void setPrintVATStatus(Integer printVATStatus) {
		this.printVATStatus = printVATStatus;
	}
	
	public List<OrderType> getOrderTypes() {
		return orderTypes;
	}

	public void setOrderTypes(List<OrderType> orderTypes) {
		this.orderTypes = orderTypes;
	}

	public String getPrintPageSize() {
		return printPageSize;
	}

	public void setPrintPageSize(String printPageSize) {
		this.printPageSize = printPageSize;
	}

	public BigDecimal getOrderValue() {
		return orderValue;
	}

	public void setOrderValue(BigDecimal orderValue) {
		this.orderValue = orderValue;
	}

	public Integer getOrderValueCompareType() {
		return orderValueCompareType;
	}

	public void setOrderValueCompareType(Integer orderValueCompareType) {
		this.orderValueCompareType = orderValueCompareType;
	}
}

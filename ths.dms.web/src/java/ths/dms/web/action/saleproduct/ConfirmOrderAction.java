package ths.dms.web.action.saleproduct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Debit;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.IsFreeItemInSaleOderDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.CalculatePromotionFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.SaleOrderVOEx2;
import ths.dms.core.entities.vo.SearchSaleTransactionErrorVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class ConfirmOrderAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	SaleOrderMgr saleOrderMgr;
	private ExceptionDayMgr exceptionDayMgr;
	private PromotionProgramMgr promotionProgramMgr;
	private ProductMgr productMgr;
	private DebitMgr debitMgr;

	private Date lockDay;
	private String staffCode;
	private List<Long> lstOrderId;
	private List<Long> lstPayReceivedId;
	private Long saleOrderId;
	private Long shopId;
	private Shop shop;
	private Long nvbhId;
	private List<Staff> lstNVBH;
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH
	private String orderType;// Loai don
	//private String errMsgToDay;
	//private ApParam apParam;
	//private HashMap<String, List<String>> hashMap;
	private List<OrderProductVO> lstQuantity;
	private List<OrderProductVO> lstPrice;
	private List<OrderProductVO> lstPromotion;
	private List<OrderProductVO> lstAmount;
	private List<OrderProductVO> lstChangeQuantity;
	private List<OrderProductVO> lstChangeRation;
	private List<OrderProductVO> lstSaleOrderHasChangedStructurePromotionProgram;	// danh sach DH co cau truc CTKM thay doi
	private List<OrderProductVO> lstKeyShop;
	private List<OrderProductVO> lstNotIntegrity;
	private String lstLostDetail;
	private String lstOrderNotIntegrity;
	//private String errMsgLot;
	private String description;
	private Integer skipDebit;
	private Integer approvedStep;
	private Integer confirmAccumulation;
	private List<StaffVO> listNVGH;
	private List<ApParam> priority;//Do uu tien
	private String deliveryStaff;
	private String priorityCode;
	private String customerCode;	
	private String customerName;
	private String orderSource;
	private String numberValue;
	private Long numberValueOrder;
	private Integer isValueOrder;
	private List<Shop> lstShop;//danh sach shop
	private String shopCode;
	private String sort;
	private String order;
	//shop cua don vi duoc chon
	private String currentShopCode;
	//staff code nvbh
	private String searchStaffCode;
	//kiem tra sai gia
	private Boolean isCheckPrice;
	private Long shopTypeId;
	private Boolean isCheckOrderDeny;
	
	public List<Shop> getLstShop() {
		return lstShop;
	}

	public void setLstShop(List<Shop> lstShop) {
		this.lstShop = lstShop;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		exceptionDayMgr = (ExceptionDayMgr) context.getBean("exceptionDayMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		debitMgr = (DebitMgr) context.getBean("debitMgr");
		Date now = DateUtil.now();
		if (!StringUtil.isNullOrEmpty(currentShopCode)) {
			shop = shopMgr.getShopByCode(currentShopCode);
			if (shop != null) {
				shopId = shop.getId();
				if (shop.getType() != null) {
					shopTypeId = shop.getType().getId();
				}
			}
			
			List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
			if (lstPr != null && lstPr.size() > 0){
				if (ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())){
					isCheckPrice = true;
				}
			}
			lockDay = shopLockMgr.getApplicationDate(shopId);
		}
		if (lockDay == null) {
			lockDay = now;
		}
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		ShopToken shop = currentUser.getShopRoot();
		if (shop == null) {
			return ERROR;
		}
		//Lay danh sach nhan vien phan quyen
		StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
		if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
			filter.setIsLstChildStaffRoot(true);
		} else {
			filter.setIsLstChildStaffRoot(false);
		}
		filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
		filter.setStatus(ActiveType.RUNNING.getValue());
		filter.setLstShopId(Arrays.asList(currentUser.getShopRoot().getShopId()));
		//Lay NVBH
		filter.setLstObjectType(Arrays.asList(StaffObjectType.NVBH.getValue(), StaffObjectType.NVVS.getValue()));
		ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
		lstNVBHVo = objVO.getLstObject();

		
		/*ObjectVO<Staff> staffVO = staffMgr.getListPreAndVanStaff(null, null, null, shop.getShopId(), ActiveType.RUNNING, Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
		if (staffVO != null && staffVO.getLstObject().size() > 0) {
			lstNVBH = staffVO.getLstObject();
		}*/
		
		//Lay nhan vien NVGH
		filter.setIsLstChildStaffRoot(false);
		filter.setLstObjectType(Arrays.asList(StaffObjectType.NVGH.getValue(), StaffObjectType.NVTT.getValue()));
		objVO = staffMgr.searchListStaffVOByFilter(filter);
		setListNVGH(objVO.getLstObject());
		
		//Load list do uu tien
		priority=apParamMgr.getListApParam(ApParamType.ORDER_PIRITY, ActiveType.RUNNING);
		if(priority==null){
			priority = new ArrayList<ApParam>();
		} else {
			ApParam optionAll = new ApParam();
			optionAll.setApParamCode("-1");
			optionAll.setApParamName("Tất cả");
			priority.add(0, optionAll);
		}
		ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getShopId(), "CONFIRM_ORDER_SEARCH");
		if (numberValueOrderParam != null) {
			numberValue = numberValueOrderParam.getCode();
		} else {
			numberValue = "0";
		}
		//load danh sach shop
		ShopFilter shopFilter = new ShopFilter();
		ObjectVO<Shop> lstShopVO = shopMgr.getListShopSpectific(shopFilter);
		if (lstShopVO!=null) {
			lstShop = lstShopVO.getLstObject();
		}
		return SUCCESS;
	}
	
	/**
	 * @author tungmt
	 * @since 12/5/2015
	 * @param shopId
	 * @return Ngay gan nhat co the approve don hang
	 */
	public Date getMaxDateApprovedOrder(Long shopId, Date startDateTemp) {
//		Date startDateTemp = DateUtil.now();
		try {
			List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_MAXDAY_APPROVE);
			int iApParamName = 0;
			if (lstPr != null && lstPr.size() > 0) {
				try {
					iApParamName = Integer.valueOf(lstPr.get(0).getValue().trim());
				} catch (Exception e1) {
					// pass through
				}
			}
			if (iApParamName > 0) {
				Integer iExcept = 0;
				dayLock = shopLockMgr.getApplicationDate(shopId);
				startDateTemp = DateUtil.moveDate(DateUtil.parseLockDate(dayLock), iApParamName * (-1), 1);
				Date dateTmp = DateUtil.parseLockDate(dayLock);
				do {
					iExcept = exceptionDayMgr.getNumOfHoliday(startDateTemp, dateTmp, shopId);
					dateTmp = DateUtil.moveDate(startDateTemp, -1, 1);
					startDateTemp = DateUtil.moveDate(startDateTemp, -iExcept, 1);
				} while (iExcept > 0);
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.getMaxDateApprovedOrder"), createLogErrorStandard(actionStartTime));
		}
		return startDateTemp;
	}

	/**
	 * Lay danh sach chi tiet don hang
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String searchDetail() {
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser != null && currentUser.getShopRoot() != null && saleOrderId != null && saleOrderId != -1 && shopId != null) {
				SoFilter filter = new SoFilter();
//				filter.setShopId(currentUser.getShopRoot().getShopId());
				filter.setShopId(shopId);
				filter.setShopTypeId(shopTypeId);
				filter.setSaleOrderId(saleOrderId);
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				List<Long> lstOrderId = new ArrayList<Long>();
				lstOrderId.add(saleOrderId);
				filter.setLstSaleOrderId(lstOrderId);
				filter.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
				filter.setIsFreeItem(0);//Chi check gia voi SP ban
				List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.SYS_WAREHOUSE_CONFIG, ActiveType.RUNNING);
				if (aparams == null || aparams.size() == 0 || Constant.SYS_WAREHOUSE_CONFIG_DIVIDUAL.equals(aparams.get(0).getValue())) {
					filter.setIsDividualWarehouse(true);
				} else {
					filter.setIsDividualWarehouse(false);
				}
				lstPrice = saleOrderMgr.getListProductPriceWarning(filter);
				List<SaleOrderDetailVOEx> lst = saleOrderMgr.getListSaleOrderDetailForConfirm(filter);
				if (lst != null && lst.size() > 0) {
					lstQuantity = saleOrderMgr.getListProductQuantityWarning(filter);
					lstChangeQuantity = saleOrderMgr.getListProductChangeQuantityWarning(filter);
					lstChangeRation = saleOrderMgr.getListProductChangeRationWarning(filter);
					lstKeyShop = saleOrderMgr.getListKeyShopWarning(filter);
					filter.setLstSaleOrderId(Arrays.asList(saleOrderId));
					this.getOrderLostDetail(filter);
					this.getListOrderNotIntegrity(filter);
					List<SaleOrderDetailVOEx> lstFooter = new ArrayList<SaleOrderDetailVOEx>();
					if (lstAmount != null && lstAmount.size() > 0 && !lstAmount.get(0).getCountSOD().equals(lstAmount.get(0).getTotalDetail())) {
						SaleOrderDetailVOEx footer = new SaleOrderDetailVOEx();
						footer.setProductCode(R.getResource("confirm.order.and.pay.rot.chi.tiet"));
						lstFooter.add(footer);
					}
					if (lstNotIntegrity != null && lstNotIntegrity.size() > 0 && saleOrderId.equals(lstNotIntegrity.get(0).getSaleOrderId())) {
						SaleOrderDetailVOEx footer = new SaleOrderDetailVOEx();
						footer.setProductCode(R.getResource("confirm.order.and.pay.error.order.not.integrity"));
						lstFooter.add(footer);
					}
					result.put("footer", lstFooter);
					for (SaleOrderDetailVOEx sod : lst) {
						if(lstQuantity != null){
							for (OrderProductVO p : lstQuantity) {
								if (p.getProductId().equals(sod.getProductId())) {
									sod.setOutOfStock(1);
								}
							}
						}
						if(lstChangeQuantity != null){
							for (OrderProductVO p : lstChangeQuantity) {
								if (p.getProductId().equals(sod.getProductId())) {
									sod.setIsChangeQuantity(1);
								}
							}
						}
						if(lstChangeRation != null){
							for (OrderProductVO p : lstChangeRation) {
								if (!StringUtil.isNullOrEmpty(p.getPromotionProgramCode()) 
										&& p.getPromotionProgramCode().equals(sod.getPromotionProgramCode())) {
									sod.setIsChangeRation(1);
								}
							}
						}
						//neu ds sp sai gia rong thi set lai gia dung
						if (lstPrice != null) {
							for (OrderProductVO listOrderProduct : lstPrice) {
								if (listOrderProduct.getProductId().equals(sod.getProductId())) {
									sod.setIsInvalidPrice(1);
								}
							}
						}
						if (lstKeyShop != null) {
							for (OrderProductVO vo : lstKeyShop) {
								if (sod.getProductId().equals(vo.getProductId()) && vo.getKsCode() != null && vo.getKsCode().equals(sod.getProgramCode())) {
									sod.setIsExceedKeyShop(1);
									break;
								}
							}
						}
					}

				} else {
					result.put("footer", Arrays.asList());
				}
				result.put("rows", lst);
			} else {
				result.put("rows", new ArrayList<SaleOrderDetailVOEx>());
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.searchDetail"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Lay danh sach cac don hang de xac nhan
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String searchOrder() {
		result.put("page", page);
		result.put("max", max);
		result.put("rows", new ArrayList<SaleOrderVOEx2>());
		result.put("total", 0);
		try {
			if (currentUser != null && currentUser.getShopRoot() != null) {
				checkCreateShopParamOrderSearch(numberValueOrder,shopId);
				SoFilter filter = new SoFilter();
				if (shopId != null && shopId > 0L) {
					filter.setShopId(shopId);
					filter.setShopTypeId(shopTypeId);
					lockDay = shopLockMgr.getNextLockedDay(shop.getId());
				}
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				if (!StringUtil.isNullOrEmpty(searchStaffCode)) {
					Staff staff = staffMgr.getStaffByCode(searchStaffCode);
					filter.setStaffId(staff.getId());
				}
				filter.setOrderSource(SaleOrderSource.TABLET);
				if (orderType == null) {
					List<OrderType> listOrderTypes = new ArrayList<OrderType>();
					listOrderTypes.add(OrderType.parseValue(OrderType.IN.getValue()));
					listOrderTypes.add(OrderType.parseValue(OrderType.CO.getValue()));
					listOrderTypes.add(OrderType.parseValue(OrderType.SO.getValue()));
					filter.setListOrderType(listOrderTypes);
				} else {
				filter.setListOrderType(Arrays.asList(OrderType.parseValue(orderType)));
				}
				filter.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
				
				filter.setShortCode(customerCode);
				filter.setCustomeName(customerName);
				filter.setDeliveryCode(deliveryStaff);
				if(!StringUtil.isNullOrEmpty(priorityCode)){
					if (!priorityCode.equals("-1")) {
						filter.setPriorityCode(priorityCode);
					}
				}
				if(!StringUtil.isNullOrEmpty(orderSource)){
					Integer sourceValue = Integer.valueOf(orderSource);
					if (sourceValue != null) {
						filter.setOrderSource(SaleOrderSource.parseValue(sourceValue));
					}
				}
				
				filter.setNumberValueOrder(new BigDecimal(numberValueOrder != null ? numberValueOrder : 0));
				filter.setIsValueOrder(isValueOrder);
				
				KPaging<SaleOrderVOEx2> kPaging = new KPaging<SaleOrderVOEx2>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				filter.setkPagingVOEx2(kPaging);
				filter.setStrListUserId(super.getStrListUserId());
				//List<SaleOrderVOEx2> lst = saleOrderMgr.getListSaleOrderVOConfirm(filter);
				ObjectVO<SaleOrderVOEx2> resultVO = saleOrderMgr.getListSaleOrderVOConfirm(filter);
				List<SaleOrderVOEx2> lst = new ArrayList<SaleOrderVOEx2>();
				if (resultVO != null) {
					lst = resultVO.getLstObject();
				}
				if (lst != null && lst.size() > 0) {
					fillListWarning(null,false);//tinh tat ca don hang trong ngay
					StringBuilder mes = new StringBuilder();
					for (SaleOrderVOEx2 so : lst) {
						mes = new StringBuilder();
						mes.append("");
						if (lstQuantity != null) {
							boolean isDividualWarehouse = false;
							List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.SYS_WAREHOUSE_CONFIG, ActiveType.RUNNING);
							if (aparams == null || aparams.size() == 0 || Constant.SYS_WAREHOUSE_CONFIG_DIVIDUAL.equals(aparams.get(0).getValue())) {
								isDividualWarehouse = true;
							}
							for (OrderProductVO vo : lstQuantity) {
								if (so.getId().equals(vo.getSaleOrderId())) {
									if (isDividualWarehouse) {
										if (IsFreeItemInSaleOderDetail.SALE.getValue().equals(vo.getIsFreeItem())) {
											mes.append(R.getResource("confirm.order.and.pay.warning.product.quantity", vo.getProductCode(), vo.getQuantity()));
										} else {
											mes.append(R.getResource("confirm.order.and.pay.warning.product.quantity.km", vo.getProductCode(), vo.getQuantity()));
										}
									} else {
										mes.append(R.getResource("confirm.order.and.pay.warning.product.quantity.dividual", vo.getProductCode(), vo.getQuantity()));
									}
								}
							}
						}
						if (lstPrice != null) {
							for (OrderProductVO vo : lstPrice) {
								if (so.getId().equals(vo.getSaleOrderId())) {
									mes.append(R.getResource("confirm.order.and.pay.warning.price.invalid", vo.getProductCode()));
									if (vo.getPrice() != null) {
										mes.append(R.getResource("confirm.order.and.pay.warning.current.price", StringUtil.convertMoney(vo.getPrice()), StringUtil.convertMoney(vo.getPackagePrice())));
									} else {
										mes.append(R.getResource("confirm.order.and.pay.warning.can.not.get.price"));
									}
								}
							}
						}
						if (lstPromotion != null) {
							for (OrderProductVO vo : lstPromotion) {
								if (so.getId().equals(vo.getSaleOrderId())) {
									mes.append(R.getResource("confirm.order.and.pay.warning.ctkm", vo.getPromotionProgramCode()!=null?vo.getPromotionProgramCode():" "));
								}
							}
						}
						if (lstAmount != null) {
							for (OrderProductVO vo : lstAmount) {
								if (so.getId().equals(vo.getSaleOrderId()) && !vo.getCountSOD().equals(vo.getTotalDetail())) {
									mes.append(R.getResource("confirm.order.and.pay.rot.chi.tiet"));
									break;
								}
							}
						}
						if (lstSaleOrderHasChangedStructurePromotionProgram != null) {
							for (OrderProductVO vo : lstSaleOrderHasChangedStructurePromotionProgram) {
								if (so.getId().equals(vo.getSaleOrderId()) && !StringUtil.isNullOrEmpty(vo.getPromotionProgramCode())) {
									mes.append(R.getResource("confirm.order.and.pay.sale.order.promotion.program.structure.changed", vo.getPromotionProgramCode()));
									break;
								}
							}
						}
						if (lstChangeQuantity != null) {
							for (OrderProductVO vo : lstChangeQuantity) {
								if (so.getId().equals(vo.getSaleOrderId())) {
									mes.append(R.getResource("confirm.order.and.pay.warning.change.quantity"));
									break;
								}
							}
						}
						if (lstChangeRation != null) {
							for (OrderProductVO vo : lstChangeRation) {
								if (so.getId().equals(vo.getSaleOrderId()) && !StringUtil.isNullOrEmpty(vo.getPromotionProgramCode())) {
									mes.append(R.getResource("confirm.order.and.pay.warning.change.ration"));
									break;
								}
							}
						}
						if (lstKeyShop != null) {
							for (OrderProductVO vo : lstKeyShop) {
								if (so.getId().equals(vo.getSaleOrderId())) {
									if (vo.getProductId() != null) {// tra thuong sp vuot muc
										mes.append(R.getResource("confirm.order.and.pay.warning.exceed.ks.product", vo.getProductCode(), vo.getKsCode()));
									} else {//tra thuong tien vuot muc
										mes.append(R.getResource("confirm.order.and.pay.warning.exceed.ks.amount", vo.getKsCode()));
									}
								}
							}
						}
						if (lstNotIntegrity != null) {
							for (OrderProductVO vo : lstNotIntegrity) {
								if (so.getId().equals(vo.getSaleOrderId())) {
									mes.append(R.getResource("confirm.order.and.pay.error.order.not.integrity"));
								}
							}
						}
						so.setMessage(mes.toString());
					}
				}
				try {
					result.put("total", resultVO.getkPaging().getTotalRows());
				} catch (Exception e) {
					result.put("total", 0);
				}
				result.put("rows", lst);
				result.put("footer", Arrays.asList());
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.searchOrder"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Lay danh sach cac don hang de thanh toan
	 * 
	 * @author tungmt ,dh2
	 * @since 29/08/2014
	 */
	public String searchPay() {
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser != null && currentUser.getShopRoot() != null) {
				KPaging<SaleOrderVO> kPaging = new KPaging<SaleOrderVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				SoFilter filter = new SoFilter();
				if (shopId == null) {
					shopId = currentUser.getShopRoot().getShopId();
				}
				filter.setShopId(shopId);
//				List<ShopVO> shopVOParam = new ArrayList<ShopVO>();
				Boolean isShowPrice = false;
				List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
				if (lstPr != null && lstPr.size() > 0){
					if (ConstantManager.TWO_TEXT.equals(lstPr.get(0).getValue())){
						isShowPrice = true;
					}
				}
				/*if ( null != currentUser.getShopRoot().getShopId() ) {
					shopVOParam = shopMgr.getListShopVOConfigShowPrice(currentUser.getShopRoot().getShopId(), 1);
				}
				if ( shopVOParam.size() > 0 ) {	
				
					if (shopVOParam.get(0).getValue() == 2) {
						result.put("isShowPrice",shopVOParam.get(0).getValue());
					}
						
				} else {
					result.put(ERROR, true);
					result.put("errMsg", "vui lòng cấu hình giá cho đơn vị hiện tại, hoặc đơn vị cha");
				}*/
				filter.setLockDay(lockDay);
				if (!StringUtil.isNullOrEmpty(staffCode)){
					Staff st = staffMgr.getStaffByCode(staffCode);
					if (st != null){
						filter.setStaffId(st.getId());
					}
				}
//				filter.setStaffId(nvbhId);
				filter.setkPagingVO(kPaging);
				if (!StringUtil.isNullOrEmpty(sort)) {
					if ( "refOrderNumber".equals(sort) ) {
						sort = " nvl(refOrderNumber,'') ";
					} else if ( "orderNumber".equals(sort) ) {
						sort = " nvl(orderNumber,'') ";
					} else if ( "payReceivedNumber".equals(sort) ) {
						sort = " nvl(payReceivedNumber,'') ";
					} else if ( "staffCode".equals(sort) ) {
						sort = " nvl(staffCode,'')";
					} else if ( "customerCode".equals(sort) ) {
						sort = " nvl(customerCode,'')";
					}else if ( "total".equals(sort) ) {
						sort = " nvl(total,0)";
					}else if ( "discount".equals(sort) ) {
						sort = " nvl(discount,'')";
					}else if ( "amount".equals(sort) ) {
						sort = " nvl(amount,'')";
					}else if ( "payDate".equals(sort) ) {
						sort = " payDate";
					}else if ( "address".equals(sort) ) {
						sort = " nvl(address,'')";
					}else if ( "deliveryCode".equals(sort) ) {
						sort = " nvl(deliveryCode,'')";
					}
				}
				filter.setOrder(order);
				filter.setSort(sort);
				ObjectVO<SaleOrderVO> vo = saleOrderMgr.getListSaleOrderForPay(filter);
				if (vo != null) {
					result.put("total", vo.getkPaging().getTotalRows());
					result.put("rows", vo.getLstObject());
				} else {
					result.put("total", 0);
					result.put("rows", new ArrayList<SaleOrderVO>());
				}
				result.put("isShowPrice", isShowPrice);
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.searchPay"), createLogErrorStandard(actionStartTime));
			result.put("total", 0);
			result.put("rows", new ArrayList<SaleOrderVO>());
		}
		return JSON;
	}
	
	public void getOrderLostDetail(SoFilter filter){
		try{
			lstLostDetail = saleOrderMgr.getListOrderNumberLostDetail(filter);
			if(!StringUtil.isNullOrEmpty(lstLostDetail)){
				String[] str = lstLostDetail.split(","); 
				for(int i = 1 ; i < str.length ; i++){//chay tu 1 vi  ",ABC,DEF"
					if(lstAmount == null){
						lstAmount = new ArrayList<OrderProductVO>();
					}
					OrderProductVO opVO = new OrderProductVO();
					opVO.setSaleOrderId(Long.parseLong(str[i]));
					opVO.setCountSOD(0);
					opVO.setTotalDetail(1);
					lstAmount.add(opVO);
				}
			}
		}catch(Exception ex){
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.getOrderLostDetail"), createLogErrorStandard(actionStartTime));
		}
	}
	
	public void getListOrderNotIntegrity(SoFilter filter) {
		try {
			lstOrderNotIntegrity = saleOrderMgr.checkOrderIntegrity(filter);
			if (!StringUtil.isNullOrEmpty(lstOrderNotIntegrity)) {
				String[] str = lstOrderNotIntegrity.split(",");
				for (int i = 1; i < str.length; i++) {
					if (lstNotIntegrity == null) {
						lstNotIntegrity = new ArrayList<OrderProductVO>();
					}
					OrderProductVO opVO = new OrderProductVO();
					opVO.setSaleOrderId(Long.parseLong(str[i]));
					lstNotIntegrity.add(opVO);
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.getListOrderNotIntegrity"), createLogErrorStandard(actionStartTime));
		}
	}

	/**
	 * Lay danh sach cac san pham sai gia, so luong, CTKM het han va don hang
	 * rot chi tiet
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public void fillListWarning(List<Long> lstOrderId, Boolean isConfirm) {
		try {
			SoFilter filter = new SoFilter();
			filter.setShopId(shopId);
			filter.setShopTypeId(shopTypeId);
			filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
			filter.setLockDay(lockDay);
			filter.setLstSaleOrderId(lstOrderId);
			filter.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
			filter.setOrderSource(SaleOrderSource.TABLET);
			filter.setOrderType(OrderType.IN);
			filter.setIsConfirm(isConfirm);
			List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.SYS_WAREHOUSE_CONFIG, ActiveType.RUNNING);
			if (aparams == null || aparams.size() == 0 || Constant.SYS_WAREHOUSE_CONFIG_DIVIDUAL.equals(aparams.get(0).getValue())) {
				filter.setIsDividualWarehouse(true);
			} else {
				filter.setIsDividualWarehouse(false);
			}
			lstQuantity = saleOrderMgr.getListProductQuantityWarning(filter);
			filter.setIsFreeItem(0);//chi xet gia voi SP ban
			if (Boolean.TRUE.equals(isCheckPrice)){
				lstPrice = saleOrderMgr.getListProductPriceWarning(filter);
			}			
			filter.setIsFreeItem(null);
			lstPromotion = saleOrderMgr.getListProductPromotionWarning(filter);
			this.getOrderLostDetail(filter);
			this.getListOrderNotIntegrity(filter);
			lstSaleOrderHasChangedStructurePromotionProgram = promotionProgramMgr.getSaleOrdersHasChangedStructurePromotionProgramWithFilter(filter);
			lstChangeQuantity = saleOrderMgr.getListProductChangeQuantityWarning(filter);
			lstChangeRation = saleOrderMgr.getListProductChangeRationWarning(filter);
			lstKeyShop = saleOrderMgr.getListKeyShopWarning(filter);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.fillListWarning"), createLogErrorStandard(actionStartTime));
		}
	}
	
	public Boolean checkDenyOrderSO(List<SaleOrder> lstOrder) {
		Boolean flag = true;
		try {
			List<Long> lstOrderSO = new ArrayList<Long>();
			String errDenySO = "";
			String errDenyCO = "";
			String errConfirm = "";
			for (SaleOrder so : lstOrder) {
				if (OrderType.SO.equals(so.getOrderType())) {
					lstOrderSO.add(so.getId());
				} else if (OrderType.CO.equals(so.getOrderType())) {
					if (so.getFromSaleOrder() != null && SaleOrderStatus.APPROVED.equals(so.getFromSaleOrder().getApproved())) {
						errDenyCO += so.getOrderNumber() + ", ";
					} else if (so.getFromSaleOrder() != null && SaleOrderStatus.NOT_YET_APPROVE.equals(so.getFromSaleOrder().getApproved())) {
						errConfirm += so.getOrderNumber() + ", ";
					}
				}
			}
			if (lstOrderSO != null && lstOrderSO.size() > 0) {
				SaleOrderFilter<SaleOrder> filter = new SaleOrderFilter<SaleOrder>();
				filter.setLstFromSaleOrderId(lstOrderSO);
				ObjectVO<SaleOrder> lstCO = saleOrderMgr.getListSaleOrder(filter, null);
				if (lstCO != null && lstCO.getLstObject() != null) {
					for (SaleOrder co : lstCO.getLstObject()) {
						errDenySO +=  co.getOrderNumber() +", ";
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(errDenySO)) {
				errDenySO = errDenySO.substring(0, errDenySO.length()-2);
				result.put("errDenySO", R.getResource("sale.product.confirm.order.errdeny.so", errDenySO));
				flag = false;
			}
			if (!StringUtil.isNullOrEmpty(errDenyCO)) {
				errDenyCO = errDenyCO.substring(0, errDenyCO.length()-2);
				result.put("errDenyCO", R.getResource("sale.product.confirm.order.errdeny.co", errDenyCO));
				flag = false;
			}
			if (!Boolean.TRUE.equals(isCheckOrderDeny) && !StringUtil.isNullOrEmpty(errConfirm)) {
				errConfirm = errConfirm.substring(0, errConfirm.length()-2);
				result.put("errConfirm", R.getResource("sale.product.confirm.order.errdeny.confirm", errConfirm));
				flag = false;
			}
			result.put("error", !flag);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.checkDenyOrderSO"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put("error", true);
			return false;
		}
		return flag;
	}

	/**
	 * Tu choi don hang
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String denyOrder() {
		resetToken(result);
		result.put("error", false);
		try {
			if (shopId != null && lockDay != null && lstOrderId != null && lstOrderId.size() > 0) {
				SoFilter filter = new SoFilter();
				filter.setShopId(shopId);
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				filter.setLstSaleOrderId(lstOrderId);
				filter.setDescription(description);
				filter.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
				List<SaleOrder> lstOrder = saleOrderMgr.getListSaleOrderConfirm(filter);
				if (!checkDenyOrderSO(lstOrder)) {
					return JSON;
				}
				
				//Get list order SO of CO
				List<SaleOrder> lstSOOrder = new ArrayList<>();
				for(SaleOrder so : lstOrder) {
					if(OrderType.CO.equals(so.getOrderType()) &&
							so.getFromSaleOrder() != null && SaleOrderStatus.NOT_YET_APPROVE.equals(so.getFromSaleOrder().getApproved())) {
						lstSOOrder.add(so.getFromSaleOrder());
					}
				}
				
				if(!lstSOOrder.isEmpty()) {
					lstOrder.addAll(lstSOOrder);
				}
					
				if (lstOrder != null && lstOrder.size() > 0) {
					saleOrderMgr.denyListSaleOrder(lstOrder, filter, getLogInfoVO());
				} else if (lstOrderId != null && lstOrderId.size() > 0) {
					result.put("errMsg", R.getResource("comon.not.in.order.type.in"));
					result.put("error", true);
				}
			}
		} catch (Exception ex) {
			result.put("error", true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.denyOrder"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Huy don thanh toan
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String cancelPay() {
		resetToken(result);
		result.put("error", false);
		try {
			if (currentUser != null && shopId != null && lockDay != null && lstPayReceivedId != null && lstPayReceivedId.size() > 0) {
				SoFilter filter = new SoFilter();
				filter.setShopId(shopId);
				filter.setApproval(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				filter.setLstPayReceivedId(lstPayReceivedId);
				filter.setListOrderType(Arrays.asList(OrderType.IN));
				filter.setDescription(description);
				filter.setStaffCode(currentUser.getUserName());
				saleOrderMgr.cancelPay(filter);
			}
		} catch (Exception ex) {
			result.put("error", true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.cancelPay"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Huy don hang
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String cancelOrder() {
		resetToken(result);
		result.put("error", false);
		try {
			if (shopId != null && lockDay != null && lstOrderId != null && lstOrderId.size() > 0) {
				SoFilter filter = new SoFilter();
				filter.setShopId(shopId);
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				filter.setLstSaleOrderId(lstOrderId);
				filter.setDescription(description);
				filter.setListOrderType(Arrays.asList(OrderType.IN));
				if (approvedStep!= null) {
					filter.setApprovedStep(SaleOrderStep.parseValue(approvedStep));
				}
				List<SaleOrder> lstOrder = saleOrderMgr.getListSaleOrderConfirm(filter);
				if (lstOrder != null && lstOrder.size() > 0) {
					saleOrderMgr.cancelListSaleOrder(lstOrder, filter, getLogInfoVO());
				} else if (lstOrderId != null && lstOrderId.size() > 0) {
					result.put("errMsg", R.getResource("comon.not.in.order.type.in"));
					result.put("error", true);
				}
			}
		} catch (Exception ex) {
			result.put("error", true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.cancelOrder"), createLogErrorStandard(actionStartTime));
		}

		return JSON;
	}

	/**
	 * Chap nhan don hang
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Danh sach cac don hang loi khong chap nhan duoc
	 * @exception BusinessException
	 * @see 
	 * @since 29/08/2014
	 * @serial
	 */
	public String confirmOrder() {
		resetToken(result);
		result.put("error", true);
		try {
			if (shopId != null && lockDay != null && lstOrderId != null && lstOrderId.size() > 0) {
				//Lay danh sach san pham vuot ton kho
				String mes = "";
				fillListWarning(lstOrderId,true);
				List<SearchSaleTransactionErrorVO> lstTotalErr = new ArrayList<SearchSaleTransactionErrorVO>();
				Map<Long, SearchSaleTransactionErrorVO> mapErr = new HashMap<Long , SearchSaleTransactionErrorVO>();
				if ((lstQuantity != null && lstQuantity.size() > 0) || (lstPromotion != null && lstPromotion.size() > 0) 
						|| (lstAmount != null && lstAmount.size() > 0) || (lstPrice != null && lstPrice.size() > 0)
						|| (lstChangeQuantity != null && lstChangeQuantity.size() > 0) || (lstChangeRation != null && lstChangeRation.size() > 0)
						|| (lstKeyShop != null && lstKeyShop.size() > 0)) {
					if (lstQuantity != null && lstQuantity.size() > 0) {
						for (OrderProductVO vo : lstQuantity) {
							SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
							if (err == null) {
								err = new SearchSaleTransactionErrorVO();
								err.setId(vo.getSaleOrderId());
								SaleOrder so = saleOrderMgr.getSaleOrderById(vo.getSaleOrderId());
								if (so != null) {
									err.setOrderNumber(so.getOrderNumber());
								}
							}
							err.setQuantityErr(vo.getProductCode());
							mapErr.put(vo.getSaleOrderId(),err);
						}
					} 
					if (lstPromotion != null && lstPromotion.size() > 0) {
						for (OrderProductVO vo : lstPromotion) {
							SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
							if(err == null){
								err = new SearchSaleTransactionErrorVO();
								err.setId(vo.getSaleOrderId());
								SaleOrder so = saleOrderMgr.getSaleOrderById(vo.getSaleOrderId());
								if (so != null) {
									err.setOrderNumber(so.getOrderNumber());
								}
							}
							err.setPromotionErr("X");
							mapErr.put(vo.getSaleOrderId(), err);
						}
					} 
					if (lstAmount != null && lstAmount.size() > 0) {
						for (OrderProductVO vo : lstAmount) {
							if (!vo.getCountSOD().equals(vo.getTotalDetail())) {
								SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
								if(err == null){
									err = new SearchSaleTransactionErrorVO();
									err.setId(vo.getSaleOrderId());
									SaleOrder so = saleOrderMgr.getSaleOrderById(vo.getSaleOrderId());
									if(so != null){
										err.setOrderNumber(so.getOrderNumber());
									}
								}
								err.setAmountErr("X");
								mapErr.put(vo.getSaleOrderId(), err);
							}
						}
					} 
					if (lstPrice != null && lstPrice.size() > 0) {
						for (OrderProductVO vo : lstPrice) {
							SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
							if(err == null){
								err = new SearchSaleTransactionErrorVO();
								err.setId(vo.getSaleOrderId());
								SaleOrder so = saleOrderMgr.getSaleOrderById(vo.getSaleOrderId());
								if(so != null){
									err.setOrderNumber(so.getOrderNumber());
								}
							}
							err.setPriceErr("X");
							mapErr.put(vo.getSaleOrderId(),err);
						}
					}
					if (lstChangeQuantity != null && lstChangeQuantity.size() > 0) {
						for (OrderProductVO vo : lstChangeQuantity) {
							SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
							if(err == null){
								err = new SearchSaleTransactionErrorVO();
								err.setId(vo.getSaleOrderId());
								SaleOrder so = saleOrderMgr.getSaleOrderById(vo.getSaleOrderId());
								if(so != null){
									err.setOrderNumber(so.getOrderNumber());
								}
							}
							err.setChangeQuantity("X");
							mapErr.put(vo.getSaleOrderId(), err);
						}
					}
					if (lstChangeRation != null && lstChangeRation.size() > 0) {
						for (OrderProductVO vo : lstChangeRation) {
							SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
							if(err == null){
								err = new SearchSaleTransactionErrorVO();
								err.setId(vo.getSaleOrderId());
								SaleOrder so = saleOrderMgr.getSaleOrderById(vo.getSaleOrderId());
								if(so != null){
									err.setOrderNumber(so.getOrderNumber());
								}
							}
							err.setChangeRation("X");
							mapErr.put(vo.getSaleOrderId(), err);
						}
					}
					if (lstKeyShop != null && lstKeyShop.size() > 0) {
						for (OrderProductVO vo : lstKeyShop) {
							SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
							if(err == null){
								err = new SearchSaleTransactionErrorVO();
								err.setId(vo.getSaleOrderId());
								SaleOrder so = saleOrderMgr.getSaleOrderById(vo.getSaleOrderId());
								if(so != null){
									err.setOrderNumber(so.getOrderNumber());
								}
							}
							err.setKeyShopErr("X");
							mapErr.put(vo.getSaleOrderId(), err);
						}
					}
					if (lstNotIntegrity != null && lstNotIntegrity.size() > 0) {
						for (OrderProductVO vo : lstNotIntegrity) {
							SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
							if(err == null){
								err = new SearchSaleTransactionErrorVO();
								err.setId(vo.getSaleOrderId());
								SaleOrder so = saleOrderMgr.getSaleOrderById(vo.getSaleOrderId());
								if(so != null){
									err.setOrderNumber(so.getOrderNumber());
								}
							}
							err.setOrderNotIntegrityErr("X");
							mapErr.put(vo.getSaleOrderId(), err);
						}
					}
				}
				
				// bao loi don CO ma don SO chua duyet
				SoFilter filter = new SoFilter();
				filter.setLstSaleOrderId(lstOrderId);
				List<OrderProductVO> lstCOErr = saleOrderMgr.getCONotHaveApprovedSO(filter);
				if (lstCOErr != null && lstCOErr.size() > 0) {
					for (OrderProductVO vo : lstCOErr) {
						SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
						if (err == null){
							err = new SearchSaleTransactionErrorVO();
							err.setId(vo.getSaleOrderId());
							err.setOrderNumber(vo.getOrderNumber());
						}
						err.setSOCOErr("X");
						mapErr.put(vo.getSaleOrderId(),err);
					}
				}
				
				// kiem tra so suat
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(dayLock);
				List<OrderProductVO> lstPortionErr = saleOrderMgr.getOrderNotEnoughPortion(filter);
				if (lstPortionErr != null && lstPortionErr.size() > 0) {
					for (OrderProductVO vo : lstPortionErr) {
						SearchSaleTransactionErrorVO err = mapErr.get(vo.getSaleOrderId());
						if (err == null) {
							err = new SearchSaleTransactionErrorVO();
							err.setId(vo.getSaleOrderId());
							err.setOrderNumber(vo.getOrderNumber());
						}
						err.setNotEnoughPortion("X");
						mapErr.put(vo.getSaleOrderId(), err);
					}
				}
				
				//dungdq3 -- begin
				List<OrderProductVO> listSaleOrderAndCusStatusZero = saleOrderMgr.getListSaleOrderID(lstOrderId);
				if(listSaleOrderAndCusStatusZero != null && listSaleOrderAndCusStatusZero.size() > 0) {
					for(OrderProductVO orderProductVO : listSaleOrderAndCusStatusZero) {
						SearchSaleTransactionErrorVO err = mapErr.get(orderProductVO.getSaleOrderId());
						if (err == null) {
							err = new SearchSaleTransactionErrorVO();
							err.setId(orderProductVO.getSaleOrderId());
							err.setOrderNumber(orderProductVO.getOrderNumber());
						}
						err.setCustomerNotActive("X");
						mapErr.put(orderProductVO.getSaleOrderId(), err);
					}
				}
				//dungdq3 -- end
				
				filter.setShopId(shopId);
				filter.setLockDay(lockDay);
				filter.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
				List<SaleOrder> lstOrder = saleOrderMgr.getListSaleOrderConfirm(filter);
				if (lstOrder != null && lstOrder.size() > 0) {
					List<SaleOrder> lstOrderConfirm = new ArrayList<SaleOrder>();
					List<Long> lstPassOrderId = new ArrayList<Long>();
//					boolean b = true;
					for (SaleOrder so : lstOrder) {
						if(!OrderType.CO.equals(so.getOrderType()) && !OrderType.SO.equals(so.getOrderType())){// don SO, CO ko check han muc KH
							SearchSaleTransactionErrorVO errVo = checkCustomer(so);
							if (!StringUtil.isNullOrEmpty(errVo.getDebitMax()) || !StringUtil.isNullOrEmpty(errVo.getDebitMaxDate())) {
								SearchSaleTransactionErrorVO err = mapErr.get(errVo.getId());
								if(err == null){
									err = new SearchSaleTransactionErrorVO();
									err.setId(errVo.getId());
									err.setOrderNumber(errVo.getOrderNumber());
								}
								err.setDebitMax(errVo.getDebitMax());
								err.setDebitMaxDate(errVo.getDebitMaxDate());
								mapErr.put(errVo.getId(),err);
							}
						}
//						b = saleOrderMgr.checkPromotionOpenOfSaleOrder(so.getId(), so.getCustomer().getId(), dayLock, lstPassOrderId);
//						if (!b) {
//							SearchSaleTransactionErrorVO err = mapErr.get(so.getId());
//							if(err == null){
//								err = new SearchSaleTransactionErrorVO();
//								err.setId(so.getId());
//								err.setOrderNumber(so.getOrderNumber());
//							}
//							err.setOpenPromo("X");
//							mapErr.put(err.getId(),err);
//						}
						
//						// xu ly khuyen mai tich luy
//						b = saleOrderMgr.checkAccumulativePromotionOfSaleOrder(so.getId(), dayLock, lstPassOrderId);
//						if (!b) {
//							SearchSaleTransactionErrorVO err = mapErr.get(so.getId());
//							if(err == null){
//								err = new SearchSaleTransactionErrorVO();
//								err.setId(so.getId());
//								err.setOrderNumber(so.getOrderNumber());
//							}
//							err.setAccumulation("X");
//							mapErr.put(err.getId(),err);
//						}
						
						if (mapErr.get(so.getId()) == null) {
							lstPassOrderId.add(so.getId());
						}
					}
					for (SaleOrder so : lstOrder) {
						if (mapErr.get(so.getId()) == null) {
							lstOrderConfirm.add(so);
						}
					}
					List<SearchSaleTransactionErrorVO> lstErr = null;
					lstErr = saleOrderMgr.confirmListSaleOrder(lstOrderConfirm, description, getLogInfoVO());
					if(lstErr != null && lstErr.size() > 0){
						for(SearchSaleTransactionErrorVO errVo: lstErr){
							//loai don loi de thong bao cac don thanh cong
							for(int i = 0 ; i < lstOrderConfirm.size() ; i++){
								if(lstOrderConfirm.get(i).getId().equals(errVo.getId())){
									lstOrderConfirm.remove(i);
									lstTotalErr.add(errVo);
									break;
								}
							}
						}
					}
					for (SaleOrder so : lstOrder) {//order theo order_date (lstOrder da order roi nen add list theo lstOrder)
						SearchSaleTransactionErrorVO err = mapErr.get(so.getId());
						if(err != null){
							lstTotalErr.add(err);
						}
					}
					result.put("lstErrors", lstTotalErr);
					if(lstOrderConfirm != null && lstOrderConfirm.size() > 0 ){
						String successOrder = lstOrderConfirm.get(0).getOrderNumber();
						for(int i = 1 ; i < lstOrderConfirm.size() ; i++){
							successOrder += ", "+lstOrderConfirm.get(i).getOrderNumber();
						}
						result.put("lstSuccess", successOrder);
					}
					result.put("isConfirm", true);
					if (!StringUtil.isNullOrEmpty(mes)) {
						result.put("errMsg", mes);
					}
					result.put("error", false);
				}else{
					result.put("errMsg","DON_HANG_DA_XU_LY"); //R.getResource("common.sale.order.don.hang.da.xu.ly"));
				}
			}
		} catch (Exception e) {
			result.put("error", true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			if(e.getMessage() != null && e.getMessage().equals("DON_HANG_DA_XU_LY")){
				result.put("errMsg","DON_HANG_DA_XU_LY"); //R.getResource("common.sale.order.don.hang.da.xu.ly"));
			}
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.confirmOrder"), createLogErrorStandard(actionStartTime));
		}

		return JSON;
	}

	/**
	 * Chap nhan thanh toan
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String confirmPay() {
		result.put("error", true);
		try {
			if (shop == null) {
				result.put("error", true);
				result.put("errMsg", R.getResource("common.shop.param.not.exist", currentShopCode));
				return JSON;
			}
			if (shopId != null && lockDay != null && lstPayReceivedId != null && lstPayReceivedId.size() > 0) {
				SoFilter filter = new SoFilter();
				filter.setShopId(shopId);
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				//				filter.setLstSaleOrderId(lstOrderId);
				filter.setLstPayReceivedId(lstPayReceivedId);
				ObjectVO<SaleOrderVO> vo = saleOrderMgr.getListSaleOrderForPay(filter);
				if (vo != null && vo.getLstObject().size() > 0) {
					List<Long> lstPRId = new ArrayList<Long>();
					BigDecimal amount = null;
					BigDecimal remain = null;
					BigDecimal discount = null;
					for (SaleOrderVO sov : vo.getLstObject()) {
						// lacnv1 - debit_detail
						remain = sov.getRemain().abs();
						amount = sov.getAmount();
						discount = sov.getDiscount();
						amount = amount.add(discount);
						amount = amount.abs();
						// --
						if (remain.compareTo(amount) >= 0) {
							lstPRId.add(sov.getPayReceivedId());
						} else {
							// lacnv1 - debit_detail
							result.put("error", true);
							result.put("errMsg", R.getResource("confirm.order.thanh.toan.vuot.qua.no"));
							return JSON;
							// --
						}
					}
					filter.setLstPayReceivedId(lstPRId);
					saleOrderMgr.confirmPayListSaleOrder(filter, description, getLogInfoVO());
				}
				result.put("error", false);
			}
		} catch (Exception ex) {
			result.put("error", true);
			String s = ex.getMessage();
			if (s.contains("TOTAL_PAY_MORE_THAN_TOTAL")) {
				int idx = s.indexOf("TOTAL_PAY_MORE_THAN_TOTAL");
				s = s.substring(idx);
				s = s.replace("TOTAL_PAY_MORE_THAN_TOTAL", "");
				result.put("errMsg", R.getResource("confirm.order.thanh.toan.vuot.qua.no2", s));
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.confirmPay"), createLogErrorStandard(actionStartTime));
			}
		}

		return JSON;
	}
	
	/**
	 * tinh khuyen mai
	 * 
	 * @author tungmt
	 * @since 11/10/2014
	 */
	public String payment(SaleOrder so){
		try {
			Map<Long, List<SaleOrderLot>> mapSaleOrderLot = new HashMap<Long, List<SaleOrderLot>>();
			//List<SaleOrderLot> listSaleOrderLot = null;
			List<SaleOrderDetailVO> lstSalesOrderDetailVO = new ArrayList<SaleOrderDetailVO>();
			//PromotionProductsVO productsVO = null;
			long channelTypeId = 0;
			if (so.getCustomer().getChannelType() != null) {
				channelTypeId = so.getCustomer().getChannelType().getId();
			}
			
			//Tinh KM
			SaleOrderVO saleOrderVO = new SaleOrderVO();
			CalculatePromotionFilter calculatePromotionFilter = new CalculatePromotionFilter();
			calculatePromotionFilter.setMapSaleOrderLot(mapSaleOrderLot);
			calculatePromotionFilter.setLstSalesOrderDetail(lstSalesOrderDetailVO);
			if (so.getShop() != null) {
				calculatePromotionFilter.setShopId(so.getShop().getId());
				calculatePromotionFilter.setShopChannel(so.getShop().getShopChannel());
			}
			if (so.getStaff() != null) {
				calculatePromotionFilter.setStaffId(so.getStaff().getId());
			}
			if (so.getCustomer() != null) {
				calculatePromotionFilter.setCustomerId(so.getCustomer().getId());
			}
			calculatePromotionFilter.setCustomerTypeId(channelTypeId);
			calculatePromotionFilter.setOrderDate(lockDay);
			calculatePromotionFilter.setSaleOrderId(null);
			calculatePromotionFilter.setOrderVO(saleOrderVO);
			promotionProgramMgr.calculatePromotionProducts2(calculatePromotionFilter);
//			/*productsVO = */promotionProgramMgr.calculatePromotionProducts2(mapSaleOrderLot, lstSalesOrderDetailVO,
//					so.getShop().getId(), so.getShop().getShopChannel(), so.getStaff().getId(),
//					so.getCustomer().getId(), channelTypeId, lockDay, null, saleOrderVO);
		} catch (BusinessException ex) {
			result.put("error", true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.payment"), createLogErrorStandard(actionStartTime));
		}
		
		return JSON;
	}

	/**
	 * apply gia moi
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String applyNewPrice() {
		resetToken(result);
		result.put("error", false);
		try {
			if (shopId != null && lockDay != null && lstOrderId != null && lstOrderId.size() > 0) {
				SoFilter filter = new SoFilter();
				filter.setShopId(shopId);
				filter.setShopTypeId(shopTypeId);
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				filter.setLstSaleOrderId(lstOrderId);
				filter.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
				filter.setOrderSource(SaleOrderSource.TABLET);
				filter.setOrderType(OrderType.IN);
//				filter.setIsFreeItem(0);
				List<SaleOrderDetail> lst = saleOrderMgr.getListSaleOrderDetailConfirm(filter);
				lstPrice = saleOrderMgr.getListProductPriceWarning(filter);
				if (lst != null && lst.size() > 0 && lstPrice != null && lstPrice.size() > 0) {
					List<SaleOrderDetail> lstUpdate = new ArrayList<SaleOrderDetail>();
					Date updateDate = commonMgr.getSysDate();
					List<SaleOrder> lstSOChangePrice = new ArrayList<SaleOrder>();
					for (SaleOrderDetail sod : lst) {
						for (OrderProductVO vo : lstPrice) {
							if (sod.getProduct() != null && vo.getProductId().equals(sod.getProduct().getId())) {
								sod.setPriceValue(vo.getPrice());
								sod.setPriceNotVat(vo.getPriceNotVat());
								sod.setVat(vo.getVat());
								sod.setPrice(productMgr.getPriceById(vo.getPriceId()));
								sod.setUpdateDate(updateDate);
								sod.setUpdateUser(currentUser.getUserName());
								if(sod.getIsFreeItem()!=null && sod.getIsFreeItem()==0){
									sod.setAmount(sod.getPriceValue().multiply(new BigDecimal(sod.getQuantity())));
								}
								lstUpdate.add(sod);
								break;
							}
						}
						//tinh lai amount cho saleOrder
						Boolean flag=true;
						for(SaleOrder so:lstSOChangePrice){
							if(so.getId().equals(sod.getSaleOrder().getId())){
								if(sod.getIsFreeItem()!=null && sod.getIsFreeItem()==0){
									so.setAmount(so.getAmount().add(sod.getAmount()));
								}
								flag=false;
								break;
							}
						}
						if(flag){
							SaleOrder so=sod.getSaleOrder().clone();
							so.setId(sod.getSaleOrder().getId());
							so.setAmount(sod.getAmount());
							lstSOChangePrice.add(so);
						}
					}
					if (lstUpdate.size() > 0) {
						saleOrderMgr.updateListSaleOrderDetail(lstUpdate);
						for(SaleOrder so:lstSOChangePrice){
							SaleOrder soUpdate = saleOrderMgr.getSaleOrderById(so.getId());
							if(soUpdate!=null){
								soUpdate.setAmount(so.getAmount());
								soUpdate.setTotal(soUpdate.getAmount().subtract(soUpdate.getDiscount()));
								saleOrderMgr.updateSaleOrder(soUpdate);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			result.put("error", true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.applyNewPrice"), createLogErrorStandard(actionStartTime));
		}

		return JSON;
	}

	/**
	 * Check han muc no cua khach hang theo don hang
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public SearchSaleTransactionErrorVO checkCustomer(SaleOrder so) {
		SearchSaleTransactionErrorVO errorVO = new SearchSaleTransactionErrorVO();
		try {
			/** KH ap dung muc no , Tinh so muc no toi da */
			if (so.getCustomer().getApplyDebitLimited() != null && so.getCustomer().getApplyDebitLimited() == 1 && !BigDecimal.ZERO.equals(so.getAmount())) {
				errorVO.setId(so.getId());
				errorVO.setOrderNumber(so.getOrderNumber());
				errorVO.setCustomerCode(so.getCustomer().getCustomerCode());
				errorVO.setCustomerName(so.getCustomer().getCustomerName());
				Debit debitCustomer = debitMgr.getDebit(so.getCustomer().getId(), DebitOwnerType.CUSTOMER);
				if (debitCustomer != null) {
					if (debitCustomer.getTotalDebit() == null)
						debitCustomer.setTotalDebit(BigDecimal.ZERO);
					BigDecimal totalDebit = debitCustomer.getTotalDebit().add(so.getAmount());
					if (so.getCustomer().getMaxDebitAmount().intValue() != -1 && so.getCustomer().getMaxDebitAmount().compareTo(totalDebit) < 0) {
						errorVO.setDebitMax("X");
					}
					Integer debitDate = saleOrderMgr.getOrderDebitMaxCustomer(so, lockDay);
					if (debitDate != null && so.getCustomer().getMaxDebitDate() != -1 && debitDate > so.getCustomer().getMaxDebitDate()) {
						errorVO.setDebitMaxDate("X");
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.SearchSaleTransactionErrorVO"), createLogErrorStandard(actionStartTime));
		}
		return errorVO;
	}

	/**
	 * Xac nhan don hang ko can check han muc khach hang
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String skipDebitCustomer() {
		resetToken(result);
		result.put("error", false);
		try {
			if (shopId != null && lockDay != null && lstOrderId != null && lstOrderId.size() > 0) {
				SoFilter filter = new SoFilter();
				filter.setShopId(shopId);
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				filter.setLstSaleOrderId(lstOrderId);
				filter.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
				List<SaleOrder> lstOrder = saleOrderMgr.getListSaleOrderConfirm(filter);
				if (lstOrder != null && lstOrder.size() > 0) {
					saleOrderMgr.confirmListSaleOrder(lstOrder, description, getLogInfoVO());
				}
			}
		} catch (Exception e) {
			result.put("error", true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.skipDebitCustomer"), createLogErrorStandard(actionStartTime));
		}

		return JSON;
	}
	
	/**
	 * Xac nhan don hang ko can check loi
	 * 
	 * @author tungmt
	 * @since 29/08/2014
	 */
	public String acceptOrderErr() {
		resetToken(result);
		result.put("error", false);
		try {
			Shop sh = null;
			if (!StringUtil.isNullOrEmpty(shopCode)){
				sh = shopMgr.getShopByCode(shopCode);
			}
			if (sh == null){
				result.put("error", true);
				result.put("errMsg", R.getResource("common.shop.param.not.exist", shopCode));
				return JSON;
			}
			shopId = sh.getId();
			if (shopId != null && lockDay != null && lstOrderId != null && lstOrderId.size() > 0) {
				SoFilter filter = new SoFilter();
				filter.setShopId(shopId);
				filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
				filter.setLockDay(lockDay);
				filter.setLstSaleOrderId(lstOrderId);
				filter.setApprovedStep(SaleOrderStep.NOT_YET_CONFIRM);
				List<SaleOrder> lstOrder = saleOrderMgr.getListSaleOrderConfirm(filter);
				if (lstOrder != null && lstOrder.size() > 0) {
					saleOrderMgr.confirmListSaleOrder(lstOrder, description, getLogInfoVO());
				}
			}
		} catch (Exception e) {
			result.put("error", true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.acceptOrderErr"), createLogErrorStandard(actionStartTime));
		}

		return JSON;
	}
	
	/**
	 * lay ds nv theo shop don vi
	 * @author trietptm
	 * @return
	 * @throws Exception
	 */
	public String loadSellStaffByShopCode() {
		try{
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			if (shopId != null){
				StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
				filter.setIsLstChildStaffRoot(false);
				filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
				filter.setStatus(ActiveType.RUNNING.getValue());
				filter.setLstShopId(Arrays.asList(shopId));
				//Lay NVBH
				filter.setObjectType(StaffSpecificType.STAFF.getValue());
				filter.setIsGetChildShop(false);
				filter.setUserId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());	
				ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
				lstNVBHVo = objVO.getLstObject();
			}
			result.put("lstNVBHVo", lstNVBHVo);
		}catch(Exception ex){
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.ConfirmOrderAction.loadSellStaffByShopCode"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Long getNvbhId() {
		return nvbhId;
	}

	public void setNvbhId(Long nvbhId) {
		this.nvbhId = nvbhId;
	}

	public List<Staff> getLstNVBH() {
		return lstNVBH;
	}

	public void setLstNVBH(List<Staff> lstNVBH) {
		this.lstNVBH = lstNVBH;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public List<Long> getLstOrderId() {
		return lstOrderId;
	}

	public void setLstOrderId(List<Long> lstOrderId) {
		this.lstOrderId = lstOrderId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSkipDebit() {
		return skipDebit;
	}

	public void setSkipDebit(Integer skipDebit) {
		this.skipDebit = skipDebit;
	}

	public List<Long> getLstPayReceivedId() {
		return lstPayReceivedId;
	}

	public void setLstPayReceivedId(List<Long> lstPayReceivedId) {
		this.lstPayReceivedId = lstPayReceivedId;
	}

	public Integer getApprovedStep() {
		return approvedStep;
	}

	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getListNVGH() {
		return listNVGH;
	}

	public void setListNVGH(List<StaffVO> listNVGH) {
		this.listNVGH = listNVGH;
	}

	public List<ApParam> getPriority() {
		return priority;
	}

	public void setPriority(List<ApParam> priority) {
		this.priority = priority;
	}

	public String getPriorityCode() {
		return priorityCode;
	}

	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}

	public String getDeliveryStaff() {
		return deliveryStaff;
	}

	public void setDeliveryStaff(String deliveryStaff) {
		this.deliveryStaff = deliveryStaff;
	}

	public Integer getConfirmAccumulation() {
		return confirmAccumulation;
	}

	public void setConfirmAccumulation(Integer confirmAccumulation) {
		this.confirmAccumulation = confirmAccumulation;
	}

	public String getLstLostDetail() {
		return lstLostDetail;
	}

	public void setLstLostDetail(String lstLostDetail) {
		this.lstLostDetail = lstLostDetail;
	}

	public String getNumberValue() {
		return numberValue;
	}

	public void setNumberValue(String numberValue) {
		this.numberValue = numberValue;
	}

	public Long getNumberValueOrder() {
		return numberValueOrder;
	}

	public void setNumberValueOrder(Long numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}

	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getCurrentShopCode() {
		return currentShopCode;
	}

	public void setCurrentShopCode(String currentShopCode) {
		this.currentShopCode = currentShopCode;
	}

	public String getSearchStaffCode() {
		return searchStaffCode;
	}

	public void setSearchStaffCode(String searchStaffCode) {
		this.searchStaffCode = searchStaffCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Boolean getIsCheckPrice() {
		return isCheckPrice;
	}

	public void setIsCheckPrice(Boolean isCheckPrice) {
		this.isCheckPrice = isCheckPrice;
	}
	
	public Long getShopTypeId() {
		return shopTypeId;
	}

	public void setShopTypeId(Long shopTypeId) {
		this.shopTypeId = shopTypeId;
	}

	public Boolean getIsCheckOrderDeny() {
		return isCheckOrderDeny;
	}

	public void setIsCheckOrderDeny(Boolean isCheckOrderDeny) {
		this.isCheckOrderDeny = isCheckOrderDeny;
	}

}

package ths.dms.web.action.saleproduct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CarMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class CreateSaleOrderVansaleAction extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CarMgr carMgr;
	private StockMgr stockMgr;
	private SaleOrderMgr saleOrderMgr;
	private ProductMgr productMgr;
	private ApParamMgr apParamMgr;
	private List<Car> listCars;
	private List<Staff> listStaff;
	private Shop shop;
	private Date sysDate;
	private Date lockDay;
	private Integer isShow;
	private String userName;
	private String saleStaffCode;
	private String code;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		carMgr = (CarMgr)context.getBean("carMgr");
		stockMgr = (StockMgr) context.getBean("stockMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		productMgr = (ProductMgr)context.getBean("productMgr");
		apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
		try {
			staff = getStaffByCurrentUser();
			if(staff!=null && staff.getShop()!=null){
				shop = staff.getShop();	
			}			
			if(commonMgr!=null){
				sysDate = commonMgr.getSysDate();
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}	
		/** Kiem tra shop da chot ngay hay chua
		 *  Neu co thi luu ngay don hang la ngay chot
		 * */
		lockDay = shopLockMgr.getNextLockedDay(shop.getId());
		if(lockDay == null){
			lockDay = sysDate;
		}
	};
	public String getPageDB(){
		try{
			isShow = DateUtil.compareTwoDate(sysDate, lockDay);
			if(isShow == 0){
				code = stockMgr.generateStockTransCodeVansale(getCurrentShop().getId(),"DB");
				userName = getCurrentUser().getUserName();
				listCars = new ArrayList<Car>();
				ObjectVO<Car> lstCarsVO = carMgr.getListCar(null, shop.getId(), null, null, null, null, null, null, ActiveType.RUNNING, false);	
				if(lstCarsVO!=null){
					listCars = lstCarsVO.getLstObject();
				}
				ObjectVO<Staff> staffVO = staffMgr.getListPreAndVanStaff(null, null, null, shop.getId(), 
						ActiveType.RUNNING, Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
				if(staffVO!=null && staffVO.getLstObject().size()>0){
					listStaff = staffVO.getLstObject();
				}
			}
		}catch(Exception ex){
			
		}
		return SUCCESS;
	}
	public String getSaleProductStaff(){
		try{
			Staff saleStaff = null;
			if(saleStaffCode != null){
				saleStaff = staffMgr.getStaffByCode(saleStaffCode);
				if(saleStaff == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, saleStaffCode));
					return JSON;
				}
			}else{
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE,"Nhân viên"));
			}
			
			if(saleStaff != null) {
				List<OrderProductVO> listOrderProduct = saleOrderMgr.getListOrderProductVansale(null, shop.getId(), saleStaff.getId(), null, null,lockDay).getLstObject();
				Map<String, List<ProductLot>> mapProductLot = null;
//				if(listOrderProduct != null){
//					mapProductLot = new HashMap<String, List<ProductLot>>();
//					for(OrderProductVO pro: listOrderProduct){
//						List<ProductLot> lstProLot = getProductLotEx(pro.getProductId());
//						if(lstProLot != null && lstProLot.size() > 0){
//							mapProductLot.put(pro.getProductCode(), lstProLot);
//						}
//					}
//				}
				result.put("listSaleProduct", listOrderProduct);
				result.put("mapProductLot", mapProductLot);
				result.put(ERROR, false);
			}
		}
		catch(Exception ex){
			
		}
		return JSON;
	}
	public List<ProductLot> getProductLotEx(Long id) throws Exception{
		List<ProductLot> listProductLot = new ArrayList<ProductLot>();
		List<ProductLot> __listProductLot = new ArrayList<ProductLot>();
		listProductLot=productMgr.getProductLotByProductAndOwner(id, shop.getId(), StockObjectType.SHOP);
		Integer NUM_DAY_LOT_EXPIRE  = 0;				
		ApParam ap = apParamMgr.getApParamByCodeEx(ConstantManager.AP_PARAM_CODE_NUM_DAY_LOT_EXPIRE, ActiveType.RUNNING);
		if(ap!=null){
			NUM_DAY_LOT_EXPIRE = Integer.valueOf(ap.getValue());
		}			
		for(ProductLot pl: listProductLot){				
			if(NUM_DAY_LOT_EXPIRE>=0){
				Date moveDate = DateUtil.moveDate(DateUtil.now(), NUM_DAY_LOT_EXPIRE, DateUtil.DATE_MONTH);
				if(pl.getExpiryDate()!=null && DateUtil.compareDateWithoutTime(pl.getExpiryDate(), moveDate)>=0){
					__listProductLot.add(pl);
				}
			}else{
				__listProductLot.add(pl);
			}
		}
		return __listProductLot;
	}
	public List<Car> getListCars() {
		return listCars;
	}
	public void setListCars(List<Car> listCars) {
		this.listCars = listCars;
	}
	public List<Staff> getListStaff() {
		return listStaff;
	}
	public void setListStaff(List<Staff> listStaff) {
		this.listStaff = listStaff;
	}
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public Date getSysDate() {
		return sysDate;
	}
	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}
	public Date getLockDay() {
		return lockDay;
	}
	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}
	public Integer getIsShow() {
		return isShow;
	}
	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSaleStaffCode() {
		return saleStaffCode;
	}
	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}
	
	
}

package ths.dms.web.action.saleproduct;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.vo.SaleOrderDetailGroupVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class ConfirmOrderFromTabletAction extends AbstractAction {
	private String poNumber;
	private Integer approvedChoose;
	private String customerCode;
	private String customerName;
	private Long staffId;
	private Integer prorityChoose;
	private List<StatusBean> listApproved;
	private List<ApParam> listPriority;
	private List<SaleOrder> listPO;
	private List<SaleOrderDetailVOEx> listSaleOrder;
	private long saleOrderId;
	private Shop shop;
	private String shopCode;
	private List<String> listRedRow;
	private List<Long> listAccept;
	private List<Long> listRefuse;
	private List<Long> notAvailable;
	private String note;
	
	private Date now;
	
	ApParamMgr apParamMgr;
	SaleOrderMgr saleOrderMgr;
	CommonMgr commonMgr;
	StaffMgr staffMgr;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		
		
		
		now = commonMgr.getSysDate();
		
		listApproved = new ArrayList<StatusBean>();
		//-1: don hang tao tren tablet yeu cau xac nhan
		StatusBean confirmApprove = new StatusBean();
		confirmApprove.setName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "confirm.order.from.table.approve"));
		confirmApprove.setValue((long)SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM.getValue());
		listApproved.add(confirmApprove);
		//-2: don hang tao tren tablet chua yeu cau xac nhan
		StatusBean notConfirmApprove = new StatusBean();
		notConfirmApprove.setName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "confirm.order.from.table.not.approve"));
		notConfirmApprove.setValue((long)SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM.getValue());
		listApproved.add(notConfirmApprove);
		
		listPriority = apParamMgr.getListApParam(ApParamType.ORDER_PIRITY, ActiveType.RUNNING);
		
		Staff staff = null;
		if(currentUser != null && currentUser.getUserName() != null){
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			if(staff != null && staff.getShop() != null){
				shop = staff.getShop();
			}
		}
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		Staff staff = null;
		if(currentUser != null && currentUser.getUserName() != null){
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
			if(staff != null && staff.getShop() != null){
				shop = staff.getShop();
			}
		}
		if(shop == null) {
			return ERROR;
		}
		listPO = saleOrderMgr.getListSaleOrderFromTabletByCondition(null, 
				SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM, 
				null, null, null, null, now, now);
		return SUCCESS;
	}
	
	public String searchPOConfirmFromTablet() {
		resetToken(result);
		try {
			Staff staff = null;
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
				}
			}
			if(shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			listPO = saleOrderMgr.getListSaleOrderFromTabletByCondition(poNumber, 
					approvedChoose == null ? SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM : SaleOrderStatus.parseValue(approvedChoose), 
					StringUtil.isNullOrEmpty(customerCode)?null:StringUtil.getFullCode(shopCode, customerCode),
					//customerCode == null ? null : customerCode + (shopCode == null ? "" : shopCode), @tientv11: thay doi 
					customerName, staffId, prorityChoose == -1 ? null : prorityChoose, now, now);
			return SUCCESS;
		} catch (Exception e) {
			LogUtility.logError(e, "ConfirmOrderFromTabletAction.searchPOConfirmFromTablet");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}

	public String getSaleOrderDetail() {
		resetToken(result);
		try {
			Staff staff = null;
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
				}
			}
			if(shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			/*
			 * comment code loi compile
			 * @modified by tuannd20
			 * @date 20/12/2014
			 */
			//listSaleOrder = saleOrderMgr.getListSaleOrderDetailVOEx(null, saleOrderId, null, null, null).getLstObject();
			List<SaleOrderDetailGroupVO> listSaleOrderDetailGroup = saleOrderMgr.getListSaleOrderDetailGroupVO(null, saleOrderId, null).getLstObject();
			listRedRow = new ArrayList<String>();
			for(SaleOrderDetailGroupVO sodgVO : listSaleOrderDetailGroup) {
				if(sodgVO.getQuantity() > sodgVO.getStockQuantity()) {
					listRedRow.add(sodgVO.getProductCode());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ConfirmOrderFromTabletAction.getSaleOrderDetail");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String checkStockAvaiable() {
		try {
			resetToken(result);
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
				}
			}
			if(shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			List<Product> listRefuseProduct = saleOrderMgr.checkEnoughProductQuantityInListOrder(shop.getId(), listAccept);
			if(listRefuseProduct == null || listRefuseProduct.size() == 0) {
				result.put("isAvaiable", true);
				result.put(ERROR, false);
			} else {
				result.put("isAvaiable", false);
				result.put("listRefuseProduct", listRefuseProduct);
				result.put(ERROR, false);
				return ERROR;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ConfirmOrderFromTabletAction.getSaleOrderDetail");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}
	
	
	/*//HuyNP4
	public String checkQuantityInListOrder() {
		try {
			resetToken(result);
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
				}
			}
			if(shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			List<Product> listRefuseProduct = saleOrderMgr.checkQuantityCheckAndMax(saleOrder);
			if(listRefuseProduct == null || listRefuseProduct.size() == 0) {
				result.put("isAvaiable", true);
				result.put(ERROR, false);
			} else {
				result.put("isAvaiable", false);
				result.put("listRefuseProduct", listRefuseProduct);
				result.put(ERROR, false);
				return ERROR;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ConfirmOrderFromTabletAction.getSaleOrderDetail");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return SUCCESS;
	}*/
	
	public String accept() {
		try {
			resetToken(result);
			Staff staff = null;
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
				}
			}
			if(shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			listPO = saleOrderMgr.acceptSaleOrderFromTable(listAccept, getLogInfoVO());
			result.put(ERROR, false);
			result.put("listRefuse", listPO);
			return SUCCESS;
		} catch (Exception e) {
			LogUtility.logError(e, "ConfirmOrderFromTabletAction.search");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		
	}
	
	public String refuse() {
		try {
			resetToken(result);
			Staff staff = null;
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
				}
			}
			if(shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			saleOrderMgr.cancelSaleOrderFromTable(listRefuse, note);
			return SUCCESS;
		} catch (Exception e) {
			LogUtility.logError(e, "ConfirmOrderFromTabletAction.search");
			
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}



	public String getCustomerName() {
		return customerName;
	}



	public String getPoNumber() {
		return poNumber;
	}



	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}



	public Integer getApprovedChoose() {
		return approvedChoose;
	}



	public void setApprovedChoose(Integer approvedChoose) {
		this.approvedChoose = approvedChoose;
	}



	public String getCustomerCode() {
		return customerCode;
	}



	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}



	public Long getStaffId() {
		return staffId;
	}



	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}



	public Integer getProrityChoose() {
		return prorityChoose;
	}



	public void setProrityChoose(Integer prorityChoose) {
		this.prorityChoose = prorityChoose;
	}



	public List<StatusBean> getListApproved() {
		return listApproved;
	}



	public void setListApproved(List<StatusBean> listApproved) {
		this.listApproved = listApproved;
	}



	public List<ApParam> getListPriority() {
		return listPriority;
	}



	public void setListPriority(List<ApParam> listPriority) {
		this.listPriority = listPriority;
	}



	public void setListPO(List<SaleOrder> listPO) {
		this.listPO = listPO;
	}



	public List<SaleOrder> getListPO() {
		return listPO;
	}



	public void setSaleOrderId(long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}



	public long getSaleOrderId() {
		return saleOrderId;
	}



	public void setListSaleOrder(List<SaleOrderDetailVOEx> listSaleOrder) {
		this.listSaleOrder = listSaleOrder;
	}



	public List<SaleOrderDetailVOEx> getListSaleOrder() {
		return listSaleOrder;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setListRedRow(List<String> listRedRow) {
		this.listRedRow = listRedRow;
	}

	public List<String> getListRedRow() {
		return listRedRow;
	}

	public List<Long> getListAccept() {
		return listAccept;
	}

	public void setListAccept(List<Long> listAccept) {
		this.listAccept = listAccept;
	}

	public List<Long> getListRefuse() {
		return listRefuse;
	}

	public void setListRefuse(List<Long> listRefuse) {
		this.listRefuse = listRefuse;
	}

	public List<Long> getNotAvailable() {
		return notAvailable;
	}

	public void setNotAvailable(List<Long> notAvailable) {
		this.notAvailable = notAvailable;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getNote() {
		return note;
	}
}

package ths.dms.web.action.saleproduct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.PrintOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.ConfirmFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderInvoiceVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PrintOrderVO;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class PrintOrderAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	//ApParamMgr apParamMgr;
	SaleOrderMgr saleOrderMgr;
	//CommonMgr commonMgr;
	//StaffMgr staffMgr;
	//private PromotionProgramMgr promotionProgramMgr;
	private StockMgr stockMgr;	
	//private ProductMgr productMgr;
	//private DebitMgr debitMgr;
	private ExceptionDayMgr exceptionDayMgr;
	
	private Date lockDay;
	private List<Long> lstOrderId;
	private List<String> lstOrderNumber;
	private Long saleOrderId;
	private Long shopId;
	private Long nvbhId;
	private String nvbhCode;
	private Long nvghId;
	private String nvghCode;
	private List<Staff> lstNVBH;
	private List<StaffVO> lstNVBHVo;//Danh sach NVBH
	private List<Staff> lstNVGH;
	private List<StaffVO> lstNVGHVo;//Danh sach NVGH
	private String refOrderNumber;
	private Integer orderSource;
	private String fromDate;
	private String toDate;
	private String currentShopCode;
	private String customerCode;
	private String customerName;
	private String orderType;
	private List<Long> listId;
	private List<Long> lstRemovedInvoice;
	private String numberValue;
	private Long numberValueOrder;
	private Integer isValueOrder;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		//apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		exceptionDayMgr = (ExceptionDayMgr) context.getBean("exceptionDayMgr");
		stockMgr = (StockMgr) context.getBean("stockMgr");
		//commonMgr = (CommonMgr) context.getBean("commonMgr");
		//staffMgr = (StaffMgr) context.getBean("staffMgr");
		//promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
		
		//shopLockMgr = (ShopLockMgr) context.getBean("shopLockMgr");
		//productMgr = (ProductMgr) context.getBean("productMgr");
		//debitMgr = (DebitMgr) context.getBean("debitMgr");
		//Calendar calendar = new GregorianCalendar();
//		Date now = DateUtil.now();
//		if(currentUser!=null && currentUser.getShopRoot()!=null){
//			shopId=currentUser.getShopRoot().getShopId();
//	        lockDay = shopLockMgr.getApplicationDate(shopId);
//		}
//		if(lockDay == null){
//			lockDay = now;
//		}
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		ShopToken shop = currentUser.getShopRoot();
		if(shop == null) {
			return ERROR;
		}
		//Lay danh sach nhan vien theo phan quyen
//		StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
//		if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
//			filter.setIsLstChildStaffRoot(true);
//		} else {
//			filter.setIsLstChildStaffRoot(false);
//		}
//		filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
//		filter.setStatus(ActiveType.RUNNING.getValue());
//		filter.setLstShopId(Arrays.asList(currentUser.getShopRoot().getShopId()));
//		//Lay NVBH
//		filter.setLstObjectType(Arrays.asList(StaffObjectType.NVBH.getValue(), StaffObjectType.NVVS.getValue()));
//		ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
//		lstNVBHVo = objVO.getLstObject();
//
//		//Lay nhan vien thu tien & NVGH
//		filter.setIsLstChildStaffRoot(false);
//		filter.setLstObjectType(Arrays.asList(StaffObjectType.NVGH.getValue(), StaffObjectType.NVTT.getValue()));
//		objVO = staffMgr.searchListStaffVOByFilter(filter);
//		lstNVGHVo = objVO.getLstObject();
		
		/*ObjectVO<Staff> nvbhVO = staffMgr.getListPreAndVanStaff(null, null, null, shop.getShopId(), 
				ActiveType.RUNNING, Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
		if(nvbhVO!=null && nvbhVO.getLstObject().size()>0){
			lstNVBH = nvbhVO.getLstObject();
		}
		ObjectVO<Staff> nvghVO = staffMgr.getListPreAndVanStaff(null, null, null, shop.getShopId(), 
				ActiveType.RUNNING, Arrays.asList(StaffObjectType.NVGH, StaffObjectType.NVTT));
		if(nvghVO!=null && nvghVO.getLstObject().size()>0){
			lstNVGH = nvghVO.getLstObject();
		}*/
//		fromDate=DateUtil.toDateString(lockDay, DateUtil.DATE_FORMAT_STR);
//		shopId = shop.getShopId();
//		ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shop.getShopId(), "CONFIRM_ORDER_SEARCH");
//		if (numberValueOrderParam != null) {
//			numberValue = numberValueOrderParam.getCode();
//		} else {
//			numberValue = "0";
//		}
		return SUCCESS;
	}
	
	
	/**
	 * Lay danh sach cac don hang de thanh toan
	 * @author tungmt
	 */
	public String searchPay() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<SaleOrderVO>());
		try {
			if (!StringUtil.isNullOrEmpty(currentShopCode)) {
				Shop shop = shopMgr.getShopByCode(currentShopCode);
				if (shop != null) {
					shopId = shop.getId();
					lockDay = shopLockMgr.getApplicationDate(shopId);

					if (lockDay == null) {
						Date now = DateUtil.now();
						lockDay = now;
					}
					OrderType type = OrderType.parseValue(orderType);
					checkCreateShopParamOrderSearch(numberValueOrder, shopId);
					KPaging<PrintOrderVO> kPaging = new KPaging<PrintOrderVO>();
					kPaging.setPageSize(max);
					kPaging.setPage(page - 1);
					PrintOrderFilter filter = new PrintOrderFilter();
					filter.setShopId(shopId);
					filter.setLockDay(lockDay);
					filter.setOrderType(type);
					filter.setRefOrderNumber(refOrderNumber);
					filter.setOrderSource(SaleOrderSource.parseValue(orderSource));
					filter.setFromDate(this.getMaxDateApprovedOrder(shopId, lockDay));
					filter.setToDate(lockDay);
					filter.setCustomerCode(customerCode);
					filter.setCustomeName(customerName);
					filter.setStaffCode(nvbhCode);
					filter.setDeliveryCode(nvghCode);
					filter.setkPaging(kPaging);
					filter.setNumberValueOrder(numberValueOrder);
					filter.setIsValueOrder(isValueOrder);
					filter.setStrListUserId(getStrListUserId());
					ObjectVO<PrintOrderVO> vo = saleOrderMgr.getListPrintOrderVO(filter);
					if (vo != null) {
						result.put("total", vo.getkPaging().getTotalRows());
						result.put("rows", vo.getLstObject());
					} else {
						result.put("total", 0);
						result.put("rows", new ArrayList<SaleOrderVO>());
					}
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintOrderAction.searchPay()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	public String confirm() {
		resetToken(result);
		try {
			if (!StringUtil.isNullOrEmpty(currentShopCode)) {
				Shop shop = shopMgr.getShopByCode(currentShopCode);
				if (shop != null) {
					shopId = shop.getId();
					lockDay = shopLockMgr.getApplicationDate(shopId);

					if (lockDay == null) {
						Date now = DateUtil.now();
						lockDay = now;
					}
					List<Map<String, String>> listErr = new ArrayList<Map<String, String>>();
					Map<String, List<String>> errorOrders = null;
					List<Long> ordersToConfirm = new ArrayList<Long>();
					if (OrderType.GO.getValue().equals(orderType) || OrderType.DP.getValue().equals(orderType)) {
						List<StockTrans> listStockTrans = stockMgr.getListStockTransById(listId);
						// check kiem kho
						for (StockTrans stockTrans : listStockTrans) {
							boolean isError = false;
							Map<String, String> mapError = new HashMap<String, String>();
							mapError.put("orderNumber", stockTrans.getStockTransCode());
							mapError.put("discountAmountPromotion", "");
							mapError.put("numPromotion", "");
							mapError.put("stockCounting", "");
							mapError.put("promotion", "");
							mapError.put("overRewardMoney", "");
							mapError.put("overRewardProduct", "");
							mapError.put("failQuantityStockTotal", "");
							mapError.put("orderConfirmedAlready", "");
							mapError.put("customerNotActive", "");
							mapError.put("systemError", "");
							// check kiem kho
							StockStransFilter stockStransFilter = new StockStransFilter();
							stockStransFilter.setStockTransId(stockTrans.getId());
							if (saleOrderMgr.checkStockCountingForStockTrans(stockStransFilter)) {
								isError = true;
								mapError.put("stockCounting", "x");
							}
							if (isError) {
								listErr.add(mapError);
							} else {
								ordersToConfirm.add(stockTrans.getId());
							}
						}
						if (ordersToConfirm != null && ordersToConfirm.size() > 0) {
							ConfirmFilter param = new ConfirmFilter();
							param.setListId(ordersToConfirm);
							param.setLogInfo(getLogInfoVO());
							saleOrderMgr.updateGODPOrder4Print(param);
						}
					} else {
						Map<String, Map<String, String>> mapErrorValidate = new HashMap<String, Map<String, String>>();
						List<SaleOrder> lstSaleOrder = saleOrderMgr.getListSaleOrderById2(listId);
						List<OrderInvoiceVO> lstOrderHasInvoice = new ArrayList<OrderInvoiceVO>();
						if ((OrderType.CM.getValue().equals(orderType) || OrderType.CO.getValue().equals(orderType)) && null == lstRemovedInvoice) {
							OrderInvoiceVO orIn = null;
							for (SaleOrder so : lstSaleOrder) {
								if (so.getFromSaleOrder() == null) {
									continue;
								}
								List<Invoice> listInvoice = saleOrderMgr.getListInvoiceBySaleOrderId2(so.getFromSaleOrder().getId(), 0); // don le
								String invoiceString = "";
								if (listInvoice.size() > 0) {
									invoiceString = listInvoice.get(0).getInvoiceNumber();
									for (int i = 1, n = listInvoice.size(); i < n; i++) {
										invoiceString += ", " + listInvoice.get(i).getInvoiceNumber();
									}
								}
								if (!StringUtil.isNullOrEmpty(invoiceString)) {
									orIn = new OrderInvoiceVO();
									//orIn.setOrderId(so.getFromSaleOrder().getId());
									orIn.setReturnOrderId(so.getId());
									orIn.setOrderNumber(so.getFromSaleOrder().getOrderNumber());
									orIn.setReturnOrderNumber(so.getOrderNumber());
									orIn.setInvoiceNumber(invoiceString);
									lstOrderHasInvoice.add(orIn);
								}
							}
							if (lstOrderHasInvoice.size() > 0) {
								result.put("lstOrderHasInvoice", lstOrderHasInvoice);
								result.put(ERROR, false);
								return JSON;
							}
						} 
						
						if (OrderType.IN.getValue().equals(orderType) || OrderType.SO.getValue().equals(orderType)) {
							// check tra thuong keyShop, check ton kho, check so xuat, max so tien KM, max so luong KM
							mapErrorValidate = saleOrderMgr.prepareUpdateSaleOrder4Print(lstSaleOrder, lockDay);
							
							// kiem tra khach hang tam ngung
							List<OrderProductVO> listSaleOrderAndCusStatusZero = saleOrderMgr.getListSaleOrderID(listId);
							if (listSaleOrderAndCusStatusZero != null && listSaleOrderAndCusStatusZero.size() > 0) {
								for (OrderProductVO orderProductVO : listSaleOrderAndCusStatusZero) {
									for (SaleOrder so : lstSaleOrder) {
										if (so.getId().equals(orderProductVO.getSaleOrderId())) {
											Map<String, String> mapErrorOrder = new HashMap<String, String>();
											mapErrorOrder.put("customerNotActive", "x");
											mapErrorValidate.put(so.getOrderNumber(), mapErrorOrder);
										}
									}
								}
							}
						}
						
						List<Long> saleOrdersToConfirm = new ArrayList<Long>();
						for (SaleOrder so : lstSaleOrder) {
							boolean isError = false;
							Map<String, String> mapError = new HashMap<String, String>();
							mapError.put("orderNumber", so.getOrderNumber());
							mapError.put("promotion", "");
							mapError.put("discountAmountPromotion", "");
							mapError.put("numPromotion", "");
							mapError.put("stockCounting", "");
							mapError.put("overRewardMoney", "");
							mapError.put("overRewardProduct", "");
							mapError.put("failQuantityStockTotal", "");
							if (mapErrorValidate.containsKey(so.getOrderNumber())) {
								isError = true;
								mapError.putAll(mapErrorValidate.get(so.getOrderNumber()));
							}
							// check kiem kho
							if (saleOrderMgr.checkStockCounting(so.getId())) {
								isError = true;
								mapError.put("stockCounting", "x");
							}
							mapError.put("customerNotActive", "");
							if (isError) {
								listErr.add(mapError);
							} else {
								saleOrdersToConfirm.add(so.getId());
							}
						}
						
						if (saleOrdersToConfirm != null && saleOrdersToConfirm.size() > 0) {
							if (lstRemovedInvoice == null) {
								lstRemovedInvoice = new ArrayList<Long>();
							}
							
							ConfirmFilter param = new ConfirmFilter();
							param.setListSaleOrderId(saleOrdersToConfirm);
							param.setShopId(shopId);
							param.setLockDate(lockDay);
							param.setLstRemovedInvoice(lstRemovedInvoice);
							param.setLogInfo(getLogInfoVO());
							param.setIsUpdateStock(false);
							errorOrders = saleOrderMgr.updateSaleOrder4Print(param);
						}
					}
					if (errorOrders != null && errorOrders.size() > 0) {
						for (Map.Entry<String, List<String>> entry : errorOrders.entrySet()) {
							if (!StringUtil.isNullOrEmpty(entry.getKey()) && entry.getValue() != null && entry.getValue().size() > 0) {
								for (String orderNumber : entry.getValue()) {
									Map<String, String> mapError = new HashMap<String, String>();
									mapError.put("orderNumber", orderNumber);
									mapError.put("discountAmountPromotion", "");
									mapError.put("numPromotion", "");
									mapError.put("stockCounting", "");
									mapError.put("promotion", "");
									mapError.put("overRewardMoney", "");
									mapError.put("overRewardProduct", "");
									mapError.put("failQuantityStockTotal", "");
									mapError.put("orderConfirmedAlready", "");
									mapError.put("customerNotActive", "");
									mapError.put("systemError", "");
									mapError.put(entry.getKey(), "x");
									listErr.add(mapError);
								}
							}
						}
					}
					result.put(ERROR, false);
					result.put("listError", listErr);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintOrderAction.confirm()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			if (e.getMessage() != null && e.getMessage().equals("DON_HANG_DA_XU_LY")) {
				result.put("errMsg", "DON_HANG_DA_XU_LY");// R.getResource("common.sale.order.don.hang.da.xu.ly"));
			}
		}
		return JSON;
	}
	
	/**
	 * Xac nhan va cap nhat kho
	 * @author trietptm
	 * @return String
	 * @since Oct 22, 2015
	 */
	public String confirmAndUpdateStock() {
		resetToken(result);
		try {
			result.put("errCode", "");
			if (!StringUtil.isNullOrEmpty(currentShopCode)) {
				Shop shop = shopMgr.getShopByCode(currentShopCode);
				if (shop != null) {
					shopId = shop.getId();
					lockDay = shopLockMgr.getApplicationDate(shopId);

					if (lockDay == null) {
						Date now = DateUtil.now();
						lockDay = now;
					}
					List<Map<String, String>> listErr = new ArrayList<Map<String, String>>();
					Map<String, List<String>> errorOrders = null;
					List<Long> ordersToConfirm = new ArrayList<Long>();
					if (OrderType.GO.getValue().equals(orderType) || OrderType.DP.getValue().equals(orderType)) {
						List<StockTrans> listStockTrans = stockMgr.getListStockTransById(listId);
						// check kiem kho
						for (StockTrans stockTrans : listStockTrans) {
							boolean isError = false;
							Map<String, String> mapError = new HashMap<String, String>();
							mapError.put("orderNumber", stockTrans.getStockTransCode());
							mapError.put("discountAmountPromotion", "");
							mapError.put("numPromotion", "");
							mapError.put("stockCounting", "");
							mapError.put("promotion", "");
							mapError.put("overRewardMoney", "");
							mapError.put("overRewardProduct", "");
							mapError.put("failQuantityStockTotal", "");
							mapError.put("orderConfirmedAlready", "");
							mapError.put("customerNotActive", "");
							mapError.put("systemError", "");
							// check kiem kho
							StockStransFilter stockStransFilter = new StockStransFilter();
							stockStransFilter.setStockTransId(stockTrans.getId());
							if (saleOrderMgr.checkStockCountingForStockTrans(stockStransFilter)) {
								isError = true;
								mapError.put("stockCounting", "x");
							}
							if (isError) {
								listErr.add(mapError);
							} else {
								ordersToConfirm.add(stockTrans.getId());
							}
						}

						ConfirmFilter param = new ConfirmFilter();
						param.setListId(ordersToConfirm);
						param.setLogInfo(getLogInfoVO());
						param.setIsUpdateStock(true);
						errorOrders = saleOrderMgr.updateGODPOrderAndUpdateStock4Print(param);
					} else {
						Map<String, Map<String, String>> mapErrorValidate = new HashMap<String, Map<String, String>>();
						List<SaleOrder> lstSaleOrder = saleOrderMgr.getListSaleOrderById2(listId);
						List<OrderInvoiceVO> lstOrderHasInvoice = new ArrayList<OrderInvoiceVO>();
						if ((OrderType.CM.getValue().equals(orderType) || OrderType.CO.getValue().equals(orderType)) && null == lstRemovedInvoice) {
							OrderInvoiceVO orIn = null;
							for (SaleOrder so : lstSaleOrder) {
								if (so.getFromSaleOrder() == null) {
									continue;
								}
								List<Invoice> listInvoice = saleOrderMgr.getListInvoiceBySaleOrderId2(so.getFromSaleOrder().getId(), 0); // don le
								String invoiceString = "";
								if (listInvoice.size() > 0) {
									invoiceString = listInvoice.get(0).getInvoiceNumber();
									for (int i = 1, n = listInvoice.size(); i < n; i++) {
										invoiceString += ", " + listInvoice.get(i).getInvoiceNumber();
									}
								}
								if (!StringUtil.isNullOrEmpty(invoiceString)) {
									orIn = new OrderInvoiceVO();
									//orIn.setOrderId(so.getFromSaleOrder().getId());
									orIn.setReturnOrderId(so.getId());
									orIn.setOrderNumber(so.getFromSaleOrder().getOrderNumber());
									orIn.setReturnOrderNumber(so.getOrderNumber());
									orIn.setInvoiceNumber(invoiceString);
									lstOrderHasInvoice.add(orIn);
								}
							}
							if (lstOrderHasInvoice.size() > 0) {
								result.put("lstOrderHasInvoice", lstOrderHasInvoice);
								result.put(ERROR, false);
								return JSON;
							}
						}

						if (OrderType.IN.getValue().equals(orderType) || OrderType.SO.getValue().equals(orderType)) {
							// check tra thuong keyShop, check ton kho, check so xuat, max so tien KM, max so luong KM
							mapErrorValidate = saleOrderMgr.prepareUpdateSaleOrder4Print(lstSaleOrder, lockDay);

							// kiem tra khach hang tam ngung
							List<OrderProductVO> listSaleOrderAndCusStatusZero = saleOrderMgr.getListSaleOrderID(listId);
							if (listSaleOrderAndCusStatusZero != null && listSaleOrderAndCusStatusZero.size() > 0) {
								for (OrderProductVO orderProductVO : listSaleOrderAndCusStatusZero) {
									for (SaleOrder so : lstSaleOrder) {
										if (so.getId().equals(orderProductVO.getSaleOrderId())) {
											Map<String, String> mapErrorOrder = new HashMap<String, String>();
											mapErrorOrder.put("customerNotActive", "x");
											mapErrorValidate.put(so.getOrderNumber(), mapErrorOrder);
										}
									}
								}
							}
						}

						for (SaleOrder so : lstSaleOrder) {
							boolean isError = false;
							Map<String, String> mapError = new HashMap<String, String>();
							mapError.put("orderNumber", so.getOrderNumber());
							mapError.put("discountAmountPromotion", "");
							mapError.put("numPromotion", "");
							mapError.put("stockCounting", "");
							mapError.put("promotion", "");
							mapError.put("overRewardMoney", "");
							mapError.put("overRewardProduct", "");
							mapError.put("failQuantityStockTotal", "");
							mapError.put("orderConfirmedAlready", "");
							mapError.put("customerNotActive", "");
							mapError.put("systemError", "");
							if (mapErrorValidate.containsKey(so.getOrderNumber())) {
								isError = true;
								mapError.putAll(mapErrorValidate.get(so.getOrderNumber()));
							}

							// check kiem kho
							if (saleOrderMgr.checkStockCounting(so.getId())) {
								isError = true;
								mapError.put("stockCounting", "x");
							}

							if (isError) {
								listErr.add(mapError);
							} else {
								ordersToConfirm.add(so.getId());
							}
						}

						if (ordersToConfirm != null && ordersToConfirm.size() > 0) {
							if (lstRemovedInvoice == null) {
								lstRemovedInvoice = new ArrayList<Long>();
							}
							ConfirmFilter param = new ConfirmFilter();
							param.setListSaleOrderId(ordersToConfirm);
							param.setShopId(shopId);
							param.setLockDate(lockDay);
							param.setLstRemovedInvoice(lstRemovedInvoice);
							param.setLogInfo(getLogInfoVO());
							param.setIsUpdateStock(true);
							errorOrders = saleOrderMgr.updateSaleOrder4Print(param);
						}
					}
					if (errorOrders != null && errorOrders.size() > 0) {
						if (errorOrders.containsKey("productStockTotalSalePlan")) {
							List<String> lstCode = errorOrders.get("productStockTotalSalePlan");
							StringBuilder strCode = new StringBuilder();
							for (String code : lstCode) {
								strCode.append(", ").append(code);
							}
							result.put("errCode", strCode.toString().replaceFirst(", ", ""));
							errorOrders.remove("productStockTotalSalePlan");
						}
						for (Map.Entry<String, List<String>> entry : errorOrders.entrySet()) {
							if (!StringUtil.isNullOrEmpty(entry.getKey()) && entry.getValue() != null && entry.getValue().size() > 0) {
								for (String orderNumber : entry.getValue()) {
									Map<String, String> mapError = new HashMap<String, String>();
									mapError.put("orderNumber", orderNumber);
									mapError.put("discountAmountPromotion", "");
									mapError.put("numPromotion", "");
									mapError.put("stockCounting", "");
									mapError.put("promotion", "");
									mapError.put("overRewardMoney", "");
									mapError.put("overRewardProduct", "");
									mapError.put("failQuantityStockTotal", "");
									mapError.put("orderConfirmedAlready", "");
									mapError.put("customerNotActive", "");
									mapError.put("systemError", "");
									mapError.put(entry.getKey(), "x");
									listErr.add(mapError);
								}
							}
						}
					}
					result.put(ERROR, false);
					result.put("listError", listErr);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintOrderAction.confirmAndUpdateStock()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * cancel order
	 * @author tuannd20
	 * @return
	 * @date 03/11/2014
	 */
	public String cancelOrder() {
		try {
			if (lstOrderNumber == null || lstOrderNumber.size() == 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.product.print.order.cancel.no.order"));
				return JSON;
			}
			Shop sh = null;
			if (!StringUtil.isNullOrEmpty(currentShopCode)){
				sh = shopMgr.getShopByCode(currentShopCode);
			}
			if (sh == null){
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.param.not.exist", currentShopCode));
				return JSON;
			}
			shopId = sh.getId();
			List<String> successOrders = new ArrayList<String>();
			List<String> rawErrorOrders = new ArrayList<String>();
			Map<String, List<String>> processResutl = saleOrderMgr.cancelOrdersInPrintOrderStep(shopId, lstOrderNumber, getLogInfoVO());
			if (processResutl != null) {
				successOrders = processResutl.get("successOrders");
				result.put("successOrders", successOrders);
				
				rawErrorOrders = processResutl.get("errorOrders");
				List<String> errorOrders = new ArrayList<String>();
				if (rawErrorOrders.size() > 0) {
					for (String errorOrderMsg : rawErrorOrders) {
						String[] errorMsgParts = errorOrderMsg.split("_");
						String errorMsg = "";
						if (errorMsgParts.length > 1) {
							errorMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, errorMsgParts[0], errorMsgParts[1]);
						} else if (errorMsgParts.length > 0) {
							errorMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, errorMsgParts[0]);
						}
						errorOrders.add(errorMsg);
					}
					result.put("errorOrdersMsg", errorOrders);
				}
				result.put(ERROR, false);
			}
			
		} catch (Exception e) {
			if (DEBUG) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintOrderAction.cancelOrder()"), createLogErrorStandard(actionStartTime));
			}
			if (e instanceof BusinessException) {
				BusinessException be = (BusinessException)e;
				List<String> errorCodes = be.getErrorCodes();
				if (errorCodes != null && errorCodes.size() > 0) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, errorCodes.get(0)));
				}
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));				
			}
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * lay ds nv theo shop don vi
	 * @author trietptm
	 * @return
	 * @throws Exception
	 */
	public String loadStaffByShopCode() {
		try {
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.currentShop.is.null"));
				result.put(ERROR, true);
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(currentShopCode)) {
				Shop shop = shopMgr.getShopByCode(currentShopCode);
				shopId = shop.getId();

				StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
				filter.setIsLstChildStaffRoot(false);
				filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
				filter.setStatus(ActiveType.RUNNING.getValue());
				filter.setLstShopId(Arrays.asList(shopId));
				filter.setIsGetChildShop(false);
				//Lay NVBH
				filter.setObjectType(StaffSpecificType.STAFF.getValue());
				filter.setUserId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
				lstNVBHVo = objVO.getLstObject();

				//Lay NVGH
				filter.setIsLstChildStaffRoot(false);
				filter.setObjectType(StaffSpecificType.NVGH.getValue());
				filter.setUserId(null);
				filter.setRoleId(null);
				objVO = staffMgr.searchListStaffVOByFilter(filter);
				lstNVGHVo = objVO.getLstObject();

				//cau hinh show gia hay khong
				List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_SHOW_PRICE);
				if (lstPr != null && lstPr.size() > 0 && ConstantManager.ONE_TEXT.equals(lstPr.get(0).getValue())) {//cho show gia
					result.put("isAllowShowPrice", true);
				} else {
					result.put("isAllowShowPrice", false);
				}
				ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shopId, "CONFIRM_ORDER_SEARCH");
				if (numberValueOrderParam != null) {
					numberValue = numberValueOrderParam.getCode();
				} else {
					numberValue = "0";
				}

				lockDay = shopLockMgr.getNextLockedDay(shopId);
				if (lockDay == null) {
					Calendar calendar = new GregorianCalendar();
					Date now = calendar.getTime();
					lockDay = now;
				}
				result.put("lockDate", DateUtil.toDateString(lockDay, DateUtil.DATE_FORMAT_DDMMYYYY));
			}

			result.put("numberValue", numberValue);
			result.put("shopId", shopId);
			result.put("lstNVBHVo", lstNVBHVo);
			result.put("lstNVGHVo", lstNVGHVo);

		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintOrderAction.loadStaffByShopCode()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * 
	 * @author trietptm
	 * @param shopId
	 * @param startDateTemp
	 * @return Ngay gan nhat co the approve don hang
	 * @since Aug 24, 2015
	 */
	public Date getMaxDateApprovedOrder(Long shopId, Date startDateTemp) {
		try {
			List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_MAXDAY_APPROVE);
			int iApParamName = 0;
			if (lstPr != null && lstPr.size() > 0) {
				try {
					iApParamName = Integer.valueOf(lstPr.get(0).getValue().trim());
				} catch (Exception e1) {
					// pass through
				}
			}
			if (iApParamName > 0) {
				Integer iExcept = 0;
				dayLock = shopLockMgr.getLockedDay(shopId);
				if (dayLock == null) {
					dayLock = DateUtil.now();
				}
				startDateTemp = DateUtil.moveDate(DateUtil.parseLockDate(dayLock), iApParamName * (-1), 1);
				Date dateTmp = DateUtil.parseLockDate(dayLock);
				do {
					iExcept = exceptionDayMgr.getNumOfHoliday(startDateTemp, dateTmp, shopId);
					dateTmp = DateUtil.moveDate(startDateTemp, -1, 1);
					startDateTemp = DateUtil.moveDate(startDateTemp, -iExcept, 1);
				} while (iExcept > 0);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.saleproduct.PrintOrderAction.getMaxDateApprovedOrder()"), createLogErrorStandard(actionStartTime));
		}
		return startDateTemp;
	}
	
	public String getNumberValue() {
		return numberValue;
	}

	public void setNumberValue(String numberValue) {
		this.numberValue = numberValue;
	}

	public Long getNumberValueOrder() {
		return numberValueOrder;
	}

	public void setNumberValueOrder(Long numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}

	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}

	public List<Long> getLstOrderId() {
		return lstOrderId;
	}

	public void setLstOrderId(List<Long> lstOrderId) {
		this.lstOrderId = lstOrderId;
	}

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getNvbhId() {
		return nvbhId;
	}

	public void setNvbhId(Long nvbhId) {
		this.nvbhId = nvbhId;
	}

	public Long getNvghId() {
		return nvghId;
	}

	public void setNvghId(Long nvghId) {
		this.nvghId = nvghId;
	}

	public List<Staff> getLstNVBH() {
		return lstNVBH;
	}

	public void setLstNVBH(List<Staff> lstNVBH) {
		this.lstNVBH = lstNVBH;
	}

	public List<Staff> getLstNVGH() {
		return lstNVGH;
	}

	public void setLstNVGH(List<Staff> lstNVGH) {
		this.lstNVGH = lstNVGH;
	}

	public String getRefOrderNumber() {
		return refOrderNumber;
	}

	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}

	public Integer getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(Integer orderSource) {
		this.orderSource = orderSource;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getNvbhCode() {
		return nvbhCode;
	}

	public void setNvbhCode(String nvbhCode) {
		this.nvbhCode = nvbhCode;
	}

	public String getNvghCode() {
		return nvghCode;
	}

	public void setNvghCode(String nvghCode) {
		this.nvghCode = nvghCode;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public List<Long> getListId() {
		return listId;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

	public List<String> getLstOrderNumber() {
		return lstOrderNumber;
	}

	public void setLstOrderNumber(List<String> lstOrderNumber) {
		this.lstOrderNumber = lstOrderNumber;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public List<Long> getLstRemovedInvoice() {
		return lstRemovedInvoice;
	}

	public void setLstRemovedInvoice(List<Long> lstRemovedInvoice) {
		this.lstRemovedInvoice = lstRemovedInvoice;
	}

	public String getCurrentShopCode() {
		return currentShopCode;
	}

	public void setCurrentShopCode(String currentShopCode) {
		this.currentShopCode = currentShopCode;
	}


}
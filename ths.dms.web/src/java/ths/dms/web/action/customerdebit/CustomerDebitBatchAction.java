package ths.dms.web.action.customerdebit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Bank;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebitDetailType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.DebitFilter;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.DebitCustomerParams;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PayReceivedVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.ExceptionCode;
import ths.dms.core.memcached.MemcachedUtils;

public class CustomerDebitBatchAction extends AbstractAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6433873780895305521L;

	private StaffMgr staffMgr;
	private CustomerMgr customerMgr;
	private DebitMgr debitMgr;

	private Staff staff;
	private Shop shop;
	private String staffCode;
	private String shortCode;
	private String shopCode;
	private Integer type;
	private String payerName;
	private String payerAddress;
	private String paymentReason;

	private String payreceiptCode;
	private BigDecimal payreceiptValue;
	private BigDecimal debitPreRemain;
	private BigDecimal debitPostRemain;
	List<CustomerDebitVO> lstOrderDebit;
	List<Long> lstCustomerDebitID;
	private List<String> lstStaffCode;
	private String lstDebitId;
	private List<String> lstBankCode;
	private List<BigDecimal> lstPayAmount;
	private List<BigDecimal> lstDiscount;
//	private List<Staff> listNVGH;
//	private List<Staff> listNVTT;
//	private List<Staff> listNVBH;
	private List<StaffVO> lstNVBHVo;
	private List<StaffVO> lstNVGHVo;
	private List<StaffVO> lstNVTTVo;
	private List<Bank> listBank;
	private String fromDate;
	private String toDate;
	private String staffDeliverCode;
	private String staffPaymentCode;
	private String lstOrderNumbers;

	public boolean checkExistShopOfUserLogin() {
		staff = getStaffByCurrentUser();
		if (staff == null) {
			return false;
		}
		try {
			shop = shopMgr.getShopById(getCurrentUser().getShopRoot().getShopId());
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.checkExistShopOfUserLogin"), createLogErrorStandard(actionStartTime));
		}
		if (shop == null)
			return false;
		return true;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		debitMgr = (DebitMgr) context.getBean("debitMgr");

	}

	@Override
	public String execute() {
		resetToken(result);
//		try {
//
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.execute"), createLogErrorStandard(actionStartTime));
//		}
		return SUCCESS;
	}

	/**
	 * Gets the info. Ham getInfo() su dung de tim kiem danh sach don hang con
	 * no cua khach hang
	 * 
	 * @param :shortCode (Ma KH)& staffCode(Ma NVBH)
	 * @since 10/09/2014
	 * @author phuongvm
	 */
	public String getInfo() {
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			lstOrderDebit = new ArrayList<CustomerDebitVO>();
			result.put("rows", lstOrderDebit);
			if (!validateShop()) {
				return JSON;
			}
			Customer customerTemp = null;
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				customerTemp = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), shortCode));
				if (customerTemp == null) {
					result.put(ERROR, true);
					result.put("rows", lstOrderDebit);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
					return JSON;
				}
			}

			Date fromDateTemp = null;
			Date toDateTemp = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fromDateTemp = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				toDateTemp = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			}

			Staff staffDeliver = new Staff();
			if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
				staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, shopId, ActiveType.RUNNING);
				if (staffDeliver == null) {
					result.put(ERROR, true);
					result.put("rows", lstOrderDebit);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.delivery"));
					return JSON;
				}
			}

			Staff staffPayment = new Staff();
			if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
				staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, shopId, ActiveType.RUNNING);
				if (staffPayment == null) {
					result.put(ERROR, true);
					result.put("rows", lstOrderDebit);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.cashier"));
					return JSON;
				}
			}

			Staff nvbh = new Staff();
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				nvbh = staffMgr.getStaffByInfoBasic(staffCode, shopId, ActiveType.RUNNING);
				if (nvbh == null) {
					result.put(ERROR, true);
					result.put("rows", lstOrderDebit);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "ss.ds3.sheet1.header.nvbh"));
					return JSON;
				}
			}
			if (customerTemp != null && (!ActiveType.RUNNING.equals(customerTemp.getStatus()) || !shopId.equals(customerTemp.getShop().getId()))) {
				result.put(ERROR, true);
				result.put("rows", lstOrderDebit);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
				return JSON;
			}
			if (customerTemp == null || shopId.equals(customerTemp.getShop().getId())) {
				DebitFilter<CustomerDebitVO> filter = new DebitFilter<CustomerDebitVO>();
				filter.setShopId(shopId);
				filter.setShortCode(shortCode);
				filter.setIsReveived(type == 1);
				filter.setFromDate(fromDateTemp);
				filter.setToDate(toDateTemp);
				filter.setDeliveryId(staffDeliver.getId());
				filter.setCashierId(staffPayment.getId());
				filter.setStaffId(nvbh.getId());
				filter.setLstOrderNumbers(lstOrderNumbers);
				lstOrderDebit = debitMgr.getListCustomerDebitVOByFilter(filter);
//					if (type == 0) {
//						lstOrderDebit = debitMgr.getListCustomerDebitVO(shop.getId(), shortCode, null, null, null, null, false, fromDateTemp, toDateTemp, staffDeliver.getId(), staffPayment.getId(), nvbh.getId());
//					} else if (type == 1) {
//						lstOrderDebit = debitMgr.getListCustomerDebitVO(shop.getId(), shortCode, null, null, null, null, true, fromDateTemp, toDateTemp, staffDeliver.getId(), staffPayment.getId(), nvbh.getId());
//					}
				debitPreRemain = BigDecimal.ZERO;//debitMgr.getTotalDebit(custId, DebitOwnerType.CUSTOMER);
			}
			Date lockDate = shopLockMgr.getNextLockedDay(shopId);
			if (lockDate == null) {
				lockDate = commonMgr.getSysDate();
			}
			payreceiptCode = debitMgr.getPayReceivedStringSugguest(lockDate);
			
			result.put("rows", lstOrderDebit);
			result.put("debitPreRemain", debitPreRemain);
			result.put("payreceiptCode", payreceiptCode);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.getInfo"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * exportExcel.
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 21/05/2014
	 */
	public String exportExcel() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			if (currentUser == null || currentUser.getShopRoot() == null
					|| currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			if (!validateShop()) {
				return JSON;
			}
			Map<Long, BigDecimal> mapPayDiscountStr = new HashMap<Long, BigDecimal>();
			Map<Long, BigDecimal> mapPayAmountStr = new HashMap<Long, BigDecimal>();
			Map<Long, BigDecimal> mapRemainStr = new HashMap<Long, BigDecimal>();
			Map<Long, BigDecimal> mapTotalStr = new HashMap<Long, BigDecimal>();
			Map<Long, String> mapNVTTStr = new HashMap<Long, String>();
			if (!StringUtil.isNullOrEmpty(arrPayDiscountStr)) {
				String[] arrPayDiscount = arrPayDiscountStr.split(",");
				for (String value : arrPayDiscount) {
					String[] arrTmp = value.split("_");
					mapPayDiscountStr.put(Long.valueOf(arrTmp[0]), BigDecimal.valueOf(Double.valueOf(arrTmp[1])));
				}
			}

			if (!StringUtil.isNullOrEmpty(arrPayAmountStr)) {
				String[] arrPayAmount = arrPayAmountStr.split(",");
				for (String value : arrPayAmount) {
					String[] arrTmp = value.split("_");
					mapPayAmountStr.put(Long.valueOf(arrTmp[0]), BigDecimal.valueOf(Double.valueOf(arrTmp[1])));
				}
			}

			if (!StringUtil.isNullOrEmpty(arrRemainStr)) {
				String[] arrRemain = arrRemainStr.split(",");
				for (String value : arrRemain) {
					String[] arrTmp = value.split("_");
					mapRemainStr.put(Long.valueOf(arrTmp[0]), BigDecimal.valueOf(Double.valueOf(arrTmp[1])));
				}
			}
			if (!StringUtil.isNullOrEmpty(arrTotalStr)) {
				String[] arrTotal = arrTotalStr.split(",");
				for (String value : arrTotal) {
					String[] arrTmp = value.split("_");
					mapTotalStr.put(Long.valueOf(arrTmp[0]), BigDecimal.valueOf(Double.valueOf(arrTmp[1])));
				}
			}
			if (!StringUtil.isNullOrEmpty(arrNVTTStr)) {
				String[] arrNVTT = arrNVTTStr.split(",");
				for (String value : arrNVTT) {
					String[] arrTmp = value.split("_");
					mapNVTTStr.put(Long.valueOf(arrTmp[0]), arrTmp[1]);
				}
			}
			
			lstOrderDebit = new ArrayList<CustomerDebitVO>();
			Customer customerTemp = null;
			//Long custId = null;
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				customerTemp = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), shortCode));
				if (customerTemp == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
					return JSON;
				}
			}

			Date fromDateTemp = null;
			Date toDateTemp = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fromDateTemp = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				toDateTemp = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			}

			Staff staffDeliver = new Staff();
			if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
				staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, shopId, ActiveType.RUNNING);
				if (staffDeliver == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.delivery"));
					return JSON;
				}
			}

			Staff staffPayment = new Staff();
			if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
				staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, shopId, ActiveType.RUNNING);
				if (staffPayment == null) {
					result.put(ERROR, true);
					result.put("rows", lstOrderDebit);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.cashier"));
					return JSON;
				}
			}

			Staff nvbh = new Staff();
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				nvbh = staffMgr.getStaffByInfoBasic(staffCode, shopId, ActiveType.RUNNING);
				if (nvbh == null) {
					result.put(ERROR, true);
					result.put("rows", lstOrderDebit);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "ss.ds3.sheet1.header.nvbh"));
					return JSON;
				}
			}
			if (customerTemp != null && (!ActiveType.RUNNING.equals(customerTemp.getStatus()) || !shopId.equals(customerTemp.getShop().getId()))) {
				result.put(ERROR, true);
				result.put("rows", lstOrderDebit);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
				return JSON;
			}
			if (customerTemp == null || shopId.equals(customerTemp.getShop().getId())) {
				List<Long> lstDebit = null;
				if (lstDebitId != null && lstDebitId.trim().length() > 0) {
					lstDebit = new ArrayList<Long>();
					for (String str : lstDebitId.split(",")) {
						Long id = Long.valueOf(str.trim());
						lstDebit.add(id);
					}
				}
				if (type == 0) {
					lstOrderDebit = debitMgr.getListCustomerDebitVOEx(shopId, shortCode, null, null, null, null, false, fromDateTemp, toDateTemp, staffDeliver.getId(), staffPayment.getId(), nvbh.getId(), lstDebit);
				} else if (type == 1) {
					lstOrderDebit = debitMgr.getListCustomerDebitVOEx(shopId, shortCode, null, null, null, null, true, fromDateTemp, toDateTemp, staffDeliver.getId(), staffPayment.getId(), nvbh.getId(), lstDebit);
				}
			}
			
			if (lstOrderDebit.isEmpty()) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			} else {
				for (int i = 0; i < lstOrderDebit.size(); i++) {
					if (mapPayDiscountStr != null && mapPayDiscountStr.containsKey(lstOrderDebit.get(i).getDebitId())) {
						lstOrderDebit.get(i).setDiscountAmount(mapPayDiscountStr.get(lstOrderDebit.get(i).getDebitId()));
					}
					if (mapPayAmountStr != null && mapPayAmountStr.containsKey(lstOrderDebit.get(i).getDebitId())) {
						lstOrderDebit.get(i).setPayAmt(mapPayAmountStr.get(lstOrderDebit.get(i).getDebitId()));
					}
//					if (mapRemainStr != null && mapRemainStr.containsKey(lstOrderDebit.get(i).getDebitId())) {
//						lstOrderDebit.get(i).setRemain(mapPayDiscountStr.get(lstOrderDebit.get(i).getDebitId()));
//					}
					if (mapTotalStr != null && mapTotalStr.containsKey(lstOrderDebit.get(i).getDebitId())) {
						lstOrderDebit.get(i).setTotalPay(mapTotalStr.get(lstOrderDebit.get(i).getDebitId()));
					}
					if (mapNVTTStr != null && mapNVTTStr.containsKey(lstOrderDebit.get(i).getDebitId())) {
						lstOrderDebit.get(i).setNvttCodeName(mapNVTTStr.get(lstOrderDebit.get(i).getDebitId()));
					}
				}
				
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + ConstantManager.TEMPLATE_CUSTOMERDEBIT_BATCH_EXPORT;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_CUSTOMERDEBIT_BATCH_EXPORT;
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("shopName", shop.getShopCode() + " - " + shop.getShopName());
				params.put("report", lstOrderDebit);
				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(LIST, outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			}

		} catch (Exception ex) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.exportExcel"), createLogErrorStandard(actionStartTime));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);				
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Import customer debit. Hàm importCustomerDebit() su dung de thanh toan cong no
	 * 
	 * @param:
	 * @since 10/09/2014
	 * @author phuongvm
	 */
	public String importCustomerDebit() throws Exception {
		resetToken(result);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null
					|| currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return ERROR;
			}
			staff = getStaffByCurrentUser();
			if (staff == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return ERROR;
			}
			if (!validateShop()) {
				return ERROR;
			}
			errMsg = ValidateUtil.validateField(payreceiptCode, "pay.receive.pay.number", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
			if (StringUtil.isNullOrEmpty(errMsg)) {
				payreceiptCode = payreceiptCode.trim();
				PayReceived test = debitMgr.getPayReceivedByNumber(payreceiptCode, shopId);
				if (test != null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, false, "pay.receive.pay.number");
				}
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(payerName, "pay.receive.payer.name", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(payerAddress, "pay.receive.payer.address", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(paymentReason, "pay.receive.payment.reason", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return ERROR;
			}

			List<DebitCustomerParams> lstDebits = new ArrayList<DebitCustomerParams>();
			List<Long> lstCustIds = null;
			if (payreceiptValue.compareTo(BigDecimal.ZERO) != 1) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_POSSITIVE_INTEGER, false, "customerdebit.batch.payreceiptvalue"));
				return ERROR;
			}
			
			Long custId = null;
			Customer cust = null;
			DebitCustomerParams custDebit = null;
			List<Long> lstDebitIds = null;
			List<BigDecimal> lstDebitAmts = null;
			List<BigDecimal> lstDiscountAmts = null;
			BigDecimal debitAmt = null;
			boolean isBreak = false;
			int idx = -1;
			List<Long> lstFromObject = new ArrayList<Long>();
			List<String> lstCashierCode = new ArrayList<String>();
			
			int COMPARE_VALUE = 1;
			if (type == 0) {
				COMPARE_VALUE = -1;
			}

			BigDecimal sum = BigDecimal.ZERO;
			BigDecimal payreceiptValueTmp = null;
			if (type == 1) {
				payreceiptValueTmp = payreceiptValue;
			} else {
				payreceiptValueTmp = payreceiptValue.negate();
			}
			DebitDetail debitdetail = null;
			BigDecimal remain = null;
			BigDecimal discount = null;
			BigDecimal amount = null;
			BigDecimal paid = null;

			for (int i = 0, sz = lstCustomerDebitID.size(); i < sz; ++i) {
				if (lstPayAmount.get(i) == null || lstPayAmount.get(i).signum() == 0) {
					continue;
				}
				debitdetail = debitMgr.getDebitDetailById(lstCustomerDebitID.get(i));
				if (lstStaffCode != null && !StringUtil.isNullOrEmpty(lstStaffCode.get(i))) {
					Staff staffCashier = staffMgr.getStaffByCodeAndShopId(lstStaffCode.get(i), shopId);
					if (staffCashier != null) {
						debitdetail.setCashier(staffCashier);
					}
					if (debitdetail.getFromObjectId() != null) {
						lstFromObject.add(debitdetail.getFromObjectId());
						lstCashierCode.add(lstStaffCode.get(i));
					}
				}
				remain = lstPayAmount.get(i);
				custId = debitdetail.getDebit().getObjectId();
				discount = lstDiscount.get(i);
				if (discount.signum() == (-1) * COMPARE_VALUE) {
					discount = discount.negate();
				}
				amount = remain.add(discount);

				if (type == 1 && amount.signum() < 1) {
					result.put(ERROR, true);
					result.put("errorReloadPage", true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.receipt.not.negative"));
					return ERROR;
				} else if (type == 0 && amount.signum() > -1) {
					result.put(ERROR, true);
					result.put("errorReloadPage", true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.pay.not.negative"));
					return ERROR;
				}

				if (payreceiptValueTmp.compareTo(remain) == COMPARE_VALUE) { //thu=1,chi=-1
					sum = sum.add(remain);
					debitAmt = remain;
					payreceiptValueTmp = payreceiptValueTmp.subtract(remain);
				} else {
					if (payreceiptValueTmp.signum() == COMPARE_VALUE) { //thu=1,chi=-1
						sum = sum.add(payreceiptValueTmp);
						debitAmt = payreceiptValueTmp;
					}
					isBreak = true;
				}
				
				paid = debitMgr.getPaymentAmountOfDebitDetail(lstCustomerDebitID.get(i), null);
				if (paid == null) {
					paid = BigDecimal.ZERO;
				}
				if (type == 1) { // phieu thu
					if (paid.add(debitAmt.add(discount)).compareTo(debitdetail.getTotal()) > 0) {
						result.put(ERROR, true);
						result.put("errorReloadPage", false);
						result.put("errMsg", R.getResource("debit.detail.da.lap.het.phieu.thu", debitdetail.getFromObjectNumber()));
						return ERROR;
					}
				} else { // phieu chi
					if (paid.add(debitAmt.add(discount)).compareTo(debitdetail.getTotal()) < 0) {
						result.put(ERROR, true);
						result.put("errorReloadPage", false);
						result.put("errMsg", R.getResource("debit.detail.da.lap.het.phieu.chi", debitdetail.getFromObjectNumber()));
						return ERROR;
					}
				}

				if (custDebit == null && custId != null) {
					cust = customerMgr.getCustomerById(custId);
					if (cust == null || !shopId.equals(cust.getShop().getId())) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
						return ERROR;
					}
					custDebit = new DebitCustomerParams();
					custDebit.setShopId(shopId);
					custDebit.setCustomerId(custId);
					custDebit.setPayReceivedNumber(payreceiptCode);
					custDebit.setDebtPaymentType(DebtPaymentType.MANUAL);
					custDebit.setPayReceivedType(PayReceivedType.THANH_TOAN);
					custDebit.setAmount(debitAmt);
					lstDebitIds = new ArrayList<Long>();
					lstDebitIds.add(debitdetail.getId());
					custDebit.setLstDebitId(lstDebitIds);
					lstDebitAmts = new ArrayList<BigDecimal>();
					lstDebitAmts.add(debitAmt);
					custDebit.setLstDebitAmt(lstDebitAmts);
					lstDiscountAmts = new ArrayList<BigDecimal>();
					lstDiscountAmts.add(discount);
					custDebit.setLstDiscount(lstDiscountAmts);
					custDebit.setCreateUser(currentUser.getUserName());
					lstCustIds = new ArrayList<Long>();
					lstCustIds.add(custId);
					custDebit.setLstCustId(lstCustIds);
					String bankCode = lstBankCode.get(i);
					Bank b = null;
					if (!StringUtil.isNullOrEmpty(bankCode)) {
						b = debitMgr.getBankByCodeAndShop(shopId, bankCode.trim());
					}
					if (b != null) {
						custDebit.setBankId(b.getId());
					}
					custDebit.setPayerName(payerName);
					custDebit.setPayerAddress(payerAddress);
					custDebit.setPaymentReason(paymentReason);
					lstDebits.add(custDebit);
				} else if (custId != null) {
					idx = custDebit.getLstCustId().indexOf(custId);
					if (idx < 0) {
						cust = customerMgr.getCustomerById(custId);
						if (cust == null || !shopId.equals(cust.getShop().getId())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
							return ERROR;
						}
					}
					custDebit.setAmount(custDebit.getAmount().add(debitAmt));
					custDebit.getLstDebitId().add(debitdetail.getId());
					custDebit.getLstDebitAmt().add(debitAmt);
					custDebit.getLstDiscount().add(discount);
					custDebit.getLstCustId().add(custId);
				}

				if (isBreak) {
					break;
				}
			}
			//KIEM TRA GIA TRI PHIEU THU/CHI PHAI NHO HON HOAC BANG TONG SO TIEN TRA
			if (type == 1) { // thu
				if (payreceiptValue.compareTo(sum) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.receipt.smaller.or.equal"));
					return ERROR;
				}
			} else { // chi
				BigDecimal test2 = sum.abs();
				if (payreceiptValue.compareTo(test2) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.pay.smaller.or.equal"));
					return ERROR;
				}
			}
			debitMgr.updateListDebitOfCustomer(lstDebits, PaymentStatus.PAID);
			
			/* Cap nhat truong NVTT (Cashier) */
			if (lstFromObject != null && lstFromObject.size() > 0) {
				debitMgr.updateListCashierIdSaleOder(lstFromObject, lstCashierCode, staff.getStaffCode(), shop.getId());
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			if (e.getMessage().equals(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN)) {
				result.put("errorReloadPage", true);
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.importCustomerDebit"), createLogErrorStandard(actionStartTime));
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @return String
	 * @since Dec 4, 2015
	 */
	public String changeShop() {
		result.put("lstSale", new ArrayList<StaffVO>());
		result.put("lstCashier", new ArrayList<StaffVO>());
		result.put("lstDeliver", new ArrayList<StaffVO>());
		result.put("listBank", new ArrayList<StaffVO>());
		result.put("fromDate", dayLock);
		result.put("toDate", dayLock);
		result.put("payreceiptCode", "");
		try {
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return JSON;
			}
			if (!validateShop()) {
				return JSON;
			}

			StaffFilter filter = new StaffFilter();
			filter.setShopId(shop.getId());
			filter.setStatus(ActiveType.RUNNING);
			filter.setSpecType(StaffSpecificType.STAFF);

			//Lay NVBH
			filter.setStrListUserId(super.getStrListUserId());
			ObjectVO<StaffVO> objVO = staffMgr.getListStaffVO(filter);
			lstNVBHVo = objVO.getLstObject();

			String strLstShopId = super.getStrListShopId();
			//Lay nhan vien thu tien
			filter.setStrListUserId(null);
			filter.setStrListShopId(strLstShopId);
			filter.setSpecType(StaffSpecificType.NVTT);
			ObjectVO<StaffVO> objVOCash = staffMgr.getListStaffVO(filter);
			lstNVTTVo = objVOCash.getLstObject();

			//Lay nhan vien NVGH
			filter.setStrListUserId(null);
			filter.setStrListShopId(strLstShopId);
			filter.setSpecType(StaffSpecificType.NVGH);
			ObjectVO<StaffVO> objVODeliver = staffMgr.getListStaffVO(filter);
			lstNVGHVo = objVODeliver.getLstObject();

			listBank = debitMgr.getListBankByShop(shopId, ActiveType.RUNNING);
			
			Date lockDate = shopLockMgr.getNextLockedDay(shopId);
			if (lockDate == null) {
				lockDate = commonMgr.getSysDate();
			}
			fromDate = DateUtil.toDateString(lockDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			payreceiptCode = debitMgr.getPayReceivedStringSugguest(lockDate);
			
			if (lstNVBHVo != null) {
				result.put("lstSale", lstNVBHVo);
			}
			if (lstNVTTVo != null) {
				result.put("lstCashier", lstNVTTVo);
			}
			if (lstNVGHVo != null) {
				result.put("lstDeliver", lstNVGHVo);
			}
			
			if (listBank != null) {
				result.put("listBank", listBank);
			}
			result.put("fromDate", fromDate);
			result.put("toDate", fromDate);
			result.put("payreceiptCode", payreceiptCode);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.changeShop"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	private boolean validateShop() throws BusinessException {
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			shop = shopMgr.getShopByCode(shopCode);
		} else {
			result.put(ERROR, true);
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable"));
			result.put("errMsg", errMsg);
			return false;
		}
		if (shop == null) {
			result.put(ERROR, true);
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable"));
			result.put("errMsg", errMsg);
			return false;
		}
		shopId = shop.getId();
		if (super.getMapShopChild().get(shopId) == null) {
			result.put(ERROR, true);
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
			result.put(ERR_MSG, errMsg);
			return false;
		}
		return true;
	}

	/* sontt19 */
	//	public String viewPayExcelFile() {
	//		isError = true;
	//		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,4);	
	//		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
	//			try {
	//				lstPayBean = new ArrayList<PayBeanOfCustomerDebit>();
	//				for(int i=0;i<lstData.size();i++){
	//					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
	//						PayBeanOfCustomerDebit payBean = new PayBeanOfCustomerDebit();
	//						List<String> row = lstData.get(i);
	//						if(row.size() > 0){
	//							payBean.setStaffCode(row.get(0));
	//						}
	//						if(row.size() > 1){
	//							payBean.setCustomerCode(row.get(1));
	//						}
	//						if(row.size() > 2){
	//							payBean.setPayType(row.get(2));
	//						}
	//						if(row.size() > 3){
	//							payBean.setPayCode(row.get(3));
	//						}
	//						if(row.size() > 4){
	//							payBean.setMoney(row.get(4));
	//						}
	//						lstPayBean.add(payBean);												
	//					}
	//				}				
	//			} catch (Exception e) {
	//				LogUtility.logError(e, e.getMessage());
	//				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	//			}
	//		}
	//		if (StringUtil.isNullOrEmpty(errMsg)) {
	//			isError = false;
	//		} else{
	//			result.put("errMsg", errMsg);
	//		}
	//		result.put("error", isError);
	//		result.put("lstPayBean", lstPayBean);
	//		return SUCCESS;
	//	}
	//	private String xcel() {
	//		isError = true;
	//		totalItem = 0;
	//		List<CellBean> lstFails = new ArrayList<CellBean>();
	//		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,5);
	//		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {		
	//			try {
	//				for(int i=0;i<lstData.size();i++){
	//					if(lstData.get(i)!= null && lstData.get(i).size() > 0){ // && lstData.get(i).size()>= 5){
	//						List<String> row = lstData.get(i);
	//						////////////////////Xu ly ma chua validate
	//						String itemStaffCode = row.get(0);
	//						String itemCustomerShortCode = row.get(1);
	//						String itemPayType = row.get(2);
	//						String itemPayCode = row.get(3);
	//						String itemValuePay = row.get(4);
	//						//Chuan bi du lieu cho ham getListCustomerDebitVO
	//						listCustomerDebit = new ArrayList<CustomerDebitVO>();
	//						boolean isRecevied;
	//						BigDecimal valuePay;
	//						valuePay = BigDecimal.valueOf(Long.parseLong(itemValuePay));
	//						if("0".equalsIgnoreCase(itemPayType)){
	//							//phieu thu
	//							isRecevied = true;
	//						}else{
	//							//phieu chi
	//							valuePay = valuePay.multiply(new BigDecimal((-1)));
	//							isRecevied = false;
	//						}
	//						listCustomerDebit = debitMgr.getListCustomerDebitVO(shopId,
	//								staff2Code, null, shortCode, null, valuePay, null,
	//								null, isRecevied); // true: phieu thu , false: phieu chi
	//						//Chuan bi du lieu cho ham updateDebitOfCustomer
	//						Long customerId = new Long(0);
	//						List<Long> lstDebitId = new ArrayList<Long>();
	//						List<BigDecimal> lstDebitAmt = new ArrayList<BigDecimal>();
	//						debitMgr.updateDebitOfCustomer(shopId, customerId, itemPayCode, DebtPaymentType.AUTO, valuePay, lstDebitId, lstDebitAmt,staff.getStaffCode());
	//						//////////////////
	//					}
	//				}
	//			}catch (Exception e) {
	//				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");				
	//				LogUtility.logError(e, e.getMessage());
	//			}
	//		}
	//		
	//		return SUCCESS;
	//	}
	/* sontt19 */
	/*private Shop getShopOfCurrentUser() {
		staff = getStaffByCurrentUser();
		if (staff != null && staff.getShop() != null) {
			// Ta lay shop cua user current ra dung: dung shopId va shopCode:
			shop = staff.getShop();
			if (shop != null && shop.getId() != null && shop.getShopCode() != null) {
				return shop;
			}
		}
		return null;
	}*/

	public String checkDuplicate(List<List<String>> lstData, int k) {//check trung khoa
		String mes = "";
		if (lstData.get(k) == null || lstData.get(k).size() < 4) {
			mes += "1";//Du lieu khong hop le
		} else {
			//String staffCode = lstData.get(k).get(0).trim();
			String customerCode = lstData.get(k).get(0).trim().toUpperCase();
			String type = lstData.get(k).get(1).trim();
			//String chungtu = lstData.get(k).get(3).trim();
			if (StringUtil.isNullOrEmpty(customerCode) || StringUtil.isNullOrEmpty(type)) {
				mes += "1";//Du lieu khong hop le
			} else {
				for (int i = 0; i < lstData.size(); i++) {
					if (i != k && lstData.get(i) != null && lstData.get(i).size() >= 4) {
						List<String> row = lstData.get(i);
						if (customerCode.equalsIgnoreCase(row.get(0).trim()) && type.equalsIgnoreCase(row.get(1).trim())) {
							if (isView == 0) {
								mes += (i + 2) + " , ";//stt dong import khac voi dong xem
							} else {
								mes += (i + 1) + " , ";
							}
						}
					}
				}
				if (mes.length() > 0) {
					mes = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.duplicate") + mes.substring(0, mes.length() - 3);
				}
			}
		}
		return mes;
	}

	public boolean checkDuplicateAll(List<List<String>> lstData) {
		try {
			for (int i = 0; i < lstData.size(); i++) {
				String mesTemp = checkDuplicate(lstData, i);
				if (!StringUtil.isNullOrEmpty(mesTemp) && !mesTemp.equals("1")) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Creates the sale order.
	 * 
	 * @return the string
	 * @author tungmt
	 */
	public String checkStaff(Staff staff, StaffSpecificType staffSpecificType) {
		if (staff == null) {
			return ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.code"));
		}
		if (staff.getShop() == null || !staff.getShop().getId().equals(shop.getId())) {
			return ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, staff.getStaffCode(), shop.getShopCode());
		}
		if (staff.getStaffType() == null || (staff.getStaffType() != null && staff.getStaffType().getSpecificType() != null 
				&& !staff.getStaffType().getSpecificType().getValue().equals(staffSpecificType.getValue()))) {
			return ValidateUtil.getErrorMsg(ConstantManager.ERR_SS_NOT_GENERAL, staff.getStaffCode(), staffSpecificType.toString());
		}
		if (!staff.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
			return ValidateUtil.getErrorMsg(ConstantManager.ERR_PAUSE_STATUS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.code"));
		}
		return "";
	}

	/* sontt19 */
//	public String importExcel() {
//		shop = getShopOfCurrentUser();
//		if (shop == null) {
//			isError = true;
//			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//			return SUCCESS;
//		} else {//do import excel
//			isError = true;
//			totalItem = 0;
//			lstView = new ArrayList<CellBean>();
//			typeView = true;
//			String message = "";
//			String msg;
//			List<CellBean> lstFails = new ArrayList<CellBean>();
//			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 5);
//			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
//				try {
//					boolean isUpdate = checkDuplicateAll(lstData);// neu co dong trung thi khong cho update gi ca
//
//					List<String> row = null;
//					BigDecimal valuePay = null;
//					BigDecimal discount = null;
//					boolean isRecevied = true;
//					//String itemStaffCode = null;
//					String itemCustomerShortCode = null;
//					String itemPayType = null;
//					String itemPayCode = null;
//					String itemValuePay = null;
//					String itemDiscount = null;
//					Long tien = null;
//					List<Long> lstDebitId = null;
//					List<BigDecimal> lstDebitAmt = null;
//					List<BigDecimal> lstDiscount = null;
//					List<CustomerDebitVO> listCustomerDebit = null;
//					for (int i = 0, sz = lstData.size(); i < sz; i++) {
//						if (lstData.get(i) != null && lstData.get(i).size() > 0) {
//							message = "";
//							totalItem++;
//							row = lstData.get(i);
//							listCustomerDebit = new ArrayList<CustomerDebitVO>();
//							//kiem tra trung khoa
//							String msgTemp = checkDuplicate(lstData, i);
//							if (!StringUtil.isNullOrEmpty(msgTemp) && !msgTemp.equals("1")) {
//								message += msgTemp;
//							}
//							//end-khai bao
//							if (row != null && row.size() >= 5 && isUpdate) {//do something:
//								/*
//								 * ////MaNV itemStaffCode = row.get(0); msg =
//								 * ValidateUtil.validateField(itemStaffCode,
//								 * "catalog.staff.code",
//								 * 50,ConstantManager.ERR_REQUIRE
//								 * ,ConstantManager
//								 * .ERR_EXIST_SPECIAL_CHAR_IN_CODE
//								 * ,ConstantManager.ERR_MAX_LENGTH); //Maxlength
//								 * cua MaNV trong tai lieu BA la 50
//								 * if(!StringUtil.isNullOrEmpty(msg)){ message
//								 * += msg; }else{//Validate cho MaNV: Ma NVBH
//								 * phai ton tai va thuoc NPP Staff cell0Staff =
//								 * staffMgr
//								 * .getStaffByCode(itemStaffCode);//Truyen xuong
//								 * core da upperCase va xet status co hieu luc.
//								 * String mesStaff =
//								 * checkStaff(cell0Staff,StaffObjectType.NVBH);
//								 * if(!mesStaff.equals("")){ message +=
//								 * mesStaff+"\n"; } }
//								 */
//
//								////MaKH 
//								itemCustomerShortCode = row.get(0);
//								msg = ValidateUtil.validateField(itemCustomerShortCode, "catalog.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
//								//MaKH trong tai lieu BA ko can xet MaxLength.
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {//Validate cho MaKH: Ma KH phai ton tai va thuoc NPP
//									Customer cell1Customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), itemCustomerShortCode));//Truyen xuong core da upperCase va xet status co hieu luc.
//									if (cell1Customer == null) {//Ma KH khong ton tai trong he thong.
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.code");
//									} else {//check thuoc NPP
//										if (!(cell1Customer.getShop() != null && cell1Customer.getShop().getId() != null && cell1Customer.getShop().getId().equals(shop.getId()))) {//Ma KH khong thuoc NPP.
//											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_DEBIT_BATCH_NOT_IN_SHOP, true, "catalog.customer.code");
//										} else if (!ActiveType.RUNNING.equals(cell1Customer.getStatus())) {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.customer.not.running") + "\n";
//										}
//									}
//								}
//
//								////LoaiCT
//								itemPayType = row.get(1);
//								//Validate cho LoaiCT
//								msg = ValidateUtil.validateField(itemPayType, "customerdebit.batch.payreceipttype", 1, ConstantManager.ERR_REQUIRE);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									if (!("0".equalsIgnoreCase(itemPayType) || "1".equalsIgnoreCase(itemPayType))) {//Loai CT khong hop le.
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID, true, "customerdebit.batch.payreceipttype");
//									}
//								}
//
//								////SoCT - Validate cho SoCT
//								itemPayCode = row.get(2);
//								msg = ValidateUtil.validateField(itemPayCode, "customerdebit.batch.payreceiptcode", 40, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
//								//So CT luc thi maxlength la 20, luc thi maxlength la 40.
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								}
//
//								////SoTien + chietKhau
//								itemValuePay = row.get(3);
//								itemDiscount = row.get(4);
//								itemValuePay = itemValuePay.replace(",", "");
//								itemDiscount = itemDiscount.replace(",", "");
//								if (StringUtil.isNullOrEmpty(itemDiscount)) {
//									itemDiscount = "0";
//								}
//								//Validate cho SoTien: so tien >0, dai khong qua 17 ki tu:
//								msg = ValidateUtil.validateField(itemValuePay, "customerdebit.batch.payreceiptvalue", 17, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(msg)) {
//									message += msg;
//								} else {
//									tien = null;
//									try {
//										tien = Long.valueOf(itemValuePay);
//									} catch (NumberFormatException e) {//So tien khong dung dinh dang.
//										msg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID, true, "customerdebit.batch.payreceiptvalue");
//										message += msg;
//									}
//									if (tien != null) {
//										if (tien <= 0) {//So tien phai lon hon 0.
//											msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_BATCH_MONEY_NOT_GREATER_ZERO);
//											message += msg + "\n";
//										} else {
//											valuePay = BigDecimal.valueOf(tien);
//											if ("0".equalsIgnoreCase(itemPayType)) {//phieu thu
//												isRecevied = true;
//											} else {//phieu chi
//												valuePay = valuePay.negate();
//												isRecevied = false;
//											}
//										}
//									}
//								}
//								//msgTemp =  ValidateUtil.validateField(itemDiscount, "customerdebit.batch.discount", 17, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
//								if (!StringUtil.isNullOrEmpty(itemDiscount)) {
//									tien = null;
//									try {
//										tien = Long.valueOf(itemDiscount);
//									} catch (NumberFormatException e) {//Chiet khau khong dung dinh dang.
//										msgTemp = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID, true, "customerdebit.batch.discount");
//										message += msgTemp;
//									}
//									if (tien != null) {
//										if (tien < 0) {//chiet khau phai lon hon bang 0.
//											msgTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.discount.not.greater.zero");
//											message += msgTemp + "\n";
//										} else {
//											discount = BigDecimal.valueOf(tien);
//											if (!"0".equalsIgnoreCase(itemPayType)) {
//												discount = discount.negate();
//											}
//										}
//									}
//								}
//								if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(msgTemp)) {
//									listCustomerDebit = debitMgr.getListCustomerDebitVO(shop.getId(), itemCustomerShortCode, null, valuePay.add(discount), null, null, isRecevied, null, null, null, null, null); // true: phieu thu , false: phieu chi
//									if (listCustomerDebit != null && listCustomerDebit.size() > 0) {
//										BigDecimal total = BigDecimal.ZERO;
//										for (CustomerDebitVO cd : listCustomerDebit) {
//											total = total.add(cd.getPayAmt());
//										}
//										if (valuePay.add(discount).abs().compareTo(total.abs()) > 0) {
//											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.money.greater.debit1") + total.abs();
//										}
//									} else if (StringUtil.isNullOrEmpty(message)) {
//										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_BATCH_NOT_EXIST_DEBIT, itemCustomerShortCode);
//									}
//								}
//							}
//
//							////////////////////////still in loop for, out of if
//							if (isView == 0) {
//								if (StringUtil.isNullOrEmpty(message) && isUpdate) {
//									//////////////////Thao tac xuong DATABASE:///////////////////////
//									if ("0".equalsIgnoreCase(itemPayType)) {//phieu thu
//										isRecevied = true;
//									} else {//phieu chi
//										isRecevied = false;
//									}
//									listCustomerDebit = debitMgr.getListCustomerDebitVO(shop.getId(), itemCustomerShortCode, null, valuePay.add(discount), null, null, isRecevied, null, null, null, null, null); // true: phieu thu , false: phieu chi
//									if (listCustomerDebit != null && listCustomerDebit.size() > 0) {
//										lstDebitId = new ArrayList<Long>();
//										lstDebitAmt = new ArrayList<BigDecimal>();
//										lstDiscount = new ArrayList<BigDecimal>();
//										Long customerId = 0L;
//										for (CustomerDebitVO cd : listCustomerDebit) {
//											lstDebitId.add(cd.getDebitId());
//											if (discount.signum() != 0) {
//												if (discount.abs().compareTo(cd.getPayAmt().abs()) >= 0) {
//													lstDebitAmt.add(BigDecimal.ZERO);
//													lstDiscount.add(cd.getPayAmt());
//													discount = discount.subtract(cd.getPayAmt());
//												} else {
//													lstDebitAmt.add(cd.getPayAmt().subtract(discount));
//													lstDiscount.add(discount);
//													discount = BigDecimal.ZERO;
//												}
//											} else {
//												lstDebitAmt.add(cd.getPayAmt());
//												lstDiscount.add(BigDecimal.ZERO);
//											}
//										}
//										customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), itemCustomerShortCode));
//										if (customer != null) {
//											customerId = customer.getId();//Now, all data is ready for method updateDebitOfCustomer:
//											debitMgr.updateDebitOfCustomer(shop.getId(), customerId, itemPayCode, DebtPaymentType.AUTO, valuePay,
//													lstDebitId, lstDebitAmt, lstDiscount, staff.getStaffCode(), PayReceivedType.THANH_TOAN, null, PaymentStatus.PAID);
//										}
//									}
//									///////////////End-Thao tac DATABASE
//								} else {
//									lstFails.add(StringUtil.addFailBean(row, message));
//								}
//								typeView = false;
//							} else {
//								message = StringUtil.convertHTMLBreakLine(message);
//								lstView.add(StringUtil.addFailBean(row, message));
//								typeView = true;
//							}
//						}
//					}
//					//Out of loop for: Export error
//					getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CUSTOMER_DEBIT_BATCH_FAIL);
//				} catch (Exception e) {
//					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//					LogUtility.logError(e, e.getMessage());
//				}
//				if (StringUtil.isNullOrEmpty(errMsg)) {
//					isError = false;
//					return SUCCESS;
//				}
//			}
//		}
//		//
//		return SUCCESS;
//	}
	
	/**
	 * Xuat phieu thanh toan
	 * 
	 * @author lacnv1
	 * @since Mar 23, 2015
	 */
	public String exportPayment() throws Exception {
		resetToken(result);
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser == null || currentUser.getShopRoot() == null
					|| currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return JSON;
			}

			if (!validateShop()) {
				return JSON;
			}
			
			errMsg = ValidateUtil.validateField(payreceiptCode, "pay.receive.pay.number", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
			if (StringUtil.isNullOrEmpty(errMsg)) {
				PayReceived test = debitMgr.getPayReceivedByNumber(payreceiptCode.trim(), shopId);
				if (test != null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, false, "pay.receive.pay.number");
				}
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(payerName, "pay.receive.payer.name", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(payerAddress, "pay.receive.payer.address", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(paymentReason, "pay.receive.payment.reason", 250, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}

			List<Long> lstCustIds = null;
			if (payreceiptValue.compareTo(BigDecimal.ZERO) != 1) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_POSSITIVE_INTEGER, false, "customerdebit.batch.payreceiptvalue"));
				return JSON;
			}
			Long custId = null;
			Customer cust = null;
			DebitCustomerParams custDebit = null;
			List<Long> lstDebitIds = null;
			List<BigDecimal> lstDebitAmts = null;
			List<BigDecimal> lstDiscountAmts = null;
			BigDecimal debitAmt = null;
			boolean isBreak = false;
			int idx = -1;
			List<Long> lstFromObjectId = new ArrayList<Long>();
			
			payreceiptCode = payreceiptCode.trim();
			
			int COMPARE_VALUE = 1;
			if (type == 0) {
				COMPARE_VALUE = -1;
			}

			BigDecimal sum = BigDecimal.ZERO;
			BigDecimal payreceiptValueTmp = null;
			if (type == 1) {
				payreceiptValueTmp = payreceiptValue;
			} else {
				payreceiptValueTmp = payreceiptValue.negate();
			}
			DebitDetail debitdetail = null;
			BigDecimal remain = null;
			BigDecimal discount = null;
			BigDecimal amount = null;
			BigDecimal paid = null;

			for (int i = 0, sz = lstCustomerDebitID.size(); i < sz; ++i) {
				if (lstPayAmount.get(i) == null || lstPayAmount.get(i).signum() == 0) {
					continue;
				}
				debitdetail = debitMgr.getDebitDetailById(lstCustomerDebitID.get(i));
				if (debitdetail.getFromObjectId() != null) {
					lstFromObjectId.add(debitdetail.getFromObjectId());
				}
				remain = lstPayAmount.get(i);
				custId = debitdetail.getDebit().getObjectId();
				discount = lstDiscount.get(i);
				if (discount.signum() == (-1) * COMPARE_VALUE) {
					discount = discount.negate();
				}
				amount = remain.add(discount);

				if (type == 1 && amount.signum() < 1) {
					result.put(ERROR, true);
					result.put("errorReloadPage", true);
					result.put("errMsg", R.getResource("customerdebit.batch.import.receipt.not.negative"));
					return JSON;
				} else if (type == 0 && amount.signum() > -1) {
					result.put(ERROR, true);
					result.put("errorReloadPage", true);
					result.put("errMsg", R.getResource("customerdebit.batch.import.pay.not.negative"));
					return JSON;
				}

				if (payreceiptValueTmp.compareTo(remain) == COMPARE_VALUE) { //thu=1,chi=-1
					sum = sum.add(remain);
					debitAmt = remain;
					payreceiptValueTmp = payreceiptValueTmp.subtract(remain);
				} else {
					if (payreceiptValueTmp.signum() == COMPARE_VALUE) { //thu=1,chi=-1
						sum = sum.add(payreceiptValueTmp);
						debitAmt = payreceiptValueTmp;
					}
					isBreak = true;
				}
				
				paid = debitMgr.getPaymentAmountOfDebitDetail(lstCustomerDebitID.get(i), null);
				if (paid == null) {
					paid = BigDecimal.ZERO;
				}
				if (type == 1) { // phieu thu
					if (paid.add(debitAmt.add(discount)).compareTo(debitdetail.getTotal()) > 0) {
						result.put(ERROR, true);
						result.put("errorReloadPage", false);
						result.put("errMsg", R.getResource("debit.detail.da.lap.het.phieu.thu", debitdetail.getFromObjectNumber()));
						return JSON;
					}
				} else { // phieu chi
					if (paid.add(debitAmt.add(discount)).compareTo(debitdetail.getTotal()) < 0) {
						result.put(ERROR, true);
						result.put("errorReloadPage", false);
						result.put("errMsg", R.getResource("debit.detail.da.lap.het.phieu.chi", debitdetail.getFromObjectNumber()));
						return JSON;
					}
				}

				if (custDebit == null && custId != null) {
					cust = customerMgr.getCustomerById(custId);
					if (cust == null || !shopId.equals(cust.getShop().getId())) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
						return JSON;
					}
					custDebit = new DebitCustomerParams();
					custDebit.setShopId(shop.getId());
					custDebit.setCustomerId(custId);
					custDebit.setPayReceivedNumber(payreceiptCode);
					custDebit.setDebtPaymentType(DebtPaymentType.MANUAL);
					custDebit.setPayReceivedType(PayReceivedType.THANH_TOAN);
					custDebit.setAmount(debitAmt);
					lstDebitIds = new ArrayList<Long>();
					lstDebitIds.add(debitdetail.getId());
					custDebit.setLstDebitId(lstDebitIds);
					lstDebitAmts = new ArrayList<BigDecimal>();
					lstDebitAmts.add(debitAmt);
					custDebit.setLstDebitAmt(lstDebitAmts);
					lstDiscountAmts = new ArrayList<BigDecimal>();
					lstDiscountAmts.add(discount);
					custDebit.setLstDiscount(lstDiscountAmts);
					custDebit.setCreateUser(currentUser.getUserName());
					lstCustIds = new ArrayList<Long>();
					lstCustIds.add(custId);
					custDebit.setLstCustId(lstCustIds);
					String bankCode = lstBankCode.get(i);
					Bank b = null;
					if (!StringUtil.isNullOrEmpty(bankCode)) {
						b = debitMgr.getBankByCodeAndShop(shop.getId(), bankCode.trim());
					}
					if (b != null) {
						custDebit.setBankId(b.getId());
					}
					custDebit.setPayerName(payerName);
					custDebit.setPayerAddress(payerAddress);
					custDebit.setPaymentReason(paymentReason);
				} else if (custId != null) {
					idx = custDebit.getLstCustId().indexOf(custId);
					if (idx < 0) {
						cust = customerMgr.getCustomerById(custId);
						if (cust == null || !shopId.equals(cust.getShop().getId())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
							return JSON;
						}
					}
					custDebit.setAmount(custDebit.getAmount().add(debitAmt));
					custDebit.getLstDebitId().add(debitdetail.getId());
					custDebit.getLstDebitAmt().add(debitAmt);
					custDebit.getLstDiscount().add(discount);
					custDebit.getLstCustId().add(custId);
				}

				if (isBreak) {
					break;
				}
			}
			//KIEM TRA GIA TRI PHIEU THU/CHI PHAI NHO HON HOAC BANG TONG SO TIEN TRA
			if (type == 1) { // thu
				if (payreceiptValue.compareTo(sum) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("customerdebit.batch.import.receipt.smaller.or.equal"));
					return JSON;
				}
			} else { // chi
				BigDecimal test2 = sum.abs();
				if (payreceiptValue.compareTo(test2) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("customerdebit.batch.import.pay.smaller.or.equal"));
					return JSON;
				}
			}
			
			custDebit.setLstFromObjectId(lstFromObjectId);
			custDebit.setLstCashierCode(lstStaffCode);
			PayReceived payReceived = debitMgr.exportPayment(custDebit, getLogInfoVO());
			
			if (payReceived != null) {
				List<CustomerDebitVO> lstDebitVO = debitMgr.getCustomerDebitOfPayReceived(shop.getId(), payReceived.getId());
				result.put("lstDebitVO", lstDebitVO);
				PayReceivedVO payVO = debitMgr.getPayReceivedVOForExport(payReceived.getId());
				if (payVO != null) {
					HashMap<String, Object> parameters = new HashMap<String, Object>();
					parameters.put("sName", shop.getShopName());
//					if (!StringUtil.isNullOrEmpty(shop.getAddress())) {
//						parameters.put("sAddress", shop.getAddress());
//					} else {
//						parameters.put("sAddress", "");
//					}
//					parameters.put("printDate", DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY));
					parameters.put("printDateTime", DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_NOW));
					parameters.put("receiptType", payVO.getReceiptType());
					if (PaymentStatus.NOT_PAID_YET.getValue().equals(payVO.getStatus())) {
						if (payVO.getRemain() != null) {
							payVO.setRemain(payVO.getRemain().subtract(payVO.getAmount()));
							if (payVO.getDiscount() != null) {
								payVO.setRemain(payVO.getRemain().subtract(payVO.getDiscount()));
							}
						}
					}
					if (ReceiptType.PAID.getValue().equals(payVO.getReceiptType())) {
						payVO.setAmount(payVO.getAmount().negate());
						if (payVO.getDiscount() != null) {
							payVO.setDiscount(payVO.getDiscount().negate());
						}
						if (payVO.getRemain() != null) {
							payVO.setRemain(payVO.getRemain().negate());
						}
					}
					
					JRDataSource dataSource = new JRBeanCollectionDataSource(Arrays.asList(payVO));
					String outputPath = ReportUtils.exportFromFormat(FileExtension.PDF, parameters, dataSource, ShopReportTemplate.PAYMENT_RPT);
					result.put("hasData", true);
					result.put(REPORT_PATH, outputPath);
					session.setAttribute("downloadPath", outputPath);
					
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				}
			}
			result.put(ERROR, false);
		} catch (Exception ex) {
			result.put(ERROR, true);
			if (ex.getMessage().contains(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN)) {
				result.put("errorReloadPage", true);
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.exportPayment"), createLogErrorStandard(actionStartTime));
			return JSON;
		}
		return JSON;
	}
	
	/**
	 * Tai file template import thanh toan
	 * 
	 * @author lacnv1
	 * @since Mar 24, 2015
	 */
	public String downloadTemplate() throws Exception {
		try {
			if (currentUser == null || currentUser.getShopRoot() == null
					|| currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (!validateShop()) {
				return JSON;
			}
			Customer customerTemp = null;
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				customerTemp = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), shortCode));
				if (customerTemp == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
					return JSON;
				}
			}
			
			Date fromDateTemp = null;
			Date toDateTemp = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fromDateTemp = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				toDateTemp = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			}

			Staff staffDeliver = new Staff();
			if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
				staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, shopId, ActiveType.RUNNING);
				if (staffDeliver == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.delivery"));
					return JSON;
				}
			}

			Staff staffPayment = new Staff();
			if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
				staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, shopId, ActiveType.RUNNING);
				if (staffPayment == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.cashier"));
					return JSON;
				}
			}

			Staff nvbh = new Staff();
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				nvbh = staffMgr.getStaffByInfoBasic(staffCode, shopId, ActiveType.RUNNING);
				if (nvbh == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "ss.ds3.sheet1.header.nvbh"));
					return JSON;
				}
			}
			if (customerTemp != null && (!ActiveType.RUNNING.equals(customerTemp.getStatus()) || !shopId.equals(customerTemp.getShop().getId()))) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
				return JSON;
			}
			List<CustomerDebitVO> lstDebit = null;
			int NUM = 1;
			if (customerTemp == null || shopId.equals(customerTemp.getShop().getId())) {
				if (type == 0) { // phieu chi
					NUM = -1;
				}
				DebitFilter<CustomerDebitVO> filter = new DebitFilter<CustomerDebitVO>();
				filter.setShopId(shop.getId());
				filter.setShortCode(shortCode);
				filter.setIsReveived(type == 1);
				filter.setFromDate(fromDateTemp);
				filter.setToDate(toDateTemp);
				filter.setDeliveryId(staffDeliver.getId());
				filter.setCashierId(staffPayment.getId());
				filter.setStaffId(nvbh.getId());
				filter.setLstOrderNumbers(lstOrderNumbers);
				lstDebit = debitMgr.getListCustomerDebitVOByFilter(filter);
			}
			
			Map<String, Object> beans = new HashMap<String, Object>();
			if (lstDebit == null || lstDebit.size() == 0) {
				beans.put("hasData", 0);
			} else {
				beans.put("hasData", 1);
				beans.put("lstDebit", lstDebit);
				beans.put("minus", NUM);
			}
			
			String outputPath = ReportUtils.exportXLSWithJxls(beans, Configuration.getExcelTemplatePathCustomerDebit(), ConstantManager.IMPORT_CUSTOMER_DEBIT_BATCH_TEMPLATE, null);
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.downloadTemplate"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Import thanh toan tu excel
	 * 
	 * @author lacnv1
	 * @since Mar 24, 2015
	 */
	public String importExcel() throws Exception {
		resetToken(result);
		try {
			isError = true;
			if (currentUser == null || currentUser.getShopRoot() == null
					|| currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return SUCCESS;
			}
			if (!validateShop()) {
				return SUCCESS;
			}
			totalItem = 0;
			excelFileContentType = "";
			List<CellBean> lstFails = new ArrayList<CellBean>();
			final int NUM_COL = 8;
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, NUM_COL);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				List<String> row = null;
				String value = null;
				String payReceivedNumber = null;
				int col = 0;
				final String PHIEU_THU = "0";
				final String PHIEU_CHI = "1";
//				final String DA_THANH_TOAN = "1";
//				final String CHUA_THANH_TOAN  = "0";
				final int MONEY_MAX_LENGTH = 17;
				Integer receiptType = null;
				StringBuilder message = null;
				String msg = null;
				boolean isPaymentError = false;
				DebitDetail dd = null;
				BigDecimal paid = null;
				BigDecimal amount = null;
				BigDecimal discount = null;
				BigDecimal tmpNo = null;
				Date dateTmp = null;
				DebitCustomerParams debitVO = null;
				List<DebitCustomerParams> lstDebit = new ArrayList<DebitCustomerParams>();
				List<Integer> lstErrorIdx = null;
				List<String> lstPayReceivedNumber = new ArrayList<String>();
				List<String> lstFromObjNumber = new ArrayList<String>();
				boolean isDefferentType = false;
				PayReceived test = null;
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					message = new StringBuilder();
					row = lstData.get(i);
					if (row == null || row.size() < NUM_COL) {
						continue; // khong thuc hien neu so cot doc duoc nho hon so cot tren file mau
					}
					col = 0;
					totalItem++;
					
					receiptType = null;
					isDefferentType = false;
					// Loai thanh toan
					value = row.get(col++);
					if (StringUtil.isNullOrEmpty(payReceivedNumber) && StringUtil.isNullOrEmpty(value)) {
						if (!isPaymentError) {
							debitVO = null;
							message.append("\n").append(R.getResource("import.payment.not.receipt.type"));
						}
					} else if (!StringUtil.isNullOrEmpty(value)) {
						if (!PHIEU_THU.equals(value.trim()) && !PHIEU_CHI.equals(value.trim())) {
							if (!isPaymentError) {
								message.append("\n").append(R.getResource("import.payment.wrong.receipt.type"));
							}
						} else {
							receiptType = Integer.valueOf(value.trim());
							payReceivedNumber = null;
						}
					}
					
					// So chung tu
					value = row.get(col++);
					if (StringUtil.isNullOrEmpty(payReceivedNumber) && StringUtil.isNullOrEmpty(value)) {
						if (!isPaymentError) {
							debitVO = null;
							message.append("\n").append(R.getResource("import.payment.not.number"));
						}
					} else if (!StringUtil.isNullOrEmpty(value) && (payReceivedNumber == null || !payReceivedNumber.equalsIgnoreCase(value.trim()))) {
						if (receiptType == null) {
							if (!isPaymentError) {
								debitVO = null;
								message.append("\n").append(R.getResource("import.payment.not.receipt.type"));
							}
						} else if (lstPayReceivedNumber.contains(value.trim().toUpperCase())) {
							message.append("\n").append(R.getResource("import.payment.pay.received.number.duplicate"));
							debitVO = null;
						} else {
							msg = ValidateUtil.getErrorMsgOfSpecialCharInCode(value, "rpt.7.2.5.pt.soct");
							if (StringUtil.isNullOrEmpty(msg)) {
								payReceivedNumber = value.trim().toUpperCase();
								test = debitMgr.getPayReceivedByNumber(payReceivedNumber, shopId);
								if (test != null) {
									message.append("\n").append(ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, false, "pay.receive.pay.number"));
								}
								debitVO = new DebitCustomerParams();
								debitVO.setPayReceivedNumber(payReceivedNumber);
								debitVO.setReceiptType(receiptType);
								debitVO.setLstDebitId(new ArrayList<Long>());
								debitVO.setLstCustId(new ArrayList<Long>());
								debitVO.setLstDebitAmt(new ArrayList<BigDecimal>());
								debitVO.setLstDiscount(new ArrayList<BigDecimal>());
								debitVO.setShopId(shopId);
								debitVO.setDebtPaymentType(DebtPaymentType.MANUAL);
								debitVO.setPayReceivedType(PayReceivedType.THANH_TOAN);
								debitVO.setAmount(BigDecimal.ZERO);
								debitVO.setCreateUser(currentUser.getUserName());
								isPaymentError = false;
								lstErrorIdx = new ArrayList<Integer>();
								lstPayReceivedNumber.add(payReceivedNumber);
							} else {
								message.append("\n").append(msg.trim());
							}
						}
					} else if (debitVO != null) {
						if (receiptType != null && !receiptType.equals(debitVO.getReceiptType())) {
							message.append("\n").append(R.getResource("import.payment.not.same.receipt.type"));
						}
					}
					
					if (debitVO != null) {
						receiptType = debitVO.getReceiptType();
						// Ngay don hang
						value = row.get(3); // phai dung cot ngay don hang de lay dung don hang no
						if (StringUtil.isNullOrEmpty(value)) {
							message.append("\n").append(ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "baocao.7.2.15.ptdtbhsku.menu11"));
						} else {
							boolean err = DateUtil.checkInvalidFormatDate(value.trim());
							if (err) {
								message.append("\n").append(R.getResource("common.invalid.format.month", R.getResource("baocao.7.2.15.ptdtbhsku.menu11")));
							} else {
								dateTmp = DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY);
							}
						}
						
						// So don hang
						value = row.get(col++);
						if (StringUtil.isNullOrEmpty(value)) {
							message.append("\n").append(ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "rpt.pt.menu2"));
						} else if (dateTmp != null) {
							if (lstFromObjNumber.contains(value.trim().toUpperCase()+"_"+DateUtil.toDateString(dateTmp, DateUtil.DATE_FORMAT_DDMMYYYY))) {
								message.append("\n").append(R.getResource("import.payment.from.object.number.duplicate"));
							} else {
								lstFromObjNumber.add(value.trim().toUpperCase()+"_"+DateUtil.toDateString(dateTmp, DateUtil.DATE_FORMAT_DDMMYYYY));
								dd = debitMgr.getDebitDetailByObjectNumber(shopId, value.trim(), dateTmp);
								if (dd == null) {
									message.append("\n").append(R.getResource("import.payment.not.debit.detail"));
								} else if (!DebitDetailType.BAN_HANG.getValue().equals(dd.getType())
										&& !DebitDetailType.KH_TRA_HANG.getValue().equals(dd.getType())
										&& !DebitDetailType.TANG_NO_KH.getValue().equals(dd.getType())
										&& !DebitDetailType.GIAM_NO_KH.getValue().equals(dd.getType())) {
									message.append("\n").append(R.getResource("import.payment.different.receipt.type"));
									isDefferentType = true;
								} else if ((ReceiptType.RECEIVED.getValue().equals(receiptType)
												&& !DebitDetailType.BAN_HANG.getValue().equals(dd.getType())
												&& !DebitDetailType.TANG_NO_KH.getValue().equals(dd.getType()))
										|| (ReceiptType.PAID.getValue().equals(receiptType)
												&& !DebitDetailType.KH_TRA_HANG.getValue().equals(dd.getType())
												&& !DebitDetailType.GIAM_NO_KH.getValue().equals(dd.getType()))) {
									message.append("\n").append(R.getResource("import.payment.different.receipt.type"));
									isDefferentType = true;
								}
							}
						} else {
							message.append("\n").append(R.getResource("import.payment.not.debit.detail"));
						}
						col++; // bo qua cot ngay don hang do da lay o phia tren
						
						// So tien no
						value = row.get(col++); // cot nay chi co gia tri tham khao, khong xu ly
						
						// So tien
						amount = null;
						value = row.get(col++);
						if (StringUtil.isNullOrEmpty(value)) {
							message.append("\n").append(ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "customerdebit.batch.payreceiptvalue"));
						} else if (value.trim().length() > MONEY_MAX_LENGTH) {
							message.append("\n").append(ValidateUtil.getErrorMsg(ConstantManager.ERR_MAX_LENGTH,
									R.getResource("customerdebit.batch.payreceiptvalue"), MONEY_MAX_LENGTH));
						} else {
							value = value.replace(",", "");
							try {
								amount = new BigDecimal(value);
							} catch (Exception e1) {
								message.append("\n").append(R.getResource("common.possitive.number", R.getResource("customerdebit.batch.payreceiptvalue")));
							}
							if (amount == null || amount.signum() < 1) {
								message.append("\n").append(R.getResource("common.possitive.number", R.getResource("customerdebit.batch.payreceiptvalue")));
							}
							if (amount != null) {
								if (ReceiptType.PAID.getValue().equals(debitVO.getReceiptType())) { // chi
									amount = amount.negate();
								}
								debitVO.setAmount(debitVO.getAmount().add(amount));
							}
							debitVO.getLstDebitAmt().add(amount);
						}
						
						// Chiet khau
						discount = null;
						value = row.get(col++);
						if (StringUtil.isNullOrEmpty(value)) {
							//message.append("\n").append(ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "customerdebit.batch.discount"));
							discount = BigDecimal.ZERO;
							debitVO.getLstDiscount().add(discount);
						} else if (value.trim().length() > MONEY_MAX_LENGTH) {
							message.append("\n").append(ValidateUtil.getErrorMsg(ConstantManager.ERR_MAX_LENGTH,
									R.getResource("customerdebit.batch.discount"), MONEY_MAX_LENGTH));
						} else {
							value = value.replace(",", "");
							try {
								discount = new BigDecimal(value);
							} catch (Exception e1) {
								message.append("\n").append(R.getResource("common.possitive.number", R.getResource("customerdebit.batch.discount")));
							}
							if (discount == null || discount.signum() < 1) {
								message.append("\n").append(R.getResource("common.possitive.number", R.getResource("customerdebit.batch.discount")));
							}
							if (discount != null && ReceiptType.PAID.getValue().equals(debitVO.getReceiptType())) { // chi
								discount = discount.negate();
							}
							debitVO.getLstDiscount().add(discount);
						}
						
						if (!isDefferentType && amount != null && discount != null && dd != null) {
							tmpNo = amount.add(discount);
							if (ReceiptType.RECEIVED.getValue().equals(debitVO.getReceiptType())
									&& (dd.getRemain() == null || dd.getRemain().compareTo(tmpNo) < 0)) { // thu
								message.append("\n").append(R.getResource("customerdebit.batch.import.receipt.smaller.or.equal"));
							} else if (ReceiptType.PAID.getValue().equals(debitVO.getReceiptType())
									&& (dd.getRemain() == null || dd.getRemain().compareTo(tmpNo) > 0)) {
								message.append("\n").append(R.getResource("customerdebit.batch.import.pay.smaller.or.equal"));
							} else {
								paid = debitMgr.getPaymentAmountOfDebitDetail(dd.getId(), null);
								if (ReceiptType.RECEIVED.getValue().equals(receiptType)) {
									if (paid.add(tmpNo).compareTo(dd.getTotal()) > 0) {
										message.append("\n").append(R.getResource("debit.detail.da.lap.het.phieu.thu", dd.getFromObjectNumber()));
									} else {
										debitVO.getLstDebitId().add(dd.getId());
										debitVO.getLstCustId().add(dd.getDebit().getObjectId());
									}
								} else if (paid.add(tmpNo).compareTo(dd.getTotal()) < 0) {
									message.append("\n").append(R.getResource("debit.detail.da.lap.het.phieu.chi", dd.getFromObjectNumber()));
								} else {
									debitVO.getLstDebitId().add(dd.getId());
									debitVO.getLstCustId().add(dd.getDebit().getObjectId());
								}
							}
						}
						
						// Trang thai
//						value = row.get(col++);
//						if (debitVO.getLstCustId().size() < 2 && StringUtil.isNullOrEmpty(value)) {
//							if (!isPaymentError) {
//								message.append("\n").append(ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "po.conf.group.err.imp.status"));
//							}
//						} else if (!StringUtil.isNullOrEmpty(value)) {
//							if (!CHUA_THANH_TOAN.equals(value.trim()) && !DA_THANH_TOAN.equals(value.trim())) {
//								message.append("\n").append(R.getResource("import.payment.wrong.status"));
//							} else {
//								debitVO.setPaymentStatus(Integer.valueOf(value.trim()));
//							}
//						}
						debitVO.setPaymentStatus(PaymentStatus.PAID.getValue());
					}
					
					// kiem tra xuat thong bao loi
					msg = message.toString();
					if (isPaymentError && StringUtil.isNullOrEmpty(msg)) {
						msg = R.getResource("import.payment.errorpayment");
					}
					if (!StringUtil.isNullOrEmpty(msg)) {
						if (!isPaymentError) {
							isPaymentError = true;
							if (debitVO != null && lstErrorIdx != null && lstErrorIdx.size() > 0) {
								List<String> rowTmp = null;
								for (int j = 0, szj = lstErrorIdx.size(); j < szj; j++) {
									rowTmp = lstData.get(lstErrorIdx.get(j));
									lstFails.add(StringUtil.addFailBean(rowTmp, R.getResource("import.payment.errorpayment")));
								}
								rowTmp = null;
								lstErrorIdx = null;
								lstDebit.remove(debitVO);
							}
						}
						msg = msg.replaceFirst("\\n", "");
						lstFails.add(StringUtil.addFailBean(row, msg));
						continue; // dong bi loi
					}
					
					lstErrorIdx.add(i);
					if (debitVO != null && !lstDebit.contains(debitVO)) {
						lstDebit.add(debitVO);
					}
				}
				debitMgr.updateListDebitOfCustomer(lstDebit, null);
				getOutputFailExcelFile(lstFails, ConstantManager.IMPORT_CUSTOMER_DEBIT_BATCH_FAIL);
				isError = false;
			} else if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = R.getResource("import.excel.empty.content");
				return SUCCESS;
			}
		} catch (Exception ex) {
			isError = true;
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.importExcel"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return SUCCESS;
	}

	/**
	 * Thay doi NVTT cong no KH
	 * 
	 * @author nhutnn
	 * @since 17/11/2015
	 */
	public String updateCustomerDebitDetail() throws Exception {
		resetToken(result);
		actionStartTime = DateUtil.now();
		result.put(ERROR, false);
		String msg = "";
		try {
			if (currentUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			if (!validateShop()) {
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(msg) && (lstCustomerDebitID == null || lstCustomerDebitID.isEmpty())) {
				msg = R.getResource("import.payment.pay.received.no.detail.update");
			}
			Staff cashierStaff = null;
			if (StringUtil.isNullOrEmpty(msg)) {
				cashierStaff = staffMgr.getStaffByCode(staffCode);
				msg = validateCashierStaff(staffCode, cashierStaff);
			}

			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}

			DebitFilter<CustomerDebitVO> filter = new DebitFilter<CustomerDebitVO>();
			filter.setShopId(shopId);
			filter.setLstDebitDetailId(lstCustomerDebitID);
			filter.setUserName(currentUser.getUserName());
			filter.setCashierStaff(cashierStaff);
			debitMgr.updateCashierForListDebitDetail(filter);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "vnm.web.action.CustomerDebitBatchAction.updateCustomerDebitDetail"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}

		return JSON;
	}
	
	/**
	 * validate NVTT/NVGH:
	 * - maxlength ma NVTT/NVGH
	 * - NVTT/NVGH phai o trang thai hoat dong
	 * 
	 * @author nhutnn
	 * @param cashierCode ma NVTT
	 * @param cashierStaff entity NVTT
	 * @return thong bao loi
	 * @throws BusinessException 
	 * @since 17/11/2015
	 */
	private String validateCashierStaff(String cashierCode, Staff cashierStaff) throws BusinessException {
		String msg = ValidateUtil.validateField(cashierCode, "pay.received.ma.nvtt", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
		if (!StringUtil.isNullOrEmpty(msg)) {
			return msg;
		}
		if (cashierStaff == null) {
			msg = R.getResource("common.not.exist.in.db", R.getResource("pay.received.ma.nvtt", cashierCode));
		} else if (!ActiveType.RUNNING.equals(cashierStaff.getStatus())) {
			msg = R.getResource("common.catalog.status.in.active", R.getResource("pay.received.ma.nvtt", cashierCode));
		} else if (cashierStaff.getStaffType() != null && cashierStaff.getStaffType().getSpecificType() != null
				&& !(StaffSpecificType.NVTT.getValue().equals(cashierStaff.getStaffType().getSpecificType().getValue()) || StaffSpecificType.NVGH.getValue().equals(cashierStaff.getStaffType().getSpecificType().getValue()))) {
			msg = R.getResource("pay.received.ma.nvtt.invalid");
		} else if (cashierStaff.getShop() == null || !shopId.equals(cashierStaff.getShop().getId())) {
			msg = R.getResource("pay.received.ma.nvtt.invalid.shop");
		}
		return msg;
	}
	
	private File excelFile;
	private String excelFileContentType;

	//private String staff2Code;
	private Customer customer;
	private List<CellBean> lstView;
	private Boolean typeView;
	private Integer isView;

	private String arrPayDiscountStr;
	private String arrRemainStr;
	private String arrTotalStr;
	private String arrNVTTStr;
	private String arrPayAmountStr;

	public String getArrNVTTStr() {
		return arrNVTTStr;
	}

	public void setArrNVTTStr(String arrNVTTStr) {
		this.arrNVTTStr = arrNVTTStr;
	}

	public String getArrPayDiscountStr() {
		return arrPayDiscountStr;
	}

	public void setArrPayDiscountStr(String arrPayDiscountStr) {
		this.arrPayDiscountStr = arrPayDiscountStr;
	}

	public String getArrRemainStr() {
		return arrRemainStr;
	}

	public void setArrRemainStr(String arrRemainStr) {
		this.arrRemainStr = arrRemainStr;
	}

	public String getArrTotalStr() {
		return arrTotalStr;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/*
	 * public String getStaff2Code() { return staff2Code; }
	 * 
	 * public void setStaff2Code(String staff2Code) { this.staff2Code =
	 * staff2Code; }
	 */

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<CellBean> getLstView() {
		return lstView;
	}

	public void setLstView(List<CellBean> lstView) {
		this.lstView = lstView;
	}

	public Boolean getTypeView() {
		return typeView;
	}

	public void setTypeView(Boolean typeView) {
		this.typeView = typeView;
	}

	public Integer getIsView() {
		return isView;
	}

	public void setIsView(Integer isView) {
		this.isView = isView;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public List<BigDecimal> getLstPayAmount() {
		return lstPayAmount;
	}

	public void setLstPayAmount(List<BigDecimal> lstPayAmount) {
		this.lstPayAmount = lstPayAmount;
	}

	public List<BigDecimal> getLstDiscount() {
		return lstDiscount;
	}

	public void setLstDiscount(List<BigDecimal> lstDiscount) {
		this.lstDiscount = lstDiscount;
	}

	public String getArrPayAmountStr() {
		return arrPayAmountStr;
	}

	public void setArrPayAmountStr(String arrPayAmountStr) {
		this.arrPayAmountStr = arrPayAmountStr;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getPayerAddress() {
		return payerAddress;
	}

	public void setPayerAddress(String payerAddress) {
		this.payerAddress = payerAddress;
	}

	public String getPaymentReason() {
		return paymentReason;
	}

	public void setPaymentReason(String paymentReason) {
		this.paymentReason = paymentReason;
	}

	public String getLstOrderNumbers() {
		return lstOrderNumbers;
	}

	public void setLstOrderNumbers(String lstOrderNumbers) {
		this.lstOrderNumbers = lstOrderNumbers;
	}
	
	public List<String> getLstBankCode() {
		return lstBankCode;
	}

	public void setLstBankCode(List<String> lstBankCode) {
		this.lstBankCode = lstBankCode;
	}

	public String getLstDebitId() {
		return lstDebitId;
	}

	public void setLstDebitId(String lstDebitId) {
		this.lstDebitId = lstDebitId;
	}

	public List<String> getLstStaffCode() {
		return lstStaffCode;
	}

	public void setLstStaffCode(List<String> lstStaffCode) {
		this.lstStaffCode = lstStaffCode;
	}
	
	public String getStaffDeliverCode() {
		return staffDeliverCode;
	}

	public void setStaffDeliverCode(String staffDeliverCode) {
		this.staffDeliverCode = staffDeliverCode;
	}

	public String getStaffPaymentCode() {
		return staffPaymentCode;
	}

	public void setStaffPaymentCode(String staffPaymentCode) {
		this.staffPaymentCode = staffPaymentCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getPayreceiptCode() {
		return payreceiptCode;
	}

	public void setPayreceiptCode(String payreceiptCode) {
		this.payreceiptCode = payreceiptCode;
	}

	public BigDecimal getPayreceiptValue() {
		return payreceiptValue;
	}

	public void setPayreceiptValue(BigDecimal payreceiptValue) {
		this.payreceiptValue = payreceiptValue;
	}

	public BigDecimal getDebitPreRemain() {
		return debitPreRemain;
	}

	public void setDebitPreRemain(BigDecimal debitPreRemain) {
		this.debitPreRemain = debitPreRemain;
	}

	public BigDecimal getDebitPostRemain() {
		return debitPostRemain;
	}

	public void setDebitPostRemain(BigDecimal debitPostRemain) {
		this.debitPostRemain = debitPostRemain;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public CustomerMgr getCustomerMgr() {
		return customerMgr;
	}

	public void setCustomerMgr(CustomerMgr customerMgr) {
		this.customerMgr = customerMgr;
	}

	public DebitMgr getDebitMgr() {
		return debitMgr;
	}

	public void setDebitMgr(DebitMgr debitMgr) {
		this.debitMgr = debitMgr;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public List<CustomerDebitVO> getLstOrderDebit() {
		return lstOrderDebit;
	}

	public void setLstOrderDebit(List<CustomerDebitVO> lstOrderDebit) {
		this.lstOrderDebit = lstOrderDebit;
	}

	public List<Long> getLstCustomerDebitID() {
		return lstCustomerDebitID;
	}

	public void setLstCustomerDebitID(List<Long> lstCustomerDebitID) {
		this.lstCustomerDebitID = lstCustomerDebitID;
	}

	public List<Bank> getListBank() {
		return listBank;
	}

	public void setListBank(List<Bank> listBank) {
		this.listBank = listBank;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public List<StaffVO> getLstNVTTVo() {
		return lstNVTTVo;
	}

	public void setLstNVTTVo(List<StaffVO> lstNVTTVo) {
		this.lstNVTTVo = lstNVTTVo;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
}
package ths.dms.web.action.customerdebit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Debit;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DebitPaymentType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.DebitFilter;
import ths.dms.core.entities.vo.DebitCustomerVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

public class CustomerDebitShowAction extends AbstractAction {

	private static final long serialVersionUID = -3879932000634710804L;
	private String shortCode; // Mã ngắn KH
	private Date fromDate; // Từ Ngày
	private Date toDate; // Đến Ngày
	private String fromDateS;
	private String toDateS;
	private String customerName; // Tên Khách Hàng
	private List<DebitCustomerVO> listdebitCustomer;
	private List<CellBean> listBeans;
	private InputStream inputStream;
	private String totalDebit;
	private String outputName;
	private Integer status;
	private String statusPay;
	
	private List<StaffVO> lstNVBHVo;
	private List<StaffVO> lstNVGHVo;
	private List<StaffVO> lstNVTTVo;
	private String staffDeliverCode;
	private String staffPaymentCode;
	private String staffCode;
	
	/** vuongmq; 05/06/2015; xem danh sach don hang nhieu*/
	private String lstOrderNumbers;
	private Staff staff;
	private Customer customer;
	private StaffMgr staffMgr;
	private DebitMgr deBitMgr;
	private CustomerMgr customerMgr;
	private String shopCode;
	private Shop shop;
	private ShopMgr shopMgr;
	private Integer checkdata;

	@Override
	public void prepare() throws Exception {
		try {
			super.prepare();
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitShowAction.prepare"), createLogErrorStandard(actionStartTime));
		}
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		deBitMgr = (DebitMgr) context.getBean("debitMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
//		try {
//			if (currentUser == null || currentUser.getShopRoot() == null
//					|| currentUser.getShopRoot().getShopId() == null) {
//				return PAGE_NOT_PERMISSION;
//			}
//			fromDateS = null;
//			toDateS = fromDateS;
//			if (currentUser != null && currentUser.getUserName() != null) {
//				staff = staffMgr.getStaffByCode(currentUser.getUserName());
//				if (staff != null && staff.getShop() != null) {
//					shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
//					if (shop == null) {
//						shop = staff.getShop();
//					}
//					shopCode = shop.getShopCode();
//					
//					StaffFilter staffFilter = new StaffFilter();
//					staffFilter.setStatus(ActiveType.RUNNING);
//					staffFilter.setShopCode(shop.getShopCode());
//					staffFilter.setIsGetShopOnly(true);
//					staffFilter.setLstStaffType(Arrays.asList(StaffObjectType.NVTT.getValue(), StaffObjectType.NVGH.getValue()));
//					ObjectVO<Staff> staffVO = staffMgr.getListStaff(staffFilter);
//					if (staffVO != null) {
//						listNVTT = staffVO.getLstObject();
//					}
//					if (listNVTT != null) {
//						listNVGH = new ArrayList<Staff>();
//						listNVGH.addAll(listNVTT);
//					}
//					staffVO = staffMgr.getListPreAndVanStaff(null, null, null, shop.getId(), ActiveType.RUNNING, Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
//					if (staffVO != null) {
//						listNVBH = staffVO.getLstObject();
//					}
//				}
//			}
//		} catch (Exception ex) {
//			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitShowAction.execute"), createLogErrorStandard(actionStartTime));
//		}
		return SUCCESS;
	}

	public String getInfo() {
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return ERROR;
			}
			result.put("page", page);
			result.put("max", max);
			if (!validateShop()) {
				return JSON;
			}

			if (!StringUtil.isNullOrEmpty(fromDateS)) {
				fromDate = ths.dms.web.utils.DateUtil.parse(fromDateS, ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(toDateS)) {
				toDate = ths.dms.web.utils.DateUtil.parse(toDateS, ConstantManager.FULL_DATE_FORMAT);
			} else {
				toDate = DateUtil.getCurrentGMTDate();
			}
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), shortCode));
				if (customer == null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer");
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					result.put("rows", new ArrayList<DebitCustomerVO>());
					result.put("total", 0);
					return JSON;
				}
				Debit db = deBitMgr.getDebit(customer.getId(), DebitOwnerType.CUSTOMER);
				if (db == null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customer.debit.not.exist");
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					result.put("rows", new ArrayList<DebitCustomerVO>());
					result.put("total", 0);
					return JSON;
				}
				totalDebit = convertMoney(db.getTotalDebit());
			}		
			totalDebit = "0";
			DebitPaymentType typeTime = null;
			if (status != null && status != -1) {
				typeTime = DebitPaymentType.parseValue(status);
			}

			Staff staffDeliver = new Staff();
			if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
				staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, shopId, ActiveType.RUNNING);
				if (staffDeliver == null) {
					result.put(ERROR, true);
					result.put("rows", new ArrayList<DebitCustomerVO>());
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.delivery"));
					return JSON;
				}
			}

			Staff staffPayment = new Staff();
			if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
				staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, shopId, ActiveType.RUNNING);
				if (staffPayment == null) {
					result.put(ERROR, true);
					result.put("rows", new ArrayList<DebitCustomerVO>());
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.cashier"));
					return JSON;
				}
			}

			Staff nvbh = new Staff();
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				nvbh = staffMgr.getStaffByInfoBasic(staffCode, shopId, ActiveType.RUNNING);
				if (nvbh == null) {
					result.put(ERROR, true);
					result.put("rows", new ArrayList<DebitCustomerVO>());
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "ss.ds3.sheet1.header.nvbh"));
					return JSON;
				}
			}

			List<Integer> statusPayment = null;
			if (!StringUtil.isNullOrEmpty(statusPay)) {
				String[] lstStatus = statusPay.split(",");
				statusPayment = new ArrayList<Integer>();
				for (int i = 0, sz = lstStatus.length; i < sz; i++) {
					statusPayment.add(Integer.valueOf(lstStatus[i]));
				}
			}
			Date lockDay = shopLockMgr.getApplicationDate(shopId);
			if (lockDay == null) {
				lockDay = commonMgr.getSysDate();
			}
			/** vuongmq; 05/06/2015; xem danh sach don hang nhieu; them lstOrderNumbers*/
			KPaging<DebitCustomerVO> paging = new KPaging<DebitCustomerVO>();
			paging.setPage(page - 1);
			paging.setPageSize(max);
			DebitFilter<DebitCustomerVO> filter = new DebitFilter<DebitCustomerVO>();
			filter.setPaging(paging);
			filter.setShopId(shopId);
			filter.setShortCode(shortCode);
			filter.setFromDate(fromDate);
			filter.setToDate(toDate);
			filter.setDebitPaymentType(typeTime);
			filter.setDeliveryId(staffDeliver.getId());
			filter.setCashierId(staffPayment.getId());
			filter.setStaffId(nvbh.getId());
			filter.setStatusPayment(statusPayment);
			filter.setLstOrderNumbers(lstOrderNumbers);
			filter.setLockDate(lockDay);
			ObjectVO<DebitCustomerVO> objVO = deBitMgr.getListDebitCustomerVO(filter);
			if (objVO != null) {
				//listdebitCustomer = objVO.getLstObject();
				result.put("rows", objVO.getLstObject());
				result.put("total", objVO.getkPaging().getTotalRows());
			} else {
				result.put("rows", new ArrayList<DebitCustomerVO>());
				result.put("total", 0);
			}
			/*listdebitCustomer = deBitMgr.getDebitCustomer(shop.getId(),	null, shortCode, customerName, fromDate, toDate,typeTime);
			if(listdebitCustomer==null){
				listdebitCustomer = new ArrayList<DebitCustomerVO>();
			}*/
			//result.put("rows", listdebitCustomer);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitShowAction.getInfo"), createLogErrorStandard(actionStartTime));
			result.put("rows", new ArrayList<DebitCustomerVO>());
			result.put("total", 0);
		}
		return JSON;
	}

	public String exportDebitCustomer() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return ERROR;
			}
			if (!validateShop()) {
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(fromDateS)) {
				fromDate = ths.dms.web.utils.DateUtil.parse(fromDateS, ConstantManager.FULL_DATE_FORMAT);
			}
			if(!StringUtil.isNullOrEmpty(toDateS)){
				toDate = ths.dms.web.utils.DateUtil.parse(toDateS, ConstantManager.FULL_DATE_FORMAT);
			} else {
				toDate = DateUtil.getCurrentGMTDate();
			}
			listdebitCustomer = new ArrayList<DebitCustomerVO>();
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), shortCode));
				if (customer == null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer");
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
			}
			DebitPaymentType typeTime = null;
			if (status != null && status != -1) {
				typeTime = DebitPaymentType.parseValue(status);
			}

			Staff staffDeliver = new Staff();
			if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
				staffDeliver = staffMgr.getStaffByCode(staffDeliverCode);
				if (staffDeliver == null) {
					result.put(ERROR, true);
					result.put("rows", new ArrayList<DebitCustomerVO>());
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.delivery"));
					return JSON;
				}
			}

			Staff staffPayment = new Staff();
			if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
				staffPayment = staffMgr.getStaffByCode(staffPaymentCode);
				if (staffPayment == null) {
					result.put(ERROR, true);
					result.put("rows", new ArrayList<DebitCustomerVO>());
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.cashier"));
					return JSON;
				}
			}

			Staff nvbh = new Staff();
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				nvbh = staffMgr.getStaffByCode(staffCode);
				if (nvbh == null) {
					result.put(ERROR, true);
					result.put("rows", new ArrayList<DebitCustomerVO>());
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "ss.ds3.sheet1.header.nvbh"));
					return JSON;
				}
			}
			List<Integer> statusPayment = null;
			if (!StringUtil.isNullOrEmpty(statusPay)) {
				String[] lstStatus = statusPay.split(",");
				statusPayment = new ArrayList<Integer>();
				for (int i = 0, sz = lstStatus.length; i < sz; i++) {
					statusPayment.add(Integer.valueOf(lstStatus[i]));
				}
			}
			/**
			 * vuongmq; 05/06/2015; xem danh sach don hang nhieu; them
			 * lstOrderNumbers
			 */
			Date lockDay = shopLockMgr.getApplicationDate(shopId);
			if (lockDay == null) {
				lockDay = commonMgr.getSysDate();
			}
			DebitFilter<DebitCustomerVO> filter = new DebitFilter<DebitCustomerVO>();
			filter.setShopId(shop.getId());
			filter.setShortCode(shortCode);
			filter.setFromDate(fromDate);
			filter.setToDate(toDate);
			filter.setDebitPaymentType(typeTime);
			filter.setDeliveryId(staffDeliver.getId());
			filter.setCashierId(staffPayment.getId());
			filter.setStaffId(nvbh.getId());
			filter.setStatusPayment(statusPayment);
			filter.setLstOrderNumbers(lstOrderNumbers);
			filter.setLockDate(lockDay);
			ObjectVO<DebitCustomerVO> objVO = deBitMgr.getListDebitCustomerVO(filter);
			if (objVO != null) {
				listdebitCustomer = objVO.getLstObject();
			} else {
				listdebitCustomer = null;
			}

			//listdebitCustomer = deBitMgr.getDebitCustomer(shop.getId(),null, shortCode, customerName, fromDate, toDate, typeTime);
			BigDecimal remainAmount = BigDecimal.ZERO;
			//Map<String, BigDecimal> remainAmountMap = new HashMap<String, BigDecimal>();
			Date payDate = null;
			BigDecimal dTemp = null;
			if (listdebitCustomer != null && listdebitCustomer.size() > 0) {
				for (int i = 0; i < listdebitCustomer.size(); i++) {
					payDate = listdebitCustomer.get(i).getPayDate();
					dTemp = listdebitCustomer.get(i).getAmountRemain();
					if (BigDecimal.ZERO.equals(dTemp)) {
						listdebitCustomer.get(i).setPayType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.show.debit.paytype.done"));
					} else if (payDate == null || getTypeDebitCustomer(payDate) == 3) {
						listdebitCustomer.get(i).setPayType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.show.debit.paytype.not.critical"));
					} else if (getTypeDebitCustomer(payDate) == 1) {
						listdebitCustomer.get(i).setPayType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.show.debit.paytype.overdue"));
					} else if (getTypeDebitCustomer(payDate) == 2) {
						listdebitCustomer.get(i).setPayType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.show.debit.paytype.critical"));
					}
					//remainAmountMap.put(listdebitCustomer.get(i).getShortCode(), dTemp);
					remainAmount = remainAmount.add(dTemp);
				}
			}			
			
			/*for(String key : remainAmountMap.keySet()) {
				remainAmount = remainAmount.add(remainAmountMap.get(key));
			}*/
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report", listdebitCustomer);
			String shopName = "";
			String shopAddress = "";
			if (request.getSession().getAttribute(ConstantManager.SESSION_SHOP) != null) {
				Shop shop = (Shop) request.getSession().getAttribute(ConstantManager.SESSION_SHOP);
				if (shop != null) {
					shopName = shop.getShopName();
					shopAddress = shop.getAddress();
					if (!StringUtil.isNullOrEmpty(shortCode)) {
						Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), shortCode));
						if (customer == null) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer");
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						Debit db = deBitMgr.getDebit(customer.getId(), DebitOwnerType.CUSTOMER);
						if (db == null) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customer.debit.not.exist");
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						totalDebit = convertMoney(db.getTotalDebit());
					}
					totalDebit = "0";
				}
			}
			params.put("shopName", shopName);
			params.put("shopAddress", shopAddress);
			params.put("fromDate", DateUtil.toDateString(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			params.put("toDate", DateUtil.toDateString(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			params.put("customerName", customerName);
			params.put("toDay", DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR));
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			params.put("dateFormat", dateFormat);
			params.put("totalDebit", totalDebit);
			params.put("remainAmount", remainAmount);

			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCustomerDebit();//Configuration.getStoreRealPath();
			String templateFileName = folder + ConstantManager.EXPORT_CUSTOMER_DEBIT_TEMPLATE_EXPORT;
			templateFileName = templateFileName.replace('/', File.separatorChar);

			outputName = ConstantManager.EXPORT_USTOMER_DEBIT_TEMPLATE_FILE_NAME + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION;
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, params);

			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();

			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);

			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitShowAction.exportDebitCustomer"), createLogErrorStandard(actionStartTime));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @return String
	 * @since Dec 4, 2015
	 */
	public String changeShop() {
		result.put("lstSale", new ArrayList<StaffVO>());
		result.put("lstCashier", new ArrayList<StaffVO>());
		result.put("lstDeliver", new ArrayList<StaffVO>());
		result.put("listBank", new ArrayList<StaffVO>());
		result.put("fromDate", dayLock);
		result.put("toDate", dayLock);
		try {
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return ERROR;
			}
			if (!validateShop()) {
				return JSON;
			}

			StaffFilter filter = new StaffFilter();
			filter.setShopId(shop.getId());
			filter.setStatus(ActiveType.RUNNING);
			filter.setSpecType(StaffSpecificType.STAFF);

			//Lay NVBH
			filter.setStrListUserId(super.getStrListUserId());
			ObjectVO<StaffVO> objVO = staffMgr.getListStaffVO(filter);
			lstNVBHVo = objVO.getLstObject();

			String strLstShopId = super.getStrListShopId();
			//Lay nhan vien thu tien
			filter.setStrListUserId(null);
			filter.setStrListShopId(strLstShopId);
			filter.setSpecType(StaffSpecificType.NVTT);
			ObjectVO<StaffVO> objVOCash = staffMgr.getListStaffVO(filter);
			lstNVTTVo = objVOCash.getLstObject();

			//Lay nhan vien NVGH
			filter.setStrListUserId(null);
			filter.setStrListShopId(strLstShopId);
			filter.setSpecType(StaffSpecificType.NVGH);
			ObjectVO<StaffVO> objVODeliver = staffMgr.getListStaffVO(filter);
			lstNVGHVo = objVODeliver.getLstObject();

			Date lockDate = shopLockMgr.getNextLockedDay(shopId);
			if (lockDate == null) {
				lockDate = commonMgr.getSysDate();
			}
			fromDateS = DateUtil.toDateString(lockDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			
			if (lstNVBHVo != null) {
				result.put("lstSale", lstNVBHVo);
			}
			if (lstNVTTVo != null) {
				result.put("lstCashier", lstNVTTVo);
			}
			if (lstNVGHVo != null) {
				result.put("lstDeliver", lstNVGHVo);
			}
			
			result.put("fromDate", fromDateS);
			result.put("toDate", fromDateS);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitShowAction.changeShop"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	private boolean validateShop() throws BusinessException {
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			shop = shopMgr.getShopByCode(shopCode);
		} else {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
			return false;
		}
		if (shop == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
			return false;
		}
		shopId = shop.getId();
		if (super.getMapShopChild().get(shopId) == null) {
			result.put(ERROR, true);
			result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
			return false;
		}
		return true;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getStaffDeliverCode() {
		return staffDeliverCode;
	}

	public void setStaffDeliverCode(String staffDeliverCode) {
		this.staffDeliverCode = staffDeliverCode;
	}

	public String getStaffPaymentCode() {
		return staffPaymentCode;
	}

	public void setStaffPaymentCode(String staffPaymentCode) {
		this.staffPaymentCode = staffPaymentCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStatusPay() {
		return statusPay;
	}

	public void setStatusPay(String statusPay) {
		this.statusPay = statusPay;
	}

	public String getLstOrderNumbers() {
		return lstOrderNumbers;
	}

	public void setLstOrderNumbers(String lstOrderNumbers) {
		this.lstOrderNumbers = lstOrderNumbers;
	}

	public List<StaffVO> getLstNVBHVo() {
		return lstNVBHVo;
	}

	public void setLstNVBHVo(List<StaffVO> lstNVBHVo) {
		this.lstNVBHVo = lstNVBHVo;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public List<StaffVO> getLstNVTTVo() {
		return lstNVTTVo;
	}

	public void setLstNVTTVo(List<StaffVO> lstNVTTVo) {
		this.lstNVTTVo = lstNVTTVo;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(String totalDebit) {
		this.totalDebit = totalDebit;
	}

	public List<CellBean> getListBeans() {
		return listBeans;
	}

	public void setListBeans(List<CellBean> listBeans) {
		this.listBeans = listBeans;
	}

	public List<DebitCustomerVO> getListdebitCustomer() {
		return listdebitCustomer;
	}

	public void setListdebitCustomer(List<DebitCustomerVO> listdebitCustomer) {
		this.listdebitCustomer = listdebitCustomer;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public String getOutputName() {
		return outputName;
	}

	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public DebitMgr getDeBitMgr() {
		return deBitMgr;
	}

	public void setDeBitMgr(DebitMgr deBitMgr) {
		this.deBitMgr = deBitMgr;
	}

	public CustomerMgr getCustomerMgr() {
		return customerMgr;
	}

	public void setCustomerMgr(CustomerMgr customerMgr) {
		this.customerMgr = customerMgr;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public InputStream getInputStream() {
		return inputStream;
	}

	public Integer getCheckdata() {
		return checkdata;
	}

	public void setCheckdata(Integer checkdata) {
		this.checkdata = checkdata;
	}

	public String getFromDateS() {
		return fromDateS;
	}

	public void setFromDateS(String fromDateS) {
		this.fromDateS = fromDateS;
	}

	public String getToDateS() {
		return toDateS;
	}

	public void setToDateS(String toDateS) {
		this.toDateS = toDateS;
	}
}

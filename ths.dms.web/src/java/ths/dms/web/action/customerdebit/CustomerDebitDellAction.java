package ths.dms.web.action.customerdebit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.DebitCustomerParams;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class CustomerDebitDellAction extends AbstractAction {

	private static final long serialVersionUID = -3879932000634710804L;
	private Long debitId;
	private BigDecimal fromMoney;
	private BigDecimal toMoney;
	private String shortCode;
	private String orderNumber;
	private List<CustomerDebitVO> listCustomerDebit;
	private Staff staff;
	private StaffMgr staffMgr;
	private Shop shop;
	private ShopMgr shopMgr;
	private DebitMgr deBitMgr;
	private CustomerMgr customerMgr;
	private Customer customer;
	private Long shopId;
	private String shopCode;
	private List<Long> lstDebitId;
	private List<BigDecimal> lstDebitAmt;
	private List<Long> lstDebitId1;
	private List<BigDecimal> lstDebitAmt1;
	private List<StaffVO> lstNVBHVo;
	private List<StaffVO> lstNVGHVo;
	private List<StaffVO> lstNVTTVo;
	private String fromDate;
	private String toDate;
	private String staffDeliverCode;
	private String staffPaymentCode;
	private String staffCode;

	@Override
	public void prepare() throws Exception {
		try {
			super.prepare();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitDellAction.prepare"), createLogErrorStandard(actionStartTime));
		}
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		deBitMgr = (DebitMgr) context.getBean("debitMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getShopRoot().getShopId() == null) {
				return PAGE_NOT_PERMISSION;
			}
			staff = getStaffByCurrentUser();
			shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			if (shop == null) {
				shop = staff.getShop();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitDellAction.execute"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	public String getInfo() {
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return ERROR;
			}
			if (!validateShop()) {
				return JSON;
			}
			if (fromMoney == null && toMoney == null) {
				listCustomerDebit = new ArrayList<CustomerDebitVO>();
				result.put("rows", listCustomerDebit);
				return JSON;
			}
			Date fromDateTemp = null;
			Date toDateTemp = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fromDateTemp = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				toDateTemp = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			}
			Staff staffDeliver = new Staff();
			if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
				staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, shopId, ActiveType.RUNNING);
			}
			Staff staffPayment = new Staff();
			if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
				staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, shopId, ActiveType.RUNNING);
			}
			Staff nvbh = null;
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				nvbh = staffMgr.getStaffByInfoBasic(staffCode, shopId, ActiveType.RUNNING);
			}
			listCustomerDebit = deBitMgr.getListCustomerDebitVO(shopId, shortCode, orderNumber, null, fromMoney, toMoney, null, fromDateTemp, toDateTemp, staffDeliver.getId(), staffPayment.getId(), nvbh == null ? null : nvbh.getId());
			if (listCustomerDebit == null) {
				listCustomerDebit = new ArrayList<CustomerDebitVO>();
			}
			result.put("rows", listCustomerDebit);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitDellAction.getInfo"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	public String removedebit() {
		resetToken(result);
		boolean error = true;
		errMsg = "";
		//Long customerId = null;
		/** modify by lacnv1 - khong xet theo KH */
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getShopRoot().getShopId() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return ERROR;
			}
			staff = getStaffByCurrentUser();
			if (staff == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			if (!validateShop()) {
				return JSON;
			}
			List<DebitCustomerParams> lstDebits = new ArrayList<DebitCustomerParams>();
			List<Long> lstCustIds = null;
			DebitCustomerParams custDebit = null;
			DebitDetail debitdetail = null;
			Long custId = null;
			Customer cust = null;
			List<Long> lstDebitIds = null;
			List<BigDecimal> lstDebitAmts = null;
			int idx = -1;
			//phuongvm - kiem tra neu da lap het phieu thu thong bao khong cho xoa
			BigDecimal paid = null;
			if (lstDebitId != null && lstDebitAmt != null && lstDebitId.size() > 0 && lstDebitAmt.size() > 0) {
				for (int i = 0, sz = lstDebitId.size(); i < sz; i++) {
					debitdetail = deBitMgr.getDebitDetailById(lstDebitId.get(i));
					paid = deBitMgr.getPaymentAmountOfDebitDetail(lstDebitId.get(i), null);
					if (paid == null) {
						paid = BigDecimal.ZERO;
					}
					BigDecimal dbAmt = BigDecimal.ZERO;
					if (lstDebitAmt.size() > i && lstDebitAmt.get(i) != null) {
						dbAmt = lstDebitAmt.get(i);
					}
					if (paid.add(dbAmt).compareTo(debitdetail.getTotal()) > 0) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("debit.detail.da.lap.het.phieu.thu.xoa.du.no", debitdetail.getFromObjectNumber()));
						return JSON;
					}
				}
			}
			//phuongvm - kiem tra neu da lap het phieu chi thong bao khong cho xoa
			if (lstDebitId1 != null && lstDebitAmt1 != null && lstDebitId1.size() > 0 && lstDebitAmt1.size() > 0) {
				for (int i = 0, sz = lstDebitId1.size(); i < sz; i++) {
					debitdetail = deBitMgr.getDebitDetailById(lstDebitId1.get(i));
					paid = deBitMgr.getPaymentAmountOfDebitDetail(lstDebitId1.get(i), null);
					if (paid == null) {
						paid = BigDecimal.ZERO;
					}
					BigDecimal dbAmt = BigDecimal.ZERO;
					if (lstDebitAmt1.size() > i && lstDebitAmt1.get(i) != null) {
						dbAmt = lstDebitAmt1.get(i);
					}
					if (paid.add(dbAmt).compareTo(debitdetail.getTotal()) < 0) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("debit.detail.da.lap.het.phieu.chi.xoa.du.no", debitdetail.getFromObjectNumber()));
						return JSON;
					}
				}
			}

			if (lstDebitId != null && lstDebitAmt != null && lstDebitId.size() > 0 && lstDebitAmt.size() > 0) {
				for (int i = 0, sz = lstDebitId.size(); i < sz; i++) {
					debitdetail = deBitMgr.getDebitDetailById(lstDebitId.get(i));
					custId = debitdetail.getDebit().getObjectId();

					if (custDebit == null && custId != null) {
						cust = customerMgr.getCustomerById(custId);
						if (cust == null || !shopId.equals(cust.getShop().getId())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
							return ERROR;
						}
						custDebit = new DebitCustomerParams();
						custDebit.setShopId(shop.getId());
						custDebit.setCustomerId(custId);
						custDebit.setDebtPaymentType(DebtPaymentType.REMOVE_SMALL_DEBT);
						custDebit.setPayReceivedType(PayReceivedType.CHIET_KHAU);
						lstDebitIds = new ArrayList<Long>();
						lstDebitIds.add(debitdetail.getId());
						custDebit.setLstDebitId(lstDebitIds);
						lstDebitAmts = new ArrayList<BigDecimal>();
						lstDebitAmts.add(lstDebitAmt.get(i));
						custDebit.setLstDebitAmt(lstDebitAmts);
						custDebit.setCreateUser(staff.getStaffCode());
						lstCustIds = new ArrayList<Long>();
						lstCustIds.add(custId);
						custDebit.setLstCustId(lstCustIds);
						lstDebits.add(custDebit);
					} else if (custId != null) {
						idx = custDebit.getLstCustId().indexOf(custId);
						if (idx < 0) {
							cust = customerMgr.getCustomerById(custId);
							if (cust == null || !shopId.equals(cust.getShop().getId())) {
								result.put(ERROR, true);
								result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
								return ERROR;
							}
						}
						custDebit.getLstDebitId().add(debitdetail.getId());
						custDebit.getLstDebitAmt().add(lstDebitAmt.get(i));
						custDebit.getLstCustId().add(custId);
					}
				}
			}
			custDebit = null;
			lstCustIds = new ArrayList<Long>();
			if (lstDebitId1 != null && lstDebitAmt1 != null && lstDebitId1.size() > 0 && lstDebitAmt1.size() > 0) {
				for (int i = 0, sz = lstDebitId1.size(); i < sz; i++) {
					debitdetail = deBitMgr.getDebitDetailById(lstDebitId1.get(i));
					custId = debitdetail.getDebit().getObjectId();

					if (custDebit == null && custId != null) {
						cust = customerMgr.getCustomerById(custId);
						if (cust == null || !shopId.equals(cust.getShop().getId())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
							return ERROR;
						}
						custDebit = new DebitCustomerParams();
						custDebit.setShopId(shop.getId());
						custDebit.setCustomerId(custId);
						custDebit.setDebtPaymentType(DebtPaymentType.REMOVE_SMALL_DEBT);
						custDebit.setPayReceivedType(PayReceivedType.CHIET_KHAU);
						lstDebitIds = new ArrayList<Long>();
						lstDebitIds.add(debitdetail.getId());
						custDebit.setLstDebitId(lstDebitIds);
						lstDebitAmts = new ArrayList<BigDecimal>();
						lstDebitAmts.add(lstDebitAmt1.get(i));
						custDebit.setLstDebitAmt(lstDebitAmts);
						custDebit.setCreateUser(staff.getStaffCode());
						lstCustIds = new ArrayList<Long>();
						lstCustIds.add(custId);
						custDebit.setLstCustId(lstCustIds);
						lstDebits.add(custDebit);
					} else if (custId != null) {
						idx = custDebit.getLstCustId().indexOf(custId);
						if (idx < 0) {
							cust = customerMgr.getCustomerById(custId);
							if (cust == null || !shopId.equals(cust.getShop().getId())) {
								result.put(ERROR, true);
								result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
								return ERROR;
							}
						}
						custDebit.getLstDebitId().add(debitdetail.getId());
						custDebit.getLstDebitAmt().add(lstDebitAmt1.get(i));
						custDebit.getLstCustId().add(custId);
					}
				}
			}
			deBitMgr.updateListDebitOfCustomer(lstDebits, PaymentStatus.PAID);
			error = false;
		} catch (Exception e) {
			if (null != e.getMessage() && "DEBIT_DETAIL_NOT_REMAIN".equals(e.getMessage())) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.del.debit.not.avaialble");
			}
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitDellAction.removedebit"), createLogErrorStandard(actionStartTime));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @return String
	 * @since Dec 4, 2015
	 */
	public String changeShop() {
		result.put("lstSale", new ArrayList<StaffVO>());
		result.put("lstCashier", new ArrayList<StaffVO>());
		result.put("lstDeliver", new ArrayList<StaffVO>());
		result.put("listBank", new ArrayList<StaffVO>());
		result.put("fromDate", dayLock);
		result.put("toDate", dayLock);
		result.put("payreceiptCode", "");
		try {
			if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return ERROR;
			}
			if (!validateShop()) {
				return JSON;
			}

			StaffFilter filter = new StaffFilter();
			filter.setShopId(shop.getId());
			filter.setStatus(ActiveType.RUNNING);
			filter.setSpecType(StaffSpecificType.STAFF);

			//Lay NVBH
			filter.setStrListUserId(super.getStrListUserId());
			ObjectVO<StaffVO> objVO = staffMgr.getListStaffVO(filter);
			lstNVBHVo = objVO.getLstObject();

			String strLstShopId = super.getStrListShopId();
			//Lay nhan vien thu tien
			filter.setStrListUserId(null);
			filter.setStrListShopId(strLstShopId);
			filter.setSpecType(StaffSpecificType.NVTT);
			ObjectVO<StaffVO> objVOCash = staffMgr.getListStaffVO(filter);
			lstNVTTVo = objVOCash.getLstObject();

			//Lay nhan vien NVGH
			filter.setStrListUserId(null);
			filter.setStrListShopId(strLstShopId);
			filter.setSpecType(StaffSpecificType.NVGH);
			ObjectVO<StaffVO> objVODeliver = staffMgr.getListStaffVO(filter);
			lstNVGHVo = objVODeliver.getLstObject();

			if (lstNVBHVo != null) {
				result.put("lstSale", lstNVBHVo);
			}
			if (lstNVTTVo != null) {
				result.put("lstCashier", lstNVTTVo);
			}
			if (lstNVGHVo != null) {
				result.put("lstDeliver", lstNVGHVo);
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitBatchAction.changeShop"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	private boolean validateShop() throws BusinessException {
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			shop = shopMgr.getShopByCode(shopCode);
		} else {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
			return false;
		}
		if (shop == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
			return false;
		}
		shopId = shop.getId();
		if (super.getMapShopChild().get(shopId) == null) {
			result.put(ERROR, true);
			result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
			return false;
		}
		return true;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public BigDecimal getFromMoney() {
		return fromMoney;
	}

	public void setFromMoney(BigDecimal fromMoney) {
		this.fromMoney = fromMoney;
	}

	public BigDecimal getToMoney() {
		return toMoney;
	}

	public void setToMoney(BigDecimal toMoney) {
		this.toMoney = toMoney;
	}

	public List<Long> getLstDebitId() {
		return lstDebitId;
	}

	public void setLstDebitId(List<Long> lstDebitId) {
		this.lstDebitId = lstDebitId;
	}

	public List<BigDecimal> getLstDebitAmt() {
		return lstDebitAmt;
	}

	public void setLstDebitAmt(List<BigDecimal> lstDebitAmt) {
		this.lstDebitAmt = lstDebitAmt;
	}

	public Long getDebitId() {
		return debitId;
	}

	public void setDebitId(Long debitId) {
		this.debitId = debitId;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public List<CustomerDebitVO> getListCustomerDebit() {
		return listCustomerDebit;
	}

	public void setListCustomerDebit(List<CustomerDebitVO> listCustomerDebit) {
		this.listCustomerDebit = listCustomerDebit;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public DebitMgr getDeBitMgr() {
		return deBitMgr;
	}

	public void setDeBitMgr(DebitMgr deBitMgr) {
		this.deBitMgr = deBitMgr;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getStaffDeliverCode() {
		return staffDeliverCode;
	}

	public void setStaffDeliverCode(String staffDeliverCode) {
		this.staffDeliverCode = staffDeliverCode;
	}

	public String getStaffPaymentCode() {
		return staffPaymentCode;
	}

	public void setStaffPaymentCode(String staffPaymentCode) {
		this.staffPaymentCode = staffPaymentCode;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public CustomerMgr getCustomerMgr() {
		return customerMgr;
	}

	public void setCustomerMgr(CustomerMgr customerMgr) {
		this.customerMgr = customerMgr;
	}

	public List<Long> getLstDebitId1() {
		return lstDebitId1;
	}

	public void setLstDebitId1(List<Long> lstDebitId1) {
		this.lstDebitId1 = lstDebitId1;
	}

	public List<BigDecimal> getLstDebitAmt1() {
		return lstDebitAmt1;
	}

	public void setLstDebitAmt1(List<BigDecimal> lstDebitAmt1) {
		this.lstDebitAmt1 = lstDebitAmt1;
	}
}

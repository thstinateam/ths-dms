package ths.dms.web.action.customerdebit;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.DebitCustomerParams;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class CustomerDebitReduceAction extends AbstractAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6433873780895305521L;

	private StaffMgr staffMgr;
	private CustomerMgr customerMgr;
	private DebitMgr debitMgr;

	private Staff staff;
	private Shop shop;
	private String staffCode;
	private String shortCode;
	private Integer type;
	private Integer payReceivedType;

	private String payreceiptCode;
	private BigDecimal payreceiptValue;
	private BigDecimal debitPreRemain;
	private BigDecimal debitPostRemain;
	List<CustomerDebitVO> lstOrderDebit;
	List<Long> lstCustomerDebitID;
	private List<BigDecimal> lstPayAmount;
	private List<BigDecimal> lstDiscount;
	private List<Staff> listNVGH;
	private List<Staff> listNVTT;
	private List<Staff> listNVBH;
	private String fromDate;
	private String toDate;
	private String staffDeliverCode;
	private String staffPaymentCode;

	public String getStaffDeliverCode() {
		return staffDeliverCode;
	}

	public void setStaffDeliverCode(String staffDeliverCode) {
		this.staffDeliverCode = staffDeliverCode;
	}

	public String getStaffPaymentCode() {
		return staffPaymentCode;
	}

	public void setStaffPaymentCode(String staffPaymentCode) {
		this.staffPaymentCode = staffPaymentCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public List<Staff> getListNVGH() {
		return listNVGH;
	}

	public void setListNVGH(List<Staff> listNVGH) {
		this.listNVGH = listNVGH;
	}

	public List<Staff> getListNVTT() {
		return listNVTT;
	}

	public void setListNVTT(List<Staff> listNVTT) {
		this.listNVTT = listNVTT;
	}

	public String getPayreceiptCode() {
		return payreceiptCode;
	}

	public void setPayreceiptCode(String payreceiptCode) {
		this.payreceiptCode = payreceiptCode;
	}

	public BigDecimal getPayreceiptValue() {
		return payreceiptValue;
	}

	public void setPayreceiptValue(BigDecimal payreceiptValue) {
		this.payreceiptValue = payreceiptValue;
	}

	public BigDecimal getDebitPreRemain() {
		return debitPreRemain;
	}

	public void setDebitPreRemain(BigDecimal debitPreRemain) {
		this.debitPreRemain = debitPreRemain;
	}

	public BigDecimal getDebitPostRemain() {
		return debitPostRemain;
	}

	public void setDebitPostRemain(BigDecimal debitPostRemain) {
		this.debitPostRemain = debitPostRemain;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public CustomerMgr getCustomerMgr() {
		return customerMgr;
	}

	public void setCustomerMgr(CustomerMgr customerMgr) {
		this.customerMgr = customerMgr;
	}

	public DebitMgr getDebitMgr() {
		return debitMgr;
	}

	public void setDebitMgr(DebitMgr debitMgr) {
		this.debitMgr = debitMgr;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public List<CustomerDebitVO> getLstOrderDebit() {
		return lstOrderDebit;
	}

	public void setLstOrderDebit(List<CustomerDebitVO> lstOrderDebit) {
		this.lstOrderDebit = lstOrderDebit;
	}

	public List<Long> getLstCustomerDebitID() {
		return lstCustomerDebitID;
	}

	public void setLstCustomerDebitID(List<Long> lstCustomerDebitID) {
		this.lstCustomerDebitID = lstCustomerDebitID;
	}

	public boolean checkExistShopOfUserLogin() {
		staff = getStaffByCurrentUser();
		if (staff == null) {
			return false;
		}
		shop = staff.getShop();
		if (shop == null)
			return false;
		return true;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		debitMgr = (DebitMgr) context.getBean("debitMgr");

	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			if (checkExistShopOfUserLogin()) {
				Date lockDate = shopLockMgr.getNextLockedDay(shop.getId());
				if (lockDate == null) {
					lockDate = commonMgr.getSysDate();
				}
				fromDate = DateUtil.toDateString(lockDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				toDate = fromDate;

				payreceiptCode = debitMgr.getPayReceivedStringSugguest(dayLock);

				StaffFilter staffFilter = new StaffFilter();
				staffFilter.setStatus(ActiveType.RUNNING);
				staffFilter.setShopCode(shop.getShopCode());
				staffFilter.setIsGetShopOnly(true);
				staffFilter.setStaffType(StaffObjectType.NVTT);
				ObjectVO<Staff> staffVO = staffMgr.getListStaff(staffFilter);
				if (staffVO != null) {
					listNVTT = staffVO.getLstObject();
				}
				staffFilter.setStaffType(StaffObjectType.NVGH);
				staffVO = staffMgr.getListStaff(staffFilter);
				if (staffVO != null) {
					listNVGH = staffVO.getLstObject();
				}

				staffVO = staffMgr.getListPreAndVanStaff(null, null, null, shop.getId(), ActiveType.RUNNING, Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
				if (staffVO != null) {
					listNVBH = staffVO.getLstObject();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Gets the info. Ham getInfo() su dung de tim kiem danh sach don hang con
	 * no cua khach hang
	 * 
	 * @param :shortCode (Ma KH)& staffCode(Ma NVBH)
	 * @since Sep 19, 2012
	 * @author thongnm1
	 */
	public String getInfo() {
		try {
			lstOrderDebit = new ArrayList<CustomerDebitVO>();
			result.put("rows", lstOrderDebit);
			//			if(!checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR) || !checkExistShopOfUserLogin()){
			//				return JSON;
			//			}
			//boolean isPermission = checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR);
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop /* && !StringUtil.isNullOrEmpty(shortCode) */) { // khong bat buoc KH
				Customer customerTemp = null;
				//Long custId = null;
				if (!StringUtil.isNullOrEmpty(shortCode)) {
					customerTemp = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), shortCode));
					if (customerTemp == null) {
						result.put(ERROR, true);
						result.put("rows", lstOrderDebit);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
						return JSON;
					}
				}

				Date fromDateTemp = null;
				Date toDateTemp = null;
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					fromDateTemp = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					toDateTemp = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
				}

				Staff staffDeliver = new Staff();
				if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
					staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, staff.getShop().getId(), ActiveType.RUNNING);
					if (staffDeliver == null) {
						result.put(ERROR, true);
						result.put("rows", lstOrderDebit);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.delivery"));
						return JSON;
					}
				}

				Staff staffPayment = new Staff();
				if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
					staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, staff.getShop().getId(), ActiveType.RUNNING);
					if (staffPayment == null) {
						result.put(ERROR, true);
						result.put("rows", lstOrderDebit);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.cashier"));
						return JSON;
					}
				}

				Staff nvbh = new Staff();
				if (!StringUtil.isNullOrEmpty(staffCode)) {
					nvbh = staffMgr.getStaffByInfoBasic(staffCode, staff.getShop().getId(), ActiveType.RUNNING);
					if (nvbh == null) {
						result.put(ERROR, true);
						result.put("rows", lstOrderDebit);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "ss.ds3.sheet1.header.nvbh"));
						return JSON;
					}
				}
				if (customerTemp != null && (!ActiveType.RUNNING.equals(customerTemp.getStatus()) || !shop.getId().equals(customerTemp.getShop().getId()))) {
					result.put(ERROR, true);
					result.put("rows", lstOrderDebit);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
					return JSON;
				}
				if (customerTemp == null || staff.getShop().getId().equals(customerTemp.getShop().getId())) {
					if (type == 0) {
						lstOrderDebit = debitMgr.getListCustomerDebitVO(shop.getId(), shortCode, null, null, null, null, false, fromDateTemp, toDateTemp, staffDeliver.getId(), staffPayment.getId(), nvbh.getId());
					} else if (type == 1) {
						lstOrderDebit = debitMgr.getListCustomerDebitVO(shop.getId(), shortCode, null, null, null, null, true, fromDateTemp, toDateTemp, staffDeliver.getId(), staffPayment.getId(), nvbh.getId());
					}
					debitPreRemain = BigDecimal.ZERO;//debitMgr.getTotalDebit(custId, DebitOwnerType.CUSTOMER);
				}
				payreceiptCode = debitMgr.getPayReceivedStringSugguest(dayLock);
			}

			result.put("rows", lstOrderDebit);
			result.put("debitPreRemain", debitPreRemain);
			result.put("payreceiptCode", payreceiptCode);
		} catch (Exception ex) {
			LogUtility.logError(ex, "Error CustomerDebitBatchAction.getInfo: " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Import customer debit. Hàm importCustomerDebit() su dung de thanh toan
	 * cong no
	 * 
	 * @param:
	 * @since Sep 19, 2012
	 * @author thongnm1
	 */
	public String importCustomerDebit() {
		resetToken(result);
		try {
			// CHECK QUYEN
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}
			/** modify by lacnv1 - Thanh toan theo don hang cua nhieu khach hang */
			List<DebitCustomerParams> lstDebits = new ArrayList<DebitCustomerParams>();
			List<Long> lstCustIds = new ArrayList<Long>();
			if (payreceiptValue.compareTo(BigDecimal.ZERO) != 1) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_POSSITIVE_INTEGER, false, "customerdebit.batch.payreceiptvalue"));
				return ERROR;
			}
			Long custId = null;
			Customer cust = null;
			DebitCustomerParams custDebit = null;
			List<Long> lstDebitIds = null;
			List<BigDecimal> lstDebitAmts = null;
			List<BigDecimal> lstDiscountAmts = null;
			BigDecimal debitAmt = null;
			boolean isBreak = false;
			int idx = -1;

			if (type == 1) {
				BigDecimal sum = new BigDecimal(0);
				BigDecimal payreceiptValueTmp = payreceiptValue;
				DebitDetail debitdetail = null;
				BigDecimal remain = null;
				BigDecimal discount = null;
				BigDecimal amount = null;
				String payRecvNo = payreceiptCode;

				for (int i = 0, sz = lstCustomerDebitID.size(); i < sz; ++i) {
					if (BigDecimal.ZERO.equals(lstPayAmount.get(i))) {
						continue;
					}
					debitdetail = debitMgr.getDebitDetailById(lstCustomerDebitID.get(i));
					remain = lstPayAmount.get(i);
					custId = debitdetail.getDebit().getObjectId();
					discount = BigDecimal.ZERO;
					amount = remain.add(discount);

					if (amount.compareTo(BigDecimal.ZERO) < 1) {
						result.put(ERROR, true);
						result.put("errorReloadPage", true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.receipt.not.negative"));
						return ERROR;
					}

					if (payreceiptValueTmp.compareTo(remain) == 1) {
						sum = sum.add(remain);
						debitAmt = remain;
						payreceiptValueTmp = payreceiptValueTmp.subtract(remain);
					} else {
						if (payreceiptValueTmp.compareTo(BigDecimal.ZERO) == 1) {
							sum = sum.add(payreceiptValueTmp);
							debitAmt = payreceiptValueTmp;
						}
						isBreak = true;
					}

					if (custId != null && !lstCustIds.contains(custId)) {
						lstCustIds.add(custId);
						cust = customerMgr.getCustomerById(custId);
						if (cust == null || !staff.getShop().getId().equals(cust.getShop().getId())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
							return ERROR;
						}
						custDebit = new DebitCustomerParams();
						custDebit.setShopId(shop.getId());
						custDebit.setCustomerId(custId);
						custDebit.setPayReceivedNumber(payRecvNo);
						payRecvNo = payreceiptCode + lstCustIds.size();
						custDebit.setDebtPaymentType(DebtPaymentType.MANUAL);
						custDebit.setPayReceivedType(PayReceivedType.CHIET_KHAU);
						custDebit.setAmount(debitAmt);
						lstDebitIds = new ArrayList<Long>();
						lstDebitIds.add(debitdetail.getId());
						custDebit.setLstDebitId(lstDebitIds);
						lstDebitAmts = new ArrayList<BigDecimal>();
						lstDebitAmts.add(debitAmt);
						custDebit.setLstDebitAmt(lstDebitAmts);
						lstDiscountAmts = new ArrayList<BigDecimal>();
						lstDiscountAmts.add(discount);
						custDebit.setLstDiscount(lstDiscountAmts);
						custDebit.setCreateUser(staff.getStaffCode());
						lstDebits.add(custDebit);
					} else if (custId != null) {
						idx = lstCustIds.indexOf(custId);
						if (idx > -1 && idx < lstDebits.size()) {
							custDebit = lstDebits.get(idx);
							custDebit.setAmount(custDebit.getAmount().add(debitAmt));
							custDebit.getLstDebitId().add(debitdetail.getId());
							custDebit.getLstDebitAmt().add(debitAmt);
							custDebit.getLstDiscount().add(discount);
						}
					}

					if (isBreak) {
						break;
					}
				}
				//KIEM TRA GIA TRI PHIEU THU PHAI NHO HON HOAC BANG TONG SO TIEN TRA  
				if (payreceiptValue.compareTo(sum) == 1) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.receipt.smaller.or.equal"));
					return ERROR;
				}	
				debitMgr.updateListDebitOfCustomer(lstDebits, PaymentStatus.PAID);
			} else if( type == 0) {				
				BigDecimal sum = new BigDecimal(0);
				BigDecimal payreceiptValueTmp = payreceiptValue.negate();
				DebitDetail debitdetail = null;
				BigDecimal remain = null;
				BigDecimal discount = null;
				BigDecimal amount = null;

				String payRecvNo = payreceiptCode;
				for (int i = 0, sz = lstCustomerDebitID.size(); i < sz; ++i) {
					if (BigDecimal.ZERO.equals(lstPayAmount.get(i))) {
						continue;
					}
					debitdetail = debitMgr.getDebitDetailById(lstCustomerDebitID.get(i));
					remain = lstPayAmount.get(i);
					custId = debitdetail.getDebit().getObjectId();
					discount = BigDecimal.ZERO;
					amount = remain.add(discount);

					if (amount.compareTo(BigDecimal.ZERO) > -1) {
						result.put(ERROR, true);
						result.put("errorReloadPage", true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.pay.not.negative"));
						return ERROR;
					}

					if (payreceiptValueTmp.compareTo(remain) == -1) {
						sum = sum.add(remain);
						debitAmt = remain;
						payreceiptValueTmp = payreceiptValueTmp.subtract(remain);
					} else {
						if (payreceiptValueTmp.compareTo(BigDecimal.ZERO) == -1) {
							sum = sum.add(payreceiptValueTmp);
							debitAmt = payreceiptValueTmp;
						}
						isBreak = true;
					}

					if (custId != null && !lstCustIds.contains(custId)) {
						lstCustIds.add(custId);
						cust = customerMgr.getCustomerById(custId);
						if (cust == null || !staff.getShop().getId().equals(cust.getShop().getId())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "customerdebit.batch.import.permission.error"));
							return ERROR;
						}
						custDebit = new DebitCustomerParams();
						custDebit.setShopId(shop.getId());
						custDebit.setCustomerId(custId);
						custDebit.setPayReceivedNumber(payRecvNo);
						payRecvNo = payreceiptCode + lstCustIds.size();
						custDebit.setDebtPaymentType(DebtPaymentType.MANUAL);
						custDebit.setPayReceivedType(PayReceivedType.CHIET_KHAU);
						custDebit.setAmount(debitAmt);
						lstDebitIds = new ArrayList<Long>();
						lstDebitIds.add(debitdetail.getId());
						custDebit.setLstDebitId(lstDebitIds);
						lstDebitAmts = new ArrayList<BigDecimal>();
						lstDebitAmts.add(debitAmt);
						custDebit.setLstDebitAmt(lstDebitAmts);
						lstDiscountAmts = new ArrayList<BigDecimal>();
						lstDiscountAmts.add(discount);
						custDebit.setLstDiscount(lstDiscountAmts);
						custDebit.setCreateUser(staff.getStaffCode());
						lstDebits.add(custDebit);
					} else if (custId != null) {
						idx = lstCustIds.indexOf(custId);
						if (idx > -1 && idx < lstDebits.size()) {
							custDebit = lstDebits.get(idx);
							custDebit.setAmount(custDebit.getAmount().add(debitAmt));
							custDebit.getLstDebitId().add(debitdetail.getId());
							custDebit.getLstDebitAmt().add(debitAmt);
							custDebit.getLstDiscount().add(discount);
						}
					}

					if (isBreak) {
						break;
					}
				}
				//KIEM TRA GIA TRI PHIEU CHI PHAI NHO HON HOAC BANG TONG SO TIEN TRA  
				BigDecimal test2 = sum.abs();
				if (payreceiptValue.compareTo(test2) == 1) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.pay.smaller.or.equal"));
					return ERROR;
				}
				//debitMgr.updateDebitOfCustomer(shop.getId(), customerTemp.getId(), payreceiptCode, DebtPaymentType.MANUAL, payreceiptValue.negate(), lstCustomerDebitIDTmp, lstCustomerDebitValue, staff.getStaffCode());
				debitMgr.updateListDebitOfCustomer(lstDebits, PaymentStatus.PAID);
			} 
		} catch (Exception e) {
			result.put(ERROR, true);
			if (e.getMessage().equals(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN)) {
				result.put("errorReloadPage", true);
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			LogUtility.logError(e, "Error CustomerDebitBatchAction.updateOrderDebitCustomer: " + e.getMessage());
			return ERROR;
		}
		return SUCCESS;
	}

	private Shop getShopOfCurrentUser() {
		staff = getStaffByCurrentUser();
		if (staff != null && staff.getShop() != null) {
			// Ta lay shop cua user current ra dung: dung shopId va shopCode:
			shop = staff.getShop();
			if (shop != null && shop.getId() != null && shop.getShopCode() != null) {
				return shop;
			}
		}
		return null;
	}

	public String checkDuplicate(List<List<String>> lstData, int k) {//check trung khoa
		String mes = "";
		if (lstData.get(k) == null || lstData.get(k).size() < 4) {
			mes += "1";//Du lieu khong hop le
		} else {
			//String staffCode = lstData.get(k).get(0).trim();
			String customerCode = lstData.get(k).get(0).trim().toUpperCase();
			String type = lstData.get(k).get(1).trim();
			//String chungtu = lstData.get(k).get(3).trim();
			if (StringUtil.isNullOrEmpty(customerCode) || StringUtil.isNullOrEmpty(type)) {
				mes += "1";//Du lieu khong hop le
			} else {
				for (int i = 0; i < lstData.size(); i++) {
					if (i != k && lstData.get(i) != null && lstData.get(i).size() >= 4) {
						List<String> row = lstData.get(i);
						if (customerCode.equalsIgnoreCase(row.get(0).trim()) && type.equalsIgnoreCase(row.get(1).trim())) {
							if (isView == 0) {
								mes += (i + 2) + " , ";//stt dong import khac voi dong xem
							} else {
								mes += (i + 1) + " , ";
							}
						}
					}
				}
				if (mes.length() > 0) {
					mes = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.duplicate") + mes.substring(0, mes.length() - 3);
				}
			}
		}
		return mes;
	}

	public boolean checkDuplicateAll(List<List<String>> lstData) {
		try {
			for (int i = 0; i < lstData.size(); i++) {
				String mesTemp = checkDuplicate(lstData, i);
				if (!StringUtil.isNullOrEmpty(mesTemp) && !mesTemp.equals("1")) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Creates the sale order.
	 * 
	 * @return the string
	 * @author tungmt
	 */
	public String checkStaff(Staff staff, StaffObjectType staffObjectType) {
		/*
		 * if(staff==null){ return
		 * ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB
		 * ,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
		 * "catalog.staff.code")); } if(staff.getShop() == null ||
		 * !staff.getShop().getId().equals(shop.getId())) { return
		 * ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP,
		 * staff.getStaffCode(), shop.getShopCode()); } if(staff.getStaffType()
		 * == null || (staff.getStaffType() != null &&
		 * !staff.getStaffType().getObjectType
		 * ().equals(staffObjectType.getValue()))){ return
		 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SS_NOT_GENERAL,
		 * staff.getStaffCode(), staffObjectType.toString()); }
		 * if(!staff.getStatus
		 * ().getValue().equals(ActiveType.RUNNING.getValue())){ return
		 * ValidateUtil
		 * .getErrorMsg(ConstantManager.ERR_PAUSE_STATUS,Configuration
		 * .getResourceString(ConstantManager.VI_LANGUAGE,
		 * "catalog.staff.code")); }
		 */
		return "";
	}

	/* sontt19 */
	public String importExcel() {
		shop = getShopOfCurrentUser();
		if (shop == null) {
			isError = true;
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			return SUCCESS;
		} else {//do import excel
			isError = true;
			totalItem = 0;
			lstView = new ArrayList<CellBean>();
			typeView = true;
			String message = "";
			String msg;
			List<CellBean> lstFails = new ArrayList<CellBean>();
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 4);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				try {
					boolean isUpdate = checkDuplicateAll(lstData);// neu co dong trung thi khong cho update gi ca
					for (int i = 0; i < lstData.size(); i++) {
						if (lstData.get(i) != null && lstData.get(i).size() > 0) {
							message = "";
							totalItem++;
							List<String> row = lstData.get(i);
							//Khai bao nhung bien can thiet de import 1 record xuong:
							BigDecimal valuePay = null;
							boolean isRecevied;
							//String itemStaffCode = null;
							String itemCustomerShortCode = null;
							String itemPayType = null;
							String itemPayCode = null;
							String itemValuePay = null;
							List<CustomerDebitVO> listCustomerDebit = new ArrayList<CustomerDebitVO>();
							//kiem tra trung khoa
							String mesTemp = checkDuplicate(lstData, i);
							if (!StringUtil.isNullOrEmpty(mesTemp) && !mesTemp.equals("1")) {
								message += mesTemp;
							}
							//end-khai bao
							if (row != null && row.size() >= 4 && isUpdate) {//do something:
								////MaKH 
								itemCustomerShortCode = row.get(0);
								msg = ValidateUtil.validateField(itemCustomerShortCode, "catalog.customer.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
								//MaKH trong tai lieu BA ko can xet MaxLength.
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {//Validate cho MaKH: Ma KH phai ton tai va thuoc NPP
									Customer cell1Customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), itemCustomerShortCode));//Truyen xuong core da upperCase va xet status co hieu luc.
									if (cell1Customer == null) {//Ma KH khong ton tai trong he thong.
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.code");
									} else {//check thuoc NPP
										if (!(cell1Customer.getShop() != null && cell1Customer.getShop().getId() != null && cell1Customer.getShop().getId().equals(shop.getId()))) {//Ma KH khong thuoc NPP.
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_DEBIT_BATCH_NOT_IN_SHOP, true, "catalog.customer.code");
										} else if (!ActiveType.RUNNING.equals(cell1Customer.getStatus())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.customer.not.running") + "\n";
										}
									}
								}

								////LoaiCT
								itemPayType = row.get(1);
								//Validate cho LoaiCT
								msg = ValidateUtil.validateField(itemPayType, "customerdebit.batch.payreceipttype", 1, ConstantManager.ERR_REQUIRE);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									if (!("0".equalsIgnoreCase(itemPayType) || "1".equalsIgnoreCase(itemPayType))) {//Loai CT khong hop le.
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID, true, "customerdebit.batch.payreceipttype");
									}
								}

								////SoCT - Validate cho SoCT
								itemPayCode = row.get(2);
								msg = ValidateUtil.validateField(itemPayCode, "customerdebit.batch.payreceiptcode", 40, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								//So CT luc thi maxlength la 20, luc thi maxlength la 40.
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}

								////SoTien
								itemValuePay = row.get(3);
								itemValuePay = itemValuePay.replace(",", "");
								//Validate cho SoTien: so tien >0, dai khong qua 17 ki tu:
								msg = ValidateUtil.validateField(itemValuePay, "customerdebit.batch.payreceiptvalue", 17, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									Long tien = null;
									try {
										//Long.parseLong(itemValuePay);
										tien = Long.parseLong(itemValuePay);
									} catch (NumberFormatException e) {//So tien khong dung dinh dang.
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID, true, "customerdebit.batch.payreceiptvalue");
									}
									if (tien != null) {
										if (tien <= 0) {//So tien phai lon hon 0.
											message += ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_BATCH_MONEY_NOT_GREATER_ZERO) + "\n";
										} else {
											valuePay = BigDecimal.valueOf(tien);//Long.parseLong(itemValuePay));
											if ("0".equalsIgnoreCase(itemPayType)) {//phieu thu
												isRecevied = true;
											} else {//phieu chi
												valuePay = valuePay.multiply(new BigDecimal((-1)));
												isRecevied = false;
											}
											listCustomerDebit = debitMgr.getListCustomerDebitVO(shop.getId(), itemCustomerShortCode, null, valuePay, null, null, isRecevied, null, null, null, null, null); // true: phieu thu , false: phieu chi
											if (listCustomerDebit != null && listCustomerDebit.size() > 0) {
												BigDecimal total = new BigDecimal(0);
												for (CustomerDebitVO cd : listCustomerDebit) {
													total = total.add(cd.getPayAmt());
												}
												if (Math.abs(valuePay.longValue()) > Math.abs(total.longValue())) {
													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.money.greater.debit") + Math.abs(total.longValue());
												}
											} else if (StringUtil.isNullOrEmpty(message)) {
												message += ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_BATCH_NOT_EXIST_DEBIT, itemCustomerShortCode);
											}
										}
									}
								}
							}

							////////////////////////still in loop for, out of if
							if (isView == 0) {
								if (StringUtil.isNullOrEmpty(message) && isUpdate) {
									//////////////////Thao tac xuong DATABASE:///////////////////////
									if ("0".equalsIgnoreCase(itemPayType)) {//phieu thu
										isRecevied = true;
									} else {//phieu chi
										isRecevied = false;
									}
									listCustomerDebit = debitMgr.getListCustomerDebitVO(shop.getId(), itemCustomerShortCode, null, valuePay, null, null, isRecevied, null, null, null, null, null); // true: phieu thu , false: phieu chi
									if (listCustomerDebit != null && listCustomerDebit.size() > 0) {
										List<Long> lstDebitId = new ArrayList<Long>();
										List<BigDecimal> lstDebitAmt = new ArrayList<BigDecimal>();
										Long customerId = new Long(0);
										for (CustomerDebitVO cd : listCustomerDebit) {
											lstDebitId.add(cd.getDebitId());
											lstDebitAmt.add(cd.getPayAmt());
										}
										customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shop.getShopCode(), itemCustomerShortCode));
										if (customer != null) {
											customerId = customer.getId();//Now, all data is ready for method updateDebitOfCustomer:
											debitMgr.updateDebitOfCustomer(shop.getId(), customerId, itemPayCode, DebtPaymentType.AUTO, 
													valuePay, lstDebitId, lstDebitAmt, null, staff.getStaffCode(), PayReceivedType.THANH_TOAN, null, PaymentStatus.PAID);
										}
									}
									///////////////End-Thao tac DATABASE
								} else {
									lstFails.add(StringUtil.addFailBean(row, message));
								}
								typeView = false;
							} else {
								message = StringUtil.convertHTMLBreakLine(message);
								lstView.add(StringUtil.addFailBean(row, message));
								typeView = true;
							}
						}
					}
					//Out of loop for: Export error
					getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CUSTOMER_DEBIT_BATCH_FAIL);
				} catch (Exception e) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
					LogUtility.logError(e, e.getMessage());
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					isError = false;
					return SUCCESS;
				}
			}
		}
		//
		return SUCCESS;
	}

	private File excelFile;
	private String excelFileContentType;

	//private String staff2Code;
	private Customer customer;
	private List<CellBean> lstView;
	private Boolean typeView;
	private Integer isView;

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<CellBean> getLstView() {
		return lstView;
	}

	public void setLstView(List<CellBean> lstView) {
		this.lstView = lstView;
	}

	public Boolean getTypeView() {
		return typeView;
	}

	public void setTypeView(Boolean typeView) {
		this.typeView = typeView;
	}

	public Integer getIsView() {
		return isView;
	}

	public void setIsView(Integer isView) {
		this.isView = isView;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public List<Staff> getListNVBH() {
		return listNVBH;
	}

	public void setListNVBH(List<Staff> listNVBH) {
		this.listNVBH = listNVBH;
	}

	public List<BigDecimal> getLstPayAmount() {
		return lstPayAmount;
	}

	public void setLstPayAmount(List<BigDecimal> lstPayAmount) {
		this.lstPayAmount = lstPayAmount;
	}

	public Integer getPayReceivedType() {
		return payReceivedType;
	}

	public void setPayReceivedType(Integer payReceivedType) {
		this.payReceivedType = payReceivedType;
	}

	public List<BigDecimal> getLstDiscount() {
		return lstDiscount;
	}

	public void setLstDiscount(List<BigDecimal> lstDiscount) {
		this.lstDiscount = lstDiscount;
	}
}
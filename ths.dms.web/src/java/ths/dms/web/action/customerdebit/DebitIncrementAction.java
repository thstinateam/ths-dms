package ths.dms.web.action.customerdebit;

import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Action: lap phieu dieu chinh tang no
 * 
 * @author lacnv1
 * @since Apr 2, 2014
 */
public class DebitIncrementAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private CustomerMgr customerMgr;
	private DebitMgr debitMgr;
	
	private String dbNumber;
	private String shortCode;
	private Integer type;
	private Integer debitType;
	private BigDecimal amount;
	private String reason;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		customerMgr = (CustomerMgr)context.getBean("customerMgr");
		debitMgr = (DebitMgr)context.getBean("debitMgr");
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		return SUCCESS;
	}
	
	/**
	 * Luu phieu dieu chinh tang no
	 * 
	 * @author lacnv1
	 * @since Apr 2, 2014
	 */
	public String saveDebit() throws Exception {
		try {
			resetToken(result);
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
			Shop sh = staff.getShop();
			
			if (type == null) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,
						false, "common.label.type"));
				return JSON;
			}
			if (type == 1 && StringUtil.isNullOrEmpty(shortCode)) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,
						false, "baocao.dtbh.tddstkh.nh.mh.khachhang"));
				return JSON;
			}
			if (amount == null || BigDecimal.ZERO.equals(amount)) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.money.not.greater.zero"));
				return JSON;
			}
			Customer cust = null;
			if (!StringUtil.isNullOrEmpty(shortCode)) {
				List<Customer> lstCust = customerMgr.getListCustomerByShortCode(sh.getId(), shortCode);
				//cust = customerMgr.getCustomerByCode(StringUtil.getFullCode(sh.getShopCode(), shortCode));
				if (lstCust == null || lstCust.size() == 0) {
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,
							false, "baocao.dtbh.tddstkh.nh.mh.khachhang"));
					return JSON;
				}
				if (lstCust.size() == 1) {
					cust = lstCust.get(0);
				} else {
					int c = 0;
					for (int i = 0, sz = lstCust.size(); i < sz; i++) {
						if (ActiveType.RUNNING.equals(lstCust.get(i).getStatus())) {
							cust = lstCust.get(i);
							c++;
							if (c > 1) {
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.error.data"));
								return JSON;
							}
						}
					}
				}
				if (cust == null || !ActiveType.RUNNING.equals(cust.getStatus())) {
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,
							false, "baocao.dtbh.tddstkh.nh.mh.khachhang"));
					return JSON;
				}
			}
			//Long custId = (cust == null || type == 2) ? null : cust.getId();
			
			// kiem tra ma so phieu da co chua
			
			if (debitType != null && debitType == 2) {
				amount = amount.negate();
			}
			// luu thong tin
			debitMgr.createDebitIncrement(sh.getId(), dbNumber, amount, cust, reason, getLogInfoVO());
			
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/** getters and setters */
	public String getDbNumber() {
		return dbNumber;
	}

	public void setDbNumber(String dbNumber) {
		this.dbNumber = dbNumber;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getDebitType() {
		return debitType;
	}

	public void setDebitType(Integer debitType) {
		this.debitType = debitType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
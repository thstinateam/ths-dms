package ths.dms.web.action.customerdebit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * The Class CustomerDebitAutoAction.
 */
public class CustomerDebitAutoAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5094236695100587342L;

	/** The shop code. */
	private String shopCode;

	/** The staff code. */
	private String staffCode;

	private List<StaffVO> lstNVGHVo;
	private List<StaffVO> lstNVTTVo;

	/** The staff. */
	private Staff staff;

	/** The staff mgr. */
	private StaffMgr staffMgr;

	/** The de bit mgr. */
	private DebitMgr deBitMgr;

	/** The shop. */
	private Shop shop;

	/** The shop id. */
	private Long shopId;

	/** The value pay. */
	private BigDecimal valuePay;

	/** The before pay. */
	private BigDecimal beforePay;

	/** The after pay. */
	private BigDecimal afterPay;

	/** The short code. */
	private String shortCode;

	/** The staff2 code. */
	//	private String staff2Code;

	/** The err msg. */
	private String errMsg;

	/** The status. */
	private int status;

	/** The warning. */
	private int warning;

	/** The tt. */
	private BigDecimal tt;

	/** The ttt. */
	private BigDecimal ttt;

	/** The ta. */
	private BigDecimal ta;

	/** The total is received. */
	private BigDecimal totalIsReceived;

	/** The lst debit id. */
	private List<Long> lstDebitId;

	/** The lst debit amt. */
	private List<BigDecimal> lstDebitAmt;

	/** The list customer debit. */
	private List<CustomerDebitVO> listCustomerDebit;
	// 
	/** The lst id. */
	private List<Long> lstId;

	/** The pay received number. */
	private String payReceivedNumber;

	/** The customer. */
	private Customer customer;

	private String fromDate;
	private String toDate;
	private String staffDeliverCode;
	private String staffPaymentCode;

	/** The customer mgr. */
	private CustomerMgr customerMgr;

	/**
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		try {
			super.prepare();
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitAutoAction.prepare"), createLogErrorStandard(actionStartTime));
		}
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		deBitMgr = (DebitMgr) context.getBean("debitMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
	}

	/**
	 * @author tientv
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		try {
			if (isShopLocked()) {
				return SHOP_LOCK;
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitAutoAction.execute"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * Gets the info.
	 * 
	 * @return the info
	 * @author tientv
	 * @see update customer code
	 */
	public String getInfo() throws Exception {
		try {
			listCustomerDebit = new ArrayList<CustomerDebitVO>();
			result.put("rows", listCustomerDebit);
			result.put("totalIsReceived", 0);
			if (currentUser != null && currentUser.getUserName() != null) {
				if (!validateShop()) {
					return JSON;
				}
				customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, shortCode));
				if (customer == null) {
					/** KH khong ton tai */
					result.put("warning", 4);
				} else {
					boolean isRecevied = true;
					/** Phieu thu */
					if (status == 2) {
						isRecevied = false;
					}
					Date fDate = null;
					Date tDate = null;
					if (!StringUtil.isNullOrEmpty(fromDate)) {
						fDate = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
					}
					if (!StringUtil.isNullOrEmpty(toDate)) {
						tDate = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
					}
					Staff staffDeliver = new Staff();
					if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
						staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, shopId, ActiveType.RUNNING);
					}
					Staff staffPayment = new Staff();
					if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
						staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, shopId, ActiveType.RUNNING);
					}
					totalIsReceived = deBitMgr.getCustomerTotalDebitDetail(shopId, null, null, shortCode, null, null, null, isRecevied, staffDeliver.getId(), staffPayment.getId(), fDate, tDate);
					BigDecimal val = valuePay;
					if (status == 2) {
						totalIsReceived = totalIsReceived.negate();
						val = val.negate();
					}
					result.put("totalIsReceived", totalIsReceived);
					listCustomerDebit = deBitMgr.getListCustomerDebitVO(shopId, shortCode, null, val, null, null, isRecevied, fDate, tDate, staffDeliver.getId(), staffPayment.getId(), null); // true: phieu thu , false: phieu chi
					if (valuePay.compareTo(totalIsReceived) > 0) {
						if (totalIsReceived.equals(BigDecimal.ZERO)) {
							result.put("warning", 3);
							return JSON;
						}
						result.put("warning", 1);
						return JSON;
					}
					if (listCustomerDebit.size() > 0) {
						result.put("warning", 2);
						result.put("ttt", totalIsReceived.subtract(valuePay));
					}
				}
			}
			result.put("rows", listCustomerDebit);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitAutoAction.getInfo"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * 
	 * lay cong no thanh toan
	 * @author trietptm
	 * @return String
	 * @throws Exception
	 * @since Dec 25, 2015
	 */
	public String getCurrentDebit() throws Exception {
		try {
			if (!validateShop()) {
				return JSON;
			}
			Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, shortCode));
			if (customer == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "customerdebit.batch.customercode"));
				return JSON;
			}
			boolean isRecevied = true;
			/** Phieu thu */
			if (status == 2) {
				isRecevied = false;
			}
			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			}
			Staff staffDeliver = new Staff();
			if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
				staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, shopId, ActiveType.RUNNING);
			}
			Staff staffPayment = new Staff();
			if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
				staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, shopId, ActiveType.RUNNING);
			}
			totalIsReceived = deBitMgr.getCustomerTotalDebitDetail(shopId, null, null, shortCode, null, null, null, isRecevied, staffDeliver.getId(), staffPayment.getId(), fDate, tDate);
			if (status == 2) {
				totalIsReceived = totalIsReceived.negate();
			}
			result.put("currentDebit", totalIsReceived);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitAutoAction.getCurrentDebit"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Save pay.
	 * 
	 * @return the string
	 */
	public String savePay() throws Exception {
		resetToken(result);
		boolean error = true;
		errMsg = "";
		try {
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (!validateShop()) {
					return JSON;
				}
				errMsg = ValidateUtil.validateField(payReceivedNumber, "customerdebit.auto.pay.received.number", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, shortCode));
				Long customerId = new Long(0);
				if (customer != null) {
					customerId = customer.getId();
				}
				boolean isRecevied = true;
				if (status == 2) {
					isRecevied = false;
				}
				Date fDate = null;
				Date tDate = null;
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					fDate = ths.dms.web.utils.DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					tDate = ths.dms.web.utils.DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
				}

				Staff staffDeliver = new Staff();
				if (!StringUtil.isNullOrEmpty(staffDeliverCode)) {
					//staffDeliver = staffMgr.getStaffByCode(staffDeliverCode);
					staffDeliver = staffMgr.getStaffByInfoBasic(staffDeliverCode, shopId, ActiveType.RUNNING);
				}
				Staff staffPayment = new Staff();
				if (!StringUtil.isNullOrEmpty(staffPaymentCode)) {
					//staffPayment = staffMgr.getStaffByCode(staffPaymentCode);
					staffPayment = staffMgr.getStaffByInfoBasic(staffPaymentCode, shopId, ActiveType.RUNNING);
				}

				totalIsReceived = deBitMgr.getCustomerTotalDebitDetail(shopId, null, null, shortCode, null, null, null, isRecevied, staffDeliver.getId(), staffPayment.getId(), fDate, tDate);
				if (status == 2) {
					totalIsReceived = totalIsReceived.negate();
				}
				if (valuePay.compareTo(totalIsReceived) > 0) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerAuto.fail.savePay");
					result.put(ERROR, error);
					result.put("errMsg", errMsg);
					return JSON;
				}
				try {
					if (staff != null) {
						if (status == 2) {
							valuePay = valuePay.negate();
						}
						deBitMgr.updateDebitOfCustomer(shopId, customerId, payReceivedNumber, DebtPaymentType.AUTO, valuePay, lstDebitId, lstDebitAmt, null, staff.getStaffCode(), PayReceivedType.THANH_TOAN, null, PaymentStatus.PAID);
						Date lockDate = shopLockMgr.getNextLockedDay(shopId);
						if (lockDate == null) {
							lockDate = commonMgr.getSysDate();
						}
						payReceivedNumber = deBitMgr.getPayReceivedStringSugguest(lockDate);
						error = false;
					} else {
						error = true;
					}
				} catch (Exception e) {
					LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitAutoAction.savePay"), createLogErrorStandard(actionStartTime));
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitAutoAction.savePay"), createLogErrorStandard(actionStartTime));
		}
		result.put(ERROR, error);
		result.put("payReceivedNumber", payReceivedNumber);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_CUSTAUTO_UPDATE, null, "customerdebit.auto.update.error");
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * xu ly khi chon shop
	 * 
	 * @author trietptm
	 * @return String
	 * @since Dec 21, 2015
	 */
	public String changeShop() {
		result.put("lstSale", new ArrayList<StaffVO>());
		result.put("lstCashier", new ArrayList<StaffVO>());
		result.put("lstDeliver", new ArrayList<StaffVO>());
		result.put("listBank", new ArrayList<StaffVO>());
		result.put("payReceivedNumber", "");
		try {
			if (!validateShop()) {
				return JSON;
			}
			StaffFilter filter = new StaffFilter();
			filter.setShopId(shop.getId());
			filter.setStatus(ActiveType.RUNNING);
			filter.setSpecType(StaffSpecificType.STAFF);

			String strLstShopId = super.getStrListShopId();
			//Lay nhan vien thu tien
			filter.setStrListUserId(null);
			filter.setStrListShopId(strLstShopId);
			filter.setSpecType(StaffSpecificType.NVTT);
			ObjectVO<StaffVO> objVOCash = staffMgr.getListStaffVO(filter);
			lstNVTTVo = objVOCash.getLstObject();

			//Lay nhan vien NVGH
			filter.setStrListUserId(null);
			filter.setStrListShopId(strLstShopId);
			filter.setSpecType(StaffSpecificType.NVGH);
			ObjectVO<StaffVO> objVODeliver = staffMgr.getListStaffVO(filter);
			lstNVGHVo = objVODeliver.getLstObject();

			Date lockDate = shopLockMgr.getNextLockedDay(shopId);
			if (lockDate == null) {
				lockDate = commonMgr.getSysDate();
			}
			payReceivedNumber = deBitMgr.getPayReceivedStringSugguest(lockDate);

			if (lstNVTTVo != null) {
				result.put("lstCashier", lstNVTTVo);
			}
			if (lstNVGHVo != null) {
				result.put("lstDeliver", lstNVGHVo);
			}

			result.put("payReceivedNumber", payReceivedNumber);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.CustomerDebitShowAction.changeShop"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	private boolean validateShop() throws BusinessException {
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			shop = shopMgr.getShopByCode(shopCode);
		} else {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
			return false;
		}
		if (shop == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
			return false;
		}
		shopId = shop.getId();
		if (super.getMapShopChild().get(shopId) == null) {
			result.put(ERROR, true);
			result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
			return false;
		}
		return true;
	}

	/**
	 * Gets the pay received number.
	 * 
	 * @return the pay received number
	 */
	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}

	/**
	 * Sets the pay received number.
	 * 
	 * @param payReceivedNumber
	 *            the new pay received number
	 */
	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Gets the warning.
	 * 
	 * @return the warning
	 */
	public int getWarning() {
		return warning;
	}

	/**
	 * Sets the warning.
	 * 
	 * @param warning
	 *            the new warning
	 */
	public void setWarning(int warning) {
		this.warning = warning;
	}

	/**
	 * Gets the tt.
	 * 
	 * @return the tt
	 */
	public BigDecimal getTt() {
		return tt;
	}

	/**
	 * Sets the tt.
	 * 
	 * @param tt
	 *            the new tt
	 */
	public void setTt(BigDecimal tt) {
		this.tt = tt;
	}

	/**
	 * Gets the ttt.
	 * 
	 * @return the ttt
	 */
	public BigDecimal getTtt() {
		return ttt;
	}

	/**
	 * Sets the ttt.
	 * 
	 * @param ttt
	 *            the new ttt
	 */
	public void setTtt(BigDecimal ttt) {
		this.ttt = ttt;
	}

	/**
	 * Gets the ta.
	 * 
	 * @return the ta
	 */
	public BigDecimal getTa() {
		return ta;
	}

	/**
	 * Sets the ta.
	 * 
	 * @param ta
	 *            the new ta
	 */
	public void setTa(BigDecimal ta) {
		this.ta = ta;
	}

	public BigDecimal getTotalIsReceived() {
		return totalIsReceived;
	}

	public void setTotalIsReceived(BigDecimal totalIsReceived) {
		this.totalIsReceived = totalIsReceived;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * Gets the lst debit id.
	 * 
	 * @return the lst debit id
	 */
	public List<Long> getLstDebitId() {
		return lstDebitId;
	}

	/**
	 * Sets the lst debit id.
	 * 
	 * @param lstDebitId
	 *            the new lst debit id
	 */
	public void setLstDebitId(List<Long> lstDebitId) {
		this.lstDebitId = lstDebitId;
	}

	/**
	 * Gets the lst debit amt.
	 * 
	 * @return the lst debit amt
	 */
	public List<BigDecimal> getLstDebitAmt() {
		return lstDebitAmt;
	}

	/**
	 * Sets the lst debit amt.
	 * 
	 * @param lstDebitAmt
	 *            the new lst debit amt
	 */
	public void setLstDebitAmt(List<BigDecimal> lstDebitAmt) {
		this.lstDebitAmt = lstDebitAmt;
	}

	/**
	 * Gets the shop.
	 * 
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 * 
	 * @param shop
	 *            the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * Gets the shop id.
	 * 
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 * 
	 * @param shopId
	 *            the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the value pay.
	 * 
	 * @return the value pay
	 */
	public BigDecimal getValuePay() {
		return valuePay;
	}

	/**
	 * Sets the value pay.
	 * 
	 * @param valuePay
	 *            the new value pay
	 */
	public void setValuePay(BigDecimal valuePay) {
		this.valuePay = valuePay;
	}

	/**
	 * Gets the before pay.
	 * 
	 * @return the before pay
	 */
	public BigDecimal getBeforePay() {
		return beforePay;
	}

	/**
	 * Sets the before pay.
	 * 
	 * @param beforePay
	 *            the new before pay
	 */
	public void setBeforePay(BigDecimal beforePay) {
		this.beforePay = beforePay;
	}

	/**
	 * Gets the after pay.
	 * 
	 * @return the after pay
	 */
	public BigDecimal getAfterPay() {
		return afterPay;
	}

	/**
	 * Sets the after pay.
	 * 
	 * @param afterPay
	 *            the new after pay
	 */
	public void setAfterPay(BigDecimal afterPay) {
		this.afterPay = afterPay;
	}

	/**
	 * Gets the short code.
	 * 
	 * @return the short code
	 */
	public String getShortCode() {
		return shortCode;
	}

	/**
	 * Sets the short code.
	 * 
	 * @param shortCode
	 *            the new short code
	 */
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	/**
	 * Gets the list customer debit.
	 * 
	 * @return the list customer debit
	 */
	public List<CustomerDebitVO> getListCustomerDebit() {
		return listCustomerDebit;
	}

	/**
	 * Sets the list customer debit.
	 * 
	 * @param listCustomerDebit
	 *            the new list customer debit
	 */
	public void setListCustomerDebit(List<CustomerDebitVO> listCustomerDebit) {
		this.listCustomerDebit = listCustomerDebit;
	}

	/**
	 * Gets the lst id.
	 * 
	 * @return the lst id
	 */
	public List<Long> getLstId() {
		return lstId;
	}

	/**
	 * Sets the lst id.
	 * 
	 * @param lstId
	 *            the new lst id
	 */
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	// 
	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#getStaff()
	 */
	public Staff getStaff() {
		return staff;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.web.action.general.AbstractAction#setStaff(ths.dms.core.entities
	 * .Staff)
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	/**
	 * Gets the shop code.
	 * 
	 * @return the shop code
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * Sets the shop code.
	 * 
	 * @param shopCode
	 *            the new shop code
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * Gets the staff code.
	 * 
	 * @return the staff code
	 */
	public String getStaffCode() {
		return staffCode;
	}

	/**
	 * Sets the staff code.
	 * 
	 * @param staffCode
	 *            the new staff code
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	/**
	 * Gets the list debit.
	 * 
	 * @return the list debit
	 */
	public List<CustomerDebitVO> getListDebit() {
		return listCustomerDebit;
	}

	/**
	 * Sets the list debit.
	 * 
	 * @param listDebit
	 *            the new list debit
	 */
	public void setListDebit(List<CustomerDebitVO> listDebit) {
		this.listCustomerDebit = listDebit;
	}

	/**
	 * Gets the customer.
	 * 
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * Sets the customer.
	 * 
	 * @param customer
	 *            the new customer
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getStaffDeliverCode() {
		return staffDeliverCode;
	}

	public void setStaffDeliverCode(String staffDeliverCode) {
		this.staffDeliverCode = staffDeliverCode;
	}

	public String getStaffPaymentCode() {
		return staffPaymentCode;
	}

	public void setStaffPaymentCode(String staffPaymentCode) {
		this.staffPaymentCode = staffPaymentCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public List<StaffVO> getLstNVGHVo() {
		return lstNVGHVo;
	}

	public void setLstNVGHVo(List<StaffVO> lstNVGHVo) {
		this.lstNVGHVo = lstNVGHVo;
	}

	public List<StaffVO> getLstNVTTVo() {
		return lstNVTTVo;
	}

	public void setLstNVTTVo(List<StaffVO> lstNVTTVo) {
		this.lstNVTTVo = lstNVTTVo;
	}
}

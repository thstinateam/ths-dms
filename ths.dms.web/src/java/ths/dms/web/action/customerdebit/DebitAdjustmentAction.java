package ths.dms.web.action.customerdebit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.vo.DebitObjectVO;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.DebitObject;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Action: lap phieu dieu chinh cong no
 * 
 * @author lacnv1
 * @since Jun 03, 2014
 */
public class DebitAdjustmentAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private CustomerMgr customerMgr;
	private DebitMgr debitMgr;
	private String shopCode;
	private List<DebitObject> lstDebit;
	private Integer type;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		debitMgr = (DebitMgr) context.getBean("debitMgr");
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		shopCode = currentUser.getShopRoot().getShopCode();
		return SUCCESS;
	}

	/**
	 * Luu phieu dieu chinh cong no
	 * 
	 * @author lacnv1
	 * @since Jun 04, 2014
	 */
	public String saveDebit() throws Exception {
		try {
			resetToken(result);
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			shopId = shop.getId();
			if (super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}

			if (type == null || lstDebit == null || lstDebit.size() == 0) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.data.not.full"));
				return JSON;
			}
			String dbNumber = null;
			String shortCode = null;
			BigDecimal amount = null;
			String reason = null;
			DebitObject dbObj = null;
			Long custId = null;
			Customer cust = null;
			List<DebitObjectVO> lst = new ArrayList<DebitObjectVO>();
			DebitObjectVO vo = null;
			for (int j = 0, sz1 = lstDebit.size(); j < sz1; j++) {
				dbObj = lstDebit.get(j);
				shortCode = dbObj.getShortCode();
				reason = dbObj.getReason();
				dbNumber = dbObj.getDbNumber();
				amount = dbObj.getAmount();
				errMsg = ValidateUtil.validateField(dbNumber, "debit.adjustment.db.number", 40, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(reason, "debit.adjustment.reason", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				if (type == 1 && StringUtil.isNullOrEmpty(shortCode)) {
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "baocao.dtbh.tddstkh.nh.mh.khachhang"));
					return JSON;
				}
				if (amount == null || BigDecimal.ZERO.equals(amount)) {
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customerdebit.batch.import.money.not.greater.zero"));
					return JSON;
				}
				cust = null;
				if (!StringUtil.isNullOrEmpty(shortCode)) {
					List<Customer> lstCust = customerMgr.getListCustomerByShortCode(shopId, shortCode);
					if (lstCust == null || lstCust.size() == 0) {
						result.put("errMsg", R.getResource("common.customer.not.exists", shortCode, shop.getShopCode()));
						return JSON;
					}
					if (lstCust.size() == 1) {
						cust = lstCust.get(0);
					} else {
						int c = 0;
						for (int i = 0, sz = lstCust.size(); i < sz; i++) {
							if (ActiveType.RUNNING.equals(lstCust.get(i).getStatus())) {
								cust = lstCust.get(i);
								c++;
								if (c > 1) {
									result.put("errMsg", R.getResource("common.error.data"));
									return JSON;
								}
							}
						}
					}
					if (cust == null || !ActiveType.RUNNING.equals(cust.getStatus())) {
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, R.getResource("baocao.dtbh.tddstkh.nh.mh.khachhang") + " " + shortCode.toUpperCase()));
						return JSON;
					}
				}
				custId = (cust == null || type == 2) ? null : cust.getId();

				// kiem tra ma so phieu da co chua
				boolean b = debitMgr.existsDebitNumber(shopId, dbNumber.trim());
				if (b) {
					result.put("errMsg", R.getResource("debit.adjustment.number.exists", dbNumber));
					return JSON;
				}

				if (dbObj.getDebitType() != null && 2 == dbObj.getDebitType()) {
					amount = amount.negate();
				}
				// luu thong tin
				//debitMgr.createDebitIncrement(shopId, dbNumber, amount, custId, reason, getLogInfoVO());
				vo = new DebitObjectVO();
				vo.setShopId(shopId);
				vo.setDebitNumber(dbNumber);
				vo.setAmount(amount);
				vo.setReason(reason);
				vo.setCustomerId(custId);
				vo.setCustomer(cust);
				lst.add(vo);
			}
			debitMgr.createListDebitIncrement(lst, getLogInfoVO());
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.customerdebit.DebitAdjustmentAction.saveDebit"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/** getters and setters */
	public List<DebitObject> getLstDebit() {
		return lstDebit;
	}

	public void setLstDebit(List<DebitObject> lstDebit) {
		this.lstDebit = lstDebit;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
}
/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package ths.dms.web.action.vansale;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductOrderVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransVO;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;


/**
 * Lap don tra hang vansale (GO)
 * 
 * @author lacnv1
 * @since Sep 25, 2014
 */
public class VansaleOrderReturnAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private StockManagerMgr stockManagerMgr;
	
	private StockMgr stockMgr;

	private boolean lockPage;
	
	private String staffCode;
	
	private List<String> lstDP;
	
	private List<Staff> lstStaff;
	private List<ProductOrderVO> lstProducts;
	
	private String shopCodeFilter; //shop don vi chon
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockManagerMgr = (StockManagerMgr)context.getBean("stockManagerMgr");
		stockMgr = (StockMgr)context.getBean("stockMgr");
	}
	
	@Override
	public String execute() throws Exception {
		try {
			resetToken(result);
		} catch (Exception ex) {
			LogUtility.logError(ex, "VansaleOrderReturnAction.execute - " + ex.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Lay ds sp tra hang cho nv
	 * 
	 * @author lacnv1
	 * @since Sep 25, 2014
	 */
	public String search() throws Exception {
		try {
			if (StringUtil.isNullOrEmpty(staffCode)) {
				result.put("total", 0);
				result.put("rows", new ArrayList<StockTotalVO>());
				return JSON;
			}
			
//			ShopToken sh = currentUser.getShopRoot();
			Shop sh = shopMgr.getShopByCode(shopCodeFilter);
			if (sh == null) {
				result.put("total", 0);
				result.put("rows", new ArrayList<StockTotalVO>());
				return JSON;
			}
			List<Long> lstShop = getListShopChildId();
			if (!lstShop.contains(sh.getId())) {
				result.put("total", 0);
				result.put("rows", new ArrayList<StockTotalVO>());
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}
			
			Staff st = staffMgr.getStaffByCode(staffCode);
			if (st == null) {
				result.put("total", 0);
				result.put("rows", new ArrayList<StockTotalVO>());
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, staffCode));
				return JSON;
			}
			
			if (st.getShop() == null || !sh.getId().equals(st.getShop().getId())) {
				result.put("total", 0);
				result.put("rows", new ArrayList<StockTotalVO>());
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_USERLOGIN, staffCode));
				return JSON;
			}
			
			Date pDate = commonMgr.getSysDate();
			int num = stockMgr.countVansaleOrderNotUpdatedStock(sh.getId(), st.getId(), pDate); // lay so don vansale chua cap nhat phai thu va kho
			if (num > 0) {
				result.put("total", 0);
				result.put("rows", new ArrayList<StockTotalVO>());
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("stock.vansale.go.not.updated.stock.total", num));
				return JSON;
			}
			
			boolean existsGO = stockManagerMgr.checkExistsGOStockTrans(st.getId(), pDate); // da ton tai don GO trong ngay
			if (existsGO) {
				result.put("total", 0);
				result.put("rows", new ArrayList<StockTotalVO>());
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("stock.vansale.go.staff.exists.GO"));
				return JSON;
			}
			
			List<StockTotalVO> lst = stockManagerMgr.getListProductDPByStaff(sh.getId(), st.getId());
			if (lst == null) {
				result.put("total", 0);
				result.put("rows", new ArrayList<StockTotalVO>());
			} else {
				result.put("total", lst.size());
				result.put("rows", lst);
			}
			
			//lay don DP
			StockStransFilter filter = new StockStransFilter();
			filter.setApproved(SaleOrderStatus.APPROVED);
			filter.setApprovedStep(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT);
			filter.setToOwnerID(st.getId());//nv chon
			filter.setShopId(sh.getId());
			List<StockTransVO> lstTrans = stockManagerMgr.getListDPByStaff(filter);
			if(lstTrans != null && lstTrans.size() > 0){
				String strDP = lstTrans.get(0).getStockTransCode();
				for(int i = 1 ; i < lstTrans.size() ; i++){
					strDP += ", "+ lstTrans.get(i).getStockTransCode();
				}
				result.put("lstDP", strDP);
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "VansaleOrderReturnAction.searchProduct - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put("rows", new ArrayList<StockTotalVO>());
		}
		return JSON;
	}
	
	/**
	 * Luu thong tin don hang
	 * 
	 * @author lacnv1
	 * @since Sep 25, 2014
	 */
	public String saveSaleOrder() throws Exception {
		try {
			resetToken(result);
			result.put(ERROR, true);
			
			Date now = commonMgr.getSysDate();
			Shop sh = shopMgr.getShopByCode(shopCodeFilter);
			if (sh == null) {
				result.put("errMsg", R.getResource("stock.vansale.go.not.lockedday"));
				return JSON;
			}
			List<Long> lstShop = getListShopChildId();
			if (!lstShop.contains(sh.getId())) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}			
			if (StringUtil.isNullOrEmpty(staffCode)) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.staff.code.saleplan"));
				return JSON;
			}
			Staff st = staffMgr.getStaffByCode(staffCode);
			if (st == null) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.staff.code.saleplan"));
				return JSON;
			}
			if (st.getShop() == null || !sh.getId().equals(st.getShop().getId())) {
				result.put("errMsg", R.getResource("ss.not.belong.general", staffCode, sh.getShopCode()));
				return JSON;
			}
			if (!StaffSpecificType.STAFF.getValue().equals(st.getStaffType().getSpecificType().getValue())
					&& !StaffSpecificType.SUPERVISOR.getValue().equals(st.getStaffType().getSpecificType().getValue())) {
				result.put("errMsg", R.getResource("catalog.not.nvbh", staffCode));
				return JSON;
			}
			
			String s = shopLockMgr.getListStaffLockStockYet(st.getId().toString(), null);
			if (!StringUtil.isNullOrEmpty(s)) {
				result.put("errMsg", R.getResource("stock.vansale.go.not.locked.stock.yet"));
				return JSON;
			}
			
			int num = stockMgr.countVansaleOrderNotUpdatedStock(sh.getId(), st.getId(), now); // lay so don vansale chua cap nhat phai thu va kho
			if (num > 0) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("stock.vansale.go.not.updated.stock.total", num));
				return JSON;
			}
			
			boolean b = stockManagerMgr.checkExistsGOStockTrans(st.getId(), now);
			if (b) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("stock.vansale.go.staff.exists.GO"));
				return JSON;
			}
			
			//Kiem tra dieu kien xuat kho: con don chua duyet cap nhat kho trang thai <>3
			boolean stockTransExist = stockManagerMgr.checkExistsGOStockTransNotApproves(st.getId(),"DP");
			if (stockTransExist) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("ss.not.belong.exist.DP.not.approved"));
				return JSON;
			}
			
			if (lstProducts == null || lstProducts.size() == 0) {
				result.put("errMsg", R.getResource("create.po.confirm.not.exist.product"));
				return JSON;
			}
			
			StockTrans stockTrans = stockManagerMgr.createStockTransGO(st, lstProducts, getLogInfoVO());
			if(stockTrans != null && lstDP != null){//them id vao cac don DP
				for(String dp : lstDP){
					StockTrans stockTransTemp = stockMgr.getStockTransByCodeAndShop(dp,stockTrans.getShop().getId());
					if(stockTransTemp != null){
						stockTransTemp.setFromStockTrans(stockTrans);
						stockMgr.updateStockTrans(stockTransTemp);
					}
				}
			}
			result.put("stockTransCode", stockTrans.getStockTransCode());
			
			result.put(ERROR, true);
		} catch (Exception ex) {
			result.put(ERROR, true);
			String s = ex.getMessage();
			if (s.startsWith(ProductOrderVO.ERR_NOT_ENOUGH)) { // tong so luong nhap sp khac ton kho NV
				s = s.replace(ProductOrderVO.ERR_NOT_ENOUGH, "");
				result.put("errMsg", R.getResource("stock.vansale.go.quantity.not.equals", s));
			} else {
				LogUtility.logError(ex, "VansaleOrderReturnAction.saveSaleOrder - " + ex.getMessage());
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		}
		return JSON;
	}
	
	/**
	 * Lay ds nhan vien
	 * 
	 * @author trietptm
	 * @since July 22, 2015
	 */
	public String changeShop() throws Exception {
		try {
			
			// Danh sach NV
			StaffFilter filter1 = new StaffFilter();
			filter1.setStatus(ActiveType.RUNNING);
			filter1.setShopCode(shopCodeFilter);
			filter1.setSpecType(StaffSpecificType.STAFF);
			filter1.setStrListUserId(getStrListUserId());
			ObjectVO<Staff> objStaffVO = staffMgr.getListStaffLockedStock(filter1);
			lstStaff = objStaffVO.getLstObject();
			result.put("lstStaff", lstStaff);
			
			Shop shop = shopMgr.getShopByCode(shopCodeFilter);
			dayLock = shopLockMgr.getNextLockedDay(shop.getId());
			if (dayLock == null) {
				dayLock = commonMgr.getSysDate();
			}
			lockDate = DateUtil.toDateSimpleFormatString(dayLock);
			result.put("lockDate", lockDate);
			
			// Kiem tra dk ngay chot
//			Date now = commonMgr.getSysDate();
//			if (DateUtil.compareDateWithoutTime(dayLock, now) != 0) {
//				result.put("errMsg", R.getResource("stock.vansale.go.not.lockedday"));
//				result.put("lockPage", true);
//			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "VansaleOrderCreationAction.execute - " + ex.getMessage());
		}
		return JSON;
	}
	

	/** getters + setters */
	/** */
	public List<Staff> getLstStaff() {
		return lstStaff;
	}

	public void setLstStaff(List<Staff> lstStaff) {
		this.lstStaff = lstStaff;
	}

	public boolean isLockPage() {
		return lockPage;
	}

	public void setLockPage(boolean lockPage) {
		this.lockPage = lockPage;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public List<ProductOrderVO> getLstProducts() {
		return lstProducts;
	}

	public void setLstProducts(List<ProductOrderVO> lstProducts) {
		this.lstProducts = lstProducts;
	}

	public List<String> getLstDP() {
		return lstDP;
	}

	public void setLstDP(List<String> lstDP) {
		this.lstDP = lstDP;
	}

	public String getShopCodeFilter() {
		return shopCodeFilter;
	}

	public void setShopCodeFilter(String shopCodeFilter) {
		this.shopCodeFilter = shopCodeFilter;
	}

	
}
/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.web.action.vansale;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.CarMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StockTransLotOwnerType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.CarSOFilter;
import ths.dms.core.entities.vo.CarSOVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductOrderVO;
import ths.dms.core.entities.vo.StockTotalVO;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Lap don ban hang vansale (DP)
 * 
 * @author lacnv1
 * @since Sep 22, 2014
 */
public class VansaleOrderCreationAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private StockManagerMgr stockManagerMgr;
	private StockMgr stockMgr;
	private ShopMgr shopMgr;

	private boolean lockPage = false;
	private boolean isPaging = false;

	private String staffCode;
	private String code;
	private String name;
	private Long carId;
	private Integer checkStock;

	private List<CarSOVO> lstCar;
	private List<Staff> lstStaff;
	private List<ProductOrderVO> lstProducts;
	private Long orderId;
	private String stockTransCode;
	private String fromStockTransCode;

	private String shopCodeFilter;//shop don vi

	@Override
	public void prepare() throws Exception {
		super.prepare();
		stockManagerMgr = (StockManagerMgr) context.getBean("stockManagerMgr");
		stockMgr = (StockMgr) context.getBean("stockMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
	}

	@Override
	public String execute() throws Exception {
		try {
			resetToken(result);
			// Khac ngay chot thi khong duoc xuat kho
			Date now = commonMgr.getSysDate();
			if (DateUtil.compareDateWithoutTime(dayLock, now) != 0) {
				errMsg = R.getResource("stock.vansale.dp.not.lockedday");
				lockPage = true;
				return SUCCESS;
			}

			// Kiem tra don vi
			ShopToken sh = currentUser.getShopRoot();
			if (sh == null) {
				errMsg = R.getResource("stock.vansale.dp.not.lockedday");
				lockPage = true;
				return SUCCESS;
			}

		} catch (Exception ex) {
			LogUtility.logError(ex, "VansaleOrderCreationAction.execute - " + ex.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Lay ds nhan vien
	 * 
	 * @author longnh15
	 * @since July 02, 2015
	 */
	public String searchStaff() throws Exception {
		try {

			// ds NVBH						
			StaffFilter filter1 = new StaffFilter();
			filter1.setStatus(ActiveType.RUNNING);
			filter1.setShopCode(shopCodeFilter);//shop don vi);
			//filter1.setChannelTypeType(ChannelTypeType.STAFF);
			//filter1.setLstChannelObjectType(lstStaffType);
			filter1.setSpecType(StaffSpecificType.STAFF);
			filter1.setStrListUserId(getStrListUserId());
			ObjectVO<Staff> objStaffVO = staffMgr.getListStaffEx2(filter1);
			lstStaff = objStaffVO.getLstObject();
			result.put("lstStaff", lstStaff);
			//result.put("rows", objStaffVO.getLstObject());
			//result.put("total", objStaffVO.getkPaging().getTotalRows());

			// ds xe
			Shop shop = shopMgr.getShopByCode(shopCodeFilter);
			CarSOFilter filter = new CarSOFilter();
			filter.setShopId(shop.getId());
			filter.setStatus(ActiveType.RUNNING);
			filter.setInChildShop(false);
			CarMgr carMgr = (CarMgr) context.getBean("carMgr");
			ObjectVO<CarSOVO> objCarVO = carMgr.getListCar(filter);
			lstCar = objCarVO.getLstObject();
			result.put("lstCar", lstCar);
		} catch (Exception ex) {
			LogUtility.logError(ex, "VansaleOrderCreationAction.execute - " + ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Lay ds sp ban
	 * 
	 * @author lacnv1
	 * @since Sep 23, 2014
	 */
	public String searchProduct() throws Exception {
		try {
			// Kiem tra tham so
			if (StringUtil.isNullOrEmpty(staffCode)) {
				result.put("rows", new ArrayList<StockTotalVO>());
				return JSON;
			}
			ShopToken sh = currentUser.getShopRoot();
			if (sh == null) {
				result.put("rows", new ArrayList<StockTotalVO>());
				return JSON;
			}
			Staff st = staffMgr.getStaffByCode(staffCode);
			if (st == null || st.getShop() == null) { //|| !sh.getShopId().equals(st.getShop().getId()) //remove phan quyen theo cung NPP
				result.put("rows", new ArrayList<StockTotalVO>());
				return JSON;
			}
			//			if (!StaffObjectType.NVBH.getValue().equals(st.getStaffType().getObjectType())
			//					&& !StaffObjectType.NVVS.getValue().equals(st.getStaffType().getObjectType())) {
			//				result.put("rows", new ArrayList<StockTotalVO>());
			//				return JSON;
			//			}

			// Lay ds sp
			BasicFilter<StockTotalVO> filter = new BasicFilter<StockTotalVO>();
			filter.setShopId(st.getShop().getId());
			filter.setLongG(st.getId());
			filter.setIntG(checkStock);

			if (isPaging) { // co phan trang (dialog)
				filter.setCode(code);
				filter.setName(name);
				KPaging<StockTotalVO> paging = new KPaging<StockTotalVO>();
				paging.setPage(page - 1);
				paging.setPageSize(max);
				filter.setkPaging(paging);

				result.put("page", page);
				result.put("max", max);
			}

			ObjectVO<StockTotalVO> obj = stockManagerMgr.getListStockTotalVOBySaler(filter);

			if (isPaging) {
				result.put("total", obj.getkPaging().getTotalRows());
			}
			result.put("rows", obj.getLstObject());
		} catch (Exception ex) {
			LogUtility.logError(ex, "VansaleOrderCreationAction.searchProduct - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Luu thong tin don hang
	 * 
	 * @author lacnv1
	 * @since Sep 24, 2014
	 */
	public String saveSaleOrder() throws Exception {
		try {
			resetToken(result);
			result.put(ERROR, true);

			// Kiem tra tham so
			ShopToken sh = currentUser.getShopRoot();
			if (sh == null) {
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(staffCode)) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.staff.code.saleplan"));
				return JSON;
			}
			Staff st = staffMgr.getStaffByCode(staffCode);
			if (st == null) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.staff.code.saleplan"));
				return JSON;
			}

			//Kiem tra dieu kien xuat kho: con don chua duyet cap nhat kho trang thai <>3
			boolean stockTransExist = stockManagerMgr.checkExistsGOStockTransNotApproves(st.getId(), "GO");
			if (stockTransExist) {
				result.put("errMsg", R.getResource("ss.not.belong.exist.GO.not.approved"));
				return JSON;
			}

			if (carId == null || carId < 0) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "sp.create.order.car.error.not.found"));
				return JSON;
			}
			CarMgr carMgr = (CarMgr) context.getBean("carMgr");
			Car car = carMgr.getCarById(carId);
			if (car == null) {
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "sp.create.order.car.error.not.found"));
				return JSON;
			}
			if (car.getShop() == null) { //|| !sh.getShopId().equals(car.getShop().getId())
				result.put("errMsg", R.getResource("ss.not.belong.general", R.getResource("sp.create.order.car.error.not.found"), sh.getShopCode()));
				return JSON;
			}

			if (lstProducts == null || lstProducts.size() == 0) {
				result.put("errMsg", R.getResource("create.po.confirm.not.exist.product"));
				return JSON;
			}

			// Luu thong tin
			StockTrans stockTrans = stockManagerMgr.createStockTransDP(st, car, lstProducts, getLogInfoVO());

			result.put("stockTransCode", stockTrans.getStockTransCode());

			result.put(ERROR, true);
		} catch (Exception ex) {
			result.put(ERROR, true);
			String s = ex.getMessage();
			if (s.startsWith(ProductOrderVO.ERR_NOT_ENOUGH)) { // khong du ton kho dap ung (do trong qua trinh tao co nguoi khac cap nhat ton kho)
				s = s.replace(ProductOrderVO.ERR_NOT_ENOUGH, "");
				result.put("errMsg", R.getResource("stock.product.not.enough.quantity", s));
			} else {
				LogUtility.logError(ex, "VansaleOrderCreationAction.saveSaleOrder - " + ex.getMessage());
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		}
		return JSON;
	}

	public String getOrderDetail() {
		try {
			if (orderId == null) {
				result.put("rows", new ArrayList<StockTotalVO>());
				return JSON;
			}
			StockTrans st = stockMgr.getStockTransById(orderId);
			if (st == null) {
				result.put("rows", new ArrayList<StockTotalVO>());
				return JSON;
			}
			BasicFilter<StockTotalVO> filter = new BasicFilter<StockTotalVO>();
			filter.setId(orderId);
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCodeFilter)) {
				shop = shopMgr.getShopByCode(shopCodeFilter);
			}
			if (shop == null) {
				result.put("rows", new ArrayList<StockTotalVO>());
				return JSON;
			}
			filter.setShopId(shop.getId());
			if (OrderType.DP.getValue().equals(st.getTransType()) || OrderType.DCG.getValue().equals(st.getTransType())) {
				filter.setFromId(StockTransLotOwnerType.WAREHOUSE.getValue().longValue());
			} else if (OrderType.GO.getValue().equals(st.getTransType()) || OrderType.DCT.getValue().equals(st.getTransType())) {
				filter.setToId(StockTransLotOwnerType.WAREHOUSE.getValue().longValue());
			} else if (OrderType.DC.getValue().equals(st.getTransType())) {
				filter.setFromId(StockTransLotOwnerType.UPDATED.getValue().longValue());//from luc nao cung co, to co the co or khong -> chon from
			}
			// Lay ds sp
			List<StockTotalVO> lstFooter = new ArrayList<StockTotalVO>();
			StockTotalVO footer = new StockTotalVO();
			ObjectVO<StockTotalVO> obj = null;
			if (OrderType.DP.getValue().equals(st.getTransType()) || OrderType.GO.getValue().equals(st.getTransType())) {
				obj = stockManagerMgr.getListStockTransDetail(filter);
			} else {
				obj = stockManagerMgr.getListStockTransDetailForDC(filter);
			}
			if (obj != null && obj.getLstObject() != null) {
				List<StockTotalVO> lst = obj.getLstObject();
				footer.setAmount(st.getTotalAmount());
				footer.setWeight(st.getTotalWeight());
				footer.setIsFooter(true);
				lstFooter.add(footer);
				result.put("rows", lst);
				result.put("footer", lstFooter);
			} else {
				result.put("rows", new ArrayList<StockTotalVO>());
				lstFooter.add(footer);
				result.put("footer", lstFooter);
			}
			result.put(ERROR, false);
		} catch (Exception ex) {
			LogUtility.logError(ex, "VansaleOrderCreationAction.getOrderDetail - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/** getters + setters */
	/** */
	public List<ProductOrderVO> getLstProducts() {
		return lstProducts;
	}

	public void setLstProducts(List<ProductOrderVO> lstSaleOrder) {
		this.lstProducts = lstSaleOrder;
	}

	public List<CarSOVO> getLstCar() {
		return lstCar;
	}

	public void setLstCar(List<CarSOVO> lstCar) {
		this.lstCar = lstCar;
	}

	public List<Staff> getLstStaff() {
		return lstStaff;
	}

	public void setLstStaff(List<Staff> lstStaff) {
		this.lstStaff = lstStaff;
	}

	public boolean isLockPage() {
		return lockPage;
	}

	public void setLockPage(boolean lockPage) {
		this.lockPage = lockPage;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public boolean getIsPaging() {
		return isPaging;
	}

	public void setIsPaging(boolean isPaging) {
		this.isPaging = isPaging;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public Integer getCheckStock() {
		return checkStock;
	}

	public void setCheckStock(Integer checkStock) {
		this.checkStock = checkStock;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getFromStockTransCode() {
		return fromStockTransCode;
	}

	public void setFromStockTransCode(String fromStockTransCode) {
		this.fromStockTransCode = fromStockTransCode;
	}

	public String getStockTransCode() {
		return stockTransCode;
	}

	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}

	public String getShopCodeFilter() {
		return shopCodeFilter;
	}

	public void setShopCodeFilter(String shopCodeFilter) {
		this.shopCodeFilter = shopCodeFilter;
	}
}
package ths.dms.web.action.superviseshop;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.vo.ActionLogVO;
import ths.dms.core.entities.vo.CustomerPositionVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.SuperVisorInfomationVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;

public class VisitResultAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2083175666718778707L;

	private StaffMgr staffMgr;
	private ShopMgr shopMgr;
	private CommonMgr commonMgr;
	private SuperviserMgr superviserMgr;
	
	private Staff staff;
	private Shop shop;
	
	private String shopCode;
	private String staffSaleCode;
	private String saleDate;

	private String staffCode;

	private Boolean allLevel;

	private Integer staffType;

	private Integer objectType;

	private Long staffId;
	
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public CommonMgr getCommonMgr() {
		return commonMgr;
	}

	public void setCommonMgr(CommonMgr commonMgr) {
		this.commonMgr = commonMgr;
	}

	public SuperviserMgr getSuperviserMgr() {
		return superviserMgr;
	}

	public void setSuperviserMgr(SuperviserMgr superviserMgr) {
		this.superviserMgr = superviserMgr;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getStaffSaleCode() {
		return staffSaleCode;
	}

	public void setStaffSaleCode(String staffSaleCode) {
		this.staffSaleCode = staffSaleCode;
	}

	public String getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}
	
	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Boolean getAllLevel() {
		return allLevel;
	}

	public void setAllLevel(Boolean allLevel) {
		this.allLevel = allLevel;
	}

	public Integer getStaffType() {
		return staffType;
	}

	public void setStaffType(Integer staffType) {
		this.staffType = staffType;
	}
	
	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	
	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public boolean checkExistShopOfUserLogin(){
		if(currentUser != null && currentUser.getUserName() != null){
			try {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
			} catch (BusinessException e) {
				LogUtility.logError(e, e.getMessage());
				return false;
			}
			if(staff != null && staff.getShop() != null){
				shop = staff.getShop();
			}
		}
		if(shop == null) return false;
		else return true;
	}
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		superviserMgr = (SuperviserMgr) context.getBean("superviserMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
	}
	
	@Override
	public String execute() {
		resetToken(result);
		String result = SUCCESS;
		try {
			// CHECK QUYEN
			boolean isExistShop = checkExistShopOfUserLogin();
			if(isExistShop){
				saleDate = DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			}else{
				result = PAGE_NOT_FOUND;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return result;
	}

	/**
	 * Gets the info.
	 * Ham getInfo() su dung de tim kiem ket qua ghe tham
	 * @return the info
	 * @since Jan 15, 2013
	 * @author thongnm1
	 */
	
	public String getInfo() {
		result.put("page", page);
		result.put("max", max);
		try {
			boolean isExistShop = checkExistShopOfUserLogin();
			if(isExistShop&& !StringUtil.isNullOrEmpty(shopCode)&&!StringUtil.isNullOrEmpty(saleDate)){
				Shop shopTmp = shopMgr.getShopByCode(shopCode);
				Date saleDateTmp = DateUtil.parse(saleDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if(saleDateTmp != null&& shopTmp != null && shopTmp.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
					KPaging<ActionLogVO> kPaging = new KPaging<ActionLogVO>();
					kPaging.setPageSize(max);
					kPaging.setPage(page - 1);
					ObjectVO<ActionLogVO> lstActionLogVO = null;
					lstActionLogVO = superviserMgr.getListVisitPlanBySuperVisor(kPaging, shopTmp.getId(), staffSaleCode, saleDateTmp,false);
					if (lstActionLogVO != null) {
						result.put("total", lstActionLogVO.getkPaging().getTotalRows());
						result.put("rows", lstActionLogVO.getLstObject());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error VisitResultAction.getInfo: " + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Init seller position
	 * Giam sat vi tri cua NVBH
	 * @since Jan 16, 2013
	 * @author lampv1
	 */
	public String initSellerPosition() {
		String result = LIST;
		try {
			//check permission
			boolean isExistShop = checkExistShopOfUserLogin();
			if(!(isExistShop)){
				result = PAGE_NOT_FOUND;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error VisitResultAction.initSellerPosition: " + e.getMessage());
		}
		return result;
	}
	
	/**
	 * Search seller position
	 * Tim kiem giam sat vi tri cua NVBH
	 * @since Jan 16, 2013
	 * @author lampv1
	 */
	public String searchSellerPosition() {
		result = new HashMap<String, Object>();
		result.put("page", page);
	    result.put("max", max);
		try {
			KPaging<StaffVO> kPaging = new KPaging<StaffVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			if(StringUtil.isNullOrEmpty(shopCode)){
				result.put(SUCCESS, false);
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"seller.position.shop.code.invalid"));
				return JSON;
			}
			List<StaffObjectType> lstChannelObjectType = new ArrayList<StaffObjectType>();
			if(staffType==-1){//Tat ca
				lstChannelObjectType.add(StaffObjectType.NVBH);
				lstChannelObjectType.add(StaffObjectType.NVGS);
				lstChannelObjectType.add(StaffObjectType.TBHV);
			}else if(staffType>0){
				lstChannelObjectType.add(StaffObjectType.parseValue(staffType));
			}
//			StaffObjectType staffTypeObj = null;
//			if(staffType!=-1){
//				lstChannelObjectType.add(1);
//			}
//			boolean isGetShopOnly = true;
//			if(allLevel){
//				isGetShopOnly = false;
//			}
			//ObjectVO<Staff> lstStaff = staffMgr.getListStaffWithFullCondition(kPaging, staffCode, null, null, null, ActiveType.RUNNING, shopCode, null, ChannelTypeType.STAFF, lstChannelObjectType, null, null, isGetShopOnly, null, null, null);
			//ObjectVO<Staff> lstStaff = staffMgr.getListStaff(kPaging, staffCode, null, null, null, null, shopCode, staffTypeObj, null, isGetShopOnly, null, null, null);
			ObjectVO<StaffVO> lstStaff = staffMgr.getListStaffForSuperVisorWithCondition(kPaging, staffCode, null, shopCode, ActiveType.RUNNING, ChannelTypeType.STAFF, lstChannelObjectType, allLevel);
			if(lstStaff!= null){
				result.put("total", lstStaff.getkPaging().getTotalRows());
				result.put("rows", lstStaff.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error VisitResultAction.searchSellerPosition: " + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Get seller position detail
	 * Thong tin chi tiet vi tri cua NVBH
	 * @since Jan 16, 2013
	 * @author lampv1
	 */
	public String getSellerPositionDetail() {
		result = new HashMap<String, Object>();
		try {
			//check permission
			boolean isExistShop = checkExistShopOfUserLogin();
			if(isExistShop && objectType>0 && staffId>0){
				if(StaffObjectType.TBHV.equals(StaffObjectType.parseValue(objectType))){
					//lay du lieu TBHV va danh sach nha phan phoi dua len ban do
					//get main info
					List<SuperVisorInfomationVO> lstParentSuperVisor = superviserMgr.getInfomationOfParentSuper(staffId);
					if(lstParentSuperVisor!=null && lstParentSuperVisor.size()>0){
						ObjectVO<SuperVisorInfomationVO> mainVO = new ObjectVO<SuperVisorInfomationVO>();
						mainVO.setLstObject(lstParentSuperVisor);
						ObjectVO<SuperVisorInfomationVO> mainInfo = mainVO;
						result.put("mainInfo", mainInfo.getLstObject());
						//get child info
						List<StaffPositionVO> lstGSNPP = superviserMgr.getListSuperPosition(staffId);
						if(lstParentSuperVisor!=null && lstParentSuperVisor.size()>0){
							ObjectVO<StaffPositionVO> childVO = new ObjectVO<StaffPositionVO>();
							childVO = new ObjectVO<StaffPositionVO>();
							childVO.setLstObject(lstGSNPP);
							ObjectVO<StaffPositionVO> childInfo = childVO;
							result.put("childInfo", childInfo.getLstObject());
						}
					}
					result.put(SUCCESS, true);
				}else if(StaffObjectType.NVGS.equals(StaffObjectType.parseValue(objectType))){
					//lay du lieu GSNPP va danh sach nha phan phoi dua len ban do
					//get main info
					List<SuperVisorInfomationVO> lstSuperVisor = superviserMgr.getInfomationOfSuper(staffId);
					if(lstSuperVisor!=null && lstSuperVisor.size()>0){
						ObjectVO<SuperVisorInfomationVO> mainVO = new ObjectVO<SuperVisorInfomationVO>();
						mainVO.setLstObject(lstSuperVisor);
						ObjectVO<SuperVisorInfomationVO> mainInfo = mainVO;
						result.put("mainInfo", mainInfo.getLstObject());
						//get child info
						List<StaffPositionVO> lstNVBH = superviserMgr.getListStaffPosition(staffId, StaffObjectType.NVBH);
						if(lstSuperVisor!=null && lstSuperVisor.size()>0){
							ObjectVO<StaffPositionVO> childVO = new ObjectVO<StaffPositionVO>();
							childVO = new ObjectVO<StaffPositionVO>();
							childVO.setLstObject(lstNVBH);
							ObjectVO<StaffPositionVO> childInfo = childVO;
							result.put("childInfo", childInfo.getLstObject());
						}
					}
					result.put(SUCCESS, true);
				}else if(StaffObjectType.NVBH.equals(StaffObjectType.parseValue(objectType))){
					//lay du lieu NVBH va danh sach nha phan phoi dua len ban do
					//get main info
					List<SuperVisorInfomationVO> lstNVBH = superviserMgr.getInfomationOfStaff(staffId);
					if(lstNVBH!=null && lstNVBH.size()>0){
						ObjectVO<SuperVisorInfomationVO> mainVO = new ObjectVO<SuperVisorInfomationVO>();
						mainVO.setLstObject(lstNVBH);
						ObjectVO<SuperVisorInfomationVO> mainInfo = mainVO;
						result.put("mainInfo", mainInfo.getLstObject());
						//get child info
						List<CustomerPositionVO> lstCustomer = superviserMgr.getListCustomerForStaff(staffId);
						if(lstCustomer!=null && lstCustomer.size()>0){
							ObjectVO<CustomerPositionVO> childVO = new ObjectVO<CustomerPositionVO>();
							childVO = new ObjectVO<CustomerPositionVO>();
							childVO.setLstObject(lstCustomer);
							ObjectVO<CustomerPositionVO> childInfo = childVO;
							result.put("childInfo", childInfo.getLstObject());
						}
					}
				}
				result.put(SUCCESS, true);
			}else{
				return PAGE_NOT_FOUND;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error VisitResultAction.getSellerPositionDetail: " + e.getMessage());
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * Get NVBH detail
	 * Thong tin chi tiet NVBH
	 * @since Jan 16, 2013
	 * @author lampv1
	 */
	public String getNVBHDetail() {
		result = new HashMap<String, Object>();
		try {
			//check permission
			boolean isExistShop = checkExistShopOfUserLogin();
			if(isExistShop && StaffObjectType.NVBH.equals(StaffObjectType.parseValue(objectType))){
					List<SuperVisorInfomationVO> lstSuperVisor = superviserMgr.getInfomationOfStaff(staffId);
					if(lstSuperVisor!=null && lstSuperVisor.size()>0){
						ObjectVO<SuperVisorInfomationVO> mainVO = new ObjectVO<SuperVisorInfomationVO>();
						mainVO.setLstObject(lstSuperVisor);
						ObjectVO<SuperVisorInfomationVO> mainInfo = mainVO;
						result.put("mainInfo", mainInfo.getLstObject());
					}
					result.put(SUCCESS, true);
				}else{
					return PAGE_NOT_FOUND;
				}
		} catch (Exception e) {
			LogUtility.logError(e, "Error VisitResultAction.getNVBHDetail: " + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Get NVBH detail
	 * Thong tin chi tiet NVBH
	 * @since Jan 16, 2013
	 * @author lampv1
	 */
	public String getGSNPPdetail() {
		result = new HashMap<String, Object>();
		try {
			//check permission
			boolean isExistShop = checkExistShopOfUserLogin();
			if( isExistShop && StaffObjectType.NVGS.equals(StaffObjectType.parseValue(objectType))){
					List<SuperVisorInfomationVO> lstNVBH = superviserMgr.getInfomationOfSuper(staffId);
					if(lstNVBH!=null && lstNVBH.size()>0){
						ObjectVO<SuperVisorInfomationVO> childVO = new ObjectVO<SuperVisorInfomationVO>();
						childVO = new ObjectVO<SuperVisorInfomationVO>();
						childVO.setLstObject(lstNVBH);
						ObjectVO<SuperVisorInfomationVO> childInfo = childVO;
						result.put("childInfo", childInfo.getLstObject());
					}
					result.put(SUCCESS, true);
				}else{
					return PAGE_NOT_FOUND;
				}
		} catch (Exception e) {
			LogUtility.logError(e, "Error VisitResultAction.getNVBHDetail: " + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Get staff info
	 * Lay thong tin cua staff
	 * @since Jan 16, 2013
	 * @author lampv1
	 */
	public String getStaffInfo() {
		result = new HashMap<String, Object>();
		/*try {
			//check permission
			boolean isExistShop = checkExistShopOfUserLogin();
			if( isExistShop){
					Staff staff = staffMgr.getStaffById(staffId);
					if(staff!=null){
						result.put("staffCode", !StringUtil.isNullOrEmpty(staff.getStaffCode())?staff.getStaffCode():"");
						result.put("staffName", !StringUtil.isNullOrEmpty(staff.getStaffName())?staff.getStaffName():"");
						result.put("shopCode", ((staff.getShop()!=null && !StringUtil.isNullOrEmpty(staff.getShop().getShopCode()))?staff.getShop().getShopCode():""));
						result.put("objectType", (staff.getStaffType() !=null?staff.getStaffType().getObjectType():""));
						result.put(SUCCESS, true);
					}
				}else{
					return PAGE_NOT_FOUND;
				}
		} catch (Exception e) {
			LogUtility.logError(e, "Error VisitResultAction.getNVBHDetail: " + e.getMessage());
		}*/
		return JSON;
	}
	
	/**
	 * Gets the visit result.
	 * Ham getVisitResult() su dung de load danh sach toa do vi tri cac cua hang 
	 * ma NVBH di qua trong ngay
	 * @return the info
	 * @since Jan 15, 2013
	 * @author thongnm1
	 */
	public String getVisitResult() {
		result = new HashMap<String, Object>();
		try {
			boolean isExistShop = checkExistShopOfUserLogin();
			if(isExistShop && !StringUtil.isNullOrEmpty(shopCode)&&!StringUtil.isNullOrEmpty(saleDate)){
				Date saleDateTmp = DateUtil.parse(saleDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if(saleDateTmp != null) {
					Staff staffSale = staffMgr.getStaffByCode(staffSaleCode);
					if(staffSale != null && staffSale.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
						List<CustomerPositionVO> lstVisitResult = superviserMgr.getListCustomerPositionEx(staffSale.getId(), saleDateTmp);
						if(lstVisitResult != null){
							for(CustomerPositionVO customerPositionVOTmp : lstVisitResult){
								customerPositionVOTmp.safeSetNull();
							}
							result.put("total", lstVisitResult.size());
							result.put("visitResultLst", lstVisitResult);
							result.put(SUCCESS, true);
						} else {
							result.put(SUCCESS, false);
						}
					}
				}
			}
			
		} catch (Exception e) {
			LogUtility.logError(e, "Error VisitResultAction.loadMap: " + e.getMessage());
		}
		return JSON;
	}
}

package ths.dms.web.action.superviseshop;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.TrainingPlanVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class TrainingPlanAction extends AbstractAction {

	private static final long serialVersionUID = 4360158146842225350L;

	private StaffMgr staffMgr;
	private SuperviserMgr superviserMgr;
	private File excelFile;
	private String excelFileContentType;
	private CommonMgr commonMgr;
	private HashMap<String, Object> beans;

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/** Shop root theo nhan vien dang nhap current.getShopRoot() */
	private Shop shop;

	private String shopCode;
	private String shopName;

	private List<StaffVO> lstGsnpp;
	private String dateStr;
	private Long shopId;
	private Long gsnppId;
	private List<Long> lstId;
	private List<Long> lstDelete;
	private List<TrainingPlanVO> listCheck;
	private List<TrainingPlanVO> lstTrainingPlan;
	private boolean pressCheckbox;
	private Integer shopType;
	private Integer soNPP;
	private String resetToken;

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		superviserMgr = (SuperviserMgr) context.getBean("superviserMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		lstGsnpp = new ArrayList<StaffVO>();
		try {
			Shop shopChoose = null;
			if (currentUser != null && currentUser.getUserName() != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentUser.getShopRoot() != null) {
					if (currentUser.getShopRoot().getShopId() != null) {
						shopChoose = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
					}
				}
			}

			if (shopChoose != null) {
				shop = shopChoose;
				shopCode = shop.getShopCode();
				shopName = shop.getShopName();
			} else {
				if (staff != null && staff.getShop() != null) {
					shop = staff.getShop();
					shopCode = shop.getShopCode();
					shopName = shop.getShopName();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	/**
	 * Load thong tin khoi tao trang traningPlanPage.
	 * 
	 * @author tulv2
	 * @since 10.02.2014
	 * */
	@Override
	public String execute() {
		try {
			resetToken(result);
			lstGsnpp = new ArrayList<StaffVO>();
			if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null) {
				dateStr = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
				shopId = currentUser.getShopRoot().getShopId();

				List<Shop> shopTemp = getAllListShopByStaffLogin();
				if (shopTemp != null) {
					shopName = shopMgr.getShopById(shopId).getShopName();
					soNPP = shopTemp.size();
					result.put("soNPP", soNPP);
					result.put("shopName", shopName);
				}
			} else {
				LogUtility.logResponse("Chua dang nhap he thong", "execute()", "TranningPlanAction");
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Lay danh sach giam sat nha phan phoi truyen theo shop va thuoc quyen quan
	 * ly cua nhan vien dang nhap
	 * 
	 * @author tulv2
	 * @since 10.06.2014
	 * 
	 * @author hunglm16
	 * @since October 23,2014
	 * @description Ra soat phan quyen
	 * */
	public String getListGSNPP() {
		try {
			lstGsnpp = new ArrayList<StaffVO>();
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			if (checkShopInOrgAccessById(shopId)) {
				filter.setShopId(shopId);
			} else {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			}
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				filter.setIsLstChildStaffRoot(true);
				objVO = staffMgr.getListGSByStaffRootWithInParentStaffMap(filter);
			} else {
				filter.setIsLstChildStaffRoot(false);
				objVO = staffMgr.getListGSByStaffRootWithNVBH(filter);
			}
			if (objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
				setLstGsnpp(objVO.getLstObject());
				for (int i = 0; i < objVO.getLstObject().size(); i++) {
					lstGsnpp.get(i).setStaffName(objVO.getLstObject().get(i).getStaffCode() + " - " + objVO.getLstObject().get(i).getStaffName());
				}
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	public String search() {
		result.put("page", page);
		result.put("rows", rows);
		try {
			KPaging<TrainingPlanVO> kPaging = new KPaging<TrainingPlanVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			Shop so = shopMgr.getShopById(shopId);
			Staff s = staffMgr.getStaffById(gsnppId);
			if ((so == null || s == null) && gsnppId > 0) {
				result.put("total", 0);
				result.put("rows", new ArrayList<Staff>());
				return JSON;
			}
			boolean flagChildCms = false;
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				flagChildCms = true;
			}
			ObjectVO<TrainingPlanVO> temp = null;
			dateStr = "01/" + dateStr;
			Date date = DateUtil.parse(dateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
//			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
//				if (!checkShopInOrgAccessByIdWithIsLevel(shopId, ShopDecentralizationSTT.NPP.getValue())) { //|| so.getShopType()!=1
//					result.put("total", 0);
//					result.put("rows", new ArrayList<Staff>());
//					return JSON;
//				}
//			}
			//else {
//			}
			temp = superviserMgr.getListTrainingPlan(kPaging, shopId, gsnppId, date, currentUser.getShopRoot().getShopId(), currentUser.getStaffRoot().getStaffId(), flagChildCms);

			List<Shop> shopTemp = getAllListShopByStaffLogin();//new ArrayList<Shop>();//shopMgr.getListSubShopbyMienOrVung(shopId);
			if (shopTemp != null) {
				shopName = shopMgr.getShopById(shopId).getShopName();
				soNPP = shopTemp.size();
				result.put("soNPP", soNPP);
				result.put("shopName", shopName);
			}
			if (temp != null) {
				result.put("total", temp.getkPaging().getTotalRows());
				result.put("rows", temp.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	private List<Shop> getAllListShopByStaffLogin() throws BusinessException {
		List<Long> lstShopId = new ArrayList<Long>();
		lstShopId.add(shopId);
		List<Shop> shopTemp = shopMgr.getAllShopChildrenByLstId(lstShopId);
		return shopTemp;
	}

	public HashMap<String, Object> getBeans() {
		return beans;
	}

	public void setBeans(HashMap<String, Object> beans) {
		this.beans = beans;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getGsnppId() {
		return gsnppId;
	}

	public void setGsnppId(Long gsnppId) {
		this.gsnppId = gsnppId;
	}

	public List<TrainingPlanVO> getListCheck() {
		return listCheck;
	}

	public void setListCheck(List<TrainingPlanVO> listCheck) {
		this.listCheck = listCheck;
	}

	public List<TrainingPlanVO> getLstTrainingPlan() {
		return lstTrainingPlan;
	}

	public void setLstTrainingPlan(List<TrainingPlanVO> lstTrainingPlan) {
		this.lstTrainingPlan = lstTrainingPlan;
	}

	/**
	 * 
	 */
	/*
	 * private void createData() { // TODO Auto-generated method stub
	 * lstTrainingPlan = new ArrayList<TrainingPlanVO>(); for (int i = 0; i <
	 * 10; i++) { TrainingPlanVO add = new TrainingPlanVO();
	 * add.setCodeGSNPP("VNM_" + i); add.setNameGSNPP("Vinamilk_" + i);
	 * add.setNameNVBH("SaleMan_VNM_" + i); add.setCodeNVBH("SaleMan_" + i);
	 * add.setCodeNPP("VNM_Admin"); add.setId(new Long(i + 1));
	 * lstTrainingPlan.add(add); } }
	 */

	public String export() {
		return JSON;
	}

	public String checkOnExcel(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			if (StringUtil.isNullOrEmpty(row1.get(0))) {
				message = ValidateUtil.validateField(row1.get(0), "ss.traningplan.date.code", null, ConstantManager.ERR_REQUIRE);
				if (StringUtil.isNullOrEmpty(row1.get(1))) {
					message += ValidateUtil.validateField(row1.get(1), "ss.traningplan.npp.code", null, ConstantManager.ERR_REQUIRE);
					if (StringUtil.isNullOrEmpty(row1.get(2))) {
						message += ValidateUtil.validateField(row1.get(2), "ss.traningplan.gsnpp.code", null, ConstantManager.ERR_REQUIRE);
						if (StringUtil.isNullOrEmpty(row1.get(3))) {
							message += ValidateUtil.validateField(row1.get(3), "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE);
							return message; // 4 truong deu rong
						}
						return message; // 3 truong deu rong
					}
					if (StringUtil.isNullOrEmpty(row1.get(3))) {
						message += ValidateUtil.validateField(row1.get(3), "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE);
						return message; // 3 truong deu rong
					}
					return message; // 2 truong deu rong
				}
				if (StringUtil.isNullOrEmpty(row1.get(2))) {
					message += ValidateUtil.validateField(row1.get(2), "ss.traningplan.gsnpp.code", null, ConstantManager.ERR_REQUIRE);
					if (StringUtil.isNullOrEmpty(row1.get(3))) {
						message += ValidateUtil.validateField(row1.get(3), "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE);
						return message; // 3 truong deu rong
					}
					return message; //2 truong deu rong
				}
				if (StringUtil.isNullOrEmpty(row1.get(3))) {
					message += ValidateUtil.validateField(row1.get(3), "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE);
					return message; //2 truong deu rong
				}
				return message; // 1 truong deu rong
			}
			if (StringUtil.isNullOrEmpty(row1.get(1))) {
				message += ValidateUtil.validateField(row1.get(1), "ss.traningplan.npp.code", null, ConstantManager.ERR_REQUIRE);
				if (StringUtil.isNullOrEmpty(row1.get(2))) {
					message += ValidateUtil.validateField(row1.get(2), "ss.traningplan.gsnpp.code", null, ConstantManager.ERR_REQUIRE);
					if (StringUtil.isNullOrEmpty(row1.get(3))) {
						message += ValidateUtil.validateField(row1.get(3), "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE);
						return message; // 3 truong rong
					}
					return message; // 2 truong rong
				}
				if (StringUtil.isNullOrEmpty(row1.get(3))) {
					message += ValidateUtil.validateField(row1.get(3), "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE);
					return message; // 2 truong rong
				}
				return message; // 1 truong rong
			}
			if (StringUtil.isNullOrEmpty(row1.get(2))) {
				message += ValidateUtil.validateField(row1.get(2), "ss.traningplan.gsnpp.code", null, ConstantManager.ERR_REQUIRE);
				if (StringUtil.isNullOrEmpty(row1.get(3))) {
					message += ValidateUtil.validateField(row1.get(3), "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE);
					return message; // 2 truong rong
				}
				return message; // 1 truong rong
			}
			if (StringUtil.isNullOrEmpty(row1.get(3))) {
				message += ValidateUtil.validateField(row1.get(3), "ss.traningplan.salestaff.code", null, ConstantManager.ERR_REQUIRE);
				return message; // 1 truong rong
			}
			//message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).equals(row2.get(0)) && row1.get(1).equals(row2.get(1)) && row1.get(2).equals(row2.get(2))) {
						if (row1.get(3).equals(row2.get(3))) {
							if (StringUtil.isNullOrEmpty(message)) {
								message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
							} else {
								message += ", " + (k + 2);
							}
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
			}
			if (StringUtil.isNullOrEmpty(message)) {
				for (int k = 0; k < lstData.size(); k++) {
					if (lstData.get(k) != null && k != j) {
						List<String> row2 = lstData.get(k);
						if (row1.get(0).equalsIgnoreCase(row2.get(0)) && row1.get(1).equalsIgnoreCase(row2.get(1)) && row1.get(2).equalsIgnoreCase(row2.get(2))) {
							if (!row1.get(3).equalsIgnoreCase(row2.get(3))) {
								if (StringUtil.isNullOrEmpty(message)) {
									message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
								} else {
									message += ", " + (k + 2);
								}
							}
						}
					}
				}
				if (!StringUtil.isNullOrEmpty(message)) {
					message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.xay.ra.truong.hop");
				}
			}
		}
		return message;
	}

	/**
	 * Lay danh sach giam sat
	 * 
	 * @author hunglm16
	 * @since October 23,2014
	 * @return the string
	 */
	private List<StaffVO> getListStaffGS() {
		try {
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			// Dieu kien loc nhan vien theo nha phan phoi va nhan vien dang nhap
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			if (checkShopInOrgAccessById(shopId)) {
				filter.setShopId(shopId);
			} else {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			}
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				filter.setIsLstChildStaffRoot(true);
				objVO = staffMgr.getListGSByStaffRootWithInParentStaffMap(filter);
			} else {
				filter.setIsLstChildStaffRoot(false);
				objVO = staffMgr.getListGSByStaffRootWithNVBH(filter);
			}

			if (objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
				return objVO.getLstObject();
			}

			return new ArrayList<StaffVO>();

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return null;
	}

	/**
	 * Lay danh sach NVBH
	 * 
	 * @author hunglm16
	 * @since October 23,2014
	 * @return the string
	 */
	private List<StaffVO> getListStaffNVBH(Integer... objectType) {
		try {
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				filter.setIsLstChildStaffRoot(true);
			} else {
				filter.setIsLstChildStaffRoot(false);
			}
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			List<Long> lstShopId = new ArrayList<Long>();
			lstShopId.add(currentUser.getShopRoot().getShopId());
			filter.setLstShopId(lstShopId);
			List<Integer> lstOType = new ArrayList<Integer>();
			for (Integer ojbType : objectType) {
				lstOType.add(ojbType);
			}
			if (lstOType.size() > 0) {
				filter.setLstObjectType(lstOType);
			} else {
				return new ArrayList<StaffVO>();
			}
			ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
			if (objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
				return objVO.getLstObject();
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return new ArrayList<StaffVO>();
	}

	/**
	 * Nhap bieu mau huan luyen tu file excel.
	 * 
	 * @author vuongmq
	 * @modify tulv2
	 * @since 10.06.2014
	 * */
	public String importExcelTrainingPlan() {
		/*List<StaffVO> listStaffGSInCMS = getListStaffGS();
		List<StaffVO> listStaffNVBHInCMS = getListStaffNVBH(StaffObjectType.NVVS.getValue(), StaffObjectType.NVBH.getValue());
		if (listStaffGSInCMS == null || listStaffGSInCMS.isEmpty()) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.lst.staff.gs");
			return ERROR;
		}
		if (listStaffNVBHInCMS == null || listStaffNVBHInCMS.isEmpty()) {
			errMsg = "Không xác định được các NVBH thuộc vai trò đăng nhập";
			return ERROR;
		}
		isError = true;
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 4);
		if ((lstData == null || lstData.size() == 0) && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.nodata");
			return ERROR;
		}
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				Date t = commonMgr.getSysDate();
				totalItem = 0;
				lstFails = new ArrayList<CellBean>();
				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						message = "";
						totalItem++;
						List<String> row = lstData.get(i);
						Date dateExcel = null;
						Shop shopExcel = null;
						Staff staffExcel = null;
						Staff staffSaleExcel = null;
						String checkMessage1 = checkOnExcel(lstData, i);
						if (StringUtil.isNullOrEmpty(checkMessage1)) {
							if (row.size() > 0) {
								String value = row.get(0);
								String checkMessage = ValidateUtil.validateField(value, "ss.traningplan.date.code", null, ConstantManager.ERR_REQUIRE);
								if (StringUtil.isNullOrEmpty(checkMessage)) {
									checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(value, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.traningplan.date.code"), null);
									if (StringUtil.isNullOrEmpty(checkMessage)) {
										dateExcel = DateUtil.parse(value, DateUtil.DATE_FORMAT_STR);
										if (DateUtil.compareDateWithoutDay(dateExcel, DateUtil.now()) < 0) {
											checkMessage = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.month.not.after.sysmonth") + "\n";
										}
										if (!StringUtil.isNullOrEmpty(checkMessage)) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_SS_GREATER_DATENOW_GENERAL, true, "ss.traningplan.date.code.month");
										}
									} else {
										message += checkMessage;
									}
								} else {
									message += checkMessage;
								}
							}
							// Ma NPP
							if (StringUtil.isNullOrEmpty(message)) {
								if (row.size() > 1) {
									String value = row.get(1);
									String checkMessage = ValidateUtil.validateField(value, "ss.traningplan.npp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (StringUtil.isNullOrEmpty(checkMessage)) {
										shopExcel = shopMgr.getShopByCode(value);
										if (shopExcel == null) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "ss.traningplan.npp.code");
										} else {
											if (!checkShopInOrgAccessById(shopExcel.getId())) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.invalidShop");
											} else if (!checkShopInOrgAccessByIdWithIsLevel(shopExcel.getId(), ShopDecentralizationSTT.NPP.getValue())) { // kt dang nhap GS moi kiem tra shop chon, con VNM thi cho import het
												message += "Đơn vị không phải là một NPP";
											}
										}
									} else {
										message += checkMessage;
									}
								}
							}
							// Ma GSNPP
							if (StringUtil.isNullOrEmpty(message)) {
								if (row.size() > 2) {
									String value = row.get(2);
									String checkMessage = ValidateUtil.validateField(value, "ss.traningplan.gsnpp.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (StringUtil.isNullOrEmpty(checkMessage)) {
										staffExcel = staffMgr.getStaffByCode(value);
										if (staffExcel == null) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "ss.traningplan.gsnpp.code");
										} else {
											if (!ActiveType.RUNNING.getValue().equals(staffExcel.getStatus().getValue())) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.GSNPP.isrunning.undefined") + "\n";
											} else {
												boolean flagStaffCode = false;
												for (StaffVO voStaff : listStaffNVBHInCMS) {
													if (voStaff.getShopCode().trim().toLowerCase().equals(value.trim().toLowerCase())) {
														flagStaffCode = false;
														break;
													}
												}
												if (flagStaffCode) {
													message += "Mã GSNPP không được xác định với vai trò cho tài khoản đang sử dụng \n";
												}
											}
										}
									} else {
										message += checkMessage;
									}
								}
							}
							// Ma NVBH
							if (StringUtil.isNullOrEmpty(message)) {
								if (row.size() > 3) {
									String value = row.get(3);
									String checkMessage = ValidateUtil.validateField(value, "ss.traningplan.salestaff.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (StringUtil.isNullOrEmpty(checkMessage)) {
										staffSaleExcel = staffMgr.getStaffByCode(value);
										if (staffSaleExcel == null) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "ss.traningplan.salestaff.code");
										} else {
											if (staff.getStaffType().getObjectType() == 5) {
												if (!shop.getId().equals(staffSaleExcel.getShop().getId())) {
													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_SS_NOT_BELONG_GENERAL, true, "ss.traningplan.salestaff.code", "ss.traningplan.npp.code");
												}
											}
											//ERR_SS_NOT_BELONG_GENERAL
											if (!staffSaleExcel.getStatus().equals(ActiveType.RUNNING)) {
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "ss.traningplan.salestaff.code");
											} else {
												if (staffSaleExcel.getStaffType().getObjectType() == null || staffSaleExcel.getStaffType().getObjectType() != 1 && staffSaleExcel.getStaffType().getObjectType() != 2) { // check NVBH neu khong fai presale, vansale thi bao loi 
													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_SS_NOT_GENERAL, true, "ss.traningplan.salestaff.code", "ss.traningplan.salestaff");
												} else if (shopExcel != null && !staffSaleExcel.getShop().getId().equals(shopExcel.getId())) {
													message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_SS_TRANINGPLAN_NOT_SAME_SHOP_STAFFSALE, true);
												} else {
													boolean flagStaffNVBH = false;
													for (StaffVO voStaff : listStaffGSInCMS) {
														if (voStaff.getShopCode().trim().toLowerCase().equals(value.trim().toLowerCase())) {
															flagStaffNVBH = false;
															break;
														}
													}
													if (flagStaffNVBH) {
														message += "Mã GSNPP không được xác định với vai trò cho tài khoản đang sử dụng \n";
													} else if (staffExcel != null && !staffMgr.checkParentChildrentByDoubleStaffId(staffExcel.getId(), staffSaleExcel.getId())) {
														//Kiem tra phan quyen du lieu cha con
														message += "Mã GSNPP không được phân cấp quyền quản lý NVBH tương ứng \n";
													}
												}
											}
										}
									} else {
										message += checkMessage;
									}
								}
							}
							//
							if (StringUtil.isNullOrEmpty(message)) {
								TrainingPlan trainingPlan = superviserMgr.getTrainingPlan(staffExcel.getStaffCode(), shopExcel.getId(), dateExcel);
								if (trainingPlan == null) {
									superviserMgr.createTrainingPlanDetailByOwnerIdAndStaffId(shopExcel, staffExcel.getId(), staffSaleExcel.getId(), dateExcel);
								} else {
									if (trainingPlan.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
										TrainingPlanDetail trainingPlanDetail = superviserMgr.getTrainingPlanDetail(staffExcel.getStaffCode(), dateExcel);
										if (trainingPlanDetail == null) {
											trainingPlanDetail = new TrainingPlanDetail();
											trainingPlanDetail.setShop(shopExcel);
											trainingPlanDetail.setStaff(staffSaleExcel);
											trainingPlanDetail.setStatus(TrainningPlanDetailStatus.TRAINED);
											trainingPlanDetail.setTrainingDate(dateExcel);
											trainingPlanDetail.setTrainingPlan(trainingPlan);
											trainingPlanDetail.setCreateDate(t);
											superviserMgr.createTrainingPlanDetail(trainingPlanDetail, getLogInfoVO());
										} else {
											if (trainingPlanDetail.getTrainingPlan().getId().equals(trainingPlan.getId())) {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.exists");
											} else {
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.exists1");
											}
											lstFails.add(StringUtil.addFailBean(row, message));
										}
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.err.hethan") + "\n";
										lstFails.add(StringUtil.addFailBean(row, message));
									}
								}
							} else {
								lstFails.add(StringUtil.addFailBean(row, message));
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, checkMessage1));
						}
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_SUPERVISESHOP_MANAGE_TRAININGPLAN_GSNPP);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				return ERROR;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}*/
		return SUCCESS;
	}

	public List<StaffVO> getLstGsnpp() {
		return lstGsnpp;
	}

	public void setLstGsnpp(List<StaffVO> lstGsnpp) {
		this.lstGsnpp = lstGsnpp;
	}

	public String exportExcelTrainingPlan() {
		try {
			Staff nvgs = null;
			if (shopId != null && shopId > 0) {
				shop = shopMgr.getShopById(shopId);
				if (shop == null) {
					return JSON;
				}
			}
			if (gsnppId != null && gsnppId > 0) {
				nvgs = staffMgr.getStaffById(gsnppId);
				if (nvgs == null) {
					return JSON;
				}
			}
			boolean flagChildCms = false;
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				flagChildCms = true;
			}
			Date date = DateUtil.parse(dateStr, DateUtil.DATE_M_Y);
			ObjectVO<TrainingPlanVO> vo = new ObjectVO<TrainingPlanVO>();
			if (date != null && shop.getId() != null) {
				vo = superviserMgr.getListTrainingPlan(null, shop.getId(), gsnppId, date, currentUser.getShopRoot().getShopId(), currentUser.getStaffRoot().getStaffId(), flagChildCms);
			}
			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
				lstTrainingPlan = new ArrayList<TrainingPlanVO>();
				lstTrainingPlan = vo.getLstObject();
				beans = new HashMap<String, Object>();
				beans.put("lst", lstTrainingPlan);

				if (shop != null) {
					beans.put("shopCode", shop.getShopCode() + " - " + shop.getShopName());
				} else {
					beans.put("shopCode", "Vinamilk");
				}
				if (nvgs != null) {
					beans.put("nvgs", nvgs.getStaffCode() + " - " + nvgs.getStaffName());
				} else {
					beans.put("nvgs", "GSNPP");
				}
				SimpleDateFormat format = new SimpleDateFormat("MM/yyyy");
				dateStr = format.format(date);
				beans.put("date", dateStr);
				String outputDownload = ReportUtils.exportExcelJxls(beans, ShopReportTemplate.QLHL, FileExtension.XLS);
				result.put(ERROR, false);
				result.put(LIST, outputDownload);
				result.put(REPORT_PATH, outputDownload);
				result.put("hasData", true);
			} else {
				result.put("hasData", false);
			}

		} catch (Exception e) {
			LogUtility.logError(e, "TrainingPlanAction.exportExcelTrainingPlan(): " + e.getMessage());
			result.put(ERROR, true);
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return JSON;
	}

	public String delPlan() throws BusinessException {
		resetToken(result);
		try {
			boolean flagChildCms = false;
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				flagChildCms = true;
			}
			List<Long> lstTrainList = new ArrayList<Long>();
			if (pressCheckbox) {
				dateStr = "01/" + dateStr;
				Date date = DateUtil.parse(dateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
				ObjectVO<TrainingPlanVO> temp = superviserMgr.getListTrainingPlan(null, shopId, gsnppId, date, currentUser.getShopRoot().getShopId(), currentUser.getStaffRoot().getStaffId(), flagChildCms);
				if (temp != null) {
					lstDelete = new ArrayList<Long>();
					for (int i = 0; i < temp.getLstObject().size(); i++) {
						TrainingPlanVO voTmp = temp.getLstObject().get(i);
						if (voTmp.getTrainingDate() != null && DateUtil.compareTwoDate(voTmp.getTrainingDate(), DateUtil.now()) > -1) {
							lstDelete.add(voTmp.getId());
						}
						if (!lstTrainList.contains(voTmp.getTrainingPlanId()))
							lstTrainList.add(voTmp.getTrainingPlanId());
					}
				}
			}
//			List<Long> lstResult = new ArrayList<Long>(); //danh sach dong co lich huan luyen
			List<Long> lstDeleteTmp = new ArrayList<Long>();
//			boolean checkR = false;
			for (int i = 0; i < lstDelete.size(); i++) {
				/*
				 * if (superviserMgr.checkIsExistResult(lstDelete.get(i))){
				 * lstResult.add(lstDelete.get(i)); checkR = true; } else {
				 * lstDeleteTmp.add(lstDelete.get(i)); }
				 */
				lstDeleteTmp.add(lstDelete.get(i));
			}
			superviserMgr.deleteListTrainingPlanDetail(lstDeleteTmp, lstTrainList);
//			if (checkR) {
//				result.put(ERROR, true);
//				if (pressCheckbox) {
//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.isExistResultAll"));
//					result.put("deleteAll", true);
//				} else {
//					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.isExistResult"));
//					result.put("deleteAll", false);
//				}
//				result.put("lst", lstResult);
//				result.put("hastraining", true);
//			} else {
//				result.put(ERROR, false);
//			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "TrainingPlanAction.delPlan():" + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return JSON;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstDelete(List<Long> lstDelete) {
		this.lstDelete = lstDelete;
	}

	public List<Long> getLstDelete() {
		return lstDelete;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setPressCheckbox(boolean pressCheckbox) {
		this.pressCheckbox = pressCheckbox;
	}

	public boolean isPressCheckbox() {
		return pressCheckbox;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setSoNPP(Integer soNPP) {
		this.soNPP = soNPP;
	}

	public Integer getSoNPP() {
		return soNPP;
	}

	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}

	public String getResetToken() {
		return resetToken;
	}
}

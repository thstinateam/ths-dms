package ths.dms.web.action.superviseshop;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.RoutingCustomerVO;
import ths.dms.core.entities.vo.StaffVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class ManageRouteSetOrderAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1175816561529955961L;
	private StaffMgr staffMgr;
	private SuperviserMgr superMgr;
	private CommonMgr commonMgr;
	private ShopMgr shopMgr;

	private Integer saleDate;
	private List<RoutingCustomer> listRoutingCustomer;
	private List<Integer> lstSeq;
	private List<String> lstShortCode;

	private List<Long> lstRoutingCustomerId;

	private Shop chooseShop;
	private Long routingId;
	private Routing routing;

	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		superMgr = (SuperviserMgr) context.getBean("superviserMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			if (routingId != null) {
				routing = superMgr.getRoutingById(routingId);
				if (routing == null) {
					return PAGE_NOT_FOUND;
				}
			} else {
				return PAGE_NOT_FOUND;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Hien thi chi tiet Tuyen
	 * 
	 * @author hunglm16
	 * @since October 7,2014
	 * @description Cap nhat pha quyen CMS
	 */
	public String detail() {
		try {
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			}
			if (shop == null || !ShopSpecificType.NPP.equals(shop.getType().getSpecificType())) {
				return PAGE_NOT_PERMISSION;
			}
			if (routingId != null) {
				routing = superMgr.getRoutingById(routingId);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Search route set order.
	 * 
	 * @return the string
	 * @author hungtt
	 * @since Oct 10, 2012
	 * 
	 * @author hunglm16
	 * @since April 10, 2014
	 * @description Update
	 * 
	 * @author hunglm16
	 * @since October 7,2014
	 * @description Cap nhat pha quyen CMS
	 */
	public String searchRouteSetOrder() {
		try {
			result.put("rows", new ArrayList<RoutingCustomerVO>());
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shopTmp = shopMgr.getShopByCode(shopCode);
				if (shopTmp != null) {
					shopId = shopTmp.getId();
				}
			}
			if (shopId == null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			if (routingId != null && saleDate != null && saleDate > 0 && saleDate < 8) {
				List<RoutingCustomerVO> listRoutingCustomerVO = superMgr.getListRoutingCustomerNew(null, routingId, ActiveType.RUNNING, saleDate, true, shopId, null, currentUser.getStaffRoot().getStaffId());
				if (listRoutingCustomerVO != null) {
					result.put("rows", listRoutingCustomerVO);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error ManageRouteSetOrderAction.searchRouteSetOrder" + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Update routing customer.
	 * 
	 * @return the string
	 * @author hungtt
	 * @since Oct 23, 2012
	 * 
	 * 
	 * Cap nhat phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since October 6,2014
	 */
	public String updateRoutingCustomer() {
		resetToken(result);
		errMsg = "";
		try {
			if (shopId == null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			BasicFilter<StaffVO> filter = new BasicFilter<StaffVO>();
			filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
			List<Long> lstLongS = new ArrayList<Long>();
			lstLongS.add(shopId);
			filter.setArrLongS(lstLongS);
			if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
				filter.setParentId(currentUser.getStaffRoot().getStaffId());
			}
			chooseShop = shopMgr.getShopById(shopId);
			superMgr.updateListRoutingCustomerByListShortCode(lstShortCode, lstSeq, chooseShop, saleDate, getCurrentUser().getUserName(), routingId);

		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "Error ManageRouteSetOrder.updateRoutingCustomer: " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}

	////////////////////////////@: GETTER-SETTER :@/////////////////////////////////////////
	private Long shopId;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public SuperviserMgr getSuperMgr() {
		return superMgr;
	}

	public void setSuperMgr(SuperviserMgr superMgr) {
		this.superMgr = superMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public CommonMgr getCommonMgr() {
		return commonMgr;
	}

	public void setCommonMgr(CommonMgr commonMgr) {
		this.commonMgr = commonMgr;
	}

	public Integer getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(Integer saleDate) {
		this.saleDate = saleDate;
	}

	public List<RoutingCustomer> getListRoutingCustomer() {
		return listRoutingCustomer;
	}

	public void setListRoutingCustomer(List<RoutingCustomer> listRoutingCustomer) {
		this.listRoutingCustomer = listRoutingCustomer;
	}

	public Long getRoutingId() {
		return routingId;
	}

	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}

	public Routing getRouting() {
		return routing;
	}

	public void setRouting(Routing routing) {
		this.routing = routing;
	}

	public List<Integer> getLstSeq() {
		return lstSeq;
	}

	public void setLstSeq(List<Integer> lstSeq) {
		this.lstSeq = lstSeq;
	}

	public List<Long> getLstRoutingCustomerId() {
		return lstRoutingCustomerId;
	}

	public void setLstRoutingCustomerId(List<Long> lstRoutingCustomerId) {
		this.lstRoutingCustomerId = lstRoutingCustomerId;
	}

	public List<String> getLstShortCode() {
		return lstShortCode;
	}

	public void setLstShortCode(List<String> lstShortCode) {
		this.lstShortCode = lstShortCode;
	}
}

package ths.dms.web.action.superviseshop;

import java.util.Date;
import java.util.List;

import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.VisitPlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class ManageSalePlanAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3112326637639439857L;

	private StaffMgr staffMgr;
	private ShopMgr shopMgr;
	private SuperviserMgr superviserMgr;

	private Staff staff;
	private Shop shop;

	private String shopCode;
	private String staffSaleCode;
	private String monthPlan;
	private List<VisitPlan> lstVisitPlan;

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public SuperviserMgr getSuperviserMgr() {
		return superviserMgr;
	}

	public void setSuperviserMgr(SuperviserMgr superviserMgr) {
		this.superviserMgr = superviserMgr;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getStaffSaleCode() {
		return staffSaleCode;
	}

	public void setStaffSaleCode(String staffSaleCode) {
		this.staffSaleCode = staffSaleCode;
	}

	public String getMonthPlan() {
		return monthPlan;
	}

	public void setMonthPlan(String monthPlan) {
		this.monthPlan = monthPlan;
	}

	public List<VisitPlan> getLstVisitPlan() {
		return lstVisitPlan;
	}

	public void setLstVisitPlan(List<VisitPlan> lstVisitPlan) {
		this.lstVisitPlan = lstVisitPlan;
	}

	public boolean checkExistShopOfUserLogin() {
		if (currentUser != null && currentUser.getUserName() != null) {
			try {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
			} catch (BusinessException e) {
				LogUtility.logError(e, e.getMessage());
				return false;
			}
			if (staff != null && staff.getShop() != null) {
				shop = staff.getShop();
			}
		}
		if (shop == null)
			return false;
		else
			return true;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		superviserMgr = (SuperviserMgr) context.getBean("superviserMgr");

	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			// CHECK QUYEN VA TON TAI DON VI USER DANG NHAP
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop) {
				monthPlan = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_M_Y);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Gets the info. Ham getInfo() su dung de tim kiem danh sach khach hang cua
	 * NVBH trong thang
	 * 
	 * @param :shopCode(Ma don vi-ko bat buoc) & staffSaleCode (Ma NVBH-bat
	 *        buoc)& monthPlan (Thang can xem-bat buoc)
	 * @since Nov 7, 2012
	 * @author thongnm1
	 */
	public String getInfo() {
		result.put("page", page);
		result.put("max", max);
		errMsg = "";
		try {
			// CHECK QUYEN
			//CHECK TON TAI DON VI USER DANG NHAP
			boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP);
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return ERROR;
			}
			if (StringUtil.isNullOrEmpty(staffSaleCode)) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "ss.traningplan.salestaff.code");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return ERROR;
			}
			if (StringUtil.isNullOrEmpty(monthPlan)) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "ss.traningplan.monthplan.code");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return ERROR;
			}

			Staff staffTmp = staffMgr.getStaffByCode(staffSaleCode);
			if (staffTmp == null) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "ss.traningplan.salestaff.code");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return ERROR;
			}
			Date monthPlanDate = DateUtil.parse(monthPlan, DateUtil.DATE_M_Y);
			if (monthPlanDate == null) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID_FORMAT, false, "ss.traningplan.monthplan.code");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return ERROR;
			}
			KPaging<VisitPlan> kPaging = new KPaging<VisitPlan>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			ObjectVO<VisitPlan> lstVisitPlanVO = null;
			if (StringUtil.isNullOrEmpty(shopCode) || shopCode.equalsIgnoreCase(shop.getShopCode())) {
				lstVisitPlanVO = superviserMgr.getListVisitPlanForManagerPlan(kPaging, staffTmp.getShop().getId(), staffTmp.getId(), DateUtil.getMonth(monthPlanDate), DateUtil.getYear(monthPlanDate), ActiveType.RUNNING);
			} else {
				Shop shopTmp = shopMgr.getShopByCode(shopCode);
				if (shopTmp == null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "ss.traningplan.shop.code");
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return ERROR;
				} else {
					if (!checkShopInOrgAccessByCode(shopCode)) {
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_SS_NOT_BELONG_SHOPUSER, false, "ss.traningplan.shop.code");
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return ERROR;
					}
					lstVisitPlanVO = superviserMgr.getListVisitPlanForManagerPlan(kPaging, shopTmp.getId(), staffTmp.getId(), DateUtil.getMonth(monthPlanDate), DateUtil.getYear(monthPlanDate), ActiveType.RUNNING);
				}
			}
			if (lstVisitPlanVO != null) {
				result.put("total", lstVisitPlanVO.getkPaging().getTotalRows());
				result.put("rows", lstVisitPlanVO.getLstObject());
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, "Error ManageSalePlanAction.getInfo: " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			return ERROR;
		}
		return SUCCESS;
	}
}

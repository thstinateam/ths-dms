package ths.dms.web.action.superviseshop;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;
import org.hibernate.sql.Update;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.VisitPlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.RoutingCustomerType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.RoutingGeneralFilter;
import ths.dms.core.entities.vo.ErrorVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.RoutingCustomerVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.memcached.MemcachedUtils;

/**
 * Thiet lap Tuyen
 * 
 * @author hunglm16
 * @since 2013
 * @description update theo CMS
 * @since October 6,2014
 */
public class ManageRouteCreateAction extends AbstractAction {

	private static final long serialVersionUID = 8545089141689758868L;

	private SuperviserMgr superviserMgr;
	private CustomerMgr customerMgr;
	private ShopMgr shopMgr;
	private SuperviserMgr superMgr;
	private CommonMgr commonMgr;

	private List<RoutingCustomer> lstRoutingCustomer;
	private List<RoutingCustomerVO> lstRoutingCustomerVO;
	private List<Staff> lstSaleStaff;
	private List<ErrorVO> lstError;
	private List<RoutingVO> listRouting;
	private List<CellBean> lstView;
	private List<ShopVO> listShopVO;
	private List<Long> lstExceptionCus;
	private List<Long> lstId;
	private List<Long> lstRoutingCustomerEditDelete;
	private List<String> lstRoutingCustomerAdd;
	private List<String> lstRoutingCustomerEdit;

	private File excelFile;
	private Shop shop;
	private Staff staff;
	private Customer customer;
	private Routing route;

	private Boolean typeView;

	private Long routeId;
	private Long routingCustomerId;

	private Integer status;
	private Integer isView;
	private Integer staffRoleType;

	private String shortCode;
	private String sysdateStr;
	private String yesterdateStr;
	private String backdateStr;
	private String customerName;
	private String lstDeleteStr;
	private String strExceptionCus;
	private String address;
	private String lstIdStr;
	private String fDateStr;
	private String tDateStr;
	private String startDateStr;
	private String endDateStr;
	private String excelFileContentType;
	private String customerCode;
	private String staffSaleCode;
	private String shopCode;
	private String routingCode;
	private String routingName;
	private String shortCodeDialog;
	private String shopCodeDialog;
	private String customerNameDialog;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		superviserMgr = (SuperviserMgr) context.getBean("superviserMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		superMgr = (SuperviserMgr) context.getBean("superviserMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			// CHECK QUYEN VA TON TAI DON VI CUA USER DANG NHAP
			RoutingGeneralFilter<RoutingVO> filter = new RoutingGeneralFilter<RoutingVO>();
			filter.setShopId(currentUser.getShopRoot().getShopId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			listRouting = superviserMgr.searchListRoutingVOByFilter(filter).getLstObject();
		} catch (Exception e) {
			LogUtility.logError(e, "Error ManageRouteCreateAction");
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem tuyen
	 * 
	 * @return the string
	 * @author hunglm16
	 * @since October 6, 2014
	 */
	public String searchRoute() {
		result.put("total", 0);
		result.put("rows", new ArrayList<RoutingVO>());
		try {
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shopTmp = shopMgr.getShopByCode(shopCode);
				if (shopTmp != null) {
					shopId = shopTmp.getId();
				}
			}
			if (shopId == null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			RoutingGeneralFilter<RoutingVO> filter = new RoutingGeneralFilter<RoutingVO>();
			filter.setShopId(shopId);
			KPaging<RoutingVO> kPaging = new KPaging<RoutingVO>();
			kPaging.setPage(page - 1);
			kPaging.setPageSize(max);
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(staffSaleCode)) {
				filter.setStaffCode(staffSaleCode);
			}
			if (!StringUtil.isNullOrEmpty(routingCode)) {
				filter.setRoutingCode(routingCode);
			}
			if (!StringUtil.isNullOrEmpty(routingName)) {
				filter.setRoutingName(routingName);
			}
			if (status != null) {
				ActiveType statusTemp = ActiveType.parseValue(status);
				if (statusTemp != null) {
					filter.setStatus(status);
				}
			}
			ObjectVO<RoutingVO> lstVisitPlanVO = new ObjectVO<RoutingVO>();
			lstVisitPlanVO = superviserMgr.searchListRoutingVOByFilter(filter);
			if (lstVisitPlanVO != null) {
				result.put("total", lstVisitPlanVO.getkPaging().getTotalRows());
				result.put("rows", lstVisitPlanVO.getLstObject());
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "Error ManageRouteAssignAction.searchRoute" + e.getMessage());
			return JSON;
		}
		return JSON;
	}

	/**
	 * update Routing New
	 * 
	 * @author hunglm16
	 * @since March 25, 2014
	 * 
	 * Cap nhat CMS
	 * @author hunglm16
	 * @since October 6,2014
	 */
	public String editRoute() {
		resetToken(result);
		try {
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			}
			if (shop == null || !ShopSpecificType.NPP.equals(shop.getType().getSpecificType())) {
				return PAGE_NOT_PERMISSION;
			}
			shopId = shop.getId();
			sysdateStr = DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
			yesterdateStr = DateUtil.toDateString(commonMgr.getDateBySysdateForNumberDay(-1, true), DateUtil.DATE_FORMAT_DDMMYYYY);
			route = superviserMgr.getRoutingById(routeId);
			if (route == null || !shopId.equals(route.getShop().getId())) {
				return PAGE_NOT_PERMISSION;
			}
			RoutingGeneralFilter<RoutingVO> filter = new RoutingGeneralFilter<RoutingVO>();
			filter.setShopId(shopId);
			filter.setStatus(ActiveType.RUNNING.getValue());
			listRouting = superviserMgr.searchListRoutingVOByFilter(filter).getLstObject();
			if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
				lstSaleStaff = superMgr.getListNVBHInTuyenNew(currentUser.getStaffRoot().getStaffId(), route.getShop().getId());
			} else {
				lstSaleStaff = superMgr.getListNVBHInTuyenNew(null, route.getShop().getId());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Them moi Routing
	 * 
	 * @author hunglm16
	 * @since April 1, 2014
	 * 
	 * @exception Update
	 * @author hunglm16
	 * @since October 6,2014
	 * @description Cap nhat phan quyen CMS
	 */
	public String addRoute() {
		resetToken(result);
		String result = SUCCESS;
		try {
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					shopId = shop.getId();
				} else {
					return PAGE_NOT_PERMISSION;
				}
			} else {
				return PAGE_NOT_PERMISSION;
			}
			RoutingGeneralFilter<RoutingVO> filter = new RoutingGeneralFilter<RoutingVO>();
			filter.setShopId(shopId);
			listRouting = superviserMgr.searchListRoutingVOByFilter(filter).getLstObject();
			// Lay danh sach la NPP
			BasicFilter<ShopVO> filterS = new BasicFilter<ShopVO>();
			filterS.setLongG(currentUser.getShopRoot().getShopId());
			filterS.setStatus(ActiveType.RUNNING.getValue());
			filterS.setShopType(ShopSpecificType.NPP);
			listShopVO = shopMgr.getListShopVOBasicByFilter(filterS).getLstObject();
			// Lay danh sach RoutingCustomer thuoc hien tai va tuong lai
			lstRoutingCustomerVO = superviserMgr.getListRusCusByEndateNextSysDate();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return result;
	}

	/**
	 * Delete the route.
	 * 
	 * @return the string
	 * @author thongnm
	 * @since Oct 19, 2012
	 * 
	 * @author hunglm16
	 * @since October 9,214
	 * @description Cap nhat phan quyen CMS
	 */
	public String deleteRoute() {
		resetToken(result);
		errMsg = "";
		try {
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shopTmp = shopMgr.getShopByCode(shopCode);
				if (shopTmp != null) {
					shopId = shopTmp.getId();
				}
			}
			if (shopId == null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			if (routeId == null) {
				errMsg = R.getResource("ss.create.route.undefined.for.delete");
			} else {
				Routing routing = superviserMgr.getRoutingById(routeId);
				if (routing == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT);
				} else {
					if (!ActiveType.RUNNING.equals(routing.getStatus())) {
						errMsg = R.getResource("ss.create.route.not.running", routing.getRoutingCode());
					} else if (!shopId.equals(routing.getShop().getId())) {
						//Kiem tra Tuyen co thuoc Quyen quan ly hay khong
						errMsg = R.getResource("ss.create.route.not.belong.shop", StringUtil.isNullOrEmpty(shopCode) ? currentUser.getShopRoot().getShopCode() : shopCode);
					} else {
						//Kiem tra xem da duoc giao tuyen cho NV hay chua
						if (!superviserMgr.checkRoutingErrDelete(routeId, shopId)) {
							errMsg = R.getResource("ss.create.route.assigned.nvbh.now");
						} else {
							superviserMgr.offRouting(routing.getId(), currentUser.getUserName());
						}
					}
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		} else {
			result.put(ERROR, false);
		}
		return JSON;
	}

	/**
	 * Search customer.
	 * 
	 * @return the string
	 * @author ThongNM
	 * @since Oct 15, 2012
	 * 
	 *        Update CMS
	 * 
	 * @author hunglm16
	 * @since October 7,2014
	 */
	public String searchCustomer() {
		try {
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					shopId = shop.getId();
				} else {
					return PAGE_NOT_PERMISSION;
				}
			} else {
				return PAGE_NOT_PERMISSION;
			}
			KPaging<Customer> kPaging = new KPaging<Customer>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			String shortCodeTmp = null;
			String customerNameTmp = null;
			if (!StringUtil.isNullOrEmpty(shortCodeDialog)) {
				shortCodeTmp = shortCodeDialog;
			}
			if (!StringUtil.isNullOrEmpty(customerNameDialog)) {
				customerNameTmp = customerNameDialog;
			}
			Date startDate = null;
			Date endDate = null;
			if (!StringUtil.isNullOrEmpty(startDateStr)) {
				startDate = DateUtil.parse(startDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (!StringUtil.isNullOrEmpty(endDateStr)) {
				endDate = DateUtil.parse(endDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			ObjectVO<RoutingCustomerVO> lstRoutingCustomerVOData = superviserMgr.getListRoutingCustomerByCusOrRouting(routeId, shopId, null, shortCode, customerName, address, startDate, endDate, status);
			if(lstRoutingCustomerVOData != null && lstRoutingCustomerVOData.getLstObject() != null && lstRoutingCustomerVOData.getLstObject().size() > 0) {
				lstExceptionCus = new ArrayList<Long>();
				for (RoutingCustomerVO routingCustomerVO : lstRoutingCustomerVOData.getLstObject()) {
					lstExceptionCus.add(routingCustomerVO.getCustomerId());
				}
			}
			/*if (!StringUtil.isNullOrEmpty(strExceptionCus)) {
				String[] str = strExceptionCus.split(",");
				lstExceptionCus = new ArrayList<Long>();
				for (String s : str) {
					lstExceptionCus.add(Long.valueOf(s));
				}
			}*/
			ObjectVO<Customer> lstCustomer = superviserMgr.getListCustomerForOnlyShop(kPaging, shortCodeTmp, customerNameTmp, shopCode, lstExceptionCus);
			if (lstCustomer != null) {
				result.put("total", lstCustomer.getkPaging().getTotalRows());
				result.put("rows", lstCustomer.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error ManageTransStockAction.searchCustomer" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * update
	 * 
	 * @author hunglm16
	 * @since April 2, 2014
	 * 
	 * @author hunglm16
	 * @since October 7,2014
	 * @description Cap nhat CMS
	 */
	public String addOrSaveRouting() {
		resetToken(result);
		errMsg = "";
		try {
			boolean isEditOrDelete = false;
			boolean flag = false;
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
				if (shop != null && ShopSpecificType.NPP.equals(shop.getType().getSpecificType())) {
					flag = true;
				}
			}
			if (!flag) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.cms.shop.undefined"));
				return JSON;
			}

			Date startDate = null;
			Date endDate = null;
			Date yesterDate = commonMgr.getDateBySysdateForNumberDay(-1, true);
			if (routeId != null && routeId > 0) {
				Shop shopUpdate = shopMgr.getShopById(shopId);
				Routing routing = superviserMgr.getRoutingById(routeId);
				errMsg = ValidateUtil.getErrorMsgOfSpecialCharInName(routingName, "ss.routingname");
				if (!errMsg.equals("")) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				List<RoutingCustomer> listRoutingCustomerInsert = new ArrayList<RoutingCustomer>();
				List<RoutingCustomer> listRoutingCustomerUpdate = new ArrayList<RoutingCustomer>();
				List<Long> lstRouCusId = new ArrayList<Long>();
				if (!StringUtil.isNullOrEmpty(lstDeleteStr)) {
					lstDeleteStr = lstDeleteStr.replace("-1;", "");
					lstDeleteStr = lstDeleteStr.replace("-", "");
					String[] arrLstDel = lstDeleteStr.split(";");
					if (arrLstDel != null && arrLstDel.length > 0) {
						for (int i = 0; i < arrLstDel.length; i++) {
							if (!StringUtil.isNullOrEmpty(arrLstDel[i])) {
								lstRouCusId.add(Long.valueOf(arrLstDel[i]));
							}
						}
					}
					if (lstRouCusId != null && lstRouCusId.size() > 0) {
						// Lay danh sach RoutingCustomer by List Long
						List<RoutingCustomer> lstDel = superviserMgr.getListRoutingCustomerListRouCusID(lstRouCusId);
						if (lstDel != null && lstDel.size() > 0) {
							for (int i = 0; i < lstDel.size(); i++) {
								if (DateUtil.compareDateWithoutTime(lstDel.get(i).getStartDate(), yesterDate) != 1) {
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("ss.create.route.allow.delete.cust.from.date.after.now"));
									return JSON;
								}
								lstDel.get(i).setStatus(ActiveType.DELETED);
								lstDel.get(i).setUpdateDate(new Date());
								lstDel.get(i).setUpdateUser(getCurrentUser().getUserName());
								listRoutingCustomerUpdate.add(lstDel.get(i));
								isEditOrDelete = true;
							}
						}
					}
				}
				if (lstRoutingCustomerEdit != null) {
					for (int i = 0; i < lstRoutingCustomerEdit.size(); i++) {
						startDate = null;
						endDate = null;
						String[] routingCustomerString = lstRoutingCustomerEdit.get(i).split(";");
						Long routingCustomerId = Long.parseLong(routingCustomerString[15].toString());
						RoutingCustomer routingCustomer = superviserMgr.getRoutingCustomerById(routingCustomerId);
						if (!routingCustomer.getRouting().getId().equals(routing.getId())) {
							result.put(ERROR, true);
							errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT);
							result.put("errMsg", errMsg);
							return JSON;
						}
						Integer monday = 0, tuesday = 0, wednesday = 0, thursday = 0, friday = 0, saturday = 0, sunday = 0, weekInterval = 1, isDel = 0, week1 = 0, week2 = 0, week3 = 0, week4 = 0;
						if (!StringUtil.isNullOrEmpty(routingCustomerString[1]) && !"K".equals(routingCustomerString[1].trim().toUpperCase())) {
							monday = Integer.parseInt(routingCustomerString[1].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[2]) && !"K".equals(routingCustomerString[2].trim().toUpperCase())) {
							tuesday = Integer.parseInt(routingCustomerString[2].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[3]) && !"K".equals(routingCustomerString[3].trim().toUpperCase())) {
							wednesday = Integer.parseInt(routingCustomerString[3].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[4]) && !"K".equals(routingCustomerString[4].trim().toUpperCase())) {
							thursday = Integer.parseInt(routingCustomerString[4].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[5]) && !"K".equals(routingCustomerString[5].trim().toUpperCase())) {
							friday = Integer.parseInt(routingCustomerString[5].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[6]) && !"K".equals(routingCustomerString[6].trim().toUpperCase())) {
							saturday = Integer.parseInt(routingCustomerString[6].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[7]) && !"K".equals(routingCustomerString[7].trim().toUpperCase())) {
							sunday = Integer.parseInt(routingCustomerString[7].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[20]) && !"K".equals(routingCustomerString[20].trim().toUpperCase())) {
							isDel = Integer.parseInt(routingCustomerString[20].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[23]) && !"K".equals(routingCustomerString[23].trim().toUpperCase())) {
							week1 = Integer.parseInt(routingCustomerString[23].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[24]) && !"K".equals(routingCustomerString[24].trim().toUpperCase())) {
							week2 = Integer.parseInt(routingCustomerString[24].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[25]) && !"K".equals(routingCustomerString[25].trim().toUpperCase())) {
							week3 = Integer.parseInt(routingCustomerString[25].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[26]) && !"K".equals(routingCustomerString[26].trim().toUpperCase())) {
							week4 = Integer.parseInt(routingCustomerString[26].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[17])) {
							// Check co phai kieu ngay hay khong
							if (commonMgr.checkValidDateDate(routingCustomerString[17].trim()).intValueExact() != 1) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("ss.create.route.cust.from.date.invalid", routingCustomer.getCustomer().getShortCode()));
								return JSON;
							}
							startDate = DateUtil.parse(routingCustomerString[17], DateUtil.DATE_FORMAT_DDMMYYYY);
							if (startDate != null) {
								// Kiem tra voi ngay hien tai
								if (DateUtil.compareDateWithoutTime(routingCustomer.getStartDate(), yesterDate) != 1) {
									// So sanh phai lon hon hoac bang ngay hom
									// nay
									if (DateUtil.compareDateWithoutTime(startDate, routingCustomer.getStartDate()) != 0) {
										result.put(ERROR, true);
										result.put("errMsg", R.getResource("ss.create.route.cust.from.date.not.edit", routingCustomer.getCustomer().getShortCode()));
										return JSON;
									}
								} else {
									if (!StringUtil.isNullOrEmpty(routingCustomerString[22]) && routingCustomerString[22].trim().toLowerCase().equals("coppy")) {
										if (DateUtil.compareDateWithoutTime(startDate, yesterDate) == -1) {
											result.put(ERROR, true);
											result.put("errMsg", R.getResource("ss.create.route.cust.from.date.after.yesterday", routingCustomer.getCustomer().getShortCode()));
											return JSON;
										}
									} else if (DateUtil.compareDateWithoutTime(startDate, yesterDate) != 1) {
										result.put(ERROR, true);
										result.put("errMsg", R.getResource("ss.create.route.cust.from.date.after.now", routingCustomer.getCustomer().getShortCode()));
										return JSON;
									}
								}
							}
						} else {
							// Tu ngay khong duoc phep de trong
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("imp.tuyen.clmn.error.tuNgay.null"));
							return JSON;
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[18]) && !"K".equals(routingCustomerString[18].toUpperCase())) {
							// Check co phai kieu ngay hay khong
							if (commonMgr.checkValidDateDate(routingCustomerString[18].trim()).intValueExact() != 1) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("ss.create.route.cust.to.date.invalid", routingCustomer.getCustomer().getShortCode()));
								return JSON;
							}
							endDate = DateUtil.parse(routingCustomerString[18], DateUtil.DATE_FORMAT_DDMMYYYY);
							if (endDate != null) {
								// Kiem tra voi ngay hien tai
								if (!StringUtil.isNullOrEmpty(routingCustomerString[22]) && routingCustomerString[22].trim().toLowerCase().equals("coppy")) {
									// Den ngay phai lon hon hoac bang ngay hom
									// qua
									if (DateUtil.compareDateWithoutTime(endDate, commonMgr.getDateBySysdateForNumberDay(-1, true)) == -1) {
										result.put(ERROR, true);
										result.put("errMsg", R.getResource("ss.create.route.cust.to.date.coppy.after.yesterday"));
										return JSON;
									}
								} else if (DateUtil.compareDateWithoutTime(endDate, yesterDate) != 1) {
									// Den ngay phai lon hon hoac bang ngay hom
									// nay
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("ss.create.route.cust.to.date.after.now", routingCustomer.getCustomer().getShortCode()));
									return JSON;
								}
							}
						}
						// lay ngay tuong lai gan nhat neu co tuyen trong tuong
						// lai
						RoutingCustomerFilter filter = new RoutingCustomerFilter();
						filter.setToDate(endDate);

						if (isDel == 1) {
							routingCustomer.setMonday(RoutingCustomerType.parseValue(monday));
							routingCustomer.setTuesday(RoutingCustomerType.parseValue(tuesday));
							routingCustomer.setWednesday(RoutingCustomerType.parseValue(wednesday));
							routingCustomer.setThursday(RoutingCustomerType.parseValue(thursday));
							routingCustomer.setFriday(RoutingCustomerType.parseValue(friday));
							routingCustomer.setSaturday(RoutingCustomerType.parseValue(saturday));
							routingCustomer.setSunday(RoutingCustomerType.parseValue(sunday));
							routingCustomer.setWeekInterval(weekInterval);
							routingCustomer.setStartDate(startDate);
							routingCustomer.setWeek1(RoutingCustomerType.parseValue(week1));
							routingCustomer.setWeek2(RoutingCustomerType.parseValue(week2));
							routingCustomer.setWeek3(RoutingCustomerType.parseValue(week3));
							routingCustomer.setWeek4(RoutingCustomerType.parseValue(week4));
						}
						routingCustomer.setEndDate(endDate);
						routingCustomer.setStatus(ActiveType.RUNNING);
						routingCustomer.setUpdateDate(commonMgr.getSysDate());
						routingCustomer.setUpdateUser(getCurrentUser().getUserName());
						listRoutingCustomerUpdate.add(routingCustomer);
						routingCustomer.setWeekInterval(weekInterval);
						isEditOrDelete = true;
					}
				}
				if (isEditOrDelete) {
					superviserMgr.updateRoutingCustomer(null, listRoutingCustomerUpdate, null);
				}

				if (lstRoutingCustomerAdd != null) {
					for (int i = 0; i < lstRoutingCustomerAdd.size(); i++) {
						startDate = null;
						endDate = null;
						String[] routingCustomerString = lstRoutingCustomerAdd.get(i).split(";");
						String shortCode = routingCustomerString[11].toString();
						Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopUpdate.getShopCode(), shortCode));
						if (customer == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, shortCode));
							return JSON;
						}
						if (!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, shortCode));
							return JSON;
						}
						Integer monday = 0, tuesday = 0, wednesday = 0, thursday = 0, friday = 0, saturday = 0, sunday = 0, week1 = 0, week2 = 0, week3 = 0, week4 = 0;
						if (!StringUtil.isNullOrEmpty(routingCustomerString[1]) && !"K".equals(routingCustomerString[1].trim().toUpperCase())) {
							monday = Integer.parseInt(routingCustomerString[1].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[2]) && !"K".equals(routingCustomerString[2].trim().toUpperCase())) {
							tuesday = Integer.parseInt(routingCustomerString[2].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[3]) && !"K".equals(routingCustomerString[3].trim().toUpperCase())) {
							wednesday = Integer.parseInt(routingCustomerString[3].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[4]) && !"K".equals(routingCustomerString[4].trim().toUpperCase())) {
							thursday = Integer.parseInt(routingCustomerString[4].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[5]) && !"K".equals(routingCustomerString[5].trim().toUpperCase())) {
							friday = Integer.parseInt(routingCustomerString[5].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[6]) && !"K".equals(routingCustomerString[6].trim().toUpperCase())) {
							saturday = Integer.parseInt(routingCustomerString[6].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[7]) && !"K".equals(routingCustomerString[7].trim().toUpperCase())) {
							sunday = Integer.parseInt(routingCustomerString[7].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[23]) && !"K".equals(routingCustomerString[23].trim().toUpperCase())) {
							week1 = Integer.parseInt(routingCustomerString[23].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[24]) && !"K".equals(routingCustomerString[24].trim().toUpperCase())) {
							week2 = Integer.parseInt(routingCustomerString[24].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[25]) && !"K".equals(routingCustomerString[25].trim().toUpperCase())) {
							week3 = Integer.parseInt(routingCustomerString[25].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[26]) && !"K".equals(routingCustomerString[26].trim().toUpperCase())) {
							week4 = Integer.parseInt(routingCustomerString[26].toString());
						}

						if (!StringUtil.isNullOrEmpty(routingCustomerString[17])) {
							// Check co phai kieu ngay hay khong
							if (commonMgr.checkValidDateDate(routingCustomerString[17].trim()).intValueExact() != 1) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("ss.create.route.cust.from.date.invalid", customer.getShortCode()));
								return JSON;
							}
							startDate = DateUtil.parse(routingCustomerString[17], DateUtil.DATE_FORMAT_DDMMYYYY);
							if (startDate != null) {
								// Kiem tra voi ngay hien tai
								if (DateUtil.compareDateWithoutTime(startDate, yesterDate) != 1) {
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("ss.create.route.cust.from.date.after.now", customer.getShortCode()));
									return JSON;
								}
							}
						} else {
							// Tu ngay khong duoc phep de trong
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("imp.tuyen.clmn.error.tuNgay.null"));
							return JSON;
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[18]) && !"K".equals(routingCustomerString[18].toUpperCase())) {
							// Check co phai kieu ngay hay khong
							if (commonMgr.checkValidDateDate(routingCustomerString[18].trim()).intValueExact() != 1) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("ss.create.route.cust.to.date.invalid", customer.getShortCode()));
								return JSON;
							}
							endDate = DateUtil.parse(routingCustomerString[18], DateUtil.DATE_FORMAT_DDMMYYYY);
							if (endDate != null) {
								if (DateUtil.compareDateWithoutTime(endDate, startDate) == -1) {
									// Den ngay phai lon hon hoac bang ngay hom
									// nay
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("ss.create.route.cust.from.date.before.to.date", customer.getShortCode()));
									return JSON;
								} else if (DateUtil.compareDateWithoutTime(endDate, yesterDate) != 1) {
									// Den ngay phai lon hon hoac bang ngay hom
									// nay
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("ss.create.route.cust.to.date.after.now", customer.getShortCode()));
									return JSON;
								}
							}
						}

						// Kiem tra co bi trung voi khach hang khac duoi DB hay
						// khong
						RoutingCustomer routingCustomer = new RoutingCustomer();
						routingCustomer.setMonday(RoutingCustomerType.parseValue(monday));
						routingCustomer.setTuesday(RoutingCustomerType.parseValue(tuesday));
						routingCustomer.setWednesday(RoutingCustomerType.parseValue(wednesday));
						routingCustomer.setThursday(RoutingCustomerType.parseValue(thursday));
						routingCustomer.setFriday(RoutingCustomerType.parseValue(friday));
						routingCustomer.setSaturday(RoutingCustomerType.parseValue(saturday));
						routingCustomer.setSunday(RoutingCustomerType.parseValue(sunday));
						routingCustomer.setWeekInterval(1);
						routingCustomer.setStartDate(startDate);
						routingCustomer.setEndDate(endDate);
						routingCustomer.setStatus(ActiveType.RUNNING);
						routingCustomer.setCreateUser(getCurrentUser().getUserName());
						routingCustomer.setRouting(routing);
						routingCustomer.setCustomer(customer);
						listRoutingCustomerInsert.add(routingCustomer);
						routingCustomer.setWeek1(RoutingCustomerType.parseValue(week1));
						routingCustomer.setWeek2(RoutingCustomerType.parseValue(week2));
						routingCustomer.setWeek3(RoutingCustomerType.parseValue(week3));
						routingCustomer.setWeek4(RoutingCustomerType.parseValue(week4));
					}
					superviserMgr.updateRoutingCustomer(null, null, listRoutingCustomerInsert);
				}
				result.put(LIST, "/superviseshop/manageroute-create/editroute?shopId=" + shopId + "&routeId=" + routing.getId());
			} else {
				Shop shopAdd = shopMgr.getShopById(shopId);
				shopCode = shopAdd.getShopCode();
				if (StringUtil.isNullOrEmpty(routingCode)) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("ss.create.route.add.save.routingcode.is.null"));
					return JSON;
				}
				Routing routingTest = superviserMgr.getRoutingByCode(shopAdd.getId(), routingCode);
				if (routingTest != null) {
					result.put(ERROR, true);
					result.put("errorType", 1);
					result.put("errMsg", R.getResource("ss.create.route.add.save.routing.exits"));
					return JSON;
				}
				if (currentUser != null && currentUser.getUserName() != null) {
					Staff staffCheck = staffMgr.getStaffByCode(currentUser.getUserName());
					if (staffCheck != null && staffCheck.getShop().getShopCode() != null) {
						if (StringUtil.isNullOrEmpty(shopCode)) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("ss.create.route.add.save.shop.is.null"));
							return JSON;
						}
						errMsg = ValidateUtil.getErrorMsgOfSpecialCharInCode(routingCode, "ss.routingcode");
						if (!errMsg.equals("")) {
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						errMsg = ValidateUtil.getErrorMsgOfSpecialCharInName(routingName, "ss.routingname");
						if (!errMsg.equals("")) {
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
							return JSON;
						}
						List<RoutingCustomer> listRoutingCustomer = new ArrayList<RoutingCustomer>();
						if (lstRoutingCustomerAdd != null) {
							for (int i = 0; i < lstRoutingCustomerAdd.size(); i++) {
								String[] routingCustomerString = lstRoutingCustomerAdd.get(i).split(";");
								String shortCode = routingCustomerString[11].toString();
								Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, shortCode));
								if (customer == null) {
									result.put(ERROR, true);
									result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, shortCode));
									return JSON;
								}
								if (!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
									result.put(ERROR, true);
									result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_INACTIVE, shortCode));
									return JSON;
								}
								Integer monday = 0, tuesday = 0, wednesday = 0, thursday = 0, friday = 0, saturday = 0, sunday = 0, weekInterval = 1, startWeek = 1;
								if (!StringUtil.isNullOrEmpty(routingCustomerString[1])) {
									monday = Integer.parseInt(routingCustomerString[1].toString());
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[2])) {
									tuesday = Integer.parseInt(routingCustomerString[2].toString());
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[3])) {
									wednesday = Integer.parseInt(routingCustomerString[3].toString());
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[4])) {
									thursday = Integer.parseInt(routingCustomerString[4].toString());
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[5])) {
									friday = Integer.parseInt(routingCustomerString[5].toString());
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[6])) {
									saturday = Integer.parseInt(routingCustomerString[6].toString());
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[7])) {
									sunday = Integer.parseInt(routingCustomerString[7].toString());
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[8])) {
									weekInterval = Integer.parseInt(routingCustomerString[8].toString());
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[9])) {
									startWeek = Integer.parseInt(routingCustomerString[9].toString());
								}
								if (weekInterval == -1) {
									weekInterval = null;
								} else {
									if (weekInterval <= 0 || weekInterval >= 100) {
										result.put(ERROR, true);
										result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SEQ_MAX_CREATE_ROUTE, R.getResource("ss.assign.route.set.order.error.seq.max")));
										return JSON;
									}
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[17])) {
									startDate = DateUtil.parse(routingCustomerString[17], DateUtil.DATE_FORMAT_DDMMYYYY);
									if (startDate != null) {
										// So sanh phai lon hon hoac bang ngay
										// hom nay
										if (DateUtil.compareDateWithoutTime(startDate, commonMgr.getDateBySysdateForNumberDay(0, true)) == -1) {
											result.put(ERROR, true);
											result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_START_WEEK_MAX_CREATE_ROUTE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
													"ss.assign.route.set.order.error.startDate.isTuongLai")));
											return JSON;
										}
									}
								} else {
									// Tu ngay khong duoc phep de trong
									result.put(ERROR, true);
									result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_START_WEEK_MAX_CREATE_ROUTE, R.getResource("imp.tuyen.clmn.error.tuNgay.null")));
									return JSON;
								}
								if (!StringUtil.isNullOrEmpty(routingCustomerString[18])) {
									endDate = DateUtil.parse(routingCustomerString[18], DateUtil.DATE_FORMAT_DDMMYYYY);
									if (endDate != null) {
										// Den ngay phai lon hon hoac bang ngay
										// hom nay
										if (DateUtil.compareDateWithoutTime(endDate, commonMgr.getDateBySysdateForNumberDay(0, true)) == -1) {
											result.put(ERROR, true);
											result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_START_WEEK_MAX_CREATE_ROUTE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
													"ss.assign.route.set.order.error.endDate.isNotQuaKhu")));
											return JSON;
										}
									}
								}
								// Validate truong ngay & customer trong routing
								// customer
								RoutingCustomer routingCustomer = new RoutingCustomer();
								routingCustomer.setMonday(RoutingCustomerType.parseValue(monday));
								routingCustomer.setTuesday(RoutingCustomerType.parseValue(tuesday));
								routingCustomer.setWednesday(RoutingCustomerType.parseValue(wednesday));
								routingCustomer.setThursday(RoutingCustomerType.parseValue(thursday));
								routingCustomer.setFriday(RoutingCustomerType.parseValue(friday));
								routingCustomer.setSaturday(RoutingCustomerType.parseValue(saturday));
								routingCustomer.setSunday(RoutingCustomerType.parseValue(sunday));
								routingCustomer.setWeekInterval(weekInterval);
								// routingCustomer.setStartWeek(startWeek);
								routingCustomer.setStartDate(startDate);
								routingCustomer.setEndDate(endDate);
								routingCustomer.setStatus(ActiveType.RUNNING);
								routingCustomer.setCreateUser(getCurrentUser().getUserName());
								routingCustomer.setCustomer(customer);
								listRoutingCustomer.add(routingCustomer);
							}
						}
						if (listRoutingCustomer != null && listRoutingCustomer.size() > 0) {
							Routing routing01 = new Routing();
							routing01.setCreateUser(getCurrentUser().getUserName());
							routing01.setStatus(ActiveType.RUNNING);
							routing01.setShop(shopAdd);
							routing01.setRoutingCode(routingCode.toUpperCase());
							routing01.setRoutingName(routingName);
							superviserMgr.createRoutingWithEntity(routing01, listRoutingCustomer);
							listRouting = superviserMgr.getListVisitPlanByCondition(null, null, staff.getId(), ActiveType.RUNNING, null, null, null, null).getLstObject();
							result.put("tree", listRouting);
							Routing resultRoute = superviserMgr.getRoutingByCode(shopAdd.getId(), routingCode);
							result.put(LIST, "/superviseshop/manageroute-create/editroute?shopId=" + shopId + "&routeId=" + resultRoute.getId());
						}
					}
				}

			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * Tao moi Tuyen
	 * 
	 * @author hunglm16
	 * @since April 15, 2014
	 * 
	 * Tich hop CMS
	 * @author hunglm16
	 * @since October 6,2014
	 * */
	public String creatRoutingNew() {
		resetToken(result);
		errMsg = "";
		try {
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					shopId = shop.getId();
				}
			}
			if (shopId == null) {
				errMsg = R.getResource("ss.create.route.add.save.routingcode.is.null");
			}
			if (errMsg.trim().length() == 0 && StringUtil.isNullOrEmpty(routingCode)) {
				errMsg = R.getResource("ss.create.route.add.save.routingcode.is.null");
			} else if (errMsg.trim().length() == 0) {
				errMsg = ValidateUtil.getErrorMsgOfSpecialCharInCode(routingCode, "ss.routingcode");
			}

			if (errMsg.trim().length() == 0 && StringUtil.isNullOrEmpty(routingName)) {
				errMsg = R.getResource("ss.create.route.add.save.routingname.is.null");
			} else if (errMsg.trim().length() == 0) {
				errMsg = ValidateUtil.getErrorMsgOfSpecialCharInName(routingName, "ss.routingname");
			}

			if (errMsg.trim().length() == 0) {
				shopCode = shop.getShopCode();
			} else {
				errMsg = R.getResource("ss.create.route.add.save.shop.is.null");
			}

			if (errMsg.trim().length() > 0) {
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}

			Routing routingTest = superviserMgr.getRoutingByCode(shop.getId(), routingCode);
			if (routingTest != null) {
				result.put(ERROR, true);
				result.put("errorType", 1);
				result.put("errMsg", R.getResource("ss.create.route.add.save.routing.exits"));
				return JSON;
			}
			Routing routing01 = new Routing();
			routing01.setCreateUser(currentUser.getUserName());
			routing01.setStatus(ActiveType.RUNNING);
			routing01.setShop(shop);
			routing01.setRoutingCode(routingCode.toUpperCase());
			routing01.setRoutingName(routingName);
			routing01.setCreateDate(new Date());
			Routing resultRoute = superviserMgr.createRoutingWithEntityNew(routing01);
			result.put(LIST, "/superviseshop/manageroute-create/editroute?routeId=" + resultRoute.getId() + "&shopCode=" + shop.getShopCode());

		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * Cap nhat tat ca cac tuyen
	 * 
	 * @author hunglm16
	 * @since April 15, 2014
	 * 
	 * Cap nhat CMS
	 * 
	 * @author hunglm16
	 * @since Octiber 6,2014
	 * */
	public String updateRoutingNew() {
		resetToken(result);
		errMsg = "";
		try {
			Shop shopUpdate = new Shop();
			if (shopId == null) {
				errMsg = R.getResource("ss.create.route.add.save.routingcode.is.null");
				result.put(ERROR, true);
				return JSON;
			}
			if (errMsg.trim().length() == 0 && StringUtil.isNullOrEmpty(routingCode)) {
				errMsg = R.getResource("ss.create.route.add.save.routingcode.is.null");
			} else if (errMsg.trim().length() == 0) {
				errMsg = ValidateUtil.getErrorMsgOfSpecialCharInCode(routingCode, "ss.routingcode");
			}
			if (errMsg.trim().length() == 0 && StringUtil.isNullOrEmpty(routingName)) {
				errMsg = R.getResource("ss.create.route.add.save.routingname.is.null");
			} else if (errMsg.trim().length() == 0) {
				errMsg = ValidateUtil.getErrorMsgOfSpecialCharInName(routingName, "ss.routingname");
			}
			if (errMsg.trim().length() == 0) {
				// Xu ly cap nhat Tuyen
				shopUpdate = shopMgr.getShopById(shopId);
				shopCode = shopUpdate.getShopCode();
				Routing routing01 = new Routing();
				routing01 = superviserMgr.getRoutingById(routeId);
				if (routing01 == null) {
					errMsg = R.getResource("ss.create.route.khong.tim.thay.tuyen.cap.nhat");
				} else if (routing01.getShop() != null && !shopUpdate.getId().equals(routing01.getShop().getId())) {
					errMsg = R.getResource("ss.create.route.tuyen.khong.thuoc.npp");
				} else {
					routing01.setUpdateUser(getCurrentUser().getUserName());
					routing01.setRoutingName(routingName);
					routing01.setUpdateDate(new Date());
					superviserMgr.updateRouting(routing01);
					result.put(LIST, "/superviseshop/manageroute-create/editroute?routeId=" + routing01.getId());
				}
			}
			if (errMsg.trim().length() > 0) {
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * @author hunglm16
	 * @since March 19, 2014
	 * @description Export excel tuyen
	 * */
	public String exportTuyenByExcelPOI() {
		// XUAT EXCEL: Theo Dieu Kien Tim Kiem Tuyen
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			shopId = currentUser.getShopRoot().getShopId();
			Date sysdate = commonMgr.getSysDate();
			List<RoutingVO> lstData = superviserMgr.exportTuyenBySelect(shopId, routingCode, routingName, staffSaleCode, sysdate, null);

			if (lstData != null && lstData.size() > 0) {
				// Init XSSF workboook
				String outputName = ConstantManager.EXPORT_GSBH_TUYEN + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				// Tao shett
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("epx.tuyen.sheet1.nameSheet"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				ExcelPOIProcessUtils mySheet = new ExcelPOIProcessUtils();

				// Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(13);
				// set static Column width
				mySheet.setColumnsWidth(sheetData, 0, 97, 97, 142, 97, 142, 232, 79, 79, 79, 79, 79, 79, 79, 79, 109, 109);
				// Size Row
				mySheet.setRowsHeight(sheetData, 0, 40);
				// nen trang
				sheetData.setDisplayGridlines(false);

				// header
				mySheet.addCell(sheetData, 0, 0, R.getResource("imp.exp.tuyen.clmn.maNPP"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 1, 0, R.getResource("imp.epx.tuyen.clmn.maTuyen"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 2, 0, R.getResource("imp.epx.tuyen.clmn.tenTuyen"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 3, 0, R.getResource("imp.epx.tuyen.clmn.maKH"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 4, 0, R.getResource("imp.epx.tuyen.clmn.tenKH"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 5, 0, R.getResource("imp.epx.tuyen.clmn.diaChi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 6, 0, R.getResource("imp.epx.tuyen.clmn.thu2"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 7, 0, R.getResource("imp.epx.tuyen.clmn.thu3"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 8, 0, R.getResource("imp.epx.tuyen.clmn.thu4"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 9, 0, R.getResource("imp.epx.tuyen.clmn.thu5"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 10, 0, R.getResource("imp.epx.tuyen.clmn.thu6"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 11, 0, R.getResource("imp.epx.tuyen.clmn.thu7"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 12, 0, R.getResource("imp.epx.tuyen.clmn.chuNhat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 13, 0, R.getResource("imp.epx.tuyen.clmn.tanSuat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 14, 0, R.getResource("imp.epx.tuyen.clmn.tuNgay"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
				mySheet.addCell(sheetData, 15, 0, R.getResource("imp.epx.tuyen.clmn.denNgay"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));

				// Xu lý do du lieu Detail
				int d = 0;
				for (d = 0; d < lstData.size(); d++) {
					mySheet.setRowsHeight(sheetData, d + 1, 15);
					mySheet.addCell(sheetData, 0, d + 1, lstData.get(d).getShopCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 1, d + 1, lstData.get(d).getRoutingCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 2, d + 1, lstData.get(d).getRoutingName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 3, d + 1, lstData.get(d).getCustomerShortCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 4, d + 1, lstData.get(d).getCustomerName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 5, d + 1, lstData.get(d).getCustomerAddress(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 6, d + 1, lstData.get(d).getMonDayStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 7, d + 1, lstData.get(d).getTuesDayStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 8, d + 1, lstData.get(d).getWednesDayStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 9, d + 1, lstData.get(d).getThursDayStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 10, d + 1, lstData.get(d).getFirtDayStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 11, d + 1, lstData.get(d).getSaturDayStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 12, d + 1, lstData.get(d).getSunDayStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					mySheet.addCell(sheetData, 13, d + 1, lstData.get(d).getWeekInterval(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					mySheet.addCell(sheetData, 14, d + 1, lstData.get(d).getStartDateStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
					mySheet.addCell(sheetData, 15, d + 1, lstData.get(d).getEndDateStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
				}
				out = new FileOutputStream(exportFileName);
				workbook.write(out);

				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(REPORT_PATH, outputPath);
				result.put(ERROR, false);
				result.put("hasData", true);

				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				errMsg = R.getResource("epx.tuyen.sheet1.data.null");
				result.put(ERROR, true);
				result.put("data.hasData", true);
			}
		} catch (Exception e) {
			errMsg = R.getResource("common.report.error.system");
			result.put(ERROR, true);
			result.put("data.hasData", false);
			result.put("errMsg", errMsg);
			LogUtility.logError(e, e.getMessage());
		} finally {
			if (out != null) {
				IOUtils.closeQuietly(out);
			}
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * @author hunglm16
	 * @since March 19, 2014
	 * @description Export excel tuyen Teamplate
	 * */
	public String exportTuyenByExcel() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					shopId = shop.getId();
				}
			}
			if (shopId == null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			Date sysdate = commonMgr.getSysDate();
			List<RoutingVO> lstData = new ArrayList<RoutingVO>();
			if (status != null && status == 1) {
				lstData = superviserMgr.exportTuyenBySelect(shopId, routingCode, routingName, staffSaleCode, sysdate, null);
			} else {
				// Xuat theo tieu chi chon
				lstData = superviserMgr.exportTuyenBySelect(shopId, routingCode, routingName, staffSaleCode, sysdate, lstId);
			}
			if (lstData != null && lstData.size() > 0) {
				// Xu lý do du lieu Detail
				String filePath = this.exportExcelDataRouting(lstData, ConstantManager.TEMPLATE_EXPORT_TUYEN);
			} else {
				errMsg = R.getResource("common.export.excel.null");
				result.put(ERROR, true);
				result.put("data.hasData", false);
				result.put("errMsg", errMsg);
			}
		} catch (Exception e) {
			errMsg = R.getResource("common.report.error.system");
			result.put(ERROR, true);
			result.put("data.hasData", false);
			result.put("errMsg", errMsg);
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	private String exportExcelDataRouting(List<RoutingVO> lstData, String tempFileName) {
		String outputPath = "";
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (lstData != null && lstData.size() > 0) {
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathSuperviseShop() + tempFileName;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.EXPORT_GSBH_TUYEN + FileExtension.XLS.getValue();
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				Map<String, Object> params = initImportTempateHeader();
				params.put("report", lstData);
				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(REPORT_PATH, outputPath);
				result.put(ERROR, false);
				result.put("hasData", true);

				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put("hasData", false);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return outputPath;
	}

	/**
	 * @return JSON
	 * @author hunglm16
	 * @since March 25, 2014
	 * @description sear for Map
	 * */
	public String loadMapByRoutingCustomer() {
		try {
			Shop shop = shopMgr.getShopById(shopId);
			status = 1;
			ObjectVO<RoutingCustomerVO> lstRoutingCustomerVOData = superviserMgr.getListRoutingCustomerByCusOrRouting(routeId, shop.getId(), null, shortCode, customerName, address, null, null, status);
			lstRoutingCustomerVO = lstRoutingCustomerVOData.getLstObject();
			result.put("lstData", lstRoutingCustomerVO);
			result.put(ERROR, false);
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Danh sach khach hang (routingcustomer)
	 * 
	 * @return html
	 * @author hunglm16
	 * @since March 25, 2014
	 * @description editrouteSearchCustomer
	 * */
	public String editrouteSearchCustomer() {
		try {
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					shopId = shop.getId();
				}
			}
			if (shopId == null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			Date startDate = null;
			Date endDate = null;
			if (!StringUtil.isNullOrEmpty(startDateStr)) {
				startDate = DateUtil.parse(startDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (!StringUtil.isNullOrEmpty(endDateStr)) {
				endDate = DateUtil.parse(endDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (status == null) {
				status = 1;
			}
			ObjectVO<RoutingCustomerVO> lstRoutingCustomerVOData = superviserMgr.getListRoutingCustomerByCusOrRouting(routeId, shopId, null, shortCode, customerName, address, startDate, endDate, status);
			result.put("lstData", lstRoutingCustomerVOData.getLstObject());
			result.put(ERROR, false);
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * @return html
	 * @author hunglm16
	 * @since March 25, 2014
	 * @description Delete Customer to Routing Customer
	 * */
	public String editrouteDeleteCustomer() {
		try {
			shopId = currentUser.getShopRoot().getShopId();
			RoutingCustomer rouCus = new RoutingCustomer();
			if (routingCustomerId != null) {
				rouCus = superviserMgr.getRoutingCustomerById(routingCustomerId);
				if (rouCus != null) {
					rouCus.setStatus(ActiveType.DELETED);
					rouCus.setUpdateDate(new Date());
					rouCus.setUpdateUser(getCurrentUser().getUserName());
					superviserMgr.updateRoutingCustomer(rouCus);
				}
			}

			Shop shop = shopMgr.getShopById(shopId);
			Date startDate = null;
			Date endDate = null;
			if (!StringUtil.isNullOrEmpty(fDateStr)) {
				startDate = DateUtil.parse(fDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (!StringUtil.isNullOrEmpty(tDateStr)) {
				endDate = DateUtil.parse(tDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}

			ObjectVO<RoutingCustomerVO> lstRoutingCustomerVOData = superviserMgr.getListRoutingCustomerByCusOrRouting(routeId, shop.getId(), null, shortCode, customerName, address, startDate, endDate, 1);
			lstRoutingCustomerVO = lstRoutingCustomerVOData.getLstObject();
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, e.getMessage());
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * @return html
	 * @author hunglm16
	 * @since March 26, 2014
	 * @description Insert Customer to Row ting customer
	 * */
	public String editrouteInsertCustomer() {
		try {
			shopId = currentUser.getShopRoot().getShopId();
			ObjectVO<RoutingCustomerVO> lstRoutingCustomerVOData = new ObjectVO<RoutingCustomerVO>();
			List<Long> lstCustomerID = new ArrayList<Long>();
			lstError = new ArrayList<ErrorVO>();
			Shop shop = shopMgr.getShopById(shopId);
			Date startDate = null;
			Date endDate = null;
			if (!StringUtil.isNullOrEmpty(startDateStr)) {
				startDate = DateUtil.parse(startDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (!StringUtil.isNullOrEmpty(endDateStr)) {
				endDate = DateUtil.parse(endDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (!StringUtil.isNullOrEmpty(lstIdStr)) {
				String[] arrStrID = lstIdStr.split(";");
				for (int i = 0; i < arrStrID.length; i++) {
					if (!StringUtil.isNullOrEmpty(arrStrID[i])) {
						lstCustomerID.add(Long.valueOf(arrStrID[i].toString().trim()));
					}
				}
			}

			if (lstCustomerID != null && lstCustomerID.size() > 0) {
				List<RoutingCustomerVO> lstErrData = superviserMgr.getListRouCusWitFDateMinByListCustomerID(lstCustomerID, null);
				if (lstErrData != null && lstErrData.size() > 0) {
					for (RoutingCustomerVO it : lstErrData) {
						ErrorVO errItem = new ErrorVO();
						errItem.setValue(it.getShortCode());
						errItem.setValue1(it.getRoutingCode());
						errItem.setDescripton(it.getStartDateStr());
						lstError.add(errItem);
					}
				}
			}

			RoutingCustomerFilter filter = new RoutingCustomerFilter();
			filter.setRoutingId(routeId);
			filter.setUserName(getCurrentUser().getUserName());
			// Cho them moi vao he thong
			superviserMgr.insertListCusInRouCus(lstCustomerID, filter);

			lstRoutingCustomerVOData = superviserMgr.getListRoutingCustomerByCusOrRouting(routeId, shop.getId(), null, shortCode, customerName, address, startDate, endDate, 1);
			lstRoutingCustomerVO = lstRoutingCustomerVOData.getLstObject();
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, e.getMessage());
		}
		isError = false;
		return SUCCESS;
	}

	/**
	 * @return html
	 * @author hunglm16
	 * @since March 26, 2014
	 * @description Insert Customer to Row ting customer
	 * */
	public String editrouteUpdateCustomer() {
		try {
			resetToken(result);
			errMsg = "";
			shopId = currentUser.getShopRoot().getShopId();
			if (routeId != null && routeId > 0) {
				Date startDate = null;
				Date endDate = null;
				Routing routing = superviserMgr.getRoutingById(routeId);
				errMsg = ValidateUtil.getErrorMsgOfSpecialCharInName(routingName, "ss.routingname");
				if (!errMsg.equals("")) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				List<RoutingCustomer> listRoutingCustomerUpdate = new ArrayList<RoutingCustomer>();
				if (lstRoutingCustomerEdit != null) {
					for (int i = 0; i < lstRoutingCustomerEdit.size(); i++) {
						String[] routingCustomerString = lstRoutingCustomerEdit.get(i).split(";");
						Long routingCustomerId = Long.parseLong(routingCustomerString[15].toString());
						RoutingCustomer routingCustomer = superviserMgr.getRoutingCustomerById(routingCustomerId);
						if (!routingCustomer.getRouting().getId().equals(routing.getId())) {
							result.put(ERROR, true);
							errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT);
							result.put("errMsg", errMsg);
							return JSON;
						}
						Integer monday = 0, tuesday = 0, wednesday = 0, thursday = 0, friday = 0, saturday = 0, sunday = 0, weekInterval = 1, startWeek = 1;
						if (!StringUtil.isNullOrEmpty(routingCustomerString[1])) {
							monday = Integer.parseInt(routingCustomerString[1].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[2])) {
							tuesday = Integer.parseInt(routingCustomerString[2].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[3])) {
							wednesday = Integer.parseInt(routingCustomerString[3].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[4])) {
							thursday = Integer.parseInt(routingCustomerString[4].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[5])) {
							friday = Integer.parseInt(routingCustomerString[5].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[6])) {
							saturday = Integer.parseInt(routingCustomerString[6].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[7])) {
							sunday = Integer.parseInt(routingCustomerString[7].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[8])) {
							weekInterval = Integer.parseInt(routingCustomerString[8].toString());
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[9])) {
							startWeek = Integer.parseInt(routingCustomerString[9].toString());
						}
						if (weekInterval == -1) {
							weekInterval = null;
						} else {
							if (weekInterval <= 0 || weekInterval >= 100) {
								result.put(ERROR, true);
								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SEQ_MAX_CREATE_ROUTE, R.getResource("ss.assign.route.set.order.error.seq.max")));
								return JSON;
							}
						}
						if (startWeek == -1) {
							startWeek = null;
						} else {
							if (startWeek <= 0 || startWeek >= 54) {
								result.put(ERROR, true);
								result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_START_WEEK_MAX_CREATE_ROUTE, R.getResource("ss.assign.route.set.order.error.startweek.max")));
								return JSON;
							}
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[17])) {
							startDate = DateUtil.parse(routingCustomerString[17], DateUtil.DATE_FORMAT_DDMMYYYY);
						}
						if (!StringUtil.isNullOrEmpty(routingCustomerString[18])) {
							endDate = DateUtil.parse(routingCustomerString[18], DateUtil.DATE_FORMAT_DDMMYYYY);
						}
						// lay ngay tuong lai gan nhat neu co tuyen trong tuong
						// lai
						RoutingCustomerFilter filter = new RoutingCustomerFilter();
						filter.setToDate(endDate);
						Date startMinNextSysDate = null;
						if (endDate != null) {
							startMinNextSysDate = superviserMgr.getMinStartDateByFilter(filter);
						}
						routingCustomer.setMonday(RoutingCustomerType.parseValue(monday));
						routingCustomer.setTuesday(RoutingCustomerType.parseValue(tuesday));
						routingCustomer.setWednesday(RoutingCustomerType.parseValue(wednesday));
						routingCustomer.setThursday(RoutingCustomerType.parseValue(thursday));
						routingCustomer.setFriday(RoutingCustomerType.parseValue(friday));
						routingCustomer.setSaturday(RoutingCustomerType.parseValue(saturday));
						routingCustomer.setSunday(RoutingCustomerType.parseValue(sunday));
						routingCustomer.setWeekInterval(weekInterval);
						// routingCustomer.setStartWeek(startWeek);
						routingCustomer.setStatus(ActiveType.RUNNING);
						routingCustomer.setStartDate(startDate);
						routingCustomer.setEndDate(startMinNextSysDate);
						routingCustomer.setUpdateDate(commonMgr.getSysDate());
						routingCustomer.setUpdateUser(currentUser.getUserName());
						listRoutingCustomerUpdate.add(routingCustomer);
					}
					routing.setUpdateUser(currentUser.getUserName());
					routing.setUpdateDate(commonMgr.getSysDate());
					routing.setStatus(ActiveType.RUNNING);
					routing.setRoutingName(routingName);
					superviserMgr.updateRouting(routing, null, listRoutingCustomerUpdate, null);
					result.put(LIST, "/superviseshop/manageroute-create/editroute?routeId=" + routing.getId());
					ObjectVO<RoutingCustomerVO> lstRoutingCustomerVOData = new ObjectVO<RoutingCustomerVO>();
					lstRoutingCustomerVOData = superviserMgr.getListRoutingCustomerByCusOrRouting(routeId, shop.getId(), null, shortCode, customerName, address, startDate, endDate, 1);
					lstRoutingCustomerVO = lstRoutingCustomerVOData.getLstObject();
				}
			}
		} catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, e.getMessage());
		}
		isError = false;
		return SUCCESS;
	}

	private final Integer MAX_LENGTH_ROUTE_CODE = 100;

	/**
	 * Import Tuyen
	 * 
	 * @author hunglm16
	 * @since March 18, 2014
	 * @description Import Tuyen by Excel
	 * */
	public String importTuyenByExcel() {
		resetToken(result);
		shopId = currentUser.getShopRoot().getShopId();
		// do import excel
		isError = true;
		totalItem = 0;
		lstView = new ArrayList<CellBean>();
		typeView = true;
		String message = "";
		String msg;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 28);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 2) {
			try {
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					shop = shopMgr.getShopByCode(shopCode);
				}
				if (shop == null) {
					shop = shopMgr.getShopById(shopId);
				}
				// Khai bao thuoc tinh chung
				RoutingCustomer routingCus = new RoutingCustomer();
				VisitPlan visitPlan = new VisitPlan();
				List<Staff> lstStaff = new ArrayList<Staff>();
				Routing routingNew = new Routing();
				int flagRowtingNew = 0;
				boolean flagNgay = false;
				boolean flagCKGT = false;
				int countStaffCode = 0;
				int flagTTGT = 0;
				int countSTTGT = 1;
				Date fDate = null;
				Date tDate = null;
				Customer cusNew = new Customer();
				Date sysDate = commonMgr.getDateBySysdateForNumberDay(0, true);
				List<Long> lstShopChild = getListShopChildId();
				for (int i = 2; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						routingCus = new RoutingCustomer();
						visitPlan = new VisitPlan();
						flagRowtingNew = 0;// Mac dinh rong
						countStaffCode = 0;
						flagTTGT = 0;
						message = "";
						fDate = null;
						tDate = null;
						totalItem++;
						Shop sTmp = null;
						Integer tansuat = null;
						List<String> row = lstData.get(i);
						if (row != null && row.size() >= 24) {
							if (row.size() > C_SHOPCODE) {//Ma NPP
								String value = row.get(C_SHOPCODE);
								msg = ValidateUtil.validateField(value, "catalog.customer.import.shopcode", 40, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (StringUtil.isNullOrEmpty(msg)) {
									sTmp = shopMgr.getShopByCode(value);
									if (sTmp == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.shop.code");
									} else {
										if (lstShopChild != null && !lstShopChild.contains(sTmp.getId())) {
											message += ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_AREA);
										} else if (!ShopSpecificType.NPP.equals(sTmp.getType().getSpecificType())) {
											message += R.getResource("common.cms.shop.islevel5.undefined");
										} else if (!ActiveType.RUNNING.equals(sTmp.getStatus())) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
										}
									}
								} else {
									message += msg;
								}
							}

							msg = "";
							if (StringUtil.isNullOrEmpty(message) && sTmp != null) {
								String value = row.get(C_ROUTINGCODE).trim();
								if (value.length() > MAX_LENGTH_ROUTE_CODE) {
									msg = R.getResource("imp.tuyen.route.code.max.length", MAX_LENGTH_ROUTE_CODE.toString());
								} else {
									//Ma tuyen
									msg = ValidateUtil.validateField(value, "imp.epx.tuyen.clmn.maTuyen", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
									if (StringUtil.isNullOrEmpty(msg)) {
										routingNew = superviserMgr.getRoutingMultiChoice(null, sTmp.getId(), value, null);
										if (routingNew != null && routingNew.getStatus() != null) {
											if (ActiveType.STOPPED.getValue().equals(routingNew.getStatus().getValue())) {
												msg = R.getResource("imp.tuyen.clmn.error.maTuyen.stop") + "\n";
											} else {
												flagRowtingNew = 1;// update routing
											}
										} else {
											if (!StringUtil.isNullOrEmpty(row.get(C_ROUTINGNAME))) {
												flagRowtingNew = 2;// insert routing
											} else {
												msg = R.getResource("imp.tuyen.clmn.error.maTuyen.undefined.tenTuyen.null");
												msg = msg.trim() + "\n";
											}
										}
									} 
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							// Ten tuyen
							msg = "";
							if (StringUtil.isNullOrEmpty(message)) {
								if (!StringUtil.isNullOrEmpty(row.get(C_ROUTINGNAME))) {
									msg = ValidateUtil.validateField(row.get(C_ROUTINGNAME).trim(), "imp.epx.tuyen.clmn.tenTuyen", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									}
								}
							}
							// Ma NVBH
							msg = "";
							if (StringUtil.isNullOrEmpty(message) && sTmp != null) {
								msg = ValidateUtil.validateField(row.get(C_NVBH).trim(), "imp.epx.tuyen.clmn.maNBH", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_NEW);
								if (!StringUtil.isNullOrEmpty(row.get(C_NVBH))) {
									lstStaff = staffMgr.getStaffMultilChoice(null, row.get(C_NVBH), sTmp.getId(), null);
									if (lstStaff != null && lstStaff.size() > 0) {
										if (super.getMapUserId().get(lstStaff.get(0).getId()) == null) {
											msg = R.getResource("catalog.staff.code.permission1") + "\n";
										} else if (!ActiveType.RUNNING.equals(lstStaff.get(0).getStatus())) {
											msg = R.getResource("imp.tuyen.clmn.error.maNVBH.stop") + "\n";
										} else {
											if (!StaffSpecificType.STAFF.equals(lstStaff.get(0).getStaffType().getSpecificType())) {
												msg = R.getResource("imp.tuyen.clmn.error.maNVBH.notNVBH") + "\n";
											} else {
												// Kiem tra khong co dong trung
												for (int j = 2; j <= i - 1; j++) {
													List<String> rowJ = lstData.get(j);
													if (rowJ != null && rowJ.size() >= 21 && this.isDuplicateRow(row, rowJ, null) && !StringUtil.isNullOrEmpty(row.get(C_NVBH)) && !StringUtil.isNullOrEmpty(rowJ.get(C_NVBH))
															&& !row.get(C_NVBH).toUpperCase().equals(rowJ.get(C_NVBH).toUpperCase())) {
														msg = R.getResource("imp.tuyen.clmn.exist.nvbh.in.line", String.valueOf(j - 1));
														break;
													}
												}
											}
										}
									} else {
										msg = R.getResource("imp.tuyen.clmn.error.maNVBH.undefined") + "\n";
									}
								} else {
									msg = R.getResource("imp.tuyen.clmn.error.maNVBH.null") + "\n";
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							msg = "";
							if (StringUtil.isNullOrEmpty(message) && sTmp != null) {
								// Ma KH
								String value = row.get(C_CUSTOMERCODE).trim();
								msg = ValidateUtil.validateField(value, "imp.epx.tuyen.clmn.maKH", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_NEW);
								if (!StringUtil.isNullOrEmpty(value)) {
									cusNew = customerMgr.getCustomerMultiChoine(null, sTmp.getId(), value, null);
									if (cusNew != null) {
										if (!ActiveType.RUNNING.equals(cusNew.getStatus())) {
											msg = R.getResource("imp.tuyen.clmn.error.maKH.stop") + "\n";
										} else {
											// Kiem tra khong co dong trung
											for (int j = 2; j <= i - 1; j++) {
												List<String> rowJ = lstData.get(j);
												if (rowJ != null && rowJ.size() >= 21 && this.isDuplicateRow(row, rowJ, C_CUSTOMERCODE)) {
													msg = R.getResource("imp.tuyen.clmn.duplicate.line.and.line", String.valueOf(i - 2), String.valueOf(j - 2));
													break;
												}
											}
										}
									} else {
										msg = R.getResource("imp.tuyen.clmn.error.maKH.notInShop") + "\n";
									}
								} else {
									msg = R.getResource("imp.tuyen.clmn.error.maKH.null") + "\n";
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}

							msg = "";
							if (StringUtil.isNullOrEmpty(message)) {
								// Từ ngày
								if (!StringUtil.isNullOrEmpty(row.get(C_FROMDATE))) {
									String value = row.get(C_FROMDATE).trim();
									msg = ValidateUtil.validateField(value, "imp.epx.tuyen.clmn.tuNgay", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										if (commonMgr.checkValidDateDate(value).intValueExact() > 0) {
											fDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
										}
										if (fDate == null) {
											msg = R.getResource("imp.tuyen.clmn.error.tuNgay.undefinedDate") + "\n";
										} else {
											// Kiem tra voi ngay hom nay
											if (DateUtil.compareDateWithoutTime(fDate, DateUtil.now(DateUtil.DATE_FORMAT_DDMMYYYY)) < 0) {
												msg = R.getResource("imp.tuyen.clmn.error.tuNgay.backSysDate") + "\n";
											}
										}
									}
								} else {
									msg = R.getResource("imp.tuyen.clmn.error.tuNgay.null") + "\n";
								}
								if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
									message += msg;
								}
								
								msg = "";
								if (StringUtil.isNullOrEmpty(message)) {
									// Đến ngày
									if (!StringUtil.isNullOrEmpty(row.get(C_TODATE))) {
										String value = row.get(C_TODATE).trim();
										msg = ValidateUtil.validateField(value, "imp.epx.tuyen.clmn.denNgay", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
										if (!StringUtil.isNullOrEmpty(msg)) {
											message += msg;
										} else {
											if (commonMgr.checkValidDateDate(value).intValueExact() > 0) {
												tDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
											}
											if (tDate == null) {
												msg = R.getResource("imp.tuyen.clmn.error.denNgay.undefinedDate") + "\n";
											} else {
												//Kiem tra voi ngay hom nay
												if (DateUtil.compareDateWithoutTime(tDate, DateUtil.now(DateUtil.DATE_FORMAT_DDMMYYYY)) < 0) {
													msg = R.getResource("imp.tuyen.clmn.error.denNgay.backSysDate") + "\n";
												} else {
													if (fDate != null && tDate != null && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
														msg = R.getResource("imp.tuyen.clmn.error.tuNgay.nextToDate") + "\n";
													}
												}
											}
										}
									}
									if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
										message += msg;
									}
								}
							}

							msg = "";
							//tan suat
							if (row.size() > C_TANSUAT) {
								String value = row.get(C_TANSUAT).trim();
								if (!StringUtil.isNullOrEmpty(value)) {
									msg = ValidateUtil.validateField(value, "catalog.customer.tansuat", 2, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {
										tansuat = Integer.parseInt(value);
										if (tansuat < 0) {
											message += R.getResource("imp.tuyen.clmn.error.tanSuat");
										}
									}
								}
							}

							// Tan suat W1
							msg = "";
							flagCKGT = false;
							if (StringUtil.isNullOrEmpty(message)) {
								// W1
								if (!StringUtil.isNullOrEmpty(row.get(C_W1))) {
									String value = row.get(C_W1).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.W1") + "\n";
									} else {
										flagCKGT = true;
										routingCus.setWeek1(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setWeek1(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							// Tan suat W2
							msg = "";
							if (StringUtil.isNullOrEmpty(message)) {
								// W2
								if (!StringUtil.isNullOrEmpty(row.get(C_W2))) {
									String value = row.get(C_W2).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.W2") + "\n";
									} else {
										flagCKGT = true;
										routingCus.setWeek2(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setWeek2(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}

							// Tan suat W3
							msg = "";
							if (StringUtil.isNullOrEmpty(message)) {
								// W3
								if (!StringUtil.isNullOrEmpty(row.get(C_W3))) {
									String value = row.get(C_W3).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.W3") + "\n";
									} else {
										flagCKGT = true;
										routingCus.setWeek3(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setWeek3(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}

							// Tan suat W4
							msg = "";
							if (StringUtil.isNullOrEmpty(message)) {
								// W4
								if (!StringUtil.isNullOrEmpty(row.get(C_W4))) {
									String value = row.get(C_W4).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.W4") + "\n";
									} else {
										flagCKGT = true;
										routingCus.setWeek4(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setWeek4(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							if (StringUtil.isNullOrEmpty(message) && !flagCKGT) {
								msg = R.getResource("imp.tuyen.clmn.error.value.x.fail.W") + "\n";
							}
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
							// Danh sach cac thu duoc check trong tuan
							msg = "";
							flagNgay = false;
							if (StringUtil.isNullOrEmpty(message)) {
								// Thu 2
								if (!StringUtil.isNullOrEmpty(row.get(C_T2))) {
									String value = row.get(C_T2).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.thu2") + "\n";
									} else {
										flagNgay = true;
										routingCus.setMonday(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setMonday(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								//Thu 3
								if (!StringUtil.isNullOrEmpty(row.get(C_T3))) {
									String value = row.get(C_T3).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.thu3") + "\n";
									} else {
										flagNgay = true;
										routingCus.setTuesday(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setTuesday(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								//Thu 4
								if (!StringUtil.isNullOrEmpty(row.get(C_T4))) {
									String value = row.get(C_T4).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.thu4") + "\n";
									} else {
										flagNgay = true;
										routingCus.setWednesday(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setWednesday(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								// Thu 5
								if (!StringUtil.isNullOrEmpty(row.get(C_T5))) {
									String value = row.get(C_T5).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.thu5") + "\n";
									} else {
										flagNgay = true;
										routingCus.setThursday(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setThursday(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								// Thu 6
								if (!StringUtil.isNullOrEmpty(row.get(C_T6))) {
									String value = row.get(C_T6).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.thu6") + "\n";
									} else {
										flagNgay = true;
										routingCus.setFriday(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setFriday(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								// Thu 7
								if (!StringUtil.isNullOrEmpty(row.get(C_T7))) {
									String value = row.get(C_T7).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.thu7") + "\n";
									} else {
										flagNgay = true;
										routingCus.setSaturday(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setSaturday(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							if (StringUtil.isNullOrEmpty(message)) {
								// Chu nhat
								if (!StringUtil.isNullOrEmpty(row.get(C_CN))) {
									String value = row.get(C_CN).trim();
									if (!"X".equals(value.toUpperCase())) {
										msg = R.getResource("imp.tuyen.clmn.error.value.x.chuNhat") + "\n";
									} else {
										flagNgay = true;
										routingCus.setSunday(RoutingCustomerType.GO);
									}
								} else {
									routingCus.setSunday(RoutingCustomerType.NOTGO);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}
							}
							if (StringUtil.isNullOrEmpty(message) && !flagNgay) {
								msg = R.getResource("imp.tuyen.clmn.error.value.x.fail") + "\n";
							}
							if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
								message += msg;
							}

							//check tan suat
							if (StringUtil.isNullOrEmpty(message)) {
								int tansuatRC = this.getTansuat(routingCus);
								routingCus.setFrequency(tansuatRC);
								if (tansuat != null && tansuat >= 0 && tansuatRC != tansuat) {
									message += R.getResource("imp.epx.tuyen.clmn.error.tansuat");
								}
							}

							countSTTGT = 1;
							if (StringUtil.isNullOrEmpty(message)) {
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T2))) {
									// Thu tu ghe tham - T2
									String value = row.get(C_SEQ_T2).replace(",", "");
									value = value.replace(".", "");
									value = value.trim();
									msg = ValidateUtil.validateField(value.trim(), "imp.epx.tuyen.clmn.ttgt.thu2", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH,
											ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {/*
										if (Integer.valueOf(value) > 0) {
											//Kiem tra thu duoc chon
											if (StringUtil.isNullOrEmpty(row.get(C_T2))) {
												msg = R.getResource("imp.epx.tuyen.clmn.ttgt.thu2.notCheck") + "\n";
											} else {
												// Kiem tra khong duoc trung thu
												// tu ghe tham
												for (int j = i - 1; j >= 2; j--) {
													List<String> rowJ = lstData.get(j);
													if (i != j && !StringUtil.isNullOrEmpty(rowJ.get(C_SEQ_T2)) && this.isDuplicateRow(row, rowJ, null)) {
														countSTTGT++;
														if (rowJ.get(C_SEQ_T2).trim().equals(row.get(C_SEQ_T2).trim())) {
															flagTTGT = j;
															break;
														}
													}
												}
												if (flagTTGT > 0) {
													msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.khongDuocTrung") + R.getResource("imp.tuyen.clmn.t2.duplicate.line", String.valueOf(i - 1), String.valueOf(flagTTGT - 1));
												} else if (routingNew != null) {
													List<RoutingCustomer> lstRoucusSTT = superviserMgr.getListRoutingCustomersBySeq(routingNew.getId(), 2);
													// Kiem tra khong duoc vuot qua so luong khach hang voi tuyen trong ngay
													if (lstRoucusSTT != null) {
														for (int t = 0; t < lstRoucusSTT.size(); t++) {
															if (Integer.valueOf(value).equals(lstRoucusSTT.get(t).getSeq2())) {
																flagTTGT = t;
																break;
															}
														}
														if (flagTTGT > 0) {
															msg = R.getResource("imp.tuyen.clmn.stt.exist", lstRoucusSTT.get(flagTTGT).getCustomer().getShortCode(), lstRoucusSTT.get(flagTTGT).getCustomer().getCustomerName());
														} else {
															countSTTGT = countSTTGT + lstRoucusSTT.size();//superviserMgr.countSEQBySeqAndRoutingId(routingNew.getId(), 2);
															if (countSTTGT < Integer.valueOf(value)) {
																msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.lonHonSoKH") + R.getResource("imp.tuyen.clmn.cust.in.route", String.valueOf(countSTTGT));
															}
														}
													}
												}
											}
										}
									*/}
									if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
										message += msg;
									}
								}
							}

							countSTTGT = 1;
							if (StringUtil.isNullOrEmpty(message)) {
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T3))) {
									//Thu tu ghe tham - T3
									String value = row.get(C_SEQ_T3).replace(",", "");
									value = value.replace(".", "");
									value = value.trim();
									msg = ValidateUtil.validateField(value.trim(), "imp.epx.tuyen.clmn.ttgt.thu3", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH,
											ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {/*
										// Kiem tra thu duoc chon
										if (Integer.valueOf(value) > 0) {
											if (StringUtil.isNullOrEmpty(row.get(C_T3))) {
												msg = R.getResource("imp.epx.tuyen.clmn.ttgt.thu3.notCheck") + "\n";
											} else {
												//Kiem tra khong duoc trung thu tu ghe tham
												for (int j = i - 1; j >= 2; j--) {
													List<String> rowJ = lstData.get(j);
													if (i != j && !StringUtil.isNullOrEmpty(rowJ.get(C_SEQ_T3)) && this.isDuplicateRow(row, rowJ, null)) {
														countSTTGT++;
														if (rowJ.get(C_SEQ_T3).trim().equals(row.get(C_SEQ_T3).trim())) {
															flagTTGT = j;
															break;
														}
													}
												}
												if (flagTTGT > 0) {
													msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.khongDuocTrung") + R.getResource("imp.tuyen.clmn.t3.duplicate.line", String.valueOf(i - 1), String.valueOf(flagTTGT - 1));
												} else if (routingNew != null) {
													List<RoutingCustomer> lstRoucusSTT = superviserMgr.getListRoutingCustomersBySeq(routingNew.getId(), 2);
													// Kiem tra khong duoc vuot qua so luong khach hàng voi tuyen trong ngay
													if (lstRoucusSTT != null) {
														for (int t = 0; t < lstRoucusSTT.size(); t++) {
															if (Integer.valueOf(value).equals(lstRoucusSTT.get(t).getSeq3())) {
																flagTTGT = t;
																break;
															}
														}
														if (flagTTGT > 0) {
															msg = R.getResource("imp.tuyen.clmn.stt.exist", lstRoucusSTT.get(flagTTGT).getCustomer().getShortCode(), lstRoucusSTT.get(flagTTGT).getCustomer().getCustomerName());
														} else {
															countSTTGT = countSTTGT + lstRoucusSTT.size();
															if (countSTTGT < Integer.valueOf(value)) {
																msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.lonHonSoKH") + R.getResource("imp.tuyen.clmn.cust.in.route", String.valueOf(countSTTGT));
															}
														}
													}
												}
											}
										}
									*/}
									if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
										message += msg;
									}
								}
							}

							countSTTGT = 1;
							if (StringUtil.isNullOrEmpty(message)) {
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T4))) {
									// Thu tu ghe tham - T4
									String value = row.get(C_SEQ_T4).replace(",", "");
									value = value.replace(".", "");
									value = value.trim();
									msg = ValidateUtil.validateField(value.trim(), "imp.epx.tuyen.clmn.ttgt.thu4", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH,
											ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {/*
										if (Integer.valueOf(value) > 0) {
											// Kiem tra thu duoc chon
											if (StringUtil.isNullOrEmpty(row.get(C_T4))) {
												msg = R.getResource("imp.epx.tuyen.clmn.ttgt.thu4.notCheck") + "\n";
											} else {
												// Kiem tra khong duoc trung thu tu ghe tham
												for (int j = i - 1; j >= 2; j--) {
													List<String> rowJ = lstData.get(j);
													if (!StringUtil.isNullOrEmpty(rowJ.get(C_SEQ_T4)) && this.isDuplicateRow(row, rowJ, null)) {
														countSTTGT++;
														if (rowJ.get(C_SEQ_T4).trim().equals(row.get(C_SEQ_T4).trim())) {
															flagTTGT = j;
															break;
														}
													}
												}
												if (flagTTGT > 0) {
													msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.khongDuocTrung") + R.getResource("imp.tuyen.clmn.t4.duplicate.line", String.valueOf(i - 1), String.valueOf(flagTTGT - 1));
												} else if (routingNew != null) {
													List<RoutingCustomer> lstRoucusSTT = superviserMgr.getListRoutingCustomersBySeq(routingNew.getId(), 3);
													//Kiem tra khong duoc vuot qua so luong khach hàng voi tuyen trong ngay
													if (lstRoucusSTT != null) {
														for (int t = 0; t < lstRoucusSTT.size(); t++) {
															if (Integer.valueOf(value).equals(lstRoucusSTT.get(t).getSeq4())) {
																flagTTGT = t;
																break;
															}
														}
														if (flagTTGT > 0) {
															msg = R.getResource("imp.tuyen.clmn.stt.exist", lstRoucusSTT.get(flagTTGT).getCustomer().getShortCode(), lstRoucusSTT.get(flagTTGT).getCustomer().getCustomerName());
														} else {
															countSTTGT = countSTTGT + lstRoucusSTT.size();
															if (countSTTGT < Integer.valueOf(value)) {
																msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.lonHonSoKH") + R.getResource("imp.tuyen.clmn.cust.in.route", String.valueOf(countSTTGT));
															}
														}
													}
												}
											}
										}
									*/}
									if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
										message += msg;
									}
								}
							}

							countSTTGT = 1;
							if (StringUtil.isNullOrEmpty(message)) {
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T5))) {
									// Thu tu ghe tham - T5
									String value = row.get(C_SEQ_T5).replace(",", "");
									value = value.replace(".", "");
									value = value.trim();
									msg = ValidateUtil.validateField(value.trim(), "imp.epx.tuyen.clmn.ttgt.thu5", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH,
											ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {/*
										if (Integer.valueOf(value) > 0) {
											//Kiem tra thu duoc chon
											if (StringUtil.isNullOrEmpty(row.get(C_T5))) {
												msg = R.getResource("imp.epx.tuyen.clmn.ttgt.thu5.notCheck") + "\n";
											} else {
												// Kiem tra khong duoc trung thu  tu ghe tham
												for (int j = i - 1; j >= 2; j--) {
													List<String> rowJ = lstData.get(j);
													if (i != j && !StringUtil.isNullOrEmpty(rowJ.get(C_SEQ_T5)) && this.isDuplicateRow(row, rowJ, null)) {
														countSTTGT++;
														if (rowJ.get(C_SEQ_T5).trim().equals(row.get(C_SEQ_T5).trim())) {
															flagTTGT = j;
															break;
														}
													}
												}
												if (flagTTGT > 0) {
													msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.khongDuocTrung") + R.getResource("imp.tuyen.clmn.t5.duplicate.line", String.valueOf(i - 1), String.valueOf(flagTTGT - 1));
												} else if (routingNew != null) {
													List<RoutingCustomer> lstRoucusSTT = superviserMgr.getListRoutingCustomersBySeq(routingNew.getId(), 4);
													//Kiem tra khong duoc vuot qua so luong khach hàng voi tuyen trong ngay
													if (lstRoucusSTT != null) {
														for (int t = 0; t < lstRoucusSTT.size(); t++) {
															if (Integer.valueOf(value).equals(lstRoucusSTT.get(t).getSeq5())) {
																flagTTGT = t;
																break;
															}
														}
														if (flagTTGT > 0) {
															msg = R.getResource("imp.tuyen.clmn.stt.exist", lstRoucusSTT.get(flagTTGT).getCustomer().getShortCode(), lstRoucusSTT.get(flagTTGT).getCustomer().getCustomerName());
														} else {
															countSTTGT = countSTTGT + lstRoucusSTT.size();
															if (countSTTGT < Integer.valueOf(value)) {
																msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.lonHonSoKH") + R.getResource("imp.tuyen.clmn.cust.in.route", String.valueOf(countSTTGT));
															}
														}
													}
												}
											}
										}
									*/}
									if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
										message += msg;
									}
								}
							}

							countSTTGT = 1;
							if (StringUtil.isNullOrEmpty(message)) {
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T6))) {
									// Thu tu ghe tham - T6
									String value = row.get(C_SEQ_T6).replace(",", "");
									value = value.replace(".", "");
									value = value.trim();
									msg = ValidateUtil.validateField(value.trim(), "imp.epx.tuyen.clmn.ttgt.thu6", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH,
											ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {/*
										if (Integer.valueOf(value) > 0) {
											// Kiem tra thu duoc chon
											if (StringUtil.isNullOrEmpty(row.get(C_T6))) {
												msg = R.getResource("imp.epx.tuyen.clmn.ttgt.thu6.notCheck") + "\n";
											} else {
												// Kiem tra khong duoc trung thu tu ghe tham
												for (int j = i - 1; j >= 2; j--) {
													List<String> rowJ = lstData.get(j);
													if (i != j && !StringUtil.isNullOrEmpty(rowJ.get(C_SEQ_T6)) && this.isDuplicateRow(row, rowJ, null)) {
														countSTTGT++;
														if (rowJ.get(C_SEQ_T6).trim().equals(row.get(C_SEQ_T6).trim())) {
															flagTTGT = j;
															break;
														}
													}
												}
												if (flagTTGT > 0) {
													msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.khongDuocTrung") + R.getResource("imp.tuyen.clmn.t6.duplicate.line", String.valueOf(i - 1), String.valueOf(flagTTGT - 1));
												} else if (routingNew != null) {
													List<RoutingCustomer> lstRoucusSTT = superviserMgr.getListRoutingCustomersBySeq(routingNew.getId(), 5);
													//Kiem tra khong duoc vuot qua so luong khach hàng voi tuyen trong ngay
													if (lstRoucusSTT != null) {
														for (int t = 0; t < lstRoucusSTT.size(); t++) {
															if (Integer.valueOf(value).equals(lstRoucusSTT.get(t).getSeq6())) {
																flagTTGT = t;
																break;
															}
														}
														if (flagTTGT > 0) {
															msg = R.getResource("imp.tuyen.clmn.stt.exist", lstRoucusSTT.get(flagTTGT).getCustomer().getShortCode(), lstRoucusSTT.get(flagTTGT).getCustomer().getCustomerName());
														} else {
															countSTTGT = countSTTGT + lstRoucusSTT.size();
															if (countSTTGT < Integer.valueOf(value)) {
																msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.lonHonSoKH") + R.getResource("imp.tuyen.clmn.cust.in.route", String.valueOf(countSTTGT));
															}
														}
													}
												}
											}
										}
									*/}
									if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
										message += msg;
									}
								}
							}

							countSTTGT = 1;
							if (StringUtil.isNullOrEmpty(message)) {
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T7))) {
									// Thu tu ghe tham - T7
									String value = row.get(C_SEQ_T7).replace(",", "");
									value = value.replace(".", "");
									value = value.trim();
									msg = ValidateUtil.validateField(value.trim(), "imp.epx.tuyen.clmn.ttgt.thu7", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH,
											ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {/*
										if (Integer.valueOf(value) > 0) {
											// Kiem tra thu duoc chon
											if (StringUtil.isNullOrEmpty(row.get(C_T7))) {
												msg = R.getResource("imp.epx.tuyen.clmn.ttgt.thu7.notCheck") + "\n";
											} else {
												// Kiem tra khong duoc trung thu tu ghe tham
												for (int j = i - 1; j >= 2; j--) {
													List<String> rowJ = lstData.get(j);
													if (!StringUtil.isNullOrEmpty(rowJ.get(C_SEQ_T7)) && this.isDuplicateRow(row, rowJ, null)) {
														countSTTGT++;
														if (rowJ.get(C_SEQ_T7).trim().equals(row.get(C_SEQ_T7).trim())) {
															flagTTGT = j;
															break;
														}
													}
												}
												if (flagTTGT > 0) {
													msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.khongDuocTrung") + R.getResource("imp.tuyen.clmn.t7.duplicate.line", String.valueOf(i - 1), String.valueOf(flagTTGT - 1));
												} else if (routingNew != null) {
													List<RoutingCustomer> lstRoucusSTT = superviserMgr.getListRoutingCustomersBySeq(routingNew.getId(), 6);
													// Kiem tra khong duoc vuot qua so luong khach hàng voi tuyen trong ngay
													if (lstRoucusSTT != null) {
														for (int t = 0; t < lstRoucusSTT.size(); t++) {
															if (Integer.valueOf(value).equals(lstRoucusSTT.get(t).getSeq7())) {
																flagTTGT = t;
																break;
															}
														}
														if (flagTTGT > 0) {
															msg = R.getResource("imp.tuyen.clmn.stt.exist", lstRoucusSTT.get(flagTTGT).getCustomer().getShortCode(), lstRoucusSTT.get(flagTTGT).getCustomer().getCustomerName());
														} else {
															countSTTGT = countSTTGT + lstRoucusSTT.size();
															if (countSTTGT < Integer.valueOf(value)) {
																msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.lonHonSoKH") + R.getResource("imp.tuyen.clmn.cust.in.route", String.valueOf(countSTTGT));
															}
														}
													}
												}
											}
										}
									*/}
									if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
										message += msg;
									}
								}
							}

							countSTTGT = 1;
							if (StringUtil.isNullOrEmpty(message)) {
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_CN))) {
									// Thu tu ghe tham - CN
									String value = row.get(C_SEQ_CN).replace(",", "");
									value = value.replace(".", "");
									value = value.trim();
									msg = ValidateUtil.validateField(value, "imp.epx.tuyen.clmn.ttgt.chuNhat", 2, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH,
											ConstantManager.ERR_INTEGER);
									if (!StringUtil.isNullOrEmpty(msg)) {
										message += msg;
									} else {/*
										if (Integer.valueOf(value.trim()) > 0) {
											// Kiem tra thu duoc chon
											if (StringUtil.isNullOrEmpty(row.get(C_CN))) {
												msg = R.getResource("imp.epx.tuyen.clmn.ttgt.thu8.notCheck") + "\n";
											} else {
												// Kiem tra khong duoc trung thu tu ghe tham
												for (int j = i - 1; j >= 2; j--) {
													List<String> rowJ = lstData.get(j);
													if (i != j && !StringUtil.isNullOrEmpty(rowJ.get(C_SEQ_CN)) && this.isDuplicateRow(row, rowJ, null)) {
														countSTTGT++;
														if (rowJ.get(C_SEQ_CN).trim().equals(value)) {
															flagTTGT = j;
															break;
														}
													}
												}
												if (flagTTGT > 0) {
													msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.khongDuocTrung") + R.getResource("imp.tuyen.clmn.cn.duplicate.line", String.valueOf(i - 1), String.valueOf(flagTTGT - 1));
												} else if (routingNew != null) {
													List<RoutingCustomer> lstRoucusSTT = superviserMgr.getListRoutingCustomersBySeq(routingNew.getId(), 7);
													//Kiem tra khong duoc vuot qua so luong khach hàng voi tuyen trong ngay
													if (lstRoucusSTT != null) {
														for (int t = 0; t < lstRoucusSTT.size(); t++) {
															if (Integer.valueOf(value).equals(lstRoucusSTT.get(t).getSeq8())) {
																flagTTGT = t;
																break;
															}
														}
														if (flagTTGT > 0) {
															msg = R.getResource("imp.tuyen.clmn.stt.exist", lstRoucusSTT.get(flagTTGT).getCustomer().getShortCode(), lstRoucusSTT.get(flagTTGT).getCustomer().getCustomerName());
														} else {
															countSTTGT = countSTTGT + lstRoucusSTT.size();
															if (countSTTGT < Integer.valueOf(row.get(26).trim())) {
																msg = R.getResource("imp.tuyen.clmn.error.value.TTGT.lonHonSoKH") + R.getResource("imp.tuyen.clmn.cust.in.route", String.valueOf(countSTTGT));
															}
														}
													}
												}
											}
										}
									*/}
									if (!StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(message)) {
										message += msg;
									}
								}
							}

							if (StringUtil.isNullOrEmpty(message)) {
								// tanSuat
								/* Xu ly them vao DB */
								if (flagRowtingNew == 2) {
									// Neu tuyen chua co hoac da xoa, tao moi
									// tuyen
									routingNew = new Routing();
									routingNew.setRoutingCode(row.get(C_ROUTINGCODE).trim().toUpperCase());// Ma Tuyen
									routingNew.setRoutingName(row.get(C_ROUTINGNAME).trim());// Ten tuyen
									routingNew.setShop(sTmp);// ID NPP
									routingNew.setStatus(ActiveType.RUNNING);//Trang thai du thao
									routingNew.setCreateUser(currentUser.getUserName());
									routingNew.setCreateDate(new Date());
									routingNew = superviserMgr.createRoutingImport(routingNew);
								}
								// Them khach hang vao tuyen
								routingCus.setCustomer(cusNew);
								routingCus.setRouting(routingNew);
								routingCus.setWeekInterval(1);

								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T2))) {
									routingCus.setSeq2(Integer.valueOf(row.get(C_SEQ_T2).trim()));
								}
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T3))) {
									routingCus.setSeq3(Integer.valueOf(row.get(C_SEQ_T3).trim()));
								}
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T4))) {
									routingCus.setSeq4(Integer.valueOf(row.get(C_SEQ_T4).trim()));
								}
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T5))) {
									routingCus.setSeq5(Integer.valueOf(row.get(C_SEQ_T5).trim()));
								}
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T6))) {
									routingCus.setSeq6(Integer.valueOf(row.get(C_SEQ_T6).trim()));
								}
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_T7))) {
									routingCus.setSeq7(Integer.valueOf(row.get(C_SEQ_T7).trim()));
								}
								if (!StringUtil.isNullOrEmpty(row.get(C_SEQ_CN))) {
									routingCus.setSeq8(Integer.valueOf(row.get(C_SEQ_CN).trim()));
								}
								routingCus.setCreateUser(currentUser.getUserName());
								routingCus.setCreateDate(new Date());

								//routingCus.setStartWeek(1);
								routingCus.setStartDate(fDate);
								routingCus.setEndDate(tDate);
								routingCus.setStatus(ActiveType.RUNNING);
								routingCus.setShop(sTmp);
								superviserMgr.updateRoutingCustomerImportFile(routingCus);
								countStaffCode = 0;
								for (int c = i - 1; c >= 2; c--) {
									List<String> rowC = lstData.get(c);
									if (this.isDuplicateRow(row, rowC, C_NVBH)) {
										countStaffCode++;
									} else {
										break;
									}
								}

								if (countStaffCode <= 1) {
									//Cap nhap ban Visiplan
									visitPlan = new VisitPlan();
									visitPlan.setRouting(routingNew);
									visitPlan.setShop(sTmp);
									visitPlan.setStaff(lstStaff.get(0));
									visitPlan.setFromDate(sysDate);
									visitPlan.setToDate(null);
									visitPlan.setCreateUser(currentUser.getUserName());
									visitPlan.setCreateDate(new Date());
									visitPlan.setStatus(ActiveType.RUNNING);
									superviserMgr.updateVisitplanImportFile(visitPlan);
								}
							} else {
								// Xuat file loi
								lstFails.add(StringUtil.addFailBean(row, message));
							}
						}
					}
				}
				// Out of loop for: Export error
				//				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_SUPERVISESHOP_MANAGERROUTER_CRETE_FAIL);
				getOutputFailExcelFileMultiLanguage(lstFails, ConstantManager.TEMPLATE_SUPERVISESHOP_MANAGERROUTER_CRETE_FAIL, initImportTempateHeader(), R.getResource("ss.create.route.import.template.error"));
			} catch (Exception e) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				LogUtility.logError(e, e.getMessage());
			}
		} else if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = true;
			errMsg = R.getResource("ss.create.route.tap.tin.excel.chua.nhap.thong.tin");
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	public boolean isDuplicateRow(List<String> row, List<String> rowJ, Integer i) {
		if (StringUtil.isNullOrEmpty(row.get(C_SHOPCODE)) || StringUtil.isNullOrEmpty(rowJ.get(C_SHOPCODE)) || !row.get(C_SHOPCODE).toUpperCase().equals(rowJ.get(C_SHOPCODE).toUpperCase())) {
			return false;
		}
		if (StringUtil.isNullOrEmpty(row.get(C_ROUTINGCODE)) || StringUtil.isNullOrEmpty(rowJ.get(C_ROUTINGCODE)) || !row.get(C_ROUTINGCODE).toUpperCase().equals(rowJ.get(C_ROUTINGCODE).toUpperCase())) {
			return false;
		}
		if (i != null && (StringUtil.isNullOrEmpty(row.get(i)) || StringUtil.isNullOrEmpty(rowJ.get(i)) || !row.get(i).toUpperCase().equals(rowJ.get(i).toUpperCase()))) {
			return false;
		}
		return true;
	}

	public int getTansuat(RoutingCustomer rc) {
		int week = rc.getWeek1().getValue() + rc.getWeek2().getValue() + rc.getWeek3().getValue() + rc.getWeek4().getValue();
		int day = rc.getMonday().getValue() + rc.getTuesday().getValue() + rc.getWednesday().getValue() + rc.getThursday().getValue() + rc.getFriday().getValue() + rc.getSaturday().getValue() + rc.getSunday().getValue();
		return week * day;
	}

	public boolean checkDuplicateAll(List<List<String>> lstData) {
		try {
			for (int i = 0; i < lstData.size(); i++) {
				String mesTemp = checkDuplicate(lstData, i);
				if (!StringUtil.isNullOrEmpty(mesTemp) && !mesTemp.equals("1")) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String checkDuplicate(List<List<String>> lstData, int k) {// check
																		// trung
																		// khoa
		String mes = "";
		if (lstData.get(k) == null || lstData.get(k).size() < 16) {
			mes += "1";// Du lieu khong hop le
		} else {
			// String staffCode = lstData.get(k).get(0).trim();
			String customerCode = lstData.get(k).get(0).trim().toUpperCase();
			String type = lstData.get(k).get(1).trim();
			// String chungtu = lstData.get(k).get(3).trim();
			if (StringUtil.isNullOrEmpty(customerCode) || StringUtil.isNullOrEmpty(type)) {
				mes += "1";// Du lieu khong hop le
			} else {
				for (int i = 0; i < lstData.size(); i++) {
					if (i != k && lstData.get(i) != null && lstData.get(i).size() >= 16) {
						List<String> row = lstData.get(i);
						if (customerCode.equalsIgnoreCase(row.get(0).trim()) && type.equalsIgnoreCase(row.get(1).trim())) {
							if (isView == 0) {
								mes += (i + 2) + " , ";// stt dong import khac
														// voi dong xem
							} else {
								mes += (i + 1) + " , ";
							}
						}
					}
				}
				if (mes.length() > 0) {
					mes = R.getResource("common.import.excel.error.dongTrungDuLieu") + mes.substring(0, mes.length() - 3);
				}
			}
		}
		return mes;
	}

	/**
	 * Tao file template dung de import
	 * 
	 * @author tungmt
	 * @return Doi tuong chua duong dan toi file template
	 * @since 25/05/2015
	 */
	public String downloadTemplate() {
		String reportToken = retrieveReportToken(reportCode);
		if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		List<CellBean> lstFails = new ArrayList<CellBean>();
		lstFails.add(new CellBean());
		getOutputFailExcelFileMultiLanguage(lstFails, ConstantManager.IMPORT_ROUTE_TEMPATE_FILE_NAME, initImportTempateHeader(), R.getResource("ss.create.route.import.template"));
		result.put(REPORT_PATH, fileNameFail);
		result.put(LIST, fileNameFail);
		result.put(ERROR, false);
		result.put("hasData", true);
		try {
			MemcachedUtils.putValueToMemcached(reportToken, fileNameFail, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	private Map<String, Object> initImportTempateHeader() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", R.getResource("ss.create.route.import.title"));
		params.put("shopCode", R.getResource("jsp.common.shop.code"));
		params.put("stt", R.getResource("jsp.common.numerical.order"));
		params.put("routeCode", R.getResource("jsp.common.route.code"));
		params.put("routeName", R.getResource("jsp.common.route.name"));
		params.put("nvbhCode", R.getResource("ss.create.route.import.nvbh.code"));
		params.put("customerCode", R.getResource("jsp.common.customer.code"));
		params.put("fromDate", R.getResource("jsp.common.from.date"));
		params.put("toDate", R.getResource("jsp.common.to.date"));
		params.put("ckgt", R.getResource("ss.create.route.import.chu.ky.ghe.tham"));
		params.put("w1", R.getResource("ss.create.route.import.w1"));
		params.put("w2", R.getResource("ss.create.route.import.w2"));
		params.put("w3", R.getResource("ss.create.route.import.w3"));
		params.put("w4", R.getResource("ss.create.route.import.w4"));
		params.put("ngt", R.getResource("ss.create.route.import.ngay.ghe.tham"));
		params.put("ttgt", R.getResource("ss.create.route.import.thu.tu.ghe.tham"));
		params.put("t2", R.getResource("jsp.common.thu.2"));
		params.put("t3", R.getResource("jsp.common.thu.3"));
		params.put("t4", R.getResource("jsp.common.thu.4"));
		params.put("t5", R.getResource("jsp.common.thu.5"));
		params.put("t6", R.getResource("jsp.common.thu.6"));
		params.put("t7", R.getResource("jsp.common.thu.7"));
		params.put("cn", R.getResource("jsp.common.thu.cn"));
		params.put("tansuat", R.getResource("imp.epx.tuyen.clmn.tanSuat"));
		params.put("updateDate", R.getResource("ss.create.route.update.date"));
		params.put("msgErr", R.getResource("attribute.error"));
		return params;
	}
	
	private int checkTrungTTGT(int dateNeedChecked, List<String> row, List<List<String>> lstData, int i, int seq){
		int flagTTGT = 0;
		for (int j = i - 1; j >= 2; j--) {
			List<String> rowJ = lstData.get(j);
			if (i != j && !StringUtil.isNullOrEmpty(rowJ.get(seq)) && this.isDuplicateRow(row, rowJ, null)) {
//				countSTTGT++;
				if (rowJ.get(seq).trim().equals(row.get(seq).trim())) {
					flagTTGT = j;
					break;
				}
			}
		}
		return flagTTGT;
	}

	/**
	 * Tao file mau import gia san pham
	 * 
	 * @author tungmt
	 * @return Ten file template moi duoc tao
	 * @since 25/05/2015
	 */
	//	private String createImportTemplateFile() {
	//		String importTemplateFileName = null;
	//
	//		FileInputStream fileInputStream = null;
	//		FileOutputStream fileOutputStream = null;
	//		XSSFWorkbook workbook = null;
	//		OPCPackage opcPackage = null;
	//		try {
	//			/*
	//			 * tao file template moi tu file template mau
	//			 */
	//			String importTemplateFileAbsolutePath = this.generateTemplateImportFileFromOriginalFile();
	//			importTemplateFileName = Paths.get(importTemplateFileAbsolutePath).getFileName().toString();
	//
	//			/*
	//			 * ghi du lieu
	//			 */
	//			fileInputStream = new FileInputStream(importTemplateFileAbsolutePath);
	//			opcPackage = OPCPackage.open(fileInputStream);
	//			workbook = new XSSFWorkbook(opcPackage);
	//
	//			/*
	//			 * ghi header sheet import
	//			 */
	//			int IMPORT_SHEET_INDEX = 0;
	//			this.writeImportSheetHeader(workbook, IMPORT_SHEET_INDEX);
	//
	//			fileOutputStream = new FileOutputStream(importTemplateFileAbsolutePath);
	//			workbook.write(fileOutputStream);
	//			fileOutputStream.close();
	//		} catch (Exception e) {
	//			LogUtility.logError(e, e.getMessage());
	//		} finally {
	//			IOUtils.closeQuietly(fileInputStream);
	//			IOUtils.closeQuietly(fileOutputStream);
	//			IOUtils.closeQuietly(opcPackage);
	//		}
	//
	//		return importTemplateFileName;
	//	}

	/**
	 * Ghi header sheet import
	 * 
	 * @author tungmt
	 * @return String Duong dan tuyet doi toi file template tra ve client
	 * @throws IOException
	 * @since 25/05/2015
	 */
	//	private String generateTemplateImportFileFromOriginalFile() throws IOException {
	//		final String resourceDirectoryPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathSuperviseShop();
	//		final String sourceTemplateFileName = ConstantManager.IMPORT_ROUTE_TEMPATE_FILE_NAME + FileExtension.XLSX.getValue();
	//		final String sourceTemplateFileAbsoluteName = resourceDirectoryPath + File.separator + sourceTemplateFileName;
	//
	//		String importTemplateFileName = R.getResource("ss.create.route.file.name.import") + "_"
	//				+ DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
	//		final String importTemplateFileAbsolutePath = Configuration.getStoreRealPath() + File.separator + importTemplateFileName;
	//		FileUtility.copyFile(sourceTemplateFileAbsoluteName, importTemplateFileAbsolutePath);
	//		return importTemplateFileAbsolutePath;
	//	}

	/**
	 * Ghi header sheet import
	 * 
	 * @author tungmt
	 * @param workbook
	 *            Workbook ghi du lieu
	 * @param importSheetIndex
	 *            Vi tri sheet ghi du lieu
	 * @since 25-05-2015
	 */
	//	private void writeImportSheetHeader(XSSFWorkbook workbook, int importSheetIndex) {
	//		try {
	//			XSSFSheet importSheet = workbook.getSheetAt(importSheetIndex);
	//			if (importSheet != null) {
	//				workbook.setSheetName(importSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.update.title.template.file.import.sheet_name"));
	//				ExcelPOIProcessUtils utils = new ExcelPOIProcessUtils();
	//				int row = 0;
	//				int col = 0;//25 cot
	//				utils.addCellsAndMerged(importSheet, row, row++, col, col+24, R.getResource("jsp.common.shop.code"));
	//				utils.addCellsAndMerged(importSheet, row, row++, col, col+24, "");
	//				
	//				utils.addCellsAndMerged(importSheet, row, row+1, col, col++, R.getResource("jsp.common.numerical.order"));
	//				utils.addCellsAndMerged(importSheet, row, row+1, col, col++, R.getResource("jsp.common.route.code"));
	//				utils.addCellsAndMerged(importSheet, row, row+1, col, col++, R.getResource("jsp.common.route.name"));
	//				utils.addCellsAndMerged(importSheet, row, row+1, col, col++, R.getResource("ss.create.route.import.nvbh.code"));
	//				utils.addCellsAndMerged(importSheet, row, row+1, col, col++, R.getResource("jsp.common.customer.code"));
	//				utils.addCellsAndMerged(importSheet, row, row+1, col, col++, R.getResource("jsp.common.from.date"));
	//				utils.addCellsAndMerged(importSheet, row, row+1, col, col++, R.getResource("jsp.common.to.date"));
	//				utils.addCellsAndMerged(importSheet, row, row, col, col+3, R.getResource("ss.create.route.import.chu.ky.ghe.tham"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("ss.create.route.import.w1"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("ss.create.route.import.w2"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("ss.create.route.import.w3"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("ss.create.route.import.w4"));
	//				utils.addCellsAndMerged(importSheet, row, row, col, col+6, R.getResource("ss.create.route.import.ngay.ghe.tham"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.2"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.3"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.4"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.5"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.6"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.7"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.cn"));
	//				utils.addCellsAndMerged(importSheet, row, row, col, col+6, R.getResource("ss.create.route.import.thu.tu.ghe.tham"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.2"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.3"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.4"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.5"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.6"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.7"));
	//				utils.addCellsAndMerged(importSheet, row+1, row+1, col, col++, R.getResource("jsp.common.thu.cn"));
	//			}
	//		} catch (RowsExceededException e) {
	//			LogUtility.logError(e, e.getMessage());
	//		} catch (WriteException e) {
	//			LogUtility.logError(e, e.getMessage());
	//		}
	//	}

	private int C_SHOPCODE = 1;
	private int C_ROUTINGCODE = 2;
	private int C_ROUTINGNAME = 3;
	private int C_NVBH = 4;
	private int C_CUSTOMERCODE = 5;
	private int C_FROMDATE = 6;
	private int C_TODATE = 7;
	private int C_TANSUAT = 8;
	private int C_W1 = 9;
	private int C_W2 = 10;
	private int C_W3 = 11;
	private int C_W4 = 12;
	private int C_T2 = 13;
	private int C_T3 = 14;
	private int C_T4 = 15;
	private int C_T5 = 16;
	private int C_T6 = 17;
	private int C_T7 = 18;
	private int C_CN = 19;
	private int C_SEQ_T2 = 20;
	private int C_SEQ_T3 = 21;
	private int C_SEQ_T4 = 22;
	private int C_SEQ_T5 = 23;
	private int C_SEQ_T6 = 24;
	private int C_SEQ_T7 = 25;
	private int C_SEQ_CN = 26;

	// ///////////@: GETTER/SETTER :@///////////////////////////////////
	private Long shopId;
	private List<ShopVO> lstShopVO;

	public List<ShopVO> getLstShopVO() {
		return lstShopVO;
	}

	public void setLstShopVO(List<ShopVO> lstShopVO) {
		this.lstShopVO = lstShopVO;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getYesterdateStr() {
		return yesterdateStr;
	}

	public void setYesterdateStr(String yesterdateStr) {
		this.yesterdateStr = yesterdateStr;
	}

	public String getBackdateStr() {
		return backdateStr;
	}

	public void setBackdateStr(String backdateStr) {
		this.backdateStr = backdateStr;
	}

	public String getSysdateStr() {
		return sysdateStr;
	}

	public void setSysdateStr(String sysdateStr) {
		this.sysdateStr = sysdateStr;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<Staff> getLstSaleStaff() {
		return lstSaleStaff;
	}

	public void setLstSaleStaff(List<Staff> lstSaleStaff) {
		this.lstSaleStaff = lstSaleStaff;
	}

	public List<Long> getLstExceptionCus() {
		return lstExceptionCus;
	}

	public void setLstExceptionCus(List<Long> lstExceptionCus) {
		this.lstExceptionCus = lstExceptionCus;
	}

	public String getStrExceptionCus() {
		return strExceptionCus;
	}

	public void setStrExceptionCus(String strExceptionCus) {
		this.strExceptionCus = strExceptionCus;
	}

	public String getLstDeleteStr() {
		return lstDeleteStr;
	}

	public void setLstDeleteStr(String lstDeleteStr) {
		this.lstDeleteStr = lstDeleteStr;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public List<ErrorVO> getLstError() {
		return lstError;
	}

	public String getfDateStr() {
		return fDateStr;
	}

	public void setfDateStr(String fDateStr) {
		this.fDateStr = fDateStr;
	}

	public String gettDateStr() {
		return tDateStr;
	}

	public void settDateStr(String tDateStr) {
		this.tDateStr = tDateStr;
	}

	public void setLstError(List<ErrorVO> lstError) {
		this.lstError = lstError;
	}

	public String getLstIdStr() {
		return lstIdStr;
	}

	public void setLstIdStr(String lstIdStr) {
		this.lstIdStr = lstIdStr;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getStaffRoleType() {
		return staffRoleType;
	}

	public void setStaffRoleType(Integer staffRoleType) {
		this.staffRoleType = staffRoleType;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<CellBean> getLstView() {
		return lstView;
	}

	public void setLstView(List<CellBean> lstView) {
		this.lstView = lstView;
	}

	public Boolean getTypeView() {
		return typeView;
	}

	public void setTypeView(Boolean typeView) {
		this.typeView = typeView;
	}

	public Integer getIsView() {
		return isView;
	}

	public void setIsView(Integer isView) {
		this.isView = isView;
	}

	public Long getRoutingCustomerId() {
		return routingCustomerId;
	}

	public void setRoutingCustomerId(Long routingCustomerId) {
		this.routingCustomerId = routingCustomerId;
	}

	public List<RoutingCustomerVO> getLstRoutingCustomerVO() {
		return lstRoutingCustomerVO;
	}

	public void setLstRoutingCustomerVO(List<RoutingCustomerVO> lstRoutingCustomerVO) {
		this.lstRoutingCustomerVO = lstRoutingCustomerVO;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Routing getRoute() {
		return route;
	}

	public void setRoute(Routing route) {
		this.route = route;
	}

	public Long getRouteId() {
		return routeId;
	}

	public void setRouteId(Long routeId) {
		this.routeId = routeId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getStaffSaleCode() {
		return staffSaleCode;
	}

	public void setStaffSaleCode(String staffSaleCode) {
		this.staffSaleCode = staffSaleCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getRoutingCode() {
		return routingCode;
	}

	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}

	public String getRoutingName() {
		return routingName;
	}

	public void setRoutingName(String routingName) {
		this.routingName = routingName;
	}

	public List<RoutingVO> getListRouting() {
		return listRouting;
	}

	public List<ShopVO> getListShopVO() {
		return listShopVO;
	}

	public void setListShopVO(List<ShopVO> listShopVO) {
		this.listShopVO = listShopVO;
	}

	public void setListRouting(List<RoutingVO> listRouting) {
		this.listRouting = listRouting;
	}

	public List<RoutingCustomer> getLstRoutingCustomer() {
		return lstRoutingCustomer;
	}

	public void setLstRoutingCustomer(List<RoutingCustomer> lstRoutingCustomer) {
		this.lstRoutingCustomer = lstRoutingCustomer;
	}

	public String getShortCodeDialog() {
		return shortCodeDialog;
	}

	public void setShortCodeDialog(String shortCodeDialog) {
		this.shortCodeDialog = shortCodeDialog;
	}

	public String getShopCodeDialog() {
		return shopCodeDialog;
	}

	public void setShopCodeDialog(String shopCodeDialog) {
		this.shopCodeDialog = shopCodeDialog;
	}

	public String getCustomerNameDialog() {
		return customerNameDialog;
	}

	public void setCustomerNameDialog(String customerNameDialog) {
		this.customerNameDialog = customerNameDialog;
	}

	public List<String> getLstRoutingCustomerAdd() {
		return lstRoutingCustomerAdd;
	}

	public void setLstRoutingCustomerAdd(List<String> lstRoutingCustomerAdd) {
		this.lstRoutingCustomerAdd = lstRoutingCustomerAdd;
	}

	public List<String> getLstRoutingCustomerEdit() {
		return lstRoutingCustomerEdit;
	}

	public void setLstRoutingCustomerEdit(List<String> lstRoutingCustomerEdit) {
		this.lstRoutingCustomerEdit = lstRoutingCustomerEdit;
	}

	public List<Long> getLstRoutingCustomerEditDelete() {
		return lstRoutingCustomerEditDelete;
	}

	public void setLstRoutingCustomerEditDelete(List<Long> lstRoutingCustomerEditDelete) {
		this.lstRoutingCustomerEditDelete = lstRoutingCustomerEditDelete;
	}
}

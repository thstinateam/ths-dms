package ths.dms.web.action.superviseshop;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.VisitPlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.filter.VisitPlanFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * The Class ManageRouteAssignAction.
 */
public class ManageRouteAssignAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2260920940274717960L;
	private StaffMgr staffMgr;
	private SuperviserMgr superMgr;
	private ShopMgr shopMgr;
	private CommonMgr commonMgr;

	private Long routingId;
	private Routing routing;
	private List<RoutingCustomer> lstRoutingCustomer;
	private List<Staff> lstSaleStaff;
	private String fromDate;
	private String toDate;
	private Long saleStaffId;
	private Integer status;
	private Long visitPlanId;
	private Shop chooseShop;
	private Integer isAllowDuplicateRoute;
	private Boolean isAllowDelete;
	private final String FROM_DATE_LESS_THAN = "FROM_DATE_LESS_THAN";
	private final String FROM_DATE_EQUAL = "FROM_DATE_EQUAL";

	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		superMgr = (SuperviserMgr) context.getBean("superviserMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		String result = SUCCESS;
		try {
			if (routingId != null) {
				routing = superMgr.getRoutingById(routingId);
				if (routing == null) {
					return PAGE_NOT_FOUND;
				}
				if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
					lstSaleStaff = superMgr.getListNVBHInTuyenNew(currentUser.getStaffRoot().getStaffId(), routing.getShop().getId());
				} else {
					lstSaleStaff = superMgr.getListNVBHInTuyenNew(null, routing.getShop().getId());
				}
				fromDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
				toDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				return PAGE_NOT_FOUND;
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return result;
	}

	public String getListSaleStaff() {
		try {
			if (routingId != null) {
				routing = superMgr.getRoutingById(routingId);
				if (routing != null && routing.getShop() != null) {
					if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
						lstSaleStaff = superMgr.getListNVBHInTuyenNew(currentUser.getStaffRoot().getStaffId(), routing.getShop().getId());
					} else {
						lstSaleStaff = superMgr.getListNVBHInTuyenNew(null, routing.getShop().getId());
					}
					result.put("rows", lstSaleStaff);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Man hinh Chi tiet Tuyen
	 * 
	 * @return JSP
	 * @author hunglm16
	 * @since October 9,2014
	 * */
	public String detail() {
		try {
			shopId = currentUser.getShopRoot().getShopId();
			chooseShop = shopMgr.getShopById(shopId);
			fromDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			toDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			if (routingId != null) {
				routing = superMgr.getRoutingById(routingId);
				if (routing != null && routing.getShop() != null) {
					if (currentUser.getListUser() != null && currentUser.getListUser().size() > 0) {
						lstSaleStaff = superMgr.getListNVBHInTuyenNew(currentUser.getStaffRoot().getStaffId(), routing.getShop().getId());
					} else {
						lstSaleStaff = superMgr.getListNVBHInTuyenNew(null, routing.getShop().getId());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Search StaffInRouting
	 * 
	 * @author thongnm
	 * @return the string
	 */
	public String searchStaffRouting() {
		try {
			KPaging<VisitPlan> kPaging = new KPaging<VisitPlan>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			if (routingId == null)
				routingId = 0L;
			SupFilter<VisitPlan> filter = new SupFilter<VisitPlan>();
			filter.setkPaging(kPaging);
			filter.setRoutingId(routingId);
			filter.setStrStaffId(super.getStrListUserId());
			ObjectVO<VisitPlan> lstVisitPlanVO = superMgr.getListVisitPlanByRoutingId(filter);
			if (lstVisitPlanVO != null) {
				result.put("total", lstVisitPlanVO.getkPaging().getTotalRows());
				result.put("rows", lstVisitPlanVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Search CustomerInRouting
	 * 
	 * @author thongnm
	 * @return the string
	 */
	public String searchCustomerRouting() {
		try {
			if (routingId != null) {
				ObjectVO<RoutingCustomer> lstRoutingCustomerVO = superMgr.getListRoutingCustomer(null, routingId, ActiveType.RUNNING, null, null, currentUser.getShopRoot().getShopId(), null, currentUser.getStaffRoot().getStaffId());
				if (lstRoutingCustomerVO != null) {
					lstRoutingCustomer = lstRoutingCustomerVO.getLstObject();
				}
			}
			if (lstRoutingCustomer == null)
				lstRoutingCustomer = new ArrayList<RoutingCustomer>();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	public Boolean checkStaffAssignOtherRoute(Long routeId, Long staffId, Date fromDate, Date toDate) {
		try {
			VisitPlanFilter filter = new VisitPlanFilter();
			filter.setExceptRouteId(routeId);
			filter.setFromDate(fromDate);
			filter.setToDate(toDate);
			filter.setStaffId(staffId);
			filter.setStatus(ActiveType.RUNNING.getValue());
			VisitPlan vp = superMgr.getVisitPlanMultiChoce(filter);
			if (vp == null || vp.getRouting() == null) {
				return false;
			}
			result.put(ERROR, true);
			result.put("errMsg", R.getResource("ss.assign.route.staff.set.order.route", vp.getRouting().getRoutingCode()));
		} catch (BusinessException e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "Error ManageRouteAssignAction.addOrUpdateStaff: " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return true;
	}

	/**
	 * AddOrUpdate staff.
	 * 
	 * update
	 * 
	 * @author hunglm16
	 * @since April 2, 2014
	 */
	public String addOrUpdateStaff() {
		resetToken(result);
		errMsg = "";
		status = 1;
		try {
			if (shopId == null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			chooseShop = shopMgr.getShopById(shopId);
			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("imp.tuyen.clmn.error.tuNgay.null"));
				return ERROR;
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if (visitPlanId != null && visitPlanId > 0) {
				VisitPlan visitPlan = superMgr.getVisitPlanById(visitPlanId);
				if (visitPlan == null || !chooseShop.getId().equals(visitPlan.getShop().getId())) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
					return ERROR;
				}
				if (DateUtil.compareDateWithoutTime(visitPlan.getFromDate(), commonMgr.getDateBySysdateForNumberDay(0, true)) == -1) {
					fDate = visitPlan.getFromDate();
				}
				if (DateUtil.compareDateWithoutTime(fDate, commonMgr.getDateBySysdateForNumberDay(0, true)) > -1) {
					visitPlan.setFromDate(fDate);
				}
				if (tDate != null && DateUtil.compareDateWithoutTime(tDate, fDate) == -1) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("msg.common.err.7"));
					return ERROR;
				} else if (tDate != null && DateUtil.compareDateWithoutTime(tDate, commonMgr.getDateBySysdateForNumberDay(0, true)) == -1) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("msg.common.err.6"));
					return ERROR;
				}

				visitPlan.setToDate(tDate);
				visitPlan.setStatus(ActiveType.parseValue(status));
				visitPlan.setUpdateUser(currentUser.getUserName());
				visitPlan.setUpdateDate(commonMgr.getSysDate());
				superMgr.updateVisitplanByChangeTime(visitPlan, false, isAllowDelete);
			} else {
				routing = superMgr.getRoutingById(routingId);
				if (routing == null || !chooseShop.getId().equals(routing.getShop().getId())) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
					return ERROR;
				}
				Staff saleStaff = staffMgr.getStaffById(saleStaffId);
				if (saleStaff == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "ss.nvbh"));
					return ERROR;
				}
				if (super.getMapUserId().get(saleStaff.getId()) == null) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("catalog.staff.code.permission1"));
					return ERROR;
				}
				if (saleStaff.getShop() == null || chooseShop == null || !chooseShop.getId().equals(saleStaff.getShop().getId())) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("catalog.staff.code.not.choose"));
					return ERROR;
				}
				if (!ActiveType.RUNNING.equals(saleStaff.getStatus())) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.status.not.running.staff"));
					return ERROR;
				}

				if (DateUtil.compareDateWithoutTime(fDate, commonMgr.getDateBySysdateForNumberDay(0, true)) == -1) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("msg.common.err.5"));
					return ERROR;
				}
				if (tDate != null && DateUtil.compareDateWithoutTime(tDate, fDate) == -1) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("msg.common.err.7"));
					return ERROR;
				} else if (tDate != null && DateUtil.compareDateWithoutTime(tDate, commonMgr.getDateBySysdateForNumberDay(0, true)) == -1) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("msg.common.err.6"));
					return ERROR;
				}
				if (checkStaffAssignOtherRoute(routingId, saleStaffId, fDate, tDate)) {
					return ERROR;
				}
				VisitPlan visitPlan = new VisitPlan();
				visitPlan.setFromDate(fDate);
				visitPlan.setToDate(tDate);
				visitPlan.setRouting(routing);
				visitPlan.setShop(routing.getShop());
				visitPlan.setStaff(saleStaff);
				visitPlan.setStatus(ActiveType.RUNNING);
				visitPlan.setCreateUser(getCurrentUser().getUserName());

				superMgr.updateVisitplanByChangeTime(visitPlan, true, isAllowDelete);
			}
		} catch (Exception e) {
			if (FROM_DATE_LESS_THAN.equals(e.getMessage())) {
				result.put("errMsg", R.getResource("ss.create.route.assigned.nvbh"));
			} else if (FROM_DATE_EQUAL.equals(e.getMessage())) {
				result.put("errMsg", R.getResource("ss.create.route.assigned.nvbh.confirm"));
				result.put("confirmDelete", true);
			} else {
				LogUtility.logError(e, "Error ManageRouteAssignAction.addOrUpdateStaff: " + e.getMessage());
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			result.put(ERROR, true);
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}

	/**
	 * Delete staff.
	 * 
	 * @author hunglm16
	 * @since October 9,2014
	 * @description Update CMS
	 */
	public String deleteStaff() {
		resetToken(result);
		errMsg = "";
		try {
			shopId = currentUser.getShopRoot().getShopId();
			chooseShop = shopMgr.getShopById(shopId);
			VisitPlan visitPlan = superMgr.getVisitPlanById(visitPlanId);
			if (visitPlan == null || !chooseShop.getId().equals(visitPlan.getShop().getId())) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
				return ERROR;
			}
			visitPlan.setStatus(ActiveType.DELETED);
			superMgr.updateVisitPlan(visitPlan);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "Error ManageRouteAssignAction.deleteStaff: " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}

	/***************************** GETTER-SETTER ****************************************/
	private Long shopId;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public List<Staff> getLstSaleStaff() {
		return lstSaleStaff;
	}

	public void setLstSaleStaff(List<Staff> lstSaleStaff) {
		this.lstSaleStaff = lstSaleStaff;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public SuperviserMgr getSuperMgr() {
		return superMgr;
	}

	public void setSuperMgr(SuperviserMgr superMgr) {
		this.superMgr = superMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public CommonMgr getCommonMgr() {
		return commonMgr;
	}

	public void setCommonMgr(CommonMgr commonMgr) {
		this.commonMgr = commonMgr;
	}

	public Long getRoutingId() {
		return routingId;
	}

	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}

	public List<RoutingCustomer> getLstRoutingCustomer() {
		return lstRoutingCustomer;
	}

	public void setLstRoutingCustomer(List<RoutingCustomer> lstRoutingCustomer) {
		this.lstRoutingCustomer = lstRoutingCustomer;
	}

	public Routing getRouting() {
		return routing;
	}

	public void setRouting(Routing routing) {
		this.routing = routing;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Long getSaleStaffId() {
		return saleStaffId;
	}

	public void setSaleStaffId(Long saleStaffId) {
		this.saleStaffId = saleStaffId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getVisitPlanId() {
		return visitPlanId;
	}

	public void setVisitPlanId(Long visitPlanId) {
		this.visitPlanId = visitPlanId;
	}

	public Boolean getIsAllowDelete() {
		return isAllowDelete;
	}

	public void setIsAllowDelete(Boolean isAllowDelete) {
		this.isAllowDelete = isAllowDelete;
	}

	public Integer getIsAllowDuplicateRoute() {
		return isAllowDuplicateRoute;
	}

	public void setIsAllowDuplicateRoute(Integer isAllowDuplicateRoute) {
		this.isAllowDuplicateRoute = isAllowDuplicateRoute;
	}
}

package ths.dms.web.action.superviseshop;

import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.enumtype.VSARole;
import ths.dms.web.utils.LogUtility;

public class ManageCustomerAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5526590480818301193L;
	private StaffMgr staffMgr;
	private SuperviserMgr superviserMgr;
	private CustomerMgr customerMgr;

	private Shop shop;
	private Staff staff;
	private Long customerId;
	private Customer customer;
	private List<MediaItem> listMediaItemStore;
	private List<MediaItem> listMediaItemDisplay;
	private List<MediaItem> listMediaItemSalePoint;
	private MediaItem mediaItemStore;
	private MediaItem mediaItemDisplay;	
	private MediaItem mediaItemSalePoint;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		superviserMgr=(SuperviserMgr) context.getBean("superviserMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		
	}
	
	/**
	 * Kiem tra quyen.
	 *
	 * @return true, if successful
	 * @author tungmt
	 * @since Sep 27, 2012
	 */
	public boolean checkRolesAndGetShop(VSARole... expertRole){
		try {
			
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null)
					shop = staff.getShop();
			}
			if(shop == null) return false;
			return checkRoles(expertRole);
		} catch (Exception e) {
			LogUtility.logError(e, "SearchSaleTransactionAction.checkRolesAndGetShop");
			return false;
		}	
	}

	@Override
	public String execute() {
		resetToken(result);
		return SUCCESS;
	}
	
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<MediaItem> getListMediaItemStore() {
		return listMediaItemStore;
	}

	public void setListMediaItemStore(List<MediaItem> listMediaItemStore) {
		this.listMediaItemStore = listMediaItemStore;
	}

	public List<MediaItem> getListMediaItemDisplay() {
		return listMediaItemDisplay;
	}

	public void setListMediaItemDisplay(List<MediaItem> listMediaItemDisplay) {
		this.listMediaItemDisplay = listMediaItemDisplay;
	}

	public List<MediaItem> getListMediaItemSalePoint() {
		return listMediaItemSalePoint;
	}

	public void setListMediaItemSalePoint(List<MediaItem> listMediaItemSalePoint) {
		this.listMediaItemSalePoint = listMediaItemSalePoint;
	}

	public MediaItem getMediaItemDisplay() {
		return mediaItemDisplay;
	}

	public void setMediaItemDisplay(MediaItem mediaItemDisplay) {
		this.mediaItemDisplay = mediaItemDisplay;
	}

	public MediaItem getMediaItemStore() {
		return mediaItemStore;
	}

	public void setMediaItemStore(MediaItem mediaItemStore) {
		this.mediaItemStore = mediaItemStore;
	}

	public MediaItem getMediaItemSalePoint() {
		return mediaItemSalePoint;
	}

	public void setMediaItemSalePoint(MediaItem mediaItemSalePoint) {
		this.mediaItemSalePoint = mediaItemSalePoint;
	}
	
	
}

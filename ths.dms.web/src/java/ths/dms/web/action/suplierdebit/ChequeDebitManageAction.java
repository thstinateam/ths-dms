package ths.dms.web.action.suplierdebit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.BankMgr;
import ths.dms.core.business.PoMgr;
import ths.dms.core.entities.Bank;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoSecVO;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;



// TODO: Auto-generated Javadoc
/**
 * The Class ChequeDebitManageAction.
 */
public class ChequeDebitManageAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9052225357472171023L;
	

	/** The shop. */
	private Shop shop;
	
	/** The staff. */
	private Staff staff;	
	
	private String fromDate;
	
	private String toDate;
	
	private String poConfirmNumber;
	
	private String soUyNhiem;
	
	private BigDecimal totalMoney;
	
	private Long poVnmId;
	
	private Long debitDetailId;
	
	private Integer type;
	
	private List<StatusBean> listBank;
	
	private Integer bankId;
	/** The staff mgr. */
	//private StaffMgr staffMgr;
	
	/** The debit mgr. */
	//private DebitMgr debitMgr;

	private PoMgr poMgr;
	
	private BankMgr bankMgr;
	/**
	 * Prepare.
	 *
	 * @throws Exception the exception
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		//staffMgr = (StaffMgr) context.getBean("staffMgr");
		//debitMgr = (DebitMgr) context.getBean("debitMgr");	
		poMgr = (PoMgr)context.getBean("poMgr");
		bankMgr = (BankMgr)context.getBean("bankMgr");
	}

	/**
	 * Execute.
	 *
	 * @return the string
	 * @throws Exception the exception
	 * @author tientv
	 */
	@Override
	public String execute() throws Exception {
		actionStartTime = DateUtil.now();
		resetToken(result);		
		try {			
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ChequeDebitManager.execute()"+ e.getMessage(), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	
	/**
	 * @author tientv
	 */
	public String search(){
		try {
			actionStartTime = DateUtil.now();
			resetToken(result);		
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			shopId = shop.getId();
			if (super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}
			
			result.put("rows", new ArrayList<PoSecVO>());
			Date fDate = null;
			Date tDate = null;
			if(!StringUtil.isNullOrEmpty(fromDate)){
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			ObjectVO<PoSecVO> vos = poMgr.getListPoSecVO(null, shop.getId(), poConfirmNumber, fDate, tDate, type);
			if(vos!=null){
				result.put("rows", vos.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ChequeDebitManager.search()"+ e.getMessage(), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	public String getShopInitData(){
		try {
			resetToken(result);
			actionStartTime = DateUtil.now();
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			shopId = shop.getId();
			if (super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}
			
			Date date = null;
			if (shop != null) {
				date = shopLockMgr.getApplicationDate(shop.getId());
			}
			if (date != null) {
				toDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
				fromDate = DateUtil.toDateString(DateUtil.moveDate(date, -1, 2), DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				date = DateUtil.now();
				toDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
				fromDate = DateUtil.toDateString(DateUtil.moveDate(date,-1, 2),DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			// add list bank
			List<Bank> lstbank = bankMgr.getListBankByShopAscBankName(shop.getId());
			listBank = new ArrayList<StatusBean>();
			if(lstbank.size() > 0) {
				for(int i = 0; i< lstbank.size(); i++) {
					StatusBean statusBean = new StatusBean(lstbank.get(i).getId(), lstbank.get(i).getBankName());
					listBank.add(statusBean);
				}
			}
			result.put("listBank", listBank);
			result.put("toDate", toDate);
			result.put("fromDate", fromDate);
			result.put(ERROR, false);
			
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ChequeDebitManager.search()"+ e.getMessage(), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	
	/**
	 * @author tientv
	 */
	
	public String payOrder(){
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			result.put(ERROR, true);
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			shopId = shop.getId();
			if (super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}
			if (type == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT, ""));
				return JSON;
			}
			if (type == 0) { // no
				poMgr.createSec(soUyNhiem, totalMoney, poVnmId, shop.getId(), currentUser.getUserName(),
						debitDetailId, PayReceivedType.THANH_TOAN, bankId);
			} else { // co
				poMgr.createSecGiamNo(soUyNhiem, totalMoney, poVnmId, shop.getId(), currentUser.getUserName(), debitDetailId, bankId);
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			if ("So tien nhap vao vuot qua no don hang".equals(e.getMessage())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "suplier.debit.manage.exceed.money.input"));
			} else if ("debitDetailId not exists".equals(e.getMessage())) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "suplier.debit.manage.exceed"));
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			LogUtility.logErrorStandard(e, "ChequeDebitManager.payOrder()" + e.getMessage(), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	public String getPoConfirmNumber() {
		return poConfirmNumber;
	}

	public void setPoConfirmNumber(String poConfirmNumber) {
		this.poConfirmNumber = poConfirmNumber;
	}

	public String getSoUyNhiem() {
		return soUyNhiem;
	}

	public void setSoUyNhiem(String soUyNhiem) {
		this.soUyNhiem = soUyNhiem;
	}

	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	/**
	 * Gets the shop.
	 *
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Sets the shop.
	 *
	 * @param shop the new shop
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	
	/**
	 * @author tientv
	 */
	public Staff getStaff() {
		return staff;
	}

	/**
	 * @author tientv
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Long getPoVnmId() {
		return poVnmId;
	}

	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}

	public Long getDebitDetailId() {
		return debitDetailId;
	}

	public void setDebitDetailId(Long debitDetailId) {
		this.debitDetailId = debitDetailId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public List<StatusBean> getListBank() {
		return listBank;
	}

	public void setListBank(List<StatusBean> listBank) {
		this.listBank = listBank;
	}

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
}

package ths.dms.web.action.suplierdebit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DebitType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.vo.DebitShopVO;
import ths.dms.core.exceptions.ExceptionCode;

public class SuplierDebitManageAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9052225357472171023L;
	

	private Shop shop;
	private Staff staff;
	private BigDecimal currentDebit;//so no hien tai
	private BigDecimal currentDebitVNMToNPP;//so no hien tai VNM voi NPP
	private String invoiceNumber;//so hoa don
	private List<StatusBean> listInvoiceType;//chon loai hoa don
	private long invoiceTypeChoice;//loai hoa don nguoi dung chon
	private BigDecimal money;
	private BigDecimal moneyItem;
	private List<DebitShopVO> listDebitShop;//danh sach no cua NPP
	private long reasonChoice;//ly do cap nhat nguoi dung chon
	private BigDecimal remainAfterPaid;//so tien con lai sau khi click xem phan bo
	private String message;//
	private boolean isPageLoad = true;
	private String lockDate;
	private List<Long> listDebitId;
	private String shopCode;
	
	StaffMgr staffMgr;
	DebitMgr debitMgr;
	ShopMgr shopMgr;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		actionStartTime = DateUtil.now();
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		debitMgr = (DebitMgr) context.getBean("debitMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);		
		try {
			if(isShopLocked()){
				return SHOP_LOCK;
			}
//			if(currentUser != null && currentUser.getShopRoot().getShopId() != null){
//				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
//			}
//			invoiceNumber = debitMgr.getPayReceivedStringSugguest(dayLock);
//			currentDebit = debitMgr.getCurrentDebit(shop.getId(), DebitOwnerType.SHOP, DebitType.THU); //TUNGTT
//			currentDebitVNMToNPP = debitMgr.getCurrentDebit(shop.getId(), DebitOwnerType.SHOP, DebitType.CHI).multiply(new BigDecimal(-1)); //TUNGTT
//			
//			Date date = shopLockMgr.getApplicationDate(shop.getId());
//			if (date != null) {
//				lockDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
//			} else {
//				lockDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
//			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logErrorStandard(e, e.getMessage(), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}
	
	public String watchDistribute(){
		try {
			resetToken(result);
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			shopId = shop.getId();
			if (super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}
			currentDebit = debitMgr.getCurrentDebit(shop.getId(), DebitOwnerType.SHOP, DebitType.THU);
			currentDebitVNMToNPP = debitMgr.getCurrentDebit(shop.getId(), DebitOwnerType.SHOP, DebitType.CHI).multiply(new BigDecimal(-1)); //TUNGTT
			Date date = shopLockMgr.getApplicationDate(shop.getId());
			invoiceNumber = debitMgr.getPayReceivedStringSugguest(date);
			if (date != null) {
				lockDate = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				lockDate = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			
			listDebitShop = new ArrayList<DebitShopVO>();
			result.put("errMsg", "");
			result.put("rows", listDebitShop);
			result.put("currentDebit", currentDebit);
			result.put("currentDebitVNMToNPP", currentDebitVNMToNPP);
			result.put("lockDate", lockDate);
			result.put("invoiceNumber", invoiceNumber);
			
			if(invoiceTypeChoice == ConstantManager.INVOICE_TYPE_PAID) {
				
				if(money == null || money.doubleValue() <= 0){
					if (isPageLoad) {
						List<DebitShopVO> _listDebitShop = debitMgr.getDebitVNMOfShop(shop.getId(), invoiceTypeChoice);
						result.put("isPageLoad", true);
						result.put("rows", _listDebitShop);
					}
					return JSON;
				}
				
				moneyItem = BigDecimal.ZERO;
				List<DebitShopVO> _listDebitShop = debitMgr.getListDebitShopVO(shop.getId(), PoType.PO_CONFIRM);
				BigDecimal moneyRemain = money;
				
				remainAfterPaid = new BigDecimal(0);
				for(DebitShopVO dSVO : _listDebitShop) {
					if (moneyRemain.compareTo(dSVO.getRemain()) > 0) {
						dSVO.setUserPaid(dSVO.getRemain());
						dSVO.setRemainAfterPaid(new BigDecimal(0));
						moneyRemain = moneyRemain.subtract(dSVO.getRemain());
						BigDecimal bg = dSVO.getRemain();
						moneyItem = moneyItem.add(bg);
						listDebitShop.add(dSVO);
					} else if(moneyRemain.compareTo(dSVO.getRemain()) == 0) {
						dSVO.setUserPaid(dSVO.getRemain());
						dSVO.setRemainAfterPaid(new BigDecimal(0));
						moneyRemain = new BigDecimal(0);
						BigDecimal bg = dSVO.getRemain();
						moneyItem = moneyItem.add(bg);
						listDebitShop.add(dSVO);
					} else {
						if(moneyRemain.compareTo(new BigDecimal(0)) > 0) {
							listDebitShop.add(dSVO);
						}
						dSVO.setUserPaid(moneyRemain);
						dSVO.setRemainAfterPaid(dSVO.getRemain().subtract(moneyRemain));
						/*if(!moneyRemain.equals(BigDecimal.ZERO)){
							BigDecimal bg = dSVO.getRemain().subtract(moneyRemain);
							moneyItem = moneyItem.add(bg);
						}*/
						BigDecimal bg = dSVO.getRemain();
						moneyItem = moneyItem.add(bg);
						moneyRemain = new BigDecimal(0);
					}
				}
				if(moneyRemain.compareTo(new BigDecimal(0)) > 0) {					
					BigDecimal _errMoney = moneyRemain.subtract(money);
					if(_errMoney.compareTo(BigDecimal.ZERO) == 0) {
						/**khong con don hang tra nao*/
						listDebitShop = new ArrayList<DebitShopVO>();
						result.put("rows", listDebitShop);
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_SUPLIER_DONT_HAVE_INCOME_ORDER));
						return JSON;
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_SUPLIER_PAYMENT_MONEY, convertMoney((BigDecimal)displayPositiveNumber(_errMoney))));
						listDebitShop = new ArrayList<DebitShopVO>();
						result.put("rows", listDebitShop);
						return JSON;
					}
				}
				remainAfterPaid = moneyItem.subtract(money);
			} else {
				/**loai chung tu la nhan tien khi tra hang*/				
				if(money == null || money.doubleValue() <= 0){
					if (isPageLoad) {
						List<DebitShopVO> _listDebitShop = debitMgr.getDebitVNMOfShop(shop.getId(), invoiceTypeChoice);
						result.put("isPageLoad", true);
						result.put("rows", _listDebitShop);
					}
					return JSON;
				}
				moneyItem = BigDecimal.ZERO;
				List<DebitShopVO> _listDebitShop = debitMgr.getListDebitShopVO(shop.getId(), PoType.RETURNED_SALES_ORDER);				
				BigDecimal moneyRemain = BigDecimal.ZERO.subtract(money);
				
				remainAfterPaid = new BigDecimal(0);				
				for(DebitShopVO dSVO : _listDebitShop) {
					if(moneyRemain.compareTo(dSVO.getRemain()) < 0) {
						dSVO.setUserPaid(dSVO.getRemain());
						dSVO.setRemainAfterPaid(new BigDecimal(0));
						moneyRemain = moneyRemain.subtract(dSVO.getRemain());
						BigDecimal bg = dSVO.getRemain();
						moneyItem = moneyItem.add(bg);
						listDebitShop.add(dSVO);
					} else if(moneyRemain.compareTo(dSVO.getRemain()) == 0) {
						dSVO.setUserPaid(dSVO.getRemain());
						dSVO.setRemainAfterPaid(new BigDecimal(0));
						moneyRemain = new BigDecimal(0);
						BigDecimal bg = dSVO.getRemain();
						moneyItem = moneyItem.add(bg);
						listDebitShop.add(dSVO);
					} else {
						if(moneyRemain.compareTo(new BigDecimal(0)) < 0) {
							listDebitShop.add(dSVO);
						}
						dSVO.setUserPaid(moneyRemain);
						dSVO.setRemainAfterPaid(dSVO.getRemain().subtract(moneyRemain));
						/*if(!moneyRemain.equals(BigDecimal.ZERO)){
							BigDecimal bg = dSVO.getRemain().subtract(moneyRemain);
							moneyItem = BigDecimal.ZERO.subtract(moneyItem.add(bg));
						}*/
						BigDecimal bg = dSVO.getRemain();
						moneyItem = moneyItem.add(bg);
						moneyRemain = new BigDecimal(0);
					}
				}				
				if(moneyRemain.compareTo(new BigDecimal(0)) < 0) {
					BigDecimal _errMoney = moneyRemain.add(money);
					if(_errMoney.compareTo(BigDecimal.ZERO) == 0) {
						/** Khong co don hang tra nao */
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_SUPLIER_DONT_HAVE_RETURN_ORDER));
						listDebitShop = new ArrayList<DebitShopVO>();
						result.put("rows", listDebitShop);
						return JSON;
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_SUPLIER_RECEIVE_MONEY, convertMoney((BigDecimal)displayPositiveNumber(_errMoney))));
						listDebitShop = new ArrayList<DebitShopVO>();
						result.put("rows", listDebitShop);
						return JSON;
					}
				}
				remainAfterPaid = BigDecimal.ZERO.subtract(moneyItem).subtract(money);	
				/*remainAfterPaid = moneyItem;*/
			}
			result.put("rows", listDebitShop);
			result.put("remainAfterPaid", remainAfterPaid);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			listDebitShop = new ArrayList<DebitShopVO>();
			result.put("rows", listDebitShop);
			LogUtility.logErrorStandard(e, "Error SuplierDebitManageAction.watchDistribute", createLogErrorStandard(actionStartTime));
			return ERROR;
		}
		return JSON;
	}

	public String paidDebit() {
		resetToken(result);
		try {
			listDebitShop = new ArrayList<DebitShopVO>();
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			shopId = shop.getId();
			if (super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}
			
			if(invoiceNumber ==  null || invoiceNumber.equals("")) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, "Số chứng từ"));
				return ERROR;
			}
			
			if(invoiceTypeChoice == ConstantManager.INVOICE_TYPE_PAID) {
				List<DebitShopVO> _listDebitShop = debitMgr.getListDebitShopVO(shop.getId(), PoType.PO_CONFIRM);

				BigDecimal moneyRemain = new BigDecimal(0);
				
				if(money != null && money.doubleValue() > 0){
					moneyRemain = money;
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_SUPLIER_INVALID_MONEY));
					return ERROR;
				}
				
				remainAfterPaid = new BigDecimal(0);
				
				List<Long> listDebitId = new ArrayList<Long>();
				List<BigDecimal> listPaidAmount = new ArrayList<BigDecimal>();
				
				for(DebitShopVO dSVO : _listDebitShop) {
					if(moneyRemain.compareTo(dSVO.getRemain()) > 0) {
						dSVO.setUserPaid(dSVO.getRemain());
						dSVO.setRemainAfterPaid(new BigDecimal(0));
						moneyRemain = moneyRemain.subtract(dSVO.getRemain());
						listDebitId.add(dSVO.getDebitId());
						listPaidAmount.add(dSVO.getRemain());
					} else if(moneyRemain.compareTo(dSVO.getRemain()) == 0) {
						dSVO.setUserPaid(dSVO.getRemain());
						dSVO.setRemainAfterPaid(new BigDecimal(0));
						moneyRemain = new BigDecimal(0);
						listDebitId.add(dSVO.getDebitId());
						listPaidAmount.add(dSVO.getRemain());
					} else {
						if(moneyRemain.compareTo(new BigDecimal(0)) > 0) {
							listDebitId.add(dSVO.getDebitId());
							listPaidAmount.add(moneyRemain);
						}
						dSVO.setUserPaid(moneyRemain);
						dSVO.setRemainAfterPaid(dSVO.getRemain().subtract(moneyRemain));
						remainAfterPaid = remainAfterPaid.add(dSVO.getRemainAfterPaid());
						moneyRemain = new BigDecimal(0);
					}
				}
				
				if(moneyRemain.compareTo(new BigDecimal(0)) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_SUPLIER_MONEY_ERR));
					return ERROR;
				}
				
				if(listDebitId.isEmpty() || listPaidAmount.isEmpty() || listDebitId.size() != listPaidAmount.size()) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_USER_INPUT));
					return ERROR;
				}
				
				debitMgr.updateDebitOfShop(shop.getId(), invoiceNumber, money, listDebitId, listPaidAmount, ReceiptType.PAID, currentUser.getUserName());
			} else {
				List<DebitShopVO> _listDebitShop = debitMgr.getListDebitShopVO(shop.getId(), PoType.RETURNED_SALES_ORDER);

				BigDecimal moneyRemain = new BigDecimal(0);
				
				if(money != null && money.doubleValue() > 0){
					moneyRemain = new BigDecimal(0).subtract(money);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_SUPLIER_INVALID_MONEY));
					return ERROR;
				}
				
				remainAfterPaid = new BigDecimal(0);
				
				List<Long> listDebitId = new ArrayList<Long>();
				List<BigDecimal> listPaidAmount = new ArrayList<BigDecimal>();
				
				for(DebitShopVO dSVO : _listDebitShop) {
					if(moneyRemain.compareTo(dSVO.getRemain()) < 0) {
						dSVO.setUserPaid(dSVO.getRemain());
						dSVO.setRemainAfterPaid(new BigDecimal(0));
						moneyRemain = moneyRemain.subtract(dSVO.getRemain());
						listDebitId.add(dSVO.getDebitId());
						listPaidAmount.add(dSVO.getRemain());
					} else if(moneyRemain.compareTo(dSVO.getRemain()) == 0) {
						dSVO.setUserPaid(dSVO.getRemain());
						dSVO.setRemainAfterPaid(new BigDecimal(0));
						moneyRemain = new BigDecimal(0);
						listDebitId.add(dSVO.getDebitId());
						listPaidAmount.add(dSVO.getRemain());
					} else {
						if(moneyRemain.compareTo(new BigDecimal(0)) < 0) {
							listDebitId.add(dSVO.getDebitId());
							listPaidAmount.add(moneyRemain);
						}
						dSVO.setUserPaid(moneyRemain);
						dSVO.setRemainAfterPaid(dSVO.getRemain().subtract(moneyRemain));
						remainAfterPaid = remainAfterPaid.add(dSVO.getRemainAfterPaid());
						moneyRemain = new BigDecimal(0);
					}
				}
				
				if(moneyRemain.compareTo(new BigDecimal(0)) > 0) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_DEBIT_SUPLIER_MONEY_ERR));
					return ERROR;
				}
				
				if(listDebitId.isEmpty() || listPaidAmount.isEmpty() || listDebitId.size() != listPaidAmount.size()) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_USER_INPUT));
					return ERROR;
				}
				
				debitMgr.updateDebitOfShop(shop.getId(), invoiceNumber, new BigDecimal(0).subtract(money), listDebitId, listPaidAmount, ReceiptType.RECEIVED, currentUser.getUserName());
			}
			
			currentDebit = debitMgr.getDebitOfShop(shop.getId());
			result.put(ERROR, false);
			result.put("currentDebit", currentDebit);
			
			return SUCCESS;
		}
		catch (Exception e) {
			LogUtility.logErrorStandard(e, "Error SuplierDebitManageAction.paidDebit", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			if(e.getMessage().equals(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN)){
				result.put("errorReloadPage", true);
			} else{
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
			return ERROR;
		}
	}
	
	public String exportExcelDebit(){
		try{
			staff = getStaffByCurrentUser();
			result.put(ERROR, true);
			if (staff == null || staff.getShop() == null) {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}

			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			
			if (shop == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
				return JSON;
			}
			shopId = shop.getId();
			if (super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}
			
			listDebitShop = debitMgr.getDebitVNMOfShopAndId(shop.getId(), listDebitId);
			if(listDebitShop != null && listDebitShop.size() >0){
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathShopDebit() + ConstantManager.TEMPLATE_SHOP_DEBIT;
				templateFileName = templateFileName.replace('/', File.separatorChar);
				
				String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_SHOP_DEBIT;
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("shopName", shop.getShopName());
				params.put("date", DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR));
				params.put("listDebit", listDebitShop);
				InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
				inputStream.close();
				OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				os.close();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put("hasData", true);
				result.put(REPORT_PATH, outputPath);
			}else{
				result.put("hasData", false);
			}
			result.put(ERROR, false);
		}catch(Exception ex){
			LogUtility.logErrorStandard(ex, "SuplierDebit.exportExcelDebit: "+ ex.getMessage(), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public BigDecimal getCurrentDebit() {
		return currentDebit;
	}

	public void setCurrentDebit(BigDecimal currentDebit) {
		this.currentDebit = currentDebit;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public List<StatusBean> getListInvoiceType() {
		listInvoiceType = new ArrayList<StatusBean>();
		StatusBean invoiceTypePaid = new StatusBean(ConstantManager.INVOICE_TYPE_PAID, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "invoice.type.paid"));
		StatusBean invoiceTypeReceived = new StatusBean(ConstantManager.INVOICE_TYPE_RECEIVED, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "invoice.type.received"));
		listInvoiceType.add(invoiceTypePaid);
		listInvoiceType.add(invoiceTypeReceived);
		return listInvoiceType;
	}

	public void setListInvoiceType(List<StatusBean> listInvoiceType) {
		this.listInvoiceType = listInvoiceType;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getCurrentDate() {
		return DateUtil.toDateString(new Date(), DateUtil.DATE_FORMAT_DDMMYYYY);
	}

	public void setListDebitShop(List<DebitShopVO> listDebitShop) {
		this.listDebitShop = listDebitShop;
	}

	public List<DebitShopVO> getListDebitShop() {
		return listDebitShop;
	}

	public void setRemainAfterPaid(BigDecimal remainAfterPaid) {
		this.remainAfterPaid = remainAfterPaid;
	}

	public BigDecimal getRemainAfterPaid() {
		return remainAfterPaid;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setReasonChoice(long reasonChoice) {
		this.reasonChoice = reasonChoice;
	}

	public long getReasonChoice() {
		return reasonChoice;
	}

	public long getInvoiceTypeChoice() {
		return invoiceTypeChoice;
	}

	public void setInvoiceTypeChoice(long invoiceTypeChoice) {
		this.invoiceTypeChoice = invoiceTypeChoice;
	}

	public boolean isPageLoad() {
		return isPageLoad;
	}

	public void setPageLoad(boolean isPageLoad) {
		this.isPageLoad = isPageLoad;
	}

	public String getLockDate() {
		return lockDate;
	}

	public void setLockDate(String lockDate) {
		this.lockDate = lockDate;
	}

	public BigDecimal getCurrentDebitVNMToNPP() {
		return currentDebitVNMToNPP;
	}

	public void setCurrentDebitVNMToNPP(BigDecimal currentDebitVNMToNPP) {
		this.currentDebitVNMToNPP = currentDebitVNMToNPP;
	}

	public List<Long> getListDebitId() {
		return listDebitId;
	}

	public void setListDebitId(List<Long> listDebitId) {
		this.listDebitId = listDebitId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
}

package ths.dms.web.action.suplierdebit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IsFreeItemInSaleOderDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderDetailFilter;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderIsAttr;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Action: Dieu chinh doanh thu
 * 
 * @author hunglm16
 * @since September 08, 2014
 */
public class AdjustedRevenueAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private CustomerMgr customerMgr;
	private DebitMgr debitMgr;
	private SaleOrderMgr saleOrderMgr;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		debitMgr = (DebitMgr) context.getBean("debitMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		lstShopVO = new ArrayList<ShopVO>();
		lstShopVO = this.getListShopIsLevel5Current();
		/** Xu ly lay shopRoot @author hunglm16 **/
		shopVO = new ShopVO();
		shopVO.setShopId(currentUser.getShopRoot().getShopId());
		shopVO.setShopCode(currentUser.getShopRoot().getShopCode());
		shopVO.setShopName(currentUser.getShopRoot().getShopName());
		shopVO.setParentId(currentUser.getShopRoot().getParentId());
		shopVO.setObjectType(currentUser.getShopRoot().getObjectType());
		shopVO.setIsLevel(currentUser.getShopRoot().getIsLevel());

		return SUCCESS;
	}

	/**
	 * Load trang Lap phieu dieu chinh don hang
	 * 
	 * @author hunglm16
	 * @since September 8,2014
	 * @description Lap phieu dieu chinh (AI, AD)
	 * */
	public String viewSaleOrderAIADInsertOrUpdate() {
		lstShopVO = new ArrayList<ShopVO>();
		lstShopVO = this.getListShopIsLevel5Current();
		/** Xu ly lay shopRoot @author hunglm16 **/
		shopVO = new ShopVO();
		shopVO.setShopId(currentUser.getShopRoot().getShopId());
		shopVO.setShopCode(currentUser.getShopRoot().getShopCode());
		shopVO.setShopName(currentUser.getShopRoot().getShopName());
		shopVO.setParentId(currentUser.getShopRoot().getParentId());
		shopVO.setObjectType(currentUser.getShopRoot().getObjectType());
		shopVO.setIsLevel(currentUser.getShopRoot().getIsLevel());
		saleOrder = new SaleOrder();
		title = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.dcdt.change.title.insert");
		if (id != null) {
			try {
				saleOrder = saleOrderMgr.getSaleOrderById(id);
				if (saleOrder != null) {
					if (!SaleOrderStatus.NOT_YET_APPROVE.getValue().equals(saleOrder.getApproved().getValue()) || !checkShopInOrgAccessById(saleOrder.getShop().getId())) {
						return PAGE_NOT_PERMISSION;
					} else {
						title = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.dcdt.change.title.edit");

					}
				} else {
					return PAGE_NOT_FOUND;
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return SUCCESS;
	}

	/**
	 * Tim kiem don hang
	 * 
	 * @author hunglm16
	 * @since September 8,2014
	 * @description Tim kiem Don tang giam (AI, AD)
	 * */
	public String searchSaleOrderAIAD() {
		result.put("page", page);
		result.put("max", max);
		try {
			SaleOrderFilter<SaleOrderVO> filter = new SaleOrderFilter<SaleOrderVO>();
			KPaging<SaleOrderVO> kPaging = new KPaging<SaleOrderVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(shopCode);

				if (!StringUtil.isNullOrEmpty(oderNumber)) {
					filter.setSaleOderNumber(oderNumber);
				}
				if (!StringUtil.isNullOrEmpty(orderType) && !orderType.trim().toUpperCase().equals("ISALL")) {
					filter.setOrderTypeStr(orderType);
				}
				if (!StringUtil.isNullOrEmpty(isJoinOderNumber)) {
					filter.setIsJoinSaleOderNumber(isJoinOderNumber);
				}
				if (!StringUtil.isNullOrEmpty(fromDateStr)) {
					filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
				}
				if (!StringUtil.isNullOrEmpty(toDateStr)) {
					filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
				}
				if (approved != null && approved > -1) {
					filter.setStatus(approved);
				}

				ObjectVO<SaleOrderVO> data = saleOrderMgr.getListSaleOrderAIADVOByFilter(filter);
				if (data != null && !data.getLstObject().isEmpty()) {
					result.put("total", data.getkPaging().getTotalRows());
					result.put("rows", data.getLstObject());
				} else {
					result.put("total", 0);
					result.put("rows", new ArrayList<SaleOrderVO>());
				}
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<SaleOrderVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem chi tiet don hang
	 * 
	 * @author hunglm16
	 * @since September 8,2014
	 * @description Tim kiem Chi tiet Don tang giam (AI, AD)
	 * */
	public String searchSaleOrderDetailAIAD() {
		try {
			SaleOrderDetailFilter<SaleOrderDetailVOEx> filter = new SaleOrderDetailFilter<SaleOrderDetailVOEx>();
			boolean flag = false;
			if (saleOrderId != null) {
				filter.setSaleOrderId(saleOrderId);
				flag = true;
			}
			if (!StringUtil.isNullOrEmpty(orderNumber)) {
				filter.setOrderNumber(orderNumber);
				flag = true;
			}
			if (!StringUtil.isNullOrEmpty(productCode)) {
				filter.setProductCode(productCode);
			}
			if (!StringUtil.isNullOrEmpty(productName)) {
				filter.setProductName(productName);
			}
			if (!StringUtil.isNullOrEmpty(textG) && isAttr != null) {
				List<String> lstPrdCode = new ArrayList<String>();
				String[] arrCode = textG.split("ESC");
				if (arrCode.length > 0) {
					for (String value : arrCode) {
						lstPrdCode.add(value);
					}
					filter.setIsArrLongId(isAttr);
					filter.setArrStr(lstPrdCode);
				}
			}
			if (flag) {
				ObjectVO<SaleOrderDetailVOEx> data = saleOrderMgr.getListSaleOrderDetailVOByFilter(filter);
				if (data != null && !data.getLstObject().isEmpty()) {
					result.put("rows", data.getLstObject());
				} else {
					result.put("rows", new ArrayList<SaleOrderVO>());
				}
			} else {
				result.put("rows", new ArrayList<SaleOrderDetailVOEx>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tim kiem chi tiet don hang
	 * 
	 * @author hunglm16
	 * @since September 8,2014
	 * @description Tim kiem Chi tiet Don tang giam (AI, AD)
	 * */
	public String searchSaleOrderIsJoinDetail() {
		try {
			SaleOrderDetailFilter<SaleOrderDetailVOEx> filter = new SaleOrderDetailFilter<SaleOrderDetailVOEx>();
			boolean flag = false;
			//Chi lay don hang ban
			filter.setIsFreeItem(IsFreeItemInSaleOderDetail.SALE.getValue());

			if (saleOrderId != null) {
				filter.setSaleOrderId(saleOrderId);
				flag = true;
			}
			if (!StringUtil.isNullOrEmpty(orderNumber)) {
				filter.setOrderNumber(orderNumber);
				flag = true;
			}
			if (!StringUtil.isNullOrEmpty(productCode)) {
				filter.setProductCode(productCode);
			}
			if (!StringUtil.isNullOrEmpty(productName)) {
				filter.setProductName(productName);
			}
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setType(SaleOrderType.NOT_YET_RETURNED.getValue());
			filter.setIsAttr(SaleOrderIsAttr.AIORAD.getValue());

			if (!StringUtil.isNullOrEmpty(textG) && isAttr != null) {
				List<String> lstPrdCode = new ArrayList<String>();
				String[] arrCode = textG.split("ESC");
				if (arrCode.length > 0) {
					for (String value : arrCode) {
						lstPrdCode.add(value);
					}
					filter.setIsArrLongId(isAttr);
					filter.setArrStr(lstPrdCode);
				}
			}
			if (flag) {
				ObjectVO<SaleOrderDetailVOEx> data = saleOrderMgr.getListSaleOrderDetailVOByFilter(filter);
				if (data != null && !data.getLstObject().isEmpty()) {
					result.put("rows", data.getLstObject());
				} else {
					result.put("rows", new ArrayList<SaleOrderVO>());
				}
			} else {
				result.put("rows", new ArrayList<SaleOrderDetailVOEx>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Xoa Don Hang
	 * 
	 * @author hunglm16
	 * @since September 8,2014
	 * @description Xoa Don tang giam (AI, AD)
	 * */
	public String deleteSaleOrderAIAD() {
		try {
			String msg = "";
			if (id != null) {
				SaleOrder saleOrder = new SaleOrder();
				saleOrder = saleOrderMgr.getSaleOrderById(id);
				if (saleOrder != null) {
					if (checkShopInOrgAccessById(saleOrder.getShop().getId())) {
						if (!SaleOrderStatus.NOT_YET_APPROVE.getValue().equals(saleOrder.getApproved().getValue())) {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.dcdt.err.del.isdefined.notstop");
						} else {
							saleOrderMgr.deleteSaleOrderAIAD(saleOrder);
						}
					} else {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.dcdt.err.del.isdefined");
					}
				} else {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.dcdt.err.del.isdefined.notshop");
				}
			} else {
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.isnotnull.shopcn.shop");
			}
			if (msg.length() > 0) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * Search sale order.
	 * 
	 * @author hunglm16
	 * @description Cap nhat theo phan quyen CMS
	 * @since September 10,2014
	 */
	public String searchDialogSaleOrder() throws Exception {
		result.put("page", page);
		result.put("max", max);
		try {
			if (shopId == null || shopId <= 0 && !checkShopInOrgAccessByIdWithIsLevel(shopId, 5)) {
				/**
				 * Kiem tra Don vi duoc chon co la NPP va ton tai trong CMS @author
				 * hunglm16
				 **/
				result.put("total", 0);
				result.put("rows", new ArrayList<SaleOrder>());
				return JSON;
			}
			SaleOrderFilter<SaleOrder> filter = new SaleOrderFilter<SaleOrder>();
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);

			filter.setShopId(shopId);

			Date lockDate = shopLockMgr.getApplicationDate(shopId);

			Date endDate = null;
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, ConstantManager.FULL_DATE_FORMAT));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				endDate = DateUtil.parse(toDateStr, ConstantManager.FULL_DATE_FORMAT);
				if (endDate != null && DateUtil.compareDateWithoutTime(endDate, lockDate) == 1) {
					filter.setToDate(lockDate);
				} else {
					filter.setToDate(endDate);
				}
			}
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setType(SaleOrderType.NOT_YET_RETURNED.getValue());

			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setShortCode(shortCode);
			}
			if (!StringUtil.isNullOrEmpty(customerName)) {
				filter.setCustomerName(customerName);
			}
			if (customerId != null) {
				filter.setCustomerId(customerId);
			}
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				filter.setStaffCode(staffCode);
			}
			if (!StringUtil.isNullOrEmpty(orderNumber)) {
				filter.setOrderNumber(orderNumber);
			}
			if (!StringUtil.isNullOrEmpty(orderType)) {
				filter.setOrderType(OrderType.parseValue(orderType));
			}
			if (!StringUtil.isNullOrEmpty(deliveryCode)) {
				filter.setDeliveryCode(deliveryCode);
			}
			ObjectVO<SaleOrder> objectVOSaleOrder = saleOrderMgr.getListSaleOrderExByFilter(filter, null);
			if (objectVOSaleOrder != null && objectVOSaleOrder.getLstObject() != null) {
				result.put("total", objectVOSaleOrder.getkPaging().getTotalRows());
				result.put("rows", objectVOSaleOrder.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<SaleOrder>());
			}

		} catch (BusinessException ex) {
			LogUtility.logError(ex, "Error ReturnProductOrderAction.searchSaleOrder - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Create Or Update SalaOrder(AI, AD)
	 * 
	 * @author hunglm16
	 * @since September 11,2014
	 * @description Them moi hoac cap nhat thong tin cua Phieu dieu chinh doanh
	 *              thu
	 * */
	public String createOrUpdateSaleOrderAIAD() {
		errMsg = "";
		String msg = "";
		try {
			if (flag != null && flag) {
				SaleOrder saleOrderNew = new SaleOrder();
				//THEM MOI PHIEU DIEU CHINH DOANH THU
				if (shopId == null) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.undefined.shop");
				} else if (!checkShopInOrgAccessById(shopId)) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.undefined.shop.cms");
				}
				if (msg.length() == 0) {
					if (StringUtil.isNullOrEmpty(isJoinOderNumber)) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.isJoinOderNumber");
					} else {
						SaleOrder saleOrderIsJoin = new SaleOrder();
						saleOrderIsJoin = saleOrderMgr.getSaleOrderByOrderNumber(isJoinOderNumber, shopId);
						if (saleOrderIsJoin == null) {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.isJoinOderNumber");
						} else if (!SaleOrderType.NOT_YET_RETURNED.getValue().equals(saleOrderIsJoin.getType().getValue())) {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.isJoinOderNumber.this");
						} else if (!shopId.equals(saleOrderIsJoin.getShop().getId())) {
							msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.isshop.cms.from");
						} else {
							saleOrderNew.setFromSaleOrder(saleOrderIsJoin);
						}
					}
				}
				if (msg.length() == 0 && !StringUtil.isNullOrEmpty(typeStr) && (typeStr.trim().toUpperCase().equals(OrderType.AI) || !typeStr.trim().toUpperCase().equals(OrderType.AD))) {
					saleOrderNew.setOrderType(OrderType.parseValue(typeStr));
				} else if (msg.length() == 0) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.ordertype.this");
				}

				if (msg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}

				if (!StringUtil.isNullOrEmpty(description)) {
					saleOrderNew.setDescription(description);
				}
				saleOrderNew.setCustomer(saleOrderNew.getFromSaleOrder().getCustomer());
				saleOrderNew.setStaff(saleOrderNew.getFromSaleOrder().getStaff());
				saleOrderNew.setShop(shopMgr.getShopById(shopId));
				saleOrderNew.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
				saleOrderNew.setApprovedStep(SaleOrderStep.CONFIRMED);
				saleOrderNew.setCreateUser(currentUser.getUserName());
				saleOrderNew.setCreateDate(new Date());
				saleOrderNew.setOrderDate(dayLock);
				Long id = saleOrderMgr.createSaleOrderAIAD(saleOrderNew, arrStrInsertData);
				result.put("saleOrderId", id);
			} else if (flag != null) {
				SaleOrder saleOrderUpdate = new SaleOrder();
				//CAP NHAT PHIEU DIEU CHINH DOANH THU
				if (saleOrderId == null) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.undefined");
				} else {
					saleOrderUpdate = saleOrderMgr.getSaleOrderById(saleOrderId);
					if (saleOrderUpdate == null) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.undefined");
					}
//					else if (!saleOrderUpdate.getApproved().getValue().equals(SaleOrderStatus.NOT_YET_APPROVE.getValue())) {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.update.this");
//					}
				}
//				if (approved != null && approved > -1 && approved < 2) {
//					saleOrderUpdate.setApproved(SaleOrderStatus.parseValue(approved));
//				} else {
//					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.dcdt.err.del.isdefined.notstatus");
//				}
				if (msg.length() > 0) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}

				if (!StringUtil.isNullOrEmpty(description)) {
					saleOrderUpdate.setDescription(description);
				}

				saleOrderUpdate.setUpdateDate(new Date());
				saleOrderUpdate.setUpdateUser(currentUser.getUserName());
				//saleOrderUpdate.setApproved(SaleOrderStatus.parseValue(value));

				arrLong = new ArrayList<Long>();
				if (!StringUtil.isNullOrEmpty(arrStrDeleteData)) {
					String[] arrDeleteData = arrStrDeleteData.split("ESC");
					if (arrDeleteData.length > 0) {
						for (String value : arrDeleteData) {
							arrLong.add(Long.valueOf(value));
						}
					}
				}
				saleOrderMgr.updateSaleOrderAIAD(saleOrderUpdate, arrStrInsertData, arrLong, arrStrUpdateData);
			} else {
				msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "saleorder.aiad.err.undefined.flag");
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	///////// ** GETTER/SETTER **///////////////
	private List<ShopVO> lstShopVO = new ArrayList<ShopVO>();
	private List<SaleOrder> listSaleOrder;
	private List<Long> arrLong;
	private List<String> arrStr;

	private SaleOrder saleOrder;
	private ShopVO shopVO;

	private Long id;
	private Long saleOrderId;
	private Long customerId;
	private Long shopId;

	private Integer approved;
	private Integer type;

	private Boolean flag;
	private Boolean isAttr;

	private String shopCode;
	private String orderType;
	private String fromDateStr;
	private String toDateStr;
	private String oderNumber;
	private String isJoinOderNumber;
	private String title;
	private String statusStr;
	private String typeStr;
	private String shortCode;
	private String customerName;
	private String staffCode;
	private String orderNumber;
	private String deliveryCode;
	private String arrStrInsertData;
	private String arrStrUpdateData;
	private String description;
	private String productCode;
	private String productName;
	private String textG;
	private String arrStrDeleteData;

	public String getArrStrDeleteData() {
		return arrStrDeleteData;
	}

	public void setArrStrDeleteData(String arrStrDeleteData) {
		this.arrStrDeleteData = arrStrDeleteData;
	}

	public List<String> getArrStr() {
		return arrStr;
	}

	public String getTextG() {
		return textG;
	}

	public void setTextG(String textG) {
		this.textG = textG;
	}

	public Boolean getIsAttr() {
		return isAttr;
	}

	public void setIsAttr(Boolean isAttr) {
		this.isAttr = isAttr;
	}

	private List<SaleOrderDetail> listSaleOrderDetail = new ArrayList<SaleOrderDetail>();

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public List<SaleOrderDetail> getListSaleOrderDetail() {
		return listSaleOrderDetail;
	}

	public void setListSaleOrderDetail(List<SaleOrderDetail> listSaleOrderDetail) {
		this.listSaleOrderDetail = listSaleOrderDetail;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<Long> getArrLong() {
		return arrLong;
	}

	public void setArrLong(List<Long> arrLong) {
		this.arrLong = arrLong;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArrStrInsertData() {
		return arrStrInsertData;
	}

	public void setArrStrInsertData(String arrStrInsertData) {
		this.arrStrInsertData = arrStrInsertData;
	}

	public String getArrStrUpdateData() {
		return arrStrUpdateData;
	}

	public void setArrStrUpdateData(String arrStrUpdateData) {
		this.arrStrUpdateData = arrStrUpdateData;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public List<SaleOrder> getListSaleOrder() {
		return listSaleOrder;
	}

	public void setListSaleOrder(List<SaleOrder> listSaleOrder) {
		this.listSaleOrder = listSaleOrder;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ShopVO getShopVO() {
		return shopVO;
	}

	public void setShopVO(ShopVO shopVO) {
		this.shopVO = shopVO;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public String getOderNumber() {
		return oderNumber;
	}

	public void setOderNumber(String oderNumber) {
		this.oderNumber = oderNumber;
	}

	public String getIsJoinOderNumber() {
		return isJoinOderNumber;
	}

	public void setIsJoinOderNumber(String isJoinOderNumber) {
		this.isJoinOderNumber = isJoinOderNumber;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getFromDateStr() {
		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}

	public List<ShopVO> getLstShopVO() {
		return lstShopVO;
	}

	public void setLstShopVO(List<ShopVO> lstShopVO) {
		this.lstShopVO = lstShopVO;
	}
}
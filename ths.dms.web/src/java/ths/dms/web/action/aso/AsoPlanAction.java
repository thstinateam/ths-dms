/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.web.action.aso;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.AsoPlanMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.AsoPlan;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AsoPlanType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.AsoPlanVO;
import ths.dms.core.entities.vo.DynamicBigVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.filter.CommonFilter;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Action phan bo ke hoach aso  (active sale Order)
 * @author vuongmq
 * @since 19/10/2015
 */
public class AsoPlanAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private Long curShopId;
	private Long routingId;
	private Integer yearPeriod;
	private Integer numPeriod;
	private String shopCode;
	private String staffCode;
	private Integer type;

	private File excelFile;
	private String excelFileContentType;

	private Shop shop;

	private List<AsoPlanVO> lstAsoPlanVO;
	private List<DynamicBigVO> lstAsoPlanObject;
	private List<StaffVO> lstStaff;

	private AsoPlanMgr asoPlanMgr;
	private ProductMgr productMgr;
	private ProductInfoMgr productInfoMgr;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		asoPlanMgr = (AsoPlanMgr) context.getBean("asoPlanMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
	}

	@Override
	public String execute() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			lstYear = cycleMgr.getListYearOfAllCycle();
			Date curdate = commonMgr.getSysDate();
			yearPeriod = DateUtil.getYear(curdate);
			Cycle curCycle = cycleMgr.getCycleByDate(curdate);
			if (curCycle != null) {
				numPeriod = curCycle.getNum();
			} else {
				numPeriod = 1;
			}
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				shopId = shop.getId();
				curShopId = shop.getId();
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.AsoPlanAction.execute()"), createLogErrorStandard(startLogDate));
		}
		return SUCCESS;
	}

	/**
	 * Search ASO PLAN list
	 * @author vuongmq
	 * @return String
	 * @since 19/10/2015
	 */
	public String search() {
		this.getListDataSearch();
		return SUCCESS;
	}

	/**
	 * Lay danh sach du lieu seach do vao lstAsoPlanVO
	 * @author vuongmq
	 * @return String
	 * @since 19/10/2015
	 */
	private void getListDataSearch() {
		Date startLogDate = DateUtil.now();
		try {
			if (shopCode != null && yearPeriod != null && numPeriod != null) {
				Shop sh = shopMgr.getShopByCode(shopCode);
				Cycle c = cycleMgr.getCycleByYearAndNum(yearPeriod, numPeriod);
				Staff staff = staffMgr.getStaffByCode(staffCode);
				if (sh != null && c != null && staff != null) {
					CommonFilter filter = new CommonFilter();
					filter.setStaffId(staff.getId());
					List<RoutingVO> lstRoutingVO = asoPlanMgr.getRoutingVOByFilter(filter);
					if (lstRoutingVO != null && lstRoutingVO.size() > 0) {
						RoutingVO routingVO = lstRoutingVO.get(0);
						CommonFilter fi = new CommonFilter();
						fi.setPlanType(type);
						fi.setShopId(sh.getId());
						fi.setCycleId(c.getId());
						fi.setStaffId(staff.getId());
						fi.setRoutingId(routingVO.getRoutingId());
						lstAsoPlanVO = asoPlanMgr.getListAsoPlanVOByFilter(fi);
					}
				}
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.AsoPlanAction.getListDataSearch()"), createLogErrorStandard(startLogDate));
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
	}

	/**
	 * Lay danh sach staff theo shopId
	 * @author vuongmq
	 * @return String
	 * @since 19/10/2015
	 */
	public String getListStaff() {
		Date startLogDate = DateUtil.now();
		if (currentUser != null && currentUser.getStaffRoot() != null) {
			try {
				if (shopId != null && shopMgr.getShopById(shopId) != null) {
					StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
					filter.setIsLstChildStaffRoot(false);
					filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
					filter.setStatus(ActiveType.RUNNING.getValue());
					filter.setLstShopId(Arrays.asList(shopId));
					filter.setIsGetChildShop(false);
					filter.setObjectType(StaffSpecificType.STAFF.getValue());
					filter.setStaffIdListStr(getStrListUserId());
					ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
					lstStaff = objVO.getLstObject();
				} else {
					lstStaff = new ArrayList<StaffVO>();
				}
			}
			catch (BusinessException e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.AsoPlanAction.getListStaff()"), createLogErrorStandard(startLogDate));
				result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		}
		return JSON;
	}

	/**
	 * Lay Routing cua staff theo shopId
	 * @author vuongmq
	 * @return String
	 * @since 20/10/2015
	 */
	public String getRoutingStaff() {
		Date startLogDate = DateUtil.now();
		if (currentUser != null && currentUser.getStaffRoot() != null) {
			try {
				if (!StringUtil.isNullOrEmpty(staffCode)) {
					Staff staff = staffMgr.getStaffByCode(staffCode);
					if (staff != null) {
						CommonFilter filter = new CommonFilter();
						filter.setStaffId(staff.getId());
						List<RoutingVO> lstRoutingVO = asoPlanMgr.getRoutingVOByFilter(filter);
						if (lstRoutingVO != null && lstRoutingVO.size() > 0) {
							RoutingVO routingVO = lstRoutingVO.get(0);
							result.put("routingStaff", routingVO.getRoutingCode() + " - " + routingVO.getRoutingName());
							result.put("routingId", routingVO.getRoutingId());
						}
					}
				}
			}
			catch (BusinessException e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.AsoPlanAction.getRoutingStaff()"), createLogErrorStandard(startLogDate));
				result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		}
		return JSON;
	}

	/**
	 * Check Aso plan
	 * @author vuongmq
	 * @return String
	 * @since 21/10/2015
	 */
	public String checkAsoPlan() {
		Date startLogDate = DateUtil.now();
		if (currentUser != null && currentUser.getStaffRoot() != null) {
			try {
				if (shopCode != null && yearPeriod != null && numPeriod != null) {
					Shop sh = shopMgr.getShopByCode(shopCode);
					Cycle c = cycleMgr.getCycleByYearAndNum(yearPeriod, numPeriod);
					Staff staff = staffMgr.getStaffByCode(staffCode);
					if (sh != null && c != null && staff != null) {
						CommonFilter filter = new CommonFilter();
						filter.setStaffId(staff.getId());
						List<RoutingVO> lstRoutingVO = asoPlanMgr.getRoutingVOByFilter(filter);
						if (lstRoutingVO != null && lstRoutingVO.size() > 0) {
							RoutingVO routingVO = lstRoutingVO.get(0);
							CommonFilter fi = new CommonFilter();
							String errConfirm = "";
							if (AsoPlanType.SKU.getValue().equals(type)) {
								type = AsoPlanType.SUB_CAT.getValue();
								errConfirm = R.getResource("aso_set_target_routing_confirm", R.getResource("aso_type_sub_cat"), R.getResource("aso_type_sku"), R.getResource("aso_type_sub_cat"));
							} else {
								type = AsoPlanType.SKU.getValue();
								errConfirm = R.getResource("aso_set_target_routing_confirm", R.getResource("aso_type_sku"), R.getResource("aso_type_sub_cat"), R.getResource("aso_type_sku"));
							}
							fi.setPlanType(type);
							fi.setShopId(sh.getId());
							fi.setCycleId(c.getId());
							fi.setRoutingId(routingVO.getRoutingId());
							List<AsoPlan> lstAsoPlan = asoPlanMgr.getListAsoByFilter(fi);
							if (lstAsoPlan != null && lstAsoPlan.size() > 0) {
								result.put("flag", ActiveType.WAITING.getValue());
								result.put(ERR_MSG, errConfirm);
							} else {
								result.put("flag", ActiveType.RUNNING.getValue());
							}
						} else {
							result.put("flag", ActiveType.STOPPED.getValue());
							result.put(ERR_MSG, R.getResource("aso_staff_not_exists_routing", staff.getStaffName()));
						}
					}
				}
			}
			catch (BusinessException e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.AsoPlanAction.checkAsoPlan()"), createLogErrorStandard(startLogDate));
				result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		}
		return JSON;
	}

	/**
	 * Save ASO PLAN list
	 * @author vuongmq
	 * @return String
	 * @since 19/10/2015
	 */
	public String saveInfo() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			if (lstAsoPlanObject != null && lstAsoPlanObject.size() > 0) {
				Date curdate = commonMgr.getSysDate();
				Integer year = DateUtil.getYear(curdate);
				Cycle curCycle = cycleMgr.getCycleByDate(curdate);
				if (yearPeriod != null && yearPeriod < year) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("kpi_year_must_greater_or_equal_sysyear"));
					return JSON;
				}
				if (yearPeriod != null && yearPeriod.equals(year) && curCycle != null && numPeriod != null && numPeriod.compareTo(curCycle.getNum()) < 0) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("kpi_period_must_greater_or_equal_curperiod"));
					return JSON;
				}
				AsoPlanVO asoPlanVO = new AsoPlanVO();
				Cycle c = cycleMgr.getCycleByYearAndNum(yearPeriod, numPeriod);
				if (c != null) {
					asoPlanVO.setObjectType(type);
					asoPlanVO.setCycleId(c.getId());
					asoPlanVO.setShopCode(shopCode);
					asoPlanVO.setStaffCode(staffCode);
					asoPlanVO.setRoutingId(routingId);
					asoPlanMgr.createOrUpdateListAso(asoPlanVO, lstAsoPlanObject, getLogInfoVO());
				} else {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.not.exist.in.db", R.getResource("sale_plan_period")));
					return JSON;
				}
			} else {
				String errType = "";
				if (AsoPlanType.SKU.getValue().equals(type)) {
					errType = R.getResource("aso_type_sku");
				} else {
					errType = R.getResource("aso_type_sub_cat");
				}
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("aso_list_null", errType));
				return JSON;
			}
		} catch (BusinessException e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, R.getResource("system.error"));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.AsoPlan.saveInfo()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * reset ASO PLAN list
	 * @author vuongmq
	 * @return String
	 * @since 23/10/2015
	 */
	public String resetAso() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			if (currentUser != null && currentUser.getStaffRoot() != null) {
				Date curdate = commonMgr.getSysDate();
				Integer year = DateUtil.getYear(curdate);
				Cycle curCycle = cycleMgr.getCycleByDate(curdate);
				if (yearPeriod != null && yearPeriod < year) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("kpi_year_must_greater_or_equal_sysyear"));
					return JSON;
				}
				if (yearPeriod != null && yearPeriod.equals(year) && curCycle != null && numPeriod != null && numPeriod.compareTo(curCycle.getNum()) < 0) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("kpi_period_must_greater_or_equal_curperiod"));
					return JSON;
				}
				Cycle c = cycleMgr.getCycleByYearAndNum(yearPeriod, numPeriod);
				if (c == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.not.exist.in.db", R.getResource("sale_plan_period")));
					return JSON;
				}
				Shop sh = shopMgr.getShopByCode(shopCode);
				if (sh == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.not.exist.in.db", R.getResource("catalog.customer.shop.code.NPP")));
					return JSON;
				}
				Staff staff = staffMgr.getStaffByCode(staffCode);
				if (staff == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.not.exist.in.db", R.getResource("catalog.staff.code.saleplan")));
					return JSON;
				}
				CommonFilter filter = new CommonFilter();
				filter.setStaffId(staff.getId());
				filter.setRoutingId(routingId);
				List<RoutingVO> lstRoutingVO = asoPlanMgr.getRoutingVOByFilter(filter);
				if (lstRoutingVO == null || lstRoutingVO.size() == 0) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("aso_staff_not_exists_routing_err"));
					return JSON;
				}
				AsoPlanVO asoPlanVO = new AsoPlanVO();
				asoPlanVO.setCycleId(c.getId());
				asoPlanVO.setRoutingId(routingId);
				asoPlanVO.setShopId(sh.getId());
				asoPlanVO.setObjectType(type);
				asoPlanMgr.resetAso(asoPlanVO, getLogInfoVO());
				result.put(ERROR, false);
			}
		} catch (BusinessException e) {
			result.put(ERROR, true);
			result.put(ERR_MSG, R.getResource("system.error"));
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.AsoPlan.resetAso()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Export ASO list
	 * @author vuongmq
	 * @return String
	 * @since 21/10/2015
	 */
	public String exportExcel() {
		Date startDate = DateUtil.now();
		try {
			if (shopCode != null && yearPeriod != null && numPeriod != null) {
				String reportToken = retrieveReportToken(reportCode);
				if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
					return JSON;
				}
				this.getListDataSearch();
				if (lstAsoPlanVO != null && lstAsoPlanVO.size() > 0) {
					Map<String, Object> beans = new HashMap<String, Object>();
					beans.put("type", type);
					beans.put("lstData", lstAsoPlanVO);
					String outputPath = exportExcelJxls(beans, Configuration.getExcelTemplatePathCatalog(), ConstantManager.TEMPLATE_ASO_TARGET);
					result.put(LIST, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, true);
					result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null"));
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "web.action.aso.AsoPlanAction.exportExcel()"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Import ASO list
	 * @author vuongmq
	 * @return String
	 * @since 22/10/2015
	 */
	public String importAso(){
		Date startDate = DateUtil.now();
		try {
			resetToken(result);
			totalItem = 0;
			isError = true;
			totalItem = 0;
		    List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 11);
		    List<CellBean> lstFails = new ArrayList<CellBean>();
		    lstView = new ArrayList<CellBean>();
		    typeView = true;
	    	if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
	    		List<String> row = null;
	    		String message = "";
				String errLocal = "";
	    		String value = "";
			    Date curdate = commonMgr.getSysDate();
				Integer curYear = DateUtil.getYear(curdate);
				Cycle curCycle = cycleMgr.getCycleByDate(curdate);
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					row = lstData.get(i);
					if (row != null && row.size() > 0) {
						message = "";
						errLocal = "";
						totalItem++;
						Shop shopExcel = null;
						Staff staffExcel = null;
						Routing routingExcel = null;
						Integer year = null;
						Cycle cycleExcel = null;
						Product productExcel = null;
						ProductInfo productInfoExcel = null;
						BigDecimal plan = null;
						//Ma NPP
						value = row.get(0);
						errLocal = ValidateUtil.validateField(value, "catalog.customer.shop.code.NPP", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
						if (StringUtil.isNullOrEmpty(errLocal)) {
							shopExcel = shopMgr.getShopByCode(value);
							if (shopExcel == null) {
								errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("catalog.customer.shop.code.NPP"));
								errLocal += "\n";
							} else {
								if (!ActiveType.RUNNING.equals(shopExcel.getStatus())) {
									errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("common.unit"));
									errLocal += "\n";
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						//Ten NPP
						value = row.get(1);
						errLocal = "";
						errLocal = ValidateUtil.validateField(value, "ss.ds3.sheet1.header.tenNPP", 250, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						//Ma NV
						value = row.get(2);
						errLocal = "";
						errLocal = ValidateUtil.validateField(value, "catalog.staff.code.saleplan", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
						if (StringUtil.isNullOrEmpty(errLocal)) {
							staffExcel = staffMgr.getStaffByCode(value);
							if (staffExcel == null) {
								errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("catalog.staff.code.saleplan"));
								errLocal += "\n";
							} else {
								if (!ActiveType.RUNNING.equals(staffExcel.getStatus())) {
									errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("baocao.nv"));
									errLocal += "\n";
								} else {
									// validate NV cua NPP
									if (shopExcel != null && staffExcel.getShop() != null && !shopExcel.getId().equals(staffExcel.getShop().getId())) {
										errLocal += R.getResource("common.staff.not.belong.shop", staffExcel.getStaffCode(), shopExcel.getShopCode());
										errLocal += "\n";
									}
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						//Ten NV
						value = row.get(3);
						errLocal = "";
						errLocal = ValidateUtil.validateField(value, "ss.vt5.sheet1.header.tenNVBH", 250, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						//Ma Tuyen
						value = row.get(4);
						errLocal = "";
						errLocal = ValidateUtil.validateField(value, "ss.routingcode", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
						if (StringUtil.isNullOrEmpty(errLocal)) {
							if (shopExcel != null) {
								routingExcel = superviserMgr.getRoutingMultiChoice(null, shopExcel.getId(), value, null);
								if (routingExcel == null) {
									errLocal += R.getResource("ss.create.route.not.belong.shop", shopExcel.getShopCode());
									errLocal += "\n";
								} else {
									if (!ActiveType.RUNNING.equals(routingExcel.getStatus())) {
										errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("jsp.common.tuyen"));
										errLocal += "\n";
									} else {
										// validate tuyen cua nhan vien
										if (staffExcel != null) {
											CommonFilter filter = new CommonFilter();
											filter.setStaffId(staffExcel.getId());
											filter.setRoutingId(routingExcel.getId());
											List<RoutingVO> lstRoutingVO = asoPlanMgr.getRoutingVOByFilter(filter);
											if (lstRoutingVO == null || lstRoutingVO.size() == 0) {
												errLocal += R.getResource("aso_staff_not_exists_routing_err");
												errLocal += "\n";
											}
										}
									}
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						//Ten Tuyen
						value = row.get(5);
						errLocal = "";
						errLocal = ValidateUtil.validateField(value, "ss.routingname", 250, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						// nam
						value = row.get(6);
						errLocal = "";
						errLocal = ValidateUtil.validateField(value, "catalog.year.code", null, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						if (StringUtil.isNullOrEmpty(errLocal)) {
							year = Integer.parseInt(value);
							if (year < curYear) {
								errLocal += R.getResource("kpi_year_must_greater_or_equal_sysyear");
							}
						}
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						//chu ky
						value = row.get(7);
						errLocal = "";
						errLocal = ValidateUtil.validateField(value, "sale_plan_period", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						if (StringUtil.isNullOrEmpty(errLocal)) {
							CycleFilter filter = new CycleFilter();
							filter.setCycleName(value);
							cycleExcel = cycleMgr.getCycleByFilter(filter);
							if (cycleExcel == null) {
								errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("sale_plan_period"));
								errLocal += "\n";
							} else {
								if (cycleExcel.getStatus() != null && !ActiveType.RUNNING.getValue().equals(cycleExcel.getStatus())) {
									errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("sale_plan_period"));
									errLocal += "\n";
								}
							}
						}
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						errLocal = "";
						if (year != null && cycleExcel != null) {
							if (!year.equals(DateUtil.getYear(cycleExcel.getYear()))) {
								errLocal += R.getResource("aso_import_year_of_cycle_not_same");
								errLocal += "\n";
							}
							if (curYear.equals(year) && curCycle != null && cycleExcel.getNum() < curCycle.getNum()) {
								errLocal += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.plan.qm.excel.allocation.date.allocation");
								errLocal += "\n";
							}
						}
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						// Ma SKU; ten SKU
						if (type != null && AsoPlanType.SKU.getValue().equals(type)) {
							//Ma SKU
							value = row.get(8);
							errLocal = "";
							errLocal = ValidateUtil.validateField(value, "key.shop.import.sku", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(errLocal)) {
								productExcel = productMgr.getProductByCode(value);
								if (productExcel == null) {
									errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("key.shop.import.sku"));
									errLocal += "\n";
								} else {
									if (!ActiveType.RUNNING.equals(productExcel.getStatus())) {
										errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("catalog.product.san.pham"));
										errLocal += "\n";
									}
								}
							}
							if (!StringUtil.isNullOrEmpty(errLocal)) {
								message += errLocal + "\n";
							}
							// ten SKU
							value = row.get(9);
							errLocal = "";
							errLocal = ValidateUtil.validateField(value, "aso_ten_sku", 250, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
							if (!StringUtil.isNullOrEmpty(errLocal)) {
								message += errLocal + "\n";
							}
						} else {
							//Ma Nganh hang con
							value = row.get(8);
							errLocal = "";
							errLocal = ValidateUtil.validateField(value, "catalog.quota.group.sub.cat.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
							if (StringUtil.isNullOrEmpty(errLocal)) {
								productInfoExcel = productInfoMgr.getProductInfoByWithNotSubCat(value, ProductType.SUB_CAT, null);
								if (productInfoExcel == null) {
									errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, R.getResource("catalog.quota.group.sub.cat.code"));
									errLocal += "\n";
								} else {
									if (!ActiveType.RUNNING.equals(productInfoExcel.getStatus())) {
										errLocal += ValidateUtil.getErrorMsg(ConstantManager.ERR_STATUS_PAUSE, R.getResource("catalog.categoryvn"));
										errLocal += "\n";
									}
								}
							}
							if (!StringUtil.isNullOrEmpty(errLocal)) {
								message += errLocal + "\n";
							}
							// ten nganh hang con
							value = row.get(9);
							errLocal = "";
							errLocal = ValidateUtil.validateField(value, "catalog.product.sub.cat.name", 250, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
							if (!StringUtil.isNullOrEmpty(errLocal)) {
								message += errLocal + "\n";
							}
						}
						// Chi tieu
						value = row.get(10);
						errLocal = "";
						errLocal = ValidateUtil.validateField(value, "aso_ten_chi_tieu", 10, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						if (StringUtil.isNullOrEmpty(errLocal)) {
							plan = new BigDecimal(value);
						}
						if (!StringUtil.isNullOrEmpty(errLocal)) {
							message += errLocal + "\n";
						}
						// inssert vao DP
						if (StringUtil.isNullOrEmpty(message)) {
							AsoPlan asoPlan = new AsoPlan();
							asoPlan.setShop(shopExcel);
							asoPlan.setRouting(routingExcel);
							asoPlan.setCycle(cycleExcel);
							asoPlan.setObjectType(AsoPlanType.parseValue(type));
							asoPlan.setPlan(plan);
							asoPlan.setCreateDate(curdate);
							asoPlan.setCreateUser(currentUser.getUserName());
							Integer typeCheck = AsoPlanType.SKU.getValue();
							if (type != null && AsoPlanType.SKU.getValue().equals(type)) {
								typeCheck = AsoPlanType.SUB_CAT.getValue();
								if (productExcel != null) {
									asoPlan.setObjectId(productExcel.getId());
								}
							} else {
								if (productInfoExcel != null) {
									asoPlan.setObjectId(productInfoExcel.getId());
								}
							}
							CommonFilter fi = new CommonFilter();
							fi.setPlanType(typeCheck);
							fi.setShopId(shopExcel.getId());
							fi.setCycleId(cycleExcel.getId());
							fi.setRoutingId(routingExcel.getId());
							List<AsoPlan> lstAsoPlan = asoPlanMgr.getListAsoByFilter(fi);
							// luc nay se xoa het lstAsoPlan.size() > 0 check, va insert loai Aso type moi
							if (lstAsoPlan != null && lstAsoPlan.size() > 0) {
								asoPlanMgr.createOrUpdateImportAso(asoPlan, lstAsoPlan);
							} else {
								asoPlanMgr.createOrUpdateImportAso(asoPlan, null);
							}
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_ASO_TARGET_ERR);
	    	} else {
				errMsg = R.getResource("customer.display.program.nodata");
				return SUCCESS;
			}
		} catch (BusinessException ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "web.action.aso.AsoPlanAction.importExcel()"), createLogErrorStandard(startDate));
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
	    return SUCCESS;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Long getCurShopId() {
		return curShopId;
	}

	public void setCurShopId(Long curShopId) {
		this.curShopId = curShopId;
	}

	public Integer getYearPeriod() {
		return yearPeriod;
	}

	public void setYearPeriod(Integer yearPeriod) {
		this.yearPeriod = yearPeriod;
	}

	public Integer getNumPeriod() {
		return numPeriod;
	}

	public void setNumPeriod(Integer numPeriod) {
		this.numPeriod = numPeriod;
	}

	public List<Integer> getLstYear() {
		return lstYear;
	}

	public void setLstYear(List<Integer> lstYear) {
		this.lstYear = lstYear;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public List<StaffVO> getLstStaff() {
		return lstStaff;
	}

	public void setLstStaff(List<StaffVO> lstStaff) {
		this.lstStaff = lstStaff;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getRoutingId() {
		return routingId;
	}

	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}

	public List<AsoPlanVO> getLstAsoPlanVO() {
		return lstAsoPlanVO;
	}

	public void setLstAsoPlanVO(List<AsoPlanVO> lstAsoPlanVO) {
		this.lstAsoPlanVO = lstAsoPlanVO;
	}

	public List<DynamicBigVO> getLstAsoPlanObject() {
		return lstAsoPlanObject;
	}

	public void setLstAsoPlanObject(List<DynamicBigVO> lstAsoPlanObject) {
		this.lstAsoPlanObject = lstAsoPlanObject;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

}

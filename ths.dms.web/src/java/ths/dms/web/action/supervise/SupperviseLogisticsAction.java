package ths.dms.web.action.supervise;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CarMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserLogisticsMgr;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.RoutingCar;
import ths.dms.core.entities.RoutingCarCust;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CarFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.vo.CarVO;
import ths.dms.core.entities.vo.LatLngVO;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.JETreeGridLogisticsNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtilsEx;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelProcessUtilsEx;

public class SupperviseLogisticsAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long shopId;
	private List<Long> lstShopId;

	private StaffMgr staffMgr;
	private ShopMgr shopMgr;
	private CustomerMgr customerMgr;
	private SuperviserLogisticsMgr superviserLogisticsMgr;
	private CarMgr carMgr;
	private ApParamMgr apParamMgr;

	private List<JETreeGridLogisticsNode> lstCarTreeResult;
	private List<CarVO> lstShopCar;
	private List<CarVO> lstCust;
	private String shopCode;
	private String shopName;
	private List<LatLngVO> lstCustLatLng;
	private Long maxLogId;
	private List<Car> lstCar;
	private List<Staff> lstDeliveryStaff;
	private Long carId;
	private String deliveryDate;
	private List<Long> lstCustomerId;
	private List<Long> lstId;
	private List<Integer> lstSeq;
	private Long nvghId;
	private Long routingId;
	private String carNumber;
	private String fromDate;
	private String toDate;
	private ReportUtilsEx rUtils;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		try {
			shopMgr = (ShopMgr) context.getBean("shopMgr");
			staffMgr = (StaffMgr) context.getBean("staffMgr");
			superviserLogisticsMgr = (SuperviserLogisticsMgr) context.getBean("superviserLogisticsMgr");
			customerMgr = (CustomerMgr) context.getBean("customerMgr");
			carMgr = (CarMgr) context.getBean("carMgr");
			apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		if (currentUser != null) {
			Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
			if (staff != null) {
				if (shopId == null)
					shopId = staff.getShop().getId();
			}
		}
		return SUCCESS;
	}

	public String executeManagerRoute() {
		resetToken(result);
		try {
			if (currentUser != null) {
				Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null) {
					if (shopId == null)
						shopId = staff.getShop().getId();
				}
				ObjectVO<Car> vo = carMgr.getListCar(null, shopId, null, null, null, null, null, null, ActiveType.RUNNING, true);
				if (vo != null && vo.getLstObject() != null) {
					lstCar = vo.getLstObject();
				} else {
					lstCar = new ArrayList<Car>();
				}
				Calendar calendar = new GregorianCalendar();
				Date now = calendar.getTime();
				deliveryDate = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.executeManagerRoute");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	public String executeBC() {
		resetToken(result);
		try {
			if (currentUser != null) {
				Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null) {
					if (shopId == null)
						shopId = staff.getShop().getId();
				}
				Calendar calendar = new GregorianCalendar();
				Date now = calendar.getTime();
				fromDate = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.executeManagerRoute");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	public JETreeGridLogisticsNode convertCarToNode(CarVO temp) {
		JETreeGridLogisticsNode node = new JETreeGridLogisticsNode();
		node.setId(temp.getShopId());
		node.setText(temp.getShopCode() + " - " + temp.getShopName());
		node.setAttr(temp);
		if (ShopObjectType.NPP.getValue().equals(temp.getShopType())) {
			node.setState(ConstantManager.JSTREE_STATE_LEAF);
		} else {
			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
		}
		return node;
	}

	public String getListShopCarOneNode() {
		try {
			Shop currentShop = getCurrentShop();
			lstCarTreeResult = new ArrayList<JETreeGridLogisticsNode>();
			if (shopId == null || shopId == 0) {
				if (currentShop != null) {
					if (!StringUtil.isNullOrEmpty(shopCode) || !StringUtil.isNullOrEmpty(shopName)) {
						List<Shop> lstShop = superviserLogisticsMgr.getListAncestor(currentShop.getId(), shopCode, shopName);
						if (lstShop == null || lstShop.size() == 0) {//load mac dinh
							lstShopCar = superviserLogisticsMgr.getListShopCarOneNode(currentShop.getId());
						} else {
							lstCarTreeResult = searchShop(lstShop);
							return JSON;
						}
					} else {
						lstShopCar = superviserLogisticsMgr.getListShopCarOneNode(currentShop.getId());
					}
				}
			} else {
				Shop shop = shopMgr.getShopById(shopId);
				if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
					lstShopCar = superviserLogisticsMgr.getListShopCarOneNode(shop.getId());
				}
			}
			List<JETreeGridLogisticsNode> lstTreeGrid = new ArrayList<JETreeGridLogisticsNode>();
			Integer countCar = 0, countWarning = 0, countRouting = 0;
			if (lstShopCar != null && lstShopCar.size() > 0) {
				for (CarVO temp : lstShopCar) {
					countCar += temp.getCountCar();
					countWarning += temp.getCountWarning();
					countRouting += temp.getCountRouting();
					JETreeGridLogisticsNode node = convertCarToNode(temp);
					lstTreeGrid.add(node);
				}
			}
			if ((shopId == null || shopId == 0)) {
				CarVO carVO = new CarVO();
				carVO.setShopId(currentShop.getId());
				carVO.setCountCar(countCar);
				carVO.setCountWarning(countWarning);
				carVO.setCountRouting(countRouting);
				carVO.setShopCode(currentShop.getShopCode());
				carVO.setShopName(currentShop.getShopName());
				carVO.setShopType(0);
				JETreeGridLogisticsNode node = convertCarToNode(carVO);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setChildren(lstTreeGrid);
				lstCarTreeResult.add(node);
			} else {
				lstCarTreeResult = lstTreeGrid;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListShopCarOneNode");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public List<JETreeGridLogisticsNode> searchShop(List<Shop> lstShop) {
		List<JETreeGridLogisticsNode> lstTreeRoot = new ArrayList<JETreeGridLogisticsNode>();
		JETreeGridLogisticsNode root = new JETreeGridLogisticsNode();
		try {
			Long idBold = lstShop.get(0).getId();
			int level = lstShop.size() - 1;
			Integer countCar = 0, countWarning = 0;
			List<CarVO> lstShopCar1 = superviserLogisticsMgr.getListShopCarOneNode(lstShop.get(level).getId());
			if (level >= 0 && lstShopCar1 != null && lstShopCar1.size() > 0) {
				level--;
				List<JETreeGridLogisticsNode> lstTree1 = new ArrayList<JETreeGridLogisticsNode>();
				for (CarVO car1 : lstShopCar1) {
					if (car1.getShopId().equals(idBold)) {
						car1.setIsBold(true);
					}
					countCar = countCar + car1.getCountCar();
					countWarning = countWarning + car1.getCountWarning();
					JETreeGridLogisticsNode node1 = convertCarToNode(car1);
					lstTree1.add(node1);
					if (level >= 0 && lstShop.get(level).getId().equals(car1.getShopId())) {
						List<CarVO> lstShopCar2 = superviserLogisticsMgr.getListShopCarOneNode(lstShop.get(level).getId());
						if (level >= 0 && lstShopCar2 != null && lstShopCar2.size() > 0) {
							level--;
							List<JETreeGridLogisticsNode> lstTree2 = new ArrayList<JETreeGridLogisticsNode>();
							node1.setChildren(lstTree2);
							node1.setState(ConstantManager.JSTREE_STATE_OPEN);
							for (CarVO car2 : lstShopCar2) {
								if (car2.getShopId().equals(idBold)) {
									car2.setIsBold(true);
								}
								JETreeGridLogisticsNode node2 = convertCarToNode(car2);
								lstTree2.add(node2);
								if (level >= 0 && lstShop.get(level).getId().equals(car2.getShopId())) {
									List<CarVO> lstShopCar3 = superviserLogisticsMgr.getListShopCarOneNode(lstShop.get(level).getId());
									if (level >= 0 && lstShopCar3 != null && lstShopCar3.size() > 0) {
										level--;
										List<JETreeGridLogisticsNode> lstTree3 = new ArrayList<JETreeGridLogisticsNode>();
										node2.setChildren(lstTree3);
										node2.setState(ConstantManager.JSTREE_STATE_OPEN);
										for (CarVO car3 : lstShopCar3) {
											if (car3.getShopId().equals(idBold)) {
												car3.setIsBold(true);
											}
											JETreeGridLogisticsNode node3 = convertCarToNode(car3);
											lstTree3.add(node3);
										}
									}
								}
							}
						}
					}
				}
				CarVO carVO = new CarVO();
				carVO.setShopId(lstShop.get(lstShop.size() - 1).getId());
				carVO.setCountCar(countCar);
				carVO.setCountWarning(countWarning);
				carVO.setShopCode(lstShop.get(lstShop.size() - 1).getShopCode());
				carVO.setShopName(lstShop.get(lstShop.size() - 1).getShopName());
				carVO.setShopType(0);
				root = convertCarToNode(carVO);
				root.setChildren(lstTree1);
				root.setState(ConstantManager.JSTREE_STATE_OPEN);
			}
			lstTreeRoot.add(root);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.searchShop");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return lstTreeRoot;
	}

	public String getListShopCar() {
		try {
			if (currentUser != null) {
				Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null) {
					shopId = staff.getShop().getId();
				}
			}
			lstShopCar = superviserLogisticsMgr.getListShopCar(shopId);
			result.put(ERROR, false);
			result.put("lstShop", lstShopCar);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListShopCar");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListSubShopCar() {
		try {
			result.put(ERROR, false);
			Shop currentShop = getCurrentShop();
			Shop shop = shopMgr.getShopById(shopId);
			if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
				lstShopCar = superviserLogisticsMgr.getListShopCarOneNode(shop.getId());
				result.put("lst", lstShopCar);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.getListSubShopCar");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListCustShop() {
		try {
			result.put(ERROR, false);
			Shop currentShop = getCurrentShop();
			Shop shop = shopMgr.getShopById(shopId);
			if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
				lstCust = superviserLogisticsMgr.getListCustShop(shopId);
				result.put("lstCust", lstCust);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListCustShop");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListCustArea() {
		try {
			result.put(ERROR, false);
			Shop currentShop = getCurrentShop();
			Shop shop = shopMgr.getShopById(shopId);
			if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
				List<LatLngVO> lstCust = superviserLogisticsMgr.getListCustomerArea(shopId);
				result.put("lstCust", lstCust);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListCustShop");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListCar() {
		try {
			result.put(ERROR, false);
			Shop currentShop = getCurrentShop();
			Shop shop = shopMgr.getShopById(shopId);
			if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
				result.put("lstCarPosition", superviserLogisticsMgr.getListCarPosition(shopId, maxLogId));
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListCar");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListRoutingCust() {
		try {
			result.put(ERROR, false);
			Shop currentShop = getCurrentShop();
			Shop shop = shopMgr.getShopById(shopId);
			if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
				CarFilter filter = new CarFilter();
				filter.setShopId(shopId);
				result.put("lstRoutingCustPosition", superviserLogisticsMgr.getListRoutingCustPosition(filter));
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListRoutingCust");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListRoutingCustStatus() {
		try {
			result.put(ERROR, false);
			Shop currentShop = getCurrentShop();
			Shop shop = shopMgr.getShopById(shopId);
			if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
				CarFilter filter = new CarFilter();
				filter.setShopId(shopId);
				result.put("lstRoutingCustStatus", superviserLogisticsMgr.getListRoutingCustStatus(filter));
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListRoutingCust");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListCarKP() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<CarVO>());
		try {
			KPaging<CarVO> kPaging = new KPaging<CarVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			CarFilter filter = new CarFilter();
			filter.setkPaging(kPaging);
			filter.setParentShopId(shopId);
			filter.setCarNumber(carNumber);
			filter.setDeliveryDate(DateUtil.parse(deliveryDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			ObjectVO<CarVO> vo = superviserLogisticsMgr.getListCarKP(filter);
			if (vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListCarKP");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListCarVungKP() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<CarVO>());
		try {
			KPaging<CarVO> kPaging = new KPaging<CarVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			CarFilter filter = new CarFilter();
			filter.setkPaging(kPaging);
			filter.setParentShopId(shopId);
			filter.setCarNumber(carNumber);
			//			filter.setDeliveryDate(DateUtil.parse(deliveryDate, DateUtil.DATE_FORMAT_STR));
			ObjectVO<CarVO> vo = superviserLogisticsMgr.getListCarVungKP(filter);
			if (vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListCarKP");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListRoutingExistKP() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<CarVO>());
		try {
			KPaging<CarVO> kPaging = new KPaging<CarVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			List<Long> lstShopId = new ArrayList<Long>();
			lstShopId.add(shopId);
			CarFilter filter = new CarFilter();
			filter.setkPaging(kPaging);
			filter.setLstShopId(lstShopId);
			filter.setDeliveryDate(DateUtil.parse(deliveryDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			ObjectVO<CarVO> vo = superviserLogisticsMgr.getListRoutingExistKP(filter);
			if (vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.getListRoutingExistKP");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String updateListOverCust() {
		resetToken(result);
		try {
			CarFilter filter = new CarFilter();
			filter.setShopId(shopId);
			lstCustLatLng = superviserLogisticsMgr.getListCustomerByShop(filter);
			if (lstCustLatLng != null && lstCustLatLng.size() > 0) {
				lstCustLatLng = graham();
				if (lstCustLatLng.size() > 0) {
					superviserLogisticsMgr.deleteCustomerArea(shopId, getLogInfoVO());
					superviserLogisticsMgr.createListCustomerArea(lstCustLatLng, getLogInfoVO());
				}
				result.put("lstCust", lstCustLatLng);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseLogisticsAction.updateListOverCust");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/*
	 * author:tungmt 16/05/2014 1: nam ngoai vung phu 0: nam trong vung phu
	 */
	public int checkWarningCar(List<LatLngVO> lstCustLatLng, LatLngVO car) {
		car.setGoc(goc(lstCustLatLng.get(0), car));
		if (lstCustLatLng.get(0).getGoc() <= car.getGoc() && car.getGoc() >= lstCustLatLng.get(lstCustLatLng.size() - 1).getGoc()) {
			if (direct(lstCustLatLng.get(lstCustLatLng.size() - 1), car, lstCustLatLng.get(0)) <= 0) {
				return 0;//trong
			} else {
				return 1;//ngoai
			}
		} else {
			for (int i = 0; i < lstCustLatLng.size() - 1; i++) {
				if (lstCustLatLng.get(i).getGoc() <= car.getGoc() && car.getGoc() <= lstCustLatLng.get(i + 1).getGoc()) {
					if (direct(lstCustLatLng.get(i), car, lstCustLatLng.get(i + 1)) <= 0) {
						return 0;//trong
					} else {
						return 1;//ngoai
					}
				}
			}
		}
		return 0;
	}

	public void hoanDoi(LatLngVO a, LatLngVO b) {
		Double lat = a.getLat();
		a.setLat(b.getLat());
		b.setLat(lat);
		Double lng = a.getLng();
		a.setLng(b.getLng());
		b.setLng(lng);
		Double goc = a.getGoc();
		a.setGoc(b.getGoc());
		b.setGoc(goc);
		Long customerId = a.getCustomerId();
		a.setCustomerId(b.getCustomerId());
		b.setCustomerId(customerId);
	}

	public Double goc(LatLngVO m1, LatLngVO m2) {
		Double t = 0d;
		Double dx = m2.getLat() - m1.getLat();
		Double dy = m2.getLng() - m1.getLng();
		if (dx == 0 && dy == 0) {
			t = 0d;
		} else {
			t = dy / (Math.abs(dx) + Math.abs(dy));
		}
		if (dx < 0) {
			t = 2 - t;
		} else if (dy < 0) {
			t = 4 + t;
		}
		return t * 90;
	}

	public Integer direct(LatLngVO m1, LatLngVO m2, LatLngVO m3) {
		Double dx1 = m2.getLat() - m1.getLat();
		Double dy1 = m2.getLng() - m1.getLng();
		Double dx2 = m3.getLat() - m1.getLat();
		Double dy2 = m3.getLng() - m1.getLng();
		Double temp = dy2 * dx1 - dy1 * dx2;
		if (temp > 0)
			return 1;
		if (temp < 0)
			return -1;
		if ((dx1 * dx2 < 0) || (dy1 * dy2 < 0))
			return 1;
		else {
			if (Math.sqrt(dx1) + Math.sqrt(dy1) >= Math.sqrt(dx2) + Math.sqrt(dy2)) {
				return 0;
			} else {
				return 1;
			}
		}
	}

	public void swap() {
		Double t = 0d;
		List<Double> g = new ArrayList<Double>();
		g.add(0d);
		lstCustLatLng.get(0).setGoc(0d);
		for (int i = 1; i < lstCustLatLng.size(); i++) {
			g.add(goc(lstCustLatLng.get(0), lstCustLatLng.get(i)));
			lstCustLatLng.get(i).setGoc(g.get(i));
		}
		LatLngVO voTemp = null;
		for (int i = 1; i < lstCustLatLng.size() - 1; i++) {
			for (int j = i + 1; j < lstCustLatLng.size(); j++) {
				if (g.get(i) > g.get(j)) {
					t = g.get(i);
					g.set(i, g.get(j));
					g.set(j, t);
					voTemp = lstCustLatLng.get(i);
					lstCustLatLng.set(i, lstCustLatLng.get(j));
					lstCustLatLng.set(j, voTemp);
					//					hoanDoi(lstCustLatLng.get(i),lstCustLatLng.get(j));
					//					Double lat=lstCustLatLng.get(i).getLat();
					//					lstCustLatLng.get(i).setLat(lstCustLatLng.get(j).getLat());
					//					lstCustLatLng.get(j).setLat(lat);
					//					Double lng=lstCustLatLng.get(i).getLng();
					//					lstCustLatLng.get(i).setLng(lstCustLatLng.get(j).getLng());
					//					lstCustLatLng.get(j).setLng(lng);
					//					Double goc=lstCustLatLng.get(i).getGoc();
					//					lstCustLatLng.get(i).setGoc(lstCustLatLng.get(j).getGoc());
					//					lstCustLatLng.get(j).setGoc(goc);
					//					Long customerId=lstCustLatLng.get(i).getCustomerId();
					//					lstCustLatLng.get(i).setCustomerId(lstCustLatLng.get(j).getCustomerId());
					//					lstCustLatLng.get(j).setCustomerId(customerId);
				}
			}
		}
	}

	public List<LatLngVO> graham() {
		int min = 0;
		for (int i = 1; i < lstCustLatLng.size(); i++) {
			if (lstCustLatLng.get(i).getLng() < lstCustLatLng.get(min).getLng())
				min = i;
		}
		for (int i = 1; i < lstCustLatLng.size(); i++) {
			if ((lstCustLatLng.get(i).getLng() == lstCustLatLng.get(min).getLng()) && (lstCustLatLng.get(i).getLat() > lstCustLatLng.get(min).getLat()))
				min = i;
		}
		LatLngVO q = new LatLngVO(lstCustLatLng.get(0));
		lstCustLatLng.set(0, new LatLngVO(lstCustLatLng.get(min)));
		lstCustLatLng.set(min, new LatLngVO(q));
		this.swap();
		List<LatLngVO> b = new ArrayList<LatLngVO>();
		b.add(lstCustLatLng.get(lstCustLatLng.size() - 1));
		for (int i = 0; i < 2; i++) {
			//			b.add(new LatLngVO(lstCustLatLng.get(i)));
			b.add(lstCustLatLng.get(i));
		}

		int m = 2;
		for (int i = 2; i < lstCustLatLng.size(); i++) {
			if (!lstCustLatLng.get(i).getLat().equals(lstCustLatLng.get(i - 1).getLat()) || !lstCustLatLng.get(i).getLng().equals(lstCustLatLng.get(i - 1).getLng())) {
				while (direct(b.get(m - 1), b.get(m), lstCustLatLng.get(i)) <= 0) {
					b.remove(m);
					m = m - 1;
				}
				m = m + 1;//inc(m,1);
				//				b.add(new LatLngVO(lstCustLatLng.get(i)));
				b.add(lstCustLatLng.get(i));
			}
		}
		return b;
	}

	public String getAllListCustomerForRouting() {
		try {
			ObjectVO<CarVO> vo = null;
			CarFilter filter = new CarFilter();
			filter.setCarId(carId);
			filter.setDeliveryDate(DateUtil.parse(deliveryDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			vo = superviserLogisticsMgr.getListCustomerByRouting(filter);
			List<RoutingCar> lstRC = superviserLogisticsMgr.getRoutingCarByFilter(filter);
			if (lstRC != null && lstRC.size() > 0 && lstRC.get(0).getStaff() != null) {
				result.put("nvgh", lstRC.get(0).getStaff().getId());
			}
			result.put("lst", vo.getLstObject());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	public String getListCustomerForRouting() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<CarVO> kPaging = new KPaging<CarVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			CarFilter filter = new CarFilter();
			filter.setkPaging(kPaging);
			filter.setCarId(carId);
			filter.setDeliveryDate(DateUtil.parse(deliveryDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			ObjectVO<CarVO> vo = superviserLogisticsMgr.getListCustomerByRouting(filter);
			result.put("total", vo.getkPaging().getTotalRows());
			result.put("rows", vo.getLstObject());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	public String updateRoute() {
		resetToken(result);
		try {
			result.put(ERROR, false);
			Date date = DateUtil.parse(deliveryDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (lstId != null && lstId.size() > 0 && lstCustomerId != null && lstSeq != null) {
				RoutingCar rcar = new RoutingCar();
				Long idTemp = 0l;
				for (int i = 0; i < lstId.size(); i++) {
					if (lstId.get(i) != 0) {
						idTemp = lstId.get(i);
						break;
					}
				}
				if (idTemp == 0) {
					Car car = carMgr.getCarById(carId);
					if (car != null) {
						rcar.setCar(car);
						rcar.setShop(rcar.getCar().getShop());
					}
					if (superviserLogisticsMgr.checkDriverHasCar(null, nvghId, date)) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "supervise.logistics.driver.has.car"));
						return JSON;
					}
					if (nvghId != null && nvghId != -1) {
						Staff staff = staffMgr.getStaffById(nvghId);
						rcar.setStaff(staff);
					}
					rcar.setDeliveryDate(date);
					rcar.setStatus(ActiveType.RUNNING);
					rcar = superviserLogisticsMgr.createRoutingCar(rcar, getLogInfoVO());
				} else {
					CarFilter filter = new CarFilter();
					filter.setCarId(carId);
					filter.setDeliveryDate(date);
					List<RoutingCar> lst = superviserLogisticsMgr.getRoutingCarByFilter(filter);
					if (lst != null && lst.size() > 0) {
						rcar = lst.get(0);
						Staff staff = null;
						if (superviserLogisticsMgr.checkDriverHasCar(rcar.getId(), nvghId, date)) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "supervise.logistics.driver.has.car"));
							return JSON;
						}
						if (nvghId != null && nvghId != -1) {
							staff = staffMgr.getStaffById(nvghId);
						}
						rcar.setStaff(staff);
						superviserLogisticsMgr.updateRoutingCar(rcar, getLogInfoVO());
					}
				}
				if (idTemp != 0) {
					RoutingCarCust rcarCust = superviserLogisticsMgr.getRoutingCarCustById(idTemp);
					if (rcarCust != null && rcarCust.getRoutingCar() != null) {
						rcar = rcarCust.getRoutingCar();
					} else {//loi du lieu
						result.put(ERROR, true);
						return JSON;
					}
				}
				for (int i = 0; i < lstId.size(); i++) {
					if (lstId.get(i) == null || lstId.get(i) == 0) {//create
						if (rcar != null) {
							RoutingCarCust rCust = new RoutingCarCust();
							Customer cust = customerMgr.getCustomerById(lstCustomerId.get(i));
							if (cust != null) {
								rCust.setCustomer(cust);
							}
							rCust.setRoutingCar(rcar);
							if (lstSeq.get(i) != null && lstSeq.get(i) != 0) {
								rCust.setSeq(lstSeq.get(i));
							}
							superviserLogisticsMgr.createRoutingCarCust(rCust, getLogInfoVO());
						}
					} else {//update
						RoutingCarCust rCust = superviserLogisticsMgr.getRoutingCarCustById(lstId.get(i));
						if (rCust != null) {
							if (lstSeq.get(i) != null && lstSeq.get(i) == 0) {
								rCust.setSeq(null);
							} else {
								rCust.setSeq(lstSeq.get(i));
							}
							superviserLogisticsMgr.updateRoutingCarCust(rCust, getLogInfoVO());
						}
					}
				}
			} else if (carId != null && !StringUtil.isNullOrEmpty(deliveryDate)) {
				CarFilter filter = new CarFilter();
				filter.setCarId(carId);
				filter.setDeliveryDate(date);
				List<RoutingCar> lst = superviserLogisticsMgr.getRoutingCarByFilter(filter);
				if (lst != null && lst.size() > 0) {
					RoutingCar rcar = lst.get(0);
					Staff staff = null;
					if (superviserLogisticsMgr.checkDriverHasCar(rcar.getId(), nvghId, date)) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "supervise.logistics.driver.has.car"));
						return JSON;
					}
					if (nvghId != null && nvghId != -1) {
						staff = staffMgr.getStaffById(nvghId);
					}
					rcar.setStaff(staff);
					superviserLogisticsMgr.updateRoutingCar(rcar, getLogInfoVO());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
		}
		return JSON;
	}

	public String getListNVGH() {
		try {
			result.put(ERROR, false);
			Car car = carMgr.getCarById(carId);
			if (car != null && car.getShop() != null) {
				StaffFilter filter = new StaffFilter();
				filter.setStaffType(StaffObjectType.DRIVER);
				filter.setShopId(car.getShop().getId());
				filter.setIsGetShopOnly(false);
				ObjectVO<Staff> voStaff = staffMgr.getListStaff(filter);
				if (voStaff != null) {
					result.put("lst", voStaff.getLstObject());
					return JSON;
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
		}
		result.put("lst", new ArrayList<Staff>());
		return JSON;
	}

	/**
	 * @author tungmt
	 * @description Bao cao vt11
	 * @param shopId
	 *            , fromDate,toDate
	 * @since 06/06/2014
	 */
	public String exportVT11() {
		Date fDate = null;
		Date tDate = null;
		rUtils = new ReportUtilsEx();
		result.put(ERROR, true);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Shop shop = staff.getShop();
			//------------------------------------Begin validate------------------------------------
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			if (StringUtil.isNullOrEmpty(fromDate)) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.fromdate.null"));
				return JSON;

			}
			if (StringUtil.isNullOrEmpty(toDate)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.todate.null"));
				return JSON;
			}
			fDate = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			tDate = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			if (fDate == null || tDate == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.date.invalid.format"));
				return JSON;
			}
			//------------------------------------End Validate------------------------------------
			//GetDetail
			CarFilter filter = new CarFilter();
			filter.setLstShopId(lstShopId);
			filter.setFromDate(fDate);
			filter.setToDate(tDate);
			List<CarVO> lstDetail = superviserLogisticsMgr.getBCVT11(filter);
			if (lstDetail == null || lstDetail.size() <= 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			//Init XSSF workboook
			String outputName = "bc_vt_1_1_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			int rowNumInWindow = 1000; //Number of remain row in Window;
			SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Sheet 1");
			Map<String, XSSFCellStyle> styles = ExcelProcessUtilsEx.createStylesPOI(workbook);

			//set header & menu Row width
			rUtils.setRowsHeight(sheet, 0, 15, 15, 15, 12, 25, 15, 12, 25);

			//set static Col width
			rUtils.setColumnsWidth(sheet, 0, 150, 250, 100, 250, 100, 250, 300, 130, 80, 80, 250, 300, 130);

			sheet.setDisplayGridlines(false);

			//------------------ Header Area ------------------
			Calendar calFDate = Calendar.getInstance();
			calFDate.setTime(fDate);
			Calendar calTDate = Calendar.getInstance();
			calTDate.setTime(tDate);
			rUtils.addCells(sheet, 0, 0, 3, 0, shop.getShopName().toUpperCase(), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 1, 3, 1, shop.getAddress() != null ? shop.getAddress().toUpperCase() : "", styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 2, 3, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "reprot.device.print.date") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 4, 6, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.1.lan.vung").toUpperCase(), styles.get(ExcelProcessUtilsEx.HEADER));
			rUtils.addCells(sheet, 0, 5, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.tu.dau.ngay.den.cuoi.ngay", fromDate, toDate), styles.get(ExcelProcessUtilsEx.BOLD_CENTER));
			//------------------ End Header Area ------------------

			//------------------ Menu Area ------------------
			//reset Menu Style's all border to Thin
			ExcelProcessUtilsEx.setBorderForCell(styles.get(ExcelProcessUtilsEx.MENU), BorderStyle.THIN, ExcelProcessUtilsEx.poiBlack);
			//static columns
			String[] str = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.1.lan.vung.menu").split(";");
			for (int i = 0; i < str.length; i++) {
				rUtils.addCell(sheet, i, 7, str[i], styles.get(ExcelProcessUtilsEx.MENU));
			}

			//------------------ Detail Area ------------------
			int indexRow = 8;
			XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_CENTER).getFont().setBold(true);
			styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));
			CarVO temp = lstDetail.get(0);
			Long carIdTemp = temp.getCarId();
			for (int index = 1, sizeTmp1 = lstDetail.size(); index < sizeTmp1; index++) {
				XSSFCellStyle curFormatLeft = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_LEFT);
				XSSFCellStyle curFormatRight = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT);
				XSSFCellStyle curFormatCenter = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_CENTER);
				CarVO elem = lstDetail.get(index);
				if (elem.getCarId() == carIdTemp) {
					if (elem.getIsWarning() == 0 && temp.getIsWarning() == 1) {
						if (elem.getCreateDate().equals(temp.getCreateDate())) {
							rUtils.addCell(sheet, 0, indexRow, temp.getMien(), curFormatLeft);
							rUtils.addCell(sheet, 1, indexRow, temp.getShopCode(), curFormatLeft);
							rUtils.addCell(sheet, 2, indexRow, temp.getCarNumber(), curFormatLeft);
							rUtils.addCell(sheet, 3, indexRow, temp.getStaffCode(), curFormatLeft);
							rUtils.addCell(sheet, 4, indexRow, temp.getCreateDate(), curFormatCenter);
							rUtils.addCell(sheet, 5, indexRow, temp.getCustomerCode(), curFormatLeft);
							rUtils.addCell(sheet, 6, indexRow, temp.getAddress(), curFormatLeft);
							rUtils.addCell(sheet, 7, indexRow, temp.getDistance() != null ? temp.getDistance().toString() : "", curFormatRight);
							rUtils.addCell(sheet, 8, indexRow, temp.getCreateTime().toString(), curFormatCenter);
							rUtils.addCell(sheet, 9, indexRow, elem.getCreateTime().toString(), curFormatCenter);
							rUtils.addCell(sheet, 10, indexRow, elem.getCustomerCode(), curFormatLeft);
							rUtils.addCell(sheet, 11, indexRow, elem.getAddress(), curFormatLeft);
							rUtils.addCell(sheet, 12, indexRow, elem.getDistance() != null ? elem.getDistance().toString() : "", curFormatRight);
							indexRow++;
						} else {//khac ngay thi tach thanh 2 dong
							rUtils.addCell(sheet, 0, indexRow, temp.getMien(), curFormatLeft);
							rUtils.addCell(sheet, 1, indexRow, temp.getShopCode(), curFormatLeft);
							rUtils.addCell(sheet, 2, indexRow, temp.getCarNumber(), curFormatLeft);
							rUtils.addCell(sheet, 3, indexRow, temp.getStaffCode(), curFormatLeft);
							rUtils.addCell(sheet, 4, indexRow, temp.getCreateDate(), curFormatCenter);
							rUtils.addCell(sheet, 5, indexRow, temp.getCustomerCode(), curFormatLeft);
							rUtils.addCell(sheet, 6, indexRow, temp.getAddress(), curFormatLeft);
							rUtils.addCell(sheet, 7, indexRow, temp.getDistance() != null ? temp.getDistance().toString() : "", curFormatRight);
							rUtils.addCell(sheet, 8, indexRow, temp.getCreateTime().toString(), curFormatCenter);
							rUtils.addCell(sheet, 9, indexRow, "24:00", curFormatCenter);
							rUtils.addCell(sheet, 10, indexRow, "", curFormatLeft);
							rUtils.addCell(sheet, 11, indexRow, "", curFormatLeft);
							rUtils.addCell(sheet, 12, indexRow, "", curFormatRight);
							indexRow++;
							rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
							rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
							rUtils.addCell(sheet, 2, indexRow, elem.getCarNumber(), curFormatLeft);
							rUtils.addCell(sheet, 3, indexRow, elem.getStaffCode(), curFormatLeft);
							rUtils.addCell(sheet, 4, indexRow, elem.getCreateDate(), curFormatCenter);
							rUtils.addCell(sheet, 5, indexRow, "", curFormatLeft);
							rUtils.addCell(sheet, 6, indexRow, "", curFormatLeft);
							rUtils.addCell(sheet, 7, indexRow, "", curFormatRight);
							rUtils.addCell(sheet, 8, indexRow, "00:00", curFormatCenter);
							rUtils.addCell(sheet, 9, indexRow, elem.getCreateTime().toString(), curFormatCenter);
							rUtils.addCell(sheet, 10, indexRow, elem.getCustomerCode(), curFormatLeft);
							rUtils.addCell(sheet, 11, indexRow, elem.getAddress(), curFormatLeft);
							rUtils.addCell(sheet, 12, indexRow, elem.getDistance() != null ? elem.getDistance().toString() : "", curFormatRight);
							indexRow++;
						}
					}
				} else if (temp.getIsWarning() == 1) {
					rUtils.addCell(sheet, 0, indexRow, temp.getMien(), curFormatLeft);
					rUtils.addCell(sheet, 1, indexRow, temp.getShopCode(), curFormatLeft);
					rUtils.addCell(sheet, 2, indexRow, temp.getCarNumber(), curFormatLeft);
					rUtils.addCell(sheet, 3, indexRow, temp.getStaffCode(), curFormatLeft);
					rUtils.addCell(sheet, 4, indexRow, temp.getCreateDate(), curFormatCenter);
					rUtils.addCell(sheet, 5, indexRow, temp.getCustomerCode(), curFormatLeft);
					rUtils.addCell(sheet, 6, indexRow, temp.getAddress(), curFormatLeft);
					rUtils.addCell(sheet, 7, indexRow, temp.getDistance() != null ? temp.getDistance().toString() : "", curFormatRight);
					rUtils.addCell(sheet, 8, indexRow, temp.getCreateTime().toString(), curFormatCenter);
					rUtils.addCell(sheet, 9, indexRow, "", curFormatCenter);
					rUtils.addCell(sheet, 10, indexRow, "", curFormatLeft);
					rUtils.addCell(sheet, 11, indexRow, "", curFormatLeft);
					rUtils.addCell(sheet, 12, indexRow, "", curFormatRight);
					indexRow++;
				}
				carIdTemp = elem.getCarId();
				temp = elem;
			}
			sheet.flushRows();
			//------------------ End Detail Area ------------------

			FileOutputStream out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "SupperviseLogisticsAction.exportVT11  - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @description Bao cao vt12
	 * @param shopId
	 *            , fromDate,toDate
	 * @since 18/06/2014
	 */
	public String exportVT12() {
		Date fDate = null;
		Date tDate = null;
		rUtils = new ReportUtilsEx();
		result.put(ERROR, true);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Shop shop = staff.getShop();
			//------------------------------------Begin validate------------------------------------
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			if (StringUtil.isNullOrEmpty(fromDate)) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.fromdate.null"));
				return JSON;

			}
			if (StringUtil.isNullOrEmpty(toDate)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.todate.null"));
				return JSON;
			}
			fDate = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			tDate = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			if (fDate == null || tDate == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.date.invalid.format"));
				return JSON;
			}
			//------------------------------------End Validate------------------------------------
			//GetDetail
			CarFilter filter = new CarFilter();
			filter.setLstShopId(lstShopId);
			filter.setFromDate(fDate);
			filter.setToDate(tDate);
			List<CarVO> lstDetail = superviserLogisticsMgr.getBCVT12(filter);
			if (lstDetail == null || lstDetail.size() <= 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			//Init XSSF workboook
			String outputName = "bc_vt_1_2_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			int rowNumInWindow = 1000; //Number of remain row in Window;
			SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Sheet 1");
			Map<String, XSSFCellStyle> styles = ExcelProcessUtilsEx.createStylesPOI(workbook);

			//set header & menu Row width
			rUtils.setRowsHeight(sheet, 0, 15, 15, 15, 12, 25, 15, 12, 25);

			//set static Col width
			rUtils.setColumnsWidth(sheet, 0, 150, 250, 100, 250, 100, 80, 80, 250, 160, 100, 250);

			sheet.setDisplayGridlines(false);

			//------------------ Header Area ------------------
			Calendar calFDate = Calendar.getInstance();
			calFDate.setTime(fDate);
			Calendar calTDate = Calendar.getInstance();
			calTDate.setTime(tDate);
			rUtils.addCells(sheet, 0, 0, 3, 0, shop.getShopName().toUpperCase(), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 1, 3, 1, shop.getAddress() != null ? shop.getAddress().toUpperCase() : "", styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 2, 3, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "reprot.device.print.date") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 4, 6, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.2.dung.do").toUpperCase(), styles.get(ExcelProcessUtilsEx.HEADER));
			rUtils.addCells(sheet, 0, 5, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.tu.dau.ngay.den.cuoi.ngay", fromDate, toDate), styles.get(ExcelProcessUtilsEx.BOLD_CENTER));
			//------------------ End Header Area ------------------

			//------------------ Menu Area ------------------
			//reset Menu Style's all border to Thin
			ExcelProcessUtilsEx.setBorderForCell(styles.get(ExcelProcessUtilsEx.MENU), BorderStyle.THIN, ExcelProcessUtilsEx.poiBlack);
			//static columns
			String[] str = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.2.dung.do.menu").split(";");
			for (int i = 0; i < str.length; i++) {
				rUtils.addCell(sheet, i, 7, str[i], styles.get(ExcelProcessUtilsEx.MENU));
			}

			//------------------ Detail Area ------------------
			int indexRow = 8;
			XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_CENTER).getFont().setBold(true);
			styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));
			CarVO temp = lstDetail.get(0);
			Long carIdTemp = temp.getCarId();
			for (int index = 1, sizeTmp1 = lstDetail.size(); index < sizeTmp1; index++) {
				XSSFCellStyle curFormatLeft = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_LEFT);
				XSSFCellStyle curFormatRight = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT);
				XSSFCellStyle curFormatCenter = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_CENTER);
				CarVO elem = lstDetail.get(index);
				if (elem.getCarId() == carIdTemp) {
					if (elem.getGpsSpeed() > 0 && temp.getGpsSpeed() == 0 && DateUtil.dateDiffMinute(temp.getTime(), elem.getTime()) > 15) {
						String isOr = "";
						if (!StringUtil.isNullOrEmpty(temp.getCustomerCode())) {
							isOr = temp.getIsOr().equals("0") ? "Ngoại tuyến" : "Trong tuyến";
						}
						if (elem.getCreateDate().equals(temp.getCreateDate())) {
							rUtils.addCell(sheet, 0, indexRow, temp.getMien(), curFormatLeft);
							rUtils.addCell(sheet, 1, indexRow, temp.getShopCode(), curFormatLeft);
							rUtils.addCell(sheet, 2, indexRow, temp.getCarNumber(), curFormatLeft);
							rUtils.addCell(sheet, 3, indexRow, temp.getStaffCode(), curFormatLeft);
							rUtils.addCell(sheet, 4, indexRow, temp.getCreateDate(), curFormatCenter);
							rUtils.addCell(sheet, 5, indexRow, temp.getCreateTime().toString(), curFormatCenter);
							rUtils.addCell(sheet, 6, indexRow, elem.getCreateTime().toString(), curFormatCenter);
							rUtils.addCell(sheet, 7, indexRow, temp.getCustomerCode(), curFormatLeft);
							rUtils.addCell(sheet, 8, indexRow, temp.getDistance() != null ? temp.getDistance().toString() : "", curFormatRight);
							rUtils.addCell(sheet, 9, indexRow, isOr, curFormatLeft);
							rUtils.addCell(sheet, 10, indexRow, temp.getTongSoDiemGiao(), curFormatRight);
							indexRow++;
						} else {//khac ngay thi tach thanh 2 dong
							rUtils.addCell(sheet, 0, indexRow, temp.getMien(), curFormatLeft);
							rUtils.addCell(sheet, 1, indexRow, temp.getShopCode(), curFormatLeft);
							rUtils.addCell(sheet, 2, indexRow, temp.getCarNumber(), curFormatLeft);
							rUtils.addCell(sheet, 3, indexRow, temp.getStaffCode(), curFormatLeft);
							rUtils.addCell(sheet, 4, indexRow, temp.getCreateDate(), curFormatCenter);
							rUtils.addCell(sheet, 5, indexRow, temp.getCreateTime().toString(), curFormatCenter);
							rUtils.addCell(sheet, 6, indexRow, "24:00", curFormatCenter);
							rUtils.addCell(sheet, 7, indexRow, temp.getCustomerCode(), curFormatLeft);
							rUtils.addCell(sheet, 8, indexRow, temp.getDistance() != null ? temp.getDistance().toString() : "", curFormatRight);
							rUtils.addCell(sheet, 9, indexRow, isOr, curFormatLeft);
							rUtils.addCell(sheet, 10, indexRow, temp.getTongSoDiemGiao(), curFormatRight);
							indexRow++;
							rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
							rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
							rUtils.addCell(sheet, 2, indexRow, elem.getCarNumber(), curFormatLeft);
							rUtils.addCell(sheet, 3, indexRow, elem.getStaffCode(), curFormatLeft);
							rUtils.addCell(sheet, 4, indexRow, elem.getCreateDate(), curFormatCenter);
							rUtils.addCell(sheet, 5, indexRow, "00:00", curFormatCenter);
							rUtils.addCell(sheet, 6, indexRow, elem.getCreateTime().toString(), curFormatCenter);
							rUtils.addCell(sheet, 7, indexRow, elem.getCustomerCode(), curFormatLeft);
							rUtils.addCell(sheet, 8, indexRow, elem.getDistance() != null ? temp.getDistance().toString() : "", curFormatRight);
							rUtils.addCell(sheet, 9, indexRow, isOr, curFormatLeft);
							rUtils.addCell(sheet, 10, indexRow, elem.getTongSoDiemGiao(), curFormatRight);
							indexRow++;
						}
					}
				} else if (temp.getGpsSpeed() == 0) {
					rUtils.addCell(sheet, 0, indexRow, temp.getMien(), curFormatLeft);
					rUtils.addCell(sheet, 1, indexRow, temp.getShopCode(), curFormatLeft);
					rUtils.addCell(sheet, 2, indexRow, temp.getCarNumber(), curFormatLeft);
					rUtils.addCell(sheet, 3, indexRow, temp.getStaffCode(), curFormatLeft);
					rUtils.addCell(sheet, 4, indexRow, temp.getCreateDate(), curFormatCenter);
					rUtils.addCell(sheet, 5, indexRow, temp.getCreateTime().toString(), curFormatCenter);
					rUtils.addCell(sheet, 6, indexRow, "", curFormatCenter);
					rUtils.addCell(sheet, 7, indexRow, temp.getCustomerCode(), curFormatLeft);
					rUtils.addCell(sheet, 8, indexRow, temp.getDistance() != null ? temp.getDistance().toString() : "", curFormatRight);
					rUtils.addCell(sheet, 9, indexRow, temp.getIsOr().equals("0") ? "Ngoại tuyến" : "Trong tuyến", curFormatLeft);
					rUtils.addCell(sheet, 10, indexRow, temp.getTongSoDiemGiao(), curFormatRight);
					indexRow++;
				}
				carIdTemp = elem.getCarId();
				temp = elem;

			}
			sheet.flushRows();
			//------------------ End Detail Area ------------------

			FileOutputStream out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "SupperviseLogisticsAction.exportVT11  - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @description Bao cao vt13
	 * @param shopId
	 *            , fromDate,toDate
	 * @since 20/06/2014
	 */
	public String exportVT13() {
		Date fDate = null;
		Date tDate = null;
		rUtils = new ReportUtilsEx();
		result.put(ERROR, true);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Shop shop = staff.getShop();
			//------------------------------------Begin validate------------------------------------
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			if (StringUtil.isNullOrEmpty(fromDate)) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.fromdate.null"));
				return JSON;

			}
			if (StringUtil.isNullOrEmpty(toDate)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.todate.null"));
				return JSON;
			}
			fDate = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			tDate = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			if (fDate == null || tDate == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.date.invalid.format"));
				return JSON;
			}
			//------------------------------------End Validate------------------------------------
			//GetDetail
			CarFilter filter = new CarFilter();
			filter.setLstShopId(lstShopId);
			filter.setFromDate(fDate);
			filter.setToDate(tDate);
			List<CarVO> lstDetail = superviserLogisticsMgr.getBCVT13(filter);
			if (lstDetail == null || lstDetail.size() <= 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			//Init XSSF workboook
			String outputName = "bc_vt_1_3_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			int rowNumInWindow = 1000; //Number of remain row in Window;
			SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Sheet 1");
			Map<String, XSSFCellStyle> styles = ExcelProcessUtilsEx.createStylesPOI(workbook);

			//set header & menu Row width
			rUtils.setRowsHeight(sheet, 0, 15, 15, 15, 12, 25, 15, 12, 25);

			//set static Col width
			rUtils.setColumnsWidth(sheet, 0, 150, 250, 100, 250, 100, 250, 300, 200, 80, 80, 250, 300, 200);

			sheet.setDisplayGridlines(false);

			//------------------ Header Area ------------------
			Calendar calFDate = Calendar.getInstance();
			calFDate.setTime(fDate);
			Calendar calTDate = Calendar.getInstance();
			calTDate.setTime(tDate);
			rUtils.addCells(sheet, 0, 0, 3, 0, shop.getShopName().toUpperCase(), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 1, 3, 1, shop.getAddress() != null ? shop.getAddress().toUpperCase() : "", styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 2, 3, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "reprot.device.print.date") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 4, 9, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.3.mat.tin.hieu.xe").toUpperCase(), styles.get(ExcelProcessUtilsEx.HEADER));
			rUtils.addCells(sheet, 0, 5, 9, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.tu.dau.ngay.den.cuoi.ngay", fromDate, toDate), styles.get(ExcelProcessUtilsEx.BOLD_CENTER));
			//------------------ End Header Area ------------------

			//------------------ Menu Area ------------------
			//reset Menu Style's all border to Thin
			ExcelProcessUtilsEx.setBorderForCell(styles.get(ExcelProcessUtilsEx.MENU), BorderStyle.THIN, ExcelProcessUtilsEx.poiBlack);
			//static columns
			String[] str = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.3.mat.tin.hieu.xe.menu").split(";");
			for (int i = 0; i < str.length; i++) {
				rUtils.addCell(sheet, i, 7, str[i], styles.get(ExcelProcessUtilsEx.MENU));
			}

			//------------------ Detail Area ------------------
			int indexRow = 8;
			XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_CENTER).getFont().setBold(true);
			styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));
			for (int index = 0, sizeTmp1 = lstDetail.size(); index < sizeTmp1; index++) {
				XSSFCellStyle curFormatLeft = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_LEFT);
				XSSFCellStyle curFormatRight = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT);
				XSSFCellStyle curFormatCenter = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_CENTER);
				CarVO elem = lstDetail.get(index);
				if (elem.getLastCreateDate().equals(elem.getCreateDate())) {
					rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
					rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
					rUtils.addCell(sheet, 2, indexRow, elem.getCarNumber(), curFormatLeft);
					rUtils.addCell(sheet, 3, indexRow, elem.getStaffCode(), curFormatLeft);
					rUtils.addCell(sheet, 4, indexRow, elem.getLastCreateDate(), curFormatCenter);
					rUtils.addCell(sheet, 5, indexRow, elem.getLastCustomerCode(), curFormatLeft);
					rUtils.addCell(sheet, 6, indexRow, elem.getLastAddress(), curFormatLeft);
					rUtils.addCell(sheet, 7, indexRow, elem.getLastDistance() != null ? elem.getLastDistance().toString() : "", curFormatRight);
					rUtils.addCell(sheet, 8, indexRow, elem.getLastCreateTime(), curFormatCenter);
					rUtils.addCell(sheet, 9, indexRow, elem.getCreateTime(), curFormatCenter);
					rUtils.addCell(sheet, 10, indexRow, elem.getCustomerCode(), curFormatLeft);
					rUtils.addCell(sheet, 11, indexRow, elem.getAddress(), curFormatLeft);
					rUtils.addCell(sheet, 12, indexRow, elem.getDistance() != null ? elem.getDistance().toString() : "", curFormatRight);
					indexRow++;
				} else {//tach thanh 2 ngay
					Date fd = DateUtil.parse(elem.getLastCreateDate() + " " + elem.getLastCreateTime(), DateUtil.DATETIME_FORMAT_STR);
					Date td = DateUtil.parse(elem.getLastCreateDate() + " 23:59", DateUtil.DATETIME_FORMAT_STR);
					long time = DateUtil.dateDiffMinute(fd, td);
					if (time >= 30) {//neu thoi gian hom truoc vuot qua 15p thi ghi file
						rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
						rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
						rUtils.addCell(sheet, 2, indexRow, elem.getCarNumber(), curFormatLeft);
						rUtils.addCell(sheet, 3, indexRow, elem.getStaffCode(), curFormatLeft);
						rUtils.addCell(sheet, 4, indexRow, elem.getLastCreateDate(), curFormatCenter);
						rUtils.addCell(sheet, 5, indexRow, elem.getLastCustomerCode(), curFormatLeft);
						rUtils.addCell(sheet, 6, indexRow, elem.getLastAddress(), curFormatLeft);
						rUtils.addCell(sheet, 7, indexRow, elem.getLastDistance() != null ? elem.getLastDistance().toString() : "", curFormatRight);
						rUtils.addCell(sheet, 8, indexRow, elem.getLastCreateTime(), curFormatCenter);
						rUtils.addCell(sheet, 9, indexRow, "24:00", curFormatCenter);
						rUtils.addCell(sheet, 10, indexRow, "", curFormatLeft);
						rUtils.addCell(sheet, 11, indexRow, "", curFormatLeft);
						rUtils.addCell(sheet, 12, indexRow, "", curFormatRight);
						indexRow++;
					}
					fd = DateUtil.parse(elem.getCreateDate() + " 00:00", DateUtil.DATETIME_FORMAT_STR);
					td = DateUtil.parse(elem.getCreateDate() + " " + elem.getCreateTime(), DateUtil.DATETIME_FORMAT_STR);
					time = DateUtil.dateDiffMinute(fd, td);
					if (time > 30) {
						rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
						rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
						rUtils.addCell(sheet, 2, indexRow, elem.getCarNumber(), curFormatLeft);
						rUtils.addCell(sheet, 3, indexRow, elem.getStaffCode(), curFormatLeft);
						rUtils.addCell(sheet, 4, indexRow, elem.getCreateDate(), curFormatCenter);
						rUtils.addCell(sheet, 5, indexRow, "", curFormatLeft);
						rUtils.addCell(sheet, 6, indexRow, "", curFormatLeft);
						rUtils.addCell(sheet, 7, indexRow, "", curFormatRight);
						rUtils.addCell(sheet, 8, indexRow, "00:00", curFormatCenter);
						rUtils.addCell(sheet, 9, indexRow, elem.getCreateTime(), curFormatCenter);
						rUtils.addCell(sheet, 10, indexRow, elem.getCustomerCode(), curFormatLeft);
						rUtils.addCell(sheet, 11, indexRow, elem.getAddress(), curFormatLeft);
						rUtils.addCell(sheet, 12, indexRow, elem.getDistance() != null ? elem.getDistance().toString() : "", curFormatRight);
						indexRow++;
					}
				}
			}
			sheet.flushRows();
			//------------------ End Detail Area ------------------

			FileOutputStream out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "SupperviseLogisticsAction.exportVT11  - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @description Bao cao vt14
	 * @param shopId
	 *            , carNumber
	 * @since 07/06/2014
	 */
	public String exportVT14() {
		Date fDate = null;
		Date tDate = null;
		rUtils = new ReportUtilsEx();
		result.put(ERROR, true);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Shop shop = staff.getShop();
			//------------------------------------Begin validate------------------------------------
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			//------------------------------------End Validate------------------------------------
			//GetDetail
			//			Float latLngDistance=0.023f; 
			//			ApParam temp = apParamMgr.getApParamByCode("LATLNG_CONFIG", null);
			//			if(temp!=null){
			//				latLngDistance=Float.parseFloat(temp.getValue());
			//			}
			CarFilter filter = new CarFilter();
			filter.setLstShopId(lstShopId);
			filter.setCarNumber(carNumber);
			//			filter.setLatLngDistance(latLngDistance);
			List<CarVO> lstDetail = superviserLogisticsMgr.getBCVT14(filter);
			if (lstDetail == null || lstDetail.size() <= 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			//Init XSSF workboook
			String outputName = "bc_vt_1_4_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			int rowNumInWindow = 1000; //Number of remain row in Window;
			SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Sheet 1");
			Map<String, XSSFCellStyle> styles = ExcelProcessUtilsEx.createStylesPOI(workbook);

			//set header & menu Row width
			rUtils.setRowsHeight(sheet, 0, 15, 15, 15, 12, 25, 15, 12, 25);

			//set static Col width
			rUtils.setColumnsWidth(sheet, 0, 120, 120, 100, 250, 120, 120, 250, 300, 150);

			sheet.setDisplayGridlines(false);

			//------------------ Header Area ------------------
			Date now = DateUtil.now();
			Calendar calFDate = Calendar.getInstance();
			calFDate.setTime(now);

			rUtils.addCells(sheet, 0, 0, 3, 0, shop.getShopName().toUpperCase(), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 1, 3, 1, shop.getAddress() != null ? shop.getAddress().toUpperCase() : "", styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 2, 3, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "reprot.device.print.date") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 4, 8, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.4.vi.tri.xe").toUpperCase(), styles.get(ExcelProcessUtilsEx.HEADER));
			rUtils.addCells(sheet, 0, 5, 8, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.ngay.gio.phut", DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY), String.valueOf(DateUtil.getHour24(now)), String
					.valueOf(DateUtil.getMinute(now))), styles.get(ExcelProcessUtilsEx.BOLD_CENTER));
			//------------------ End Header Area ------------------

			//------------------ Menu Area ------------------
			//reset Menu Style's all border to Thin
			ExcelProcessUtilsEx.setBorderForCell(styles.get(ExcelProcessUtilsEx.MENU), BorderStyle.THIN, ExcelProcessUtilsEx.poiBlack);
			//static columns
			String[] str = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.4.vi.tri.xe.menu").split(";");
			for (int i = 0; i < str.length; i++) {
				rUtils.addCell(sheet, i, 7, str[i], styles.get(ExcelProcessUtilsEx.MENU));
			}

			//------------------ Detail Area ------------------
			int indexRow = 8;
			XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_CENTER).getFont().setBold(true);
			styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));

			for (int index = 0, sizeTmp1 = lstDetail.size(); index < sizeTmp1; index++) {
				XSSFCellStyle curFormatLeft = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_LEFT);
				XSSFCellStyle curFormatRight = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT);
				XSSFCellStyle curFormatCenter = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_CENTER);
				CarVO elem = lstDetail.get(index);
				rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
				rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
				rUtils.addCell(sheet, 2, indexRow, elem.getCarNumber(), curFormatLeft);
				rUtils.addCell(sheet, 3, indexRow, elem.getStaffCode(), curFormatLeft);
				rUtils.addCell(sheet, 4, indexRow, elem.getLat() != null ? elem.getLat().toString() : "", curFormatLeft);
				rUtils.addCell(sheet, 5, indexRow, elem.getLng() != null ? elem.getLng().toString() : "", curFormatLeft);
				rUtils.addCell(sheet, 6, indexRow, elem.getCustomerCode(), curFormatLeft);
				rUtils.addCell(sheet, 7, indexRow, elem.getAddress(), curFormatLeft);
				rUtils.addCell(sheet, 8, indexRow, elem.getDistance() != null ? elem.getDistance().toString() : "", curFormatRight);
				indexRow++;
			}
			sheet.flushRows();
			//------------------ End Detail Area ------------------

			FileOutputStream out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "SupperviseLogisticsAction.exportVT11  - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @description Bao cao vt15
	 * @param shopId
	 *            ,carNumber, fromDate
	 * @since 06/06/2014
	 */
	public String exportVT15() {
		Date fDate = null;
		Date tDate = null;
		rUtils = new ReportUtilsEx();
		result.put(ERROR, true);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Shop shop = staff.getShop();
			//------------------------------------Begin validate------------------------------------
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			if (StringUtil.isNullOrEmpty(fromDate)) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.fromdate.null"));
				return JSON;

			}
			if (StringUtil.isNullOrEmpty(toDate)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.todate.null"));
				return JSON;
			}
			fDate = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			tDate = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			if (fDate == null || tDate == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.date.invalid.format"));
				return JSON;
			}
			//------------------------------------End Validate------------------------------------
			//GetDetail
			CarFilter filter = new CarFilter();
			filter.setLstShopId(lstShopId);
			filter.setFromDate(fDate);
			filter.setToDate(tDate);
			filter.setCarNumber(carNumber);
			List<CarVO> lstDetail = superviserLogisticsMgr.getBCVT15(filter);
			if (lstDetail == null || lstDetail.size() <= 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			//Init XSSF workboook
			String outputName = "bc_vt_1_5_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			int rowNumInWindow = 1000; //Number of remain row in Window;
			SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Sheet 1");
			Map<String, XSSFCellStyle> styles = ExcelProcessUtilsEx.createStylesPOI(workbook);

			//set header & menu Row width
			rUtils.setRowsHeight(sheet, 0, 15, 15, 15, 12, 25, 15, 12, 25);

			//set static Col width
			rUtils.setColumnsWidth(sheet, 0, 150, 250, 100, 250, 250, 300, 100, 200, 200, 100);

			sheet.setDisplayGridlines(false);

			//------------------ Header Area ------------------
			Calendar calFDate = Calendar.getInstance();
			calFDate.setTime(fDate);
			rUtils.addCells(sheet, 0, 0, 3, 0, shop.getShopName().toUpperCase(), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 1, 3, 1, shop.getAddress() != null ? shop.getAddress().toUpperCase() : "", styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 2, 3, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "reprot.device.print.date") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 4, 9, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.5.tuyen").toUpperCase(), styles.get(ExcelProcessUtilsEx.HEADER));
			rUtils.addCells(sheet, 0, 5, 9, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.ngay", fromDate), styles.get(ExcelProcessUtilsEx.BOLD_CENTER));
			//------------------ End Header Area ------------------

			//------------------ Menu Area ------------------
			//reset Menu Style's all border to Thin
			ExcelProcessUtilsEx.setBorderForCell(styles.get(ExcelProcessUtilsEx.MENU), BorderStyle.THIN, ExcelProcessUtilsEx.poiBlack);
			//static columns
			String[] str = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.5.tuyen.menu").split(";");
			for (int i = 0; i < str.length; i++) {
				rUtils.addCell(sheet, i, 7, str[i], styles.get(ExcelProcessUtilsEx.MENU));
			}

			//------------------ Detail Area ------------------
			int indexRow = 8;
			XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_CENTER).getFont().setBold(true);
			styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));

			for (int index = 0, sizeTmp1 = lstDetail.size(); index < sizeTmp1; index++) {
				XSSFCellStyle curFormatLeft = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_LEFT);
				XSSFCellStyle curFormatRight = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT);
				XSSFCellStyle curFormatCenter = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_CENTER);
				CarVO elem = lstDetail.get(index);
				rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
				rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
				rUtils.addCell(sheet, 2, indexRow, elem.getCarNumber(), curFormatLeft);
				rUtils.addCell(sheet, 3, indexRow, elem.getStaffCode(), curFormatLeft);
				rUtils.addCell(sheet, 4, indexRow, elem.getCustomerCode(), curFormatLeft);
				rUtils.addCell(sheet, 5, indexRow, elem.getAddress(), curFormatLeft);
				rUtils.addCell(sheet, 6, indexRow, elem.getIsOr(), curFormatCenter);
				rUtils.addCell(sheet, 7, indexRow, elem.getSeq().toString(), curFormatRight);
				rUtils.addCell(sheet, 8, indexRow, elem.getSeqSet() != null ? elem.getSeqSet().toString() : "", curFormatRight);
				rUtils.addCell(sheet, 9, indexRow, elem.getCreateTime(), curFormatCenter);
				indexRow++;
			}
			sheet.flushRows();
			//------------------ End Detail Area ------------------

			FileOutputStream out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "SupperviseLogisticsAction.exportVT15  - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @description Bao cao vt16
	 * @param shopId
	 *            ,carNumber, deliverDate
	 * @since 06/06/2014
	 */
	public String exportVT16() {
		Date fDate = null;
		Date tDate = null;
		rUtils = new ReportUtilsEx();
		result.put(ERROR, true);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Shop shop = staff.getShop();
			//------------------------------------Begin validate------------------------------------
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			if (StringUtil.isNullOrEmpty(fromDate)) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.fromdate.null"));
				return JSON;

			}
			if (StringUtil.isNullOrEmpty(toDate)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.todate.null"));
				return JSON;
			}
			fDate = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			tDate = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			if (fDate == null || tDate == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.date.invalid.format"));
				return JSON;
			}
			//------------------------------------End Validate------------------------------------
			//GetDetail
			CarFilter filter = new CarFilter();
			filter.setLstShopId(lstShopId);
			filter.setFromDate(fDate);
			filter.setToDate(fDate);
			filter.setCarNumber(carNumber);
			List<CarVO> lstDetail = superviserLogisticsMgr.getBCVT16(filter);
			if (lstDetail == null || lstDetail.size() <= 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			//Init XSSF workboook
			String outputName = "bc_vt_1_6_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			int rowNumInWindow = 1000; //Number of remain row in Window;
			SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Sheet 1");
			Map<String, XSSFCellStyle> styles = ExcelProcessUtilsEx.createStylesPOI(workbook);

			//set header & menu Row width
			rUtils.setRowsHeight(sheet, 0, 15, 15, 15, 12, 25, 15, 12, 25);

			//set static Col width
			rUtils.setColumnsWidth(sheet, 0, 250, 150, 250, 250, 250);

			sheet.setDisplayGridlines(false);

			//------------------ Header Area ------------------
			Calendar calFDate = Calendar.getInstance();
			calFDate.setTime(fDate);
			rUtils.addCells(sheet, 0, 0, 3, 0, shop.getShopName().toUpperCase(), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 1, 3, 1, shop.getAddress() != null ? shop.getAddress().toUpperCase() : "", styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 2, 3, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "reprot.device.print.date") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 4, 4, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.6.tuyen").toUpperCase(), styles.get(ExcelProcessUtilsEx.HEADER));
			rUtils.addCells(sheet, 0, 5, 4, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.ngay", fromDate), styles.get(ExcelProcessUtilsEx.BOLD_CENTER));
			//------------------ End Header Area ------------------

			//------------------ Menu Area ------------------
			//reset Menu Style's all border to Thin
			ExcelProcessUtilsEx.setBorderForCell(styles.get(ExcelProcessUtilsEx.MENU), BorderStyle.THIN, ExcelProcessUtilsEx.poiBlack);
			//static columns
			String[] str = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.vt1.6.tuyen.menu").split(";");
			for (int i = 0; i < str.length; i++) {
				rUtils.addCell(sheet, i, 7, str[i], styles.get(ExcelProcessUtilsEx.MENU));
			}

			//------------------ Detail Area ------------------
			int indexRow = 8;
			XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_CENTER).getFont().setBold(true);
			styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));

			for (int index = 0, sizeTmp1 = lstDetail.size(); index < sizeTmp1; index++) {
				XSSFCellStyle curFormatLeft = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_LEFT);
				XSSFCellStyle curFormatRight = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT);
				XSSFCellStyle curFormatCenter = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_CENTER);
				CarVO elem = lstDetail.get(index);
				rUtils.addCell(sheet, 0, indexRow, elem.getShopCode(), curFormatLeft);
				rUtils.addCell(sheet, 1, indexRow, elem.getCarNumber(), curFormatLeft);
				rUtils.addCell(sheet, 2, indexRow, elem.getDriverCode(), curFormatLeft);
				rUtils.addCell(sheet, 3, indexRow, elem.getStaffCode(), curFormatLeft);
				rUtils.addCell(sheet, 4, indexRow, elem.getCustomerCode(), curFormatLeft);
				indexRow++;
			}
			sheet.flushRows();
			//------------------ End Detail Area ------------------

			FileOutputStream out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "SupperviseLogisticsAction.exportVT15  - " + e.getMessage());
		}
		return JSON;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public List<JETreeGridLogisticsNode> getLstCarTreeResult() {
		return lstCarTreeResult;
	}

	public void setLstCarTreeResult(List<JETreeGridLogisticsNode> lstCarTreeResult) {
		this.lstCarTreeResult = lstCarTreeResult;
	}

	public List<CarVO> getLstShopCar() {
		return lstShopCar;
	}

	public void setLstShopCar(List<CarVO> lstShopCar) {
		this.lstShopCar = lstShopCar;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public List<CarVO> getLstCust() {
		return lstCust;
	}

	public void setLstCust(List<CarVO> lstCust) {
		this.lstCust = lstCust;
	}

	public List<LatLngVO> getLstCustLatLng() {
		return lstCustLatLng;
	}

	public void setLstCustLatLng(List<LatLngVO> lstCustLatLng) {
		this.lstCustLatLng = lstCustLatLng;
	}

	public Long getMaxLogId() {
		return maxLogId;
	}

	public void setMaxLogId(Long maxLogId) {
		this.maxLogId = maxLogId;
	}

	public List<Car> getLstCar() {
		return lstCar;
	}

	public void setLstCar(List<Car> lstCar) {
		this.lstCar = lstCar;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}

	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<Integer> getLstSeq() {
		return lstSeq;
	}

	public void setLstSeq(List<Integer> lstSeq) {
		this.lstSeq = lstSeq;
	}

	public List<Staff> getLstDeliveryStaff() {
		return lstDeliveryStaff;
	}

	public void setLstDeliveryStaff(List<Staff> lstDeliveryStaff) {
		this.lstDeliveryStaff = lstDeliveryStaff;
	}

	public Long getNvghId() {
		return nvghId;
	}

	public void setNvghId(Long nvghId) {
		this.nvghId = nvghId;
	}

	public Long getRoutingId() {
		return routingId;
	}

	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

}

package ths.dms.web.action.supervise;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffRoleType;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.StaffListPositionVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.JETreeGridNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class SupperviseSalesStaffAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private String shopCode;
	private String shopName;
	private Long shopIdSearch;
	private String staffCode;
	private Long staffId;
	private Integer type;
	private Staff staff;
	private Long customerId;
	private Long idStaffSelected;
	private Long parentStaffId;
	/*
	 * @PhuT variable
	 */
	private Long gdmId;
	private Long tbhvId;
	private Long nvgsId;
	private Long nvbhId;

	private Integer lhl;
	private List<JETreeGridNode> lstStaffTreeResult;

	private StaffMgr staffMgr;
	private SuperviserMgr superviserMgr;
	private CustomerMgr customerMgr;

	/** For Giam sat lo trinh NVBH */
	private String dateTime;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		superviserMgr = (SuperviserMgr) context.getBean("superviserMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");

		if (currentUser != null) {
			shopId = currentUser.getShopRoot().getShopId();
		}
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		return SUCCESS;
	}

	public JETreeGridNode converterTreeVOToTreeNode(TreeVO<StaffPositionVO> treevo, List<StaffPositionVO> lstStaffTraining) {
		StaffPositionVO staff = treevo.getObject();
		List<JETreeGridNode> lstStaffTree = new ArrayList<JETreeGridNode>();
		if (lhl != null && lhl != 0 && staff.getRoleType() != null && staff.getRoleType() < 3) {
			boolean flag = false;
			for (int k = 0; k < lstStaffTraining.size(); k++) {
				if (lstStaffTraining.get(k).getStaffId().equals(staff.getStaffId())) {
					flag = true;
					break;
				}
				if (lstStaffTraining.get(k).getStaffOwnerId().equals(staff.getStaffId())) {
					flag = true;
					break;
				}
			}
			if (!flag)
				return null;//ko co lich huan luyen
		}
		staff.setStaffName(getRoleName(staff.getRoleType()) + staff.getStaffName());
		if (staff.getCreateTime() != null) {
			staff.setHhmm(new SimpleDateFormat("HH:mm").format(staff.getCreateTime()));
		}
		JETreeGridNode node = new JETreeGridNode();
		node.setText(staff.getStaffName());
		node.setId(staff.getStaffId());
		node.setAttr(staff);
		if (staff.getCountStaff() != null && staff.getCountStaff() > 0 && staff.getRoleType() != 1) {
			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
		} else if (staff.getCountStaff() != null) {
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
		} else {
			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
		}
		if (treevo != null && treevo.getListChildren() != null && treevo.getListChildren().size() > 0) {
			for (TreeVO<StaffPositionVO> temp : treevo.getListChildren()) {
				JETreeGridNode nodeTemp = converterTreeVOToTreeNode(temp, lstStaffTraining);
				if (nodeTemp != null)
					lstStaffTree.add(nodeTemp);
			}
			node.setChildren(lstStaffTree);
			if (lstStaffTree != null && lstStaffTree.size() == 0) {
				node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			} else
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
		}
		return node;
	}

	public String getRoleName(Integer roleType) {
		if (StaffObjectType.NHVNM.getValue().equals(roleType)) {
			return "VNM - ";
		} else if (StaffObjectType.TBHM.getValue().equals(roleType)) {
			return "GĐM - ";
		} else if (StaffObjectType.TBHV.getValue().equals(roleType)) {
			return "TBHV - ";
		} else if (StaffObjectType.NVGS.getValue().equals(roleType)) {
			return "GSNPP - ";
		} else {
			return "";
		}
	}

	public String getListStaffPositionOneNode() {
		/*
		 * try { Date date = DateUtil.now(); if
		 * (!StringUtil.isNullOrEmpty(dateTime)) { date =
		 * DateUtil.parse(dateTime, DateUtil.DATE_FORMAT_STR); }
		 * List<JETreeGridNode> lstStaffTreeCurrent = new
		 * ArrayList<JETreeGridNode>(); List<JETreeGridNode> lstStaffTree = new
		 * ArrayList<JETreeGridNode>(); Staff staffCurrent = null; Shop
		 * currentShop = null; if (currentUser != null &&
		 * currentUser.getShopRoot() != null) { currentShop =
		 * shopMgr.getShopById(currentUser.getShopRoot().getShopId()); }
		 * staffCurrent = staffMgr.getStaffByCode(currentUser.getUserName()); if
		 * (staffCurrent == null) { return JSON; } Staff loadUser = null;
		 * SupFilter filter = new SupFilter(); filter.setOneNode(true);
		 * filter.setDateTime(date); filter.setShopId(shopId); if (currentShop
		 * != null) { List<StaffPositionVO> lstStaff = null;
		 * List<StaffPositionVO> lstStaffTraining = null; Long shopIdSearch =
		 * null;//dung de get staff co trainingplan if
		 * (staffCurrent.getStaffType() != null &&
		 * !StaffObjectType.KTNPP.getValue
		 * ().equals(staffCurrent.getStaffType().getObjectType()) &&
		 * (!StringUtil.isNullOrEmpty(shopCode) ||
		 * !StringUtil.isNullOrEmpty(shopName))) { if (staffCurrent != null &&
		 * staffCurrent.getShop() != null) { loadUser = staffCurrent;
		 * shopIdSearch = staffCurrent.getShop().getId(); }
		 * filter.setLoginStaff(staffCurrent); filter.setShopCode(shopCode);
		 * filter.setShopName(shopName); filter.setDateTime(date);
		 * TreeVO<StaffPositionVO> treevo = staffMgr.searchStaffEx(filter); if
		 * (lhl != null && lhl == 1) { lstStaffTraining =
		 * staffMgr.getListStaffPositionWithTraining(shopIdSearch); } if (treevo
		 * != null) { for (TreeVO<StaffPositionVO> temp :
		 * treevo.getListChildren()) { JETreeGridNode node =
		 * converterTreeVOToTreeNode(temp, lstStaffTraining); if (node != null)
		 * { lstStaffTree.add(node); } } } } else { if (staffId == null ||
		 * staffId == 0) { if (staffCurrent != null && staffCurrent.getShop() !=
		 * null) { loadUser = staffCurrent; if (shopId != null && shopId > 0)
		 * {//lay theo id search shopIdSearch = shopId; } else { shopId =
		 * loadUser.getShop().getId(); shopIdSearch =
		 * loadUser.getShop().getId(); }
		 * filter.setRoleType(staffCurrent.getStaffType() != null ?
		 * staffCurrent.getStaffType().getObjectType() : null);
		 * filter.setStaffId(loadUser.getId()); lstStaff =
		 * staffMgr.getListStaffPositionOptionsDate(filter); for
		 * (StaffPositionVO vo : lstStaff) {
		 * vo.setStaffOwnerId(loadUser.getId()); } } } else { Staff staff =
		 * staffMgr.getStaffById(staffId); if (staff != null) { shopIdSearch =
		 * staff.getShop().getId(); filter.setStaffId(staffId); lstStaff =
		 * staffMgr.getListStaffPositionOptionsDate(filter); for
		 * (StaffPositionVO vo : lstStaff) { vo.setStaffOwnerId(staff.getId());
		 * } } } if (lhl != null && lhl == 1) { lstStaffTraining =
		 * staffMgr.getListStaffPositionWithTraining(shopIdSearch); } if
		 * (lstStaff != null && lstStaff.size() > 0) { for (StaffPositionVO temp
		 * : lstStaff) { temp.setStaffName(getRoleName(temp.getRoleType()) +
		 * temp.getStaffName()); if (temp.getCreateTime() != null) {
		 * temp.setHhmm(new
		 * SimpleDateFormat("HH:mm").format(temp.getCreateTime())); } }
		 * List<Long> lstException = new ArrayList<Long>(); for (int i = 0; i <
		 * lstStaff.size(); i++) { JETreeGridNode node = new JETreeGridNode();
		 * node.setText(lstStaff.get(i).getStaffName());
		 * node.setId(lstStaff.get(i).getStaffId());
		 * node.setAttr(lstStaff.get(i)); if (lstStaff.get(i).getRoleType() == 1
		 * || lstStaff.get(i).getRoleType() == 2) {
		 * node.setState(ConstantManager.JSTREE_STATE_LEAF); } else if
		 * (lstStaff.get(i).getCountStaff() != null &&
		 * lstStaff.get(i).getCountStaff() > 0) {
		 * node.setState(ConstantManager.JSTREE_STATE_CLOSE); } else if
		 * (lstStaff.get(i).getCountStaff() != null) {
		 * node.setState(ConstantManager.JSTREE_STATE_LEAF); } else {
		 * node.setState(ConstantManager.JSTREE_STATE_CLOSE); } if (lhl == null
		 * || lhl == 0 || lstStaff.get(i).getRoleType() == null ||
		 * (lstStaff.get(i).getRoleType() != null &&
		 * !StaffObjectType.NVBH.getValue
		 * ().equals(lstStaff.get(i).getRoleType())) &&
		 * !StaffObjectType.NVVS.getValue
		 * ().equals(lstStaff.get(i).getRoleType())) { lstStaffTree.add(node); }
		 * else {// filter lay ra danh sach co nv co lich huan luyen for (int k
		 * = 0; k < lstStaffTraining.size(); k++) { if
		 * (lstStaffTraining.get(k).getStaffId
		 * ().equals(lstStaff.get(i).getStaffId())) { lstStaffTree.add(node); }
		 * if (lstStaffTraining.get(k).getStaffOwnerId().equals(lstStaff.get(i).
		 * getStaffId())) { Boolean flag = true; for (int j = 0; j <
		 * lstException.size(); j++) {//neu chua add vao tree thi add if
		 * (lstStaffTraining
		 * .get(k).getStaffOwnerId().equals(lstException.get(j))) { flag =
		 * false; break; } } if (flag) { lstStaffTree.add(node);
		 * lstException.add(lstStaffTraining.get(k).getStaffOwnerId()); } } } }
		 * } } } } if (loadUser != null) { if
		 * (StaffObjectType.KTNPP.getValue().equals
		 * (loadUser.getStaffType().getObjectType())) { lstStaffTreeResult =
		 * lstStaffTree; } else {
		 * loadUser.setStaffName(getRoleName(loadUser.getStaffType
		 * ().getObjectType()) + loadUser.getStaffName()); JETreeGridNode node =
		 * new JETreeGridNode(); if
		 * (StaffObjectType.NHVNM.getValue().equals(staffCurrent
		 * .getStaffType().getObjectType())) {
		 * node.setText(staffCurrent.getStaffName()); StaffPositionVO cNodeAttr
		 * = new StaffPositionVO(); cNodeAttr.setStaffId(staffCurrent.getId());
		 * cNodeAttr.setStaffName(staffCurrent.getStaffName());
		 * cNodeAttr.setRoleType(StaffObjectType.NHVNM.getValue());
		 * cNodeAttr.setShopCode(staffCurrent.getShop().getShopCode());
		 * cNodeAttr.setStaffOwnerId(-1L); node.setAttr(cNodeAttr);
		 * node.setState(ConstantManager.JSTREE_STATE_OPEN);
		 * 
		 * node.setChildren(lstStaffTree); node.setId(staffCurrent.getId()); }
		 * else { node.setText(loadUser.getStaffName()); StaffPositionVO
		 * cNodeAttr = new StaffPositionVO();
		 * cNodeAttr.setStaffId(loadUser.getId());
		 * cNodeAttr.setStaffName(loadUser.getStaffName());
		 * cNodeAttr.setRoleType(loadUser.getStaffType().getObjectType());
		 * cNodeAttr.setShopCode(loadUser.getShop().getShopCode());
		 * cNodeAttr.setStaffOwnerId(-1L); node.setAttr(cNodeAttr); if
		 * (lstStaffTree != null && lstStaffTree.size() > 0) {
		 * node.setState(ConstantManager.JSTREE_STATE_OPEN); } else {
		 * node.setState(ConstantManager.JSTREE_STATE_LEAF); }
		 * node.setChildren(lstStaffTree); node.setId(loadUser.getId()); }
		 * 
		 * lstStaffTreeCurrent.add(node); lstStaffTreeResult =
		 * lstStaffTreeCurrent; } return JSON; } lstStaffTreeResult =
		 * lstStaffTree; } catch (Exception e) { LogUtility.logError(e,
		 * e.getMessage()); }
		 */
		return JSON;
	}

	//	public String getListCustomerBySaleStaffHasVisitPlan(){
	//		try {
	//			if(staffId==null){
	//				return JSON;
	//			}
	//		    List<CustomerVO> list = superviserMgr.getListCustomerBySaleStaffHasVisitPlan(staffId);
	//            result.put("customers",list);
	//			
	//		} catch (Exception e) {
	//			LogUtility.logError(e,"SupperviseSalesAction.getListCustomerBySaleStaffHasVisitPlan");
	//		}
	//		return JSON;
	//	}

	public String getListStaffListPositionHasVisitPlan() {
		/*
		 * Date date = DateUtil.now(); if (!StringUtil.isNullOrEmpty(dateTime))
		 * { date = DateUtil.parse(dateTime, DateUtil.DATE_FORMAT_STR); }
		 * 
		 * if (currentUser != null) { staffCode = currentUser.getUserName(); try
		 * { Staff staff = staffMgr.getStaffByCode(staffCode); if (staff !=
		 * null) { StaffListPositionVO staffListPosition = null; SupFilter
		 * filter = new SupFilter(); filter.setStaffId(idStaffSelected);
		 * filter.setDateTime(date); filter.setLoginStaffId(staff.getId());
		 * filter.setRoleType(staff.getStaffType().getObjectType());
		 * filter.setShopId(staff.getShop().getId()); staffListPosition =
		 * staffMgr.getStaffPositionEx(filter);
		 * 
		 * Staff staffOwner = staffMgr.getStaffById(parentStaffId); if
		 * (staffOwner != null) {
		 * staffListPosition.setStaffOwnerId(staff.getId());
		 * staffListPosition.setStaffOwnerCode(staff.getStaffCode());
		 * staffListPosition.setStaffOwnerName(staff.getStaffName()); }
		 * result.put("staffListPosition", staffListPosition); result.put(ERROR,
		 * false); } } catch (BusinessException e) { LogUtility.logError(e,
		 * "SupperviseSalesAction.getListStaffPositionHasVisitPlan");
		 * result.put(ERROR, true); result.put("errMsg",
		 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM)); } }
		 */
		return JSON;
	}

	public String showDirectionStaff() {
		try {
			if (staffId == null) {
				return JSON;
			}
			List<StaffPositionLog> list = superviserMgr.showDirectionStaff(staffId);
			result.put("list", list);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getListCustomerBySaleStaffHasVisitPlan");
		}
		return JSON;
	}

	public Long getId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getShopIdSearch() {
		return shopIdSearch;
	}

	public void setShopIdSearch(Long shopIdSearch) {
		this.shopIdSearch = shopIdSearch;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getGdmId() {
		return gdmId;
	}

	public void setGdmId(Long gdmId) {
		this.gdmId = gdmId;
	}

	public Long getTbhvId() {
		return tbhvId;
	}

	public void setTbhvId(Long tbhvId) {
		this.tbhvId = tbhvId;
	}

	public Long getNvgsId() {
		return nvgsId;
	}

	public void setNvgsId(Long nvgsId) {
		this.nvgsId = nvgsId;
	}

	public Long getNvbhId() {
		return nvbhId;
	}

	public void setNvbhId(Long nvbhId) {
		this.nvbhId = nvbhId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getLhl() {
		return lhl;
	}

	public void setLhl(Integer lhl) {
		this.lhl = lhl;
	}

	public List<JETreeGridNode> getLstStaffTreeResult() {
		return lstStaffTreeResult;
	}

	public void setLstStaffTreeResult(List<JETreeGridNode> lstStaffTreeResult) {
		this.lstStaffTreeResult = lstStaffTreeResult;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public Long getIdStaffSelected() {
		return idStaffSelected;
	}

	public void setIdStaffSelected(Long idStaffSelected) {
		this.idStaffSelected = idStaffSelected;
	}

	public Long getParentStaffId() {
		return parentStaffId;
	}

	public void setParentStaffId(Long parentStaffId) {
		this.parentStaffId = parentStaffId;
	}

}

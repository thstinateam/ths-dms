package ths.dms.web.action.supervise;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserDeviceMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ToolFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ToolVO;
import ths.dms.gateway.mqtt.producer.FridgeRequestProducer;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.JETreeGridDeviceNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtilsEx;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelProcessUtilsEx;

public class SupperviseDeviceAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long shopId;

	private StaffMgr staffMgr;
	private ShopMgr shopMgr;
	private SuperviserDeviceMgr superviserDeviceMgr;
	private ApParamMgr apParamMgr;

	private List<ToolVO> lstShopTool;
	private List<JETreeGridDeviceNode> lstToolTreeResult;

	private Integer typeSup;
	private Integer distance = 15;
	private String distanceConfig;
	private String shopCode;
	private String shopName;
	private String toolCode;
	private String customerCode;
	private String customerName;
	private Boolean isWarning;
	private Long chipId;
	private String chipCode;
	private Float minTemp;
	private Float maxTemp;
	private String tempConfig;
	private String positionConfig;
	private String fromDate;
	private String toDate;
	private String lat;
	private String lng;
	private ReportUtilsEx rUtils;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		try {
			shopMgr = (ShopMgr) context.getBean("shopMgr");
			staffMgr = (StaffMgr) context.getBean("staffMgr");
			superviserDeviceMgr = (SuperviserDeviceMgr) context.getBean("superviserDeviceMgr");
			apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
			ApParam temp = apParamMgr.getApParamByCode("DISTANCE_CONFIG", null);
			if (temp != null) {
				distance = Integer.parseInt(temp.getValue());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		if (currentUser != null) {
			Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
			if (staff != null) {
				if (shopId == null)
					shopId = staff.getShop().getId();
			}
		}
		return SUCCESS;
	}

	public String executeConfig() throws Exception {
		resetToken(result);
		if (currentUser != null) {
			Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
			if (staff != null) {
				if (shopId == null)
					shopId = staff.getShop().getId();
				ApParam temp = apParamMgr.getApParamByCode("TEMP_CONFIG", null);
				ApParam pos = apParamMgr.getApParamByCode("POSITION_CONFIG", null);
				if (temp != null) {
					tempConfig = temp.getValue();
				}
				if (pos != null) {
					positionConfig = pos.getValue();
				}
				if (distance != null) {
					distanceConfig = distance.toString();
				}
			}
		}
		return SUCCESS;
	}

	public String executeBC() throws Exception {
		resetToken(result);
		if (currentUser != null) {
			Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
			if (staff != null) {
				if (shopId == null)
					shopId = staff.getShop().getId();
			}
		}
		Date now = new Date();
		fromDate = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
		toDate = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
		return SUCCESS;
	}

	public String getListShopTool() {
		try {
			if (currentUser != null) {
				Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null) {
					shopId = staff.getShop().getId();
				}
			}
			if (typeSup != null && typeSup == 1) {//nhiet do
				lstShopTool = superviserDeviceMgr.getListShopTool(shopId, 1);
			} else {//vi tri
				lstShopTool = superviserDeviceMgr.getListShopTool(shopId, 0);
			}
			result.put(ERROR, false);
			result.put("lstShop", lstShopTool);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.getListShopTool");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListTool() {
		try {
			if (typeSup != null && typeSup == 1) {//nhiet do
				lstShopTool = superviserDeviceMgr.getListTool(shopId, 1);
			} else {//vi tri
				lstShopTool = superviserDeviceMgr.getListTool(shopId, 0);
			}
			result.put(ERROR, false);
			result.put("lstTool", lstShopTool);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.getListTool");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListToolKP() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<ToolVO>());
		try {
			KPaging<ToolVO> kPaging = new KPaging<ToolVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ToolFilter filter = new ToolFilter();
			filter.setkPaging(kPaging);
			filter.setParentShopId(shopId);
			filter.setTypeSup(typeSup);
			filter.setToolCode(toolCode);
			filter.setCustomerCode(customerCode);
			filter.setIsWarning(isWarning);
			ObjectVO<ToolVO> vo = superviserDeviceMgr.getListToolKP(filter);
			if (vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.getListToolKP");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public JETreeGridDeviceNode convertToolToNode(ToolVO temp) {
		JETreeGridDeviceNode node = new JETreeGridDeviceNode();
		node.setId(temp.getShopId());
		node.setText(temp.getShopCode() + " - " + temp.getShopName());
		node.setAttr(temp);
		if (ShopObjectType.NPP.getValue().equals(temp.getShopType())) {
			node.setState(ConstantManager.JSTREE_STATE_LEAF);
		} else {
			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
		}
		return node;
	}

	public List<JETreeGridDeviceNode> searchShop(List<Shop> lstShop, Integer distance, Integer typeSup) {
		List<JETreeGridDeviceNode> lstTreeRoot = new ArrayList<JETreeGridDeviceNode>();
		JETreeGridDeviceNode root = new JETreeGridDeviceNode();
		try {
			Long idBold = lstShop.get(0).getId();
			int level = lstShop.size() - 1;
			Integer countTool = 0, countWarning = 0;
			List<ToolVO> lstShopTool1 = superviserDeviceMgr.getListShopToolOneNode(lstShop.get(level).getId(), typeSup);
			if (level >= 0 && lstShopTool1 != null && lstShopTool1.size() > 0) {
				level--;
				List<JETreeGridDeviceNode> lstTree1 = new ArrayList<JETreeGridDeviceNode>();
				for (ToolVO tool1 : lstShopTool1) {
					if (tool1.getShopId().equals(idBold)) {
						tool1.setIsBold(true);
					}
					countTool = countTool + tool1.getCountTool();
					countWarning = countWarning + tool1.getCountWarning();
					JETreeGridDeviceNode node1 = convertToolToNode(tool1);
					lstTree1.add(node1);
					if (level >= 0 && lstShop.get(level).getId().equals(tool1.getShopId())) {
						List<ToolVO> lstShopTool2 = superviserDeviceMgr.getListShopToolOneNode(lstShop.get(level).getId(), typeSup);
						if (level >= 0 && lstShopTool2 != null && lstShopTool2.size() > 0) {
							level--;
							List<JETreeGridDeviceNode> lstTree2 = new ArrayList<JETreeGridDeviceNode>();
							node1.setChildren(lstTree2);
							node1.setState(ConstantManager.JSTREE_STATE_OPEN);
							for (ToolVO tool2 : lstShopTool2) {
								if (tool2.getShopId().equals(idBold)) {
									tool2.setIsBold(true);
								}
								JETreeGridDeviceNode node2 = convertToolToNode(tool2);
								lstTree2.add(node2);
								if (level >= 0 && lstShop.get(level).getId().equals(tool2.getShopId())) {
									List<ToolVO> lstShopTool3 = superviserDeviceMgr.getListShopToolOneNode(lstShop.get(level).getId(), typeSup);
									if (level >= 0 && lstShopTool3 != null && lstShopTool3.size() > 0) {
										level--;
										List<JETreeGridDeviceNode> lstTree3 = new ArrayList<JETreeGridDeviceNode>();
										node2.setChildren(lstTree3);
										node2.setState(ConstantManager.JSTREE_STATE_OPEN);
										for (ToolVO tool3 : lstShopTool3) {
											if (tool3.getShopId().equals(idBold)) {
												tool3.setIsBold(true);
											}
											JETreeGridDeviceNode node3 = convertToolToNode(tool3);
											lstTree3.add(node3);
										}
									}
								}
							}
						}
					}
				}
				ToolVO toolVO = new ToolVO();
				toolVO.setShopId(lstShop.get(lstShop.size() - 1).getId());
				toolVO.setCountTool(countTool);
				toolVO.setCountWarning(countWarning);
				toolVO.setShopCode(lstShop.get(lstShop.size() - 1).getShopCode());
				toolVO.setShopName(lstShop.get(lstShop.size() - 1).getShopName());
				toolVO.setShopType(0);
				root = convertToolToNode(toolVO);
				root.setChildren(lstTree1);
				root.setState(ConstantManager.JSTREE_STATE_OPEN);
			}
			lstTreeRoot.add(root);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.searchShop");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return lstTreeRoot;
	}

	public String getListSubShopTool() {
		try {
			result.put(ERROR, false);
			Shop currentShop = getCurrentShop();
			if (typeSup == null) {
				typeSup = 0;
			}
			Shop shop = shopMgr.getShopById(shopId);
			if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
				lstShopTool = superviserDeviceMgr.getListShopToolOneNode(shop.getId(), typeSup);
				result.put("lst", lstShopTool);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.getListSubShopTool");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListShopToolOneNode() {
		try {
			Shop currentShop = getCurrentShop();
			lstToolTreeResult = new ArrayList<JETreeGridDeviceNode>();
			if (typeSup == null) {
				typeSup = 0;
			}
			if (shopId == null || shopId == 0) {
				if (currentShop != null) {
					if (!StringUtil.isNullOrEmpty(shopCode) || !StringUtil.isNullOrEmpty(shopName)) {
						List<Shop> lstShop = superviserDeviceMgr.getListAncestor(currentShop.getId(), shopCode, shopName);
						if (lstShop == null || lstShop.size() == 0) {//load mac dinh
							lstShopTool = superviserDeviceMgr.getListShopToolOneNode(currentShop.getId(), typeSup);
						} else {
							lstToolTreeResult = searchShop(lstShop, distance, typeSup);
							return JSON;
						}
					} else {
						lstShopTool = superviserDeviceMgr.getListShopToolOneNode(currentShop.getId(), typeSup);
					}
				}
			} else {
				Shop shop = shopMgr.getShopById(shopId);
				if (currentShop != null && shop != null && shopMgr.checkAncestor(currentShop.getShopCode(), shop.getShopCode())) {
					lstShopTool = superviserDeviceMgr.getListShopToolOneNode(shop.getId(), typeSup);
				}
			}
			List<JETreeGridDeviceNode> lstTreeGrid = new ArrayList<JETreeGridDeviceNode>();
			Integer countTool = 0, countWarning = 0;
			if (lstShopTool != null && lstShopTool.size() > 0) {
				for (ToolVO temp : lstShopTool) {
					countTool += temp.getCountTool();
					countWarning += temp.getCountWarning();
					JETreeGridDeviceNode node = convertToolToNode(temp);
					lstTreeGrid.add(node);
				}
			}
			if ((shopId == null || shopId == 0)) {
				ToolVO toolVO = new ToolVO();
				toolVO.setShopId(currentShop.getId());
				toolVO.setCountTool(countTool);
				toolVO.setCountWarning(countWarning);
				toolVO.setShopCode(currentShop.getShopCode());
				toolVO.setShopName(currentShop.getShopName());
				toolVO.setShopType(0);
				JETreeGridDeviceNode node = convertToolToNode(toolVO);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setChildren(lstTreeGrid);
				lstToolTreeResult.add(node);
			} else {
				lstToolTreeResult = lstTreeGrid;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.getListShopToolOneNode");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String updateChipTemp() {
		resetToken(result);
		result.put(ERROR, false);
		try {
			if (currentUser != null && chipId != null && minTemp != null && maxTemp != null) {
				superviserDeviceMgr.updateChip(chipId, minTemp, maxTemp);
				ToolFilter filter = new ToolFilter();
				filter.setChipId(chipId);
				ObjectVO<ToolVO> vo = superviserDeviceMgr.getListChip(filter);
				if (vo != null && vo.getLstObject() != null && !vo.getLstObject().isEmpty()) {
					for (ToolVO child : vo.getLstObject()) {
						configNguong(child.getChipCode(), child.getChipVer(), minTemp.intValue(), maxTemp.intValue());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.updateChip");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String updateChipPosition() {
		resetToken(result);
		result.put(ERROR, false);
		try {
			if (currentUser != null && chipId != null && lat != null && lng != null) {
				ToolFilter filter = new ToolFilter();
				filter.setChipId(chipId);
				ObjectVO<ToolVO> vo = superviserDeviceMgr.getListChip(filter);
				if (vo != null && vo.getLstObject() != null && !vo.getLstObject().isEmpty()) {
					for (ToolVO child : vo.getLstObject()) {
						configVitri(child.getChipCode(), child.getChipVer(), lat, lng);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.updateChipPosition");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String updateConfig() {
		resetToken(result);
		result.put(ERROR, false);
		try {
			if (currentUser != null) {
				if (distanceConfig != null) {
					int distanceInterval = Integer.parseInt(distanceConfig);
					superviserDeviceMgr.updateConfigDistance(distanceInterval);
					ToolFilter filter = new ToolFilter();
					ObjectVO<ToolVO> vo = superviserDeviceMgr.getListChip(filter);
					if (vo != null && vo.getLstObject() != null && !vo.getLstObject().isEmpty()) {
						for (ToolVO child : vo.getLstObject()) {
							configNguongViTri(child.getChipCode(), child.getChipVer(), distanceInterval);
						}
					}
				} else {
					int gpsInterval = Integer.parseInt(positionConfig);
					int tempInterval = Integer.parseInt(tempConfig);
					superviserDeviceMgr.updateConfig(tempInterval, gpsInterval);
					ToolFilter filter = new ToolFilter();
					ObjectVO<ToolVO> vo = superviserDeviceMgr.getListChip(filter);
					if (vo != null && vo.getLstObject() != null && !vo.getLstObject().isEmpty()) {
						for (ToolVO child : vo.getLstObject()) {
							configTansuat(child.getChipCode(), child.getChipVer(), gpsInterval, tempInterval);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.updateConfig");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListChip() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<ToolVO>());
		try {
			KPaging<ToolVO> kPaging = new KPaging<ToolVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ToolFilter filter = new ToolFilter();
			filter.setkPaging(kPaging);
			filter.setChipCode(chipCode);
			filter.setMinTemp(minTemp);
			filter.setMaxTemp(maxTemp);
			ObjectVO<ToolVO> vo = superviserDeviceMgr.getListChip(filter);
			if (vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.getListChip");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getListChipForCustomer() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<ToolVO>());
		try {
			KPaging<ToolVO> kPaging = new KPaging<ToolVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ToolFilter filter = new ToolFilter();
			filter.setkPaging(kPaging);
			filter.setChipCode(chipCode);
			filter.setCustomerCode(customerCode);
			filter.setCustomerName(customerName);
			ObjectVO<ToolVO> vo = superviserDeviceMgr.getListChipForCustomer(filter);
			if (vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseDeviceAction.getListChipForCustomer");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	private void configTansuat(String chipCode, String chipVer, int gpsInterval, int tempInterval) {
		FridgeRequestProducer producer = new FridgeRequestProducer(chipVer + "." + chipCode);
		//gpsInterval:tần suất gửi GPS, tempInterval: tần suất gửi nhiệt độ. Nếu gửi thành công commandId>0, nếu thất bại =-1
		try {
			int commandId = producer.requestUpdateInterval(gpsInterval, tempInterval);
			System.out.println("cau hinh tan suat " + chipVer + "." + chipCode + " :" + commandId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtility.logError(e, "SupperviseDeviceAction.configTansuat:" + e.getMessage());
		}
	}

	private void configNguong(String chipCode, String chipVer, int minTemp, int maxTemp) {
		FridgeRequestProducer producer = new FridgeRequestProducer(chipVer + "." + chipCode);
		//gpsInterval:tần suất gửi GPS, tempInterval: tần suất gửi nhiệt độ. Nếu gửi thành công commandId>0, nếu thất bại =-1
		try {
			int commandId = producer.requestUpdateTempThreshold(minTemp, maxTemp);
			System.out.println("cau hinh nguong nhiet do " + chipVer + "." + chipCode + " :" + commandId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtility.logError(e, "SupperviseDeviceAction.configNguong:" + e.getMessage());
		}
	}

	public static void configVitri(String chipCode, String chipVer, String lat, String lng) {
		FridgeRequestProducer producer = new FridgeRequestProducer(chipVer + "." + chipCode);
		try {
			int commandId = producer.requestConfigLatLng(Double.parseDouble(lat), Double.parseDouble(lng));
			System.out.println("cau hinh vi tri " + chipVer + "." + chipCode + commandId);
		} catch (Exception e) {
			LogUtility.logError(e, "configVitri - " + e.getMessage());
		}
	}

	public static void configNguongViTri(String chipCode, String chipVer, Integer distance) {
		FridgeRequestProducer producer = new FridgeRequestProducer(chipVer + "." + chipCode);
		try {
			int commandId = producer.requestConfigDistance(distance);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtility.logError(e, "configNguongViTri - " + e.getMessage());
		}
	}

	/**
	 * @author tungmt
	 * @description Bao cao giam sat nhiet do
	 * @param shopId
	 *            , fromDate,toDate
	 * @since 22/04/2014
	 */
	public String exportTemperature() {
		Date fDate = null;
		Date tDate = null;
		rUtils = new ReportUtilsEx();
		result.put(ERROR, true);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Shop shop = staff.getShop();
			//------------------------------------Begin validate------------------------------------
			if (shopId == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.shop.null"));
				return JSON;
			}
			Shop exportShop = shopMgr.getShopById(shopId);
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			if (exportShop == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.shop.null"));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(fromDate)) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.fromdate.null"));
				return JSON;

			}
			if (StringUtil.isNullOrEmpty(toDate)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.todate.null"));
				return JSON;
			}
			fDate = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			tDate = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			if (fDate == null || tDate == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.date.invalid.format"));
				return JSON;
			}
			//------------------------------------End Validate------------------------------------
			//GetDetail
			ToolFilter filter = new ToolFilter();
			filter.setParentShopId(shopId);
			filter.setFromDate(fDate);
			filter.setToDate(tDate);
			List<ToolVO> lstDetail = superviserDeviceMgr.getBCTemperature(filter);
			if (lstDetail == null || lstDetail.size() <= 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			//Init XSSF workboook
			String outputName = "bc_vi_pham_nhiet_do_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			int rowNumInWindow = 1000; //Number of remain row in Window;
			SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Sheet 1");
			Map<String, XSSFCellStyle> styles = ExcelProcessUtilsEx.createStylesPOI(workbook);

			//set header & menu Row width
			rUtils.setRowsHeight(sheet, 0, 15, 15, 15, 12, 20, 15, 12, 25);

			//set static Col width
			rUtils.setColumnsWidth(sheet, 0, 150, 200, 150, 200, 250, 150, 120, 100);

			sheet.setDisplayGridlines(false);

			//------------------ Header Area ------------------
			Calendar calFDate = Calendar.getInstance();
			calFDate.setTime(fDate);
			Calendar calTDate = Calendar.getInstance();
			calTDate.setTime(tDate);
			rUtils.addCells(sheet, 0, 0, 3, 0, exportShop.getShopName().toUpperCase(), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 1, 3, 1, exportShop.getAddress() != null ? exportShop.getAddress().toUpperCase() : "", styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 2, 3, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "reprot.device.print.date") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 4, 7, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.device.temperature").toUpperCase(), styles.get(ExcelProcessUtilsEx.HEADER));
			rUtils.addCells(sheet, 0, 5, 7, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.tu.dau.ngay.den.cuoi.ngay", fromDate, toDate), styles.get(ExcelProcessUtilsEx.BOLD_CENTER));
			//------------------ End Header Area ------------------

			//------------------ Menu Area ------------------
			//reset Menu Style's all border to Thin
			ExcelProcessUtilsEx.setBorderForCell(styles.get(ExcelProcessUtilsEx.MENU), BorderStyle.THIN, ExcelProcessUtilsEx.poiBlack);
			//static columns
			String[] str = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.device.temperature.menu").split(";");
			for (int i = 0; i < str.length; i++) {
				rUtils.addCell(sheet, i, 7, str[i], styles.get(ExcelProcessUtilsEx.MENU));
			}

			//------------------ Detail Area ------------------
			int indexRow = 8;
			XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_CENTER).getFont().setBold(true);
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));

			for (int index = 0, sizeTmp1 = lstDetail.size(); index < sizeTmp1; index++) {
				XSSFCellStyle curFormatLeft = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_LEFT);
				XSSFCellStyle curFormatRight = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT);
				XSSFCellStyle curFormatCenter = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_CENTER);
				ToolVO elem = lstDetail.get(index);
				rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
				rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
				rUtils.addCell(sheet, 2, indexRow, elem.getToolCode(), curFormatLeft);
				rUtils.addCell(sheet, 3, indexRow, elem.getCustomerCode(), curFormatLeft);
				rUtils.addCell(sheet, 4, indexRow, elem.getAddress(), curFormatLeft);
				rUtils.addCell(sheet, 5, indexRow, elem.getCreateDate(), curFormatCenter);
				rUtils.addCell(sheet, 6, indexRow, elem.getMinMaxTemp(), curFormatCenter);
				rUtils.addCell(sheet, 7, indexRow, elem.getTemperature().toString(), curFormatRight);
				indexRow++;
			}
			sheet.flushRows();
			//------------------ End Detail Area ------------------

			FileOutputStream out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "SaleTransferReportAction.exportDV15 (DV1.5) - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * @author tungmt
	 * @description Bao cao giam sat nhiet do
	 * @param shopId
	 *            , fromDate,toDate
	 * @since 22/04/2014
	 */
	public String exportPosition() {
		Date fDate = null;
		Date tDate = null;
		rUtils = new ReportUtilsEx();
		result.put(ERROR, true);
		try {
			staff = getStaffByCurrentUser();
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Shop shop = staff.getShop();
			//------------------------------------Begin validate------------------------------------
			if (shopId == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.shop.null"));
				return JSON;
			}
			Shop exportShop = shopMgr.getShopById(shopId);
			if (shop == null) {
				return PAGE_NOT_PERMISSION;
			}
			if (exportShop == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.shop.null"));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(fromDate)) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.fromdate.null"));
				return JSON;

			}
			if (StringUtil.isNullOrEmpty(toDate)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.no.allow.todate.null"));
				return JSON;
			}
			fDate = DateUtil.parse(fromDate, ConstantManager.FULL_DATE_FORMAT);
			tDate = DateUtil.parse(toDate, ConstantManager.FULL_DATE_FORMAT);
			if (fDate == null || tDate == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.date.invalid.format"));
				return JSON;
			}
			//------------------------------------End Validate------------------------------------
			//GetDetail
			ToolFilter filter = new ToolFilter();
			filter.setParentShopId(shopId);
			filter.setFromDate(fDate);
			filter.setToDate(tDate);
			List<ToolVO> lstDetail = superviserDeviceMgr.getBCPosition(filter);
			if (lstDetail == null || lstDetail.size() <= 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}

			//Init XSSF workboook
			String outputName = "bc_vi_pham_vi_tri_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			int rowNumInWindow = 1000; //Number of remain row in Window;
			SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Sheet 1");
			Map<String, XSSFCellStyle> styles = ExcelProcessUtilsEx.createStylesPOI(workbook);

			//set header & menu Row width
			rUtils.setRowsHeight(sheet, 0, 15, 15, 15, 12, 20, 15, 12, 25);

			//set static Col width
			rUtils.setColumnsWidth(sheet, 0, 150, 200, 150, 200, 250, 150, 120, 100);

			sheet.setDisplayGridlines(false);

			//------------------ Header Area ------------------
			Calendar calFDate = Calendar.getInstance();
			calFDate.setTime(fDate);
			Calendar calTDate = Calendar.getInstance();
			calTDate.setTime(tDate);
			rUtils.addCells(sheet, 0, 0, 3, 0, exportShop.getShopName().toUpperCase(), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 1, 3, 1, exportShop.getAddress() != null ? exportShop.getAddress().toUpperCase() : "", styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 2, 3, 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "reprot.device.print.date") + " " + DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR), styles.get(ExcelProcessUtilsEx.BOLD));
			rUtils.addCells(sheet, 0, 4, 6, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.device.position").toUpperCase(), styles.get(ExcelProcessUtilsEx.HEADER));
			rUtils.addCells(sheet, 0, 5, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.action.tu.dau.ngay.den.cuoi.ngay", fromDate, toDate), styles.get(ExcelProcessUtilsEx.BOLD_CENTER));
			//------------------ End Header Area ------------------

			//------------------ Menu Area ------------------
			//reset Menu Style's all border to Thin
			ExcelProcessUtilsEx.setBorderForCell(styles.get(ExcelProcessUtilsEx.MENU), BorderStyle.THIN, ExcelProcessUtilsEx.poiBlack);
			//static columns
			String[] str = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.device.position.menu").split(";");
			for (int i = 0; i < str.length; i++) {
				rUtils.addCell(sheet, i, 7, str[i], styles.get(ExcelProcessUtilsEx.MENU));
			}

			//------------------ Detail Area ------------------
			int indexRow = 8;
			XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_CENTER).getFont().setBold(true);
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));
			styles.get(ExcelProcessUtilsEx.DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT).setDataFormat(fmt.getFormat("#,##0"));

			for (int index = 0, sizeTmp1 = lstDetail.size(); index < sizeTmp1; index++) {
				XSSFCellStyle curFormatLeft = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_LEFT);
				XSSFCellStyle curFormatRight = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_RIGHT);
				XSSFCellStyle curFormatCenter = styles.get(ExcelProcessUtilsEx.DETAIL_NORMAL_DOTTED_CENTER);
				ToolVO elem = lstDetail.get(index);
				rUtils.addCell(sheet, 0, indexRow, elem.getMien(), curFormatLeft);
				rUtils.addCell(sheet, 1, indexRow, elem.getShopCode(), curFormatLeft);
				rUtils.addCell(sheet, 2, indexRow, elem.getToolCode(), curFormatLeft);
				rUtils.addCell(sheet, 3, indexRow, elem.getCustomerCode(), curFormatLeft);
				rUtils.addCell(sheet, 4, indexRow, elem.getAddress(), curFormatLeft);
				rUtils.addCell(sheet, 5, indexRow, elem.getCreateDate(), curFormatCenter);
				rUtils.addCell(sheet, 6, indexRow, elem.getDistance().toString(), curFormatRight);
				indexRow++;
			}
			sheet.flushRows();
			//------------------ End Detail Area ------------------

			FileOutputStream out = new FileOutputStream(exportFileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "SaleTransferReportAction.exportDV15 (DV1.5) - " + e.getMessage());
		}
		return JSON;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public List<ToolVO> getLstShopTool() {
		return lstShopTool;
	}

	public void setLstShopTool(List<ToolVO> lstShopTool) {
		this.lstShopTool = lstShopTool;
	}

	public List<JETreeGridDeviceNode> getLstToolTreeResult() {
		return lstToolTreeResult;
	}

	public void setLstToolTreeResult(List<JETreeGridDeviceNode> lstToolTreeResult) {
		this.lstToolTreeResult = lstToolTreeResult;
	}

	public Integer getTypeSup() {
		return typeSup;
	}

	public void setTypeSup(Integer typeSup) {
		this.typeSup = typeSup;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getToolCode() {
		return toolCode;
	}

	public void setToolCode(String toolCode) {
		this.toolCode = toolCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Boolean getIsWarning() {
		return isWarning;
	}

	public void setIsWarning(Boolean isWarning) {
		this.isWarning = isWarning;
	}

	public Long getChipId() {
		return chipId;
	}

	public void setChipId(Long chipId) {
		this.chipId = chipId;
	}

	public Float getMinTemp() {
		return minTemp;
	}

	public void setMinTemp(Float minTemp) {
		this.minTemp = minTemp;
	}

	public Float getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(Float maxTemp) {
		this.maxTemp = maxTemp;
	}

	public String getChipCode() {
		return chipCode;
	}

	public void setChipCode(String chipCode) {
		this.chipCode = chipCode;
	}

	public String getTempConfig() {
		return tempConfig;
	}

	public void setTempConfig(String tempConfig) {
		this.tempConfig = tempConfig;
	}

	public String getPositionConfig() {
		return positionConfig;
	}

	public void setPositionConfig(String positionConfig) {
		this.positionConfig = positionConfig;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getDistanceConfig() {
		return distanceConfig;
	}

	public void setDistanceConfig(String distanceConfig) {
		this.distanceConfig = distanceConfig;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

}

package ths.dms.web.action.supervise;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.MathUtil;
import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.vo.AmountAndAmountPlanVO;
import ths.dms.core.entities.vo.AmountPlanCustomerVO;
import ths.dms.core.entities.vo.AmountPlanStaffDetailVO;
import ths.dms.core.entities.vo.AmountPlanStaffVO;
import ths.dms.core.entities.vo.CustomerSaleVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.NVBHSaleVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopSaleVO;
import ths.dms.core.entities.vo.StaffPositionLogVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.SupervisorSaleVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.JETreeGridNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class SupperviseSalesAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final int SHOP_TYPE = 1;
	private final int STAFF_TYPE = 2;
	
	private final int CHECKED = 1;
	
	private Long nodeId;//nodeId tren cay don vi - nhan vien
	private Integer nodeType;//1: SHOP, 2: STAFF
	
	private Long shopId;
	private String shopCode;
	private String shopName;
	private Long shopIdSearch;
	private String staffCode;
	private Long staffId;
	private Integer type;
	private Staff staff;
	private Long customerId;
	/*
	 * @PhuT variable
	 */
	private Long gdmId;
	private Long tbhvId;
	private Long nvgsId;
	private Long nvbhId;

	/** So ngay ban hang ke hoach. */
	private Integer soKeHoach;

	/** So ngay ban hang da qua. */
	private Integer soBHDaQua;

	/** Tien do chuan. */
	private Integer tienDoChuan;
	private List<AmountPlanStaffDetailVO> amountPlanStaffDetailVOs;
	private BigDecimal dayAmountPlan;
	private BigDecimal dayAmountApproved;
	private BigDecimal dayAmount;
	private BigDecimal monthAmountPlan;
	private BigDecimal monthAmountApproved;
	private BigDecimal monthAmount;
	private BigDecimal monthApprovedAmount;//Doanh so duyet trong thang
	private Integer dayQuantityPlan;
	private Integer dayQuantity;
	private Integer dayQuantityApproved;
	private Integer monthQuantityPlan;
	private Integer monthQuantity;
	private Integer monthQuantityApproved;
	private Integer progress;
	private Integer numPointLine;
	private BigDecimal exist;
	private Integer lhl;

	private List<JETreeGridNode> lstStaffTreeResult;

	private StaffMgr staffMgr;
	private ShopMgr shopMgr;
	private SuperviserMgr superviserMgr;
	private CommonMgr commonMgr;
	
	private Integer checked;
	private String checkDate;
	private String fromTime;
	private String toTime;
	private String sysdateMMddYyyyStr;
	
	
//	private List<StaffType>
	private CustomerMgr customerMgr;
	//private ExceptionDayMgr exceptionDayMgr;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		superviserMgr = (SuperviserMgr) context.getBean("superviserMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		//exceptionDayMgr = (ExceptionDayMgr) context.getBean("exceptionDayMgr");
		if (currentUser != null) {
			shopId = currentUser.getShopRoot().getShopId();
		}
	}

	@Override
	public String execute() throws Exception {
		resetToken(result);
		if (currentUser != null) {
			Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
			if (staff != null && staff.getStaffType() != null && staff.getStaffType().getSpecificType() != null) {
				type = staff.getStaffType().getSpecificType().getValue();
			}
			if (currentUser.getShopRoot() != null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
			ApParam ap = apParamMgr.getApParamByCodeX(ConstantManager.SYS_SALE_ROUTE, ApParamType.SYS_CONFIG, ActiveType.RUNNING);
			sysSaleRoute = ConstantManager.ONE_TEXT;
			if (ap != null && !StringUtil.isNullOrEmpty(ap.getValue())) {
				if (ConstantManager.TWO_TEXT.equals(ap.getValue().trim())) {
					sysSaleRoute = ConstantManager.TWO_TEXT;
				}
			}
			sysdateMMddYyyyStr = DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATA_FORMAT_MM_DD_YYYY);
		}
		return SUCCESS;
	}

	public String getTBHVInfo() {
		try {
			Staff staff = staffMgr.getStaffById(tbhvId);
			if (staff == null) {
				return SUCCESS;
			}
			AmountAndAmountPlanVO vo = superviserMgr.getAmountAndAmountPlanByStaff(staff);
			if (vo != null) {
				dayAmountPlan = vo.getDayAmountPlan();
				dayAmountApproved = vo.getDayAmountApproved();
				dayAmount = vo.getDayAmount();
				monthAmountPlan = vo.getMonthAmountPlan();
				monthAmountApproved = vo.getMonthAmountApproved();
				monthAmount = vo.getMonthAmount();
				tienDoChuan = vo.getTienDoChuan();
			}
			result.put("dayAmountPlan", dayAmountPlan);
			result.put("dayAmount", dayAmount);
			result.put("dayAmountApproved", dayAmountApproved);
			result.put("monthAmountPlan", monthAmountPlan);
			result.put("monthAmount", monthAmount);
			result.put("monthAmountApproved", monthAmountApproved);
			result.put("tienDoChuan", tienDoChuan);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getDSNgayTBHV");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * Lay thong tin doanh so, san luong cua NVGS quan ly tat ca NVBH (staff), marker
	 * @author vuongmq
	 * @return SUCCESS
	 * @since 19/08/2015
	 */
	public String getNVGSInfo() {
		try {
			Staff staff = staffMgr.getStaffById(nvgsId);
			if (staff == null) {
				result.put("errMsg", R.getResource("common.not.login"));
				result.put(ERROR, true);
				return SUCCESS;
			}
			if (staff != null && staff.getShop() == null) {
				result.put("errMsg", R.getResource("common.no.data.shop.error"));
				result.put(ERROR, true);
				return SUCCESS;
			}
			/** staff(NVGS): lay cac loai filter.setObjetType() no truyen vao; 1: NVBH */
			RptStaffSaleFilter filter = new RptStaffSaleFilter();
			filter.setObjetType(StaffSpecificType.STAFF.getValue());
			ApParam ap = apParamMgr.getApParamByCodeX(ConstantManager.SYS_SALE_ROUTE, ApParamType.SYS_CONFIG, ActiveType.RUNNING);
			String type = ConstantManager.ONE_TEXT;
			if (ap != null && !StringUtil.isNullOrEmpty(ap.getValue())) {
				if (ConstantManager.TWO_TEXT.equals(ap.getValue().trim())) {
					type = ConstantManager.TWO_TEXT;
				}
			}
			filter.setType(type);
			
			AmountAndAmountPlanVO vo = superviserMgr.getAmountAndAmountPlanByStaffSupervise(staff, filter);
			if (vo != null) {
				dayAmountPlan = vo.getDayAmountPlan();
				dayAmountApproved = vo.getDayAmountApproved();
				dayAmount = vo.getDayAmount();
				monthAmountPlan = vo.getMonthAmountPlan();
				monthAmountApproved = vo.getMonthAmountApproved();
				monthAmount = vo.getMonthAmount();
				tienDoChuan = vo.getTienDoChuan();
				dayQuantityPlan = vo.getDayQuantityPlan();
				dayQuantityApproved = vo.getDayQuantityApproved();
				dayQuantity = vo.getDayQuantity();
				monthQuantityPlan = vo.getMonthQuantityPlan();
				monthQuantityApproved = vo.getMonthQuantityApproved();
				monthQuantity = vo.getMonthQuantity();
			}
			result.put("dayAmountPlan", dayAmountPlan);
			result.put("dayAmount", dayAmount);
			result.put("dayAmountApproved", dayAmountApproved);
			result.put("monthAmountPlan", monthAmountPlan);
			result.put("monthAmount", monthAmount);
			result.put("monthAmountApproved", monthAmountApproved);
			result.put("tienDoChuan", tienDoChuan);
			// vuongmq; 18/08/2015; lay them so luong doanh so
			result.put("dayQuantityPlan", dayQuantityPlan);
			result.put("dayQuantityApproved", dayQuantityApproved);
			result.put("dayQuantity", dayQuantity);
			result.put("monthQuantityPlan", monthQuantityPlan);
			result.put("monthQuantityApproved", monthQuantityApproved);
			result.put("monthQuantity", monthQuantity);
			result.put("phoneStaff", !StringUtil.isNullOrEmpty(this.viewStaffPhone(staff)) ? this.viewStaffPhone(staff) : "");
			//Lay thong tin pin/wifi
			StaffPositionLog stPs = superviserMgr.getNewestStaffPositionLog(staff.getId(), staff.getShop().getId());
			if (stPs != null) {
				result.put("pin", stPs.getPinRound());
				if (!StringUtil.isNullOrEmpty(stPs.getNetworkType())) {
					result.put("networkType", stPs.getNetworkType().toUpperCase());
				}
				result.put("networkSpeed", stPs.getNetworkSpeed());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getNVGSInfo() " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/***
	 * Lay thong tin doanh so, san luong cua staff, marker
	 * @author vuongmq
	 * @return SUCCESS
	 * @since 18/08/2015
	 */
	public String getNVBHInfo() {
		try {
			Staff staff = staffMgr.getStaffById(nvbhId);
			if (staff == null) {
				result.put("errMsg", R.getResource("common.not.login"));
				result.put(ERROR, true);
				return SUCCESS;
			}
			if (staff.getShop() == null) {
				result.put("errMsg", R.getResource("common.no.data.shop.error"));
				result.put(ERROR, true);
				return SUCCESS;
			}
			AmountAndAmountPlanVO vo = superviserMgr.getAmountAndAmountPlanByStaff(staff);
			if (vo != null) {
				dayAmountPlan = vo.getDayAmountPlan();
				dayAmountApproved = vo.getDayAmountApproved();
				dayAmount = vo.getDayAmount();
				monthAmountPlan = vo.getMonthAmountPlan();
				monthAmountApproved = vo.getMonthAmountApproved();
				monthAmount = vo.getMonthAmount();
				tienDoChuan = vo.getTienDoChuan();
				dayQuantityPlan = vo.getDayQuantityPlan();
				dayQuantityApproved = vo.getDayQuantityApproved();
				dayQuantity = vo.getDayQuantity();
				monthQuantityPlan = vo.getMonthQuantityPlan();
				monthQuantityApproved = vo.getMonthQuantityApproved();
				monthQuantity = vo.getMonthQuantity();
			}
			CustomerVO custVisit = superviserMgr.getCustomerVisitting(nvbhId);
			result.put("dayAmountPlan", dayAmountPlan);
			result.put("dayAmountApproved", dayAmountApproved);
			result.put("dayAmount", dayAmount);
			result.put("monthAmountPlan", monthAmountPlan);
			result.put("monthAmountApproved", monthAmountApproved);
			result.put("monthAmount", monthAmount);
			// vuongmq; 18/08/2015; lay them so luong doanh so
			result.put("dayQuantityPlan", dayQuantityPlan);
			result.put("dayQuantityApproved", dayQuantityApproved);
			result.put("dayQuantity", dayQuantity);
			result.put("monthQuantityPlan", monthQuantityPlan);
			result.put("monthQuantityApproved", monthQuantityApproved);
			result.put("monthQuantity", monthQuantity);
			result.put("tienDoChuan", tienDoChuan);
			result.put("custVisit", custVisit != null ? custVisit.getShortCode() + " - " + custVisit.getCustomerName() : "");
			result.put("custVisitAddr", custVisit != null && !StringUtil.isNullOrEmpty(custVisit.getAddress()) ? custVisit.getAddress() : "");
			result.put("phoneStaff", !StringUtil.isNullOrEmpty(this.viewStaffPhone(staff)) ? this.viewStaffPhone(staff) : "");
			//Lay thong tin pin/wifi
			StaffPositionLog stPs = superviserMgr.getNewestStaffPositionLog(staff.getId(), staff.getShop().getId());
			if (stPs != null) {
				result.put("pin", stPs.getPinRound());
				if (!StringUtil.isNullOrEmpty(stPs.getNetworkType())) {
					result.put("networkType", stPs.getNetworkType().toUpperCase());
				}
				result.put("networkSpeed", stPs.getNetworkSpeed());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getNVBHInfo() " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * Lay thong tin phone cua Staff
	 * @author vuongmq
	 * @return String
	 * @since 06/11/2015
	 */
	private String viewStaffPhone(Staff staff) {
		String phoneStr = "";
		if (staff != null) {
			if (!StringUtil.isNullOrEmpty(staff.getPhone()) && !StringUtil.isNullOrEmpty(staff.getMobilephone())) {
				phoneStr = staff.getPhone() + " - " + staff.getMobilephone();
			} else if (!StringUtil.isNullOrEmpty(staff.getPhone())) {
				phoneStr = staff.getPhone();
			} else {
				phoneStr = staff.getMobilephone();
			}
		}
		return phoneStr;
	}
	
	/**
	 * Lay thong tin doanh so, san luong cua staff ung voi KH
	 * @author vuongmq
	 * @return JSON
	 * @since 18/08/2015
	 */
	public String getRptStaffSaleAndCustomer() {
		try {
			if (nvbhId == null) {
				result.put("errMsg", R.getResource("common.not.login"));
				result.put(ERROR, true);
				return JSON;
			}
			if (customerId == null) {
				result.put("errMsg", R.getResource("common.catalog.code.not.exist", R.getResource("qltt_doi_tuong_khachhang")));
				result.put(ERROR, true);
				return JSON;
			}
			RptStaffSaleFilter filter = new RptStaffSaleFilter();
			filter.setStaffId(nvbhId);
			filter.setCustomerId(customerId);
			filter.setObjetType(StaffObjectType.NV.getValue());
			AmountPlanCustomerVO amountPlanCustomerVO = superviserMgr.getAmountPlanCustomerByStaff(filter);
			result.put("customer", amountPlanCustomerVO);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getRptStaffSaleAndCustomer() " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Lay thong tin doanh so, san luong cua staff ung voi KH
	 * @author vuongmq
	 * @return JSON
	 * @since 18/07/2015
	 */
	public String getListShopOfSupervise() {
		try {
			if (nvgsId == null) {
				result.put("errMsg", R.getResource("common.not.login"));
				result.put(ERROR, true);
				return JSON;
			}
			RptStaffSaleFilter filter = new RptStaffSaleFilter();
			filter.setStaffId(nvgsId);
			List<Shop> lstShops = shopMgr.getListShopOfSupervise(filter);
			result.put("rows", lstShops);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getListShopOfSupervise() " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	public String getDSNgayGDM() {
		try {
			if (gdmId != null && gdmId > 0) {
				ObjectVO<NVBHSaleVO> vo = superviserMgr.getListTBHMSaleVO(null, gdmId, shopId);
				result.put(ERROR, false);
				result.put("lstGDMSaleInfo", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getDSNgayGDM" + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	public String getDSNgayTBHV() {
		try {
			if (tbhvId != null && tbhvId > 0) {
				ObjectVO<SupervisorSaleVO> vo = superviserMgr.getListSupervisorSaleVO(null, tbhvId, shopId);
				if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
					for (int i = 0; i < vo.getLstObject().size(); i++) {
						if (vo.getLstObject().get(i).getDayAmountPlan() == null) {
							vo.getLstObject().get(i).setDayAmountPlan(BigDecimal.ZERO);
						} else {
							vo.getLstObject().get(i).setDayAmountPlan(vo.getLstObject().get(i).getDayAmountPlan().divide(BigDecimal.valueOf(1000)).setScale(0, RoundingMode.HALF_DOWN));
						}
					}
				}
				result.put(ERROR, false);
				result.put("lstNVGSSaleInfo", vo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getDSNgayTBHV");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * Lay thong tin doanh so, san luong Ngay, Luy ke NVGS ung voi NVBH
	 * @author vuongmq
	 * @return SUCCESS
	 * @since 21/08/2015
	 */
	public String getDSNgayNVGS() {
		try {
			if (nvgsId != null && nvgsId > 0) {
				Staff staffGS = staffMgr.getStaffById(nvgsId);
				if (staffGS == null) {
					result.put("errMsg", R.getResource("common.catalog.code.not.exist", R.getResource("common.label.staff")));
					result.put(ERROR, true);
					return SUCCESS;
				}
				if (super.getMapUserId().get(nvgsId) == null) {
					result.put("errMsg", R.getResource("catalog.staff.code.permission1"));
					result.put(ERROR, true);
					return SUCCESS;
				}
				RptStaffSaleFilter filter = new RptStaffSaleFilter();
				filter.setStaffId(nvgsId); // ma NVGS
				if (staffGS != null && staffGS.getStaffType() != null && staffGS.getStaffType().getSpecificType() != null) {
					filter.setSpecificType(staffGS.getStaffType().getSpecificType().getValue()); // Specific type cua NVGS
				}
				filter.setShopId(shopIdSearch);
				filter.setObjetType(StaffSpecificType.STAFF.getValue()); // nhung staff: 1 cua NVBH cua NVGS QL
				
				//trungtm6 
				ApParam ap = apParamMgr.getApParamByCodeX(ConstantManager.SYS_SALE_ROUTE, ApParamType.SYS_CONFIG, ActiveType.RUNNING);
				String type = ConstantManager.ONE_TEXT;
				if (ap != null && !StringUtil.isNullOrEmpty(ap.getValue())) {
					if (ConstantManager.TWO_TEXT.equals(ap.getValue().trim())) {
						type = ConstantManager.TWO_TEXT;
					}
				}
				filter.setType(type);
				
				ObjectVO<NVBHSaleVO> vo = superviserMgr.getListNVBHSaleVO(null, filter);
				if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
					result.put("lstNVBHSaleInfo", vo.getLstObject());
				} else {
					result.put("lstNVBHSaleInfo", new ArrayList<NVBHSaleVO>());
				}
				result.put(ERROR, false);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getDSNgayNVGS() " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * Lay thong tin doanh so, san luong Ngay, Luy ke staff ung voi KH
	 * @author vuongmq
	 * @return SUCCESS
	 * @since 20/08/2015
	 */
	public String getDSNgayNVBH() {
		try {
			if (nvbhId != null && nvbhId > 0) {
				Staff staffBH = staffMgr.getStaffById(nvbhId);
				if (staffBH == null) {
					result.put("errMsg", R.getResource("common.catalog.code.not.exist", R.getResource("common.label.staff")));
					result.put(ERROR, true);
					return SUCCESS;
				}
				if (super.getMapUserId().get(nvbhId) == null) {
					result.put("errMsg", R.getResource("catalog.staff.code.permission1"));
					result.put(ERROR, true);
					return SUCCESS;
				}
				ObjectVO<CustomerSaleVO> vo = superviserMgr.getListCustomerSaleVO(null, nvbhId);
				if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
					result.put("lstCustomerSaleInfo", vo.getLstObject());
				} else {
					result.put("lstCustomerSaleInfo", new ArrayList<CustomerSaleVO>());
				}
				result.put(ERROR, false);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getDSNgayNVBH() " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	/**
	 * Lay thong tin doanh so, san luong Ngay, Luy ke cua shop
	 * @author vuongmq
	 * @return SUCCESS
	 * @since 21/09/2015
	 */
	public String getDSNgayShop() {
		Date startLogDate = DateUtil.now();
		try {
			result.put("lstShopSaleInfo", new ArrayList<ShopSaleVO>());
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				if (shopIdSearch != null && shopIdSearch > 0) {
					Shop shop = shopMgr.getShopById(shopIdSearch);
					if (shop == null) {
						result.put("errMsg", R.getResource("common.catalog.code.not.exist", R.getResource("catalog.unit.tree")));
						result.put(ERROR, true);
						return SUCCESS;
					}
					if (super.getMapShopChild().get(shopIdSearch) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
					Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
					Long roleId = currentUser.getRoleToken().getRoleId();
					RptStaffSaleFilter filter = new RptStaffSaleFilter();
					filter.setShopId(shopIdSearch);
					filter.setStaffRootId(staffIdRoot);
					filter.setRoleId(roleId);
					ObjectVO<ShopSaleVO> vo = superviserMgr.getListShopSaleVO(null, filter);
					if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
						result.put("lstShopSaleInfo", vo.getLstObject());
					}
					result.put(ERROR, false);
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.SupperviseSalesAction.getDSNgayShop()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}
	
	public JETreeGridNode converterTreeVOToTreeNode(TreeVO<StaffPositionVO> treevo, List<StaffPositionVO> lstStaffTraining) {
		StaffPositionVO staff = treevo.getObject();
		List<JETreeGridNode> lstStaffTree = new ArrayList<JETreeGridNode>();
		if (lhl != null && lhl != 0 && staff.getRoleType() != null && staff.getRoleType() < 3) {
			boolean flag = false;
			for (int k = 0; k < lstStaffTraining.size(); k++) {
				if (lstStaffTraining.get(k).getStaffId().equals(staff.getStaffId())) {
					flag = true;
					break;
				}
				if (lstStaffTraining.get(k).getStaffOwnerId().equals(staff.getStaffId())) {
					flag = true;
					break;
				}
			}
			if (!flag)
				return null;//ko co lich huan luyen
		}
		staff.setStaffName(getRoleName(staff.getRoleType()) + staff.getStaffName());
		if (staff.getCreateTime() != null) {
			staff.setHhmm(new SimpleDateFormat("HH:mm").format(staff.getCreateTime()));
		}
		JETreeGridNode node = new JETreeGridNode();
		node.setText(staff.getStaffName());
//		node.setId(String.valueOf(STAFF_TYPE) + "-" + String.valueOf(staff.getStaffId()));
		node.setId(staff.getStaffId());
		node.setAttr(staff);
		if (staff.getCountStaff() != null && staff.getCountStaff() > 0 && staff.getRoleType() != 1) {
			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
		} else if (staff.getCountStaff() != null) {
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
		} else {
			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
		}
		if (treevo != null && treevo.getListChildren() != null && treevo.getListChildren().size() > 0) {
			for (TreeVO<StaffPositionVO> temp : treevo.getListChildren()) {
				JETreeGridNode nodeTemp = converterTreeVOToTreeNode(temp, lstStaffTraining);
				if (nodeTemp != null)
					lstStaffTree.add(nodeTemp);
			}
			node.setChildren(lstStaffTree);
			if (lstStaffTree != null && lstStaffTree.size() == 0) {
				node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			} else
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
		}
		return node;
	}

	public String getRoleName(Integer roleType) {
		if (StaffObjectType.NHVNM.getValue().equals(roleType)) {
			return "VNM - ";
		} else if (StaffObjectType.TBHM.getValue().equals(roleType)) {
			return "GĐM - ";
		} else if (StaffObjectType.TBHV.getValue().equals(roleType)) {
			return "TBHV - ";
		} else if (StaffObjectType.NVGS.getValue().equals(roleType)) {
			return "GSNPP - ";
		} else {
			return "";
		}
	}

	/**
	 * @author trungtm6
	 * @since 18/08/2015
	 * @param obj
	 * @param type
	 * @return
	 */
	private JETreeGridNode addInfoForNode(Object obj, int type, Integer check, String iconUrl) {
		JETreeGridNode node = new JETreeGridNode();
		if (type == SHOP_TYPE) {
			Shop sh = (Shop)obj;
			if (sh != null) {
//				node.setId(String.valueOf(SHOP_TYPE) + "-" + String.valueOf(sh.getId()));
				node.setId(sh.getId());
				node.setText(sh.getShopCode() + " - " + sh.getShopName());
				node.setNodeType(SHOP_TYPE);
				node.setState(ConstantManager.JSTREE_STATE_CLOSE);
				if (sh.getStatus() != null) {
					node.setStatus(sh.getStatus().getValue());
				}
				
				StaffPositionVO nodeAttr = new StaffPositionVO();
				nodeAttr.setText(sh.getShopName());
				nodeAttr.setShopId(sh.getId());
				nodeAttr.setShopCode(sh.getShopCode());
				nodeAttr.setShopName(sh.getShopName());
				if (sh.getType() != null && sh.getType().getSpecificType() != null) {
					nodeAttr.setRoleType(sh.getType().getSpecificType().getValue());
				}
				node.setAttr(nodeAttr);
				if(sh.getParentShop() != null) {
					node.setParentID(sh.getParentShop().getId());
				}
			}
		} else if (type == STAFF_TYPE) {
			StaffPositionVO st = (StaffPositionVO)obj;
//			node.setId(String.valueOf(STAFF_TYPE) + "-" + String.valueOf(st.getStaffId()));
			node.setId(st.getStaffId());
			node.setText(st.getStaffCode() + " - " + st.getStaffName());
			node.setNodeType(STAFF_TYPE);
			node.setState(ConstantManager.JSTREE_STATE_LEAF);
			node.setStatus(st.getStatus());
			node.setParentID(st.getShopId());
			node.setAttr(st);
			/*StaffPositionVO nodeAttr = new StaffPositionVO();
			nodeAttr.setText(st.getStaffCode() + " - " + st.getStaffName());
			node.setAttr(nodeAttr);*/
		}
		node.setChecked(check);
		node.setIconCls(Configuration.getImgServerPath() + iconUrl);
		return node;
	}
	
	
	/**
	 * @author trungtm6
	 * @since 18/08/2015
	 * @param lstObj
	 * @param type
	 * @return
	 */
	/*private List<JETreeGridNode> addInfoForListNode(List<Object> lstObj, int type) {
		List<JETreeGridNode> lstNode = new ArrayList<JETreeGridNode>();
		for (Object obj: lstObj) {
			JETreeGridNode node = addInfoForNode(obj, type);
			lstNode.add(node);
		}
		return lstNode;
	}*/
	
	//start comment
	/*if (currentShop != null) {
		List<StaffPositionVO> lstStaff = null;
		List<StaffPositionVO> lstStaffTraining = null;
//		if (staffCurrent.getStaffType() != null && !StaffObjectType.KTNPP.getValue().equals(staffCurrent.getStaffType().getObjectType()) && (!StringUtil.isNullOrEmpty(shopCode) || !StringUtil.isNullOrEmpty(shopName))) {
		if (staffCurrent.getStaffType() != null && !StaffObjectType.KTNPP.getValue().equals(staffCurrent.getStaffType().getSpecificType()) 
				&& (!StringUtil.isNullOrEmpty(shopCode) || !StringUtil.isNullOrEmpty(shopName))) {
			if (staffCurrent != null && staffCurrent.getShop() != null) {
				loadUser = staffCurrent;
			}
			filter.setLoginStaff(staffCurrent);
			filter.setShopCode(shopCode);
			filter.setShopName(shopName);
			filter.setDateTime(commonMgr.getSysDate());
			TreeVO<StaffPositionVO> treevo = staffMgr.searchStaff(filter);
			if (lhl != null && lhl == 1) {
				lstStaffTraining = staffMgr.getListStaffPositionWithTraining(shopId);
			}
			if (treevo != null) {
				for (TreeVO<StaffPositionVO> temp : treevo.getListChildren()) {
					JETreeGridNode node = converterTreeVOToTreeNode(temp, lstStaffTraining);
					if (node != null) {
						lstStaffTree.add(node);
					}
				}
			}
		} else {
			if (staffId == null || staffId == 0) {
				staffCurrent = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staffCurrent != null && staffCurrent.getShop() != null) {
					loadUser = staffCurrent;
					filter.setRoleType(staffCurrent.getStaffType() != null ? staffCurrent.getStaffType().getObjectType() : null);
					filter.setOneNode(true);
					filter.setStaffId(loadUser.getId());
					lstStaff = staffMgr.getListStaffPosition(filter);
					if (lstStaff != null) {
						for (StaffPositionVO vo : lstStaff) {
							vo.setStaffOwnerId(loadUser.getId());
						}
					}
				}
			} else {
				Staff staff = staffMgr.getStaffById(staffId);
				if (staff != null) {
					filter.setOneNode(true);
					filter.setStaffId(staffId);
					lstStaff = staffMgr.getListStaffPosition(filter);
				}
				if (lstStaff != null) {
					for (StaffPositionVO vo : lstStaff) {
						vo.setStaffOwnerId(staffId);
					}
				}
			}
			if (lhl != null && lhl == 1) {
				lstStaffTraining = staffMgr.getListStaffPositionWithTraining(shopId);
			}
			if (lstStaff != null && lstStaff.size() > 0) {
				for (StaffPositionVO temp : lstStaff) {
					temp.setStaffName(getRoleName(temp.getRoleType()) + temp.getStaffName());
					if (temp.getCreateTime() != null) {
						temp.setHhmm(new SimpleDateFormat("HH:mm").format(temp.getCreateTime()));
					}
				}
				List<Long> lstException = new ArrayList<Long>();
				for (int i = 0; i < lstStaff.size(); i++) {
					JETreeGridNode node = new JETreeGridNode();
					node.setText(lstStaff.get(i).getStaffName());
					node.setId(lstStaff.get(i).getStaffId());
					node.setAttr(lstStaff.get(i));
					if (lstStaff.get(i).getRoleType() == 1 || lstStaff.get(i).getRoleType() == 2) {
						node.setState(ConstantManager.JSTREE_STATE_LEAF);
					} else if (lstStaff.get(i).getCountStaff() != null && lstStaff.get(i).getCountStaff() > 0) {
						node.setState(ConstantManager.JSTREE_STATE_CLOSE);
					} else if (lstStaff.get(i).getCountStaff() != null) {
						node.setState(ConstantManager.JSTREE_STATE_LEAF);
					} else {
						node.setState(ConstantManager.JSTREE_STATE_CLOSE);
					}
					if (lhl == null || lhl == 0 || lstStaff.get(i).getRoleType() == null || (lstStaff.get(i).getRoleType() != null && !StaffObjectType.NVBH.getValue().equals(lstStaff.get(i).getRoleType()))
							&& !StaffObjectType.NVVS.getValue().equals(lstStaff.get(i).getRoleType())) {
						lstStaffTree.add(node);
					} else {// filter lay ra danh sach co nv co lich huan luyen
						for (int k = 0; k < lstStaffTraining.size(); k++) {
							if (lstStaffTraining.get(k).getStaffId().equals(lstStaff.get(i).getStaffId())) {
								lstStaffTree.add(node);
							}
							if (lstStaffTraining.get(k).getStaffOwnerId().equals(lstStaff.get(i).getStaffId())) {
								Boolean flag = true;
								for (int j = 0; j < lstException.size(); j++) {//neu chua add vao tree thi add
									if (lstStaffTraining.get(k).getStaffOwnerId().equals(lstException.get(j))) {
										flag = false;
										break;
									}
								}
								if (flag) {
									lstStaffTree.add(node);
									lstException.add(lstStaffTraining.get(k).getStaffOwnerId());
								}
							}
						}
					}
				}
			}
		}
	}
	if (loadUser != null) {
		if (StaffObjectType.KTNPP.getValue().equals(loadUser.getStaffType().getObjectType())) {
			lstStaffTreeResult = lstStaffTree;
		} else {
			loadUser.setStaffName(getRoleName(loadUser.getStaffType().getObjectType()) + loadUser.getStaffName());
			JETreeGridNode node = new JETreeGridNode();
			if (StaffObjectType.NHVNM.getValue().equals(staffCurrent.getStaffType().getObjectType())) {
				node.setText(staffCurrent.getStaffName());
				StaffPositionVO cNodeAttr = new StaffPositionVO();
				cNodeAttr.setStaffId(staffCurrent.getId());
				cNodeAttr.setStaffName(staffCurrent.getStaffName());
				cNodeAttr.setRoleType(StaffObjectType.NHVNM.getValue());
				cNodeAttr.setShopCode(staffCurrent.getShop().getShopCode());
				cNodeAttr.setStaffOwnerId(-1L);
				node.setAttr(cNodeAttr);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);

				node.setChildren(lstStaffTree);
				node.setId(staffCurrent.getId());
			} else {
				node.setText(loadUser.getStaffName());
				StaffPositionVO cNodeAttr = new StaffPositionVO();
				cNodeAttr.setStaffId(loadUser.getId());
				cNodeAttr.setStaffName(loadUser.getStaffName());
				cNodeAttr.setRoleType(loadUser.getStaffType().getObjectType());
				cNodeAttr.setShopCode(loadUser.getShop().getShopCode());
				cNodeAttr.setStaffOwnerId(-1L);
				node.setAttr(cNodeAttr);
				if (lstStaffTree != null && lstStaffTree.size() > 0) {
					node.setState(ConstantManager.JSTREE_STATE_OPEN);
				} else {
					node.setState(ConstantManager.JSTREE_STATE_LEAF);
				}
				node.setChildren(lstStaffTree);
				node.setId(loadUser.getId());
			}

			lstStaffTreeCurrent.add(node);
			lstStaffTreeResult = lstStaffTreeCurrent;
		}
		return JSON;
	}*/
	//end comment
	
	/**
	 * Load Cay don vi - nhan vien 
	 * @author trungtm6
	 * @since
	 * @return
	 * 
	 * @modify hunglm16
	 * @since 24/11/2015
	 * @description ra soat phan quyen
	 */
	public String getListStaffPositionOneNode() {
		try {
			List<JETreeGridNode> lstStaffTree = new ArrayList<JETreeGridNode>();
			Staff staffCurrent = null;
			Shop currentShop = null;
			if (currentUser != null && currentUser.getShopRoot() != null) {
				currentShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			staffCurrent = staffMgr.getStaffByCode(currentUser.getUserName());
			if (staffCurrent == null) {
				return JSON;
			}
			//			Staff loadUser = null;
			SupFilter filter = new SupFilter();
			List<ApParam> lstAp = apParamMgr.getListApParamByType(ApParamType.STAFF_TYPE_SHOW_SUP, ActiveType.RUNNING);
			String strStaffTypeId = "-1";
			//only add staff_type inserted in ap_param
			if (lstAp != null && lstAp.size() > 0) {
				for (ApParam ap : lstAp) {
					strStaffTypeId += ", ";
					strStaffTypeId += ap.getValue();
				}
			}
			if (currentShop != null) {
				if (nodeId == null) {// first load
					//Load shop root
					checked = CHECKED;//node root
					JETreeGridNode root = addInfoForNode(currentShop, SHOP_TYPE, checked, currentShop.getType().getIconUrl());
					root.setRoot(true);
					List<JETreeGridNode> childNodes = new ArrayList<JETreeGridNode>();
					filter.setShopId(currentShop.getId());//load list staff for shop
					filter.setStrStaffTypeId(strStaffTypeId);
					filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
					filter.setRoleId(currentUser.getRoleToken().getRoleId());
					filter.setShopRootId(currentUser.getShopRoot().getShopId());
					filter.setStrStaffId(getStrListUserId());
					List<StaffPositionVO> childStaff = staffMgr.getListStaffPosition(filter);
					if (childStaff != null && childStaff.size() > 0) {
						for (StaffPositionVO st : childStaff) {
							childNodes.add(addInfoForNode(st, STAFF_TYPE, checked, st.getIconUrl()));
						}
					}

					ShopFilter childF = new ShopFilter();
					childF.setShopId(currentShop.getId());
					childF.setStatus(ActiveType.RUNNING);
					childF.setStrShopId(getStrListShopId());
					childF.setStaffRootId(currentUser.getStaffRoot().getStaffId());
					childF.setRoleId(currentUser.getRoleToken().getRoleId());
					childF.setShopRootId(currentUser.getShopRoot().getShopId());
					List<Shop> lstChild = shopMgr.getListNextChildShop(childF);
					if (lstChild != null && lstChild.size() > 0) {
						for (Shop sh : lstChild) {
							JETreeGridNode childNode = addInfoForNode(sh, SHOP_TYPE, checked, sh.getType().getIconUrl());
							setAllChildStaffForNode(sh, childNode);
							childNodes.add(childNode);
						}
					}
					root.setChildren(childNodes);
					root.setState(ConstantManager.JSTREE_STATE_OPEN); //open root node
					setAllChildStaffForNode(currentShop, root);
					lstStaffTree.add(root);
				} else {//expand child node
					Shop aNode = shopMgr.getShopById(nodeId);
					if (aNode != null) {
						if (!getMapShopChild().containsKey(aNode.getId())) {
							aNode = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
						}
					}
					if (aNode != null) {
						List<JETreeGridNode> childNodes = new ArrayList<JETreeGridNode>();
						filter.setShopId(aNode.getId());//load list staff for shop
						filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
						filter.setRoleId(currentUser.getRoleToken().getRoleId());
						filter.setShopRootId(currentUser.getShopRoot().getShopId());
						filter.setStrStaffTypeId(strStaffTypeId);
						List<StaffPositionVO> childStaff = staffMgr.getListStaffPosition(filter);
						if (childStaff != null && childStaff.size() > 0) {
							for (StaffPositionVO st : childStaff) {
								childNodes.add(addInfoForNode(st, STAFF_TYPE, checked, st.getIconUrl()));
							}
						}
						ShopFilter childF = new ShopFilter();
						childF.setShopId(aNode.getId());
						childF.setStatus(ActiveType.RUNNING);
						childF.setStrShopId(getStrListShopId());
						childF.setStaffRootId(currentUser.getStaffRoot().getStaffId());
						childF.setRoleId(currentUser.getRoleToken().getRoleId());
						childF.setShopRootId(currentUser.getShopRoot().getShopId());
						List<Shop> lstChild = shopMgr.getListNextChildShop(childF);
						if (lstChild != null && lstChild.size() > 0) {
							for (Shop sh : lstChild) {
								JETreeGridNode childNode = addInfoForNode(sh, SHOP_TYPE, checked, sh.getType().getIconUrl());

								//Load all child staff of shop
								setAllChildStaffForNode(sh, childNode);

								childNodes.add(childNode);
							}
						}
						lstStaffTree = childNodes;
					}
				}
			}
			lstStaffTreeResult = lstStaffTree;
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	private void setAllChildStaffForNode(Shop shop, JETreeGridNode node) throws BusinessException {
		//Load all child staff of shop
		StaffFilter stFilter = new StaffFilter();
		stFilter.setShopId(shop.getId());
		stFilter.setStrListUserId(getStrListUserId());
		stFilter.setAllStaffChildShop(true);
		stFilter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
		stFilter.setRoleId(currentUser.getRoleToken().getRoleId());
		stFilter.setShopRootId(currentUser.getShopRoot().getShopId());
		ObjectVO<StaffVO> stVo = staffMgr.getListStaffVO(stFilter);
		
		if (stVo != null && stVo.getLstObject() != null) {
			node.setLstAllStaff(stVo.getLstObject());
		} else {
			node.setLstAllStaff(new ArrayList<StaffVO>());
		}
	}

	/*public String getListStaffPositionForShop() {
		if (currentUser != null) {
			staffCode = currentUser.getUserName();
			SupFilter filter = new SupFilter();
			filter.setShopId(shopId);
			try {
				Staff staff = staffMgr.getStaffByCode(staffCode);
				if (staff != null) {
					List<StaffPositionVO> lstStaff = null;
					List<StaffPositionVO> lstStaffTraining = null;
					lstStaffTraining = staffMgr.getListStaffPositionWithTraining(shopId);
					filter.setRoleType(staff.getStaffType() != null ? staff.getStaffType().getObjectType() : null);
					if (StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType())) {
						filter.setStaffId(staff.getId());
						lstStaff = staffMgr.getListStaffPosition(filter);
						StaffPositionVO gs = staffMgr.getStaffPosition(staff.getId());
						if (gs != null) {
							lstStaff.add(gs);
						}
					} else {
						filter.setStaffId(staff.getId());
						lstStaff = staffMgr.getListStaffPosition(filter);
					}

					if (lstStaff != null && lstStaff.size() > 0) {
						for (StaffPositionVO temp : lstStaff) {
							if (staff.getStaffType() != null && StaffObjectType.KTNPP.getValue().equals(staff.getStaffType().getObjectType())) {
								temp.setLstStaffOwnerIdStr(staff.getId().toString());
							}
							if (temp.getCreateTime() != null) {
								//temp.setHhmm(new SimpleDateFormat("HH:MM").format(temp.getCreateTime()));
								Calendar calendar = Calendar.getInstance();
								calendar.setTime(temp.getCreateTime());
								int hours = calendar.get(Calendar.HOUR_OF_DAY);
								int minutes = calendar.get(Calendar.MINUTE);
								temp.setHhmm((hours > 9 ? hours : ("0" + hours)) + ":" + (minutes > 9 ? minutes : ("0" + minutes)));
							}
						}
					}
					filter.setDateTime(commonMgr.getSysDate());
					//					List<ParentStaffMapVO> lstParent =staffMgr.getListParentStaffMapVO(filter);
					result.put("lstStaff", lstStaff);
					//					result.put("lstParentStaff", lstParent);
					result.put("lstStaffTraining", lstStaffTraining);
					result.put(ERROR, false);
				}
			} catch (BusinessException e) {
				LogUtility.logError(e, "SupperviseSalesAction.getListStaffOwner");
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		}
		return JSON;
	}*/
	
	/**
	 * Get list staff position
	 * @author trungtm6
	 * @since 24/08/2015
	 * @return
	 * 
	 * @modify hunglm16
	 * @since 24/11/2015
	 * @description ra soat phan quyen
	 */
	public String getListStaffPositionForShop() {
		Shop currentShop = null;
		try {
			if (currentUser != null && currentUser.getShopRoot() != null) {
				currentShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			if (currentShop != null) {
				staffCode = currentUser.getUserName();
				SupFilter filter = new SupFilter();
				filter.setShopId(currentShop.getId());
				filter.setChildShop(true);
				filter.setStrSpecType(StaffSpecificType.STAFF.getValue().toString() + ", " + StaffSpecificType.SUPERVISOR.getValue().toString() + ", " + StaffSpecificType.MANAGER.getValue().toString());
				filter.setStrStaffId(getStrListUserId());

				//add contraint ap_param config staff_type_id 
				filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				filter.setShopRootId(currentUser.getShopRoot().getShopId());
				List<ApParam> lstAp = apParamMgr.getListApParamByType(ApParamType.STAFF_TYPE_SHOW_SUP, ActiveType.RUNNING);
				String strStaffTypeId = "-1";
				//only add staff_type inserted in ap_param
				if (lstAp != null && lstAp.size() > 0) {
					for (ApParam ap : lstAp) {
						strStaffTypeId += ", ";
						strStaffTypeId += ap.getValue();
					}
				}
				filter.setStrStaffTypeId(strStaffTypeId);

				Staff staff = staffMgr.getStaffByCode(staffCode);
				if (staff != null) {
					List<StaffPositionVO> lstStaff = null;
					lstStaff = staffMgr.getListStaffPosition(filter);
					result.put("lstStaff", lstStaff);
					result.put(ERROR, false);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getListStaffOwner");
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		
		return JSON;
	}
	
	

	public String getListTrainingStaffPosition() {
		if (currentUser != null) {
			staffCode = currentUser.getUserName();
			try {
				Staff staff = staffMgr.getStaffByCode(staffCode);
				if (staff != null) {
					List<StaffPositionVO> lstStaff = null;
					lstStaff = staffMgr.getListStaffPositionWithTraining(shopIdSearch);
					result.put("lstStaff", lstStaff);
					result.put(ERROR, false);
				}
			} catch (BusinessException e) {
				LogUtility.logError(e, "SupperviseSalesAction.getListStaffOwner");
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		}
		return JSON;
	}

	public String monthAmountPlan() {
		try {
			if (staffId == null) {
				return SUCCESS;
			}
			staff = staffMgr.getStaffById(staffId);
			if (staff == null) {
				return SUCCESS;
			}
			AmountPlanStaffVO object = superviserMgr.getAmountPlanOfStaff(StaffObjectType.parseValue(type), staffId, shopId);
			if (object == null) {
				return SUCCESS;
			}
			soKeHoach = object.getDsBHKH();
			soBHDaQua = object.getSnBHDQ();
			tienDoChuan = object.getTienDoChuan();
			amountPlanStaffDetailVOs = object.getDetail();
			monthAmountPlan = BigDecimal.ZERO;
			monthAmount = BigDecimal.ZERO;
			monthApprovedAmount = BigDecimal.ZERO;
			progress = 0;
			exist = BigDecimal.ZERO;
			if (amountPlanStaffDetailVOs != null && amountPlanStaffDetailVOs.size() > 0) {
				for (AmountPlanStaffDetailVO detailVO : amountPlanStaffDetailVOs) {
					monthAmountPlan = monthAmountPlan.add(detailVO.getMonthAmountPlan());
					monthAmount = monthAmount.add(detailVO.getMonthAmount());
					monthApprovedAmount = monthApprovedAmount.add(detailVO.getMonthApprovedAmount());
					progress += detailVO.getProgress();
					if (detailVO.getExist().compareTo(BigDecimal.ZERO) > 0) {
						exist = exist.add(detailVO.getExist());
					}
				}
				exist = monthAmountPlan.subtract(monthAmount);
				//				Float f = monthAmount.floatValue()*100/monthAmountPlan.floatValue();
				//				progress = f.intValue();
				if (monthAmount.floatValue() == 0 && monthAmountPlan.floatValue() == 0) {
					progress = 0;
				}
				if (monthAmount.floatValue() != 0 && monthAmountPlan.floatValue() == 0) {
					progress = 100;
				}
				if (monthAmount.floatValue() != 0 && monthAmountPlan.floatValue() != 0) {
					Float f = monthAmount.floatValue() * 100 / monthAmountPlan.floatValue();
					progress = f.intValue();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.monthAmountPlan");
		}
		return SUCCESS;
	}

//	public String getListCustomerBySaleStaffHasVisitPlan() {
//		try {
//			if (staffId == null) {
//				return JSON;
//			}
//			List<CustomerVO> list = superviserMgr.getListCustomerBySaleStaffHasVisitPlan(staffId);
//			result.put("customers", list);
//		} catch (Exception e) {
//			LogUtility.logError(e, "SupperviseSalesAction.getListCustomerBySaleStaffHasVisitPlan");
//		}
//		return JSON;
//	}
	
	public String getListCustomerBySaleStaffHasVisitPlan() {
		try {
			if (staffId == null) {
				return JSON;
			}
			request = ServletActionContext.getRequest();
//			Long shopId = (Long) request.getSession().getAttribute(ConstantManager.SESSION_SHOP_ID);
			Date nowDate = commonMgr.getSysDate();
			if (!StringUtil.isNullOrEmpty(checkDate)) {
				nowDate = DateUtil.parse(checkDate, ConstantManager.FULL_DATE_FORMAT);
			}
//			List<CustomerVO> list = customerMgr.getListCustomerBySaleStaffHasVisitPlan(shopId, staffId, nowDate);
			List<CustomerVO> list = superviserMgr.getListCustomerBySaleStaffHasVisitPlan(staffId, nowDate);
			result.put("customers", list);

//			ths.dms.core.entities.filter.StaffFilter filter = new ths.dms.core.entities.filter.StaffFilter();
//			filter.setShopId(shopId);
//			filter.setStaffId(staffId);
//			filter.setCheckDate(nowDate);
//			filter.setActionLogObjectType(0);
//			Double distance = superviserMgr.getDistanceOfStaff(filter);
//			BigDecimal distanceBD = BigDecimal.ZERO;
//			if (distance != null) {
//				distanceBD = BigDecimal.valueOf(distance).setScale(1, BigDecimal.ROUND_HALF_UP);
//			}
//			result.put("distance", distanceBD);/** meter unit */
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.getListCustomerBySaleStaffHasVisitPlan");
		}
		return JSON;
	}

	public String showDirectionStaff() {
		try {
			if (staffId == null) {
				return JSON;
			}
			List<StaffPositionLog> list = superviserMgr.showDirectionStaff(staffId);
			result.put("list", list);
		} catch (Exception e) {
			LogUtility.logError(e, "SupperviseSalesAction.showDirectionStaff");
		}
		return JSON;
	}
	
	/**
	 * Tinh khoang cach theo duong di ngan nhat.
	 * @author trietptm
	 * @return JSON
	 * @since Sep 19, 2015
	 */
	public String calcShortestDistance() {
		try {
			if (staffId == null) {
				return JSON;
			}
			Date nowDate = DateUtil.now();
			if (!StringUtil.isNullOrEmpty(checkDate)) {
				nowDate = DateUtil.parse(checkDate, ConstantManager.FULL_DATE_FORMAT);
			}
			Integer numPoint = 0;
			ApParam nbPoint = apParamMgr.getApParamByCode(Constant.CF_NUMBER_POINTS_PER_HOUR, ApParamType.SYS_CONFIG);
			if (nbPoint != null) {
				numPoint = Integer.valueOf(nbPoint.getValue());
			}
			List<ActionLog> lstActionLog = staffMgr.getListActionLog(staffId, nowDate, fromTime, toTime);
			List<StaffPositionLogVO> listVO = new ArrayList<StaffPositionLogVO>();
			for (ActionLog actionLog : lstActionLog) {
				StaffPositionLogVO vo = new StaffPositionLogVO();
				vo.setCreateDate(actionLog.getStartTime());
				vo.setLat(actionLog.getLat());
				vo.setLng(actionLog.getLng());
				vo.setShop(actionLog.getShop());
				vo.setStaff(actionLog.getStaff());
				listVO.add(vo);
			}
			List<StaffPositionLog> lstStaffPositionLog = new ArrayList<StaffPositionLog>();
			StaffPositionLog lastPosition = staffMgr.getLastStaffPositionLog(staffId, nowDate, fromTime, toTime);
			if (numPoint == 0 || (type != null && type == 1)) {
				Long shopId = (Long) request.getSession().getAttribute(ConstantManager.SESSION_SHOP_ID);
				List<CustomerVO> listCustomer = staffMgr.getListCustomerHasVisitPlan(shopId, staffId, nowDate, fromTime, toTime);
				for (CustomerVO obj : listCustomer) {
					if ((obj.getIsOr() != null && obj.getIsOr() == 1) || (obj.getCount() != null && obj.getCount() >= 1) || (obj.getObjectType() != null && obj.getObjectType() == 0 && obj.getEndTime() == null)
							|| (obj.getObjectType() != null && (obj.getObjectType() == 0 || obj.getObjectType() == 1))) {
						StaffPositionLog log = new StaffPositionLog();
						log.setLat(Double.valueOf(obj.getLat()));
						log.setLng(Double.valueOf(obj.getLng()));
						log.setCreateDate(obj.getStartTime());
						lstStaffPositionLog.add(log);
					}
				}
			} else {
				lstStaffPositionLog = staffMgr.showDirectionStaff(staffId, nowDate, numPoint, fromTime, toTime);
			}
			if (lastPosition != null) {
				boolean isExist = false;
				for (StaffPositionLog tmp : lstStaffPositionLog) {
					if (lastPosition.getId().equals(tmp.getId())) {
						isExist = true;
						break;
					}
				}
				if (!isExist) {
					lstStaffPositionLog.add(lastPosition);
				}
			}
			for (StaffPositionLog staffPositionLog : lstStaffPositionLog) {
				StaffPositionLogVO vo = new StaffPositionLogVO();
				vo.setCreateDate(staffPositionLog.getCreateDate());
				Double lat = staffPositionLog.getLat();
				Double lng = staffPositionLog.getLng();
				vo.setLat(lat == null ? null : Float.valueOf(lat.toString()));
				vo.setLng(lng == null ? null : Float.valueOf(lng.toString()));
				vo.setShop(staffPositionLog.getShop());
				vo.setStaff(staffPositionLog.getStaff());
				listVO.add(vo);
			}
			Collections.sort(listVO, new Comparator<StaffPositionLogVO>() {
				@Override
				public int compare(StaffPositionLogVO p1, StaffPositionLogVO p2) {
					return p1.getCreateDate().compareTo(p2.getCreateDate());
				}
			});
			ApParam shortestDistance = apParamMgr.getApParamByCode(Constant.SHORTEST_DISTANCE, ApParamType.SYS_CONFIG);
			Integer value = 0;
			if (shortestDistance != null) {
				value = Integer.valueOf(shortestDistance.getValue());
			}
			int size = listVO.size();
			List<StaffPositionLogVO> listStandard = new ArrayList<StaffPositionLogVO>();
			for (int i = 0; i < size - 1; i++) {
				if (!listStandard.contains(listVO.get(i))) {
					listStandard.add(listVO.get(i));
				}
				for (int j = i + 1; j < size; j++) {
					StaffPositionLogVO start = listVO.get(i);
					StaffPositionLogVO end = listVO.get(j);
					double x = MathUtil.distFrom(start.getLat(), start.getLng(), end.getLat(), end.getLng());
					if (x >= value) {
						i = j - 1;
						listStandard.add(end);
						break;
					}
				}
			}
			result.put("list", listStandard);
		} catch (Exception e) {
			result.put("list", new ArrayList<StaffPositionLogVO>());
			LogUtility.logError(e, "SupperviseSalesAction.calcShortestDistance");
		}
		return JSON;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getShopIdSearch() {
		return shopIdSearch;
	}

	public void setShopIdSearch(Long shopIdSearch) {
		this.shopIdSearch = shopIdSearch;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getTbhvId() {
		return tbhvId;
	}

	public void setTbhvId(Long tbhvId) {
		this.tbhvId = tbhvId;
	}

	public Long getGdmId() {
		return gdmId;
	}

	public void setGdmId(Long gdmId) {
		this.gdmId = gdmId;
	}

	public Long getNvgsId() {
		return nvgsId;
	}

	public void setNvgsId(Long nvgsId) {
		this.nvgsId = nvgsId;
	}

	public Long getNvbhId() {
		return nvbhId;
	}

	public void setNvbhId(Long nvbhId) {
		this.nvbhId = nvbhId;
	}

	public Integer getSoKeHoach() {
		return soKeHoach;
	}

	public void setSoKeHoach(Integer soKeHoach) {
		this.soKeHoach = soKeHoach;
	}

	public Integer getSoBHDaQua() {
		return soBHDaQua;
	}

	public void setSoBHDaQua(Integer soBHDaQua) {
		this.soBHDaQua = soBHDaQua;
	}

	public Integer getTienDoChuan() {
		return tienDoChuan;
	}

	public void setTienDoChuan(Integer tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public List<AmountPlanStaffDetailVO> getAmountPlanStaffDetailVOs() {
		return amountPlanStaffDetailVOs;
	}

	public void setAmountPlanStaffDetailVOs(List<AmountPlanStaffDetailVO> amountPlanStaffDetailVOs) {
		this.amountPlanStaffDetailVOs = amountPlanStaffDetailVOs;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public BigDecimal getMonthAmountPlan() {
		return monthAmountPlan;
	}

	public void setMonthAmountPlan(BigDecimal monthAmountPlan) {
		this.monthAmountPlan = monthAmountPlan;
	}

	public BigDecimal getMonthAmount() {
		return monthAmount;
	}

	public void setMonthAmount(BigDecimal monthAmount) {
		this.monthAmount = monthAmount;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	public BigDecimal getExist() {
		return exist;
	}

	public void setExist(BigDecimal exist) {
		this.exist = exist;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public BigDecimal getDayAmountPlan() {
		return dayAmountPlan;
	}

	public void setDayAmountPlan(BigDecimal dayAmountPlan) {
		this.dayAmountPlan = dayAmountPlan;
	}

	public BigDecimal getDayAmount() {
		return dayAmount;
	}

	public void setDayAmount(BigDecimal dayAmount) {
		this.dayAmount = dayAmount;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getLhl() {
		return lhl;
	}

	public void setLhl(Integer lhl) {
		this.lhl = lhl;
	}

	public List<JETreeGridNode> getLstStaffTreeResult() {
		return lstStaffTreeResult;
	}

	public void setLstStaffTreeResult(List<JETreeGridNode> lstStaffTreeResult) {
		this.lstStaffTreeResult = lstStaffTreeResult;
	}

	public BigDecimal getDayAmountApproved() {
		return dayAmountApproved;
	}

	public void setDayAmountApproved(BigDecimal dayAmountApproved) {
		this.dayAmountApproved = dayAmountApproved;
	}

	public BigDecimal getMonthAmountApproved() {
		return monthAmountApproved;
	}

	public void setMonthAmountApproved(BigDecimal monthAmountApproved) {
		this.monthAmountApproved = monthAmountApproved;
	}

	public BigDecimal getMonthApprovedAmount() {
		return monthApprovedAmount;
	}

	public void setMonthApprovedAmount(BigDecimal monthApprovedAmount) {
		this.monthApprovedAmount = monthApprovedAmount;
	}

	public Integer getDayQuantityPlan() {
		return dayQuantityPlan;
	}

	public void setDayQuantityPlan(Integer dayQuantityPlan) {
		this.dayQuantityPlan = dayQuantityPlan;
	}

	public Integer getDayQuantity() {
		return dayQuantity;
	}

	public void setDayQuantity(Integer dayQuantity) {
		this.dayQuantity = dayQuantity;
	}

	public Integer getDayQuantityApproved() {
		return dayQuantityApproved;
	}

	public void setDayQuantityApproved(Integer dayQuantityApproved) {
		this.dayQuantityApproved = dayQuantityApproved;
	}

	public Integer getMonthQuantityPlan() {
		return monthQuantityPlan;
	}

	public void setMonthQuantityPlan(Integer monthQuantityPlan) {
		this.monthQuantityPlan = monthQuantityPlan;
	}

	public Integer getMonthQuantity() {
		return monthQuantity;
	}

	public void setMonthQuantity(Integer monthQuantity) {
		this.monthQuantity = monthQuantity;
	}

	public Integer getMonthQuantityApproved() {
		return monthQuantityApproved;
	}

	public void setMonthQuantityApproved(Integer monthQuantityApproved) {
		this.monthQuantityApproved = monthQuantityApproved;
	}

	public Long getNodeId() {
		return nodeId;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public Integer getNodeType() {
		return nodeType;
	}

	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}

	public Integer getChecked() {
		return checked;
	}

	public void setChecked(Integer checked) {
		this.checked = checked;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public String getSysdateMMddYyyyStr() {
		return sysdateMMddYyyyStr;
	}

	public void setSysdateMMddYyyyStr(String sysdateMMddYyyyStr) {
		this.sysdateMMddYyyyStr = sysdateMMddYyyyStr;
	}

	public Integer getNumPointLine() {
		return numPointLine;
	}

	public void setNumPointLine(Integer numPointLine) {
		this.numPointLine = numPointLine;
	}

}

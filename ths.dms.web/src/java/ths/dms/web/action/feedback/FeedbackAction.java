/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.web.action.feedback;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.FeedbackMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.FeedbackStaff;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.FeedbackStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.MediaType;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.CustomerFilter;
import ths.dms.core.entities.filter.FeedbackFilter;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.FeedbackVO;
import ths.dms.core.entities.vo.FeedbackVOSave;
import ths.dms.core.entities.vo.FeedbackVOTmp;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.ImageUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Action Feedback
 * @author vuongmq
 * @since 11/11/2015 
 */
public class FeedbackAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	public static final Integer SIZE_1024 = 1024;
	
	private Long curShopId;
	private Long shopId;
	private Long staffId;
	private Long feedbackId;
	private Long typeId;
	private Long maxTotalSize;
	private String shopCode;
	private String staffCode;
	private String customerCode;
	private String fromDate;
	private String toDate;
	private String typeParam;
	private String lstIdStr;
	private String content;
	private String fileExConfig;
	
	private Integer type;
	private Integer status;
	
	private Shop shop;
	private List<Long> lstId;
	private List<ApParam> lstApParam;
	private List<FeedbackVOTmp> lstStaffId;
	private List<File> images;
	private List<String> imagesContentType;
    private List<String> imagesFileName;
    
	private FeedbackMgr feedbackMgr;
	private CustomerMgr customerMgr;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		feedbackMgr = (FeedbackMgr) context.getBean("feedbackMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
	}

	@Override
	public String execute() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			Date curdate = commonMgr.getSysDate();
			fromDate = DateUtil.toDateString(DateUtil.get7AfterDate(curdate), DateUtil.DATE_FORMAT_DDMMYYYY);
			toDate = DateUtil.toDateString(curdate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				shopId = shop.getId();
				curShopId = shop.getId();
			}
			lstApParam = apParamMgr.getListApParam(ApParamType.FEEDBACK_TYPE, ActiveType.RUNNING);
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.FeedbackAction.execute()"), createLogErrorStandard(startLogDate));
		}
		return SUCCESS;
	}

	/**
	 * Search Feedback list
	 * @author vuongmq
	 * @return String
	 * @since 11/11/2015
	 */
	public String search() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			FeedbackFilter filter = new FeedbackFilter();
			KPaging<FeedbackVO> kPaging = new KPaging<FeedbackVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPagingVO(kPaging);
			filter.setStaffRootId(currentUser.getUserId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			if (type != null && FeedbackStatus.COMPLETE.getValue().equals(type)) {
				// seach theo doi van de
				if (shopId != null) {
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
						return JSON;
					}
					Shop shop = shopMgr.getShopById(shopId);
					if (shop != null) {
						filter.setShopId(shop.getId());
					}
				} else {
					filter.setShopId(currentUser.getShopRoot().getShopId());
				}
				filter.setCreateUserId(currentUser.getUserId()); //lay danh sach cua user dang nhap tao
				if (!StringUtil.isNullOrEmpty(staffCode)) {
					Staff staff = staffMgr.getStaffByCode(staffCode);
					filter.setStaffId(staff != null ? staff.getId() : null);
				}
				if (!StringUtil.isNullOrEmpty(customerCode)) {
					filter.setCustomerCode(customerCode); // ma ten dia chi
				}
			} else {
				// seach van de can thuc hien
				filter.setStaffId(currentUser.getUserId()); //lay danh sach cua user dang nhap can thuc hien
			}
			if (status != null && status > ActiveType.DELETED.getValue()) {
				filter.setStatus(status);
			}
			if (!StringUtil.isNullOrEmpty(typeParam)) {
				filter.setTypeParam(typeParam);
			}
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			ObjectVO<FeedbackVO> vo = feedbackMgr.getListFeedbackVOByFilter(filter);
			if (vo != null && vo.getLstObject() != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<FeedbackVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.FeedbackAction.search()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
	    return JSON;
	}

	/**
	 * Ham lay danh sach Media item
	 * @author vuongmq
	 * @since 17/11/2015
	 */
	public String getListItemAttach() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			result.put("rows", new ArrayList<MediaItem>());
			if (feedbackId != null) {
				FeedbackFilter filter = new FeedbackFilter();
				filter.setCreateUserId(currentUser.getUserId());
				filter.setFlagGetStaffId(Constant.FEEDBACK_CREATE_AND_OWNER_STAFF_ID);
				List<FeedbackVO> lstFeedbackIdOfStaff = feedbackMgr.getListFeedbackIdCheckByFilter(filter);
				if (lstFeedbackIdOfStaff != null && !lstFeedbackIdOfStaff.isEmpty()) {
					boolean flagInList = this.checkFeedbackIdInList(lstFeedbackIdOfStaff, feedbackId); 
					if (flagInList) {
						FeedbackFilter feedbackFilter = new FeedbackFilter();
						feedbackFilter.setFeedbackId(feedbackId);
						feedbackFilter.setType(MediaObjectType.IMAGE_FEEDBACK.getValue());
						List<MediaItem> lst = feedbackMgr.getListMediaItemFeedbackFilter(feedbackFilter);
						if (lst != null) {
							result.put("rows", lst);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.FeedbackAction.getListItemAttach()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put("rows", new ArrayList<MediaItem>());
		}
		return JSON;
	}

	/**
	 * Xu ly checkFeedbackIdInList
	 * @author vuongmq
	 * @param lstFeedbackIdOfStaff
	 * @return boolean
	 * @since Dec 17, 2015
	*/
	private boolean checkFeedbackIdInList(List<FeedbackVO> lstFeedbackIdOfStaff, Long feedbackId) {
		boolean flag = false;
		for (FeedbackVO feedbackVO : lstFeedbackIdOfStaff) {
			if (feedbackVO != null && feedbackVO.getFeedbackId() != null && feedbackVO.getFeedbackId().equals(feedbackId)) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	/**
	 * updateFeedback list, duyet or tu choi
	 * @author vuongmq
	 * @return String
	 * @since 17/11/2015
	 */
	public String updateFeedback() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		String err = null;
		result.put(ERROR, false);
		try {
			if (lstId != null && lstId.size() > 0) {
				if (!FeedbackStatus.checkFeedbackStatus(type)) {
					err = R.getResource("feedback.status.err");
				} else {
					FeedbackFilter filter = new FeedbackFilter();
					filter.setCreateUserId(currentUser.getUserId());
					// Can thuc hien; type: hoan thanh: 1
					if (FeedbackStatus.COMPLETE.getValue().equals(type)) {
						filter.setFlagGetStaffId(Constant.FEEDBACK_OWNER_STAFF_ID);
					} else if (FeedbackStatus.NEW.getValue().equals(type) || FeedbackStatus.APPROVED.getValue().equals(type)) {
						//Danh sach van de; type: tu choi: 0, or duyet: 2
						filter.setFlagGetStaffId(Constant.FEEDBACK_CREATE_STAFF_ID);
					}
					List<FeedbackVO> lstFeedbackIdOfStaff = feedbackMgr.getListFeedbackIdCheckByFilter(filter);
					
					FeedbackFilter filterUpdate = new FeedbackFilter();
					filterUpdate.setLstId(lstId);
					List<FeedbackStaff> lstFeedbackStaff = feedbackMgr.getListFeedbackStaffByFilter(filterUpdate);
					boolean flagInList = false;
					if (lstFeedbackStaff != null && lstFeedbackIdOfStaff != null && !lstFeedbackIdOfStaff.isEmpty()) {
						for (FeedbackStaff feedbackStaff : lstFeedbackStaff) {
							if (feedbackStaff != null && feedbackStaff.getFeedback() != null && feedbackStaff.getFeedback().getId() != null) {
								flagInList = this.checkFeedbackIdInList(lstFeedbackIdOfStaff, feedbackStaff.getFeedback().getId()); 
							}
							if (!flagInList) {
								break;
							}
						}
					}
					
					if (flagInList) {
						feedbackMgr.updateFeedback(lstFeedbackStaff, type, getLogInfoVO());
					} else {
						err = R.getResource("feedback.lst.invalid.err");
					}
				}
				
			} else {
				err = R.getResource("feedback.lst.select.err");
			}
		} catch (Exception e) {
			err = R.getResource("system.error");
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.FeedbackAction.updateFeedback()"), createLogErrorStandard(startLogDate));
		}
		
		if (!StringUtil.isNullOrEmpty(err)) {
			result.put(ERROR, true);
			result.put(ERR_MSG, err);
		}
		return JSON;
	}
	
	/**
	 * view detail
	 * @author vuongmq
	 * @return String
	 * @since 17/11/2015
	 */
	public String viewDetail() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			Date curdate = commonMgr.getSysDate();
			fromDate = DateUtil.toDateString(curdate, DateUtil.DATE_FORMAT_DDMMYYYY);
			lstApParam = apParamMgr.getListApParam(ApParamType.FEEDBACK_TYPE, ActiveType.RUNNING);
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				shopId = shop.getId();
				curShopId = shop.getId();
			}
			maxTotalSize = this.getTotalSizeParam();
			fileExConfig = FileUtility.getAddFileExtension(this.getFileExConfigAttach());
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.FeedbackAction.viewDetail()"), createLogErrorStandard(startLogDate));
		}
		return SUCCESS;
	}

	/**
	 * Xu ly getTotalSizeParam
	 * @author vuongmq
	 * @return Long
	 * @throws BusinessException
	 * @since Nov 26, 2015
	 */
	private Long getTotalSizeParam() throws BusinessException {
		long totalSize = 0L;
		ApParam apParam = apParamMgr.getApParamByCodeX(Constant.MAX_SIZE_UPLOAD_ATTACH, ApParamType.SYS, ActiveType.RUNNING);
		if (apParam != null && ValidateUtil.validateNumber(apParam.getValue())) {
			totalSize = Long.parseLong(apParam.getValue());
		} else {
			totalSize = Long.parseLong(String.valueOf(Configuration.getMaxImageUploadSize())) * SIZE_1024;
		}
		return totalSize;
	}
	
	/**
	 * Xu ly getFileExConfigAttach
	 * @author vuongmq
	 * @return String
	 * @throws BusinessException
	 * @since Nov 26, 2015
	 */
	private String getFileExConfigAttach() throws BusinessException {
		ApParam apParam = apParamMgr.getApParamByCodeX(Constant.WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH, ApParamType.SYS, ActiveType.RUNNING);
		String attachFileExConfig = apParam != null ? apParam.getValue() : Configuration.getUploadFileParamPath();
		return attachFileExConfig;
	}
	
	/**
	 * Search getListStaffFeedback
	 * @author vuongmq
	 * @return String
	 * @since 18/11/2015
	 */
	public String getListStaffFeedback() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			StaffFilter filter = new StaffFilter();
			KPaging<StaffVO> kPaging = new KPaging<StaffVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setStaffVOPaging(kPaging);
			if (shopId != null) {
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				if (shop != null) {
					filter.setShopId(shop.getId());
				}
			} else {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			}
			filter.setStaffRootId(currentUser.getUserId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			if (typeId != null && typeId > ActiveType.DELETED.getValue()) {
				filter.setTypeId(typeId);
			}
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				filter.setStaffCode(staffCode);
			}
			if (!StringUtil.isNullOrEmpty(lstIdStr)) {
				filter.setLstNotInId(this.getLstIdtoLstIdStr(lstIdStr));
			}
			ObjectVO<StaffVO> vo = null;
			if (type != null && StaffSpecificType.SUPERVISOR.getValue().equals(type)) {
				// lay quan ly cu staff dang nhap
				filter.setStaffId(currentUser.getUserId());
				vo = staffMgr.getListStaffVOUserMapStaffByFilter(filter);
			} else {
				filter.setIsCurrentUser(true);
				vo = staffMgr.getListStaffVOFeedbackByFilter(filter);
			}
			if (vo != null && vo.getLstObject() != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<StaffVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.FeedbackAction.getListStaffFeedback()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
	    return JSON;
	}

	/**
	 * Search getListCustomerFeedback
	 * @author vuongmq
	 * @return String
	 * @since 18/11/2015
	 */
	public String getListCustomerFeedback() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			CustomerFilter<CustomerVO> filter = new CustomerFilter<CustomerVO>();
			KPaging<CustomerVO> kPaging = new KPaging<CustomerVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			
			filter.setStaffRootId(currentUser.getUserId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				filter.setCustomerCode(customerCode);
			}
			if (!StringUtil.isNullOrEmpty(lstIdStr)) {
				filter.setLstNotInId(this.getLstIdtoLstIdStr(lstIdStr));
			}
			ObjectVO<CustomerVO> vo = null;
			if (type != null && StaffSpecificType.STAFF.getValue().equals(type)) {
				// lay ds customer tuyen cua staff duoc chon
				filter.setStaffId(staffId);
				vo = customerMgr.getCustomerVORoutingFeedback(filter);
			} else {
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					Shop shop = shopMgr.getShopByCode(shopCode);
					if (shop != null) {
						filter.setShopId(shop.getId());
						if (super.getMapShopChild().get(shop.getId()) == null) {
							result.put(ERROR, true);
							result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
							result.put("total", 0);
							result.put("rows", new ArrayList<CustomerVO>());
							return JSON;
						}
					} else {
						result.put(ERROR, true);
						result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
						result.put("total", 0);
						result.put("rows", new ArrayList<CustomerVO>());
						return JSON;
					}
				} else {
					filter.setShopId(currentUser.getShopRoot().getShopId());
				}
				vo = customerMgr.getCustomerVOFeedback(filter);
			}
			if (vo != null && vo.getLstObject() != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<CustomerVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.FeedbackAction.getListCustomerFeedback()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
	    return JSON;
	}

	/**
	 * Xu ly getLstIdtoLstIdStr
	 * @author vuongmq
	 * @param str
	 * @return List<Long>
	 * @since Nov 23, 2015
	 */
	private List<Long> getLstIdtoLstIdStr(String str) {
		List<Long> lstId = null;
		if (!StringUtil.isNullOrEmpty(str)) {
			lstId = new ArrayList<>(); 
			String strTmp[] = str.split(",");
			for (int i = 0, sz = strTmp.length; i < sz; i++) {
				if (!StringUtil.isNullOrEmpty(strTmp[i])) {
					lstId.add(Long.parseLong(strTmp[i].trim()));
				}
			}
		}
		return lstId;
	}
	
	/**
	 * saveFeedback tao van de
	 * @author vuongmq
	 * @return String
	 * @since 21/11/2015
	 */
	public String saveFeedback() {
		actionStartTime = DateUtil.now();
		resetToken(result);
		try {
			// xu ly create feedback
			if (currentUser == null || currentUser.getUserId() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			List<String> processFailImages = new ArrayList<String>();
			/***list danh sach image*/
			List<MediaItem> lstMediaItem = null;
			if (images != null && images.size() > 0) {
				/** store all image on disk first  */
				result.put("received", images.size());
				String attachFileExConfig = this.getFileExConfigAttach();
				errMsg = "";
				Long totalSizeFile = 0L;
				for (int i = 0, sz = images.size(); i < sz; i++) {
					totalSizeFile += images.get(i).length();
					String imageFile = imagesFileName.get(i);
					if (imageFile == null || images.get(i) == null || !FileUtility.checkFileExInWhiteListAttach(imageFile, attachFileExConfig)) {
						errMsg += imageFile + endline(1);
						processFailImages.add(imageFile);
					}
				}
				Long totalSizeParam = this.getTotalSizeParam();
				totalSizeParam += totalSizeParam * SIZE_1024;
				if (Long.compare(totalSizeParam, totalSizeFile) < 0) {
					errMsg = R.getResource("jsp.common.s.upload.maximum", totalSizeParam / SIZE_1024 / SIZE_1024);
					result.put(ERR_MSG, errMsg);
					return JSON;
				}
				// neu 1 file loi thi role back lai het file
				result.put("fail", processFailImages);
				if (errMsg != null && errMsg.length() > 0) {
					String msgImage = R.getResource("image.invalid.type.support", attachFileExConfig) + endline(1);
					result.put(ERR_MSG, msgImage + errMsg);
					return JSON;
				}
				// xu ly save file
				lstMediaItem = this.storeImageOnDisk(images, imagesFileName, imagesContentType, currentUser.getUserId());
				System.gc();
			}
			String msg = "";
			if (StringUtil.isNullOrEmpty(typeParam)) {
				msg = R.getResource("feedback.type.require");
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (StringUtil.isNullOrEmpty(fromDate)) {
					msg = R.getResource("feedback.remind.date.require");
				}
			}
			Date remindDate = null;
			if (StringUtil.isNullOrEmpty(msg)) {
				remindDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_NOW);
				if (remindDate == null) {
					msg = R.getResource("feedback.remind.date.format");
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTimeFull(DateUtil.now(), remindDate) > 0) {
					msg = R.getResource("feedback.remind.less.now.err");
				}
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				msg = ValidateUtil.validateField(content, "feedback.content", 1000, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				if (lstStaffId == null || lstStaffId.size() == 0) {
					msg = R.getResource("feedback.staff.done.require");
				}
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			FeedbackVOSave feedbackVOSave = new FeedbackVOSave();
			feedbackVOSave.setTypeParam(typeParam);
			feedbackVOSave.setRemindDate(remindDate);
			feedbackVOSave.setContent(content);
			feedbackVOSave.setLstStaffId(lstStaffId);
			feedbackVOSave.setLstMediaItem(lstMediaItem);
			feedbackMgr.saveFeedback(feedbackVOSave, getLogInfoVO());
		} catch (Exception e) {
			String errMsg = R.getResource("system.error");
			String err = e.getMessage();
			if (err != null && ExceptionCode.ERR_FILE_CREATE_FAIL.equals(err)) {
				errMsg = R.getResource("feedback.upload.create.file.err");
			}
			result.put(ERROR, true);
			result.put(ERR_MSG, errMsg);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.FeedbackAction.saveFeedback()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	/**
	 * 
	 * Xu ly storeImageOnDisk
	 * @author vuongmq
	 * @param receivedFiles
	 * @param filesName
	 * @param filesContentType
	 * @param staffId
	 * @return List<MediaItem>
	 * @throws Exception 
	 * @since Nov 26, 2015
	 */
	private List<MediaItem> storeImageOnDisk(List<File> receivedFiles, List<String> filesName, List<String> filesContentType, Long staffId) throws Exception {
		Date startLogDate = DateUtil.now();
		List<MediaItem> mediaItems = new ArrayList<MediaItem>();
		FileInputStream from = null;
		FileOutputStream to = null;
		try {
			// Tao danh sach Image mediaItem
			if (receivedFiles != null && receivedFiles.size() > 0) {
				Staff staff = staffMgr.getStaffById(staffId);
				Date sys = commonMgr.getSysDate();
				String realPath = Configuration.getImageRealSOPath();
				Calendar calF = Calendar.getInstance();
				String url = ImageUtility.createDetailUploadFilePath(calF, MediaObjectType.IMAGE_FEEDBACK.getValue(), null); //url thumbnail and image
				String folderImages = realPath + url;
				if (!FileUtility.isFolderExist(folderImages)) {
					FileUtility.createDirectory(folderImages);
				}
				for (int i = 0, sz = receivedFiles.size(); i < sz; i++) {
					try {
						File videoFile = receivedFiles.get(i);
						Calendar cal = Calendar.getInstance();
						String format = "." + FileUtility.getFileItemExtensionByFileName(filesName.get(i));
						Integer tmpM = cal.get(Calendar.MONTH) + 1;
						String dd = String.valueOf(cal.get(Calendar.DATE) > 10 ? cal.get(Calendar.DATE) : "0" + cal.get(Calendar.DATE));
						String MM = String.valueOf(tmpM > 10 ? tmpM : "0" + tmpM);
						String hh = String.valueOf(cal.get(Calendar.HOUR_OF_DAY) > 10 ? cal.get(Calendar.HOUR_OF_DAY) : "0" + cal.get(Calendar.HOUR_OF_DAY));
						String mm = String.valueOf(cal.get(Calendar.MINUTE) > 10 ? cal.get(Calendar.MINUTE) : "0" + cal.get(Calendar.MINUTE));
						String ss = String.valueOf(cal.get(Calendar.SECOND) > 10 ? cal.get(Calendar.SECOND) : "0" + cal.get(Calendar.SECOND));
						String fileNameMedia = staffId + "_" + dd + MM + cal.get(Calendar.YEAR) + hh + mm + ss + Long.toString(cal.getTimeInMillis()) + format;
						String fileNameUrl = url + fileNameMedia;
						File file = new File(folderImages + fileNameMedia);
						/* Begin copy file **/
						from = new FileInputStream(videoFile);
						to = new FileOutputStream(file);
						byte[] buffer = new byte[4096];
						int bytesRead;
						while ((bytesRead = from.read(buffer)) != -1) {
							to.write(buffer, 0, bytesRead); // write
						}
						/* End copy file **/
						MediaItem mediaItem = new MediaItem();
						mediaItem.setUrl(fileNameUrl);
						mediaItem.setThumbUrl(fileNameUrl);
						mediaItem.setTitle(filesName.get(i));
						mediaItem.setStaff(staff);
						mediaItem.setObjectType(MediaObjectType.IMAGE_FEEDBACK);
						mediaItem.setMediaType(MediaType.IMAGE);
						mediaItem.setFileSize(Float.valueOf(file.length()));
						mediaItem.setStatus(ActiveType.RUNNING);
						mediaItem.setCreateDate(sys);
						mediaItem.setCreateUser(currentUser.getUserName());
						mediaItems.add(mediaItem);
					} catch (Exception e) {
						LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.feedback.FeedbackAction.storeImageOnDisk() - receivedFiles index" + receivedFiles.get(i)), createLogErrorStandard(startLogDate));
						throw new Exception(ExceptionCode.ERR_FILE_CREATE_FAIL, e);
					} finally {
						if (from != null) {
							from.close();
						}
						if (to != null) {
							to.close();
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.feedback.FeedbackAction.storeImageOnDisk()"), createLogErrorStandard(startLogDate));
			throw new Exception(ExceptionCode.ERR_FILE_CREATE_FAIL, e);
		} finally {
			if (from != null) {
				from.close();
			}
			if (to != null) {
				to.close();
			}
		}
		return mediaItems;
	}
	
	public Long getCurShopId() {
		return curShopId;
	}

	public void setCurShopId(Long curShopId) {
		this.curShopId = curShopId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public List<ApParam> getLstApParam() {
		return lstApParam;
	}

	public void setLstApParam(List<ApParam> lstApParam) {
		this.lstApParam = lstApParam;
	}

	public String getTypeParam() {
		return typeParam;
	}

	public void setTypeParam(String typeParam) {
		this.typeParam = typeParam;
	}

	public Long getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(Long feedbackId) {
		this.feedbackId = feedbackId;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getLstIdStr() {
		return lstIdStr;
	}

	public void setLstIdStr(String lstIdStr) {
		this.lstIdStr = lstIdStr;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<FeedbackVOTmp> getLstStaffId() {
		return lstStaffId;
	}

	public void setLstStaffId(List<FeedbackVOTmp> lstStaffId) {
		this.lstStaffId = lstStaffId;
	}

	public final Long getMaxTotalSize() {
		return maxTotalSize;
	}

	public final void setMaxTotalSize(Long maxTotalSize) {
		this.maxTotalSize = maxTotalSize;
	}

	public final List<File> getImages() {
		return images;
	}

	public final void setImages(List<File> images) {
		this.images = images;
	}

	public final List<String> getImagesContentType() {
		return imagesContentType;
	}

	public final void setImagesContentType(List<String> imagesContentType) {
		this.imagesContentType = imagesContentType;
	}

	public final List<String> getImagesFileName() {
		return imagesFileName;
	}

	public final void setImagesFileName(List<String> imagesFileName) {
		this.imagesFileName = imagesFileName;
	}

	public final String getFileExConfig() {
		return fileExConfig;
	}

	public final void setFileExConfig(String fileExConfig) {
		this.fileExConfig = fileExConfig;
	}

}

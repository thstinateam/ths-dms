package ths.dms.web.action.general;

import java.util.ArrayList;
import java.util.List;

import viettel.passport.client.RoleToken;
import viettel.passport.client.ShopToken;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.NewTreeVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.TreeFlagExtentVO;
import ths.dms.core.entities.vo.TreeVOBasic;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * The Class CommonAction.
 */
public class CmsAction extends AbstractAction {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1313955969119454349L;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		super.execute();
		resetToken(result);
		return SUCCESS;
	}
	/**
	 * Lay danh sach tat ca NPP duoc phan quyen
	 * 
	 * @author hunglm16
	 * @since October 8,2014
	 * */
	public String getListShopIsLV5ByShopRoot(){
		result.put("rows", this.getListShopInOrgAccessByIdWithIsLevel(ShopSpecificType.NPP.getValue()));
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Lay danh sach tat ca NPP duoc phan quyen
	 * 
	 * @author hunglm16
	 * @since October 8,2014
	 * */
	public String getListShopIsChange(){
		result.put("rows", currentUser.getListShopByRole());
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Cap nhat Org_Access tuong tac
	 * 
	 * @author hunglm16
	 * @since March 22,2014
	 * */
	public String setUpdateSessionIsChange() {
		try {
			ShopToken shopRoot = new ShopToken();
			shopRoot = this.getShopInRoleById(shopId);
			this.setSessionChangeShopRoot(shopRoot);
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, e.getMessage());
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Cap nhat lai quyen NPP
	 * 
	 * @author hunglm16
	 * @since October 8,2014
	 * */
	public String setUpdateSessionIsShopIsLevel5() {
		if (shopId == null || !checkShopInOrgAccessByIdWithIsLevel(shopId, ShopSpecificType.NPP.getValue())) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
			return JSON;
		}
		try {
			ShopToken shopRoot = new ShopToken();
			shopRoot = this.getShopTockenInOrgAccessById(shopId);
			this.setSessionChangeShopRoot(shopRoot);
			//result.put("urlRedrect", ServletActionContext.getRequest().getRequestURI().trim());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, e.getMessage());
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Load lai trang chu va lua chon vai tro
	 * 
	 * @author hunglm16
	 * @since AUGUST 08,2014
	 * @return JSON
	 * */
	public String nextByRole(){
		try{
//			boolean flag = false;
//			if (id != null && id > 0) {
//				flag = true;
//			}
//			if (flag) {
//				lstRoleToken = new ArrayList<RoleToken>();
//				lstRoleToken = currentUser.getListRole();
//				flag = false;
//				for(int i=0; i<lstRoleToken.size(); i++){
//					if(lstRoleToken.get(i).getRoleId().equals(id)){
//						flag = true;
//						break;
//					}
//				}
//			}
//			if (!flag) {
//				result.put(ERROR, true);
//				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cms.login.home.err.isdefined.notrole"));
//				return JSON;
//			}
//			Staff staffCurrent = staffMgr.getStaffByCode(currentUser.getUserName());
//			CmsFilter filter = new CmsFilter();
//			filter.setUserId(currentUser.getUserId());
//			filter.setRoleId(id);
//			List<ShopVO> rows  = cmsMgr.getListShopInOrgAccess(filter);
//			if (rows == null || rows.size() == 0) {
//				ShopVO shopVo = new ShopVO();
//				shopVo.setShopId(staffCurrent.getShop().getId());
//				shopVo.setShopCode(staffCurrent.getShop().getShopCode());
//				shopVo.setShopName(staffCurrent.getShop().getShopName());
//				if (staffCurrent.getShop().getParentShop() != null) {
//					shopVo.setParentId(staffCurrent.getShop().getParentShop().getId());								
//				}
//				shopVo.setObjectType(staffCurrent.getShop().getType().getObjectType());
//				Integer isLevel = commonMgr.getIslevel(staffCurrent.getShop().getId());
//				shopVo.setIsLevel(isLevel);
//				rows.add(shopVo);
//			}
			List<ShopVO> lstShop = new ArrayList<ShopVO>();
			if (id != null) {
				lstShop = getListShopByRole(id);
			}
			result.put("rows", lstShop);			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Load lai trang chu khi chon vai tro
	 * 
	 * @author hunglm16
	 * @since AUGUST 08,2014
	 * @return JSON
	 * */
	public String updateIsOnline(){
		resetToken(result);
		errMsg ="";
		try{
			boolean flag = false;
			if (id != null && id > 0) {
				flag = true;
			}
			int dem = 0;
			if (flag) {
				lstRoleToken = new ArrayList<RoleToken>();
				lstRoleToken = currentUser.getListRole();
				flag = false;
				for (int i = 0; i < lstRoleToken.size(); i++) {
					dem = i;
					if (lstRoleToken.get(i).getRoleId().equals(id)) {
						flag = true;
						break;
					}
					
				}
			}
			if (!flag) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cms.login.home.err.isdefined.notrole"));
				return JSON;
			}
			CmsFilter filter = new CmsFilter();
			filter.setUserId(currentUser.getUserId());
			filter.setRoleId(id);
			filter.setAppId(currentUser.getAppToken().getAppId());
			if (lstId != null && lstId.size() > 0) {
				//Lay danh sach shop thuoc vai tro va danh sach Id tuong ung
				filter.setLstId(lstId);
				shopToken = new ShopToken();
				Shop shop = new Shop();
				shop = shopMgr.getShopById(lstId.get(0));
				if (shop != null) {
					shopToken.setShopId(shop.getId());
					shopToken.setShopCode(shop.getShopCode());
					shopToken.setShopName(shop.getShopName());
					if (shop.getParentShop() != null && shop.getParentShop().getId() != null) {
						shopToken.setParentId(shop.getParentShop().getId());
					}
					shopToken.setObjectType(shop.getType().getSpecificType() != null ? shop.getType().getSpecificType().getValue() : null);
					Integer isLevel = commonMgr.getIslevel(shop.getId());
					shopToken.setIsLevel(isLevel);
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cms.login.home.err.isdefined.notshop"));
				return JSON;
			}
			//setSesionNexRole
			setSessionNextRole(lstRoleToken.get(dem), shopToken);
		} catch (Exception e) {
			result.put(ERROR,true);
			LogUtility.logError(e,e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR,false);
		return JSON;
	}
	/**
	 * search tree shop by filter
	 * 
	 * @author hunglm16
	 * @since August 17,2014
	 * @return JSON
	 * @description Co the them tham so tuy bien voi CMS Fiter o day 
	 * */
	public String searchTreeShop() {
		actionStartTime = DateUtil.now();
		try {
			lstTreeShopVO = new ArrayList<TreeVOBasic<ShopVO>>();
			currentShop = shopMgr.getShopByParentIdNull();
			BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
			if (currentUser != null && currentUser.getShopRoot() != null) {
				filter.setId(currentUser.getShopRoot().getShopId());
			} else {
				filter.setId(currentShop.getId());
			}
			//filter.setLongG(currentUser.getRoleToken().getRoleId());
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
			// lay du lieu phan quyen duoi DB
			filter.setUserId(currentUser.getUserId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setStrShopId(getStrListShopId());
			TreeVOBasic<ShopVO> tree = new TreeVOBasic<ShopVO>();
			tree = cmsMgr.getTreeFullShopVOByFilter(filter);

			this.setStateNotInTree(tree, null);
			lstTreeShopVO.add(tree);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CmsAction.searchTreeShop()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put("rows", lstTreeShopVO);
		result.put(ERROR, false);
		return JSON;
	}
	
	/**
	 * Xu ly node tren cay
	 * 
	 * @author hunglm16
	 * @since August 17,2014
	 * @return JSON
	 * @description Co the tuy chinh de phu hop voi tung nghiep vu chuc nang
	 * */
	private void setStateNotInTree(TreeVOBasic<ShopVO> node, NewTreeVO<ShopVO, TreeFlagExtentVO<ShopVO>> channel){
		if(node!=null){
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<TreeVOBasic<ShopVO>> lstChild = new ArrayList<TreeVOBasic<ShopVO>>();
			lstChild = node.getChildren();
			if(lstChild!=null && !lstChild.isEmpty()){
				for(TreeVOBasic<ShopVO> nodeC:lstChild){
					//this.setStateNotInTree(nodeC, null);
					nodeC.setState(ConstantManager.JSTREE_STATE_OPEN);
					this.setStateNotInTree(nodeC, null);
					break;
				}
			}
		}
	}
	
	//////GETTER/SETTER
	private Long id;
	private List<RoleToken> lstRoleToken;
	private List<Long> lstId;
	private List<ShopToken> lstShopToken;
	private ShopToken shopToken;
	private List<TreeVOBasic<ShopVO>> lstTreeShopVO;
	private String code;
	private String name;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TreeVOBasic<ShopVO>> getLstTreeShopVO() {
		return lstTreeShopVO;
	}

	public void setLstTreeShopVO(List<TreeVOBasic<ShopVO>> lstTreeShopVO) {
		this.lstTreeShopVO = lstTreeShopVO;
	}

	public ShopToken getShopToken() {
		return shopToken;
	}

	public void setShopToken(ShopToken shopToken) {
		this.shopToken = shopToken;
	}

	public List<ShopToken> getLstShopToken() {
		return lstShopToken;
	}

	public void setLstShopToken(List<ShopToken> lstShopToken) {
		this.lstShopToken = lstShopToken;
	}
	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<RoleToken> getLstRoleToken() {
		return lstRoleToken;
	}

	public void setLstRoleToken(List<RoleToken> lstRoleToken) {
		this.lstRoleToken = lstRoleToken;
	}
}

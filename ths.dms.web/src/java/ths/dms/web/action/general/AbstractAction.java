package ths.dms.web.action.general;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.AppSetting;
import ths.dms.helper.Configuration;
import ths.dms.helper.LogInfo;
import ths.dms.helper.R;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.business.common.DynamicAttributeHelper;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.VSARole;
import ths.dms.web.enumtype.ViewActionType;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import viettel.passport.client.AppToken;
import viettel.passport.client.ControlToken;
import viettel.passport.client.CurrentPageToken;
import viettel.passport.client.FormToken;
import viettel.passport.client.RoleToken;
import viettel.passport.client.ShopToken;
import viettel.passport.client.ShopVOToken;
import viettel.passport.client.StaffToken;
import viettel.passport.client.UserToken;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import ths.dms.core.business.ApParamEquipMgr;
import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.AttributeMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.CmsMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CycleMgr;
import ths.dms.core.business.EquipProposalBorrowMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.ShopLockMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.AttributeValue;
import ths.dms.core.entities.AttributeValueDetail;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AppLogTypeEnum;
import ths.dms.core.entities.enumtype.ArithmeticEnum;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.enumtype.FormTypeCMS;
import ths.dms.core.entities.enumtype.FuncType;
import ths.dms.core.entities.enumtype.PaymentType;
import ths.dms.core.entities.enumtype.PermissionType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.SpecificGeneral;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.CmsVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.LogInfoVO4J;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.RoleVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.TreeVOBasic;
import ths.core.entities.vo.rpt.AppLogVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.filter.PriviledgeInfo;
import ths.dms.core.memcached.MemcachedUtils;

/**
 * @author hunglm16
 * @since October 8,2014
 * @description Dam nhan quan ly chinh
 * */
public class AbstractAction extends ActionSupport implements Preparable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2824406329982182819L;

	private ViewActionType viewActionType = ViewActionType.CREATE;

	/** The Constant JSON. */
	public static final String JSON = "json";
	public static final String LIST = "view";
	public static final String REDIRECT = "redirect";
	public static final int IMPORT_EXCEL = 0;
	public static final int VIEW_EXCEL = 1;
	public static final String PAGE_NOT_FOUND = "404";
	public static final String PAGE_NOT_PERMISSION = "550";
	public static final String SHOP_LOCK = "shop_lock";
	public static final String REPORT_PATH = "path";
	public static final Integer SIZE_ZERO = 0;
	public static final Integer CHIN_CHIN_CHIN = 999;

	public static final String ERR_MSG = "errMsg";
	/*
	 * indicate debug mode (true) or release mode (false)
	 * set to false when release
	 */
	protected final Boolean DEBUG = false;
	protected final ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");

	protected HttpSession session;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	private String systimestamp;
	protected ShopMgr shopMgr;
	protected StaffMgr staffMgr;
	protected SuperviserMgr superviserMgr;
	protected ShopLockMgr shopLockMgr;
	protected CommonMgr commonMgr;
	protected SaleOrderMgr saleOrderMgr;
	protected CmsMgr cmsMgr;
	protected ApParamMgr apParamMgr;
	protected ChannelTypeMgr channelTypeMgr;
	protected DynamicAttributeHelper dynamicAttributeHelper;
	protected EquipmentManagerMgr equipmentManagerMgr;
	protected ApParamEquipMgr apParamEquipMgr;
	protected EquipProposalBorrowMgr equipProposalBorrowMgr;

	protected UserToken currentUser;
	protected CurrentPageToken currentPageToken;

	protected Staff staff;
	protected Shop shopCurrentPage;
	private InputStream inputStream;

	protected Map<String, Integer> mapControl;
	protected Map<String, Object> result;
	protected Map<String, Object> parametersReport; // hoanv25,14/01/2015

	protected List<ControlToken> listControl;
	protected List<StatusBean> lstPaymentType;
	protected List<StatusBean> lstStatusBean;
	protected List<StatusBean> lstStatusBeanWithWaiting;
	protected List<Attribute> lstAttribute;
	protected List<AttributeDynamicVO> lstAttributeDynamic;
	protected List<AttributeDetailVO> lstAttributeDetail;
	protected List<Long> lstAttributeId;
	protected List<Integer> lstAttributeColumnType;
	protected List<Integer> lstAttributeColumnValueType;
	protected List<String> lstAttributeValue;
	protected List<List<String>> lstObjectValue;

	/**Lay du lieu ap_param*/
	protected List<ApParam> lstActiveType;

	protected int page;
	protected int max;
	protected int rows;

	private Integer reportForm;

	/**Lay du lieu sort grid*/
	protected String order; // loai order: asc, desc
	protected String sort;  // ten cot sort
	protected String lockDate;

	/** Doi tuong ghi log ung dung */
	protected LogInfoVO logObj;

	/** Ma chuc nang nguoi dung truy cap */
	protected String functionCode;

	protected String actionTypeCode;

	protected String lang;

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	/** The token. */
	protected String token;
	protected String reportCode;

	/** The err msg. */
	protected String errMsg;

	/** The is error. */
	protected boolean isError;

	/** The lst payment type. */

	/** The num fail. */
	protected int numFail;
	protected int totalItem;
	protected int numFinish;
	protected String fileNameFail;
	private int newToken;

	protected LogInfo logInfo; /** Thong tin doi tuong ghi log ung dung */

	protected List<CellBean> lstView;
	protected boolean typeView;
	private boolean activePermission;
	private boolean shopEnable;
	private boolean developCustomer;
	private boolean shopEnableBoth;
	private boolean commercialSupportPermission;
	private boolean headOfficePermission;

	private boolean saleMTPermission;
	private boolean vnm_ho;

	private boolean shopLocked;
	protected boolean isFullPrivilege;
	protected boolean sessionData;
	protected Boolean isShowDialogIsFcnIsLv5;

	/**
	 * Thoi gian bat dau thuc hien chuc nang
	 */
	protected Date actionStartTime;

	private String outputName;
	protected String shopCode;
	protected String shopName;
	protected String formatType;
	protected Shop currentShop;

	protected String urlRedrectCurentPage;

	protected Date dayLock;

	//Thong tin cau hinh ap_param;
	protected String sysDigitDecimal;
	protected String sysDecimalPoint;
	protected String sysCurrency;
	protected String sysSaleRoute;

	protected Integer isView;
	private Integer separationLot;

	protected List<List<AttributeDetailVO>> lstObjectAttributeDetail;
	protected List<TreeVOBasic<ShopVO>> cbxTreeShopVO;

	protected Long shopIdIsFcnIsLv5;
	protected Long shopId;

	protected Integer isShowChooseShopIsLv5;

	public static final String CONFIRM_ORDER_SEARCH = "CONFIRM_ORDER_SEARCH";
	public static final String ORDER_SEARCH = "ORDER_SEARCH";
	public static final String PRINT_DELIVERY_ORDER = "PRINT_DELIVERY_ORDER";

	protected List<Integer> lstYear;

	protected CycleMgr cycleMgr;

	public boolean isDevelopCustomer() {
		return checkRoles(VSARole.VNM_SALESONLINE_HO);
	}

	public boolean isShopEnable() {
		return checkRoles(VSARole.VNM_SALESONLINE_HO);
	}

	public String getSysDigitDecimal() {
		return sysDigitDecimal;
	}

	public void setSysDigitDecimal(String sysDigitDecimal) {
		this.sysDigitDecimal = sysDigitDecimal;
	}

	public String getSysDecimalPoint() {
		return sysDecimalPoint;
	}

	public void setSysDecimalPoint(String sysDecimalPoint) {
		this.sysDecimalPoint = sysDecimalPoint;
	}

	public String getSysCurrency() {
		return sysCurrency;
	}

	public void setSysCurrency(String sysCurrency) {
		this.sysCurrency = sysCurrency;
	}

	public boolean isShopEnableBoth() {
		return checkRoles(VSARole.VNM_SALESONLINE_HO);
	}

	public Shop getChooseShop() {
		if (request.getSession().getAttribute(ConstantManager.SESSION_SHOP_CHOOSE) != null) {
			return (Shop) request.getSession().getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
		}
		return null;
	}

	public List<StatusBean> getLstStatusBean() {
		lstStatusBean = new ArrayList<StatusBean>();
		StatusBean pausedStatus = new StatusBean(Long.valueOf(ActiveType.STOPPED.getValue()), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "pause.status.name"));
		StatusBean actionStatus = new StatusBean(Long.valueOf(ActiveType.RUNNING.getValue()), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.name"));
		lstStatusBean.add(actionStatus);
		lstStatusBean.add(pausedStatus);
		return lstStatusBean;
	}

	public void setLstStatusBean(List<StatusBean> lstStatusBean) {
		this.lstStatusBean = lstStatusBean;
	}

	public List<StatusBean> getLstStatusBeanWithWaiting() {
		List<StatusBean> lstStatusBeanWaiting = new ArrayList<StatusBean>();
		lstStatusBeanWaiting = getLstStatusBean();
		if (lstStatusBeanWaiting != null) {
			StatusBean waitingStatus = new StatusBean(Long.valueOf(ActiveType.WAITING.getValue()), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.waiting"));
			lstStatusBeanWaiting.add(waitingStatus);
		}
		return lstStatusBeanWaiting;
	}

	public Shop getCurrentShop() throws BusinessException {
		/*if (request.getSession().getAttribute(ConstantManager.SESSION_SHOP) != null) {
			return (Shop) request.getSession().getAttribute(ConstantManager.SESSION_SHOP);
		}
		return null;*/
		//trungtm6 sua lay theo shopRoot
		Shop curShop = null;
		if (currentUser != null && currentUser.getShopRoot() != null){
			curShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId()) ;
		} else if (request.getSession().getAttribute(ConstantManager.SESSION_SHOP) != null) {
			curShop = (Shop) request.getSession().getAttribute(ConstantManager.SESSION_SHOP);
		}
		return curShop;
	}

	/**
	 * Lay shop hien tai (refactor tu ham tim kiem khach hang)
	 * @author tuannd20
	 * @param shopCode ma shop dung de lay shop_token
	 * @return shop cua user dang dang nhap
	 * @throws BusinessException
	 * @since 04/09/2015
	 */
	protected Shop getCurrentShop(String shopCode) throws BusinessException {
		ShopToken shToken;
		if (shopCode == null || shopCode.isEmpty()) {
			shToken = currentUser.getShopRoot();
		} else {
			shToken = getShopTockenInOrgAccessByCode(shopCode);
		}
		if (shToken != null) {
			return shopMgr.getShopById(shToken.getShopId());
		}
		return null;
	}

	/**
	 * Lay shop_id cua shop hien tai
	 * @author tuannd20
	 * @param shopCode ma shop dung de lay shop_token
	 * @return shop_id cua shop cua user dang nhap
	 * @throws BusinessException
	 * @since 04/09/2015
	 */
	protected Long getCurrentShopId(String shopCode) throws BusinessException {
		Shop currentShop = getCurrentShop(shopCode);
		return currentShop != null ? currentShop.getId() : null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.opensymphony.xwork2.Preparable#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
		systimestamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
		actionStartTime = DateUtil.now(); // phuc vu ghi log chuc nang
		request = ServletActionContext.getRequest();
		response = ServletActionContext.getResponse();
		cmsMgr = (CmsMgr) context.getBean("cmsMgr");
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
		apParamEquipMgr = (ApParamEquipMgr) context.getBean("apParamEquipMgr");
		session = this.request.getSession(true);
		if (request.getSession().getAttribute("cmsUserToken") != null) {
			currentUser = (UserToken) request.getSession().getAttribute("cmsUserToken");
		}
		if (request != null && request.getSession() != null && request.getSession().getAttribute(ConstantManager.SESSION_TOKEN) != null) {
			token = String.valueOf(request.getSession().getAttribute(ConstantManager.SESSION_TOKEN));
		}
		/** Dan ngon ngu */
		String language = (String)session.getAttribute("lang");
		if(!StringUtil.isNullOrEmpty(language) && StringUtil.isNullOrEmpty(lang)){
			lang = language;
		}else if(StringUtil.isNullOrEmpty(lang)){
			lang = AppSetting.getStringValue("lang");
		}
		session.setAttribute("lang",lang);
		ActionContext.getContext().setLocale(new Locale(lang));

		result = new HashMap<String, Object>();
		result.put("token", token);
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		superviserMgr = (SuperviserMgr) context.getBean("superviserMgr");
		shopLockMgr = (ShopLockMgr) context.getBean("shopLockMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		cycleMgr = (CycleMgr) context.getBean("cycleMgr");
		max = rows;
		shopLocked = false;
		isFullPrivilege = false;
		isShowChooseShopIsLv5 = 1;
		urlRedrectCurentPage = request.getRequestURI().trim();

		if (currentUser != null && currentUser.getShopRoot() != null) {
			/**
			 * Ghi nhan log xu ly ung dung
			 *
			 * @author hunglm16
			 * @since August 27, 205
			 * @description Ham nay se phan luong chay song song, khong anh huong den luong he thong
			 * */
			this.createLogFormAccess();

			shopCode = currentUser.getShopRoot().getShopCode();
			shopName = currentUser.getShopRoot().getShopName();
			dayLock = shopLockMgr.getNextLockedDay(currentUser.getShopRoot().getShopId());
			if (dayLock == null) {
				dayLock = commonMgr.getSysDate();
			}
			lockDate = DateUtil.toDateSimpleFormatString(dayLock);
			listControl = new ArrayList<ControlToken>();
			FormToken form = getFormToken();
			mapControl = new HashMap<String, Integer>();
			if (form != null && form.getFullPrivilege() != null && form.getFullPrivilege() != 0) {
				isFullPrivilege = true;
			}
			if (form != null && form.getListControl() != null && form.getListControl().size() > 0) {
				if (form != null && form.getListControl() != null && form.getListControl().size() > 0) {
					listControl = form.getListControl();
					for (ControlToken controlToken : listControl) {
						mapControl.put(controlToken.getControlCode(), controlToken.getStatus());
					}
				}
			}
			if (!ShopSpecificType.NPP.getValue().equals(currentUser.getShopRoot().getIsLevel())) {
				if (currentUser.getListForm() != null && !currentUser.getListForm().isEmpty()) {
					checkFormIslevel5ByShop(request.getRequestURI().trim(), currentUser.getListForm());
				}
			}
			this.getLogInfoVO();
			/** Ghi nhan log xu ly ung dung */
			request.getSession().setAttribute(ConstantManager.SESSION_LOG_ERROR_STANDARD, this.createLogErrorStandard(DateUtil.now()));
		}
		request.getSession().setAttribute("currentPageToken", new CurrentPageToken());

		//Trungtm6 bo sung thong tin cau hinh ap_param
		ApParamFilter fitler = new ApParamFilter();
		fitler.setLstApCode(Arrays.asList(ConstantManager.SYS_DIGIT_DECIMAL, ConstantManager.SYS_DECIMAL_POINT, ConstantManager.SYS_CURRENCY));
		List<ApParam> lstAp = apParamMgr.getListApParamByFilter(fitler);
		sysSaleRoute = ConstantManager.ONE_TEXT;
		if (lstAp != null && lstAp.size() > 0){
			for (ApParam ap: lstAp){
				if (ConstantManager.SYS_DIGIT_DECIMAL.equals(ap.getApParamCode())){
					sysDigitDecimal = ap.getValue();
				} else if (ConstantManager.SYS_DECIMAL_POINT.equals(ap.getApParamCode())){
					sysDecimalPoint = ap.getValue();
				} else if (ConstantManager.SYS_CURRENCY.equals(ap.getApParamCode())){
					sysCurrency = ap.getValue();
				} else if (ConstantManager.SYS_SALE_ROUTE.equals(ap.getApParamCode())){
					sysSaleRoute = ap.getValue();
				}
			}
		}

	}

	/**
	 * Ghi log chuc nang menu
	 *
	 * @author hunglm16
	 * @return
	 * @since August 12,2015
	 */
	public void createLogFormAccess() {
		ExecutorService logformAccessService = Executors.newCachedThreadPool();
		logformAccessService.execute(new Runnable() {
			@Override
			public void run() {
				Date startLogDate = DateUtil.now();
				try {
					if (StringUtil.isNullOrEmpty(urlRedrectCurentPage)) {
						//Khong ghi log khi khong xac dinh duoc duong dan
						return;
					}
					if (urlRedrectCurentPage.equals(StringUtil.APP_LOG_HOME)) {
						//Khong ghi log voi trang chu
						return;
					}
					if (urlRedrectCurentPage.equals(StringUtil.APP_LOG_LOGIN)) {
						return;
					}
					AppLogVO appLog = new AppLogVO();
					//Date createLogDate = commonMgr.getSysDate();
					Date createLogDate = DateUtil.now();
					//ID bang {De rong}
					//ID Nhan vien
					appLog.setStaffId(currentUser.getUserId());
					//Ngay ghi log
					appLog.setLogDate(createLogDate);
					//Dia chi IP
					String ipAddress = request.getHeader("X-FORWARDED-FOR");
					if (ipAddress == null) {
						ipAddress = request.getRemoteAddr();
					}
					appLog.setIpAddress(ipAddress);
					//Ma ung dung
					appLog.setAppCode(Configuration.getDomainCode());
					//URL Gui den server
					String urlRequest = Configuration.getHostDomain() + urlRedrectCurentPage;
					appLog.setUrlRequest(urlRequest);
					//Ngay Tao
					appLog.setCreateDate(createLogDate);
					//Nguoi tao
					appLog.setCreateUser(currentUser.getUserName());
					//Xu ly Log Home
					if (currentUser.getShopRoot() != null) {
						//Ma Don Vi
						appLog.setShopId(currentUser.getShopRoot().getShopId());
						//ID Nhan vien thay the
						appLog.setIngeritStaffId(currentUser.getStaffRoot().getStaffId());
						//ID Vai tro
						appLog.setRoleId(currentUser.getRoleToken().getRoleId());
						//Loai log
						Integer permissionType = null;
						if (reportForm != null && reportForm == 1) {
							appLog.setLogType(AppLogTypeEnum.REPORT.getValue());
							permissionType = PermissionType.REPORT.getValue();
						} else {
							appLog.setLogType(AppLogTypeEnum.FORM.getValue());
							permissionType = PermissionType.FUNCTION.getValue();
						}
						CmsVO formAcess = cmsMgr.getFormCmsVoByUrl(urlRedrectCurentPage, permissionType, currentUser.getRoleToken().getRoleId(), currentUser.getUserId(), currentUser.getStaffRoot().getStaffId());
						if (formAcess != null) {
							//ID Man hinh
							appLog.setFormId(formAcess.getFormId());
						} else {
							appLog.setLogType(null);
						}
						//Them vao DB
						if (appLog.getLogType() != null && appLog.getIpAddress() != null) {
//							appLog = commonMgr.createEntity(appLog);
							//... Ghi log thanh file
							LogUtility.logAccessStandard(appLog);
						}
					}
				} catch (Exception e) {
					LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.general.AbstractAction.createLogFormAccess"), createLogErrorStandard(startLogDate));
				}
			}
		});
		logformAccessService.shutdown();
	}

	/**
	 * Ghi log chuc nang login
	 *
	 * @author hunglm16
	 * @param url
	 * @return
	 * @since August 12,2015
	 */
	public void createLogFormAccessLogin() {
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser != null) {
				if (StringUtil.isNullOrEmpty(urlRedrectCurentPage)) {
					return;
				}
				/*AppLog appLog = new AppLog();
				Date createLogDate = commonMgr.getSysDate();
				//ID Nhan vien
				appLog.setStaffId(currentUser.getUserId());
				//Ngay ghi log
				appLog.setLogDate(createLogDate);
				//Dia chi IP
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
					ipAddress = request.getRemoteAddr();
					if (StringUtil.isNullOrEmpty(ipAddress)) {
						ipAddress = request.getRemoteHost();
					}
				}
				appLog.setIpAddress(ipAddress);
				//Ma ung dung
				appLog.setAppCode(Configuration.getDomainCode());
				//URL Gui den server
				String urlRequest = Configuration.getHostDomain() + StringUtil.APP_LOG_LOGIN;
				appLog.setUrlRequest(urlRequest);
				//Ngay Tao
				appLog.setCreateDate(createLogDate);
				//Nguoi tao
				appLog.setCreateUser(currentUser.getUserName());
				//Xu ly Log
				if (urlRedrectCurentPage.equals(StringUtil.APP_LOG_LOGIN)) {
					//Loai log
					appLog.setLogType(AppLogTypeEnum.LOGIN.getValue());
					appLog = commonMgr.createEntity(appLog);
				}*/
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.general.AbstractAction.createLogFormAccess"), createLogErrorStandard(startLogDate));
		}
	}

	/**
	 * generate report token
	 * @author tuannd20
	 * @return Token
	 * @since 13/04/2015
	 */
	public String generateReportToken() {
		String reportToken = UUID.randomUUID().toString();
		return reportToken;
	}

	/**
	 * Lay report token da luu trong session
	 * @author tuannd20
	 * @param sessionKey Key luu token trong session
	 * @return Gia tri report_token da luu trong session
	 * @since 13/04/2015
	 */
	protected String retrieveReportToken(String sessionKey) {
		Object sessionValue = getSessionValue(sessionKey);
		String reportToken = sessionValue != null ? sessionValue.toString() : null;
		return reportToken;
	}

	/**
	 * put value to session
	 * @author tuannd20
	 * @param key Key on session
	 * @param value Value to put
	 * @since 13/04/2015
	 */
	public void putSessionValue(String key, Object value) throws IllegalArgumentException {
		if (StringUtil.isNullOrEmpty(key)) {
			throw new IllegalArgumentException("Key must not null.");
		}
		request.getSession().setAttribute(key, value);
	}

	/**
	 * Lay gia tri da put trong session
	 * @author tuannd20
	 * @param key Key cua gia tri
	 * @return Gia tri da put trong session. Null neu nhu ko co
	 * @since 13/04/2015
	 */
	protected Object getSessionValue(String key) {
		Object sessionValue = null;
		if (!StringUtil.isNullOrEmpty(key)) {
			sessionValue = request.getSession().getAttribute(key);
		}
		return sessionValue;
	}

	/**
	 * Lay thoi gian timeout khi put gia tri vao memcached o chuc nang bao cao. DB Key: SYS_REPORT_MEMCACHED_TIMEOUT
	 * @author tuannd20
	 * @return Gia tri timeout config trong DB
	 * @since 13/04/2015
	 */
	public int retrieveReportMemcachedTimeout() {
		final String SYS_REPORT_MEMCACHED_TIMEOUT = "SYS_REPORT_MEMCACHED_TIMEOUT";
		int reportMemcachedTimeout = ConstantManager.SYS_CONFIG_DEFAULT_REPORT_MEMCACHED_TIMEOUT_IN_SECOND;
		try {
			ApParam apParam = apParamMgr.getApParamByCodeEx(SYS_REPORT_MEMCACHED_TIMEOUT, ActiveType.RUNNING);
			if (apParam != null) {
				String apParamValue = apParam.getValue();
				reportMemcachedTimeout = Integer.parseInt(apParamValue);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Fail to retrive config for report memcached timeout. Use default value instead.");
		}
		return reportMemcachedTimeout;
	}

	/**
	 * Kiem tra URL co thuoc man hinh chuc nang isLevel5 (NPP)
	 *
	 * @param uri
	 *            not null, lstForm not null, arrCode not null
	 * @author hunglm16
	 * @since October 9,2014
	 * */
	private void checkFormIslevel5ByShop(String uri, List<FormToken> lstForm) {
		if (isShowChooseShopIsLv5 == 0) {
			return;
		}
		for (FormToken form : lstForm) {
			if (!StringUtil.isNullOrEmpty(form.getUrl()) && uri.trim().toLowerCase().equals(form.getUrl()) && SpecificGeneral.NPP.getValue().equals(form.getSpecific())) {
				isShowChooseShopIsLv5 = 0;
				return;
			} else if (form.getListForm() != null && !form.getListForm().isEmpty()) {
				checkFormIslevel5ByShop(uri, form.getListForm());
			}
		}
	}

	/**
	 * Generate token.
	 */
	public void generateToken() {
		if (request != null && request.getSession() != null && newToken == 0) {
			token = UUID.randomUUID().toString();
			request.getSession().setAttribute(ConstantManager.SESSION_TOKEN, token);
		}
	}

	/**
	 * Gets the session token.
	 *
	 * @return the session token
	 */
	public String getSessionToken() {
		if (request != null && request.getSession() != null && request.getSession().getAttribute(ConstantManager.SESSION_TOKEN) != null) {
			return request.getSession().getAttribute(ConstantManager.SESSION_TOKEN).toString();
		}
		return null;
	}

	public String getComboboxTreeShopVO() {
		try {
			BasicFilter<ShopVO> cmsFilter = new BasicFilter<ShopVO>();
			cmsFilter.setUserId(currentUser.getUserId());
			cmsFilter.setId(currentUser.getShopRoot().getShopId());
			cmsFilter.setRoleId(currentUser.getRoleToken().getRoleId());
			cmsFilter.setStrShopId(getStrListShopId());
			TreeVOBasic<ShopVO> treeVO = cmsMgr.getTreeFullShopVOByFilter(cmsFilter);
			if (treeVO != null) {
				cbxTreeShopVO = new ArrayList<TreeVOBasic<ShopVO>>();
				cbxTreeShopVO.add(treeVO);
			} else {
				cbxTreeShopVO = new ArrayList<TreeVOBasic<ShopVO>>();
			}
		} catch (Exception e) {
			cbxTreeShopVO = new ArrayList<TreeVOBasic<ShopVO>>();
			LogUtility.logError(e, "ReportManagerAction.prepare - " + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Cap nhat lai tocken
	 * @author hunglm16
	 * @param objJson
	 * @return
	 * @since 08-09-2015
	 */
	public String resetToken(Map<String, Object> objJson) {
		if (request != null && request.getSession() != null) {
			if (request.getSession().getAttribute(ConstantManager.SESSION_TOKEN) != null) {
				token = request.getSession().getAttribute(ConstantManager.SESSION_TOKEN).toString();
			}
			if (StringUtil.isNullOrEmpty(token) && newToken == 0) {
				token = UUID.randomUUID().toString();
				request.getSession().setAttribute(ConstantManager.SESSION_TOKEN, token);
			}
			objJson.put("token", token);
		}
		if (!StringUtil.isNullOrEmpty(token)) {
			return token;
		}
		return null;
	}

	public String genExportFileSuffix() {
		Date now = DateUtil.now();
		StringBuffer buff = new StringBuffer();
		buff.append(DateUtil.getSecond(now));
		buff.append("_");
		buff.append(DateUtil.getHour(now));
		buff.append("_");
		buff.append(DateUtil.getMinute(now));
		buff.append("_");
		buff.append(DateUtil.getDay(now));
		buff.append("_");
		buff.append(DateUtil.getMonth(now));
		buff.append("_");
		buff.append(DateUtil.getYear(now));
		return buff.toString();
	}

	public String genFileSuffix() {
		Calendar cal = Calendar.getInstance();
		Date now = DateUtil.now();
		StringBuffer buff = new StringBuffer();
		buff.append(DateUtil.getYear(now));
		buff.append(DateUtil.getMonth(now));
		buff.append(DateUtil.getDay(now));
		buff.append(DateUtil.getHour(now));
		buff.append(DateUtil.getMinute(now));
		buff.append(cal.getTimeInMillis());
		return buff.toString();
	}

	/**
	 * Gets the log info vo.
	 *
	 * @author tientv11 Thiet lap lai ham ghi log ung dung
	 * @return the log info vo
	 */
	public LogInfoVO getLogInfoVO() {
		logObj = new LogInfoVO();
		logObj.setIp(request.getRemoteAddr());
		logObj.setStaffCode(currentUser.getUserName());
		logObj.setShopRootId(currentUser.getShopRoot().getShopId());
		logObj.setStaffRootId(currentUser.getStaffRoot().getStaffId());
		try {
			if (session.getId() != null) {
				logObj.setSessionId(session.getId());
			}
			logObj.setAppCode(Configuration.getDomainCode());
			logObj.setStartDate(DateUtil.now());

			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			logObj.setIp(ipAddress);
			logObj.setFunctionCode(functionCode);
			logObj.setActionType(actionTypeCode);
		} catch (Exception e) {
			System.out.println("get: log error");
		}
		return logObj;
	}

	public void getOutputFailExcelFile(List<CellBean> lstFails, String templateName) {
		if (lstFails.size() > 0) {
			numFail = lstFails.size();
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + templateName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + templateName;
			String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report", lstFails);
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (InvalidFormatException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (IOException e) {
				//System.out.print(e.getMessage());
				LogUtility.logError(e, e.getMessage());
			}
			fileNameFail = Configuration.getStoreImportFailDownloadPath() + outputName;
		}
	}
	/**
	 * Export Excel khong can qua chung thuc
	 * @param beans
	 * @param basePath
	 * @param tempFileName
	 * @return
	 * @throws Exception
	 * @since 15/09/2015
	 */
	public String exportExcelJxlsNotAuthentication(Map<String, Object> beans, String basePath, String tempFileName) throws Exception {
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			if(beans != null){
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + basePath + tempFileName;//Configuration.getExcelTemplatePath()
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName =  DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + tempFileName;
				String exportFileName = Configuration.getStoreImportDownloadPath() + outputName;
				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				outputPath = Configuration.getStoreImportFailDownloadPath() + outputName;
				result.put(LIST, outputPath);
			}
		} catch (Exception e) {
			outputPath = "";
			throw new Exception("AbstractAction.exportExcelJxls()" + e.getMessage());
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				os.flush();
				os.close();
			}
		}
		return outputPath;
	}
	/**
	 * Export template
	 * @author trungtm6
	 * @since 15/07/2015
	 * @param beans
	 * @param basePath
	 * @param tempFileName
	 * @return
	 * @throws Exception
	 */
	public String exportExcelJxls(Map<String, Object> beans, String basePath, String tempFileName) throws Exception {
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			if(beans != null){
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + basePath + tempFileName;//Configuration.getExcelTemplatePath()
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName =  DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + tempFileName;
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(LIST, outputPath);
				MemcachedUtils.putValueToMemcached(retrieveReportToken(reportCode), outputPath, retrieveReportMemcachedTimeout());
			}
		} catch (Exception e) {
			outputPath = "";
			throw new Exception("AbstractAction.exportExcelJxls()" + e.getMessage());
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				os.flush();
				os.close();
			}
		}
		return outputPath;
	}

	/**Xuat file excel import loi, da ngon ngu
	 * @author cuonglt3
	 * @param lstFails
	 * @param templateName
	 * @param params
	 * @param outputName
	 */
	public void getOutputFailExcelFileMultiLanguage(List<CellBean> lstFails, String templateName, Map<String, Object> params, String outputName) {
		if (lstFails.size() > 0) {
			numFail = lstFails.size();
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + templateName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + templateName;
			String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);
			params.put("report", lstFails);
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (InvalidFormatException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (IOException e) {
				//System.out.print(e.getMessage());
				LogUtility.logError(e, e.getMessage());
			}
			fileNameFail = Configuration.getStoreImportFailDownloadPath() + outputName;
		}
	}
	/**
	 * xuat file  bao loi dung duong dan Import
	 * @author phuocdh2
	 * @param params
	 * @param templateName
	 */
	public void getOutputFailExcel(List<CellBean> lstFails, String templateName) {
		if (lstFails.size() > 0) {
			numFail = lstFails.size();
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + templateName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + templateName;
			String outputFileName = Configuration.getStoreImportDownloadPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report", lstFails);
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (InvalidFormatException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (IOException e) {
				//System.out.print(e.getMessage());
				LogUtility.logError(e, e.getMessage());
			}
			fileNameFail = Configuration.getStoreImportFailDownloadPath() + outputName;
		}
	}
	/**
	 * Thiet ke template hoac day du lieu vao file excel XLS
	 * @author phuocdh2
	 * @param params
	 * @param templateName
	 */
	public String putDataToExcelXLS(Map<String, Object> params, String templateName) {
		return putDataToExcelXLS(params,templateName,templateName,null,null);
	}
	/**
	 * Thiet ke template hoac day du lieu vao file excel XLS
	 * @author longnh15
	 * @param params
	 * @param templateName
	 * @param outputTemplateName
	 * @param oldSheetName
	 * @param newSheetName
	 */
	public String putDataToExcelXLS(Map<String, Object> params, String templateName, String outputFile, String oldSheetName, String newSheetName) {
		String path = "";
		if (params != null && params.size() >0) {
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog() + templateName;
			templateFileName = templateFileName.replace('/', File.separatorChar);

			//String outputName = templateName;
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + outputFile;
			String outputFileName = Configuration.getStoreRealPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);

			XLSTransformer transformer = new XLSTransformer();
			try {
				if (oldSheetName!=null && newSheetName!=null)
				{
					transformer.setSpreadsheetToRename(oldSheetName, newSheetName);
				}
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (InvalidFormatException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (IOException e) {
				LogUtility.logError(e, e.getMessage());
			}
			path =  Configuration.getExportExcelPath() + outputName;
		}
		return path;
	}
	/**
	 * Do tieu de da ngon ngu va du lieu ra excel XLS
	 * @author longnh15
	 * @param params : gom tieu de da ngon ngu
	 * @param templateName : ten file template
	 * @param outputTemplateName
	 * @param oldSheetName
	 * @param newSheetName
	 */
	public void exportExcelDataNotSessionXLS(Map<String, Object> params,List<CellBean> lstData, String tempFileName, String outputFile, String oldSheetName, String newSheetName) {
		try {
			if (lstData != null && lstData.size() > 0) {
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + tempFileName;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				//String outputName = templateName;
				String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + outputFile;
				String outputFileName = Configuration.getStoreRealPath() + outputName;
				outputFileName = outputFileName.replace('/', File.separatorChar);

				params.put("report", lstData);
				InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				if (oldSheetName!=null && newSheetName!=null)
				{
					transformer.setSpreadsheetToRename(oldSheetName, newSheetName);
				}
				org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
				inputStream.close();
				OutputStream os = new BufferedOutputStream(new FileOutputStream(outputFileName));
				resultWorkbook.write(os);
				os.flush();
				os.close();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(LIST, outputPath);
				result.put(REPORT_PATH, outputPath);
				result.put("hasData", true);
				MemcachedUtils.putValueToMemcached(retrieveReportToken(reportCode), outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put("hasData", false);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}
	/**
	 * Do tieu de da ngon ngu va du lieu ra excel XLS
	 * @author phuocdh2
	 * @param params : gom tieu de da ngon ngu
	 * @param templateName : ten file template
	 */
	public void exportExcelDataNotSessionXLS(Map<String, Object> params,List<CellBean> lstData, String tempFileName) {
		try {
			if (lstData != null && lstData.size() > 0) {
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + tempFileName;
				templateFileName = templateFileName.replace('/', File.separatorChar);//
				String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + tempFileName;
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				String outputPath = Configuration.getExportExcelPath() + outputName;

				//quangnv11 20150527 very large size data export
				if(outputName.endsWith("jasper")){
					outputName = outputName.replace(".jasper", ".xls");
					exportFileName = Configuration.getStoreRealPath() + outputName;
					outputPath = Configuration.getExportExcelPath() + outputName;
					System.out.println("Jasper report!");
						if(lstData.size()>2000){
						    JRSwapFile swapFile = new JRSwapFile(Configuration.getStoreRealPath(), 1024, 1024);
							JRSwapFileVirtualizer virtualizer = new JRSwapFileVirtualizer(2, swapFile, true);
							params.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
						}
				        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new File(templateFileName));
				        params.put(JRParameter.IS_IGNORE_PAGINATION, new Boolean(false));
				        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(lstData));
				        JRXlsExporter exporter = new JRXlsExporter();
				        exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
				        exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, exportFileName);
				        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				        exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
				       // exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				        exporter.exportReport();
				}else{
					//Map<String, Object> params = new HashMap<String, Object>();
					params.put("report", lstData);
					InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					org.apache.poi.ss.usermodel.Workbook wb = transformer.transformXLS(inputStream, params);;
					inputStream.close();
					OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					wb.write(os);
					os.flush();
					os.close();
				}
				result.put(LIST, outputPath);
				result.put(REPORT_PATH, outputPath);
				result.put("hasData", true);
			} else {
				result.put("hasData", false);
			}
		}  catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	/**
	 * getOutputFailForImportSaleOrder.
	 *
	 * @param money
	 *            the money
	 * @author nhanlt
	 * @since Apr 26, 2014
	 */
	public void getOutputFailForImportSaleOrder(List<CellBean> lstFails, Integer numFailS, String templateName) {
		if (lstFails.size() > 0) {
			numFail = numFailS;
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + templateName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + templateName;
			String outputFileName = Configuration.getStoreRealPath() + outputName;
			outputFileName = outputFileName.replace('/', File.separatorChar);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report", lstFails);
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.transformXLS(templateFileName, params, outputFileName);
			} catch (ParsePropertyException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (InvalidFormatException e) {
				LogUtility.logError(e, e.getMessage());
			} catch (IOException e) {
				//System.out.print(e.getMessage());
				LogUtility.logError(e, e.getMessage());
			}
			fileNameFail = Configuration.getExportExcelPath() + outputName;
		}
	}

	/**
	 * Convert money.
	 *
	 * @param money
	 *            the money
	 * @return the string
	 * @author phut
	 * @since Aug 27, 2012
	 */
	public String convertMoney(BigDecimal money) {
		String result = "";
		/* String _money = money.longValue() + ""; */
		String _money = money.toBigInteger() + "";
		int isDot = 1;
		for (int i = _money.length(); i > 0; i--) {
			char ch = _money.charAt(i - 1);
			if (isDot == 3 && i != 1) {
				if (_money.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "," + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}

	/**
	 * convertMoneyPOVNM
	 * Dung cho cac don Tra thi hien thi so duong
	 * @param number
	 * @return the string
	 * @author vuongmq
	 * @since Aug 10, 2015
	 */
	public String convertMoneyPOVNM(BigDecimal money) {
		String result = "";
		if (money == null) {
			money = BigDecimal.ZERO;
		}
		// mong muon don tra se lay gia tri tong tien la: Duong
		money = money.abs();
		String _money = money.toBigInteger() + "";
		int isDot = 1;
		for (int i = _money.length(); i > 0; i--) {
			char ch = _money.charAt(i - 1);
			if (isDot == 3 && i != 1) {
				if (_money.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "," + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}

	/**
	 * Convert money.
	 *
	 * @param money
	 *            the money
	 * @return the string
	 * @author phut
	 * @since Aug 27, 2012
	 */
	public String convertMoneyVAT(BigDecimal money) {
		String result = "";
		/* String _money = money.longValue() + ""; */
		String _money = money.toBigInteger() + "";
		int isDot = 1;
		for (int i = _money.length(); i > 0; i--) {
			char ch = _money.charAt(i - 1);
			if (isDot == 3 && i != 1) {
				if (_money.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "." + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}

	/***
	 * Format phan nghin cho so
	 * @author trungtm6
	 * @since 09/06/2015
	 * @param number
	 * @return
	 */
	public String formatNumber(BigDecimal number) {
		String result = "";
		if (number != null) {
			result = number.toString();
		}
		if (number != null && Math.abs(number.doubleValue()) > 999){
			BigDecimal temp = number;
			if (number.compareTo(BigDecimal.ZERO) < 0){
				temp = number.multiply(new BigDecimal(-1));
			}
			String strNum = temp.toString();
			StringBuilder decStr = new StringBuilder("");
			if (strNum.indexOf(".") > 0) {//so thuc
				String[] arr = temp.toString().split("\\.");
				if (arr != null && arr.length == 2){
					String decimal = new StringBuilder(arr[0]).reverse().toString();
					for (int i = 0; i < decimal.length(); i++){
						if (i > 0 && (i % 3) == 0){
							decStr.append(",");
						}
						decStr.append(decimal.charAt(i));
					}
					result = decStr.reverse().toString() + "." + arr[1];
					if (number.compareTo(BigDecimal.ZERO) < 0){
						result = "-" + result;
					}
				}
			} else {
				String decimal = new StringBuilder(strNum).reverse().toString();
				for (int i = 0; i < decimal.length(); i++){
					if (i > 0 && (i % 3) == 0){
						decStr.append(",");
					}
					decStr.append(decimal.charAt(i));
				}
				result = decStr.reverse().toString();
				if (number.compareTo(BigDecimal.ZERO) < 0){
					result = "-" + result;
				}
			}

		}
		return result;
	}

	/**
	 * @author trungtm6
	 * @since 20/07/2015
	 * @param size: lst data size
	 * @return
	 */
	public String checkMaxRecordImportPermiss(int size){
		String res = "";
		try{
			ApParam ap = apParamMgr.getApParamByCodeEx(ConstantManager.MAX_RECORD_IMPORT, ActiveType.RUNNING);
			int maxRec = ConstantManager.MAX_RECORDS_IMPORT;
			if (ap != null && ap.getValue() != null){
				try{
					maxRec = Integer.parseInt(ap.getValue());
				} catch (Exception ex){
					maxRec = ConstantManager.MAX_RECORDS_IMPORT;
				}
			}
			if (size > maxRec){
				res = R.getResource("import.max.record.permiss", maxRec);
			}
		} catch(Exception e){
			LogUtility.logError(e, "checkMaxRecordImportPermiss" + e.getMessage());
		}
		return res;
	}

	/**
	 * Check valid token for import
	 * @author trungtm6
	 * @since 20/07/2015
	 * @return
	 */
	public Boolean checkValidToken(){
		Boolean res = true;
		Object sesToken = request.getSession().getAttribute(ConstantManager.SESSION_TOKEN);
		Object reqToken = request.getParameter(ConstantManager.TOKEN_NAME);
		if(reqToken != null && sesToken != null){
			if (!reqToken.equals(sesToken)){
				res = false;
			} else {
				String resetToken = UUID.randomUUID().toString();
				request.getSession().setAttribute(ConstantManager.SESSION_TOKEN, resetToken);
				token = resetToken;
			}
		}
		return res;
	}

	/**
	 * Display date.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 * @author phut
	 * @since Aug 31, 2012
	 */
	public String displayDate(Date date) {
		return DateUtil.toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
	}

	/**
	 * Display number.
	 *
	 * @param number
	 *            the number
	 * @return the string
	 * @author phut
	 * @since Aug 31, 2012
	 */
	public String displayNumber(Object number) {
		String result = "";
		if (number instanceof Short) {
			Short __number = (Short) number;
			String _number = __number.shortValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "," + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Integer) {
			Integer __number = (Integer) number;
			String _number = __number.intValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "," + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Long) {
			Long __number = (Long) number;
			String _number = __number.longValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "," + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Double) {
			Double __number = (Double) number;
			DecimalFormat formater = new DecimalFormat("#0.00");
			result = formater.format(__number);
		} else if (number instanceof Float) {
			Float __number = (Float) number;
			DecimalFormat formater = new DecimalFormat("#0.00");
			result = formater.format(__number);
		} else if (number instanceof BigDecimal) {
			BigDecimal __number = (BigDecimal) number;
			result = convertMoney(__number);
		}
		return result;
	}

	/**
	 * Display number.
	 * Dung cho cac don Tra thi hien thi so duong
	 * @param number
	 * @return the string
	 * @author vuongmq
	 * @since Aug 11, 2015
	 */
	public String displayNumberPOVNM(Object number) {
		String result = "";
		if (number instanceof Short) {
			Short __number = (Short) number;
			// mong muon don tra se lay gia tri tong tien la: Duong
			__number = (short) Math.abs(__number);
			String _number = __number.shortValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "," + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Integer) {
			Integer __number = (Integer) number;
			// mong muon don tra se lay gia tri tong tien la: Duong
			__number = Math.abs(__number);
			String _number = __number.intValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "," + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Long) {
			Long __number = (Long) number;
			// mong muon don tra se lay gia tri tong tien la: Duong
			__number = Math.abs(__number);
			String _number = __number.longValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "," + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Double) {
			Double __number = (Double) number;
			// mong muon don tra se lay gia tri tong tien la: Duong
			__number = Math.abs(__number);
			DecimalFormat formater = new DecimalFormat("#0.00");
			result = formater.format(__number);
		} else if (number instanceof Float) {
			Float __number = (Float) number;
			// mong muon don tra se lay gia tri tong tien la: Duong
			__number = Math.abs(__number);
			DecimalFormat formater = new DecimalFormat("#0.00");
			result = formater.format(__number);
		} else if (number instanceof BigDecimal) {
			BigDecimal __number = (BigDecimal) number;
			// mong muon don tra se lay gia tri tong tien la: Duong
			__number = __number.abs();
			result = convertMoney(__number);
		}
		return result;
	}

	/**
	 * Lay thong tin Don vi hien thi sau khi dang nhap
	 *
	 * @author hunglm16
	 * @since August 22,2014
	 * */
	public ShopToken getCurrentShopInSession() {
		if (currentUser.getShopRoot() != null) {
			return currentUser.getShopRoot();
		}
		return new ShopToken();
	}

	/**
	 * Kiem tra danh sach tham so la shopCode co thuoc quyen du lieu hay khong?
	 *
	 * @author hunglm16
	 * @return true: thoa man , false: co 1 nhat 1 ma don vi khong thuoc quyen
	 *         quan ly
	 * @since August 22,2014
	 * */
	public boolean checkShopPermission(String... arrShopCode) {
		if (arrShopCode == null || arrShopCode.length == 0) {
			return false;
		}
		int dem = 0;
		for (int i = 0; i < arrShopCode.length; i++) {
			for (ShopToken shopTK : currentUser.getListShop()) {
				if (arrShopCode[i].trim().toUpperCase().equals(shopTK.getShopCode().trim().toUpperCase())) {
					dem++;
					break;
				}
			}
		}
		if (dem < arrShopCode.length) {
			return false;
		}
		return true;
	}

	/**
	 * Kiem tra danh sach tham so la staffCode co thuoc quyen du lieu hay khong?
	 *
	 * @author hunglm16
	 * @return true: thoa man , false: co 1 nhat 1 ma don vi khong thuoc quyen
	 *         quan ly
	 * @since August 22,2014
	 * */
	public boolean checkStaffByStaffRoot(String staffCode) {
		if (StringUtil.isNullOrEmpty(staffCode)) {
			return false;
		}
		staffCode = staffCode.trim().toLowerCase();
		if (staffCode.toUpperCase().equals(currentUser.getStaffRoot().getStaffCode().trim().toUpperCase())) {
			return true;
		}
		BasicFilter<BasicVO> filter = new BasicFilter<BasicVO>();
		if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
			staffCode = staffCode.trim().toLowerCase();
			for (StaffToken staffTk : currentUser.getListUser()) {
				if (staffCode.equals(staffTk.getStaffCode().trim().toLowerCase())){
					return true;
				}
			}
			return false;
		}
		filter.setFlagCMS(false);
		filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
		filter.setShopRootId(currentUser.getShopRoot().getShopId());
		filter.setCode(staffCode);
		try {
			return commonMgr.checkStaffInCMSByFilterWithCMS(filter);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return false;
	}

	/**
	 * Display number.
	 *
	 * @param number
	 *            the number
	 * @return the string
	 * @author phut
	 * @since Aug 31, 2012
	 */
	public String displayNumberVAT(Object number) {
		String result = "";
		if (number instanceof Short) {
			Short __number = (Short) number;
			String _number = __number.shortValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "." + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Integer) {
			Integer __number = (Integer) number;
			String _number = __number.intValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "." + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Long) {
			Long __number = (Long) number;
			String _number = __number.longValue() + "";
			int isDot = 1;
			for (int i = _number.length(); i > 0; i--) {
				char ch = _number.charAt(i - 1);
				if (isDot == 3 && i != 1) {
					if (_number.charAt(i - 2) == '-') {
						result = ch + result;
						isDot = 0;
					} else {
						result = "." + ch + result;
						isDot = 0;
					}
				} else {
					result = ch + result;
				}
				isDot++;
			}
		} else if (number instanceof Double) {
			Double __number = (Double) number;
			DecimalFormat formater = new DecimalFormat("#0.00");
			result = formater.format(__number);
		} else if (number instanceof Float) {
			Float __number = (Float) number;
			DecimalFormat formater = new DecimalFormat("#0.00");
			result = formater.format(__number);
		} else if (number instanceof BigDecimal) {
			BigDecimal __number = (BigDecimal) number;
			result = convertMoneyVAT(__number);
		}
		return result;
	}

	/**
	 * Get quantity formatter.
	 *
	 * @param quantity
	 * @param convfact
	 * @author khanhnl
	 * @since Aug 30, 2012
	 */
	public String cellQuantityFormatter(Integer quantity, Integer i_convfact) {
		Integer bigUnit = 0;
		Integer smallUnit = 0;
		Integer convfact = 1;
		String cellQty = "";
		if (i_convfact != null && i_convfact > 0) {
			convfact = i_convfact;
		}
		bigUnit = quantity / convfact;
		smallUnit = quantity % convfact;
		cellQty = bigUnit.toString() + '/' + smallUnit.toString();
		return cellQty;
	}

	/**
	 * Gets the quantity.
	 *
	 * @param amount
	 * @param convfact
	 * @author khanhnl
	 * @since Sep 04, 2012
	 */
	public static Integer getQuantity(String amount, Integer convfact, String option) {
		if (convfact == null || convfact <= 0) {
			convfact = 1;
		}
		int bigUnit = 0;
		int smallUnit = 0;
		if (!amount.contains("/")) {
			smallUnit = Integer.parseInt(amount);
		} else {
			String[] arrCount = amount.split("/");
			if (arrCount.length > 0) {
				if (arrCount[0].trim().length() == 0) {
					bigUnit = 0;
				} else {
					bigUnit = Integer.parseInt(arrCount[0].trim());
				}
				if (arrCount.length > 1) {
					if (arrCount[1].trim().length() == 0) {
						smallUnit = 0;
					} else {
						smallUnit = Integer.parseInt(arrCount[1].trim());
						if (smallUnit >= convfact) {
							int addBig = smallUnit / convfact;
							int mod = smallUnit % convfact;
							bigUnit = bigUnit + addBig;
							smallUnit = mod;
						}
					}
				}
			}
		}
		if (option.equalsIgnoreCase("Quantity")) {
			return bigUnit * convfact + smallUnit;
		} else if (option.equalsIgnoreCase("Package")) {
			return bigUnit;
		} else if (option.equalsIgnoreCase("Unit")) {
			return smallUnit;
		}
		return 0;
	}

	public String convertConvfact(Integer quality, Integer convfact) {
		if (convfact == null || convfact <= 0) {
			convfact = 1;
		}
		return StringUtil.convertConvfact(quality, convfact);
	}

	/**
	 * Check Inherit user priv by User login
	 *
	 * @author hunglm16
	 * */
	public int checkInheritUserPrivByUserLogin() {
		if (currentUser != null && !currentUser.getUserName().trim().equals(currentUser.getStaffRoot().getStaffCode().trim())) {
			return 0;
		}
		return 1;
	}

	/**
	 * Check shopRoot Single
	 *
	 * @author hunglm16
	 * */
	public int checkShopRootSingle() {
		if (currentUser != null && currentUser.getListShopByRole() != null && currentUser.getListShopByRole().size() > 1) {
			return 0;
		}
		return 1;
	}

	/**
	 * Gets the info header.
	 *
	 * @return the info header
	 */
	public String getInfoHeader() {
		StringBuilder sb = new StringBuilder();
		if (request.getSession().getAttribute(ConstantManager.SESSION_HEADER) != null) {
			return String.valueOf(request.getSession().getAttribute(ConstantManager.SESSION_HEADER));
		} else if (currentUser != null) {
			StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (staffMgr != null && shopMgr != null) {
				sb.append("  ");
				sb.append(currentUser.getUserName());
				if (currentUser.getStaffRoot() != null) {
					sb.append(" - ");
					sb.append(currentUser.getStaffRoot().getStaffName());
				}

				try {
					staff = staffMgr.getStaffByCode(currentUser.getUserName());
					if (staff != null && currentUser.getShopRoot() != null) {
//						sb.append("  ");
						ShopToken chooseShop = currentUser.getShopRoot();
//						if (staff.getStaffType().getObjectType().equals(StaffObjectType.NVGS.getValue())) {
//							//(Shop)request.getSession().getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
//							if (chooseShop == null) {
//								sb.append(staff.getShop().getShopName());
//							} else {
//								sb.append(chooseShop.getShopName());
//							}
//						} else {
//							sb.append(currentUser.getShopRoot().getShopName());
//						}
//						sb.append(",");
						Shop sh = shopMgr.getShopById(chooseShop.getShopId());
						request.getSession().setAttribute(ConstantManager.SESSION_SHOP_ID, chooseShop.getShopId());
						if (sh != null) {
							request.getSession().setAttribute(ConstantManager.SESSION_SHOP, sh);
						}
					}
//					if (staff != null) {
//						sb.append(" ");
//						sb.append(staff.getStaffCode());
//						sb.append(" - ");
//						sb.append(staff.getStaffName());
//						request.getSession().setAttribute(ConstantManager.SESSION_HEADER, sb.toString());
//					}
				} catch (BusinessException e) {
					LogUtility.logError(e, e.getMessage());
				}

			}
		}
		return sb.toString();
	}

	/***
	 * Lay hinh anh avatar cua CurrentUser, neu khong co lay default
	 * @author vuongmq
	 * @return String
	 * @throws BusinessException
	 * @since 24/08/2015
	 */
	public String getImageCurrentUser() {
		/*if (currentUser != null) {
			try {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (staff != null && staff.getAvatarUrl() != null && StringUtil.isNullOrEmpty(staff.getAvatarUrl())) {
					return Configuration.getImgTabletPortalServerPath() + staff.getAvatarUrl();
				}
			} catch (BusinessException e) {
				LogUtility.logError(e, e.getMessage());
			}
		}*/
		return "/resources/images/default_user.jpg";
	}

	/**
	 * Lay doi tuong Staff theo phan quyen CMS
	 *
	 * @author hunglm16
	 * @since January 08,2014
	 * */
	public Staff getStaffByCurrentUser() {
		if (currentUser != null) {
			StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
			try {
				staff = staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId());
			} catch (BusinessException e) {
//				LogUtility.logError(e, e.getMessage());
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.AbstractAction.getStaffByCurrentUser()"), createLogErrorStandard(actionStartTime));
			}
		}
		return staff;
	}

	/**
	 * Kiem tra quyen.
	 *
	 * @return true, if successful
	 * @author tungmt
	 * @since Sep 27, 2012
	 */
	public boolean checkRoles(VSARole... expertRole) {
		List<RoleToken> listRoles = currentUser.getListRole();
		boolean isPermission = false;
		for (RoleToken rToken : listRoles) {
			if (expertRole != null) {
				for (VSARole vsaRole : expertRole) {
					if (rToken.getRoleCode().equals(vsaRole.getValue())) {
						isPermission = true;
						break;
					}
				}
			}
		}
		return isPermission;
	}

	/**
	 * Display positive number.
	 *
	 * @param number
	 *            the number
	 * @return the object
	 * @author phut
	 * @since Oct 3, 2012
	 */
	public Object displayPositiveNumber(Object number) {
		if (number instanceof Integer) {
			int _number = (Integer) number;
			return _number < 0 ? 0 - _number : _number;
		}

		if (number instanceof Long) {
			long _number = (Long) number;
			return _number < 0 ? 0 - _number : _number;
		}

		if (number instanceof Float) {
			float _number = (Float) number;
			return _number < 0 ? 0 - _number : _number;
		}

		if (number instanceof Double) {
			double _number = (Double) number;
			return _number < 0 ? 0 - _number : _number;
		}

		if (number instanceof BigDecimal) {
			BigDecimal _number = (BigDecimal) number;
			return _number.compareTo(new BigDecimal(0)) < 0 ? new BigDecimal(0).subtract(_number) : _number;
		}
		return null;
	}

	public static String convertFloatNumberOnly(Object number) {
		if (number instanceof Float) {
			float _number = (Float) number;
			String __number = _number + "";
			Pattern p = Pattern.compile("[0-9]*.0*");
			Matcher m = p.matcher(__number);
			if (m.matches()) {
				int index = __number.indexOf(".");
				if (index != -1) {
					return __number.substring(0, index);
				} else {
					return __number;
				}
			} else {
				return __number;
			}
		}
		if (number instanceof Double) {
			double _number = (Double) number;
			String __number = _number + "";
			Pattern p = Pattern.compile("[0-9]*.0*");
			Matcher m = p.matcher(__number);
			if (m.matches()) {
				int index = __number.indexOf(".");
				if (index != -1) {
					return __number.substring(0, index);
				} else {
					return __number;
				}
			} else {
				return __number;
			}
		}

		return null;
	}

	public static String codeNameDisplay(String code, String name) {
		return StringUtil.CodeAddName(code, name);
	}

	public static String codeNameDisplayEx(String code, String name) {
		return StringUtil.CodeAddNameEx(code, name);
	}

	public List<List<String>> getExcelData(File excelFile, String excelFileContentType, String errMsgExl, Integer limitColumn) {
		final int startReadRowIndex = 1;
		return getExcelData(excelFile, excelFileContentType, errMsgExl, limitColumn, startReadRowIndex);
	}

	/**
	 * Doc du lieu trong file excel
	 * @author tuannd20
	 * @param excelFile file excel
	 * @param excelFileContentType contentType cua file dang doc
	 * @param errMsgExl
	 * @param limitColumn so column can doc du lieu
	 * @param startReadRowIndex vi tri dong bat dau doc (bat dau tu 0)
	 * @return du lieu trong file excel
	 * @since 10/09/2015
	 */
	protected List<List<String>> getExcelData(File excelFile, String excelFileContentType, String errMsgExl, Integer limitColumn, Integer startReadRowIndex) {
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		if (StringUtil.isNullOrEmpty(errMsg)) {
			if (startReadRowIndex < 0) {
				startReadRowIndex = 0;
			}
			List<List<String>> excelDatas = getExcelDataWithHeader(excelFile, excelFileContentType, errMsgExl, limitColumn);
			if (excelDatas != null) {
				for (int i = 0, size = Math.min(startReadRowIndex, excelDatas.size()); i < size; i++) {
					excelDatas.remove(0);
				}
			}
			return excelDatas;
		}
		return null;
	}

	public List<List<String>> getExcelDataEx(File excelFile, String excelFileContentType, String errMsgExl, Integer limitColumn) {
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		if (StringUtil.isNullOrEmpty(errMsg)) {
			return FileUtility.getExcelDataEx(excelFile, limitColumn, false);
		}
		return null;
	}

	public List<List<String>> getExcelDataForSOImport(File excelFile, String excelFileContentType, String errMsgExl, Integer limitColumn) {
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		if (StringUtil.isNullOrEmpty(errMsg)) {
			return FileUtility.getExcelDataForSOImport(excelFile, limitColumn, false);
		}
		return null;
	}

	public List<List<String>> getExcelDataNew(File excelFile, String excelFileContentType, String errMsgExl, Integer limitColumn) {
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		if (StringUtil.isNullOrEmpty(errMsg)) {
			return FileUtility.getExcelDataNew(excelFile, limitColumn, false, 1, 3);
		}
		return null;
	}

	public List<List<String>> getExcelDataWithHeader(File excelFile, String excelFileContentType, String errMsgExl, Integer limitColumn) {
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		if (StringUtil.isNullOrEmpty(errMsg)) {
			return FileUtility.getExcelData(excelFile, limitColumn, true);
		}
		return null;
	}

	public List<List<String>> getExcelDataAndEmptyRow(File excelFile, String excelFileContentType, String errMsgExl, Integer limitColumn) {
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		if (StringUtil.isNullOrEmpty(errMsg)) {
			return FileUtility.getExcelDataAndEmptyRow(excelFile, limitColumn);
		}
		return null;
	}

	public Integer separationLotAuto(String code) {
		try {
			ApParamMgr apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
			if (apParamMgr == null) {
				return -1;
			}
			ApParam apParam = null;
			if (ConstantManager.ISSUED_STOCK_AP_PARAM_CODE.equals(code) || ConstantManager.RECEIVED_STOCK_AP_PARAM_CODE.equals(code)) {
				apParam = apParamMgr.getApParamByCode(code, ApParamType.SALE_ORDER_APPROVED);
			} else {
				apParam = apParamMgr.getApParamByCodeEx(code, ActiveType.RUNNING);
			}
			if (apParam == null) {
				return -1;
			}
			return Integer.valueOf(apParam.getValue());
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
		}
		return -1;
	}

	public Integer getTypeDebitCustomer(Date date) {
		return StringUtil.getTypeDebitCustomer(date);
	}

	public Object callFunctionFromText(Object obj, String funcName, Class[] lstArg, Object[] lstValue) {
		try {
			Method objMethod = obj.getClass().getMethod(funcName, lstArg);
			return objMethod.invoke(obj, lstValue);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return null;
	}

	public void loadAttributeData(Object object, TableHasAttribute tableHasAttribute) {
		AttributeMgr attributeMgr = (AttributeMgr) context.getBean("attributeMgr");
		try {
			ObjectVO<Attribute> objAttr = attributeMgr.getListAttribute(null, tableHasAttribute, null, null, null, ActiveType.RUNNING);
			if (objAttr != null) {
				lstAttribute = objAttr.getLstObject();
				if (lstAttribute != null) {
					lstObjectValue = new ArrayList<List<String>>();
					lstObjectAttributeDetail = new ArrayList<List<AttributeDetailVO>>();
					for (int i = 0; i < lstAttribute.size(); i++) {
						if (AttributeColumnType.CHOICE.equals(lstAttribute.get(i).getValueType()) || AttributeColumnType.MULTI_CHOICE.equals(lstAttribute.get(i).getValueType())) {
							ObjectVO<AttributeDetailVO> objAttrDetail = attributeMgr.getListAttributeDetailByAttributeId(null, lstAttribute.get(i).getId(), ActiveType.RUNNING);
							lstAttributeDetail = objAttrDetail.getLstObject();//danh sach gia tri cua <option> tag
							lstObjectAttributeDetail.add(lstAttributeDetail);
							if (object != null) {
								Method objMethod = object.getClass().getMethod("getId");
								Object val = objMethod.invoke(object);
								if (val instanceof Long) {
									Long __val = (Long) val;
									List<AttributeValueDetail> lstAttrValueDetail = attributeMgr.getListValue4Select(__val, lstAttribute.get(i).getId(), ActiveType.RUNNING);
									List<String> __lstObjValue = new ArrayList<String>();
									for (int a = 0; a < lstAttributeDetail.size(); a++) {
										for (int b = 0; b < lstAttrValueDetail.size(); b++) {
											if (lstAttributeDetail.get(a).getId().equals(lstAttrValueDetail.get(b).getAttributeDetail().getId())) {
												__lstObjValue.add(lstAttrValueDetail.get(b).getAttributeDetail().getId().toString());
											}
										}
									}
									lstObjectValue.add(__lstObjValue);
								}
							} else {
								List<String> emptyValue = new ArrayList<String>();
								emptyValue.add("-1");
								lstObjectValue.add(emptyValue);
							}
						} else {
							if (object != null) {
								Method objMethod = object.getClass().getMethod("getId");
								Object val = objMethod.invoke(object);
								if (val instanceof Long) {
									Long __val = (Long) val;
									AttributeValue attrValue = attributeMgr.getValueOfObject(__val, lstAttribute.get(i).getId());
									List<String> __lstObjValue = new ArrayList<String>();
									if (attrValue == null) {
										__lstObjValue.add("");
									} else {
										if (AttributeColumnType.DATE_TIME.equals(lstAttribute.get(i).getValueType())) {
											__lstObjValue.add(DateUtil.convertFormatStrFromAtt(attrValue.getValue()));
										} else
											__lstObjValue.add(attrValue.getValue());
									}
									lstObjectValue.add(__lstObjValue);
								}
							} else {
								List<String> emptyValue = new ArrayList<String>();
								emptyValue.add("");
								lstObjectValue.add(emptyValue);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	public void exportExcelDataNotSession(List<CellBean> lstData, String tempFileName) {
		try {
			if (lstData != null && lstData.size() > 0) {
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + tempFileName;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + tempFileName;
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("report", lstData);
				InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
				inputStream.close();
				OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				os.close();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(LIST, outputPath);
				result.put(REPORT_PATH, outputPath);
				result.put("hasData", true);
				MemcachedUtils.putValueToMemcached(retrieveReportToken(reportCode), outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put("hasData", false);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	public String convertStartWeek(int startWeek) {
		int year = DateUtil.getYear(DateUtil.now());
		return DateUtil.getFirstDayOfWeek(startWeek, year);
	}

	public void clearDataReportSession() {
		session.setAttribute(ConstantManager.SESSION_REPORT_DATA, null);
		session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, null);
		session.setAttribute(ConstantManager.SESSION_REPORT_NUM_ROW, null);
		session.setAttribute(ConstantManager.SESSION_REPORT_TOTAL_ROW, null);
	}

	public String endline(Integer isView) {
		if (isView == 0) {
			return "\n";
		}
		return "</br>";
	}

	public void updateTotalRowGrid() {
		result.put("page", page);
		if (result.get("total") == null)
			result.put("total", 0);
		if (result.get("rows") == null)
			result.put("rows", rows);
	}

	public void returnError(String errMsg) {
		result.put(ERROR, true);
		result.put("errMsg", errMsg);
		updateTotalRowGrid();
	}

	public Integer getQuantity(String amount, Integer convfact) {
		if (StringUtil.isNullOrEmpty(amount)) {
			return 0;
		}
		if (convfact == null || (convfact != null && convfact == 0)) {
			convfact = 1;
		}
		Integer bigUnit = 0;
		Integer smallUnit = 0;
		amount = new String(amount);
		if (amount.indexOf('/') == -1) {
			smallUnit = Integer.valueOf(amount);
		} else {
			String[] arrCount = amount.split("/");
			if (arrCount.length < 2) {
				return null;
			}
			if (arrCount.length > 0) {
				if (arrCount[0].trim().length() == 0) {
					bigUnit = 0;
				} else {
					bigUnit = Integer.valueOf(arrCount[0].trim());
				}
				if (arrCount[1].trim().length() == 0) {
					smallUnit = 0;
				} else {
					smallUnit = Integer.valueOf(arrCount[1].trim());
					if (smallUnit >= convfact) {
						Integer addBig = (smallUnit / convfact) * 10;
						Double mod = smallUnit.doubleValue() % convfact;
						bigUnit = bigUnit + addBig;
						smallUnit = mod.intValue();
					}
				}
			}
		}
		return bigUnit * convfact + smallUnit;
	}

	public List<ControlToken> getControls(List<ControlToken> lst, CmsVO vo) {
		if (vo.getControlId() == null || vo.getControlId() == 0) {
			return lst;
		}
		boolean flag = true;
		if (lst == null) {
			lst = new ArrayList<ControlToken>();
		}
		if (lst.size() > 0) {
			for (ControlToken ctk : lst) {
				if (ctk.getControlId().equals(vo.getControlId())) {
					flag = false;
					break;
				}
			}
		}
		if (flag) {
			ControlToken ctk = new ControlToken();
			ctk.setControlId(vo.getControlId());
			ctk.setControlCode(vo.getControlCode());
			ctk.setControlName(vo.getControlName());
			ctk.setStatus(vo.getStatus());
			ctk.setFullPrivilege(vo.getFullPrivilege());
			lst.add(ctk);
		}
		return lst;
	}

	/**
	 * Bo sung con chau cho Form
	 *
	 * @author hunglm16
	 * @since October 7,2014
	 * */
	public List<FormToken> getListChildForm(FormToken ft, LinkedHashMap<Long, FormToken> forms) {
		ArrayList<FormToken> lst = new ArrayList<FormToken>();
		for (Entry<Long, FormToken> entry : forms.entrySet()) {
			FormToken form = entry.getValue();
			if (form.getParentFormId() != null && form.getParentFormId().equals(ft.getFormId())) {
				form.setListForm(this.getListChildForm(form, forms));
				lst.add(form);
			}
		}
		return lst;
	}

	/**
	 * @author hunglm16
	 * @since AUGUST 13,2014
	 * @description Kiem tra con chau trong cay don vi CMS
	 * */
	public boolean checkChildrenInTreeShopCms(ShopVOToken root, CmsVO shopVo) {
		if (root != null) {
			List<ShopVOToken> children = root.getChildren();
			if (!children.isEmpty()) {
				for (ShopVOToken node : children) {
					if (node.getShopId().equals(shopVo.getShopId())) {
						return false;
					} else {
						this.checkChildrenInTreeShopCms(node, shopVo);
					}
				}
			}
		}
		return true;

	}

	/**
	 * @author hunglm16
	 * @since AUGUST 13,2014
	 * @description Kiem tra va them danh sach nhan vien neu co
	 * */
	public List<StaffToken> getListStaffTocken(List<CmsVO> lstStaff, CmsVO shopVo) {
		List<StaffToken> lstData = new ArrayList<StaffToken>();
		if (lstStaff != null && !lstStaff.isEmpty()) {
			for (CmsVO staffVo : lstStaff) {
				if (staffVo.getShopId().equals(shopVo.getShopId())) {
					StaffToken staffTk = new StaffToken();
					staffTk.setStaffId(staffVo.getStaffId());
					staffTk.setStaffCode(staffVo.getStaffCode());
					staffTk.setShopId(staffVo.getShopId());
					lstData.add(staffTk);
				}
			}
		}
		return lstData;
	}

	/**
	 * @author hunglm16
	 * @since AUGUST 13,2014
	 * @description Kiem tra va them danh sach con chau neu co
	 * */
	public List<ShopVOToken> getListShopChildren(List<CmsVO> lstShop, CmsVO root) {
		List<ShopVOToken> lstData = new ArrayList<ShopVOToken>();
		for (CmsVO shopVo : lstShop) {
			if (shopVo.getParentId().equals(root.getShopId())) {
				ShopVOToken children = new ShopVOToken();
				children.setShopId(shopVo.getShopId());
				children.setShopCode(shopVo.getShopCode());
				children.setParentId(root.getShopId());
				lstData.add(children);
			}
		}
		return lstData;
	}

	/**
	 * convert list CmsVO thanh list formtoken
	 *
	 * @author hunglm16
	 * @since October 7,2014
	 * @param lst
	 * @return
	 */
	public List<FormToken> getListFormToken(List<CmsVO> lst) {
		LinkedHashMap<Long, FormToken> forms = new LinkedHashMap<Long, FormToken>();
		//set control vao tung form(chua phan cap theo form cha con)
		List<Long> lstFormId = new ArrayList<Long>();
		for (CmsVO vo : lst) {
			FormToken form = forms.get(vo.getFormId());
			if (form != null) {
				form.setListControl(this.getControls(form.getListControl(), vo));
			} else {
				form = new FormToken();
				form.setListControl(this.getControls(null, vo));
				form.setFormId(vo.getFormId());
				form.setFormCode(vo.getFormCode());
				form.setFormName(vo.getFormName());
				form.setUrl(vo.getUrl());
				form.setParentFormId(vo.getParentFormId());
				form.setFullPrivilege(vo.getFullPrivilege());
				form.setFormType(vo.getFormType());
				form.setSpecific(vo.getSpecific());
			}
			forms.put(form.getFormId(), form);
			lstFormId.add(form.getFormId());
		}
		//phan cap form theo cha con
		List<FormToken> lstForm = new ArrayList<FormToken>();
		for (Entry<Long, FormToken> entry : forms.entrySet()) {
			FormToken form = entry.getValue();
			if (form.getParentFormId() != null && (form.getParentFormId() == 0 || lstFormId.indexOf(form.getParentFormId()) == -1)) {
				form.setListForm(this.getListChildForm(form, forms));
				lstForm.add(form);
			}
		}
		return lstForm;
	}

	/**
	 * convert list CmsVO thanh list formtoken
	 *
	 * @author hunglm16
	 * @since October 7,2014
	 * @param lst
	 * @return
	 */
	public List<FormToken> getListFormMenu(List<CmsVO> lst) {
		LinkedHashMap<Long, FormToken> forms = new LinkedHashMap<Long, FormToken>();
		//set control vao tung form(chua phan cap theo form cha con)
		List<Long> lstFormId = new ArrayList<Long>();
		for (CmsVO vo : lst) {
			FormToken form = forms.get(vo.getFormId());
			if (form != null && !FormTypeCMS.SUBCOMPONENT.getValue().equals(form.getFormType())) {
				form.setListControl(this.getControls(form.getListControl(), vo));
			} else {
				form = new FormToken();
				form.setListControl(this.getControls(null, vo));
				form.setFormId(vo.getFormId());
				form.setFormCode(vo.getFormCode());
				form.setFormName(vo.getFormName());
				form.setUrl(vo.getUrl());
				form.setParentFormId(vo.getParentFormId());
				form.setFullPrivilege(vo.getFullPrivilege());
				form.setFormType(vo.getFormType());
				form.setSpecific(vo.getSpecific());
			}
			if (!FormTypeCMS.SUBCOMPONENT.getValue().equals(form.getFormType())) {
				forms.put(form.getFormId(), form);
				lstFormId.add(form.getFormId());
			}
		}
		//phan cap form theo cha con
		List<FormToken> lstForm = new ArrayList<FormToken>();
		for (Entry<Long, FormToken> entry : forms.entrySet()) {
			FormToken form = entry.getValue();
			if (form.getParentFormId() != null && (form.getParentFormId() == 0 || lstFormId.indexOf(form.getParentFormId()) == -1)) {
				form.setListForm(this.getListChildForm(form, forms));
				lstForm.add(form);
			}
		}
		return lstForm;
	}

	/**
	 * Tach CmsVO thanh Role tocken
	 *
	 * @author hunglm16
	 * */
	public List<RoleToken> getListRoleToken(List<RoleVO> lst) {
		HashMap<Long, RoleToken> roles = new HashMap<Long, RoleToken>();
		for (RoleVO vo : lst) {
			if (vo.getRoleId() != null && vo.getRoleId() != 0 && roles.get(vo.getRoleId()) == null) {
				RoleToken roleToken = new RoleToken();
				roleToken.setRoleCode(vo.getRoleCode());
				roleToken.setRoleName(vo.getRoleName());
				roleToken.setRoleId(vo.getRoleId());
				roleToken.setInheritUserPriv(vo.getInheritUserId());
				roles.put(vo.getRoleId(), roleToken);
			}
		}
		//set list role
		List<RoleToken> lstRole = new ArrayList<RoleToken>();
		for (Entry<Long, RoleToken> entry : roles.entrySet()) {
			lstRole.add(entry.getValue());
		}
		return lstRole;
	}
	public List<RoleToken> getListRoleTokenOLD(List<CmsVO> lst) {
		HashMap<Long, RoleToken> roles = new HashMap<Long, RoleToken>();
		for (CmsVO vo : lst) {
			if (vo.getRoleId() != null && vo.getRoleId() != 0 && roles.get(vo.getRoleId()) == null) {
				RoleToken roleToken = new RoleToken();
				roleToken.setRoleCode(vo.getRoleCode());
				roleToken.setRoleName(vo.getRoleName());
				roleToken.setRoleId(vo.getRoleId());
				roleToken.setInheritUserPriv(vo.getInheritUserPriv());
				roles.put(vo.getRoleId(), roleToken);
			}
		}
		//set list role
		List<RoleToken> lstRole = new ArrayList<RoleToken>();
		for (Entry<Long, RoleToken> entry : roles.entrySet()) {
			lstRole.add(entry.getValue());
		}
		return lstRole;
	}

	public void setSessionLogin() {
		try {
			currentUser = this.getCurrentUserSession();
			if (currentUser != null) {
				String appCode = Configuration.getDomainCode();

				currentUser.setAppCode(appCode);
				CmsVO appVO = cmsMgr.getAppTockenVOByCode(appCode);
				AppToken appToken = new AppToken();
				if (appVO != null) {
					appToken.setAppId(appVO.getAppId());
					appToken.setAppCode(appVO.getAppCode());
					appToken.setAppName(appVO.getAppName());
				}
				//Set danh sach Vai tro voi User tuong ung
				currentUser.setAppToken(appToken);
//				CmsFilter filter = new CmsFilter();
//				filter.setUserId(currentUser.getUserId());
//				filter.setAppCode(appCode);
//				filter.setAppId(appToken.getAppId());
//				List<CmsVO> lstRole = cmsMgr.getListRoleByUser(filter);
				List<RoleVO> lstRole = cmsMgr.getListOwnerRoleShopVOByStaff(currentUser.getUserId());

				if (lstRole != null && lstRole.size() > 0) {
					currentUser.setListRole(this.getListRoleToken(lstRole));
					Map<Long, RoleVO> mapRole = new HashMap<Long, RoleVO>();
					for (RoleVO vo : lstRole) {
						List<ShopVO> listShop = vo.getListShop();
						if (listShop == null || listShop.size() == 0) {
							Staff staff = staffMgr.getStaffById(currentUser.getUserId());
							if (staff == null || staff.getShop() == null) {
								return ;
							}
							CmsFilter filter = new CmsFilter();
							filter.setStaffRootId(currentUser.getUserId());
							filter.setShopId(staff.getShop().getId());
							List<ShopVO> listShop1 = cmsMgr.getListShopVOByStaffVO(filter);
							vo.setListShop(listShop1);
						}
						mapRole.put(vo.getRoleId(), vo);
					}
					request.getSession().setAttribute("mapRole", mapRole);
				}
				request.getSession().setAttribute("cmsUserToken", currentUser);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	public UserToken getCurrentUserSession() {
		if (request.getSession().getAttribute("cmsUserToken") != null) {
			return (UserToken) request.getSession().getAttribute("cmsUserToken");
		}
		return null;
	}

	public Long getCurrentRoleId() {
		currentUser = this.getCurrentUserSession();
		if(currentUser != null && currentUser.getRoleToken() != null) {
			return currentUser.getRoleToken().getRoleId();
		}
		return null;
	}

	public List<ShopVO> getListShopByRole(Long roleId) {
		if(roleId == null) {
			roleId = this.getCurrentRoleId();
		}
		Map<Long, RoleVO> map = this.getMapRoleVO();
		if (roleId != null && map.get(roleId) != null) {
			return map.get(roleId).getListShop();
		}
		List<ShopVO> lstShop = new ArrayList<ShopVO>();
		for (Entry<Long, RoleVO> entry : map.entrySet()) {
			RoleVO vo = entry.getValue();
			lstShop.addAll(vo.getListShop());
		}
		return lstShop;
	}

	public Map<Long, ShopVO> getMapShopByRole(Long roleId) {
		if(roleId == null) {
			roleId = this.getCurrentRoleId();
		}
		Map<Long, RoleVO> map = this.getMapRoleVO();
		Map<Long, ShopVO> mapShop = new HashMap<Long, ShopVO>();
		RoleVO role = map.get(roleId);
		for (ShopVO vo : role.getListShop()) {
			mapShop.put(vo.getShopId(), vo);
		}
		return mapShop;
	}

	public List<Long> getListShopIdByRole(Long roleId) {
		List<ShopVO> lstShop = this.getListShopByRole(roleId);
		List<Long> lstId = new ArrayList<Long>();
		for (ShopVO vo : lstShop) {
			lstId.add(vo.getId());
		}
		return lstId;
	}

	public List<Long> getListShopChildId() {
		Map<Long, Long> map = this.getMapShopChild();
		List<Long> lstId = new ArrayList<Long>();
		for (Entry<Long, Long> entry : map.entrySet()) {
			lstId.add(entry.getValue());
		}
		return lstId;
	}

	public String getStrListShopId() {
		List<Long> lstShop = this.getListShopChildId();
		String str = "-1";
		for (Long id : lstShop) {
			str += "," + id.toString();
		}
		return str;
	}

	public HashMap<Long, RoleVO> getMapRoleVO() {
		if(request.getSession().getAttribute("mapRole") != null) {
			HashMap<Long, RoleVO> attribute2 = (HashMap<Long, RoleVO>) request.getSession().getAttribute("mapRole");
			HashMap<Long, RoleVO> attribute = attribute2;
			return attribute;
		}
		return new HashMap<Long, RoleVO>();
	}

	public HashMap<Long, Long> getMapUserId() {
		if(request.getSession().getAttribute("mapUser") != null) {
			return (HashMap<Long, Long>) request.getSession().getAttribute("mapUser");
		}
		return new HashMap<Long, Long>();
	}

	public Map<Long, Long> getMapShopChild() {
		if (request.getSession().getAttribute("mapShop") != null) {
			return (HashMap<Long, Long>) request.getSession().getAttribute("mapShop");
		}
		return new HashMap<Long, Long>();
	}

	public void setUserId2Session(Long staffId) {
		Map<Long, Long> map = this.getMapUserId();
		map.put(staffId, staffId);
		request.getSession().setAttribute("mapUser", map);
	}

	public void setShopId2Session(Long shopId) {
		Map<Long, Long> map = this.getMapShopChild();
		map.put(shopId, shopId);
		request.getSession().setAttribute("mapShop", map);
	}

	public void setSessionUserInherit() {
		if (request.getSession().getAttribute("cmsUserToken") != null) {
			try{
				currentUser = (UserToken) request.getSession().getAttribute("cmsUserToken");
				if(currentUser.getStaffRoot() == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
					return;
				}
				CmsFilter filter = new CmsFilter();
				filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
				filter.setShopRootId(currentUser.getShopRoot().getShopId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				List<CmsVO> lstStaff = cmsMgr.getListChildStaffInherit(filter);
				if (lstStaff != null && lstStaff.size() > 0) {
					Map<Long, Long> map = new HashMap<Long, Long>();
					for (CmsVO vo : lstStaff) {
						map.put(vo.getStaffId(), vo.getStaffId());
					}
					request.getSession().setAttribute("mapUser", map);
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
	}

	public void setSessionShopInherit() {
		if (request.getSession().getAttribute("cmsUserToken") != null) {
			try{
				currentUser = (UserToken) request.getSession().getAttribute("cmsUserToken");
				if(currentUser.getStaffRoot() == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
					return;
				}
				CmsFilter filter = new CmsFilter();
				filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
				filter.setShopRootId(currentUser.getShopRoot().getShopId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				List<CmsVO> lstShop = cmsMgr.getListChildShopInherit(filter);
				if (lstShop != null && lstShop.size() > 0) {
					Map<Long, Long> map = new HashMap<Long, Long>();
					for (CmsVO vo : lstShop) {
						map.put(vo.getShopId(), vo.getShopId());
					}
					request.getSession().setAttribute("mapShop", map);
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
	}

	public List<Long> getListUserInherit(){
		List<Long> lstId = new ArrayList<Long>();
		Map<Long, Long> map = this.getMapUserId();
		for (Entry<Long, Long> entry : map.entrySet()) {
			lstId.add(entry.getValue());
		}
		return lstId;
	}

	public String getStrListUserId() {
		List<Long> lstId = this.getListUserInherit();
		String str = "-1";
		for (Long id : lstId) {
			str += "," + id.toString();
		}
		return str;
	}


	/**
	 * Cap nhat Session khi chon vai tro va shop mac dinh
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @description Set Session Sau khi chon Vai tro sau khi dang nhap
	 * */
	public void setSessionNextRole(RoleToken roleToken, ShopToken shopToken) {
		try {
			Long staffRootId = -1l;
			if (currentUser != null) {
				currentUser = (UserToken) request.getSession().getAttribute("cmsUserToken");
				currentUser.setRoleToken(roleToken);
				//Gan quyen StaffRoot
				if (roleToken.getInheritUserPriv() != null) {
					staffRootId = roleToken.getInheritUserPriv();
				} else {
					staffRootId = currentUser.getUserId();
				}
				Staff staffRvo = staffMgr.getStaffById(staffRootId);
				if (staffRvo == null) {
					return;
				}
				StaffToken staff = new StaffToken();
				staff.setIsLevel(1);
				staff.setShopId(staffRvo.getShop().getId());
				staff.setStaffId(staffRvo.getId());
				staff.setStaffCode(staffRvo.getStaffCode());
				staff.setStaffName(staffRvo.getStaffName());
				staff.setObjectType(staffRvo.getStaffType().getSpecificType() != null ? staffRvo.getStaffType().getSpecificType().getValue() : null);
				currentUser.setStaffRoot(staff);
				//Get ListShopInRole
				CmsFilter filter = null;
				List<ShopToken> lstShopToken = null;
				if (currentUser.getListShopByRole() == null || currentUser.getListShopByRole().size() == 0) {
					List<ShopVO> lstShop = getListShopByRole(roleToken.getRoleId());
					lstShopToken = new ArrayList<ShopToken>();
					if (lstShop == null || lstShop.isEmpty() || lstShop.size() == 0) {
						lstShop = new ArrayList<ShopVO>();
						Staff staffCurrent= null;
						if (currentUser.getStaffRoot() != null) {
							staffCurrent = staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId());
						} else {
							staffCurrent = staffMgr.getStaffById(currentUser.getUserId());
						}
						ShopVO shopVo = new ShopVO();
						if (staffCurrent != null) {
							shopVo.setShopId(staffCurrent.getShop().getId());
							shopVo.setShopCode(staffCurrent.getShop().getShopCode());
							shopVo.setShopName(staffCurrent.getShop().getShopName());
							if (staffCurrent.getShop().getParentShop() != null) {
								shopVo.setParentId(staffCurrent.getShop().getParentShop().getId());
							}
							//shopVo.setObjectType(staffCurrent.getShop().getType().getObjectType());
							Integer isLevel = commonMgr.getIslevel(staffCurrent.getShop().getId());
							shopVo.setIsLevel(isLevel);
						}
						lstShop.add(shopVo);
					}
					for (ShopVO vo : lstShop) {
						ShopToken shopTK = new ShopToken();
						shopTK.setShopId(vo.getShopId());
						shopTK.setShopCode(vo.getShopCode());
						shopTK.setShopName(vo.getShopName());
						shopTK.setParentId(vo.getParentId());
						shopTK.setObjectType(vo.getObjectType());
						shopTK.setIsLevel(vo.getObjectType());
						shopTK.setShopChannel(vo.getShopChannel());
						lstShopToken.add(shopTK);
					}
					currentUser.setListShopByRole(lstShopToken);
					lstShopToken = null;
				}
				lstShopToken = null;
				//Gan cac gia tri mac dinh dinh kem
				currentUser.setShopRoot(shopToken);
				if (shopToken != null) {
					dayLock = shopLockMgr.getApplicationDate(shopToken.getShopId());
					lockDate = DateUtil.toDateSimpleFormatString(dayLock);
				}
				//Them dong du lieu man hinh va Url vao Session
				filter = new CmsFilter();
				filter.setRoleId(roleToken.getRoleId());
				filter.setAppId(currentUser.getAppToken().getAppId());
				filter.setUserId(currentUser.getUserId());
				List<CmsVO> lst = cmsMgr.getListFormControlByRole(filter);
				if (lst != null && !lst.isEmpty()) {
					List<String> urls = new ArrayList<String>();
					for (CmsVO formVO : lst) {
						if (!StringUtil.isNullOrEmpty(formVO.getUrl())) {
							urls.add(formVO.getUrl().trim());
						}
					}
					currentUser.setListUrl(urls);
					currentUser.setListForm(this.getListFormToken(lst));
					currentUser.setListMenu(this.getListFormMenu(lst));
				}
				//Them dong du lieu man hinh va Url vao Session
				staff = null;
				filter = new CmsFilter();
				filter.setUserId(roleToken.getInheritUserPriv());
				List<StaffVO> lstStaffVO = null;
				try {
					lstStaffVO = cmsMgr.getListStaffByUserWithRole(filter);
				} catch (Exception e) {
					LogUtility.logError(e, e.getMessage());
				}
				List<StaffToken> lstStaff = new ArrayList<StaffToken>();
				if (lstStaffVO != null && lstStaffVO.size() > 0) {
					for (StaffVO vo : lstStaffVO) {
						staff = new StaffToken();
						staff.setIsLevel(vo.getIsLevel());
						staff.setParentStaffId(vo.getParentStaffId());
						staff.setParentStaffMapId(vo.getParentStaffMapId());
						staff.setShopId(vo.getShopId());
						staff.setStaffCode(vo.getStaffCode());
						staff.setStaffId(vo.getStaffId());
						staff.setStaffName(vo.getStaffName());
						staff.setObjectType(vo.getObjectType());
						lstStaff.add(staff);
						if (vo.getStaffId().equals(filter.getUserId())) {
							currentUser.setStaffRoot(staff);
						}
					}
				}
				currentUser.setListUser(lstStaff);
				request.getSession().setAttribute("cmsUserToken", currentUser);

				lstStaffVO = null;
				setSessionUserInherit();
				setSessionShopInherit();

				//Get listShop
				List<ShopVO> lstShopCmsFilter = null;
				filter = new CmsFilter();
				filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
				filter.setShopRootId(currentUser.getShopRoot().getShopId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				lstShopCmsFilter = cmsMgr.getFullShopOwnerByFilter(filter);
				lstShopToken = new ArrayList<ShopToken>();
				if (lstShopCmsFilter != null) {
					for (ShopVO vo : lstShopCmsFilter) {
						ShopToken item = new ShopToken();
						item.setShopId(vo.getShopId());
						item.setShopCode(vo.getShopCode());
						item.setShopName(vo.getShopName());
						item.setParentId(vo.getParentId());
						item.setObjectType(vo.getObjectType());
						item.setIsLevel(vo.getObjectType());
						item.setShopChannel(vo.getShopChannel());
						lstShopToken.add(item);
					}
				}
				currentUser.setListShop(lstShopToken);
				//Get listShopAllByRole
				lstShopCmsFilter = this.getListShopByRole(roleToken.getRoleId());
				lstShopToken = new ArrayList<ShopToken>();
				if (lstShopCmsFilter != null) {
					for (ShopVO vo : lstShopCmsFilter) {
						ShopToken item = new ShopToken();
						item.setShopId(vo.getShopId());
						item.setShopCode(vo.getShopCode());
						item.setShopName(vo.getShopName());
						item.setParentId(vo.getParentId());
						item.setObjectType(vo.getObjectType());
						item.setIsLevel(vo.getIsLevel());
						item.setShopChannel(vo.getShopChannel());
						lstShopToken.add(item);
					}
				}
				currentUser.setListShopAllByRole(lstShopToken);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	/**
	 * Cap nhat Session khi chon vai tro va shop mac dinh
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @description Set Session Sau khi chon Vai tro sau khi dang nhap
	 * */
	public void setSessionChangeShopRoot(ShopToken shopRoot) {
		try {
			if (currentUser != null) {
				currentUser = (UserToken) request.getSession().getAttribute("cmsUserToken");
				CmsFilter filter = new CmsFilter();
				filter.setUserId(currentUser.getUserId());
				List<Long> lstLongId = new ArrayList<Long>();
				lstLongId.add(shopRoot.getShopId());
				filter.setLstId(lstLongId);
				List<ShopVO> lstShopCmsFilter = cmsMgr.getFullShopInOrgAccessByFilter(filter);
				List<ShopToken> lstShopToken = new ArrayList<ShopToken>();
				if (lstShopCmsFilter != null) {
					for (ShopVO vo : lstShopCmsFilter) {
						ShopToken item = new ShopToken();
						item.setShopId(vo.getShopId());
						item.setShopCode(vo.getShopCode());
						item.setShopName(vo.getShopName());
						item.setParentId(vo.getParentId());
						item.setObjectType(vo.getObjectType());
						item.setIsLevel(vo.getIsLevel());
						item.setShopChannel(vo.getIsLevel());
						lstShopToken.add(item);
					}
				}
				currentUser.setListShop(lstShopToken);
				currentUser.setShopRoot(shopRoot);
				request.getSession().setAttribute("cmsUserToken", currentUser);
				lstShopToken = null;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	/**
	 * Khoi tao Session khi mong muon tao 1 bien toan cuc rieng biet trong phien
	 * lam viec
	 *
	 * @author hunglm16
	 * @since October 8,2014
	 * */
	public void setSessionCurrentPage(CurrentPageToken currentPageToken) {
		try {
			request.getSession().setAttribute("currentPageToken", currentPageToken);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	/**
	 * Cap nhat Session khi chon vai tro va shop mac dinh
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @description Set Session Sau khi chon Vai tro sau khi dang nhap
	 * */
	public CurrentPageToken getSessionCurrentPage() {
		return (CurrentPageToken) request.getSession().getAttribute("currentPageToken");
	}

	/**
	 * Lay doi tuong Form
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @description Lay doi tuong Form tu Form va Url co san
	 * */
	public FormToken getFormToken(String url, List<FormToken> lstForm) {
		for (FormToken f : lstForm) {
			if (url.equalsIgnoreCase(f.getUrl())) {
				return f;
			} else if (f.getListForm() != null && f.getListForm().size() > 0) {
				FormToken form = getFormToken(url, f.getListForm());
				if (form != null) {
					return form;
				}
			}
		}
		return null;
	}

	/**
	 * Lay doi tuong Form co trong Session
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @description currentUser da co san trong session
	 * */
	public FormToken getFormToken() {
		if (currentUser != null) {
			if (currentUser.getListForm() != null && currentUser.getListForm().size() > 0 && request.getRequestURI() != null) {
				return getFormToken(request.getRequestURI(), currentUser.getListForm());
			}
		}
		return null;
	}

	/**
	 * Tao mot Map Control
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @description Muc dich dung de phan quyen tren giao dien
	 * */
	public Map<String, Integer> getMapControlToken() {
		FormToken form = getFormToken();
		if (form != null && form.getListControl() != null && form.getListControl().size() > 0) {
			List<ControlToken> listControl = form.getListControl();
			Map<String, Integer> map = new HashMap<String, Integer>();
			for (ControlToken controlToken : listControl) {
				map.put(controlToken.getControlCode(), controlToken.getStatus());
			}
			return map;
		}
		return null;
	}

	/**
	 * Tao mot Map Control
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @description Muc dich dung de phan quyen tren giao dien
	 * */
	public List<Long> getListOrgAccess() {
		List<Long> listOrgAccess = null;
		if (currentUser != null) {
			listOrgAccess = new ArrayList<Long>();
			List<ShopToken> listShop = currentUser.getListShop();
			for (ShopToken shopToken : listShop) {
				listOrgAccess.add(shopToken.getShopId());
			}
		}
		return listOrgAccess;
	}

	/**
	 * Kiem tra Shop thoa man
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @param id
	 * @description Tham so la ShopId, chi su dung ham khi da dang nhap va chon
	 *              don vi mac dinh thanh cong
	 * */
	public Boolean checkShopInOrgAccessById(Long shopId) {
		if (shopId == null) {
			return false;
		}
		Long shopCheck = getMapShopChild().get(shopId);
		if (shopCheck != null && shopCheck > 0) {
			return true;
		}
		return false;
	}


	/**
	 * Kiem tra Shop thoa man
	 *
	 * @author hunglm16
	 * @since September 10,2014
	 * @param id
	 *            , isLevel
	 * @description Tham so la ShopId, chi su dung ham khi da dang nhap va chon don vi mac dinh thanh cong
	 * */
	public Boolean checkShopInOrgAccessByIdWithIsLevel(Long shopId, Integer isLevel) {
		/**
		 * isLevel = 1: VNM isLevel = 2: Kenh isLevel = 3: Vung isLevel = 4:
		 * Mien isLevel = 5: NPP
		 *
		 * oracle function lien quan: decode_islevel_vnm enum java lien quan:
		 * ShopDecentralizationSTT
		 * */
		if (shopId == null) {
			return false;
		}

		for (ShopToken shopTK : currentUser.getListShop()) {
			if (shopId.equals(shopTK.getShopId()) && shopTK.getIsLevel().equals(isLevel)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Lay danh sach Shop thoa man theo Islevel
	 *
	 * @author hunglm16
	 * @since September 10,2014
	 * @param id
	 *            , isLevel
	 * @description Tham so la ShopId, chi su dung ham khi da dang nhap va chon
	 *              don vi mac dinh thanh cong
	 * */
	public List<ShopVO> getLstShopInOrgAccessByIdWithIsLevel(Integer isLevel) {
		/**
		 * isLevel = 1: VNM isLevel = 2: Kenh isLevel = 3: Vung isLevel = 4:
		 * Mien isLevel = 5: NPP
		 *
		 * oracle function lien quan: decode_islevel_vnm enum java lien quan:
		 * ShopDecentralizationSTT
		 * */
		List<ShopVO> lstData = new ArrayList<ShopVO>();
		if (isLevel != null) {
			for (ShopToken shopTK : currentUser.getListShop()) {
				if (shopTK.getIsLevel().equals(isLevel)) {
					ShopVO vo = new ShopVO();
					vo.setIsLevel(shopTK.getIsLevel());
					vo.setObjectType(shopTK.getObjectType());
					vo.setParentId(shopTK.getParentId());
					vo.setShopCode(shopTK.getShopCode());
					vo.setShopId(shopTK.getShopId());
					vo.setShopName(shopTK.getShopName());
					vo.setStatus(ActiveType.RUNNING.getValue());
					lstData.add(vo);
				}
			}
		}
		return lstData;
	}

	/**
	 * Kiem tra Shop voi User dang nhap
	 *
	 * @author hunglm16
	 * @since 11/11/2015
	 * */
	public boolean checkExistShopOfUserLogin() {
		if (currentUser == null || currentUser.getStaffRoot() == null || currentUser.getShopRoot() == null) {
			return false;
		}
		return true;
	}

	/**
	 * Lay Shop thoa man theo Id
	 *
	 * @author hunglm16
	 * @since September 10,2014
	 * @param id
	 *            , isLevel
	 * @description Tham so la ShopId, chi su dung ham khi da dang nhap va chon
	 *              don vi mac dinh thanh cong
	 * */
	public ShopVO getShopInOrgAccessByIdWithIsLevel(Long shopId, Integer isLevel) {
		/**
		 * isLevel = 1: VNM isLevel = 2: Kenh isLevel = 3: Vung isLevel = 4:
		 * Mien isLevel = 5: NPP
		 *
		 * oracle function lien quan: decode_islevel_vnm enum java lien quan:
		 * ShopDecentralizationSTT
		 * */
		if (shopId == null) {
			return null;
		}

		for (ShopToken shopTK : currentUser.getListShop()) {
			if (shopId.equals(shopTK.getShopId()) && shopTK.getIsLevel().equals(isLevel)) {
				ShopVO vo = new ShopVO();
				vo.setIsLevel(shopTK.getIsLevel());
				vo.setObjectType(shopTK.getObjectType());
				vo.setParentId(shopTK.getParentId());
				vo.setShopCode(shopTK.getShopCode());
				vo.setShopId(shopTK.getShopId());
				vo.setShopName(shopTK.getShopName());
				vo.setStatus(ActiveType.RUNNING.getValue());
				return vo;
			}
		}
		return null;
	}

	/**
	 * Lay Shop thoa man theo Id
	 *
	 * @author hunglm16
	 * @since September 10,2014
	 * @param id
	 *            , isLevel
	 * @description Tham so la ShopId, chi su dung ham khi da dang nhap va chon
	 *              don vi mac dinh thanh cong
	 * */
	public ShopToken getShopTockenInOrgAccessById(Long shopId) {
		if (shopId == null) {
			return null;
		}
		for (ShopToken shopTK : currentUser.getListShop()) {
			if (shopId.equals(shopTK.getShopId())) {
				return shopTK;
			}
		}
		return null;
	}

	/**
	 * Lay Shop thoa man theo Id
	 *
	 * @author hunglm16
	 * @since March 22,2014
	 * @description Lay trong role lua chon tuong tac he thong
	 * */
	public ShopToken getShopInRoleById(Long shopId) {
		if (shopId == null) {
			return null;
		}
		for (ShopToken shopTK : currentUser.getListShopAllByRole()) {
			if (shopId.equals(shopTK.getShopId())) {
				return shopTK;
			}
		}
		return null;
	}

	/**
	 * Lay Shop thoa man theo Id
	 *
	 * @author hunglm16
	 * @since September 10,2014
	 * @param id
	 *            , isLevel
	 * @description Tham so la ShopId, chi su dung ham khi da dang nhap va chon
	 *              don vi mac dinh thanh cong
	 * */
	public ShopToken getShopTockenInOrgAccessByCode(String shopCode) {
		if (StringUtil.isNullOrEmpty(shopCode)) {
			return null;
		}
		shopCode = shopCode.trim().toLowerCase();
		for (ShopToken shopTK : currentUser.getListShop()) {
			if (shopCode.equals(shopTK.getShopCode().trim().toLowerCase())) {
				return shopTK;
			}
		}
		return null;
	}

	/**
	 * Lay Shop thoa man theo Id
	 *
	 * @author hunglm16
	 * @since September 10,2014
	 * @param id
	 *            , isLevel
	 * @description Tham so la ShopId, chi su dung ham khi da dang nhap va chon
	 *              don vi mac dinh thanh cong
	 * */
	public List<ShopVO> getListShopInOrgAccessByIdWithIsLevel(Integer isLevel) {
		/**
		 * isLevel = 1: VNM isLevel = 2: Kenh isLevel = 3: Vung isLevel = 4:
		 * Mien isLevel = 5: NPP
		 *
		 * oracle function lien quan: decode_islevel_vnm enum java lien quan:
		 * ShopDecentralizationSTT
		 * */

		List<ShopVO> lstData = new ArrayList<ShopVO>();

		for (ShopToken shopTK : currentUser.getListShop()) {
			if (shopTK.getIsLevel().equals(isLevel)) {
				ShopVO vo = new ShopVO();
				vo.setIsLevel(isLevel);
				vo.setObjectType(shopTK.getObjectType());
				vo.setParentId(shopTK.getParentId());
				vo.setShopCode(shopTK.getShopCode());
				vo.setShopId(shopTK.getShopId());
				vo.setShopName(shopTK.getShopName());
				vo.setStatus(ActiveType.RUNNING.getValue());
				lstData.add(vo);
			}
		}
		return lstData;
	}

	/**
	 * Lay Shop thoa man theo Id
	 *
	 * @author hunglm16
	 * @since March 22,2014
	 * @param id
	 *            - isLevel
	 * @descriptiopn isLevel tuong ung trong ShopDecentralizationSTT
	 * */
	public List<ShopVO> getListShopInRoleByIdWithIsLevel(Integer isLevel) {

		List<ShopVO> lstData = new ArrayList<ShopVO>();

		for (ShopToken shopTK : currentUser.getListShopAllByRole()) {
			if (shopTK.getIsLevel().equals(isLevel)) {
				ShopVO vo = new ShopVO();
				vo.setIsLevel(isLevel);
				vo.setObjectType(shopTK.getObjectType());
				vo.setParentId(shopTK.getParentId());
				vo.setShopCode(shopTK.getShopCode());
				vo.setShopId(shopTK.getShopId());
				vo.setShopName(shopTK.getShopName());
				vo.setStatus(ActiveType.RUNNING.getValue());
				lstData.add(vo);
			}
		}
		return lstData;
	}

	/**
	 * Kiem tra Shop thoa man
	 *
	 * @author hunglm16
	 * @since August 12,2014
	 * @param id
	 * @description Tham so la shopCode, chi su dung ham khi da dang nhap va
	 *              chon don vi mac dinh thanh cong
	 * */
	public Boolean checkShopInOrgAccessByCode(String code) {
		if (StringUtil.isNullOrEmpty(code)) {
			return false;
		}
		for (ShopToken shopTK : currentUser.getListShop()) {
			if (code.trim().toLowerCase().equals(shopTK.getShopCode().trim().toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Lay tat danh sach la cac Don vi NPP
	 *
	 * @author hunglm16
	 * @since September 8,2014
	 * @param id
	 * @description Lay tat danh sach la cac Don vi NPP
	 * */
	public List<ShopVO> getListShopIsLevel5Current() {
		List<ShopVO> lstData = new ArrayList<ShopVO>();
		for (ShopToken shopT : currentUser.getListShop()) {
			if (ShopSpecificType.NPP.getValue().equals(shopT.getIsLevel())) {
				ShopVO item = new ShopVO();
				item.setShopId(shopT.getShopId());
				item.setShopCode(shopT.getShopCode());
				item.setShopName(shopT.getShopName());
				item.setParentId(shopT.getParentId());
				item.setObjectType(shopT.getObjectType());
				item.setIsLevel(shopT.getIsLevel());
				lstData.add(item);
			}
		}
		return lstData;
	}

	/**
	 * Check ancestor.
	 *
	 * @param shopCode
	 *            the shop code
	 * @return true, if successful
	 * @author hungtx
	 * @since Sep 17, 2012
	 */
	public boolean checkAncestor(String shopCode) {
		if (currentUser != null) {
			try {
				StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
				ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
				String parentShopCode = "";
				if (staffMgr != null) {
					Staff curStaff = staffMgr.getStaffByCode(currentUser.getUserName());
					if (curStaff != null && curStaff.getShop() != null) {
						parentShopCode = curStaff.getShop().getShopCode();
					}
					return shopMgr.checkAncestor(parentShopCode, shopCode);
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return false;
	}

	/**
	 * check staff in CMS
	 *
	 * @author hunglm16
	 * @since October 6,2014
	 * */
	public boolean checkStaffInCMS(Long staffId) {
		if (staffId == null) {
			return false;
		}
		if (staffId.equals(currentUser.getStaffRoot().getStaffId())) {
			return true;
		}
		BasicFilter<BasicVO> filter = new BasicFilter<BasicVO>();
		if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
			for (StaffToken staffTk : currentUser.getListUser()) {
				if (staffId.equals(staffTk.getStaffId())) {
					return true;
				}
			}
			return false;
		}
		filter.setFlagCMS(false);
		filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
		filter.setShopRootId(currentUser.getShopRoot().getShopId());
		filter.setId(staffId);
		try {
			return commonMgr.checkStaffInCMSByFilterWithCMS(filter);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return false;
	}

	/**
	 * get List Form by SubConponent
	 *
	 * @return data
	 * @author hunglm16
	 * @since October 6,2014
	 * */
	public List<String> getListSubconponentByStringInCMS() {
		List<String> lstData = new ArrayList<String>();
		if (currentUser.getListForm() != null && !currentUser.getListForm().isEmpty()) {
			getListSubconponentInCurrentUser(currentUser.getListForm(), lstData);
		}
		return lstData;
	}

	/**
	 * get List Form by SubConponent/ lay con chau
	 *
	 * @author hunglm16
	 * @since October 6,2014
	 * */
	public List<String> getListSubconponentInCurrentUser(List<FormToken> lstForm, List<String> lstData) {
		for (FormToken form : lstForm) {
			if (FormTypeCMS.SUBCOMPONENT.getValue().equals(form.getFormType())) {
				if (!StringUtil.isNullOrEmpty(form.getUrl())) {
					lstData.add(form.getUrl());
				}
			} else {
				if (form.getListForm() != null && !form.getListForm().isEmpty()) {
					this.getListSubconponentInCurrentUser(form.getListForm(), lstData);
				}
			}
		}
		return lstData;
	}

	/**
	 * lay thong tin dia chi cua KH bao gom quan, huyen, xa
	 *
	 * @author tuannd20
	 * @param customer
	 * @return dia chi cua KH voi thong tin quan, huyen
	 */
	public String getCustomerFullAddress(Customer customer) {
		StringBuilder customerFullAddress = new StringBuilder(customer.getAddress());
		try {
			/** IDP: vuongmq 12/12/2015; Address da day du dia chi cua KH*/
			/*Area area = customer.getArea();
			if (area != null && area.getType() == AreaType.WARD) {
				if (!StringUtil.isNullOrEmpty(customerFullAddress.toString())) {
					customerFullAddress.append(", ");
				}
				customerFullAddress.append(area.getAreaName());
				area = area.getParentArea();
			}
			if (area != null && area.getType() == AreaType.DISTRICT) {
				if (!StringUtil.isNullOrEmpty(customerFullAddress.toString())) {
					customerFullAddress.append(", ");
				}
				customerFullAddress.append(area.getAreaName());
				area = area.getParentArea();
			}*/
			/*
			 * if (area != null && area.getType() == AreaType.PROVINCE) {
			 * customerFullAddress.append(", ");
			 * customerFullAddress.append(area.getAreaName()); area =
			 * area.getParentArea(); }
			 */
		} catch (Exception e) {
			// pass through
		}
		return customerFullAddress.toString();
	}

	/**
	 * Lay gia tri max de show capcha
	 *
	 * @author hunglm16
	 * @since December 25,2014
	 * */
	public static Integer loginCapcharMax() {
		Integer loginCapcharMax = StringUtil.LOGIN_CAPCHAR_MAX;
		String loginCapcharMaxStr = AppSetting.getStringValue("loginCapcharMax");
		if (!StringUtil.isNullOrEmptyNotTrim(loginCapcharMaxStr) && StringUtil.isNumberInt(loginCapcharMaxStr)) {
			loginCapcharMax = Integer.valueOf(loginCapcharMaxStr);
		}
		//request.getSession().setAttribute("loginCapcharMax", loginCapcharMax);
		return loginCapcharMax;
	}

	/**
	 * Thong tin ghi log chuc nang nghiep vu
	 *
	 * @author hunglm16
	 */
	public LogInfoVO4J createLogErrorStandard(Date startDate) {
		if (startDate == null) {
			startDate = DateUtil.now();
		}
		LogInfoVO4J logInfo = new LogInfoVO4J();
		//Bat dau giao dich (start_action)
		logInfo.setStartTime(DateUtil.toDateString(startDate, DateUtil.DATE_FORMAT_NOW));
		//Ket thuc giao dich (end_action)
		//... Auto
		//Bat dau ket noi (Log ket noi den he thong khac nhu: DB, Core)
		//...
		//Ket thuc ket noi
		//...
		//Ma ung dung (app_code)
		logInfo.setAppCode(Configuration.getDomainCode());
		logInfo.setObjectId(null);
		//Thoi gian bat dau thuc hien (start_time (YYYY/MM/DD hh:mi:ss:ms)
		//..
		//Tai khoan tac dong (userName)
		logInfo.setLoginUserCode(currentUser.getUserName());
		//Dia chi Ip tac dong cua may tram (IP Address)
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		logInfo.setIpClientAddress(ipAddress);
		//Duong dan (path)
		//Chuc nang tac dong hoac URL_request trong giao dich (Function URL_request)
		String functionCode = ServletActionContext.getActionMapping().getNamespace();
		functionCode = functionCode.replaceAll("/", "_").toUpperCase();
		logInfo.setFunctionCode(functionCode);
		String action_type = (String) session.getAttribute(ConstantManager.SESSION_ACTION_TYPE);
		if (action_type == null) {
			action_type = FuncType.READ.getValue();
		}
		logInfo.setFunctionType(FuncType.parseValue(action_type));
		//Cac tham so thuc thuc hien trong giao dich
		Enumeration<String> parameterNames = request.getParameterNames();
		StringBuilder logBuilder = new StringBuilder();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			if (paramName.equals("token")) {
				continue;
			}
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0, size = paramValues.length; i < size; i++) {
				String paramValue = paramValues[i];
				logBuilder.append(paramName + " : " + paramValue);
			}
			logBuilder.append("|");
		}
		logInfo.setDescription(logBuilder.toString());

		return logInfo;
	}

	/**
	 * Khoi tao logInfo - logLogginAndLoguot
	 *
	 * @author hunglm16
	 * @since August 13,2015
	 * */
	public LogInfoVO4J createLogLogginAndLoguot(Date startDate, String actionResult) {
		if (startDate == null) {
			startDate = DateUtil.now();
		}
		LogInfoVO4J logInfo = new LogInfoVO4J();
		//Thoi gian dang nhap, dang xuat
		logInfo.setStartTime(DateUtil.toDateString(startDate, DateUtil.DATE_FORMAT_NOW));
		//Ma ung dung (CMS)
		logInfo.setAppCode(Configuration.getDomainCode());
		//Tai khoan dang nhap
		if (currentUser != null) {
			logInfo.setLoginUserCode(currentUser.getUserName());
		}
		//Dia chi IP cua may tram
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
		   ipAddress = request.getRemoteAddr();
		}
		logInfo.setIpClientAddress(ipAddress);
		//Duong danh truy cap
		logInfo.setUri(request.getRequestURI());
		//Ket qua hoac hanh dong dang nhap thanh cong hoac khong thanh cong (result, action)
		logInfo.setActionResult(actionResult);
		//Khoang thoi gian thuc hien (Tu khi bat dau den ket thuc tinh theo mili-second)
		logInfo.setStartForEndTimeMiliSecon(String.valueOf(DateUtil.dateDiffMinute(DateUtil.now(), startDate)));
		return logInfo;
	}

	protected void checkCreateShopParamOrderSearch(Long numberValueOrder, Long shopId) {
		try {
			if (numberValueOrder != null && shopId != null) {
				ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shopId, "CONFIRM_ORDER_SEARCH");
				if (numberValueOrderParam != null && ActiveType.RUNNING.equals(numberValueOrderParam.getStatus())) {
					numberValueOrderParam.setCode(numberValueOrder.toString());
					numberValueOrderParam.setName(numberValueOrder.toString());
					numberValueOrderParam.setUpdateUser(currentUser.getUserName());
					saleOrderMgr.updateShopParamByInvoice(numberValueOrderParam);
				} else if (numberValueOrderParam == null) {
					Shop shop = shopMgr.getShopById(shopId);
					ShopParam addNumberValueOrderParam = new ShopParam();
					addNumberValueOrderParam.setShop(shop);
					addNumberValueOrderParam.setType("CONFIRM_ORDER_SEARCH");
					addNumberValueOrderParam.setStatus(ActiveType.RUNNING);
					addNumberValueOrderParam.setCode(numberValueOrder.toString());
					addNumberValueOrderParam.setName(numberValueOrder.toString());
					addNumberValueOrderParam.setCreateUser(currentUser.getUserName());
					saleOrderMgr.createShopParamByInvoice(addNumberValueOrderParam);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}

	}

	/**
	 * trungtm6 merge from vnm on 21/07/2015
	 * @param numberValueOrder
	 * @param shopId
	 * @param typeParamSearch
	 */
	protected void checkCreateShopParamOrderSearch(BigDecimal numberValueOrder, Long shopId, String typeParamSearch) {
		try {
			if (numberValueOrder != null && shopId != null) {
				ShopParam numberValueOrderParam = commonMgr.getShopParamByType(shopId, typeParamSearch);
				if (numberValueOrderParam != null && ActiveType.RUNNING.equals(numberValueOrderParam.getStatus())) {
					numberValueOrderParam.setCode(numberValueOrder.toString());
					numberValueOrderParam.setName(numberValueOrder.toString());
					numberValueOrderParam.setUpdateUser(currentUser.getUserName());
					saleOrderMgr.updateShopParamByInvoice(numberValueOrderParam);
				} else if (numberValueOrderParam == null) {
					Shop shop = shopMgr.getShopById(shopId);
					ShopParam addNumberValueOrderParam = new ShopParam();
					addNumberValueOrderParam.setShop(shop);
					addNumberValueOrderParam.setType(typeParamSearch);
					addNumberValueOrderParam.setStatus(ActiveType.RUNNING);
					addNumberValueOrderParam.setCode(numberValueOrder.toString());
					addNumberValueOrderParam.setName(numberValueOrder.toString());
					addNumberValueOrderParam.setCreateUser(currentUser.getUserName());
					saleOrderMgr.createShopParamByInvoice(addNumberValueOrderParam);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}

	}

	/**
	 * Kiem tra user login co quan ly shopId ko
	 * @author tuannd20
	 * @param shopId shopId cua shop dang kiem tra
	 * @return true: user login quan ly shopId. Nguoc lai, false
	 * @since 28/08/2015
	 */
	protected Boolean isLoginUserManageShop(Long shopId) {
		Map<Long, Long> childShops = this.getMapShopChild();
		return childShops != null && shopId != null && childShops.containsKey(shopId);
	}

	/**
	 * Kiem tra user login co quan ly staffId ko
	 * @author tuannd20
	 * @param staffId staffId cua shop dang kiem tra
	 * @return true: user login quan ly staffId. Nguoc lai, false
	 * @since 28/08/2015
	 */
	protected Boolean isLoginUserManageStaff(Long staffId) {
		HashMap<Long, Long> managingStaffs = this.getMapUserId();
		return managingStaffs != null && staffId != null && managingStaffs.containsKey(staffId);
	}

	/**
	 * loc danh sach nhan vien theo phan quyen
	 * @author tuannd20
	 * @param staffs danh sach nhan vien can loc
	 * @param keepStoppedStaff true: giu lai nhan vien tam ngung. false, loai bo ca nhan vien tam ngung
	 * @return danh sach nhan vien da loc theo phan quyen
	 * @since 07/10/2015
	 */
	protected List<Staff> filterPriviledgedStaffs(List<Staff> staffs, Boolean keepStoppedStaff) {
		if (staffs == null) {
			return null;
		}
		List<Staff> priviledgedStaffs = new ArrayList<Staff>();
		for (Staff staff : staffs) {
			if (staff != null
				&& (isLoginUserManageStaff(staff.getId()) || (keepStoppedStaff && staff.getStatus() == ActiveType.STOPPED))) {
				priviledgedStaffs.add(staff);
			}
		}
		return priviledgedStaffs;
	}

	/**
	 * loc danh sach don vi theo phan quyen
	 * @author tuannd20
	 * @param shops danh sach don vi can loc
	 * @param keepStoppedShop true: giu lai cac shop tam ngung. false: loai bo cac shop tam ngung
	 * @return danh sach don vi da loc theo phan quyen
	 * @since 07/10/2015
	 */
	protected List<Shop> filterPriviledgedShops(List<Shop> shops, Boolean keepStoppedShop) {
		if (shops == null) {
			return null;
		}
		List<Shop> priviledgedShops = new ArrayList<Shop>();
		for (Shop shop : shops) {
			if (shop != null
				&& (isLoginUserManageShop(shop.getId()) || (keepStoppedShop && shop.getStatus() == ActiveType.STOPPED))) {
				priviledgedShops.add(shop);
			}
		}
		return priviledgedShops;
	}

	/**
	 * Set thong tin phan quyen
	 * @author tuannd20
	 * @param priviledgeInfo doi tuong luu thong tin phan quyen
	 * @since 08/10/2015
	 */
	protected void setPriviledgeInfo(PriviledgeInfo priviledgeInfo) {
		if (priviledgeInfo != null && currentUser != null) {
			if (currentUser.getShopRoot() != null) {
				priviledgeInfo.setShopRootId(currentUser.getShopRoot().getShopId());
			}
			if (currentUser.getStaffRoot() != null) {
				priviledgeInfo.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			}
			if (currentUser.getRoleToken() != null) {
				priviledgeInfo.setRoleId(currentUser.getRoleToken().getRoleId());
			}
		}
	}

	/**
	 * Kiem tra khoang ngay bao cao
	 * @author hunglm16
	 * @param fDate
	 * @param tDate
	 * @param appCode
	 * @param defaultConfig
	 * @return
	 * @throws BusinessException
	 * @since 12/11/2015
	 */
	private String checkMaxNumberDayReport(Date fDate, Date tDate, String appCode, String defaultConfig) throws BusinessException {
		String msg = null;
		int sysMaxNumberDay = 0;//Mac dinh
		ApParam app = apParamMgr.getApParamByCode(appCode, ApParamType.SYS_CONFIG);
		if (app != null && !StringUtil.isNullOrEmpty(app.getValue()) && StringUtil.isNumberInt(app.getValue())) {
			sysMaxNumberDay = Integer.valueOf(app.getValue()).intValue();
		} else {
			if (StringUtil.isNullOrEmpty(defaultConfig) || !StringUtil.isNumberInt(defaultConfig)) {
				msg = R.getResource("common.sysconfig.error.sys.max.number.day.rp.undefined");
			} else {
				sysMaxNumberDay = Integer.valueOf(defaultConfig).intValue();
			}
		}
		if (StringUtil.isNullOrEmpty(msg) && commonMgr.arithmeticForDateByDateWithTrunc(tDate, fDate, ArithmeticEnum.MINUS) >= sysMaxNumberDay) {
			msg = R.getResource("common.compare.error.less.todate.fromdate.with.sysconfig", String.valueOf(sysMaxNumberDay));
		}
		return msg;
	}

	/**
	 * Kiem tra khoang ngay bao cao ngan
	 * @author hunglm16
	 * @param fDate
	 * @param tDate
	 * @return thong tin loi neu co
	 * @throws BusinessException
	 * @since 12/11/2015
	 */
	public String checkMaxNumberDayReportShort(Date fDate, Date tDate) throws BusinessException {
		return checkMaxNumberDayReport(fDate, tDate, ConstantManager.sysMaxNumberDayReportShort, Configuration.getNumberDayShortDefaultReport());
	}

	/**
	 * Kiem tra khoang ngay bao cao trung binh
	 * @author hunglm16
	 * @param fDate
	 * @param tDate
	 * @return thong tin loi neu co
	 * @throws BusinessException
	 * @since 12/11/2015
	 */
	public String checkMaxNumberDayReportMedium(Date fDate, Date tDate) throws BusinessException {
		return checkMaxNumberDayReport(fDate, tDate, ConstantManager.sysMaxNumberDayReportMedium, Configuration.getNumberDayMediumDefaultReport());
	}

	/**
	 * Kiem tra khoang ngay bao cao dai
	 * @author hunglm16
	 * @param fDate
	 * @param tDate
	 * @return thong tin loi neu co
	 * @throws BusinessException
	 * @since 12/11/2015
	 */
	public String checkMaxNumberDayReportLong(Date fDate, Date tDate) throws BusinessException {
		return checkMaxNumberDayReport(fDate, tDate, ConstantManager.sysMaxNumberDayReportLong, Configuration.getNumberDayLongDefaultReport());
	}


	/**
	 * Xu ly checkCurrentUserAndShopId
	 * @author vuongmq
	 * @param msg
	 * @return String
	 * @throws BusinessException
	 * @since Dec 24, 2015
	*/
	public String checkCurrentUserAndShopId(Long shopId) throws BusinessException {
		String msg = null;
		if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
			msg = R.getResource("common.cms.undefined");
		}
		if (StringUtil.isNullOrEmpty(msg)) {
			if (shopId == null) {
				msg = R.getResource("catalog.unit.tree.not.exist");
			} else {
				Shop shop = shopMgr.getShopById(shopId);
				if (shop != null) {
					if (this.getMapShopChild().get(shop.getId()) == null) {
						msg = R.getResource("common.cms.shop.undefined");
					}
				} else {
					msg = R.getResource("common.cms.shop.undefined");
				}
			}
		}
		return msg;
	}

	/**
	 * Thay the dau ' va / tranh loi xss
	 *
	 * @author tamvnm
	 * @param str
	 * @return chuoi da thay doi
	 * @since 29 Oct 2015
	 */
	public String escapeHTMLForXSS(String str) {
		try {
			if (StringUtil.isNullOrEmpty(str)) {
				return str;
			}
			str = str.replace("'", "\\'");
			str = str.replace("\"", "\\\"");
			return str.replace("/", "\\/");
		} catch (Exception e) {
			// pass through
			return str;
		}
	}

	/////////////////@: GETTER/SETTER :@//////////////////////////
	public Shop getShopCurrentPage() {
		return shopCurrentPage;
	}

	public void setShopCurrentPage(Shop shopCurrentPage) {
		this.shopCurrentPage = shopCurrentPage;
	}

	public Long getShopIdIsFcnIsLv5() {
		return shopIdIsFcnIsLv5;
	}

	public void setShopIdIsFcnIsLv5(Long shopIdIsFcnIsLv5) {
		this.shopIdIsFcnIsLv5 = shopIdIsFcnIsLv5;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public CurrentPageToken getCurrentPageToken() {
		return currentPageToken;
	}

	public void setCurrentPageToken(CurrentPageToken currentPageToken) {
		this.currentPageToken = currentPageToken;
	}

	public Boolean getIsShowDialogIsFcnIsLv5() {
		return isShowDialogIsFcnIsLv5;
	}

	public void setIsShowDialogIsFcnIsLv5(Boolean isShowDialogIsFcnIsLv5) {
		this.isShowDialogIsFcnIsLv5 = isShowDialogIsFcnIsLv5;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	protected String q;

	public boolean getFullPrivilege() {
		return this.isFullPrivilege;
	}

	public void setFullPrivilege(boolean isFullPrivilege) {
		this.isFullPrivilege = isFullPrivilege;
	}

	public Map<String, Integer> getMapControl() {
		return mapControl;
	}

	public void setMapControl(Map<String, Integer> mapControl) {
		this.mapControl = mapControl;
	}

	public List<ControlToken> getListControl() {
		return listControl;
	}

	public void setListControl(List<ControlToken> listControl) {
		this.listControl = listControl;
	}

	public String getLockDate() {
		return lockDate;
	}

	public void setLockDate(String lockDate) {
		this.lockDate = lockDate;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}

	public List<List<AttributeDetailVO>> getLstObjectAttributeDetail() {
		return lstObjectAttributeDetail;
	}

	public void setLstObjectAttributeDetail(List<List<AttributeDetailVO>> lstObjectAttributeDetail) {
		this.lstObjectAttributeDetail = lstObjectAttributeDetail;
	}

	public String getFormatType() {
		return formatType;
	}

	public void setFormatType(String formatType) {
		this.formatType = formatType;
	}

	public boolean isSessionData() {
		return sessionData;
	}

	public void setSessionData(boolean sessionData) {
		this.sessionData = sessionData;
	}

	public String getOutputName() {
		return outputName;
	}

	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public List<List<String>> getLstObjectValue() {
		return lstObjectValue;
	}

	public void setLstObjectValue(List<List<String>> lstObjectValue) {
		this.lstObjectValue = lstObjectValue;
	}

	public List<ApParam> getLstActiveType() {
		return lstActiveType;
	}

	public void setLstActiveType(List<ApParam> lstActiveType) {
		this.lstActiveType = lstActiveType;
	}

	public List<Attribute> getLstAttribute() {
		return lstAttribute;
	}

	public void setLstAttribute(List<Attribute> lstAttribute) {
		this.lstAttribute = lstAttribute;
	}

	public List<AttributeDetailVO> getLstAttributeDetail() {
		return lstAttributeDetail;
	}

	public void setLstAttributeDetail(List<AttributeDetailVO> lstAttributeDetail) {
		this.lstAttributeDetail = lstAttributeDetail;
	}

	public List<String> getLstAttributeValue() {
		return lstAttributeValue;
	}

	public boolean isVnm_ho() {
		return checkRoles(VSARole.VNM_SALESONLINE_HO);
	}

	public void setVnm_ho(boolean vnm_ho) {
		this.vnm_ho = vnm_ho;
	}

	public void setLstAttributeValue(List<String> lstAttributeValue) {
		this.lstAttributeValue = lstAttributeValue;
	}

	public List<Long> getLstAttributeId() {
		return lstAttributeId;
	}

	public void setLstAttributeId(List<Long> lstAttributeId) {
		this.lstAttributeId = lstAttributeId;
	}

	public List<Integer> getLstAttributeColumnType() {
		return lstAttributeColumnType;
	}

	public void setLstAttributeColumnType(List<Integer> lstAttributeColumnType) {
		this.lstAttributeColumnType = lstAttributeColumnType;
	}

	public List<Integer> getLstAttributeColumnValueType() {
		return lstAttributeColumnValueType;
	}

	public void setLstAttributeColumnValueType(List<Integer> lstAttributeColumnValueType) {
		this.lstAttributeColumnValueType = lstAttributeColumnValueType;
	}

	public void setActivePermission(boolean activePermission) {
		this.activePermission = activePermission;
	}

	public void setShopEnable(boolean shopEnable) {
		this.shopEnable = shopEnable;
	}

	public void setDevelopCustomer(boolean developCustomer) {
		this.developCustomer = developCustomer;
	}

	public void setShopEnableBoth(boolean shopEnableBoth) {
		this.shopEnableBoth = shopEnableBoth;
	}

	public void setHeadOfficePermission(boolean headOfficePermission) {
		this.headOfficePermission = headOfficePermission;
	}

	public Integer getSeparationLot() {
		return separationLot;
	}

	public void setSeparationLot(Integer separationLot) {
		this.separationLot = separationLot;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public List<TreeVOBasic<ShopVO>> getCbxTreeShopVO() {
		return cbxTreeShopVO;
	}

	public void setCbxTreeShopVO(List<TreeVOBasic<ShopVO>> cbxTreeShopVO) {
		this.cbxTreeShopVO = cbxTreeShopVO;
	}

	public void setLstStatusBeanWithWaiting(List<StatusBean> lstStatusBeanWithWaiting) {
		this.lstStatusBeanWithWaiting = lstStatusBeanWithWaiting;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}

	public Map<String, Object> getParametersReport() {
		return parametersReport;
	}

	public void setParametersReport(Map<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public UserToken getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(UserToken currentUser) {
		this.currentUser = currentUser;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public List<StatusBean> getLstPaymentType() {
		lstPaymentType = new ArrayList<StatusBean>();
		StatusBean moneyType = new StatusBean(Long.valueOf(String.valueOf(PaymentType.MONEY.getValue())), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "payment.type.money"));
		StatusBean bankTransferType = new StatusBean(Long.valueOf(String.valueOf(PaymentType.BANK_TRANSFER.getValue())), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "payment.type.bank.transfer"));
		lstPaymentType.add(moneyType);
		lstPaymentType.add(bankTransferType);
		return lstPaymentType;
	}

	public void setLstPaymentType(List<StatusBean> lstPaymentType) {
		this.lstPaymentType = lstPaymentType;
	}

	public int getNumFail() {
		return numFail;
	}

	public void setNumFail(int numFail) {
		this.numFail = numFail;
	}

	public String getFileNameFail() {
		return fileNameFail;
	}

	public void setFileNameFail(String fileNameFail) {
		this.fileNameFail = fileNameFail;
	}

	public Integer getIsView() {
		return isView;
	}

	public void setIsView(Integer isView) {
		this.isView = isView;
	}

	public List<CellBean> getLstView() {
		return lstView;
	}

	public void setLstView(List<CellBean> lstView) {
		this.lstView = lstView;
	}

	public boolean isTypeView() {
		return typeView;
	}

	public void setTypeView(boolean typeView) {
		this.typeView = typeView;
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public boolean isActivePermission() {
		return checkRoles(VSARole.VNM_SALESONLINE_HO);
	}

	public boolean isCommercialSupportPermission() {
		return checkRoles(VSARole.VNM_SALESONLINE_HO);
	}

	public void setCommercialSupportPermission(boolean commercialSupportPermission) {
		this.commercialSupportPermission = commercialSupportPermission;
	}

	public int getNewToken() {
		return newToken;
	}

	public void setNewToken(int newToken) {
		this.newToken = newToken;
	}

	public boolean isShopLocked() {
		return shopLocked;
	}

	public void setShopLocked(boolean shopLocked) {
		this.shopLocked = shopLocked;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getIsShowChooseShopIsLv5() {
		return isShowChooseShopIsLv5;
	}

	public void setIsShowChooseShopIsLv5(Integer isShowChooseShopIsLv5) {
		this.isShowChooseShopIsLv5 = isShowChooseShopIsLv5;
	}

	public String getUrlRedrectCurentPage() {
		return urlRedrectCurentPage;
	}

	public void setUrlRedrectCurentPage(String urlRedrectCurentPage) {
		this.urlRedrectCurentPage = urlRedrectCurentPage;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public LogInfo getLogInfo() {
		return logInfo;
	}

	public void setLogInfo(LogInfo logInfo) {
		this.logInfo = logInfo;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public List<AttributeDynamicVO> getLstAttributeDynamic() {
		return lstAttributeDynamic;
	}

	public void setLstAttributeDynamic(List<AttributeDynamicVO> lstAttributeDynamic) {
		this.lstAttributeDynamic = lstAttributeDynamic;
	}

	public List<Integer> getLstYear() {
		return lstYear;
	}

	public void setLstYear(List<Integer> lstYear) {
		this.lstYear = lstYear;
	}

	public String getSystimestamp() {
		return systimestamp;
	}

	public void setSystimestamp(String systimestamp) {
		this.systimestamp = systimestamp;
	}

	public int getNumFinish() {
		return numFinish;
	}

	public void setNumFinish(int numFinish) {
		this.numFinish = numFinish;
	}

	public String getReportCode() {
		return reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public Integer getReportForm() {
		return reportForm;
	}

	public void setReportForm(Integer reportForm) {
		this.reportForm = reportForm;
	}

	public ViewActionType getViewActionType() {
		return viewActionType;
	}

	public void setViewActionType(ViewActionType viewActionType) {
		this.viewActionType = viewActionType;
	}

}

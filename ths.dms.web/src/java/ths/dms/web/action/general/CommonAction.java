package ths.dms.web.action.general;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import viettel.passport.client.RoleToken;
import viettel.passport.client.ShopToken;
import viettel.passport.client.UserToken;

import ths.dms.core.business.AreaMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DisplayProgrameMgr;
import ths.dms.core.business.EquipRecordMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.KeyShopMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Area;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.KS;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StDisplayProgramVNM;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentStatisticRecordStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.ShopDecentralizationSTT;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.CustomerFilter;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.filter.EquipItemFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipRoleFilter;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.EquipItemVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipmentManagerVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.TreeVOBasic;
import ths.dms.core.entities.vo.WarehouseVO;
import ths.dms.core.filter.UnitFilter;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.AppSetting;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.servlet.CaptchaServlet;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * The Class CommonAction.
 */
public class CommonAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1313955969119454349L;
	
	private KeyShopMgr keyShopMgr;
	
	private StockManagerMgr stockManagerMgr;
	
	private boolean isShowPrice = false;
	
	private Long idParentCat;
	
	private Long cycleId;
	
	private Long ksId;
	
	private Long idParentSubCat;
	
	private Integer isShow = 0;
	
	private Integer stSpecType;
	
	private Integer typeCurrentUser;
	
	public final Integer getTypeCurrentUser() {
		return typeCurrentUser;
	}

	public final void setTypeCurrentUser(Integer typeCurrentUser) {
		this.typeCurrentUser = typeCurrentUser;
	}

	private final int MAP_TYPE_VIETTEL = 1;
	
	private Boolean isShowAllShop;
	
	private Boolean isChoiceAll;
	
	private List<Long> lstShopId;
	
	private Integer year;
	private Integer numPeriod;
	
	private Integer viewChangePassDefault;

	private String roleCode;

	private String roleName;

	private Long staffRoleId;

	private Integer statusRole;
	
	private Boolean flagPagination;
	
	private Long objectId;
	
	private String equipCode;
	private String seriNumber;
	private Long categoryId;
	private Long providerId;
	private Integer yearManufacture;
	private Long groupId;
	private String stockCode;
	private Integer usageStatus;
	private Integer stockType;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	
	private String strEquip;
	
	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		super.execute();
		resetToken(result);
		isShowPrice = true;
		return SUCCESS;
	}
	
	/**
	 * Search Staff by All.
	 * 
	 * @author tamvnm
	 * @since March 25,2015
	 * @return the string
	 */
	public String searchEquipRoleShowList() {
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<ShopVO>());
			KPaging<EquipRole> kPaging = new KPaging<EquipRole>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			EquipRoleFilter filter = new EquipRoleFilter();
			
			filter.setkPaging(kPaging);
			
			if(roleCode != null) {
				if(!roleCode.isEmpty()) {
					filter.setRoleCode(roleCode);
				}
			}
			
			if(roleName != null) {
				if(!roleName.isEmpty()) {
					filter.setRoleName(roleName);
				}
			}
			if(staffRoleId != null) {
				filter.setStaffId(staffRoleId);
			}
			
			if(statusRole != null) {
				filter.setStatusRole(statusRole);
			}
			
			ObjectVO<EquipRole> objVO = equipmentManagerMgr.searchListEquipRoleByFilter(filter);
			if (objVO != null) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public String checkShopLock(){
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser != null && currentUser.getShopRoot() != null && ShopSpecificType.NPP.getValue().equals(currentUser.getShopRoot().getIsLevel())) {
				if (currentUser.getCheckShopLock() == null || currentUser.getCheckShopLock() == 0) {
					Date lockDate = shopLockMgr.getNextLockedDay(currentUser.getShopRoot().getShopId());
					Date currentDate = commonMgr.getSysDate();
					if (lockDate != null) {
						if (DateUtil.compareDateWithoutTime(lockDate, currentDate) == -1) {
							result.put(ERROR, true);
							result.put("currentDate", DateUtil.toDateSimpleFormatString(currentDate));
							result.put("lockDate", DateUtil.toDateSimpleFormatString(lockDate));
							//Cap nhat lai Session
							currentUser.setCheckShopLock(1);
							request.getSession().setAttribute("cmsUserToken", currentUser);
						} else {
							result.put(ERROR, false);
						}
					} else {
						result.put(ERROR, false);
					}
				} else {
					result.put(ERROR, false);
				}
			} else {
				result.put(ERROR, false);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.checkShopLock()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * @author hunglm16
	 * @since AUGUST 08,2014
	 * @description Load trang chu
	 * */

	public String home() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		if (currentUser != null) {
			try {
				lstRoleToken = currentUser.getListRole();
				// kiem tra co trung mat khau mac dinh trong ap_param hay khong
				if (!this.checkPolicyPassword()) {
					viewChangePassDefault = StatusType.ACTIVE.getValue();
					return SUCCESS;
				} else {
					viewChangePassDefault = StatusType.INACTIVE.getValue();
				}
				//Kiem tra tinh duy nhat ve quyen
				if (lstRoleToken != null && !lstRoleToken.isEmpty() && lstRoleToken.size() == 1 && currentUser.getShopRoot() == null) {
					List<ShopVO> lstShop = getListShopByRole(lstRoleToken.get(0).getRoleId());
					lstShopToken = new ArrayList<ShopToken>();
					if (lstShop == null || lstShop.isEmpty() || lstShop.size() == 0) {
						lstShop = new ArrayList<ShopVO>();
						Staff staffCurrent = staffMgr.getStaffById(currentUser.getUserId());
						ShopVO shopVo = new ShopVO();
						if (staffCurrent != null) {
							shopVo.setShopId(staffCurrent.getShop().getId());
							shopVo.setShopCode(staffCurrent.getShop().getShopCode());
							shopVo.setShopName(staffCurrent.getShop().getShopName());
							Shop sh = staffCurrent.getShop();
							if (sh.getParentShop() != null) {
								shopVo.setParentId(staffCurrent.getShop().getParentShop().getId());
							}
							if (sh.getType() != null && sh.getType().getSpecificType() != null && ShopSpecificType.NPP.getValue().equals(sh.getType().getSpecificType().getValue())) {
									shopVo.setIsLevel(ShopSpecificType.NPP.getValue());			
									shopVo.setObjectType(ShopSpecificType.NPP.getValue());
							}
						}
						lstShop.add(shopVo);
					}
					for (ShopVO vo : lstShop) {
						ShopToken shopTK = new ShopToken();
						shopTK.setShopId(vo.getShopId());
						shopTK.setShopCode(vo.getShopCode());
						shopTK.setShopName(vo.getShopName());
						shopTK.setParentId(vo.getParentId());
						shopTK.setObjectType(vo.getObjectType());
						shopTK.setIsLevel(vo.getIsLevel());
						shopTK.setShopChannel(vo.getShopChannel());
						lstShopToken.add(shopTK);
					}

					if (lstShopToken.size() > 1) {
						return SUCCESS;
					} else {
						setSessionNextRole(lstRoleToken.get(0), lstShopToken.get(0));
					}
				}
				if (id != null && lstId != null && lstId.size() > 0) {
					
					for (int i = 0; i < lstRoleToken.size(); i++) {
						if (lstRoleToken.get(i).getRoleId().equals(id)) {
							roleToken = lstRoleToken.get(i);
							break;
						}
					}
					if (roleToken != null) {
						//Lay danh sach don vi thuoc vai tro
						lstShopToken = new ArrayList<ShopToken>();
						shopToken = currentUser.getShopRoot();
						List<ShopVO> lstShop = getListShopByRole(roleToken.getRoleId());
						if (!lstShop.isEmpty()) {
							for (ShopVO vo : lstShop) {
								ShopToken shopTK = new ShopToken();
								shopTK.setShopId(vo.getShopId());
								shopTK.setShopCode(vo.getShopCode());
								shopTK.setShopName(vo.getShopName());
								shopTK.setParentId(vo.getParentId());
								shopTK.setObjectType(vo.getObjectType());
								shopTK.setIsLevel(vo.getIsLevel());
								shopTK.setShopChannel(vo.getShopChannel());
								lstShopToken.add(shopTK);
							}
						}
						//set session
						setSessionNextRole(roleToken, shopToken);
					}
				}
				roleToken = currentUser.getRoleToken();
				lstShopToken = new ArrayList<ShopToken>();
				lstShopToken = currentUser.getListShop();
			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.home()"), createLogErrorStandard(startLogDate));
			}
		}
		return SUCCESS;
	}

	/**
	 * Check mat khau co giong mat khau mac dinh hay khong
	 * @author vuongmq
	 * return boolean
	 * @since 30/10/2015
	 * */
	private boolean checkPolicyPassword() {
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser != null && currentUser.getUserId() != null) {
				Staff staff = staffMgr.getStaffById(currentUser.getUserId());
				if (staff != null) {
					ApParam apParam = apParamMgr.getApParamByCode(ConstantManager.SYS_DEFAULT_PASSWORD, ApParamType.SYS_CONFIG);
					if (apParam != null) {
						String passDefault = StringUtil.generateHash(apParam.getValue(), staff.getStaffCode().trim().toLowerCase());
						if (staff.getPassword().equals(passDefault)) {
							return false;
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.checkPassDefault()"), createLogErrorStandard(startLogDate));
		}
		return true;
	}
	
	/**
	 * @author hunglm16
	 * @since AUGUST 14,2014
	 * @description get List Shop with Permission Data by User
	 * */
	private void getListShopByTreeShop(TreeVOBasic<ShopVO> node, List<ShopToken> lstShopToken) {
		if (node != null) {
			List<TreeVOBasic<ShopVO>> children = node.getChildren();
			if (children != null && !children.isEmpty()) {
				for (TreeVOBasic<ShopVO> nodeC : children) {
					this.getListShopByTreeShop(nodeC, lstShopToken);
				}
			} else {
				ShopToken shopTK = new ShopToken();
				shopTK.setShopId(node.getId());
				shopTK.setShopCode(node.getCode());
				shopTK.setShopName(node.getName());
				shopTK.setParentId(node.getParentId());
				lstShopToken.add(shopTK);
			}
		}
	}

	/**
	 * Tim kiem Don vi
	 * 
	 * @author hunglm16
	 * @since August 16,2014
	 * @return List Shop
	 * @description Tim kiem shop nhung phu thuoc don vi lua chon theo he thong
	 *              phan quyen CMS
	 */
	public String searchShopShowListByFilterCMS() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
			KPaging<ShopVO> kPaging = new KPaging<ShopVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
//			if (isShowAllShop == null || Boolean.FALSE.equals(isShowAllShop)) { 
				filter.setStrShopId(super.getStrListShopId());
//			}
			filter.setStatus(ActiveType.RUNNING.getValue());
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
			if (integerG != null) {
				filter.setIntFlag(integerG);
			}
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<ShopVO> data = new ObjectVO<ShopVO>();
			data = shopMgr.getListShopVOBasicByFilter(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<WarehouseVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchShopShowListByFilterCMS()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Tim kiem Don vi hien thi dang Datagrid.
	 * 
	 * @author hunglm16
	 * @since August 16,2014
	 * @return List Shop
	 * @description Tim kiem shop la NPP nhung phu thuoc don vi lua chon theo he
	 *              thong phan quyen CMS
	 */
	public String searchShopShowListNPPByFilterCMS() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
			KPaging<ShopVO> kPaging = new KPaging<ShopVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setShopType(ShopSpecificType.NPP);
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
			filter.setIntFlag(ShopSpecificType.NPP.getValue());
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<ShopVO> data = new ObjectVO<ShopVO>();
			data = shopMgr.getListShopVOBasicByFilter(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<ShopVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchShopShowListNPPByFilterCMS()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * Tim kiem Don vi hien thi dang Datagrid GT MT
	 * 
	 * @author hunglm16
	 * @since August 16,2014
	 * @return List Shop
	 * @description Tim kiem shop la NPP nhung phu thuoc don vi lua chon theo he
	 *              thong phan quyen CMS
	 */
	public String searchShopShowListNPPByFilterCMS_GTMT() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
			KPaging<ShopVO> kPaging = new KPaging<ShopVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			filter.setStatus(ActiveType.RUNNING.getValue());
//			filter.setShopType(ShopSpecificType.NPP);
			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
//			filter.setIntFlag(ShopSpecificType.NPP.getValue());
			filter.setIntFlag(ShopDecentralizationSTT.NPP.getValue());
			filter.setIsUnionShopRoot(isUnionShopRoot);
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<ShopVO> data = new ObjectVO<ShopVO>();
			data = shopMgr.getListShopVOBasicByFilter_GTMT(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<ShopVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchShopShowListNPPByFilterCMS()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Check session.
	 * 
	 * @return the string
	 */
	public String checkSession() {
		return SUCCESS;
	}

	/**
	 * Tim kiem san pham trong nghanh hang Z
	 * 
	 * @author hunglm16
	 * @since September 4,2014
	 * @description Ai lam lien quan den ham nay yeu cau sua lai noi dung ghi
	 *              chu.Cam on!
	 * 
	 * */
	public String searchProductInCatZ() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Product> kPaging = new KPaging<Product>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
			ObjectVO<Product> lstProduct = productMgr.getListProductInCatZ(kPaging, code, name);

			if (lstProduct != null) {
				result.put("total", lstProduct.getkPaging().getTotalRows());
				result.put("rows", lstProduct.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchProductInCatZ()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Search category.
	 * 
	 * @return the string
	 * @author hungtx
	 */
	public String searchCategory() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<ProductInfo> kPaging = new KPaging<ProductInfo>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			if (productInfoMgr != null) {
				/*ObjectVO<ProductInfo> shopVO = productInfoMgr.getListProductInfo(kPaging, code, name, null, ActiveType.RUNNING, ProductType.CAT, true);
				if (shopVO != null) {
					result.put("total", shopVO.getkPaging().getTotalRows());
					result.put("rows", shopVO.getLstObject());
				}*/
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchCategory()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Search province.
	 * 
	 * @return the string
	 * @author phut
	 * @since Aug 21, 2012
	 */
	public String searchProvince() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Area> kPaging = new KPaging<Area>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
			if (areaMgr != null) {
				/*ObjectVO<Area> areaVO = areaMgr.getListArea(kPaging, null, null, null, ActiveType.RUNNING, code, name, null, null, null, null, AreaType.PROVINCE, null);
				if (areaVO != null) {
					result.put("total", areaVO.getkPaging().getTotalRows());
					result.put("rows", areaVO.getLstObject());
				}*/
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchProvince()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Search customer type.
	 * 
	 * @author hunglm16
	 * @since September 4,2014
	 * @return the string
	 */
	public String searchCustomerType() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<ChannelType> kPaging = new KPaging<ChannelType>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			if (channelTypeMgr != null) {
				ChannelTypeFilter filter = new ChannelTypeFilter();
				filter.setChannelTypeCode(code);
				filter.setChannelTypeName(name);
				filter.setType(ChannelTypeType.CUSTOMER);
				filter.setStatus(ActiveType.RUNNING);
				ObjectVO<ChannelType> channelTypeVO = channelTypeMgr.getListChannelType(filter, kPaging);
				if (channelTypeVO != null) {
					result.put("total", channelTypeVO.getkPaging().getTotalRows());
					result.put("rows", channelTypeVO.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchCustomerType()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Dang nhap he thong
	 * 
	 * @author hunglm16
	 * @since August 21,2014
	 * @return page
	 * @description Neu dang nhap thanh cong se tra ve trang chu, nguoc lai se
	 *              quay ve login
	 * */
	public String login() {
		Date startLogDate = DateUtil.now();
		try {
			if (username != null || password != null) {
				if (StringUtil.isNullOrEmpty(username) && StringUtil.isNullOrEmpty(password)) {
					errMsg = R.getResource("chua_dien_tt_dang_nhap_hoac_mat_khau");
					return ERROR;
				}
				Integer attemptLogin = 0;
				if (request.getSession(false).getAttribute("attemptLogin") == null) {
					request.getSession().setAttribute("attemptLogin", 0);
				} else {
					attemptLogin = (Integer) request.getSession().getAttribute("attemptLogin");
					request.getSession().setAttribute("attemptLogin", attemptLogin + 1);
				}
				if (!StringUtil.isNullOrEmpty(username) && !StringUtil.isNullOrEmpty(password)) {
					if (attemptLogin >= loginCapcharMax()) {
						String sessionToken = CaptchaServlet.getToken(request.getSession());
						String capcha = "";
						if (request.getParameter("capcha") != null) {
							capcha = request.getParameter("capcha").toString();
							capcha = capcha.trim();
						}
						if (!(sessionToken != null && sessionToken.equals(capcha))) {
							errMsg = R.getResource("ma_bao_ve_khong_hop_le");
							return ERROR;
						}
					}
					password = StringUtil.generateHash(password, username.trim().toLowerCase());
					Staff staff = staffMgr.getStaffLogin(username, password);
					if (staff == null) {
						errMsg = R.getResource("ten_dang_nhap_hoac_mk_khong_dung");
					} else {
						request.getSession().invalidate();
						request.getSession().setAttribute("attemptLogin", null);
						currentUser = new UserToken();
						currentUser.setUserName(staff.getStaffCode());
						currentUser.setFullName(staff.getStaffName());
						currentUser.setUserId(staff.getId());
						currentUser.setAppCode(AppSetting.getStringValue("domainCode"));
						request.getSession().setAttribute("cmsUserToken", currentUser);
						setSessionLogin();
						
						/** Ban do su dung */
						ApParam sp = apParamMgr.getApParamByCode(Constant.SYS_MAP_TYPE, ApParamType.SYS_CONFIG);
						if (sp != null && ActiveType.RUNNING.equals(sp.getStatus())) {
							request.getSession().setAttribute("mapType", sp.getValue());
						} else {
							request.getSession().setAttribute("mapType", MAP_TYPE_VIETTEL); // ban do Viettel
						}
						//Thuc hien ghi log LOGIN_SUCCESS
						LogUtility.logLogginAndLoguot(null, StringUtil.LOG_LOGIN_SUCCESS, createLogLogginAndLoguot(startLogDate, StringUtil.LOG_LOGIN_ERROR));
						/**
						 * Ghi nhan log xu ly ung dung
						 * 
						 * @author hunglm16
						 * @since August 27, 205
						 * @description Ham nay se phan luong chay song song, khong anh huong den luong he thong
						 * */
						this.createLogFormAccessLogin();
						response.sendRedirect("/index.jsp");
					}
				} else if (StringUtil.isNullOrEmpty(username)) {
					errMsg = R.getResource("chua_dien_tt_dang_nhap");
				} else if (StringUtil.isNullOrEmpty(password)) {
					errMsg = R.getResource("chua_dien_tt_mk");
				}
			}
		} catch (Exception e) {
			LogUtility.logLogginAndLoguot(e, StringUtil.LOG_LOGIN_ERROR, createLogLogginAndLoguot(startLogDate, StringUtil.LOG_LOGIN_ERROR));
		}
		return ERROR;
	}

	/**
	 * Dang xuat he thong
	 * 
	 * @author hunglm16
	 * @since August 21,2014
	 * @return page
	 * @description Xoa tat ca du lieu co trong session, quay tro ve trang Dang
	 *              nhap
	 * */
	public void logout() throws Exception {
		Date startLogDate = DateUtil.now();
		String logoutUrl = AppSetting.getStringValue("loginUrl");
		request.getSession().invalidate();
		/**
		 * Xu ly ghi log len DB
		 * 
		 * @author hunglm16
		 * @since Auguts 27, 2015
		 * */
		/*if (currentUser != null && currentUser.getShopRoot() != null) {
			AppLog appLog = new AppLog();
			Date createLogDate = commonMgr.getSysDate();
			//ID bang {De rong}
			//Ngay ghi log
			appLog.setLogDate(createLogDate);
			//Dia chi IP
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			appLog.setIpAddress(ipAddress);
			//Ma ung dung
			appLog.setAppCode(Configuration.getAppCode());
			//URL Gui den server
			String urlRequest = Configuration.getHostDomain() + StringUtil.APP_LOG_LOGOUT;
			appLog.setUrlRequest(urlRequest);
			//Ngay Tao
			appLog.setCreateDate(createLogDate);
			//Xu ly Log Home
			appLog.setLogType(AppLogTypeEnum.LOGOUT.getValue());
			//ID Nhan vien
			appLog.setStaffId(currentUser.getUserId());
			//Ma Don Vi
			appLog.setShopId(currentUser.getShopRoot().getShopId());
			//ID Nhan vien thay the
			appLog.setIngeritStaffId(currentUser.getStaffRoot().getStaffId());
			//Nguoi tao
			appLog.setCreateUser(currentUser.getUserName());
			//id Vai tro
			appLog.setRoleId(currentUser.getRoleToken().getRoleId());
			commonMgr.createEntity(appLog);
		}*/
		LogUtility.logLogginAndLoguot(null, StringUtil.LOG_LOGOUT_SUCCESS, createLogLogginAndLoguot(startLogDate, StringUtil.LOG_LOGIN_ERROR));
		response.sendRedirect(logoutUrl);
	}

	/**
	 * Thay doi mat khau
	 * 
	 * @author hunglm16
	 * @since August 21,2014
	 * @return page Login
	 * @description
	 * */
	/**
	 * Thay doi mat khau popup o trang login
	 * @author vuongmq 
	 * @return String
	 * @throws Exception
	 * @since 25/08/2015
	 */
	public String changePasswordPopup() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			if (username != null || password != null) {
				if (!StringUtil.isNullOrEmpty(username) && !StringUtil.isNullOrEmpty(password)) {
					if (StringUtil.isNullOrEmpty(newPass)) {
						errMsg = R.getResource("ban_chua_nhap_mat_khau");
						return ERROR;
					}
					if (!newPass.equals(retypedPass)) {
						errMsg = R.getResource("mat_khau_xac_nhan_khong_dung");
						return ERROR;
					}
					password = StringUtil.generateHash(password, username.trim().toLowerCase());
					Staff staff = staffMgr.getStaffLogin(username, password);
					Integer acp = (Integer) request.getSession().getAttribute("attemptChangePassword");
					if (staff == null) {
						errMsg = R.getResource("ten_dang_nhap_hoac_mk_khong_dung");
						if (acp == null) {
							request.getSession().setAttribute("attemptChangePassword", 0);
						} else {
							request.getSession().setAttribute("attemptChangePassword", acp++);
						}
					} else {
						if (acp != null && acp > StringUtil.LOGIN_CAPCHAR_MAX) {
							String sessionToken = CaptchaServlet.getToken(request.getSession());
							String capcha = "";
							if (request.getParameter("capcha") != null) {
								capcha = request.getParameter("capcha").toString();
								capcha = capcha.trim();
							}
							if (!(sessionToken != null && sessionToken.equals(capcha))) {
								errMsg = R.getResource("ma_bao_ve_khong_hop_le");
								return ERROR;
							}
						}
						newPass = StringUtil.generateHash(newPass, username.trim().toLowerCase());
						staff.setPassword(newPass);
						staffMgr.updateStaff(staff, null);
						request.getSession().setAttribute("attemptChangePassword", null);
					}
				} else if (!StringUtil.isNullOrEmpty(username)) {
					errMsg = R.getResource("chua_dien_tt_dang_nhap");
				} else if (!StringUtil.isNullOrEmpty(password)) {
					errMsg = R.getResource("chua_dien_tt_mk");
				}
			} else {
				return ERROR;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.changePasswordPopup()"), createLogErrorStandard(startLogDate));
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * Thay doi mat khau chinh sua moi popup sau khi dang nhap thanh cong
	 * @author vuongmq 
	 * @return String
	 * @throws Exception
	 * @since 25/08/2015
	 */
	public String changePassword() {
		Date startLogDate = DateUtil.now();
		resetToken(result);
		try {
			result.put(ERROR, false);
			if (currentUser == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.login"));
				result.put(ERROR, true);
				return JSON;
			}
			Staff _currStaff = staffMgr.getStaffByCode(currentUser.getUserName());
			if (_currStaff == null) {
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.not.login"));
				result.put(ERROR, true);
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(password)) {
				if (StringUtil.isNullOrEmpty(newPass)) {
					result.put(ERROR, true);
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.missing.new.password");
					result.put("errMsg", errMsg);
					return JSON;
				}
				if (!newPass.equals(retypedPass)) {
					result.put(ERROR, true);
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.retype.password.incorrect");
					result.put("errMsg", errMsg);
					return JSON;
				}
				/*errMsg = ValidateUtil.validateField(newPass, "page.login.new.pwd", 6, ConstantManager.ERR_CHANGE_PASS_NOT_EXIST_UPPER_LOWER);
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				errMsg = ValidateUtil.validateField(retypedPass, "common.password.retype", 6, ConstantManager.ERR_CHANGE_PASS_NOT_EXIST_UPPER_LOWER);
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}*/
				ApParam apParam = apParamMgr.getApParamByCode(ConstantManager.SYS_DEFAULT_PASSWORD, ApParamType.SYS_CONFIG);
				if (apParam != null) {
					if (newPass.equals(apParam.getValue())) {
						result.put(ERROR, true);
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.password.new.same.password.default");
						result.put("errMsg", errMsg);
						return JSON;
					}
				}
				password = StringUtil.generateHash(password, _currStaff.getStaffCode().trim().toLowerCase());
				Staff staff = staffMgr.getStaffLogin(_currStaff.getStaffCode(), password);
				if (staff == null) {
					result.put(ERROR, true);
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.password.incorrect");
					result.put("errMsg", errMsg);
					return JSON;
				} else {
					newPass = StringUtil.generateHash(newPass, _currStaff.getStaffCode().trim().toLowerCase());
					if (!staff.getPassword().equals(newPass)) {
						// password new not same password old
						staff.setPassword(newPass); // da gen
						staffMgr.updateStaff(staff, null);
					} else {
						// error: password new same password old
						result.put(ERROR, true);
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.password.new.same.password.old");
						result.put("errMsg", errMsg);
						return JSON;
					}
				}
			} else {
				result.put(ERROR, true);
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.password.required");
				result.put("errMsg", errMsg);
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.changePassword()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * Search customer.
	 * 
	 * @author nhanlt6
	 * @return the string
	 * @description vuongmq su dung ben dialog(khach hang) chi dinh nhan vien
	 *              giao hang
	 * @date 25/08/2014
	 */
	public String searchCustomerInShopOnly() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Customer> kPaging = new KPaging<Customer>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
			ObjectVO<Customer> customerVO = null;
			if (customerMgr != null) {
				Long parentShopId = currentUser.getShopRoot().getShopId();
				if (shopId != null && shopId != 0) {
					parentShopId = shopId;
				}
				if (shopCode == null && parentShopId != null) {
					Shop shopTemp = shopMgr.getShopById(parentShopId);
					if (shopTemp != null) {
						shopCode = shopTemp.getShopCode();
					}
				}
				if (shopCode == null && parentShopId == null) {
					Shop shopTemp = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
					if (shopTemp != null)
						shopCode = shopTemp.getShopCode();
				}
				ActiveType aStatus = ActiveType.RUNNING;
				if (status != null) {
					if (status < 0) {
						aStatus = null;
					} else {
						aStatus = ActiveType.parseValue(status);
						if (aStatus == null) {
							aStatus = ActiveType.RUNNING;
						}
					}
				}
				customerVO = customerMgr.getListCustomerEx1ByShopId(kPaging, code, name, shopCode, null, address, aStatus);
				if (customerVO != null) {
					result.put("total", customerVO.getkPaging().getTotalRows());
					result.put("rows", customerVO.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchCustomerInShopOnly()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * Search customer.
	 * 
	 * @author nhanlt6 - longnh15 edit phân quyền 
	 * @return the string
	 * @description vuongmq su dung ben dialog(khach hang) chi dinh nhan vien
	 *              giao hang
	 * @date 25/08/2014
	 */
	public String searchCustomerInShopFilter() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<CustomerVO> kPaging = new KPaging<CustomerVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
			ObjectVO<CustomerVO> customerVO = null;
			if (customerMgr != null) {
				Long parentShopId = currentUser.getShopRoot().getShopId();
				if (shopId != null && shopId != 0) {
					Shop shop = shopMgr.getShopById(shopId);
					if (shop != null) {
						shopCode = shop.getShopCode();
					}
				}
				if (shopCode == null && parentShopId != null) {
					Shop shopTemp = shopMgr.getShopById(parentShopId);
					if (shopTemp != null) {
						shopCode = shopTemp.getShopCode();
					}
				}
				if (shopCode == null && parentShopId == null) {
					Shop shopTemp = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
					if (shopTemp != null)
						shopCode = shopTemp.getShopCode();
				}
				ActiveType aStatus = ActiveType.RUNNING;
				if (status != null) {
					if (status < 0) {
						aStatus = null;
					} else {
						aStatus = ActiveType.parseValue(status);
						if (aStatus == null) {
							aStatus = ActiveType.RUNNING;
						}
					}
				}
				customerVO = customerMgr.getListCustomerFilterByShopId(kPaging, code, name, shopCode, null, address, aStatus, getStrListShopId(), cycleId, ksId);
				if (customerVO != null) {
					result.put("total", customerVO.getkPaging().getTotalRows());
					result.put("rows", customerVO.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchCustomerInShopFilter()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	/**
	 * Search product by All.
	 * 
	 * @author hunglm16
	 * @since Sepetember 4,2014
	 * @return the string
	 */
	public String searchProductAll() {
		Date startLogDate = DateUtil.now();
		try {
			ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
			result.put("total", 0);
			result.put("rows", new ArrayList<Product>());

			ProductFilter filter = new ProductFilter();

			KPaging<Product> kPaging = new KPaging<Product>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);

			filter.setkPaging(kPaging);
			filter.setProductCode(code);
			filter.setProductName(name);
			filter.setStatusInt(status);
			filter.setStatus(ActiveType.RUNNING);
			if (longType != null && longType > -1) {
				filter.setCatId(longType);
			}

			//Bo sung lay theo nganh hang
			if (!StringUtil.isNullOrEmpty(catCode)) {
				List<String> lstCatCodes = null;
				if (catCode.startsWith("-1")) {
					lstCatCodes = null;
				} else if (catCode.contains(",")) {
					String arr[] = catCode.split(",");
					lstCatCodes = new ArrayList<String>();
					for (String str : arr) {
						lstCatCodes.add(str.toUpperCase());
					}
				} else {
					lstCatCodes = new ArrayList<String>();
					lstCatCodes.add(catCode.toUpperCase());
				}
				filter.setLstCategoryCodes(lstCatCodes);
			}

			ObjectVO<Product> productVO = productMgr.getAllListProductEx(filter);

			if (productVO != null) {
				result.put("total", productVO.getkPaging().getTotalRows());
				result.put("rows", productVO.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchProductAll()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Search Staff by All.
	 * 
	 * @author hunglm16
	 * @since Sepetember 4,2014
	 * @return the string
	 */
	public String searchStaffShowList() {
		Date startLogDate = DateUtil.now();
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<ShopVO>());

			if (currentUser != null && currentUser.getStaffRoot() != null) {
				StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
				if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
					filter.setIsLstChildStaffRoot(true);
				} else {
					filter.setIsLstChildStaffRoot(false);
				}

				KPaging<StaffVO> kPaging = new KPaging<StaffVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				filter.setkPaging(kPaging);
				filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());

				if (parentStaffId != null) {
					filter.setParentStaffId(parentStaffId);
				}

				if (!StringUtil.isNullOrEmpty(code)) {
					filter.setStaffCode(code);
				}

				///Hai tham so truyen co the khac nhau tuy vao dialog
				if (!StringUtil.isNullOrEmpty(name)) {
					filter.setStaffName(name);
				}

				if (!StringUtil.isNullOrEmpty(arrParentCodeStr)) {
					String[] arrStrPcode = arrParentCodeStr.split(",");
					if (arrStrPcode != null && arrStrPcode.length > 0) {
						List<String> lstPCode = new ArrayList<String>();
						for (String value : arrStrPcode) {
							lstPCode.add(value);
						}
						filter.setLstParentStaffCode(lstPCode);
					}
				}

				if (status != null) {
					filter.setStatus(status);
				} else {
					filter.setStatus(ActiveType.RUNNING.getValue());
				}

				List<Long> lstShopId = new ArrayList<Long>();
				if (shopId != null) {
					if (checkShopInOrgAccessById(shopId)) {
						lstShopId.add(shopId);
					} else {
						return JSON;
					}
				} else if (!StringUtil.isNullOrEmpty(arrIdStr)) {
					String[] arrShopIdTemp = arrIdStr.split(",");
					if (arrShopIdTemp != null && arrShopIdTemp.length > 0) {
						for (String value : arrShopIdTemp) {
							Long shopIdT = Long.valueOf(value);
							if (!StringUtil.isNullOrEmpty(value) && checkShopInOrgAccessById(shopIdT)) {
								lstShopId.add(shopIdT);
							}
						}
						if (lstShopId == null || lstShopId.size() == 0) {
							return JSON;
						}
					}
				} else {
					lstShopId.add(currentUser.getShopRoot().getShopId());
				}
				filter.setLstShopId(lstShopId);
				//filter.setInheritUserPriv(currentUser.getStaffRoot().getStaffId());
				if (objectType != null) {
					filter.setObjectType(objectType);
					//Khong phan quyen Parent_staff_map voi Nhan vien thu tien, Nhan vien giao hang
					if (StaffObjectType.NVTT.getValue().equals(objectType) || StaffObjectType.NVGH.getValue().equals(objectType)) {
						filter.setIsLstChildStaffRoot(false);
						/*
						 * patch: nghiep vu hien tai khong phan biet NVBH & NVTT
						 * @modified by tuannd20
						 * @date 14/11/2014
						 */
						filter.setLstObjectType(Arrays.asList(StaffObjectType.NVGH.getValue(), StaffObjectType.NVTT.getValue()));
						filter.setObjectType(null);
					}
				}
				if (!StringUtil.isNullOrEmpty(lstObjectType)) { // lacnv1: them tham so ds object_type
					String[] arr = lstObjectType.split(",");
					List<Integer> lstOType = new ArrayList<Integer>();
					for (String s : arr) {
						lstOType.add(Integer.valueOf(s.trim()));
					}
					if (lstOType.size() > 0) {
						filter.setLstObjectType(lstOType);
					}
				}

				ObjectVO<StaffVO> objVO = staffMgr.searchListStaffVOByFilter(filter);
				if (objVO != null) {
					result.put("total", objVO.getkPaging().getTotalRows());
					result.put("rows", objVO.getLstObject());
				}
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchStaffShowList()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Search Customer by All.
	 * 
	 * @author hunglm16
	 * @since Sepetember 4,2014
	 * @return the string
	 * @description Phan quyen theo CMS
	 */
	public String searchCustomerShowList() {
		Date startLogDate = DateUtil.now();
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<CustomerVO>());

			CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");

			CustomerFilter<CustomerVO> filter = new CustomerFilter<CustomerVO>();
			KPaging<CustomerVO> kPaging = new KPaging<CustomerVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);

			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setShortCode(code);
			}
//			if (!StringUtil.isNullOrEmpty(name)) {
//				filter.setCustomerName(name);
//			}

			if (status != null) {
				filter.setStatus(status);
			} else {
				filter.setStatus(ActiveType.RUNNING.getValue());
			}

			List<Long> lstShopId = new ArrayList<Long>();
			if (shopId != null) {
				if (checkShopInOrgAccessById(shopId)) {
					lstShopId.add(shopId);
				} else {
					return JSON;
				}
			} else if (!StringUtil.isNullOrEmpty(arrIdStr)) {
				String[] arrShopIdTemp = arrIdStr.split(",");
				if (arrShopIdTemp != null && arrShopIdTemp.length > 0) {
					for (String value : arrShopIdTemp) {
						Long shopIdT = Long.valueOf(value);
						if (!StringUtil.isNullOrEmpty(value) && checkShopInOrgAccessById(shopIdT)) {
							lstShopId.add(shopIdT);
						}
					}
					if (lstShopId == null || lstShopId.size() == 0) {
						return JSON;
					}
				}
			} else if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					lstShopId.add(shop.getId());
				} else {
					return JSON;
				}
			} else {
				lstShopId.add(currentUser.getShopRoot().getShopId());
			}
			filter.setLstShopId(lstShopId);

			if (objectType != null) {
				filter.setObjectType(objectType);
			}

			ObjectVO<CustomerVO> objVO = customerMgr.searchListCustomerVOByFilter(filter);
			if (objVO != null) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchCustomerShowList()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * Search Customer by All.
	 * 
	 * @author hunglm16
	 * @since Sepetember 4,2014
	 * @return the string
	 * @description Phan quyen theo CMS
	 */
	public String searchCustomerShowListForRouting() {
		Date startLogDate = DateUtil.now();
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<CustomerVO>());
			CustomerFilter<CustomerVO> filter = new CustomerFilter<CustomerVO>();

			CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
			if (currentUser.getListUser() != null && !currentUser.getListUser().isEmpty()) {
				filter.setIsFlagCMS(true);
			} else {
				filter.setIsFlagCMS(false);
			}
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			KPaging<CustomerVO> kPaging = new KPaging<CustomerVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);

			if (!StringUtil.isNullOrEmpty(code)) {
				filter.setShortCode(code);
			}
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setCustomerName(name);
			}

			if (status != null && status > -1) {
				filter.setStatus(status);
			} else {
				filter.setStatus(ActiveType.RUNNING.getValue());
			}

			List<Long> lstShopId = new ArrayList<Long>();
			if (shopId != null) {
				if (checkShopInOrgAccessById(shopId)) {
					lstShopId.add(shopId);
				} else {
					return JSON;
				}
			} else if (!StringUtil.isNullOrEmpty(arrIdStr)) {
				String[] arrShopIdTemp = arrIdStr.split(",");
				if (arrShopIdTemp != null && arrShopIdTemp.length > 0) {
					for (String value : arrShopIdTemp) {
						Long shopIdT = Long.valueOf(value);
						if (!StringUtil.isNullOrEmpty(value) && checkShopInOrgAccessById(shopIdT)) {
							lstShopId.add(shopIdT);
						}
					}
					if (lstShopId == null || lstShopId.size() == 0) {
						return JSON;
					}
				}
			} else {
				lstShopId.add(currentUser.getShopRoot().getShopId());
			}
			filter.setLstShopId(lstShopId);

			if (objectType != null) {
				filter.setObjectType(objectType);
			}

			if (!StringUtil.isNullOrEmpty(staffCode)) {
				filter.setStaffCode(staffCode);
			}
			
			if (checkInCurrentWeek != null && checkInCurrentWeek == 1) {
				filter.setpDate(DateUtil.now());
			}

			ObjectVO<CustomerVO> objVO = customerMgr.searchListCustomerVOForRoutingByFilter(filter);
			if (objVO != null) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchCustomerShowListForRouting()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	public String getCustomerInfo() {
		Date startLogDate = DateUtil.now();
		try {
			CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
			customerVo = customerMgr.getCustomerVO(customerId);
			List<ShopVO> shopVOParam = new ArrayList<ShopVO>();
			Long shopId = currentUser.getShopRoot().getShopId();
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					shopId = shop.getId();
				}
			}
			if ( null != customerVo.getShopId()) {
				shopVOParam = shopMgr.getListShopVOConfigShowPrice(shopId , status);
			}
			
			if ( shopVOParam.size() > 0 ) {
				customerVo.setValueShowPrice(shopVOParam.get(0).getValue());
				isShow = shopVOParam.get(0).getValue();
				if ( customerVo.getValueShowPrice() == 2 ) { // 2: hien doanh so ra
					customerVo.setIsShowPrice(null);
				} else { // 1 : an doanh so di
					customerVo.setIsShowPrice(1);  
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_product_customer_cau_hinh_gia"));
			}
			if ( customerVo != null ) {
				result.put("customerCode", customerVo.getCustomerCode());
				result.put("customerShortCode", customerVo.getShortCode());
				result.put("customerName", customerVo.getCustomerName());
				result.put("address", customerVo.getAddress());
				result.put("phone", customerVo.getPhone());
				result.put("mobiphone", customerVo.getMobiphone());
				result.put("shopTypeName", customerVo.getShopTypeName());
				result.put("loyalty", customerVo.getLoyalty());
				result.put("contactName", customerVo.getContactName());
				result.put("totalInDate", customerVo.getTotalInDate());
				result.put("numSkuInDate", customerVo.getNumSkuInDate());
				result.put("numOrderInMonth", customerVo.getNumOrderInMonth());
				result.put("avgTotalInLastTwoMonth", customerVo.getAvgTotalInLastTwoMonth());
				result.put("totalInMonth", customerVo.getTotalInMonth());
				result.put("quantityInDate", customerVo.getQuantityInDate());
				result.put("avgQuantityInLastTwoMonth", customerVo.getAvgQuantityInLastTwoMonth());
				result.put("quantityInMonth", customerVo.getQuantityInMonth());
				result.put("shopId", customerVo.getShopId());
				result.put("isShowPrice",customerVo.getValueShowPrice());
			}
			
			if ( !StringUtil.isNullOrEmpty(typeReturn) ) {
				return SUCCESS;
			}
		} catch ( Exception e ) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getCustomerInfo()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Search EquipItemPopUpRepair show list 
	 * @author vuongmq
	 * @since 65/04/2015
	 * @return the string
	 */
	public String searchEquipItemPopUpRepair() {
		actionStartTime = DateUtil.now();
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<EquipItemVO>());
			KPaging<EquipItemVO> kPaging = new KPaging<EquipItemVO>();
			EquipItemFilter filter = new EquipItemFilter();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			if (!StringUtil.isNullOrEmpty(name)) {
				filter.setName(name);
			}
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				//Equipment equip = equipmentManagerMgr.getEquipmentEntityByCode(equipCode);
				EquipmentFilter<Equipment> filterE = new EquipmentFilter<Equipment>();
				/** vuongmq; 27/05/2015; them ma NPP; va staff dang nhap */
				if (currentUser != null) {
					filterE.setStaffRoot(currentUser.getUserId());
					if (currentUser.getShopRoot() != null) {
						filterE.setShopRoot(currentUser.getShopRoot().getShopId());
					} else {
						// common.cms.shop.undefined  = Không tìm thấy Đơn vị tương ứng với quyền được cấp.
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("common.cms.shop.undefined"));
						return JSON;
					}
				} else {
					// loi khong ton tai user dang nhap
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.not.login"));
					return JSON;
				}
				filterE.setCode(equipCode);
				filterE.setStaffRoot(currentUser.getUserId());
				/** vuongmq; 10/06/2015; lay so lan sua chua thiet bi cho chinh xac; Defect 227832; chi can truyen Ma thiet bi de lay equip_id la OK*/
				/*filterE.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
				filterE.setTradeType(null);*/
				Equipment equip = equipmentManagerMgr.getEquipmentByCodeFilter(filterE);
				if (equip != null) {
					filter.setEquipId(equip.getId());
				}
			}
			if (!StringUtil.isNullOrEmpty(strEquip)) {
				String[] array = strEquip.split(",");
				List<Long> lstEquipItemIdExcept = new ArrayList<Long>();
				for (int i = 0, sz = array.length; i < sz; i++) {
					lstEquipItemIdExcept.add(Long.valueOf(array[i].trim()));
				}
				filter.setLstEquipItemIdExcept(lstEquipItemIdExcept);
			}
			ObjectVO<EquipItemVO> objVO = equipmentManagerMgr.getListEquipItemPopUpRepair(filter);
			if (objVO != null && objVO.getLstObject() != null && objVO.getLstObject().size() > 0) {
				result.put("total", objVO.getkPaging().getTotalRows());
				result.put("rows", objVO.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchEquipItemPopUpRepair()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	
	public boolean isShowPrice() {
		return isShowPrice;
	}

	public void setShowPrice(boolean isShowPrice) {
		this.isShowPrice = isShowPrice;
	}

	public String getListSaleOrderByCustomer() {
		Date startLogDate = DateUtil.now();
		try {
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
			kPaging.setPageSize(5);
			SaleOrderMgr saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
			SoFilter soFilter = new SoFilter();
			
			soFilter.setApproval(SaleOrderStatus.APPROVED);
			soFilter.setCustomerId(customerId);
			soFilter.setIsCheckOrderDateDesc(true);
			soFilter.setkPaging(kPaging);
			ObjectVO<SaleOrder> saleOrderVo = saleOrderMgr.getListSaleOrderEx1(soFilter);
			
			Long shopId = currentUser.getShopRoot().getShopId();
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					shopId = shop.getId();
				}
			}
			
			if (saleOrderVo != null) {
				List<ShopVO> shopVOParam = new ArrayList<ShopVO>();
				if ( null != shopCode ) {
					shopVOParam = shopMgr.getListShopVOConfigShowPrice(shopId , status);
				}
				if ( shopVOParam.size() > 0 ) {
					if (saleOrderVo.getLstObject().size() > 0) {
						saleOrderVo.getLstObject().get(0).setIsShowPrice(shopVOParam.get(0).getValue());
					}
				} else {
					result.put(ERROR, true);
					result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_product_customer_cau_hinh_gia"));
				}
				result.put("total", saleOrderVo.getLstObject().size());
				result.put("rows", saleOrderVo.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListSaleOrderByCustomer()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Tim kiem SP
	 * 
	 * @return the string
	 * @author lamnh
	 * @since 30/07/2012
	 */
	public String searchProduct() throws Exception {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Product> kPaging = new KPaging<Product>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");

			ProductFilter filter = new ProductFilter();
			filter.setkPaging(kPaging);
			filter.setProductCode(code);
			filter.setProductName(name);
			filter.setStatus(ActiveType.RUNNING);
			filter.setIsExceptZCat(true);
			filter.setCheckBrand(checkBrand);
			if (!StringUtil.isNullOrEmpty(lstCategoryCodes)) {
				String arr[] = lstCategoryCodes.split(",");
				List<String> lstCodes = new ArrayList<String>();
				for (int i = 0; i < arr.length; i++) {
					if (arr[i] != null && !"-1".equals(arr[i].trim())) {
						lstCodes.add(arr[i].trim());
					} else {
						lstCodes = null;
						break;
					}
				}
				filter.setLstCategoryCodes(lstCodes);
			}

			ObjectVO<Product> productVO = productMgr.getListProductEx(filter);

			if (productVO != null) {
				result.put("total", productVO.getkPaging().getTotalRows());
				result.put("rows", productVO.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchProduct()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Search Staff by All.
	 * 
	 * @author hunglm16
	 * @since Sepetember 4,2014
	 * @return the string
	 */
	public String searchStaffForUnitTree() {
		Date startLogDate = DateUtil.now();
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<Staff>());

			KPaging<Staff> kPaging = new KPaging<Staff>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (currentUser != null && currentUser.getStaffRoot() != null) {
				StaffFilter filter = new StaffFilter();
				filter.setStatus(ActiveType.RUNNING);
				filter.setLstChannelObjectType(Arrays.asList(objectType));
				filter.setkPaging(kPaging);
				filter.setStaffName(name);
				filter.setStaffCode(code);
				if (StaffObjectType.NVBH.getValue().equals(objectType)) {
					filter.setLstChannelObjectType(Arrays.asList(StaffObjectType.NVBH.getValue(), StaffObjectType.NVVS.getValue()));
				}
				ObjectVO<Staff> vo = staffMgr.getListStaff(filter);
				if (vo != null) {
					result.put("total", vo.getkPaging().getTotalRows());
					result.put("rows", vo.getLstObject());
				}
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.searchStaffForUnitTree()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Search CTTB cho bc 7.6.5
	 * 
	 * @author tungmt
	 * @since 08/10/2014
	 * @return the string
	 */
	public String getListDisplayProgramInTowDay() {
		Date startLogDate = DateUtil.now();
		//Lay danh sach CTTB dua theo nam
		result.put("page", page);
		result.put("rows", rows);
		if (rows == 0)
			rows = 10;
		if (page == 0)
			page = 1;
		DisplayProgrameMgr displayProgrameMgr = (DisplayProgrameMgr) context.getBean("displayProgrameMgr");
		try {
			Staff currentStaff = null;
			if (currentUser != null) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null) {
					return JSON;
				}
				if (shopId == null) {
					shopId = currentUser.getShopRoot().getShopId();
				}
			} else {
				return JSON;
			}
			KPaging<StDisplayProgramVNM> kPaging = new KPaging<StDisplayProgramVNM>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<StDisplayProgramVNM> displayProgramVO = new ObjectVO<StDisplayProgramVNM>();

			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);

			displayProgramVO = displayProgrameMgr.getListDisplayProgramInTowDate(kPaging, shopId, fDate, tDate, code, name);
			if (displayProgramVO != null) {
				result.put("total", displayProgramVO.getkPaging().getTotalRows());
				result.put("rows", displayProgramVO.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<StDisplayProgramVNM>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListDisplayProgramInTowDay()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * Tim kiem danh sach lich su giao nhan
	 * 
	 * @author phuongvm
	 * @since 16/12/2014
	 * @return
	 */
	public String searchHistoryEquipRecordVO() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipRecordVO>());
		try {
			EquipmentManagerMgr equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
			EquipRecordFilter<EquipRecordVO> filter = new EquipRecordFilter<EquipRecordVO>();
			if (flagPagination != null && flagPagination) {
				KPaging<EquipRecordVO> kPaging = new KPaging<EquipRecordVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				filter.setkPaging(kPaging);
			}
			if (objectId != null) {
				filter.setEquipId(objectId);
			}
			if (objectType != null) {
				filter.setRecordType(objectType);
			}
			ObjectVO<EquipRecordVO> objVO = equipmentManagerMgr.getHistoryEquipRecordByFilter(filter);
			if (objVO != null && objVO.getLstObject() != null && !objVO.getLstObject().isEmpty()) {
				if (flagPagination != null && flagPagination) {
					result.put("total", objVO.getkPaging().getTotalRows());
				}
				result.put("rows", objVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**	 
	 * Lay danh sach shop theo filter
	 * @author trungtm6
	 * @since 21/04/2015
	 * @return
	 * 
	 * @modify hunglm16
	 * @since 24/11/2015
	 * @desscription Bo sung phan quyen
	 */
	public String getListShopFilter() {
		Date startLogDate = DateUtil.now();
		result.put("rows", new ArrayList<Shop>());
		result.put("total", 0);
		try {
			ShopFilter shopFilter = new ShopFilter();
			shopFilter.setShopId(getCurrentShop().getId());
			shopFilter.setSpecType(ShopSpecificType.NPP);
			shopFilter.setStrShopId(super.getStrListShopId());
			shopFilter.setCycleId(cycleId);
			
			shopFilter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			shopFilter.setRoleId(currentUser.getRoleToken().getRoleId());
			shopFilter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<Shop> vo = shopMgr.getListShop(shopFilter);
			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
				result.put("rows", vo.getLstObject());
				result.put("total", vo.getkPaging().getTotalRows());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListShopFilter()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * Ham lay danh sach giam sat theo shopn
	 * 
	 * @author hoanv25
	 * @since July 09, 2015
	 * @description creat
	 */
	public String getListGSByShopId() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			ImageFilter filter=new ImageFilter();
			filter.setShopIdListStr(getStrListShopId());
			StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
			arrGSVO = new ArrayList<ImageVO>();
			if (shopId != null && checkShopInOrgAccessById(shopId)) {
				arrGSVO = staffMgr.getListGSByNPP(shopId, filter);
			} else if (shopId == null && currentUser != null && currentUser.getShopRoot() != null) {
				shopId = currentUser.getShopRoot().getShopId();
				arrGSVO = staffMgr.getListGSByNPP(shopId, filter);
			} else {
				arrGSVO = new ArrayList<ImageVO>();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListGSByShopId()"), createLogErrorStandard(startLogDate));
			arrGSVO = new ArrayList<ImageVO>();
		}
		return JSON;
	}
	
	/**
	 * Ham lay danh sach NVBH theo shop
	 * 
	 * @author hoanv25
	 * @since July 09, 2015
	 * @description creat
	 */
	public String getListNVBHByShopId() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			ImageFilter filter=new ImageFilter();
			filter.setShopIdListStr(getStrListShopId());
			StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
			arrNVBHVO = new ArrayList<ImageVO>();
			if (this.currentUser != null) {
				if ((this.lstShopId != null) && (this.lstShopId.size() > 0)) {
					arrNVBHVO = staffMgr.getListNVBHByNPPShop(lstShopId, filter);
				} else {
					shopId = currentUser.getShopRoot().getShopId();
					arrNVBHVO = staffMgr.getListNVBHByNPP(shopId, filter);
				}
			}
			 
		/*	if (shopId != null && checkShopInOrgAccessById(shopId)) {
				arrNVBHVO = staffMgr.getListNVBHByNPP(shopId, filter);
			} else if (shopId == null && currentUser != null && currentUser.getShopRoot() != null) {
				shopId = currentUser.getShopRoot().getShopId();
				arrNVBHVO = staffMgr.getListNVBHByNPP(shopId, filter);
			} else {
				arrNVBHVO = new ArrayList<ImageVO>();
			}*/
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListNVBHByShopId()"), createLogErrorStandard(startLogDate));
			arrNVBHVO = new ArrayList<ImageVO>();
		}
		return JSON;
	}
	
	/**
	 * Ham lay danh sach san pham theo nganh hang 
	 * 
	 * @author hoanv25
	 * @since July 10, 2015
	 * @description creat
	 */
	public String getListProductByCat() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			ImageFilter filter = new ImageFilter();
			filter.setShopIdListStr(getStrListShopId());
			ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
			arrProductVo = new ArrayList<ImageVO>();
			if (idParentCat != null && idParentSubCat != null) {
				arrProductVo = productMgr.getListParentCatByCatAndSubCat(idParentCat, idParentSubCat);
			}
			if ((idParentSubCat != null && idParentCat == null) || (idParentSubCat != null && idParentCat == 0)) {
				arrProductVo = productMgr.getListParentCatBySubCat(idParentSubCat);
			} else {
				arrProductVo = productMgr.getListParentCatByCat(idParentCat);
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListProductByCat()"), createLogErrorStandard(startLogDate));
			arrProductVo = new ArrayList<ImageVO>();
		}
		return JSON;
	}
	
	/**	 
	 * Lay danh sach staff theo filter
	 * @author trungtm6
	 * @since 25/04/2015
	 * @return
	 */
	public String getListStaffFilter() {
		Date startLogDate = DateUtil.now();
		result.put("rows", new ArrayList<Staff>());
		result.put("total", 0);
		try {
			StaffFilter filter = new StaffFilter();
			Shop curShop = getCurrentShop();
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop sh = shopMgr.getShopByCode(shopCode);
				if (sh != null) {
					curShop = sh;
				}
			}
			filter.setShopId(curShop.getId());
			if (stSpecType != null && stSpecType >= 0) {
				filter.setSpecType(StaffSpecificType.parseValue(stSpecType));
			}

			ObjectVO<Staff> vo = staffMgr.getListStaff(filter);
			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
				result.put("rows", vo.getLstObject());
				result.put("total", vo.getkPaging().getTotalRows());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListStaffFilter()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/***
	 * Load list distributor by filter
	 * @author trungtm6 
	 * @since 23/04/2015
	 * @return
	 */
	public String getListChildShop() {
		Date startLogDate = DateUtil.now();
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<Shop>());

			KPaging<Shop> kPaging = new KPaging<Shop>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null && currentUser.getShopRoot() != null) {
				ShopFilter filter = new ShopFilter();
				filter.setStatus(ActiveType.RUNNING);
				filter.setSpecType(ShopSpecificType.NPP);
				if (shopId != null) {
					filter.setShopId(shopId);
				} else {
					filter.setShopId(currentUser.getShopRoot().getShopId());
				}
				filter.setStaffRootId(currentUser.getUserId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				filter.setShopRootId(currentUser.getShopRoot().getShopId());
				filter.setStrShopId(super.getStrListShopId());
				filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				filter.setShopRootId(currentUser.getShopRoot().getShopId());
				ObjectVO<Shop> vo = shopMgr.getListChildShop(filter);
				List<Shop> rows = new ArrayList<Shop>();
				if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0){
					rows = vo.getLstObject();
				}
				if (Boolean.TRUE.equals(isChoiceAll)) {
					Shop all = new Shop();
					all.setShopName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.status.all"));
					rows.add(0, all);
				}
				result.put("rows", rows);
			}

		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListChildShop()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	public String getListCycleFilter() {
		Date startLogDate = DateUtil.now();
		result.put("lstCycle", new ArrayList<CycleVO>());
		result.put("currentNum", null);
		try {
			CycleFilter filter = new CycleFilter();
			if (year != null) {
				filter.setYear(year);
			}
			if (numPeriod != null) {
				filter.setNum(numPeriod);
			}
			List<CycleVO> lstCycle = cycleMgr.getListCycleByFilter(filter);
			if (lstCycle != null && lstCycle.size() > 0) {
//				for (CycleVO cycleVO : lstCycle) {
//					cycleVO.setCycleName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale_plan_period") + " " + cycleVO.getNum());
//				}
				if (Boolean.TRUE.equals(isChoiceAll)) {
					CycleVO all = new CycleVO();
					all.setCycleName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.status.all"));
					lstCycle.add(0, all);
				}
				result.put("lstCycle", lstCycle);
			}
			Cycle curCycle = cycleMgr.getCycleByDate(commonMgr.getSysDate());
			if (curCycle != null) {
				result.put("currentNum", curCycle.getNum());
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListCycleFilter()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/***
	 * @author vuongmq
	 * @date 03/08/2015
	 * @description lay ngay chot khi select shopCode trong cbxShop
	 */
	public String viewDayLockShop() {
		Date startLogDate = DateUtil.now();
		try {
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					Date lockD = shopLockMgr.getNextLockedDay(shop.getId());
					if (lockD  == null) {
						lockD = commonMgr.getSysDate();
					}
					lockDate = DateUtil.convertDateByString(lockD, DateUtil.DATE_FORMAT_DDMMYYYY);
					result.put("lockDate", lockDate); // ngay chot, la ngay lam viec
					Date tDate = lockD;
					Date fDate = DateUtil.getFirstDateInMonth(tDate);//DateUtil.addDate(tDate, -30);
					fromDate = DateUtil.toDateString(fDate, DateUtil.DATE_FORMAT_DDMMYYYY);
					toDate = lockDate;//DateUtil.toDateString(tDate, DateUtil.DATE_FORMAT_STR);
					result.put("fromDate", fromDate); // ngay dau thang cua ngay chot
					result.put("toDate", toDate); // ngay cua ngay chot
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.viewDayLockShop()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * View thong tin ShopVO; dung cho upParentShop ben CN: GSBH
	 * @author vuongmq
	 * @return String
	 * @since 23/09/2015
	 */
	public String viewShopInfo() {
		Date startLogDate = DateUtil.now();
		try {
			Shop shop = null;
			if (shopId != null && shopId > 0) {
				shop = shopMgr.getShopById(shopId);
				if (shop == null) {
					result.put("errMsg", R.getResource("common.catalog.code.not.exist", R.getResource("catalog.unit.tree")));
					result.put(ERROR, true);
					return SUCCESS;
				}
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
					return JSON;
				}
				ShopVO shopVO = new ShopVO();
				shopVO.setShopId(shop.getId());
				shopVO.setShopCode(shop.getShopCode());
				shopVO.setShopName(shop.getShopName());
				if (shop.getParentShop() != null && currentUser.getShopRoot() != null && shop.getId() != null 
						&& !shop.getId().equals(currentUser.getShopRoot().getShopId())) {
					Shop shopParent = shop.getParentShop();
					if (super.getMapShopChild().get(shopParent.getId()) == null) {
						result.put("errMsg", R.getResource("common.cms.shop.undefined.ex.param", shopParent.getShopCode()));
					} else {
						shopVO.setParentShopId(shopParent.getId());
						shopVO.setParentShopCode(shopParent.getShopCode());
						shopVO.setParentShopName(shopParent.getShopName());
					}
				}
				result.put("shop", shopVO);
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.viewShopInfo()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * loadProgramKeyShop load key shop
	 * @author vuongmq
	 * @since 28/10/2015
	 */
	public String loadProgramKeyShop() {
		Date startLogDate = DateUtil.now();
		try {
			KeyShopFilter filter = new KeyShopFilter();
			filter.setSort("toCycleId");
			filter.setOrder("desc");
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setStrLstShopId(super.getStrListShopId());
			if (shopId != null && super.getMapShopChild().get(shopId) != null) {
				filter.setShopId(shopId);
			} else if (currentUser.getShopRoot() != null) {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			} else {
				return JSON;
			}
			if (currentUser.getShopRoot() != null && shopId != null && shopId.equals(currentUser.getShopRoot().getShopId())) {
				//neu chon la shop_root (tat ca) thi se xet theo admin or createuser
				filter.setCreateUser(currentUser.getUserName());
				if (currentUser.getStaffRoot() != null) {
					filter.setIsAdmin(StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()));
				}
			}
			filter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setShopRootId(currentUser.getShopRoot().getShopId());
			keyShopMgr = (KeyShopMgr) context.getBean("keyShopMgr");
			ObjectVO<KeyShopVO> vo = keyShopMgr.getListKeyShopVOByFilter(filter);
			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
				List<KeyShopVO> lstKeyShop = vo.getLstObject();
				if (Boolean.TRUE.equals(isChoiceAll)) {
					KeyShopVO all = new KeyShopVO();
					all.setKsId(ActiveType.DELETED.getValue().longValue());
					all.setKsCode(R.getResource("common.select.program"));
					all.setKsName(R.getResource("common.select.program"));
					lstKeyShop.add(0, all);
				}
				result.put("rows", lstKeyShop);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "web.action.General.loadProgramKeyShop()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * viewCycleKeyShop view danh sach cycle key shop
	 * @author vuongmq
	 * @since 28/10/2015
	 */
	public String viewCycleKeyShop() {
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser != null && currentUser.getStaffRoot() != null) {
				keyShopMgr = (KeyShopMgr) context.getBean("keyShopMgr");
				KS keyShop = keyShopMgr.getKSById(ksId);
				if (keyShop == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("keyshop.error.not.exist.keyshop"));
					return JSON;
				}
				CycleFilter filter = new CycleFilter();
				filter.setBeginDate(keyShop.getFromCycle() != null ? keyShop.getFromCycle().getBeginDate() : null);
				filter.setEndDate(keyShop.getToCycle() != null ? keyShop.getToCycle().getEndDate() : null);
				filter.setStatus(ActiveType.RUNNING);
				List<CycleVO> lstCycle = cycleMgr.getListCycleByFilter(filter);
				if (lstCycle != null && lstCycle.size() > 0) {
					result.put("lstCycle", lstCycle);
				}
				result.put("fromCycleId", keyShop.getFromCycle() != null ? keyShop.getFromCycle().getId() : "");
				result.put("toCycleId", keyShop.getToCycle() != null ? keyShop.getToCycle().getId() : "");
			} else {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.session.expired"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "web.action.General.viewCycleKeyShop()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Ham lay danh sach StaffVO theo phan quyen shop root
	 * @author vuongmq
	 * @since 13/11/2015
	 */
	public String getListStaffVOInherit() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			result.put("rows", new ArrayList<StaffVO>());
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getRoleToken() != null) {
				UnitFilter unitFilter = new UnitFilter();
				unitFilter.setManageUnitId(shopId);
				unitFilter.setShopRootId(currentUser.getShopRoot().getShopId());
				unitFilter.setStaffRootId(currentUser.getUserId());
				unitFilter.setRoleId(currentUser.getRoleToken().getRoleId());
				if (typeCurrentUser != null && StaffSpecificType.STAFF.getValue().equals(typeCurrentUser)) {
					unitFilter.setIsCurrentUser(true);
				}
				List<StaffVO> lst = staffMgr.getListStaffVOInherit(null, unitFilter);
				if (lst != null) {
					if (Boolean.TRUE.equals(isChoiceAll)) {
						StaffVO all = new StaffVO();
						all.setStaffId(ActiveType.DELETED.getValue().longValue());
						//all.setStaffCode(R.getResource("sale_plan_choose_staff"));
						all.setStaffName(R.getResource("sale_plan_choose_staff"));
						lst.add(0, all);
					}
					result.put("rows", lst);
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListStaffVOInherit()"), createLogErrorStandard(startLogDate));
			result.put("rows", new ArrayList<StaffVO>());
		}
		return JSON;
	}
	
	/**
	 * Ham lay danh sach StaffType theo phan quyen shop root, don vi chon
	 * @author vuongmq
	 * @since 18/11/2015
	 */
	public String getListStaffType() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			result.put("rows", new ArrayList<StaffType>());
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getRoleToken() != null) {
				UnitFilter unitFilter = new UnitFilter();
				unitFilter.setManageUnitId(shopId);
				unitFilter.setShopRootId(currentUser.getShopRoot().getShopId());
				unitFilter.setStaffRootId(currentUser.getUserId());
				unitFilter.setRoleId(currentUser.getRoleToken().getRoleId());
				if (type != null && StaffSpecificType.STAFF.getValue().equals(type)) {
					// lay luon staff_type cua current user
					unitFilter.setIsCurrentUser(true);
				}
				List<StaffType> lst = staffMgr.getListStaffType(unitFilter);
				if (lst != null) {
					result.put("rows", lst);
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.getListStaffType()"), createLogErrorStandard(startLogDate));
			result.put("rows", new ArrayList<StaffType>());
		}
		return JSON;
	}
	
	/**
	 * Tim hinh anh bien ban thiet bi
	 * 
	 * @author hunglm16
	 * @since August 16,2014
	 * @return List Shop
	 */
	public String searchEquipAttachFile() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipAttachFile>());
		try {
			EquipRecordMgr equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
			BasicFilter<EquipAttachFile> filter = new BasicFilter<EquipAttachFile>();
			if (flagPagination != null && flagPagination) {
				KPaging<EquipAttachFile> kPaging = new KPaging<EquipAttachFile>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				filter.setkPaging(kPaging);
			}

			if (objectId != null) {
				filter.setObjectId(objectId);
			}

			if (objectType != null) {
				filter.setObjectType(objectType);
			}

			ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
			objVO = equipRecordMgr.searchEquipAttachFileByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				if (flagPagination != null && flagPagination) {
					result.put("total", objVO.getkPaging().getTotalRows());
				}
				result.put("rows", objVO.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<WarehouseVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Download EquipAttachFile
	 * 
	 * @author hunglm16
	 * @since Feb 03,2014
	 * */
	public String dowloadEquipAttachFile() {
		try {
			/*vuongmq; 07/05/2015; ATTT xuat file*/
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			EquipRecordMgr equipRecordMgr = (EquipRecordMgr) context.getBean("equipRecordMgr");
			BasicFilter<EquipAttachFile> filter = new BasicFilter<EquipAttachFile>();
			if (objectId != null) {
				filter.setObjectId(objectId);
			}
			if (id != null) {
				filter.setId(id);
			}
			ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
			objVO = equipRecordMgr.searchEquipAttachFileByFilter(filter);
			if (objVO != null && !objVO.getLstObject().isEmpty()) {
				
				String exportPath = Configuration.getExportExcelPath();
				String storeRealPath = Configuration.getStoreRealPath();
				//Thu muc goc chua file upload cua thiet bi
				String fileUrl = Configuration.getFileEquipServerDocPath();
				EquipAttachFile item = objVO.getLstObject().get(0);
				fileUrl = fileUrl + item.getUrl();
				
				File fileItem = new File(fileUrl);
				exportPath = exportPath + item.getFileName();
				storeRealPath = storeRealPath + item.getFileName();
				
				File newFile = new File(storeRealPath);
				FileUtils.copyFile(fileItem, newFile);
				
				result.put(REPORT_PATH, exportPath);
				result.put(LIST, exportPath);
				
				/*vuongmq; 07/05/2015; ATTT xuat file*/
				MemcachedUtils.putValueToMemcached(reportToken, exportPath, retrieveReportMemcachedTimeout());
			}
			result.put(ERROR, false);
			result.put("hasData", true);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			//tamvnm: them truong hop loi ko tim thay file
			result.put(ERROR, false);
			result.put("hasData", false);
		}
		return JSON;
	}
	
	/**
	 * Xu ly changeShopGetWareHouse
	 * @author vuongmq
	 * @return String
	 * @since Dec 7, 2015
	 */
	public String changeShopGetWareHouse() {
		actionStartTime = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
			}
			if (shop == null) {
				result.put("lstWarehouseVO", new ArrayList<WarehouseVO>());
				result.put(ERR_MSG, R.getResource("msg.common.shop.err.1"));
				return JSON;
			}
			BasicFilter<WarehouseVO> filter = new BasicFilter<WarehouseVO>();
			filter.setLongG(shop.getId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			// lay du lieu phan quyen duoi DB; da noi chuoi 990
			filter.setStrShopId(super.getStrListShopId());
			stockManagerMgr = (StockManagerMgr) context.getBean("stockManagerMgr");
			List<WarehouseVO> lstWarehouseVO = stockManagerMgr.getListWarehouseVOByFilter(filter).getLstObject();
			if (lstWarehouseVO != null && !lstWarehouseVO.isEmpty()) {
				if (Boolean.TRUE.equals(isChoiceAll)) {
					WarehouseVO all = new WarehouseVO();
					all.setWarehouseId(ActiveType.DELETED.getValue().longValue());
					//all.setWarehouseCode(R.getResource("jsp_common_select_warehouse"));
					all.setWarehouseName(R.getResource("jsp_common_select_warehouse"));
					lstWarehouseVO.add(0, all);
				}
			} else {
				lstWarehouseVO = new ArrayList<WarehouseVO>();
			}
			result.put("lstWarehouseVO", lstWarehouseVO);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CommonAction.changeShop()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Lay thong tin dia ban
	 * 
	 * @author hunglm16
	 * @since August 17, 2015
	 * */
	public String getAreaEntity() {
		Date startLogDate = DateUtil.now();
		try {
			Area area = new Area();
			if (id != null) {
				//Lay theo Id
				area = commonMgr.getEntityById(Area.class, id);
			} else if (customerId != null) {
				//Lay theo id Khach Hag
				Customer cus = commonMgr.getEntityById(Customer.class, customerId);
				if (cus != null && cus.getArea() != null) {
					area = cus.getArea();
				}
			} else if (staffId != null) {
				//Lay theo Id Nhan Vien
				Staff staff = commonMgr.getEntityById(Staff.class, staffId);
				if (staff != null && staff.getArea() != null) {
					area = staff.getArea();
				}
			} if (!StringUtil.isNullOrEmpty(shortCode) && !StringUtil.isNullOrEmpty(shopCode)) {
				//Lay theo Ma KH va Ma Don Vi
				String customerCode = StringUtil.createCustomerCode(shortCode, shopCode);
				if (!StringUtil.isNullOrEmpty(customerCode)) {
					CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
					Customer cus = customerMgr.getCustomerByCode(customerCode);
					if (cus != null && cus.getArea() != null) {
						area = cus.getArea();
					}
				}
			}
			result.put("area", area);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.general.CommonAction.getArea"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	
	/**
	 * Tim kiem thiet bi co an theo phan quyen CMS danh cho kiem ke
	 * 
	 * @author phuongvm
	 * @since 07/07/2015
	 * @description filter.getShopRoot() is not null
	 * */
	public String searchEquipByShopRootInStockEx() {
		result.put("total", 0);
		result.put("rows", new ArrayList<EquipmentManagerVO>());
		try {
			EquipmentManagerMgr equipmentManagerMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
			EquipmentFilter<EquipmentManagerVO> equipmentFilter = new EquipmentFilter<EquipmentManagerVO>();
			if (id != null) {
				equipmentFilter.setId(id);
			}
			if (flagPagination != null && flagPagination) {
				KPaging<EquipmentManagerVO> kPaging = new KPaging<EquipmentManagerVO>();
				kPaging.setPageSize(max);
				kPaging.setPage(page - 1);
				equipmentFilter.setkPaging(kPaging);
			}
			equipmentFilter.setShopRoot(currentUser.getShopRoot().getShopId());
			if (!StringUtil.isNullOrEmpty(equipCode)) {
				equipmentFilter.setCode(equipCode);
			}
			if (!StringUtil.isNullOrEmpty(seriNumber)) {
				equipmentFilter.setSeriNumber(seriNumber);
			}
			if (categoryId != null) {
				equipmentFilter.setEquipCategoryId(categoryId);
			}
			if (groupId != null) {
				equipmentFilter.setEquipGroupId(groupId);
			}
			if (providerId != null) {
				equipmentFilter.setEquipProviderId(providerId);
			}
			if (yearManufacture != null) {
				equipmentFilter.setYearManufacture(yearManufacture);
			}
			if (!StringUtil.isNullOrEmpty(stockCode)) {
				equipmentFilter.setStockCode(stockCode);
			}
//			if (tradeStatus != null) {
//				equipmentFilter.setTradeStatus(tradeStatus);
//			} else {
//				equipmentFilter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//			}
			if (status != null) {
				equipmentFilter.setStatusEquip(status);
			} else {
				equipmentFilter.setStatusEquip(StatusType.ACTIVE.getValue());
			}
			if (usageStatus != null) {
				equipmentFilter.setUsageStatus(usageStatus);
			}
			if (stockType != null) {
				equipmentFilter.setStockType(stockType);
			}
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				equipmentFilter.setShopCode(shopCode);
			}
			if (!StringUtil.isNullOrEmpty(customerCode)) {
				equipmentFilter.setCustomerCode(customerCode);
			}
//			if (!StringUtil.isNullOrEmpty(customerName)) {
//				equipmentFilter.setCustomerName(customerName);
//			}
//			if (!StringUtil.isNullOrEmpty(customerAddress)) {
//				equipmentFilter.setCustomerAddress(customerAddress);
//			}
			ObjectVO<EquipmentManagerVO> equipmentDeliverys = equipmentManagerMgr.getListEquipmentVOByShopInCMSEx(equipmentFilter);
			if (equipmentDeliverys.getLstObject() != null) {
				result.put("total", equipmentDeliverys.getkPaging().getTotalRows());
				result.put("rows", equipmentDeliverys.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "web.log.message.error", "ths.dms.web.action.general.CommonAction.searchEquipByShopRootInStockEx"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
		}
		return JSON;
	}
	
	/**
	 * loadProgramStatistic load kiem ke
	 * @author vuongmq
	 * @since 28/03/2016
	 */
	public String loadProgramStatistic() {
		Date startLogDate = DateUtil.now();
		try {
			result.put("rows", new ArrayList<EquipmentVO>());
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
			if (shopId != null && super.getMapShopChild().get(shopId) != null) {
				filter.setShopId(shopId);
			} else if (currentUser.getShopRoot() != null) {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			} else {
				return JSON;
			}
			List<Integer> lstRecordStatus = new ArrayList<Integer>();
			lstRecordStatus.add(EquipmentStatisticRecordStatus.RUNNING.getValue());
			lstRecordStatus.add(EquipmentStatisticRecordStatus.DONE.getValue());
			filter.setLstRecordStatus(lstRecordStatus);
			filter.setOrderStatistic(" order by from_date desc, to_date ");
			ObjectVO<EquipmentVO> listEquipPeriod = equipmentManagerMgr.getListEquipmentStatisticPeriod(filter);
			if (listEquipPeriod != null && listEquipPeriod.getLstObject() != null) {
				result.put("rows", listEquipPeriod.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "web.action.General.loadProgramStatistic()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	public String test(){
		System.out.println("Test");
		return SUCCESS;
	}
	
	///////////GETTER/SETTER
	/**
	 * Khai bao ca ham GET / SET
	 * 
	 * @author hunglm16
	 * @since August 21,2014
	 * @return GET/SET
	 * @description Yeu cau khai bao cac tham so chung, de tat ca cac chuc nang
	 *              khi goi toi de co the ke thua
	 * */
	//Oject
	private CustomerVO customerVo;
	private ShopToken shopToken;
	private ShopVO shopVO;
	private RoleToken roleToken;
	//List
	private List<ShopToken> lstShopToken;
	private List<RoleToken> lstRoleToken;
	private List<ImageVO> arrImagesVO;
	private List<ImageVO> arrGSVO;
	private List<ImageVO> arrNVBHVO;
	private List<ImageVO> arrProductVo;
	private List<StaffVO> arrStaffVO;	
	private List<Long> lstId;
	private List<Long> arrLong;
	private List<Long> arrLongG;
	private List<String> arrString;
	private List<String> arrStringG;
	private List<String> lstPart;
	private List<Integer> lstStaffType;
	//Long
	private Long id;
	private Long inheritUserPriv;
	private Long shopId;
	private Long staffId;
	private Long parentStaffId;

	private Long longG;
	private Long customerId;
	private Long longType;
	//Integer
	private Integer status;
	private Integer type;
	private Integer objectType;
	private Integer seq;
	private Integer integerG;
	private Integer checkInCurrentWeek;
	//String
	private String code;
	private String name;
	private String typeStr;
	private String address;
	private String shopCode;
	private String shortCode;
	private String strG;
	private String txtG;
	private String fromDate;
	private String toDate;
	private String username;
	private String password;
	private String newPass;
	private String retypedPass;
	private String errMsg;
	private String excelFileName;
	private String statusStr;
	private String typeReturn;
	private String strLstShopId;
	private String staffCode;
	private String staffCodeStr;
	private String arrParentCodeStr;
	private String arrIdStr;
	private String lstCategoryCodes;
	private String lstObjectType;
	private String catCode;
	private String lstTBHVCode;
	
	private Boolean isUnionShopRoot;

	public Long getParentStaffId() {
		return parentStaffId;
	}

	public void setParentStaffId(Long parentStaffId) {
		this.parentStaffId = parentStaffId;
	}

	private Boolean checkBrand;

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public Boolean getCheckBrand() {
		return checkBrand;
	}

	public void setCheckBrand(Boolean checkBrand) {
		this.checkBrand = checkBrand;
	}

	public String getArrIdStr() {
		return arrIdStr;
	}

	public void setArrIdStr(String arrIdStr) {
		this.arrIdStr = arrIdStr;
	}

	public String getArrParentCodeStr() {
		return arrParentCodeStr;
	}

	public void setArrParentCodeStr(String arrParentCodeStr) {
		this.arrParentCodeStr = arrParentCodeStr;
	}

	public String getStaffCodeStr() {
		return staffCodeStr;
	}

	public void setStaffCodeStr(String staffCodeStr) {
		this.staffCodeStr = staffCodeStr;
	}

	public Long getInheritUserPriv() {
		return inheritUserPriv;
	}

	public void setInheritUserPriv(Long inheritUserPriv) {
		this.inheritUserPriv = inheritUserPriv;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}


	public String getLstTBHVCode() {
		return lstTBHVCode;
	}

	public void setLstTBHVCode(String lstTBHVCode) {
		this.lstTBHVCode = lstTBHVCode;
	}
	
	public Boolean getIsUnionShopRoot() {
		return isUnionShopRoot;
	}

	public void setIsUnionShopRoot(Boolean isUnionShopRoot) {
		this.isUnionShopRoot = isUnionShopRoot;
	}

	public Long getLongType() {
		return longType;
	}

	public void setLongType(Long longType) {
		this.longType = longType;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public List<String> getLstPart() {
		return lstPart;
	}

	public void setLstPart(List<String> lstPart) {
		this.lstPart = lstPart;
	}

	public String getExcelFileName() {
		return excelFileName;
	}

	public void setExcelFileName(String excelFileName) {
		this.excelFileName = excelFileName;
	}

	public List<RoleToken> getLstRoleToken() {
		return lstRoleToken;
	}

	public void setLstRoleToken(List<RoleToken> lstRoleToken) {
		this.lstRoleToken = lstRoleToken;
	}

	public CustomerVO getCustomerVo() {
		return customerVo;
	}

	public void setCustomerVo(CustomerVO customerVo) {
		this.customerVo = customerVo;
	}

	public ShopToken getShopToken() {
		return shopToken;
	}

	public void setShopToken(ShopToken shopToken) {
		this.shopToken = shopToken;
	}

	public ShopVO getShopVO() {
		return shopVO;
	}

	public void setShopVO(ShopVO shopVO) {
		this.shopVO = shopVO;
	}

	public RoleToken getRoleToken() {
		return roleToken;
	}

	public void setRoleToken(RoleToken roleToken) {
		this.roleToken = roleToken;
	}

	public List<ShopToken> getLstShopToken() {
		return lstShopToken;
	}

	public void setLstShopToken(List<ShopToken> lstShopToken) {
		this.lstShopToken = lstShopToken;
	}

	public List<ImageVO> getArrImagesVO() {
		return arrImagesVO;
	}

	public void setArrImagesVO(List<ImageVO> arrImagesVO) {
		this.arrImagesVO = arrImagesVO;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<Long> getArrLong() {
		return arrLong;
	}

	public void setArrLong(List<Long> arrLong) {
		this.arrLong = arrLong;
	}

	public List<Long> getArrLongG() {
		return arrLongG;
	}

	public void setArrLongG(List<Long> arrLongG) {
		this.arrLongG = arrLongG;
	}

	public List<String> getArrString() {
		return arrString;
	}

	public void setArrString(List<String> arrString) {
		this.arrString = arrString;
	}

	public List<String> getArrStringG() {
		return arrStringG;
	}

	public void setArrStringG(List<String> arrStringG) {
		this.arrStringG = arrStringG;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLongG() {
		return longG;
	}

	public void setLongG(Long longG) {
		this.longG = longG;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getIntegerG() {
		return integerG;
	}

	public void setIntegerG(Integer integerG) {
		this.integerG = integerG;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getStrG() {
		return strG;
	}

	public void setStrG(String strG) {
		this.strG = strG;
	}

	public String getTxtG() {
		return txtG;
	}

	public void setTxtG(String txtG) {
		this.txtG = txtG;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPass() {
		return newPass;
	}

	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}

	public String getRetypedPass() {
		return retypedPass;
	}

	public void setRetypedPass(String retypedPass) {
		this.retypedPass = retypedPass;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getTypeReturn() {
		return typeReturn;
	}

	public void setTypeReturn(String typeReturn) {
		this.typeReturn = typeReturn;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getLstCategoryCodes() {
		return lstCategoryCodes;
	}

	public void setLstCategoryCodes(String lstCategoryCodes) {
		this.lstCategoryCodes = lstCategoryCodes;
	}

	public String getLstObjectType() {
		return lstObjectType;
	}

	public void setLstObjectType(String lstObjectType) {
		this.lstObjectType = lstObjectType;
	}

	public Integer getCheckInCurrentWeek() {
		return checkInCurrentWeek;
	}

	public void setCheckInCurrentWeek(Integer checkInCurrentWeek) {
		this.checkInCurrentWeek = checkInCurrentWeek;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public Integer getStSpecType() {
		return stSpecType;
	}

	public void setStSpecType(Integer stSpecType) {
		this.stSpecType = stSpecType;
	}

	public Boolean getIsShowAllShop() {
		return isShowAllShop;
	}

	public void setIsShowAllShop(Boolean isShowAllShop) {
		this.isShowAllShop = isShowAllShop;
	}

	public String getStrLstShopId() {
		return strLstShopId;
	}

	public List<ImageVO> getArrGSVO() {
		return arrGSVO;
	}

	public void setArrGSVO(List<ImageVO> arrGSVO) {
		this.arrGSVO = arrGSVO;
	}

	public List<ImageVO> getArrNVBHVO() {
		return arrNVBHVO;
	}

	public void setArrNVBHVO(List<ImageVO> arrNVBHVO) {
		this.arrNVBHVO = arrNVBHVO;
	}

	public void setStrLstShopId(String strLstShopId) {
		this.strLstShopId = strLstShopId;
	}

	public List<ImageVO> getArrProductVo() {
		return arrProductVo;
	}

	public void setArrProductVo(List<ImageVO> arrProductVo) {
		this.arrProductVo = arrProductVo;
	}

	public Long getIdParentCat() {
		return idParentCat;
	}

	public void setIdParentCat(Long idParentCat) {
		this.idParentCat = idParentCat;
	}

	public Long getIdParentSubCat() {
		return idParentSubCat;
	}

	public void setIdParentSubCat(Long idParentSubCat) {
		this.idParentSubCat = idParentSubCat;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Long getKsId() {
		return ksId;
	}

	public void setKsId(Long ksId) {
		this.ksId = ksId;
	}
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getNumPeriod() {
		return numPeriod;
	}

	public void setNumPeriod(Integer numPeriod) {
		this.numPeriod = numPeriod;
	}

	public Boolean getIsChoiceAll() {
		return isChoiceAll;
	}

	public void setIsChoiceAll(Boolean isChoiceAll) {
		this.isChoiceAll = isChoiceAll;
	}

	public Integer getViewChangePassDefault() {
		return viewChangePassDefault;
	}

	public void setViewChangePassDefault(Integer viewChangePassDefault) {
		this.viewChangePassDefault = viewChangePassDefault;
	}

	public List<StaffVO> getArrStaffVO() {
		return arrStaffVO;
	}

	public void setArrStaffVO(List<StaffVO> arrStaffVO) {
		this.arrStaffVO = arrStaffVO;
	}

	public List<Integer> getLstStaffType() {
		return lstStaffType;
	}

	public void setLstStaffType(List<Integer> lstStaffType) {
		this.lstStaffType = lstStaffType;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Long getStaffRoleId() {
		return staffRoleId;
	}

	public void setStaffRoleId(Long staffRoleId) {
		this.staffRoleId = staffRoleId;
	}

	public Integer getStatusRole() {
		return statusRole;
	}

	public void setStatusRole(Integer statusRole) {
		this.statusRole = statusRole;
	}
	
	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	
	public Boolean getFlagPagination() {
		return flagPagination;
	}

	public void setFlagPagination(Boolean flagPagination) {
		this.flagPagination = flagPagination;
	}
	
	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public StockManagerMgr getStockManagerMgr() {
		return stockManagerMgr;
	}

	public void setStockManagerMgr(StockManagerMgr stockManagerMgr) {
		this.stockManagerMgr = stockManagerMgr;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public Integer getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(Integer usageStatus) {
		this.usageStatus = usageStatus;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getStrEquip() {
		return strEquip;
	}

	public void setStrEquip(String strEquip) {
		this.strEquip = strEquip;
	}
}

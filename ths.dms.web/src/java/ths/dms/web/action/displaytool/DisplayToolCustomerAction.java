package ths.dms.web.action.displaytool;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DisplayToolMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.DisplayTool;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffRoleType;
import ths.dms.core.entities.filter.DisplayToolCustomerFilter;
import ths.dms.core.entities.filter.DisplayToolFilter;
import ths.dms.core.entities.vo.DisplayToolCustomerVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffComboxVO;
import ths.dms.core.entities.vo.StaffSimpleVO;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class DisplayToolCustomerAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6747207823708173739L;

	private Long staffId;

	private Long shopId;

	private String dateStr;

	private String dateStrExcel;

	private String staffCode;

	private String shopCode;

	private List<DisplayToolCustomerVO> lstToolCustomer;

	private File excelFile;

	private List<StaffComboxVO> lstStaff;

	private List<Long> displayToolsId;

	private String yesterDay;

	private ProductMgr productMgr;
	private CustomerMgr customerMgr;

	private CommonMgr commonMgr;
	private Shop shop;

	private Staff staff;

	private DisplayToolMgr displayToolMgr;

	private HashMap<String, Object> beans;

	private List<DisplayToolCustomerVO> lstDisplayToolCustomerVO;

	private Integer typeCheck;

	public Integer getTypeCheck() {
		return typeCheck;
	}

	public void setTypeCheck(Integer typeCheck) {
		this.typeCheck = typeCheck;
	}

	public List<Long> getDisplayToolsId() {
		return displayToolsId;
	}

	public void setDisplayToolsId(List<Long> displayToolsId) {
		this.displayToolsId = displayToolsId;
	}

	public List<StaffComboxVO> getLstStaff() {
		return lstStaff;
	}

	public void setLstStaff(List<StaffComboxVO> lstStaff) {
		this.lstStaff = lstStaff;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getYesterDay() {
		return yesterDay;
	}

	public void setYesterDay(String yesterDay) {
		this.yesterDay = yesterDay;
	}

	public List<DisplayToolCustomerVO> getLstToolCustomer() {
		return lstToolCustomer;
	}

	public void setLstToolCustomer(List<DisplayToolCustomerVO> lstToolCustomer) {
		this.lstToolCustomer = lstToolCustomer;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	private String excelFileContentType;

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Long getStaffId() {
		return staffId;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getDateStrExcel() {
		return dateStrExcel;
	}

	public void setDateStrExcel(String dateStrExcel) {
		this.dateStrExcel = dateStrExcel;
	}

	/**
	 * @author tientv
	 */
	@Override
	public void prepare() throws Exception {
		try {
			super.prepare();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		displayToolMgr = (DisplayToolMgr) context.getBean("displayToolMgr");
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		staff = getStaffByCurrentUser();
		commonMgr = (CommonMgr) context.getBean("commonMgr");

		//shopId = staff.getShop().getShopId();
		if (currentUser != null && currentUser.getUserName() != null) {
			staff = staffMgr.getStaffByCode(currentUser.getUserName());
		}
	}

	/**
	 * @author tientv
	 */
	@Override
	public String execute() throws Exception {
		resetToken(result);
		try {
			if (staff == null) {
				return PAGE_NOT_PERMISSION;
			}
			Date date = DateUtil.moveDate(DateUtil.now(), -1, 2);
			String mm = DateUtil.getMonth(date) < 10 ? "0" + DateUtil.getMonth(date) : String.valueOf(DateUtil.getMonth(date));
			yesterDay = String.valueOf(mm + "/" + DateUtil.getYear(date));
		} catch (Exception e) {
			LogUtility.logError(e, "Exception");
			return PAGE_NOT_FOUND;
		}
		return SUCCESS;
	}

	/**
	 * @author tientv
	 * @see Tim kiem khach hang su dung TU
	 */
	public String searchDisplayToolCustomer() {
		return JSON;
	}

	/**
	 * @see : KIEM TRA SHOP TYPE: 1: SHOP not found. 2. SUCCESS, 3: SHOP IS NOT
	 *      DISTRIBUTOR
	 * @author tientv
	 * @return String
	 */
	public String checkShopType() {
		return JSON;
	}

	/**
	 * @see DANH SACH NHAN VIEN THEO DON VI.
	 * @author tientv
	 * @return
	 */
	public String searchStaffOfShop() {
		return JSON;
	}

	/**
	 * @see XOA DANH SACH TU
	 * @author tientv
	 * @return
	 */
	public String delDisplayToolsCustomer() {
		return JSON;
	}

	/**
	 * SAO CHEP TU
	 * 
	 * @author tientv
	 * @date: update 14/04/2014 vuongmq
	 */

	public String copyDisplayTools() {
		try {
			DisplayToolFilter filter = new DisplayToolFilter();
			// Sao chep tat ca
			String errMsg = "";
			Date date = DateUtil.moveDate(DateUtil.now(), -1, 2);
			yesterDay = DateUtil.toMonthYearString(date);
			filter.setShopId(shopId);
			filter.setInMonth(yesterDay);
			Shop sh = shopMgr.getShopById(shopId);
			if (sh == null || !checkAncestor(sh.getShopCode())) {
				return JSON;
			}
			if (!StringUtil.isNullOrEmpty(staffCode)) {
				// Sao cheo 1 nhan vien cho khach hang muon tu
				Staff s = staffMgr.getStaffByCode(staffCode);
				filter.setStaffId(s.getId());
			}
			List<StaffSimpleVO> staffIdMonthCurent = new ArrayList<StaffSimpleVO>();
			List<StaffSimpleVO> staffIdDisplaytool = displayToolMgr.getListDisplayToolStaffId(filter);
			if (staffIdDisplaytool == null || staffIdDisplaytool.size() == 0) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "device.display.tool.customer.no.row");
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			} else {
				for (StaffSimpleVO st : staffIdDisplaytool) {
					Boolean flag = displayToolMgr.checkDateMonthToDay(shopId, st.getStaffId(), DateUtil.toMonthYearString(DateUtil.now()));
					if (flag) {
						errMsg += "<br />" + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "device.display.tool.customer.date") + st.getStaffCode() + " - " + st.getStaffName();
					} else {
						staffIdMonthCurent.add(st);
					}
				}
			}
			String successMsg = "";
			if (staffIdMonthCurent.size() > 0) {
				for (StaffSimpleVO stDisplaytool : staffIdMonthCurent) {
					filter.setStaffId(stDisplaytool.getStaffId());
					ObjectVO<DisplayTool> vos = displayToolMgr.getListDisplayTool(null, filter);
					if (vos == null || vos.getLstObject().size() == 0) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "device.display.tool.customer.no.row");
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
					List<DisplayTool> list = new ArrayList<DisplayTool>();
					DisplayTool dt = null;
					Date createDate = commonMgr.getSysDate();
					Date inMonth = DateUtil.parse("01/" + DateUtil.toMonthYearString(DateUtil.now()), DateUtil.DATE_FORMAT_DDMMYYYY);
					if (vos != null && vos.getLstObject().size() > 0) {
						for (DisplayTool vo : vos.getLstObject()) {
							dt = new DisplayTool();
							dt.setShop(vo.getShop());
							dt.setStaff(vo.getStaff());
							dt.setCustomerId(vo.getCustomerId());
							dt.setInMonth(inMonth);
							dt.setCreateUser(staff.getStaffCode());
							dt.setCreateDate(createDate);
							dt.setAmount(vo.getAmount());
							dt.setQuantity(vo.getQuantity());
							dt.setProduct(vo.getProduct());
							list.add(dt);
						}
					}
					displayToolMgr.createListDisplayTool(list);
					successMsg += "<br />" + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khmuontu.customer.success.data") + stDisplaytool.getStaffCode() + " - " + stDisplaytool.getStaffName();
				}
			}
			if (successMsg.length() > 0) {
				result.put(ERROR, false);
				result.put("successMsg", successMsg);
				result.put("errMsg", errMsg);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "DisplayToolCustomerAction.copyDisplayTools");
		}
		return JSON;
	}

	/**
	 * @author vuongdl
	 */
	public String exportExcelToolCustomer() {
		/*
		 * try { staffId = null; Staff staffSearch = null;
		 * if(!StringUtil.isNullOrEmpty(staffCode)){ staffSearch =
		 * staffMgr.getStaffByCode(staffCode); if(staffSearch==null){ return
		 * JSON; } staffId = staffSearch.getId(); } if(shopId!=null &&
		 * shopId>0){ shop = shopMgr.getShopById(shopId); if(shop==null){ return
		 * JSON; } } beans = new HashMap<String, Object>();
		 * ObjectVO<DisplayToolCustomerVO> vo = new
		 * ObjectVO<DisplayToolCustomerVO>(); DisplayToolCustomerFilter filter =
		 * new DisplayToolCustomerFilter(); filter.setShopId(shopId==15?
		 * null:shopId); filter.setStaffId(staffId); filter.setMonth(dateStr);
		 * if
		 * (staff.getStaffType().getObjectType().equals(StaffRoleType.GSNPP.getValue
		 * ())){ filter.setGsnppId(staff.getId()); } vo =
		 * displayToolMgr.getListDisplayToolCustomerVo(null, filter); if(vo !=
		 * null && vo.getLstObject().size()>0){ lstDisplayToolCustomerVO = new
		 * ArrayList<DisplayToolCustomerVO>(); lstDisplayToolCustomerVO =
		 * vo.getLstObject(); beans.put("lst", lstDisplayToolCustomerVO);
		 * if(staffSearch!=null){ beans.put("staffCodeName",
		 * staffSearch.getStaffCode() + " - " + staffSearch.getStaffName());
		 * }else{ beans.put("staffCodeName","Tất cả"); } if(shop!=null){
		 * beans.put("shopCode",shop.getShopCode() + " - " +
		 * shop.getShopName()); }else{ beans.put("shopCode",""); }
		 * beans.put("dateStr", dateStr); String outputDownload =
		 * ReportUtils.exportExcelJxls(beans,
		 * ShopReportTemplate.QLTB_KHMT,FileExtension.XLS); result.put(ERROR,
		 * false); result.put(LIST, outputDownload); result.put("hasData",
		 * true); } else{ result.put("hasData", false); } } catch (Exception e)
		 * { LogUtility.logError(e,
		 * "DisplayToolCustomerAction.exportExcelToolCustomer(): " +
		 * e.getMessage()); result.put(ERROR, true); result.put("errMsg",
		 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM)); return JSON; }
		 */
		return JSON;
	}

	public String check1(List<List<String>> lstData, int j) { // check trung khoa
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).equalsIgnoreCase(row2.get(0)) && row1.get(1).equalsIgnoreCase(row2.get(1)) && row1.get(2).equalsIgnoreCase(row2.get(2)) && row1.get(3).equalsIgnoreCase(row2.get(3))) {
						if (StringUtil.isNullOrEmpty(message)) {
							message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu") + (j + 2) + ", " + (k + 2);
						} else {
							message += ", " + (k + 2);
						}

					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
			}
		}
		return message;
	}

	public List<CellBean> checkListFail(List<CellBean> lstData) { // check trung khoa
		String message = "";
		for (int j = 0; j < lstData.size(); j++) {
			if (lstData.get(j) != null) {
				CellBean row1 = lstData.get(j);
				message = "";
				if (!StringUtil.isNullOrEmpty(row1.getContent1()) && !StringUtil.isNullOrEmpty(row1.getContent2()) && !StringUtil.isNullOrEmpty(row1.getContent3()) && !StringUtil.isNullOrEmpty(row1.getContent4())) {
					for (int k = 0; k < lstData.size(); k++) {
						if (lstData.get(k) != null && k != j) {
							CellBean row2 = lstData.get(k);
							if (row1.getContent1().equalsIgnoreCase(row2.getContent1()) && row1.getContent2().equalsIgnoreCase(row2.getContent2()) && row1.getContent3().equalsIgnoreCase(row2.getContent3())
									&& row1.getContent4().equalsIgnoreCase(row2.getContent4())) {
								if (StringUtil.isNullOrEmpty(message)) {
									message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.dong.thu");
									message += (j + 2) + ", " + (k + 2);
								} else {
									message += ", " + (k + 2);
								}
							}
						}
					}
					if (!StringUtil.isNullOrEmpty(message)) {
						message += " " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "huanluyen.trung.nhau");
						lstData.get(j).setErrMsg(message);
					}
				}
			}
		}
		return lstData;
	}

	/**
	 * @author phuongVM
	 * @see IMPORT KHACH HANG MUON TU
	 */
	public String importExcelCustomerToool() {
		return SUCCESS;
	}
}

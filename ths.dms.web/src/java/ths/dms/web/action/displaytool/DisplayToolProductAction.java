package ths.dms.web.action.displaytool;

import java.util.ArrayList;
import java.util.List;

//import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.DisplayToolMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.DisplayToolFilter;
import ths.dms.core.entities.vo.DisplayToolVO;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.utils.LogUtility;

public class DisplayToolProductAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2768846090955365141L;
	private Staff staff;
	private Shop shop;
	private DisplayToolMgr displayToolMgr;
	private ProductMgr productMgr;

	private Integer status;
	private Long displayToolId;
	private String displayToolCode;
	private String displayToolName;
	private List<StatusBean> listStatus;
	private List<Product> lstProduct;
	private List<Long> lstProductId;
	private Long productId;
	private String lstNewDisplayToolId;
	private String lstNewDisplayToolCode;
	private String lstNewDisplayToolName;

	private Long id;

	private Integer add;
	private Long newDisplayToolId;
	private String newDisplayToolCode;
	private String newDisplayToolName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Product> getLstProduct() {
		return lstProduct;
	}

	public void setLstProduct(List<Product> lstProduct) {
		this.lstProduct = lstProduct;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public DisplayToolMgr getDisplayToolMgr() {
		return displayToolMgr;
	}

	public void setDisplayToolMgr(DisplayToolMgr displayToolMgr) {
		this.displayToolMgr = displayToolMgr;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getDisplayToolId() {
		return displayToolId;
	}

	public void setDisplayToolId(Long displayToolId) {
		this.displayToolId = displayToolId;
	}

	public String getDisplayToolCode() {
		return displayToolCode;
	}

	public void setDisplayToolCode(String displayToolCode) {
		this.displayToolCode = displayToolCode;
	}

	public String getDisplayToolName() {
		return displayToolName;
	}

	public void setDisplayToolName(String displayToolName) {
		this.displayToolName = displayToolName;
	}

	public List<StatusBean> getListStatus() {
		return listStatus;
	}

	public void setListStatus(List<StatusBean> listStatus) {
		this.listStatus = listStatus;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	//	public boolean checkExistShopOfUserLogin(){
	//	if(currentUser != null && currentUser.getUserName() != null){
	//		try {
	////			staff = staffMgr.getStaffByCode(currentUser.getUserName());
	//		} catch (BusinessException e) {
	//			return false;
	//		}
	//		if(staff != null && staff.getShop() != null){
	//			shop = staff.getShop();
	//		}
	//	}
	//	if(shop == null) return false;
	//	else return true;
	//}

	public void setAdd(Integer add) {
		this.add = add;
	}

	public Integer getAdd() {
		return add;
	}

	public Long getNewDisplayToolId() {
		return newDisplayToolId;
	}

	public void setNewDisplayToolId(Long newDisplayToolId) {
		this.newDisplayToolId = newDisplayToolId;
	}

	public String getNewDisplayToolCode() {
		return newDisplayToolCode;
	}

	public void setNewDisplayToolCode(String newDisplayToolCode) {
		this.newDisplayToolCode = newDisplayToolCode;
	}

	public String getNewDisplayToolName() {
		return newDisplayToolName;
	}

	public void setNewDisplayToolName(String newDisplayToolName) {
		this.newDisplayToolName = newDisplayToolName;
	}

	public void setLstProductId(List<Long> lstProductId) {
		this.lstProductId = lstProductId;
	}

	public List<Long> getLstProductId() {
		return lstProductId;
	}

	public void setLstNewDisplayToolId(String lstNewDisplayToolId) {
		this.lstNewDisplayToolId = lstNewDisplayToolId;
	}

	public String getLstNewDisplayToolId() {
		return lstNewDisplayToolId;
	}

	public void setLstNewDisplayToolCode(String lstNewDisplayToolCode) {
		this.lstNewDisplayToolCode = lstNewDisplayToolCode;
	}

	public String getLstNewDisplayToolCode() {
		return lstNewDisplayToolCode;
	}

	public void setLstNewDisplayToolName(String lstNewDisplayToolName) {
		this.lstNewDisplayToolName = lstNewDisplayToolName;
	}

	public String getLstNewDisplayToolName() {
		return lstNewDisplayToolName;
	}

	@Override
	public void prepare() throws Exception {
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		displayToolMgr = (DisplayToolMgr) context.getBean("displayToolMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		super.prepare();
	}

	@Override
	public String execute() {
		resetToken(result);
		String result = SUCCESS;
		try {
			mapControl = getMapControlToken();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return result;
	}

	/**
	 * Search DisplayTool (Tim kiem tu cua san pham)
	 * 
	 * @author liemtpt
	 * @return the string
	 */
	public String searchDisplayTool() {
		result.put("page", page);
		result.put("rows", rows);
		try {
			KPaging<DisplayToolVO> kPaging = new KPaging<DisplayToolVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			DisplayToolFilter filter = new DisplayToolFilter();
			filter.setDisplayToolCode(displayToolCode);
			filter.setDisplayToolName(displayToolName);
			filter.setStatus(ActiveType.RUNNING);
			filter.setkPaging(kPaging);
			ObjectVO<DisplayToolVO> lstDisplayToolVO = displayToolMgr.getListDisplayToolOfProduct(filter);
			List<DisplayToolVO> lst = lstDisplayToolVO.getLstObject();
			if (add != null && add == 1) {
				if (lstNewDisplayToolId != null && lstNewDisplayToolId.length() > 0) {
					String[] _lstNewDisplayToolId = lstNewDisplayToolId.split(";");
					String[] _lstNewDisplayToolCode = lstNewDisplayToolCode.split(";");
					String[] _lstNewDisplayToolName = lstNewDisplayToolName.split(";");
					for (int i = 0; i < _lstNewDisplayToolId.length; i++) {
						newDisplayToolId = Long.valueOf(_lstNewDisplayToolId[i].trim());
						newDisplayToolCode = _lstNewDisplayToolCode[i].trim();
						newDisplayToolName = _lstNewDisplayToolName[i].trim();
						DisplayToolVO newDTVO = new DisplayToolVO();
						newDTVO.setDisplayToolId(newDisplayToolId);
						newDTVO.setDisplayToolCode(newDisplayToolCode);
						newDTVO.setDisplayToolName(newDisplayToolName);
						lst.add(newDTVO);
						lstDisplayToolVO.getkPaging().setTotalRows(lstDisplayToolVO.getkPaging().getTotalRows() + 1);
					}
				}
			}
			if (lstDisplayToolVO != null) {
				result.put("total", lstDisplayToolVO.getkPaging().getTotalRows());
				result.put("rows", lst);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Search Product Of DisplayTool.
	 * 
	 * @return the string
	 * @author thongnm
	 */
	public String searchProduct() {
		result.put("page", page);
		result.put("rows", rows);
		try {
			ObjectVO<Product> lstProductVO = displayToolMgr.getListProductOfDisplayTool(null, displayToolId);
			if (lstProductVO != null) {
				result.put("total", lstProductVO.getLstObject().size());
				result.put("rows", lstProductVO.getLstObject());
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Add display tool.
	 * 
	 * @return the string
	 * @author thongnm
	 */
	public String createProductOfDisplayTool() {
		resetToken(result);
		try {
			Product p = productMgr.getProductByCode(displayToolCode);
			if (p != null) {
				displayToolMgr.createDisplayToolListProducts(p.getId(), lstProductId);
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			// TODO: handle exception
			LogUtility.logError(e, e.getMessage());
			System.out.print(e.getMessage());
			result.put(ERROR, false);
		}
		return JSON;
	}

	/**
	 * Delete display tool.
	 * 
	 * @return the string
	 * @author liemtpt
	 */
	public String deleteDisplayTool() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			displayToolMgr.deleteDisplayTool(id);
			error = false;
		} catch (Exception e) {
			errMsg += e.getMessage();
			LogUtility.logError(e, e.getMessage());
		}
		result.put(ERROR, error);
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Delete product of display tool.
	 * 
	 * @return the string
	 * @author liemtpt
	 */
	public String deleteProductOfDisplayTool() {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		lstProductId = new ArrayList<Long>();
		lstProductId.add(productId);
		try {
			displayToolMgr.deleteDisplayToolListProducts(id, lstProductId);
			error = false;
		} catch (Exception e) {
			errMsg += e.getMessage();
			LogUtility.logError(e, e.getMessage());
		}
		result.put(ERROR, error);
		result.put("errMsg", errMsg);
		return JSON;
	}
}

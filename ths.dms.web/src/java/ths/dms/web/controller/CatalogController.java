/*
 * 
 */
package ths.dms.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import viettel.passport.client.UserToken;
import vn.kunkun.vietbando.LatLng;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.AreaMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.CycleCountMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.DisplayProgrameMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.FocusProgramMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.SaleLevelCatMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.business.VietBanDoMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Area;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountResult;
import ths.dms.core.entities.Debit;
import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.FocusChannelMap;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.SaleLevelCat;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StDisplayPlAmtDtl;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffGroup;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.TreeVOType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductCatVO;
import ths.dms.core.entities.vo.ProductSubCatVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.RoutingTreeVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.entities.vo.ShopTreeVO;
import ths.dms.core.entities.vo.StaffGroupTreeVO;
import ths.dms.core.entities.vo.StaffTreeVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.TreeExVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.bean.CategoryBean;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.ComboTreeBean;
import ths.dms.web.bean.ComboTreeBeanAttr;
import ths.dms.web.bean.JETreeNode;
import ths.dms.web.bean.JETreeNodeAttr;
import ths.dms.web.bean.JsTreeNode;
import ths.dms.web.bean.JsTreeNodeAttr;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.bean.TreeBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CatalogController.
 */
@Controller
@RequestMapping("/catalog")
public class CatalogController {

	/** The context. */
	private final ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext-service.xml");

	private final int COMBOTREE_ONE_NODE = 1;

	/**
	 * Instantiates a new catalog controller.
	 */
	public CatalogController() {
	}

	/**
	 * Gets the customer type tree.
	 * 
	 * @return the customer type tree
	 */
	@RequestMapping(value = "/customer-type/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getCustomerTypeTree() {
		List<JsTreeNode> lstTree = new ArrayList<JsTreeNode>();
		try {
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			JsTreeNode root = new JsTreeNode();
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId("-2");
			nodeAttr.setContentItemId("");
			root.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog_customer_type_title"));
			root.setAttr(nodeAttr);

			List<JsTreeNode> lstCustomerTypeTree = new ArrayList<JsTreeNode>();
			if (channelTypeMgr != null) {
				TreeVO<ChannelType> channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVO(null, ChannelTypeType.CUSTOMER, false);
				if (channelTypeTreeVO.getObject() == null && channelTypeTreeVO.getListChildren() != null) {
					for (int i = 0; i < channelTypeTreeVO.getListChildren().size(); i++) {
						JsTreeNode tmp = new JsTreeNode();
						tmp = getJsTreeNode(tmp, channelTypeTreeVO.getListChildren().get(i));
						lstCustomerTypeTree.add(tmp);
					}
				} else {
					JsTreeNode bean = new JsTreeNode();
					bean = getJsTreeNode(bean, channelTypeTreeVO);
					lstCustomerTypeTree.add(bean);
				}
			}
			root.setChildren(lstCustomerTypeTree);
			root.setState(ConstantManager.JSTREE_STATE_OPEN);
			lstTree.add(root);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstTree;
	}

	/**
	 * Gets the car type tree.
	 * 
	 * @return the car type tree
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/car-type/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getCarTypeTree() {
		List<JsTreeNode> lstCarTypeTree = new ArrayList<JsTreeNode>();
		try {
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			if (channelTypeMgr != null) {
				TreeVO<ChannelType> channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVO(null, ChannelTypeType.SHOP, true);
				if (channelTypeTreeVO.getObject() == null && channelTypeTreeVO.getListChildren() != null) {
					for (int i = 0; i < channelTypeTreeVO.getListChildren().size(); i++) {
						JsTreeNode tmp = new JsTreeNode();
						tmp = getJsTreeNode(tmp, channelTypeTreeVO.getListChildren().get(i));
						lstCarTypeTree.add(tmp);
					}
				} else {
					JsTreeNode bean = new JsTreeNode();
					bean = getJsTreeNode(bean, channelTypeTreeVO);
					lstCarTypeTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstCarTypeTree;
	}

	/**
	 * Gets the staff type tree.
	 * 
	 * @return the staff type tree
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/staff-type/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getStaffTypeTree() {
		List<JsTreeNode> lstTree = new ArrayList<JsTreeNode>();
		JsTreeNode root = new JsTreeNode();
		JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
		nodeAttr.setId("-1");
		nodeAttr.setContentItemId("");
		root.setData("Loại nhân viên");
		root.setAttr(nodeAttr);

		List<JsTreeNode> lstStaffTypeTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		try {
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			if (channelTypeMgr != null) {
				TreeVO<ChannelType> channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVO(null, ChannelTypeType.STAFF, true);
				if (channelTypeTreeVO.getObject() == null && channelTypeTreeVO.getListChildren() != null) {
					for (int i = 0; i < channelTypeTreeVO.getListChildren().size(); i++) {
						JsTreeNode tmp = new JsTreeNode();
						tmp = getJsTreeNode(tmp, channelTypeTreeVO.getListChildren().get(i));
						lstStaffTypeTree.add(tmp);
					}
				} else {
					bean = getJsTreeNode(bean, channelTypeTreeVO);
					lstStaffTypeTree.add(bean);
				}
			}
			root.setChildren(lstStaffTypeTree);
			root.setState(ConstantManager.JSTREE_STATE_OPEN);
			lstTree.add(root);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstTree;
	}

	/**
	 * Gets the sale type tree.
	 * 
	 * @return the sale type tree
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/sale-type/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getSaleTypeTree() {
		List<JsTreeNode> lstStaffTypeTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		try {
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			if (channelTypeMgr != null) {
				TreeVO<ChannelType> channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVO(null, ChannelTypeType.SALE_MAN, true);
				if (channelTypeTreeVO.getObject() == null && channelTypeTreeVO.getListChildren() != null) {
					for (int i = 0; i < channelTypeTreeVO.getListChildren().size(); i++) {
						JsTreeNode tmp = new JsTreeNode();
						tmp = getJsTreeNode(tmp, channelTypeTreeVO.getListChildren().get(i));
						lstStaffTypeTree.add(tmp);
					}
				} else {
					bean = getJsTreeNode(bean, channelTypeTreeVO);
					lstStaffTypeTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstStaffTypeTree;
	}

	/**
	 * Gets the unit tree.
	 * 
	 * @param shopCode
	 *            the shop code
	 * @return the unit tree
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/unit-tree/{shopCode}/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getUnitTree(@PathVariable("shopCode") String shopCode) {
		List<JsTreeNode> lstUnitTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					Shop shop = shopMgr.getShopByCode(shopCode);
					TreeVO<Shop> shopTreeVO = shopMgr.getShopTreeVO(shop, null, null, null, true);
					if (shopTreeVO.getObject() == null && shopTreeVO.getListChildren() != null) {
						for (int i = 0; i < shopTreeVO.getListChildren().size(); i++) {
							JsTreeNode tmp = new JsTreeNode();
							tmp = getUnitTreeBean(tmp, shopTreeVO.getListChildren().get(i));
							lstUnitTree.add(tmp);
						}
					} else {
						bean = getUnitTreeBean(bean, shopTreeVO);
						lstUnitTree.add(bean);
					}
				}
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstUnitTree;
	}

	/**
	 * Gets the area tree.
	 * 
	 * @return the area tree
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/area-tree/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getAreaTree() {

		List<JsTreeNode> lstAreaTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		try {
			AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
			if (areaMgr != null) {
				TreeVO<Area> areaTreeVO = areaMgr.getAreaTreeVOEx(null, null, ActiveType.RUNNING);
				if (areaTreeVO.getObject() == null && areaTreeVO.getListChildren() != null) {
					for (int i = 0; i < areaTreeVO.getListChildren().size(); i++) {
						JsTreeNode tmp = new JsTreeNode();
						tmp = getAreaTreeBean(tmp, areaTreeVO.getListChildren().get(i));
						lstAreaTree.add(tmp);
					}
				} else {
					bean = getAreaTreeBean(bean, areaTreeVO);
					lstAreaTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstAreaTree;
	}

	/**
	 * Gets the unit tree bean.
	 * 
	 * @param bean
	 *            the bean
	 * @param shopTree
	 *            the shop tree
	 * @return the unit tree bean
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	private JsTreeNode getUnitTreeBean(JsTreeNode bean, TreeVO<Shop> shopTree) {
		if (shopTree != null && shopTree.getObject() != null) {
			bean.setData(shopTree.getObject().getShopName());
//			shopTree.getObject().getType().getChannelTypeCode();
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(shopTree.getObject().getId().toString());
			nodeAttr.setContentItemId(shopTree.getObject().getShopCode());
//			nodeAttr.setShopType(shopTree.getObject().getType().getChannelTypeCode());
			bean.setAttr(nodeAttr);
			List<JsTreeNode> lstChild = new ArrayList<JsTreeNode>();
			if (shopTree.getListChildren() != null && shopTree.getListChildren().size() > 0) {
				for (int i = 0; i < shopTree.getListChildren().size(); i++) {
					JsTreeNode subBean = new JsTreeNode();
					subBean = getUnitTreeBean(subBean, shopTree.getListChildren().get(i));
					lstChild.add(subBean);
				}
				bean.setChildren(lstChild);
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
			}
		}
		return bean;
	}

	/**
	 * Gets the unit tree bean with child.
	 * 
	 * @param bean
	 *            the bean
	 * @param shopTree
	 *            the shop tree
	 * @return the unit tree bean with child
	 * @author hungtx
	 * @since Oct 26, 2012
	 */
	private JsTreeNode getUnitTreeBeanWithChild(JsTreeNode bean, TreeVO<Shop> shopTree) {
		if (shopTree != null && shopTree.getObject() != null) {
			bean.setData(StringUtil.isNullOrEmpty(shopTree.getObject().getShopCode()) ? shopTree.getObject().getShopName() : shopTree.getObject().getShopCode() + " - " + shopTree.getObject().getShopName());

			//shopTree.getObject().getType().getChannelTypeCode();
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(shopTree.getObject().getId().toString());
			nodeAttr.setContentItemId(shopTree.getObject().getShopCode());
			if (shopTree.getObject().getType() != null) {
//				nodeAttr.setShopType(shopTree.getObject().getType().getChannelTypeCode());
			}
			bean.setAttr(nodeAttr);
			List<JsTreeNode> lstChild = new ArrayList<JsTreeNode>();
			if (shopTree.getListChildren() != null && shopTree.getListChildren().size() > 0) {
				for (int i = 0; i < shopTree.getListChildren().size(); i++) {
					JsTreeNode subBean = new JsTreeNode();
					subBean = getUnitTreeBeanWithChild(subBean, shopTree.getListChildren().get(i));
					lstChild.add(subBean);
				}
				bean.setChildren(lstChild);
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
			} else {
				bean.setState(ConstantManager.JSTREE_STATE_CLOSE);
			}
		}
		return bean;
	}

	/**
	 * Gets the unit tree bean with child containt code and name.
	 * 
	 * @param bean
	 *            the bean
	 * @param shopTree
	 *            the shop tree
	 * @return the unit tree bean with child
	 * @author hungtx
	 * @since Oct 26, 2012
	 */
	private JsTreeNode getUnitTreeBeanWithChildContaintName(JsTreeNode bean, TreeVO<Shop> shopTree) {
		/*if (shopTree != null && shopTree.getObject() != null) {
			String data = shopTree.getObject().getShopCode() + " - " + shopTree.getObject().getShopName();
			bean.setData(data);
			//shopTree.getObject().getType().getChannelTypeCode();
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(shopTree.getObject().getId().toString());
			nodeAttr.setContentItemId(shopTree.getObject().getShopCode());
			if (shopTree.getObject().getType() != null) {
				nodeAttr.setShopType(shopTree.getObject().getType().getChannelTypeCode());
			}
			bean.setAttr(nodeAttr);
			List<JsTreeNode> lstChild = new ArrayList<JsTreeNode>();
			if (shopTree.getListChildren() != null && shopTree.getListChildren().size() > 0) {
				for (int i = 0; i < shopTree.getListChildren().size(); i++) {
					JsTreeNode subBean = new JsTreeNode();
					subBean = getUnitTreeBeanWithChildContaintName(subBean, shopTree.getListChildren().get(i));
					lstChild.add(subBean);
				}
				bean.setChildren(lstChild);
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
			} else {
				bean.setState(ConstantManager.JSTREE_STATE_CLOSE);
			}
		}
		return bean;*/
		return null;
	}

	/**
	 * Gets the area tree bean.
	 * 
	 * @param bean
	 *            the bean
	 * @param areaTree
	 *            the area tree
	 * @return the area tree bean
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	private JsTreeNode getAreaTreeBean(JsTreeNode bean, TreeVO<Area> areaTree) {
		if (areaTree != null && areaTree.getObject() != null) {
			bean.setData(areaTree.getObject().getAreaName());
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(areaTree.getObject().getId().toString());
			nodeAttr.setContentItemId(areaTree.getObject().getAreaCode());
			bean.setAttr(nodeAttr);
			List<JsTreeNode> lstChild = new ArrayList<JsTreeNode>();
			if (areaTree.getListChildren() != null && areaTree.getListChildren().size() > 0) {
				for (int i = 0; i < areaTree.getListChildren().size(); i++) {
					JsTreeNode subBean = new JsTreeNode();
					subBean = getAreaTreeBean(subBean, areaTree.getListChildren().get(i));
					lstChild.add(subBean);
				}
				bean.setChildren(lstChild);
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
			}
		}
		return bean;
	}

	/**
	 * Gets the js tree node.
	 * 
	 * @param node
	 *            the node
	 * @param channelType
	 *            the channel type
	 * @return the js tree node
	 * @author hungtx
	 */
	private JsTreeNode getJsTreeNode(JsTreeNode node, TreeVO<ChannelType> channelType) {
		if (channelType != null && channelType.getObject() != null) {
			node.setData(channelType.getObject().getChannelTypeCode() + "-" + channelType.getObject().getChannelTypeName());
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(channelType.getObject().getId().toString());
			nodeAttr.setContentItemId(channelType.getObject().getChannelTypeCode());
			node.setAttr(nodeAttr);
			List<JsTreeNode> lstChild = new ArrayList<JsTreeNode>();
			if (channelType.getListChildren() != null && channelType.getListChildren().size() > 0) {
				for (int i = 0; i < channelType.getListChildren().size(); i++) {
					JsTreeNode subBean = new JsTreeNode();
					subBean = getJsTreeNode(subBean, channelType.getListChildren().get(i));
					lstChild.add(subBean);
				}
				node.setChildren(lstChild);
				node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			}
		}
		return node;
	}

	/**
	 * Gets the shop name by shop code.
	 * 
	 * @param shopCode
	 *            the shop code
	 * @return the shop name by shop code
	 * @author hungtx
	 * @since 26/07/2012
	 */
	@RequestMapping(value = "/delivery-group/shop/{code}/name", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getShopNameByShopCode(@PathVariable("code") String shopCode) {
		StatusBean shopName = new StatusBean();
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			try {
				ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
				if (shopMgr != null) {
					Shop shop = shopMgr.getShopByCode(shopCode);
					if (shop != null) {
						shopName.setName(shop.getShopName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return shopName;
	}

	@RequestMapping(value = "/customer/{shopcode}/{shortcode}/name", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getCustomerNameByShortCode(@PathVariable("shortcode") String shortCode, @PathVariable("shopcode") String shopCode) {
		StatusBean customerName = new StatusBean();
		if (!StringUtil.isNullOrEmpty(shortCode)) {
			try {
				CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
				if (customerMgr != null) {
					Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, shortCode));
					if (customer != null) {
						customerName.setName(customer.getCustomerName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return customerName;
	}

	@RequestMapping(value = "/shop/{shopId}/code", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getShopCodeByShopId(@PathVariable("shopId") Long shopId) {
		StatusBean shopBean = new StatusBean();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				Shop shop = shopMgr.getShopById(shopId);
				if (shop != null) {
					shopBean.setName(shop.getShopCode());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return shopBean;
	}

	@RequestMapping(value = "/object/{objectId}/{objectTypeValue}/code", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getObjectCodeByObjectId(@PathVariable("objectId") Long objectId, @PathVariable("objectTypeValue") Integer objectTypeValue) {
		StatusBean bean = new StatusBean();
		try {
			ChannelTypeType objectType = ChannelTypeType.parseValue(objectTypeValue);
			if (objectType.equals(ChannelTypeType.CUSTOMER)) {

			} else if (objectType.equals(ChannelTypeType.SHOP)) {
				ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
				if (shopMgr != null) {
					Shop shop = shopMgr.getShopById(objectId);
					if (shop != null) {
						bean.setName(shop.getShopCode());
					}
				}
			} else if (objectType.equals(ChannelTypeType.STAFF)) {

			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return bean;
	}

	/**
	 * Gets the staff name by staff code.
	 * 
	 * @param staffCode
	 *            the staff code
	 * @return the staff name by staff code
	 */
	@RequestMapping(value = "/delivery-group/staff/{code}/name", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getStaffNameByStaffCode(@PathVariable("code") String staffCode) {
		StatusBean staffName = new StatusBean();
		if (!StringUtil.isNullOrEmpty(staffCode)) {
			try {
				StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
				if (staffMgr != null) {
					Staff staff = staffMgr.getStaffByCode(staffCode);
					if (staff != null) {
						staffName.setName(staff.getStaffName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return staffName;
	}

	/**
	 * Gets the customer name by customer code.
	 * 
	 * @param customerCode
	 *            the customer code
	 * @param shopCode
	 *            the shop code
	 * @return the customer name by customer code
	 */
	@RequestMapping(value = "/delivery-group/customer/{code}/{shopCode}/name", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getCustomerNameByCustomerCode(@PathVariable("code") String customerCode, @PathVariable("shopCode") String shopCode) {
		StatusBean customerName = new StatusBean();
		if (!StringUtil.isNullOrEmpty(customerCode)) {
			try {
				CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
				if (customerMgr != null) {
					Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, customerCode));
					if (customer != null) {
						customerName.setName(customer.getCustomerName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return customerName;
	}

	/**
	 * Gets the promotion type.
	 * 
	 * @param typeCode
	 *            the type code
	 * @return the promotion type
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/promotion/name/{typeCode}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getPromotionType(@PathVariable("typeCode") Long typeCode) {
		StatusBean promotionTypeName = new StatusBean(0L, "");
		if (typeCode != null) {
			try {
				ApParamMgr apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
				if (apParamMgr != null) {
					ApParam apParam = apParamMgr.getApParamById(typeCode);
					if (apParam != null) {
						promotionTypeName.setName(apParam.getValue());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return promotionTypeName;
	}


	/**
	 * Lay danh sach HTBH
	 * @modify hunglm16
	 * @param id
	 * @return Danh sach HTBH dang hoat dong
	 * @since 05/11/2015
	 */
	@RequestMapping(value = "/focus-program/product/sale-type-code", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getSaleTypeCodeForFocus(@RequestParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			FocusProgramMgr focusProgramMgr = (FocusProgramMgr) context.getBean("focusProgramMgr");
			if (focusProgramMgr != null) {
				ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, id, null, ActiveType.RUNNING);
				if (focusChannelMapVO != null) {
					result.put("total", focusChannelMapVO.getLstObject().size());
					result.put("rows", focusChannelMapVO.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/focus-program/product/type", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getTypeForFocus() {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ApParamMgr apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
			if (apParamMgr != null) {
				ProductFilter filter = new ProductFilter();
				filter.setIsExceptZCat(true);
				filter.setIsPriceValid(true);
				filter.setStatus(ActiveType.RUNNING);
				List<ApParam> lstType = apParamMgr.getListApParam(ApParamType.FOCUS_PRODUCT_TYPE, ActiveType.RUNNING);
				if (lstType != null) {
					result.put("total", lstType.size());
					result.put("rows", lstType);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/bary-centric/sale-type/list", method = RequestMethod.GET)
	public @ResponseBody
	List<FocusChannelMap> getSaleTypeCodeByFocusId(@RequestParam("id") Long id) {
		List<FocusChannelMap> lstFocusChannelMap = new ArrayList<FocusChannelMap>();
		try {
			FocusProgramMgr focusProgramMgr = (FocusProgramMgr) context.getBean("focusProgramMgr");
			if (focusProgramMgr != null) {
				ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, id, null, ActiveType.RUNNING);
				if (focusChannelMapVO != null) {
					lstFocusChannelMap = focusChannelMapVO.getLstObject();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstFocusChannelMap;
	}

	@RequestMapping(value = "/product/list", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getJsonListProduct() {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
			if (productMgr != null) {
				ProductFilter filter = new ProductFilter();
				filter.setIsExceptZCat(true);
				filter.setIsPriceValid(true);
				filter.setStatus(ActiveType.RUNNING);
				ObjectVO<Product> lstProduct = productMgr.getListProductEx(filter);
				if (lstProduct != null) {
					result.put("total", lstProduct.getLstObject().size());
					result.put("rows", lstProduct.getLstObject());
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return result;
	}

	/**
	 * Gets the list staff type.
	 * 
	 * @param focusProgramId
	 *            the focus program id
	 * @return the list staff type
	 * @author hungtx
	 * @since 02/08/2012
	 */
	@RequestMapping(value = "/focus-program/{focusProgramId}/staff-type/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getListStaffType(@PathVariable("focusProgramId") Long focusProgramId) {
		List<StatusBean> lstStaffType = new ArrayList<StatusBean>();
		if (focusProgramId != null && focusProgramId > 0) {
			try {
				FocusProgramMgr focusProgramMgr = (FocusProgramMgr) context.getBean("focusProgramMgr");
				ApParamMgr apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
				if (focusProgramMgr != null) {
					ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, focusProgramId, null, ActiveType.RUNNING);
					if (focusChannelMapVO != null && focusChannelMapVO.getLstObject() != null && focusChannelMapVO.getLstObject().size() > 0) {
						for (int i = 0; i < focusChannelMapVO.getLstObject().size(); i++) {
							if (focusChannelMapVO.getLstObject().get(i).getSaleTypeCode() != null) {
								StatusBean obj = new StatusBean();
								obj.setName(focusChannelMapVO.getLstObject().get(i).getSaleTypeCode());
								ApParam ap = apParamMgr.getApParamByCode(focusChannelMapVO.getLstObject().get(i).getSaleTypeCode(), ApParamType.STAFF_SALE_TYPE);
								obj.setValue(ap.getId());
								lstStaffType.add(obj);
							}

						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return lstStaffType;
	}

	//me
	@RequestMapping(value = "/focus-program/apparam/{code}/id", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getApParamIdByApParamCode(@PathVariable("code") String apParamCode) {
		StatusBean apParamIdBean = new StatusBean();
		if (!StringUtil.isNullOrEmpty(apParamCode)) {
			try {
				ApParamMgr apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
				if (apParamMgr != null) {
					ApParam apParam = apParamMgr.getApParamByCode(apParamCode, null);
					if (apParam != null) {
						apParamIdBean.setValue(apParam.getId());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return apParamIdBean;
	}

	//
	/**
	 * Gets the list customer type.
	 * 
	 * @param shopId
	 *            the shop id
	 * @return the list customer type
	 * @author hungtx
	 * @since 03/08/2012
	 * @RequestMapping(value = "/delivery-group/{shopId}/customer-type/list",
	 *                       method = RequestMethod.GET) public @ResponseBody
	 *                       List<StatusBean>
	 *                       getListCustomerTypeByShop(@PathVariable("shopId")
	 *                       Long shopId){ List<StatusBean> lstCustomerType =
	 *                       new ArrayList<StatusBean>(); if(shopId!= null &&
	 *                       shopId > 0){ try{ GroupTransferMgr groupTransferMgr
	 *                       = (GroupTransferMgr)context.getBean(
	 *                       "groupTransferMgr"); if(groupTransferMgr!= null){
	 *                       ObjectVO<GroupTransfer> groupTransferVO =
	 *                       groupTransferMgr.getListGroupTransfer(null, shopId,
	 *                       null, null, null, null,
	 *                       ActiveType.RUNNING,null,true); if(groupTransferVO!=
	 *                       null && groupTransferVO.getLstObject()!= null &&
	 *                       groupTransferVO.getLstObject().size() > 0){ for(int
	 *                       i=0;i<groupTransferVO.getLstObject().size();i++){
	 *                       StatusBean obj = new StatusBean();
	 *                       obj.setName(groupTransferVO
	 *                       .getLstObject().get(i).getGroupTransferName());
	 *                       obj.setValue(groupTransferVO.getLstObject().get(i).
	 *                       getId()); lstCustomerType.add(obj); } } } }catch
	 *                       (Exception e) {
	 *                       LogUtility.logError(e,e.getMessage()); } } return
	 *                       lstCustomerType; }
	 */

	/**
	 * Gets the list cashier by shop.
	 * 
	 * @param shopCode
	 *            the shop code
	 * @return the list cashier by shop
	 * @author hungtx
	 * @since 03/08/2012
	 */
	@RequestMapping(value = "/staff/{shopCode}/cashier/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getListCashierByShop(@PathVariable("shopCode") String shopCode) {
		List<StatusBean> lstCustomerType = new ArrayList<StatusBean>();
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			try {
				StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
				if (staffMgr != null) {
					StaffFilter filter = new StaffFilter();
					filter.setStatus(ActiveType.RUNNING);
					filter.setShopCode(shopCode);
					ObjectVO<Staff> staffVO = staffMgr.getListStaff(filter);
					if (staffVO != null && staffVO.getLstObject() != null && staffVO.getLstObject().size() > 0) {
						for (int i = 0; i < staffVO.getLstObject().size(); i++) {
							StatusBean obj = new StatusBean();
							obj.setName(staffVO.getLstObject().get(i).getStaffName());
							obj.setValue(staffVO.getLstObject().get(i).getId());
							lstCustomerType.add(obj);
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return lstCustomerType;
	}

	/**
	 * Gets the vietbando image url.
	 * 
	 * @param address
	 *            the address
	 * @return the vietbando image url
	 */
	@RequestMapping(value = "/vietbando/image/{address}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getVietbandoImageUrl(@PathVariable("address") String address) {
		StatusBean res = new StatusBean();
		if (!StringUtil.isNullOrEmpty(address)) {
			VietBanDoMgr vietbandoMgr = (VietBanDoMgr) context.getBean("vietBanDoMgr");
			Double lat = ConstantManager.LAT_LNG_EMPTY;
			Double lng = ConstantManager.LAT_LNG_EMPTY;
			if (vietbandoMgr != null) {
				try {
					LatLng latLng = vietbandoMgr.fromStreetToLatLng(address, StringUtil.getRequestLog());
					if (latLng != null) {
						lat = latLng.getLat();
						lng = latLng.getLng();
					}
				} catch (BusinessException e) {
					LogUtility.logError(e, e.getMessage());
				}
			}
			res.setName(StringUtil.getMapImageUrl(lat, lng, ConstantManager.MAP_ZOOM_DEFAULT));
		}
		return res;
	}

	/**
	 * Gets the category name.
	 * 
	 * @param categoryType
	 *            the category type
	 * @return the category name
	 */
	@RequestMapping(value = "/category/name/{categoryType}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getCategoryName(@PathVariable("categoryType") Long categoryType) {
		StatusBean categoryName = new StatusBean(0L, "");
		if (categoryType != null) {
			try {
				ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
				if (productInfoMgr != null) {
					ProductInfo productInfo = productInfoMgr.getProductInfoById(categoryType);
					if (productInfo != null) {
						categoryName.setName(productInfo.getProductInfoName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return categoryName;
	}

	/**
	 * Gets the list level by cat.
	 * 
	 * @param categoryType
	 *            the category type
	 * @return the list level by cat
	 */
	@RequestMapping(value = "/category/{categoryType}/level/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getListLevelByCat(@PathVariable("categoryType") Long categoryType) {
		List<StatusBean> lstLevel = new ArrayList<StatusBean>();
		if (categoryType != null && categoryType != -2) {
			try {
				SaleLevelCatMgr saleLevelCatMgr = (SaleLevelCatMgr) context.getBean("saleLevelCatMgr");
				if (saleLevelCatMgr != null) {
					List<String> lstLevelIncome = null;
					if (categoryType == -1) {
						lstLevelIncome = saleLevelCatMgr.getListSaleLevel(null, null, null, ActiveType.RUNNING);
					} else {
						lstLevelIncome = saleLevelCatMgr.getListSaleLevel(categoryType, null, null, ActiveType.RUNNING);
					}
					if (lstLevelIncome != null && lstLevelIncome.size() > 0) {
						for (int i = 0; i < lstLevelIncome.size(); i++) {
							StatusBean obj = new StatusBean();
							obj.setName(lstLevelIncome.get(i));
							lstLevel.add(obj);
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return lstLevel;
	}

	/**
	 * Gets the list sale level cat.
	 * 
	 * @param catId
	 *            the cat id
	 * @return the list sale level cat
	 */
	@RequestMapping(value = "/sale-level-cat/list/{catId}/{levelId}", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getListSaleLevelCat(@PathVariable("catId") Long catId, @PathVariable("levelId") Long levelId) {
		List<StatusBean> lstCustomerType = new ArrayList<StatusBean>();
		if (catId != null && catId > 0) {
			try {
				SaleLevelCatMgr saleLevelCatMgr = (SaleLevelCatMgr) context.getBean("saleLevelCatMgr");
				if (saleLevelCatMgr != null) {
					ObjectVO<SaleLevelCat> saleLevelCatVO = saleLevelCatMgr.getListSaleLevelCat(null, catId, null, null, ActiveType.RUNNING);
					if (saleLevelCatVO != null && saleLevelCatVO.getLstObject() != null && saleLevelCatVO.getLstObject().size() > 0) {
						SaleLevelCat level = saleLevelCatMgr.getSaleLevelCatById(levelId);
						if (level != null && !level.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
							saleLevelCatVO.getLstObject().add(0, level);
						}
						for (int i = 0; i < saleLevelCatVO.getLstObject().size(); i++) {
							StatusBean obj = new StatusBean();
							obj.setName(saleLevelCatVO.getLstObject().get(i).getSaleLevelName());
							obj.setValue(saleLevelCatVO.getLstObject().get(i).getId());
							lstCustomerType.add(obj);
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return lstCustomerType;
	}

	/**
	 * Gets the list cat.
	 * 
	 * @param catId
	 *            the cat id
	 * @return the list cat
	 * @author tientv
	 */
	@RequestMapping(value = "/customer-cat/list/{catId}", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getListCat(@PathVariable("catId") Long catId) {
		List<StatusBean> lstCustomerType = new ArrayList<StatusBean>();
		if (catId != null) {
			try {
				ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
				if (productInfoMgr != null) {
					/*ObjectVO<ProductInfo> CatVO = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true);
					List<ProductInfo> lstCat = new ArrayList<ProductInfo>();
					if (CatVO != null) {
						lstCat = CatVO.getLstObject();
					}
					ProductInfo pf = productInfoMgr.getProductInfoById(catId);
					if (pf != null && pf.getStatus().getValue().equals(ActiveType.STOPPED.getValue())) {
						lstCat.add(0, pf);
					}
					for (ProductInfo obj : lstCat) {
						StatusBean statusBean = new StatusBean();
						statusBean.setName(StringUtil.CodeAddName(obj.getProductInfoCode(), obj.getProductInfoName()));
						statusBean.setValue(obj.getId());
						lstCustomerType.add(statusBean);
					}*/
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return lstCustomerType;
	}

	/**
	 * Gets the promotion name.
	 * 
	 * @param promotionCode
	 *            the promotion code
	 * @return the promotion name
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/promotion/code/{promotionCode}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getPromotionName(@PathVariable("promotionCode") String promotionCode) {
		StatusBean promotionTypeName = new StatusBean(0L, "");
		if (promotionCode != null) {
			try {
				PromotionProgramMgr promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
				if (promotionProgramMgr != null) {
					PromotionProgram promotionProgram = promotionProgramMgr.getPromotionProgramByCode(promotionCode);
					if (promotionProgram != null) {
						promotionTypeName.setName(promotionProgram.getPromotionProgramName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return promotionTypeName;
	}

	/**
	 * Gets the product name.
	 * 
	 * @param productCode
	 *            the product code
	 * @return the product name
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/product/getname/{productCode}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getProductName(@PathVariable("productCode") String productCode) {
		StatusBean promotionTypeName = new StatusBean(0L, "");
		if (productCode != null) {
			try {
				ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
				if (productMgr != null) {
					Product product = productMgr.getProductByCode(productCode);
					if (product != null) {
						promotionTypeName.setName(product.getProductName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return promotionTypeName;
	}

	/**
	 * Gets the supervisor staff name.
	 * 
	 * @param staffCode
	 *            the staff code
	 * @return the supervisor staff name
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/supervisor-staff/name/{staffCode}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getSupervisorStaffName(@PathVariable("staffCode") String staffCode) {
		StatusBean staffName = new StatusBean(0L, "");
		if (staffCode != null) {
			try {
				StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
				if (staffMgr != null) {
					Staff staff = staffMgr.getStaffByCode(staffCode);
					if (staff != null) {
						staffName.setName(staff.getStaffName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return staffName;
	}

	/**
	 * Gets the product category of focus program.
	 * 
	 * @param displayProgramId
	 *            the display program id
	 * @return the product category of focus program
	 */
	@RequestMapping(value = "/display-program/product/category/{displayProgramId}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getProductCategoryOfFocusProgram(@PathVariable("displayProgramId") String displayProgramId) {
		StatusBean bean = new StatusBean();
		if (displayProgramId != null) {
			try {
				/*
				 * DisplayProgramMgr displayProgramMgr =
				 * (DisplayProgramMgr)context.getBean("displayProgramMgr");
				 * if(displayProgramMgr!= null){ DisplayProgram displayProgram =
				 * displayProgramMgr
				 * .getDisplayProgramById(Long.valueOf(displayProgramId));
				 * if(displayProgram!= null ){
				 * bean.setName(displayProgram.getCat().getProductInfoCode()); }
				 * }
				 */
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return bean;
	}

	/**
	 * Gets the customer shop tree.
	 * 
	 * @return the customer shop tree
	 */
	@RequestMapping(value = "/customer/shop/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getCustomerShopTree() {
		List<JsTreeNode> lstUnitTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
			if (shopMgr != null && staffMgr != null) {
				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
				HttpSession session = attr.getRequest().getSession(true);
				UserToken currentUser = null;
				if (session.getAttribute("cmsUserToken") != null) {
					currentUser = (UserToken) session.getAttribute("cmsUserToken");
					Shop shop = null;
					if (currentUser != null) {
						Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
						if (staff != null) {
							shop = staff.getShop();
						}
						TreeVO<Shop> shopTreeVO = shopMgr.getShopTreeVO(shop, null, null, null, true);
						if (shopTreeVO.getObject() == null && shopTreeVO.getListChildren() != null) {
							for (int i = 0; i < shopTreeVO.getListChildren().size(); i++) {
								JsTreeNode tmp = new JsTreeNode();
								tmp = getUnitTreeBean(tmp, shopTreeVO.getListChildren().get(i));
								lstUnitTree.add(tmp);
							}
						} else {
							bean = getUnitTreeBean(bean, shopTreeVO);
							lstUnitTree.add(bean);
						}
					}
				}
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstUnitTree;
	}

	/**
	 * Gets the list exception days.
	 * 
	 * @param month
	 *            the month
	 * @param year
	 *            the year
	 * @return the list exception days
	 */
	@RequestMapping(value = "/sale-day/except-day/{month}/{year}/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CellBean> getListExceptionDays(@PathVariable("month") String month, @PathVariable("year") String year, @RequestParam("shopCode") String shopCode) {
		List<CellBean> lstExcDays = new ArrayList<CellBean>();
		try {
			ExceptionDayMgr exceptionDayMgr = (ExceptionDayMgr) context.getBean("exceptionDayMgr");
			ApParamMgr apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			List<ExceptionDay> lstDays = null;
			Long shopId = -1L;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			HashMap<Long, Long> mapShop = null;
			if (session.getAttribute("mapShop") != null) {
				mapShop = (HashMap<Long, Long>) session.getAttribute("mapShop");
			}
			if (shopMgr != null) {
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					Shop s = shopMgr.getShopByCode(shopCode);
					if (s != null) {
						if (mapShop != null && mapShop.get(shopId) != null) {
							return lstExcDays;
						}
						shopId = s.getId();
					}
				}
			}
			
			if (exceptionDayMgr != null && apParamMgr != null) {
				if (!StringUtil.isNullOrEmpty(month) && !"0".equals(month)) {
					lstDays = exceptionDayMgr.getExceptionDayByMonth(Integer.valueOf(month), Integer.valueOf(year), shopId);
				} else {
					lstDays = exceptionDayMgr.getExceptionDayByYear(Integer.valueOf(year), shopId);
				}
				if (lstDays != null) {
					int[] arrNumDays = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
					for (int i = 0; i < lstDays.size(); i++) {
						CellBean bean = new CellBean();
						Calendar calendar = new GregorianCalendar();
						calendar.setTime(lstDays.get(i).getDayOff());
						bean.setContent1("tm_" + String.valueOf(calendar.get(Calendar.DATE)) + "_" + String.valueOf(calendar.get(Calendar.MONTH) + 1));
						lstExcDays.add(bean);
						arrNumDays[calendar.get(Calendar.MONTH)] = arrNumDays[calendar.get(Calendar.MONTH)] + 1;
					}
					CellBean bean = new CellBean();
					bean.setContent1(String.valueOf(arrNumDays[0]));
					bean.setContent2(String.valueOf(arrNumDays[1]));
					bean.setContent3(String.valueOf(arrNumDays[2]));
					bean.setContent4(String.valueOf(arrNumDays[3]));
					bean.setContent5(String.valueOf(arrNumDays[4]));
					bean.setContent6(String.valueOf(arrNumDays[5]));
					bean.setContent7(String.valueOf(arrNumDays[6]));
					bean.setContent8(String.valueOf(arrNumDays[7]));
					bean.setContent9(String.valueOf(arrNumDays[8]));
					bean.setContent10(String.valueOf(arrNumDays[9]));
					bean.setContent11(String.valueOf(arrNumDays[10]));
					bean.setContent12(String.valueOf(arrNumDays[11]));
					lstExcDays.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}

		return lstExcDays;
	}
	/**
	 * Gets the list exception days for working date.
	 * 
	 * @param month the month
	 * @param year the year
	 * @return the list exception days
	 */
	@RequestMapping(value = "/working-date/except-day/{month}/{year}/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CellBean> getListExceptionDays4WorkingDate(@PathVariable("month") String month, @PathVariable("year") String year, @RequestParam("shopCode") String shopCode) {
		List<CellBean> lstExcDays = new ArrayList<CellBean>();
		try {
			ExceptionDayMgr exceptionDayMgr = (ExceptionDayMgr) context.getBean("exceptionDayMgr");
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			List<ExceptionDay> lstDays = null;
			Long shopId = -1L;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = null;
			HashMap<Long, Long> mapShop = null;
			if (session.getAttribute("cmsUserToken") != null) {
				currentUser = (UserToken) session.getAttribute("cmsUserToken");
			}
			if (session.getAttribute("mapShop") != null) {
				mapShop = (HashMap<Long, Long>) session.getAttribute("mapShop");
			}
			if (shopMgr != null) {
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					Shop s = shopMgr.getShopByCode(shopCode);
					if (s != null) {
						if (mapShop != null && mapShop.get(shopId) != null) {
							return lstExcDays;
						}
						shopId = s.getId();
					}
				}
			}
			if (exceptionDayMgr != null /*&& apParamMgr != null*/) {
				if (!StringUtil.isNullOrEmpty(month) && !"0".equals(month)) {
					lstDays = exceptionDayMgr.getExceptionDayByMonth(Integer.valueOf(month), Integer.valueOf(year), shopId);
				} else {
					lstDays = exceptionDayMgr.getExceptionDayByYear(Integer.valueOf(year), shopId);
				}
				if (lstDays != null) {
					int[] arrNumDays = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
					for (int i = 0; i < lstDays.size(); i++) {
						CellBean bean = new CellBean();
						Calendar calendar = new GregorianCalendar();
						calendar.setTime(lstDays.get(i).getDayOff());
						bean.setContent1("tm_" + String.valueOf(calendar.get(Calendar.DATE)) + "_" + String.valueOf(calendar.get(Calendar.MONTH) + 1));
						if (lstDays.get(i).getReason() != null) {
							bean.setContent2(String.valueOf(lstDays.get(i).getReason()));
							bean.setContent3(String.valueOf(lstDays.get(i).getReason()));
						}
						
						lstExcDays.add(bean);
						arrNumDays[calendar.get(Calendar.MONTH)] = arrNumDays[calendar.get(Calendar.MONTH)] + 1;
					}
					CellBean bean = new CellBean();
					bean.setContent1(String.valueOf(arrNumDays[0]));
					bean.setContent2(String.valueOf(arrNumDays[1]));
					bean.setContent3(String.valueOf(arrNumDays[2]));
					bean.setContent4(String.valueOf(arrNumDays[3]));
					bean.setContent5(String.valueOf(arrNumDays[4]));
					bean.setContent6(String.valueOf(arrNumDays[5]));
					bean.setContent7(String.valueOf(arrNumDays[6]));
					bean.setContent8(String.valueOf(arrNumDays[7]));
					bean.setContent9(String.valueOf(arrNumDays[8]));
					bean.setContent10(String.valueOf(arrNumDays[9]));
					bean.setContent11(String.valueOf(arrNumDays[10]));
					bean.setContent12(String.valueOf(arrNumDays[11]));
					lstExcDays.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}

		return lstExcDays;
	}
	/**
	 * Gets the max stock card number.
	 * 
	 * @param cycleCountId
	 *            the cycle count id
	 * @return the max stock card number
	 */
	@RequestMapping(value = "/stock/cycleCount/{cycleCountCode}/list", method = RequestMethod.GET)
	public @ResponseBody
	Integer getMaxStockCardNumber(@PathVariable("cycleCountCode") String cycleCountCode) {
		Integer maxStockCardNumber = 0;
		if (!StringUtil.isNullOrEmpty(cycleCountCode)) {
			try {
				Long ownerId = null;
				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
				HttpSession session = attr.getRequest().getSession(true);
				if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP_ID) != null) {
					ownerId = Long.valueOf(String.valueOf(session.getAttribute(ConstantManager.SESSION_SHOP_ID)));
				}
				if (ownerId == null)
					return null;
				CycleCountMgr cycleCountMgr = (CycleCountMgr) context.getBean("cycleCountMgr");
				if (cycleCountMgr != null) {
					CycleCount cycleCount = cycleCountMgr.getCycleCountByCodeAndShop(cycleCountCode, ownerId);
					if (cycleCount != null) {
						maxStockCardNumber = cycleCountMgr.getMaxStockCardNumber(cycleCount.getId());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return maxStockCardNumber;
	}

	/**
	 * Gets availableQuantity of Product.
	 * 
	 * @param shopCode
	 *            the shop code
	 * @param productCode
	 *            lot
	 * @param lot
	 *            the lot
	 * @return the available quantity product
	 * @author ThongNM
	 */
	@RequestMapping(value = "/stock/cyclecount/product/getavailablequantity", method = RequestMethod.GET)
	public @ResponseBody
	Integer getAvailableQuantityProduct(@RequestParam("shopCode") String shopCode, @RequestParam("productCode") String productCode, @RequestParam("lot") String lot) {
		Integer availableQuantity = 0;
		if (!StringUtil.isNullOrEmpty(productCode)) {
			try {
				StockMgr stockMgr = (StockMgr) context.getBean("stockMgr");
				ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
				ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (lot == null || lot.equals("")) {
					StockTotal stocktotal = stockMgr.getStockTotalByProductCodeAndOwner(productCode, StockObjectType.SHOP, shop.getId());
					if (stocktotal != null) {
						availableQuantity = stocktotal.getAvailableQuantity();
					}
				} else {
					Product product = productMgr.getProductByCode(productCode);
					if (product != null) {
						ProductLot productlot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lot, shop.getId(), StockObjectType.SHOP, product.getId());
						if (productlot != null) {
							availableQuantity = productlot.getAvailableQuantity();
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return availableQuantity;
	}

	/**
	 * Gets Quantity of StockTotal or ProductLot ( Ton kho).
	 * 
	 * @param shopCode
	 *            the shop code
	 * @param productCode
	 *            lot
	 * @param lot
	 *            the lot
	 * @return the quantity before counted
	 * @author ThongNM
	 */
	@RequestMapping(value = "/stock/cyclecount/product/getquantitybeforecounted", method = RequestMethod.GET)
	public @ResponseBody
	Integer getQuantityBeforeCounted(@RequestParam("shopCode") String shopCode, @RequestParam("productCode") String productCode, @RequestParam("lot") String lot) {
		Integer quantityBeforeCounted = 0;
		if (!StringUtil.isNullOrEmpty(productCode)) {
			try {
				StockMgr stockMgr = (StockMgr) context.getBean("stockMgr");
				ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
				ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (lot.equals("")) {
					StockTotal stocktotal = stockMgr.getStockTotalByProductCodeAndOwner(productCode, StockObjectType.SHOP, shop.getId());
					if (stocktotal != null) {
						quantityBeforeCounted = stocktotal.getQuantity();
					}
				} else {
					Product product = productMgr.getProductByCode(productCode);
					if (product != null) {
						ProductLot productlot = stockMgr.getProductLotByCodeAndOwnerAndProduct(lot, shop.getId(), StockObjectType.SHOP, product.getId());
						if (productlot != null) {
							quantityBeforeCounted = productlot.getQuantity();
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return quantityBeforeCounted;
	}

	/**
	 * Gets ParentChanelType.
	 * 
	 * @param channelTypeId
	 *            the channel type id
	 * @return the channel type
	 * @author ThongNM
	 */
	@RequestMapping(value = "/catalog/staff-type/channeltype", method = RequestMethod.GET)
	public @ResponseBody
	ChannelType getChannelType(@RequestParam("channelTypeId") Long channelTypeId) {
		ChannelType channelType = null;
		try {
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			channelType = channelTypeMgr.getChannelTypeById(channelTypeId);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return channelType;
	}

	/**
	 * Gets TotalDebit of Customer
	 * 
	 * @param shorCode
	 *            & shopCode
	 * @return total debit value
	 */
	@RequestMapping(value = "/debit/gettotaldebit", method = RequestMethod.GET)
	public @ResponseBody
	BigDecimal getTotalDebitCustomer(@RequestParam("shortCode") String shortCode, @RequestParam("shopCode") String shopCode) {
		BigDecimal totalDebitBeforePay = new BigDecimal(0);
		if (!StringUtil.isNullOrEmpty(shortCode) && !StringUtil.isNullOrEmpty(shopCode)) {
			try {
				DebitMgr debitMgr = (DebitMgr) context.getBean("debitMgr");
				CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
				if (debitMgr != null && customerMgr != null) {
					Customer customerTemp = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, shortCode));
					if (customerTemp != null) {
						Debit debitGeneralTmp = debitMgr.getDebit(customerTemp.getId(), DebitOwnerType.CUSTOMER);
						totalDebitBeforePay = debitGeneralTmp.getTotalDebit();
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return totalDebitBeforePay;
	}

	/**
	 * Gets the before pay.
	 * 
	 * @param shortCode
	 *            the short code
	 * @param shopCode
	 *            the shop code
	 * @return the before pay
	 * @author CuongND
	 */
	@RequestMapping(value = "/debitAuto/getBeforePay/{shortCode}/{shopCode}/json", method = RequestMethod.GET)
	public @ResponseBody
	BigDecimal getBeforePay(@PathVariable("shortCode") String shortCode, @PathVariable("shopCode") String shopCode) {
		BigDecimal result = new BigDecimal(0);
		if (!StringUtil.isNullOrEmpty(shortCode) && !StringUtil.isNullOrEmpty(shopCode)) {
			try {
				DebitMgr debitMgr = (DebitMgr) context.getBean("debitMgr");
				CustomerMgr customer = (CustomerMgr) context.getBean("customerMgr");
				Customer a = customer.getCustomerByCode(StringUtil.getFullCode(shopCode, shortCode));
				if (a != null) {
					Debit b = debitMgr.getDebit(a.getId(), DebitOwnerType.CUSTOMER);
					if (b != null) {
						result = b.getTotalDebit();
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return result;
	}

	/**
	 * Gets the staff name by code.
	 * 
	 * @param staffCode
	 *            the staff code
	 * @return the staff name by code
	 */
	@RequestMapping(value = "/stock/receivedGetName/{staffCode}/list", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getStaffNameByCode(@PathVariable("staffCode") String staffCode) {
		StatusBean result = new StatusBean();
		if (!StringUtil.isNullOrEmpty(staffCode)) {
			try {
				StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
				if (staffMgr != null) {
					Staff a = staffMgr.getStaffByCode(staffCode);
					if (a != null) {
						result.setName(a.getStaffName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return result;
	}

	/**
	 * Gets the customer name by code.
	 * 
	 * @param shortCode
	 *            the short code
	 * @return the customer name by code
	 */
	@RequestMapping(value = "/showdebit/showdebitGetName/{shortCode}/{shopCode}/list", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getCustomerNameByCode(@PathVariable("shortCode") String shortCode, @PathVariable("shopCode") String shopCode) {
		StatusBean result = new StatusBean();
		if (!StringUtil.isNullOrEmpty(shortCode) && !StringUtil.isNullOrEmpty(shopCode)) {
			try {
				CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
				if (customerMgr != null) {
					Customer a = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, shortCode));
					if (a != null) {
						result.setName(a.getCustomerName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return result;
	}

	/**
	 * Load combo tree.
	 * 
	 * @param node
	 *            the node
	 * @param channelType
	 *            the channel type
	 * @return the combo tree bean
	 * @author hungtx
	 * @since Sep 25, 2012
	 */
	private ComboTreeBean loadComboTreeForChannelType(ComboTreeBean node, TreeVO<ChannelType> channelType) {
		if (channelType != null && channelType.getObject() != null) {
			node.setId(String.valueOf(channelType.getObject().getId().toString()));
			node.setText(channelType.getObject().getChannelTypeCode() + "-" + channelType.getObject().getChannelTypeName());
			List<ComboTreeBean> lstChild = new ArrayList<ComboTreeBean>();
			if (channelType.getListChildren() != null && channelType.getListChildren().size() > 0) {
				for (int i = 0; i < channelType.getListChildren().size(); i++) {
					ComboTreeBean subBean = new ComboTreeBean();
					subBean.setState(ConstantManager.JSTREE_STATE_CLOSE);
					subBean = loadComboTreeForChannelType(subBean, channelType.getListChildren().get(i));
					lstChild.add(subBean);
				}
				node.setChildren(lstChild);
				node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			} else {
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
			}
		}
		return node;
	}

	/**
	 * Gets the customer type combo tree.
	 * 
	 * @param type
	 *            the type
	 * @param currentNode
	 *            the current node
	 * @return the customer type combo tree
	 */
	@RequestMapping(value = "/customer-type/combotree/{type}/{currentNode}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getCustomerTypeComboTree(@PathVariable("type") int type, @PathVariable("currentNode") Long currentNode) {
		List<ComboTreeBean> lstCustomerTypeTree = new ArrayList<ComboTreeBean>();
		ComboTreeBean allBean = new ComboTreeBean();
		allBean.setId(String.valueOf(ConstantManager.SELECT_ALL));
		allBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.type.select"));
		lstCustomerTypeTree.add(allBean);
		try {
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			if (channelTypeMgr != null) {
				TreeVO<ChannelType> channelTypeTreeVO = null;
				if (type == 1) {
					channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVOEx(null, ChannelTypeType.CUSTOMER, currentNode);
				} else {
					channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVO(null, ChannelTypeType.CUSTOMER, false);
				}
				if (channelTypeTreeVO.getObject() == null && channelTypeTreeVO.getListChildren() != null) {
					for (int i = 0; i < channelTypeTreeVO.getListChildren().size(); i++) {
						ComboTreeBean tmp = new ComboTreeBean();
						tmp = loadComboTreeForChannelType(tmp, channelTypeTreeVO.getListChildren().get(i));
						lstCustomerTypeTree.add(tmp);
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					bean = loadComboTreeForChannelType(bean, channelTypeTreeVO);
					bean.setState(ConstantManager.JSTREE_STATE_OPEN);
					lstCustomerTypeTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstCustomerTypeTree;
	}

	/**
	 * Node open.
	 * 
	 * @param shopId
	 *            the shop id
	 * @param currentShopId
	 *            the current shop id
	 * @return the combo tree bean
	 * @author tientv
	 * @since
	 */
	private HashMap<Long, Long> nodeOpen(Long shopId, Long currentShopId) {
		HashMap<Long, Long> list = new HashMap<Long, Long>();
		ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
		try {
			Shop shop = shopMgr.getShopById(shopId);
			//list.add(currentShopId);
			if (shop == null) {
				return list;
			}
			while (shop.getParentShop() != null && !shop.getId().equals(currentShopId)) {
				shop = shopMgr.getShopById(shop.getParentShop().getId());
				list.put(shop.getId(), shop.getId());
			}

		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
		}
		return list;
	}

	/**
	 * Gets the staff type combo tree.
	 * 
	 * @param type
	 *            the type
	 * @param currentNode
	 *            the current node
	 * @return the staff type combo tree
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/staff-type/combotree/{type}/{currentNode}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getStaffTypeComboTree(@PathVariable("type") int type, @PathVariable("currentNode") Long currentNode) {
		List<ComboTreeBean> lstStaffTypeTree = new ArrayList<ComboTreeBean>();
		ComboTreeBean nodeNull = new ComboTreeBean();
		nodeNull.setId("-1");
		nodeNull.setState("open");
		nodeNull.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.tree.choose.parent"));
		lstStaffTypeTree.add(nodeNull);
		try {
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			if (channelTypeMgr != null) {
				TreeVO<ChannelType> channelTypeTreeVO = null;
				if (type == 1) {
					channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVOEx(null, ChannelTypeType.STAFF, currentNode);
				} else {
					channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVO(null, ChannelTypeType.STAFF, true);
				}
				if (channelTypeTreeVO.getObject() == null && channelTypeTreeVO.getListChildren() != null) {
					for (int i = 0; i < channelTypeTreeVO.getListChildren().size(); i++) {
						ComboTreeBean tmp = new ComboTreeBean();
						tmp = loadComboTreeForChannelType(tmp, channelTypeTreeVO.getListChildren().get(i));
						lstStaffTypeTree.add(tmp);
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					bean = loadComboTreeForChannelType(bean, channelTypeTreeVO);
					lstStaffTypeTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstStaffTypeTree;
	}

	/**
	 * Gets the list product of display program.
	 * 
	 * @param displayProgramId
	 *            the display program id
	 * @param productCode
	 *            the product code
	 * @param productName
	 *            the product name
	 * @param nodeType
	 *            the node type
	 * @param catId
	 *            the cat id
	 * @param subCatId
	 *            the sub cat id
	 * @return the list product of display program
	 */
	/*
	 * @RequestMapping(value = "/display-program/product/list.json", method =
	 * RequestMethod.GET) public @ResponseBody List<JsTreeNode>
	 * getListProductOfDisplayProgram(@RequestParam("displayProgramId") long
	 * displayProgramId,
	 * 
	 * @RequestParam("productCode") String
	 * productCode,@RequestParam("productName") String productName,
	 * @RequestParam("nodeType") String nodeType, @RequestParam("catId") long
	 * catId, @RequestParam("subCatId") long subCatId){ List<JsTreeNode> lstCat
	 * = new ArrayList<JsTreeNode>();
	 * if(!StringUtil.isNullOrEmpty(productCode)){ productCode =
	 * productCode.trim(); } if(!StringUtil.isNullOrEmpty(productName)){
	 * productName = productName.trim(); } try{ DisplayProgramMgr
	 * displayProgramMgr =
	 * (DisplayProgramMgr)context.getBean("displayProgramMgr");
	 * if(displayProgramMgr!= null){ if(!StringUtil.isNullOrEmpty(productCode)
	 * || !StringUtil.isNullOrEmpty(productName)) { boolean isFirst = true;
	 * List<ProductCatVO> focusProgramDetailVo =
	 * displayProgramMgr.getListProductCatVOEx(productCode, productName,
	 * null,null); for(int i = 0; i < focusProgramDetailVo.size(); i++) {
	 * JsTreeNode pNode = new JsTreeNode(); ProductCatVO catVo =
	 * focusProgramDetailVo.get(i); if(isFirst) { if(catVo.getProductInfo() !=
	 * null) { pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - "
	 * + catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr
	 * = new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr); if(catVo.getLstProductSubCatVO()!= null &&
	 * catVo.getLstProductSubCatVO().size()>0){
	 * pNode.setState(ConstantManager.JSTREE_STATE_OPEN); } else {
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * if(catVo.getLstProductSubCatVO()!= null &&
	 * catVo.getLstProductSubCatVO().size()>0){ List<JsTreeNode> lstSubCat = new
	 * ArrayList<JsTreeNode>(); for (int j = 0; j <
	 * catVo.getLstProductSubCatVO().size(); j++) { JsTreeNode subCatNode = new
	 * JsTreeNode(); ProductSubCatVO subCatVO =
	 * catVo.getLstProductSubCatVO().get(j); if(isFirst) {
	 * if(subCatVO.getProductInfo()!= null){
	 * subCatNode.setData(subCatVO.getProductInfo().getProductInfoCode() + " - "
	 * + subCatVO.getProductInfo().getProductInfoName()); JsTreeNodeAttr
	 * scNodeAttr = new JsTreeNodeAttr();
	 * scNodeAttr.setId(String.valueOf(subCatVO.getProductInfo().getId()));
	 * scNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * scNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(scNodeAttr); if(subCatVO.getLstProduct()!= null &&
	 * subCatVO.getLstProduct().size()>0) {
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_OPEN); } else {
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * if(subCatVO.getLstProduct()!= null && subCatVO.getLstProduct().size()>0){
	 * List<JsTreeNode> lstProduct = new ArrayList<JsTreeNode>(); for (int k =
	 * 0; k < subCatVO.getLstProduct().size(); k++) { JsTreeNode productNode =
	 * new JsTreeNode(); Product product = subCatVO.getLstProduct().get(k);
	 * if(product!= null){ productNode.setData(product.getProductCode() + " - "
	 * + product.getProductName()); JsTreeNodeAttr pNodeAttr = new
	 * JsTreeNodeAttr(); pNodeAttr.setId(String.valueOf(product.getId()));
	 * pNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * pNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNode.setAttr(pNodeAttr);
	 * productNode.setState(ConstantManager.JSTREE_STATE_OPEN); }
	 * lstProduct.add(productNode); } subCatNode.setChildren(lstProduct);
	 * isFirst = false; } } else { if(subCatVO.getProductInfo()!= null){
	 * subCatNode.setData(subCatVO.getProductInfo().getProductInfoCode() + " - "
	 * + subCatVO.getProductInfo().getProductInfoName()); JsTreeNodeAttr
	 * scNodeAttr = new JsTreeNodeAttr();
	 * scNodeAttr.setId(String.valueOf(subCatVO.getProductInfo().getId()));
	 * scNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * scNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(scNodeAttr);
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * lstSubCat.add(subCatNode); } pNode.setChildren(lstSubCat); }
	 * 
	 * } else { if(catVo.getProductInfo() != null) {
	 * pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " +
	 * catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr =
	 * new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr);
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * lstCat.add(pNode); } } else {
	 * if(nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_CAT) &&
	 * catId == 0 && subCatId == 0) { List<ProductCatVO> focusProgramDetailVo =
	 * displayProgramMgr.getListProductCatVOEx(productCode, productName,
	 * null,null); for(int i = 0; i < focusProgramDetailVo.size(); i++) {
	 * JsTreeNode pNode = new JsTreeNode(); ProductCatVO catVo =
	 * focusProgramDetailVo.get(i); if(catVo.getProductInfo() != null) {
	 * pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " +
	 * catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr =
	 * new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr);
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } lstCat.add(pNode);
	 * } } else
	 * if(nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_SUBCAT) &&
	 * catId > 0 && subCatId == 0) { List<ProductInfo> listSubCatVo =
	 * displayProgramMgr.getListSubCat(productCode, productName, null,
	 * catId,null); for(int i = 0; i < listSubCatVo.size(); i++) { JsTreeNode
	 * subCatNode = new JsTreeNode(); ProductInfo subCat = listSubCatVo.get(i);
	 * subCatNode.setData(subCat.getProductInfoCode() + " - " +
	 * subCat.getProductInfoName()); JsTreeNodeAttr subCatNodeAttr = new
	 * JsTreeNodeAttr(); subCatNodeAttr.setId(String.valueOf(subCat.getId()));
	 * subCatNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(subCatNodeAttr);
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
	 * lstCat.add(subCatNode); } } else if
	 * (nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_PRODUCT) &&
	 * catId > 0 && subCatId > 0) { List<Product> listProduct =
	 * displayProgramMgr.getListProductBySubCat(productCode, productName, null,
	 * catId, subCatId,null); for(int i = 0; i < listProduct.size(); i++) {
	 * JsTreeNode productNode = new JsTreeNode(); Product product =
	 * listProduct.get(i); productNode.setData(product.getProductCode() + " - "
	 * + product.getProductName()); JsTreeNodeAttr productNodeAttr = new
	 * JsTreeNodeAttr(); productNodeAttr.setId(String.valueOf(product.getId()));
	 * productNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNodeAttr
	 * .setContentItemId(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNode.setAttr(productNodeAttr);
	 * productNode.setState(ConstantManager.JSTREE_STATE_OPEN);
	 * lstCat.add(productNode); } } } } }catch (Exception e) {
	 * LogUtility.logError(e,e.getMessage()); } return lstCat; }
	 */
	//Editor CuongND

	/*
	 * @RequestMapping(value = "/display-program/productDisplayGroup/list",
	 * method = RequestMethod.GET) public @ResponseBody List<JsTreeNode>
	 * getListProductOfDisplayDisplayGroup(@RequestParam("displayProgramId")
	 * long displayProgramId,
	 * 
	 * @RequestParam("productCode") String
	 * productCode,@RequestParam("productName") String productName,
	 * @RequestParam("nodeType") String nodeType, @RequestParam("catId") long
	 * catId, @RequestParam("subCatId") long subCatId){ List<JsTreeNode> lstCat
	 * = new ArrayList<JsTreeNode>();
	 * if(!StringUtil.isNullOrEmpty(productCode)){ productCode =
	 * productCode.trim(); } if(!StringUtil.isNullOrEmpty(productName)){
	 * productName = productName.trim(); } try{ DisplayProgramMgr
	 * displayProgramMgr =
	 * (DisplayProgramMgr)context.getBean("displayProgramMgr");
	 * if(displayProgramMgr!= null){ if(!StringUtil.isNullOrEmpty(productCode)
	 * || !StringUtil.isNullOrEmpty(productName)) { boolean isFirst = true;
	 * List<ProductCatVO> focusProgramDetailVo =
	 * displayProgramMgr.getListProductCatVOEx(productCode, productName,
	 * displayProgramId,true); for(int i = 0; i < focusProgramDetailVo.size();
	 * i++) { JsTreeNode pNode = new JsTreeNode(); ProductCatVO catVo =
	 * focusProgramDetailVo.get(i); if(isFirst) { if(catVo.getProductInfo() !=
	 * null) { pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - "
	 * + catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr
	 * = new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr); if(catVo.getLstProductSubCatVO()!= null &&
	 * catVo.getLstProductSubCatVO().size()>0){
	 * pNode.setState(ConstantManager.JSTREE_STATE_OPEN); } else {
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * if(catVo.getLstProductSubCatVO()!= null &&
	 * catVo.getLstProductSubCatVO().size()>0){ List<JsTreeNode> lstSubCat = new
	 * ArrayList<JsTreeNode>(); for (int j = 0; j <
	 * catVo.getLstProductSubCatVO().size(); j++) { JsTreeNode subCatNode = new
	 * JsTreeNode(); ProductSubCatVO subCatVO =
	 * catVo.getLstProductSubCatVO().get(j); if(isFirst) {
	 * if(subCatVO.getProductInfo()!= null){
	 * subCatNode.setData(subCatVO.getProductInfo().getProductInfoCode() + " - "
	 * + subCatVO.getProductInfo().getProductInfoName()); JsTreeNodeAttr
	 * scNodeAttr = new JsTreeNodeAttr();
	 * scNodeAttr.setId(String.valueOf(subCatVO.getProductInfo().getId()));
	 * scNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * scNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(scNodeAttr); if(subCatVO.getLstProduct()!= null &&
	 * subCatVO.getLstProduct().size()>0) {
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_OPEN); } else {
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * if(subCatVO.getLstProduct()!= null && subCatVO.getLstProduct().size()>0){
	 * List<JsTreeNode> lstProduct = new ArrayList<JsTreeNode>(); for (int k =
	 * 0; k < subCatVO.getLstProduct().size(); k++) { JsTreeNode productNode =
	 * new JsTreeNode(); Product product = subCatVO.getLstProduct().get(k);
	 * if(product!= null){ productNode.setData(product.getProductCode() + " - "
	 * + product.getProductName()); JsTreeNodeAttr pNodeAttr = new
	 * JsTreeNodeAttr(); pNodeAttr.setId(String.valueOf(product.getId()));
	 * pNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * pNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNode.setAttr(pNodeAttr);
	 * productNode.setState(ConstantManager.JSTREE_STATE_OPEN); }
	 * lstProduct.add(productNode); } subCatNode.setChildren(lstProduct);
	 * isFirst = false; } } else { if(subCatVO.getProductInfo()!= null){
	 * subCatNode.setData(subCatVO.getProductInfo().getProductInfoCode() + " - "
	 * + subCatVO.getProductInfo().getProductInfoName()); JsTreeNodeAttr
	 * scNodeAttr = new JsTreeNodeAttr();
	 * scNodeAttr.setId(String.valueOf(subCatVO.getProductInfo().getId()));
	 * scNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * scNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(scNodeAttr);
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * lstSubCat.add(subCatNode); } pNode.setChildren(lstSubCat); }
	 * 
	 * } else { if(catVo.getProductInfo() != null) {
	 * pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " +
	 * catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr =
	 * new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr);
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * lstCat.add(pNode); } } else {
	 * if(nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_CAT) &&
	 * catId == 0 && subCatId == 0) { List<ProductCatVO> focusProgramDetailVo =
	 * displayProgramMgr.getListProductCatVOEx(productCode, productName,
	 * displayProgramId,true); for(int i = 0; i < focusProgramDetailVo.size();
	 * i++) { JsTreeNode pNode = new JsTreeNode(); ProductCatVO catVo =
	 * focusProgramDetailVo.get(i); if(catVo.getProductInfo() != null) {
	 * pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " +
	 * catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr =
	 * new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr);
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } lstCat.add(pNode);
	 * } } else
	 * if(nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_SUBCAT) &&
	 * catId > 0 && subCatId == 0) { List<ProductInfo> listSubCatVo =
	 * displayProgramMgr.getListSubCat(productCode, productName,
	 * displayProgramId, catId,true); for(int i = 0; i < listSubCatVo.size();
	 * i++) { JsTreeNode subCatNode = new JsTreeNode(); ProductInfo subCat =
	 * listSubCatVo.get(i); subCatNode.setData(subCat.getProductInfoCode() +
	 * " - " + subCat.getProductInfoName()); JsTreeNodeAttr subCatNodeAttr = new
	 * JsTreeNodeAttr(); subCatNodeAttr.setId(String.valueOf(subCat.getId()));
	 * subCatNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(subCatNodeAttr);
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
	 * lstCat.add(subCatNode); } } else if
	 * (nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_PRODUCT) &&
	 * catId > 0 && subCatId > 0) { List<Product> listProduct =
	 * displayProgramMgr.getListProductBySubCat(productCode, productName,
	 * displayProgramId, catId, subCatId,true); for(int i = 0; i <
	 * listProduct.size(); i++) { JsTreeNode productNode = new JsTreeNode();
	 * Product product = listProduct.get(i);
	 * productNode.setData(product.getProductCode() + " - " +
	 * product.getProductName()); JsTreeNodeAttr productNodeAttr = new
	 * JsTreeNodeAttr(); productNodeAttr.setId(String.valueOf(product.getId()));
	 * productNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNodeAttr
	 * .setContentItemId(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNode.setAttr(productNodeAttr);
	 * productNode.setState(ConstantManager.JSTREE_STATE_OPEN);
	 * lstCat.add(productNode); } } } } }catch (Exception e) {
	 * LogUtility.logError(e,e.getMessage()); } return lstCat; }
	 */

	/**
	 * Gets the list shop of device.
	 * 
	 * @param code
	 *            : parentCode
	 * @param shopCode
	 *            the shop code
	 * @param shopName
	 *            the shop name
	 * @return the list shop of
	 * @author ThongNM
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/device/shop/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getListShopDevice(@RequestParam("code") String code, @RequestParam("shopCode") String shopCode, @RequestParam("shopName") String shopName) {
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			shopCode = shopCode.trim();
		}
		if (!StringUtil.isNullOrEmpty(shopName)) {
			shopName = shopName.trim();
		}
		List<JsTreeNode> lstUnitTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				if (!StringUtil.isNullOrEmpty(code)) {
					/*
					 * comment code loi compile
					 * @modified by tuannd20
					 * @date 20/12/2014
					 */
					/*Shop shop = shopMgr.getShopByCode(code);
					TreeVO<Shop> shopTreeVO = shopMgr.getShopTreeVOOneLevel(shop.getId(), null, shopCode, shopName, null, null, null);
					if (shopTreeVO.getObject() == null && shopTreeVO.getListChildren() != null) {
						for (int i = 0; i < shopTreeVO.getListChildren().size(); i++) {
							JsTreeNode tmp = new JsTreeNode();
							tmp = getUnitTreeBeanWithChild(tmp, shopTreeVO.getListChildren().get(i));
							lstUnitTree.add(tmp);
						}
					} else {
						bean = getUnitTreeBeanWithChild(bean, shopTreeVO);
						lstUnitTree.add(bean);
					}*/
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstUnitTree;
	}

	/**
	 * Gets the list sub shop device
	 * 
	 * @param parentId
	 *            the parent id
	 * @return the list sub shop promotion
	 * @author thongnm
	 * @since Oct 30, 2012
	 */
	@RequestMapping(value = "/device/sub-shop/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getListSubShopDevice(@RequestParam("id") Long parentId) {
		List<JsTreeNode> lstShop = new ArrayList<JsTreeNode>();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				List<Shop> lstSubShop = shopMgr.getListSubShopEx(parentId, ActiveType.RUNNING, null);
				if (lstSubShop != null && lstSubShop.size() > 0) {
					for (int i = 0; i < lstSubShop.size(); i++) {
						JsTreeNode node = new JsTreeNode();
						node.setData(lstSubShop.get(i).getShopCode() + " - " + lstSubShop.get(i).getShopName());
						JsTreeNodeAttr cNodeAttr = new JsTreeNodeAttr();
						cNodeAttr.setId(String.valueOf(lstSubShop.get(i).getId()));
						node.setAttr(cNodeAttr);
						node.setState(ConstantManager.JSTREE_STATE_CLOSE);
						lstShop.add(node);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShop;
	}

	/**
	 * Gets the list shop of promotion program.
	 * 
	 * @param promotionId
	 *            the promotion id
	 * @param code
	 *            the code
	 * @param shopCode
	 *            the shop code
	 * @param shopName
	 *            the shop name
	 * @return the list shop of promotion program
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/promotion-program/shop/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getListShopOfPromotionProgram(@RequestParam("promotionId") long promotionId, @RequestParam("code") String code, @RequestParam("shopCode") String shopCode, @RequestParam("shopName") String shopName) {
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			shopCode = shopCode.trim();
		}
		if (!StringUtil.isNullOrEmpty(shopName)) {
			shopName = shopName.trim();
		}
		List<JsTreeNode> lstUnitTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			PromotionProgramMgr promotionProgramMgr = (PromotionProgramMgr) context.getBean("promotionProgramMgr");
			if (shopMgr != null) {
				if (promotionProgramMgr != null) {
					ObjectVO<PromotionShopMap> lstPSMap = promotionProgramMgr.getListPromotionShopMap(null, promotionId, null, null, null, null, null);
					List<Long> lstShopId = new ArrayList<Long>();
					if (lstPSMap != null && lstPSMap.getLstObject() != null) {
						for (int i = 0; i < lstPSMap.getLstObject().size(); i++) {
							if (lstPSMap.getLstObject().get(i).getShop() != null) {
								//								List<Shop> lstTempShop = shopMgr.getListSubShop(lstPSMap.getLstObject().get(i).getShop().getId(), ActiveType.RUNNING);
								//								boolean flag = true;
								//								if(lstTempShop != null && lstTempShop.size() == 0){
								//									flag = false;
								//								}
								//								Integer num = 0;
								//								for(Shop subShop:lstTempShop){
								//									PromotionShopMap psMap = promotionProgramMgr.getPromotionShopMap(subShop.getId(), promotionId);
								//									if(psMap != null){
								//										num++;
								//									}
								//								}
								//								if(num.equals(lstTempShop.size())){
								//									flag = false;
								//								}
								//								if(!flag){
								//									lstShopId.add(lstPSMap.getLstObject().get(i).getShop().getId());
								//								}
								lstShopId.add(lstPSMap.getLstObject().get(i).getShop().getId());
							}
						}
					}
					if (!StringUtil.isNullOrEmpty(code)) {
						Shop shop = shopMgr.getShopByCode(code);
						if (shop != null) {
							/*
							 * comment code loi compile
							 * @modified by tuannd20
							 * @date 20/12/2014
							 */
							/*TreeVO<Shop> shopTreeVO = shopMgr.getShopTreeVOOneLevel(shop.getId(), lstShopId, shopCode, shopName, promotionId, ProgramObjectType.PROMOTION_PROGRAM, null);
							if (shopTreeVO.getObject() == null && shopTreeVO.getListChildren() != null) {
								for (int i = 0; i < shopTreeVO.getListChildren().size(); i++) {
									JsTreeNode tmp = new JsTreeNode();
									tmp = getUnitTreeBeanWithChild(tmp, shopTreeVO.getListChildren().get(i));
									lstUnitTree.add(tmp);
								}
							} else {
								bean = getUnitTreeBeanWithChild(bean, shopTreeVO);
								lstUnitTree.add(bean);
							}*/
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstUnitTree;
	}

	/**
	 * Gets the list sub shop promotion.
	 * 
	 * @author tungmt
	 * @since Oct 30, 2012
	 */
	@RequestMapping(value = "/incentive/sub-shop/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getListSubShopOfIncentive(@RequestParam("id") Long parentId, @RequestParam("incentiveId") Long incentiveId) {
		List<JsTreeNode> lstShop = new ArrayList<JsTreeNode>();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				/*
				 * comment code loi compile
				 * @modified by tuannd20
				 * @date 20/12/2014
				 */
				/*List<Shop> lstSubShop = shopMgr.getListSubShopForIncentiveProgram(parentId, ActiveType.RUNNING, incentiveId);
				if (lstSubShop != null && lstSubShop.size() > 0) {
					for (int i = 0; i < lstSubShop.size(); i++) {
						JsTreeNode node = new JsTreeNode();
						node.setData(lstSubShop.get(i).getShopCode() + " - " + lstSubShop.get(i).getShopName());
						JsTreeNodeAttr cNodeAttr = new JsTreeNodeAttr();
						cNodeAttr.setId(String.valueOf(lstSubShop.get(i).getId()));
						node.setAttr(cNodeAttr);
						List<Shop> lstSubSubShop = shopMgr.getListSubShopForIncentiveProgram(lstSubShop.get(i).getId(), ActiveType.RUNNING, incentiveId);
						if (lstSubSubShop != null && lstSubSubShop.size() > 0)
							node.setState(ConstantManager.JSTREE_STATE_CLOSE);
						lstShop.add(node);
					}
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShop;
	}

	/**
	 * Gets the list cc result.
	 * 
	 * @param id
	 *            the id
	 * @return the list cc result
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws BusinessException
	 *             the business exception
	 * @author ThongNM
	 */

	@RequestMapping(value = "/getCCResult/{productId}/{cycleCountMapProductId}/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CycleCountResult> getListCCResult(@PathVariable("productId") Long productId, @PathVariable("cycleCountMapProductId") Long cycleCountMapProductId) throws NumberFormatException, BusinessException {
		List<CycleCountResult> lst = new ArrayList<CycleCountResult>();
		try {
			CycleCountMgr cycleCountMgr = (CycleCountMgr) context.getBean("cycleCountMgr");
			if (cycleCountMgr == null) {
				return lst;
			}
			Long ownerId = null;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP_ID) != null) {
				ownerId = Long.valueOf(String.valueOf(session.getAttribute(ConstantManager.SESSION_SHOP_ID)));
			}
			if (ownerId == null) {
				return lst;
			}
			if (cycleCountMapProductId.equals(-1L)) {
				lst = cycleCountMgr.getListCycleCountResultEx(null, null, ownerId, StockObjectType.SHOP, productId);
			} else {
				ObjectVO<CycleCountResult> objectVO = cycleCountMgr.getListCycleCountResult(null, new Long(cycleCountMapProductId));
				if (objectVO != null) {
					lst = objectVO.getLstObject();
				}
			}
			if (lst == null) {
				return new ArrayList<CycleCountResult>();
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
			return new ArrayList<CycleCountResult>();
		}
		return lst;
	}

	/**
	 * Gets the list cc result.
	 * 
	 * @param id
	 *            the id
	 * @return the list cc result
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws BusinessException
	 *             the business exception
	 * @author tientv11
	 */

	@RequestMapping(value = "/list-product-lot/{productId}/{cycleCountMapProductId}/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CycleCountResult> getListLotOfProduct(@PathVariable("productId") Long productId, @PathVariable("cycleCountMapProductId") Long cycleCountMapProductId) throws NumberFormatException, BusinessException {
		List<CycleCountResult> lst = new ArrayList<CycleCountResult>();
		try {
			CycleCountMgr cycleCountMgr = (CycleCountMgr) context.getBean("cycleCountMgr");
			if (cycleCountMgr == null) {
				return lst;
			}
			Long ownerId = null;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP_ID) != null) {
				ownerId = Long.valueOf(String.valueOf(session.getAttribute(ConstantManager.SESSION_SHOP_ID)));
			}
			if (ownerId == null) {
				return lst;
			}
			if (cycleCountMapProductId.equals(-1L)) {
				cycleCountMapProductId = null;
			}
			lst = cycleCountMgr.getListCycleCountResultEx(null, cycleCountMapProductId, ownerId, StockObjectType.SHOP, productId);
			if (lst == null) {
				return new ArrayList<CycleCountResult>();
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
			return new ArrayList<CycleCountResult>();
		}
		return lst;
	}

	/**
	 * Gets the staff tree of display program.
	 * 
	 * @param displayProgramId
	 *            the display program id
	 * @param staffCode
	 *            the staff code
	 * @param staffName
	 *            the staff name
	 * @param id
	 *            the id
	 * @return the staff tree of display program
	 * @author hungtx
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/display-program/staff/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getStaffTreeOfDisplayProgram(@RequestParam("displayProgramId") long displayProgramId, @RequestParam("staffCode") String staffCode, @RequestParam("staffName") String staffName, long id) {
		List<JsTreeNode> lstStaffTree = new ArrayList<JsTreeNode>();
		try {
			StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
			Long parentShopId = null;
			if (id == 0) {
				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
				HttpSession session = attr.getRequest().getSession(true);
				if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP_ID) != null) {
					parentShopId = Long.valueOf(String.valueOf(session.getAttribute(ConstantManager.SESSION_SHOP_ID)));
				}
			} else {
				parentShopId = id;
			}
			if (staffMgr != null) {
				//		    	TreeExVO<Shop, Staff> staffVO =  staffMgr.getTreeStaffNotInDisplayProgramEx(parentShopId, displayProgramId, staffCode, staffName, null);
				if (id == 0) {
					/*
					 * comment code loi compile
					 * @modified by tuannd20
					 * @date 20/12/2014
					 */
					/*TreeExVO<Shop, Staff> staffVO = staffMgr.getTreeStaffNotInDisplayProgramEx(parentShopId, displayProgramId, staffCode, staffName, null);
					if (staffVO == null) {
						return lstStaffTree;
					}
					if (staffVO.getObject() == null && staffVO.getListChildren() != null) {
						for (int i = 0; i < staffVO.getListChildren().size(); i++) {
							JsTreeNode tmp = new JsTreeNode();
							tmp = getJsTreeExNode(tmp, staffVO.getListChildren().get(i));
							lstStaffTree.add(tmp);
						}
					} else {
						JsTreeNode bean = new JsTreeNode();
						bean = getJsTreeExNode(bean, staffVO);
						lstStaffTree.add(bean);
					}*/
				} else {
					/*
					 * comment code loi compile
					 * @modified by tuannd20
					 * @date 20/12/2014
					 */
					/*TreeExVO<Shop, Staff> staffVO = staffMgr.getChildTreeStaffNotInDisplayProgram(parentShopId, displayProgramId, staffCode, staffName, null);
					if (staffVO == null) {
						return lstStaffTree;
					}
					if (staffVO.getListChildren() != null && staffVO.getListLeaves() != null) {
						for (int i = 0; i < staffVO.getListChildren().size(); i++) {
							JsTreeNode tmp = new JsTreeNode();
							tmp = getJsTreeExNode(tmp, staffVO.getListChildren().get(i));
							lstStaffTree.add(tmp);
						}
						for (int i = 0; i < staffVO.getListLeaves().size(); i++) {
							Staff leaf = staffVO.getListLeaves().get(i);
							JsTreeNode leafNode = new JsTreeNode();
							if (leaf.getStaffCode() == null && leaf.getStaffName() != null) {
								leafNode.setData(leaf.getStaffName());
							} else if (leaf.getStaffCode() != null && leaf.getStaffName() == null) {

							} else if (leaf.getStaffCode() != null && leaf.getStaffName() != null) {
								leafNode.setData(leaf.getStaffCode() + " - " + leaf.getStaffName());
							}
							JsTreeNodeAttr leafNodeAttr = new JsTreeNodeAttr();
							leafNodeAttr.setId(leaf.getId().toString());
							leafNodeAttr.setContentItemId(String.valueOf(ChannelTypeType.STAFF.getValue()));
							leafNode.setAttr(leafNodeAttr);
							leafNode.setState(ConstantManager.JSTREE_STATE_OPEN);
							lstStaffTree.add(leafNode);
						}
					}*/
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstStaffTree;
	}

	/**
	 * Gets the js tree ex node.
	 * 
	 * @param node
	 *            the node
	 * @param staffNode
	 *            the staff node
	 * @return the js tree ex node
	 * @author hungtx
	 * @since Oct 1, 2012
	 */
	private JsTreeNode getJsTreeExNode(JsTreeNode node, TreeExVO<Shop, Staff> staffNode) {
		if (staffNode != null && staffNode.getObject() != null) {
			if (staffNode.getObject().getShopCode() == null && staffNode.getObject().getShopName() != null) {
				node.setData(staffNode.getObject().getShopName());
			} else if (staffNode.getObject().getShopCode() != null && staffNode.getObject().getShopName() == null) {
				node.setData(staffNode.getObject().getShopCode());
			} else if (staffNode.getObject().getShopCode() != null && staffNode.getObject().getShopName() != null) {
				node.setData(staffNode.getObject().getShopCode() + " - " + staffNode.getObject().getShopName());
			}

			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(staffNode.getObject().getId().toString());
			nodeAttr.setContentItemId(String.valueOf(ChannelTypeType.SHOP.getValue()));
			node.setAttr(nodeAttr);
			List<JsTreeNode> lstChild = new ArrayList<JsTreeNode>();
			if (staffNode.getListLeaves() != null && staffNode.getListLeaves().size() > 0) {
				for (int j = 0; j < staffNode.getListLeaves().size(); j++) {
					JsTreeNode leafNode = new JsTreeNode();
					leafNode.setData(staffNode.getListLeaves().get(j).getStaffCode() + " - " + staffNode.getListLeaves().get(j).getStaffName());
					JsTreeNodeAttr leafNodeAttr = new JsTreeNodeAttr();
					leafNodeAttr.setId(staffNode.getListLeaves().get(j).getId().toString());
					leafNodeAttr.setContentItemId(String.valueOf(ChannelTypeType.STAFF.getValue()));
					leafNode.setAttr(leafNodeAttr);
					leafNode.setState(ConstantManager.JSTREE_STATE_OPEN);
					lstChild.add(leafNode);
				}
			}
			if (staffNode.getListChildren() != null && staffNode.getListChildren().size() > 0) {
				for (int i = 0; i < staffNode.getListChildren().size(); i++) {
					JsTreeNode subBean = new JsTreeNode();
					subBean = getJsTreeExNode(subBean, staffNode.getListChildren().get(i));
					lstChild.add(subBean);
				}
			}
			node.setChildren(lstChild);
			if (lstChild.size() > 0) {
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
			} else {
				node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			}
		}
		return node;
	}

	/**
	 * Gets the sale type combo tree.
	 * 
	 * @param type
	 *            the type
	 * @param currentNode
	 *            the current node
	 * @return the sale type combo tree
	 * @author lamnh
	 * @since Oct 2, 2012
	 */
	@RequestMapping(value = "/sale-type/combotree/{type}/{currentNode}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getSaleTypeComboTree(@PathVariable("type") int type, @PathVariable("currentNode") Long currentNode) {
		List<ComboTreeBean> lstStaffTypeTree = new ArrayList<ComboTreeBean>();
		ComboTreeBean nullBean = new ComboTreeBean();
		nullBean.setId("-1");
		nullBean.setState("open");
		nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.tree.choose.parent"));
		lstStaffTypeTree.add(nullBean);
		try {
			ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
			if (channelTypeMgr != null) {
				TreeVO<ChannelType> channelTypeTreeVO = null;
				if (type == 1) {
					channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVOEx(null, ChannelTypeType.SALE_MAN, currentNode);
				} else {
					channelTypeTreeVO = channelTypeMgr.getChannelTypeTreeVO(null, ChannelTypeType.SALE_MAN, true);
				}
				if (channelTypeTreeVO.getObject() == null && channelTypeTreeVO.getListChildren() != null) {
					for (int i = 0; i < channelTypeTreeVO.getListChildren().size(); i++) {
						ComboTreeBean tmp = new ComboTreeBean();
						tmp = loadComboTreeForChannelType(tmp, channelTypeTreeVO.getListChildren().get(i));
						lstStaffTypeTree.add(tmp);
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					bean = loadComboTreeForChannelType(bean, channelTypeTreeVO);
					lstStaffTypeTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstStaffTypeTree;
	}

	/**
	 * Gets the customer tree of display program.
	 * 
	 * @param staffMapId
	 *            the staff map id
	 * @param customerCode
	 *            the customer code
	 * @param customerName
	 *            the customer name
	 * @param shopCode
	 *            the shop code
	 * @return the customer tree of display program
	 * @author hungtx
	 * @since Oct 2, 2012
	 */
	@RequestMapping(value = "/display-program/customer/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getCustomerTreeOfDisplayProgram(@RequestParam("staffMapId") Long staffMapId, @RequestParam("customerCode") String customerCode, @RequestParam("customerName") String customerName, @RequestParam("shopCode") String shopCode) {
		List<JsTreeNode> lstStaffTree = new ArrayList<JsTreeNode>();
		try {
			CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (customerMgr != null && shopMgr != null) {
				if (staffMapId == null) {
					return lstStaffTree;
				}
				if (StringUtil.isNullOrEmpty(shopCode)) {
					return lstStaffTree;
				}
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop == null) {
					return lstStaffTree;
				}
				KPaging<Customer> kPaging = new KPaging<Customer>();
				kPaging.setPageSize(Configuration.getMaxNodeOnTree());
				kPaging.setPage(0);
				ObjectVO<Customer> objectVO = customerMgr.getListCustomerEx(kPaging, customerCode, customerName, shopCode, shop.getId(), ActiveType.RUNNING);
				if (objectVO == null) {
					return lstStaffTree;
				}
				TreeExVO<Shop, Customer> customerVO = new TreeExVO<Shop, Customer>(); // customerMgr.getTreeCustomer(parentShopId, customerCode, customerName, Configuration.getMaxNodeOnTree());/*getTreeCustomerNotInDisplayProgram(parentShopId, staffMapId, customerCode, customerName,Configuration.getMaxNodeOnTree());*/
				customerVO.setObject(shop);
				customerVO.setListLeaves(objectVO.getLstObject());
				if (customerVO.getObject() == null && customerVO.getListChildren() != null) {
					for (int i = 0; i < customerVO.getListChildren().size(); i++) {
						JsTreeNode tmp = new JsTreeNode();
						tmp = getJsTreeExCustomerNode(tmp, customerVO.getListChildren().get(i));
						lstStaffTree.add(tmp);
					}
				} else {
					JsTreeNode bean = new JsTreeNode();
					bean = getJsTreeExCustomerNode(bean, customerVO);
					lstStaffTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstStaffTree;
	}

	/**
	 * Gets the js tree ex customer node.
	 * 
	 * @param node
	 *            the node
	 * @param customerNode
	 *            the customer node
	 * @return the js tree ex customer node
	 * @author hungtx
	 * @since Oct 2, 2012
	 */
	private JsTreeNode getJsTreeExCustomerNode(JsTreeNode node, TreeExVO<Shop, Customer> customerNode) {
		if (customerNode != null && customerNode.getObject() != null) {
			if (customerNode.getObject().getShopCode() == null && customerNode.getObject().getShopName() != null) {
				node.setData(customerNode.getObject().getShopName());
			} else if (customerNode.getObject().getShopCode() != null && customerNode.getObject().getShopName() == null) {
				node.setData(customerNode.getObject().getShopCode());
			} else if (customerNode.getObject().getShopCode() != null && customerNode.getObject().getShopName() != null) {
				node.setData(customerNode.getObject().getShopCode() + " - " + customerNode.getObject().getShopName());
			}
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(customerNode.getObject().getId().toString());
			nodeAttr.setContentItemId(String.valueOf(ChannelTypeType.SHOP.getValue()));
			node.setAttr(nodeAttr);
			List<JsTreeNode> lstChild = new ArrayList<JsTreeNode>();
			if (customerNode.getListChildren() != null && customerNode.getListChildren().size() > 0) {
				for (int i = 0; i < customerNode.getListChildren().size(); i++) {
					JsTreeNode subBean = new JsTreeNode();
					subBean = getJsTreeExCustomerNode(subBean, customerNode.getListChildren().get(i));
					lstChild.add(subBean);
				}
			}
			if (customerNode.getListLeaves() != null && customerNode.getListLeaves().size() > 0) {
				for (int j = 0; j < customerNode.getListLeaves().size(); j++) {
					JsTreeNode leafNode = new JsTreeNode();
					leafNode.setData(customerNode.getListLeaves().get(j).getShortCode() + " - " + customerNode.getListLeaves().get(j).getCustomerName());
					JsTreeNodeAttr leafNodeAttr = new JsTreeNodeAttr();
					leafNodeAttr.setId(customerNode.getListLeaves().get(j).getId().toString());
					leafNodeAttr.setContentItemId(String.valueOf(ChannelTypeType.CUSTOMER.getValue()));
					leafNode.setAttr(leafNodeAttr);
					lstChild.add(leafNode);
				}
			}
			node.setChildren(lstChild);
			node.setState(ConstantManager.JSTREE_STATE_OPEN);
		}
		return node;
	}

	/**
	 * Gets the shop tree of focus program.
	 * 
	 * @param focusProgramId
	 *            the focus program id
	 * @param shopCode
	 *            the shop code
	 * @param shopName
	 *            the shop name
	 * @return the shop tree of focus program
	 * @author hungtx
	 * @since Oct 2, 2012
	 */
	@RequestMapping(value = "/focus-program/shop/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getShopTreeOfFocusProgram(@RequestParam("focusProgramId") long focusProgramId, @RequestParam("shopCode") String shopCode, @RequestParam("shopName") String shopName) {
		if (!StringUtil.isNullOrEmpty(shopCode)) {
			shopCode = shopCode.trim();
		}
		if (!StringUtil.isNullOrEmpty(shopName)) {
			shopName = shopName.trim();
		}
		List<JsTreeNode> lstShopTree = new ArrayList<JsTreeNode>();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			FocusProgramMgr focusProgramMgr = (FocusProgramMgr) context.getBean("focusProgramMgr");
			Long parentShopId = null;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP_ID) != null) {
				parentShopId = Long.valueOf(String.valueOf(session.getAttribute(ConstantManager.SESSION_SHOP_ID)));
			}

			if (shopMgr != null && focusProgramMgr != null) {
				ObjectVO<FocusShopMap> lstFSMap = focusProgramMgr.getListFocusShopMap(null, focusProgramId, null, null, null, null);
				List<Long> lstShopId = new ArrayList<Long>();
				if (lstFSMap != null && lstFSMap.getLstObject() != null) {
					for (int i = 0; i < lstFSMap.getLstObject().size(); i++) {
						if (lstFSMap.getLstObject().get(i).getShop() != null) {
							/*
							 * List<Shop> lstTempShop =
							 * shopMgr.getListSubShop(lstFSMap
							 * .getLstObject().get(i).getShop().getId(),
							 * ActiveType.RUNNING); boolean flag = true;
							 * if(lstTempShop != null && lstTempShop.size() ==
							 * 0){ flag = false; } Integer num = 0; for(Shop
							 * subShop:lstTempShop){ FocusShopMap psMap =
							 * focusProgramMgr
							 * .getFocusShopMapById(focusProgramId); if(psMap !=
							 * null){ num++; } }
							 * if(num.equals(lstTempShop.size())){ flag = false;
							 * } if(!flag){
							 * lstShopId.add(lstFSMap.getLstObject()
							 * .get(i).getShop().getId()); }
							 */
							if (!lstFSMap.getLstObject().get(i).getStatus().getValue().equals(ActiveType.DELETED.getValue())) {
								lstShopId.add(lstFSMap.getLstObject().get(i).getShop().getId());
							}
						}
					}
				}

				/*
				 * comment code loi compile
				 * @modified by tuannd20
				 * @date 20/12/2014
				 */
				/*TreeVO<Shop> shopVO = shopMgr.getShopTreeVOOneLevel(parentShopId, lstShopId, shopCode, shopName, focusProgramId, ProgramObjectType.FOCUS_PROGRAM, null);
				if (shopVO.getObject() == null && shopVO.getListChildren() != null) {
					for (int i = 0; i < shopVO.getListChildren().size(); i++) {
						JsTreeNode tmp = new JsTreeNode();
						tmp = getUnitTreeBeanWithChildContaintName(tmp, shopVO.getListChildren().get(i));
						lstShopTree.add(tmp);
					}
				} else {
					JsTreeNode bean = new JsTreeNode();
					bean = getUnitTreeBeanWithChildContaintName(bean, shopVO);
					lstShopTree.add(bean);
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShopTree;
	}

	/**
	 * Gets the list product of focus program.
	 * 
	 * @param focusProgramId
	 *            the focus program id
	 * @param productCode
	 *            the product code
	 * @param productName
	 *            the product name
	 * @param nodeType
	 *            nodeType: cat | sub-cat | product
	 * @param catId
	 *            the cat id
	 * @param subCatId
	 *            the sub cat id
	 * @return the list product of focus program
	 * @author hungtx
	 * @since Oct 3, 2012
	 */
	@RequestMapping(value = "/focus-program/product/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getListProductOfFocusProgram(@RequestParam("focusProgramId") long focusProgramId, @RequestParam("productCode") String productCode, @RequestParam("productName") String productName, @RequestParam("nodeType") String nodeType,
			@RequestParam("catId") long catId, @RequestParam("subCatId") long subCatId) {
		List<JsTreeNode> lstCat = new ArrayList<JsTreeNode>();
		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = productCode.trim();
		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			productName = productName.trim();
		}
		try {
			FocusProgramMgr focusProgramMgr = (FocusProgramMgr) context.getBean("focusProgramMgr");
			if (focusProgramMgr != null) {
				if (!StringUtil.isNullOrEmpty(productCode) || !StringUtil.isNullOrEmpty(productName)) {
					boolean isFirst = true;
					List<ProductCatVO> focusProgramDetailVo = focusProgramMgr.getListProductCatVOEx(productCode, productName, focusProgramId);
					for (int i = 0; i < focusProgramDetailVo.size(); i++) {
						JsTreeNode pNode = new JsTreeNode();
						ProductCatVO catVo = focusProgramDetailVo.get(i);
						if (isFirst) {
							if (catVo.getProductInfo() != null) {
								pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " + catVo.getProductInfo().getProductInfoName());
								JsTreeNodeAttr cNodeAttr = new JsTreeNodeAttr();
								cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
								cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
								cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
								pNode.setAttr(cNodeAttr);
								if (catVo.getLstProductSubCatVO() != null && catVo.getLstProductSubCatVO().size() > 0) {
									pNode.setState(ConstantManager.JSTREE_STATE_OPEN);
								} else {
									pNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
								}
							}

							if (catVo.getLstProductSubCatVO() != null && catVo.getLstProductSubCatVO().size() > 0) {
								List<JsTreeNode> lstSubCat = new ArrayList<JsTreeNode>();
								for (int j = 0; j < catVo.getLstProductSubCatVO().size(); j++) {
									JsTreeNode subCatNode = new JsTreeNode();
									ProductSubCatVO subCatVO = catVo.getLstProductSubCatVO().get(j);
									if (isFirst) {
										if (subCatVO.getProductInfo() != null) {
											subCatNode.setData(subCatVO.getProductInfo().getProductInfoCode() + " - " + subCatVO.getProductInfo().getProductInfoName());
											JsTreeNodeAttr scNodeAttr = new JsTreeNodeAttr();
											scNodeAttr.setId(String.valueOf(subCatVO.getProductInfo().getId()));
											scNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
											scNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
											subCatNode.setAttr(scNodeAttr);
											if (subCatVO.getLstProduct() != null && subCatVO.getLstProduct().size() > 0) {
												subCatNode.setState(ConstantManager.JSTREE_STATE_OPEN);
											} else {
												subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
											}
										}
										if (subCatVO.getLstProduct() != null && subCatVO.getLstProduct().size() > 0) {
											List<JsTreeNode> lstProduct = new ArrayList<JsTreeNode>();
											for (int k = 0; k < subCatVO.getLstProduct().size(); k++) {
												JsTreeNode productNode = new JsTreeNode();
												Product product = subCatVO.getLstProduct().get(k);
												if (product != null) {
													productNode.setData(product.getProductCode() + " - " + product.getProductName());
													JsTreeNodeAttr pNodeAttr = new JsTreeNodeAttr();
													pNodeAttr.setId(String.valueOf(product.getId()));
													pNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
													pNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
													productNode.setAttr(pNodeAttr);
													productNode.setState(ConstantManager.JSTREE_STATE_OPEN);
												}
												lstProduct.add(productNode);
											}
											subCatNode.setChildren(lstProduct);
											isFirst = false;
										}
									} else {
										if (subCatVO.getProductInfo() != null) {
											subCatNode.setData(subCatVO.getProductInfo().getProductInfoCode() + " - " + subCatVO.getProductInfo().getProductInfoName());
											JsTreeNodeAttr scNodeAttr = new JsTreeNodeAttr();
											scNodeAttr.setId(String.valueOf(subCatVO.getProductInfo().getId()));
											scNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
											scNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
											subCatNode.setAttr(scNodeAttr);
											subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
										}
									}

									lstSubCat.add(subCatNode);
								}
								pNode.setChildren(lstSubCat);
							}

						} else {
							if (catVo.getProductInfo() != null) {
								pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " + catVo.getProductInfo().getProductInfoName());
								JsTreeNodeAttr cNodeAttr = new JsTreeNodeAttr();
								cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
								cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
								cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
								pNode.setAttr(cNodeAttr);
								pNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
							}
						}

						lstCat.add(pNode);
					}
				} else {
					if (nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_CAT) && catId == 0 && subCatId == 0) {
						List<ProductCatVO> focusProgramDetailVo = focusProgramMgr.getListProductCatVOEx(productCode, productName, focusProgramId);
						for (int i = 0; i < focusProgramDetailVo.size(); i++) {
							JsTreeNode pNode = new JsTreeNode();
							ProductCatVO catVo = focusProgramDetailVo.get(i);
							if (catVo.getProductInfo() != null) {
								pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " + catVo.getProductInfo().getProductInfoName());
								JsTreeNodeAttr cNodeAttr = new JsTreeNodeAttr();
								cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
								cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
								cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
								pNode.setAttr(cNodeAttr);
								pNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
							}
							lstCat.add(pNode);
						}
					} else if (nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_SUBCAT) && catId > 0 && subCatId == 0) {
						List<ProductInfo> listSubCatVo = focusProgramMgr.getListSubCat(productCode, productName, focusProgramId, catId);
						for (int i = 0; i < listSubCatVo.size(); i++) {
							JsTreeNode subCatNode = new JsTreeNode();
							ProductInfo subCat = listSubCatVo.get(i);
							subCatNode.setData(subCat.getProductInfoCode() + " - " + subCat.getProductInfoName());
							JsTreeNodeAttr subCatNodeAttr = new JsTreeNodeAttr();
							subCatNodeAttr.setId(String.valueOf(subCat.getId()));
							subCatNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
							subCatNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
							subCatNode.setAttr(subCatNodeAttr);
							subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
							lstCat.add(subCatNode);
						}
					} else if (nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_PRODUCT) && catId > 0 && subCatId > 0) {
						List<Product> listProduct = focusProgramMgr.getListProductBySubCat(productCode, productName, focusProgramId, catId, subCatId);
						for (int i = 0; i < listProduct.size(); i++) {
							JsTreeNode productNode = new JsTreeNode();
							Product product = listProduct.get(i);
							productNode.setData(product.getProductCode() + " - " + product.getProductName());
							JsTreeNodeAttr productNodeAttr = new JsTreeNodeAttr();
							productNodeAttr.setId(String.valueOf(product.getId()));
							productNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
							productNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
							productNode.setAttr(productNodeAttr);
							productNode.setState(ConstantManager.JSTREE_STATE_OPEN);
							lstCat.add(productNode);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstCat;
	}

	/**
	 * Gets the level of display program.
	 * 
	 * @param displayProgramId
	 *            the display program id
	 * @return the level of display program
	 * @author hungtx
	 * @since Oct 4, 2012
	 */
	//	@RequestMapping(value = "/display-program/{displayProgramId}/level/list", method = RequestMethod.GET)
	//	public @ResponseBody List<StatusBean> getLevelOfDisplayProgram(@PathVariable("displayProgramId") Long displayProgramId){
	//		List<StatusBean> lstStaffType = new ArrayList<StatusBean>();
	//		if(displayProgramId!= null && displayProgramId > 0){
	//			try{
	//			    DisplayProgramMgr displayProgramMgr = (DisplayProgramMgr)context.getBean("displayProgramMgr");		    
	//			    if(displayProgramMgr!= null){
	//			    	ObjectVO<DisplayProgramLevel> displayProgramLevelVO = displayProgramMgr.getListDisplayProgramLevel(null, displayProgramId,null,null,ActiveType.RUNNING);							
	//					if(displayProgramLevelVO!= null && displayProgramLevelVO.getLstObject()!= null && displayProgramLevelVO.getLstObject().size() > 0){
	//						for(int i=0;i<displayProgramLevelVO.getLstObject().size();i++){
	//							if(displayProgramLevelVO.getLstObject()!= null){
	//								StatusBean obj = new StatusBean();
	//								obj.setName(displayProgramLevelVO.getLstObject().get(i).getLevelCode());
	//								obj.setValue(displayProgramLevelVO.getLstObject().get(i).getId());
	//								lstStaffType.add(obj);
	//							}							
	//						}
	//					}
	//			    }
	//			}catch (Exception e) {
	//			    LogUtility.logError(e,e.getMessage());
	//			}
	//		}		
	//		return lstStaffType;		
	//	}

	/**
	 * Gets the list cashier in shop.
	 * 
	 * @param shopCode
	 *            the shop code
	 * @return the list cashier in shop
	 * @author hungtx
	 * @since Oct 8, 2012
	 */
	@RequestMapping(value = "/customer/shop/cashier/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getListCashierInShop(@RequestParam("shopCode") String shopCode, @RequestParam("customerCode") String customerCode) {
		List<StatusBean> lstStaff = new ArrayList<StatusBean>();
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		String parentShopCode = null;
		if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP) != null) {
			parentShopCode = ((Shop) session.getAttribute(ConstantManager.SESSION_SHOP)).getShopCode();
		}
		if (StringUtil.isNullOrEmpty(shopCode)) {
			shopCode = parentShopCode;
		}
		try {
			StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
			CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
			if (staffMgr != null) {
				List<Staff> lstCashier = new ArrayList<Staff>();
				/*
				 * comment code loi compile
				 * @modified by tuannd20
				 * @date 20/12/2014
				 */
				/*ObjectVO<Staff> staffVO = staffMgr.getListCashierByShop(null, shopCode, parentShopCode, ActiveType.RUNNING, null);
				if (staffVO != null) {
					lstCashier = staffVO.getLstObject();
				}*/
				if (!StringUtil.isNullOrEmpty(customerCode)) {
					Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, customerCode));
					if (customer != null) {
						Staff staff = customer.getCashierStaff();
						if (staff != null && staff.getStatus().getValue().equals(ActiveType.STOPPED.getValue())) {
							lstCashier.add(staff);
						}
					}
				}
				for (Staff obj : lstCashier) {
					StatusBean statusBean = new StatusBean();
					statusBean.setName(StringUtil.CodeAddName(obj.getStaffCode(), obj.getStaffName()));
					statusBean.setValue(obj.getId());
					lstStaff.add(statusBean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstStaff;
	}

	/**
	 * Gets the list reason group.
	 * 
	 * @param status
	 *            the status
	 * @return the list reason group
	 * @author hungtx
	 * @since Oct 24, 2012
	 * @RequestMapping(value = "/reason-group/list", method = RequestMethod.GET)
	 *                       public @ResponseBody List<StatusBean>
	 *                       getListReasonGroup(@RequestParam("status") int
	 *                       status){ List<StatusBean> lstReasonGroup = new
	 *                       ArrayList<StatusBean>(); try{ ReasonMgr reasonMgr =
	 *                       (ReasonMgr)context.getBean("reasonMgr");
	 *                       if(reasonMgr!= null){ ObjectVO<ReasonGroup>
	 *                       reasonGroupVO =
	 *                       reasonMgr.getListReasonGroup(null,null
	 *                       ,null,null,ActiveType.parseValue(status));
	 *                       if(reasonGroupVO!= null &&
	 *                       reasonGroupVO.getLstObject()!= null &&
	 *                       reasonGroupVO.getLstObject().size() > 0){ for(int
	 *                       i=0;i<reasonGroupVO.getLstObject().size();i++){
	 *                       if(reasonGroupVO.getLstObject()!= null){ StatusBean
	 *                       obj = new StatusBean(); String name =
	 *                       reasonGroupVO.
	 *                       getLstObject().get(i).getReasonGroupCode()+
	 *                       "-"+reasonGroupVO
	 *                       .getLstObject().get(i).getReasonGroupName();
	 *                       obj.setName(name);
	 *                       obj.setValue(reasonGroupVO.getLstObject
	 *                       ().get(i).getId()); lstReasonGroup.add(obj); } } }
	 *                       } }catch (Exception e) {
	 *                       LogUtility.logError(e,e.getMessage()); } return
	 *                       lstReasonGroup; }
	 */

	/**
	 * Gets the list product info.
	 * 
	 * @param type
	 *            the type
	 * @param status
	 *            the status
	 * @return the list product info
	 * @author hungtx
	 * @since Oct 25, 2012
	 */
	@RequestMapping(value = "/product-info/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getListProductInfo(@RequestParam("type") int type, @RequestParam("status") int status) {
		List<StatusBean> lstProductInfo = new ArrayList<StatusBean>();
		try {
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			if (productInfoMgr != null) {
				/*ObjectVO<ProductInfo> productInfoVO = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.parseValue(status), ProductType.parseValue(type), true);
				if (productInfoVO != null && productInfoVO.getLstObject() != null && productInfoVO.getLstObject().size() > 0) {
					for (int i = 0; i < productInfoVO.getLstObject().size(); i++) {
						if (productInfoVO.getLstObject() != null) {
							StatusBean obj = new StatusBean();
							obj.setName(productInfoVO.getLstObject().get(i).getProductInfoCode());
							obj.setValue(productInfoVO.getLstObject().get(i).getId());
							lstProductInfo.add(obj);
						}
					}
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstProductInfo;
	}

	/**
	 * Gets the list sub shop.
	 * 
	 * @param parentId
	 *            the parent id
	 * @param focusProgramId
	 *            the focus program id
	 * @return the list sub shop
	 * @author hungtx
	 * @since Oct 25, 2012
	 */
	@RequestMapping(value = "/sub-shop/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getListSubShop(@RequestParam("id") Long parentId, @RequestParam("focusProgramId") Long focusProgramId) {
		List<JsTreeNode> lstShop = new ArrayList<JsTreeNode>();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				List<Shop> lstSubShop = shopMgr.getListSubShopForFocusProgram(parentId, ActiveType.RUNNING, focusProgramId);
				if (lstSubShop != null && lstSubShop.size() > 0) {
					for (int i = 0; i < lstSubShop.size(); i++) {
						JsTreeNode node = new JsTreeNode();
						node.setData(lstSubShop.get(i).getShopCode() + " - " + lstSubShop.get(i).getShopName());
						JsTreeNodeAttr cNodeAttr = new JsTreeNodeAttr();
						cNodeAttr.setId(String.valueOf(lstSubShop.get(i).getId()));
						node.setAttr(cNodeAttr);
						node.setState(ConstantManager.JSTREE_STATE_CLOSE);
						lstShop.add(node);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShop;
	}

	/**
	 * Gets the area tree ex.
	 * 
	 * @param parentId
	 *            the parent id
	 * @return the area tree ex
	 * @author lamnh
	 * @since Oct 25, 2012
	 */
	@RequestMapping(value = "/area-tree-ex/{parentId}/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getAreaTreeEx(@PathVariable Long parentId) {
		List<JsTreeNode> lstAreaTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		JsTreeNode root = new JsTreeNode();
		JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();

		List<JsTreeNode> lstTree = new ArrayList<JsTreeNode>();
		try {
			AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
			if (areaMgr != null) {
				if (parentId != null && parentId == 0) {
					parentId = null;
					nodeAttr.setId("-1");
					nodeAttr.setContentItemId("");
					root.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area.common.tree.title"));
					root.setAttr(nodeAttr);
				}
				List<Area> lstArea = areaMgr.getListSubArea(parentId, ActiveType.RUNNING, false);
				if (lstArea != null) {
					int size = lstArea.size();
					for (int i = 0; i < size; i++) {
						JsTreeNode tmp = new JsTreeNode();
						tmp = getAreaTreeBeanEx(tmp, lstArea.get(i));
						if (lstArea.get(i) != null && areaMgr.checkIfAreaHasAllChildStopped(lstArea.get(i).getId())) {
							tmp.setState(ConstantManager.JSTREE_STATE_LEAF);
						}
						lstAreaTree.add(tmp);
					}
				} else {
					bean = getAreaTreeBeanEx(bean, null);
					lstAreaTree.add(bean);
				}
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		//		return lstAreaTree;
		if (parentId == null) {
			root.setChildren(lstAreaTree);
			root.setState(ConstantManager.JSTREE_STATE_OPEN);
			lstTree.add(root);
			return lstTree;
		}
		return lstAreaTree;
	}

	/**
	 * Gets the area tree bean ex.
	 * 
	 * @param bean
	 *            the bean
	 * @param area
	 *            the area
	 * @return the area tree bean ex
	 * @author lamnh
	 * @since Oct 25, 2012
	 */

	private JsTreeNode getAreaTreeBeanEx(JsTreeNode bean, Area area) {
		if (area != null) {
			bean.setData(area.getAreaName());
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(area.getId().toString());
			nodeAttr.setContentItemId(area.getAreaCode());
			bean.setAttr(nodeAttr);
			bean.setState(ConstantManager.JSTREE_STATE_CLOSE);
		}
		return bean;
	}

	/**
	 * Gets the unit tree bean ex.
	 * 
	 * @param bean
	 *            the bean
	 * @param shop
	 *            the shop
	 * @return the unit tree bean ex
	 * @author lamnh
	 * @since Oct 25, 2012
	 */
	private JsTreeNode getUnitTreeBeanEx(JsTreeNode bean, Shop shop) {
		if (shop != null) {
			bean.setData(shop.getShopName());
			JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
			nodeAttr.setId(shop.getId().toString());
			nodeAttr.setContentItemId(shop.getShopCode());
			bean.setAttr(nodeAttr);
			bean.setState(ConstantManager.JSTREE_STATE_CLOSE);
		}
		return bean;
	}

	/**
	 * Gets the unit tree ex.
	 * 
	 * @param parentId
	 *            the parent id
	 * @param first
	 *            the first
	 * @return the unit tree ex
	 * @author lamnh
	 * @since Oct 25, 2012
	 */
	@RequestMapping(value = "/unit-tree-ex/{parentId}/{first}/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getUnitTreeEx(@PathVariable Long parentId, @PathVariable Integer first) {
		List<JsTreeNode> lstShopTree = new ArrayList<JsTreeNode>();
		JsTreeNode bean = new JsTreeNode();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				if (first != null && first == 0) {
					Shop shop = shopMgr.getShopById(parentId);
					JsTreeNode tmp = new JsTreeNode();
					tmp = getUnitTreeBeanEx(tmp, shop);
					tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					if (parentId != null && parentId == 0) {
						parentId = null;
					}
					List<Shop> lstShop = shopMgr.getListSubShop(parentId, ActiveType.RUNNING);
					List<JsTreeNode> lstShopTree1 = new ArrayList<JsTreeNode>();
					if (lstShop != null) {
						int size = lstShop.size();
						for (int i = 0; i < size; i++) {
							JsTreeNode tmp1 = new JsTreeNode();
							tmp1 = getUnitTreeBeanEx(tmp1, lstShop.get(i));
							lstShopTree1.add(tmp1);
						}
					} else {
						bean = getUnitTreeBeanEx(bean, null);
						lstShopTree.add(bean);
					}
					tmp.setChildren(lstShopTree1);
					lstShopTree.add(tmp);
				} else {
					if (parentId != null && parentId == 0) {
						parentId = null;
					}
					List<Shop> lstShop = shopMgr.getListSubShop(parentId, ActiveType.RUNNING);
					if (lstShop != null) {
						int size = lstShop.size();
						for (int i = 0; i < size; i++) {
							JsTreeNode tmp = new JsTreeNode();
							tmp = getUnitTreeBeanEx(tmp, lstShop.get(i));
							lstShopTree.add(tmp);
						}
					} else {
						bean = getUnitTreeBeanEx(bean, null);
						lstShopTree.add(bean);
					}
				}
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShopTree;
	}

	/**
	 * Load combo tree for shop ex.
	 * 
	 * @param node
	 *            the node
	 * @param shop
	 *            the shop
	 * @return the combo tree bean
	 */
	private ComboTreeBean loadComboTreeForShopEx(ComboTreeBean node, Shop shop) {
		if (shop != null) {
			node.setId(String.valueOf(shop.getId().toString()));
			node.setText(shop.getShopCode() + "-" + shop.getShopName());
			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			node.setChildren(new ArrayList<ComboTreeBean>());
		}
		return node;
	}

	/**
	 * Gets the area tree combo tree.
	 * 
	 * @param shopCode
	 *            the shop code
	 * @param type
	 *            the type
	 * @param view
	 *            the view
	 * @param currentNode
	 *            the current node
	 * @param shopId
	 *            the shop id
	 * @return the area tree combo tree
	 * @author tientv
	 * @since
	 */
	@RequestMapping(value = "/shop-tree-ex/combotree/{shopCode}/{type}/{view}/{currentNode}/{shopId}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getUnitTreeComboTreeEx(@PathVariable("shopCode") String shopCode, @PathVariable("type") int type, @PathVariable("view") int view, @PathVariable("currentNode") Long currentNode, @PathVariable("shopId") Long shopId) {
		List<ComboTreeBean> lstShopTree = new ArrayList<ComboTreeBean>();
		ComboTreeBean nullBean = new ComboTreeBean();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			Shop currentShop = null;
			if (shopMgr != null) {
				List<Shop> shopTreeVO = null;
				if (currentNode != null && currentNode == 0) {
					currentNode = null;
					nullBean.setId("-1");
					nullBean.setState("open");
					nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.tree.choose.parent"));
					lstShopTree.add(nullBean);
				}
				Shop shop = null;
				StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
				if (staffMgr != null) {
					ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
					HttpSession session = attr.getRequest().getSession(true);
					UserToken currentUser = null;
					if (session.getAttribute("cmsUserToken") != null) {
						currentUser = (UserToken) session.getAttribute("cmsUserToken");
						if (currentUser != null) {
							Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
							if (staff != null) {
								shop = staff.getShop();
							}
						}
					}
				}
				currentShop = shopMgr.getShopByCode(shopCode);
				if (type == 0) {
					shopTreeVO = new ArrayList<Shop>();
					shopTreeVO.add(currentShop);
				} else {
					if (shopId != null && shopId == 0) {
						shopId = null;
					}
					shopTreeVO = shopMgr.getListSubShopEx(currentNode, ActiveType.RUNNING, shopId);
				}
				if (shopTreeVO != null) {
					if (view == 1) {
						ComboTreeBean node = new ComboTreeBean();
						Shop ccShop = shopMgr.getShopById(currentNode);
						node = loadComboTreeForShopEx(node, ccShop);
						List<ComboTreeBean> child = new ArrayList<ComboTreeBean>();
						for (int i = 0; i < shopTreeVO.size(); i++) {
							ComboTreeBean tmp = new ComboTreeBean();
							tmp = loadComboTreeForShopEx(tmp, shopTreeVO.get(i));
							child.add(tmp);
						}
						node.setChildren(child);
						lstShopTree.add(node);
					} else {
						for (int i = 0; i < shopTreeVO.size(); i++) {
							ComboTreeBean tmp = new ComboTreeBean();
							tmp = loadComboTreeForShopEx(tmp, shopTreeVO.get(i));
							lstShopTree.add(tmp);
						}
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					bean = loadComboTreeForAreaEx(bean, null);
					lstShopTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShopTree;
	}

	/**
	 * Gets the category of product.
	 * 
	 * @param status
	 *            the status
	 * @param categoryId
	 *            the category id
	 * @return the category of product
	 * @author lamnh
	 * @since Oct 29, 2012
	 */
	@RequestMapping(value = "/category-product/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getCategoryOfProduct(@RequestParam("status") Integer status, @RequestParam("categoryId") Long categoryId) {
		List<StatusBean> lstCategoryProduct = new ArrayList<StatusBean>();
		try {
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			if (productInfoMgr != null) {
				if (status != null && status == -2) {
					ProductInfo productInfo = productInfoMgr.getProductInfoById(categoryId);
					if (productInfo != null && ActiveType.STOPPED.equals(productInfo.getStatus())) {
						StatusBean obj = new StatusBean();
						obj.setName(productInfo.getProductInfoCode() + " - " + productInfo.getProductInfoName());
						obj.setValue(productInfo.getId());
						lstCategoryProduct.add(obj);
					}
				}
				/*ObjectVO<ProductInfo> categoryVO = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true);
				if (categoryVO != null && categoryVO.getLstObject() != null && categoryVO.getLstObject().size() > 0) {
					for (int i = 0; i < categoryVO.getLstObject().size(); i++) {
						if (categoryVO.getLstObject() != null) {
							StatusBean obj = new StatusBean();
							obj.setName(categoryVO.getLstObject().get(i).getProductInfoCode() + " - " + categoryVO.getLstObject().get(i).getProductInfoName());
							obj.setValue(categoryVO.getLstObject().get(i).getId());
							lstCategoryProduct.add(obj);
						}
					}
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstCategoryProduct;
	}

	/**
	 * Check province code.
	 * 
	 * @param provinceCode
	 *            the province code
	 * @return the status bean
	 * @author lamnh
	 * @since Oct 30, 2012
	 */
	@RequestMapping(value = "/province/code/{provinceCode}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean checkProvinceCode(@PathVariable("provinceCode") String provinceCode) {
		StatusBean province = new StatusBean(0L, "");
		if (provinceCode != null) {
			try {
				AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
				if (areaMgr != null) {
					Area area = areaMgr.getAreaByCode(provinceCode);
					if (area != null) {
						province.setValue(area.getId());
						province.setName(area.getAreaName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return province;
	}

	/**
	 * Check district code.
	 * 
	 * @param districtCode
	 *            the district code
	 * @return the status bean
	 * @author lamnh
	 * @since Oct 30, 2012
	 */
	@RequestMapping(value = "/district/code/{districtCode}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean checkDistrictCode(@PathVariable("districtCode") String districtCode) {
		StatusBean district = new StatusBean(0L, "");
		if (districtCode != null) {
			try {
				AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
				if (areaMgr != null) {
					Area area = areaMgr.getAreaByCode(districtCode);
					if (area != null) {
						district.setValue(area.getId());
						district.setName(area.getAreaName());
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return district;
	}

	/**
	 * Gets the list sub shop promotion.
	 * 
	 * @param parentId
	 *            the parent id
	 * @param promotionId
	 *            the promotion id
	 * @return the list sub shop promotion
	 * @author lamnh
	 * @since Oct 30, 2012
	 */
	@RequestMapping(value = "/promotion-program/sub-shop/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getListSubShopPromotion(@RequestParam("id") Long parentId, @RequestParam("promotionId") Long promotionId) {
		List<JsTreeNode> lstShop = new ArrayList<JsTreeNode>();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				List<Shop> lstSubShop = shopMgr.getListSubShopForPromotionProgram(parentId, ActiveType.RUNNING, promotionId);
				if (lstSubShop != null && lstSubShop.size() > 0) {
					for (int i = 0; i < lstSubShop.size(); i++) {
						JsTreeNode node = new JsTreeNode();
						node.setData(lstSubShop.get(i).getShopCode() + " - " + lstSubShop.get(i).getShopName());
						JsTreeNodeAttr cNodeAttr = new JsTreeNodeAttr();
						cNodeAttr.setId(String.valueOf(lstSubShop.get(i).getId()));
						node.setAttr(cNodeAttr);
						node.setState(ConstantManager.JSTREE_STATE_CLOSE);
						lstShop.add(node);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShop;
	}

	/**
	 * Gets the staff type.
	 * 
	 * @param channelTypeId
	 *            the channel type id
	 * @return the staff type
	 */
	@RequestMapping(value = "/staff/gettype/{channelTypeId}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getStaffType(@PathVariable("channelTypeId") Long channelTypeId) {
		StatusBean promotionTypeName = new StatusBean(0L, "");
		if (channelTypeId != null) {
			try {
				ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
				if (channelTypeMgr != null) {
					ChannelType channelType = channelTypeMgr.getChannelTypeById(channelTypeId);
					if (channelType != null) {
						if (StaffObjectType.NVBH.getValue().equals(channelType.getObjectType()) || StaffObjectType.NVVS.getValue().equals(channelType.getObjectType())) {
							promotionTypeName.setValue(channelType.getId());
						} else {
							promotionTypeName.setValue(0L);
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return promotionTypeName;
	}

	/**
	 * Gets the product level stopped.
	 * 
	 * @param status
	 *            the status
	 * @param productLevelId
	 *            the product level id
	 * @return the product level stopped
	 * @RequestMapping(value = "/product/product-level/list", method =
	 *                       RequestMethod.GET) public @ResponseBody
	 *                       List<StatusBean>
	 *                       getProductLevelStopped(@RequestParam("status")
	 *                       Integer status,@RequestParam("productLevelId") Long
	 *                       productLevelId){ List<StatusBean> lstProductLevel =
	 *                       new ArrayList<StatusBean>(); try{ ProductLevelMgr
	 *                       productLevelMgr = (ProductLevelMgr)context.getBean(
	 *                       "productLevelMgr"); if(productLevelMgr!= null){
	 *                       if(status !=null && status == -2){ ProductLevel
	 *                       productLevel = productLevelMgr.getProductLevelById(
	 *                       productLevelId); if(productLevel != null &&
	 *                       ActiveType
	 *                       .STOPPED.equals(productLevel.getStatus())){
	 *                       StatusBean obj = new StatusBean();
	 *                       obj.setName(productLevel.getProductLevelCode() +
	 *                       " - " + productLevel.getProductLevelName());
	 *                       obj.setValue(productLevel.getId());
	 *                       lstProductLevel.add(obj); } }
	 *                       ObjectVO<ProductLevel> lstLevelTmp =
	 *                       productLevelMgr.getListProductLevel(null, null,
	 *                       null, ActiveType.RUNNING, null,null,null);
	 *                       if(lstLevelTmp!= null &&
	 *                       lstLevelTmp.getLstObject()!= null &&
	 *                       lstLevelTmp.getLstObject().size() > 0){ for(int
	 *                       i=0;i<lstLevelTmp.getLstObject().size();i++){
	 *                       if(lstLevelTmp.getLstObject()!= null){ StatusBean
	 *                       obj = new StatusBean();
	 *                       obj.setName(lstLevelTmp.getLstObject
	 *                       ().get(i).getProductLevelCode() + " - " +
	 *                       lstLevelTmp
	 *                       .getLstObject().get(i).getProductLevelName());
	 *                       obj.setValue
	 *                       (lstLevelTmp.getLstObject().get(i).getId());
	 *                       lstProductLevel.add(obj); } } } } }catch (Exception
	 *                       e) { LogUtility.logError(e,e.getMessage()); }
	 *                       return lstProductLevel; }
	 */

	/**
	 * Gets the flavour stopped.
	 * 
	 * @param status
	 *            the status
	 * @param flavourId
	 *            the flavour id
	 * @return the flavour stopped
	 */
	@RequestMapping(value = "/product/flavour/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getFlavourStopped(@RequestParam("status") Integer status, @RequestParam("flavourId") Long flavourId) {
		List<StatusBean> lstFlavour = new ArrayList<StatusBean>();
		try {
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			if (productInfoMgr != null) {
				if (status != null && status == -2) {
					ProductInfo productInfo = productInfoMgr.getProductInfoById(flavourId);
					if (productInfo != null && ActiveType.STOPPED.equals(productInfo.getStatus())) {
						StatusBean obj = new StatusBean();
						obj.setName(productInfo.getProductInfoCode() + " - " + productInfo.getProductInfoName());
						obj.setValue(productInfo.getId());
						lstFlavour.add(obj);
					}
				}
				/*ObjectVO<ProductInfo> lstFlavourTmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.FLAVOUR, true);
				if (lstFlavourTmp != null && lstFlavourTmp.getLstObject() != null && lstFlavourTmp.getLstObject().size() > 0) {
					for (int i = 0; i < lstFlavourTmp.getLstObject().size(); i++) {
						if (lstFlavourTmp.getLstObject() != null) {
							StatusBean obj = new StatusBean();
							obj.setName(lstFlavourTmp.getLstObject().get(i).getProductInfoCode() + " - " + lstFlavourTmp.getLstObject().get(i).getProductInfoName());
							obj.setValue(lstFlavourTmp.getLstObject().get(i).getId());
							lstFlavour.add(obj);
						}
					}
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstFlavour;
	}

	/**
	 * Gets the packing stopped.
	 * 
	 * @param status
	 *            the status
	 * @param packingId
	 *            the packing id
	 * @return the packing stopped
	 */
	@RequestMapping(value = "/product/packing/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getPackingStopped(@RequestParam("status") Integer status, @RequestParam("packingId") Long packingId) {
		List<StatusBean> lstPacking = new ArrayList<StatusBean>();
		try {
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			if (productInfoMgr != null) {
				if (status != null && status == -2) {
					ProductInfo productInfo = productInfoMgr.getProductInfoById(packingId);
					if (productInfo != null && ActiveType.STOPPED.equals(productInfo.getStatus())) {
						StatusBean obj = new StatusBean();
						obj.setName(productInfo.getProductInfoCode() + " - " + productInfo.getProductInfoName());
						obj.setValue(productInfo.getId());
						lstPacking.add(obj);
					}
				}
				/*ObjectVO<ProductInfo> lstFlavourTmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.PACKING, true);
				if (lstFlavourTmp != null && lstFlavourTmp.getLstObject() != null && lstFlavourTmp.getLstObject().size() > 0) {
					for (int i = 0; i < lstFlavourTmp.getLstObject().size(); i++) {
						if (lstFlavourTmp.getLstObject() != null) {
							StatusBean obj = new StatusBean();
							obj.setName(lstFlavourTmp.getLstObject().get(i).getProductInfoCode() + " - " + lstFlavourTmp.getLstObject().get(i).getProductInfoName());
							obj.setValue(lstFlavourTmp.getLstObject().get(i).getId());
							lstPacking.add(obj);
						}
					}
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstPacking;
	}

	/**
	 * Gets the brand stopped.
	 * 
	 * @param status
	 *            the status
	 * @param brandId
	 *            the brand id
	 * @return the brand stopped
	 */
	@RequestMapping(value = "/product/brand/list", method = RequestMethod.GET)
	public @ResponseBody
	List<StatusBean> getBrandStopped(@RequestParam("status") Integer status, @RequestParam("brandId") Long brandId) {
		List<StatusBean> lstBrand = new ArrayList<StatusBean>();
		try {
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			if (productInfoMgr != null) {
				if (status != null && status == -2) {
					ProductInfo productInfo = productInfoMgr.getProductInfoById(brandId);
					if (productInfo != null && ActiveType.STOPPED.equals(productInfo.getStatus())) {
						StatusBean obj = new StatusBean();
						obj.setName(productInfo.getProductInfoCode() + " - " + productInfo.getProductInfoName());
						obj.setValue(productInfo.getId());
						lstBrand.add(obj);
					}
				}
				/*ObjectVO<ProductInfo> lstFlavourTmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.BRAND, true);
				if (lstFlavourTmp != null && lstFlavourTmp.getLstObject() != null && lstFlavourTmp.getLstObject().size() > 0) {
					for (int i = 0; i < lstFlavourTmp.getLstObject().size(); i++) {
						if (lstFlavourTmp.getLstObject() != null) {
							StatusBean obj = new StatusBean();
							obj.setName(lstFlavourTmp.getLstObject().get(i).getProductInfoCode() + " - " + lstFlavourTmp.getLstObject().get(i).getProductInfoName());
							obj.setValue(lstFlavourTmp.getLstObject().get(i).getId());
							lstBrand.add(obj);
						}
					}
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstBrand;
	}

	/**
	 * Gets the uom1 stopped.
	 * 
	 * @param status
	 *            the status
	 * @param uom1Code
	 *            the uom1 code
	 * @return the uom1 stopped
	 */
	@RequestMapping(value = "/product/uom1/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CategoryBean> getUom1Stopped(@RequestParam("status") Integer status, @RequestParam("uom1Code") String uom1Code) {
		List<CategoryBean> lstUom1 = new ArrayList<CategoryBean>();
		try {
			ApParamMgr apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
			if (apParamMgr != null) {
				if (status != null && status == -2) {
					ApParam apParam = apParamMgr.getApParamByCode(uom1Code, ApParamType.UOM);
					if (apParam != null && ActiveType.STOPPED.equals(apParam.getStatus())) {
						CategoryBean obj = new CategoryBean();
						obj.setName(apParam.getApParamCode() + " - " + apParam.getApParamName());
						obj.setCode(apParam.getApParamCode());
						lstUom1.add(obj);
					}
				}
				List<ApParam> lstUomTmp = apParamMgr.getListApParam(ApParamType.UOM, ActiveType.RUNNING);
				if (lstUomTmp != null && lstUomTmp.size() > 0) {
					for (int i = 0; i < lstUomTmp.size(); i++) {
						CategoryBean obj = new CategoryBean();
						obj.setName(lstUomTmp.get(i).getApParamCode() + " - " + lstUomTmp.get(i).getApParamName());
						obj.setCode(lstUomTmp.get(i).getApParamCode());
						lstUom1.add(obj);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstUom1;
	}

	/**
	 * Gets the uom2 stopped.
	 * 
	 * @param status
	 *            the status
	 * @param uom2Code
	 *            the uom2 code
	 * @return the uom2 stopped
	 */
	@RequestMapping(value = "/product/uom2/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CategoryBean> getUom2Stopped(@RequestParam("status") Integer status, @RequestParam("uom2Code") String uom2Code) {
		List<CategoryBean> lstUom2 = new ArrayList<CategoryBean>();
		try {
			ApParamMgr apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
			if (apParamMgr != null) {
				if (status != null && status == -2) {
					ApParam apParam = apParamMgr.getApParamByCode(uom2Code, ApParamType.UOM);
					if (apParam != null && ActiveType.STOPPED.equals(apParam.getStatus())) {
						CategoryBean obj = new CategoryBean();
						obj.setName(apParam.getApParamCode() + " - " + apParam.getApParamName());
						obj.setCode(apParam.getApParamCode());
						lstUom2.add(obj);
					}
				}
				List<ApParam> lstUomTmp = apParamMgr.getListApParam(ApParamType.UOM, ActiveType.RUNNING);
				if (lstUomTmp != null && lstUomTmp.size() > 0) {
					for (int i = 0; i < lstUomTmp.size(); i++) {
						CategoryBean obj = new CategoryBean();
						obj.setName(lstUomTmp.get(i).getApParamCode() + " - " + lstUomTmp.get(i).getApParamName());
						obj.setCode(lstUomTmp.get(i).getApParamCode());
						lstUom2.add(obj);
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstUom2;
	}

	@RequestMapping(value = "/level/code/{category}/{subCategory}", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean getLevelCode(@PathVariable("category") Long categoryId, @PathVariable("subCategory") Long subCategoryId) {
		StatusBean level = new StatusBean(0L, "");
		try {
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			if (productInfoMgr != null) {
				if (categoryId != null && categoryId != -2 && subCategoryId != null && subCategoryId != -2) {
					ProductInfo category = productInfoMgr.getProductInfoById(categoryId);
					ProductInfo subCategory = productInfoMgr.getProductInfoById(subCategoryId);
					if (category != null && subCategory != null) {
						level.setName(category.getProductInfoCode() + "." + subCategory.getProductInfoCode());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return level;
	}

	/*
	 * @RequestMapping(value = "/intensive-program/product/list", method =
	 * RequestMethod.GET) public @ResponseBody List<JsTreeNode>
	 * getListProductOfIncentiveProgram(@RequestParam("incentiveProgramId") long
	 * incentiveProgramId,
	 * 
	 * @RequestParam("productCode") String
	 * productCode,@RequestParam("productName") String productName,
	 * @RequestParam("nodeType") String nodeType, @RequestParam("catId") long
	 * catId, @RequestParam("subCatId") long subCatId){ List<JsTreeNode> lstCat
	 * = new ArrayList<JsTreeNode>();
	 * if(!StringUtil.isNullOrEmpty(productCode)){ productCode =
	 * productCode.trim(); } if(!StringUtil.isNullOrEmpty(productName)){
	 * productName = productName.trim(); } try{
	 * 
	 * DisplayProgramMgr displayProgramMgr =
	 * (DisplayProgramMgr)context.getBean("displayProgramMgr");
	 * if(displayProgramMgr!= null){ if(!StringUtil.isNullOrEmpty(productCode)
	 * || !StringUtil.isNullOrEmpty(productName)) { boolean isFirst = true;
	 * 
	 * List<ProductCatVO> focusProgramDetailVo =
	 * displayProgramMgr.getListProductCatVOEx(productCode, productName,
	 * incentiveProgramId,null); for(int i = 0; i < focusProgramDetailVo.size();
	 * i++) { JsTreeNode pNode = new JsTreeNode(); ProductCatVO catVo =
	 * focusProgramDetailVo.get(i); if(isFirst) { if(catVo.getProductInfo() !=
	 * null) { pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - "
	 * + catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr
	 * = new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr); if(catVo.getLstProductSubCatVO()!= null &&
	 * catVo.getLstProductSubCatVO().size()>0){
	 * pNode.setState(ConstantManager.JSTREE_STATE_OPEN); } else {
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * if(catVo.getLstProductSubCatVO()!= null &&
	 * catVo.getLstProductSubCatVO().size()>0){ List<JsTreeNode> lstSubCat = new
	 * ArrayList<JsTreeNode>(); for (int j = 0; j <
	 * catVo.getLstProductSubCatVO().size(); j++) { JsTreeNode subCatNode = new
	 * JsTreeNode(); ProductSubCatVO subCatVO =
	 * catVo.getLstProductSubCatVO().get(j); if(isFirst) {
	 * if(subCatVO.getProductInfo()!= null){
	 * subCatNode.setData(subCatVO.getProductInfo().getProductInfoCode() + " - "
	 * + subCatVO.getProductInfo().getProductInfoName()); JsTreeNodeAttr
	 * scNodeAttr = new JsTreeNodeAttr();
	 * scNodeAttr.setId(String.valueOf(subCatVO.getProductInfo().getId()));
	 * scNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * scNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(scNodeAttr); if(subCatVO.getLstProduct()!= null &&
	 * subCatVO.getLstProduct().size()>0) {
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_OPEN); } else {
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * if(subCatVO.getLstProduct()!= null && subCatVO.getLstProduct().size()>0){
	 * List<JsTreeNode> lstProduct = new ArrayList<JsTreeNode>(); for (int k =
	 * 0; k < subCatVO.getLstProduct().size(); k++) { JsTreeNode productNode =
	 * new JsTreeNode(); Product product = subCatVO.getLstProduct().get(k);
	 * if(product!= null){ productNode.setData(product.getProductCode() + " - "
	 * + product.getProductName()); JsTreeNodeAttr pNodeAttr = new
	 * JsTreeNodeAttr(); pNodeAttr.setId(String.valueOf(product.getId()));
	 * pNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * pNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNode.setAttr(pNodeAttr);
	 * productNode.setState(ConstantManager.JSTREE_STATE_LEAF); }
	 * lstProduct.add(productNode); } subCatNode.setChildren(lstProduct);
	 * isFirst = false; } } else { if(subCatVO.getProductInfo()!= null){
	 * subCatNode.setData(subCatVO.getProductInfo().getProductInfoCode() + " - "
	 * + subCatVO.getProductInfo().getProductInfoName()); JsTreeNodeAttr
	 * scNodeAttr = new JsTreeNodeAttr();
	 * scNodeAttr.setId(String.valueOf(subCatVO.getProductInfo().getId()));
	 * scNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * scNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(scNodeAttr);
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * lstSubCat.add(subCatNode); } pNode.setChildren(lstSubCat); }
	 * 
	 * } else { if(catVo.getProductInfo() != null) {
	 * pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " +
	 * catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr =
	 * new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr);
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } }
	 * 
	 * lstCat.add(pNode); } } else {
	 * if(nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_CAT) &&
	 * catId == 0 && subCatId == 0) { List<ProductCatVO> focusProgramDetailVo =
	 * displayProgramMgr.getListProductCatVOEx(productCode, productName,
	 * incentiveProgramId,null); for(int i = 0; i < focusProgramDetailVo.size();
	 * i++) { JsTreeNode pNode = new JsTreeNode(); ProductCatVO catVo =
	 * focusProgramDetailVo.get(i); if(catVo.getProductInfo() != null) {
	 * pNode.setData(catVo.getProductInfo().getProductInfoCode() + " - " +
	 * catVo.getProductInfo().getProductInfoName()); JsTreeNodeAttr cNodeAttr =
	 * new JsTreeNodeAttr();
	 * cNodeAttr.setId(String.valueOf(catVo.getProductInfo().getId()));
	 * cNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * cNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_CAT);
	 * pNode.setAttr(cNodeAttr);
	 * pNode.setState(ConstantManager.JSTREE_STATE_CLOSE); } lstCat.add(pNode);
	 * } } else
	 * if(nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_SUBCAT) &&
	 * catId > 0 && subCatId == 0) { List<ProductInfo> listSubCatVo =
	 * displayProgramMgr.getListSubCat(productCode, productName,
	 * incentiveProgramId, catId,null); for(int i = 0; i < listSubCatVo.size();
	 * i++) { JsTreeNode subCatNode = new JsTreeNode(); ProductInfo subCat =
	 * listSubCatVo.get(i); subCatNode.setData(subCat.getProductInfoCode() +
	 * " - " + subCat.getProductInfoName()); JsTreeNodeAttr subCatNodeAttr = new
	 * JsTreeNodeAttr(); subCatNodeAttr.setId(String.valueOf(subCat.getId()));
	 * subCatNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNodeAttr.setContentItemId(ConstantManager.JSTREE_NODE_TYPE_SUBCAT);
	 * subCatNode.setAttr(subCatNodeAttr);
	 * subCatNode.setState(ConstantManager.JSTREE_STATE_CLOSE);
	 * lstCat.add(subCatNode); } } else if
	 * (nodeType.equalsIgnoreCase(ConstantManager.JSTREE_NODE_TYPE_PRODUCT) &&
	 * catId > 0 && subCatId > 0) { List<Product> listProduct =
	 * displayProgramMgr.getListProductBySubCat(productCode, productName,
	 * incentiveProgramId, catId, subCatId,null); for(int i = 0; i <
	 * listProduct.size(); i++) { JsTreeNode productNode = new JsTreeNode();
	 * Product product = listProduct.get(i);
	 * productNode.setData(product.getProductCode() + " - " +
	 * product.getProductName()); JsTreeNodeAttr productNodeAttr = new
	 * JsTreeNodeAttr(); productNodeAttr.setId(String.valueOf(product.getId()));
	 * productNodeAttr.setClassStyle(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNodeAttr
	 * .setContentItemId(ConstantManager.JSTREE_NODE_TYPE_PRODUCT);
	 * productNode.setAttr(productNodeAttr);
	 * productNode.setState(ConstantManager.JSTREE_STATE_LEAF);
	 * lstCat.add(productNode); } } } } }catch (Exception e) {
	 * LogUtility.logError(e,e.getMessage()); } return lstCat; }
	 */

	//	@RequestMapping(value = "/programme-display/shop/list", method = RequestMethod.GET)
	//	public @ResponseBody List<JsTreeNode> getShopTreeOfDisplayProgram(@RequestParam("displayProgramId") long displayProgramId,
	//			@RequestParam("shopCode") String shopCode,@RequestParam("shopName") String shopName){
	//		if(!StringUtil.isNullOrEmpty(shopCode)){
	//			shopCode = shopCode.trim();
	//		}
	//		if(!StringUtil.isNullOrEmpty(shopName)){
	//			shopName = shopName.trim();
	//		}
	//	    List<JsTreeNode> lstShopTree = new ArrayList<JsTreeNode>();		
	//		try{
	//		    ShopMgr shopMgr = (ShopMgr)context.getBean("shopMgr");
	//		    DisplayProgramMgr displayProgramMgr = (DisplayProgramMgr) context.getBean("displayProgramMgr");
	//		    Long parentShopId = null;
	//		    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	//		    HttpSession session =  attr.getRequest().getSession(true);
	//		    if(session!= null && session.getAttribute(ConstantManager.SESSION_SHOP_ID)!= null){
	//		    	parentShopId = Long.valueOf(String.valueOf(session.getAttribute(ConstantManager.SESSION_SHOP_ID)));
	//		    }
	//		    
	//		    if(shopMgr!= null && displayProgramMgr != null){
	//		    	ObjectVO<DisplayShopMap> lstFSMap = displayProgramMgr.getListDisplayShopMap(null, displayProgramId, null, null, null, null);
	//				List<Long> lstShopId = new ArrayList<Long>();
	//				if(lstFSMap != null && lstFSMap.getLstObject() != null){
	//					for(int i=0;i<lstFSMap.getLstObject().size();i++){
	//						if(lstFSMap.getLstObject().get(i).getShop() != null){
	//							if(!lstFSMap.getLstObject().get(i).getStatus().getValue().equals(ActiveType.DELETED.getValue())) {
	//								lstShopId.add(lstFSMap.getLstObject().get(i).getShop().getId());
	//							}
	//						}
	//					}
	//				}
	//		    	
	//		    	TreeVO<Shop> shopVO =  shopMgr.getShopTreeVOOneLevel(parentShopId, lstShopId, shopCode, shopName, displayProgramId, ProgramObjectType.DISPLAY_PROGRAM,null);
	//				if(shopVO.getObject() == null && shopVO.getListChildren() != null){
	//					for(int i=0;i<shopVO.getListChildren().size();i++){
	//						JsTreeNode tmp = new JsTreeNode();
	//						tmp = getUnitTreeBeanWithChildContaintName(tmp, shopVO.getListChildren().get(i));
	//						lstShopTree.add(tmp);
	//					}
	//				}else{
	//					JsTreeNode bean = new JsTreeNode(); 
	//					bean = getUnitTreeBeanWithChildContaintName(bean, shopVO);
	//					lstShopTree.add(bean);
	//				}
	//		    }
	//		}catch (Exception e) {
	//		    LogUtility.logError(e,e.getMessage());
	//		}
	//		return lstShopTree;		
	//	}

	@RequestMapping(value = "/sub-shop/programme-display/list", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getListSubShopOfDisplayProgram(@RequestParam("id") Long parentId, @RequestParam("displayProgramId") Long displayProgramId) {
		List<JsTreeNode> lstShop = new ArrayList<JsTreeNode>();
		try {
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (shopMgr != null) {
				/*
				 * comment code loi compile
				 * @modified by tuannd20
				 * @date 20/12/2014
				 */
				/*List<Shop> lstSubShop = shopMgr.getListSubShopForDisplayProgram(parentId, ActiveType.RUNNING, displayProgramId);
				if (lstSubShop != null && lstSubShop.size() > 0) {
					for (int i = 0; i < lstSubShop.size(); i++) {
						JsTreeNode node = new JsTreeNode();
						node.setData(lstSubShop.get(i).getShopCode() + " - " + lstSubShop.get(i).getShopName());
						JsTreeNodeAttr cNodeAttr = new JsTreeNodeAttr();
						cNodeAttr.setId(String.valueOf(lstSubShop.get(i).getId()));
						node.setAttr(cNodeAttr);
						node.setState(ConstantManager.JSTREE_STATE_CLOSE);
						lstShop.add(node);
					}
				}*/
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShop;
	}

	@RequestMapping(value = "/display-program/treeQoutaShowErrorMsg/{productId}/list.json", method = RequestMethod.GET)
	public @ResponseBody
	List<String> checkQoutaTree(@PathVariable("productId") String strproductId) throws BusinessException {
		ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
		String[] temp = strproductId.split(",");
		List<String> result = new ArrayList<String>();
		String tt = "";
		for (int i = 0; i < temp.length; i++) {
			Product a = productMgr.getProductById(Long.parseLong(temp[i]));
			if (StringUtil.isNullOrEmpty(tt)) {
				tt += a.getProductCode();
			} else {
				tt += "," + a.getProductCode();
			}
		}
		result.add(tt);
		return result;
	}

	@RequestMapping(value = "/customer/dskh/reporter/list", method = RequestMethod.GET)
	public @ResponseBody
	Customer showCustomerByShopId(@RequestParam("shopId") Long shopId, @RequestParam("shortCode") String shortCode) {
		Customer customer = new Customer();
		try {
			if (shopId != null && shortCode != null) {
				ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
				CustomerMgr customerMgr = (CustomerMgr) context.getBean("customerMgr");
				Shop shop = shopMgr.getShopById(shopId);
				if (shop == null)
					return customer;
				String customerCode = StringUtil.getFullCode(shop.getShopCode(), shortCode);
				customer = customerMgr.getCustomerByCode(customerCode);
				if (customer == null)
					return customer;
				return customer;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return customer;
	}

	@RequestMapping(value = "/shop/combotree/{type}/{shopId}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getShopComboTree(@PathVariable("type") int type, @PathVariable("shopId") Long shopId) {
		/*List<ComboTreeBean> lstShopTree = new ArrayList<ComboTreeBean>();
		Shop currentShop = null;
		List<Shop> shopTreeVO = null;
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		Staff staff = null;
		StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
		UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
		if (currentUser != null && currentUser.getUserName() != null) {
			try {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE) != null && staff != null && staff.getStaffType().getObjectType() == StaffObjectType.NVGS.getValue()) {
			currentShop = (Shop) session.getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
		} else {
			if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP) != null) {
				currentShop = (Shop) session.getAttribute(ConstantManager.SESSION_SHOP);
			}
		}
		*//** vuongmq: khong can title chon đơn vị; 24-Oct, 2014*//*
		if (shopId != null && shopId == 0) {
			shopId = null;
			ComboTreeBean bean = new ComboTreeBean();
			bean.setId(String.valueOf(ConstantManager.NOT_STATUS));
			bean.setState(ConstantManager.JSTREE_STATE_OPEN);
			bean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "shop.common.tree.choose"));
			lstShopTree.add(bean);
		}
		if (currentShop != null) {
			if (type == COMBOTREE_ONE_NODE) {
				ComboTreeBean tmp = new ComboTreeBean();
				tmp = loadComboTreeForShop(tmp, currentShop);
				lstShopTree.add(tmp);
			} else {
				try {
					ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
					Shop selShop = shopMgr.getShopById(shopId);
					if (selShop == null) {
						ComboTreeBean bean = new ComboTreeBean();
						lstShopTree.add(bean);
						return lstShopTree;
					}
					shopTreeVO = shopMgr.getListSubShopEx(selShop.getId(), ActiveType.RUNNING, null);

					if (shopTreeVO != null) {
						ComboTreeBean tmp = null;
						Shop shT = null;
						if (ShopObjectType.VNM.getValue().equals(selShop.getType().getObjectType())) {
							Integer objType = null;
							for (int i = 0, sz = shopTreeVO.size(); i < sz; i++) {
								shT = shopTreeVO.get(i);
								objType = shT.getType().getObjectType();
								if (ShopObjectType.GT.getValue().equals(objType) || ShopObjectType.KA.getValue().equals(objType) || ShopObjectType.ST.getValue().equals(objType)) {
									tmp = new ComboTreeBean();
									tmp = loadComboTreeForShop(tmp, shT);
									lstShopTree.add(tmp);
								}
							}
						} else {
							for (int i = 0, sz = shopTreeVO.size(); i < sz; i++) {
								shT = shopTreeVO.get(i);
								if (shT.getType().getObjectType().equals(selShop.getType().getObjectType() + 1)) {
									tmp = new ComboTreeBean();
									tmp = loadComboTreeForShop(tmp, shT);
									lstShopTree.add(tmp);
								}
							}
						}
					} else {
						ComboTreeBean bean = new ComboTreeBean();
						lstShopTree.add(bean);
					}
				} catch (Exception e) {
					LogUtility.logError(e, e.getMessage());
				}
			}
		}
		return lstShopTree;*/
		return null;
	}

	/*
	 * private ComboTreeBean loadComboTreeForShopOld(ComboTreeBean node,Shop
	 * shop){ if(shop!=null){
	 * node.setId(String.valueOf(shop.getId().toString()));
	 * node.setText(shop.getShopName());
	 * node.setState(ConstantManager.JSTREE_STATE_CLOSE); node.setChildren(new
	 * ArrayList<ComboTreeBean>()); } return node; }
	 */
	@RequestMapping(value = "/parentshop/combotree/{shopId}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getParentShopComboTree(@PathVariable("shopId") Long shopId) {
		/*ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
		List<ComboTreeBean> lstShopTree = new ArrayList<ComboTreeBean>();
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		UserToken currentUser = null;
		if (session.getAttribute("cmsUserToken") != null) {
			currentUser = (UserToken) session.getAttribute("cmsUserToken");
		}
		ComboTreeBean nullBean = new ComboTreeBean();
		nullBean.setId("-2");
		nullBean.setState("open");
		nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "shop.common.tree.choose"));
		lstShopTree.add(nullBean);
		Shop sh = null;
		try {
			Shop currentShop = null;
			if (currentUser != null) {
				currentShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			if (shopId != null && shopId > 0) {
				sh = shopMgr.getShopById(shopId);
				if (sh == null) {
					return lstShopTree;
				}

				List<Shop> lstShop = getListShopParent(sh, currentUser);
				if (lstShop == null) {
					lstShop = new ArrayList<Shop>();
				}

				ComboTreeBean node = null;
				ComboTreeBean tmp = null;
				ComboTreeBean nodeT = null;
				List<Shop> lst = null;

				Shop shNode = currentShop;
				nodeT = new ComboTreeBean();
				nodeT = loadComboTreeForShop(nodeT, shNode);
				lstShopTree.add(nodeT);

				Shop shT = null;
				int sz = lstShop.size() - 1;
				int j = 0, sz1 = 0;
				Integer shType = null;
				for (int i = sz; i > 0; i--) {
					node = nodeT;
					ShopFilter filter = new ShopFilter();
					filter.setIsGetOneChildLevel(true);
					filter.setLstParentId(Arrays.asList(shNode.getId()));
					ObjectVO<Shop> vo = shopMgr.getListShop(filter);
					//					lst = shopMgr.getListSubShop(shNode.getId(), ActiveType.RUNNING);
					if (vo != null && vo.getLstObject() != null) {
						lst = vo.getLstObject();
					} else {
						lst = new ArrayList<Shop>();
					}
					node.setState(ConstantManager.JSTREE_STATE_OPEN);
					shType = shNode.getType().getObjectType();
					if (ShopObjectType.VNM.getValue().equals(shType)) {
						Integer objType = null;
						for (j = 0, sz1 = lst.size(); j < sz1; j++) {
							shT = lst.get(j);
							objType = shT.getType().getObjectType();
							if (ShopObjectType.GT.getValue().equals(objType) || ShopObjectType.KA.getValue().equals(objType) || ShopObjectType.ST.getValue().equals(objType)) {
								tmp = new ComboTreeBean();
								tmp = loadComboTreeForShop(tmp, shT);
								node.getChildren().add(tmp);
								if (shT.getId().equals(lstShop.get(i - 1).getId())) {
									shNode = shT;
									nodeT = tmp;
								}
							}
						}
					} else {
						for (j = 0, sz1 = lst.size(); j < sz1; j++) {
							shT = lst.get(j);
							if (shT.getType().getObjectType().equals(shType + 1)) {
								tmp = new ComboTreeBean();
								tmp = loadComboTreeForShop(tmp, shT);
								node.getChildren().add(tmp);
								if (shT.getId().equals(lstShop.get(i - 1).getId())) {
									shNode = shT;
									nodeT = tmp;
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
		}
		return lstShopTree;*/
		return null;
	}

	public String getStaffName(Staff staff) {
		String result = "";
		/*if (StaffObjectType.TBHM.getValue().equals(staff.getStaffType().getObjectType())) {
			result = staff.getStaffName();
		} else if (StaffObjectType.TBHV.getValue().equals(staff.getStaffType().getObjectType())) {
			result = staff.getStaffName();
		} else if (StaffObjectType.NHVNM.getValue().equals(staff.getStaffType().getObjectType())) {
			result = "VNM - " + staff.getStaffName();
		} else {
			result = staff.getStaffCode() + " - " + staff.getStaffName();
		}*/
		return result;
	}

	public List<Shop> getListShopParent(Shop shop, UserToken currentUser) {
		List<Shop> lstShop = new ArrayList<Shop>();
		try {
			while (shop != null) {
				lstShop.add(shop);
				shop = shop.getParentShop();
			}
		} catch (Exception e) {
		}
		return lstShop;
	}

	public String getIconForStaff(Staff staff) {
		String icon = "";
		return icon;
	}

	public String getIconForGroup(StaffGroup sg) {
		String icon = "";
		return icon;
	}

	public StaffTreeVO converterStaffTreeVO(Staff staff) {
		StaffTreeVO st = new StaffTreeVO();
		try {
			if (staff != null) {
				st.setId(staff.getId());
				st.setShop(converterShopTreeVO(staff.getShop(), 1));
				st.setStaffCode(staff.getStaffCode());
				st.setStaffName(staff.getStaffName());
				st.setStaffType(staff.getStaffType());
			}
		} catch (Exception e) {
		}
		return st;
	}

	public ShopTreeVO converterShopTreeVO(Shop shop, Integer parent) {
		ShopTreeVO st = new ShopTreeVO();
		try {
			if (shop != null) {
				st.setId(shop.getId());
				st.setShopCode(shop.getShopCode());
				st.setShopName(shop.getShopName());
				st.setType(shop.getType());
				st.setStatus(shop.getStatus());
				if (parent == 1 && shop.getParentShop() != null) {
					st.setParentShop(converterShopTreeVO(shop.getParentShop(), 0));
				} else {
					st.setParentShop(null);
				}
			}
		} catch (Exception e) {
		}
		return st;
	}

	public StaffGroupTreeVO converterStaffGroupTreeVO(StaffGroup staffGroup) {
		StaffGroupTreeVO sg = new StaffGroupTreeVO();
		try {
			if (staffGroup != null) {
				sg.setId(staffGroup.getId());
				sg.setShop(converterShopTreeVO(staffGroup.getShop(), 1));
				sg.setStaffGroupName(staffGroup.getStaffGroupName());
			}
		} catch (Exception e) {
		}
		return sg;
	}

	public List<JETreeNode> parseStaff(List<StaffGroup> staffGroup) {
		StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
		List<JETreeNode> lstBean = new ArrayList<JETreeNode>();
		try {
			for (StaffGroup sg : staffGroup) {
				JETreeNode bean = new JETreeNode();
				List<JETreeNode> children = new ArrayList<JETreeNode>();
				JETreeNodeAttr attrGroup = new JETreeNodeAttr();
				attrGroup.setGroup(converterStaffGroupTreeVO(sg));
				bean.setAttributes(attrGroup);
				ObjectVO<Staff> lstStaffVO = null;
				List<Staff> lstStaff = new ArrayList<Staff>();
				lstStaffVO = staffMgr.getListStaffInStaffGroup(null, sg.getId(), ActiveType.RUNNING);
				if (lstStaffVO != null)
					lstStaff = lstStaffVO.getLstObject();
				for (Staff staff : lstStaff) {
					if (staff != null) {
						JETreeNode node = new JETreeNode();
						JETreeNodeAttr attr = new JETreeNodeAttr();
						attr.setStaff(converterStaffTreeVO(staff));
						attr.setGroup(converterStaffGroupTreeVO(sg));
						node.setAttributes(attr);
						node.setId("s" + staff.getId());
						node.setState(ConstantManager.JSTREE_STATE_LEAF);
						node.setText(getStaffName(staff));
						node.setIconCls(getIconForStaff(staff));
						children.add(node);
					}
				}
				bean.setId("g" + sg.getId());
				bean.setText(sg.getStaffGroupName());
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
				bean.setChildren(children);
				bean.setIconCls(getIconForGroup(sg));
				lstBean.add(bean);
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstBean;
	}

	public List<JETreeNode> parseShop(List<Shop> lst) {
		StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
		ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
		List<JETreeNode> lstBean = new ArrayList<JETreeNode>();
		try {
			for (Shop shop : lst) {
				if (shop != null) {
					JETreeNode bean = new JETreeNode();
					JETreeNodeAttr attr = new JETreeNodeAttr();
					attr.setShop(converterShopTreeVO(shop, 1));
					bean.setId(shop.getId().toString());
					bean.setAttributes(attr);
					if (ActiveType.RUNNING.equals(shop.getStatus())) {
						//						bean.setText(shop.getShopCode()+" - "+shop.getShopName()+Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.running"));
						bean.setText(shop.getShopCode() + " - " + shop.getShopName());
					} else {
						//						bean.setText(shop.getShopCode()+" - "+shop.getShopName()+Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.stop"));
						bean.setText(shop.getShopCode() + " - " + shop.getShopName());
					}
					List<Shop> lstSubShop = null;
					ObjectVO<StaffGroup> lstStaffGroupVO = null;
					lstSubShop = shopMgr.getListSubShop(shop.getId(), null);
					lstStaffGroupVO = staffMgr.getListStaffGroup(null, shop.getId(), null);
					if ((lstSubShop != null && lstSubShop.size() > 0) || (lstStaffGroupVO != null && lstStaffGroupVO.getLstObject() != null && lstStaffGroupVO.getLstObject().size() > 0)) {
						if (lstStaffGroupVO != null && lstStaffGroupVO.getLstObject() != null && lstStaffGroupVO.getLstObject().size() > 0)
							attr.setHasGroup(1);
						bean.setState(ConstantManager.JSTREE_STATE_CLOSE);
					} else {
						bean.setState(ConstantManager.JSTREE_STATE_LEAF);
						bean.setIconCls("triconline");
					}
					lstBean.add(bean);
				}
			}
		} catch (BusinessException e) {
		}
		return lstBean;
	}

	public List<JETreeNode> getTreeNodeByShop(Long id) {
		/*StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
		ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
		List<JETreeNode> lstUnitTree = new ArrayList<JETreeNode>();
		try {
			ObjectVO<StaffGroup> lstStaffGroupVO = staffMgr.getListStaffGroup(null, id, null);
			List<StaffGroup> lstStaffGroup = new ArrayList<StaffGroup>();
			if (lstStaffGroupVO != null)
				lstStaffGroup = lstStaffGroupVO.getLstObject();
			List<JETreeNode> lstNodeStaff = parseStaff(lstStaffGroup);
			for (JETreeNode temp : lstNodeStaff) {
				lstUnitTree.add(temp);
			}
			Shop shop = shopMgr.getShopById(id);
			if (shop != null) {
				List<Shop> lstSubShop = shopMgr.getListSubShop(id, null);
				if (lstStaffGroupVO != null)
					lstStaffGroup = lstStaffGroupVO.getLstObject();
				List<JETreeNode> lstNodeShop = parseShop(lstSubShop);
				if (ShopObjectType.VNM.getValue().equals(shop.getType().getObjectType())) {
					Integer objType = null;
					for (JETreeNode temp : lstNodeShop) {
						if (temp.getAttributes() != null && temp.getAttributes().getShop() != null) {
							objType = temp.getAttributes().getShop().getType().getObjectType();
							if (ShopObjectType.GT.getValue().equals(objType) || ShopObjectType.KA.getValue().equals(objType) || ShopObjectType.ST.getValue().equals(objType)) {
								lstUnitTree.add(temp);
							}
						}
					}
				} else {
					for (JETreeNode temp : lstNodeShop) {
						if (temp.getAttributes() != null && temp.getAttributes().getShop() != null && temp.getAttributes().getShop().getType().getObjectType().equals(shop.getType().getObjectType() + 1)) {
							lstUnitTree.add(temp);
						}
					}
				}
			}
		} catch (BusinessException e) {

		}
		return lstUnitTree;*/
		return null;
	}

	public List<JETreeNode> getTreeNodeByShopParent(List<Shop> lstShop, Integer level) {
		List<JETreeNode> lstUnitTree = new ArrayList<JETreeNode>();
		if (level.equals(0)) {
			if (lstShop.get(level) != null) {
				lstUnitTree = getTreeNodeByShop(lstShop.get(level).getId());
			}
			return lstUnitTree;
		} else {
			lstUnitTree = getTreeNodeByShop(lstShop.get(level).getId());
			List<JETreeNode> lstUnitTreeChild = getTreeNodeByShopParent(lstShop, level - 1);
			for (JETreeNode temp : lstUnitTree) {
				if (temp.getAttributes().getShop() != null && temp.getAttributes().getShop().getId().equals(lstShop.get(level - 1).getId())) {
					temp.setChildren(lstUnitTreeChild);
					temp.setState(ConstantManager.JSTREE_STATE_OPEN);
				}
			}
		}
		return lstUnitTree;
	}

	private List<JETreeNode> getTreeNodeByShopParentX(ShopMgr shopMgr, List<Shop> lstShop, Integer level) throws Exception {
		List<JETreeNode> lstUnitTree = new ArrayList<JETreeNode>();
		Shop shT = lstShop.get(level);
		if (level.equals(0)) {
			if (shT != null) {
				lstUnitTree = getTreeNodeByShop(lstShop.get(level).getId());
			}
			return lstUnitTree;
		}

		lstUnitTree = getTreeNodeByShop(shT.getId());
		List<JETreeNode> lstUnitTreeChild = getTreeNodeByShopParentX(shopMgr, lstShop, level - 1);
		Shop sh = lstShop.get(level - 1);
		ShopTreeVO attSh = null;
		for (JETreeNode temp : lstUnitTree) {
			attSh = temp.getAttributes().getShop();
			if (attSh == null) {
				continue;
			}
			if (attSh.getId().equals(sh.getId())) {
				temp.setChildren(lstUnitTreeChild);
				temp.setState(ConstantManager.JSTREE_STATE_OPEN);
				break;
			}
		}
		return lstUnitTree;
	}

	/***************************** CAY DON VI OLD VERSION ********************************/
	//	/**
	//	 * Gets the unit tree combo tree.
	//	 *
	//	 * @param shopCode the shop code
	//	 * @param type the type
	//	 * @param currentNode the current node
	//	 * @return the unit tree combo tree
	//	 * @author lamnh,tientv
	//	 * @since Oct 1, 2012
	//	 */
	//	@RequestMapping(value = "/unit-tree/combotree/{shopCode}/{type}/{currentNode}", method = RequestMethod.POST)
	//	public @ResponseBody List<ComboTreeBean> getUnitTreeComboTree(@PathVariable("shopCode") String shopCode,@PathVariable("type") int type, @PathVariable("currentNode") Long currentNode){	    
	//	    List<ComboTreeBean> lstUnitTree = new ArrayList<ComboTreeBean>();
	//	    ComboTreeBean nullBean = new ComboTreeBean();
	//	    nullBean.setId("-1");
	//	    nullBean.setState("open");
	//	    nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.tree.choose.parent"));
	//	    lstUnitTree.add(nullBean);
	//		try{
	//		    ShopMgr shopMgr = (ShopMgr)context.getBean("shopMgr");
	//		    if(shopMgr!= null){
	//		    	if(!StringUtil.isNullOrEmpty(shopCode)){
	//		    		Shop shop = shopMgr.getShopByCode(shopCode);
	//		    		TreeVO<Shop> shopTreeVO = null;
	//		    		if(type == 1){
	//		    			shopTreeVO = shopMgr.getShopTreeVO(shop,currentNode,null,null,true);
	//		    		} else {
	//		    			shopTreeVO = shopMgr.getShopTreeVO(shop,null,null,null,true);
	//		    		}
	//		    		if(shopTreeVO.getObject() == null && shopTreeVO.getListChildren() != null){
	//		    			for(int i=0;i<shopTreeVO.getListChildren().size();i++){
	//		    				ComboTreeBean tmp = new ComboTreeBean();
	//		    				tmp = loadComboTreeForShop(tmp, shopTreeVO.getListChildren().get(i));
	//		    				lstUnitTree.add(tmp);
	//		    			}
	//		    		}else{
	//		    			ComboTreeBean bean = new ComboTreeBean(); 
	//		    			bean = loadComboTreeForShop(bean, shopTreeVO);
	//		    			lstUnitTree.add(bean);
	//		    		}
	//		    	}
	//		    }
	//		}catch (Exception e) {
	//		    LogUtility.logError(e,e.getMessage());
	//		}
	//		return lstUnitTree;
	//	}
	//	/**
	//	 * Load combo tree for shop.
	//	 *
	//	 * @param node the node
	//	 * @param shop the shop
	//	 * @return the combo tree bean
	//	 * @author lamnh
	//	 * @since Oct 1, 2012
	//	 */
	//	private ComboTreeBean loadComboTreeForShop(ComboTreeBean node,TreeVO<Shop> shop){
	//	    if(shop!=null && shop.getObject()!= null){
	//	    	node.setId(String.valueOf(shop.getObject().getId().toString()));
	//	    	node.setText(shop.getObject().getShopName());
	//	    	node.setShopType(shop.getObject().getType().getChannelTypeCode());
	//	    	node.setIconCls(shop.getObject().getType().getChannelTypeCode());
	//		    List<ComboTreeBean> lstChild = new ArrayList<ComboTreeBean>();
	//		    if(shop.getListChildren()!= null && shop.getListChildren().size() > 0){
	//				for(int i=0;i<shop.getListChildren().size();i++){
	//					ComboTreeBean subBean = new ComboTreeBean();  
	//				    subBean = loadComboTreeForShop(subBean,shop.getListChildren().get(i));
	//				    lstChild.add(subBean);
	//				}
	//				node.setChildren(lstChild);				
	//				node.setState(ConstantManager.JSTREE_STATE_CLOSE);
	//		    }
	//	    } 
	//	    return node;	    
	//	}
	//	/**
	//	 * Gets the unit tree combo tree ex1.
	//	 *
	//	 * @param type the type
	//	 * @param currentNode the current node
	//	 * @param shopId the shop id
	//	 * @return the unit tree combo tree ex1
	//	 * @author lamnh
	//	 * @since Oct 30, 2012
	//	 */
	//	@RequestMapping(value = "/unit-tree-ex1/combotree/{type}/{currentNode}/{shopId}", method = RequestMethod.POST)
	//	public @ResponseBody List<ComboTreeBean> getUnitTreeComboTreeEx1(@PathVariable("type") int type, @PathVariable("currentNode") Long currentNode,@PathVariable("shopId") Long shopId){	    
	//	    List<ComboTreeBean> lstShopTree = new ArrayList<ComboTreeBean>();	
	//	    ComboTreeBean nullBean = new ComboTreeBean();
	//	    nullBean.setId("-1");
	//	    nullBean.setState("open");
	//	    nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.tree.choose.parent"));
	//	    lstShopTree.add(nullBean);
	//		try{
	//		    ShopMgr shopMgr = (ShopMgr)context.getBean("shopMgr");		    
	//		    Shop shop = null;
	//			 StaffMgr staffMgr = (StaffMgr)context.getBean("staffMgr");
	//			 if(staffMgr!= null){
	//				 ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	//				 HttpSession session =  attr.getRequest().getSession(true);
	//				 UserToken currentUser = null;
	//				 if(session.getAttribute("cmsUserToken")!= null){
	//					 currentUser = (UserToken)session.getAttribute("cmsUserToken");						
	//					 if(currentUser!= null){
	//						 Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
	//						 if(staff!= null){
	//						    shop = staff.getShop();
	//						  }
	//					 }
	//				 }
	//			 }
	//		    if(shop!=null){
	//		    	if(shop.getParentShop()!=null && shop.getParentShop().getId().equals(currentNode)){
	//		    		ComboTreeBean node = new ComboTreeBean();
	//			    	node = loadComboTreeForShopEx(node, shop.getParentShop());	
	//			    	node.setState(ConstantManager.JSTREE_STATE_OPEN);
	//	    			lstShopTree.add(node);
	//	    			return lstShopTree;
	//		    	}
	//		    }
	//		    if(currentNode.equals(shopId)){
	//		    	ComboTreeBean node = new ComboTreeBean();
	//		    	node = loadComboTreeForShopEx(node, shopMgr.getShopById(shopId));	
	//		    	node.setState(ConstantManager.JSTREE_STATE_CLOSE);
	//    			lstShopTree.add(node);
	//    			return lstShopTree;
	//			}
	//		    HashMap<Long, Long> list = this.nodeOpen(currentNode, shopId);
	//		    if(shopMgr!= null){
	//	    		TreeVO<Shop> shopTreeVO = shopMgr.getAncestor(currentNode,shopId);
	//	    		if(shopTreeVO.getObject() == null && shopTreeVO.getListChildren() != null){
	//	    			for(int i=0;i<shopTreeVO.getListChildren().size();i++){
	//	    				ComboTreeBean tmp = new ComboTreeBean();
	//	    				tmp = loadComboTreeForShop2(tmp, shopTreeVO.getListChildren().get(i),list);
	//	    				lstShopTree.add(tmp);
	//	    			}
	//	    		}else{
	//	    			ComboTreeBean bean = new ComboTreeBean();	    			
	//	    			bean = loadComboTreeForShop2(bean, shopTreeVO,list);	    			
	//	    			lstShopTree.add(bean);
	//	    		}
	//		    }
	//		   
	//		}catch (Exception e) {
	//		    LogUtility.logError(e,e.getMessage());
	//		}
	//		return lstShopTree;
	//	}
	//	/**
	//	 * Load combo tree for shop2.
	//	 *
	//	 * @param node the node
	//	 * @param shop the shop
	//	 * @param list the list
	//	 * @return the combo tree bean
	//	 * @author tientv
	//	 * @since
	//	 */
	//	private ComboTreeBean loadComboTreeForShop2(ComboTreeBean node,TreeVO<Shop> shop,HashMap<Long, Long> list){
	//	    if(shop!=null && shop.getObject()!= null){
	//	    	node.setId(String.valueOf(shop.getObject().getId().toString()));
	//	    	node.setText(shop.getObject().getShopName());
	//	    	if(shop.getObject().getType()!=null){
	//	    		node.setShopType(shop.getObject().getType().getChannelTypeCode());
	//	    		node.setIconCls(shop.getObject().getType().getChannelTypeCode());
	//	    	}
	//		    List<ComboTreeBean> lstChild = new ArrayList<ComboTreeBean>();		    	
	//		    if(shop.getListChildren()!= null && shop.getListChildren().size() > 0){
	//				for(int i=0;i<shop.getListChildren().size();i++){
	//					ComboTreeBean subBean = new ComboTreeBean();  
	//				    subBean = loadComboTreeForShop2(subBean,shop.getListChildren().get(i),list);
	//				    if(list.get(Long.valueOf(subBean.getId()))!=null){
	//				    	subBean.setState(ConstantManager.JSTREE_STATE_OPEN);
	//					}else{
	//						subBean.setState(ConstantManager.JSTREE_STATE_CLOSE);
	//					}
	//				    lstChild.add(subBean);
	//				}
	//				node.setChildren(lstChild);	
	//		    }else{
	//		    	node.setState(ConstantManager.JSTREE_STATE_CLOSE);
	//		    }
	//	    }
	//	    return node;	    
	//	}
	/***************************** CAY DIA BAN OLD VERSION ********************************/
	/*
	 * Load combo tree for area ex.
	 * 
	 * @param node the node
	 * 
	 * @param area the area
	 */
	private ComboTreeBean loadComboTreeForAreaEx(ComboTreeBean node, Area area) {
		if (area != null) {
			node.setId(String.valueOf(area.getId().toString()));
			node.setText(area.getAreaCode() + "-" + area.getAreaName());
			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			node.setChildren(new ArrayList<ComboTreeBean>());
		}
		return node;
	}

	/*
	 * Load combo tree for area.
	 * 
	 * @param node the node
	 * 
	 * @param area the area
	 * 
	 * @author lamnh
	 * 
	 * @since Oct 1, 2012
	 */
	private ComboTreeBean loadComboTreeForArea(ComboTreeBean node, TreeVO<Area> area) {
		if (area != null && area.getObject() != null) {
			node.setId(String.valueOf(area.getObject().getId().toString()));
			node.setText(area.getObject().getAreaName());
			List<ComboTreeBean> lstChild = new ArrayList<ComboTreeBean>();
			if (area.getListChildren() != null && area.getListChildren().size() > 0) {
				for (int i = 0; i < area.getListChildren().size(); i++) {
					ComboTreeBean subBean = new ComboTreeBean();
					subBean = loadComboTreeForArea(subBean, area.getListChildren().get(i));
					lstChild.add(subBean);
				}
				node.setChildren(lstChild);
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
			}

		}
		return node;
	}

	/*
	 * Load combo tree for area2.
	 * 
	 * @param node the node
	 * 
	 * @author tientv
	 * 
	 * @since
	 */
	private ComboTreeBean loadComboTreeForArea2(ComboTreeBean node, TreeVO<Area> area) {
		if (area != null && area.getObject() != null) {
			node.setId(String.valueOf(area.getObject().getId().toString()));
			node.setText(area.getObject().getAreaCode() + "-" + area.getObject().getAreaName());
			List<ComboTreeBean> lstChild = new ArrayList<ComboTreeBean>();
			if (area.getListChildren() != null && area.getListChildren().size() > 0) {
				node.setState(ConstantManager.JSTREE_STATE_CLOSE);
				node.setChecked(false);
			} else {
				//Lay all cac thang cap cha		    	
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setChecked(true);
			}
		}
		return node;
	}

	/*
	 * Gets the area tree combo tree.
	 * 
	 * @param type the type
	 * 
	 * @param currentNode the current node
	 * 
	 * @return the area tree combo tree
	 * 
	 * @author lamnh
	 * 
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/area-tree/combotree/{type}/{currentNode}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getAreaTreeComboTree(@PathVariable("type") int type, @PathVariable("currentNode") Long currentNode) {
		List<ComboTreeBean> lstAreaTree = new ArrayList<ComboTreeBean>();
		ComboTreeBean nullBean = new ComboTreeBean();
		nullBean.setId("-1");
		nullBean.setState("open");
		nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.tree.choose.parent"));
		lstAreaTree.add(nullBean);
		try {
			AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
			Area currentArea = null;
			if (areaMgr != null) {
				TreeVO<Area> areaTreeVO = null;
				if (type == 1) {
					areaTreeVO = areaMgr.getAreaTreeVOEx(null, currentNode, ActiveType.RUNNING);
					currentArea = areaMgr.getAreaById(currentNode);
					if (currentArea != null && currentArea.getParentArea() != null) {
						TreeVO<Area> parentAreaTreeVo = new TreeVO<Area>();
						parentAreaTreeVo.setListChildren(new ArrayList<TreeVO<Area>>());
						parentAreaTreeVo.setObject(currentArea.getParentArea());
						areaTreeVO.getListChildren().add(parentAreaTreeVo);
					}
				} else {
					areaTreeVO = areaMgr.getAreaTreeVOEx(null, null, ActiveType.RUNNING);
				}
				if (areaTreeVO.getObject() == null && areaTreeVO.getListChildren() != null) {
					for (int i = 0; i < areaTreeVO.getListChildren().size(); i++) {
						ComboTreeBean tmp = new ComboTreeBean();
						tmp = loadComboTreeForArea(tmp, areaTreeVO.getListChildren().get(i));
						lstAreaTree.add(tmp);
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					bean = loadComboTreeForArea(bean, areaTreeVO);
					lstAreaTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstAreaTree;
	}

	/*
	 * Gets the area tree combo tree.
	 * 
	 * @param type the type
	 * 
	 * @param view the view
	 * 
	 * @param currentNode the current node
	 * 
	 * @param areaId the area id
	 * 
	 * @return the area tree combo tree
	 * 
	 * @author lamnh,tientv
	 * 
	 * @since Oct 1, 2012
	 */
	@RequestMapping(value = "/area-tree-ex/combotree/{type}/{view}/{currentNode}/{areaId}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getAreaTreeComboTreeEx(@PathVariable("type") int type, @PathVariable("view") int view, @PathVariable("currentNode") Long currentNode, @PathVariable("areaId") Long areaId) {
		List<ComboTreeBean> lstAreaTree = new ArrayList<ComboTreeBean>();
		ComboTreeBean nullBean = new ComboTreeBean();
		try {
			AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
			Area currentArea = null;
			if (areaMgr != null) {
				List<Area> areaTreeVO = null;
				if (currentNode != null && currentNode == 0) {
					currentNode = null;
					nullBean.setId("-1");
					nullBean.setState("open");
					nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.tree.choose.parent"));
					lstAreaTree.add(nullBean);
				}
				if (type == 1) {
					areaTreeVO = areaMgr.getListSubArea(currentNode, ActiveType.RUNNING, true);
					currentArea = areaMgr.getAreaById(currentNode);
					if (currentArea != null && currentArea.getParentArea() != null) {
						areaTreeVO.add(currentArea.getParentArea());
					}
				} else {
					areaTreeVO = areaMgr.getListSubAreaEx(currentNode, ActiveType.RUNNING, areaId);
				}
				if (areaTreeVO != null) {
					if (view == 1) {
						ComboTreeBean node = new ComboTreeBean();
						Area area = areaMgr.getAreaById(currentNode);
						node = loadComboTreeForAreaEx(node, area);
						List<ComboTreeBean> child = new ArrayList<ComboTreeBean>();
						for (int i = 0; i < areaTreeVO.size(); i++) {
							ComboTreeBean tmp = new ComboTreeBean();
							tmp = loadComboTreeForAreaEx(tmp, areaTreeVO.get(i));
							child.add(tmp);
						}
						node.setChildren(child);
						lstAreaTree.add(node);
					} else {
						for (int i = 0; i < areaTreeVO.size(); i++) {
							ComboTreeBean tmp = new ComboTreeBean();
							tmp = loadComboTreeForAreaEx(tmp, areaTreeVO.get(i));
							lstAreaTree.add(tmp);
						}
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					bean = loadComboTreeForAreaEx(bean, null);
					lstAreaTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstAreaTree;
	}

	/*
	 * Gets the area tree combo tree ex1.
	 * 
	 * @param type the type
	 * 
	 * @param currentNode the current node
	 * 
	 * @return the area tree combo tree ex1
	 * 
	 * @author lamnh
	 * 
	 * @since Oct 30, 2012
	 */
	@RequestMapping(value = "/area-tree-ex1/combotree/{type}/{currentNode}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getAreaTreeComboTreeEx1(@PathVariable("type") int type, @PathVariable("currentNode") Long currentNode) {
		List<ComboTreeBean> lstAreaTree = new ArrayList<ComboTreeBean>();
		//	    ComboTreeBean nullBean = new ComboTreeBean();
		//	    nullBean.setId("-1");
		//	    nullBean.setState("open");
		//	    nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.tree.choose.parent"));
		//	    lstAreaTree.add(nullBean);
		try {
			AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
			if (areaMgr != null) {
				//Danh sach nut se mo ra khi chon
				//HashMap<Long, Long> hash = this.openNodeArea(currentNode);
				/*
				 * if(hash.size()==0){ //Neu nut dang chon la nut goc, thi lay
				 * danh sach cac nut goc ra hien thi List<Area> list_parent =
				 * areaMgr.getListSubArea(null, ActiveType.RUNNING,true);
				 * for(Area obj:list_parent){ ComboTreeBean com = new
				 * ComboTreeBean(); com.setId(String.valueOf(obj.getId()));
				 * com.setText(obj.getAreaCode()+"-"+obj.getAreaName());
				 * com.setState(ConstantManager.JSTREE_STATE_CLOSE);
				 * lstAreaTree.add(com); } return lstAreaTree; }
				 */
				Area selectArea = areaMgr.getAreaById(currentNode);
				TreeVO<Area> shopAreaVO = areaMgr.getAreaTreeVOEx(selectArea, null, ActiveType.RUNNING);
				if (shopAreaVO.getListChildren() != null) {
					for (int i = 0; i < shopAreaVO.getListChildren().size(); i++) {
						ComboTreeBean tmp = new ComboTreeBean();
						//	    				tmp = loadComboTreeForArea2(tmp, shopAreaVO.getListChildren().get(i),hash);
						tmp = loadComboTreeForArea2(tmp, shopAreaVO.getListChildren().get(i));
						lstAreaTree.add(tmp);
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					//	    			bean = loadComboTreeForArea2(bean, shopAreaVO,hash);
					bean = loadComboTreeForArea2(bean, shopAreaVO);
					lstAreaTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstAreaTree;
	}

	/*
	 * *********************************************************************************
	 * VERSION DMS-HO
	 * ***********************************************************
	 * **********************
	 */
	/***************************** COMBO TREE *********************************************/
	private Shop shopUserLogin;
	private Staff userLogin;
	private Shop shopGSNPP;

	/**
	 * @see CAY DIA BAN
	 */
	@RequestMapping(value = "/area/combotree/{title}/{areaId}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getAreaComboTree(@PathVariable("title") Integer title, @PathVariable("areaId") Long areaId) {
		List<ComboTreeBean> lstAreaTree = new ArrayList<ComboTreeBean>();
		ComboTreeBean nullBean = new ComboTreeBean();
		if (title != null && title > 0) {
			nullBean.setId("-2");
			nullBean.setState("open");
			nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area.common.tree.choose"));
			lstAreaTree.add(nullBean);
		}
		List<Area> areaTreeVO = null;
		try {
			AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
			if (areaId > 0) {
				areaTreeVO = areaMgr.getListSubAreaEx(areaId, ActiveType.RUNNING, null);
			} else {
				areaTreeVO = areaMgr.getListSubAreaEx(null, ActiveType.RUNNING, null);
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		if (areaTreeVO != null) {
			for (int i = 0; i < areaTreeVO.size(); i++) {
				ComboTreeBean tmp = new ComboTreeBean();
				tmp = loadComboTreeForArea(tmp, areaTreeVO.get(i));
				lstAreaTree.add(tmp);
			}
		} else {
			ComboTreeBean bean = new ComboTreeBean();
			lstAreaTree.add(bean);
		}

		return lstAreaTree;
	}

	private ComboTreeBean loadComboTreeForArea(ComboTreeBean node, Area area) {
		try {
			if (area != null) {
				ComboTreeBeanAttr comboTreeBeanAttr = new ComboTreeBeanAttr();
				comboTreeBeanAttr.setArea(area);

				node.setId(String.valueOf(area.getId().toString()));
				//		    	node.setText(StringUtil.CodeAddName(area.getAreaCode(), area.getAreaName()));
				node.setText(area.getAreaName());
				node.setAttributes(comboTreeBeanAttr);

				AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
				List<Area> areaTreeVO = areaMgr.getListSubAreaEx(area.getId(), ActiveType.RUNNING, null);
				if (areaTreeVO != null && areaTreeVO.size() > 0) {
					node.setState(ConstantManager.JSTREE_STATE_CLOSE);
				} else {
					node.setState(ConstantManager.JSTREE_STATE_LEAF);
					node.setIconCls("triconline");
				}
				node.setChildren(new ArrayList<ComboTreeBean>());
			}
		} catch (Exception e) {
		}
		return node;
	}

	@RequestMapping(value = "/area/combotree/{areaId}/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Long> getListAreaOfComboTreeByAreaId(@PathVariable("areaId") Long areaId) {
		List<Long> lstAreaId = new ArrayList<Long>();
		try {
			AreaMgr areaMgr = (AreaMgr) context.getBean("areaMgr");
			if (areaMgr != null) {
				Area area = areaMgr.getAreaById(areaId);
				recursionArea(lstAreaId, area);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstAreaId;
	}

	private void recursionArea(List<Long> lstAreaId, Area area) {
		if (area != null) {
			lstAreaId.add(area.getId());
			recursionArea(lstAreaId, area.getParentArea());
		}
	}

	/**
	 * @see CAY DON VI
	 */
	@RequestMapping(value = "/shop/combotree/{title}/{type}/{shopId}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getShopComboTree(@PathVariable("title") Integer title, @PathVariable("type") int type, @PathVariable("shopId") Long shopId) {

		/*ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");

		List<ComboTreeBean> lstShopTree = new ArrayList<ComboTreeBean>();
		ComboTreeBean nullBean = new ComboTreeBean();
	/*	if (title != null && title > 0) {
			nullBean.setId("-2");
			nullBean.setState("open");
			nullBean.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "shop.common.tree.choose"));
			lstShopTree.add(nullBean);
		}
		Shop currentShop = null;
		List<Shop> shopTreeVO = null;
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		UserToken currentUserToken = (UserToken) session.getAttribute("cmsUserToken");
		if (currentUserToken != null && currentUserToken.getShopRoot() != null) {
			try {
				currentShop = shopMgr.getShopById(currentUserToken.getShopRoot().getShopId());
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		if (currentShop != null) {
			Shop shopSelect = null;
			if (type == COMBOTREE_ONE_NODE) {
				ComboTreeBean tmp = new ComboTreeBean();
				tmp = loadComboTreeForShop(tmp, currentShop);
				lstShopTree.add(tmp);
			} else {
				try {
					shopSelect = shopMgr.getShopById(shopId);
					shopTreeVO = shopMgr.getListSubShopEx(shopId, ActiveType.RUNNING, null);
				} catch (Exception e) {
					LogUtility.logError(e, e.getMessage());
				}

				if (shopTreeVO != null) {
					ComboTreeBean tmp = null;
					Shop shopTmp = null;
					if (ShopObjectType.VNM.getValue().equals(shopSelect.getType().getObjectType())) {
						Integer objType = null;
						for (int i = 0, sz = shopTreeVO.size(); i < sz; i++) {
							tmp = new ComboTreeBean();
							shopTmp = shopTreeVO.get(i);
							objType = shopTmp.getType().getObjectType();
							if (ShopObjectType.GT.getValue().equals(objType) || ShopObjectType.KA.getValue().equals(objType) || ShopObjectType.ST.getValue().equals(objType)) {
								tmp = loadComboTreeForShop(tmp, shopTmp);
								lstShopTree.add(tmp);
							}
						}
					} else {
						for (int i = 0, sz = shopTreeVO.size(); i < sz; i++) {
							tmp = new ComboTreeBean();
							shopTmp = shopTreeVO.get(i);
							if (shopTmp.getType().getObjectType().equals(shopSelect.getType().getObjectType() + 1)) {
								tmp = loadComboTreeForShop(tmp, shopTmp);
								lstShopTree.add(tmp);
							}
						}
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					lstShopTree.add(bean);
				}
			}
		}
		return lstShopTree;*/
		return null;
	}

	private ComboTreeBean loadComboTreeForShop(ComboTreeBean node, Shop shop) {
		/*try {
			if (shop != null) {
				ComboTreeBeanAttr comboTreeBeanAttr = new ComboTreeBeanAttr();
				comboTreeBeanAttr.setShop(shop);

				node.setId(String.valueOf(shop.getId().toString()));
				node.setText(StringUtil.CodeAddName(shop.getShopCode(), shop.getShopName()));
				node.setAttributes(comboTreeBeanAttr);

				
				 * ShopMgr shopMgr = (ShopMgr)context.getBean("shopMgr");
				 * List<Shop> shopTreeVO =
				 * shopMgr.getListSubShopEx(shop.getId(),
				 * ActiveType.RUNNING,null); if(shopTreeVO!=null &&
				 * shopTreeVO.size()>0){
				 * node.setState(ConstantManager.JSTREE_STATE_CLOSE); }else{
				 * node.setState(ConstantManager.JSTREE_STATE_LEAF);
				 * node.setIconCls("triconline"); }
				 
				if (!ShopObjectType.NPP.getValue().equals(shop.getType().getObjectType()) && !ShopObjectType.NPP_KA.getValue().equals(shop.getType().getObjectType()) && !ShopObjectType.MIEN_ST.getValue().equals(shop.getType().getObjectType())) {
					node.setState(ConstantManager.JSTREE_STATE_CLOSE);
				} else {
					node.setState(ConstantManager.JSTREE_STATE_LEAF);
					node.setIconCls("triconline");
				}
				node.setChildren(new ArrayList<ComboTreeBean>());
			}
		} catch (Exception e) {
		}
		return node;*/
		return null;
	}

	/**
	 * @see CAY NGANH HANG
	 */
	@RequestMapping(value = "/group-po/tree/{type}", method = RequestMethod.GET)
	public @ResponseBody
	List<ComboTreeBean> getListProductOfGroupPOAuto(@RequestParam("code") String code, @RequestParam("name") String name, @RequestParam("id") Long id, @PathVariable("type") Integer type) {
		List<ComboTreeBean> lstCat = new ArrayList<ComboTreeBean>();
		try {
			ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
			if (productMgr != null) {
				List<ProductTreeVO> lstPro = null;
				if (type == 0) {
					lstPro = productMgr.getListProductTreeVO(code, name, id, ProgramObjectType.GROUP_PO, true, null);
				} else if (type == 1) {
					lstPro = productMgr.getListProductTreeVO(code, name, id, ProgramObjectType.INCENTIVE_PROGRAM, true, null);
				} else if (type == 2) {
					lstPro = productMgr.getListProductTreeVO(code, name, id, ProgramObjectType.FOCUS_PROGRAM, true, null);
				} else if (type == 3) {
					lstPro = productMgr.getListProductTreeVO(code, name, id, ProgramObjectType.EQUIPMENT, true, null);
				} else if (type == 4) {
					lstPro = productMgr.getListProductTreeVO(code, name, id, ProgramObjectType.GROUP_PO_NPP, true, null);
				}
				loadProductTreeCommon(lstPro, lstCat, code, name);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstCat;
	}

	private ComboTreeBean loadComboTreeForProduct(ComboTreeBean node, ProductTreeVO productTreeVO) {
		try {
			if (productTreeVO != null) {
				ComboTreeBeanAttr comboTreeBeanAttr = new ComboTreeBeanAttr();
				comboTreeBeanAttr.setProductTreeVO(productTreeVO);

				if (productTreeVO != null && null != productTreeVO.getParentId())
					//node.setId(String.valueOf(productTreeVO.getId().toString()+"_"+productTreeVO.getParentId().toString()));
					//SangTN: Id not contains string _, exception in here
					node.setId(String.valueOf(productTreeVO.getId().toString()));
				else
					node.setId(String.valueOf(productTreeVO.getId().toString()));
				node.setText(productTreeVO.getName());
				node.setAttributes(comboTreeBeanAttr);
			}
		} catch (Exception e) {
		}
		return node;
	}

	/***************************** TREE *******************************************************/
	/**
	 * @see CAY TUYEN
	 * 
	 * @author hunglm16
	 * @since October 9,2014
	 * @description Cap nhat theo CMS
	 */
	@RequestMapping(value = "/routing/{shopCode}/tree", method = RequestMethod.POST)
	public @ResponseBody
	List<TreeBean> getRoutingTree(@PathVariable("shopCode") String shopCode) {
		List<TreeBean> lstRoutingTree = new ArrayList<TreeBean>();
		List<TreeBean> lstRun = new ArrayList<TreeBean>();
		List<TreeBean> lstStop = new ArrayList<TreeBean>();
		TreeBean runRoot = new TreeBean();
		TreeBean stopRoot = new TreeBean();
		try {
			SuperviserMgr superMgr = (SuperviserMgr) context.getBean("superviserMgr");
			ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
			if (superMgr != null) {

				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
				HttpSession session = attr.getRequest().getSession(true);

				UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
				Long shopId = null;
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					Shop shop = shopMgr.getShopByCode(shopCode);
					if (shop != null) {
						shopId = shop.getId();
					}
				}
				if (shopId == null) {
					shopId = currentUser.getShopRoot().getShopId();
				}
				List<RoutingTreeVO> routingTreeVO = superMgr.getListRoutingShopIdNew(shopId);

				if (routingTreeVO != null) {
					runRoot.setId("-1");
					runRoot.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.name"));
					runRoot.setState(ConstantManager.JSTREE_STATE_OPEN);
					stopRoot.setId("-1");
					stopRoot.setText(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "pause.status.name"));
					stopRoot.setState(ConstantManager.JSTREE_STATE_OPEN);
					for (int i = 0; i < routingTreeVO.size(); i++) {
						TreeBean tmp = new TreeBean();
						//tmp = loadTreeForRoutingNew(tmp, routingTreeVO.get(i));
						tmp = loadTreeForRoutingVO(tmp, routingTreeVO.get(i));
						if (ActiveType.RUNNING.getValue().equals(routingTreeVO.get(i).getStatus())) {
							lstRun.add(tmp);
						} else {
							lstStop.add(tmp);
						}
					}
					runRoot.setChildren(lstRun);
					stopRoot.setChildren(lstStop);
					lstRoutingTree.add(runRoot);
					lstRoutingTree.add(stopRoot);
				} else {
					TreeBean bean = new TreeBean();
					lstRoutingTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstRoutingTree;
	}

	private TreeBean loadTreeForRouting(TreeBean node, RoutingVO routingVO) {
		try {
			if (routingVO != null) {
				node.setId(String.valueOf(routingVO.getRoutingId()));
				node.setText(StringUtil.CodeAddName(routingVO.getRoutingCode(), routingVO.getRoutingName()));
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setChildren(new ArrayList<TreeBean>());
			}
		} catch (Exception e) {
		}
		return node;
	}

	private TreeBean loadTreeForRoutingNew(TreeBean node, Routing routing) {
		try {
			if (routing != null) {
				node.setId(String.valueOf(routing.getId()));
				node.setText(StringUtil.CodeAddName(routing.getRoutingCode(), routing.getRoutingName()));
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setChildren(new ArrayList<TreeBean>());
			}
		} catch (Exception e) {
		}
		return node;
	}

	private TreeBean loadTreeForRoutingVO(TreeBean node, RoutingTreeVO routingTreeVO) {
		try {
			if (routingTreeVO != null) {
				node.setId(String.valueOf(routingTreeVO.getId()));
				node.setText(StringUtil.CodeAddName(routingTreeVO.getRoutingCode(), routingTreeVO.getRoutingName()));
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
				node.setChildren(new ArrayList<TreeBean>());
			}
		} catch (Exception e) {
		}
		return node;
	}

	/*****************************************************************************************/
	@RequestMapping(value = "/check-system-time", method = RequestMethod.GET)
	public @ResponseBody
	StatusBean checkSystemTime(@RequestParam("time") String time) {
		StatusBean statusBean = new StatusBean();
		Date dTime = DateUtil.parse(time, DateUtil.DATE_FORMAT_DDMMYYYY);
		if (DateUtil.compareDateWithoutTime(DateUtil.now(), dTime) > -1) {
			statusBean.setValue(Long.valueOf(0));
		} else {
			statusBean.setValue(Long.valueOf(1));
		}
		return statusBean;
	}

	@RequestMapping(value = "/product/tree-grid/{displayGroupType}", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getListProductTreeGrid(@RequestParam("code") String code, @RequestParam("name") String name, @RequestParam("id") Long id, @RequestParam("type") String type, @RequestParam("parentId") Long parentId,
			@RequestParam("typeObject") Integer typeObject, @RequestParam("isPromotionProduct") Boolean isPromotionProduct, @PathVariable Integer displayGroupType) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<ComboTreeBean> lstCat = new ArrayList<ComboTreeBean>();
		try {
			ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
			if (productMgr != null) {
				Boolean flag = true;
				if (isPromotionProduct != null && isPromotionProduct) {
					flag = false;
				}
				TreeVOType a = null;
				ProgramObjectType objectType = null;
				DisplayProductGroupType displayProductGroupType = null;
				if (DisplayProductGroupType.DS.getValue().equals(displayGroupType)) {
					displayProductGroupType = DisplayProductGroupType.SKU;
				} else if (DisplayProductGroupType.SKU.getValue().equals(displayGroupType)) {
					displayProductGroupType = DisplayProductGroupType.DS;
				}
				if (ProgramObjectType.DISPLAY_PROGRAM.getValue().equals(typeObject)) {
					objectType = ProgramObjectType.DISPLAY_PROGRAM;
				} else if (ProgramObjectType.PROMOTION_PROGRAM.getValue().equals(typeObject)) {
					objectType = ProgramObjectType.PROMOTION_PROGRAM;
				}
				if (!StringUtil.isNullOrEmpty(type)) {
					if (type.equals("CATEGORY")) {
						a = TreeVOType.CATEGORY;
					} else if (type.equals("SUB_CATEGORY")) {
						a = TreeVOType.SUB_CATEGORY;
					} else {
						a = TreeVOType.PRODUCT;
					}
				}
				List<ProductTreeVO> lstPro = null;
				if (!StringUtil.isNullOrEmpty(code) || !StringUtil.isNullOrEmpty(name)) {
					lstPro = productMgr.getListProductTreeVO(code, name, id, objectType, flag, displayProductGroupType);
					loadProductTreeCommon(lstPro, lstCat, code, name);
				} else {
					lstPro = productMgr.getListProductTreeVOByParentNode(id, objectType, parentId, a, flag, displayProductGroupType);
					loadProductTreeCommonDemo(lstPro, lstCat, code, name, type, parentId);
				}
				result.put("rows", lstCat);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return result;
	}

	private void loadProductTreeCommon(List<ProductTreeVO> lstPro, List<ComboTreeBean> lstCat, String code, String name) {
		if (lstPro != null) {
			for (int i = 0, m = lstPro.size(); i < m; i++) {
				ProductTreeVO p = new ProductTreeVO();
				p = lstPro.get(i);
				ComboTreeBean cat = new ComboTreeBean();
				cat = loadComboTreeForProduct(cat, p);
				List<ComboTreeBean> lstSubCat = new ArrayList<ComboTreeBean>();
				if (p.getListChildren() != null && p.getListChildren().size() > 0) {
					for (int j = 0, n = p.getListChildren().size(); j < n; j++) {
						ProductTreeVO p1 = new ProductTreeVO();
						p1 = p.getListChildren().get(j);
						ComboTreeBean subCat = new ComboTreeBean();
						subCat = loadComboTreeForProduct(subCat, p1);
						List<ComboTreeBean> lstProduct = new ArrayList<ComboTreeBean>();
						if (p1.getListChildren() != null && p1.getListChildren().size() > 0) {
							for (int k = 0, q = p1.getListChildren().size(); k < q; k++) {
								ProductTreeVO p2 = new ProductTreeVO();
								p2 = p1.getListChildren().get(k);
								ComboTreeBean product = new ComboTreeBean();
								product = loadComboTreeForProduct(product, p2);
								lstProduct.add(product);
							}
							subCat.setChildren(lstProduct);
							if (StringUtil.isNullOrEmpty(code) && StringUtil.isNullOrEmpty(name)) {
								subCat.setState(ConstantManager.JSTREE_STATE_CLOSE);
							} else {
								subCat.setState(ConstantManager.JSTREE_STATE_OPEN);
							}
						} else {
							subCat.setState(ConstantManager.JSTREE_STATE_OPEN);
						}
						lstSubCat.add(subCat);
					}
				}
				cat.setChildren(lstSubCat);
				if (StringUtil.isNullOrEmpty(code) && StringUtil.isNullOrEmpty(name)) {
					cat.setState(ConstantManager.JSTREE_STATE_CLOSE);
				} else {
					cat.setState(ConstantManager.JSTREE_STATE_OPEN);
				}
				lstCat.add(cat);
			}
		} else {
			ComboTreeBean bean = new ComboTreeBean();
			lstCat.add(bean);
		}
	}

	private void loadProductTreeCommonDemo(List<ProductTreeVO> lstPro, List<ComboTreeBean> lstCat, String code, String name, String type, Long parentId) {
		if (lstPro != null) {
			for (int i = 0, m = lstPro.size(); i < m; i++) {
				ProductTreeVO p = new ProductTreeVO();
				p = lstPro.get(i);
				ComboTreeBean cat = new ComboTreeBean();
				cat = loadComboTreeForProduct(cat, p);
				if (!TreeVOType.PRODUCT.equals(p.getType())) {
					cat.setState(ConstantManager.JSTREE_STATE_CLOSE);
				}
				lstCat.add(cat);
			}
		} else {
			ComboTreeBean bean = new ComboTreeBean();
			lstCat.add(bean);
		}
	}

	/*************** CTTB 2013 *****************************/
	/**
	 * @author loctt
	 * @since 20sep2013
	 * @param levelId
	 * @return
	 */
	@RequestMapping(value = "/display-program/level/detail/{levelId}", method = RequestMethod.GET)
	public @ResponseBody
	List<StDisplayPlAmtDtl> getDisplayProgramLevelDTLByLevelId(@PathVariable("levelId") Long levelId) {
		List<StDisplayPlAmtDtl> lstDetail = new ArrayList<StDisplayPlAmtDtl>();
		if (levelId != null) {
			try {
				DisplayProgrameMgr displayProgramMgr = (DisplayProgrameMgr) context.getBean("displayProgrameMgr");
				if (displayProgramMgr != null) {
					lstDetail = displayProgramMgr.getListDisplayProgramLevelDetailByLevelId(levelId);
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return lstDetail;
	}

	/**
	 * Lay ds nhan vien quan ly
	 * 
	 * @author tungmt
	 * @since Sep 30, 2014
	 */
	@RequestMapping(value = "/staff-unit-tree/kendo-ui-combobox", method = RequestMethod.GET)
	public @ResponseBody
	List<StaffVO> getListShop4KendoHO1() throws BusinessException {
		try {
			ObjectVO<StaffVO> lstStaff = null;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");

			if (currentUser != null) {
				StaffMgr staffMgr = (StaffMgr) context.getBean("staffMgr");
				StaffFilter filter = new StaffFilter();
				filter.setStatus(ActiveType.RUNNING);
				lstStaff = staffMgr.getListStaffVO(filter);
				if (lstStaff != null && lstStaff.getLstObject() != null) {
					return lstStaff.getLstObject();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ReportController.getListShop4KendoHO()" + e.getMessage());
		}
		return new ArrayList<StaffVO>();
	}
}
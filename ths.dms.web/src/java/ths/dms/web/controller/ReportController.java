/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import viettel.passport.client.ShopToken;
import viettel.passport.client.UserToken;

import ths.dms.core.business.CmsMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.EquipmentManagerMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.enumtype.EquipGFilter;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.PermissionType;
import ths.dms.core.entities.enumtype.ProductInfoObjectType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.CmsVO;
import ths.dms.core.entities.vo.EquipmentGroupVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PromotionProgramVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.TreeVOBasic;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.bean.ComboTreeBean;
import ths.dms.web.bean.JsTreeNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.SaleMTReportTemplate;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;

/**
 * Controler dac thu cho cac man hinh bao cao
 * 
 * @author hunglm16
 * @since 19/09/2015
 */
@Controller
@RequestMapping("/report")
public class ReportController {
	/** The context. */
	private final ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext-service.xml");
	private final int COMBOTREE_ONE_NODE = 1;

	@RequestMapping(value = "/progamram_promotion/kendoui-combobox/{status}/{shopId}", method = RequestMethod.GET)
	public @ResponseBody
	List<PromotionProgramVO> getPromotionProgramByShop(@PathVariable("status") Integer status, @PathVariable("shopId") Long shopId) {
		List<PromotionProgramVO> lstData = new ArrayList<PromotionProgramVO>();
		try {
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
			CommonMgr cmsMgr = (CommonMgr) context.getBean("commonMgr");
			if (status != null && status < ActiveType.STOPPED.getValue()) {
				status = null;
			}
			lstData = cmsMgr.getListVOPromotionProgramByShop(status, shopId, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstData;
	}

	/**
	 * Lay danh sach nganh hang
	 * 
	 * @author hunglm16
	 * @param status
	 * @param shopId
	 * @return
	 * @since October 07,2015
	 */
	@RequestMapping(value = "/product-info-cat/kendoui-combobox/{status}/{shopId}", method = RequestMethod.GET)
	public @ResponseBody
	List<ProductInfo> getLstProductInforForJSON(@PathVariable("status") Integer status, @PathVariable("shopId") Long shopId) {
		List<ProductInfo> lstData = new ArrayList<ProductInfo>();
		try {
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
			if (currentUser == null) {
				return lstData;
			}
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			
			ActiveType statusA = ActiveType.RUNNING;
			if (status != null && status > ActiveType.DELETED.getValue()) {
				statusA = ActiveType.parseValue(status);
			}
			List<ProductInfoObjectType> objTypes = new ArrayList<ProductInfoObjectType>();
			objTypes.add(ProductInfoObjectType.SP);
			boolean orderByCode = true;
			ObjectVO<ProductInfo> lstCategoryTmp = productInfoMgr.getListProductInfoEx(null, null, null, statusA, ProductType.CAT, objTypes, orderByCode);
			if (lstCategoryTmp != null && lstCategoryTmp.getLstObject() != null && !lstCategoryTmp.getLstObject().isEmpty()) {
				lstData = lstCategoryTmp.getLstObject();
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstData;
	}
	
	/**
	 * Lay danh sach nganh hang con
	 * 
	 * @author hunglm16
	 * @param status
	 * @param shopId
	 * @return
	 * @since October 07,2015
	 */
	@RequestMapping(value = "/product-info-sub-cat/kendoui-combobox/{status}/{catIdStr}", method = RequestMethod.GET)
	public @ResponseBody
	List<ProductInfo> getLstProductInforChildForJSON(@PathVariable("status") Integer status, @PathVariable("catIdStr") String catIdStr) {
		List<ProductInfo> lstData = new ArrayList<ProductInfo>();
		try {
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
			if (currentUser == null) {
				return lstData;
			}
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			List<Long> lstObjectId = new ArrayList<>();
			if (!StringUtil.isNullOrEmpty(catIdStr)) {
				String[] arrCatId = catIdStr.split(",");
				if (arrCatId != null && arrCatId.length > 0) {
					for (String value : arrCatId) {
						if (StringUtil.isNumberInt(value)) {
							lstObjectId.add(Long.valueOf(value));
						}
					}
				}
			}
			ActiveType statusA = ActiveType.RUNNING;
			if (status != null && status > ActiveType.DELETED.getValue()) {
				statusA = ActiveType.parseValue(status);
			}
			lstData = productInfoMgr.getListSubCatByListCat(statusA, ProductType.SUB_CAT, lstObjectId);
			if (lstData == null) {
				lstData = new ArrayList<ProductInfo>();
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstData;
	}

	/**
	 * @see LAY DANH SACH DON VI THEO CAY. VOI DEFAULT shopId
	 */
	@RequestMapping(value = "/shop/combotree/{type}/{shopId}", method = RequestMethod.POST)
	public @ResponseBody
	List<TreeVOBasic<ShopVO>> getShopComboTree(@PathVariable("type") int type, @PathVariable("shopId") Long shopId) {
		List<TreeVOBasic<ShopVO>> cbxTree = new ArrayList<TreeVOBasic<ShopVO>>();
		try {
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);

			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");

			CmsMgr cmsMgr = (CmsMgr) context.getBean("cmsMgr");

			BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
			filter.setId(currentUser.getShopRoot().getShopId());
			filter.setUserId(currentUser.getUserId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setStrShopId(getStrListShopId());
			TreeVOBasic<ShopVO> tree = new TreeVOBasic<ShopVO>();
			tree = cmsMgr.getTreeFullShopVOByFilter(filter);
			cbxTree.add(tree);
		} catch (BusinessException e) {
			LogUtility.logError(e, "ths.dms.web.controller.ReportController.getShopComboTree() " + e.getMessage());
		}
		return cbxTree;
	}

	/**
	 * @RequestMapping(value = "/shop/combotree/{type}/{shopId}", method =
	 *                       RequestMethod.POST) public @ResponseBody
	 *                       List<ComboTreeBean>
	 *                       getShopComboTree(@PathVariable("type") int type,
	 *                       @PathVariable("shopId") Long shopId){
	 *                       List<ComboTreeBean> lstShopTree = new
	 *                       ArrayList<ComboTreeBean>(); Shop currentShop =
	 *                       null; List<Shop> shopTreeVO = null;
	 *                       ServletRequestAttributes attr =
	 *                       (ServletRequestAttributes)
	 *                       RequestContextHolder.currentRequestAttributes();
	 *                       HttpSession session =
	 *                       attr.getRequest().getSession(true); Staff staff =
	 *                       null; StaffMgr staffMgr = (StaffMgr)
	 *                       context.getBean("staffMgr"); UserToken currentUser
	 *                       = (UserToken)session.getAttribute("cmsUserToken");
	 *                       if (currentUser != null &&
	 *                       currentUser.getUserName() != null) { try{ staff =
	 *                       staffMgr.getStaffByCode(currentUser.getUserName());
	 *                       }catch (Exception e) { LogUtility.logError(e,
	 *                       e.getMessage()); } } if(session!= null &&
	 *                       session.getAttribute
	 *                       (ConstantManager.SESSION_SHOP_CHOOSE)!= null &&
	 *                       staff!=null && staff.getStaffType().getObjectType()
	 *                       == StaffObjectType.NVGS.getValue()){ currentShop =
	 *                       (Shop)session.getAttribute(ConstantManager.
	 *                       SESSION_SHOP_CHOOSE); }else{ if(session!= null &&
	 *                       session
	 *                       .getAttribute(ConstantManager.SESSION_SHOP)!=
	 *                       null){ currentShop =
	 *                       (Shop)session.getAttribute(ConstantManager
	 *                       .SESSION_SHOP); } } if(currentShop!= null){ if(type
	 *                       == COMBOTREE_ONE_NODE){ shopTreeVO = new
	 *                       ArrayList<Shop>(); shopTreeVO.add(currentShop); }
	 *                       else { try{ ShopMgr shopMgr =
	 *                       (ShopMgr)context.getBean("shopMgr"); shopTreeVO =
	 *                       shopMgr.getListSubShopEx(shopId,
	 *                       ActiveType.RUNNING,null); }catch (Exception e) {
	 *                       LogUtility.logError(e, e.getMessage()); } }
	 *                       if(shopTreeVO != null){ for(int
	 *                       i=0;i<shopTreeVO.size();i++){ ComboTreeBean tmp =
	 *                       new ComboTreeBean(); tmp =
	 *                       loadComboTreeForShop(tmp, shopTreeVO.get(i));
	 *                       lstShopTree.add(tmp); } }else{ ComboTreeBean bean =
	 *                       new ComboTreeBean(); lstShopTree.add(bean); } }
	 *                       return lstShopTree; }
	 **/

	@RequestMapping(value = "/shop/combotree/authorize/{type}/{shopId}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getShopComboTreeEx(@PathVariable("type") int type, @PathVariable("shopId") Long shopId) {
		List<ComboTreeBean> lstShopTree = new ArrayList<ComboTreeBean>();
		Shop shopRoot = null;
		List<Shop> shopTreeVO = null;

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
		UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");

		try {
			List<Long> listShopIdCurrentUser = new ArrayList<Long>();
			if (shopId != null) {
				shopRoot = shopMgr.getShopRoot(shopId);
			}
			if (currentUser != null && currentUser.getListShop() != null && currentUser.getListShop().size() > 0) {
				shopRoot = shopMgr.getShopRoot(currentUser.getListShop().get(0).getShopId());
				for (int i = 0, size = currentUser.getListShop().size(); i < size; i++) {
					listShopIdCurrentUser.add(currentUser.getListShop().get(i).getShopId());
				}
			}

			//shopMgr.getListShopFromListShopToken(listShopIdCurrentUser,ShopObjectType.NPP.getValue());
			List<Shop> listShopIdOfShop = shopMgr.getAllShopChildrenByLstId(listShopIdCurrentUser);
			if (shopRoot != null) {
				if (type == COMBOTREE_ONE_NODE) {
					shopTreeVO = new ArrayList<Shop>();
					shopTreeVO.add(shopRoot);
				} else {
					try {
						// FIXME: 25.09.2014
						shopTreeVO = new ArrayList<Shop>(); //shopMgr.getListSubShopAuthorize(shopId,null,null,ActiveType.RUNNING,listShopIdOfShop);
					} catch (Exception e) {
						LogUtility.logError(e, e.getMessage());
					}
				}
				if (shopTreeVO != null) {
					for (int i = 0, size = shopTreeVO.size(); i < size; i++) {
						ComboTreeBean tmp = new ComboTreeBean();
						tmp = loadComboTreeForShopAuthorize(tmp, shopTreeVO.get(i), listShopIdOfShop);
						lstShopTree.add(tmp);
					}
				} else {
					ComboTreeBean bean = new ComboTreeBean();
					lstShopTree.add(bean);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return lstShopTree;
	}

	private ComboTreeBean loadComboTreeForShopAuthorize(ComboTreeBean node, Shop shop, List<Shop> listShop) {
		try {
			if (shop != null) {
				node.setId(String.valueOf(shop.getId().toString()));
				node.setText(StringUtil.CodeAddName(shop.getShopCode(), shop.getShopName()));
				List<Shop> shopTreeVO = new ArrayList<Shop>();
				if (shopTreeVO != null && shopTreeVO.size() > 0) {
					node.setState(ConstantManager.JSTREE_STATE_CLOSE);
				} else {
					node.setState(ConstantManager.JSTREE_STATE_OPEN);
				}
				node.setChildren(new ArrayList<ComboTreeBean>());
			}
		} catch (Exception e) {
		}
		return node;
	}

	@RequestMapping(value = "/shop/kendoui-combobox", method = RequestMethod.GET)
	public @ResponseBody
	List<Shop> getListShop4Kendo() throws BusinessException {
		List<Shop> listShop = new ArrayList<Shop>();
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		if (session != null) {
			// tulv2 02.10.2014
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getShopRoot().getShopId() != null 
					&& currentUser.getUserId() != null && currentUser.getRoleToken() != null && currentUser.getRoleToken().getRoleId() != null) {
				try {
					ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
					CmsMgr cmsMgr = (CmsMgr) context.getBean("cmsMgr");
					if (cmsMgr != null && shopMgr != null) {
						BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
						filter.setId(currentUser.getShopRoot().getShopId());
						filter.setUserId(currentUser.getUserId());
						filter.setRoleId(currentUser.getRoleToken().getRoleId());
						filter.setStrShopId(getStrListShopId());
						TreeVOBasic<ShopVO> cayDonViPhanQuyen = cmsMgr.getTreeFullShopVOByFilter(filter);
						if (cayDonViPhanQuyen != null) {
							List<ShopVO> lstShopVO = convertBasicTreeVOToArray(cayDonViPhanQuyen);
							for (ShopVO shopVO : lstShopVO) {
								if (shopVO != null && shopVO.getId() != null && shopVO.getId() > 0) {
									listShop.add(shopMgr.getShopById(shopVO.getId()));
								}
							}
						}
					}
				} catch (Exception e) {
					LogUtility.logError(e, "ReportController.getListShop4KendoHO()" + e.getMessage());
				}
			}
			/*
			 * ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr"); UserToken
			 * currentUser = (UserToken)session.getAttribute("cmsUserToken");
			 * if(currentUser!=null){ CmsMgr cmsMgr = (CmsMgr)
			 * context.getBean("cmsMgr"); BasicFilter<ShopVO> filter = new
			 * BasicFilter<ShopVO>();
			 * filter.setId(currentUser.getShopRoot().getShopId());
			 * filter.setUserId(currentUser.getUserId()); TreeVOBasic<ShopVO>
			 * cayDonViPhanQuyen = cmsMgr.getTreeFullShopVOByFilter(filter);
			 * //listShop = convertTreeBasic2List(cayDonViPhanQuyen);
			 * if(cayDonViPhanQuyen!=null && cayDonViPhanQuyen.getId()>0){ Shop
			 * root = shopMgr.getShopById(cayDonViPhanQuyen.getId());
			 * listShop.add(root); List<TreeVOBasic<ShopVO>> lstCon1 =
			 * cayDonViPhanQuyen.getChildren(); if(lstCon1!=null &&
			 * lstCon1.size() > 0){ for(TreeVOBasic<ShopVO> con1 : lstCon1){
			 * Shop conS1 = shopMgr.getShopById(con1.getId());
			 * listShop.add(conS1); List<TreeVOBasic<ShopVO>> lstCon2 =
			 * con1.getChildren(); if(lstCon2!=null && lstCon2.size() > 0){
			 * for(TreeVOBasic<ShopVO> con2 : lstCon2){ Shop conS2 =
			 * shopMgr.getShopById(con2.getId()); listShop.add(conS2);
			 * List<TreeVOBasic<ShopVO>> lstCon3 = con2.getChildren();
			 * if(lstCon3!=null && lstCon3.size() > 0){ for(TreeVOBasic<ShopVO>
			 * con3 : lstCon3){ Shop conS3 = shopMgr.getShopById(con3.getId());
			 * listShop.add(conS3); } } } } } } } }
			 */
		}
		return listShop;
	}

	//ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
	/*
	 * private List<Shop> convertTreeBasic2List(TreeVOBasic<ShopVO>
	 * cayDonViPhanQuyen) throws BusinessException{ List<Shop> lst = new
	 * ArrayList<Shop>(); Shop shop =
	 * shopMgr.getShopById(cayDonViPhanQuyen.getId()); lst.add(shop); if
	 * (cayDonViPhanQuyen.getChildren() == null) { return lst; } List<Shop> lst2
	 * = new ArrayList<Shop>(); for (int i = 0, sz =
	 * cayDonViPhanQuyen.getChildren().size(); i < sz; i++) { lst2 =
	 * convertTreeBasic2List(cayDonViPhanQuyen.getChildren().get(i)); for (int j
	 * = 0, szj = lst2.size(); j < szj; j++) { lst.add(lst.get(j)); } } return
	 * lst; }
	 */
	/**
	 * vuongmq, not used.
	 * 
	 * @return
	 * @throws BusinessException
	 */
	@RequestMapping(value = "/shop/kendoui-combobox-ho", method = RequestMethod.GET)
	public @ResponseBody
	List<Shop> getListShop4KendoHO() throws BusinessException {
		List<Shop> listShop = new ArrayList<Shop>();
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		// tulv2 02.10.2014
		UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
		if (currentUser != null && currentUser.getShopRoot() != null) {
			try {
				ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
				CmsMgr cmsMgr = (CmsMgr) context.getBean("cmsMgr");
				if (cmsMgr != null && shopMgr != null) {
					BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
					filter.setId(currentUser.getShopRoot().getShopId());
					filter.setUserId(currentUser.getUserId());
					filter.setRoleId(currentUser.getRoleToken().getRoleId());
					filter.setStrShopId(getStrListShopId());
					TreeVOBasic<ShopVO> cayDonViPhanQuyen = cmsMgr.getTreeFullShopVOByFilter(filter);
					if (cayDonViPhanQuyen != null) {
						List<ShopVO> lstShopVO = convertBasicTreeVOToArray(cayDonViPhanQuyen);
						for (ShopVO shopVO : lstShopVO) {
							if (shopVO != null && shopVO.getId() != null && shopVO.getId() > 0) {
								listShop.add(shopMgr.getShopById(shopVO.getId()));
							}
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, "ReportController.getListShop4KendoHO()" + e.getMessage());
			}
		}
		// end tulv2
		return listShop;
	}

	@RequestMapping(value = "/shop/salemt/kendoui-combobox", method = RequestMethod.GET)
	public @ResponseBody
	List<Shop> getListShopSaleMT4Kendo() {
		/*
		 * List<Shop> listShop = new ArrayList<Shop>(); ServletRequestAttributes
		 * attr = (ServletRequestAttributes)
		 * RequestContextHolder.currentRequestAttributes(); HttpSession session
		 * = attr.getRequest().getSession(true); ShopMgr shopMgr =
		 * (ShopMgr)context.getBean("shopMgr"); Shop currentShop = null;
		 * if(session!= null &&
		 * session.getAttribute(ConstantManager.SESSION_SHOP)!= null){
		 * currentShop =
		 * (Shop)session.getAttribute(ConstantManager.SESSION_SHOP); } try {
		 * List<Shop> lstShop = new
		 * ArrayList<Shop>();//shopMgr.getListSubShopExSaleMT
		 * (currentShop.getId(),false); if(lstShop != null && lstShop.size() >
		 * 0) { for(Shop temp : lstShop)
		 * if(ShopType.MT_STORE.getValue().equals(temp
		 * .getType().getObjectType())) listShop.add(temp);//cua hang } } catch
		 * (Exception e) { LogUtility.logError(e, e.getMessage()); } return
		 * listShop;
		 */
		return null;
	}

	@RequestMapping(value = "/vung/combotree/{type}/{shopId}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getShopComboTreeTypeVung(@PathVariable("type") int type, @PathVariable("shopId") Long shopId) {
		List<ComboTreeBean> lstShopTree = new ArrayList<ComboTreeBean>();
		Shop currentShop = null;
		List<Shop> shopTreeVO = null;
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		if (session != null && session.getAttribute(ConstantManager.SESSION_SHOP) != null) {
			currentShop = (Shop) session.getAttribute(ConstantManager.SESSION_SHOP);
		}
		if (currentShop != null) {
			if (type == COMBOTREE_ONE_NODE) {
				shopTreeVO = new ArrayList<Shop>();
				shopTreeVO.add(currentShop);
			} else {
				try {
					ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
					// FIXME: 25.09.2014
					shopTreeVO = new ArrayList<Shop>();//shopMgr.getListSubShopForVung(shopId,ActiveType.RUNNING);
				} catch (Exception e) {
					LogUtility.logError(e, e.getMessage());
				}
			}
			if (shopTreeVO != null) {
				for (int i = 0; i < shopTreeVO.size(); i++) {
					ComboTreeBean tmp = new ComboTreeBean();
					tmp = loadComboTreeForShop(tmp, shopTreeVO.get(i));
					lstShopTree.add(tmp);
				}
			} else {
				ComboTreeBean bean = new ComboTreeBean();
				lstShopTree.add(bean);
			}
		}
		return lstShopTree;
	}

	private ComboTreeBean loadComboTreeForShop(ComboTreeBean node, Shop shop) {
		try {
			if (shop != null) {
				node.setId(String.valueOf(shop.getId().toString()));
				node.setText(StringUtil.CodeAddName(shop.getShopCode(), shop.getShopName()));
				ShopMgr shopMgr = (ShopMgr) context.getBean("shopMgr");
				List<Shop> shopTreeVO = shopMgr.getListSubShopEx(shop.getId(), ActiveType.RUNNING, null);
				if (shopTreeVO != null && shopTreeVO.size() > 0) {
					node.setState(ConstantManager.JSTREE_STATE_CLOSE);
				} else {
					node.setState(ConstantManager.JSTREE_STATE_OPEN);
				}
				node.setChildren(new ArrayList<ComboTreeBean>());
			}
		} catch (Exception e) {
		}
		return node;
	}

	/**
	 * Gets the report tree.
	 * 
	 * @author hunglm16
	 * @since October 1,2014
	 * @return the report tree
	 */
	private List<JsTreeNode> getChildFormNewTreeVO(CmsVO form, List<CmsVO> lst, List<Long> lstForTreeTmp) {
		List<JsTreeNode> res = new ArrayList<JsTreeNode>();
		for (CmsVO obj : lst) {
			if (form.getFormId().compareTo(obj.getParentFormId()) == 0) {
				JsTreeNode node = new JsTreeNode(obj.getFormName(), obj.getFormCode(), obj.getFormName(), obj.getUrl());
				node.setChildren(getChildFormNewTreeVO(obj, lst, lstForTreeTmp));
				res.add(node);
				lstForTreeTmp.add(form.getFormId());
			}
		}
		return res;
	}

	/**
	 * Tao cay bao cao
	 * 
	 * @author hunglm16
	 * @since October 1,2014
	 * */
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getReportTree() {
		try {

			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
			CmsMgr cmsMgr = (CmsMgr) context.getBean("cmsMgr");
			CmsFilter filter = new CmsFilter();
			filter.setUserId(currentUser.getUserId());
			filter.setRoleId(currentUser.getRoleToken().getRoleId());
			filter.setType(PermissionType.REPORT.getValue());
			filter.setAppId(currentUser.getAppToken().getAppId());

			List<CmsVO> lstLinkReport = cmsMgr.getListFormInRoleByFilter(filter);
			List<JsTreeNode> lstReportTree = new ArrayList<JsTreeNode>();
			JsTreeNode reportRoot = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.general.text"), "", "", "");
			reportRoot.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<JsTreeNode> lstChildReport = new ArrayList<JsTreeNode>();

			lstForTreeTmp = new ArrayList<Long>();

			for (CmsVO obj : lstLinkReport) {
				boolean flag = false;
				for (Long id : lstForTreeTmp) {
					if (obj.getFormId().equals(id) || obj.getParentFormId().compareTo(id) == 0) {
						flag = true;
					}
				}
				if (!flag) {
					JsTreeNode node = new JsTreeNode(obj.getFormName(), obj.getFormCode(), obj.getFormName(), obj.getUrl());
					node.setChildren(getChildFormNewTreeVO(obj, lstLinkReport, lstForTreeTmp));
					lstChildReport.add(node);
					lstForTreeTmp.add(obj.getFormId());
				}
			}
			reportRoot.setChildren(lstChildReport);
			if (lstChildReport == null || lstChildReport.size() == 0) {
				reportRoot.setState(ConstantManager.JSTREE_STATE_LEAF);
			}
			lstReportTree.add(reportRoot);
			return lstReportTree;

		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return null;
	}
	
	/**
	 * Lay ds nhom thiet bi
	 * 
	 * @author hunglm16
	 * @since May 18,2015
	 */
	@RequestMapping(value = "/stock-equip-group/kendo-ui-combobox-ho", method = RequestMethod.GET)
	public @ResponseBody
	List<EquipmentGroupVO> getListGroupEquipForKendoHO() throws BusinessException {
		try {
			List<EquipmentGroupVO> lstGroupEquip = null;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
			if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null) {
				EquipmentManagerMgr equipMangeMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
				EquipGFilter<EquipmentGroupVO> filter = new EquipGFilter<EquipmentGroupVO>();
				String statusStr = attr.getRequest().getParameter("arrStatus");
				if (!StringUtil.isNullOrEmpty(statusStr)) {
					String [] lstStatus = statusStr.split(",");
					if (lstStatus != null &&  lstStatus.length > 0) {
						List<Integer> lst = new ArrayList<Integer>();
						for (int i = 0, isize = lstStatus.length; i < isize; i++) {
							Integer status = Integer.parseInt(lstStatus[i]);
							if (status != null) {
								lst.add(status);
							}
						}
						if (lst.size() > 0) {
							filter.setLstStatus(lst);
							lstGroupEquip = equipMangeMgr.getListEquipGroupForFilterByRPT(filter);
						}
					}
				} else {
					lstGroupEquip = equipMangeMgr.getListEquipGroupForFilter(filter);
				}
				if (lstGroupEquip != null && !lstGroupEquip.isEmpty()) {
					return lstGroupEquip;
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ReportController.getListGroupEquipForKendoHO() " + e.getMessage());
		}
		return new ArrayList<EquipmentGroupVO>();
	}
	
	/**
	 * Lay ds loai thiet bi
	 * 
	 * @author tamvnm
	 * @return list loai thiet bi
	 * @since sep 09,2015
	 */
	@RequestMapping(value = "/stock-equip-category/kendo-ui-combobox-ho", method = RequestMethod.GET)
	public @ResponseBody
	List<EquipCategory> getListCategoryEquipForKendoHO() throws BusinessException {
		try {
			List<EquipCategory> lstCategoryEquip = null;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");
			if (currentUser != null && currentUser.getStaffRoot() != null && currentUser.getShopRoot() != null) {
				EquipmentManagerMgr equipMangeMgr = (EquipmentManagerMgr) context.getBean("equipmentManagerMgr");
				EquipmentFilter<EquipCategory> filter = new EquipmentFilter<EquipCategory>();
				filter.setIsCheckStatus(Boolean.FALSE);
				lstCategoryEquip = equipMangeMgr.getListEquipmentCategoryByFilter(filter);

				if (lstCategoryEquip != null && !lstCategoryEquip.isEmpty()) {
					return lstCategoryEquip;
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ReportController.getListGroupEquipForKendoHO() " + e.getMessage());
		}
		return new ArrayList<EquipCategory>();
	}

	/**
	 * Lay ds don vi (ShopVO) theo phan quyen (thay vi dung Shop)
	 * 
	 * @author lacnv1
	 * @since Sep 30, 2014
	 */
	@RequestMapping(value = "/shop/kendo-ui-combobox-ho", method = RequestMethod.GET)
	public @ResponseBody
	List<ShopVO> getListShop4KendoHO1() throws BusinessException {
		try {
			List<ShopVO> lstShop = null;
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			UserToken currentUser = (UserToken) session.getAttribute("cmsUserToken");

			if (currentUser != null && currentUser.getShopRoot() != null) {
				CmsMgr cmsMgr = (CmsMgr) context.getBean("cmsMgr");

				BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
				filter.setId(currentUser.getShopRoot().getShopId());
				filter.setUserId(currentUser.getUserId());
				filter.setStrShopId(getStrListShopId());
				filter.setRoleId(currentUser.getRoleToken().getRoleId());
				TreeVOBasic<ShopVO> tree = cmsMgr.getTreeFullShopVOByFilter(filter);

				if (tree != null) {
					if (tree.getAttribute() == null) {
						tree.setAttribute(convertToShopToken(currentUser.getShopRoot()));
					}
					lstShop = convertBasicTreeVOToArray(tree);
					return lstShop;
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ReportController.getListShop4KendoHO()" + e.getMessage());
		}
		return new ArrayList<ShopVO>();
	}

	/**
	 * Chuyen cay TreeVOBasic<ShopVO> sang List<ShopVO>
	 * 
	 * @author lacnv1
	 * @since Sep 30, 2014
	 * @description dang dung cho cay khi su dung KendoUI
	 */
	private List<ShopVO> convertBasicTreeVOToArray(TreeVOBasic<ShopVO> tree) throws Exception {
		List<ShopVO> lst = new ArrayList<ShopVO>();
		lst.add(tree.getAttribute());
		if (tree.getChildren() == null || tree.getChildren().size() == 0) {
			return lst;
		}
		List<ShopVO> lstTmp = null;
		for (TreeVOBasic<ShopVO> t : tree.getChildren()) {
			lstTmp = convertBasicTreeVOToArray(t);
			if (lstTmp != null && lstTmp.size() > 0) {
				lst.addAll(lstTmp);
			}
		}
		return lst;
	}


	/**
	 * Gets the report tree.
	 * 
	 * @return the report tree
	 */
	@RequestMapping(value = "/salemt/tree", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getSaleMTReportTree() {
		List<JsTreeNode> lstReportTree = new ArrayList<JsTreeNode>();
		JsTreeNode baocao = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.text"), "");
		baocao.setState(ConstantManager.JSTREE_STATE_OPEN);
		List<JsTreeNode> lstChildReport = new ArrayList<JsTreeNode>();
		baocao.setChildren(lstChildReport);
		lstReportTree.add(baocao);

		//Báo cáo tồn kho
		JsTreeNode parentBCXNTK = new JsTreeNode("1." + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.parent.bcxntk"), "");
		parentBCXNTK.setState(ConstantManager.JSTREE_STATE_OPEN);
		lstChildReport.add(parentBCXNTK);
		List<JsTreeNode> lstChildBCXNTK = new ArrayList<JsTreeNode>();
		parentBCXNTK.setChildren(lstChildBCXNTK);

		JsTreeNode bcchnh = new JsTreeNode("1.1 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcchnh"), SaleMTReportTemplate.BCCHNH.getName());
		lstChildBCXNTK.add(bcchnh);
		JsTreeNode bcchxh = new JsTreeNode("1.2 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcchxh"), SaleMTReportTemplate.BCCHXH.getName());
		lstChildBCXNTK.add(bcchxh);
		JsTreeNode bctkch = new JsTreeNode("1.3 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bctkch"), SaleMTReportTemplate.BCTKCH.getName());
		lstChildBCXNTK.add(bctkch);
		JsTreeNode bcxnt = new JsTreeNode("1.4 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcxnt"), SaleMTReportTemplate.BCXNT.getName());
		lstChildBCXNTK.add(bcxnt);
		JsTreeNode bcdh = new JsTreeNode("1.5 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcdh"), SaleMTReportTemplate.BCDH.getName());
		lstChildBCXNTK.add(bcdh);
		JsTreeNode bcthh = new JsTreeNode("1.6 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcthh"), SaleMTReportTemplate.BCTHH.getName());
		lstChildBCXNTK.add(bcthh);
		JsTreeNode bcnxdch = new JsTreeNode("1.7 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcnxdch"), SaleMTReportTemplate.BCNXDCH.getName());
		lstChildBCXNTK.add(bcnxdch);
		//	    JsTreeNode bcttpt = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcttpt"),SaleMTReportTemplate.BCTTPT.getName());
		//	    lstChildBCXNTK.add(bcttpt);

		//Báo cáo bán hàng
		JsTreeNode parentBCBH = new JsTreeNode("2." + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.parent.bcbh"), "");
		parentBCBH.setState(ConstantManager.JSTREE_STATE_OPEN);
		lstChildReport.add(parentBCBH);
		List<JsTreeNode> lstChildBCBH = new ArrayList<JsTreeNode>();
		parentBCBH.setChildren(lstChildBCBH);

		JsTreeNode bcbh = new JsTreeNode("2.1 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcbh"), SaleMTReportTemplate.BCBH.getName());
		lstChildBCBH.add(bcbh);
		JsTreeNode bckm = new JsTreeNode("2.2 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bckm"), SaleMTReportTemplate.BCKM.getName());
		lstChildBCBH.add(bckm);
		JsTreeNode bcdsthd = new JsTreeNode("2.3 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcdsthd"), SaleMTReportTemplate.BCDSTHD.getName());
		lstChildBCBH.add(bcdsthd);
		JsTreeNode bcdstcat = new JsTreeNode("2.4 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcdstcat"), SaleMTReportTemplate.BCDSTCAT.getName());
		lstChildBCBH.add(bcdstcat);

		JsTreeNode bcdstcat1 = new JsTreeNode("2.5 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcdstcat1"), SaleMTReportTemplate.BCDSTCAT1.getName());
		lstChildBCBH.add(bcdstcat1);

		//Báo cáo bán hàng KA
		JsTreeNode parentBCBHKA = new JsTreeNode("3." + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.parent.bcbh.ka"), "");
		parentBCBHKA.setState(ConstantManager.JSTREE_STATE_OPEN);
		lstChildReport.add(parentBCBHKA);
		List<JsTreeNode> lstChildBCBHKA = new ArrayList<JsTreeNode>();
		parentBCBHKA.setChildren(lstChildBCBHKA);

		JsTreeNode bccpsp = new JsTreeNode("3.1 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcbh.ka.chi.phi.san.pham"), SaleMTReportTemplate.BCHTCPSP.getName());
		lstChildBCBHKA.add(bccpsp);
		JsTreeNode bccpspct = new JsTreeNode("3.2 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcbh.ka.chi.phi.san.pham.chi.tiet"), SaleMTReportTemplate.BCHTCPSPCT.getName());
		lstChildBCBHKA.add(bccpspct);
		JsTreeNode bcqldttb = new JsTreeNode("3.3 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcbh.ka.doanh.thu.thiet.bi"), SaleMTReportTemplate.BCQLDTTB.getName());
		lstChildBCBHKA.add(bcqldttb);

		JsTreeNode bccttthttmsp = new JsTreeNode("3.4 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcbh.ka.chuong.trinh.thanh.toan.ho.tro"), SaleMTReportTemplate.BCCTTTHTCTSP.getName());
		lstChildBCBHKA.add(bccttthttmsp);

		//Báo cáo dữ liệu thô
		JsTreeNode parentBCDLT = new JsTreeNode("4." + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.parent.bcdlt"), "");
		parentBCDLT.setState(ConstantManager.JSTREE_STATE_OPEN);
		lstChildReport.add(parentBCDLT);
		List<JsTreeNode> lstChildBCDLT = new ArrayList<JsTreeNode>();
		parentBCDLT.setChildren(lstChildBCDLT);

		JsTreeNode bcdlbht = new JsTreeNode("4.1 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcdlt.bcdlbht"), SaleMTReportTemplate.BCDLBHT.getName());
		lstChildBCDLT.add(bcdlbht);

		//Báo cáo khách hàng
		JsTreeNode parentBCKH = new JsTreeNode("5." + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.parent.customer"), "");
		parentBCKH.setState(ConstantManager.JSTREE_STATE_OPEN);
		lstChildReport.add(parentBCKH);
		List<JsTreeNode> lstChildBCKH = new ArrayList<JsTreeNode>();
		parentBCKH.setChildren(lstChildBCKH);
		JsTreeNode bcdskh = new JsTreeNode("5.1 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcdskh"), SaleMTReportTemplate.BCDSKH.getName());
		lstChildBCKH.add(bcdskh);
		JsTreeNode bcsn = new JsTreeNode("5.2 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bcsn"), SaleMTReportTemplate.BCSN.getName());
		lstChildBCKH.add(bcsn);

		JsTreeNode bckhkgd = new JsTreeNode("5.3 " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "salemt.report.bckhkgd"), SaleMTReportTemplate.BCKHKGD.getName());
		lstChildBCKH.add(bckhkgd);

		return lstReportTree;
	}

	/**
	 * Gets the staff tree tungmt
	 * 
	 * @return the staff tree
	 */
	@RequestMapping(value = "/staff-for-shop/tree/{shopId}", method = RequestMethod.GET)
	public @ResponseBody
	List<JsTreeNode> getStaffTreeForShop(@RequestParam("id") Long staffId, @RequestParam("lhl") Integer lhl, @PathVariable("shopId") Long shopId) {
		return null;
	}

	@RequestMapping(value = "/stocktrans/combotree/{shopId}/{staffcode}", method = RequestMethod.POST)
	public @ResponseBody
	List<ComboTreeBean> getShopComboTreeStockTrans(@PathVariable("shopId") Long shopId, @PathVariable("staffcode") String staffCode) {
		List<ComboTreeBean> lstShopTree = new ArrayList<ComboTreeBean>();
		System.out.println("execute load trans...");
		try {

			StockMgr stockMgr = (StockMgr) context.getBean("stockMgr");
			List<StockTrans> stockEntities = stockMgr.getListStockTrans(staffCode, shopId);
			if (stockEntities != null) {
				for (StockTrans stockItem : stockEntities) {
					ComboTreeBean nodeNew = new ComboTreeBean();
					loadComboTreeForStockTrans(nodeNew, stockItem);
					lstShopTree.add(nodeNew);
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return lstShopTree;
	}

	private ComboTreeBean loadComboTreeForStockTrans(ComboTreeBean node, StockTrans stockItem) {
		try {
			if (stockItem != null) {
				node.setId(String.valueOf(stockItem.getId().toString()));
				node.setText(stockItem.getStockTransCode());
				node.setState(ConstantManager.JSTREE_STATE_OPEN);
			}
		} catch (Exception e) {
		}
		return node;
	}

	private List<Long> lstForTreeTmp = new ArrayList<Long>();

	public List<Long> getLstForTreeTmp() {
		return lstForTreeTmp;
	}

	public void setLstForTreeTmp(List<Long> lstForTreeTmp) {
		this.lstForTreeTmp = lstForTreeTmp;
	}

	/**
	 * Thuc hien convert tu shoptoken sang shopVO status mac dinh la 1
	 * 
	 * @author tulv2
	 * @since 18.10.2014
	 * */
	private ShopVO convertToShopToken(ShopToken shopToken) {
		ShopVO shopVo = new ShopVO();
		shopVo.setShopId(shopToken.getShopId());
		shopVo.setShopCode(shopToken.getShopCode());
		shopVo.setShopName(shopToken.getShopName());
		shopVo.setParentId(shopToken.getParentId());
		shopVo.setIsLevel(shopToken.getIsLevel());
		shopVo.setStatus(1);
		shopVo.setObjectType(shopToken.getObjectType());
		return shopVo;
	}

	private List<Long> getListShopChildId() {
		Map<Long, Long> map = this.getMapShopChild();
		List<Long> lstId = new ArrayList<Long>();
		for (Entry<Long, Long> entry : map.entrySet()) {
			lstId.add(entry.getValue());
		}
		return lstId;
	}

	private String getStrListShopId() {
		List<Long> lstShop = this.getListShopChildId();
		String str = "-1";
		for (Long id : lstShop) {
			str += "," + id.toString();
		}
		return str;
	}

	private Map<Long, Long> getMapShopChild() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		if (session.getAttribute("mapShop") != null) {
			return (HashMap<Long, Long>) session.getAttribute("mapShop");
		}
		return new HashMap<Long, Long>();
	}
}

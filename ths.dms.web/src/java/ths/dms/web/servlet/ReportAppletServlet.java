package ths.dms.web.servlet;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ths.core.entities.vo.rpt.RptPGNVTTGVO;
import ths.dms.core.report.ShopReportMgr;

/**
 * Servlet implementation class ReportAppletServlet
 */
@WebServlet("/ReportAppletServlet")
public class ReportAppletServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportAppletServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);  
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			Map<String, Object> parametersReport = new HashMap<String, Object>();		
			parametersReport.put("logoPath", ReportUtils.getVinamilkLogoRealPath(request));
			parametersReport.put("sName", "TL");
			parametersReport.put("sAddress", "TL");
			parametersReport.put("sPhone", "01634446015");
			parametersReport.put("cDate", DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY));
			ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext-service.xml");
			ShopReportMgr shopReportMgr = (ShopReportMgr)context.getBean("shopReportMgr");
			List<RptPGNVTTGVO> lstGNTTG = shopReportMgr.getListRptPGNVTTGVOs(Long.valueOf(15), null, DateUtil.now(), DateUtil.now());
			if(lstGNTTG!= null && lstGNTTG.size()>0){
				for (RptPGNVTTGVO pGNVTTGVO : lstGNTTG) {
					if(pGNVTTGVO!= null){
						pGNVTTGVO.setTotalString(StringUtil.convertMoneyToString(pGNVTTGVO.getTotal()));
					}
				}
			}
			JRDataSource dataSource = new JRBeanCollectionDataSource(lstGNTTG);	
			JasperPrint jasperPrint = JasperFillManager.fillReport(ShopReportTemplate.TDHN_GNTTG.getTemplatePath(false, FileExtension.JASPER), parametersReport, dataSource);		
			if (jasperPrint != null){
    			response.setContentType("application/octet-stream");
    			ServletOutputStream ouputStream = response.getOutputStream();
    			
    			ObjectOutputStream oos = new ObjectOutputStream(ouputStream);
    			oos.writeObject(jasperPrint);
    			oos.flush();
    			oos.close();

    			ouputStream.flush();
    			ouputStream.close();
    		}else{
    			response.setContentType("text/html");
    			PrintWriter outp = response.getWriter();
    			outp.println("<html>");
    			outp.println("<head>");
    			outp.println("<title>JasperReports - Web Application Sample</title>");
    			outp.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
    			outp.println("</head>");
    			
    			outp.println("<body bgcolor=\"white\">");
    	
    			outp.println("<span class=\"bold\">Empty response.</span>");
    	
    			outp.println("</body>");
    			outp.println("</html>");
    		}
            
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.googlecode.jsonplugin.JSONUtil;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.vo.LogInfoVO;

import ths.dms.action.catalog.ProductCatalogAction;
import ths.dms.helper.Configuration;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.ImageUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class VideoServlet extends HttpServlet {
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
	 * 
	 * @param request servlet request
	 * @param response servlet response
	 */
	private static int MAX_UPLOAD_SIZE = Configuration.getMaxImageUploadSize() * 1024 * 1024;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * @modify hunglm16
	 * @param request servlet request
	 * @param response servlet response
	 * @since 16/10/2015
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Map<String, String> result = new HashMap<String, String>();
			String userName = !StringUtil.isNullOrEmpty(request.getParameter("currentUser")) ? request.getParameter("currentUser").trim() : null;
			Long productId = !StringUtil.isNullOrEmpty(request.getParameter("productId")) ? Long.parseLong(request.getParameter("productId").trim()) : null;
			String folder = Configuration.getStoreStaticPath();
			ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext-service.xml");
			ProductMgr productMgr = (ProductMgr) context.getBean("productMgr");
			LogInfoVO logObj = new LogInfoVO();
			logObj.setIp(request.getRemoteAddr());
			logObj.setStaffCode(userName);
			int iError = 0;
			MediaItem mediaItem = null;
			PrintWriter out = response.getWriter();
			DiskFileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(100 * 1024 * 1024);
			List items = upload.parseRequest(request);
			String videoName = "";
			String videoDescription = "";
			File videoFile = null;
			String videoFilename = "";
			long videoMaxSize = 0;
			if (productId != null) {
				Product product = productMgr.getProductById(productId);
				String categoryCode = "A";
				String productCode = "";
				if (product != null) {
					productCode = product.getProductCode();
					if (product.getCat() != null) {
						categoryCode = product.getCat().getProductInfoCode();
					}
					Iterator iter = items.iterator();
					while (iter.hasNext()) {
						DiskFileItem item = (DiskFileItem) iter.next();
						if (item.isFormField()) {
							if (item.getFieldName().equals("name")) {
								videoName = item.getString();
							} else if (item.getFieldName().equals("desc")) {
								videoDescription = item.getString();
							}
						} else {
							videoFile = item.getStoreLocation();
							videoMaxSize = item.getSize();
							Calendar cal = Calendar.getInstance();
							String url = ImageUtility.createDetailUploadPath(productId, cal, 4, categoryCode);
							String folderMedia = folder + url;
							if (!FileUtility.isFolderExist(folderMedia)) {
								FileUtility.createDirectory(folderMedia);
							}
							String format = item.getName().substring(item.getName().length() - 4, item.getName().length());
							Integer tmpM = cal.get(Calendar.MONTH) + 1;
							String dd = String.valueOf(cal.get(Calendar.DATE) > 10 ? cal.get(Calendar.DATE) : "0" + cal.get(Calendar.DATE));
							String MM = String.valueOf(tmpM > 10 ? tmpM : "0" + tmpM);
							String hh = String.valueOf(cal.get(Calendar.HOUR_OF_DAY) > 10 ? cal.get(Calendar.HOUR_OF_DAY) : "0" + cal.get(Calendar.HOUR_OF_DAY));
							String mm = String.valueOf(cal.get(Calendar.MINUTE) > 10 ? cal.get(Calendar.MINUTE) : "0" + cal.get(Calendar.MINUTE));
							String ss = String.valueOf(cal.get(Calendar.SECOND) > 10 ? cal.get(Calendar.SECOND) : "0" + cal.get(Calendar.SECOND));
							String fileNameMedia = productCode + dd + MM + cal.get(Calendar.YEAR) + hh + mm + ss + Long.toString(cal.getTimeInMillis()) + format;
							String fileNameUrl = url + fileNameMedia;
							File file = new File(folderMedia + fileNameMedia);
							if (videoMaxSize > MAX_UPLOAD_SIZE) {
								iError = 2;
								break;
							} else if (!FileUtility.isValidExtension(FileUtility.getFileItemExtension(item), FileUtility.validVideoExtension)) {
								iError = 3; // Loi duoi file
								break;
							} else {
								try {
									item.write(file);
								} catch (Exception e) {
									file.delete();
								}
								boolean isValidFileType = ValidateUtil.isValidFileType(file, Configuration.getVideoUploadFileSupport());
								if (!isValidFileType) {
									iError = 1;
									break;
								}
							}
						}
					}
				}
			}
			result.put("status", String.valueOf(iError));
			response.setCharacterEncoding(ConstantManager.UTF_8_ENCODING);
			response.getWriter().write(JSONUtil.serialize(result));
		} catch (FileUploadException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}
}

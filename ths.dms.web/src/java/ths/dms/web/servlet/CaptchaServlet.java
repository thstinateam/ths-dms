/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.github.cage.Cage;
import com.github.cage.GCage;

/**
 * Tao hinh anh Captcha
 * 
 * @author hunglm16
 * @since 29/09/2015
 */
public class CaptchaServlet extends HttpServlet {
	private static final long serialVersionUID = 1490947492185481844L;

	private static final Cage cage = new GCage();

	/**
	 * Generates a captcha token and stores it in the session.
	 * 
	 * @param session where to store the captcha.
	 */
	public static void generateToken(HttpSession session) {
		final String token = cage.getTokenGenerator().next();

		session.setAttribute("captchaToken", token);
		markTokenUsed(session, false);
	}

	/**
	 * Used to retrieve previously stored captcha token from session.
	 * 
	 * @param session where the token is possibly stored.
	 * @return token or null if there was none
	 */
	public static String getToken(HttpSession session) {
		final Object val = session.getAttribute("captchaToken");

		return val != null ? val.toString() : null;
	}

	/**
	 * Danh dau co hieu su dung/ khong su dung cho hinh anh captcha
	 * 
	 * @param session
	 *            cho co hieu
	 * @param used
	 *            false neu khong su dung hinh anh
	 */
	protected static void markTokenUsed(HttpSession session, boolean used) {
		session.setAttribute("captchaTokenUsed", used);
	}

	/**
	 * Kiem tra su dung, khong su dung trong session cho hinh anh captcha
	 * 
	 * @author hunglm16
	 * @param session su dung nhan token de su dung.
	 * @return true neu token khong nam trong session
	 * @since 29/09/2015
	 */
	protected static boolean isTokenUsed(HttpSession session) {
		return !Boolean.FALSE.equals(session.getAttribute("captchaTokenUsed"));
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		final HttpSession session = req.getSession(false);
		String token = session != null ? getToken(session) : null;
		if (token == null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Captcha not found.");
			return;
		} else if (isTokenUsed(session)) {// for refresh captch fucntion
			generateToken(session);
			token = session != null ? getToken(session) : null;
		}

		setResponseHeaders(resp);
		markTokenUsed(session, true);
		cage.draw(token, resp.getOutputStream());
	}

	/**
	 * Phuong thuc ho tro, vo hieu hoa HTTP bo nho dem
	 * 
	 * @author hunglm16
	 * @param resp response object to be modified
	 * @since 29/09/2015
	 */
	protected void setResponseHeaders(HttpServletResponse resp) {
		resp.setContentType("image/" + cage.getFormat());
		resp.setHeader("Cache-Control", "no-cache, no-store");
		resp.setHeader("Pragma", "no-cache");
		final long time = System.currentTimeMillis();
		resp.setDateHeader("Last-Modified", time);
		resp.setDateHeader("Date", time);
		resp.setDateHeader("Expires", time);
	}
}
package ths.dms.web.interceptor;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import ths.dms.web.constant.ConstantManager;

/**
 * A Struts 2 interceptor that implements a security system.
 */
public class SecurityInterceptor extends AbstractInterceptor implements
        StrutsStatics {

    /** The Constant log. */
    protected static final Logger log =
            Logger.getLogger(SecurityInterceptor.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3435205916816654000L;

    
    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.interceptor.AbstractInterceptor#init()
     */
    @Override
    public void init() {
        log.debug("Intializing SecurityInterceptor...");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.opensymphony.xwork2.interceptor.AbstractInterceptor#intercept(com
     * .opensymphony.xwork2.ActionInvocation)
     */
    @Override
	public String intercept(ActionInvocation invocation) throws Exception {
		final ActionContext context = invocation.getInvocationContext();
		HttpServletRequest request = (HttpServletRequest) context.get(HTTP_REQUEST);	
		Object reqToken = request.getParameter(ConstantManager.TOKEN_NAME);
		Object sesToken = request.getSession().getAttribute(ConstantManager.SESSION_TOKEN);
		if(reqToken!= null && sesToken!= null && reqToken.equals(sesToken)){
			String token = UUID.randomUUID().toString();
			request.getSession().setAttribute(ConstantManager.SESSION_TOKEN, token);
			String result = invocation.invoke(); 
			return 	result;		
		}else {
			return ConstantManager.ERROR_400_CODE;
		}			
    }
  }

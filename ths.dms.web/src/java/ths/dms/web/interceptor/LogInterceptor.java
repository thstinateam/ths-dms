package ths.dms.web.interceptor;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsStatics;

import viettel.passport.client.UserToken;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import ths.dms.web.utils.LogUtility;

public class LogInterceptor extends AbstractInterceptor implements StrutsStatics {

	@Override
	public String intercept(ActionInvocation ai) throws Exception {
		final ActionContext context = ai.getInvocationContext();
		HttpServletRequest request = (HttpServletRequest) context.get(HTTP_REQUEST);
		UserToken currentUser = (UserToken)request.getSession().getAttribute("cmsUserToken");
		Calendar cal = Calendar.getInstance();
		String result = null;
		try {
			result = ai.invoke();
		} catch (Exception e) {
			throw e;
		}
		return result;
	}

}

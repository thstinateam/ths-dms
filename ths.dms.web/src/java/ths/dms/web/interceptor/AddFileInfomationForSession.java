/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.interceptor;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.apache.struts2.StrutsStatics;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.StringUtil;

/**
 * A Struts 2 interceptor that implements a security system.
 */
public class AddFileInfomationForSession extends AbstractInterceptor implements
        StrutsStatics {

    /** The Constant log. */
    protected static final Logger log =
            Logger.getLogger(AddFileInfomationForSession.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3435205916816654000L;

    
    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.interceptor.AbstractInterceptor#init()
     */
    @Override
    public void init() {
        log.debug("Intializing SecurityInterceptor...");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.opensymphony.xwork2.interceptor.AbstractInterceptor#intercept(com
     * .opensymphony.xwork2.ActionInvocation)
     */
    @Override
	public String intercept(ActionInvocation invocation) throws Exception {
		final ActionContext context = invocation.getInvocationContext();
		HttpServletRequest request = (HttpServletRequest) context.get(HTTP_REQUEST);
		if (request != null && request instanceof MultiPartRequestWrapper) {
			MultiPartRequestWrapper multiPartRequest = (MultiPartRequestWrapper) request;
			Enumeration<String> fileParameterNames = multiPartRequest.getFileParameterNames();
			request.getSession().removeAttribute(ConstantManager.SESSION_FILE_NAME);
			request.getSession().removeAttribute(ConstantManager.SESSION_FILE_CONTENT_TYPE);
			while (fileParameterNames.hasMoreElements()) {
				String element = fileParameterNames.nextElement();
				String[] fileNames = multiPartRequest.getFileNames(element);
				String[] contentTypes = multiPartRequest.getContentTypes(element);
				request.getSession().setAttribute(ConstantManager.SESSION_FILE_NAME, fileNames);
				request.getSession().setAttribute(ConstantManager.SESSION_FILE_CONTENT_TYPE, contentTypes);
			}
		}
		String result = invocation.invoke(); 
		return 	result;
    }
  }

package ths.dms.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import ths.dms.web.constant.ConstantManager;

/**
 * A Struts 2 interceptor that implements a login system.
 */
public class LoginInterceptor extends AbstractInterceptor implements
        StrutsStatics {

    /** The Constant log. */
    protected static final Logger log =
            Logger.getLogger(LoginInterceptor.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3435205916816654000L;

	/*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.interceptor.AbstractInterceptor#init()
     */
    @Override
    public void init() {
        log.debug("Intializing LoginInterceptor...");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.opensymphony.xwork2.interceptor.AbstractInterceptor#intercept(com
     * .opensymphony.xwork2.ActionInvocation)
     */
    @Override
	public String intercept(ActionInvocation invocation) throws Exception {
		final ActionContext context = invocation.getInvocationContext();
		HttpServletRequest request = (HttpServletRequest) context.get(HTTP_REQUEST);
		HttpSession session = request.getSession(true);
		if (session != null && session.getAttribute(ConstantManager.USER_SESSION_KEY) != null) {
		    HttpServletResponse response = (HttpServletResponse) context.get(HTTP_RESPONSE);
            response.setHeader("Cache-Control", "no-cache"); 
            // Forces caches to obtain a new copy of the page from the origin server
            response.setHeader("Cache-Control", "no-store"); // Directs caches not to store the page under any circumstance
            response.setDateHeader("Expires", 0); // Causes the proxy cache to see the page as "stale" 
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0 backward compatibility
            return invocation.invoke();
		}else{
			return ConstantManager.LOGIN_REDIRECT;
		}
	}
  }

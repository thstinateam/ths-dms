/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.report;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.GS_1_4_VO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.memcached.MemcachedUtils;
import ths.dms.core.report.HoReportMgr;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.BeanTest;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.NumberUtil;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;
import ths.dms.web.utils.report.excel.SXSSFReportHelper;

/**
 * Bao cao kho
 * @author hunglm16
 *
 */
public class StockReportAction extends AbstractAction {
	
	private static final long serialVersionUID = -3470234290573825410L;

	private HoReportMgr hoReportMgr;
	private ProductInfoMgr productInfoMgr;
	
	private Long shopId;
	private Long cycleId;
	
	private String date;
	private String shopCode;
	private String fromDate;
	private String toDate;
	private String npp;
	private String tennpp;
	private String vung;
	private String khuVuc;
	private String orderStock;
	private String orderProduct;
	private String orderNumber;
	private String orderStatus;
	private String strListShopId;
	private String location;
	
	private Date sysDate;
	
	private List<BeanTest> lstData;
	private List<CycleVO> lstCycle;
	private List<ProductInfo> lstProductType;
	
	private HashMap<String, Object> parametersReport;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		hoReportMgr = (HoReportMgr)context.getBean("hoReportMgr");
		productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	}

	@Override
	public String execute() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			Date now = commonMgr.getSysDate();
			sysDate = now;
			date = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
			Shop shop = null;
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			if (shop != null) {
				shopId = shop.getId();
				shopCode = shop.getShopCode();
			}
			CycleFilter filter = new CycleFilter();
			filter.setLessYear(2);
			filter.setEndDate(now);
			lstCycle = cycleMgr.getListCycleByFilter(filter);
			//Lay danh sach loai sp - hoanv25 August 04/2015
			lstProductType = new ArrayList<ProductInfo>();		
			ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfoTypeStock(null,ActiveType.PRODUCTTYPE);
			if (tmp != null) {
				lstProductType = tmp.getLstObject();
			}
			FileUtility.createTempDir();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.StockReportAction.execute"), createLogErrorStandard(startLogDate));
		}	
		return SUCCESS;
	}

	/**
	 * Bao cao xuat nhap ton nutifood 3.1
	 * 
	 * @author hoanv25
	 * @since July 31/2015
	 * @return
	 * @throws Exception 
	 */
	public String exportBCXNT31() throws Exception {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);

				List<GS_1_4_VO> lstData = hoReportMgr.exportBCXNT31(staffIdRoot, userId, strListShopId, fDate, tDate, orderStock, orderProduct, cycleId);

				//************************************************************************				
				int d = 8;
				if (lstData.size() > 0) {
					String outputName = ConstantManager.EXPORT_3_1_XNT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.3.1.xnt.shet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(23);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 100, 150, 150, 150, 200, 150, 130, 180, 130, 200, 100, 100, 180, 180, 150, 150, 150, 150);
					//Size Row
					ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 25, 15, 15, 15, 15);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.title.name");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
					//info					
					ExcelPOIProcessUtils.addCell(sheetData, 2, 3, vung, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 3, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 2, 4, npp, style.get(ExcelPOIProcessUtils.NORMAL));
					if (orderStock != null && orderStock.equals("0")) {
						ExcelPOIProcessUtils.addCell(sheetData, 6, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.hb"), style.get(ExcelPOIProcessUtils.NORMAL));
					} else {
						ExcelPOIProcessUtils.addCell(sheetData, 6, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.hkm"), style.get(ExcelPOIProcessUtils.NORMAL));
					}
					if (orderProduct != null) {
						ProductInfo proSubIn = productInfoMgr.getProductInfoById(new Long(orderProduct));
						if (proSubIn != null) {
							ExcelPOIProcessUtils.addCell(sheetData, 8, 3, proSubIn.getProductInfoName(), style.get(ExcelPOIProcessUtils.NORMAL));
						}
					} else {
						ExcelPOIProcessUtils.addCell(sheetData, 8, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tatca"), style.get(ExcelPOIProcessUtils.NORMAL));
					}
					ExcelPOIProcessUtils.addCell(sheetData, 4, 4, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 4, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 8, 4, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 2, 5, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL));
					//header
					ExcelPOIProcessUtils.addCell(sheetData, 1, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 3, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.chonkho"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 7, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.loaitp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 3, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 7, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 1, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

					ExcelPOIProcessUtils.addCell(sheetData, 0, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.4.1.dsdh.header.chuky"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 2, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 3, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.mausm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 7, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenusm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 8, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 9, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.sdt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 10, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.makho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 11, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.tenkho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 12, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.masp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 13, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tensp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 14, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dvt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 15, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.donggoi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 16, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dongiatruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 17, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dongiasauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 18, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.tongtondau"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 19, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.tongnhap"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 20, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.tongxuat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 21, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.toncuoi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));

					GS_1_4_VO salesVO = null;
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getChuKy(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getPhoneNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getMaKho(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getTenKho(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getDonViTinh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getDongGoi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						Double dab = null;
						if (salesVO.getGiaChuaThue() != null) {
							dab = ((BigDecimal) salesVO.getGiaChuaThue()).doubleValue();
						}
						if (salesVO.getGiaChuaThue() != null && salesVO.getGiaChuaThue().doubleValue() > 0) {
							if (SXSSFReportHelper.checkDecimal(dab) == true) {
								ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getGiaChuaThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getGiaChuaThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getGiaSauThue() != null && salesVO.getGiaSauThue().doubleValue() > 0) {
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getGiaSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getTonDau(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getTongNhap(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getTongXuat(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 21, d, salesVO.getTonCuoi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						d++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.StockReportAction.exportBCXNT31"), createLogErrorStandard(DateUtil.now()));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * Lay gia tri vung, khu vuc, npp cho cac bao cao
	 * 
	 * @author hoanv25
	 * @since July 31/2015
	 * @return
	 * @throws  
	 */
	public void getInfoParentShop(Shop shop) {
		Shop shop1 = null, shop2 = null, shop3 = null, shop4 = null;
		if (shop != null) {
			shop1 = shop.getParentShop();
			if (shop1 != null) {
				shop2 = shop1.getParentShop();
				if (shop2 != null) {
					shop3 = shop2.getParentShop();
					if (shop3 != null) {
						shop4 = shop3.getParentShop();
					}
				}
			}
		}
		if (shop1 != null && shop2 != null && shop3 != null && shop4 != null) {//npp
			npp = shop.getShopCode();
			tennpp = shop.getShopName();
			khuVuc = shop1.getShopCode();
			vung = shop2.getShopCode();
		} else if (shop1 != null && shop2 != null && shop3 != null && shop4 == null) {//vung
			khuVuc = shop.getShopCode();
			vung = shop1.getShopCode();
		} else if (shop1 != null && shop2 != null && shop3 == null) {//khu vuc
			vung = shop.getShopCode();
		}
	}
	
	/**
	 * Bao cao xuat nhap ton chi tiet nutifood 3.2
	 * @author vuongmq
	 * @return String
	 * @throws Exception 
	 * @since 18/09/2015
	 */
	public String exportXNTCT32() throws Exception {
		Date startLogDate = DateUtil.now();
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				Cycle cycle = cycleMgr.getCycleById(cycleId);
				List<GS_1_4_VO> lstData = hoReportMgr.exportBCXNT32(staffIdRoot, roleId, strListShopId, cycleId, fDate, tDate);
				if (lstData != null && lstData.size() > 0) {
					String outputName = ConstantManager.EXPORT_3_2_XNT_CT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.3.2.xntct.shet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					//sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(11);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 100, 150, 150, 150, 200, 200, 150, 100);
					ExcelPOIProcessUtils.setColumnWidth(sheetData, 14, 160);
					ExcelPOIProcessUtils.setColumnWidth(sheetData, 35, 160);
					//Size Row
					//ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 25, 15, 15, 15, 15);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.2.xnt.title.name");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					//info					
					Shop shop = shopMgr.getShopById(shopId);
					ExcelPOIProcessUtils.addCell(sheetData, 2, 3, shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 3, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
					
					ExcelPOIProcessUtils.addCell(sheetData, 2, 4, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 4, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 4, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
					//header
					ExcelPOIProcessUtils.addCell(sheetData, 1, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.shop"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 3, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					int c = 0;
					int d = 6;
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.4.1.dsdh.header.chuky"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.mien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nha.phan.phoi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.makho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.tenkho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.masp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tensp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.quy.doi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.don.gia"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					// Dau ky
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 12, d, 14, d, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.dau.ky"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 12, 7, 13, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 12, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 13, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 14, 7, 14, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thanh.tien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					// Mua tu NCC
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 15, 6, 16, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.mua.ncc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 15, 7, 16, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 15, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 16, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 17, 6, 18, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.tra.ncc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 17, 7, 18, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 17, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 18, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 19, 6, 20, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.ban.hang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 19, 7, 20, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 19, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 20, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 21, 6, 22, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.hang.km"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 21, 7, 22, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 21, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 22, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 23, 6, 24, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.xuat.vansale"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 23, 7, 24, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 23, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 24, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 25, 6, 26, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.nhap.vansale"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 25, 7, 26, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 25, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 26, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 27, 6, 28, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.kh.tra.hang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 27, 7, 28, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 27, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 28, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 29, 6, 30, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.dc.tang.kho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 29, 7, 30, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 29, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 30, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 31, 6, 32, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.dc.giam.kho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 31, 7, 32, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 31, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 32, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					// Ton kho thuc te
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 33, 6, 35, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.ton.kho.thuc.te"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 33, 7, 34, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 33, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 34, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 35, 7, 35, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thanh.tien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					d = 9;
					GS_1_4_VO salesVO = null;
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						c = 0;
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getChuKy(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaKho(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTenKho(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getDongGoi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getGiaSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						if (salesVO.getDongGoi() != null) {
							BigDecimal dongGoi = new BigDecimal(salesVO.getDongGoi());
							if (salesVO.getTonDau() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, NumberUtil.convfactQuantityPackage(salesVO.getTonDau(), dongGoi), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, NumberUtil.convfactQuantityRetail(salesVO.getTonDau(), dongGoi), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTonDau().multiply(salesVO.getGiaSauThue()), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getNhapNuti() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapNuti().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapNuti().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getTraNuti() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTraNuti().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTraNuti().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getXuatNuti() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatNuti().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatNuti().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getXuatKM() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatKM().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatKM().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getXuatVansale() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatVansale().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatVansale().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getNhapVansale() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapVansale().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapVansale().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getTraNutiKH() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTraNutiKH().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTraNutiKH().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getNhapDc() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapDc().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapDc().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getXuatDc() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatDc().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatDc().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getTonCuoi() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, NumberUtil.convfactQuantityPackage(salesVO.getTonCuoi(), dongGoi), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, NumberUtil.convfactQuantityRetail(salesVO.getTonCuoi(), dongGoi), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTonCuoi().multiply(salesVO.getGiaSauThue()), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
						}
						d++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.shopreport.StockReportAction.exportXNTCT32()"), createLogErrorStandard(startLogDate));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao ton kho chi tiet nhan vien Vansale
	 * @author dungnt19
	 * @return String
	 * @throws Exception 
	 * @since 22/01/2016
	 */
	public String exportTKTNVVS42() throws Exception {
		Date startLogDate = DateUtil.now();
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<GS_1_4_VO> lstData = hoReportMgr.exportBCTKTNVVS42(staffIdRoot, roleId, strListShopId, fDate, tDate);
				if (lstData != null && lstData.size() > 0) {
					String outputName = ConstantManager.EXPORT_4_2_TKTNVVS + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.4.2.tktnvvs.shet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					//sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(11);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 100, 150, 200, 150, 200, 100, 100, 100);

					//Size Row
					//ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 25, 15, 15, 15, 15);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.4.2.tktnvvs.title.name");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					//info					
					Shop shop = shopMgr.getShopById(shopId);
					ExcelPOIProcessUtils.addCell(sheetData, 2, 3, shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 3, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
					
					ExcelPOIProcessUtils.addCell(sheetData, 2, 4, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 4, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
					//header
					ExcelPOIProcessUtils.addCell(sheetData, 1, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.shop"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					int c = 0;
					int d = 6;
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.region"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.area"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.shop.code"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.shop.name"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.staff.code"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.staff.name"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.product.code"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, c, d, c++, d + 2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.product.name"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					// Dau ky
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 9, d, 11, d, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.ton.dau"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 9, 7, 10, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 9, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 10, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 11, 7, 11, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thanh.tien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					
					// Mua tu NCC
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 12, 6, 13, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.nhap.kho.tu.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 12, 7, 13, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 12, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 13, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					
					
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 14, 6, 15, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.xuat.kho.tra.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 14, 7, 15, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 14, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 15, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 16, 6, 17, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.ban.hang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 16, 7, 17, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 16, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 17, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 18, 6, 19, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.tra.hang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 18, 7, 19, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 18, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 19, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					
					// Ton kho thuc te
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 20, 6, 22, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.tktvnvs.header.ton.cuoi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 20, 7, 21, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 20, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 21, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.le"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 22, 7, 22, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.xnt.header.thanh.tien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					d = 9;
					GS_1_4_VO salesVO = null;
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						c = 0;
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						if (salesVO.getDongGoi() != null) {
							BigDecimal dongGoi = new BigDecimal(salesVO.getDongGoi());
							if (salesVO.getTonDau() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, NumberUtil.convfactQuantityPackage(salesVO.getTonDau(), dongGoi), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, NumberUtil.convfactQuantityRetail(salesVO.getTonDau(), dongGoi), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTonDau().multiply(salesVO.getGiaSauThue()), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							
							if (salesVO.getNhapVansale() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapVansale().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapVansale().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getXuatVansale() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatVansale().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getXuatVansale().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							
							if (salesVO.getNhapNuti() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapNuti().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getNhapNuti().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getTraNuti() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTraNuti().longValue() / dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTraNuti().longValue() % dongGoi.longValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}

							if (salesVO.getTonCuoi() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, NumberUtil.convfactQuantityPackage(salesVO.getTonCuoi(), dongGoi), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, NumberUtil.convfactQuantityRetail(salesVO.getTonCuoi(), dongGoi), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, c++, d, salesVO.getTonCuoi().multiply(salesVO.getGiaSauThue()), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
						}
						d++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.shopreport.StockReportAction.exportBCTKTNVVS42()"), createLogErrorStandard(startLogDate));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao tinh trang don hang 3.3
	 * 
	 * @author hoanv25
	 * @version
	 * @param
	 * @since August 05/2015
	 * @return
	 * @throws Exception
	 */
	public String exportBCTTNH35() throws Exception {
		Date startLogDate = DateUtil.now();
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);

				List<GS_1_4_VO> lstData = hoReportMgr.exportBCTTNH35(staffIdRoot, userId, strListShopId, fDate, tDate, orderNumber, orderStatus, cycleId);

				//************************************************************************
				String outputName = ConstantManager.EXPORT_3_3_TTDH + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.3.3.sttnh.shet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(23);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 100, 150, 150, 200, 150, 200, 150, 150, 200, 100, 150, 150, 130, 130, 130, 150, 150, 150);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 25, 15, 15, 15, 15);
				sheetData.setDisplayGridlines(false);
				//Tittle
				String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.xnt.title.name");
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
				//info					
				ExcelPOIProcessUtils.addCell(sheetData, 2, 3, vung, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 3, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 4, npp, style.get(ExcelPOIProcessUtils.NORMAL));
				if (orderNumber != null) {
					ExcelPOIProcessUtils.addCell(sheetData, 6, 3, orderNumber, style.get(ExcelPOIProcessUtils.NORMAL));
				}
				if (orderStatus != null) {
					if (orderStatus.equals("0")) {
						ExcelPOIProcessUtils.addCell(sheetData, 8, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.dnd"), style.get(ExcelPOIProcessUtils.NORMAL));
					} else if (orderStatus.equals("1")) {
						ExcelPOIProcessUtils.addCell(sheetData, 8, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.dnmp"), style.get(ExcelPOIProcessUtils.NORMAL));
					} else if (orderStatus.equals("2")) {
						ExcelPOIProcessUtils.addCell(sheetData, 8, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.dtdd"), style.get(ExcelPOIProcessUtils.NORMAL));
					}
				} else {
					ExcelPOIProcessUtils.addCell(sheetData, 8, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tatca"), style.get(ExcelPOIProcessUtils.NORMAL));
				}
				ExcelPOIProcessUtils.addCell(sheetData, 4, 4, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 4, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 4, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 5, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL));
				//header
				ExcelPOIProcessUtils.addCell(sheetData, 1, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.maorder"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.status"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

				ExcelPOIProcessUtils.addCell(sheetData, 0, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.4.1.dsdh.header.chuky"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.4.1.dsdh.header.tinhtp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.sopodvkh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.nghiepvu"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.sdh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 11, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.masp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 12, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tensp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 13, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.loaitp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 14, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.ngayod"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 15, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.ngayxk"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 16, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.slod"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 17, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.sltn"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 18, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.conlai"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 19, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.ngayn"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 20, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.dg"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 21, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.tt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));

				int d = 8;
				if (lstData.size() > 0) {
					GS_1_4_VO salesVO = null;
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getChuKy(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTinh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getSoPODVKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getNghiepVu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getPoConfirm(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getLoai(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getNgayOrder(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getNgayXK(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getSlOrder(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getSlNhap(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getConLai(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getNgayNhan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getDonGia(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 21, d, salesVO.getThanhTien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						d++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.shopreport.StockReportAction.exportBCXNT35()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao kiem ke 3.4
	 * 
	 * @author hoanv25
	 * @version
	 * @param
	 * @since August 06/2015
	 * @return
	 * @throws Exception
	 */
	public String exportBCKK34() throws Exception {
		Date startLogDate = DateUtil.now();
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);

				List<GS_1_4_VO> lstData = hoReportMgr.exportBCKK34(staffIdRoot, userId, strListShopId, fDate, tDate, orderStock, orderProduct, orderStatus, cycleId);

				//************************************************************************
				String outputName = ConstantManager.EXPORT_3_4_KK + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.3.4.kk.shet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(23);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 100, 150, 200, 180, 150, 200, 150, 150, 150, 120, 120, 150, 130, 150, 200, 100, 100, 150, 150, 100, 100, 100, 150, 150, 150);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 25, 15, 15, 15, 15);
				sheetData.setDisplayGridlines(false);
				//Tittle
				String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.title.name");
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
				//info					
				ExcelPOIProcessUtils.addCell(sheetData, 2, 3, vung, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 3, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 4, npp, style.get(ExcelPOIProcessUtils.NORMAL));
				if (orderStock != null && orderStock.equals("0")) {
					ExcelPOIProcessUtils.addCell(sheetData, 6, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.hb"), style.get(ExcelPOIProcessUtils.NORMAL));
				} else {
					ExcelPOIProcessUtils.addCell(sheetData, 6, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.hkm"), style.get(ExcelPOIProcessUtils.NORMAL));
				}
				if (orderProduct != null) {
					ProductInfo proSubIn = productInfoMgr.getProductInfoById(new Long(orderProduct));
					if (proSubIn != null) {
						ExcelPOIProcessUtils.addCell(sheetData, 8, 3, proSubIn.getProductInfoName(), style.get(ExcelPOIProcessUtils.NORMAL));
					}
				} else {
					ExcelPOIProcessUtils.addCell(sheetData, 8, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tatca"), style.get(ExcelPOIProcessUtils.NORMAL));
				}
				if (orderStatus != null) {
					if (orderStatus.equals("0")) {
						ExcelPOIProcessUtils.addCell(sheetData, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.dth"), style.get(ExcelPOIProcessUtils.NORMAL));
					} else if (orderStatus.equals("1")) {
						ExcelPOIProcessUtils.addCell(sheetData, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.ht"), style.get(ExcelPOIProcessUtils.NORMAL));
					} else if (orderStatus.equals("2")) {
						ExcelPOIProcessUtils.addCell(sheetData, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "create_order_huy"), style.get(ExcelPOIProcessUtils.NORMAL));
					} else if (orderStatus.equals("3")) {
						ExcelPOIProcessUtils.addCell(sheetData, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.reject"), style.get(ExcelPOIProcessUtils.NORMAL));
					} else if (orderStatus.equals("4")) {
						ExcelPOIProcessUtils.addCell(sheetData, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.cd"), style.get(ExcelPOIProcessUtils.NORMAL));
					}
				}
				ExcelPOIProcessUtils.addCell(sheetData, 4, 4, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 4, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 4, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 5, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL));
				//header
				ExcelPOIProcessUtils.addCell(sheetData, 1, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.chonkho"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.loaitp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.tinhtrang"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

				ExcelPOIProcessUtils.addCell(sheetData, 0, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.4.1.dsdh.header.chuky"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.sdt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.usm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.asm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.sdkk"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 11, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.makho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 12, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.1.xnt.header.tenkho"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 13, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.nkk"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 14, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.nd"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 15, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.nguoid"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 16, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.3.sttnh.header.loaitp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 17, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.masp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 18, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tensp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 19, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dvt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 20, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.quycach"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 21, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dongiatruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 22, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dongiasauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 23, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.sltoncuoi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 24, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.slkiemk"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 25, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.chenhl"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 26, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.thanhtientruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 27, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.thanhtiensauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 28, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.3.4.kk.header.tinht"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_2_NONE_MEDIUM));

				int d = 8;
				if (lstData.size() > 0) {
					GS_1_4_VO salesVO = null;
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getChuKy(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getPhoneNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getMaKiemKe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getMaKho(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getTenKho(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getNgayKiemKe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getNgayDuyet(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getNguoiDuyet(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getLoai(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getDonViTinh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getQuyCach(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						if (salesVO.getGiaChuaThue() != null) {
							ExcelPOIProcessUtils.addCell(sheetData, 21, d, salesVO.getGiaChuaThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 21, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));

						}
						if (salesVO.getGiaSauThue() != null) {
							ExcelPOIProcessUtils.addCell(sheetData, 22, d, salesVO.getGiaSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 22, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						ExcelPOIProcessUtils.addCell(sheetData, 23, d, salesVO.getSlTonCuoi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 24, d, salesVO.getSlKiemKe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 25, d, salesVO.getChenhLech(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 26, d, salesVO.getThanhTienTruocThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 27, d, salesVO.getThanhTienSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 28, d, salesVO.getTrangThai(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						d++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.shopreport.StockReportAction.exportBCKK34()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}

	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * @return
	 */
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getNpp() {
		return npp;
	}

	public void setNpp(String npp) {
		this.npp = npp;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getKhuVuc() {
		return khuVuc;
	}

	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}

	public String getOrderStock() {
		return orderStock;
	}

	public void setOrderStock(String orderStock) {
		this.orderStock = orderStock;
	}

	public String getOrderProduct() {
		return orderProduct;
	}

	public void setOrderProduct(String orderProduct) {
		this.orderProduct = orderProduct;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<BeanTest> getLstData() {
		return lstData;
	}

	public void setLstData(List<BeanTest> lstData) {
		this.lstData = lstData;
	}

	public List<CycleVO> getLstCycle() {
		return lstCycle;
	}

	public void setLstCycle(List<CycleVO> lstCycle) {
		this.lstCycle = lstCycle;
	}

	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}

	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	public String getTennpp() {
		return tennpp;
	}

	public void setTennpp(String tennpp) {
		this.tennpp = tennpp;
	}

	public List<ProductInfo> getLstProductType() {
		return lstProductType;
	}

	public void setLstProductType(List<ProductInfo> lstProductType) {
		this.lstProductType = lstProductType;
	}
	
}

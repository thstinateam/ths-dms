/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.report;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ReceivableSummaryVO;
import ths.core.entities.vo.rpt.RptDebitRetrieveVO;
import ths.core.entities.vo.rpt.RptDebitReturnVO;
import ths.dms.core.memcached.MemcachedUtils;
import ths.dms.core.report.HoReportMgr;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.BeanTest;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIVTICTUtils;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PayableDetailVO;
import ths.dms.core.entities.vo.ReceivableSummaryVO;
import ths.core.entities.vo.rpt.RptDebitRetrieveVO;
import ths.dms.core.memcached.MemcachedUtils;
import ths.dms.core.report.HoReportMgr;

/**
 * 
 * @author thangnv31
 * @since 22 Dec, 2015
 */
public class DebitReportAction extends AbstractAction {
	
	private static final long serialVersionUID = -3470234290573825410L;

	private HoReportMgr hoReportMgr;
	private ProductInfoMgr productInfoMgr;
	
	private Long shopId;
	private Long cycleId;
	
	private String date;
	private String shopCode;
	private String fromDate;
	private String toDate;
	private String strListShopId;
	
	private Date sysDate;
	
	private List<BeanTest> lstData;
	private List<CycleVO> lstCycle;
	private List<ProductInfo> lstProductType;
	
	private HashMap<String, Object> parametersReport;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		hoReportMgr = (HoReportMgr)context.getBean("hoReportMgr");
		productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	}

	@Override
	public String execute() throws Exception {
		Date startLogDate = DateUtil.now();
		try {
			Date now = commonMgr.getSysDate();
			sysDate = now;
			date = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
			Shop shop = null;
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			if (shop != null) {
				shopId = shop.getId();
				shopCode = shop.getShopCode();
			}
			CycleFilter filter = new CycleFilter();
			filter.setLessYear(2);
			filter.setEndDate(now);
			lstCycle = cycleMgr.getListCycleByFilter(filter);
			//Lay danh sach loai sp - hoanv25 August 04/2015
			lstProductType = new ArrayList<ProductInfo>();		
			ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfoTypeStock(null,ActiveType.PRODUCTTYPE);
			if (tmp != null) {
				lstProductType = tmp.getLstObject();
			}
			FileUtility.createTempDir();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.StockReportAction.execute"), createLogErrorStandard(startLogDate));
		}	
		return SUCCESS;
	}
	
	/**
	 * BC 3.1 Bao cao cong no phai thu tong hop
	 * @author trietptm
	 * @return String
	 * @throws Exception 
	 * @since Dev 29, 2015
	 */
	public String exportReceivableSummary() throws Exception {
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		List<ReceivableSummaryVO> lstData = new ArrayList<ReceivableSummaryVO>();
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				String msg = "";
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					}
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportLong(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				Long userId = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				Shop shop = shopMgr.getShopById(shopId);
				lstData = hoReportMgr.exportReceivableSummary(userId, roleId, shopId, fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					String outputName = ConstantManager.RPT_3_1_REPORT_REVEIVABLE_SUMMARY_FILE_NAME + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("rpt.receivable.summary.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultColumnWidth(15);
					//set static Column width
					ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 40, 80, 80, 100, 150, 100, 150, 250, 110, 110, 110, 110, 110, 110, 110, 110, 110);
					sheetData.setDisplayGridlines(false);
					//Title
					ExcelPOIVTICTUtils.setRowHeight(sheetData, 0, 30);
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 0, 0, 12, 0, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.receivable.summary.title"), style.get(ExcelPOIVTICTUtils.TITLE_G));
					//info
					int rowTitle = 1;
					int colTitle = 2;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colTitle++, rowTitle, colTitle++, rowTitle, shop == null ? "" : (shop.getShopCode() + " - " + shop.getShopName()), style.get(ExcelPOIVTICTUtils.NORMAL));
					rowTitle++;
					colTitle = 2;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.fromdate"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, fromDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.todate"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, toDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIVTICTUtils.NORMAL));
					
					//header
					String[] listHeader = new String[] { "rpt.receivable.summary.header.number", "rpt.receivable.summary.header.region", "rpt.receivable.summary.header.area", 
							"rpt.receivable.summary.header.shop.code", "rpt.receivable.summary.shop.name", "rpt.receivable.summary.customer.code", "rpt.receivable.summary.customer.name",
							"rpt.receivable.summary.address", "rpt.receivable.summary.open.debit", "rpt.receivable.summary.increase", "rpt.receivable.summary.discount.receivable",
							"rpt.receivable.summary.payment", "rpt.receivable.summary.decrease", "rpt.receivable.summary.discount.payable", "rpt.receivable.summary.payment.revert",
							"rpt.receivable.summary.close.debit", "rpt.receivable.summary.maxDebit.amount"};
					int rowHeader = rowTitle + 2;
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 9, rowHeader, 11, rowHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.receivable.summary.receivable"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 12, rowHeader, 14, rowHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.receivable.summary.payable"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					
					rowHeader++;
					int colHeader = 0;
					for (int i = 0, n = listHeader.length; i < n; i++) {
						ExcelPOIVTICTUtils.addCell(sheetData, colHeader++, rowHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, listHeader[i]), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					}
					
					// fill data
					int rowData = rowHeader + 1;
					int rowStart = rowData;
					int group3 = 0;
					int group2 = 0;
					int group1 = 0;
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, n = lstData.size(); i < n; i++) {
						ReceivableSummaryVO vo = lstData.get(i);
						int colData = 0;
						
						if (StringUtil.isNullOrEmpty(vo.getAreaCode())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getShopCode())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getCustomerCode())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						}
						String styleCenter = ExcelPOIVTICTUtils.ROW_DOTTED_CENTER;
						String styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP;
						String styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO;
						if (StringUtil.isNullOrEmpty(vo.getCustomerCode())) {
							styleCenter = ExcelPOIVTICTUtils.ROW_DOTTED_CENTER_BOLD;
							styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP_BOLD;
							styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO_BOLD;
						}
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, i + 1, style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRegionCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getAreaCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopName(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getCustomerCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getCustomerName(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getAddress(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getOpenDebit(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getIncrease(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDiscountReceivable(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPayment(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDecrease(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDiscountPayable(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPaymentRevert(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getCloseDebit(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getMaxDebitAmount(), style.get(styleRight));
						rowData++;
					}
					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.flush();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.exportRawData"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			lstData.clear();
			lstData = null;
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao: exportCNPTCT3_2
	 * 
	 * @author thangnv31
	 * @return JSON
	 * @since 22/12/2015
	 * */
	public String exportCNPTCT3_2() throws Exception {
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		List<RptDebitRetrieveVO> lstData = new ArrayList<RptDebitRetrieveVO>();
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				String msg = "";
//				Long shopId = currentUser.getShopRoot().getShopId();
//				if (!StringUtil.isNullOrEmpty(strListShopId)) {
//					shopId = Long.valueOf(strListShopId);
//					if (super.getMapShopChild().get(shopId) == null) {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
//					}
//				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportLong(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				lstData = hoReportMgr.reportCnptCt32(cycleId, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), shopId);
				if (lstData != null && !lstData.isEmpty()) {
					String outputName = ConstantManager.RPT_3_2_BCCNPTCT_FILE_NAME + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("rpt.debit.retrieve.detail.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultColumnWidth(15);
					//set static Column width
					ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 40, 80, 80, 100, 200, 120, 220, 120, 100, 130, 300, 110, 110, 110, 110);
					sheetData.setDisplayGridlines(false);
					//Title
					ExcelPOIVTICTUtils.setRowHeight(sheetData, 0, 30);
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 0, 0, 12, 0, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.debit.retrieve.detail.title"), style.get(ExcelPOIVTICTUtils.TITLE_G));
					//info
					int rowTitle = 1;
					int colTitle = 2;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colTitle++, rowTitle, colTitle++, rowTitle, shop == null ? "" : (shop.getShopCode() + " - " + shop.getShopName()), style.get(ExcelPOIVTICTUtils.NORMAL));
					rowTitle++;
					colTitle = 2;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.fromdate"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, fromDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.todate"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, toDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIVTICTUtils.NORMAL));
					
					//header
					String[] listHeader = new String[] { "rpt.debit.retrieve.detail.header.number", "rpt.debit.retrieve.detail.header.region", "rpt.debit.retrieve.detail.header.area", 
							"rpt.debit.retrieve.detail.header.shop.code", "rpt.debit.retrieve.detail.shop.name", "rpt.debit.retrieve.detail.customer.code", "rpt.debit.retrieve.detail.customer.name",
							"rpt.debit.retrieve.detail.billCode", "rpt.debit.retrieve.detail.billDate", "rpt.debit.retrieve.detail.billType", "rpt.debit.retrieve.detail.reason",
							"rpt.debit.retrieve.detail.debitAmount", "rpt.debit.retrieve.detail.discountAmount", "rpt.debit.retrieve.detail.payedAmount", "rpt.debit.retrieve.detail.remainAmount"};
					int rowHeader = rowTitle + 2;
					
					rowHeader++;
					int colHeader = 0;
					for (int i = 0, n = listHeader.length; i < n; i++) {
						ExcelPOIVTICTUtils.addCell(sheetData, colHeader++, rowHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, listHeader[i]), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					}
					
					// fill data
					int rowData = rowHeader + 1;
					int rowStart = rowData;
					int group5 = 0;
					int group4 = 0;
					int group3 = 0;
					int group2 = 0;
					int group1 = 0;
					List<Integer> start5 = new ArrayList<Integer>();
					List<Integer> end5 = new ArrayList<Integer>();
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, n = lstData.size(); i < n; i++) {
						RptDebitRetrieveVO vo = lstData.get(i);
						int colData = 0;
						
						if (StringUtil.isNullOrEmpty(vo.getShopRegionCode())) {
							vo.setShopRegionCode("Tổng");
							start5.add(group5 + rowStart);
							end5.add(i + rowStart - 1);
							group5 = i + 1;
							group4++;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getShopAreaCode())) {
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getShopCode())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getCustomerCode())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getBillCode())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						}
						String styleCenter = ExcelPOIVTICTUtils.ROW_DOTTED_CENTER;
						String styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP;
						String styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO;
						if (StringUtil.isNullOrEmpty(vo.getBillCode())) {
							styleCenter = ExcelPOIVTICTUtils.ROW_DOTTED_CENTER_BOLD;
							styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP_BOLD;
							styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO_BOLD;
						}
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, i + 1, style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopRegionCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopAreaCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopName(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getCustomerCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getCustomerName(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getBillCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getBillDate(), style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getBillType(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getReason(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDebitAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDiscountAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPayedAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRemainAmount(), style.get(styleRight));
						rowData++;
					}
					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0; i < start4.size(); i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					for (int i = 0; i < start5.size(); i++) {
						sheetData.groupRow(start5.get(i), end5.get(i));
					}
					
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.flush();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.exportCNPTCT3_2"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			lstData.clear();
			lstData = null;
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao: exportCNPTR3_3
	 * 
	 * @author thangnv31
	 * @return JSON
	 * @since 22/12/2015
	 * */
	public String exportCNPTR3_3() throws Exception {
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		List<RptDebitReturnVO> lstData = new ArrayList<RptDebitReturnVO>();
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				String msg = "";
//				Long shopId = currentUser.getShopRoot().getShopId();
//				if (!StringUtil.isNullOrEmpty(strListShopId)) {
//					shopId = Long.valueOf(strListShopId);
//					if (super.getMapShopChild().get(shopId) == null) {
//						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
//					}
//				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportLong(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				lstData = hoReportMgr.reportCnptCt33(cycleId, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), shopId);
				if (lstData != null && !lstData.isEmpty()) {
					String outputName = ConstantManager.RPT_3_3_BCCNPTR_FILE_NAME + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("rpt.debit.return.general.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultColumnWidth(15);
					//set static Column width
					ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 40, 80, 80, 100, 200, 120, 100, 130, 100, 110, 110, 110, 110, 110);
					sheetData.setDisplayGridlines(false);
					//Title
					ExcelPOIVTICTUtils.setRowHeight(sheetData, 0, 30);
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 0, 0, 12, 0, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.debit.return.general.title"), style.get(ExcelPOIVTICTUtils.TITLE_G));
					//info
					int rowTitle = 1;
					int colTitle = 2;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colTitle++, rowTitle, colTitle++, rowTitle, shop == null ? "" : (shop.getShopCode() + " - " + shop.getShopName()), style.get(ExcelPOIVTICTUtils.NORMAL));
					rowTitle++;
					colTitle = 2;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.fromdate"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, fromDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.todate"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, toDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIVTICTUtils.NORMAL));
					
					//header
					String[] listHeader = new String[] { "rpt.debit.return.general.header.number", "rpt.debit.return.general.header.region", "rpt.debit.return.general.header.area", 
							"rpt.debit.return.general.header.shop.code", "rpt.debit.return.general.shop.name",
							"rpt.debit.return.general.billCode", "rpt.debit.return.general.billDate", "rpt.debit.return.general.billType", "rpt.debit.return.general.deadline",
							"rpt.debit.return.general.debitAmount", "rpt.debit.return.general.payedAmount", "rpt.debit.return.general.remainAmount",
							"rpt.debit.return.general.notexpiredAmount","rpt.debit.return.general.expiredAmount"};
					int rowHeader = rowTitle + 2;
					
					rowHeader++;
					int colHeader = 0;
					for (int i = 0, n = listHeader.length; i < n; i++) {
						ExcelPOIVTICTUtils.addCell(sheetData, colHeader++, rowHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, listHeader[i]), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					}
					
					// fill data
					int rowData = rowHeader + 1;
					int rowStart = rowData;
					int group4 = 0;
					int group3 = 0;
					int group2 = 0;
					int group1 = 0;
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, n = lstData.size(); i < n; i++) {
						RptDebitReturnVO vo = lstData.get(i);
						int colData = 0;
						
						if (StringUtil.isNullOrEmpty(vo.getShopRegionCode())) {
							vo.setShopRegionCode("Tổng");
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getShopAreaCode())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getShopCode())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getBillCode())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						}
						String styleCenter = ExcelPOIVTICTUtils.ROW_DOTTED_CENTER;
						String styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP;
						String styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO;
						if (StringUtil.isNullOrEmpty(vo.getBillCode())) {
							styleCenter = ExcelPOIVTICTUtils.ROW_DOTTED_CENTER_BOLD;
							styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP_BOLD;
							styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO_BOLD;
						}
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, i + 1, style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopRegionCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopAreaCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopName(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getBillCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getBillDate(), style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getBillType(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDeadline(), style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDebitAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPayedAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRemainAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getNotexpiredAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getExpiredAmount(), style.get(styleRight));
						rowData++;
					}
					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0; i < start4.size(); i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.flush();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.exportCNPTR3_3"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			lstData.clear();
			lstData = null;
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * BC 3.4 Bao cao cong no phai tra chi tiet
	 * @author trietptm
	 * @return String
	 * @throws Exception 
	 * @since Dev 31, 2015
	 */
	public String exportPayableDetail() throws Exception {
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		List<RptDebitReturnVO> lstData = new ArrayList<RptDebitReturnVO>();
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				String msg = "";
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					}
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportLong(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				Long userId = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				Shop shop = shopMgr.getShopById(shopId);
				lstData = hoReportMgr.reportCnptrCt34(cycleId, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), shopId);
				if (lstData != null && !lstData.isEmpty()) {
					String outputName = ConstantManager.RPT_3_4_REPORT_REVEIVABLE_SUMMARY_FILE_NAME + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("rpt.payable.detail.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultColumnWidth(15);
					//set static Column width
					ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 40, 80, 80, 100, 120, 110, 120, 110, 110, 110, 110, 110, 110, 110, 110);
					sheetData.setDisplayGridlines(false);
					//Title
					ExcelPOIVTICTUtils.setRowHeight(sheetData, 0, 30);
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 0, 0, 12, 0, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.payable.detail.title"), style.get(ExcelPOIVTICTUtils.TITLE_G));
					//info
					int rowTitle = 1;
					int colTitle = 2;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colTitle++, rowTitle, colTitle++, rowTitle, shop == null ? "" : (shop.getShopCode() + " - " + shop.getShopName()), style.get(ExcelPOIVTICTUtils.NORMAL));
					rowTitle++;
					colTitle = 2;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.fromdate"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, fromDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.todate"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, toDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIVTICTUtils.NORMAL));
					
					//header
					String[] listHeader = new String[] {"rpt.payable.detail.header.number", "rpt.payable.detail.header.region", "rpt.payable.detail.header.area", 
							"rpt.payable.detail.header.shop.code", "rpt.payable.detail.header.shop.name", "rpt.payable.detail.header.from.object.number", "rpt.payable.detail.header.invoice.number",
							"rpt.payable.detail.header.invoice.date", "rpt.payable.detail.header.type", "rpt.payable.detail.header.total", "rpt.payable.detail.header.discount.amount",
							"rpt.payable.detail.header.total.pay", "rpt.payable.detail.header.remain", "rpt.payable.detail.header.request.date", "rpt.payable.detail.header.received.date"};
					int rowHeader = rowTitle + 2;
					int colHeader = 0;
					for (int i = 0, n = listHeader.length; i < n; i++) {
						ExcelPOIVTICTUtils.addCell(sheetData, colHeader++, rowHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, listHeader[i]), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					}
					
					// fill data
					int rowData = rowHeader + 1;
					int rowStart = rowData;
					int group4 = 0;
					int group3 = 0;
					int group2 = 0;
					int group1 = 0;
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, n = lstData.size(); i < n; i++) {
						RptDebitReturnVO vo = lstData.get(i);
						int colData = 0;
						
						if (StringUtil.isNullOrEmpty(vo.getShopRegionCode())) {
							vo.setShopRegionCode("Tổng");
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getShopAreaCode())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getShopCode())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getBillCode())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						}
						String styleCenter = ExcelPOIVTICTUtils.ROW_DOTTED_CENTER;
						String styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP;
						String styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO;
						if (StringUtil.isNullOrEmpty(vo.getBillCode())) {
							styleCenter = ExcelPOIVTICTUtils.ROW_DOTTED_CENTER_BOLD;
							styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP_BOLD;
							styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO_BOLD;
						}
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, i + 1, style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopRegionCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopAreaCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopName(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getBillCode(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getInvoiceNumber(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getInvoiceDate(), style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getBillType(), style.get(styleLeft));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDebitAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDiscountAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPayedAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRemainAmount(), style.get(styleRight));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRequestDate(), style.get(styleCenter));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getReceivedDate(), style.get(styleCenter));
						rowData++;
					}
					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0; i < start4.size(); i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.flush();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.exportRawData"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			lstData.clear();
			lstData = null;
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	public List<BeanTest> getLstData() {
		return lstData;
	}

	public void setLstData(List<BeanTest> lstData) {
		this.lstData = lstData;
	}

	public List<CycleVO> getLstCycle() {
		return lstCycle;
	}

	public void setLstCycle(List<CycleVO> lstCycle) {
		this.lstCycle = lstCycle;
	}

	public List<ProductInfo> getLstProductType() {
		return lstProductType;
	}

	public void setLstProductType(List<ProductInfo> lstProductType) {
		this.lstProductType = lstProductType;
	}

	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}

	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.report;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.NumberUtil;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;
import ths.dms.web.utils.report.excel.ExcelPOIVTICTUtils;
import ths.dms.web.utils.report.excel.SXSSFReportHelper;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.KeyShopMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.KS;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ArithmeticEnum;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SpecificGeneral;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.GS_1_4_VO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProgressProgramVO;
import ths.dms.core.entities.vo.RawDataIDPVO;
import ths.dms.core.entities.vo.RawDataStockTransVO;
import ths.dms.core.entities.vo.RptSLDSVO;
import ths.dms.core.entities.vo.TimeKeepingVO;
import ths.core.entities.vo.rpt.DynamicVO;
import ths.core.entities.vo.rpt.Rpt_SLDS1_5;
import ths.dms.core.entities.vo.rpt.ho.RptCTKMVO;
import ths.dms.core.entities.vo.rpt.ho.RptKGTKHTTVO;
import ths.dms.core.entities.vo.rpt.ho.RptTGGTKHVO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_1_DSPSTN_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_2_TKDHGTN_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_3_GHTNVGH_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_4_G_BCDHCR_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_1_CTHHMV;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_2_SSSLDHMH;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_3_CTMH;
import ths.dms.core.entities.vo.rpt.ho.Rpt_5_1_TKKMVO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO_Data;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO_Header;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO_Header_Value;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_GROWTH_SKUs_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_GROWTH_SKUs_VO_Data;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;
import ths.dms.core.report.HoReportMgr;
/**
 * Action bao cao danh muc cho HO
 * 
 * @author hunglm16
 * @since September 18, 2015
 */
public class HOReportAction extends AbstractAction {

	private static final long serialVersionUID = -5774665560263108838L;

	public final int ONE_INT_G = 1;//Dinh nghia cho gia tri Hoat dong voi chuc nang, tat ca voi co hieu bao cao
	public final int ZEZO_INT_G = 0;//Dinh nghia cho gia tri Tam ngung voi chuc nang

	private HoReportMgr hoReportMgr;
	private ProductMgr productMgr;
	ProductInfoMgr productInfoMgr;
	private KeyShopMgr keyShopMgr;

	//Khai bao cac tham bien
	private Boolean checkExport;

	private Long curShopId;
	private Long chooseShopId;
	private Long catId;
	private Long cycleId;
	private Long cycleIdTo;
	private Long subCatId;
	private Long idParentCat;
	private Long ksId;

	private Integer orderSource;
	private Integer approvedStep;
	private Integer approved;
	private Integer typeReport;
	private Integer saleOrderType;
	private Integer year;

	private String shopStr;
	private String promationProgramStr;
	private String date;
	private String fromDate;
	private String toDate;
	private String strListShopId;
	private String npp;
	private String tennpp;
	private String diaChiNPP;
	private String vung;
	private String khuVuc;
	private String orderNumber;
	private String strListNvbhId;
	private String orderType;
	private String categoryId;
	private String strListProductName;
	private String subCategoryId;
	private String subCatIdStr;
	private String catIdStr;
	private String strListProductId;
	private String lstShopId;

	private Date dateSys;
	private Date sysDate;

	private List<CycleVO> lstKpi;
	private List<CycleVO> lstKeyShop;
	private List<CycleVO> lstCycle;
	private List<ProductInfo> lstCategoryType;
	private List<ProductInfo> lstSubCategoryType;
	private List<ProductInfo> lstSubCategoryCat;
	private List<BasicVO> lstYearSysTemConfig;


	@Override
	public void prepare() throws Exception {
		super.prepare();
		hoReportMgr = (HoReportMgr) context.getBean("hoReportMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		keyShopMgr = (KeyShopMgr) context.getBean("keyShopMgr");
	}

	/**
	 * Quy hoach code
	 * 
	 * @author hunglm16
	 * @since 26/09/2015
	 */
	@Override
	public String execute() throws Exception {
		try {
			Cycle cycleSys = cycleMgr.getCycleByCurrentSysdate();
			dateSys = commonMgr.getSysDate();
			//Lay mac dinh cho tu ngay dn ngay
			if (cycleSys != null && cycleSys.getBeginDate() != null) {
				fromDate = DateUtil.toDateString(cycleSys.getBeginDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				fromDate = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			toDate = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
			sysDate = dateSys;
			date = DateUtil.toDateString(dateSys, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (currentUser != null && currentUser.getUserName() != null) {
				shopId = currentUser.getShopRoot().getShopId();
				shopCode = currentUser.getShopRoot().getShopCode();
				//Lay don vi mac dinh
				curShopId = currentUser.getShopRoot().getShopId();
			}
			CycleFilter filter = new CycleFilter();
			filter.setLessYear(2);
			filter.setEndDate(dateSys);
			lstCycle = cycleMgr.getListCycleByFilter(filter);
			//Lay danh sach nganh hang
			lstCategoryType = new ArrayList<ProductInfo>();
			ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfoStock(null, null, null, null, ActiveType.RUNNING, ProductType.CAT);
			if (tmp != null) {
				lstCategoryType = tmp.getLstObject();
			}
			tmp = productInfoMgr.getListProductInfoStock(null, null, null, null, ActiveType.RUNNING, ProductType.SUB_CAT);
			lstSubCategoryType = new ArrayList<ProductInfo>();
			if (tmp != null) {
				lstSubCategoryType = tmp.getLstObject();
			}
			lstYearSysTemConfig = commonMgr.getListYearSysConfig();
			FileUtility.createTempDir();//Xu ly Fix loi POI 
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.execute"), createLogErrorStandard(DateUtil.now()));
		}
		return SUCCESS;
	}

	/**
	 * Bao cao: [1.1] Tong hop don hang phat sinh trong ngay
	 * 
	 * @author hunglm16
	 * @return JSON
	 * @since 18/10/2015
	 * */
	public String report1D1DSPSTN() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			//Xu ly Validate
			String msg = "";
			if (currentUser == null) {
				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
			}
			//Kiem tra ATTT
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
				msg = R.getResource("report.invalid.token");
			}
			//			if (StringUtil.isNullOrEmpty(msg) && cycleId == null) {
			//				msg = R.getResource("common.cycle.undefined");
			//			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
			}
//			if (StringUtil.isNullOrEmpty(msg) && commonMgr.compareDateWithoutTime(fDate, tDate).intValue() > ZEZO_INT_G) {
//				msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
//			}
			if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
				msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				msg = checkMaxNumberDayReportShort(fDate, tDate);	
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(shopStr)) {
				shopStr = String.valueOf(currentUser.getShopRoot().getShopId());
			}
			//Xu ly goi Core lay du lieu
			List<Rpt_1_1_DSPSTN_VO> lstData = hoReportMgr.report1D1DSPSTN(shopStr, cycleId, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());

			if (lstData != null && !lstData.isEmpty()) {
				//Xu ly xuat file excel
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstData", lstData);
				beans.put("tuNgay", fromDate);
				beans.put("denNgay", toDate);
				beans.put("ngayBC", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
				Shop dv = null;
				if (!StringUtil.isNullOrEmpty(shopStr)) {
					String[] arrShop = shopStr.split(",");
					if (arrShop != null && arrShop.length == 1) {
						if (StringUtil.isNumberInt(arrShop[0])) {
							dv = commonMgr.getEntityById(Shop.class, Long.valueOf(arrShop[0]));
							if (dv != null) {
								beans.put("shopReportText", dv.getShopCode() + " - " + dv.getShopName());
							}
//							if (dv != null && dv.getType() != null && dv.getType().getSpecificType() != null && ShopSpecificType.NPP.getValue().equals(dv.getType().getSpecificType().getValue())) {
//								beans.put("maNPP", dv.getShopCode() + " - " + dv.getShopName());
//								if (dv.getParentShop() != null) {
//									beans.put("maVung", dv.getParentShop().getShopCode());
//									if (dv.getParentShop().getParentShop() != null) {
//										beans.put("maMien", dv.getParentShop().getParentShop().getShopCode());
//									}
//								}
//							}
						}
					}
				}
				String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
				String templateFileName = folder + ConstantManager.RPT_1_1_THDHPSTN_TEMPLATE;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = ConstantManager.RPT_1_1_THDHPSTN_FILE_NAME + genExportFileSuffix() + FileExtension.XLSX.getValue();
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(ERROR, false);
				result.put("path", outputPath);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.report.data.null"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.reportXNT1Dot1Equip"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Bao cao: [1.2] Thong ke doin hang trong ngay
	 * 
	 * @author hunglm16
	 * @return JSON
	 * @since 22/10/2015
	 * */
	public String report1D2TKDHGTN() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			//Xu ly Validate
			String msg = "";
			if (currentUser == null) {
				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
			}
			//Kiem tra ATTT
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
				msg = R.getResource("report.invalid.token");
			}
			//			if (StringUtil.isNullOrEmpty(msg) && cycleId == null) {
			//				msg = R.getResource("common.cycle.undefined");
			//			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
				msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				msg = checkMaxNumberDayReportShort(fDate, tDate);
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(shopStr)) {
				shopStr = String.valueOf(currentUser.getShopRoot().getShopId());
			}
			//Xu ly goi Core lay du lieu
			List<Rpt_1_2_TKDHGTN_VO> lstData = hoReportMgr.report1D2TKDHGTN(shopStr, cycleId, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());

			if (lstData != null && !lstData.isEmpty()) {
				//Xu ly xuat file excel
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstData", lstData);
				beans.put("tuNgay", fromDate);
				beans.put("denNgay", toDate);
				beans.put("ngayBC", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
				Shop dv = null;
				if (!StringUtil.isNullOrEmpty(shopStr)) {
					String[] arrShop = shopStr.split(",");
					if (arrShop != null && arrShop.length == 1) {
						if (StringUtil.isNumberInt(arrShop[0])) {
							dv = commonMgr.getEntityById(Shop.class, Long.valueOf(arrShop[0]));
							if (dv != null) {
								beans.put("shopReportText", dv.getShopCode() + " - " + dv.getShopName());
							}
//							if (dv != null && dv.getType() != null && dv.getType().getSpecificType() != null && ShopSpecificType.NPP.getValue().equals(dv.getType().getSpecificType().getValue())) {
//								beans.put("maNPP", dv.getShopCode() + " - " + dv.getShopName());
//								if (dv.getParentShop() != null) {
//									beans.put("maVung", dv.getParentShop().getShopCode());
//									if (dv.getParentShop().getParentShop() != null) {
//										beans.put("maMien", dv.getParentShop().getParentShop().getShopCode());
//									}
//								}
//							}
						}
					}
				}
				String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
				String templateFileName = folder + ConstantManager.RPT_1_2_TKDHGTN_TEMPLATE;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = ConstantManager.RPT_1_2_TKDHGTN_FILE_NAME + genExportFileSuffix() + FileExtension.XLSX.getValue();
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(ERROR, false);
				result.put("path", outputPath);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.report.data.null"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.report1D2TKDHGTN"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	public String checkMaxNumberDayReport(Date fDate, Date tDate, String appCode, String defaultConfig) throws BusinessException {
		String msg = null;
		int sysMaxNumberDay = ZEZO_INT_G;
		ApParam app = apParamMgr.getApParamByCode(ConstantManager.sysMaxNumberDayReportShort, ApParamType.SYS_CONFIG);
		if (app != null && !StringUtil.isNullOrEmpty(app.getValue()) && StringUtil.isNumberInt(app.getValue())) {
			sysMaxNumberDay = Integer.valueOf(app.getValue()).intValue();
		} else {
			if (StringUtil.isNullOrEmpty(defaultConfig) || !StringUtil.isNumberInt(defaultConfig)) {
				msg = R.getResource("common.sysconfig.error.sys.max.number.day.rp.undefined");
			} else {
				sysMaxNumberDay = Integer.valueOf(defaultConfig).intValue();
			}
		}
		if (StringUtil.isNullOrEmpty(msg) && commonMgr.arithmeticForDateByDateWithTrunc(tDate, fDate, ArithmeticEnum.MINUS) >= sysMaxNumberDay) {
			msg = R.getResource("common.compare.error.less.todate.fromdate.with.sysconfig", String.valueOf(sysMaxNumberDay));
		}
		return msg;
	}
	
	
	/**
	 * Bao cao: [1.3] Giao hang (Don hang) theo nhan vien giao hang
	 * 
	 * @author hunglm16
	 * @return JSON
	 * @since 30/10/2015
	 * */
	public String report1D3GHTNVGH() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			//Xu ly Validate
			String msg = "";
			if (currentUser == null) {
				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
			}
			//Kiem tra ATTT
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
				msg = R.getResource("report.invalid.token");
			}
			//			if (StringUtil.isNullOrEmpty(msg) && cycleId == null) {
			//				msg = R.getResource("common.cycle.undefined");
			//			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
				msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				msg = checkMaxNumberDayReportShort(fDate, tDate);
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(shopStr)) {
				shopStr = String.valueOf(currentUser.getShopRoot().getShopId());
			}
			//Xu ly goi Core lay du lieu
			List<Rpt_1_3_GHTNVGH_VO> lstData = hoReportMgr.report1D3GHTNVGH(shopStr, cycleId, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());

			if (lstData != null && !lstData.isEmpty()) {
				//Xu ly xuat file excel
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstData", lstData);
				beans.put("tuNgay", fromDate);
				beans.put("denNgay", toDate);
				beans.put("ngayBC", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
				Shop dv = null;
				if (!StringUtil.isNullOrEmpty(shopStr)) {
					String[] arrShop = shopStr.split(",");
					if (arrShop != null && arrShop.length == 1) {
						if (StringUtil.isNumberInt(arrShop[0])) {
							dv = commonMgr.getEntityById(Shop.class, Long.valueOf(arrShop[0]));
							if (dv != null) {
								beans.put("shopReportText", dv.getShopCode() + " - " + dv.getShopName());
							}
//							if (dv != null && dv.getType() != null && dv.getType().getSpecificType() != null && ShopSpecificType.NPP.getValue().equals(dv.getType().getSpecificType().getValue())) {
//								beans.put("maNPP", dv.getShopCode() + " - " + dv.getShopName());
//								if (dv.getParentShop() != null) {
//									beans.put("maVung", dv.getParentShop().getShopCode());
//									if (dv.getParentShop().getParentShop() != null) {
//										beans.put("maMien", dv.getParentShop().getParentShop().getShopCode());
//									}
//								}
//							}
						}
					}
				}
				String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
				String templateFileName = folder + ConstantManager.RPT_1_3_GHTNVGH_TEMPLATE;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = ConstantManager.RPT_1_3_GHTNVGH_FILE_NAME + genExportFileSuffix() + FileExtension.XLSX.getValue();
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(ERROR, false);
				result.put("path", outputPath);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.report.data.null"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.report1D3GHTNVGH"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao: [1.4] Don hang rot
	 * 
	 * @author hunglm16
	 * @return JSON
	 * @since 30/10/2015
	 * */
	public String report1D4BCDHR() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			//Xu ly Validate
			String msg = "";
			if (currentUser == null) {
				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
			}
			//Kiem tra ATTT
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
				msg = R.getResource("report.invalid.token");
			}
			//			if (StringUtil.isNullOrEmpty(msg) && cycleId == null) {
			//				msg = R.getResource("common.cycle.undefined");
			//			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
				msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				msg = checkMaxNumberDayReportShort(fDate, tDate);
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(shopStr)) {
				shopStr = String.valueOf(currentUser.getShopRoot().getShopId());
			}
			//Xu ly goi Core lay du lieu
			Rpt_1_4_G_BCDHCR_VO data = hoReportMgr.report1D4BCDHR(shopStr, cycleId, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());

			if (data != null && data.getLstData() != null && !data.getLstData().isEmpty()) {
				//Xu ly xuat file excel
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstData", data.getLstData());
				beans.put("tongGiaTriDH", data.getTongGiaTriDH());
				beans.put("tongChietKhau", data.getTongChietKhau());
				beans.put("tongSoTienPSTN", data.getTongSoTienPSTN());
				beans.put("tuNgay", fromDate);
				beans.put("denNgay", toDate);
				beans.put("ngayBC", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
				Shop dv = null;
				if (!StringUtil.isNullOrEmpty(shopStr)) {
					String[] arrShop = shopStr.split(",");
					if (arrShop != null && arrShop.length == 1) {
						if (StringUtil.isNumberInt(arrShop[0])) {
							dv = commonMgr.getEntityById(Shop.class, Long.valueOf(arrShop[0]));
							if (dv != null) {
								beans.put("shopReportText", dv.getShopCode() + " - " + dv.getShopName());
							}
//							if (dv != null && dv.getType() != null && dv.getType().getSpecificType() != null && ShopSpecificType.NPP.getValue().equals(dv.getType().getSpecificType().getValue())) {
//								beans.put("maNPP", dv.getShopCode() + " - " + dv.getShopName());
//								if (dv.getParentShop() != null) {
//									beans.put("maVung", dv.getParentShop().getShopCode());
//									if (dv.getParentShop().getParentShop() != null) {
//										beans.put("maMien", dv.getParentShop().getParentShop().getShopCode());
//									}
//								}
//							}
						}
					}
				}
				String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
				String templateFileName = folder + ConstantManager.RPT_1_4_BCDHR_TEMPLATE;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = ConstantManager.RPT_1_4_BCDHR_FILE_NAME + genExportFileSuffix() + FileExtension.XLSX.getValue();
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(ERROR, false);
				result.put("path", outputPath);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.report.data.null"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.report1D4BCDHR"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Bao cao tong ket khuyen mai
	 * 
	 * @author hunglm16
	 * @since September 20, 2015
	 * */
	public String reportKM5D1TKKM() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			//Xu ly Validate
			String msg = "";
			if (currentUser == null) {
				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
			}
			//Kiem tra ATTT
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
				msg = R.getResource("report.invalid.token");
			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(shopStr)) {
				shopStr = String.valueOf(currentUser.getShopRoot().getShopId());
			}
			
			shopId = Long.valueOf(shopStr);
			if (super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
				return JSON;
			}
			
			if (StringUtil.isNullOrEmpty(promationProgramStr)) {
				promationProgramStr = "";
			}
			//Xu ly goi Core lay du lieu
			List<Rpt_5_1_TKKMVO> lstData = hoReportMgr.reportKM5D1TKKM(shopStr, promationProgramStr, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());

			if (lstData != null && !lstData.isEmpty()) {
				//Xu ly xuat file excel
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstData", lstData);
				beans.put("ngayBC", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
				beans.put("tuNgay", fromDate);
				beans.put("denNgay", toDate);
				Shop dv = commonMgr.getEntityById(Shop.class, shopId);
				if (dv != null) {
					beans.put("shopReportText", dv.getShopCode() + " - " + dv.getShopName());
				}
				String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
				String templateFileName = folder + ConstantManager.RPT_KM_5_1_TKKM_TEMPLATE;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = ConstantManager.RPT_KM_5_1_TKKM_FILE_NAME + genExportFileSuffix() + FileExtension.XLSX.getValue();
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(ERROR, false);
				result.put("path", outputPath);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.reportXNT1Dot1Equip"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Bao cao san luong doanh so 2.6 Danh sach don hang
	 * 
	 * @author hoanv25
	 * @since July 21/2015
	 * @return
	 * @throws Exception
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach lai Action
	 */
	public String exportBCSLDS26() throws Exception {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);

				List<GS_1_4_VO> lstData = hoReportMgr.exportBCSLDS26(staffIdRoot, userId, strListShopId, strListNvbhId, orderNumber, orderType, orderSource, approved, approvedStep, saleOrderType, cycle.getId(), fDate, tDate);

				//************************************************************************
				String outputName = ConstantManager.EXPORT_2_6_SLDS + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.2.6.slds.shet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(23);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 130, 130, 130, 150, 150, 200, 150, 150, 150, 180, 100, 100, 100, 150, 200, 150, 100, 180, 180, 150, 180, 180, 100, 200, 150, 150, 100, 100, 150, 150, 100, 150);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 25, 15, 15, 15, 15);
				sheetData.setDisplayGridlines(false);
				//Tittle
				String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.slds.title.name");
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
				//info					
				ExcelPOIProcessUtils.addCell(sheetData, 3, 4, vung, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 4, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 5, npp, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 5, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 5, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 5, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 6, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL));
				//header
				ExcelPOIProcessUtils.addCell(sheetData, 2, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 9, 0, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, 9, 1, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 2, 9, 2, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 3, 9, 3, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 4, 9, 4, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 5, 9, 5, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.mausm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 6, 9, 6, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenusm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 7, 9, 7, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.manvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 8, 9, 8, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 9, 9, 9, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.maroute"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 10, 9, 10, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenroute"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 11, 9, 11, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.ngaydathang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 12, 9, 12, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.ngayduyetdonhang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 13, 9, 13, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.makh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 14, 9, 14, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenkh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 15, 9, 15, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 16, 9, 16, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.sdh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 17, 9, 17, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.loaidh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 18, 9, 18, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.tt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 19, 9, 19, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.status"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 20, 9, 20, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.gtdh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 21, 9, 21, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.chietkhau"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 22, 9, 22, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.thanhtientruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 23, 9, 23, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.thanhtiensauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 24, 9, 32, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.ttsptdh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 24, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.masp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 25, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tensp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 26, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.giatruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 27, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.giasauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 28, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 29, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluongle"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 30, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.thanhtientruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 31, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.thanhtiensauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 32, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.loai"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 33, 9, 33, 10, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.6.dsdh.header.ghichu"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				int d = 11;
				int sdh = 0;
				int dhnv = 0;
				int dhgs = 0;
				int dhnpp = 0;
				int dhkv = 0;
				int dhv = 0;
				if (lstData.size() > 0) {
					GS_1_4_VO salesVO = null;
					int rowStart = d;
					int group6 = 0, group5 = 0, group4 = 0, group3 = 0, group2 = 0, group1 = 0;
					List<Integer> start6 = new ArrayList<Integer>();
					List<Integer> end6 = new ArrayList<Integer>();
					List<Integer> start5 = new ArrayList<Integer>();
					List<Integer> end5 = new ArrayList<Integer>();
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						if (salesVO.getSoDH() != null && !salesVO.getSoDH().equals(lstData.get(i + 1).getSoDH())) {
							sdh = sdh + 1;
							dhnv = dhnv + 1;
							dhgs = dhgs + 1;
							dhnpp = dhnpp + 1;
							dhkv = dhkv + 1;
							dhv = dhv + 1;
						} else {
							sdh = sdh + 0;
							dhnv = dhnv + 0;
							dhgs = dhgs + 0;
							dhnpp = dhnpp + 0;
							dhkv = dhkv + 0;
							dhv = dhv + 0;
						}
						if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
							start6.add(group6 + rowStart);
							end6.add(i + rowStart - 1);
							group6 = i + 1;
							group5++;
							group4++;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaKhuVuc())) {
							start5.add(group5 + rowStart);
							end5.add(i + rowStart - 1);
							group5 = i + 1;
							group4++;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNPP())) {
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaGSNPP())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNVBH())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getSoDH())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						}
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
							if (salesVO.getSoDH() != null) {
								if (salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) {
									ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
									ExcelPOIProcessUtils.addCell(sheetData, 16, d, sdh, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								}
								ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								if (salesVO.getMaGSNPP() != null && salesVO.getMaGSNPP().equals("xxx196")) {
									ExcelPOIProcessUtils.addCell(sheetData, 5, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								}
								if (salesVO.getTenGSNPP() != null && salesVO.getTenGSNPP().equals("xxx674")) {
									ExcelPOIProcessUtils.addCell(sheetData, 6, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								}
								ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getNgayDatHang(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getNgayDuyetDon(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getTenKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getSoDH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getLoaiDH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getTaoTren(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getTrangThai(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));

								ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getGiaTriDH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, 21, d, salesVO.getChietKhau(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							} else {
								if (salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) {
									ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
									ExcelPOIProcessUtils.addCell(sheetData, 16, d, sdh, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								}
								ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								if (salesVO.getMaGSNPP() != null && salesVO.getMaGSNPP().equals("xxx196")) {
									ExcelPOIProcessUtils.addCell(sheetData, 5, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								}
								if (salesVO.getTenGSNPP() != null && salesVO.getTenGSNPP().equals("xxx674")) {
									ExcelPOIProcessUtils.addCell(sheetData, 6, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								}
								ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getNgayDatHang(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getNgayDuyetDon(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getTenKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getLoaiDH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getTaoTren(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getTrangThai(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getDoanhSoSauVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 21, d, salesVO.getSlchietKhau(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							}
						} else {
							if (salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 16, d, sdh, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 5, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 6, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 9, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 13, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 14, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 15, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 18, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 19, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 20, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 21, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
							if (salesVO.getSoDH() != null) {
								ExcelPOIProcessUtils.addCell(sheetData, 22, d, salesVO.getSldoanhSoTruocVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								ExcelPOIProcessUtils.addCell(sheetData, 23, d, salesVO.getThanhTienSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 22, d, salesVO.getSldoanhSoTruocVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 23, d, salesVO.getThanhTienSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							}
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 22, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 23, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						ExcelPOIProcessUtils.addCell(sheetData, 24, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 25, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));

						if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "" || salesVO.getGiaChuaThue() == null || salesVO.getGiaChuaThue().signum() == 0) {
							ExcelPOIProcessUtils.addCell(sheetData, 26, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 26, d, salesVO.getGiaChuaThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "" || salesVO.getGiaSauThue() == null || salesVO.getGiaSauThue().signum() == 0) {
							ExcelPOIProcessUtils.addCell(sheetData, 27, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 27, d, salesVO.getGiaSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
							ExcelPOIProcessUtils.addCell(sheetData, 28, d, salesVO.getSlgiaTriDH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 29, d, salesVO.getSlsoDH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 30, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 31, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 32, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 33, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 28, d, salesVO.getThung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 29, d, salesVO.getLe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							if (salesVO.getDoanhSoTruocVAT() == null || salesVO.getDoanhSoTruocVAT().signum() == 0) {
								ExcelPOIProcessUtils.addCell(sheetData, 30, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 30, d, salesVO.getDoanhSoTruocVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getDoanhSoSauVAT() == null || salesVO.getDoanhSoSauVAT().signum() == 0) {
								ExcelPOIProcessUtils.addCell(sheetData, 31, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 31, d, salesVO.getDoanhSoSauVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 32, d, salesVO.getLoai(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 33, d, salesVO.getGhiChu(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
						if (salesVO.getMaNVBH() == null || !salesVO.getMaNVBH().equals(lstData.get(i + 1).getMaNVBH())) {
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, dhnv, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							dhnv = 0;
						}
						if (salesVO.getMaGSNPP() == null || !salesVO.getMaGSNPP().equals(lstData.get(i + 1).getMaGSNPP())) {
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, dhgs, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							dhgs = 0;
						}
						if (salesVO.getMaNPP() == null || !salesVO.getMaNPP().equals(lstData.get(i + 1).getMaNPP())) {
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, dhnpp, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							dhnpp = 0;
						}
						if (salesVO.getMaKhuVuc() == null || !salesVO.getMaKhuVuc().equals(lstData.get(i + 1).getMaKhuVuc())) {
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, dhkv, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							dhkv = 0;
						}
						if (salesVO.getMaVung() != null && !salesVO.getMaVung().equals(lstData.get(i + 1).getMaVung())) {
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, dhv, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							dhv = 0;
						}
						if (salesVO.getMaVung() == null) {
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, sdh, style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
						}
						d++;
					}

					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0; i < start4.size(); i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					for (int i = 0; i < start5.size(); i++) {
						sheetData.groupRow(start5.get(i), end5.get(i));
					}
					for (int i = 0; i < start6.size(); i++) {
						sheetData.groupRow(start6.get(i), end6.get(i));
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					/*
					 * out.close(); workbook.dispose();
					 */
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				//************************************************************************				
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportBCSLDS26", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}

	/**
	 * Lay thong tin cho cua don vi
	 * 
	 * @modify hunglm16
	 * @param shop
	 * @since 26/09/2015
	 * @description Quy hoach lai Action
	 */
	public void getInfoParentShop(Shop shop) {
		Shop shop1 = null, shop2 = null, shop3 = null, shop4 = null;
		if (shop != null) {
			shop1 = shop.getParentShop();
			if (shop1 != null) {
				shop2 = shop1.getParentShop();
				if (shop2 != null) {
					shop3 = shop2.getParentShop();
					if (shop3 != null) {
						shop4 = shop3.getParentShop();
					}
				}
			}
		}
		if (shop1 != null && shop2 != null && shop3 != null && shop4 != null) {//npp
			npp = shop.getShopCode();
			tennpp = shop.getShopName();
			diaChiNPP = shop.getAddress();
			khuVuc = shop1.getShopCode();
			vung = shop2.getShopCode();
		} else if (shop1 != null && shop2 != null && shop3 != null && shop4 == null) {//vung
			khuVuc = shop.getShopCode();
			vung = shop1.getShopCode();
		} else if (shop1 != null && shop2 != null && shop3 == null) {//khu vuc
			vung = shop.getShopCode();
		}
	}

	/**
	 * Bao cao san luong doanh so 2.4 - Theo doi KPI
	 * 
	 * @author hoanv25
	 * @since July 24/2015
	 * @return
	 * @throws Exception
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach lai Action
	 */
	public String exportSLDS2_4_TDKPI() throws Exception {
		Date startLogDate = DateUtil.now();
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Cycle cycle = cycleMgr.getCycleById(cycleId);
				if (cycle == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "kpi.cycle.not.exist"));
					return JSON;
				}
				Long userId = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<GS_1_4_VO> lstData = hoReportMgr.exportBCSLDS24(userId, roleId, strListShopId, cycle.getId());
				if (lstData != null && lstData.size() > 0) {
					String outputName = ConstantManager.EXPORT_2_4_SLDS + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.2.4.slds.shet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					//sheetData.setDefaultRowHeight((short) (15 * 30));
					sheetData.setDefaultColumnWidth(15);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 200, 150, 200, 150, 150, 130);
					//Size Row
					ExcelPOIProcessUtils.setRowHeight(sheetData, 0, 30);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.4.slds.title.name");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 10, 0, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					//info
					Shop shop = shopMgr.getShopById(shopId);
					ExcelPOIProcessUtils.addCell(sheetData, 4, 1, shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 1, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 8, 1, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));

					//header
					ExcelPOIProcessUtils.addCell(sheetData, 3, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.shop"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 7, 1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					//header detail
					ExcelPOIProcessUtils.setRowHeight(sheetData, 3, 20);
					ExcelPOIProcessUtils.setRowHeight(sheetData, 4, 20);
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 3, 0, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, 3, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.mien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 2, 3, 2, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 3, 3, 3, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 4, 3, 4, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 5, 3, 5, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.magsbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 6, 3, 6, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tengsbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 7, 3, 7, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.manvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 8, 3, 8, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					lstKpi = hoReportMgr.getHeaderBCSLDS24(String.valueOf(cycleId));
					int colki = 9;
					int col = 9;
					if (lstKpi != null && lstKpi.size() > 0) {
						for (int i = 0, sz = lstKpi.size(); i < sz; i++) {
							ExcelPOIProcessUtils.addCellsAndMerged(sheetData, colki, 3, colki + 2, 3, lstKpi.get(i).getCycleCode(), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
							ExcelPOIProcessUtils.addCell(sheetData, col, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.4.dsdh.header.kehoach"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
							ExcelPOIProcessUtils.addCell(sheetData, col + 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.4.dsdh.header.thuchien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
							ExcelPOIProcessUtils.addCell(sheetData, col + 2, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.4.dsdh.header.tiendo"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
							colki = colki + 3;
							col = col + 3;
						}
					}
					int d = 5;
					GS_1_4_VO salesVO = null;
					int rowStart = d;
					int group5 = 0, group4 = 0, group3 = 0, group2 = 0, group1 = 0;
					List<Integer> start5 = new ArrayList<Integer>();
					List<Integer> end5 = new ArrayList<Integer>();
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						//						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);						
						if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
							start5.add(group5 + rowStart);
							end5.add(i + rowStart - 1);
							group5 = i + 1;
							group4++;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaKhuVuc())) {
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNPP())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaGSNPP())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNVBH())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						}
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						if (salesVO.getMaTuyen() == null) {
							if ((salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) && salesVO.getMaNPP() == null && salesVO.getMaNVBH() == null) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							if (salesVO.getMaGSNPP() != null && salesVO.getMaGSNPP().equals("xxx196")) {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							if (salesVO.getTenGSNPP() != null && salesVO.getTenGSNPP().equals("xxx674")) {
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
//							if (salesVO.getMaTuyen() != null && salesVO.getMaTuyen().equals("xxx567")) {
//								ExcelPOIProcessUtils.addCell(sheetData, 9, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
//							} else {
//								ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
//							}
//							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							int cd = 9;
							if (salesVO.getLstColumns() != null && salesVO.getLstColumns().size() > 0) {
								for (int j = 0, sz = salesVO.getLstColumns().size(); j < sz; j++) {
									int k = j + 1;
									if (salesVO.getMaTuyen() == null || salesVO.getMaTuyen().length() <= 0) {
										if (k % 3 != 0) {
											ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT_BOLD_TWO));
										} else {
											// Tien do (cot 3) thi cho %
											ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_PERCENT_BOLD));
										}
									} else {
										if (k % 3 != 0) {
											ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT_TWO));
										} else {
											//Tien do (cot 3) thi cho %
											ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_PERCENT));
										}
									}
									cd++;
								}
							}
						} else {
							if ((salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) && salesVO.getMaNPP() == null && salesVO.getMaNVBH() == null) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							if (salesVO.getMaGSNPP() != null && salesVO.getMaGSNPP().equals("xxx196")) {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							if (salesVO.getTenGSNPP() != null && salesVO.getTenGSNPP().equals("xxx674")) {
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							if (salesVO.getMaTuyen() != null && salesVO.getMaTuyen().equals("xxx567")) {
//								ExcelPOIProcessUtils.addCell(sheetData, 9, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							} else {
//								ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
//							}
//							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							int cd = 9;
							if (salesVO.getLstColumns() != null && salesVO.getLstColumns().size() > 0) {
								for (int j = 0, sz = salesVO.getLstColumns().size(); j < sz; j++) {
									Double dab = null;
									int k = j + 1;
									if (salesVO.getLstColumns().get(j).getValue() != null) {
										dab = ((BigDecimal) salesVO.getLstColumns().get(j).getValue()).doubleValue();
									}
									if (salesVO.getMaTuyen() == null || salesVO.getMaTuyen().length() <= 0) {
										if (SXSSFReportHelper.checkDecimal(dab) == true) {
											if (k % 3 != 0) {
												ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT_BOLD_TWO));
											} else {
												// Tien do (cot 3) thi cho %
												ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_PERCENT_BOLD));
											}
										} else {
											if (k % 3 != 0) {
												ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
											} else {
												// Tien do (cot 3) thi cho %
												ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_PERCENT_BOLD));
											}
										}
									} else {
										if (SXSSFReportHelper.checkDecimal(dab) == true) {
											if (k % 3 != 0) {
												ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT_TWO));
											} else {
												// Tien do (cot 3) thi cho %
												ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_PERCENT));
											}
										} else {
											if (k % 3 != 0) {
												ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
											} else {
												// Tien do (cot 3) thi cho %
												ExcelPOIProcessUtils.addCell(sheetData, cd, d, salesVO.getLstColumns().get(j).getValue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_PERCENT));
											}
										}
									}
									cd++;
								}
							}
						}
						d++;
					}

					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0; i < start4.size(); i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					for (int i = 0; i < start5.size(); i++) {
						sheetData.groupRow(start5.get(i), end5.get(i));
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				//************************************************************************				
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.report.HOReportAction.exportSLDS2_4_TDKPI()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao san luong doanh so theo Khach Hang 2.3
	 * 
	 * @author hoanv25
	 * @since July 20/2015
	 * @return
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach lai Action
	 */
	public String exportBCSLDS23() {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);

				List<GS_1_4_VO> lstData = hoReportMgr.exportBCSLDS23(staffIdRoot, userId, strListShopId, fDate, tDate, categoryId, subCategoryId, strListProductId, cycle.getId());

				//************************************************************************
				String outputName = ConstantManager.EXPORT_2_3_SLDS + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.2.3.slds.shet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(23);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 150, 100, 100, 150, 150, 200, 150, 150, 150, 180, 180, 180, 150, 200, 100, 100, 250, 100, 100, 100, 100, 100, 180, 180);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 25, 15, 15, 15, 15);
				sheetData.setDisplayGridlines(false);
				//Tittle
				String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.slds.title.name");
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
				//info					
				ExcelPOIProcessUtils.addCell(sheetData, 3, 4, vung, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 4, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 5, npp, style.get(ExcelPOIProcessUtils.NORMAL));
				if (categoryId != null) {
					ProductInfo proIn = productMgr.getProductInfoById(new Long(categoryId));
					ExcelPOIProcessUtils.addCell(sheetData, 7, 4, proIn != null ? proIn.getProductInfoName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				}
				if (subCategoryId != null) {
					ProductInfo proSubIn = productMgr.getProductInfoById(new Long(subCategoryId));
					ExcelPOIProcessUtils.addCell(sheetData, 9, 4, proSubIn != null ? proSubIn.getProductInfoName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				}
				if (strListProductName != null && strListProductName != "" && strListProductName.length() > 0) {
					ExcelPOIProcessUtils.addCell(sheetData, 11, 4, strListProductName, style.get(ExcelPOIProcessUtils.NORMAL));
				}
				ExcelPOIProcessUtils.addCell(sheetData, 5, 5, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 5, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 5, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 6, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL));
				//header
				ExcelPOIProcessUtils.addCell(sheetData, 2, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nganhhang"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nganhhangcon"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.sanpham"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

				ExcelPOIProcessUtils.addCell(sheetData, 0, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.mausm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenusm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.manvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.maroute"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenroute"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 11, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.makh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 12, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenkh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 13, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.keyshop"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 14, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.muc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 15, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 16, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.masp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 17, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tensp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				/*
				 * ExcelPOIProcessUtils.addCell(sheetData, 13, 9,
				 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
				 * "baocao.2.2.kqdt.header.target"),
				 * style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				 * ExcelPOIProcessUtils.addCell(sheetData, 14, 9,
				 * Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
				 * "baocao.2.2.kqdt.header.targetdoanhso"),
				 * style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				 */
				ExcelPOIProcessUtils.addCell(sheetData, 18, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.giatruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 19, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.giasauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 20, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.3.kqdt.header.soluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 22, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluongle"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 21, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 23, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.doanhsotruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 24, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.doanhsosauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				int d = 10;
				if (lstData.size() > 0) {
					GS_1_4_VO salesVO = null;
					int rowStart = d;
					int group6 = 0, group5 = 0, group4 = 0, group3 = 0, group2 = 0, group1 = 0;
					List<Integer> start6 = new ArrayList<Integer>();
					List<Integer> end6 = new ArrayList<Integer>();
					List<Integer> start5 = new ArrayList<Integer>();
					List<Integer> end5 = new ArrayList<Integer>();
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
							start6.add(group6 + rowStart);
							end6.add(i + rowStart - 1);
							group6 = i + 1;
							group5++;
							group4++;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaKhuVuc())) {
							start5.add(group5 + rowStart);
							end5.add(i + rowStart - 1);
							group5 = i + 1;
							group4++;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNPP())) {
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaGSNPP())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNVBH())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaTuyen())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						} else {
							/*
							 * left =
							 * style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT);
							 * right =
							 * style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT);
							 * rightRed =
							 * style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_RED
							 * ); center =
							 * style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER
							 * );
							 */
						}
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						if (salesVO.getMaKH() == null || salesVO.getMaKH().length() <= 0 || salesVO.getMaKH() == "") {
							if (salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							if (salesVO.getMaGSNPP() != null && salesVO.getMaGSNPP().equals("xxx196")) {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							if (salesVO.getTenGSNPP() != null && salesVO.getTenGSNPP().equals("xxx674")) {
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							if (salesVO.getMaTuyen() != null && salesVO.getMaTuyen().equals("123xxx")) {
								ExcelPOIProcessUtils.addCell(sheetData, 9, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getTenKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getKeyShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getMuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
						} else {
							if (salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							if (salesVO.getMaGSNPP() != null && salesVO.getMaGSNPP().equals("xxx196")) {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							if (salesVO.getTenGSNPP() != null && salesVO.getTenGSNPP().equals("xxx674")) {
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							if (salesVO.getMaTuyen() != null && salesVO.getMaTuyen().equals("123xxx")) {
								ExcelPOIProcessUtils.addCell(sheetData, 9, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getTenKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getKeyShop(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getMuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
						if (salesVO.getMaKH() == null || salesVO.getMaKH().length() <= 0 || salesVO.getMaKH() == "" || salesVO.getGiaChuaThue() == null || salesVO.getGiaChuaThue().signum() == 0) {
							ExcelPOIProcessUtils.addCell(sheetData, 18, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getGiaChuaThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getMaKH() == null || salesVO.getMaKH().length() <= 0 || salesVO.getMaKH() == "" || salesVO.getGiaSauThue() == null || salesVO.getGiaSauThue().signum() == 0) {
							ExcelPOIProcessUtils.addCell(sheetData, 19, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getGiaSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getMaKH() == null || salesVO.getMaKH().length() <= 0 || salesVO.getMaKH() == "") {
							ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getSlqd(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getSoLuong(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getMaKH() == null || salesVO.getMaKH().length() <= 0 || salesVO.getMaKH() == "") {
							ExcelPOIProcessUtils.addCell(sheetData, 21, d, salesVO.getSlthung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 21, d, salesVO.getThung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getMaKH() == null || salesVO.getMaKH().length() <= 0 || salesVO.getMaKH() == "") {
							ExcelPOIProcessUtils.addCell(sheetData, 22, d, salesVO.getSlle(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 22, d, salesVO.getLe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						if (salesVO.getMaKH() == null || salesVO.getMaKH().length() <= 0 || salesVO.getMaKH() == "") {
							ExcelPOIProcessUtils.addCell(sheetData, 23, d, salesVO.getSldoanhSoTruocVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT_BOLD_TWO));
						} else {
							Double dab = null;
							if (salesVO.getDoanhSoTruocVAT() != null) {
								dab = ((BigDecimal) salesVO.getDoanhSoTruocVAT()).doubleValue();
							}
							if (SXSSFReportHelper.checkDecimal(dab) == true) {
								if (salesVO.getDoanhSoTruocVAT() == null || salesVO.getDoanhSoTruocVAT().signum() == 0) {
									ExcelPOIProcessUtils.addCell(sheetData, 23, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 23, d, salesVO.getDoanhSoTruocVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT));
								}
							} else {
								if (salesVO.getDoanhSoTruocVAT() == null || salesVO.getDoanhSoTruocVAT().signum() == 0) {
									ExcelPOIProcessUtils.addCell(sheetData, 23, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 23, d, salesVO.getDoanhSoTruocVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								}
							}
						}
						if (salesVO.getMaKH() == null || salesVO.getMaKH().length() <= 0 || salesVO.getMaKH() == "") {
							ExcelPOIProcessUtils.addCell(sheetData, 24, d, salesVO.getSldoanhSoSauVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT_BOLD_TWO));
						} else {
							Double da = null;
							if (salesVO.getDoanhSoSauVAT() != null) {
								da = ((BigDecimal) salesVO.getDoanhSoSauVAT()).doubleValue();
							}
							if (SXSSFReportHelper.checkDecimal(da) == true) {
								if (salesVO.getDoanhSoSauVAT() == null || salesVO.getDoanhSoSauVAT().signum() == 0) {
									ExcelPOIProcessUtils.addCell(sheetData, 24, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 24, d, salesVO.getDoanhSoSauVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT));
								}
							} else {
								if (salesVO.getDoanhSoSauVAT() == null || salesVO.getDoanhSoSauVAT().signum() == 0) {
									ExcelPOIProcessUtils.addCell(sheetData, 24, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 24, d, salesVO.getDoanhSoSauVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								}
							}
						}
						d++;
					}

					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0; i < start4.size(); i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					for (int i = 0; i < start5.size(); i++) {
						sheetData.groupRow(start5.get(i), end5.get(i));
					}
					for (int i = 0; i < start6.size(); i++) {
						sheetData.groupRow(start6.get(i), end6.get(i));
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				//************************************************************************				
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportBCSLDS23", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao San luong doanh so cua Khach Hang
	 * @author vuongmq
	 * @return String
	 * @since 26/10/2015
	 */
	public String exportBCSLDSKH() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Cycle cycle = cycleMgr.getCycleById(cycleId);
				if (cycle == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.cycle.undefined"));
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<RptSLDSVO> lstData = hoReportMgr.exportBCSLDSKH(staffIdRoot, roleId, strListShopId, fDate, tDate, categoryId, subCategoryId, strListProductId, cycle.getId());
				if (lstData != null && !lstData.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					Shop shop = shopMgr.getShopById(shopId);
					beans.put("lstData", lstData);
					beans.put("shopSelect", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("cycleName", cycle.getCycleName());
					beans.put("fromDate", fromDate);
					beans.put("toDate", toDate);
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					if (categoryId != null) {
						ProductInfo proIn = productMgr.getProductInfoById(new Long(categoryId));
						beans.put("nganhHang", proIn != null ? proIn.getProductInfoName() : "");
					}
					if (subCategoryId != null) {
						ProductInfo proSubIn = productMgr.getProductInfoById(new Long(subCategoryId));
						beans.put("nganhHangCon", proSubIn != null ? proSubIn.getProductInfoName() : "");
					}
					if (!StringUtil.isNullOrEmpty(strListProductName)) {
						beans.put("SKU", strListProductName);
					}
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.EXPORT_SLDS_KH_TEMPLATE;
					String fileName = ConstantManager.EXPORT_SLDS_KH_FILENAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
	
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
	
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportBCSLDSKH()", createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Bao cao san luong doanh so theo NVBH 2.2
	 * 
	 * @author hoanv25
	 * @since July 15/2015
	 * @return
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach lai Action
	 */
	public String exportBCSLDS22() {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (chooseShopId != null) {
					if (super.getMapShopChild().get(chooseShopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(chooseShopId);
//				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);
				if (currentUser.getUserId() != null && currentUser.getRoleToken() != null && currentUser.getRoleToken().getRoleId() != null) {
					List<GS_1_4_VO> lstData = hoReportMgr.exportBCSLDS22(currentUser.getUserId(), currentUser.getRoleToken().getRoleId(), chooseShopId, fDate, tDate, categoryId, subCategoryId, strListProductId, cycle.getId());

					//************************************************************************
					String outputName = ConstantManager.EXPORT_2_2_SLDS + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.2.2.slds.shet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(23);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 150, 100, 100, 150, 150, 150, 180, 250, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100);
					//Size Row
					ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 20, 15, 15, 15, 15, 15);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.slds.title.name");
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 13, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					//info					
					ExcelPOIProcessUtils.addCell(sheetData, 5, 4, shop.getShopCode() + " - " + shop.getShopName(), style.get(ExcelPOIProcessUtils.NORMAL));
					if (categoryId != null) {
						ProductInfo proIn = productMgr.getProductInfoById(new Long(categoryId));
						ExcelPOIProcessUtils.addCell(sheetData, 7, 4, proIn != null ? proIn.getProductInfoName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					}
					if (subCategoryId != null) {
						ProductInfo proSubIn = productMgr.getProductInfoById(new Long(subCategoryId));
						ExcelPOIProcessUtils.addCell(sheetData, 9, 4, proSubIn != null ? proSubIn.getProductInfoName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					}
					if (strListProductName != null && strListProductName != "" && strListProductName.length() > 0) {
						ExcelPOIProcessUtils.addCell(sheetData, 11, 4, strListProductName, style.get(ExcelPOIProcessUtils.NORMAL));
					}
					ExcelPOIProcessUtils.addCell(sheetData, 5, 5, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 7, 5, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 9, 5, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
					//header
					ExcelPOIProcessUtils.addCell(sheetData, 4, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.shop"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 8, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nganhhang"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 8, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nganhhangcon"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 10, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.sanpham"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

					int rowStartHeader = 7;
					int rowEndHeader = 8;
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, rowStartHeader, 0, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, rowStartHeader, 1, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.mien"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 2, rowStartHeader, 2, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 3, rowStartHeader, 3, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 4, rowStartHeader, 4, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 5, rowStartHeader, 5, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.manvbh"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 6, rowStartHeader, 6, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nvbh"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 7, rowStartHeader, 7, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.masp"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 8, rowStartHeader, 8, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tensp"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 9, rowStartHeader, 9, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.dongia"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					//tong
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 10, rowStartHeader, 13, rowStartHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.total"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 10, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 11, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluongle"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 12, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluongleqd"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 13, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.thanhtien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					//da duyet
					ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 14, rowStartHeader, 17, rowStartHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.approved"), style
							.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 14, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 15, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluongle"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 16, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.sanluongleqd"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					ExcelPOIProcessUtils.addCell(sheetData, 17, rowEndHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.2.kqdt.header.thanhtien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					int d = 9;
					if (lstData.size() > 0) {
						GS_1_4_VO salesVO = null;
						int rowStart = d;
						int group5 = 0;
						int group4 = 0;
						int group3 = 0;
						int group2 = 0;
						int group1 = 0;
						List<Integer> start5 = new ArrayList<Integer>();
						List<Integer> end5 = new ArrayList<Integer>();
						List<Integer> start4 = new ArrayList<Integer>();
						List<Integer> end4 = new ArrayList<Integer>();
						List<Integer> start3 = new ArrayList<Integer>();
						List<Integer> end3 = new ArrayList<Integer>();
						List<Integer> start2 = new ArrayList<Integer>();
						List<Integer> end2 = new ArrayList<Integer>();
						List<Integer> start1 = new ArrayList<Integer>();
						List<Integer> end1 = new ArrayList<Integer>();
						for (int i = 0, size = lstData.size(); i < size; i++) {
							salesVO = lstData.get(i);
							ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
							if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
								start5.add(group5 + rowStart);
								end5.add(i + rowStart - 1);
								group5 = i + 1;
								group4++;
								group3++;
								group2++;
								group1++;
							} else if (StringUtil.isNullOrEmpty(salesVO.getMaKhuVuc())) {
								start4.add(group4 + rowStart);
								end4.add(i + rowStart - 1);
								group4 = i + 1;
								group3++;
								group2++;
								group1++;
							} else if (StringUtil.isNullOrEmpty(salesVO.getMaNPP())) {
								start3.add(group3 + rowStart);
								end3.add(i + rowStart - 1);
								group3 = i + 1;
								group2++;
								group1++;
							} else if (StringUtil.isNullOrEmpty(salesVO.getMaNVBH())) {
								start2.add(group2 + rowStart);
								end2.add(i + rowStart - 1);
								group2 = i + 1;
								group1++;
							} else if (StringUtil.isNullOrEmpty(salesVO.getMaSP())) {
								start1.add(group1 + rowStart);
								end1.add(i + rowStart - 1);
								group1 = i + 1;
							}
							ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								if (salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) {
									ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								}
								ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								if (salesVO.getMaVung() == null || salesVO.getMaVung() == "" || salesVO.getMaVung().length() <= 0) {
									ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								} else {
									ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								}
								ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
								ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "" || salesVO.getGiaSauThue() == null || salesVO.getGiaSauThue().signum() == 0) {
								ExcelPOIProcessUtils.addCell(sheetData, 9, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getGiaSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							//tong
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getSumQuantityPackage(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getQuantityPackage(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getSumQuantityRetail(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getQuantityRetail(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getSumQuantity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getQuantity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getSumAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								//								if (salesVO.getAmount() == null || salesVO.getAmount().signum() == 0) {
								//									ExcelPOIProcessUtils.addCell(sheetData, 13, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								//								} else {
								//									ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								//								}
							}
							//da duyet
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getSlthung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getThung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getSlle(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getLe(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getSlqd(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getLeDoi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							}
							if (salesVO.getMaSP() == null || salesVO.getMaSP().length() <= 0 || salesVO.getMaSP() == "") {
								ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getSldoanhSoSauVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getDoanhSoSauVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								//								if (salesVO.getDoanhSoSauVAT() == null || salesVO.getDoanhSoSauVAT().signum() == 0) {
								//									ExcelPOIProcessUtils.addCell(sheetData, 17, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								//								} else {
								//									ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getDoanhSoSauVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
								//								}
							}
							d++;
						}

						for (int i = 0; i < start1.size(); i++) {
							sheetData.groupRow(start1.get(i), end1.get(i));
						}
						for (int i = 0; i < start2.size(); i++) {
							sheetData.groupRow(start2.get(i), end2.get(i));
						}
						for (int i = 0; i < start3.size(); i++) {
							sheetData.groupRow(start3.get(i), end3.get(i));
						}
						for (int i = 0; i < start4.size(); i++) {
							sheetData.groupRow(start4.get(i), end4.get(i));
						}
						for (int i = 0; i < start5.size(); i++) {
							sheetData.groupRow(start5.get(i), end5.get(i));
						}
						out = new FileOutputStream(exportFileName);
						workbook.write(out);
						out.close();
						workbook.dispose();
						String outputPath = Configuration.getExportExcelPath() + outputName;
						result.put(REPORT_PATH, outputPath);
						//result.put(LIST, outputPath);
						result.put(ERROR, false);
						result.put("hasData", true);
						MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
					} else {
						result.put(ERROR, false);
						result.put("hasData", false);
						return JSON;
					}
					//************************************************************************				
					lstData = null;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportBCSLDS22", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao san luong doanh so 2.1
	 * 
	 * @author hoanv25
	 * @since July 07/2015
	 * @return
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach lai Action
	 */
	public String exportBCSLDS21() {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);

				List<GS_1_4_VO> lstData = hoReportMgr.exportBCSLDS21(staffIdRoot, userId, strListShopId, fDate, tDate, categoryId, subCategoryId, strListProductId);

				//************************************************************************
				String outputName = ConstantManager.EXPORT_2_1_SLDS + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.2.1.slds.shet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(23);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 150, 100, 150, 150, 150, 150, 180, 180, 250, 180, 180, 180, 150, 150, 80, 100, 150, 150, 150, 150);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 30, 25, 15, 15, 15, 15);
				sheetData.setDisplayGridlines(false);
				//Tittle
				String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.2.1.slds.title.name");
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
				//info					
				ExcelPOIProcessUtils.addCell(sheetData, 2, 3, vung, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 3, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 4, npp, style.get(ExcelPOIProcessUtils.NORMAL));
				if (categoryId != null) {
					ProductInfo proIn = productMgr.getProductInfoById(new Long(categoryId));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 3, proIn != null ? proIn.getProductInfoName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				}
				if (subCategoryId != null) {
					ProductInfo proSubIn = productMgr.getProductInfoById(new Long(subCategoryId));
					ExcelPOIProcessUtils.addCell(sheetData, 8, 3, proSubIn != null ? proSubIn.getProductInfoName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				}
				if (strListProductName != null && strListProductName != "" && strListProductName.length() > 0) {
					ExcelPOIProcessUtils.addCell(sheetData, 10, 3, strListProductName, style.get(ExcelPOIProcessUtils.NORMAL));
				}
				ExcelPOIProcessUtils.addCell(sheetData, 4, 4, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 4, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 4, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 5, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL));
				//header
				ExcelPOIProcessUtils.addCell(sheetData, 1, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nganhhang"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nganhhangcon"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.sanpham"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));

				ExcelPOIProcessUtils.addCell(sheetData, 0, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.mausm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenusm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.manvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.makh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenkh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 11, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.masp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 12, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tensp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 13, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nhanhang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 14, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nganhhang"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 15, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nganhhangcon"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 16, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.donggoi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 17, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.huongvi"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 18, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dvt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 19, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.quycach"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 20, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.sanluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 21, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dongiatruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 22, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.dongiasauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 23, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.thanhtientruocvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 24, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.thanhtiensauvat"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				int d = 8;
				if (lstData.size() > 0) {
					GS_1_4_VO salesVO = null;
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaKhuVuc(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getTenSp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getNhanHang(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getNganhHang(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getNganhHangCon(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getDongGoi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getHuongVi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getDonViTinh(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getQuyCach(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getSanLuong(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 21, d, salesVO.getGiaChuaThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 22, d, salesVO.getGiaSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						Double dab = null;
						if (salesVO.getThanhTienTruocThue() != null) {
							dab = ((BigDecimal) salesVO.getThanhTienTruocThue()).doubleValue();
						}
						if (SXSSFReportHelper.checkDecimal(dab) == true) {
							ExcelPOIProcessUtils.addCell(sheetData, 23, d, salesVO.getThanhTienTruocThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 23, d, salesVO.getThanhTienTruocThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						Double dac = null;
						if (salesVO.getThanhTienSauThue() != null) {
							dac = ((BigDecimal) salesVO.getThanhTienSauThue()).doubleValue();
						}
						if (SXSSFReportHelper.checkDecimal(dac) == true) {
							ExcelPOIProcessUtils.addCell(sheetData, 24, d, salesVO.getThanhTienSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 24, d, salesVO.getThanhTienSauThue(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						d++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				//************************************************************************				
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportBCSLDS21", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * Sets the cycle count des.
	 * 
	 * @param
	 * @author hoanv25
	 * @since July 10/2015
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach lai Action
	 */
	public String getListSubCat() {
		try {
			lstSubCategoryCat = new ArrayList<ProductInfo>();
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			lstSubCategoryCat = productInfoMgr.getListSubCat(ActiveType.RUNNING, ProductType.SUB_CAT, idParentCat);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.getListSubCat", createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * Bao cao Key Shop vuongmq
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach lai Action
	 */
	public String exportSLDS1_5() {
		FileOutputStream out = null;
		OutputStreamWriter outW = null;
		Writer writer = null;
		try {
			errMsg = "";
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Shop shop = null;
			if (lstShopId != null && lstShopId.trim().length() > 0) {
				for (String str : lstShopId.split(",")) {
					shop = shopMgr.getShopById(new Long(str));
					if (shop == null) {
						result.put(ERROR, true);
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "catalog.unit.tree");
						return JSON;
					} else {
						if (!checkShopPermission(shop.getShopCode())) {
							result.put(ERROR, true);
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_BELONG_USERLOGIN, false, "catalog.unit.tree");
							return JSON;
						}
					}
				}

			} else {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_REQUIRE, false, "catalog.unit.tree");
				//result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_REQUIRE,false,"catalog.unit.tree"));
			}

			if (StringUtil.isNullOrEmpty(fromDate) && errMsg.length() == 0) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.fromDate");
			}
			if (StringUtil.isNullOrEmpty(toDate) && errMsg.length() == 0) {
				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.display.program.toDate");
			}
			if (errMsg.length() > 0) {
				result.put(ERROR, true);
				result.put("errMsg", errMsg);
				return JSON;
			}

			/** Lay va kiem tra co du lieu */
			List<Rpt_SLDS1_5> lst = hoReportMgr.getListSLDS1_5(lstShopId, fDate, tDate);
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			if (!"XLS".equals(formatType)) {
				String fileName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.slds.1.5.fileName");
				fileName = StringUtil.getReportNameFormat(fileName, fromDate, toDate, FileExtension.CSV.getValue());
				out = new FileOutputStream(Configuration.getStoreRealPath() + fileName);
				byte[] bom = new byte[] { (byte) 0xEF, (byte) 0xBB, (byte) 0xBF };
				out.write(bom);
				outW = new OutputStreamWriter(out, Charset.forName("UTF-8"));
				writer = new BufferedWriter(outW);
				if (!checkExport) {
					//xuat Du lieu CSV du lieu chuan
					// title
					writer.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.slds.1.5.title"));
					writer.append("\n");
					//Khoang ngay bao cao
					String dateStrTilte = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.general.tuNgay") + fromDate + " - " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.general.denNgay") + toDate;
					writer.append(dateStrTilte);
					writer.append("\n");
					String datePrint = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.general.ngayIn") + DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR);
					writer.append(datePrint);
					writer.append("\n\n");
					// menu
					writer.append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.slds.1.5.menu1"));
					for (int i = 2; i < 12; i++) {
						ReportUtils.appendStringCSVsepPrefix(writer, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.slds.1.5.menu" + i));
					}
					// data
					Rpt_SLDS1_5 vo = null;
					BigDecimal tongTien = BigDecimal.ZERO;
					for (int i = 0, size = lst.size(); i < size; i++) {
						vo = lst.get(i);
						writer.append('\n').append(vo.getMien());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getVung());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getMaNPP());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getMaNVBH());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getTenNVBH());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getSoDonHang());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getSoHoaDon());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getMaKhachHang());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getTenKhachHang());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getDiaChi());
						ReportUtils.appendCSVsepPrefix(writer, vo.getSoTien());
						tongTien = tongTien.add(vo.getSoTien());
						if (i == size - 1) {
							writer.append('\n').append("Tổng");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendStringCSVsepPrefix(writer, "");
							ReportUtils.appendCSVsepPrefix(writer, tongTien);
						}
					}
				} else {
					writer.append("Mien");
					ReportUtils.appendStringCSVsepPrefix(writer, "Vung");
					ReportUtils.appendStringCSVsepPrefix(writer, "MaNPP");
					ReportUtils.appendStringCSVsepPrefix(writer, "MaNVBH");
					ReportUtils.appendStringCSVsepPrefix(writer, "TenNVBH");
					ReportUtils.appendStringCSVsepPrefix(writer, "SoDonHang");
					ReportUtils.appendStringCSVsepPrefix(writer, "SoHoaDon");
					ReportUtils.appendStringCSVsepPrefix(writer, "MaKH");
					ReportUtils.appendStringCSVsepPrefix(writer, "TenKH");
					ReportUtils.appendStringCSVsepPrefix(writer, "DiaChi");
					ReportUtils.appendStringCSVsepPrefix(writer, "ThanhTien");
					writer.append('\n');
					// xuat Du lieu CSV du lieu tho
					// data
					Rpt_SLDS1_5 vo = null;
					for (int i = 0, size = lst.size(); i < size; i++) {
						vo = lst.get(i);
						writer.append(vo.getMien());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getVung());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getMaNPP());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getMaNVBH());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getTenNVBH());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getSoDonHang());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getSoHoaDon());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getMaKhachHang());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getTenKhachHang());
						ReportUtils.appendStringCSVsepPrefix(writer, vo.getDiaChi());
						ReportUtils.appendCSVsepPrefix(writer, vo.getSoTien());
						writer.append('\n');
					}
				}
				writer.flush();
				writer.close();
				outW.close();
				out.close();
				result.put(ERROR, false);
				result.put("hasData", true);
				if ("ZIP".equals(formatType)) {
					String fileName2 = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.slds.1.5.fileName");
					List<File> lstFile = new ArrayList<File>();
					File f = new File(Configuration.getStoreRealPath() + fileName);
					lstFile.add(f);
					fileName2 = fileName2 + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.ZIP.getValue();
					FileUtility.compress(lstFile, fileName2, Configuration.getStoreRealPath());
					for (int i = 0; i < lstFile.size(); i++) {
						lstFile.get(i).delete();
					}
					result.put(REPORT_PATH, Configuration.getExportExcelPath() + fileName2);
				} else {
					result.put(REPORT_PATH, Configuration.getExportExcelPath() + fileName);
				}
				lst = null;
				System.gc();
			} else {
				exportSLDS1_5Xlsx(lst);
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportSLDS1_5", createLogErrorStandard(actionStartTime));
			try {
				if (writer != null) {
					writer.close();
				}
				if (outW != null) {
					outW.close();
				}
				if (out != null) {
					out.close();
				}
			} catch (IOException io) {
				LogUtility.logErrorStandard(io, "ths.dms.action.report.HOReportAction.exportSLDS1_5.catch", createLogErrorStandard(actionStartTime));
			}
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * Bao cao key shop - xuat Excel XLSX
	 * 
	 * @modify hunglm16
	 * @param lst
	 * @throws Exception
	 * @since 26/09/2015
	 */
	private void exportSLDS1_5Xlsx(List<Rpt_SLDS1_5> lst) throws Exception {
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String fileName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.slds.1.5.fileName");
			fileName = StringUtil.getReportNameFormat(fileName, fromDate, toDate, FileExtension.XLSX.getValue());
			workbook = new SXSSFWorkbook(200);
			if (!checkExport) {
				// xuat Excel du lieu chuan
				/** */
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("SLDS1_5");
				sheet.setDisplayGridlines(false);
				SXSSFReportHelper.setColumnWidths(sheet, 0, (short) 20, (short) 20, (short) 15, (short) 20, (short) 28);
				SXSSFReportHelper.setColumnWidthEx(sheet, 5, 3, (short) 17);
				SXSSFReportHelper.setColumnWidths(sheet, 8, (short) 28);
				SXSSFReportHelper.setColumnWidths(sheet, 9, (short) 60);
				SXSSFReportHelper.setColumnWidths(sheet, 10, (short) 17);

				// header
				int colIdx = 0;
				int rowIdx = 0;

				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);

				// title
				rowIdx++;
				SXSSFReportHelper.addMergeCells(sheet, 0, rowIdx, 8, rowIdx++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.slds.1.5.title"), style.get(ExcelPOIProcessUtils.TITLE_BLUE));
				String dateStrTilte = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.general.tuNgay") + fromDate + " - " + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.general.denNgay") + toDate;
				SXSSFReportHelper.addMergeCells(sheet, 0, rowIdx, 8, rowIdx++, dateStrTilte, style.get(ExcelPOIProcessUtils.BOLD_CENTER));
				String printDate = DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR);
				SXSSFReportHelper.addMergeCells(sheet, 0, rowIdx, 8, rowIdx++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.general.ngayIn") + printDate, style.get(ExcelPOIProcessUtils.BOLD_CENTER));
				// menu
				rowIdx++;
				SXSSFReportHelper.setRowHeightEx(sheet, rowIdx, 1, (short) 35);
				for (int i = 1; i < 12; i++) {
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.slds.1.5.menu" + i), style.get(ExcelPOIProcessUtils.MENU));
				}
				// data
				rowIdx++;
				colIdx = 0;
				Rpt_SLDS1_5 vo = null;
				BigDecimal tongTien = BigDecimal.ZERO;
				for (int i = 0, size = lst.size(); i < size; i++, rowIdx++) {
					vo = lst.get(i);
					colIdx = 0;
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getSoDonHang(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getSoHoaDon(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getMaKhachHang(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getTenKhachHang(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, vo.getSoTien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
					tongTien = tongTien.add(vo.getSoTien());
				}
				colIdx = 0;
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "Tổng", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
				SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, tongTien, style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE03));
			} else {
				// Xuat Excel du lieu tho
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("SLDS1_5_dulieu");
				sheet.setDisplayGridlines(false);
				SXSSFReportHelper.setColumnWidths(sheet, 0, (short) 20, (short) 20, (short) 15, (short) 20, (short) 28);
				SXSSFReportHelper.setColumnWidthEx(sheet, 5, 3, (short) 17);
				SXSSFReportHelper.setColumnWidths(sheet, 8, (short) 28);
				SXSSFReportHelper.setColumnWidths(sheet, 9, (short) 60);
				SXSSFReportHelper.setColumnWidths(sheet, 10, (short) 17);
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				int colIdx = 0;
				int rowIdx = 0;
				Rpt_SLDS1_5 vo = null;
				for (int i = 0, size = lst.size(); i < size; i++, rowIdx++) {
					vo = lst.get(i);
					colIdx = 0;
					SXSSFReportHelper.setRowHeights(sheet, rowIdx, (short) 15);
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getMien(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getVung(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getSoDonHang(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getSoHoaDon(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getMaKhachHang(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getTenKhachHang(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addCell(sheet, colIdx++, rowIdx, vo.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_LEFT));
					SXSSFReportHelper.addNumberCell(sheet, colIdx++, rowIdx, vo.getSoTien(), style.get(ExcelPOIProcessUtils.ROW_RIGHT_FM_ZEZO));
				}
			}
			out = new FileOutputStream(Configuration.getStoreRealPath() + fileName);
			workbook.write(out);
			out.close();
			workbook.dispose();
			result.put(ERROR, false);
			result.put("hasData", true);
			result.put(REPORT_PATH, Configuration.getExportExcelPath() + fileName);
			lst = null;
			System.gc();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportSLDS1_5Xlsx", createLogErrorStandard(actionStartTime));
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}

	}

	/**
	 * Bao cao tong hop theo yeu cau cua KH Khong phan quyen shop; lay toan quoc
	 * 
	 * @author vuongmq
	 * @return String
	 * @since 06/10/2015
	 */
	@SuppressWarnings("rawtypes")
	public String exportGeneral() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			//Kiem tra ATTT
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("report.invalid.token"));
				return JSON;
			}
			//Xu ly Validate
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(fromDate)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.required.field", R.getResource("customer.display.program.date")));
				return JSON;
			}
			if (StringUtil.isNullOrEmpty(shopStr)) {
				shopStr = String.valueOf(currentUser.getShopRoot().getShopId());
			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			//Xu ly goi Core lay du lieu
			Map<String, Object> beans = new HashMap<String, Object>();
			String fileTemplate = "";
			String fileName = "";
			List lstData = new ArrayList<>();
			if (typeReport != null && ActiveType.RUNNING.getValue().equals(typeReport)) {
				//  Bao cao Nhan vien BH hoat dong Khong phan quyen shop; lay toan quoc
				fileTemplate = ConstantManager.RPT_BC_NVBH_ACTIVE_TEMPLATE;
				fileName = ConstantManager.RPT_BC_NVBH_ACTIVE_FILE_NAME;
				lstData = hoReportMgr.exportBCNVBHActive(shopStr, fDate);
			} else {
				//Bao cao Don hang cho KH Khong phan quyen shop; lay toan quoc
				fileTemplate = ConstantManager.RPT_BC_DH_CHO_KH_TEMPLATE;
				fileName = ConstantManager.RPT_BC_DH_CHO_KH_FILE_NAME;
				lstData = hoReportMgr.exportBCDHChoKH(shopStr, fDate);
			}
			if (!lstData.isEmpty()) {
				//Xu ly xuat file excel
				beans.put("lstData", lstData);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.report.data.null"));
				return JSON;
			}
			//Xu ly xuat file excel
			beans.put("fromDate", fromDate);
			beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
			String templateFileName = folder + fileTemplate;
			templateFileName = templateFileName.replace('/', File.separatorChar);

			String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(ERROR, false);
			result.put("path", outputPath);
			//Bo sung ATTT
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.exportGeneral()"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Bao cao 6.2.2 ASO
	 * 
	 * @author hunglm16
	 * @return Excel File
	 * @since October 09,2015
	 * @description API - POI
	 */
	public String reportASOForExcel() {
		//Dinh nghia ham xuat Excel
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			//Kiem tra ATTT
			result.put(ERROR, true);
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("report.invalid.token"));
				return JSON;
			}
			Shop shop = new Shop();
			if (shopId != null) {
				shop = shopMgr.getShopById(shopId);
				if (shop == null) {
					result.put("errMsg", R.getResource("catalog.cms.shop.not.exist"));
					return JSON;
				} else if (!currentUser.getShopRoot().getShopId().equals(shopId) && getMapShopChild().get(shopId) == null) {
					//Loi phan quyen
					result.put("errMsg", R.getResource("common.cms.shop.undefined"));
					return JSON;
				}
			} else {
				result.put("errMsg", R.getResource("catalog.cms.shop.not.exist"));
				return JSON;
			}
			//Gia tri cycle khong duoc phep rong
			if (cycleId == null) {
				result.put("errMsg", R.getResource("common.cycle.undefined"));
				return JSON;
			}
			Cycle cycle = commonMgr.getEntityById(Cycle.class, cycleId);
			if (cycle == null) {
				result.put("errMsg", R.getResource("common.cycle.undefined"));
				return JSON;
			}
			int flag = ONE_INT_G;
			if (!StringUtil.isNullOrEmpty(catIdStr)) {
				catIdStr = catIdStr.replaceAll("-2,", "").replaceAll("-2", "").trim();
			} else {
				catIdStr = "";
			}
			if (!StringUtil.isNullOrEmpty(subCatIdStr)) {
				subCatIdStr = subCatIdStr.replaceAll("-2,", "").replaceAll("-2", "").trim();
			} else {
				subCatIdStr = "";
			}
			if (!StringUtil.isNullOrEmpty(catIdStr) && StringUtil.isNullOrEmpty(subCatIdStr)) {
				//Lay cac gia tri subcat by cat
				List<Long> lstObjId = new ArrayList<Long>();
				String[] arrCatId = catIdStr.split(",");
				for (String value : arrCatId) {
					if (!StringUtil.isNullOrEmpty(value) && StringUtil.isNumberInt(value)) {
						lstObjId.add(Long.valueOf(value));
					}
				}
				if (lstObjId == null || lstObjId.isEmpty()) {
					//Khong xac dinh duoc nganh hang con
					result.put("errMsg", R.getResource("common.subcat.list.error.null"));
					return JSON;
				}
				List<ProductInfo> productInfo = productInfoMgr.getListSubCatByListCat(ActiveType.RUNNING, ProductType.SUB_CAT, lstObjId);
				if (productInfo == null || productInfo.isEmpty()) {
					//Khong xac dinh duoc nganh hang con
					result.put("errMsg", R.getResource("common.subcat.list.error.null"));
					return JSON;
				}
				subCatIdStr = productInfo.get(0).getId().toString();
				for (int sb = 1, sizesb = productInfo.size(); sb < sizesb; sb++) {
					subCatIdStr = subCatIdStr + "," + productInfo.get(sb).getId().toString();
				}
			}
			if (!StringUtil.isNullOrEmpty(subCatIdStr)) {
				flag = ZEZO_INT_G;
			}

			Rpt_HO_ASO_VO data = hoReportMgr.reportASOFullData(shopId, cycleId, flag, subCatIdStr.trim(), currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
			if (data != null && data.getLstData() != null && !data.getLstData().isEmpty()) {
				Date crDate = commonMgr.getSysDate();
				//Lay du lieu tuong tac
				Rpt_HO_ASO_VO_Header headerAutomatic = data.getHeaderAutomatic();
				List<Rpt_HO_ASO_VO_Data> lstData = data.getLstData();
				//Xu ly tao file Excel
				String outputName = ConstantManager.BC_ASO_FILE_NAME + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("bc.aso.sheet1.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(13);
				//set static Column width
				ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 80, 80, 130, 180, 130, 180, 130, 180);
				//Size Row
				ExcelPOIVTICTUtils.setRowsHeight(sheetData, 0, 28, 15, 15, 18, 18, 25);
				sheetData.setDisplayGridlines(false);
				int d = 0;
				//--> Tao Tieu de
				ExcelPOIVTICTUtils.addCell(sheetData, 0, d++, R.getResource("bc.aso.sheet1.title"), style.get(ExcelPOIVTICTUtils.TITLE_G_LEFT));
				//Khoang ngay bao cao
				ExcelPOIVTICTUtils.addCell(sheetData, 0, d, R.getResource("bc.aso.sheet1.title.time.cycle") + (": ") + cycle.getCycleName(), style.get(ExcelPOIVTICTUtils.TIMES_TITLE_G_LEFT));
				ExcelPOIVTICTUtils.addCell(sheetData, 3, d, R.getResource("bc.aso.sheet1.title.time.day") + (": ") + DateUtil.toDateString(crDate, DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIVTICTUtils.TIMES_TITLE_G_LEFT));
				if (shopId != null) {
					Shop dv = commonMgr.getEntityById(Shop.class, shopId);
					if (dv != null) {
						ExcelPOIVTICTUtils.addCell(sheetData, 5, d, R.getResource("common.shop.name.lable") + (": ") + dv.getShopCode() + " - " + dv.getShopName(), style.get(ExcelPOIVTICTUtils.TIMES_TITLE_G_LEFT));						
					}
				}
				d = d + 2;
				//header
				int c = 0;
				int dNext = d + 2;
				String header = R.getResource("bc.aso.sheet1.header.fixed");
				if (!StringUtil.isNullOrEmpty(header)) {
					String[] arrHeaderFixed = header.split(",");
					int dF = d + 1;
					c = 0;
					for (String hdVal : arrHeaderFixed) {
						ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, c, d, c, dNext, hdVal.trim(), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						c++;
					}
					for (Long idSub : headerAutomatic.getLstHeaderAsoId()) {
						//Can chieu ngang cho cot
						Rpt_HO_ASO_VO_Header_Value voSKU = headerAutomatic.getMapHeader().get(idSub);
						if (voSKU.getLstHeaderSKU().size() > 1) {
							int cNext = c + voSKU.getLstHeaderSKU().size() - 1;
							int stGroupColumn = c;
							ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, c, d, cNext, d, headerAutomatic.getMapHeaderAsoCode().get(idSub) + " - " + headerAutomatic.getMapHeaderAsoName().get(idSub), style
									.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_TOP_MEDIUM));
							if (voSKU != null) {
								for (Long idP : voSKU.getLstHeaderSKU()) {
									if (voSKU.getMapHeaderSkuType().get(idP).intValue() > ONE_INT_G) {
										ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, c, dF, c, dNext, R.getResource("bc.aso.sheet1.header.aso.tong"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
									} else {
										ExcelPOIVTICTUtils.addCell(sheetData, c, dF, voSKU.getMapHeaderSkuCode().get(idP), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN));
										ExcelPOIVTICTUtils.addCell(sheetData, c, dNext, voSKU.getMapHeaderSkuName().get(idP), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
									}
									c++;
								}
							}
							sheetData.groupColumn(stGroupColumn, cNext - 1);
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, c, d, headerAutomatic.getMapHeaderAsoCode().get(idSub) + " - " + headerAutomatic.getMapHeaderAsoName().get(idSub), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_TOP_MEDIUM));
							ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, c, dF, c, dNext, R.getResource("bc.aso.sheet1.header.aso.tong"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
							c++;
						}
					}
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, c, d, c, dNext, R.getResource("bc.aso.sheet1.header.aso.nv.tong"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					c++;
//					if (flag == ONE_INT_G) {
//						ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, c, d, c, dNext, R.getResource("bc.aso.sheet1.header.aso.nv.tong"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
//						c++;
//					}
				}
				d = dNext + 1;
				int rowStart = d;
				int group3 = 0;
				int group2 = 0;
				int group1 = 0;
				List<Integer> start3 = new ArrayList<Integer>();
				List<Integer> end3 = new ArrayList<Integer>();
				List<Integer> start2 = new ArrayList<Integer>();
				List<Integer> end2 = new ArrayList<Integer>();
				List<Integer> start1 = new ArrayList<Integer>();
				List<Integer> end1 = new ArrayList<Integer>();
				//Du lieu data
				for (int i = 0, n = lstData.size(); i < n; i++) {
					Rpt_HO_ASO_VO_Data val = lstData.get(i);
					if (StringUtil.isNullOrEmpty(val.getMaVung())) {
						start3.add(group3 + rowStart);
						end3.add(i + rowStart - 1);
						group3 = i + 1;
						group2++;
						group1++;
					} else if (StringUtil.isNullOrEmpty(val.getMaNPP())) {
						start2.add(group2 + rowStart);
						end2.add(i + rowStart - 1);
						group2 = i + 1;
						group1++;
					} else if (StringUtil.isNullOrEmpty(val.getMaTuyen())) {
						start1.add(group1 + rowStart);
						end1.add(i + rowStart - 1);
						group1 = i + 1;
					}
					c = 0;
					val.safeSetNull();
					String styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP;
					String styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO;
					if (StringUtil.isNullOrEmpty(val.getMaTuyen())) {
						styleLeft = ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP_BOLD;
						styleRight = ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO_BOLD;
					}
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getMaMien(), style.get(styleLeft));
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getMaVung(), style.get(styleLeft));
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getMaNPP(), style.get(styleLeft));
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getTenNPP(), style.get(styleLeft));
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getMaTuyen(), style.get(styleLeft));
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getTenTuyen(), style.get(styleLeft));
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getMaNV(), style.get(styleLeft));
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getTenNV(), style.get(styleLeft));
					if (val.getLstValue() != null && !val.getLstValue().isEmpty()) {
						for (DynamicVO valDt : val.getLstValue()) {
							ExcelPOIVTICTUtils.addCell(sheetData, c++, d, valDt.getValue(), style.get(styleRight));
						}
					}
					ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getTongNV(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_ORANGE01_RED));
//					if (flag == 1) {
//						ExcelPOIVTICTUtils.addCell(sheetData, c++, d, val.getTongNV(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_ORANGE01_RED));
//					}
					d++;
				}
				for (int i = 0; i < start1.size(); i++) {
					sheetData.groupRow(start1.get(i), end1.get(i));
				}
				for (int i = 0; i < start2.size(); i++) {
					sheetData.groupRow(start2.get(i), end2.get(i));
				}
				for (int i = 0; i < start3.size(); i++) {
					sheetData.groupRow(start3.get(i), end3.get(i));
				}
				
				out = new FileOutputStream(exportFileName);
				workbook.write(out);
				out.flush();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(REPORT_PATH, outputPath);
				result.put(LIST, outputPath);
				result.put(ERROR, false);
				result.put("hasData", true);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				errMsg = R.getResource("common.report.data.null");
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				result.put("data.hasData", true);
			}
		} catch (Exception e) {
			errMsg = R.getResource("common.report.error.system");
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.reportASOForExcel", createLogErrorStandard(DateUtil.now()));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e1) {
					LogUtility.logErrorStandard(e1, "ths.dms.action.report.reportASOForExcel", createLogErrorStandard(DateUtil.now()));
				}
			}
		}
		return JSON;
	}
	
	/**
	 * BC 6.1.3. Tang truong SKUs
	 * 
	 * @author hunglm16
	 * @return Excel File
	 * @since 09/11/2015
	 * @description API - POI
	 */
	public String reportGrowthSKUsForExcel() {
		//Dinh nghia ham xuat Excel
		SXSSFWorkbook workbook = null;
		FileOutputStream out = null;
		try {
			//Kiem tra ATTT
			result.put(ERROR, true);
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("report.invalid.token"));
				return JSON;
			}
			Shop shop = new Shop();
			if (shopId == null || shopId < 0l || (!currentUser.getShopRoot().getShopId().equals(shopId) && getMapShopChild().get(shopId) == null)) {
				result.put("errMsg", R.getResource("catalog.cms.shop.not.exist"));
				return JSON;
			}
			shop = shopMgr.getShopById(shopId);
			if (shop.getType() != null && shop.getType().getSpecificType() != null && SpecificGeneral.NPP.getValue().equals(shop.getType().getSpecificType().getValue())) {
				result.put("errMsg", R.getResource("common.cms.shop.islevel5.ex", R.getResource("common.un.invalid")));
				return JSON;
			}
			Rpt_HO_GROWTH_SKUs_VO data = hoReportMgr.reportGrowthSKUs(shopId, year, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
			if (data != null && data.getLstData() != null && !data.getLstData().isEmpty()) {
				Date crDate = commonMgr.getSysDate();
				//Lay du lieu tuong tac
				List<Cycle> headerAutomatic = data.getHeaderAutomatic();
				List<Rpt_HO_GROWTH_SKUs_VO_Data> lstData = data.getLstData();
				//Xu ly tao file Excel
				String outputName = ConstantManager.BC_GROWTH_SKUs_FILE_NAME + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("bc.growth.skus.sheet1.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(13);
				//set static Column width
				ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 80, 80, 130, 180);
				//Size Row
				ExcelPOIVTICTUtils.setRowsHeight(sheetData, 0, 28, 15, 15, 18, 18, 25);
				sheetData.setDisplayGridlines(false);
				int rowIndex = 0;
				//--> Tao Tieu de
				ExcelPOIVTICTUtils.addCell(sheetData, 0, rowIndex++, R.getResource("bc.growth.skus.sheet1.tilte"), style.get(ExcelPOIVTICTUtils.TITLE_G_LEFT));
				//Khoang ngay bao cao
				ExcelPOIVTICTUtils.addCell(sheetData, 0, rowIndex++, R.getResource("common.shop.name.lable") + (": ") + shop.getShopCode() + " - " + shop.getShopName(), style.get(ExcelPOIVTICTUtils.TIMES_TITLE_G_LEFT));						
				ExcelPOIVTICTUtils.addCell(sheetData, 0, rowIndex, R.getResource("bc.growth.skus.sheet1.tilte.time.year", String.valueOf(year)), style.get(ExcelPOIVTICTUtils.TIMES_TITLE_G_LEFT));
				ExcelPOIVTICTUtils.addCell(sheetData, 2, rowIndex, R.getResource("bc.growth.skus.sheet1.tilte.time.day", DateUtil.toDateString(crDate, DateUtil.DATETIME_FORMAT_STR)), style.get(ExcelPOIVTICTUtils.TIMES_TITLE_G_LEFT));
				rowIndex = rowIndex + 2;
				//header
				int columnIndex = 0;
				int firtRowHeder = rowIndex;
				int endRowHeder = rowIndex + 2;
				String header = R.getResource("bc.growth.skus.sheet1.header.fixed");
				if (!StringUtil.isNullOrEmpty(header)) {
					String[] arrHeaderFixed = header.split(",");
					int rowIndexCCHeader = rowIndex + 1;
					columnIndex = 0;
					for (String hdVal : arrHeaderFixed) {
						ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, columnIndex, firtRowHeder, columnIndex, endRowHeder, hdVal.trim(), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						columnIndex++;
					}
					if (headerAutomatic != null && !headerAutomatic.isEmpty()) {
						int maxSizeClm = headerAutomatic.size() * 3 + columnIndex - 1;
						//Xu ly cho cot nam
						ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, columnIndex, rowIndex, maxSizeClm, rowIndex, String.valueOf(year), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_TOP_MEDIUM));
						int cIndexTpm = columnIndex; //Cot dong
						//Xu ly hien thi cac so chu cau trong nam xuat
						int endIndexMager = 0;
						for (Cycle cc : headerAutomatic) {
							endIndexMager = cIndexTpm + 2; 
							ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, cIndexTpm, rowIndexCCHeader, endIndexMager, rowIndexCCHeader, cc.getNum(), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN));
							cIndexTpm = endIndexMager + 1;
						}
						//Xu ly hien thi nam va nam truoc do
						cIndexTpm = columnIndex;
						while (cIndexTpm < maxSizeClm) {
							ExcelPOIVTICTUtils.setColumnsWidth(sheetData, cIndexTpm, 90);
							ExcelPOIVTICTUtils.addCell(sheetData, cIndexTpm++, endRowHeder, year - 1, style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
							ExcelPOIVTICTUtils.addCell(sheetData, cIndexTpm++, endRowHeder, year, style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
							ExcelPOIVTICTUtils.addCell(sheetData, cIndexTpm++, endRowHeder, R.getResource("bc.growth.skus.sheet1.header.char.percent"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
						}
						endIndexMager = cIndexTpm + 2; 
						int rowIndexTmp = firtRowHeder + 1;
						int cIndexTpmFixed = cIndexTpm;
						ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, cIndexTpm, firtRowHeder, endIndexMager, rowIndexTmp, R.getResource("bc.growth.skus.sheet1.header.tong"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_TOP_MEDIUM));
						ExcelPOIVTICTUtils.addCell(sheetData, cIndexTpmFixed++, endRowHeder, year - 1, style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
						ExcelPOIVTICTUtils.addCell(sheetData, cIndexTpmFixed++, endRowHeder, year, style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
						ExcelPOIVTICTUtils.addCell(sheetData, cIndexTpmFixed++, endRowHeder, R.getResource("bc.growth.skus.sheet1.header.char.percent"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM));
					}
					//Xu ly dong tong
					//Du lieu data
					rowIndex = endRowHeder;
					for (Rpt_HO_GROWTH_SKUs_VO_Data row : lstData) {
						rowIndex++;
						columnIndex = 0;
						row.safeSetNull();
						ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getMaMien(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getMaVung(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getMaSanPham(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getTenSanPham(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT));
						if (row.getLstValue() != null && !row.getLstValue().isEmpty()) {
							for (int i = 0, sizeDt = row.getLstValue().size(); i < sizeDt; i++) {
								if ((i + 1) % 3 == 0) {
									ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getLstValue().get(i).getValue(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FLOAT_TWO));
								} else {
									ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getLstValue().get(i).getValue(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
								}
							}
						}
						ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getTongNamQk(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
						ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getTongNamHt(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FM_ZEZO));
						ExcelPOIVTICTUtils.addCell(sheetData, columnIndex++, rowIndex, row.getPhanTramTong(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FLOAT_TWO));
					}
				}
				out = new FileOutputStream(exportFileName);
				workbook.write(out);
				out.flush();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(REPORT_PATH, outputPath);
				result.put(LIST, outputPath);
				result.put(ERROR, false);
				result.put("hasData", true);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				errMsg = R.getResource("common.report.data.null");
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				result.put("data.hasData", true);
			}
		} catch (Exception e) {
			errMsg = R.getResource("common.report.error.system");
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.reportGrowthSKUsForExcel", createLogErrorStandard(DateUtil.now()));
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e1) {
					LogUtility.logErrorStandard(e1, "ths.dms.action.report.reportGrowthSKUsForExcel", createLogErrorStandard(DateUtil.now()));
				}
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao raw data
	 * @author trietptm
	 * @return String
	 * @throws Exception 
	 * @since Nov 05, 2015
	 */
	public String exportRawData() throws Exception {
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		List<RawDataIDPVO> lstData = new ArrayList<RawDataIDPVO>();
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				String msg = "";
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					}
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportShort(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				Long userId = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				Shop shop = shopMgr.getShopById(shopId);
				lstData = hoReportMgr.exportRawData(userId, roleId, shopId, fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					String outputName = ConstantManager.RPT_6_4_4_REPORT_SALEORDER_DATA_FILE_NAME + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("rpt.raw.data.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultColumnWidth(15);
					//set static Column width
					ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 40, 100, 100, 100, 150, 130, 150);
					sheetData.setDisplayGridlines(false);
					//Title
					ExcelPOIVTICTUtils.setRowHeight(sheetData, 0, 30);
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 0, 0, 12, 0, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.title"), style.get(ExcelPOIVTICTUtils.TITLE_G));
					//info
					int rowTitle = 1;
					int colTitle = 1;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.shop"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colTitle++, rowTitle, colTitle++, rowTitle, shop == null ? "" : (shop.getShopCode() + " - " + shop.getShopName()), style.get(ExcelPOIVTICTUtils.NORMAL));
					rowTitle++;
					colTitle = 1;
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.from.date"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, fromDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.to.date"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, toDate, style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colTitle++, rowTitle, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIVTICTUtils.NORMAL));
					
					//header
					String[] listHeader = new String[] { "rpt.raw.data.header.number", "rpt.raw.data.header.region", "rpt.raw.data.header.area", 
							"rpt.raw.data.header.shop.code", "rpt.raw.data.header.shop.name", "rpt.raw.data.header.staff.name", "rpt.raw.data.header.staff.code",
							"rpt.raw.data.header.routing.code", "rpt.raw.data.header.routing.name", "rpt.raw.data.header.customer.code", "rpt.raw.data.header.customer.name",
							"rpt.raw.data.header.house.number", "rpt.raw.data.header.street", "rpt.raw.data.header.precinct", "rpt.raw.data.header.district",
							"rpt.raw.data.header.province", "rpt.raw.data.header.order.number", "rpt.raw.data.header.ref.order.number", "rpt.raw.data.header.order.type",
							"rpt.raw.data.header.order.source", "rpt.raw.data.header.order.date", "rpt.raw.data.header.delivery.date", "rpt.raw.data.header.warehouse.code",
							"rpt.raw.data.header.warehouse.name", "rpt.raw.data.header.product.code", "rpt.raw.data.header.product.name", "rpt.raw.data.header.price",
							"rpt.raw.data.header.convfact", "rpt.raw.data.header.quantity.package", "rpt.raw.data.header.quantity.retail", "rpt.raw.data.header.amount",
							"rpt.raw.data.header.program.code", "rpt.raw.data.header.program.type", "rpt.raw.data.header.discount.amount", "rpt.raw.data.header.promo.package",
							"rpt.raw.data.header.promo.retail", "rpt.raw.data.header.status" };
					int rowHeader = rowTitle + 2;
					int colHeader = 0;
					for (int i = 0, n = listHeader.length; i < n; i++) {
						ExcelPOIVTICTUtils.addCell(sheetData, colHeader++, rowHeader, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, listHeader[i]), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					}
					
					// fill data
					int rowData = rowHeader + 1;
					for (int i = 0, n = lstData.size(); i < n; i++) {
						RawDataIDPVO vo = lstData.get(i);
						if (ProgramType.AUTO_PROM.getValue().toString().equals(vo.getProgramType())) {
							vo.setProgramType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.program.type.auto.promo"));
						} else if (ProgramType.MANUAL_PROM.getValue().toString().equals(vo.getProgramType())) {
							vo.setProgramType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.program.type.manual.promo"));
						} else if (ProgramType.DISPLAY_SCORE.getValue().toString().equals(vo.getProgramType())) {
							vo.setProgramType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.program.type.display.score"));
						} else if (ProgramType.PRODUCT_EXCHANGE.getValue().toString().equals(vo.getProgramType())) {
							vo.setProgramType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.program.type.product.exchange"));
						} else if (ProgramType.DESTROY.getValue().toString().equals(vo.getProgramType())) {
							vo.setProgramType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.program.type.destroy"));
						} else if (ProgramType.RETURN.getValue().toString().equals(vo.getProgramType())) {
							vo.setProgramType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.program.type.return"));
						} else if (ProgramType.KEY_SHOP.getValue().toString().equals(vo.getProgramType())) {
							vo.setProgramType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.program.type.key.shop"));
						}
						if (OrderType.IN.getValue().equals(vo.getOrderType()) || OrderType.SO.getValue().equals(vo.getOrderType())) {
							vo.setOrderType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.type.sale"));
						} else if (OrderType.CO.getValue().equals(vo.getOrderType()) || OrderType.CM.getValue().equals(vo.getOrderType())) {
							vo.setOrderType(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.type.return"));
							if (vo.getAmount() != null) {
								vo.setAmount(vo.getAmount().negate());
							}
						}
						if (SaleOrderStatus.NOT_YET_APPROVE.getValue().equals(vo.getApproved())) {
							// chua duyet
							vo.setStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.status.order.approving"));
						} else if (SaleOrderStatus.APPROVED.getValue().equals(vo.getApproved())) {
							// da duyet
							vo.setStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.status.approved"));
						} else if (SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM.getValue().equals(vo.getApproved())) {
							// tu choi
							vo.setStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.status.reject"));
						} else if (SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM.getValue().equals(vo.getApproved())) {
							// huy
							vo.setStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.status.cancel"));
						} else if (SaleOrderType.RETURNED.getValue().equals(vo.getType())) {
							// Da tra
							vo.setStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.status.return"));
						}
						
						if (SaleOrderSource.WEB.getValue().toString().equals(vo.getOrderSource())) {
							vo.setOrderSource(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.source.web"));
						} else if (SaleOrderSource.TABLET.getValue().toString().equals(vo.getOrderSource())) {
							vo.setOrderSource(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.order.source.tablet"));
						}
						
						int colData = 0;
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, i + 1, style.get(ExcelPOIVTICTUtils.ROW_DOTTED_CENTER));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRegionCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getAreaCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShopName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getStaffCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getStaffName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRoutingCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRoutingName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getShortCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getCustomerName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getHouseNumber(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getStreet(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPrecinct(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDistrictName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getProvinceName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getOrderNumber(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getRefOrderNumber(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getOrderType(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getOrderSource(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getOrderDate(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_CENTER));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDeliveryDate(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_CENTER));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getWarehouseCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getWarehouseName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getProductCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getProductName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPrice(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FLOAT_TWO));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getConvfact(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getQuantityPackage(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getQuantityRetail(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getAmount(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FLOAT_TWO));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getProgramCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getProgramType(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getDiscountAmount(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT_FLOAT_TWO));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPromoPackage(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getPromoRetail(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
						ExcelPOIVTICTUtils.addCell(sheetData, colData++, rowData, vo.getStatus(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
						rowData++;
					}
					
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.flush();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.exportRawData"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			lstData.clear();
			lstData = null;
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao raw data cho don dieu chinh
	 * @author trietptm
	 * @return String
	 * @throws Exception 
	 * @since Nov 16, 2015
	 */
	public String exportRawDataStockTrans() throws Exception {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				String msg = "";
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					}
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportMedium(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				Long userId = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<RawDataStockTransVO> lstData = hoReportMgr.exportRawDataStockTrans(userId, roleId, shopId, fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					for (int i = 0, n = lstData.size(); i < n; i++) {
						RawDataStockTransVO vo = lstData.get(i);
						if (SaleOrderStatus.NOT_YET_APPROVE.getValue().equals(vo.getApproved())) {
							vo.setStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.stock.trans.status.order.approving"));
						} else if (SaleOrderStatus.APPROVED.getValue().equals(vo.getApproved())) {
							vo.setStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.stock.trans.status.approved"));
						} else if (SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM.getValue().equals(vo.getApproved())) {
							vo.setStatus(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.raw.data.stock.trans.status.cancel"));
						}
					}
					Map<String, Object> beans = new HashMap<String, Object>();
					Shop shop = shopMgr.getShopById(shopId);
					beans.put("lstData", lstData);
					beans.put("shop", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("fromDate", fromDate);
					beans.put("toDate", toDate);
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.RPT_6_4_5_REPORT_STOCKTRANS_DATA_TEMPLATE;
					String fileName = ConstantManager.RPT_6_4_5_REPORT_STOCKTRANS_DATA_FILE_NAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.exportRawDataStockTrans"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao cham cong
	 * @author trietptm
	 * @return String
	 * @throws Exception 
	 * @since Oct 27, 2015
	 */
	public String exportTimeKeeping() throws Exception {
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		Map<Date, Integer> mapIndexOfDate = new HashMap<Date, Integer>();
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken() != null) {
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}

				Cycle cycle = cycleMgr.getCycleById(cycleId);
				if (cycle == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "kpi.cycle.not.exist"));
					return JSON;
				}
				Date fromDate = cycle.getBeginDate();
				Date toDate = cycle.getEndDate();
				int numDay = (int) DateUtil.dateDiff(fromDate, toDate) + 1;
				Long userId = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				Shop shop = shopMgr.getShopById(shopId);
				List<TimeKeepingVO> lstData = hoReportMgr.exportTimeKeeping(userId, roleId, shopId, fromDate, toDate, numDay);
				if (lstData != null && lstData.size() > 0) {
					int numCustomerVisited = 1; // so khach hang ghe tham trong ngay
					int quantity = 1;			// san luong phat sinh trong ngay
					List<ShopParam> lstConfigNumCustomerVisited = shopMgr.getConfigShopParam(shopId, ConstantManager.STAFF_VISITED_CUSTOMER_DAY);
					if (lstConfigNumCustomerVisited != null && lstConfigNumCustomerVisited.size() > 0 && lstConfigNumCustomerVisited.get(0).getValue() != null) {
						Integer value = NumberUtil.tryParseInteger(lstConfigNumCustomerVisited.get(0).getValue());
						if (value != null) {
							numCustomerVisited = value;
						}
					}
					List<ShopParam> lstConfigStaffQuantityDay = shopMgr.getConfigShopParam(shopId, ConstantManager.STAFF_QUANTITY_DAY);
					if (lstConfigStaffQuantityDay != null && lstConfigStaffQuantityDay.size() > 0 && lstConfigStaffQuantityDay.get(0).getValue() != null) {
						Integer value = NumberUtil.tryParseInteger(lstConfigStaffQuantityDay.get(0).getValue());
						if (value != null) {
							quantity = value;
						}
					}
					
					String outputName = ConstantManager.EXPORT_6_5_3_TIME_KEEPING + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("rpt.6.3.5.time.keeping.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultColumnWidth(4);
					//set static Column width
					ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 40, 100, 100, 130, 150, 100, 150);
					//Size Row
					ExcelPOIVTICTUtils.setRowHeight(sheetData, 0, 30);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.title");
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 0, 0, 20, 0, titleTemp, style.get(ExcelPOIVTICTUtils.TITLE_G));
					//info
					int rowInfo = 1;
					int colInfo = 1;
					ExcelPOIVTICTUtils.addCell(sheetData, colInfo++, rowInfo, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.shop"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colInfo, rowInfo, colInfo + 6, rowInfo, shop == null ? "" : (shop.getShopCode() + " - " + shop.getShopName()), style.get(ExcelPOIVTICTUtils.NORMAL));
					rowInfo++;
					colInfo = 1;
					ExcelPOIVTICTUtils.addCell(sheetData, colInfo++, rowInfo, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.cycle"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colInfo++, rowInfo, cycle.getCycleName(), style.get(ExcelPOIVTICTUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, colInfo++, rowInfo, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.time.now.report"), style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, colInfo++, rowInfo, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIVTICTUtils.NORMAL));
					//header
					int rowHeader1 = rowInfo + 2;
					int rowHeader2 = rowHeader1 + 1;
					int colHeader = 0;
					ExcelPOIVTICTUtils.setRowHeight(sheetData, rowHeader1, 20);
					ExcelPOIVTICTUtils.setRowHeight(sheetData, rowHeader2, 40);
					// STT
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colHeader, rowHeader1, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.number"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Mien
					colHeader++;
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colHeader, rowHeader1, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.region"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Vung
					colHeader++;
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colHeader, rowHeader1, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.area"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Ma NPP
					colHeader++;
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colHeader, rowHeader1, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.shop.code"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Ten NPP
					colHeader++;
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colHeader, rowHeader1, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.shop.name"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Ma NVBH
					colHeader++;
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colHeader, rowHeader1, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.staff.code"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Ten NVBH
					colHeader++;
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, colHeader, rowHeader1, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.staff.name"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Ngay
					int startColDay = colHeader + 1;
					int endColDay = startColDay + numDay - 1;
					Date date = fromDate;
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, startColDay, rowHeader1, endColDay, rowHeader1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.day"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					for (int i = 0; i < numDay; i++) {
						int col = startColDay + i;
						mapIndexOfDate.put(date, col);
						ExcelPOIVTICTUtils.addCell(sheetData, col, rowHeader2, DateUtil.convertDateByString(date, DateUtil.DATE_DD_MM), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
						date = DateUtil.addDate(date, 1);
					}
					// So ngay cong theo ke hoach
					colHeader = endColDay + 1;
					ExcelPOIVTICTUtils.setColumnWidth(sheetData, colHeader, 100);
					ExcelPOIVTICTUtils.addCell(sheetData, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.num.day.plan"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// So ngay cong thuc te
					colHeader++;
					ExcelPOIVTICTUtils.setColumnWidth(sheetData, colHeader, 100);
					ExcelPOIVTICTUtils.addCell(sheetData, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.num.day.real"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// So cong nghi phep
					colHeader++;
					ExcelPOIVTICTUtils.setColumnWidth(sheetData, colHeader, 100);
					ExcelPOIVTICTUtils.addCell(sheetData, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.num.day.off"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Tong cong
					colHeader++;
					ExcelPOIVTICTUtils.setColumnWidth(sheetData, colHeader, 100);
					ExcelPOIVTICTUtils.addCell(sheetData, colHeader, rowHeader2, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.num.total"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// Tong hop
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, endColDay + 1, rowHeader1, colHeader, rowHeader1, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.6.3.5.time.keeping.total"), style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					// fill row
					int rowData = rowHeader2;
					int numRealDay = 0;
					int colData = 0;
					long staffId = 0;
					for (int i = 0, size = lstData.size(); i < size; i++) {
						TimeKeepingVO vo = lstData.get(i);
						if (vo.getStaffId() != null && vo.getStaffId().longValue() != staffId) {
							rowData++;
							colData = 0;
							staffId = vo.getStaffId();
							// STT
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, rowData - 4, style.get(ExcelPOIVTICTUtils.ROW_DOTTED_CENTER));
							// Mien
							colData++;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, vo.getRegionCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
							// Vung
							colData++;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, vo.getAreaCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
							// Ma NPP
							colData++;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, vo.getShopCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
							// Ten NPP
							colData++;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, vo.getShopName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
							// Ma NVBH
							colData++;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, vo.getStaffCode(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));
							// Ten NVBH
							colData++;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, vo.getStaffName(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT_WRAP));

							for (int j = 0; j < numDay; j++) {
								int col = startColDay + j;
								ExcelPOIVTICTUtils.addCell(sheetData, col, rowData, "", style.get(ExcelPOIVTICTUtils.ROW_DOTTED_CENTER));
							}
							// So ngay cong theo ke hoach
							colData = endColDay + 1;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, vo.getNumPlanDay(), style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
							// So ngay cong thuc te
							colData++;
							if (i != 0) {
								ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData - 1, numRealDay, style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
								numRealDay = 0;
							}
							// So cong nghi phep
							colData++;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, "", style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
							// Tong cong
							colData++;
							ExcelPOIVTICTUtils.addCell(sheetData, colData, rowData, "", style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
						}
						if (vo.getNumCustomer() != null && vo.getNumCustomer() >= numCustomerVisited && vo.getQuantity() != null && vo.getQuantity() >= quantity) {
							ExcelPOIVTICTUtils.addCell(sheetData, mapIndexOfDate.get(vo.getDay()), rowData, "x", style.get(ExcelPOIVTICTUtils.ROW_DOTTED_CENTER));
							numRealDay++;
						}
						// So ngay cong thuc te
						if (i == size - 1) {
							ExcelPOIVTICTUtils.addCell(sheetData, colData - 2, rowData, numRealDay, style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT));
						}
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.flush();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				//************************************************************************				
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.report.HOReportAction.exportTimeKeeping()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			mapIndexOfDate.clear();
			mapIndexOfDate = null;
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao doanh so chuong trinh cua Khach Hang (key shop)
	 * @author vuongmq
	 * @return String
	 * @since 27/10/2015
	 */
	public String exportDSCTKH() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				KS keyShop = keyShopMgr.getKSById(ksId);
				if (keyShop == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("keyshop.error.not.exist.keyshop"));
					return JSON;
				}
				Cycle cycle = cycleMgr.getCycleById(cycleId);
				if (cycle == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("keyshop.tu.chu.ky.khong.ton.tai"));
					return JSON;
				}
				Cycle cycleTo = cycleMgr.getCycleById(cycleIdTo);
				if (cycleTo == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("keyshop.den.chu.ky.khong.ton.tai"));
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<RptSLDSVO> lstData = hoReportMgr.exportBCDSCTKH(staffIdRoot, roleId, strListShopId, ksId, cycleId, cycleIdTo);
				if (lstData != null && !lstData.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					Shop shop = shopMgr.getShopById(shopId);
					beans.put("lstData", lstData);
					beans.put("shopSelect", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("tenChuongTrinh", keyShop.getKsCode() + " - " + keyShop.getName());
					beans.put("tuChuKyCT", keyShop.getFromCycle() != null ? keyShop.getFromCycle().getCycleName() : "");
					beans.put("denChuKyCT", keyShop.getToCycle() != null ? keyShop.getToCycle().getCycleName() : "");
					beans.put("tuChuKy", cycle.getCycleName());
					beans.put("denChuKy", cycleTo.getCycleName());
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.EXPORT_DS_CT_KH_TEMPLATE;
					String fileName = ConstantManager.EXPORT_DS_CT_KH_FILENAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
	
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
	
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportDSCTKH()", createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao tien do thuc hien chuong trinh (BC 5.5)
	 * @author trietptm
	 * @return String
	 * @since Nov 28, 2015
	 */
	public String exportProgressProgram() {
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				KS keyShop = keyShopMgr.getKSById(ksId);
				if (keyShop == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("keyshop.error.not.exist.keyshop"));
					return JSON;
				}
				Cycle cycle = cycleMgr.getCycleById(cycleId);
				if (cycle == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("keyshop.tu.chu.ky.khong.ton.tai"));
					return JSON;
				}
				Cycle cycleTo = cycleMgr.getCycleById(cycleIdTo);
				if (cycleTo == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("keyshop.den.chu.ky.khong.ton.tai"));
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<ProgressProgramVO> lstData = hoReportMgr.exportProgressProgram(staffIdRoot, roleId, strListShopId, ksId, cycleId, cycleIdTo);
				if (lstData != null && !lstData.isEmpty()) {
					String textSum = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.sum");
					for (int i = 0, n = lstData.size(); i < n; i++) {
						ProgressProgramVO vo = lstData.get(i);
						if (vo.getRegionCode() == null) {
							lstData.remove(i);
							continue;
						} else if (vo.getAreaCode() == null) {
							vo.setRegionCode(textSum);
						} else if (vo.getShopCode() == null) {
							vo.setAreaCode(textSum);
						} else if (vo.getStaffCode() == null) {
							vo.setShopCode(textSum);
							vo.setShopName("");
						}
					}
					Map<String, Object> beans = new HashMap<String, Object>();
					Shop shop = shopMgr.getShopById(shopId);
					beans.put("lstData", lstData);
					beans.put("shopSelect", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("tenChuongTrinh", keyShop.getKsCode() + " - " + keyShop.getName());
					beans.put("tuChuKyCT", keyShop.getFromCycle() != null ? keyShop.getFromCycle().getCycleName() : "");
					beans.put("denChuKyCT", keyShop.getToCycle() != null ? keyShop.getToCycle().getCycleName() : "");
					beans.put("tuChuKy", cycle.getCycleName());
					beans.put("denChuKy", cycleTo.getCycleName());
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					//Xu ly xuat file excel
//					String fileTemplate = ConstantManager.EXPORT_DS_CT_KH_TEMPLATE;
//					String fileName = ConstantManager.EXPORT_DS_CT_KH_FILENAME;
					String fileTemplate = ConstantManager.RPT_5_5_REPORT_PROGRESS_PROGRAM_TEMPLATE;
					String fileName = ConstantManager.RPT_5_5_REPORT_PROGRESS_PROGRAM_FILE_NAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
	
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
	
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportProgressProgram()", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao Danh sach Aso khong hoat dong
	 * @author vuongmq
	 * @return String
	 * @since 03/11/2015
	 */
	public String exportAsoInactive() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Cycle cycle = cycleMgr.getCycleById(cycleId);
				if (cycle == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("kpi.cycle.not.exist"));
					return JSON;
				}
				Cycle cycleLast = cycleMgr.getCycleLast(cycleId);
				if (cycleLast == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("kpi.cycle.last.not.exist"));
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<RptSLDSVO> lstData = hoReportMgr.exportBC_ASO_INACTIVE(staffIdRoot, roleId, strListShopId, cycle.getId());
				if (lstData != null && !lstData.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					Shop shop = shopMgr.getShopById(shopId);
					beans.put("lstData", lstData);
					beans.put("shopSelect", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("cycleName", cycleLast.getCycleName() + " - " + cycle.getCycleName());
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.EXPORT_ASO_INACTIVE_TEMPLATE;
					String fileName = ConstantManager.EXPORT_ASO_INACTIVE_FILENAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
	
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
	
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportAsoInactive()", createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao chi tiet khuyen mai
	 * @author duongdt3
	 * @return String
	 * @since 06/11/2015
	 */
	public String exportBCCTKM() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (fDate == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.fromdate")));
					return JSON;
				}
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (tDate == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.todate")));
					return JSON;
				}
				if (DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate")));
					return JSON;
				}
				String msg = checkMaxNumberDayReportMedium(fDate, tDate);
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put(ERR_MSG, msg);
					return JSON;
				}
				
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<RptCTKMVO> lstData = hoReportMgr.exportBCCTKM(staffIdRoot, roleId, shopId, fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					Shop shop = shopMgr.getShopById(shopId);
					beans.put("lstData", lstData);
					beans.put("shopSelect", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("fromDate", fromDate);
					beans.put("toDate", toDate);
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.EXPORT_PROGRAM_DETAIL_TEMPLATE;
					String fileName = ConstantManager.EXPORT_PROGRAM_DETAIL_FILENAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
					
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
					
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportBCCTKM()", createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao thoi gian ghe tham KH
	 * @author vuongmq
	 * @return String
	 * @since 05/11/2015
	 */
	public String exportTGGTKH() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				String msg = "";
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					}
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.error.undefined", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.error.undefined", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > 0) {
					msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.compare.error.less.or.equal.tow.param", 
							Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.fromdate"), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(msg)) {
					msg = checkMaxNumberDayReportMedium(fDate, tDate);
				}
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put("errMsg", msg);
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<RptTGGTKHVO> lstData = hoReportMgr.exportTGGTKH(staffIdRoot, roleId, strListShopId, fDate, tDate);
				lstData = this.groupGSWithSameNVBH(lstData);
				if (lstData != null && !lstData.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					Shop shop = shopMgr.getShopById(shopId);
					beans.put("lstData", lstData);
					beans.put("shopSelect", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("fromDate", fromDate);
					beans.put("toDate", toDate);
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.EXPORT_TIME_VISIT_CUSTOMER_TEMPLATE;
					String fileName = ConstantManager.EXPORT_TIME_VISIT_CUSTOMER_FILENAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
	
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
	
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportTGGTKH()", createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Gom nhóm tất cả GS có cùng chung nvbh
	 * 
	 * @author: agile_dungdq3
	 * @return: List<RptTGGTKHVO>
	 * @throws:
	 * @since: 2:55:59 PM Feb 29, 2016
	 */
	private List<RptTGGTKHVO> groupGSWithSameNVBH(List<RptTGGTKHVO> lstData) {
		// TODO Auto-generated method stub
		int sizeList = lstData.size();
		for(int i = 0; i < sizeList - 1; i++) {
			RptTGGTKHVO rowI = lstData.get(i);
			for (int j = i + 1; j < sizeList; j++) {
				RptTGGTKHVO rowJ = lstData.get(j);
				if(rowI.getMaNVBH() != null && rowI.getMaNVBH().equals(rowJ.getMaNVBH()) 
						&& rowI.getDays() != null && rowI.getDays().equals(rowJ.getDays()) 
						&& rowI.getMaGSBH() != null) {
					StringBuilder sbMaGSBH = new StringBuilder(rowI.getMaGSBH());
					if (sbMaGSBH.length() > 0 ){
						sbMaGSBH.append(", ");
					}
					sbMaGSBH.append(rowJ.getMaGSBH());
					rowI.setMaGSBH(sbMaGSBH.toString());
					
					StringBuilder sbTenGSBH = new StringBuilder(rowI.getTenGSBH());
					if(sbTenGSBH.length() > 0) {
						sbTenGSBH.append(", ");
					}
					sbTenGSBH.append(rowJ.getTenGSBH());
					rowI.setTenGSBH(sbTenGSBH.toString());
					lstData.remove(j);
					sizeList--;
					j--;
				}
				
			}			
		}
		return lstData;
	}

	/**
	 * Bao cao khong ghe tham KH trong tuyen
	 * @author vuongmq
	 * @return String
	 * @since 05/11/2015
	 */
	public String exportKGTKHTT() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (fDate == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.fromdate")));
					return JSON;
				}
				if (tDate == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.todate")));
					return JSON;
				}
				if (DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate")));
					return JSON;
				}
				String msg = checkMaxNumberDayReportShort(fDate, tDate);
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put(ERR_MSG, msg);
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<RptKGTKHTTVO> lstData = hoReportMgr.exportKGTKHTT(staffIdRoot, roleId, strListShopId, fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					Shop shop = shopMgr.getShopById(shopId);
					beans.put("lstData", lstData);
					beans.put("shopSelect", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("fromDate", fromDate);
					beans.put("toDate", toDate);
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.EXPORT_NOT_VISIT_CUSTOMER_ROUTE_TEMPLATE;
					String fileName = ConstantManager.EXPORT_NOT_VISIT_CUSTOMER_ROUTE_FILENAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
	
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
	
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.exportKGTKHTT()", createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * 2.3 Bao cao chi tiet cac don mua hang
	 * Xu ly report2D3CTMH
	 * @author vuongmq
	 * @return String
	 * @throws IOException 
	 * @since Dec 29, 2015
	 */
	public String report2D3CTMH() throws IOException {
		actionStartTime = DateUtil.now();
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken().getRoleId() != null) {
				Shop shop = null;
				if (strListShopId != null) {
					Long shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					} else {
						shop = shopMgr.getShopById(shopId);
					}
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(errMsg) && fDate == null) {
					errMsg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg) && tDate == null) {
					errMsg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg) &&  DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
					errMsg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = super.checkMaxNumberDayReportMedium(fDate, tDate);;
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<Rpt_2_3_CTMH> lstData = hoReportMgr.export2D3CTMH(staffIdRoot, roleId, strListShopId, fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					String outputName = ConstantManager.EXPORT_PO_DETAIL_ORDER_FILENAME + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.2.3.ds.ctmh.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					//sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(23);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 150, 100, 100, 250, 150, 150, 100, 110, 140, 110, 250, 100, 140, 140, 140, 140, 110, 100);
					//Size Row
					ExcelPOIProcessUtils.setRowHeight(sheetData, 0, 25);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = R.getResource("baocao.2.3.ds.ctmh.title.name");
					ExcelPOIProcessUtils.addCell(sheetData, 5, 0, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					//header
					ExcelPOIProcessUtils.addCell(sheetData, 3, 1, R.getResource("baocao.1.3.kqdt.header.shop"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 1, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 3, 2, R.getResource("common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 2, R.getResource("common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 1, shop.getShopCode() + " - " + shop.getShopName(), style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 1, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 2, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 2, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
					int rowStartHeader = 4;
					ExcelPOIProcessUtils.setRowHeight(sheetData, rowStartHeader, 25);
					for (int i = 0; i < 19; i++) {
						ExcelPOIProcessUtils.addCell(sheetData, i, rowStartHeader, R.getResource("baocao.2.3.ds.ctmh.header" + i), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					int d = 5;
					Rpt_2_3_CTMH salesVO = null;
					int rowStart = d;
					int group5 = 0;
					int group4 = 0;
					int group3 = 0;
					int group2 = 0;
					List<Integer> start5 = new ArrayList<Integer>();
					List<Integer> end5 = new ArrayList<Integer>();
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						//ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						if (StringUtil.isNullOrEmpty(salesVO.getMaMien())) {
							start5.add(group5 + rowStart);
							end5.add(i + rowStart - 1);
							group5 = i + 1;
							group4++;
							group3++;
							group2++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNPP())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getProductCode())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
						}
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, (i + 1), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						if (StringUtil.isNullOrEmpty(salesVO.getProductCode())) {
							if (StringUtil.isNullOrEmpty(salesVO.getMaMien())) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("common.sum"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
						} else {
							if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("common.sum"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
						if (StringUtil.isNullOrEmpty(salesVO.getProductCode())) {
							ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getOrderNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getInvoiceNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getTypeStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getPoDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getStatusStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getProductCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getProductName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							
							ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getVatAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getDiscount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getTotal(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 18, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getOrderNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getInvoiceNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getTypeStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getPoDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getStatusStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getProductCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getProductName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getPackageQuantity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							
							ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getVatAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getDiscount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getTotal(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getDeliveryDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getPackageQuantityReceived(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
						d++;
					}
					for (int i = 0, sz = start2.size(); i < sz; i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0, sz = start3.size(); i < sz; i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0, sz = start4.size(); i < sz; i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					for (int i = 0, sz = start5.size(); i < sz; i++) {
						sheetData.groupRow(start5.get(i), end5.get(i));
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
					lstData = null;
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.report2D3CTMH()", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * Chung tu hang hoa mua vao
	 * @author dungnt19
	 * @return String
	 * @since 19/01/2016
	 */
	public String report2D1CTHHMV() throws IOException {
		actionStartTime = DateUtil.now();
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken().getRoleId() != null) {
				Shop shop = null;
				if (strListShopId != null) {
					Long shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					} else {
						shop = shopMgr.getShopById(shopId);
					}
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(errMsg) && fDate == null) {
					errMsg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg) && tDate == null) {
					errMsg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg) &&  DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
					errMsg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = super.checkMaxNumberDayReportLong(fDate, tDate);;
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<Rpt_2_1_CTHHMV> lstData = hoReportMgr.export2D1CTHHMV(staffIdRoot, roleId, strListShopId, fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					String outputName = ConstantManager.RPT_2_1_CTHHMV_FILE_NAME + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.2.1.ds.ctmh.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					//sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(23);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 150, 100, 100, 250, 150, 150, 140, 140, 140, 140, 140, 140, 140);
					//Size Row
					ExcelPOIProcessUtils.setRowHeight(sheetData, 0, 25);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = R.getResource("baocao.2.1.ds.ctmh.title.name");
					ExcelPOIProcessUtils.addCell(sheetData, 5, 0, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					//header
					ExcelPOIProcessUtils.addCell(sheetData, 3, 1, R.getResource("baocao.1.3.kqdt.header.shop"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 1, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 3, 2, R.getResource("common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 2, R.getResource("common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 1, shop.getShopCode() + " - " + shop.getShopName(), style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 1, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 2, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 2, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
					int rowStartHeader = 4;
					ExcelPOIProcessUtils.setRowHeight(sheetData, rowStartHeader, 25);
					for (int i = 0; i < 14; i++) {
						ExcelPOIProcessUtils.addCell(sheetData, i, rowStartHeader, R.getResource("baocao.2.1.ds.ctmh.header" + i), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					int d = 5;
					Rpt_2_1_CTHHMV salesVO = null;
					int rowStart = d;
					int group5 = 0;
					int group4 = 0;
					int group3 = 0;
					int group2 = 0;
					List<Integer> start5 = new ArrayList<Integer>();
					List<Integer> end5 = new ArrayList<Integer>();
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						//ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						if (StringUtil.isNullOrEmpty(salesVO.getMaMien())) {
							start5.add(group5 + rowStart);
							end5.add(i + rowStart - 1);
							group5 = i + 1;
							group4++;
							group3++;
							group2++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNPP())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
						}
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, (i + 1), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						if (StringUtil.isNullOrEmpty(salesVO.getOrderNumber())) {
							if (StringUtil.isNullOrEmpty(salesVO.getMaMien())) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("common.sum"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							
							ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getDiscount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getTotal(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
						} else {
							if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("common.sum"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							
							ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getAmount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getDiscount(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getTotal(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						}
							ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getOrderNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getAsnNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getInvoiceNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getStatusStr(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getPoDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
							ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getDeliveryDate(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
//						}
						d++;
					}
					for (int i = 0, sz = start3.size(); i < sz; i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0, sz = start4.size(); i < sz; i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					for (int i = 0, sz = start5.size(); i < sz; i++) {
						sheetData.groupRow(start5.get(i), end5.get(i));
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
					lstData = null;
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.report2D1CTHHMV()", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * So sanh so luong dat hang va mua hang theo don hang mua
	 * @author dungnt19
	 * @return String
	 * @since 19/01/2016
	 */
	public String report2D2SSSLDHMH() throws IOException {
		actionStartTime = DateUtil.now();
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null && currentUser.getRoleToken().getRoleId() != null) {
				Shop shop = null;
				if (strListShopId != null) {
					Long shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
					} else {
						shop = shopMgr.getShopById(shopId);
					}
				} else {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined");
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (StringUtil.isNullOrEmpty(errMsg) && fDate == null) {
					errMsg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg) && tDate == null) {
					errMsg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg) &&  DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
					errMsg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = super.checkMaxNumberDayReportLong(fDate, tDate);;
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<Rpt_2_2_SSSLDHMH> lstData = hoReportMgr.export2D2SSSLDHMH(staffIdRoot, roleId, strListShopId, fDate, tDate);
				if (lstData != null && !lstData.isEmpty()) {
					String outputName = ConstantManager.RPT_2_2_SSSLDHMH_FILE_NAME + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					//Init XSSF workboook
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.2.2.ds.sssldhgh.sheet.name"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					//sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(23);
					//set static Column width
					ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 100, 100, 100, 250, 140, 140, 140, 150, 250, 100, 100, 100);
					//Size Row
					ExcelPOIProcessUtils.setRowHeight(sheetData, 0, 25);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String titleTemp = R.getResource("baocao.2.2.ds.sssldhgh.title.name");
					ExcelPOIProcessUtils.addCell(sheetData, 5, 0, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BROWN));
					//header
					ExcelPOIProcessUtils.addCell(sheetData, 3, 1, R.getResource("baocao.1.3.kqdt.header.shop"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 1, R.getResource("common.time.now.report"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 3, 2, R.getResource("common.date.fromdate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 5, 2, R.getResource("common.date.todate"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 1, shop.getShopCode() + " - " + shop.getShopName(), style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 1, DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR), style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 4, 2, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIProcessUtils.addCell(sheetData, 6, 2, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
					int rowStartHeader = 4;
					ExcelPOIProcessUtils.setRowHeight(sheetData, rowStartHeader, 25);
					for (int i = 0; i < 13; i++) {
						ExcelPOIProcessUtils.addCell(sheetData, i, rowStartHeader, R.getResource("baocao.2.2.ds.sssldhgh.header" + i), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					int d = 5;
					Rpt_2_2_SSSLDHMH salesVO = null;
					int rowStart = d;
					int group5 = 0;
					int group4 = 0;
					int group3 = 0;
					int group2 = 0;
					List<Integer> start5 = new ArrayList<Integer>();
					List<Integer> end5 = new ArrayList<Integer>();
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						//ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						if (StringUtil.isNullOrEmpty(salesVO.getMaMien())) {
							start5.add(group5 + rowStart);
							end5.add(i + rowStart - 1);
							group5 = i + 1;
							group4++;
							group3++;
							group2++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNPP())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getProductCode())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
						}
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, (i + 1), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						if (StringUtil.isNullOrEmpty(salesVO.getProductCode())) {
							if (StringUtil.isNullOrEmpty(salesVO.getMaMien())) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("common.sum"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
						} else {
							if (StringUtil.isNullOrEmpty(salesVO.getMaVung())) {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, R.getResource("common.sum"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
						if (StringUtil.isNullOrEmpty(salesVO.getProductCode())) {
							ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getOrderNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getAsnNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getInvoiceNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getProductCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getProductName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_BOLD));
							
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getPackageQuantity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getPackageQuantityReceived(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_BOLD));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getPercentSuccess(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT_BOLD_TWO));
						} else {
							ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getOrderNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getAsnNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getInvoiceNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getProductCode(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getProductName(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getPackageQuantity(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getPackageQuantityReceived(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getPercentSuccess(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_FLOAT_TWO));
						}
						d++;
					}
					for (int i = 0, sz = start2.size(); i < sz; i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0, sz = start3.size(); i < sz; i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0, sz = start4.size(); i < sz; i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					for (int i = 0, sz = start5.size(); i < sz; i++) {
						sheetData.groupRow(start5.get(i), end5.get(i));
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
					lstData = null;
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.HOReportAction.report2D2SSSLDHMH()", createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
			if (out != null) {
				out.close();
			}
		}
		return JSON;
	}
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public String getShopStr() {
		return shopStr;
	}

	public void setShopStr(String shopStr) {
		this.shopStr = shopStr;
	}

	public String getPromationProgramStr() {
		return promationProgramStr;
	}

	public void setPromationProgramStr(String promationProgramStr) {
		this.promationProgramStr = promationProgramStr;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Long getCurShopId() {
		return curShopId;
	}

	public void setCurShopId(Long curShopId) {
		this.curShopId = curShopId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public Date getDateSys() {
		return dateSys;
	}

	public void setDateSys(Date dateSys) {
		this.dateSys = dateSys;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public String getNpp() {
		return npp;
	}

	public void setNpp(String npp) {
		this.npp = npp;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getKhuVuc() {
		return khuVuc;
	}

	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(Integer orderSource) {
		this.orderSource = orderSource;
	}

	public String getStrListNvbhId() {
		return strListNvbhId;
	}

	public void setStrListNvbhId(String strListNvbhId) {
		this.strListNvbhId = strListNvbhId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Integer getSaleOrderType() {
		return saleOrderType;
	}

	public void setSaleOrderType(Integer saleOrderType) {
		this.saleOrderType = saleOrderType;
	}

	public Integer getApprovedStep() {
		return approvedStep;
	}

	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getStrListProductName() {
		return strListProductName;
	}

	public void setStrListProductName(String strListProductName) {
		this.strListProductName = strListProductName;
	}

	public String getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public List<CycleVO> getLstKpi() {
		return lstKpi;
	}

	public void setLstKpi(List<CycleVO> lstKpi) {
		this.lstKpi = lstKpi;
	}

	public List<CycleVO> getLstKeyShop() {
		return lstKeyShop;
	}

	public void setLstKeyShop(List<CycleVO> lstKeyShop) {
		this.lstKeyShop = lstKeyShop;
	}

	public String getStrListProductId() {
		return strListProductId;
	}

	public void setStrListProductId(String strListProductId) {
		this.strListProductId = strListProductId;
	}

	public Long getIdParentCat() {
		return idParentCat;
	}

	public void setIdParentCat(Long idParentCat) {
		this.idParentCat = idParentCat;
	}

	public List<ProductInfo> getLstSubCategoryCat() {
		return lstSubCategoryCat;
	}

	public void setLstSubCategoryCat(List<ProductInfo> lstSubCategoryCat) {
		this.lstSubCategoryCat = lstSubCategoryCat;
	}

	public Boolean getCheckExport() {
		return checkExport;
	}

	public void setCheckExport(Boolean checkExport) {
		this.checkExport = checkExport;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(String lstShopId) {
		this.lstShopId = lstShopId;
	}

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	public List<CycleVO> getLstCycle() {
		return lstCycle;
	}

	public void setLstCycle(List<CycleVO> lstCycle) {
		this.lstCycle = lstCycle;
	}

	public List<ProductInfo> getLstCategoryType() {
		return lstCategoryType;
	}

	public void setLstCategoryType(List<ProductInfo> lstCategoryType) {
		this.lstCategoryType = lstCategoryType;
	}

	public List<ProductInfo> getLstSubCategoryType() {
		return lstSubCategoryType;
	}

	public String getSubCatIdStr() {
		return subCatIdStr;
	}

	public void setSubCatIdStr(String subCatIdStr) {
		this.subCatIdStr = subCatIdStr;
	}

	public void setLstSubCategoryType(List<ProductInfo> lstSubCategoryType) {
		this.lstSubCategoryType = lstSubCategoryType;
	}

	public Integer getTypeReport() {
		return typeReport;
	}

	public void setTypeReport(Integer typeReport) {
		this.typeReport = typeReport;
	}

	public String getTennpp() {
		return tennpp;
	}

	public void setTennpp(String tennpp) {
		this.tennpp = tennpp;
	}

	public String getDiaChiNPP() {
		return diaChiNPP;
	}

	public void setDiaChiNPP(String diaChiNPP) {
		this.diaChiNPP = diaChiNPP;
	}

	public Long getChooseShopId() {
		return chooseShopId;
	}

	public void setChooseShopId(Long chooseShopId) {
		this.chooseShopId = chooseShopId;
	}

	public Long getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public String getCatIdStr() {
		return catIdStr;
	}

	public void setCatIdStr(String catIdStr) {
		this.catIdStr = catIdStr;
	}

	public Long getKsId() {
		return ksId;
	}

	public void setKsId(Long ksId) {
		this.ksId = ksId;
	}

	public Long getCycleIdTo() {
		return cycleIdTo;
	}

	public void setCycleIdTo(Long cycleIdTo) {
		this.cycleIdTo = cycleIdTo;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public List<BasicVO> getLstYearSysTemConfig() {
		return lstYearSysTemConfig;
	}

	public void setLstYearSysTemConfig(List<BasicVO> lstYearSysTemConfig) {
		this.lstYearSysTemConfig = lstYearSysTemConfig;
	}

}
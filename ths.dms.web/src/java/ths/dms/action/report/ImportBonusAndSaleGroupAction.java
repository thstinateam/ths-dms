/**
 * 
 */
package ths.dms.action.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;

import ths.core.entities.vo.rpt.RptBCKD6;
import ths.core.entities.vo.rpt.RptBCKD6VO;
import ths.dms.core.report.CrmReportMgr;



/**
 * The Class ImportBonusAndSaleGroupAction.
 *
 * @author nhanlt
 * @see BAO CAO VANSALES
 */
public class ImportBonusAndSaleGroupAction extends AbstractAction{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3470234290573825410L;	
	
	/** The from date. */
	private String fromDate;
	
	/** The to date. */
	private String toDate;	
	
	/** The crm report mgr. */
	private CrmReportMgr crmReportMgr;
	
	/** The list data. */
	//private List<RptBCKD6VO> listData;
	
	/** The parameters report. */
	private HashMap<String, Object> parametersReport;
	
	/** The location. */
	private String location;
	


	/**
	 * Gets the from date.
	 *
	 * @return the from date
	 */
	public String getFromDate() {
		return fromDate;
	}


	/**
	 * Sets the from date.
	 *
	 * @param fromDate the new from date
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	/**
	 * Gets the to date.
	 *
	 * @return the to date
	 */
	public String getToDate() {
		return toDate;
	}


	/**
	 * Sets the to date.
	 *
	 * @param toDate the new to date
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * Gets the list data.
	 *
	 * @return the list data
	 */
//	public List<RptBCKD6VO> getListData() {
//		return listData;
//	}
//
//
//	/**
//	 * Sets the list data.
//	 *
//	 * @param listData the new list data
//	 */
//	public void setListData(List<RptBCKD6VO> listData) {
//		this.listData = listData;
//	}


	/**
	 * Gets the parameters report.
	 *
	 * @return the parameters report
	 */
	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}


	/**
	 * Sets the parameters report.
	 *
	 * @param parametersReport the parameters report
	 */
	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}


	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}


	/**
	 * Sets the location.
	 *
	 * @param location the new location
	 */
	public void setLocation(String location) {
		this.location = location;
	}


	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {		
		super.prepare();			
		crmReportMgr =  (CrmReportMgr)context.getBean("crmReportMgr");
	}
	

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute(){		
		return SUCCESS;
	}
	
	
	
	/**
	 * View sale staff.
	 *
	 * @return the string
	 * @author hungtt
	 * @since Jan 31, 2013
	 */
	@SuppressWarnings("unchecked")
	public String viewImportBonus()	{
		location = ShopReportTemplate.CRM_DSNBNH.getTemplatePath(true, FileExtension.JASPER);
		parametersReport = new HashMap<String, Object>();
		List<RptBCKD6VO> listData = null;
		if(!sessionData){
			clearDataReportSession();
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			try{
				if(fDate != null){
					parametersReport.put("fDate",DateUtil.toDateString(fDate));
				}
				if(tDate != null){
					parametersReport.put("tDate", DateUtil.toDateString(tDate));
				}
				RptBCKD6 bckd6 =  crmReportMgr.getRptBCKD6(fDate, tDate);
				if(bckd6==null){
					listData = new ArrayList<RptBCKD6VO>();
					listData.add(new RptBCKD6VO());
				}else {
					parametersReport.put("soNgayBanHang", bckd6.getSoNgayBanHang());
					parametersReport.put("soNgayThucHien", bckd6.getSoNgayThucHien());
					parametersReport.put("soNgayNghiLe", bckd6.getSoNgayNghiLe());
					parametersReport.put("tiendochuan", bckd6.getTienDoChuan().intValue());
					listData = bckd6.getLstDetail();					
					listData.add(0,new RptBCKD6VO());
				}
			}catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}			
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, listData);
			session.setAttribute(ConstantManager.SESSION_REPORT_TOTAL_ROW, listData!=null?listData.size():null);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, parametersReport);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);			
			ReportUtils.exportHtml(response, parametersReport, dataSource, ShopReportTemplate.CRM_DSNBNH);			
		} else {
			if(session.getAttribute(ConstantManager.SESSION_REPORT_DATA)!= null){
				listData = (List<RptBCKD6VO>)session.getAttribute(ConstantManager.SESSION_REPORT_DATA);
			}
			if(session.getAttribute(ConstantManager.SESSION_REPORT_PARAM)!= null){
				parametersReport = (HashMap<String, Object>)session.getAttribute(ConstantManager.SESSION_REPORT_PARAM);
			}
			FileExtension ext = FileExtension.parseValue(formatType);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
			String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource,ShopReportTemplate.CRM_DSNBNH);	
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			return JSON;
		}
		return SUCCESS;
	}
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.report;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ReportUtils;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;
import ths.dms.web.utils.report.excel.ExcelPOIVTICTUtils;
import ths.dms.web.utils.report.excel.SXSSFReportHelper;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.GS_1_2_VO;
import ths.dms.core.entities.vo.GS_1_4_VO;
import ths.dms.core.entities.vo.GS_4_2_VO;
import ths.dms.core.entities.vo.GS_KHTT;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.TimeVisitCustomerVO;
import ths.core.entities.vo.rpt.RptBCTGTMVO;
import ths.core.entities.vo.rpt.Rpt_TGLVCNVBH_VO;
import ths.dms.core.entities.vo.rpt.ho.RptCTKMVO;
import ths.dms.core.memcached.MemcachedUtils;
import ths.dms.core.report.ShopReportMgr;

/**
 * Action bao cao danh muc cho HO
 * 
 * @author hunglm16
 * @since September 18, 2015
 */
public class SuperivseReportAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private static final int VALUE_DEFAULT_INT = -1;
	public final int ZEZO_INT_G = 0;//Dinh nghia cho gia tri tat ca

	private ShopMgr shopMgr;
	protected CommonMgr commonMgr;
	protected ApParamMgr apParamMgr;
	private ShopReportMgr shopReportMgr;
	private ProductInfoMgr productInfoMgr;

	private Long cycleId;
	private Long shopId;

	private int startNpp;
	private int endNpp;
	private int startVung;
	private int endVung;
	private int startMien;
	private int endMien;
	private int countNpp;
	private int countVung;
	private int countMien;
	private int countOther;
	private int startOther;
	private int endOther;
	private int start4;
	private int start5;
	private int start6;
	private int start7;
	private int start8;
	private int end4;
	private int end5;
	private int end6;
	private int end7;
	private int end8;
	private int count4;
	private int count5;
	private int count6;
	private int count7;
	private int count8;

	private Integer staffType;
	private Integer check;

	private String fromHour;
	private String fromMinute;
	private String toHour;
	private String toMinute;
	private String date;
	private String toDate;
	private String fromDate;
	private String npp;
	private String vung;
	private String khuVuc;
	private String shopCode;
	private String lstCat;
	private String lstSubCat;
	private String strListNVBH;
	private String lstProductName;
	private String strListShopId;
	private String lstProduct;
	private String location;
	private String staffCode;
	private String staffSaleCode;
	private String staffOwnerCode;
	private Integer minutes;
	private String lstNVGSCode;
	private String lstNVBHCode;
	private String strListNvbhId;
	private String strListGsId;

	private Date sysDate;

	private List<CycleVO> lstCycle;
	private List<ProductInfo> lstCategoryType;
	private List<ProductInfo> lstSubCategoryType;
	private List<ProductInfo> lstSubCategoryCat;
	private HashMap<String, Object> parametersReport;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		shopReportMgr = (ShopReportMgr) context.getBean("shopReportMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");

		startNpp = VALUE_DEFAULT_INT;
		startVung = VALUE_DEFAULT_INT;
		startMien = VALUE_DEFAULT_INT;
		endNpp = VALUE_DEFAULT_INT;
		endVung = VALUE_DEFAULT_INT;
		endMien = VALUE_DEFAULT_INT;
		startOther = VALUE_DEFAULT_INT;
		endOther = VALUE_DEFAULT_INT;
		start4 = VALUE_DEFAULT_INT;
		start5 = VALUE_DEFAULT_INT;
		start6 = VALUE_DEFAULT_INT;
		start7 = VALUE_DEFAULT_INT;
		start8 = VALUE_DEFAULT_INT;
		end4 = VALUE_DEFAULT_INT;
		end5 = VALUE_DEFAULT_INT;
		end6 = VALUE_DEFAULT_INT;
		end7 = VALUE_DEFAULT_INT;
		end8 = VALUE_DEFAULT_INT;
		count4 = VALUE_DEFAULT_INT;
		count5 = VALUE_DEFAULT_INT;
		count6 = VALUE_DEFAULT_INT;
		count7 = VALUE_DEFAULT_INT;
		count8 = VALUE_DEFAULT_INT;
	}

	@Override
	public String execute() throws Exception {
		Date now = commonMgr.getSysDate();
		try {
			sysDate = now;
			date = DateUtil.toDateString(now, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (currentUser != null && currentUser.getUserName() != null) {
				shopId = currentUser.getShopRoot().getShopId();
				shopCode = currentUser.getShopRoot().getShopCode();
			}
			Cycle cycleSys = cycleMgr.getCycleByCurrentSysdate();
			//Lay mac dinh cho tu ngay dn ngay
			if (cycleSys != null && cycleSys.getBeginDate() != null) {
				fromDate = DateUtil.toDateString(cycleSys.getBeginDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
			} else {
				fromDate = DateUtil.toDateString(sysDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			toDate = DateUtil.toDateString(sysDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			CycleFilter filter = new CycleFilter();
			filter.setLessYear(2);
			filter.setEndDate(now);
			lstCycle = cycleMgr.getListCycleByFilter(filter);
			//Lay danh sach nganh hang - hoanv25 July 10/2015
			lstCategoryType = new ArrayList<ProductInfo>();
			ProductInfoMgr productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
			ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfoStock(null, null, null, null, ActiveType.RUNNING, ProductType.CAT);
			if (tmp != null) {
				lstCategoryType = tmp.getLstObject();
			}
			tmp = productInfoMgr.getListProductInfoStock(null, null, null, null, ActiveType.RUNNING, ProductType.SUB_CAT);
			lstSubCategoryType = new ArrayList<ProductInfo>();
			if (tmp != null) {
				lstSubCategoryType = tmp.getLstObject();
			}
			FileUtility.createTempDir();
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.SuperivseReportAction.execute"), createLogErrorStandard(DateUtil.now()));
		}
		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 * @author longnh15 @test
	 * @see Báo cáo kế hoạch tiêu thụ
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach code
	 */
	public String exportBCKHTT() {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);

				String tempCat = "";
				if (lstCat != null && !"0".equals(lstCat)) {
					ProductInfo catInfo = productInfoMgr.getProductInfoById(Long.parseLong(lstCat));
					tempCat = catInfo.getProductInfoName();
				} else {
					lstCat = null;
					tempCat = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tatca");
				}

				String tempSubCat = "";
				if (lstSubCat != null && !"0".equals(lstSubCat)) {
					ProductInfo subCatInfo = productInfoMgr.getProductInfoById(Long.parseLong(lstSubCat));
					tempSubCat = subCatInfo.getProductInfoName();
				} else {
					lstSubCat = null;
					tempSubCat = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tatca");
				}
				String tempProduct = "";
				if (lstProduct != null && !"".equals(lstProduct))
					tempProduct = lstProductName;
				else {
					lstProduct = null;
					tempProduct = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tatca");
				}
				List<GS_KHTT> lstData = shopReportMgr.getListKHTT(cycleId, shopId, fDate, tDate, lstProduct, lstSubCat, lstCat, getStrListUserId()); //strProduct, strSubCat, strCat
				String outputName = ConstantManager.EXPORT_BAO_CAO_KHTT + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.khtt.sheet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(23);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 150, 150, 150, 150, 150, 150, 150, 150, 150, 180, 180, 180, 180, 180, 180, 250, 250);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 35, 15, 15, 15, 15, 15);
				sheetData.setDisplayGridlines(false);
				//Tittle
				String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.title.name");
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 20, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
				//info					
				ExcelPOIProcessUtils.addCell(sheetData, 8, 3, vung, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 3, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));

				ExcelPOIProcessUtils.addCell(sheetData, 12, 3, tempCat, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 14, 3, tempSubCat, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 16, 3, tempProduct, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));

				ExcelPOIProcessUtils.addCell(sheetData, 8, 4, shop.getShopCode(), style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 4, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 12, 4, fromDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 14, 4, toDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 5, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				//header
				ExcelPOIProcessUtils.addCell(sheetData, 7, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 11, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.nganh"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 13, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.nhom"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 15, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.sku"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.chuky"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 11, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tungay"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 13, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.denngay"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.diachi"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));

				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 8, 0, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, 8, 1, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 2, 8, 2, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 3, 8, 3, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 4, 8, 4, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 5, 8, 5, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.mausm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 6, 8, 6, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tenusm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 7, 8, 7, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.manvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 8, 8, 8, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.nvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 9, 8, 9, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.maroute"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 10, 8, 10, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tenroute"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 11, 8, 11, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.masp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 12, 8, 12, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.sp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 13, 8, 13, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.giatruoc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 14, 8, 14, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.giasau"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 15, 8, 17, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.sanluong"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 15, 9, 15, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.kehoach"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 16, 9, 16, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.thuchien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 17, 9, 17, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tiendo"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 18, 8, 20, 8, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.doanhso"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 18, 9, 18, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.kehoach"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 19, 9, 19, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.thuchien"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 20, 9, 20, 9, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tiendo"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				int d = 10;
				if (lstData.size() > 0) {
					GS_KHTT salesVO = null;
					int rowStart = d;
					int group3 = 0, group2 = 0, group1 = 0;
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						if (StringUtil.isNullOrEmpty(salesVO.getMaMien())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaNPP())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(salesVO.getMaSP())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						}
						if (salesVO.getMaVung() == null) {
							ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, d, 6, d, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tonghethong"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT_ORANGE03));
							ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 7, d, 14, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE03));
							ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getQuantityPlan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE03));
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getQuantityApproved(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE03));
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getQuantityProgress().toString() + "%", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE03));
							ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getAmountPlan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE03));
							ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getAmountApproved(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE03));
							ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getAmountProgress().toString() + "%", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT_ORANGE03));
						} else {
							if (salesVO.getMaMien() == null) {
								ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
								ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 1, d, 4, d, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tong"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							} else if (salesVO.getMaNPP() == null) {
								ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 2, d, 4, d, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tong"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							} else if (salesVO.getMaSP() == null) {
								ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 2, d, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 3, d, 4, d, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.khtt.header.tong"), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));

							} else {
								ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
								ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
								ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							}
							ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getMaSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getTenSP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
							ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getGia(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getGiachuaVAT(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getQuantityPlan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getQuantityApproved(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getQuantityProgress().toString() + "%", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getAmountPlan(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getAmountApproved(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
							ExcelPOIProcessUtils.addCell(sheetData, 20, d, salesVO.getAmountProgress().toString() + "%", style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));

						}

						d++;
					}
					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.SuperivseReportAction.exportBCKHTT"), createLogErrorStandard(DateUtil.now()));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * Lay thong tin cap cha cua don vi
	 * 
	 * @modify hunglm16
	 * @param shop
	 * @since 26/09/2015
	 * @description Quy hoach code
	 */
	public void getInfoParentShop(Shop shop) {
		Shop shop1 = null, shop2 = null, shop3 = null, shop4 = null;
		if (shop != null) {
			shop1 = shop.getParentShop();
			if (shop1 != null) {
				shop2 = shop1.getParentShop();
				if (shop2 != null) {
					shop3 = shop2.getParentShop();
					if (shop3 != null) {
						shop4 = shop3.getParentShop();
					}
				}
			}
		}
		if (shop1 != null && shop2 != null && shop3 != null && shop4 != null) {//npp
			npp = shop.getShopCode();
			khuVuc = shop1.getShopCode();
			vung = shop2.getShopCode();
		} else if (shop1 != null && shop2 != null && shop3 != null && shop4 == null) {//vung
			khuVuc = shop.getShopCode();
			vung = shop1.getShopCode();
		} else if (shop1 != null && shop2 != null && shop3 == null) {//khu vuc
			vung = shop.getShopCode();
		}
	}

	/**
	 * View time visit customer.
	 * 
	 * @return the string - BAO CAO GHE THAM KHACH HANG
	 * @author hungtt
	 * @since Feb 5, 2013
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach code
	 */
	@SuppressWarnings("unchecked")
	public String viewTimeVisitCustomer2() {
		location = ShopReportTemplate.GSKH_BCGTKH.getTemplatePath(true, FileExtension.JASPER);
		parametersReport = new HashMap<String, Object>();
		List<TimeVisitCustomerVO> listData = null;
		if (!sessionData) {
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, null);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, null);
			formatType = FileExtension.HTML.getName();
			// get data for parameters
			parametersReport.put("cDate", DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR));
			parametersReport.put("fDate", fromDate);
			parametersReport.put("tDate", toDate);
			try {
				if (shopId != null) {
					Shop shop = shopMgr.getShopById(shopId);
					if (shop != null) {
						parametersReport.put("sName", shop.getShopName());
						if (!StringUtil.isNullOrEmpty(shop.getAddress())) {
							parametersReport.put("sAddress", shop.getAddress());
						} else {
							parametersReport.put("sAddress", "");
						}
						if (!StringUtil.isNullOrEmpty(shop.getPhone())) {
							parametersReport.put("sPhone", shop.getPhone());
						} else {
							parametersReport.put("sPhone", "");
						}
					}
				}
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Calendar cal = Calendar.getInstance();
				cal.setTime(tDate);
				Date ffDate = null;
				Date ttDate = null;
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					ffDate = DateUtil.toDate(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					ttDate = DateUtil.toDate(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				}
				Long superId = null;
				Long staffId = null;
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					Shop s = shopMgr.getShopByCode(shopCode);
					if (s == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "catalog.unit.tree"));
						return JSON;
					}
				}
				if (!StringUtil.isNullOrEmpty(staffCode)) {
					Staff staff = staffMgr.getStaffByCode(staffCode);
					if (staff == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "contract.staff.code"));
						return JSON;
					}
					staffId = staff.getId();
				}
				if (!StringUtil.isNullOrEmpty(staffOwnerCode)) {
					Staff staff = staffMgr.getStaffByCode(staffOwnerCode);
					if (staff == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "catalog.supervisor.staff.code"));
						return JSON;
					}
					superId = staff.getId();
				}
				parametersReport.put("cDate", DateUtil.toDateString(DateUtil.now(), DateUtil.DATETIME_FORMAT_STR));
				listData = shopReportMgr.getListVisitCustomer2(ffDate, ttDate, shopId, superId, staffId, false);
				if (listData != null && listData.size() > 0) {
					listData.add(0, new TimeVisitCustomerVO());
				}

			} catch (Exception e) {
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.SuperivseReportAction.viewTimeVisitCustomer2"), createLogErrorStandard(DateUtil.now()));
			}
			session.setAttribute(ConstantManager.SESSION_REPORT_DATA, listData);
			session.setAttribute(ConstantManager.SESSION_REPORT_TOTAL_ROW, listData != null ? listData.size() : null);
			session.setAttribute(ConstantManager.SESSION_REPORT_PARAM, parametersReport);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
			ReportUtils.exportHtml(response, parametersReport, dataSource, ShopReportTemplate.GSKH_BCGTKH);
		} else {
			if (session.getAttribute(ConstantManager.SESSION_REPORT_DATA) != null) {
				listData = (List<TimeVisitCustomerVO>) session.getAttribute(ConstantManager.SESSION_REPORT_DATA);
			}
			if (session.getAttribute(ConstantManager.SESSION_REPORT_PARAM) != null) {
				parametersReport = (HashMap<String, Object>) session.getAttribute(ConstantManager.SESSION_REPORT_PARAM);
			}
			FileExtension ext = FileExtension.parseValue(formatType);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
			String outputPath = ReportUtils.exportFromFormat(ext, parametersReport, dataSource, ShopReportTemplate.GSKH_BCGTKH);
			result.put(ERROR, false);
			result.put(REPORT_PATH, outputPath);
			return JSON;
		}
		return SUCCESS;
	}

	/**
	 * GS 1.2 - Thoi gian ghe tham cua nhan vien ban hang
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach code
	 * */
	public String exportBCGS_1_2() {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);
				String[] lstStaffType = R.getResource("bc.gs.1.2.staff.type").split(";");
				List<GS_1_2_VO> lstData = shopReportMgr.getListVisitNVBH(staffIdRoot, userId, strListShopId, fDate, tDate, staffType);
				if (null != lstData && lstData.size() > 0) {
					int row = 0;
					int col = 0;
					if (StringUtil.isNullOrEmpty(shop.getAddress())) {
						shop.setAddress("");
					}
					String[] menu = R.getResource("bc.gs.1.2.menu.nvbh").split(";");
					if (StaffSpecificType.SUPERVISOR.getValue().equals(staffType)) {
						menu = R.getResource("bc.gs.1.2.menu.usm").split(";");
					}
					String[] info = R.getResource("bc.gs.1.2.info").split(";");
					String outputName = ConstantManager.GS_1_2 + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("bc.gs.1.2.sheet"));
					Map<String, XSSFCellStyle> style = ExcelPOIVTICTUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(13);
					//set static Column width
					ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 50, 200, 200, 100, 200, 100, 200, 100, 200, 100, 150, 150, 200, 200, 200);
					//Size Row
					ExcelPOIVTICTUtils.setRowsHeight(sheetData, 0, 35, 15, 20, 20, 20, 20, 30);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String title = R.getResource("bc.gs.1.2.title");
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 0, row, 16, row, title, style.get(ExcelPOIVTICTUtils.TITLE_G));

					row += 2;
					//info
					col = 5;
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, info[0], style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vung, style.get(ExcelPOIVTICTUtils.NORMAL_CENTER));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, info[1], style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, khuVuc, style.get(ExcelPOIVTICTUtils.NORMAL_CENTER));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, info[2], style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, (staffType != null && staffType >= 0 && staffType < lstStaffType.length) ? lstStaffType[staffType] : "", style.get(ExcelPOIVTICTUtils.NORMAL_CENTER));
					row++;
					col = 5;
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, info[3], style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, npp, style.get(ExcelPOIVTICTUtils.NORMAL_CENTER));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, info[4], style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIVTICTUtils.NORMAL_CENTER));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, info[5], style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, fromDate, style.get(ExcelPOIVTICTUtils.NORMAL_CENTER));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, info[6], style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, toDate, style.get(ExcelPOIVTICTUtils.NORMAL_CENTER));
					row++;
					col = 5;
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, info[7], style.get(ExcelPOIVTICTUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, col++, row, shop.getAddress(), style.get(ExcelPOIVTICTUtils.NORMAL_CENTER));
					row += 2;
					col = 0;
					for (int i = 0; i < menu.length; i++) {
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, menu[i], style.get(ExcelPOIVTICTUtils.HEADER_BLUE_TOP_BOTTOM_MEDIUM));
					}
					row++;
					GS_1_2_VO vo = null;
					XSSFCellStyle left = style.get(ExcelPOIVTICTUtils.ROW_DOTTED_LEFT);
					XSSFCellStyle right = style.get(ExcelPOIVTICTUtils.ROW_DOTTED_RIGHT);
					XSSFCellStyle center = style.get(ExcelPOIVTICTUtils.ROW_DOTTED_CENTER);
					for (int i = 0, size = lstData.size(); i < size; i++) {
						vo = lstData.get(i);
						col = 0;
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, i + 1, center);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getShopLevel2Code(), left);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getShopLevel1Code(), left);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaNPP(), left);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTenNPP(), left);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaGSNPP(), left);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTenGSNPP(), left);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaNVBH(), left);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTenNVBH(), left);
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getNgay(), center);
						if ("1".equals(vo.getDungGioOrTreGio())) {//dung gio
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getThoiGianChamCong() + "(" + vo.getKhoangCachChamCong() + " m)", center);//dung gio
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", center);//tre gio
						} else if ("1".equals(vo.getDungGioOrTreGio())) {//tre gio
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", center);//dung gio
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getThoiGianChamCong() + "(" + vo.getKhoangCachChamCong() + " m)", center);//tre gio
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", center);
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", center);
						}
						if (!StringUtil.isNullOrEmpty(vo.getBatDauLamViec()) || !StringUtil.isNullOrEmpty(vo.getKetThucLamViec())) {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, (vo.getBatDauLamViec() != null ? vo.getBatDauLamViec() : "N/A") + " - " + (vo.getKetThucLamViec() != null ? vo.getKetThucLamViec() : "N/A"), center);
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", center);
						}
						if (!StringUtil.isNullOrEmpty(vo.getBatDauChamCong()) || !StringUtil.isNullOrEmpty(vo.getKetThucChamCong())) {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, (vo.getBatDauChamCong() != null ? vo.getBatDauChamCong() : "N/A") + " - " + (vo.getKetThucChamCong() != null ? vo.getKetThucChamCong() : "N/A"), center);
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", center);
						}
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getKhoangCachQuyDinh() + " m", right);
						row++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);

				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.SuperivseReportAction.exportBCGS_1_2"), createLogErrorStandard(DateUtil.now()));
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao: [6.1.2] Bao cao thoi gian lam viec cua NVBH
	 * 
	 * @author hunglm16
	 * @return JSON
	 * @since 18/10/2015
	 * */
	public String report6D1D2TGLVCNVBH() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			//Xu ly Validate
			String msg = "";
			if (currentUser == null) {
				msg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, "");
			}
			//Kiem tra ATTT
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(msg) && StringUtil.isNullOrEmpty(reportToken)) {
				msg = R.getResource("report.invalid.token");
			}
			//			if (StringUtil.isNullOrEmpty(msg) && cycleId == null) {
			//				msg = R.getResource("common.cycle.undefined");
			//			}
			Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (StringUtil.isNullOrEmpty(msg) && fDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.fromdate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && tDate == null) {
				msg = R.getResource("common.error.undefined", R.getResource("common.date.todate"));
			}
			if (StringUtil.isNullOrEmpty(msg) && DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
				msg = R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate"));
			}
			Long shopId = null;
			if (!StringUtil.isNullOrEmpty(strListShopId)) {
				shopId = Long.valueOf(strListShopId);
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
					return JSON;
				}
			} else {
				shopId = currentUser.getShopRoot().getShopId();
			}
			if (StringUtil.isNullOrEmpty(msg)) {
				msg = checkMaxNumberDayReportShort(fDate, tDate);
			}
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put("errMsg", msg);
				return JSON;
			}
			
			List<Rpt_TGLVCNVBH_VO> lstData = shopReportMgr.reportTGLVCNVBH(shopId, cycleId, fDate, tDate, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());

			if (lstData != null && !lstData.isEmpty()) {
				//Xu ly xuat file excel
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstData", lstData);
				beans.put("ngayBC", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
				beans.put("tuNgay", fromDate);
				beans.put("denNgay", toDate);
				Shop dv = commonMgr.getEntityById(Shop.class, shopId);
				if (dv != null) {
					beans.put("shopReportText", dv.getShopCode() + " - " + dv.getShopName());
				}
				String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportSupperivseTemplatePath();
				String templateFileName = folder + ConstantManager.RPT_6_1_2_THBHCNVBH_TEMPLATE;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = ConstantManager.RPT_6_1_2_THBHCNVBH_FILE_NAME + genExportFileSuffix() + FileExtension.XLSX.getValue();
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(ERROR, false);
				result.put("path", outputPath);
				//Bo sung ATTT
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.report.data.null"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.HOReportAction.reportXNT1Dot1Equip"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Bao cao ket qua di tuyen 1.3
	 * 
	 * @author hoanv25
	 * @since July 07/2015
	 * @return
	 * 
	 * @modify hunglm16
	 * @since 26/09/2015
	 * @description Quy hoach code
	 */
	public String exportBCKQT13() {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);

				Cycle cycle = cycleMgr.getCycleById(cycleId);
				List<GS_4_2_VO> lstData = shopReportMgr.getListVisit(staffIdRoot, userId, strListShopId, fDate, tDate);

				String outputName = ConstantManager.EXPORT_1_3_KET_QUA_DI_TUYEN + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
				String exportFileName = Configuration.getStoreRealPath() + outputName;
				//Init XSSF workboook
				workbook = new SXSSFWorkbook(-1);
				workbook.setCompressTempFiles(true);
				//Tao sheet
				SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("baocao.1.3.kqdt.shet.name"));
				Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
				//Set Getting Defaul
				sheetData.setDefaultRowHeight((short) (15 * 20));
				sheetData.setDefaultColumnWidth(23);
				//set static Column width
				ExcelPOIProcessUtils.setColumnsWidth(sheetData, 0, 50, 150, 150, 150, 150, 150, 150, 150, 150, 150, 180, 180, 180, 180, 180, 180, 250, 250);
				//Size Row
				ExcelPOIProcessUtils.setRowsHeight(sheetData, 0, 35, 15, 15, 15, 15, 15);
				sheetData.setDisplayGridlines(false);
				//Tittle
				String titleTemp = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.title.name");
				ExcelPOIProcessUtils.addCellsAndMerged(sheetData, 0, 0, 17, 1, titleTemp, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
				//info					
				ExcelPOIProcessUtils.addCell(sheetData, 2, 3, vung, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 3, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 4, npp, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 4, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 4, fromDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 4, toDate, style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 5, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL_CENTER));
				//header
				ExcelPOIProcessUtils.addCell(sheetData, 1, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 3, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));

				ExcelPOIProcessUtils.addCell(sheetData, 0, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.stt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 1, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 2, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 3, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 4, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tennpp"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 5, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.mausm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 6, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenusm"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 7, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.maroute"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 8, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tenroute"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 9, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.manvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 10, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.nvbh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 11, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.ngaybh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));

				ExcelPOIProcessUtils.addCell(sheetData, 12, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.skhpgttn"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 13, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.skhdgtcdh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 14, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.skhdgtkcdh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 15, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.skhdgtkcdht"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 16, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.skhdgtdc"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 17, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.skhcgt"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 18, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.skhbnkhcdh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				ExcelPOIProcessUtils.addCell(sheetData, 19, 7, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.skhbnkhkcdh"), style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
				int d = 8;
				if (lstData.size() > 0) {
					GS_4_2_VO salesVO = null;
					for (int i = 0, size = lstData.size(); i < size; i++) {
						salesVO = lstData.get(i);
						ExcelPOIProcessUtils.setRowsHeight(sheetData, d, 15);
						ExcelPOIProcessUtils.addCell(sheetData, 0, d, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						ExcelPOIProcessUtils.addCell(sheetData, 1, d, salesVO.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 2, d, salesVO.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 3, d, salesVO.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 4, d, salesVO.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 5, d, salesVO.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 6, d, salesVO.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 7, d, salesVO.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 8, d, salesVO.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 9, d, salesVO.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 10, d, salesVO.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 11, d, salesVO.getOderDateC(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIProcessUtils.addCell(sheetData, 12, d, salesVO.getHaveVisitCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));

						ExcelPOIProcessUtils.addCell(sheetData, 13, d, salesVO.getHaveOrderCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 14, d, salesVO.getNoOrderCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 15, d, salesVO.getGiveOrderCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 16, d, salesVO.getCloseCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 17, d, salesVO.getNoVisitCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 18, d, salesVO.getOverCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						ExcelPOIProcessUtils.addCell(sheetData, 19, d, salesVO.getOverOrderCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_RIGHT));
						d++;
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}

	/**
	 * Bao cao mo moi khach hang GS 1.4
	 * 
	 * @author hoanv25
	 * @since July 08/2015
	 * @return
	 */
	public String exportVT1_4_BCMMKH() {
		errMsg = "";
		FileOutputStream out = null;
		SXSSFWorkbook workbook = null;
		try {
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				Long shopId = currentUser.getShopRoot().getShopId();
				if (!StringUtil.isNullOrEmpty(strListShopId)) {
					shopId = Long.valueOf(strListShopId);
					if (super.getMapShopChild().get(shopId) == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
						return JSON;
					}
				}
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long userId = currentUser.getRoleToken().getRoleId();
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
				Shop shop = shopMgr.getShopById(shopId);
				this.getInfoParentShop(shop);
				if (!StringUtil.isNullOrEmpty(strListNvbhId)) {
					String[] arrCtt = strListNvbhId.split(",");
					int len = arrCtt.length;
					for (int i = 0; i < len; i++) {
						String value = arrCtt[i];
						Long gsIdTmp = Long.valueOf(value);
					}
				}
				Cycle cycle = cycleMgr.getCycleById(cycleId);
				String[] lstStaffType = R.getResource("bc.gs.1.2.staff.type").split(";");
				check = 0;
				if (!StringUtil.isNullOrEmpty(strListNvbhId) || !StringUtil.isNullOrEmpty(strListNvbhId)) {
					check = 1;
				}
				List<GS_1_4_VO> lstData = shopReportMgr.getNewOpendCustomerByNVBH(staffIdRoot, userId, strListShopId, fDate, tDate, check, strListGsId, strListNvbhId);
				if (null != lstData && lstData.size() > 0) {
					int row = 0;
					int col = 0;
					//************************************************************************
					if (StringUtil.isNullOrEmpty(shop.getAddress())) {
						shop.setAddress("");
					}
					String[] menu = R.getResource("bc.gs.1.4.menu").split(";");
					String[] info = R.getResource("bc.gs.1.4.info").split(";");
					String outputName = ConstantManager.GS_1_4 + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + ".xlsx";
					String exportFileName = Configuration.getStoreRealPath() + outputName;
					workbook = new SXSSFWorkbook(-1);
					workbook.setCompressTempFiles(true);
					//Tao sheet
					SXSSFSheet sheetData = (SXSSFSheet) workbook.createSheet(R.getResource("bc.gs.1.4.sheet"));
					Map<String, XSSFCellStyle> style = ExcelPOIProcessUtils.createStyles(workbook);
					//Set Getting Defaul
					sheetData.setDefaultRowHeight((short) (15 * 20));
					sheetData.setDefaultColumnWidth(13);
					//set static Column width
					ExcelPOIVTICTUtils.setColumnsWidth(sheetData, 0, 45, 150, 150, 150, 200, 100, 200, 100, 200, 100, 150, 150, 200, 200, 200, 200, 200, 200, 200, 200);
					//Size Row
					ExcelPOIVTICTUtils.setRowsHeight(sheetData, 0, 35, 15, 15, 15, 15, 15, 15);
					sheetData.setDisplayGridlines(false);
					//Tittle
					String title = R.getResource("bc.gs.1.4.title");
					ExcelPOIVTICTUtils.addCellsAndMerged(sheetData, 0, row, 16, row, title, style.get(ExcelPOIProcessUtils.TITLE_NUTI_BLACK));
					//info					
					ExcelPOIVTICTUtils.addCell(sheetData, 1, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.vung"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, 2, 4, vung, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, 3, 4, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.khuvuc"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, 4, 4, khuVuc, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, 5, 4, info[2], style.get(ExcelPOIProcessUtils.HEADER_GREY_NONE_MEDIUM));
					if (strListNVBH != null && strListNVBH.length() > 0 && strListNVBH != "") {
						ExcelPOIVTICTUtils.addCell(sheetData, 6, 4, strListNVBH, style.get(ExcelPOIProcessUtils.NORMAL));
					}
					ExcelPOIVTICTUtils.addCell(sheetData, 1, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.npp"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, 2, 5, npp, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, 3, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.chuky"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, 4, 5, cycle != null ? cycle.getCycleName() : "", style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, 5, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.tungay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, 6, 5, fromDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, 7, 5, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.denngay"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, 8, 5, toDate, style.get(ExcelPOIProcessUtils.NORMAL));
					ExcelPOIVTICTUtils.addCell(sheetData, 1, 6, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "baocao.1.3.kqdt.header.diachi"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
					ExcelPOIVTICTUtils.addCell(sheetData, 2, 6, shop.getAddress(), style.get(ExcelPOIProcessUtils.NORMAL));

					row = 8;
					col = 0;
					for (int i = 0; i < menu.length; i++) {
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, menu[i], style.get(ExcelPOIProcessUtils.HEADER_BLUE_NONE_MEDIUM));
					}
					row++;
					GS_1_4_VO vo = null;
					int rowStart = row;
					int group6 = 0, group5 = 0, group4 = 0, group3 = 0, group2 = 0, group1 = 0;
					List<Integer> start6 = new ArrayList<Integer>();
					List<Integer> end6 = new ArrayList<Integer>();
					List<Integer> start5 = new ArrayList<Integer>();
					List<Integer> end5 = new ArrayList<Integer>();
					List<Integer> start4 = new ArrayList<Integer>();
					List<Integer> end4 = new ArrayList<Integer>();
					List<Integer> start3 = new ArrayList<Integer>();
					List<Integer> end3 = new ArrayList<Integer>();
					List<Integer> start2 = new ArrayList<Integer>();
					List<Integer> end2 = new ArrayList<Integer>();
					List<Integer> start1 = new ArrayList<Integer>();
					List<Integer> end1 = new ArrayList<Integer>();
					for (int i = 0, size = lstData.size(); i < size; i++) {
						vo = lstData.get(i);
						col = 0;
						if (StringUtil.isNullOrEmpty(vo.getMaVung())) {
							start6.add(group6 + rowStart);
							end6.add(i + rowStart - 1);
							group6 = i + 1;
							group5++;
							group4++;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getMaMien())) {
							start5.add(group5 + rowStart);
							end5.add(i + rowStart - 1);
							group5 = i + 1;
							group4++;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getMaNPP())) {
							start4.add(group4 + rowStart);
							end4.add(i + rowStart - 1);
							group4 = i + 1;
							group3++;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getMaGSNPP())) {
							start3.add(group3 + rowStart);
							end3.add(i + rowStart - 1);
							group3 = i + 1;
							group2++;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getMaNVBH())) {
							start2.add(group2 + rowStart);
							end2.add(i + rowStart - 1);
							group2 = i + 1;
							group1++;
						} else if (StringUtil.isNullOrEmpty(vo.getMaTuyen())) {
							start1.add(group1 + rowStart);
							end1.add(i + rowStart - 1);
							group1 = i + 1;
						}
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, i + 1, style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						if (vo.getMaVung() != null) {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaVung(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, R.getResource("baocao.1.4.bcmmkh.total"), style.get(ExcelPOIProcessUtils.BOLD_LEFT));
						}
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaMien(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTenNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						if (vo.getMaGSNPP() != null && vo.getMaGSNPP().equals("xxx196")) {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
						if (vo.getTenGSNPP() != null && vo.getTenGSNPP().equals("xxx674")) {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTenGSNPP(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
						if (vo.getMaNVBH() != null && vo.getMaNVBH().equals("nuti")) {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, "", style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						}
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTenNVBH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTenTuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getNgay(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER));
						if (vo.getMaKH() != null) {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getMaKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						} else {
							ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTotalCustomer(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_CENTER_BOLD));
						}
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTenKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getLoaiKH(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getDiaChi(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getXaPhuong(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getQuanHuyen(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getTinhTp(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						ExcelPOIVTICTUtils.addCell(sheetData, col++, row, vo.getPhoneNumber(), style.get(ExcelPOIProcessUtils.ROW_DOTTED_LEFT));
						row++;
					}

					for (int i = 0; i < start1.size(); i++) {
						sheetData.groupRow(start1.get(i), end1.get(i));
					}
					for (int i = 0; i < start2.size(); i++) {
						sheetData.groupRow(start2.get(i), end2.get(i));
					}
					for (int i = 0; i < start3.size(); i++) {
						sheetData.groupRow(start3.get(i), end3.get(i));
					}
					for (int i = 0; i < start4.size(); i++) {
						sheetData.groupRow(start4.get(i), end4.get(i));
					}
					for (int i = 0; i < start5.size(); i++) {
						sheetData.groupRow(start5.get(i), end5.get(i));
					}
					for (int i = 0; i < start6.size(); i++) {
						sheetData.groupRow(start6.get(i), end6.get(i));
					}
					out = new FileOutputStream(exportFileName);
					workbook.write(out);
					out.close();
					workbook.dispose();
					String outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(REPORT_PATH, outputPath);
					//result.put(LIST, outputPath);
					result.put(ERROR, false);
					result.put("hasData", true);
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
					return JSON;
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
		} finally {
			if (workbook != null) {
				workbook.dispose();
			}
		}
		return JSON;
	}
	
	/**
	 * Bao cao chi tiet khuyen mai
	 * @author duongdt3
	 * @return String
	 * @since 30/12/2015
	 */
	public String exportBCTGTM() {
		Date startDate = DateUtil.now();
		InputStream inputStream = null;
		OutputStream os = null;
		String outputPath = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (currentUser != null && currentUser.getShopRoot() != null && currentUser.getStaffRoot() != null) {
				Shop shop = null;
				if (shopId != null) {
					shop = shopMgr.getShopById(shopId);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
					return JSON;
				}
				if (shop == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.name.lable")));
					return JSON;
				}
				if (super.getMapShopChild().get(shopId) == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined"));
					return JSON;
				}
				
				if (minutes == null || minutes <= ZEZO_INT_G) {
					result.put(ERROR, true);
					result.put(ERR_MSG, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.minute.too.small.undefined"));
					return JSON;
				}
				Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (fDate == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.fromdate")));
					return JSON;
				}
				Date tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				if (tDate == null) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.error.undefined", R.getResource("common.date.todate")));
					return JSON;
				}
				if (DateUtil.compareDateWithoutTime(fDate, tDate) > ZEZO_INT_G) {
					result.put(ERROR, true);
					result.put(ERR_MSG, R.getResource("common.compare.error.less.or.equal.tow.param", R.getResource("common.date.fromdate"), R.getResource("common.date.todate")));
					return JSON;
				}
				String msg = checkMaxNumberDayReportMedium(fDate, tDate);
				if (!StringUtil.isNullOrEmpty(msg)) {
					result.put(ERROR, true);
					result.put(ERR_MSG, msg);
					return JSON;
				}
				
				Long staffIdRoot = currentUser.getStaffRoot().getStaffId();
				Long roleId = currentUser.getRoleToken().getRoleId();
				List<RptBCTGTMVO> lstData = shopReportMgr.getListRptBCTGTM(staffIdRoot, roleId, shopId, fDate, tDate, minutes);
				if (lstData != null && !lstData.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					beans.put("lstData", lstData);
					beans.put("shopSelect", shop != null ? shop.getShopCode() + " - " + shop.getShopName() : "");
					beans.put("fromDate", fromDate);
					beans.put("toDate", toDate);
					beans.put("dateNow", DateUtil.toDateString(commonMgr.getSysDate(), DateUtil.DATETIME_FORMAT_STR));
					beans.put("minute", minutes);
					//Xu ly xuat file excel
					String fileTemplate = ConstantManager.EXPORT_BC_TGTM_TEMPLATE;
					String fileName = ConstantManager.EXPORT_BC_TGTM_FILENAME;
					String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getReportHoTemplatePath();
					String templateFileName = folder + fileTemplate;
					templateFileName = templateFileName.replace('/', File.separatorChar);
					
					String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
					String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
					
					inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
					XLSTransformer transformer = new XLSTransformer();
					Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
					os = new BufferedOutputStream(new FileOutputStream(exportFileName));
					resultWorkbook.write(os);
					os.flush();
					outputPath = Configuration.getExportExcelPath() + outputName;
					result.put(ERROR, false);
					result.put(REPORT_PATH, outputPath);
					//Bo sung ATTT
					MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
				} else {
					result.put(ERROR, false);
					result.put("hasData", false);
				}
				lstData = null;
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.report.SuperivseReportAction.exportBCTGTM()", createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}
	
	/**
	 * Khai bao GETTER/SETTER
	 * 
	 * @return
	 */

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Integer getStaffType() {
		return staffType;
	}

	public void setStaffType(Integer staffType) {
		this.staffType = staffType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNpp() {
		return npp;
	}

	public void setNpp(String npp) {
		this.npp = npp;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getKhuVuc() {
		return khuVuc;
	}

	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	public List<CycleVO> getLstCycle() {
		return lstCycle;
	}

	public void setLstCycle(List<CycleVO> lstCycle) {
		this.lstCycle = lstCycle;
	}

	public List<ProductInfo> getLstCategoryType() {
		return lstCategoryType;
	}

	public void setLstCategoryType(List<ProductInfo> lstCategoryType) {
		this.lstCategoryType = lstCategoryType;
	}

	public List<ProductInfo> getLstSubCategoryType() {
		return lstSubCategoryType;
	}

	public void setLstSubCategoryType(List<ProductInfo> lstSubCategoryType) {
		this.lstSubCategoryType = lstSubCategoryType;
	}

	public List<ProductInfo> getLstSubCategoryCat() {
		return lstSubCategoryCat;
	}

	public void setLstSubCategoryCat(List<ProductInfo> lstSubCategoryCat) {
		this.lstSubCategoryCat = lstSubCategoryCat;
	}

	public int getStartNpp() {
		return startNpp;
	}

	public void setStartNpp(int startNpp) {
		this.startNpp = startNpp;
	}

	public int getEndNpp() {
		return endNpp;
	}

	public void setEndNpp(int endNpp) {
		this.endNpp = endNpp;
	}

	public int getStartVung() {
		return startVung;
	}

	public void setStartVung(int startVung) {
		this.startVung = startVung;
	}

	public int getEndVung() {
		return endVung;
	}

	public void setEndVung(int endVung) {
		this.endVung = endVung;
	}

	public int getStartMien() {
		return startMien;
	}

	public void setStartMien(int startMien) {
		this.startMien = startMien;
	}

	public int getEndMien() {
		return endMien;
	}

	public void setEndMien(int endMien) {
		this.endMien = endMien;
	}

	public int getCountNpp() {
		return countNpp;
	}

	public void setCountNpp(int countNpp) {
		this.countNpp = countNpp;
	}

	public int getCountVung() {
		return countVung;
	}

	public void setCountVung(int countVung) {
		this.countVung = countVung;
	}

	public int getCountMien() {
		return countMien;
	}

	public void setCountMien(int countMien) {
		this.countMien = countMien;
	}

	public int getCountOther() {
		return countOther;
	}

	public void setCountOther(int countOther) {
		this.countOther = countOther;
	}

	public int getStartOther() {
		return startOther;
	}

	public void setStartOther(int startOther) {
		this.startOther = startOther;
	}

	public int getEndOther() {
		return endOther;
	}

	public void setEndOther(int endOther) {
		this.endOther = endOther;
	}

	public int getStart4() {
		return start4;
	}

	public void setStart4(int start4) {
		this.start4 = start4;
	}

	public int getStart5() {
		return start5;
	}

	public void setStart5(int start5) {
		this.start5 = start5;
	}

	public int getStart6() {
		return start6;
	}

	public void setStart6(int start6) {
		this.start6 = start6;
	}

	public int getStart7() {
		return start7;
	}

	public void setStart7(int start7) {
		this.start7 = start7;
	}

	public int getStart8() {
		return start8;
	}

	public void setStart8(int start8) {
		this.start8 = start8;
	}

	public int getEnd4() {
		return end4;
	}

	public void setEnd4(int end4) {
		this.end4 = end4;
	}

	public int getEnd5() {
		return end5;
	}

	public void setEnd5(int end5) {
		this.end5 = end5;
	}

	public int getEnd6() {
		return end6;
	}

	public void setEnd6(int end6) {
		this.end6 = end6;
	}

	public int getEnd7() {
		return end7;
	}

	public void setEnd7(int end7) {
		this.end7 = end7;
	}

	public int getEnd8() {
		return end8;
	}

	public void setEnd8(int end8) {
		this.end8 = end8;
	}

	public int getCount4() {
		return count4;
	}

	public void setCount4(int count4) {
		this.count4 = count4;
	}

	public int getCount5() {
		return count5;
	}

	public void setCount5(int count5) {
		this.count5 = count5;
	}

	public int getCount6() {
		return count6;
	}

	public void setCount6(int count6) {
		this.count6 = count6;
	}

	public int getCount7() {
		return count7;
	}

	public void setCount7(int count7) {
		this.count7 = count7;
	}

	public int getCount8() {
		return count8;
	}

	public void setCount8(int count8) {
		this.count8 = count8;
	}

	public String getFromHour() {
		return fromHour;
	}

	public void setFromHour(String fromHour) {
		this.fromHour = fromHour;
	}

	public String getFromMinute() {
		return fromMinute;
	}

	public void setFromMinute(String fromMinute) {
		this.fromMinute = fromMinute;
	}

	public String getToHour() {
		return toHour;
	}

	public void setToHour(String toHour) {
		this.toHour = toHour;
	}

	public String getToMinute() {
		return toMinute;
	}

	public void setToMinute(String toMinute) {
		this.toMinute = toMinute;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getLstCat() {
		return lstCat;
	}

	public void setLstCat(String lstCat) {
		this.lstCat = lstCat;
	}

	public String getLstSubCat() {
		return lstSubCat;
	}

	public void setLstSubCat(String lstSubCat) {
		this.lstSubCat = lstSubCat;
	}

	public String getStrListNVBH() {
		return strListNVBH;
	}

	public void setStrListNVBH(String strListNVBH) {
		this.strListNVBH = strListNVBH;
	}

	public String getLstProductName() {
		return lstProductName;
	}

	public void setLstProductName(String lstProductName) {
		this.lstProductName = lstProductName;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public String getLstProduct() {
		return lstProduct;
	}

	public void setLstProduct(String lstProduct) {
		this.lstProduct = lstProduct;
	}

	public ProductInfoMgr getProductInfoMgr() {
		return productInfoMgr;
	}

	public void setProductInfoMgr(ProductInfoMgr productInfoMgr) {
		this.productInfoMgr = productInfoMgr;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffSaleCode() {
		return staffSaleCode;
	}

	public void setStaffSaleCode(String staffSaleCode) {
		this.staffSaleCode = staffSaleCode;
	}

	public String getStaffOwnerCode() {
		return staffOwnerCode;
	}

	public void setStaffOwnerCode(String staffOwnerCode) {
		this.staffOwnerCode = staffOwnerCode;
	}

	public Integer getMinutes() {
		return minutes;
	}

	public void setMinutes(Integer minutes) {
		this.minutes = minutes;
	}

	public String getLstNVGSCode() {
		return lstNVGSCode;
	}

	public void setLstNVGSCode(String lstNVGSCode) {
		this.lstNVGSCode = lstNVGSCode;
	}

	public String getLstNVBHCode() {
		return lstNVBHCode;
	}

	public void setLstNVBHCode(String lstNVBHCode) {
		this.lstNVBHCode = lstNVBHCode;
	}

	public HashMap<String, Object> getParametersReport() {
		return parametersReport;
	}

	public void setParametersReport(HashMap<String, Object> parametersReport) {
		this.parametersReport = parametersReport;
	}

	public Integer getCheck() {
		return check;
	}

	public void setCheck(Integer check) {
		this.check = check;
	}

	public String getStrListNvbhId() {
		return strListNvbhId;
	}

	public void setStrListNvbhId(String strListNvbhId) {
		this.strListNvbhId = strListNvbhId;
	}

	public String getStrListGsId() {
		return strListGsId;
	}

	public void setStrListGsId(String strListGsId) {
		this.strListGsId = strListGsId;
	}
}
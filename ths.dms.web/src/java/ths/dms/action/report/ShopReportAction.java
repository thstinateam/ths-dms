/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.report;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;



/**
 * Action bao cao danh muc cho HO
 * 
 * @author hunglm16
 * @since September 18, 2015
 */
public class ShopReportAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	@Override
	public void prepare() throws Exception {
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		try {
			
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.report.ShopReportAction.execute"), createLogErrorStandard(DateUtil.now()));
		}	
		return SUCCESS;
	}
}
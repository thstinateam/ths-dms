/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.report;

import ths.dms.web.action.general.AbstractAction;

/**
 * Bao cao
 * @author hunglm16
 * @since 26/09/2015
 */
public class ReportAction extends AbstractAction {
	
	private static final long serialVersionUID = -3470234290573825410L;

	
	@Override
	public void prepare() throws Exception {
		super.prepare();
	}

	@Override
	public String execute() throws Exception {
		return SUCCESS;
	}

}

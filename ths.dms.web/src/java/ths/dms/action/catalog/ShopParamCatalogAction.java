/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.ShopParamMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopParamVO;
import ths.dms.core.entities.vo.ShopParamVODayEnd;
import ths.dms.core.entities.vo.ShopParamVODayStart;
import ths.dms.core.entities.vo.ShopParamVOProduct;
import ths.dms.core.entities.vo.ShopParamVOSave;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Mo ta class ShopParamCatalogAction.java
 * @author vuongmq
 * @since Nov 27, 2015
 */
public class ShopParamCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	private static final String DEFAULT_EXEC_TIME = "00:00";
	private Long shopId;
	private String shopCode;
	private Shop shop; 
	private String closeDayHM;
	private Integer beforeMinute;
	private ShopParamVOSave shopParamVOSave;
	
	private List<ShopParamVOProduct> lstProductVO;
	
	private ShopParamMgr shopParamMgr;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		shopParamMgr = (ShopParamMgr) context.getBean("shopParamMgr");
	}

	@Override
	public String execute() {
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				shopId = shop.getId();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ShopParamCatalogAction.execute()"), createLogErrorStandard(startLogDate));
		}
		return SUCCESS;
	}

	/**
	 * Xu ly getShopParamConfigValue
	 * @author vuongmq
	 * @return String
	 * @since Nov 30, 2015
	 */
	public String getShopParamConfigValue() {
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getUserId() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			shop = shopMgr.getShopById(shopId);
			if (shopId == null || shop == null || super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
				return JSON;
			}
			ApParam apParamMinuteBefore = apParamMgr.getApParamByCodeEx(Constant.SHOP_LOCK_EXEC_TIME_SYSDATE_BEFORE_MINUTE, ActiveType.RUNNING);
			if (apParamMinuteBefore != null && !StringUtil.isNullOrEmpty(apParamMinuteBefore.getValue()) && ValidateUtil.validateNumber(apParamMinuteBefore.getValue())) {
				beforeMinute = Integer.parseInt(apParamMinuteBefore.getValue());
			} else {
				beforeMinute = SIZE_ZERO;
			}
			ApParam apParamHM = apParamMgr.getApParamByCodeEx(Constant.SHOP_LOCK_EXEC_TIME, ActiveType.RUNNING);
			if (apParamHM != null && !StringUtil.isNullOrEmpty(apParamHM.getValue())) {
				closeDayHM = apParamHM.getValue();
			} else {
				closeDayHM = DEFAULT_EXEC_TIME;
			}
			result.put("beforeMinute", beforeMinute);
			result.put("closeDayHM", closeDayHM);
			
			shopParamVOSave = new ShopParamVOSave();
			shopParamVOSave.setShopDistanceOrder(shop.getDistanceOrder());
			String paramTmp[] = {Constant.DT_START, Constant.DT_MIDDLE, Constant.DT_END,
								 Constant.CC_DISTANCE, Constant.CC_START, Constant.CC_END};
			String code = "";
			ShopParam shopParam = null;
			for (int i = 0, sz = paramTmp.length; i < sz; i++) {
				code = paramTmp[i];
				shopParam = shopParamMgr.getShopParamByShopIdCode(shopId, code, ActiveType.RUNNING);
				if (shopParam != null) {
					if (Constant.DT_START.equals(code)) {
						shopParamVOSave.setShopDTStartId(shopParam.getId());
						shopParamVOSave.setShopDTStart(!StringUtil.isNullOrEmpty(shopParam.getValue()) ? shopParam.getValue() : "");
					} else if (Constant.DT_MIDDLE.equals(code)) {
						shopParamVOSave.setShopDTMiddleId(shopParam.getId());
						shopParamVOSave.setShopDTMiddle(!StringUtil.isNullOrEmpty(shopParam.getValue()) ? shopParam.getValue() : "");
					} else if (Constant.DT_END.equals(code)) {
						shopParamVOSave.setShopDTEndId(shopParam.getId());
						shopParamVOSave.setShopDTEnd(!StringUtil.isNullOrEmpty(shopParam.getValue()) ? shopParam.getValue() : "");
					} else if (Constant.CC_DISTANCE.equals(code)) {
						shopParamVOSave.setShopCCDistanceId(shopParam.getId());
						shopParamVOSave.setShopCCDistance(!StringUtil.isNullOrEmpty(shopParam.getValue()) ? shopParam.getValue() : "");
					} else if (Constant.CC_START.equals(code)) {
						shopParamVOSave.setShopCCStartId(shopParam.getId());
						shopParamVOSave.setShopCCStart(!StringUtil.isNullOrEmpty(shopParam.getValue()) ? shopParam.getValue() : "");
					} else if (Constant.CC_END.equals(code)) {
						shopParamVOSave.setShopCCEndId(shopParam.getId());
						shopParamVOSave.setShopCCEnd(!StringUtil.isNullOrEmpty(shopParam.getValue()) ? shopParam.getValue() : "");
					}
				}
			}
			result.put("shopParamVOSave", shopParamVOSave);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.getShopParamConfigValue.getShopParamConfigValue()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xu ly saveInfo
	 * @author vuongmq
	 * @return String
	 * @since Nov 27, 2015
	 */
	public String saveInfo() {
		resetToken(result);
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getUserId() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			shop = shopMgr.getShopById(shopId);
			if (shopId == null || shop == null || super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
				return JSON;
			}
			if (shopParamVOSave != null) {
				shopParamVOSave.setLstProductShopMapVO(lstProductVO);
			}
			String msg = this.validateShopParamVOSave(shopParamVOSave);
			if (!StringUtil.isNullOrEmpty(msg)) {
				result.put(ERROR, true);
				result.put(ERR_MSG, msg);
				return JSON;
			}
			shopParamVOSave.setShop(shop);
			shopParamMgr.saveInfo(shopParamVOSave, getLogInfoVO());
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ShopParamCatalogAction.saveInfo()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xu ly validateShopParamVOSave
	 * @author vuongmq
	 * @return String
	 * @since Nov 30, 2015
	*/
	private String validateShopParamVOSave(ShopParamVOSave params) {
		String msg = "";
		if (params == null) {
			msg = R.getResource("shop_param_is_not_param_config");
		}
		// validate value
		if (StringUtil.isNullOrEmpty(msg) && params.getShopDistanceOrder() != null) {
			msg = ValidateUtil.validateField(params.getShopDistanceOrder().toString(), "shop_param_distance_order", 10, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_INTEGER);
		}
		if (StringUtil.isNullOrEmpty(msg)) {
			msg = ValidateUtil.validateField(params.getShopDTStart(), "shop_param_dt_start", 5, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(params.getShopDTStart()) && !ValidateUtil.isValidDate(params.getShopDTStart(), DateUtil.TIME_FORMAT_STR)) {
				msg = R.getResource("shop_param_is_not_hh_mm", R.getResource("shop_param_dt_start"));
			}
		}
		if (StringUtil.isNullOrEmpty(msg)) {
			msg = ValidateUtil.validateField(params.getShopDTMiddle(), "shop_param_dt_middle", 5, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(params.getShopDTMiddle()) && !ValidateUtil.isValidDate(params.getShopDTMiddle(), DateUtil.TIME_FORMAT_STR)) {
				msg = R.getResource("shop_param_is_not_hh_mm", R.getResource("shop_param_dt_middle"));
			}
		}
		if (StringUtil.isNullOrEmpty(msg)) {
			msg = ValidateUtil.validateField(params.getShopDTEnd(), "shop_param_dt_end", 5, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(params.getShopDTEnd()) && !ValidateUtil.isValidDate(params.getShopDTEnd(), DateUtil.TIME_FORMAT_STR)) {
				msg = R.getResource("shop_param_is_not_hh_mm", R.getResource("shop_param_dt_end"));
			}
		}
		if (StringUtil.isNullOrEmpty(msg)) {
			msg = ValidateUtil.validateField(params.getShopCCDistance(), "shop_param_cc_distance", 10, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_INTEGER);
		}
		if (StringUtil.isNullOrEmpty(msg)) {
			msg = ValidateUtil.validateField(params.getShopCCStart(), "shop_param_cc_start", 5, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(params.getShopCCStart()) && !ValidateUtil.isValidDate(params.getShopCCStart(), DateUtil.TIME_FORMAT_STR)) {
				msg = R.getResource("shop_param_is_not_hh_mm", R.getResource("shop_param_cc_start"));
			}
		}
		if (StringUtil.isNullOrEmpty(msg)) {
			msg = ValidateUtil.validateField(params.getShopCCEnd(), "shop_param_cc_end", 5, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(params.getShopCCEnd()) && !ValidateUtil.isValidDate(params.getShopCCEnd(), DateUtil.TIME_FORMAT_STR)) {
				msg = R.getResource("shop_param_is_not_hh_mm", R.getResource("shop_param_cc_end"));
			}
		}
		// validate doi thoi gian chot ngay
		if (StringUtil.isNullOrEmpty(msg)) {
			if (params.getLstDayStart() != null && params.getLstDayStart().size() > 0) {
				boolean flag = false;
				for (int i = 0, sz = params.getLstDayStart().size(); i < sz; i++) {
					ShopParamVODayStart shopParamVODayStart = params.getLstDayStart().get(i);
					if (shopParamVODayStart != null && !StringUtil.isNullOrEmpty(shopParamVODayStart.getValueDateStr()) && !StringUtil.isNullOrEmpty(shopParamVODayStart.getValueTimeStr())) {
						String valueDate = shopParamVODayStart.getValueDateStr() + " " + shopParamVODayStart.getValueTimeStr();
						if (!ValidateUtil.isValidDate(valueDate, DateUtil.DATETIME_FORMAT_STR)) {
							flag = true;
							break;
						}
					} else {
						flag = true;
						break;
					}
				}
				if (flag) {
					msg = R.getResource("shop_param_cn_start_date_add");
				}
			}
		}
		// validate tam dung thoi gian chot ngay tu dong
		if (StringUtil.isNullOrEmpty(msg)) {
			if (params.getLstDayEnd() != null && params.getLstDayEnd().size() > 0) {
				String errEnd = "";
				for (int i = 0, sz = params.getLstDayEnd().size(); i < sz; i++) {
					ShopParamVODayEnd shopParamVODayEnd = params.getLstDayEnd().get(i);
					if (shopParamVODayEnd != null && !StringUtil.isNullOrEmpty(shopParamVODayEnd.getFromDateStr()) && !StringUtil.isNullOrEmpty(shopParamVODayEnd.getToDateStr())) {
						if (!ValidateUtil.isValidDate(shopParamVODayEnd.getFromDateStr(), DateUtil.DATE_FORMAT_DDMMYYYY) || !ValidateUtil.isValidDate(shopParamVODayEnd.getToDateStr(), DateUtil.DATE_FORMAT_DDMMYYYY)) {
							errEnd = R.getResource("shop_param_cn_end_date_err_format", (i + 1));
							break;
						}
					} else {
						errEnd = R.getResource("shop_param_cn_end_date_err_format", (i + 1));
						break;
					}
				}
				if (!StringUtil.isNullOrEmpty(errEnd)) {
					msg = errEnd;
				}
			}
		}
		if (StringUtil.isNullOrEmpty(msg)) {
			if (params.getLstProductShopMapVO() != null && params.getLstProductShopMapVO().size() > 0) {
				String errStock = "";
				int MAX_LENTH_3 = 3;
				for (int i = 0, sz = params.getLstProductShopMapVO().size(); i < sz; i++) {
					ShopParamVOProduct shopParamVOProduct = params.getLstProductShopMapVO().get(i);
					if (shopParamVOProduct != null && shopParamVOProduct.getStockDay() != null) {
						if (!ValidateUtil.validateNumber(shopParamVOProduct.getStockDay().toString())) {
							errStock = R.getResource("shop_param_stock_day_err_format_number", (i + 1));
							break;
						}
						if (shopParamVOProduct.getStockDay().intValue() < 0) {
							errStock = R.getResource("shop_param_stock_day_err_number", (i + 1));
							break;
						}
						if (shopParamVOProduct.getStockDay().toString().length() > MAX_LENTH_3) {
							errStock = R.getResource("shop_param_stock_day_err_max_length", (i + 1), MAX_LENTH_3);
							break;
						}
					}
				}
				if (!StringUtil.isNullOrEmpty(errStock)) {
					msg = errStock;
				}
			}
		}
		return msg;
	}

	/**
	 * Xu ly getShopLockExecTime
	 * @author vuongmq
	 * @return String
	 * @since Nov 30, 2015
	 */
	public String getShopLockExecTime() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			shop = shopMgr.getShopById(shopId);
			if (shopId == null || shop == null || super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
				return JSON;
			}
			BasicFilter<ShopParamVO> filter = new BasicFilter<>();
			KPaging<ShopParamVO> kPaging = new KPaging<ShopParamVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			filter.setShopId(shopId);
			filter.setCode(Constant.SHOP_LOCK_EXEC_TIME);
			ObjectVO<ShopParamVO> vo = shopParamMgr.getListObjectShopParamVOViewTimeByFilter(filter);
			if (vo != null && vo.getLstObject() != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<ShopParamVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ShopParamCatalogAction.getShopLockExecTime()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Xu ly getShopLockAbortDuration
	 * @author vuongmq
	 * @return String
	 * @since Nov 30, 2015
	 */
	public String getShopLockAbortDuration() {
		Date startLogDate = DateUtil.now();
		result.put("page", page);
		result.put("max", max);
		try {
			shop = shopMgr.getShopById(shopId);
			if (shopId == null || shop == null || super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
				return JSON;
			}
			BasicFilter<ShopParamVO> filter = new BasicFilter<>();
			KPaging<ShopParamVO> kPaging = new KPaging<ShopParamVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);
			filter.setShopId(shopId);
			filter.setCode(Constant.SHOP_LOCK_ABORT_DURATION);
			ObjectVO<ShopParamVO> vo = shopParamMgr.getListObjectShopParamVOByFilter(filter);
			if (vo != null && vo.getLstObject() != null) {
				this.setValueFromDateToDateStr(vo.getLstObject());
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<ShopParamVO>());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ShopParamCatalogAction.getShopLockAbortDuration()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Xu ly getProductShopMap
	 * @author vuongmq
	 * @return String
	 * @since Dec 2, 2015
	 */
	public String getProductShopMap() {
		Date startLogDate = DateUtil.now();
		/*result.put("page", page);
		result.put("max", max);*/
		try {
			shop = shopMgr.getShopById(shopId);
			if (shopId == null || shop == null || super.getMapShopChild().get(shopId) == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
				return JSON;
			}
			BasicFilter<ShopParamVOProduct> filter = new BasicFilter<>();
			/*KPaging<ShopParamVOProduct> kPaging = new KPaging<ShopParamVOProduct>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);*/
			filter.setShopId(shopId);
			List<ShopParamVOProduct> lst = shopParamMgr.getListProductShopMapByFilter(filter);
			result.put("rows", lst);
			/*if (vo != null && vo.getLstObject() != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<ShopParamVOProduct>());
			}*/
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ShopParamCatalogAction.getProductShopMap()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Xu ly setValueFromDateToDateStr
	 * @author vuongmq
	 * @param lst
	 * @return List<ShopParamVO> 
	 * @since Nov 30, 2015
	 */
	private List<ShopParamVO> setValueFromDateToDateStr(List<ShopParamVO> lst) {
		if (lst != null && lst.size() > 0) {
			for (int i = 0, sz = lst.size(); i < sz; i++) {
				ShopParamVO shopParamVO = lst.get(i);
				if (shopParamVO != null) {
					String value = lst.get(i).getValue(); 
					if (!StringUtil.isNullOrEmpty(value)) {
						shopParamVO.setValueFromDateStr(StringUtil.getDDMMYYYYStr(value, 0));
						shopParamVO.setValueToDateStr(StringUtil.getDDMMYYYYStr(value, 1));
					}
				}
			}
		}
		return lst;
	}
	
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ShopParamVOSave getShopParamVOSave() {
		return shopParamVOSave;
	}

	public void setShopParamVOSave(ShopParamVOSave shopParamVOSave) {
		this.shopParamVOSave = shopParamVOSave;
	}

	public String getCloseDayHM() {
		return closeDayHM;
	}

	public void setCloseDayHM(String closeDayHM) {
		this.closeDayHM = closeDayHM;
	}

	public Integer getBeforeMinute() {
		return beforeMinute;
	}

	public void setBeforeMinute(Integer beforeMinute) {
		this.beforeMinute = beforeMinute;
	}

	public List<ShopParamVOProduct> getLstProductVO() {
		return lstProductVO;
	}

	public void setLstProductVO(List<ShopParamVOProduct> lstProductVO) {
		this.lstProductVO = lstProductVO;
	}

}

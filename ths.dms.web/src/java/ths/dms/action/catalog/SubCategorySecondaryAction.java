package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class SubCategorySecondaryAction extends AbstractAction {	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -785153539745558535L;

	/** The id. */
	private Long productInfoId;
	
	/** The productInfoMap. */
	private String productInfoMap;
	
	/** The productInfoCode. */
	private String productInfoCode;
	
	/** The producInfoName. */
	private String productInfoName;
	
	/** The status. */
	private String status;	
	
	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;

	/** The list category . */
	private List<ProductInfo> lstCategoryType;
	
	
	public Long getProductInfoId() {
		return productInfoId;
	}

	public void setProductInfoId(Long productInfoId) {
		this.productInfoId = productInfoId;
	}

	public List<ProductInfo> getLstCategoryType() {
		return lstCategoryType;
	}

	public void setLstCategoryType(List<ProductInfo> lstCategoryType) {
		this.lstCategoryType = lstCategoryType;
	}

	/**
	 * getId
	 * @return the id
	 */
	public Long getId() {
	    return productInfoId;
	}

	/**
	 * setId
	 * @param id the id to set
	 */
	public void setId(Long productInfoId) {
	    this.productInfoId = productInfoId;
	}

	

	public String getProductInfoMap() {
		return productInfoMap;
	}

	public void setProductInfoMap(String productInfoMap) {
		this.productInfoMap = productInfoMap;
	}

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public String getProductInfoName() {
		return productInfoName;
	}

	public void setProductInfoName(String productInfoName) {
		this.productInfoName = productInfoName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ProductInfoMgr getProductInfoMgr() {
		return productInfoMgr;
	}

	public void setProductInfoMgr(ProductInfoMgr productInfoMgr) {
		this.productInfoMgr = productInfoMgr;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {
		resetToken(result);
	    status = ActiveType.RUNNING.getValue().toString();
	    lstCategoryType = new ArrayList<ProductInfo>();
		try {
			lstCategoryType = productInfoMgr.getListCatInSecondSubCat();
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
	    return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author liemtpt
	 * @since 29/07/2013 
	 */
	public String search(){
	    
	    result.put("page", page);
	    result.put("max", max);
	    try{
	    	KPaging<ProductInfoVOEx> kPaging = new KPaging<ProductInfoVOEx>();
	    	kPaging.setPageSize(max);
	    	kPaging.setPage(page-1);
	    	ObjectVO<ProductInfoVOEx> lstProductInfoOfSecondSubCat = productInfoMgr.getListProductInfoOfSecondSubCat(kPaging, productInfoMap, productInfoCode, productInfoName);
	    	if(lstProductInfoOfSecondSubCat!= null){
	    		result.put("total", lstProductInfoOfSecondSubCat.getkPaging().getTotalRows());
	    		result.put("rows", lstProductInfoOfSecondSubCat.getLstObject());		
	    	}    		
	    }catch (Exception e) {
		LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author liemtpt
	 * @since 29/07/2013 
	 */
	public String saveOrUpdate() {

		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(productInfoCode, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(productInfoName, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				if (productInfoId != null && productInfoId > 0) {
					productInfoMgr.updateToProductInfoOfSecondSubCat(productInfoMap, productInfoCode, productInfoName, null, getLogInfoVO());
//					productInfoMgr.updateToProductInfoOfSecondSubCat(productInfoCode, productInfoName, null, getLogInfoVO());
				} else {
//					ProductInfo productInfoTmp = productInfoMgr.getProductInfoByCode(productInfoCode,ProductType.SECOND_SUB_CAT);
					ProductInfo productInfoTmp = productInfoMgr.getProductInfoByCode(productInfoCode, ProductType.SECOND_SUB_CAT, productInfoMap, true);
					if(productInfoTmp!=null){
						result.put(ERROR, true);
		    			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.sub.cat.product.info.code.not.exist"));
		    			return JSON;
					}
					productInfoMgr.addToProductInfoOfSecondSubCat(productInfoMap, productInfoCode, productInfoName, null, getLogInfoVO());
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		return JSON;
	}
	

}

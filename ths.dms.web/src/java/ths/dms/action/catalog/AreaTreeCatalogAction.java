package ths.dms.action.catalog;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.AreaMgr;
import ths.dms.core.entities.Area;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class AreaTreeCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	private Long areaId;
	private String areaCode;
	private String areaName;
	private Long parentId;
	private AreaMgr areaMgr;
	private List<Area> lstParentCode;
	private Integer status;
	private File excelFile;
	private String excelFileContentType;
	private String areaParentCode;
	protected String areaParentName;
	private String sort;
	private String order;

	public String getAreaParentCode() {
		return areaParentCode;
	}

	public void setAreaParentCode(String areaParentCode) {
		this.areaParentCode = areaParentCode;
	}

	public String getAreaParentName() {
		return areaParentName;
	}

	public void setAreaParentName(String areaParentName) {
		this.areaParentName = areaParentName;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<Area> getLstParentCode() {
		return lstParentCode;
	}

	public void setLstParentCode(List<Area> lstParentCode) {
		this.lstParentCode = lstParentCode;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		areaMgr = (AreaMgr) context.getBean("areaMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		mapControl = getMapControlToken();
		return SUCCESS;
	}

	/**
	 * Search.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012
	 */
	public String search() {

		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Area> kPaging = new KPaging<Area>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if (parentId != null && parentId == -1) {
				parentId = null;
			}
			ActiveType tmp = null;
			if (status != null && status != ConstantManager.NOT_STATUS) {
				tmp = ActiveType.parseValue(status);
			}
			//phuocdh2 modified
			if (!StringUtil.isNullOrEmpty(sort)) {
				if ("areaCode".equals(sort)) {
					sort = " a.area_Code ";
				} else if ("areaName".equals(sort)) {
					sort = " a.area_Name ";
				} else if ("areaParentCode".equals(sort)) {
					sort = " ar.area_code ";
				} else if ("areaParentName".equals(sort)) {
					sort = " ar.area_name ";
				} else if ("status".equals(sort)) {
					sort = "a.status";
				}
			}
			ObjectVO<Area> lstArea = areaMgr.getListArea(kPaging, areaCode, areaName, parentId, tmp, null, null, null, null, null, null, null, true, sort, order);
			if (lstArea != null) {
				result.put("total", lstArea.getkPaging().getTotalRows());
				result.put("rows", lstArea.getLstObject());
			}

		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AreaTreeCatalogAction.search()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	public String getAreaDetailById() {
		try {
			Area area = areaMgr.getAreaById(areaId);
			result.put("area", area);
		} catch (BusinessException e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AreaTreeCatalogAction.getAreaDetailById()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	public String updateArea() {
		try {
			Area area = areaMgr.getAreaById(areaId);
			if (area != null && status != null) {
				if (ActiveType.RUNNING.equals(ActiveType.RUNNING.parseValue(status))) {
					area.setStatus(ActiveType.RUNNING);
				} else {
					area.setStatus(ActiveType.STOPPED);
				}
				areaMgr.updateArea(area, getLogInfoVO());
				result.put(ERROR, true);
			}
			result.put("area", area);
		} catch (BusinessException e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AreaTreeCatalogAction.updateArea()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Import excel file.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 *
	 * @author hunglm16
	 * @since JUNE 18,2014
	 * @description Fix bug
	 */
	public String importExcelFile() {
		//IMPORT FILE EXCEL THONG TIN CAY DIA BAN
		isError = true;
		totalItem = 0;
		String message = "";
		String msg = "";
		String value = "";
		lstView = new ArrayList<CellBean>();
		typeView = true;
		boolean isUpdate = false;
		Area temp = new Area();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 3);
		if (StringUtil.isNullOrEmpty(errMsg)) {
			if (lstData != null && lstData.size() > 0) {
				try {
					for (int i = 0; i < lstData.size(); i++) {
						if (lstData.get(i) != null && lstData.get(i).size() > 0) {
							message = "";
							totalItem++;
							temp = new Area();
							List<String> row = lstData.get(i);
							isUpdate = false;
							//Ma Dia Ban
							if (row != null && row.size() > 2) {
								msg = "";
								value = row.get(0);
								if (!StringUtil.isNullOrEmpty(value)) {
									msg = ValidateUtil.validateField(value, "catalog.area.tree.code", 40, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (StringUtil.isNullOrEmpty(msg)) {
										temp = areaMgr.getAreaByCode(value.trim());
										if (temp != null) {
											isUpdate = true;
											if (!ActiveType.RUNNING.getValue().equals(temp.getStatus().getValue())) {
												msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_code_node_exist");
											}
										} else {
											temp = new Area();
											msg += ValidateUtil.getErrorMsgOfSpecialCharInCode(value, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.area.tree.code"));//Mã địa bàn
											if (StringUtil.isNullOrEmpty(msg)) {
												for (int j = 0; j < i; j++) {
													List<String> rowJCode = lstData.get(j);
													if (rowJCode != null && rowJCode.size() > 2 && rowJCode.get(0) != null && rowJCode.get(0).trim().length() > 0 && rowJCode.get(0).trim().toUpperCase().equals(value.trim().toUpperCase())) {
														msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_row") + String.valueOf(i + 1)
																+ Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_area_code_confict_row") + String.valueOf(j + 1);
													}
												}
												temp.setAreaCode(value.trim());
											}
										}
									}
								} else {
									msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_code_is_not_null");
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									message += "\n";
								}

								//Ten Dia Ban
								msg = "";
								value = row.get(1);
								if (!StringUtil.isNullOrEmpty(value)) {
									//msg = ValidateUtil.validateField(value, "catalog.area.tree.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
									/**
									 * vuongmq; 27/08/2015; bo check kiem tra ky tu
									 * dac biet cua ten dia ban
									 */
									msg = ValidateUtil.validateField(value, "catalog.area.tree.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH);
									if (StringUtil.isNullOrEmpty(msg)) {
										//msg += ValidateUtil.getErrorMsgOfSpecialCharInName(value,"catalog.area.tree.name" );
										if (StringUtil.isNullOrEmpty(msg)) {
											/**
											 * vuongmq; 27/08/2015; bo check kiem
											 * tra trung ten cua dia ban /*for(int
											 * j=0; j<i; j++){ List<String> rowJName
											 * = lstData.get(j); if(rowJName!=null
											 * && rowJName.size()>2 &&
											 * rowJName.get(1)!=null &&
											 * rowJName.get(1).trim().length()>0 &&
											 * rowJName
											 * .get(1).trim().toUpperCase().equals
											 * (value.trim().toUpperCase())){ msg +=
											 * Configuration
											 * .getResourceString(ConstantManager
											 * .VI_LANGUAGE, "area_tree_row") +
											 * String.valueOf(i+1) +
											 * Configuration.getResourceString
											 * (ConstantManager.VI_LANGUAGE,
											 * "area_tree_area_name_confict_row") +
											 * String.valueOf(j+1); } }
											 */
											temp.setAreaName(value.trim());
										}
									}
								} else {
									msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_area_name_not_null");
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									message += "\n";
								}

								//Ma Dia Ban Cha
								msg = "";
								value = row.get(2);
								if (!StringUtil.isNullOrEmpty(value)) {
									msg = ValidateUtil.validateField(value, "catalog.area.tree.parent.code", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
									if (StringUtil.isNullOrEmpty(msg)) {
										if (value.trim().equals("-1")) {
											temp.setType(AreaType.COUNTRY);
											temp.setProvince(null);
											temp.setProvinceName(null);
											temp.setDistrict(null);
											temp.setDistrictName(null);
											temp.setPrecinct(null);
											temp.setPrecinctName(null);
										} else {
											Area parentArea = areaMgr.getAreaByCode(value.trim());
											if (parentArea != null) {
												//co ma cha->check ma cho co bi thay doi ko
												if (isUpdate) {
													if (temp.getParentArea() == null && areaMgr.getListSubArea(temp.getId()).size() > 0) {
														msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_CANT_CHANGE_PARENT);
													} else if (temp.getParentArea() != null && !temp.getParentArea().getAreaCode().equalsIgnoreCase(value) && areaMgr.getListSubArea(temp.getId()).size() > 0) {//thay doi
														msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_CANT_CHANGE_PARENT);
													}
													if (temp.getAreaCode().equalsIgnoreCase(parentArea.getAreaCode())) {
														//ma dia ban = ma cha
														msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_CANT_PARENT_MYSELF);
													}
												}
												if (ActiveType.RUNNING.equals(parentArea.getStatus())) {
													temp.setParentArea(parentArea);
													if (AreaType.COUNTRY.equals(parentArea.getType())) {
														temp.setType(AreaType.PROVINCE);
														temp.setProvince(row.get(0));
														temp.setProvinceName(row.get(1));
														temp.setDistrict(null);
														temp.setDistrictName(null);
														temp.setPrecinct(null);
														temp.setPrecinctName(null);
													} else if (AreaType.PROVINCE.equals(parentArea.getType())) {
														temp.setType(AreaType.DISTRICT);
														temp.setProvince(parentArea.getProvince());
														temp.setProvinceName(parentArea.getProvinceName());
														temp.setDistrict(row.get(0));
														temp.setDistrictName(row.get(1));
														temp.setPrecinct(null);
														temp.setPrecinctName(null);
													} else if (AreaType.DISTRICT.equals(parentArea.getType())) {
														temp.setType(AreaType.WARD);
														temp.setProvince(parentArea.getProvince());
														temp.setProvinceName(parentArea.getProvinceName());
														temp.setDistrict(parentArea.getDistrict());
														temp.setDistrictName(parentArea.getDistrictName());
														temp.setPrecinct(row.get(0));
														temp.setPrecinctName(row.get(1));
													} else {
														msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_PARENT_AREA_IN_WARD_TYPE, parentArea.getAreaCode());
													}
												} else {
													msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.area.tree.parent.code");
												}
											} else {
												msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.area.tree.parent.code");
											}
										}
									}
								} else {//ma cha null
									if (isUpdate) {
										if (temp.getParentArea() != null && areaMgr.getListSubArea(temp.getId()).size() > 0) {
											msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_CANT_CHANGE_PARENT);
										}
									}
									temp.setParentArea(null);
									temp.setType(AreaType.COUNTRY);
									temp.setProvince(null);
									temp.setProvinceName(null);
									temp.setDistrict(null);
									temp.setDistrictName(null);
									temp.setPrecinct(null);
									temp.setPrecinctName(null);
								}
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								}

								//Thoa Man D/k -> Update Database
								if (StringUtil.isNullOrEmpty(message)) {
									if (isUpdate) {
										temp.setUpdateUser(currentUser.getUserName());
										temp.setUpdateDate(commonMgr.getSysDate());
										areaMgr.updateArea(temp, getLogInfoVO());
									} else {
										temp.setCreateUser(currentUser.getUserName());
										temp.setCreateDate(commonMgr.getSysDate());
										areaMgr.createArea(temp, getLogInfoVO());
									}
								} else {
									lstFails.add(StringUtil.addFailBean(row, message));
								}
							}
						}
					}
					//Export error
					getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_AREA_TREE_FAIL);
				} catch (Exception e) {
					LogUtility.logError(e, e.getMessage());
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				}
			} else {
				errMsg = R.getResource("unit_tree.import.no.data");
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	public String getListExcelData() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (parentId != null && parentId == -1) {
				parentId = null;
			}
			ActiveType tmp = null;
			if (status != null && status != ConstantManager.NOT_STATUS) {
				tmp = ActiveType.parseValue(status);
			}
			ObjectVO<Area> lstArea = areaMgr.getListArea(null, areaCode, areaName, parentId, tmp, null, null, null, null, null, null, null, true, sort, order);
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if (lstArea != null && lstArea.getLstObject() != null) {
				int size = lstArea.getLstObject().size();
				for (int i = 0; i < size; i++) {
					Area temp = lstArea.getLstObject().get(i);
					CellBean cellBean = new CellBean();
					cellBean.setContent1(temp.getAreaCode());
					cellBean.setContent2(temp.getAreaName());
					if (temp.getParentArea() != null) {
						cellBean.setContent3(temp.getParentArea().getAreaCode());
						cellBean.setContent4(temp.getParentArea().getAreaName());
					}
					if (temp.getStatus() != null) {
						if (ActiveType.RUNNING.equals(temp.getStatus())) {
							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.name"));
						} else if (ActiveType.STOPPED.equals(temp.getStatus())) {
							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "pause.status.name"));
						}
					}
					lstFails.add(cellBean);
				}
			}

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("areaCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_code"));
			params.put("areaName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_name"));
			params.put("parentAreaCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_parent_code_id"));
			params.put("parentAreaName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_parent_name"));

			String sheetName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "template_catalog_area_sheet");
			String outputFile = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "template_catalog_area_export") + FileExtension.XLS.getValue();

			exportExcelDataNotSessionXLS(params, lstFails, ConstantManager.TEMPLATE_CATALOG_AREA_EXPORT, outputFile, "tempSheetName01", sheetName); // "tempSheetName01" - Default template excel sheetName);
		} catch (Exception e) {
			result.put(ERROR, true);
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AreaTreeCatalogAction.getListExcelData()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * modified: chinh sua template da ngon ngu va tra ve duong dan
	 *
	 * @author phuocdh2
	 * @since 13/03/2015
	 */
	public String downloadTemplate() {
		String path = "";
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("areaCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_code"));
			params.put("areaName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_name"));
			params.put("parentAreaCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_parent_code_id"));
			params.put("parentAreaName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "area_tree_parent_name"));

			List<CellBean> list = new ArrayList<CellBean>();

			params.put("listacd", list);

			String sheetName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sheet_catalog_area_import");
			String outputFile = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "template_catalog_area_import") + FileExtension.XLS.getValue();

			path = putDataToExcelXLS(params, ConstantManager.TEMPLATE_CATALOG_AREA_IMPORT, outputFile, "tempSheetName01", sheetName); // "tempSheetName01" - Default template excel sheetName

			result.put("path", path);
			MemcachedUtils.putValueToMemcached(reportToken, path, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AreaTreeCatalogAction.downloadTemplate()"), createLogErrorStandard(startLogDate));
		}
		return JSON;

	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
}

package ths.dms.action.catalog;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.PixelGrabber;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.JsPromotionShopTreeNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.ImageUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ImageMgr;
//import ths.dms.core.business.ImageMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.OfficeDocShopMap;
import ths.dms.core.entities.OfficeDocument;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DocumentType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffRoleType;
//import ths.dms.core.entities.enumtype.ShopCodeRoot;
//import ths.dms.core.entities.enumtype.StaffRoleType;
import ths.dms.core.entities.filter.OfficeDocumentFilter;
import ths.dms.core.entities.vo.NewTreeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OfficeDocmentVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;



public class ProgramDocumentAction extends AbstractAction {

	/**
	 * Khai bao thuoc tinh chung
	 * @author hunglm16
	 */
	private static final long serialVersionUID = 1L;
	
//	private DisplayProgrameMgr displayProgramMgr;
	private ShopMgr shopMgr;
	private StaffMgr staffMgr;
	private CommonMgr commonMgr;
	private ImageMgr imageMgr;
	
	private Shop shop;
	private Staff staffSign;
	private OfficeDocmentVO officeDocmentVO;
	
	private int length;
	
	private Long idOfficeDoc;
	private Long shopId;
	private Long focusId;
	private Long shopIDAd;
	
	private Integer typeDocument;
	private Integer idOfficeDocument;
	private Integer roleType;
	private Integer checkSubmit;
	private Integer idShopGT;
	
	private String documentCode;
	private String documentName;
	private String fromDate;
	private String toDate;
	private String pathLocalPdfFile;
	private String stringProgram;
	private String promotionProgramCode;
	private String promotionProgramName;
	private String shopCode;
	private String shopName;
	private Long docId;

	private List<Long> listShopId;

	private File pdfFile;
	
	/**
	 * Khai bao cac phuong thuc
	 * @author hunglm16
	 */
	
	@Override
	public void prepare(){
		try {			
			super.prepare();
			staffMgr = (StaffMgr)context.getBean("staffMgr");
			shopMgr =(ShopMgr) context.getBean("shopMgr");
			imageMgr = (ImageMgr)context.getBean("imageMgr");
			commonMgr = (CommonMgr)context.getBean("commonMgr");
			//displayProgramMgr = (DisplayProgrameMgr) context.getBean("displayProgrameMgr");
			staffSign = getStaffByCurrentUser();
			
				Shop shopChoose = (Shop)request.getSession().getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
				if (currentUser != null && currentUser.getUserName() != null) {
					staff = staffMgr.getStaffByCode(currentUser.getUserName());
				}
				if(shopChoose!=null){
					shop = shopChoose;
					shopCode = shop.getShopCode();
					shopName = shop.getShopName();
					//request.getSession().setAttribute(ConstantManager.SESSION_SHOP,shop);
				}else{
					if (staff != null && staff.getShop() != null) {
						shop = staff.getShop();
						shopCode = shop.getShopCode();
						shopName = shop.getShopName();
					}
				}
			
		} catch (Exception e) {	
			LogUtility.logError(e, "ProgramDocumentAction.prepare - " + e.getMessage());
		}		
	}

	@Override
	public String execute() {
		resetToken(result);
		fromDate = DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_DDMMYYYY);
		return SUCCESS;
	}
	
	/**
	 * Search Shop Join Shop-Map
	 * @author hunglm16
	 * @since 1/10/2013
	 * */
	public String searchShopToOfficeDocument()
	{
		/*List<JsPromotionShopTreeNode> lstStaffTypeTree = new ArrayList<JsPromotionShopTreeNode>();
		JsPromotionShopTreeNode bean = new JsPromotionShopTreeNode();
		NewTreeVO<Shop, ProgrameExtentVO> shopTree = new NewTreeVO<Shop, ProgrameExtentVO>();
		try {
			if (shopId == null) {
				if (currentUser != null) {
					Staff currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
					if(currentStaff.getStaffType().getObjectType() == StaffObjectType.NHVNM.getValue()){
						shopId = currentUser.getShopRoot().getShopId();//currentStaff.getShop().getId();
						//shopIDAd = currentStaff.getShop().getId();
						shopTree = shopMgr.getShopTreeVOForOfficeDocument(currentUser.getUserId(), currentUser.getRoleToken().getRoleId(), shopId, idOfficeDoc, promotionProgramCode, promotionProgramName);
					} else {
						//shopIDAd = Long.parseLong("0");
						result.put("solg", 0);
						result.put("rows", null);
						return JSON;
					}
				}
			}
			if (shopTree.getObject() == null&& shopTree.getListChildren() != null) {
				for (int i = 0; i < shopTree.getListChildren().size(); i++) {
					JsPromotionShopTreeNode tmp = new JsPromotionShopTreeNode();
					if (shopTree.getListChildren().get(i) != null && shopTree.getListChildren().size() > 0) {
						tmp = getJsTreeNodeFocus(tmp, shopTree.getListChildren().get(i), promotionProgramCode, promotionProgramName);
					}
					tmp.setState(ConstantManager.JSTREE_STATE_CLOSE);
					if(i==0){
						tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					}
					lstStaffTypeTree.add(tmp);
				}
			} else {
				bean = getJsTreeNodeFocus(bean, shopTree,promotionProgramCode, promotionProgramName);
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
				lstStaffTypeTree.add(bean);
			}
			if (lstStaffTypeTree != null && lstStaffTypeTree.size() > 0) {
				try {
					length = (lstStaffTypeTree.get(0).getChildren() != null) ? lstStaffTypeTree.get(0).getChildren().size()	: 0;
				} catch (NullPointerException Npe) {
					length = 0;
				}
			}
			result.put("solg", length);
			result.put("rows", lstStaffTypeTree);
			// khong su dung result.put("lstDocShopMap", lstCheckShop);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}*/
		return JSON;
	}
	
	private JsPromotionShopTreeNode getJsTreeNodeFocus(JsPromotionShopTreeNode node,NewTreeVO<Shop,ProgrameExtentVO> channelType,String promotionCode,String promotionName){
		if(channelType!=null && channelType.getObject()!= null){
	    	node.setData(channelType.getObject().getShopCode()+"-"+channelType.getObject().getShopName());
	    	node.setId(channelType.getObject().getId()); // sua getId()
	    	if(channelType.getDetail()!=null){
	    		node.setIsJoin(channelType.getDetail().getIsJoin());
		    	node.setIsShop(channelType.getDetail().getIsNPP());
		    	node.setIsChildJoin(channelType.getDetail().getIsChildJoin());
	    	}	    	
	    	node.setParentId((channelType.getObject().getParentShop() != null )?channelType.getObject().getParentShop().getId():null); // sua getId()
	    	try	{
	    		node.setCountNode(channelType.getListChildren().size());
	    	}
	    	catch(NullPointerException npe) {
	    		node.setCountNode(0);
	    	}
	    	if(!StringUtil.isNullOrEmpty(promotionCode) || !StringUtil.isNullOrEmpty(promotionName)) {
		    	if(channelType.getListChildren()!= null && channelType.getListChildren().size() > 0){
		    		node.setState(ConstantManager.JSTREE_STATE_OPEN);
			    }
	    	}else 	{
	    		if(channelType.getListChildren()!= null && channelType.getListChildren().size() > 0){
	    			node.setState(ConstantManager.JSTREE_STATE_CLOSE);
//	    			node.setState(ConstantManager.JSTREE_STATE_OPEN);
			    }
	    	}
		    List<JsPromotionShopTreeNode> lstChild = new ArrayList<JsPromotionShopTreeNode>();
		    if(channelType.getListChildren()!= null && channelType.getListChildren().size() > 0){
				for(int i=0;i<channelType.getListChildren().size();i++){
					JsPromotionShopTreeNode subBean = new JsPromotionShopTreeNode();  
				    subBean = getJsTreeNodeFocus(subBean,channelType.getListChildren().get(i),promotionCode,promotionName);
				    if(null!=subBean) {
				    	lstChild.add(subBean);
				    }
				}
				if(lstChild.size()>0) {
					node.setChildren(lstChild);
				}
				if(!StringUtil.isNullOrEmpty(promotionCode) || !StringUtil.isNullOrEmpty(promotionName))
		    	{
			    	if(channelType.getListChildren()!= null && channelType.getListChildren().size() > 0){
			    		node.setState(ConstantManager.JSTREE_STATE_OPEN);
				    }
		    	}
				//node.setState(ConstantManager.JSTREE_STATE_CLOSE);
		    }
	    } 
	    return node;	    
	}
	/**
	 * @author tientv
	 * @update hunglm16
	 * @description: Kiem tra an toan thong tin khi search
	 * @since 24/09/2013
	 */
	public String search(){
		/*result.put("page", page);
		result.put("rows", rows);
		result.put("rows",new ArrayList<OfficeDocument>());
		try {
			KPaging<OfficeDocument> kPaging = new KPaging<OfficeDocument>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			OfficeDocumentFilter filter = new OfficeDocumentFilter();
			filter.setType(DocumentType.parseValue(typeDocument));
			filter.setDocumentCode(documentCode);
			filter.setDocumentName(documentName);
			//filter.setRoleType(StaffRoleType.parseValue(currentStaff.getRoleType()));
			filter.setRoleType(StaffObjectType.parseValue(staff.getStaffType().getObjectType()));
			filter.setShopId(shop.getId()); // sua getId()
			if(!StringUtil.isNullOrEmpty(fromDate)){
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_STR));;
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_STR));;
			}
			ObjectVO<OfficeDocument> objectVO = imageMgr.getListOfficeDoc(kPaging, filter);
			if(objectVO!=null){
				result.put("total", objectVO.getkPaging().getTotalRows());
				result.put("rows", objectVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ProgramDocumentAction.search - " + e.getMessage());
		}*/
		return JSON;
	}
	
	/**
	 * @return Excel List Office document
	 * @author hunglm16
	 * @since 24/09/2013
	 * */
	public String exportOfficeDocument(){
		try {
			Staff currentStaff = null;
			if (currentUser != null) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null)
					return JSON;

			} else{
				return JSON;
			}
			KPaging<OfficeDocument> kPaging = new KPaging<OfficeDocument>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			OfficeDocumentFilter filter = new OfficeDocumentFilter();
			filter.setType(DocumentType.parseValue(typeDocument));
			filter.setDocumentCode(documentCode);
			filter.setDocumentName(documentName);
			//filter.setRoleType(StaffRoleType.parseValue(currentStaff.getRoleType()));
			filter.setShopId(currentStaff.getShop().getId()); // sua getId()
			
			if(!StringUtil.isNullOrEmpty(fromDate)){
				filter.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));;
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				filter.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));;
			}
			ObjectVO<OfficeDocument>  objectVO = imageMgr.getListOfficeDoc(null, filter);
			if(objectVO!=null && objectVO.getLstObject()!=null && objectVO.getLstObject().size()>0){
				List<CellBean> lstCelbean = new ArrayList<CellBean>();
				String tempDate = "";
				for(OfficeDocument tempOD:objectVO.getLstObject()){
					tempDate = "";
					if(tempOD!=null){
						CellBean cellBean = new CellBean();
						if(tempOD.getDocumentCode()!=null){
							cellBean.setContent1(tempOD.getDocumentCode());
						}
						if(tempOD.getDocumentName()!=null){
							cellBean.setContent2(tempOD.getDocumentName());
						}
						if(tempOD.getType()!=null){
							cellBean.setContent3(tempOD.getType().name());
						}
						if(tempOD.getFromDate()!=null){
							tempDate += DateUtil.toDateString(tempOD.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
						}else{
							tempDate+="";
						}
						if(tempOD.getToDate()!=null){
							tempDate += " - " + DateUtil.toDateString(tempOD.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY) ;
						}else{
							tempDate+=" - ";
						}
						cellBean.setContent4(tempDate);
						lstCelbean.add(cellBean);
					}
				}
				exportExcelDataDocument(lstCelbean, ConstantManager.TEMPLATE_OFFICE_DOCUMENT_EXPORT_EXCEL);
			}
			else{
				result.put(ERROR, true);
				result.put("hasData", false);
				return JSON;
			}
			return JSON;
		}catch (Exception e) {
	    	result.put(ERROR, true);
	    	result.put("hasData", false);
	    	LogUtility.logError(e, e.getMessage());
	    }
		result.put(ERROR, false);
		result.put("hasData", true);
	    return JSON;
	}
	
	/**
	 * Hien thi cong van
	 * @return
	 */
	public String viewDetailOfficeDocument(){
		float width = 0;
		String urlImage = "";
		try {
			Staff currentStaff = null;
			if (currentUser != null) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null){
					result.put(ERROR, true);
					return JSON;
				}
			} else{
				result.put(ERROR, true);
				return JSON;
			}
			MediaItem mediaItemView = new MediaItem();
			Integer object_type = MediaObjectType.IMAGE_OFFICE_DOCUMENT.getValue(); //object_type = 6; QL cong van
			mediaItemView = imageMgr.getMediaItemByDocumentID(idOfficeDoc, object_type);
			if(mediaItemView!=null){
				if(!StringUtil.isNullOrEmpty(mediaItemView.getUrl())){
					// Loai //urlImage = Configuration.getStaticAvatarServerPath()+ mediaItemView.getUrl();
					urlImage = Configuration.getServerImageDocumentPath()+ mediaItemView.getUrl();
					//urlImage = Configuration.getImgServerDocPath() + mediaItemView.getUrl();
				}
				if(mediaItemView.getWidth()!=null){
					width =  mediaItemView.getWidth();
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
		}
		result.put("urlImage", urlImage);
		result.put("widthImage", width);
		return JSON;
	}
	
	/**
	 * @return Excel List Office document
	 * @author hunglm16
	 * @since 24/09/2013
	 * */
	public String deleteOfficeDocument(){
		try {
			Staff currentStaff = null;
			if (currentUser != null) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null){
					result.put(ERROR, true);
					return JSON;
				}
			} else{
				result.put(ERROR, true);
				return JSON;
			}
			Integer kq = 0;
			OfficeDocument officeDelete = new OfficeDocument();
			officeDelete = imageMgr.getOfficeDocumentById(idOfficeDoc);
			if(officeDelete!=null){
				officeDelete.setStatus(ActiveType.DELETED);
				officeDelete.setUpdateUser(currentUser.getUserName());
				officeDelete.setUpdateDate(commonMgr.getSysDate());
				kq = imageMgr.deleteOfficeDocument(officeDelete);
				if(kq!=1){
					result.put(ERROR, true);
				}
			}
			else{
				result.put(ERROR, true);
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * @return File Excel
	 * @author hunglm16
	 * @since 24/9/2013
	 * */
	public void exportExcelDataDocument(List<CellBean> lstData, String tempFileName){
		try {
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + tempFileName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + tempFileName;
			String exportFileName = Configuration.getStoreRealPath() +"/"+ outputName;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report", lstData);
			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
		} catch (ParsePropertyException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (InvalidFormatException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (IOException e) {
			LogUtility.logError(e, e.getMessage());
		}
	}
	
	/**
	 * Insert Office document
	 * Insert Media Item
	 * @return Image JPG to convert
	 * @author hunglm16
	 * @since 23/09/2013
	 * */
	public String importPdfToOfficeDocAndMedia() {
		try {
			errMsg ="";
			Staff currentStaff = null;
			if(StringUtil.isNullOrEmpty(errMsg) && checkSubmit!=1){
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
			if (StringUtil.isNullOrEmpty(errMsg) && currentUser != null) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null){
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"cv.err.loginSystem");
				}
			}
			if(StringUtil.isNullOrEmpty(errMsg) && currentUser == null){
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"cv.err.loginSystem");
			}
			if(StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(documentCode)){
				if(imageMgr.getOfficeDocumentByCode(documentCode.toUpperCase().trim())!=null){
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"cv.err.insertOffDocSelectCODE");
				}
			}
			// true if the file path exists
			if(StringUtil.isNullOrEmpty(errMsg)&& !pdfFile.exists()){
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"cv.err.filePathNot");
			}
			/*if(!StringUtil.isNullOrEmpty(errMsg)){
				return PAGE_NOT_PERMISSION;
			}*/
			if(errMsg.length()>0){
				result.put(ERROR, true);
				result.put("errMsg", errMsg); 
				return SUCCESS;
			}
			/*---------------------------------------------------------------------*/
			String outputStr = "";
			String url = "";
			if (typeDocument == 3) {
				outputStr = "pdf_cttt_"; // ConstantManager.QLCV_CTTT_IMPORT_PDF;
			}
			if (typeDocument == 2) {
				outputStr = "pdf_cttb_"; // ConstantManager.QLCV_CTTB_IMPORT_PDF;
			}
			if (typeDocument == 1) {
				outputStr = "pdf_ctkm_"; // ConstantManager.QLCV_CTKM_IMPORT_PDF;
			}
			OfficeDocmentVO filterVO = new OfficeDocmentVO();
			if(fromDate!=null){
				filterVO.setFromDate(DateUtil.parse(fromDate.trim(), ConstantManager.FULL_DATE_FORMAT));
			}
			if(toDate!=null){
				filterVO.setToDate(DateUtil.parse(toDate.trim(), ConstantManager.FULL_DATE_FORMAT));
			}
			filterVO.setDocumentCode(documentCode.trim());
			filterVO.setType(typeDocument);
			filterVO.setDocumentName(documentName.trim());
			filterVO.setCreateDate(commonMgr.getSysDate());
			filterVO.setCreateUser(currentUser.getUserName());
			
			//Convert PDF to Image
			String outputName = outputStr + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE);//Chuoi Ten
			//String outputPath = Configuration.getImageRealDocPath() + outputName + ".jpg";//Duong dan luu file anh
			String outputPath = Configuration.getRealImageDocumentPath() + outputName + ".jpg";
			//Create URL to Media Item
			/**Nếu lưu bên SO http://192.168.1.211:9005/SO/upload/ProductImage
			 * thì url cộng thêm : /media/image/
			 * url = "/media/image/" + outputName + ".jpg";
			 * 
			 */
			/**Nếu lưu bên tablet http://192.168.1.211:9005/vdms_release/so/
			 * thì url cộng thêm : /image/2014/1/MNVS14/HT40321/
			 * url = "/image/2014/1/MNVS14/HT40321/" + outputName + ".jpg";
			 * 
			 */
			url =  outputName + ".jpg";
			filterVO.setUrl(url);
			/*---------------------------------------------------------------------*/
			try{
			    PDDocument doc = PDDocument.load(pdfFile);
				/*  // Mac dinh tablet khong dung
				 * Upload file PDF and read to byte
				FileChannel channel = raf.getChannel();
				ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
				PDFFile pdffile = new PDFFile(buf);	*/
			    
			    System.gc();
				int numPgs = doc.getNumberOfPages();
				BufferedImage bufferedImage = null;
				Graphics2D bufImageGraphics = null;
				float width = 0;
				float height = 0;
				int y = 0;
				List<PDPage> pages = doc.getDocumentCatalog().getAllPages();
				Iterator<PDPage> iter = pages.iterator();
				while (iter.hasNext()) {
				    PDPage page = iter.next();
				    BufferedImage bi = page.convertToImage();
				    if(bufferedImage == null && bufImageGraphics == null) {
				        bufferedImage = new BufferedImage(bi.getWidth(), bi.getHeight() * numPgs, BufferedImage.TYPE_INT_RGB);
				    	//bufferedImage = new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_INT_RGB);
				        bufImageGraphics = bufferedImage.createGraphics();
				    }
                    bufImageGraphics.drawImage(bi, null, 0, y);
                    y += bi.getHeight();
				}
				File yourImageFile = new File(outputPath);
				ImageIO.write(bufferedImage, "JPG", yourImageFile);// JPG, PNG
				filterVO.setWidth(width);
				filterVO.setHeight(height);
				/*---------------------------------------------------------------------*/
			}
			catch (IOException ex) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cv.err.filePathEr");
				LogUtility.logError(ex, ex.getMessage() + errMsg);
			}
			if(StringUtil.isNullOrEmpty(errMsg)){
				//Insert Database
				Integer kq  = 1;
				kq = imageMgr.insertOfficeDocument(filterVO);
				if(kq == 0){
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"cv.err.insertOfficedoCument");
				}
				if(kq == -1){
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"cv.err.insertMediaItem");
				}
				if(errMsg.length()>0){
					result.put(ERROR, true);
					result.put("errMsg", errMsg); 
					return SUCCESS;
				}
			}
			
			
		}catch (Exception e) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Insert And Update Office_Document_Shop_Map
	 * 
	 * */
	
	public String updateAndCreateOffDocShopMap(){
		resetToken(result);
		Boolean flagJson = true;
		try
		{
			Staff currentStaff = null;
			if (currentUser != null && flagJson==true) {
				currentStaff = staffMgr.getStaffByCode(currentUser.getUserName());
				if (currentStaff == null){
					flagJson = false;
				}
			}
			else{
				flagJson = false;
			}
			if(flagJson!=true){
				isError = true;
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"cv.err.loginSystem");
				return JSON;
			}
			List<OfficeDocShopMap> lstOffSh = new ArrayList<OfficeDocShopMap>();
			if(idOfficeDoc!=null){
				lstOffSh= imageMgr.getListOffShopMapToDocumentId(idOfficeDoc);
			}
			Integer kqInsert = 1, kqDelete =1;
			List<Long> g1 = new ArrayList<Long>();
			List<Long> gDelete = new ArrayList<Long>();
			List<Long> gInsert = new ArrayList<Long>();
			
			if (listShopId != null && listShopId.size()>0) {
				if (lstOffSh != null && lstOffSh.size()>0) {
					g1 = layGiaoHaiMang(listShopId, lstOffSh);
					if (g1 != null && g1.size()>0) {
						for (OfficeDocShopMap ofm : lstOffSh) {
							int flgDel = 1;
							for (Long idShopGiao : g1) {
								if (ofm.getShopId().getId().longValue() == idShopGiao.longValue()) { // sua getId()
									flgDel = 0;
								}
							}
							if(flgDel!=0){
								gDelete.add(ofm.getShopId().getId()); // sua getId()
							}
						}
						if (gDelete != null && gDelete.size()>0) {
							// Xoa bo chon
							kqDelete = deleteDocShopMap(gDelete, idOfficeDoc);
						}
					}
					for (Long idShop : listShopId) {
						int flgIns = 1;
						for (Long idShopGiao : g1) {
							if (idShop.longValue() == idShopGiao.longValue()) {
								flgIns = 0;
							}
						}
						if(flgIns!=0){
							gInsert.add(idShop);
						}
					}
					if (gInsert != null && gInsert.size()>0) {
						kqInsert = insertDocShopMap(gInsert, idOfficeDoc);
					}
				}else{
					kqInsert = insertDocShopMap(listShopId, idOfficeDoc);
				}
			}else if(lstOffSh != null){
				kqDelete = deleteDocShopMapALL(lstOffSh, idOfficeDoc);
			}
		}
		catch(Exception e)
		{
			LogUtility.logError(e, e.getMessage());
			flagJson = false;
		}
		
		if(flagJson!=true){
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}else{
			result.put(ERROR, false);
		}
		return JSON;
	}
	
	/**
	 * Xoay hinh anh Quan ly cong van
	 * @return
	 */
	public String rotateDocument() {
	    try {
	        //String folder = Configuration.getPdfImagePath();
	    	//String folder = Configuration.getImageRealDocPath();
	    	String folder = Configuration.getRealImageDocumentPath();
	    	MediaItem mediaItemView = imageMgr.getMediaItemByDocumentID(docId);
	        if(mediaItemView == null) {
	            result.put(ERROR, true);
	            result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
	            return JSON;
	        }
	        String realPath = folder + mediaItemView.getUrl();
	        //String __realPath = this.rotateImage(mediaItemView, realPath); // Mac dinh tablet khong dung
	        ImageUtility.rotateImage(realPath);
	        //__realPath = Configuration.getPdfImagePath()+__realPath; // Mac dinh tablet khong dung
	        Float width = mediaItemView.getWidth();
	        Float height = mediaItemView.getHeight();
	        
	        /*//// Mac dinh tablet khong dung 
	        mediaItemView.setUrl(__realPath);
	        mediaItemView.setWidth(height);
	        mediaItemView.setHeight(width);	        
	        imageMgr.updateMediaItem(mediaItemView);*/
	        //Configuration.getStaticAvatarServerPath();
	        //String urlImage = Configuration.getImgServerDocPath() + mediaItemView.getUrl();
	        String urlImage = Configuration.getServerImageDocumentPath() + mediaItemView.getUrl();
	        result.put(ERROR, false);
	        result.put("urlImage", urlImage);
	        result.put("widthImage", height);
	    } catch (Exception e) {
	        result.put(ERROR, true);
	        result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
	        LogUtility.logError(e, e.getMessage());
        }
	    return JSON;
	}
	
// Mac dinh tablet khong dung	
//	public static String rotateImage(MediaItem mediaItemView, String realPath) throws FileNotFoundException, IOException {
//        BufferedImage oldImage = ImageIO.read(new FileInputStream(realPath));
//        BufferedImage newImage = new BufferedImage(oldImage.getHeight(), oldImage.getWidth(), oldImage.getType());
//        Graphics2D graphics = (Graphics2D) newImage.getGraphics();
//        graphics.rotate(Math.toRadians(90), (0 + newImage.getWidth()) / 2, (0 + newImage.getHeight()) / 2);
//        graphics.drawImage(oldImage, (oldImage.getHeight() - oldImage.getWidth())/2, (oldImage.getWidth() - oldImage.getHeight())/2, oldImage.getWidth(), oldImage.getHeight(), null);
//        
//        String outputStr = "";
//        if (mediaItemView.getType() != null && mediaItemView.getType() == 2) {
//            outputStr = "pdf_cttb_"; // ConstantManager.QLCV_CTTB_IMPORT_PDF;
//        }
//        if (mediaItemView.getType() != null && mediaItemView.getType() == 1) {
//            outputStr = "pdf_ctkm_"; // ConstantManager.QLCV_CTKM_IMPORT_PDF;
//        }
//        
//        String outputName = outputStr + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE);//Chuoi Ten
//        String outputPath = Configuration.getPdfImagePath() + outputName + ".jpg";//Duong dan luu file anh
//        File outputFile = new File(outputPath);
//        if(!outputFile.exists()) {
//            outputFile.createNewFile();
//        }
//        OutputStream out = new FileOutputStream(outputFile);
//        ImageIO.write(newImage, "JPG", out);
//        out.close();
//        return outputName;
//    }
	
	private Integer deleteDocShopMapALL(List<OfficeDocShopMap> lstOffSh, Long iDOfficeDoc){
		Integer kq = 1;
		try{
			OfficeDocmentVO filterVO = new OfficeDocmentVO();
			filterVO.setCreateDate(DateUtil.now());
			filterVO.setCreateUser(currentUser.getUserName());
			kq = imageMgr.deleteOffDocShopMapALL(lstOffSh, filterVO);
		}catch (Exception e) {
			kq = -1;
		}
		return kq;
	}
	
	
	private Integer deleteDocShopMap(List<Long> lstShopID, Long iDOfficeDoc){
		Integer kq = 1;
		try{
		List<Shop> lstShop = new ArrayList<Shop>();
		for (int i = 0; i < lstShopID.size(); i++) {
			Shop s = shopMgr.getShopById(lstShopID.get(i));
			lstShop.add(s);
		}
		OfficeDocmentVO filterVO = new OfficeDocmentVO();
		filterVO.setCreateDate(DateUtil.now());
		filterVO.setCreateUser(currentUser.getUserName());
		OfficeDocument offDoc = new OfficeDocument();
		offDoc = imageMgr.getOfficeDocumentById(iDOfficeDoc);
		kq = imageMgr.deleteOffDocShopMap(lstShop, offDoc, filterVO);
		}catch (Exception e) {
			kq = -1;
		}
		return kq;
	}
	
	
	private Integer insertDocShopMap(List<Long> lstShopID, Long iDOfficeDoc){
		Integer kq = 1;
		try{
		List<Shop> lstShop = new ArrayList<Shop>();
		for (int i = 0; i < lstShopID.size(); i++) {
			Shop s = shopMgr.getShopById(lstShopID.get(i));
			lstShop.add(s);
		}
		OfficeDocmentVO filterVO = new OfficeDocmentVO();
		filterVO.setCreateDate(DateUtil.now());
		filterVO.setCreateUser(currentUser.getUserName());
		OfficeDocument offDoc = new OfficeDocument();
		offDoc = imageMgr.getOfficeDocumentById(iDOfficeDoc);
		kq = imageMgr.insertOffDocShopMap(lstShop, offDoc, filterVO);
		}catch (Exception e) {
			kq = -1;
		}
		return kq;
	}
	
	
	//Lay giao hai mang
	private List<Long> layGiaoHaiMang(List<Long> lstIdShop, List<OfficeDocShopMap> lstOffSh){
		List<Long> kq = new ArrayList<Long>();
		int flag = 0;
		if(lstIdShop.size()>lstOffSh.size()){
			for(Long idShop1:lstIdShop){
				flag = 0;
				for(OfficeDocShopMap ofm:lstOffSh){
					if(idShop1.longValue() == ofm.getShopId().getId().longValue()){ //sua getId()
						flag = 1;
					}
				}
				if(flag!=0){
					kq.add(idShop1);
				}
			}
		}else{
			for(OfficeDocShopMap ofm:lstOffSh){
				flag = 0;
				for(Long idShop2:lstIdShop){
					if(ofm.getShopId().getId().longValue() == idShop2.longValue()){ //sua getId()
						flag = 1;
					}
				}
				if(flag!=0){
					kq.add(ofm.getShopId().getId()); //sua getId()
				}
			}
		}
		return kq;
	}
	
	//Lay tru giao hai mang
	private List<Long> layTruGiaoHaiMang(List<Long> lstIdShop, List<OfficeDocShopMap> lstOffSh){
		List<Long> kq = new ArrayList<Long>();
		if(lstIdShop.size()>lstOffSh.size()){
			for(Long idShop:lstIdShop){
				for(OfficeDocShopMap ofm:lstOffSh){
					if(idShop != ofm.getShopId().getId()){ //sua getId()
						kq.add(idShop);
					}
				}
			}
		}else{
			for(OfficeDocShopMap ofm:lstOffSh){
				for(Long idShop:lstIdShop){
					if(ofm.getShopId().getId() != idShop){ //sua getId()
						kq.add(ofm.getShopId().getId()); //sua getId()
					}
				}
			}
		}
		return kq;
	}
	
	/**
	 * Process for Image
	 * @return toBufferedImage && hasAlpha
	 * @author hunglm16
	 * @since 23/9/2013
	 * */
	public BufferedImage toBufferedImage(Image image) {
		if (image instanceof BufferedImage) {
			return (BufferedImage) image;
		}
		image = new ImageIcon(image).getImage();
		boolean hasAlpha = hasAlpha(image);
		BufferedImage bimage = null;
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		try {
			int transparency = Transparency.OPAQUE;
			if (hasAlpha) {
				transparency = Transparency.BITMASK;
			}
			GraphicsDevice gs = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gs.getDefaultConfiguration();
			bimage = gc.createCompatibleImage(image.getWidth(null),
					image.getHeight(null), transparency);
		} catch (HeadlessException e) {
		}
		
		if (bimage == null) {
			// Create a buffered image using the default color model
			int type = BufferedImage.TYPE_INT_RGB;
			if (hasAlpha) {
				type = BufferedImage.TYPE_INT_ARGB;
			}
			bimage = new BufferedImage(image.getWidth(null),
					image.getHeight(null), type);
		}
		Graphics g = bimage.createGraphics();
		g.drawImage(image, 0, 0, null);
		g.dispose();
		return bimage;
	}
	public boolean hasAlpha(Image image) {
		if (image instanceof BufferedImage) {
			BufferedImage bimage = (BufferedImage) image;
			return bimage.getColorModel().hasAlpha();
		}
		PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
		try {
			pg.grabPixels();
		} catch (InterruptedException e) {
		}
		ColorModel cm = pg.getColorModel();
		return cm.hasAlpha();
	}
	
	/**
	 * Check don vi QL cong van
	 * @return
	 */
	public String getChildShopId(){
		try{
			List<Long> listShopId = new ArrayList<Long>();
			if(checkShopInOrgAccessById(shopId)) {
				List<Shop> lstShop = shopMgr.getListChildShopId(null, shopId);
				if(lstShop != null && lstShop.size() >0){
					int n = lstShop.size();
					for(int i = 0;i < n;i++){
						Long childShopId = lstShop.get(i).getId();
						listShopId.add(childShopId);
					}
				}
				result.put("listShopId", listShopId);
				result.put("length",listShopId.size());
			} else {
				result.put("listShopId", new ArrayList<Long>());
				result.put("length", 0);
			}
		}catch(Exception e){
			
		}
		return JSON;
	}
	
	/**
	 * Khai bao GETTER/SETTER
	 * @author hunglm16
	 */
	public File getPdfFile() {
		return pdfFile;
	}
	public void setPdfFile(File pdfFile) {
		this.pdfFile = pdfFile;
	}
	public Integer getTypeDocument() {
		return typeDocument;
	}
	public void setTypeDocument(Integer typeDocument) {
		this.typeDocument = typeDocument;
	}
	public String getDocumentCode() {
		return documentCode;
	}
	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaffSign() {
		return staffSign;
	}
	public void setStaffSign(Staff staffSign) {
		this.staffSign = staffSign;
	}
	public String getPathLocalPdfFile() {
		return pathLocalPdfFile;
	}
	public void setPathLocalPdfFile(String pathLocalPdfFile) {
		this.pathLocalPdfFile = pathLocalPdfFile;
	}
	public Integer getIdOfficeDocument() {
		return idOfficeDocument;
	}
	public void setIdOfficeDocument(Integer idOfficeDocument) {
		this.idOfficeDocument = idOfficeDocument;
	}
	
	public Long getIdOfficeDoc() {
		return idOfficeDoc;
	}
	public void setIdOfficeDoc(Long idOfficeDoc) {
		this.idOfficeDoc = idOfficeDoc;
	}
	
	public OfficeDocmentVO getOfficeDocmentVO() {
		return officeDocmentVO;
	}
	public void setOfficeDocmentVO(OfficeDocmentVO officeDocmentVO) {
		this.officeDocmentVO = officeDocmentVO;
	}
	
	public StaffMgr getStaffMgr() {
		return staffMgr;
	}
	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}
	public ImageMgr getImageMgr() {
		return imageMgr;
	}
	public void setImageMgr(ImageMgr imageMgr) {
		this.imageMgr = imageMgr;
	}
	public CommonMgr getCommonMgr() {
		return commonMgr;
	}
	public void setCommonMgr(CommonMgr commonMgr) {
		this.commonMgr = commonMgr;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getStringProgram() {
		return stringProgram;
	}

	public void setStringProgram(String stringProgram) {
		this.stringProgram = stringProgram;
	}
	
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}
	
	public String getPromotionProgramName() {
		return promotionProgramName;
	}

	public void setPromotionProgramName(String promotionProgramName) {
		this.promotionProgramName = promotionProgramName;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	public Long getFocusId() {
		return focusId;
	}

	public void setFocusId(Long focusId) {
		this.focusId = focusId;
	}
	
	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	public List<Long> getListShopId() {
		return listShopId;
	}

	public void setListShopId(List<Long> listShopId) {
		this.listShopId = listShopId;
	}
	
	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}
	public Long getShopIDAd() {
		return shopIDAd;
	}

	public void setShopIDAd(Long shopIDAd) {
		this.shopIDAd = shopIDAd;
	}
	
	public Integer getCheckSubmit() {
		return checkSubmit;
	}

	public void setCheckSubmit(Integer checkSubmit) {
		this.checkSubmit = checkSubmit;
	}
	
	public Integer getIdShopGT() {
		return idShopGT;
	}

	public void setIdShopGT(Integer idShopGT) {
		this.idShopGT = idShopGT;
	}

    public Long getDocId() {
        return docId;
    }

    public void setDocId(Long docId) {
        this.docId = docId;
    }
}

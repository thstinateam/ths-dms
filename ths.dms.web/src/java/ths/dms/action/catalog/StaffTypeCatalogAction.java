package ths.dms.action.catalog;

import java.util.List;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;


public class StaffTypeCatalogAction extends AbstractAction {	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	private Integer id;
	private String staffTypeCode;
	private String staffTypeName;
	private Long parentCode;
	private ChannelTypeMgr channelTypeMgr;
	private List<ChannelType> lstParentCode;
	private Integer status;
	private List<ApParam> lstStaffObjectType;
	private ApParamMgr apParamMgr;
	private Integer staffObjectType;
	private Boolean isGetParent;
	
	public Integer getStaffObjectType() {
		return staffObjectType;
	}

	public void setStaffObjectType(Integer staffObjectType) {
		this.staffObjectType = staffObjectType;
	}

	public List<ApParam> getLstStaffObjectType() {
		return lstStaffObjectType;
	}

	public void setLstStaffObjectType(List<ApParam> lstStaffObjectType) {
		this.lstStaffObjectType = lstStaffObjectType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<ChannelType> getLstParentCode() {
		return lstParentCode;
	}

	public void setLstParentCode(List<ChannelType> lstParentCode) {
		this.lstParentCode = lstParentCode;
	}

	public String getStaffTypeCode() {
		return staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	public String getStaffTypeName() {
		return staffTypeName;
	}

	public void setStaffTypeName(String staffTypeName) {
		this.staffTypeName = staffTypeName;
	}

	public Integer getId() {
	    return id;
	}
	
	public void setId(Integer id) {
	    this.id = id;
	}

	public Long getParentCode() {
		return parentCode;
	}

	public void setParentCode(Long parentCode) {
		this.parentCode = parentCode;
	}
	public Boolean getIsGetParent() {
		return isGetParent;
	}

	public void setIsGetParent(Boolean isGetParent) {
		this.isGetParent = isGetParent;
	}
	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    channelTypeMgr = (ChannelTypeMgr)context.getBean("channelTypeMgr");
	    apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {	
		resetToken(result);
	    try {
	    	ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.STAFF);							
	    	filter.setStatus(ActiveType.RUNNING);
			ObjectVO<ChannelType> lstChannelType = channelTypeMgr.getListChannelType(filter,null);
			if(lstChannelType != null){
				lstParentCode = lstChannelType.getLstObject();
			}
			lstStaffObjectType = apParamMgr.getListApParam(ApParamType.STAFF_TYPE, ActiveType.RUNNING);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	    return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012 
	 */
	public String search(){
	    
	    result.put("page", page);
	    result.put("row", rows);
	    try{
	    	KPaging<ChannelType> kPaging = new KPaging<ChannelType>();
	    	kPaging.setPageSize(rows);
	    	kPaging.setPage(page-1);
	    	if(parentCode == -1){
	    		parentCode = null;
	    	}
	    	ActiveType tmp = null;
	    	if(status != null && status != ConstantManager.NOT_STATUS){
	    		tmp = ActiveType.parseValue(status);
	    	}
	    	if(staffObjectType == -1){
	    		staffObjectType = null;
	    	}
	    	ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.STAFF);							
	    	filter.setStatus(tmp);
	    	filter.setChannelTypeCode(staffTypeCode);
	    	filter.setChannelTypeName(staffTypeName);
	    	filter.setObjectType(staffObjectType);
	    	filter.setIsGetParent(isGetParent);
	    	filter.setParentId(parentCode);
	    	ObjectVO<ChannelType> lstStaffType = channelTypeMgr.getListChannelType(filter,kPaging);
	    	if(lstStaffType!=null){
	    		result.put("total", lstStaffType.getkPaging().getTotalRows());
		    	result.put("rows", lstStaffType.getLstObject());		
	    	}	    	
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012 
	 */
	public String saveOrUpdate(){
	    
	    resetToken(result);
	    errMsg ="";
	    if(currentUser!= null){
			try{
			    ChannelType staffType = channelTypeMgr.getChannelTypeByCode(staffTypeCode, ChannelTypeType.STAFF);
			    if(ValidateUtil.validateFormatCode(staffTypeCode) == false){
			    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.code",
							Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.type.code"));
			    	result.put(ERROR, true);
					result.put("errMsg",errMsg );
		    		return ERROR;
			    }
			    if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(staffTypeCode, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
			    if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(staffTypeName, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
			    if(id!= null && id > 0){
			    	if(staffType == null){
			    		result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.staff.type.code.not.corect"));
			    		return ERROR;
			    	}
					if(staffType!= null){
						ChannelType parentChannelType = null;
						if(parentCode != null && parentCode != -1){
							parentChannelType = channelTypeMgr.getChannelTypeById(parentCode);
						}
						if(parentChannelType != null && !parentChannelType.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.type.parentcode.not.running");
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
				    		return ERROR;
						}
						if(parentChannelType != null && channelTypeMgr.checkAncestor(staffType.getId(),parentChannelType.getId())){
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_CORRECT, false, "catalog.staff.type.parentcode");
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
				    		return ERROR;
						}
						if(status != null && status != ConstantManager.NOT_STATUS){
//							if(ActiveType.STOPPED.getValue().equals(status) && (!channelTypeMgr.isAllChildStoped(staffType.getId(), ChannelTypeType.STAFF) || channelTypeMgr.isUsingByOtherRunningItem(staffType.getId(),ChannelTypeType.STAFF))){
							if(ActiveType.STOPPED.getValue().equals(status) && !channelTypeMgr.isAllChildStoped(staffType.getId(), ChannelTypeType.STAFF)){
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.type.is.used",
										Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.type.code"));
								result.put(ERROR, true);
								result.put("errMsg", errMsg);
					    		return ERROR;
							}else{
								staffType.setStatus(ActiveType.parseValue(status));
							}
						}
						staffType.setChannelTypeCode(staffTypeCode);
						staffType.setChannelTypeName(staffTypeName);
						staffType.setParentChannelType(parentChannelType);
						staffType.setType(ChannelTypeType.STAFF);
						if(parentChannelType != null){
							staffType.setObjectType(parentChannelType.getObjectType());
						}else{
							staffType.setObjectType(staffObjectType);
						}
						channelTypeMgr.updateChannelType(staffType,getLogInfoVO());
					}
			    } else {
					if(staffType!= null){
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.code.exist",
								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.type.code"));
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
			    		return ERROR;
					} else {
						ChannelType parentChannelType = null;
						if(parentCode != null && parentCode != -1){
							parentChannelType = channelTypeMgr.getChannelTypeById(parentCode);
						}
						if(parentChannelType != null && !parentChannelType.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.type.parentcode.not.running");
							result.put(ERROR, true);
							result.put("errMsg", errMsg);
				    		return ERROR;
						}
						ChannelType newStaffType = new ChannelType();
						newStaffType.setChannelTypeCode(staffTypeCode);
						newStaffType.setChannelTypeName(staffTypeName);
						newStaffType.setParentChannelType(parentChannelType);
						if(status != null && status != ConstantManager.NOT_STATUS){
							newStaffType.setStatus(ActiveType.parseValue(status));
						}
						newStaffType.setType(ChannelTypeType.STAFF);
						if(parentChannelType != null){
							if(parentChannelType.getObjectType() != null) {
								newStaffType.setObjectType(parentChannelType.getObjectType());
							}
						} else {
							newStaffType.setObjectType(staffObjectType);
						}
						newStaffType = channelTypeMgr.createChannelType(newStaffType,getLogInfoVO());
					}
			    }		    
			}catch (Exception e) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				LogUtility.logError(e, "Error StaffTypeCatalogAction.saveOrUpdate: " + e.getMessage());
				return ERROR;
			}		
	    }
	    result.put(ERROR, false);
	    return SUCCESS;
	}
	
	/**
	 * Delete staff type.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String deleteStaffType(){
	    
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
			    ChannelType channelType = channelTypeMgr.getChannelTypeByCode(staffTypeCode, ChannelTypeType.STAFF);
			    if(channelType!= null){
			    	if(!channelTypeMgr.isAllChildStoped(channelType.getId(), ChannelTypeType.STAFF) || channelTypeMgr.isUsingByOthers(channelType.getId(), ChannelTypeType.STAFF)){
			    		errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.type.is.used",
								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.type"));
			    	}else{
			    		channelType.setStatus(ActiveType.DELETED);
			    		channelTypeMgr.updateChannelType(channelType,getLogInfoVO());
			    		error = false;
			    	}
			    }
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}
	
	/**
	 * Gets the list parent code.
	 *
	 * @return the list parent code
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String getListParentCode(){
		
		try {
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.STAFF);							
	    	filter.setStatus(ActiveType.RUNNING);	    	
			ObjectVO<ChannelType> tmp = channelTypeMgr.getListChannelType(filter,null);
			if(tmp != null){
				List<ChannelType> parentTmp = tmp.getLstObject();
				result.put("lstParentCode", parentTmp);
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	public String update() {
		if (currentUser != null) {
			try {
				ChannelType ct = new ChannelType();
				if(id!=null && id>0){
					ct = channelTypeMgr.getChannelTypeById(Long.valueOf(id));
					result.put("channelType", ct);
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return JSON;
	}
}

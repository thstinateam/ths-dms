package ths.dms.action.catalog;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.utils.LogUtility;

public class SubCategoryCatalogAction extends AbstractAction {	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -785153539745558535L;

	/** The id. */
	private Long id;
	
	/** The code. */
	private String code;
	
	/** The name. */
	private String name;
	
	/** The note. */
	private String note;
	
	/** The status. */
	private String status;	
			
	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;

	/**
	 * getId
	 * @return the id
	 */
	public Long getId() {
	    return id;
	}

	/**
	 * setId
	 * @param id the id to set
	 */
	public void setId(Long id) {
	    this.id = id;
	}

	/**
	 * getCode
	 * @return the code
	 */
	public String getCode() {
	    return code;
	}

	/**
	 * setCode
	 * @param code the code to set
	 */
	public void setCode(String code) {
	    this.code = code;
	}

	/**
	 * getName
	 * @return the name
	 */
	public String getName() {
	    return name;
	}

	/**
	 * setName
	 * @param name the name to set
	 */
	public void setName(String name) {
	    this.name = name;
	}

	/**
	 * getNote
	 * @return the note
	 */
	public String getNote() {
	    return note;
	}

	/**
	 * setNote
	 * @param note the note to set
	 */
	public void setNote(String note) {
	    this.note = note;
	}

	/**
	 * getStatus
	 * @return the status
	 */
	public String getStatus() {
	    return status;
	}

	/**
	 * setStatus
	 * @param status the status to set
	 */
	public void setStatus(String status) {
	    this.status = status;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {	
	    status = ActiveType.RUNNING.getValue().toString();
	    resetToken(result);
	    return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 18/07/2012 
	 */
	public String search(){
	    
	    result.put("page", page);
	    result.put("max", max);
	    try{
	    	KPaging<ProductInfo> kPaging = new KPaging<ProductInfo>();
	    	kPaging.setPageSize(max);
	    	kPaging.setPage(page-1);
	    	/*ObjectVO<ProductInfo> lstReasonGroup = productInfoMgr.getListProductInfo(
    	    						kPaging, code, name, note,
    	    						ActiveType.parseValue(Integer.valueOf(status)),ProductType.SUB_CAT,true);
	    	if(lstReasonGroup!= null){
	    		result.put("total", lstReasonGroup.getkPaging().getTotalRows());
	    		result.put("rows", lstReasonGroup.getLstObject());		
	    	}*/    		
	    }catch (Exception e) {
		LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 18/07/2012 
	 */
//	public String saveOrUpdate(){
//	    
//	    resetToken(result);
//	    boolean error = true;
//	    if(currentUser!= null){
//		try{
//		    ProductInfo productInfo = productInfoMgr.getProductInfoByCode(code, ProductType.SUB_CAT);
//		    if(id!= null && id > 0){
//			if(productInfo!= null){ 
//			    if(id.intValue() != productInfo.getId().intValue()){			    	
//			    	result.put("errMsg",ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.sub.cat.code"));
//			    } else{
//				 productInfo.setProductInfoCode(code);
//				 productInfo.setProductInfoName(name);
//				 productInfo.setDescription(note);
//				 productInfo.setStatus(ActiveType.parseValue(Integer.valueOf(status)));
//				 productInfo.setType(ProductType.SUB_CAT);
//				 productInfoMgr.updateProductInfo(productInfo,getLogInfoVO());
//				 error = false;
//			    }
//			}    
//		    } else {
//			if(productInfo!= null){
//				result.put("errMsg",ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.sub.cat.code"));			    
//			} else {
//			    ProductInfo newProductInfo = new ProductInfo();
//			    newProductInfo.setProductInfoCode(code);
//			    newProductInfo.setProductInfoName(name);
//			    newProductInfo.setDescription(note);
//			    newProductInfo.setStatus(ActiveType.parseValue(Integer.valueOf(status)));
//			    newProductInfo.setType(ProductType.SUB_CAT);
//			    newProductInfo = productInfoMgr.createProductInfo(newProductInfo,getLogInfoVO());
//			    if(newProductInfo!= null){
//				error = false;
//			    }
//			}
//		    }		    
//		}catch (Exception e) {
//		    LogUtility.logError(e, e.getMessage());
//		}		
//	    }
//	    result.put(ERROR, error);	    
//	    return JSON;
//	}
	
	/**
	 * Delete 
	 *
	 * @return the string
	 * @author hungtx
	 * @since 18/07/2012 
	 */
//	public String delete(){
//	    
//	    resetToken(result);
//	    boolean error = true;
//	    if(currentUser!= null){
//		try{
//		    ProductInfo productInfo = productInfoMgr.getProductInfoByCode(code, ProductType.SUB_CAT);
//		    if(productInfo!= null){
//			if(productInfoMgr.isUsingByOthers(productInfo.getId())){
//				result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED_DB,
//								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.sub.cat")));						    
//			}else{
//				productInfo.setStatus(ActiveType.DELETED);
//			    productInfoMgr.updateProductInfo(productInfo,getLogInfoVO());
//			    error = false;
//			}
//		    } 
//		   		    
//		}catch (Exception e) {
//		    LogUtility.logError(e, e.getMessage());
//		}
//	    }
//	    result.put(ERROR, error);
//	    return JSON;
//	}
//
}

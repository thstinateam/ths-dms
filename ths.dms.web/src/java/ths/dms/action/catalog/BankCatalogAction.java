package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.Date;

import ths.dms.core.business.BankMgr;
import ths.dms.core.entities.Bank;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.filter.BankFilter;
import ths.dms.core.entities.vo.BankVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class BankCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	private BankMgr bankMgr;

	private String bankCode;
	private String bankName;
	private String bankAccount;
	private String bankPhone;
	private String bankAddress;
	private Integer status;
	private Long bankId;
	private Long shopId;
	private String shopCode;
	private Shop shop;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		//productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		bankMgr = (BankMgr) context.getBean("bankMgr");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {
		try {
			resetToken(result);
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				if (shop != null) {
					shopId = shop.getId();
					shopCode = shop.getShopCode();
				}
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NO_DATA)); // loi du lieu khong ton tai shop
				return JSON;
			}
		} catch (BusinessException e) {
			//			LogUtility.logError(e, "Error BankCatalogAction.execute()" + e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BankCatalogAction.execute()"), createLogErrorStandard(startLogDate));
		}
		return SUCCESS;
	}

	/**
	 * Search.
	 *
	 * @return the string
	 * @author vuongmq
	 * @since 23/09/2014
	 */
	public String search() {
		try {
			BankFilter filter = new BankFilter();
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop != null) {
					filter.setShopId(shop.getId());
				}
			}
			filter.setBankCode(bankCode);
			filter.setBankName(bankName);
			filter.setBankPhone(bankPhone);
			filter.setBankAddress(bankAddress);
			if (status != null) {
				ActiveType st = ActiveType.parseValue(status);
				filter.setStatus(st);
			}
			ObjectVO<BankVO> voBank = bankMgr.getListBank(null, filter);
			if (voBank != null) {
				//result.put("total", lstReasonGroup.getkPaging().getTotalRows());
				result.put("rows", voBank.getLstObject());
			} else {
				//result.put("total", 0);
				result.put("rows", new ArrayList<BankVO>());
			}
		} catch (BusinessException e) {
			//			LogUtility.logError(e, "BankCatalogAction.search()"+ e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BankCatalogAction.search()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Save or update. them moi va cap nhat ngan hang
	 *
	 * @return the string
	 * @author vuongmq
	 * @since 24/09/2014
	 */
	public String saveOrUpdate() {

		resetToken(result);
		try {
			if (currentUser.getUserName() != null) {
				errMsg = ValidateUtil.validateField(bankCode, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(bankName, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(bankAccount, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(bankPhone, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(bankAddress, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				if (bankId != null && bankId > 0) {
					// cap nhat ngan hang
					Bank bankUpdate = bankMgr.getBankById(bankId);
					if (bankUpdate != null) {
						/** khong cho sua don vi, ma ngan hang **/
						bankUpdate.setBankName(bankName);
						bankUpdate.setBankAccount(bankAccount);
						bankUpdate.setBankPhone(bankPhone);
						bankUpdate.setBankAddress(bankAddress);
						if (status != null) {
							ActiveType st = ActiveType.parseValue(status);
							bankUpdate.setStatus(st);
						}
						bankUpdate.setUpdateDate(DateUtil.now());
						bankUpdate.setUpdateUser(currentUser.getUserName());
						bankMgr.updateBank(bankUpdate, getLogInfoVO());
					}
				} else {
					// them moi ngan hang
					Shop shop = null;
					if (!StringUtil.isNullOrEmpty(shopCode)) {
						shop = shopMgr.getShopByCode(shopCode);
						if (shop == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, null, "catalog.bank.shopCode")); // Don vi khong ton tai trong he thong
							return JSON;
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.bank.shopCode")); // Ban chua nhap gia tri cho truong Ma ngan hang
						return JSON;
					}
					Bank bankNew = null;
					if (!StringUtil.isNullOrEmpty(bankCode)) {
						bankNew = bankMgr.getBankByCode(bankCode);
						if (bankNew != null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.bank.code")); // Ma ngan hang da ton tai trong he thong
							return JSON;
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.bank.code")); // Ban chua nhap gia tri cho truong Ma ngan hang
						return JSON;
					}
					bankNew = new Bank();
					bankNew.setBankCode(bankCode);
					bankNew.setBankName(bankName);
					bankNew.setBankAccount(bankAccount);
					bankNew.setBankPhone(bankPhone);
					bankNew.setBankAddress(bankAddress);
					if (status != null) {
						ActiveType st = ActiveType.parseValue(status);
						bankNew.setStatus(st);
					}
					bankNew.setShop(shop);
					bankNew.setCreateDate(DateUtil.now());
					bankNew.setCreateUser(currentUser.getUserName());
					bankNew = bankMgr.createBank(bankNew, getLogInfoVO());
					if (bankNew == null) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("catalog.bank.err.createbank"));
						return JSON;
					}
				}
			} else {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("catalog.bank.err.user"));
			}
		} catch (BusinessException e) {
			//			LogUtility.logError(e, "BankCatalogAction.saveOrUpdate()"+ e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BankCatalogAction.saveOrUpdate()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getBankPhone() {
		return bankPhone;
	}

	public void setBankPhone(String bankPhone) {
		this.bankPhone = bankPhone;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

}

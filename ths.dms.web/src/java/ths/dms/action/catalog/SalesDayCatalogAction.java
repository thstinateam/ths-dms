package ths.dms.action.catalog;

import java.io.File;
import java.util.Calendar;

import ths.dms.core.business.SaleDayMgr;
import ths.dms.core.entities.SaleDays;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;

public class SalesDayCatalogAction extends AbstractAction{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	private File excelFile;
	private String excelFileContentType;
	private Integer status;
	private Long salesDayId;
	private SaleDayMgr saleDayMgr;
	private Integer fromYear;
	private Integer toYear;
	private Integer january;
	private Integer february;
	private Integer march;
	private Integer april;
	private Integer may;
	private Integer june;
	private Integer july;
	private Integer august;
	private Integer september;
	private Integer october;
	private Integer november;
	private Integer december;
	private String year;
	private SaleDays saleDays;


	public SaleDays getSaleDays() {
		return saleDays;
	}

	public void setSaleDays(SaleDays saleDays) {
		this.saleDays = saleDays;
	}

	public Integer getJanuary() {
		return january;
	}

	public void setJanuary(Integer january) {
		this.january = january;
	}

	public Integer getFebruary() {
		return february;
	}

	public void setFebruary(Integer february) {
		this.february = february;
	}

	public Integer getMarch() {
		return march;
	}

	public void setMarch(Integer march) {
		this.march = march;
	}

	public Integer getApril() {
		return april;
	}

	public void setApril(Integer april) {
		this.april = april;
	}

	public Integer getMay() {
		return may;
	}

	public void setMay(Integer may) {
		this.may = may;
	}

	public Integer getJune() {
		return june;
	}

	public void setJune(Integer june) {
		this.june = june;
	}

	public Integer getJuly() {
		return july;
	}

	public void setJuly(Integer july) {
		this.july = july;
	}

	public Integer getAugust() {
		return august;
	}

	public void setAugust(Integer august) {
		this.august = august;
	}

	public Integer getSeptember() {
		return september;
	}

	public void setSeptember(Integer september) {
		this.september = september;
	}

	public Integer getOctober() {
		return october;
	}

	public void setOctober(Integer october) {
		this.october = october;
	}

	public Integer getNovember() {
		return november;
	}

	public void setNovember(Integer november) {
		this.november = november;
	}

	public Integer getDecember() {
		return december;
	}

	public void setDecember(Integer december) {
		this.december = december;
	}

	public Integer getFromYear() {
		return fromYear;
	}

	public void setFromYear(Integer fromYear) {
		this.fromYear = fromYear;
	}

	public Integer getToYear() {
		return toYear;
	}

	public void setToYear(Integer toYear) {
		this.toYear = toYear;
	}

	public Long getSalesDayId() {
		return salesDayId;
	}

	public void setSalesDayId(Long salesDayId) {
		this.salesDayId = salesDayId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}


	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    saleDayMgr = (SaleDayMgr)context.getBean("saleDayMgr");
	}
	
	@Override
	public String execute() {	
		resetToken(result);
	    return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012 
	 */
	public String search(){
	    
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<SaleDays> kPaging = new KPaging<SaleDays>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			ObjectVO<SaleDays> lstSaleDays = saleDayMgr.getListSaleDay(kPaging, fromYear, toYear, ActiveType.parseValue(status));
			if(lstSaleDays!= null){
				result.put("total", lstSaleDays.getkPaging().getTotalRows());
				result.put("rows", lstSaleDays.getLstObject());		
			}			
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012 
	 */
	public String saveOrUpdate(){
	    
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
				saleDays = saleDayMgr.getSaleDayById(salesDayId);
			    if(salesDayId!= null && salesDayId > 0){
					if(saleDays!= null){
						if(!StringUtil.isNullOrEmpty(year)){
//							saleDays.setYear(year);
							/*if(status != null && status != ConstantManager.NOT_STATUS){
								saleDays.setStatus(ActiveType.parseValue(status));
							}*/
							if(january != null && january <= dayInMonth(Integer.valueOf(year), Calendar.JANUARY)){
								saleDays.setT1(january);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","1");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(february != null && february <= dayInMonth(Integer.valueOf(year), Calendar.FEBRUARY)){
								saleDays.setT2(february);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","2");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(march != null && march <= dayInMonth(Integer.valueOf(year), Calendar.MARCH)){
								saleDays.setT3(march);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","3");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(april != null && april <= dayInMonth(Integer.valueOf(year), Calendar.APRIL)){
								saleDays.setT4(april);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","4");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(may != null && may <= dayInMonth(Integer.valueOf(year), Calendar.MAY)){
								saleDays.setT5(may);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","5");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(june != null && june <= dayInMonth(Integer.valueOf(year), Calendar.JUNE)){
								saleDays.setT6(june);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","6");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(july != null && july <= dayInMonth(Integer.valueOf(year), Calendar.JULY)){
								saleDays.setT7(july);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","7");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(august != null && august <= dayInMonth(Integer.valueOf(year), Calendar.AUGUST)){
								saleDays.setT8(august);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","8");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(september != null && september <= dayInMonth(Integer.valueOf(year), Calendar.SEPTEMBER)){
								saleDays.setT9(september);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","9");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(october != null && october <= dayInMonth(Integer.valueOf(year), Calendar.OCTOBER)){
								saleDays.setT10(october);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","10");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(november != null && november <= dayInMonth(Integer.valueOf(year), Calendar.NOVEMBER)){
								saleDays.setT11(november);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","11");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(december != null && december <= dayInMonth(Integer.valueOf(year), Calendar.DECEMBER)){
								saleDays.setT12(december);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","12");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							//saleDays.setCreateUser(currentUser.getUserName());
							saleDayMgr.updateSaleDay(saleDays,getLogInfoVO());
							error = false;
						}else{
							errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required",
									Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.year.code"));
						}
					}    
			    } else {
			    	saleDays = new SaleDays();
					if(!StringUtil.isNullOrEmpty(year)){
						SaleDays temp = saleDayMgr.getSaleDayByYear(Integer.valueOf(year));
						if(temp != null){
							errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.code.exist",
									Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.year.code"),year);
						}else{
							//saleDays.setYear(year);
							/*if(status != null && status != ConstantManager.NOT_STATUS){
								saleDays.setStatus(ActiveType.parseValue(status));
							}*/
							if(january != null && january <= dayInMonth(Integer.valueOf(year), Calendar.JANUARY)){
								saleDays.setT1(january);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","1");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(february != null && february <= dayInMonth(Integer.valueOf(year), Calendar.FEBRUARY)){
								saleDays.setT2(february);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","2");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(march != null && march <= dayInMonth(Integer.valueOf(year), Calendar.MARCH)){
								saleDays.setT3(march);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","3");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(april != null && april <= dayInMonth(Integer.valueOf(year), Calendar.APRIL)){
								saleDays.setT4(april);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","4");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(may != null && may <= dayInMonth(Integer.valueOf(year), Calendar.MAY)){
								saleDays.setT5(may);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","5");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(june != null && june <= dayInMonth(Integer.valueOf(year), Calendar.JUNE)){
								saleDays.setT6(june);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","6");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(july != null && july <= dayInMonth(Integer.valueOf(year), Calendar.JULY)){
								saleDays.setT7(july);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","7");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(august != null && august <= dayInMonth(Integer.valueOf(year), Calendar.AUGUST)){
								saleDays.setT8(august);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","8");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(september != null && september <= dayInMonth(Integer.valueOf(year), Calendar.SEPTEMBER)){
								saleDays.setT9(september);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","9");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(october != null && october <= dayInMonth(Integer.valueOf(year), Calendar.OCTOBER)){
								saleDays.setT10(october);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","10");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(november != null && november <= dayInMonth(Integer.valueOf(year), Calendar.NOVEMBER)){
								saleDays.setT11(november);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","11");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							if(december != null && december <= dayInMonth(Integer.valueOf(year), Calendar.DECEMBER)){
								saleDays.setT12(december);
							}else{
								errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.day.num","12");
								result.put(ERROR, error);
							    result.put("errMsg", errMsg);	    
							    return JSON;
							}
							//saleDays.setCreateUser(currentUser.getUserName());
							saleDayMgr.createSaleDay(saleDays,getLogInfoVO());
							error = false;
						}
					}else{
						errMsg=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required",
								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.year.code"));
					}
				}
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}		
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}	
	public String deleteSalesDay(){
	    
	    resetToken(result);
	    boolean error = true;
	    if(currentUser!= null){
			try{
				if(salesDayId != null && salesDayId > 0){
					saleDays = saleDayMgr.getSaleDayById(salesDayId);
					if(saleDays!= null){
						//saleDays.setStatus(ActiveType.DELETED);
						saleDayMgr.updateSaleDay(saleDays,getLogInfoVO());
						error = false;
					}
				}
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}
	    }
	    result.put(ERROR, error);
	    return JSON;
	}
	public String viewDetail(){
		resetToken(result);
		try {
			if(salesDayId != null && salesDayId > 0){
				saleDays = saleDayMgr.getSaleDayById(salesDayId);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return LIST;
	}
	private Integer dayInMonth(Integer year,Integer month){
		Calendar calendar = Calendar.getInstance();
		int date = 1;
		calendar.set(year, month, date);
		int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return days;
	}
}

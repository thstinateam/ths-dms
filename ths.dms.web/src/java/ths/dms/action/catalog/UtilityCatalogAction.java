package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.StockManagerMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopLock;
import ths.dms.core.entities.ShopLockLog;
import ths.dms.core.entities.ShopLockReleasedLog;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ErrorTypeShopLockLog;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.StatusShopLockLog;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.StaffFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopLockLogVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.function.common.PkgFuncGetCloseDayErrorMsg;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;
import viettel.passport.client.ShopToken;


// TODO: Auto-generated Javadoc
/**
 * The Class UtilityCatalogAction.
 */
public class UtilityCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	private static final String SHOPLOCKLOG = "SHOPLOCKLOG";
	private static final int ZEZO = 0;

	private ExceptionDayMgr exceptionDayMgr;
	private StockMgr stockMgr;
	private StockManagerMgr stockManagerMgr;

	/** The date lock. */
	private String dateLock;
	private Integer iApParamName = 0;
	private List<Long> lstShopId;

	private Long shopId;
	private String lstShopIdStr;
	protected String lockDate;


	/** The next date. */
	private String lockDateStr;
	private String nextDate;
	private Shop shop = null;
	private StockTrans stockTrans =null;
	private String startDate;
	private final String SYS_MAXDAY_APPROVE = "SYS_MAXDAY_APPROVE";
	
	private Shop shopFilter; 
	private String shopCodeFilter;//shop don vi
	private List<Long> lstStaffLock;
	private int vanLockStatus;
	
	
	private String shopCode;
	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		staff = getStaffByCurrentUser();
		exceptionDayMgr = (ExceptionDayMgr) context.getBean("exceptionDayMgr");
		stockMgr = (StockMgr)context.getBean("stockMgr");
		shop = staff.getShop();
		stockManagerMgr = (StockManagerMgr) context.getBean("stockManagerMgr");
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		try {
			ShopToken sh = currentUser.getShopRoot();
			if (sh != null) {
				shopId = sh.getShopId();
				Date appDate = shopLockMgr.getApplicationDate(sh.getShopId());
				nextDate = DateUtil.toDateSimpleFormatString(DateUtil.addDate(appDate, 1));
				Date lockDate = shopLockMgr.getLockedDay(sh.getShopId());
				dateLock = DateUtil.toDateSimpleFormatString(lockDate);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		resetToken(result);
		return SUCCESS;
	}
	/**
	 * @author tungmt - longnh15 merge from vnmso
	 * @since 13/04/2015 - 03/07/2015
	 * @return 2 ngay: ngay chot va ngay trien khai(min lockDate)
	 */
	public String getLockDateByShop() {
		try {
			if (Boolean.TRUE.equals(checkShopInOrgAccessById(shopId))) {
				Date minLockDate = shopLockMgr.getMinLockDate(shopId);
				Date appDate = shopLockMgr.getApplicationDate(shopId);
				Date lockDate = shopLockMgr.getLockedDay(shopId);
				if (minLockDate != null && minLockDate instanceof Date) {
					minLockDate = DateUtils.addDays((Date)minLockDate, 1);
					result.put("minLockDate", DateUtil.toDateSimpleFormatString(minLockDate));
				}
				if (appDate != null) {
					result.put("appDate", DateUtil.toDateSimpleFormatString(appDate));
				}
				if (lockDate != null) {
					result.put("lockDate", DateUtil.toDateSimpleFormatString(lockDate));
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	/**	
	 *
	 * @return the string
	 * @throws Exception the exception
	 * @author hoanv25
	 * @since  Nov 28,2014	
	 */
	public String lockStockStaffChange(){
		resetToken(result);
		boolean error = true;
		String errMsg = "";

		try {
			shopFilter = shopMgr.getShopByCode(shopCodeFilter);
			if (shop == null) {
				errMsg = R.getResource("common.cms.shop.islevel5.cms.undefined");
			}
			if (lstStaffLock == null) {
				result.put(ERROR, error);
				result.put("errMsg", R.getResource("cmn.vansale.close.day.select.is.null"));
				return JSON;
			}
			String errorLines = "";
			for (int i = 0; i < lstStaffLock.size(); ++i) {
				staff = staffMgr.getStaffById(lstStaffLock.get(i));
				if (staff != null && shop.getId() != null) { //&& shop.getId().equals(staff.getShop().getId())
					StockLock filter = new StockLock();
					filter.setShop(shopFilter);
					filter.setStaff(staff);
					if (vanLockStatus == 1) {
						filter.setVanLock(ActiveType.RUNNING);
					} else {
						filter.setVanLock(ActiveType.STOPPED);
					}
					StockLock stockLock = stockMgr.getStockLockByFilter(filter);
					Date sysDate = commonMgr.getSysDate();

					//Kiem tra dieu kien xuat kho: con don chua duyet cap nhat kho trang thai <>3
					boolean stockTransExist = false;
					if (vanLockStatus == 1) {
						stockTransExist = stockManagerMgr.checkExistsGOStockTransNotApproves(staff.getId(), "DP");
						if (stockTransExist) {
							errorLines = errorLines + R.getResource("staff.belong.exist.DP.not.approved", staff.getStaffCode()) + " | ";
						}
					} else {
						stockTransExist = stockManagerMgr.checkExistsGOStockTransNotApproves(staff.getId(), "GO");
						if (stockTransExist) {
							errorLines = errorLines + R.getResource("staff.belong.exist.GO.not.approved", staff.getStaffCode()) + " | ";
						}
					}
					//Kiem tra trong thá»�i gian delay váº«n cĂ²n Ä‘Æ¡n hĂ ng
					boolean saleOrderExist = false;
					if (vanLockStatus == 1) {
						ApParam aTmp = apParamMgr.getApParamByCode("VANSALE_SYNC_DATE", ApParamType.VANSALE);//"VANSALE"
						int syncDate = Integer.parseInt(aTmp.getValue());
						saleOrderExist = saleOrderMgr.checkOrderInDelay(staff.getId(), syncDate);
						if (saleOrderExist) {
							errorLines = errorLines + R.getResource("staff.belong.order.in.delay", staff.getStaffCode(), syncDate) + " | ";
						}
					}
					if (!stockTransExist && !saleOrderExist) {
						if (stockLock != null) {
							if (vanLockStatus == 1) {
								if (stockLock.getVanLock().equals(ActiveType.STOPPED)) {
									stockLock.setVanLock(ActiveType.RUNNING);
								}
							} else {
								if (stockLock.getVanLock().equals(ActiveType.RUNNING)) {
									stockLock.setVanLock(ActiveType.STOPPED);
								}
							}
							stockLock.setUpdateUser(getCurrentUser().getUserName());
							stockLock.setUpdateDate(sysDate);
							stockMgr.updateStockLock(stockLock);
						} else {
							StockLock stl = new StockLock();
							if (vanLockStatus == 1) {
								if (stl.getVanLock().equals(ActiveType.STOPPED)) {
									stl.setVanLock(ActiveType.RUNNING);
								}
							} else {
								if (stl.getVanLock().equals(ActiveType.RUNNING)) {
									stl.setVanLock(ActiveType.STOPPED);
								}
							}
							stl.setStaff(staff);
							stl.setShop(shopFilter);
							stl.setCreateDate(sysDate);
							stl.setCreateUser(getCurrentUser().getUserName());
							stockMgr.createStockLock(stl);
						}
					}
				}
				errMsg = errorLines;
			}
			if ("".equals(errMsg)) {
				error = false;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put(ERROR, error);
		result.put("errMsg", errMsg);
		return JSON;
	}
	/**
	 * Search lock.
	 * @author hoanv25 - longnh merge code form vnm so - 03/07/2015
	 * @since 28/11/2014
	 */
	public String searchLock(){
		result.put("total", 0);
		result.put("rows", new ArrayList<StaffVO>());
		try {			
			if(currentUser != null && currentUser.getUserName() != null){
				shopFilter = shopMgr.getShopByCode(shopCodeFilter);
//				staff = staffMgr.getStaffByCode(currentUser.getUserName());
//				if(staff != null && staff.getShop() != null){
//					shop = staff.getShop();	
//					shopId = shop.getId();
//					shopCode = shop.getShopCode();
//				}
			}
			KPaging<StaffVO> kPaging = new KPaging<StaffVO>();		
			kPaging.setPage(page-1);
			kPaging.setPageSize(max);
			Date startTm = null;
			if (!StringUtil.isNullOrEmpty(startDate)) {
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(startDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.managetrans.search.startdate"), null);
				if (StringUtil.isNullOrEmpty(checkMessage)) {
					startTm = DateUtil.parse(startDate, ConstantManager.FULL_DATE_FORMAT);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", checkMessage);
					return ERROR;
				}
			}		
			StaffFilter filter = new StaffFilter();
			filter.setStrListUserId(getStrListUserId());
			filter.setkPagingStockVO(kPaging);
			if (startTm != null) {
				filter.setStartTemp(startTm);
			}
			
			ObjectVO<StaffVO> lstStockTrans = null;
			filter.setShopId(shopFilter.getId());
			lstStockTrans = shopLockMgr.getListStockVOFilter(filter);
			if (lstStockTrans != null) {
				result.put(ERROR, false);
				result.put("total", lstStockTrans.getkPaging().getTotalRows());
				result.put("rows", lstStockTrans.getLstObject());
			}	
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}
	/**
	 * Closed day.
	 * 
	 * @return the string
	 * @throws Exception
	 *             the exception
	 * @author hunglm16
	 * @since JUNE 3,2014
	 * @description Load page view Locked Stock Staff
	 */
	/*
	 * public String lockStockStaffChange(){ // resetToken(result); boolean
	 * error = true; String errMsg = ""; try{ if(shop==null){ //errMsg =
	 * "KhĂ´ng xĂ¡c Ä‘á»‹nh Ä‘Æ°á»£c quyá»�n cá»§a NPP"; }else
	 * if(!StringUtil.isNullOrEmpty(staffCode)){ Staff staff =
	 * staffMgr.getStaffByInfoBasic(staffCode, shop.getId(),
	 * ActiveType.RUNNING); if(staff !=null &&
	 * !StringUtil.isNullOrEmpty(staff.getStaffCode()) &&
	 * shop.getId().equals(staff.getShop().getId())){ StockLock stockLock =
	 * stockMgr.getStockLock(shop.getId(), staff.getId()); if(stockLock!=null){
	 * stockLock.setVanLock(ActiveType.STOPPED);
	 * stockLock.setUpdateUser(getCurrentUser().getUserName());
	 * stockMgr.updateStockLock(stockLock); error = false; }else{ //Them moi
	 * STOCK_LOCK stockLock = new StockLock(); stockLock.setStaff(staff);
	 * stockLock.setShop(shop); stockLock.setVanLock(ActiveType.STOPPED);
	 * stockLock.setCreateDate(new Date());
	 * stockLock.setCreateUser(getCurrentUser().getUserName());
	 * stockMgr.insertStockLock(stockLock); error = false; } }else{ //errMsg =
	 * "NhĂ¢n viĂªn khĂ´ng thuá»™c NPP"; } }else{ //errMsg =
	 * "MĂ£ nhĂ¢n viĂªn khĂ´ng há»£p lá»‡"; } }catch (Exception e) {
	 * LogUtility.logError(e, e.getMessage()); errMsg =
	 * ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM); } result.put(ERROR,
	 * error); result.put("errMsg", errMsg); return JSON; }
	 */

	/**
	 * @author hunglm16
	 * @since JUNE 3,2014
	 * @description Load page view Locked Stock Staff
	 * */
	/*
	 * public String lockedStockStaff(){ //HIEN THI TRANG KHOA KHO NHAN VIEN
	 * try{ ObjectVO<Staff> staffVO = staffMgr.getListPreAndVanStaff(null, null,
	 * null, shop.getId(),ActiveType.RUNNING,
	 * Arrays.asList(StaffObjectType.NVBH, StaffObjectType.NVVS));
	 * if(staffVO!=null && staffVO.getLstObject().size()>0){ listSaleStaff =
	 * staffVO.getLstObject(); if(listSaleStaff!=null &&
	 * listSaleStaff.size()>0){ staffCodeFirt =
	 * listSaleStaff.get(0).getStaffCode(); //List<Long> lstStaffId = new
	 * ArrayList<Long>(); //for(Staff item:listSaleStaff){
	 * //lstStaffId.add(item.getId()); //} //lstStockLock =
	 * stockMgr.getListStockLockByListStaffId(lstStaffId, shop.getId()); } }
	 * }catch (Exception e) { LogUtility.logError(e, e.getMessage()); } return
	 * SUCCESS; }
	 */

	/**
	 * @author hunglm16
	 * @since JUNE 12,2014
	 * @description Get Stock Lock By StaffCode
	 * */
	/*
	 * public String getLockedStockStaffCode(){ StockLock stockLock = new
	 * StockLock(); try{ if(StringUtil.isNullOrEmpty(staffCode)){
	 * result.put("stockLock", stockLock); return JSON; } stockLock =
	 * stockMgr.getStockLockByStaffCode(staffCode, shop.getId());
	 * }catch(Exception e){ LogUtility.logError(e, e.getMessage()); }
	 * result.put("stockLock", stockLock); return JSON; }
	 */

	/**
	 * Chot ngay
	 * 
	 * @since
	 * 
	 * @modify hunglm16
	 * @since 11/09/2015
	 * @description bo sung phan quyen va chot ngay tu dong
	 */
	public String closedDay() throws Exception {
		resetToken(result);
		try {
			result.put(ERROR, false);
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
				//Kiem tra phan quyen du lieu
				if (!checkShopInOrgAccessById(shop.getId())) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.cms.shop.undefined.ex.param", shop.getShopCode()));
					return JSON;
				}
			} else {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			if (StringUtil.isNullOrEmpty(lockDateStr)) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.date.lock.day.compare.undefined", shop.getShopCode()));
				return JSON;
			}
			Date lockDayUser = DateUtil.parse(lockDateStr, DateUtil.DATE_FORMAT_DDMMYYYY);
			if (lockDayUser == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("common.date.lock.day.compare.undefined", shop.getShopCode()));
				return JSON;
			}

			//lay so don hang chua xu ly
			if (shop != null) {
				Date sysDate = commonMgr.getSysDate();
				Date lockDate = shopLockMgr.getApplicationDate(shop.getId());
				if (lockDate == null) {
					lockDate = sysDate;
				}

				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(sysDate);

				Calendar cal = Calendar.getInstance();
				cal.setTime(lockDate);

				cal1.set(Calendar.DATE, cal.get(Calendar.DATE));
				cal1.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
				cal1.set(Calendar.DAY_OF_WEEK_IN_MONTH, cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
				cal1.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR));
				cal1.set(Calendar.MONTH, cal.get(Calendar.MONTH));
				cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));

				lockDate = cal1.getTime();

				if (DateUtil.compareDateWithoutTime(lockDayUser, lockDate) != 0) {
					//Them yeu cau moi: so sanh ve mat hien thi va gio trong he thong
					result.put("SHOPLOCKLOG", StatusType.ACTIVE.getValue());
					result.put(ERROR, true);
					result.put("errMsg1", R.getResource("common.date.lock.day.compare.system.error", shop.getShopCode()));
					return JSON;
				}

				//Kiem tra tien trinh co thuc hien hay khong
				String errMsg1 = ckeckShopLockLogByShopAndLockDate(shop, lockDate);
				if (!StringUtil.isNullOrEmpty(errMsg1)) {
					result.put("SHOPLOCKLOG", StatusType.ACTIVE.getValue());
					result.put("errMsg1", errMsg1);
					result.put(ERROR, true);
					return JSON;
				}

				Date currentShopLock = shopLockMgr.getLockedDay(shop.getId());
				if (currentShopLock != null) {
					if (DateUtil.compareDateWithoutTime(currentShopLock, sysDate) >= 0) {
						result.put("SHOPLOCKLOG", StatusType.ACTIVE.getValue());
						result.put(ERROR, true);
						result.put("errMsg1", R.getResource("shop.locked.err.new", shop.getShopCode()));
						return JSON;
					}
				}

				//Thuc hien kiem tra loi don hang neu co
				List<PkgFuncGetCloseDayErrorMsg> lstErrorCloseDay = shopLockMgr.getErrorForCloseDay(shop.getId());
				if (lstErrorCloseDay != null && !lstErrorCloseDay.isEmpty()) {
					result.put(ERROR, true);
					result.put("lstErrorCloseDay", lstErrorCloseDay);
					return JSON;

				}
				ShopLock shopLock = new ShopLock();
				shopLock.setShop(shop);
				shopLock.setLockDate(lockDate);
				shopLock.setCreateDate(sysDate);
				shopLock.setCreateUser(currentUser.getUserName());
				shopLock.setShopLock(StatusType.ACTIVE.getValue());
				shopLock = shopLockMgr.createShopLock(shopLock);
				if (shopLock != null && iApParamName != null) {
					//chay tien trinh tong hop
					shopLockMgr.runProcess(lockDate, shop.getId());
					//chay tien trinh cap nhat passed
					shopLockMgr.updatePassedByClosingDay(shop.getId(), lockDate, currentUser.getUserName());
					//Cap nhat lai Lich su chot ngay
					ShopLockLog shopLockLog = new ShopLockLog();
					shopLockLog.setCreateUser(currentUser.getUserName());
					shopLockLog.setCreateDate(sysDate);
					//shopLockLog.setErrorType(ErrorTypeShopLockLog.NONE_ERROR.getValue());
					shopLockLog.setShopId(shop.getId());
					//shopLockLog.setStatus(StatusShopLockLog.PENDING.getValue());
					shopLockLog.setWorkingDate(lockDate);
					shopLockLog.setStatus(StatusShopLockLog.DONE.getValue());
					shopLockLog.setErrorType(null);
					shopLockLog.setQuantity(0);
					shopLockLog.setQuantitySuccess(0);
					shopLockLog.setPassed(StatusType.ACTIVE.getValue());
					commonMgr.createEntity(shopLockLog);
				} else {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
				}
				Date nextDate = shopLockMgr.getNextLockedDay(shop.getId());
				result.put("lockdate", DateUtil.toDateSimpleFormatString(lockDate));
				result.put("nextdate", DateUtil.toDateSimpleFormatString(nextDate));
				result.put("nextdate1", DateUtil.toDateSimpleFormatString(DateUtil.addDate(nextDate, 1)));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.catalog.UtilityCatalogAction.closedDay", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Cap nhat thong tin chot ngay tu dong
	 * 
	 * @author hunglm16
	 * @since September 03, 2015
	 * */
	public String changeShopLockLog() {
		resetToken(result);
		try {
			result.put(ERROR, false);
			Shop shop = null;
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				shop = shopMgr.getShopByCode(shopCode);
				//Kiem tra phan quyen du lieu
				if (!checkShopInOrgAccessById(shop.getId())) {
					result.put(ERROR, true);
					result.put("errMsg", R.getResource("common.cms.shop.undefined.ex.param", shop.getShopCode()));
					return JSON;
				}
			} else {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			Date sysDate = commonMgr.getSysDate();
			Date lockDate = shopLockMgr.getApplicationDate(shop.getId());
			if (lockDate == null) {
				lockDate = sysDate;
			} else {
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(sysDate);
				Calendar cal = Calendar.getInstance();
				cal.setTime(lockDate);
				cal1.set(Calendar.DATE, cal.get(Calendar.DATE));
				cal1.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
				cal1.set(Calendar.DAY_OF_WEEK_IN_MONTH, cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
				cal1.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR));
				cal1.set(Calendar.MONTH, cal.get(Calendar.MONTH));
				cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));

				lockDate = cal1.getTime();
			}
			//Kiem tra tien trinh co thuc hien hay khong
			String errMsg1 = ckeckShopLockLogByShopAndLockDate(shop, lockDate);
			if (!StringUtil.isNullOrEmpty(errMsg1)) {
				result.put("SHOPLOCKLOG", StatusType.ACTIVE.getValue());
				result.put("errMsg1", errMsg1);
				result.put(ERROR, true);
				return JSON;
			}
			ShopLockLog shopLockLog = new ShopLockLog();
			shopLockLog.setCreateUser(currentUser.getUserName());
			shopLockLog.setCreateDate(sysDate);
			shopLockLog.setErrorType(ErrorTypeShopLockLog.NONE_ERROR.getValue());
			shopLockLog.setShopId(shop.getId());
			shopLockLog.setStatus(StatusShopLockLog.PENDING.getValue());
			shopLockLog.setPassed(StatusType.INACTIVE.getValue());
			shopLockLog.setWorkingDate(lockDate);
			commonMgr.createEntity(shopLockLog);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.catalog.UtilityCatalogAction.changeShopLockLog", createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	private String ckeckShopLockLogByShopAndLockDate (Shop shop, Date lockDate) {
		//Kiem tra tien trinh co thuc hien hay khong
		String errMsg = "";
		try {
			List<ShopLockLogVO> lstShopLockLogVO = shopLockMgr.getShopLockLogVOByShopAndLockDate(shop.getId(), lockDate);
			if (lstShopLockLogVO != null && !lstShopLockLogVO.isEmpty()) {
				ShopLockLogVO itemError = lstShopLockLogVO.get(0);
				if (StatusShopLockLog.RUNNING.getValue().equals(itemError.getStatus())) {
					//Tien trinh dang hoat dong
					errMsg = R.getResource("shop.locked.err.lock.log.is.running", shop.getShopCode());
				} else if (StatusShopLockLog.PENDING.getValue().equals(itemError.getStatus())) {
					//Dang doi Tien trinh xu ly
					errMsg = R.getResource("shop.locked.err.lock.log.is.panding", shop.getShopCode());
				}
			}
		} catch (Exception ex) {
			LogUtility.logErrorStandard(ex, "ckeckShopLockLogByShopAndLockDate", createLogErrorStandard(actionStartTime));
		}
		return errMsg;
	}
	
	/**
	 * @author tungmt
	 * @since 12/5/2015
	 * @param shopId
	 * @return Ngay
	 */
	public Date getMaxDateApprovedOrder(Long shopId, Date startDateTemp) {
//		Date startDateTemp = DateUtil.now();
		try {
			List<ShopParam> lstPr = shopMgr.getConfigShopParam(shopId, ConstantManager.SYS_MAXDAY_APPROVE);
			iApParamName = 0;
			if (lstPr != null && lstPr.size() > 0) {
				try {
					iApParamName = Integer.valueOf(lstPr.get(0).getValue().trim());
				} catch (Exception e1) {
					// pass through
				}
			}
			if (iApParamName > 0) {
				Integer iExcept = 0;
				dayLock = shopLockMgr.getApplicationDate(shopId);
				startDateTemp = DateUtil.moveDate(DateUtil.parseLockDate(dayLock), iApParamName * (-1), 1);
				Date dateTmp = DateUtil.parseLockDate(dayLock);
				do {
					iExcept = exceptionDayMgr.getNumOfHoliday(startDateTemp, dateTmp, shopId);
					dateTmp = DateUtil.moveDate(startDateTemp, -1, 1);
					startDateTemp = DateUtil.moveDate(startDateTemp, -iExcept, 1);
				} while (iExcept > 0);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return startDateTemp;
	}

	/**
	 * Open lock.
	 * @author phuongvm
	 * @since 07/10/2014
	 * @return the string
	 * @throws Exception the exception
	 */
	public String openLock() throws Exception {
		resetToken(result);
		actionStartTime = DateUtil.now();
		result.put(ERROR, true);
		errMsg = "";
		try {
			Shop sh = null;
			if (currentUser == null || currentUser.getStaffRoot() == null) {
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			if (!checkShopInOrgAccessByCode(shopCode)) {
				result.put(ERR_MSG, R.getResource("common.cms.shop.undefined"));
				return JSON;
			} else {
				sh = shopMgr.getShopByCode(shopCode);
				if (sh == null) {
					result.put(ERR_MSG, R.getResource("catalog.unit.tree.not.exist"));
					return JSON;
				}
			}
			Date sysDate = commonMgr.getSysDate();
			Date nextDate = shopLockMgr.getApplicationDate(sh.getId());
			if (nextDate == null) {
				nextDate = sysDate;
			}
			Date fDate = DateUtil.parse(lockDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			//Kiem tra tien trinh co thuc hien hay khong
			if (StringUtil.isNullOrEmpty(errMsg) && (StringUtil.isNullOrEmpty(lockDate) || fDate == null)) {
				errMsg = R.getResource("shop.open.locked.err.undefined");
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ckeckShopLockLogByShopAndLockDate(sh, nextDate);
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = errMsg + R.getResource("shop.locked.err.open.lock.log");
					result.put("SHOPLOCKLOG", StatusType.ACTIVE.getValue());
				}
			}
			// kiem tra ngay mo chot phai nho hon ngay lam viec va bang ngay chot gan nhat
			if (StringUtil.isNullOrEmpty(errMsg)) {
				Date lockDateDB = shopLockMgr.getLockedDay(sh.getId());
				if (DateUtil.compareDateWithoutTime(lockDateDB, fDate) != 0) {
					errMsg = R.getResource("shop.locked.err.open.date.not.equal.date.lock.near");
				}
			}
			// kiem tra trong ngay neu da mo chot roi thi khong duoc chot nua
			if (StringUtil.isNullOrEmpty(errMsg)) {
				ShopLockReleasedLog shopLockReleasedLog = shopLockMgr.getShopLockReleaseLogShopAndReleaseDate(sh.getId(), sysDate);
				if (shopLockReleasedLog != null) {
					errMsg = R.getResource("shop.locked.err.open.date.done.err");
				}
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put(ERR_MSG, errMsg.trim());
				return JSON;
			}
//				List<ShopLock> lst = shopLockMgr.getListShopLock(sh.getId(), fDate);
			ShopLock shopLock = shopLockMgr.getShopLockShopAndLockDate(sh.getId(), fDate);
			if (shopLock != null) {
				Date minLockDate = shopLockMgr.getMinLockDate(sh.getId());
				if (minLockDate != null) {
					if (DateUtil.compareDateWithoutTime(fDate, minLockDate) != 1) {
						result.put(ERR_MSG, R.getResource("shop.open.locked.err"));
						return JSON;
					}
				}
//					shopLockMgr.deleteListShopLock(lst);
				shopLockMgr.deleteShopLockAndOpen(shopLock, currentUser.getStaffRoot().getStaffCode());
				result.put(ERROR, false);
			} else {
				result.put(ERR_MSG, R.getResource("shop.open.max.locked.err"));
				return JSON;
			}
//				result.put("nextdate", DateUtil.toDateSimpleFormatString(nextDate));
			Date appDate = shopLockMgr.getApplicationDate(sh.getId());
			if (appDate != null) {
				result.put("appdate", DateUtil.toDateSimpleFormatString(appDate));
			}
			Date lockDate = shopLockMgr.getLockedDay(sh.getId());
			if (lockDate != null) {
				result.put("lockdate", DateUtil.toDateSimpleFormatString(lockDate));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.UtilityCatalogAction.openLock()"), createLogErrorStandard(actionStartTime));
			result.put(ERR_MSG, ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	public String checkShopLocked() {
		try {
			Shop shop = shopMgr.getShopByCode(shopCode);
			if (shop != null) {
				Date appDate = shopLockMgr.getApplicationDate(shop.getId());
				if (appDate == null) {
					appDate = DateUtil.now();
				}
				result.put("appDate", DateUtil.toDateString(appDate, DateUtil.DATE_FORMAT_DDMMYYYY));
				result.put("nextDate", DateUtil.toDateSimpleFormatString(DateUtil.addDate(appDate, 1)));
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}

		return JSON;
	}
	
	/**	 
	 * Lay danh sach shop theo filter
	 * @author tungmt
	 * @since 23/6/2015
	 * @return
	 */
	public String getListShop() {
		result.put("rows", new ArrayList<Shop>());
		result.put("total", 0);
		result.put("page", page);
		result.put("max", max);
		try {
			if (!StringUtil.isNullOrEmpty(lstShopIdStr)) {
				String[] str = lstShopIdStr.split(",");
				lstShopId = new ArrayList<Long>();
				for (String s : str) {
					lstShopId.add(Long.parseLong(s));
				}
			}
			ShopFilter shopFilter = new ShopFilter();
			KPaging<ShopVO> kPaging = new KPaging<ShopVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			shopFilter.setkPagingVO(kPaging);
			if (lstShopId != null && lstShopId.size() > 0) {
				shopFilter.setLstParentId(lstShopId);
			} else if (currentUser != null && currentUser.getShopRoot() != null) {
				shopFilter.setLstParentId(Arrays.asList(currentUser.getShopRoot().getShopId()));
			} else {
				return PAGE_NOT_PERMISSION;
			}
			shopFilter.setSort(sort);
			shopFilter.setOrder(order);
			shopFilter.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			shopFilter.setRoleId(currentUser.getRoleToken().getRoleId());
			shopFilter.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<ShopVO> vo = shopLockMgr.getListChildShopForCloseDay(shopFilter);
			if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
				result.put("rows", vo.getLstObject());
				result.put("total", vo.getkPaging().getTotalRows());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}


	public String getDateLock() {
		return dateLock;
	}

	/**
	 * Sets the date lock.
	 * 
	 * @param dateLock
	 *            the new date lock
	 */
	public void setDateLock(String dateLock) {
		this.dateLock = dateLock;
	}

	/**
	 * Gets the next date.
	 * 
	 * @return the next date
	 */
	public String getNextDate() {
		return nextDate;
	}

	/**
	 * Sets the next date.
	 * 
	 * @param nextDate
	 *            the new next date
	 */
	public void setNextDate(String nextDate) {
		this.nextDate = nextDate;
	}

	public Integer getiApParamName() {
		return iApParamName;
	}

	public void setiApParamName(Integer iApParamName) {
		this.iApParamName = iApParamName;
	}

	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getLstShopIdStr() {
		return lstShopIdStr;
	}

	public void setLstShopIdStr(String lstShopIdStr) {
		this.lstShopIdStr = lstShopIdStr;
	}
	
	public String getShopCodeFilter() {
		return shopCodeFilter;
	}

	public void setShopCodeFilter(String shopCodeFilter) {
		this.shopCodeFilter = shopCodeFilter;
	}

	public List<Long> getLstStaffLock() {
		return lstStaffLock;
	}

	public void setLstStaffLock(List<Long> lstStaffLock) {
		this.lstStaffLock = lstStaffLock;
	}
	public int getVanLockStatus() {
		return vanLockStatus;
	}

	public void setVanLockStatus(int vanLockStatus) {
		this.vanLockStatus = vanLockStatus;
	}


	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}


	public String getLockDateStr() {
		return lockDateStr;
	}


	public void setLockDateStr(String lockDateStr) {
		this.lockDateStr = lockDateStr;
	}


	public String getLockDate() {
		return lockDate;
	}


	public void setLockDate(String lockDate) {
		this.lockDate = lockDate;
	}
	
}
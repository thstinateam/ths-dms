package ths.dms.action.catalog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vn.kunkun.rd.ImageInfo;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.LogMgr;
import ths.dms.core.business.ProductAttributeMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductIntroduction;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.MediaType;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.ProductInfoObjectType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.ProductAttributeFilter;
import ths.dms.core.entities.vo.CategoryVO;
import ths.dms.core.entities.vo.GeneralProductVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductAttributeVO;
import ths.dms.core.entities.vo.ProductExportVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.ExceptionCode;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.business.common.DynamicAttributeHelper;
import ths.dms.web.business.common.DynamicAttributeValidator;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.BusinessUtils;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.ImageUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.NumberUtil;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.Unicode2English;
import ths.dms.web.utils.ValidateUtil;

// TODO: Auto-generated Javadoc
/**
 * Action phuc vu quan ly san pham
 * 
 * @author
 * @since
 */
public class ProductCatalogAction extends AbstractAction {	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	
	private ProductAttributeMgr productAttributeMgr;
	
	private List<ProductAttributeVO> lstProductAttributes;
	
	/** The start date. */
	private String startDate;
	
	/** The end date. */
	private String endDate;
	
	/** The excel file. */
	private File excelFile;
	
	/** The excel file content type. */
	private String excelFileContentType;
	
	/** The product id. */
	private Long productId;
	
	/** The shop id. */
	private Long shopId;
	
	/** The product mgr. */
	private ProductMgr productMgr;
	

	/** The product code. */
	private String productCode;
	
	/** The product name. */
	private String productName;
	
	
	/**The product short name*/
	private String shortName;
	
	
	/**The list of product type*/
	private List<String> lstBaoBi;
	
	/** The shop code. */
	private String shopCode;
	
	/** The quantity max. */
	private Integer quantityMax;
	
	/**The grid page*/
	private Integer gridPage;
	
	/** The category. */
	private Long category;
	
	/** The subCat. */
	private Long subCat;
	
	/** The product type. */
	private Integer productType;
	
	/** The status. */
	private Integer status;
	
	/** The lst category. */
	private List<ProductInfo> lstCategory;
	
	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;
	
	/** The lst product code origin. */
	private List<Product> lstProductCodeOrigin;

	/** The product. */
	private Product product;
	
	/** The parent product code. */
	private String parentProductCode;
	
	/** The product type code. */
	private String productTypeCode;
	
	/** The lst flavour. */
	private List<ProductInfo> lstFlavour;
	
	/** The bar code. */
	private String barCode;
	
	/** The order index. */
	private Integer orderIndex;
	
	/** The level id. */
	private Long levelId;
	
	/** The flavour id. */
	private Long flavourId;
	
	/** The packing id. */
	private Long packingId;
	
	/** The uom1. */
	private String uom1;
	
	/** The uom2. */
	private String uom2;
	
	/** The convfact. */
	private Integer convfact;
	
	/** The tax id. */
	private String taxId;
	
	/** The parent code. */
	private String parentCode;
	
	/** The type code. */
	private String typeCode;
	
	/** The group id. */
	private Long groupId;
	
	/** The price id. */
	private Long priceId;
	
	/** The department id. */
	private Long departmentId;
	
	/** The p price. */
	private Price pPrice;
	
	/** The price. */
	private BigDecimal price;
	
	/** The net weight. */
	private BigDecimal netWeight;
	
	/** The gross weight. */
	private BigDecimal grossWeight;
	
	private Integer checkLot;
	
	/** The ap param mgr. */
	private ApParamMgr apParamMgr;
	
	/** The lst uom1. */
	private List<ApParam> lstUom1;
	
	/** The lst uom2. */
	private List<ApParam> lstUom2;
	
	private Boolean isExceptZCat;
	
	private Boolean isPriceValid;
	
	private Integer totalMediaItem;
	
	private Integer vnmTotalImage;
	
	private Integer vnmTotalVideo;
	
	private List<String> lstProductCode;
	
	private CommonMgr commonMgr;
	
	/** Loai bao bi sp*/
	private List<Product> lstProductType;

	
	private List<Long> lstCategoryId;
	
	private Integer checkPermission;
	
	private Price price1;
	
	private Price price2;

	/** The expire number. */
	private Integer expireNumber;
	/** expire type */
	private Integer expireType;
	
	/** The day month. */
	private Integer dayMonth;
	
	/** The product info id. */
	private Long productInfoId;
	
	private List<Long> lstProductInfoId;
	
	/** The lst department. */
	private List<ApParam> lstDepartment;
	
	private List<ApParam> lstNgayThangNam;
	
	private List<ApParam> lstCheckLot;
	
	/** The excel type. */
	private Integer excelType;
	
	/** The department code. */
	private String departmentCode;
	
	/** The price not vat. */
	private BigDecimal priceNotVat;
	
	/** The description. */
	private String description;
	
	/** The video file. */
	private File videoFile;
	
	/** The image file. */
	private File imageFile;
	
	/** The introduction id. */
	private Long introductionId;
	
	/** The product introduction. */
	private ProductIntroduction productIntroduction;
	
	/** The media item. */
	private MediaItem mediaItem;
	
	/** The lst media item. */
	private List<MediaItem> lstMediaItem;
	
	/** The lst brand. */
	private List<ProductInfo> lstBrand;
	
	/** The brand id. */
	private Long brandId;
	
	/** The lst ap param code. */
	private List<String> lstApParamCode;
	
	/** The lst ap param name. */
	private List<String> lstApParamName;
	
	/** The ap param code. */
	private String apParamCode;
	
	/** The product type text. */
	private String productTypeText;
	
	/** The commission. */
	private String commission;
	
	/** The volumn. */
	private BigDecimal volumn;

	/** The media type. */
	private Integer mediaType;

	/** The media item id. */
	private Long mediaItemId;
	
	/** The num index. */
	private Integer numIndex;
	
	/** The is view product. */
	private Boolean isViewProduct = Configuration.getSavePermission();
	
	/** The lst item. */
	private List<MediaItem> lstItem;

	/** The status s. */
	private String statusS;
	
	/** The lst ap param. */
	private List<ApParam> lstApParam;
	
	/** The actionId. */
	private Long actionId;
	private String productLevelCode;
	private String productLevelName;
	private String productFlavourCode;
	private String productFlavourName;
	
	private String productBrandCode;
	private String productBrandName;
	private String apParamsUOM1Code;
	private String apParamsUOM2Code;
	private String expiryType;
	private List<String> lstcategory;
	private List<Long> listcategory;
	private List<GeneralProductVO> lstProducts;
	
	private List<File> images;
    private List<String> imagesContentType;
    private List<String> imagesFileName;
    
    private List<Long> lstSubCategoryId;
    
	private String q;	
		
	/** The from date. */
	private String fromDate;
	
	/** The to date. */
	private String toDate;
	
	/** The show date. */
	private String showDate;
	
	/** The logMgr. */
	private LogMgr logMgr;	
	

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();				
	    productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	    productMgr = (ProductMgr)context.getBean("productMgr");
	    apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
	    commonMgr = (CommonMgr)context.getBean("commonMgr");
	    productAttributeMgr = (ProductAttributeMgr)context.getBean("productAttributeMgr");
	    dynamicAttributeHelper = (DynamicAttributeHelper) context.getBean("dynamicAttributeHelper");
	}


	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {	
		resetToken(result);
	    try {	    	
			lstDepartment = apParamMgr.getListApParam(ApParamType.PRODUCT_SUBCAT_MAP, ActiveType.RUNNING);
			lstActiveType = apParamMgr.getListActiveType(ApParamType.STATUS_TYPE);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.execute()"), createLogErrorStandard(actionStartTime));
		}
	    return SUCCESS;
	}

	/**
	 * Tim kiem san pham
	 * 
	 * @author lacnv1
	 * @return json
	 * @throws Exception
	 * @since Nov 26, 2013
	 */
	public String searchProducts() throws Exception {
		try {
			result.put("page", page);
			result.put("max", max);
			KPaging<ProductExportVO> paging = new KPaging<ProductExportVO>();
			paging.setPage(page - 1);
			paging.setPageSize(max);
			ActiveType temp = null;
	    	if(status != null && status != ConstantManager.NOT_STATUS && status != ConstantManager.DELETED){
	    		temp = ActiveType.parseValue(status);
	    	}
	    		    	
	    	ProductFilter pf = new ProductFilter();
	    	pf.setkPaging(null);
	    	pf.setProductCode(productCode);
	    	pf.setProductName(productName);
	    	pf.setStatus(temp);
	    	pf.setUom1(uom1);
	    	pf.setLstCategoryId(lstCategoryId);
	    	pf.setLstSubCategoryId(lstSubCategoryId);
	    	pf.setBrandId(brandId);
	    	pf.setFlavourId(flavourId);
	    	pf.setPackingId(packingId);
	    	if(isPriceValid != null){
	    		pf.setIsPriceValid(isPriceValid);
	    	}
	    	if(lstProductCode != null && lstProductCode.size() >0 && !StringUtil.isNullOrEmpty(lstProductCode.get(0))){
	    		lstProductCode.get(0).split(",");
	    		List<String> tmp = new ArrayList<String>(); 
	    		for(String code : lstProductCode.get(0).split(",")){
	    			tmp.add(code);
	    		}
	    		pf.setLstProductCodeSelected(tmp);
	    	}
	    	/**Sort grid*/
	    	pf.setOrder(order);
	    	pf.setSort(sort);
	    	ObjectVO<ProductExportVO> vo = productMgr.getListProductExport(pf, paging);
			if (vo != null && vo.getLstObject().size() > 0) {
				lstNgayThangNam = apParamMgr.getListApParam(ApParamType.NGAY_THANG_NAM, ActiveType.RUNNING);
				List<ProductExportVO> products = vo.getLstObject();
				for (int i = 0, n = products.size(); i < n; i++) {
					ProductExportVO product = products.get(i);
					for (int j = 0, m = lstNgayThangNam.size(); j < m; j++) {
						ApParam apParam = lstNgayThangNam.get(j);
						if (apParam.getValue().equals(product.getExpiryType())) {
							product.setExpiryType(apParam.getApParamName());
							break;
						}
					}
				}
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", products);
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<ProductExportVO>());
			}
		} catch (Exception ex) {
			result.put(ERROR, true);
			result.put("total", 0);
			result.put("rows", new ArrayList<ProductExportVO>());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(ex, ex.getMessage());
		}
		return JSON;
	}

	/**
	 * Checks if is this date valid.
	 *
	 * @param dateToValidate the date to validate
	 * @return true, if is this date valid
	 * @author liemtpt
	 */
	public boolean isThisDateValid(String dateToValidate){
		if(dateToValidate == null){
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		try {
			sdf.parse(dateToValidate);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	/**
	 * Xem chi tiet san pham hoac chuyen trang tao moi.
	 *
	 * @return the string
	 * @author LacNV
	 * @since Nov 21, 2013
	 */
	public String viewDetail() throws Exception{
		resetToken(result);
		try {
			ProductAttributeFilter filter = new ProductAttributeFilter();
			if(productId != null && productId != 0){
				product = productMgr.getProductById(productId);
				if(product != null){
					filter.setProductId(productId);
				}
			}			
			lstProductAttributes = productAttributeMgr.getListProductAttributeVOFilter(filter);
			lstNgayThangNam =  apParamMgr.getListApParam(ApParamType.NGAY_THANG_NAM, ActiveType.RUNNING);
			lstCheckLot =  apParamMgr.getListApParam(ApParamType.CHECK_LOT, ActiveType.RUNNING);
			lstActiveType =  apParamMgr.getListApParam(ApParamType.STATUS_TYPE, ActiveType.RUNNING);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.viewDetail()"), createLogErrorStandard(actionStartTime));
		}
	    return LIST;
	}
	
	
	/**
	 * Load hinh anh, video san pham
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String loadProductImage() {
		try {
			staff = getStaffByCurrentUser();
			KPaging<MediaItem> kPaging = new KPaging<MediaItem>();
			kPaging.setPageSize(ConstantManager.MAZ_ITEM);
			kPaging.setPage(0);
			lstMediaItem = productMgr.getListMediaItemByProduct(null,productId);
			vnmTotalImage = 0;
			vnmTotalVideo = 0;
			if(lstMediaItem!=null && lstMediaItem.size()>0){
				totalMediaItem = lstMediaItem.size();
				for(MediaItem item:lstMediaItem){
					if(MediaType.IMAGE.equals(item.getMediaType())){
						++vnmTotalImage;
					} else if(MediaType.VIDEO.equals(item.getMediaType())){
						++vnmTotalVideo;
					}
				}
			}
			lstMediaItem = productMgr.getListMediaItemByProduct(kPaging,productId);
			if(lstMediaItem!=null && lstMediaItem.size()>0){
				mediaItem = lstMediaItem.get(0);
			}
			productIntroduction = productMgr.getProductIntroductionByProduct(productId);
			if(productIntroduction!=null){
				introductionId = productIntroduction.getId();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.loadProductImage()"), createLogErrorStandard(actionStartTime));
		}
	    return LIST;
	}
		
	/**
	 * Thay doi thong tin san pham (cap nhat hoac tao moi).
	 *@author tientv11
	 * @return the string
	 * @since Oct 1, 2012
	 */
	public String changeProductInfo(){	    	    	    	    
	    	try {
	    		result.put(ERROR, true);
	    		errMsg = "";
	    		// product code
				if (StringUtil.isNullOrEmpty(productCode)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,false, "catalog.display.product.code");
				}
				// product name
				if (errMsg.isEmpty() && StringUtil.isNullOrEmpty(productName)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,false, "catalog.product.name");
				}
				//product short name
				/*if(errMsg.isEmpty() && StringUtil.isNullOrEmpty(shortName)){
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.product.shortname");
				}*/
				// barcode
				/*if(errMsg.isEmpty() && !StringUtil.isNullOrEmpty(barCode) &&
						!ValidateUtil.validateNumber(barCode)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INTEGER,false, "catalog.product.barcode");
				}*/
				// order index
				if (errMsg.isEmpty() && orderIndex == null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,false, "catalog.product.order.index");
				}
				// expiry number
				if (expireNumber != null &&	(expireNumber <= SIZE_ZERO || expireNumber > CHIN_CHIN_CHIN)) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.expiry");
				}
				// category
				ProductInfo cat = null;
				if (errMsg.isEmpty() && (category == null || category <= 0)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,false, "catalog.categoryvn");
				} else if (errMsg.isEmpty() && category != null && category > SIZE_ZERO) {
					cat = productInfoMgr.getProductInfoById(category);
					if (cat == null || cat.getType() != ProductType.CAT) {
						
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,false, "catalog.categoryvn");
					
					}else if(!ActiveType.RUNNING.equals(cat.getStatus())){
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,
								false, "catalog.categoryvn");
					}
				}
				// sub cat
				ProductInfo subCategory = null;
				if (errMsg.isEmpty() && (subCat == null || subCat <= 0)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,false, "catalog.categoryvn.child");
				} else if (errMsg.isEmpty() && subCat != null && subCat > SIZE_ZERO) {
					subCategory = productInfoMgr.getProductInfoById(subCat);
					if (subCategory == null || subCategory.getType() != ProductType.SUB_CAT) {
						
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,false, "catalog.categoryvn.child");
					
					}else if(!ActiveType.RUNNING.equals(cat.getStatus())){
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,
								false, "catalog.categoryvn.child");
					}
				}
				if (errMsg.isEmpty() && orderIndex != null && orderIndex < 1) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.order.index.more.than.zero");
				}
				// brand
				ProductInfo brand = null;
				/*if (errMsg.isEmpty() && (brandId == null || brandId <= 0)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,false, "catalog.product.brand");
				
				} else*/
				if (errMsg.isEmpty() && brandId != null && brandId > SIZE_ZERO) {
					brand = productInfoMgr.getProductInfoById(brandId);
					if (brand == null || brand.getType() != ProductType.BRAND) {
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,false, "catalog.product.brand");
					
					} else if(!ActiveType.RUNNING.equals(brand.getStatus())){
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,false, "catalog.product.brand");
					}
				}
				// flavour
				ProductInfo flavour = null;
				if (errMsg.isEmpty() && flavourId != null && flavourId > SIZE_ZERO) {
					flavour = productInfoMgr.getProductInfoById(flavourId);
					if (flavour == null || flavour.getType() != ProductType.FLAVOUR) {
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,false, "catalog.product.flavour");
					
					} else if(!ActiveType.RUNNING.equals(flavour.getStatus())){
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,false, "catalog.product.flavour");
					}
				}
				// packing
				ProductInfo packing = null;
				if (errMsg.isEmpty() && packingId != null && packingId > SIZE_ZERO) {
					packing = productInfoMgr.getProductInfoById(packingId);
					if (packing == null || packing.getType() != ProductType.PACKING) {
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,false, "catalog.product.packing");
					
					} else if(!ActiveType.RUNNING.equals(packing.getStatus())){
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,false, "catalog.product.packing");
					}
				}
				// Volume
				/*if (errMsg.isEmpty() && volumn != null && volumn.doubleValue() <= 0) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.volumn.minus");
				}
				// net weigth + gross weight
				if (errMsg.isEmpty() && netWeight != null &&
						netWeight.compareTo(BigDecimal.ZERO) <= 0) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.net.weight.minus");
				}
				if (errMsg.isEmpty() && grossWeight != null && grossWeight.compareTo(BigDecimal.ZERO) <= 0) {					
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.weight.minus");
				}
				if (errMsg.isEmpty() && netWeight != null && grossWeight != null) {
			    	if(netWeight.compareTo(grossWeight) > 0){
				    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NETGROSS_WEIGHT);
				    }
			    }*/
				// uom1
				if (errMsg.isEmpty()) {
					ApParam ap_uom1 = null;
					if (StringUtil.isNullOrEmpty(uom1)) {
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.product.uom1");
					} else {
//						ap_uom1 = apParamMgr.getApParamByCode(uom1, ApParamType.UOM1);
						ap_uom1 = apParamMgr.getApParamByCode(uom1, ApParamType.UOM);
						if (ap_uom1 == null) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.product.uom1");
						} else if (!ActiveType.RUNNING.equals(ap_uom1.getStatus())) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.product.uom1");
						}
					}
				}
				
				// uom2
				if (errMsg.isEmpty()) {
					ApParam ap_uom2 = null;
					if (errMsg.isEmpty() && StringUtil.isNullOrEmpty(uom2)) {
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, false, "catalog.product.uom2");
	
					} else {
//						ap_uom2 = apParamMgr.getApParamByCode(uom2, ApParamType.UOM2);
						ap_uom2 = apParamMgr.getApParamByCode(uom2, ApParamType.UOM);
						if (ap_uom2 == null) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.product.uom2");
	
						} else if (!ActiveType.RUNNING.equals(ap_uom2.getStatus())) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.product.uom2");
						}
					}
				}
				
				// convfact
				if (errMsg.isEmpty() && convfact == null) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,false, "catalog.proudct.convfact");
				} else if (convfact <= SIZE_ZERO) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.convfact.minus");
				}
				
				if (errMsg.isEmpty()) {				
			    	if (productId == null) { // Tao moi
			    		Product prod = productMgr.getProductByCode(productCode.toUpperCase());
			    		if (prod != null) {
			    			result.put(ERROR, true);
			    			result.put("errMsg", R.getResource("common.exist.code", R.getResource("jsp.common.product.code")));
			    			return JSON;
			    		}
			    		List<Product> lstTest = productMgr.getListProductsByName(productName, null);
			    		if (lstTest != null && lstTest.size() > 0) {
			    			result.put(ERROR, true);
			    			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
			    					"catalog.product.name.exist.db"));
			    			return JSON;
			    		}
			    		//kiem tra short name
			    		/*if(!StringUtil.isNullOrEmpty(shortName)){
				    		List<Product> sNameTest = productMgr.getListProductsByShortName(shortName, null);
				    		if (sNameTest != null && sNameTest.size() > 0) {
				    			result.put(ERROR, true);
				    			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.shortname.exist.db"));
				    			return JSON;
				    		}
			    		}*/
			    		product = new Product();
			    		product.setCreateUser(currentUser.getUserName());
			    		product.setCreateDate(DateUtil.now());
			    		product.setProductCode(productCode.toUpperCase());
			    	} else { // Cap nhat
			    		product = productMgr.getProductById(productId);
			    		if (product == null) {
			    			result.put(ERROR, true);
			    			result.put("errMsg" ,ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,false, "catalog.product.com"));
			    			return JSON;
			    		}
			    		List<Product> lstTest = productMgr.getListProductsByName(productName, productId);
			    		if (lstTest != null && lstTest.size() > 0) {			    			
			    			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.name.exist.db"));
			    			return JSON;
			    		}
			    		
			    		/*if(!StringUtil.isNullOrEmpty(shortName)){
				    		List<Product> sNameTest = productMgr.getListProductsByShortName(shortName, productId);
				    		if (sNameTest != null && sNameTest.size() > 0) {
				    			result.put(ERROR, true);
				    			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
				    					"catalog.product.shortname.exist.db"));
				    			return JSON;
				    		}
			    		}*/
			    		if(BusinessUtils.checkActiveType(status, ActiveType.RUNNING, ActiveType.STOPPED)){
				    		ActiveType t = ActiveType.parseValue(status);
					    	product.setStatus(t);	
			    		}
			    		product.setUpdateUser(currentUser.getUserName());
			    		product.setUpdateDate(DateUtil.now());
			    	}
			    	product.setProductName(productName);
			    	product.setShortName(shortName);
			    	String nameText = Unicode2English.codau2khongdau(productName).toLowerCase();
			    	product.setNameText(nameText);
			    	product.setExpiryDate(expireNumber);
			    	if (expireType != null && expireType >= SIZE_ZERO) {
			    		product.setExpiryType(expireType);
			    	} else {
			    		product.setExpiryType(null);
			    	}
			    	product.setBarcode(barCode);
			    	product.setCat(cat);
			    	product.setSubCat(subCategory);
			    	product.setBrand(brand);
			    	product.setOrderIndex(orderIndex);
			    	product.setFlavour(flavour);
			    	product.setPacking(packing);
			    	if (volumn != null) {
			    		product.setVolumn(volumn.doubleValue());
			    	}
			    	product.setNetWeight(netWeight);
			    	product.setGrossWeight(grossWeight);
			    	if (!StringUtil.isNullOrEmpty(uom1)) {
			    		product.setUom1(uom1);
			    	}
			    	if (!StringUtil.isNullOrEmpty(uom2)) {
			    		product.setUom2(uom2);
			    	}
			    	product.setConvfact(convfact);
//			    	if(checkLot != null){
//			    		product.setCheckLot(checkLot);
//			    	}
			    	product = productMgr.saveOrUpdateProduct(product,lstAttributeId, lstAttributeValue, lstAttributeColumnValueType,getLogInfoVO());
			    	productId = product.getId();
			    	result.put("productId", productId);
			    	result.put("product", product);
			    	result.put(ERROR, false);
				} else {
					result.put("errMsg", errMsg);
			    	result.put(ERROR, true);
				}
	    } catch (Exception ex) {	    
	    	LogUtility.logError(ex, ex.getMessage());
	    	result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
	    }
	    return JSON;
	}

	/**
	 * Cap nhat mo ta san pham
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String changeProductDescription(){
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    try{
		    if(currentUser!= null){
		    	if(productId != null && productId > 0){
		    		product = productMgr.getProductById(productId);
		    		if(product != null){
		    			productIntroduction = productMgr.getProductIntroductionByProduct(productId);
		    			if(productIntroduction != null){
		    				productIntroduction.setContent(description);
		    				productIntroduction.setUpdateUser(currentUser.getUserName());
		    				productIntroduction.setUpdateDate(new Date());
		    				productMgr.updateProductIntroduction(productIntroduction, getLogInfoVO());
		    			}else{
		    				productIntroduction = new ProductIntroduction();
		    				productIntroduction.setContent(description);
		    				productIntroduction.setProduct(product);
		    				productIntroduction.setCreateUser(currentUser.getUserName());
		    				productIntroduction.setCreateDate(new Date());
		    				productIntroduction.setStatus(ActiveType.RUNNING);
		    				productIntroduction = productMgr.createProductIntroduction(productIntroduction, getLogInfoVO());
		    			}
	    				error = false;
		    		}
		    	}
		    }
	    }catch (Exception e) {
//	    	logInfo.setFunctionType(FunctionType.CREATE);
	    	LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.changeProductDescription()"), createLogErrorStandard(actionStartTime));
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}
	
	/**
	 * Xoa san pham
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String deleteProduct(){
	    
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
				if(productId != null && productId != 0){
					if(productMgr.isProductUsingByOthers(productId)){
						errMsg= Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.is.used",
								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.product.code"));
					}else{
						product = productMgr.getProductById(productId);
						if(product!= null){
							productMgr.deleteProduct(product,getLogInfoVO());
							error = false;
						}
					}
				}
			}catch (Exception e) {
//				logInfo.setFunctionType(FunctionType.READ);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.deleteProduct()"), createLogErrorStandard(actionStartTime));
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}

	/**
	 * Xoa hinh anh/ video san pham
	 *
	 * @return the string
	 */
	public String deleteProductMediaItem(){
	    
	    resetToken(result);
	    boolean error = true;
	    if(currentUser!= null){
			try{
				if(mediaItemId != null && mediaItemId != 0){
					mediaItem = productMgr.getMediaItemById(mediaItemId);
					if(mediaItem!= null){
						if(!StringUtil.isNullOrEmpty(mediaItem.getClientThumbUrl())){
							File file = new File(Configuration.getImageRealPath() + mediaItem.getClientThumbUrl());
							file.delete();
						}
						mediaItem.setUpdateUser(currentUser.getUserName().toUpperCase());
						mediaItem.setUpdateDate(DateUtil.now());
						mediaItem.setStatus(ActiveType.DELETED);						
						productMgr.updateMediaItem(mediaItem, getLogInfoVO());
						error = false;
					}
				}
			}catch (Exception e) {
//				logInfo.setFunctionType(FunctionType.UPDATE);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.deleteProductMediaItem()"), createLogErrorStandard(actionStartTime));
			}
	    }
	    result.put(ERROR, error);
	    return JSON;
	}
	/**
	 * upload image
	 * @author vuongmq
	 * @return
	 * @date 22/01/2015
	 * 
	 * @madify hunglm16
	 * @since 14/05/2015
	 * @description Bo sung kiem tra duoi file
	 */
	public String uploadImageFile() {
		resetToken(result);
		try {
			/**process save images */
			if(productId != null){
				product = productMgr.getProductById(productId);
	    		if (product == null) {
	    			result.put(ERROR, true);
	    			result.put("errMsg" ,ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,false, "catalog.product.com"));
	    			return JSON;
	    		}
			} else {
				result.put(ERROR, true);
    			result.put("errMsg" ,R.getResource("sp.create.order.product.id.null"));
    			return JSON;
			}
			if (images != null && images.size() > 0) {
				/** store all image on disk first  */
				result.put("received", images.size());
				List<String> processFailImages = new ArrayList<String>();
				List<String> processFailImagesFormat = new ArrayList<String>(); // 1 file format fail tra ve nguyen danh sach file
				/***list danh sach image*/
				List<File> imagesImg = new ArrayList<File>();
				List<String> imagesFileNameImg = new ArrayList<String>();
				List<String> imagesContentTypeImg = new ArrayList<String>();
				List<MediaItem> mediaItemsImg = null;
				/***list danh sach video*/
				List<File> imagesVideo = new ArrayList<File>();
				List<String> imagesFileNameVideo = new ArrayList<String>();
				List<String> imagesContentTypeVideo = new ArrayList<String>();
				List<MediaItem> mediaItemsVideo = null;
				//Thong bao sai dinh dang
				String tempFormatError= StringUtil.getFormatSupport(Configuration.getImageUploadFileSupport()) 
										+ "<br>" 
										+ StringUtil.getFormatSupport(Configuration.getVideoUploadFileSupport());
				//End Thong bao
				errMsg="";
				for (int i = 0, sz = images.size(); i < sz; i++) {
					if (Configuration.getImageUploadFileSupport().indexOf(imagesContentType.get(i)) != -1) {
						if (!FileUtility.isValidExtensionInListName(FileUtility.getFileExtension(images.get(i)), FileUtility.validImageExtension)) {
							//Bo sung kiem tra file
							errMsg = R.getResource("web.validate.file.error", imagesFileName.get(i));
						}  else {
							// Configuration.getImageUploadFileSupport().contains(imagesContentType.get(i))
							if (images.get(i) == null || FileUtility.getMime(images.get(i)) == null || (FileUtility.getMime(images.get(i)) != null && Configuration.getImageUploadFileSupport().indexOf(FileUtility.getMime(images.get(i))) == -1)) {
								errMsg = errMsg + imagesFileName.get(i) + "<br>" + Configuration.getResourceString("vi", "image.invalid.type") + "<br>" + tempFormatError + "<br>-----<br>";
								processFailImagesFormat.add(i < imagesFileName.size() ? imagesFileName.get(i) : "");
							} else {
								imagesImg.add(images.get(i));
								imagesFileNameImg.add(imagesFileName.get(i));
								imagesContentTypeImg.add(imagesContentType.get(i));
							}
						}
					} else if (Configuration.getVideoUploadFileSupport().indexOf(imagesContentType.get(i)) != -1) {
						if (!FileUtility.isValidExtensionInListName(FileUtility.getFileExtension(images.get(i)), FileUtility.validVideoExtension)) {
							//Bo sung kiem tra file
							errMsg = R.getResource("web.validate.file.error", imagesFileName.get(i));
						}  else {
							if (images.get(i) == null || FileUtility.getMime(images.get(i)) == null || (FileUtility.getMime(images.get(i)) != null && Configuration.getVideoUploadFileSupport().indexOf(FileUtility.getMime(images.get(i))) == -1)) {
								errMsg = errMsg + imagesFileName.get(i) + "<br>" + R.getResource("image.invalid.type") + "<br>" + tempFormatError + "<br>-----<br>";
								processFailImagesFormat.add(i < imagesFileName.size() ? imagesFileName.get(i) : "");
							} else {
								imagesVideo.add(images.get(i));
								imagesFileNameVideo.add(imagesFileName.get(i));
								imagesContentTypeVideo.add(imagesContentType.get(i));
							}
						}
					} else if (Configuration.getVideoUploadFileSupport().indexOf(imagesContentType.get(i)) == -1) {
						// file upload khong dung dinh dang
						processFailImagesFormat.add(i < imagesFileName.size() ? imagesFileName.get(i) : "");
						errMsg = errMsg + imagesFileName.get(i) + "<br>" + R.getResource("image.invalid.type") + "<br>" + tempFormatError + "<br>-----<br>";
					} else {
						errMsg = errMsg + imagesFileName.get(i) + "<br>" + R.getResource("image.invalid") + "<br>-----<br>";
						processFailImages.add(i < imagesFileName.size() ? imagesFileName.get(i) : "");
					}
				}
				if(imagesImg != null && imagesImg.size() > 0){
					mediaItemsImg = storeImageOnDisk(imagesImg, imagesFileNameImg, imagesContentTypeImg, productId);
				}
				if(imagesVideo != null && imagesVideo.size() > 0){
					mediaItemsVideo = storeImageOnDiskVideo(imagesVideo, imagesFileNameVideo, imagesContentTypeVideo, productId);
				}
				productMgr.saveUploadImageInfo(mediaItemsImg, mediaItemsVideo, getLogInfoVO());
				if (processFailImagesFormat.size()>0)
				{
					for(int j = 0; j < processFailImagesFormat.size(); j ++)
					{
						processFailImages.add(processFailImagesFormat.get(j).toString());
					}
				}
				if(errMsg != null && errMsg.length() > 0){
					/*errMsg += "<br>" 
							+ StringUtil.getFormatSupport(Configuration.getImageUploadFileSupport()) 
							+ "<br>" 
							+ StringUtil.getFormatSupport(Configuration.getVideoUploadFileSupport());*/
					result.put("errMsg", errMsg);
					result.put("failFormat", processFailImagesFormat);
				}
				result.put("fail", processFailImages);
				System.gc();
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ImagesAction.saveUploadImage()" + e.getMessage());
			result.put("fail", imagesFileName);
		}
		return JSON;
	}
	/**
	 * store image to disk
	 * @author tuannd20
	 * @param receivedFiles		received files
	 * @param filesName			received files name
	 * @param filesContentType	received file's content type
	 * @param programCode		display program code
	 * @param programName		display program name
	 * @param shopShortName		shop's short code
	 * @return
	 * @date 15/11/2014
	 */
	private List<MediaItem> storeImageOnDisk(List<File> receivedFiles, List<String> filesName, List<String> filesContentType, Long productId) {
		List<MediaItem> mediaItems = new ArrayList<MediaItem>();
		try {
			// Tao danh sach Image mediaItem
			if (receivedFiles != null && receivedFiles.size() > 0) {
				String realPath = Configuration.getStoreStaticPath();
				Calendar calF = Calendar.getInstance();
		        Product product = null;
		        try {
		        	product = productMgr.getProductById(productId);
				} catch (Exception e) {			
					LogUtility.logError(e, e.getMessage());
				}
		        String categoryCode = "A";
		        if(product != null && product.getCat() != null){
		        	categoryCode = product.getCat().getProductInfoCode();
		        }
		        String Thumb = ImageUtility.createDetailUploadPath(productId,calF,0,categoryCode);//url thumbnail
		        String folderThumb = realPath + Thumb;
		        if (!FileUtility.isFolderExist(folderThumb)) {
		            FileUtility.createDirectory(folderThumb);
		        }
		        String Images = ImageUtility.createDetailUploadPath(productId,calF,3,categoryCode);//url Images
		        String folderImages = realPath + Images;
		        if (!FileUtility.isFolderExist(folderImages)) {
		            FileUtility.createDirectory(folderImages);
		        }
		        for(int i = 0, sz = receivedFiles.size(); i< sz; i++){
		        	Calendar cal = Calendar.getInstance();
		        	File imgFile = receivedFiles.get(i);
			        ImageInfo info = ImageUtility.getImageInfo(imgFile);
					String format = "." ;
					format +=  info!=null?info.getFormat():"jpg";
					String productCode = "";
					if(product != null){
						productCode = product.getProductCode();
					}
					Integer tmpM = cal.get(Calendar.MONTH) +1;
					String dd = String.valueOf(cal.get(Calendar.DATE)>10? cal.get(Calendar.DATE):"0"+cal.get(Calendar.DATE));
					String MM = String.valueOf(tmpM>10?tmpM:"0"+tmpM);
					String hh = String.valueOf(cal.get(Calendar.HOUR_OF_DAY)>10?cal.get(Calendar.HOUR_OF_DAY):"0"+cal.get(Calendar.HOUR_OF_DAY));
					String mm = String.valueOf(cal.get(Calendar.MINUTE)>10?cal.get(Calendar.MINUTE):"0"+cal.get(Calendar.MINUTE));
					String ss = String.valueOf(cal.get(Calendar.SECOND)>10?cal.get(Calendar.SECOND):"0"+cal.get(Calendar.SECOND));
			        String file = productCode+dd+MM+cal.get(Calendar.YEAR)+hh+mm+ss + Long.toString(cal.getTimeInMillis())+ format;        
			        
			        String fileNameThumb = folderThumb + file;        
			        String fileNameImages = folderImages + file;
			        
			        
					Boolean resThumb = ImageUtility.createThumbnail(imgFile.getAbsolutePath(), fileNameThumb,ImageUtility.INT_THUMB_SIZE_128,ImageUtility.INT_THUMB_SIZE_96);
			        if (resThumb == null || !resThumb ) {
			            return null;
			        }
			        Boolean resBigThumb = ImageUtility.createThumbnail(imgFile.getAbsolutePath(), fileNameImages,ImageUtility.INT_THUMB_SIZE_1024,ImageUtility.INT_THUMB_SIZE_768);
			        if (resBigThumb == null || !resBigThumb ) {
			            return null;
			        }
			                
			        MediaItem mediaItem = new MediaItem();
			        mediaItem.setCreateDate(DateUtil.now());
			        mediaItem.setThumbUrl(Thumb + file);
			        mediaItem.setUrl(Images + file);
			        
			        File f = new File(fileNameThumb);
			        info = new ImageInfo();
					try {
						mediaItem.setObjectId(productId);
			            info = ImageInfo.getImageInfo(f);
			            mediaItem.setWidth(Float.parseFloat(String.valueOf(info.getWidth())));
			            mediaItem.setHeight(Float.parseFloat(String.valueOf(info.getHeight())));
			        } catch (Exception e) {        	
			        	LogUtility.logError(e, e.getMessage());        	
			        }
			        mediaItem.setMediaType(MediaType.IMAGE); // 0: image; 1: video
			        mediaItem.setObjectType(MediaObjectType.IMAGE_PRODUCT); // 3: hinh san pham
			        mediaItem.setFileSize(Float.valueOf(String.valueOf(f.length())));
			        mediaItem.setCreateUser(currentUser.getUserName());
			        mediaItem.setStatus(ActiveType.RUNNING);
			        mediaItems.add(mediaItem);
		        }
			}	
		} catch (Exception e) {
			LogUtility.logError(e, "ImagesAction.storeImageOnDisk()" + e.getMessage());
		}
		return mediaItems;
	}
	//storeImageOnDiskVideo
	private List<MediaItem> storeImageOnDiskVideo(List<File> receivedFiles, List<String> filesName, List<String> filesContentType, Long productId) {
		Date startLogDate = DateUtil.now();
		List<MediaItem> mediaItems = new ArrayList<MediaItem>();
		FileInputStream from = null;
	    FileOutputStream to = null;
		try {
			// Tao danh sach Video mediaItem
			if (receivedFiles != null && receivedFiles.size() > 0) {
				String folder = Configuration.getStoreStaticPath();
				Product product = productMgr.getProductById(productId);
				String categoryCode = "A";
				String productCode = "";
				if(product != null){
					productCode = product.getProductCode();
					if(product.getCat() != null){
						categoryCode = product.getCat().getProductInfoCode();
					}
					/*videoFile = item.getStoreLocation();					       
			        videoMaxSize = item.getSize();	*/		
					for(int i = 0, sz = receivedFiles.size(); i< sz; i++){
						try {
							File videoFile = receivedFiles.get(i);
					       /* ImageInfo info = ImageUtility.getImageInfo(videoFile);
							String format = "." ;
							format +=  info!=null?info.getFormat():"mp4";*/
					        Calendar cal = Calendar.getInstance();
					        String url = ImageUtility.createDetailUploadPath(productId,cal,4,categoryCode);
					        String folderMedia = folder + url;
					        if (!FileUtility.isFolderExist(folderMedia)) {
					            FileUtility.createDirectory(folderMedia);
					        }
					        String format = filesName.get(i).substring(filesName.get(i).length()-4, filesName.get(i).length());
					        Integer tmpM = cal.get(Calendar.MONTH) +1;
							String dd = String.valueOf(cal.get(Calendar.DATE)>10? cal.get(Calendar.DATE):"0"+cal.get(Calendar.DATE));
							String MM = String.valueOf(tmpM>10?tmpM:"0"+tmpM);
							String hh = String.valueOf(cal.get(Calendar.HOUR_OF_DAY)>10?cal.get(Calendar.HOUR_OF_DAY):"0"+cal.get(Calendar.HOUR_OF_DAY));
							String mm = String.valueOf(cal.get(Calendar.MINUTE)>10?cal.get(Calendar.MINUTE):"0"+cal.get(Calendar.MINUTE));
							String ss = String.valueOf(cal.get(Calendar.SECOND)>10?cal.get(Calendar.SECOND):"0"+cal.get(Calendar.SECOND));
					        String fileNameMedia = productCode+dd+MM+cal.get(Calendar.YEAR)+hh+mm+ss + Long.toString(cal.getTimeInMillis())+ format; 
					        String fileNameUrl = url + fileNameMedia;
					        File file = new File(folderMedia + fileNameMedia);
					        //Float videoMaxSize = Float.valueOf(videoFile.length());	
							/*if(videoMaxSize > MAX_UPLOAD_SIZE){
								break;
							}else{*/
					        /* Begin copy file video**/
				            from = new FileInputStream(videoFile);
				            to = new FileOutputStream(file);
				            byte[] buffer = new byte[4096];
				            int bytesRead;
				            while ((bytesRead = from.read(buffer)) != -1) {
				                to.write(buffer, 0, bytesRead); // write
	
				            }
					        /* End copy file video**/
							MediaItem mediaItem = new MediaItem();
							mediaItem.setUrl(fileNameUrl);
							mediaItem.setThumbUrl("/resources/images/thumbnailaudiovideo.png");
							mediaItem.setObjectId(productId);
							mediaItem.setObjectType(MediaObjectType.IMAGE_PRODUCT);
							mediaItem.setMediaType(MediaType.VIDEO);        
							mediaItem.setFileSize(Float.valueOf(file.length()));
							mediaItem.setStatus(ActiveType.RUNNING);
							mediaItem.setCreateUser(currentUser.getUserName());
							mediaItems.add(mediaItem);
							//}
						} catch (Exception e) {
							LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ProductCatalogAction.storeImageOnDiskVideo() - receivedFiles index" + receivedFiles.get(i)), createLogErrorStandard(startLogDate));
						} finally {
							if (from != null) {
								from.close();
							}
							if (to != null) {
								to.close();
							}
						}
					}	
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ProductCatalogAction.storeImageOnDiskVideo()"), createLogErrorStandard(startLogDate));
		} finally {
            if (from != null) {
                try {
                    from.close();
                } catch (IOException e) {
                    ;
                }
            }
            if (to != null) {
                try {
                    to.close();
                } catch (IOException e) {
                    ;
                }
            }
        }
		return mediaItems;
	}
	/**
	 * upload hinh anh san pham
	 *
	 * @author
	 * @since
	 * @return the string
	 */
	/*public static MediaItem uploadImage(File imgFile,final String userName,final String realPath,final Long productId,ApplicationContext context,ProductMgr productMgr,LogInfoVO logObj){        
		
		Calendar cal = Calendar.getInstance();
        Product product = null;
        try {
        	product = productMgr.getProductById(productId);
		} catch (Exception e) {			
			LogUtility.logError(e, e.getMessage());
		}
        String categoryCode = "A";
        if(product != null && product.getCat() != null){
        	categoryCode = product.getCat().getProductInfoCode();
        }
        String Thumb = ImageUtility.createDetailUploadPath(productId,cal,0,categoryCode);//url thumbnail
        String folderThumb = realPath + Thumb;
        if (!FileUtility.isFolderExist(folderThumb)) {
            FileUtility.createDirectory(folderThumb);
        }
        String Images = ImageUtility.createDetailUploadPath(productId,cal,3,categoryCode);//url Images
        String folderImages = realPath + Images;
        if (!FileUtility.isFolderExist(folderImages)) {
            FileUtility.createDirectory(folderImages);
        }
        ImageInfo info = ImageUtility.getImageInfo(imgFile);
		String format = "." ;
		format +=  info!=null?info.getFormat():"jpg";
		String productCode = "";
		if(product != null){
			productCode = product.getProductCode();
		}
		Integer tmpM = cal.get(Calendar.MONTH) +1;
		String dd = String.valueOf(cal.get(Calendar.DATE)>10? cal.get(Calendar.DATE):"0"+cal.get(Calendar.DATE));
		String MM = String.valueOf(tmpM>10?tmpM:"0"+tmpM);
		String hh = String.valueOf(cal.get(Calendar.HOUR_OF_DAY)>10?cal.get(Calendar.HOUR_OF_DAY):"0"+cal.get(Calendar.HOUR_OF_DAY));
		String mm = String.valueOf(cal.get(Calendar.MINUTE)>10?cal.get(Calendar.MINUTE):"0"+cal.get(Calendar.MINUTE));
		String ss = String.valueOf(cal.get(Calendar.SECOND)>10?cal.get(Calendar.SECOND):"0"+cal.get(Calendar.SECOND));
        String file = productCode+dd+MM+cal.get(Calendar.YEAR)+hh+mm+ss + Long.toString(cal.getTimeInMillis())+ format;        
        
        String fileNameThumb = folderThumb + file;        
        String fileNameImages = folderImages + file;
        
        
		Boolean resThumb = ImageUtility.createThumbnail(imgFile.getAbsolutePath(), fileNameThumb,ImageUtility.INT_THUMB_SIZE_128,ImageUtility.INT_THUMB_SIZE_96);
        if (resThumb == null || !resThumb ) {
            return null;
        }
        Boolean resBigThumb = ImageUtility.createThumbnail(imgFile.getAbsolutePath(), fileNameImages,ImageUtility.INT_THUMB_SIZE_1024,ImageUtility.INT_THUMB_SIZE_768);
        if (resBigThumb == null || !resBigThumb ) {
            return null;
        }
                
        MediaItem mediaItem = new MediaItem();
        mediaItem.setCreateDate(DateUtil.now());
        mediaItem.setThumbUrl(Thumb + file);
        mediaItem.setUrl(Images + file);
        
        File f = new File(fileNameThumb);
        info = new ImageInfo();
		try {
			if(productId != null && productId > 0){
				product = productMgr.getProductById(productId);
				if(product != null ){
					mediaItem.setObjectId(productId);
//					mediaItem.setObjectType(MediaObjectType.IMAGE_PRODUCT.getValue());
				}
			}
            info = ImageInfo.getImageInfo(f);
            mediaItem.setWidth(Float.parseFloat(String.valueOf(info.getWidth())));
            mediaItem.setHeight(Float.parseFloat(String.valueOf(info.getHeight())));
        } catch (Exception e) {        	
        	LogUtility.logError(e, e.getMessage());        	
        }
//        mediaItem.setMediaType(MediaType.IMAGE.getValue());
        mediaItem.setFileSize(Float.valueOf(String.valueOf(f.length())));
        mediaItem.setCreateUser(userName);
//        mediaItem.setObjectType(MediaObjectType.IMAGE_PRODUCT.getValue());
        mediaItem.setStatus(ActiveType.RUNNING);
        try {
			mediaItem = productMgr.createMediaItem(mediaItem, logObj);
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());			
		}
		return mediaItem;
	}	*/
	
	/**
	 * upload video san pham
	 *
	 * @author
	 * @since
	 * @return the string
	 */
	/*public static MediaItem uploadVideo(File videoFile,String fileName,final String userName,final String realPath,final Long productId,ApplicationContext context,ProductMgr productMgr,LogInfoVO logObj){				
        MediaItem mediaItem = new MediaItem();
        mediaItem.setUrl(fileName);
		mediaItem.setThumbUrl("/resources/images/thumbnailaudiovideo.png");
		mediaItem.setObjectId(productId);
//		mediaItem.setObjectType(MediaObjectType.IMAGE_PRODUCT.getValue());
//        mediaItem.setMediaType(MediaType.VIDEO.getValue());        
        mediaItem.setFileSize(Float.valueOf(videoFile.length()));
        mediaItem.setStatus(ActiveType.RUNNING);
        try {
        	*//**@author TIENTV11 
        	 * Thay doi nghiep vu 1 san pham co nhieu video 
        	MediaItem teMediaItem = productMgr.checkIsExistsVideoOfProduct(productId);
        	if(teMediaItem!=null){
        		teMediaItem.setUrl(fileName);
        		teMediaItem.setThumbUrl("/resources/images/thumbnailaudiovideo.png");
        		teMediaItem.setFileSize(Float.valueOf(videoFile.length()));  
        		teMediaItem.setUpdateDate(DateUtil.now());
        		teMediaItem.setUpdateUser(userName);
        		mediaItem = productMgr.updateMediaItem(teMediaItem, logObj);
        	}else{
        		mediaItem.setCreateUser(userName);
        		mediaItem.setCreateDate(DateUtil.now());
        		mediaItem = productMgr.createMediaItem(mediaItem, logObj);
        	}*//*
        	mediaItem.setCreateUser(userName);
    		mediaItem.setCreateDate(DateUtil.now());
    		mediaItem = productMgr.createMediaItem(mediaItem, logObj);
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return mediaItem; 
	}*/

	/**
	 * Lay danh sach hinh anh/video san pham
	 *
	 * @return the list media item
	 * @author lamnh
	 * @since Oct 27, 2012
	 */
	public String getListMediaItem(){
	    if(currentUser!= null){
    		try{
//    			checkPermission = BusinessUtils.checkStaffType(userSignIn, StaffObjectType.ADMIN_TCT,StaffObjectType.ADMIN_HABECO) ? 1 : 0;
    			KPaging<MediaItem> kPaging = new KPaging<MediaItem>();
				kPaging.setPageSize(ConstantManager.MAZ_ITEM);
				kPaging.setPage(numIndex);
				lstMediaItem = productMgr.getListMediaItemByProduct(kPaging,productId);
				if(lstMediaItem != null){
					totalMediaItem = lstMediaItem.size();
				}
    		}catch (Exception e) {
//    			logInfo.setFunctionType(FunctionType.READ);
    			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.getListMediaItem()"), createLogErrorStandard(actionStartTime));
    		}
	    }
	    return LIST;
	}
	
	public String downloadImportTemplateFile() {
		result.put(ERROR, false);
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("hasData", 1);
			
			// tao combobox dvt han su dung
			lstNgayThangNam =  apParamMgr.getListApParam(ApParamType.NGAY_THANG_NAM, ActiveType.RUNNING);
			beans.put("lstNgayThangNam", lstNgayThangNam);
			// don vi tinh le
//			List<ApParam> lstUOM1 = apParamMgr.getListUoms(ApParamType.UOM1);
			List<ApParam> lstUOM = apParamMgr.getListUoms(ApParamType.UOM);
			beans.put("lstUOM1", lstUOM);
			// don vi tinh thung
//			List<ApParam> lstUOM2 = apParamMgr.getListUoms(ApParamType.UOM2);
			beans.put("lstUOM2", lstUOM);
			// nganh hang
			List<CategoryVO> lstCategoryVO = new ArrayList<CategoryVO>();
			List<ProductInfoObjectType> objTypes = new ArrayList<ProductInfoObjectType>();
			objTypes.add(ProductInfoObjectType.SP);			
			ObjectVO<ProductInfo> vo = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.CAT, objTypes, true);
			List<ProductInfo> lstCategory = vo.getLstObject();
			if (lstCategory!= null && lstCategory.size() > 0) {
				for (int i = 0, size = lstCategory.size(); i < size; i++) {
					CategoryVO categoryVO = new CategoryVO();
					categoryVO.setCatCode(lstCategory.get(i).getProductInfoCode());
					categoryVO.setCatName(lstCategory.get(i).getProductInfoName());
					// nganh hang con
					List<ProductInfo> lstCategoryChild = productInfoMgr.getListSubCat(ActiveType.RUNNING, ProductType.SUB_CAT, lstCategory.get(i).getId());
					if (lstCategoryChild!= null && lstCategoryChild.size() > 0) {
						for (int j = 0, n = lstCategoryChild.size(); j < n; j++) {
							if (j == 0) {
								categoryVO.setSubCatCode(lstCategoryChild.get(j).getProductInfoCode());
								categoryVO.setSubCatName(lstCategoryChild.get(j).getProductInfoName());
								lstCategoryVO.add(categoryVO);
							} else {
								CategoryVO categoryVO1 = new CategoryVO();
								categoryVO1.setCatCode(lstCategory.get(i).getProductInfoCode());
								categoryVO1.setCatName(lstCategory.get(i).getProductInfoName());
								categoryVO1.setSubCatCode(lstCategoryChild.get(j).getProductInfoCode());
								categoryVO1.setSubCatName(lstCategoryChild.get(j).getProductInfoName());
								lstCategoryVO.add(categoryVO1);
							}							
						}
					} else {
						lstCategoryVO.add(categoryVO);
					}
				}
			}
			beans.put("lstCategoryVO", lstCategoryVO);
			// nhan hang
			ObjectVO<ProductInfo> lstBrandVO = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.BRAND, objTypes, true);
			List<ProductInfo> lstBrand = lstBrandVO.getLstObject();
			if (lstBrand!= null && lstBrand.size() > 0) {
				beans.put("lstBrand", lstBrand);
			} else {
				beans.put("lstBrand", new ArrayList<ProductInfo>());
			}
			
			// huong vi
			ObjectVO<ProductInfo> lstFlavourVO = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.FLAVOUR, objTypes, true);
			List<ProductInfo> lstFlavour = lstFlavourVO.getLstObject();
			if (lstFlavour!= null && lstFlavour.size() > 0) {
				beans.put("lstFlavour", lstFlavour);
			} else {
				beans.put("lstFlavour", new ArrayList<ProductInfo>());
			}
			
			// bao bi
			ObjectVO<ProductInfo> lstPackingVO = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.PACKING, objTypes, true);
			List<ProductInfo> lstPacking = lstPackingVO.getLstObject();
			if (lstPacking!= null && lstPacking.size() > 0) {
				beans.put("lstPacking", lstPacking);
			} else {
				beans.put("lstPacking", new ArrayList<ProductInfo>());
			}
			
			beans.put("numOrder", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.num.order"));
			beans.put("infoProduct", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.product.thong.tin.san.pham"));
			beans.put("productCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.code"));
			beans.put("productName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.name"));
			beans.put("uom1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom1.excel"));
			beans.put("uom2", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom2.excel"));
			beans.put("convfact", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.proudct.convfact.sl"));
			beans.put("category", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.cat"));
			beans.put("categoryName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.cat.name"));
			beans.put("subcat", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat"));
			beans.put("subcatName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat.name"));
			beans.put("brand", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.brand"));
			beans.put("brandName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.brand.name"));
			beans.put("flavour", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.flavour"));
			beans.put("flavourName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.flavour.name"));
			beans.put("packing", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.packing"));
			beans.put("packingName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.packing.name"));
			beans.put("orderIndex", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.order.index"));
			beans.put("volume", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.volumn"));
			beans.put("netWeight", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.netweight"));
			beans.put("grossWeight", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.totalweight"));
			beans.put("barcode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.barcode.excel"));
			beans.put("expiryDate", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.product.han.su.dung"));
			beans.put("expiryType", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.expiry.dvt"));
			beans.put("status", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.status"));
			beans.put("dynamicAttribute", getProductDynamicAttributeVO());
			
			String outputPath = exportExcelJxls(beans, Configuration.getExcelTemplatePathProduct(), ConstantManager.IMPORT_PRODUCT_TEMPATE_FILE_NAME + FileExtension.XLS.getValue());
			
			result.put(ERROR, false);
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(ex, ex.getMessage());
		}
		return JSON;
	}
	
	private List<Object> getProductDynamicAttributeVO() throws BusinessException {
		return dynamicAttributeHelper.getProductDynamicAttributeVO(ActiveType.RUNNING);
	}
	
	/*public String downloadImportTemplateFile1() {
		String importTemplateFileName = this.createImportPriceTemplateFile();
		String outputPath = Configuration.getExportExcelPath() + importTemplateFileName;
		result.put(REPORT_PATH, outputPath);
		result.put(LIST, outputPath);
		result.put(ERROR, false);
		result.put("hasData", true);
		return JSON;
	}
	
	private String createImportPriceTemplateFile() {
		String outputName = null;

		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		XSSFWorkbook workbook = null;
		OPCPackage opcPackage = null;
		try {
			
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathProduct();
			String templateFileName = folder + ConstantManager.IMPORT_PRODUCT_TEMPATE_FILE_NAME + ConstantManager.EXPORT_FILE_EXTENSION_EX;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			
			outputName = ConstantManager.IMPORT_PRODUCT_TEMPATE_FILE_NAME + "_" + genExportFileSuffix() + ConstantManager.EXPORT_FILE_EXTENSION_EX;
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			fileInputStream = new FileInputStream(templateFileName);
			opcPackage = OPCPackage.open(fileInputStream);
			workbook = new XSSFWorkbook(opcPackage);
			
			//ghi header
			int IMPORT_SHEET_INDEX = 0;
			this.writeImportSheetHeader(workbook, IMPORT_SHEET_INDEX);
			int DATA_SHEET_INDEX = 1;
			XSSFSheet dataSheet = workbook.getSheetAt(DATA_SHEET_INDEX);
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			if (dataSheet != null) {
				// tao combobox dvt han su dung
				lstNgayThangNam =  apParamMgr.getListApParam(ApParamType.NGAY_THANG_NAM, ActiveType.RUNNING);
				if (lstNgayThangNam!= null && lstNgayThangNam.size() > 0) {
					for (int i = 0, size = lstNgayThangNam.size(); i < size; i++) {
						excelProcessUtil.writeCellData(dataSheet, i, 0, lstNgayThangNam.get(i).getApParamName());
					}
				}
				// don vi tinh le
				List<ApParam> lstUOM1 = apParamMgr.getListUoms(ApParamType.UOM1);
				if (lstUOM1!= null && lstUOM1.size() > 0) {
					for (int i = 0, size = lstUOM1.size(); i < size; i++) {
						excelProcessUtil.writeCellData(dataSheet, i, 1, lstUOM1.get(i).getApParamName());
					}
				}
				// don vi tinh thung
				List<ApParam> lstUOM2 = apParamMgr.getListUoms(ApParamType.UOM2);
				if (lstUOM2!= null && lstUOM2.size() > 0) {
					for (int i = 0, size = lstUOM2.size(); i < size; i++) {
						excelProcessUtil.writeCellData(dataSheet, i, 2, lstUOM2.get(i).getApParamName());
					}
				}
				// nganh hang
				List<ProductInfoObjectType> objTypes = new ArrayList<ProductInfoObjectType>();
				objTypes.add(ProductInfoObjectType.SP);			
				ObjectVO<ProductInfo> vo = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.CAT, objTypes, true);
				List<ProductInfo> lstCategory = vo.getLstObject();
				int row = 0;
				if (lstCategory!= null && lstCategory.size() > 0) {
					for (int i = 0, size = lstCategory.size(); i < size; i++) {
						excelProcessUtil.writeCellData(dataSheet, row, 3, lstCategory.get(i).getProductInfoCode());
						excelProcessUtil.writeCellData(dataSheet, row, 4, lstCategory.get(i).getProductInfoName());
						// nganh hang con
						List<ProductInfo> lstCategoryChild = productInfoMgr.getListSubCat(ActiveType.RUNNING, ProductType.SUB_CAT, lstCategory.get(i).getId());
						if (lstCategoryChild!= null && lstCategoryChild.size() > 0) {
							for (int j = 0, n = lstCategoryChild.size(); j < n; j++) {
								if (j > 0) {
									excelProcessUtil.writeCellData(dataSheet, row, 3, lstCategory.get(i).getProductInfoCode());
									excelProcessUtil.writeCellData(dataSheet, row, 4, lstCategory.get(i).getProductInfoName());
								}
								excelProcessUtil.writeCellData(dataSheet, row, 5, lstCategoryChild.get(j).getProductInfoCode());
								excelProcessUtil.writeCellData(dataSheet, row, 6, lstCategoryChild.get(j).getProductInfoName());
								row++;
							}
						} else {
							row++;
						}
					}
				}			
				// nhan hang
				ObjectVO<ProductInfo> lstBrandVO = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.BRAND, objTypes, true);
				List<ProductInfo> lstBrand = lstBrandVO.getLstObject();
				if (lstBrand!= null && lstBrand.size() > 0) {
					for (int i = 0, n = lstBrand.size(); i < n; i++) {
						excelProcessUtil.writeCellData(dataSheet, i, 7, lstBrand.get(i).getProductInfoCode());
						excelProcessUtil.writeCellData(dataSheet, i, 8, lstBrand.get(i).getProductInfoName());
					}
				}
				
				// huong vi
				ObjectVO<ProductInfo> lstFlavourVO = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.FLAVOUR, objTypes, true);
				List<ProductInfo> lstFlavour = lstFlavourVO.getLstObject();
				if (lstFlavour!= null && lstFlavour.size() > 0) {
					for (int i = 0, n = lstFlavour.size(); i < n; i++) {
						excelProcessUtil.writeCellData(dataSheet, i, 9, lstFlavour.get(i).getProductInfoCode());
						excelProcessUtil.writeCellData(dataSheet, i, 10, lstFlavour.get(i).getProductInfoName());
					}
				}
				
				// bao bi
				ObjectVO<ProductInfo> lstPackingVO = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.PACKING, objTypes, true);
				List<ProductInfo> lstPacking = lstPackingVO.getLstObject();
				if (lstPacking!= null && lstPacking.size() > 0) {
					for (int i = 0, n = lstPacking.size(); i < n; i++) {
						excelProcessUtil.writeCellData(dataSheet, i, 11, lstPacking.get(i).getProductInfoCode());
						excelProcessUtil.writeCellData(dataSheet, i, 12, lstPacking.get(i).getProductInfoName());
					}
				}
				
			}			

			fileOutputStream = new FileOutputStream(exportFileName);
			workbook.write(fileOutputStream);
			fileOutputStream.close();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		} finally {
			IOUtils.closeQuietly(fileInputStream);
			IOUtils.closeQuietly(fileOutputStream);
			IOUtils.closeQuietly(opcPackage);
		}

		return outputName;
	}
	
	private void writeImportSheetHeader(XSSFWorkbook workbook, int importSheetIndex) {
		XSSFSheet importSheet = workbook.getSheetAt(importSheetIndex);
		CreationHelper factory = workbook.getCreationHelper();
		if (importSheet != null) {
//			workbook.setSheetName(importSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.product.ds.san.pham"));
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			XSSFFont commentFont = (XSSFFont) workbook.createFont();
			ExcelPOIProcessUtils.setFontPOI(commentFont, "Arial", 8, false, ExcelPOIProcessUtils.poiBlack);
			int row = 0;
			int colunm = 0;
			// thu tu
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.num.order"));
			// ma san pham
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.code"));
			// ten san pham
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.name"));
			// trang thai
			excelProcessUtil.writeCellData(importSheet, row, colunm, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.status"));
			excelProcessUtil.writeCellComment(factory, importSheet, row, colunm++, 
					Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.status.comment"), commentFont);
			// don vi tinh le
			excelProcessUtil.writeCellData(importSheet, row, colunm, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom1.excel"));
			excelProcessUtil.writeCellComment(factory, importSheet, row, colunm++, 
					Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom1.comment"), commentFont);
			// don vi tinh thung
			excelProcessUtil.writeCellData(importSheet, row, colunm, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom2.excel"));
			excelProcessUtil.writeCellComment(factory, importSheet, row, colunm++, 
					Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom2.comment"), commentFont);
			//so luong dong goi
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.proudct.convfact.sl"));
			// nganh hang
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.cat"));
			// ten nganh hang
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.cat.name"));
			// nganh hang con
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat"));
			// ten nganh hang con
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat.name"));
			// nhan hang
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.brand"));
			// ten nhan hang
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.brand.name"));
			// huong vi
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.flavour"));
			// ten huong vi
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.flavour.name"));
			// bao bi
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.packing"));
			// ten bao bi
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.packing.name"));
			// the tich
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.volumn"));
			// khoi luong tinh
			excelProcessUtil.writeCellData(importSheet, row, colunm, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.netweight"));
			excelProcessUtil.writeCellComment(factory, importSheet, row, colunm++, 
					Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.netweight.comment"), commentFont);
			// tong khoi luong
			excelProcessUtil.writeCellData(importSheet, row, colunm, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.totalweight"));
			excelProcessUtil.writeCellComment(factory, importSheet, row, colunm++, 
					Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.totalweight.comment"), commentFont);
			// ma vach
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.barcode.excel"));
			// han su dung
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.expiry.excel"));
			// han su dung (DVT)			
			excelProcessUtil.writeCellData(importSheet, row, colunm++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.expiry.dvt"));
			
		}
	}*/
	
	/**
	 * Nhap danh sach san pham tu excel
	 * 
	 * @author trietptm
	 * @since 08/06/2015
	 */
	public String importExcelFile() {
		resetToken(result);
		totalItem = 0;
		String message = "";
		String value = "";
		Product product;
		try {			
			lstView = new ArrayList<CellBean>();
			
			List<Object> dynamicAttributeVOs = getProductDynamicAttributeVO();
			final int TOTAL_DYNAMIC_ATTRIBUTE = dynamicAttributeVOs != null ? dynamicAttributeVOs.size() : 0;
			final int START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX = 23;
			
			int totalStaticCol = 24;
			final int startDataRowIndex = 1;
			
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, totalStaticCol + TOTAL_DYNAMIC_ATTRIBUTE, startDataRowIndex);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(currentUser.getUserName());
//				List<ApParam> lstUOM1 = apParamMgr.getListUoms(ApParamType.UOM1);
//				List<ApParam> lstUOM2 = apParamMgr.getListUoms(ApParamType.UOM2);
				List<ApParam> lstUOM = apParamMgr.getListUoms(ApParamType.UOM);
				lstNgayThangNam =  apParamMgr.getListApParam(ApParamType.NGAY_THANG_NAM, ActiveType.RUNNING);
				for (int i = 0; i < lstData.size(); i++) {					
					List<String> row = lstData.get(i);
					if (row != null) {
						int totalColumnInRow = row.size();
						product = new Product();
						Boolean isCreateNew = true;
						status = -1;		
						message = "";
						totalItem++;
						
						// Thu tu
						value = row.get(0).trim();
						String msg = ValidateUtil.validateField(value, "catalog.product.num.order", 9, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
						if (StringUtil.isNullOrEmpty(msg)) {
							orderIndex = NumberUtil.tryParseInteger(value);
							if(orderIndex < 1) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.order.index.more.than.zero");
								message += "\n";
							} else {
								product.setOrderIndex(orderIndex);
							}
						} else {
							message += msg;
						}					
						
						// ma san pham
						value = row.get(1).trim();
						msg = ValidateUtil.validateField(value, "catalog.product.code", 50, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(msg)) {
							product = productMgr.getProductByCode(value, null);
							if (product != null && product.getId() != null && product.getId() > 0L) {
								isCreateNew = false;
							} else {
								isCreateNew = true;
								product = new Product();
								product.setProductCode(value);
							}
							product.setOrderIndex(orderIndex);
						} else {
							message += msg;
						}
						
						// ten san pham
						value = row.get(2).trim();
						msg = ValidateUtil.validateField(value, "catalog.product.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(msg)) {
							product.setProductName(value);
							List<Product> lstTest = null;
							if (isCreateNew) {
								lstTest = productMgr.getListProductsByName(value, null);
							} else {
								lstTest = productMgr.getListProductsByName(value, product.getId());
							}
							if (lstTest != null && lstTest.size() > 0) {
				    			message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.name.exist.db");
				    			message += "\n";
				    		}
						} else {
							message += msg;
						}
						
						// trang thai
						value = row.get(3).trim();
						msg = ValidateUtil.validateField(value, "catalog.product.status", 1, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_INTEGER_ZERO_OR_ONE);
						if (StringUtil.isNullOrEmpty(msg)) {
							if (value.equals(ActiveType.RUNNING.getValue().toString())) {
								product.setStatus(ActiveType.RUNNING);									
							} else if (value.equals(ActiveType.STOPPED.getValue().toString())){
								product.setStatus(ActiveType.STOPPED);									
							}
						} else {
							message += msg;
						}
						
						//don vi tinh le
						value = row.get(4).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							boolean isHas = false; 
							for (ApParam apParam: lstUOM) {
								if (apParam.getApParamName().equals(value)) {
									isHas = true;
									product.setUom1(apParam.getApParamCode());
									break;
								}
							}
							if (!isHas) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.product.uom1.excel");
							}
						} else {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.product.uom1.excel");
						}
						
						// don vi tinh thung
						value = row.get(5).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							boolean isHas = false; 
							for (ApParam apParam: lstUOM) {
								if (apParam.getApParamName().equals(value)) {
									isHas = true;
									product.setUom2(apParam.getApParamCode());
									break;
								}
							}
							if (!isHas) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.product.uom2.excel");
							}
						} else {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.product.uom2.excel");
						}
												
						// so luong dong goi
						value = row.get(6).trim();
						if (StringUtil.isNullOrEmpty(value)) {
							message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.proudct.convfact.sl");
						} else {
							value = value.replaceAll(",", "").trim();
							msg = ValidateUtil.validateField(value, "catalog.proudct.convfact.sl", 8, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(msg)) {								
								int convfact = Integer.valueOf(value);
								if (convfact < 1) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.proudct.convfact.more.than.zero");
									message += "\n";
								} else {
									product.setConvfact(convfact);;
								}
							} else {
								message += msg;
							}
						}
						
						// nganh hang
						boolean isHasCat = false;
						value = row.get(7).trim();
						msg = ValidateUtil.validateField(value, "catalog.product.cat", 18, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
						if (StringUtil.isNullOrEmpty(msg)) {
							ProductInfo cat = productInfoMgr.getProductInfoByCode(value, ProductType.CAT, null, true);
							if (cat != null && cat.getId() != null && cat.getId() > 0L) {
								product.setCat(cat);
								isHasCat = true;
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.product.cat");
								isHasCat = false;
							}
						} else {
							message += msg;
						}
						
						// nganh hang con
						if (isHasCat) {
							value = row.get(9).trim();
							if (StringUtil.isNullOrEmpty(value)) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.product.sub.cat");
							} else {
								msg = ValidateUtil.validateField(value, "catalog.product.sub.cat", 18, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (StringUtil.isNullOrEmpty(msg)) {
									ProductInfo subcat = productInfoMgr.getProductInfoByCode(value, ProductType.SUB_CAT, product.getCat().getProductInfoCode(), true);
									if (subcat != null && subcat.getId() != null) {
										product.setSubCat(subcat);
									} else {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat.not.in.cat");
									}
								} else {
									message += msg;
								}
							}
						}
						
						// nhan hang
						value = row.get(11).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "catalog.product.brand", 18, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								ProductInfo productInfo = productInfoMgr.getProductInfoByCode(value, ProductType.BRAND, null, true);
								if (productInfo != null && productInfo.getId() != null) {
									product.setBrand(productInfo);
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.product.brand");
								}
							} else {
								message += msg;
							}
						}
						
						// huong vi
						value = row.get(13).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "catalog.product.flavour", 18, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								ProductInfo productInfo = productInfoMgr.getProductInfoByCode(value, ProductType.FLAVOUR, null, true);
								if (productInfo != null && productInfo.getId() != null) {
									product.setFlavour(productInfo);
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.product.flavour");
								}
							} else {
								message += msg;
							}
						}
						
						// bao bi
						value = row.get(15).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							msg = ValidateUtil.validateField(value, "catalog.product.packing", 18, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								ProductInfo productInfo = productInfoMgr.getProductInfoByCode(value, ProductType.PACKING, null, true);
								if (productInfo != null && productInfo.getId() != null) {
									product.setPacking(productInfo);
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.product.packing");
								}
							} else {
								message += msg;
							}
						}
						
						// the tich
						value = row.get(17).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.replaceAll(",", "").trim();
							msg = ValidateUtil.validateField(value, "catalog.product.volumn", 7, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FLOAT_NUMBER);
							if (StringUtil.isNullOrEmpty(msg)) {								
								Double volumn = NumberUtil.tryParseDouble(value);
								if (volumn != null) {
									product.setVolumn(volumn);
								}
							} else {
								message += msg;
							}
						}
						
						// khoi luong tinh
						value = row.get(18).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.replaceAll(",", "").trim();
							msg = ValidateUtil.validateField(value, "catalog.product.netweight", 8, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FLOAT_NUMBER);
							if (StringUtil.isNullOrEmpty(msg)) {								
								BigDecimal netWeight = new BigDecimal(value);
								if (netWeight != null) {
									product.setNetWeight(netWeight);
								}
							} else {
								message += msg;
							}
						}
						
						// tong khoi luong 
						value = row.get(19).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.replaceAll(",", "").trim();
							msg = ValidateUtil.validateField(value, "catalog.product.totalweight", 9, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_FLOAT_NUMBER);
							if (StringUtil.isNullOrEmpty(msg)) {								
								BigDecimal grossWeight = new BigDecimal(value);
								if (grossWeight != null) {
									product.setGrossWeight(grossWeight);
								}
							} else {
								message += msg;
							}
						}
						
						// ma vach
						value = row.get(20).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.replaceAll(",", "").trim();
							msg = ValidateUtil.validateField(value, "catalog.product.barcode.excel", 50, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(msg)) {								
								product.setBarcode(value);
							} else {
								message += msg;
							}
						}
						
						// han su dung
						boolean isHasExpiry = false;
						value = row.get(21).trim();
						if (!StringUtil.isNullOrEmpty(value)) {
							value = value.replaceAll(",", "").trim();
							msg = ValidateUtil.validateField(value, "catalog.product.expiry.excel", 3, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(msg)) {								
								Integer expiry = NumberUtil.tryParseInteger(value);
								if (expiry != null) {
									product.setExpiryDate(expiry);
									isHasExpiry = true;
								}
							} else {
								message += msg;
							}
						}
						
						// DVT han su dung
						value = row.get(22).trim();
						if (isHasExpiry && StringUtil.isNullOrEmpty(value)) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.expiry.dvt.mandatory");
							message += "\n";
						} else if (!StringUtil.isNullOrEmpty(value)) {
							boolean isHas = false;								
							if (lstNgayThangNam != null && lstNgayThangNam.size() > 0) {
								for (ApParam apParam: lstNgayThangNam) {
									if (apParam.getApParamName().equalsIgnoreCase(value)) {
										product.setExpiryType(Integer.valueOf(apParam.getValue()));
										isHas = true;
										break;
									}
								}
							}
							if (!isHas) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.product.expiry.dvt");
							}
						}
						
						lstAttributeId = new ArrayList<Long>();
						lstAttributeValue = new ArrayList<String>();
						lstAttributeColumnValueType = new ArrayList<Integer>();
						List<String> dynamicAttributeColumnDataInImportFile = new ArrayList<String>();
						for (int j = START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX, lastDataColumnIndex = START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX + TOTAL_DYNAMIC_ATTRIBUTE
								; j <= lastDataColumnIndex; j++) {
							int dynamicAttributeSequence = j - START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX;
							if (totalColumnInRow > j) {
								String cellData = row.get(j);
								dynamicAttributeColumnDataInImportFile.add(cellData);
								AttributeDynamicVO dynamicAttributeVO = TOTAL_DYNAMIC_ATTRIBUTE > dynamicAttributeSequence ? (AttributeDynamicVO) dynamicAttributeVOs.get(dynamicAttributeSequence) : null;
								String errMsg = DynamicAttributeValidator.validateDynamicAttributeColumnData(row, j, dynamicAttributeVO);
								if (!StringUtil.isNullOrEmpty(errMsg)) {
									message += errMsg + ConstantManager.NEW_LINE_CHARACTER;
								}
								if (dynamicAttributeVO != null) {
									lstAttributeId.add(dynamicAttributeVO.getAttributeId());
									lstAttributeValue.add(DynamicAttributeValidator.getInputDynamicAttributeValue(row, j, dynamicAttributeVO));
									lstAttributeColumnValueType.add(dynamicAttributeVO.getType());
								}
							}
						}
						
						//Xy ly ghi du lieu DB
						if (StringUtil.isNullOrEmpty(message.trim())) {
							productMgr.saveOrUpdateProduct(product, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, logInfo);
							/*if (isCreateNew) {
								productMgr.createProduct(product, logInfo);
							} else {
								//Cap nhat san pham
								productMgr.updateProduct(product, logInfo);
							}*/
						} else {
							message = message.replace(ConstantManager.NEW_LINE_CHARACTER + ConstantManager.NEW_LINE_CHARACTER, ConstantManager.NEW_LINE_CHARACTER);
							CellBean failBean = StringUtil.addFailBean(row, message);
							failBean.setLstDynamic(dynamicAttributeColumnDataInImportFile);
							lstView.add(failBean);
						}
					}
				}
				
				if (lstView != null && lstView.size() > 0) {
					//Export error
					Map<String, Object> beans = new HashMap<String, Object>();					
					beans.put("orderIndex", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.order.index"));
					beans.put("productCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.code"));
					beans.put("productName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.name"));
					beans.put("status", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.status"));
					beans.put("uom1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom1.excel"));
					beans.put("uom2", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom2.excel"));
					beans.put("convfact", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.proudct.convfact.sl"));
					
					beans.put("cat", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.cat"));
					beans.put("catName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.cat.name"));
					
					beans.put("subcat", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat"));
					beans.put("subcatName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat.name"));
					
					beans.put("brand", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.brand"));
					beans.put("brandName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.brand.name"));
					
					beans.put("flavour", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.flavour"));
					beans.put("flavourName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.flavour.name"));
					
					beans.put("packing", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.packing"));
					beans.put("packingName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.packing.name"));
					
					beans.put("volume", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.volumn"));
					beans.put("netWeight", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.netweight"));
					beans.put("grossWeight", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.totalweight"));
					beans.put("barcode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.barcode.excel"));
					beans.put("expiryDate", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.product.han.su.dung"));
					beans.put("expiryType", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.expiry.dvt"));
					beans.put("note", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.note"));
					beans.put("STT", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.stt"));
					
					String exportFailTemplate = ConstantManager.TEMPLATE_PRODUCT_FAIL_WITHOUT_DYNAMIC_ATTRIBUTE;
					if (dynamicAttributeVOs != null && !dynamicAttributeVOs.isEmpty()) {
						beans.put("dynamicAttribute", dynamicAttributeVOs);
						exportFailTemplate = ConstantManager.TEMPLATE_PRODUCT_FAIL;
					}
					getOutputFailExcelFileMultiLanguage(lstView, exportFailTemplate, beans, ConstantManager.TEMPLATE_PRODUCT_FAIL);	
				}			
			} else {
				isError = true;
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "not.data.excel");
			}
		}
		catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		
		return SUCCESS;
	}
	
	/**
	 * Xuat danh sach san pham ra excel
	 * 
	 * @author lacnv1
	 * @return json
	 * @since Nov 21, 2013
	 */
	public String exportProduct() throws Exception {
		result.put(ERROR, false);
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			ActiveType temp = null;
	    	if(status != null && status != ConstantManager.NOT_STATUS && status != ConstantManager.DELETED){
	    		temp = ActiveType.parseValue(status);
	    	}
	    	ProductFilter pf = new ProductFilter();
	    	pf.setkPaging(null);
	    	pf.setProductCode(productCode);
	    	pf.setProductName(productName);
	    	pf.setStatus(temp);
	    	pf.setUom1(uom1);
	    	pf.setLstCategoryId(lstCategoryId);
	    	pf.setLstSubCategoryId(lstSubCategoryId);
	    	pf.setBrandId(brandId);
	    	pf.setFlavourId(flavourId);
	    	pf.setPackingId(packingId);
	    	if(isPriceValid != null){
	    		pf.setIsPriceValid(isPriceValid);
	    	}
	    	if(lstProductCode != null && lstProductCode.size() >0 && !StringUtil.isNullOrEmpty(lstProductCode.get(0))){
	    		lstProductCode.get(0).split(",");
	    		List<String> tmp = new ArrayList<String>(); 
	    		for(String code : lstProductCode.get(0).split(",")) {
	    			tmp.add(code);
	    		}
	    		pf.setLstProductCodeSelected(tmp);
	    	}
	    	
	    	List<ProductExportVO> products = productMgr.getProductWithDynamicAttributeInfo(pf);
			Map<String, Object> beans = new HashMap<String, Object>();
			if (products != null && products.size() > 0) {
				lstNgayThangNam = apParamMgr.getListApParam(ApParamType.NGAY_THANG_NAM, ActiveType.RUNNING);
		    	for (int i = 0; i < products.size(); i++) {
					ProductExportVO product = products.get(i);
					for (int j = 0, m = lstNgayThangNam.size(); j < m; j++) {
						ApParam apParam = lstNgayThangNam.get(j);
						if (apParam.getValue().equals(product.getExpiryType())) {
							product.setExpiryType(apParam.getApParamName());
							break;
						}
					}
				}
		    	
				beans.put("hasData", 1);
				setProductFieldText(products);
				beans.put("lstProducts", products);
			} else {
				beans.put("hasData", 0);
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,
						"ss.common.not.data"));
				return JSON;
			}
			
			beans.put("numOrder", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.numerical.order"));
			beans.put("infoProduct", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.product.thong.tin.san.pham"));
			beans.put("productCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.code"));
			beans.put("productName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.name"));
			beans.put("uom1", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom1.excel"));
			beans.put("uom2", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.uom2.excel"));
			beans.put("convfact", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.proudct.convfact.sl"));
			beans.put("category", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.cat"));
			beans.put("categoryName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.cat.name"));
			beans.put("subcat", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat"));
			beans.put("subcatName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.sub.cat.name"));
			beans.put("brand", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.brand"));
			beans.put("brandName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.brand.name"));
			beans.put("flavour", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.flavour"));
			beans.put("flavourName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.flavour.name"));
			beans.put("packing", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.packing"));
			beans.put("packingName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.packing.name"));
			beans.put("orderIndex", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.order.index"));
			beans.put("volume", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.volumn"));
			beans.put("netWeight", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.netweight"));
			beans.put("grossWeight", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.totalweight"));
			beans.put("barcode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.barcode.excel"));
			beans.put("expiryDate", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.product.han.su.dung"));
			beans.put("expiryType", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.expiry.dvt"));
			beans.put("status", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.status"));
			beans.put("dynamicAttributeHeader", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.import.template.header.dynamic.attribute"));
			
			List<Object> attributeDynamicVO = getProductDynamicAttributeVO();
			beans.put("dynamicAttribute", attributeDynamicVO);
			
			String outputPath = exportExcelJxls(beans, Configuration.getExcelTemplatePathProduct(), ConstantManager.TEMPLATE_PRODUCT_EXPORT);
			result.put(ERROR, false);
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(ex, ex.getMessage());
		}
		return JSON;
	}
	
	private void setProductFieldText(List<ProductExportVO> products) {
		for (ProductExportVO productExportVO : products) {
			setProductStatusText(productExportVO);
		}
	}

	private void setProductStatusText(ProductExportVO productExportVO) {
		String statusDescription = "";
		if (ActiveType.RUNNING.getValue().toString().equals(productExportVO.getStatus())) {
			statusDescription = R.getResource("jsp.common.status.active");
		} else if (ActiveType.STOPPED.getValue().toString().equals(productExportVO.getStatus())) {
			statusDescription = R.getResource("jsp.common.status.stoped");
		} else if (ActiveType.WAITING.getValue().toString().equals(productExportVO.getStatus())) {
			statusDescription = R.getResource("jsp.common.status.draft");
		} else if (ActiveType.REJECTED.getValue().toString().equals(productExportVO.getStatus())) {
			statusDescription = R.getResource("jsp.common.status.rejected");
		}
		
		productExportVO.setStatus(statusDescription);
	}


	/**
	 * Autocomplete cho san pham
	 * 
	 * @author
	 * @since
	 * @return the string
	 */
	public String autoCompleteForProduct(){		
		if(productMgr!= null){
			try {
				Map<String,String[]> mapParams =  request.getParameterMap();
				String productCode =  mapParams.get("filter[filters][0][value]")[0];
				String str = mapParams.get("isExceptZCat")[0];
				if(!StringUtil.isNullOrEmpty(productCode)){
					productCode = productCode.trim();
				}
				if("false".equals(str)){
					isExceptZCat = false;
				}else{
					isExceptZCat = true;
				}
				lstProducts = new ArrayList<GeneralProductVO>();
				KPaging<GeneralProductVO> kPaging = new KPaging<GeneralProductVO>();
				kPaging.setPageSize(100);
				lstProducts = productMgr.getAllProducts(productCode,kPaging,isExceptZCat);
			} catch (Exception e) {
//				logInfo.setFunctionType(FunctionType.READ);
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.autoCompleteForProduct()"), createLogErrorStandard(actionStartTime));
			}
		}			
		return JSON;
	}
	
	/**
	 * Lay danh sach san pham goc (convfact=1)
	 * 
	 * @author lacnv1
	 * @since Dec 10, 2013
	 */
	public String getParentProducts() throws Exception {
		result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<Product> paging = new KPaging<Product>();
			paging.setPageSize(max);
			paging.setPage(page-1);

			//ObjectVO<Product> vo = productMgr.getParentProducts(paging, code, name);
			ObjectVO<Product> vo = null;
			if(vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
			    result.put("rows", vo.getLstObject());
			} else {
		    	result.put("total", 0);
			    result.put("rows", new ArrayList<Product>());
			}

	    }catch (Exception e) {
//	    	logInfo.setFunctionType(FunctionType.READ);
	    	LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.ProductCatalogAction.getParentProducts()"), createLogErrorStandard(actionStartTime));
	    	result.put("total", 0);
		    result.put("rows", new ArrayList<Product>());
	    }
	    return JSON;
	}
	
	private String code;
	private String name;
	
	private Boolean isOrderByCode;
	
	/**
	 * Lay danh sach product info (nganh hang, nhan hieu, huong vi)
	 * @author lacnv1
	 * @return JSON
	 * @since Nov 22, 2013
	 */
	public String getListCategories() {
		result.put(ERROR, false);
		try {
			int itype = 1;
			if (status != null) {
				itype = status.intValue();
			}
			ProductType type = ProductType.parseValue(itype);
			List<ProductInfoObjectType> objTypes = new ArrayList<ProductInfoObjectType>();
			objTypes.add(ProductInfoObjectType.SP);
			boolean orderByCode = false;
			if (isOrderByCode != null && isOrderByCode) {
				orderByCode = true;
			}
			
			ObjectVO<ProductInfo> lstCategoryTmp = productInfoMgr.getListProductInfoEx(null, null,
					null, ActiveType.RUNNING, type, objTypes, orderByCode);
			List<ProductInfo> lst = lstCategoryTmp.getLstObject();
			if (lst == null) {
				lst = new ArrayList<ProductInfo>();
			}
			result.put("rows", lst);
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Lay danh sach don vi tinh
	 * 
	 * @author lacnv1
	 * @return json
	 * @since Nov 23, 2013
	 */
	public String getListUoms() throws Exception {
		result.put(ERROR, false);
		try {
			List<ApParam> lst = null;
			ApParamType type = null;
			if (!StringUtil.isNullOrEmpty(uom1)) {
				type = ApParamType.parseValue(uom1.toUpperCase());
			}
			if (type != null) {
				lst = apParamMgr.getListUoms(type);
			}
			if (lst == null) {
				lst = new ArrayList<ApParam>();
			}
			result.put("lstUom", lst);
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Lay danh sach product info (nganh hang, nhan hieu, huong vi)
	 * @author lacnv1
	 * @return JSON
	 * @since Nov 22, 2013
	 */
	public String getListCategoriesChild() {
		result.put(ERROR, false);
		try {
			int itype = 2;
			if (status != null) {
				itype = status.intValue();
			}
			ProductType type = ProductType.parseValue(itype);
			List<ProductInfo> lstCategoryChildTmp;
			if (productInfoId != null && productInfoId > 0L) {
				lstCategoryChildTmp = productInfoMgr.getListSubCat(ActiveType.RUNNING, type, productInfoId);
			} else {
				lstCategoryChildTmp = productInfoMgr.getListSubCatByListCat(ActiveType.RUNNING, type, lstProductInfoId);
			}
			if (lstCategoryChildTmp == null) {
				lstCategoryChildTmp = new ArrayList<ProductInfo>();
			}
			result.put("rows", lstCategoryChildTmp);
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * check trung so thu tu 
	 * @author trietptm
	 * @return JSON
	 * @since 17/06/2015
	 */
	public String checkDulicateOrderIndex() {
		result.put(ERROR, false);
		try {
			result.put("isDulicate", productMgr.checkDuplicateOrderIndex(productId, orderIndex));
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public File getExcelFile() {
		return excelFile;
	}


	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}


	public String getExcelFileContentType() {
		return excelFileContentType;
	}


	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}


	public Long getProductId() {
		return productId;
	}


	public void setProductId(Long productId) {
		this.productId = productId;
	}


	public Long getShopId() {
		return shopId;
	}


	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}


	public ProductMgr getProductMgr() {
		return productMgr;
	}


	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}


	public String getProductCode() {
		return productCode;
	}


	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	/*public String getShortName() {
		return shortName;
	}*/


	/*public void setShortName(String shortName) {
		this.shortName = shortName;
	}*/


	public List<String> getLstBaoBi() {
		return lstBaoBi;
	}


	public void setLstBaoBi(List<String> lstBaoBi) {
		this.lstBaoBi = lstBaoBi;
	}


	public String getShopCode() {
		return shopCode;
	}


	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}


	public Integer getQuantityMax() {
		return quantityMax;
	}


	public void setQuantityMax(Integer quantityMax) {
		this.quantityMax = quantityMax;
	}


	public Integer getGridPage() {
		return gridPage;
	}


	public void setGridPage(Integer gridPage) {
		this.gridPage = gridPage;
	}


	public Long getCategory() {
		return category;
	}


	public void setCategory(Long category) {
		this.category = category;
	}


	public Long getSubCat() {
		return subCat;
	}


	public void setSubCat(Long subCat) {
		this.subCat = subCat;
	}


	public Integer getProductType() {
		return productType;
	}


	public void setProductType(Integer productType) {
		this.productType = productType;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public List<ProductInfo> getLstCategory() {
		return lstCategory;
	}


	public void setLstCategory(List<ProductInfo> lstCategory) {
		this.lstCategory = lstCategory;
	}


	public ProductInfoMgr getProductInfoMgr() {
		return productInfoMgr;
	}


	public void setProductInfoMgr(ProductInfoMgr productInfoMgr) {
		this.productInfoMgr = productInfoMgr;
	}


	public List<Product> getLstProductCodeOrigin() {
		return lstProductCodeOrigin;
	}


	public void setLstProductCodeOrigin(List<Product> lstProductCodeOrigin) {
		this.lstProductCodeOrigin = lstProductCodeOrigin;
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public String getParentProductCode() {
		return parentProductCode;
	}


	public void setParentProductCode(String parentProductCode) {
		this.parentProductCode = parentProductCode;
	}


	public String getProductTypeCode() {
		return productTypeCode;
	}


	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}


	public List<ProductInfo> getLstFlavour() {
		return lstFlavour;
	}


	public void setLstFlavour(List<ProductInfo> lstFlavour) {
		this.lstFlavour = lstFlavour;
	}


	public String getBarCode() {
		return barCode;
	}


	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}


	public Long getLevelId() {
		return levelId;
	}


	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}


	public Long getFlavourId() {
		return flavourId;
	}


	public void setFlavourId(Long flavourId) {
		this.flavourId = flavourId;
	}


	public String getUom1() {
		return uom1;
	}


	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}


	public String getUom2() {
		return uom2;
	}


	public void setUom2(String uom2) {
		this.uom2 = uom2;
	}


	public Integer getConvfact() {
		return convfact;
	}


	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}


	public String getTaxId() {
		return taxId;
	}


	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}


	public String getParentCode() {
		return parentCode;
	}


	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}


	public String getTypeCode() {
		return typeCode;
	}


	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}


	public Long getGroupId() {
		return groupId;
	}


	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}


	public Long getPriceId() {
		return priceId;
	}


	public void setPriceId(Long priceId) {
		this.priceId = priceId;
	}


	public Long getDepartmentId() {
		return departmentId;
	}


	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}


	public Price getpPrice() {
		return pPrice;
	}


	public void setpPrice(Price pPrice) {
		this.pPrice = pPrice;
	}


	public BigDecimal getPrice() {
		return price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getNetWeight() {
		return netWeight;
	}


	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}


	public BigDecimal getGrossWeight() {
		return grossWeight;
	}


	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}


	public Integer getCheckLot() {
		return checkLot;
	}


	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}


	public ApParamMgr getApParamMgr() {
		return apParamMgr;
	}


	public void setApParamMgr(ApParamMgr apParamMgr) {
		this.apParamMgr = apParamMgr;
	}


	public List<ApParam> getLstUom1() {
		return lstUom1;
	}


	public void setLstUom1(List<ApParam> lstUom1) {
		this.lstUom1 = lstUom1;
	}


	public List<ApParam> getLstUom2() {
		return lstUom2;
	}


	public void setLstUom2(List<ApParam> lstUom2) {
		this.lstUom2 = lstUom2;
	}


	public Boolean getIsExceptZCat() {
		return isExceptZCat;
	}


	public void setIsExceptZCat(Boolean isExceptZCat) {
		this.isExceptZCat = isExceptZCat;
	}


	public Boolean getIsPriceValid() {
		return isPriceValid;
	}


	public void setIsPriceValid(Boolean isPriceValid) {
		this.isPriceValid = isPriceValid;
	}


	public Integer getTotalMediaItem() {
		return totalMediaItem;
	}


	public void setTotalMediaItem(Integer totalMediaItem) {
		this.totalMediaItem = totalMediaItem;
	}


	public Integer getVnmTotalImage() {
		return vnmTotalImage;
	}


	public void setVnmTotalImage(Integer vnmTotalImage) {
		this.vnmTotalImage = vnmTotalImage;
	}


	public Integer getVnmTotalVideo() {
		return vnmTotalVideo;
	}


	public void setVnmTotalVideo(Integer vnmTotalVideo) {
		this.vnmTotalVideo = vnmTotalVideo;
	}


	public List<String> getLstProductCode() {
		return lstProductCode;
	}


	public void setLstProductCode(List<String> lstProductCode) {
		this.lstProductCode = lstProductCode;
	}


	public CommonMgr getCommonMgr() {
		return commonMgr;
	}


	public void setCommonMgr(CommonMgr commonMgr) {
		this.commonMgr = commonMgr;
	}


	public List<Product> getLstProductType() {
		return lstProductType;
	}


	public void setLstProductType(List<Product> lstProductType) {
		this.lstProductType = lstProductType;
	}


	public List<Long> getLstCategoryId() {
		return lstCategoryId;
	}


	public void setLstCategoryId(List<Long> lstCategoryId) {
		this.lstCategoryId = lstCategoryId;
	}


	public Integer getCheckPermission() {
		return checkPermission;
	}


	public void setCheckPermission(Integer checkPermission) {
		this.checkPermission = checkPermission;
	}


	public Price getPrice1() {
		return price1;
	}


	public void setPrice1(Price price1) {
		this.price1 = price1;
	}


	public Price getPrice2() {
		return price2;
	}


	public void setPrice2(Price price2) {
		this.price2 = price2;
	}


	public Integer getExpireNumber() {
		return expireNumber;
	}


	public void setExpireNumber(Integer expireNumber) {
		this.expireNumber = expireNumber;
	}


	public Integer getExpireType() {
		return expireType;
	}


	public void setExpireType(Integer expireType) {
		this.expireType = expireType;
	}


	public Integer getDayMonth() {
		return dayMonth;
	}


	public void setDayMonth(Integer dayMonth) {
		this.dayMonth = dayMonth;
	}


	public Long getProductInfoId() {
		return productInfoId;
	}


	public void setProductInfoId(Long productInfoId) {
		this.productInfoId = productInfoId;
	}


	public List<ApParam> getLstDepartment() {
		return lstDepartment;
	}


	public void setLstDepartment(List<ApParam> lstDepartment) {
		this.lstDepartment = lstDepartment;
	}


	public List<ApParam> getLstActiveType() {
		return lstActiveType;
	}


	public void setLstActiveType(List<ApParam> lstActiveType) {
		this.lstActiveType = lstActiveType;
	}


	public Integer getExcelType() {
		return excelType;
	}


	public void setExcelType(Integer excelType) {
		this.excelType = excelType;
	}


	public String getDepartmentCode() {
		return departmentCode;
	}


	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}


	public List<ApParam> getLstNgayThangNam() {
		return lstNgayThangNam;
	}


	public void setLstNgayThangNam(List<ApParam> lstNgayThangNam) {
		this.lstNgayThangNam = lstNgayThangNam;
	}


	public List<ApParam> getLstCheckLot() {
		return lstCheckLot;
	}


	public void setLstCheckLot(List<ApParam> lstCheckLot) {
		this.lstCheckLot = lstCheckLot;
	}


	public BigDecimal getPriceNotVat() {
		return priceNotVat;
	}


	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public File getVideoFile() {
		return videoFile;
	}


	public void setVideoFile(File videoFile) {
		this.videoFile = videoFile;
	}


	public File getImageFile() {
		return imageFile;
	}


	public void setImageFile(File imageFile) {
		this.imageFile = imageFile;
	}


	public Long getIntroductionId() {
		return introductionId;
	}


	public void setIntroductionId(Long introductionId) {
		this.introductionId = introductionId;
	}


	public ProductIntroduction getProductIntroduction() {
		return productIntroduction;
	}


	public void setProductIntroduction(ProductIntroduction productIntroduction) {
		this.productIntroduction = productIntroduction;
	}


	public MediaItem getMediaItem() {
		return mediaItem;
	}


	public void setMediaItem(MediaItem mediaItem) {
		this.mediaItem = mediaItem;
	}


	public List<MediaItem> getLstMediaItem() {
		return lstMediaItem;
	}


	public void setLstMediaItem(List<MediaItem> lstMediaItem) {
		this.lstMediaItem = lstMediaItem;
	}


	public List<ProductInfo> getLstBrand() {
		return lstBrand;
	}


	public void setLstBrand(List<ProductInfo> lstBrand) {
		this.lstBrand = lstBrand;
	}


	public Long getBrandId() {
		return brandId;
	}


	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}


	public List<String> getLstApParamCode() {
		return lstApParamCode;
	}


	public void setLstApParamCode(List<String> lstApParamCode) {
		this.lstApParamCode = lstApParamCode;
	}


	public List<String> getLstApParamName() {
		return lstApParamName;
	}


	public void setLstApParamName(List<String> lstApParamName) {
		this.lstApParamName = lstApParamName;
	}


	public String getApParamCode() {
		return apParamCode;
	}


	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}


	public String getProductTypeText() {
		return productTypeText;
	}


	public void setProductTypeText(String productTypeText) {
		this.productTypeText = productTypeText;
	}


	public String getCommission() {
		return commission;
	}


	public void setCommission(String commission) {
		this.commission = commission;
	}


	public BigDecimal getVolumn() {
		return volumn;
	}


	public void setVolumn(BigDecimal volumn) {
		this.volumn = volumn;
	}


	public Integer getMediaType() {
		return mediaType;
	}


	public void setMediaType(Integer mediaType) {
		this.mediaType = mediaType;
	}


	public Long getMediaItemId() {
		return mediaItemId;
	}


	public void setMediaItemId(Long mediaItemId) {
		this.mediaItemId = mediaItemId;
	}


	public Integer getNumIndex() {
		return numIndex;
	}


	public void setNumIndex(Integer numIndex) {
		this.numIndex = numIndex;
	}


	public Boolean getIsViewProduct() {
		return isViewProduct;
	}


	public void setIsViewProduct(Boolean isViewProduct) {
		this.isViewProduct = isViewProduct;
	}


	public List<MediaItem> getLstItem() {
		return lstItem;
	}


	public void setLstItem(List<MediaItem> lstItem) {
		this.lstItem = lstItem;
	}


	public String getStatusS() {
		return statusS;
	}


	public void setStatusS(String statusS) {
		this.statusS = statusS;
	}


	public List<ApParam> getLstApParam() {
		return lstApParam;
	}


	public void setLstApParam(List<ApParam> lstApParam) {
		this.lstApParam = lstApParam;
	}


	public Long getActionId() {
		return actionId;
	}


	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}


	public String getProductLevelCode() {
		return productLevelCode;
	}


	public void setProductLevelCode(String productLevelCode) {
		this.productLevelCode = productLevelCode;
	}


	public String getProductLevelName() {
		return productLevelName;
	}


	public void setProductLevelName(String productLevelName) {
		this.productLevelName = productLevelName;
	}


	public String getProductFlavourCode() {
		return productFlavourCode;
	}


	public void setProductFlavourCode(String productFlavourCode) {
		this.productFlavourCode = productFlavourCode;
	}


	public String getProductFlavourName() {
		return productFlavourName;
	}


	public void setProductFlavourName(String productFlavourName) {
		this.productFlavourName = productFlavourName;
	}


	public String getProductBrandCode() {
		return productBrandCode;
	}


	public void setProductBrandCode(String productBrandCode) {
		this.productBrandCode = productBrandCode;
	}


	public String getProductBrandName() {
		return productBrandName;
	}


	public void setProductBrandName(String productBrandName) {
		this.productBrandName = productBrandName;
	}


	public String getApParamsUOM1Code() {
		return apParamsUOM1Code;
	}


	public void setApParamsUOM1Code(String apParamsUOM1Code) {
		this.apParamsUOM1Code = apParamsUOM1Code;
	}


	public String getApParamsUOM2Code() {
		return apParamsUOM2Code;
	}


	public void setApParamsUOM2Code(String apParamsUOM2Code) {
		this.apParamsUOM2Code = apParamsUOM2Code;
	}


	public String getExpiryType() {
		return expiryType;
	}


	public void setExpiryType(String expiryType) {
		this.expiryType = expiryType;
	}


	public List<String> getLstcategory() {
		return lstcategory;
	}


	public void setLstcategory(List<String> lstcategory) {
		this.lstcategory = lstcategory;
	}


	public List<Long> getListcategory() {
		return listcategory;
	}


	public void setListcategory(List<Long> listcategory) {
		this.listcategory = listcategory;
	}


	public List<GeneralProductVO> getLstProducts() {
		return lstProducts;
	}


	public void setLstProducts(List<GeneralProductVO> lstProducts) {
		this.lstProducts = lstProducts;
	}


	public String getQ() {
		return q;
	}


	public void setQ(String q) {
		this.q = q;
	}


	public String getFromDate() {
		return fromDate;
	}


	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	public String getToDate() {
		return toDate;
	}


	public void setToDate(String toDate) {
		this.toDate = toDate;
	}


	public String getShowDate() {
		return showDate;
	}


	public void setShowDate(String showDate) {
		this.showDate = showDate;
	}


	public LogMgr getLogMgr() {
		return logMgr;
	}


	public void setLogMgr(LogMgr logMgr) {
		this.logMgr = logMgr;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getIsOrderByCode() {
		return isOrderByCode;
	}


	public void setIsOrderByCode(Boolean isOrderByCode) {
		this.isOrderByCode = isOrderByCode;
	}


	public List<ProductAttributeVO> getLstProductAttributes() {
		return lstProductAttributes;
	}


	public void setLstProductAttributes(
			List<ProductAttributeVO> lstProductAttributes) {
		this.lstProductAttributes = lstProductAttributes;
	}


	public List<File> getImages() {
		return images;
	}


	public void setImages(List<File> images) {
		this.images = images;
	}


	public List<String> getImagesContentType() {
		return imagesContentType;
	}


	public void setImagesContentType(List<String> imagesContentType) {
		this.imagesContentType = imagesContentType;
	}


	public List<String> getImagesFileName() {
		return imagesFileName;
	}


	public void setImagesFileName(List<String> imagesFileName) {
		this.imagesFileName = imagesFileName;
	}


	public String getShortName() {
		return shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public Integer getOrderIndex() {
		return orderIndex;
	}


	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}


	public Long getPackingId() {
		return packingId;
	}


	public void setPackingId(Long packingId) {
		this.packingId = packingId;
	}


	public List<Long> getLstProductInfoId() {
		return lstProductInfoId;
	}


	public void setLstProductInfoId(List<Long> lstProductInfoId) {
		this.lstProductInfoId = lstProductInfoId;
	}


	public List<Long> getLstSubCategoryId() {
		return lstSubCategoryId;
	}


	public void setLstSubCategoryId(List<Long> lstSubCategoryId) {
		this.lstSubCategoryId = lstSubCategoryId;
	}
	
	
	
}

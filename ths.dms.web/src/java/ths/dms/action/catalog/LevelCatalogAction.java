package ths.dms.action.catalog;

import ths.dms.web.action.general.AbstractAction;

public class LevelCatalogAction extends AbstractAction {	
	
//	/** The Constant serialVersionUID. */
//	private static final long serialVersionUID = 4320641006832030842L;
//
//	/** The id. */
//	private Long id;
//	
//	/** The code. */
//	private String code;
//	
//	/** The name. */
//	private String name;
//	
//	/** The note. */
//	private String note;
//	
//	/** The status. */
//	private String status;	
//	
//	/** The lst cat. */
//	private List<ProductInfo> lstCat;
//	
//	/** The lst sub cat. */
//	private List<ProductInfo> lstSubCat;
//	
//	/** The lst sub brand. */
//	private List<ProductInfo> lstBrand;
//	
//	/** The cat id. */
//	private Long catId;
//	
//	/** The sub cat id. */
//	private Long subCatId;
//	
//	/** The brand id. */
//	private Long brandId;
//	
//	/** The excel file. */
//	private File excelFile;
//	
//	/** The excel file content type. */
//	private String excelFileContentType;
//			
//	/** The levels mgr. */
//	private ProductLevelMgr levelsMgr;
//	
//	/** The lst level. */
//	private List<ProductLevel> lstLevel;
//	
//	/** The product mgr. */
//	private ProductInfoMgr productInfoMgr;
//
//	/**
//	 * getId
//	 * @return the id
//	 */
//	public Long getId() {
//	    return id;
//	}
//
//	/**
//	 * setId
//	 * @param id the id to set
//	 */
//	public void setId(Long id) {
//	    this.id = id;
//	}
//
//	/**
//	 * getCode
//	 * @return the code
//	 */
//	public String getCode() {
//	    return code;
//	}
//
//	/**
//	 * setCode
//	 * @param code the code to set
//	 */
//	public void setCode(String code) {
//	    this.code = code;
//	}
//
//	/**
//	 * getName
//	 * @return the name
//	 */
//	public String getName() {
//	    return name;
//	}
//
//	/**
//	 * setName
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//	    this.name = name;
//	}
//
//	/**
//	 * getNote
//	 * @return the note
//	 */
//	public String getNote() {
//	    return note;
//	}
//
//	/**
//	 * setNote
//	 * @param note the note to set
//	 */
//	public void setNote(String note) {
//	    this.note = note;
//	}
//
//	/**
//	 * getStatus
//	 * @return the status
//	 */
//	public String getStatus() {
//	    return status;
//	}
//
//	/**
//	 * setStatus
//	 * @param status the status to set
//	 */
//	public void setStatus(String status) {
//	    this.status = status;
//	}
//
//	/**
//	 * @return the lstCat
//	 */
//	public List<ProductInfo> getLstCat() {
//	    return lstCat;
//	}
//
//	/**
//	 * @param lstCat the lstCat to set
//	 */
//	public void setLstCat(List<ProductInfo> lstCat) {
//	    this.lstCat = lstCat;
//	}
//
//	/**
//	 * @return the lstSubCat
//	 */
//	public List<ProductInfo> getLstSubCat() {
//	    return lstSubCat;
//	}
//
//	/**
//	 * @param lstSubCat the lstSubCat to set
//	 */
//	public void setLstSubCat(List<ProductInfo> lstSubCat) {
//	    this.lstSubCat = lstSubCat;
//	}
//
//	/**
//	 * @return the lstBrand
//	 */
//	public List<ProductInfo> getLstBrand() {
//	    return lstBrand;
//	}
//
//	/**
//	 * @param lstBrand the lstBrand to set
//	 */
//	public void setLstBrand(List<ProductInfo> lstBrand) {
//	    this.lstBrand = lstBrand;
//	}
//
//	/**
//	 * @return the catId
//	 */
//	public Long getCatId() {
//	    return catId;
//	}
//
//	/**
//	 * @param catId the catId to set
//	 */
//	public void setCatId(Long catId) {
//	    this.catId = catId;
//	}
//
//	/**
//	 * @return the subCatId
//	 */
//	public Long getSubCatId() {
//	    return subCatId;
//	}
//
//	/**
//	 * @param subCatId the subCatId to set
//	 */
//	public void setSubCatId(Long subCatId) {
//	    this.subCatId = subCatId;
//	}
//
//	/**
//	 * @return the brandId
//	 */
//	public Long getBrandId() {
//	    return brandId;
//	}
//
//	/**
//	 * @param brandId the brandId to set
//	 */
//	public void setBrandId(Long brandId) {
//	    this.brandId = brandId;
//	}
//
//	/**
//	 * @return the excelFile
//	 */
//	public File getExcelFile() {
//	    return excelFile;
//	}
//
//	/**
//	 * @param excelFile the excelFile to set
//	 */
//	public void setExcelFile(File excelFile) {
//	    this.excelFile = excelFile;
//	}
//
//	/**
//	 * @return the excelFileContentType
//	 */
//	public String getExcelFileContentType() {
//	    return excelFileContentType;
//	}
//
//	/**
//	 * @param excelFileContentType the excelFileContentType to set
//	 */
//	public void setExcelFileContentType(String excelFileContentType) {
//	    this.excelFileContentType = excelFileContentType;
//	}
//
//	/**
//	 * @return the lstLevel
//	 */
//	public List<ProductLevel> getLstLevel() {
//		return lstLevel;
//	}
//
//	/**
//	 * @param lstLevel the lstLevel to set
//	 */
//	public void setLstLevel(List<ProductLevel> lstLevel) {
//		this.lstLevel = lstLevel;
//	}
//
//	/* (non-Javadoc)
//	 * @see ths.dms.web.action.general.AbstractAction#prepare()
//	 */
//	@Override
//	public void prepare() throws Exception {
//	    super.prepare();
//	    levelsMgr = (ProductLevelMgr)context.getBean("productLevelMgr");
//	    productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
//	    
//	}
//	
//	/* (non-Javadoc)
//	 * @see com.opensymphony.xwork2.ActionSupport#execute()
//	 */
//	@Override
//	public String execute() {	
//	    status = ActiveType.RUNNING.getValue().toString();
//	    generateToken();
//	    try{		
//			ObjectVO<ProductInfo> catVO = productInfoMgr.getListProductInfo(null, null, null,  null,ActiveType.RUNNING,ProductType.CAT);
//			if(catVO!= null){
//			    lstCat = catVO.getLstObject();
//			}
//			ObjectVO<ProductInfo>  subCatVO = productInfoMgr.getListProductInfo(null, null, null,  null,ActiveType.RUNNING,ProductType.SUB_CAT);
//			if(subCatVO!= null){
//			    lstSubCat = subCatVO.getLstObject();
//			}			
//	    }catch (Exception e) {
//		LogUtility.logError(e, e.getMessage());
//	    }	    
//	    return SUCCESS;
//	}
//	
//	/**
//	 * Search.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since 18/07/2012 
//	 */
//	public String search(){
//	    
//	    result.put("page", page);
//	    result.put("max", max);
//	    try{
//			KPaging<ProductLevel> kPaging = new KPaging<ProductLevel>();
//			kPaging.setPageSize(max);
//			kPaging.setPage(page-1);
//			if(catId == -2){
//				catId = null;
//			}
//			if(subCatId == -2){
//				subCatId = null;
//			}			
//			ObjectVO<ProductLevel> levelVO = levelsMgr.getListProductLevel(kPaging, catId, subCatId, ActiveType.parseValue(Integer.valueOf(status)), note,code,name);
//			if(levelVO!= null){
//			    result.put("total", levelVO.getkPaging().getTotalRows());
//			    result.put("rows", levelVO.getLstObject());		
//			}    		
//		    }catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//	    }
//	    return JSON;
//	}
//
//	/**
//	 * Save or update.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since 18/07/2012 
//	 */
//	public String saveOrUpdate(){
//	    
//	    resetToken(result);
//	    boolean error = true;
//	    if(currentUser!= null){
//		try{
//			ProductLevel level = levelsMgr.getProductLevelByCode(code);
//			ProductInfo cat = productInfoMgr.getProductInfoById(catId);
//			ProductInfo subCat = productInfoMgr.getProductInfoById(subCatId);			
//			if(id != null && id == 0){
//				id = null;
//			}
//			if(levelsMgr.checkIfSubCatExist(subCatId, id)){
//				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.level.sub.cat.exist"));
//			} else if(id!= null && id > 0){
//		    	level = levelsMgr.getProductLevelById(id);
//		    	level.setProductLevelCode(cat.getProductInfoCode()+"."+subCat.getProductInfoCode());
//		    	level.setProductLevelName(name);
//		    	level.setCat(cat);
//		    	level.setSubCat(subCat);				    	
//		    	level.setDescription(note);
//		    	level.setStatus(ActiveType.parseValue(Integer.valueOf(status)));	
//		    	if(cat != null && subCat != null){
//		    		if(cat.getStatus() != null && subCat.getStatus() != null){
//		    			if(!cat.equals(level.getCat())&&!cat.getStatus().getValue().equals(ActiveType.RUNNING.getValue()) ){
//		    				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.categoryen.code"));
//		    			}else if(!subCat.equals(level.getSubCat()) &&!subCat.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
//		    				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.sub.cat.code"));
//		    			}else {
//		    				levelsMgr.updateProductLevel(level,getLogInfoVO());
//		    				error = false;
//		    			}
//		    		}
//		    	}
//		    } else {
//				if(level!= null){
//					result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.product.level.code"));
//				} else {
//					ProductLevel newLevel = new ProductLevel();
////				    newLevel.setProductLevelCode(code);
//					newLevel.setProductLevelCode(cat.getProductInfoCode()+"."+subCat.getProductInfoCode());
//				    newLevel.setProductLevelName(name);
//				    newLevel.setCat(cat);
//				    newLevel.setSubCat(subCat);				    
//				    newLevel.setDescription(note);
//				    newLevel.setStatus(ActiveType.parseValue(Integer.valueOf(status)));
//				    if(cat != null && subCat != null){
//			    		if(cat.getStatus() != null && subCat.getStatus() != null){
//			    			if(!cat.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
//			    				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.categoryen.code"));
//			    			}else if(!subCat.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
//			    				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.sub.cat.code"));
//			    			}else {
//			    				 newLevel = levelsMgr.createProductLevel(newLevel,getLogInfoVO());
//			    			}
//			    		}
//			    	}
//				   
//				    if(newLevel!= null){
//					error = false;
//				    }
//				}
//		    }		    
//		}catch (Exception e) {
//		    LogUtility.logError(e, e.getMessage());
//		}		
//	    }
//	    result.put(ERROR, error);	    
//	    return JSON;
//	}
//	
//	/**
//	 * Delete 
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since 18/07/2012 
//	 */
//	public String delete(){
//	    
//	    resetToken(result);
//	    boolean error = true;
//	    if(currentUser!= null){
//		try{
//			ProductLevel level = levelsMgr.getProductLevelByCode(code);
//		    if(level!= null){
//		    	if(levelsMgr.isUsingByOthers(level.getId())){
//		    		result.put("errMsg", 
//		    				ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED_DB,
//									Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.level")));		    						    		
//		    	} else {
//		    		level.setStatus(ActiveType.DELETED);
//		    		levelsMgr.updateProductLevel(level,getLogInfoVO());
//		    		error = false;
//		    	}
//		    } 		   		    
//		}catch (Exception e) {
//		    LogUtility.logError(e, e.getMessage());
//		}
//	    }
//	    result.put(ERROR, error);
//	    return JSON;
//	}
//	
//	/**
//	 * Import excel.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 */
//	public String importExcel() {
//		isError = true;
//		totalItem = 0;
//		String message = "";		
//		List<CellBean> lstFails = new ArrayList<CellBean>();
//		lstView = new ArrayList<CellBean>();
//	    typeView = true;
//		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,5);		
//		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
//			try {
//				for(int i=0;i<lstData.size();i++){
//					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
//						message = "";
//						totalItem++;
//						ProductLevel productLevel = new ProductLevel();
//						productLevel.setStatus(ActiveType.RUNNING);
//						List<String> row = lstData.get(i);
//						
//						if(row.size() > 2) {
//							// product level name
//							if(row.size() > 0){
//								String value = row.get(0);
//								productLevel.setProductLevelName(value);
//								message += ValidateUtil.validateField(value, "catalog.product.level.name", 150, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH);
//							}
//							
//							// category code
//							if(row.size() > 1){
//								String value = row.get(1);
//								if(!StringUtil.isNullOrEmpty(value)){
//									ProductInfo pInfo = productInfoMgr.getProductInfoByCode(value,ProductType.CAT);
//									productLevel.setCat(pInfo);
//									if(pInfo == null){
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.category.code");									
//									} else if(ActiveType.STOPPED.getValue().equals(pInfo.getStatus().getValue())) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.category.code");									
//									}
//								}else{
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.category.code");
//								}
//							}
//							
//							// sub category cat
//							if(row.size() > 2){
//								String value = row.get(2);
//								if(!StringUtil.isNullOrEmpty(value)){
//									ProductInfo pInfo = productInfoMgr.getProductInfoByCode(value,ProductType.SUB_CAT);
//									productLevel.setSubCat(pInfo);
//									if(pInfo == null){
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.sub.cat.code");									
//									} else if(ActiveType.STOPPED.getValue().equals(pInfo.getStatus().getValue())) {
//										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.sub.cat.code");									
//									}else if(levelsMgr.checkIfSubCatExist(pInfo.getId(), null)){
//										message +=  Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.level.sub.cat.exist");
//									}
//								}else{
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.sub.cat.code");
//								}
//							}
//							
//							//description
//							if(row.size() > 3){
//								String value = row.get(3);
//								productLevel.setDescription(value);								
//								message += ValidateUtil.validateField(value, "common.description", 100, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH);
//							}
//						} else{
//							message = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_DATA_NOT_FULL, false);
//						}
//						if(isView == 0){
//							if (StringUtil.isNullOrEmpty(message)) {
//								productLevel.setProductLevelCode(row.get(1)+"."+row.get(2));
//								levelsMgr.createProductLevel(productLevel,getLogInfoVO());
//							} else {
//								lstFails.add(StringUtil.addFailBean(row, message));
//							}	
//							typeView = false;
//						}else{
//							lstView.add(StringUtil.addFailBean(row, message));
//							typeView = true;
//						}
//					}
//				}				
//				//Export error
//				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_LEVEL_FAIL);
//			} catch (Exception e) {
//				LogUtility.logError(e, e.getMessage());
//				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//			}
//		}		
//		if (StringUtil.isNullOrEmpty(errMsg)) {
//			isError = false;
//		}
//		return SUCCESS;
//	}
//	public String getListExcelData(){
//	    try{
//	    	if(catId == -2){
//				catId = null;
//			}
//			if(subCatId == -2){
//				subCatId = null;
//			}	
//	    	ObjectVO<ProductLevel> levelVO = levelsMgr.getListProductLevel(null, catId, subCatId, ActiveType.parseValue(Integer.valueOf(status)), note,code,name);
//			List<CellBean> lstFails = new ArrayList<CellBean>();
//			if(levelVO != null && levelVO.getLstObject() != null){
//				int size = levelVO.getLstObject().size();
//				for(int i=0;i<size;i++){
//					ProductLevel temp = levelVO.getLstObject().get(i);
//					CellBean cellBean = new CellBean();
//					cellBean.setContent1(temp.getProductLevelCode());
//					cellBean.setContent2(temp.getProductLevelName());
//					if(temp.getCat() != null){
//						cellBean.setContent3(temp.getCat().getProductInfoCode());
//					}
//					if(temp.getSubCat() != null){
//						cellBean.setContent4(temp.getSubCat().getProductInfoCode());
//					}
//					cellBean.setContent5(temp.getDescription());
//					if(temp.getStatus() != null){
//						if(ActiveType.RUNNING.equals(temp.getStatus())){
//							cellBean.setContent6(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.name"));
//						}else if(ActiveType.STOPPED.equals(temp.getStatus())){
//							cellBean.setContent6(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"pause.status.name"));
//						}
//					}
//					lstFails.add(cellBean);
//				}
//			}
//			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_LEVEL_EXPORT);
//	    }catch (Exception e) {
//	    	result.put(ERROR, true);
//	    	LogUtility.logError(e, e.getMessage());
//	    }
//	    return JSON;
//	}

}
/**
 *
 */
package ths.dms.action.catalog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.AttributeMgr;
import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.AttributeDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeColumnValue;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

/**
 * @author hungtx
 *
 */
public class AttributesManagerAction extends AbstractAction{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8104111217356548361L;
	private Long id;
	private int type;
	private String code;
	private String name;
	private Integer columnType;
	private String columnName;
	private int columnValueType;
	private int status;
	private String tableName;
	private String note;
	private List<StatusBean> lstColumnType;
	private List<StatusBean> lstColumnValueType;
	private AttributeMgr attributeMgr;
	private List<Attribute> lstAttribute;
	private int listAll;
	private List<StatusBean> lstColumnName;
	private String detailValue;
	private String detailName;
	private Long detailId;
	private List<Long> lstId;
	private List<String> lstTitle;
	private List<List<String>> lstData;
	private int typeView;
	private File excelFile;
	private String excelFileContentType;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getColumnType() {
		return columnType;
	}

	public void setColumnType(Integer columnType) {
		this.columnType = columnType;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public int getColumnValueType() {
		return columnValueType;
	}

	public void setColumnValueType(int columnValueType) {
		this.columnValueType = columnValueType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<StatusBean> getLstColumnType() {
		return lstColumnType;
	}

	public void setLstColumnType(List<StatusBean> lstColumnType) {
		this.lstColumnType = lstColumnType;
	}

	public List<StatusBean> getLstColumnValueType() {
		return lstColumnValueType;
	}

	public void setLstColumnValueType(List<StatusBean> lstColumnValueType) {
		this.lstColumnValueType = lstColumnValueType;
	}

	public int getListAll() {
		return listAll;
	}

	public void setListAll(int listAll) {
		this.listAll = listAll;
	}

	public List<StatusBean> getLstColumnName() {
		return lstColumnName;
	}

	public void setLstColumnName(List<StatusBean> lstColumnName) {
		this.lstColumnName = lstColumnName;
	}

	public List<Attribute> getLstAttribute() {
		return lstAttribute;
	}

	public void setLstAttribute(List<Attribute> lstAttribute) {
		this.lstAttribute = lstAttribute;
	}

	public String getDetailValue() {
		return detailValue;
	}

	public void setDetailValue(String detailValue) {
		this.detailValue = detailValue;
	}

	public String getDetailName() {
		return detailName;
	}

	public void setDetailName(String detailName) {
		this.detailName = detailName;
	}

	public Long getDetailId() {
		return detailId;
	}

	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public List<String> getLstTitle() {
		return lstTitle;
	}

	public void setLstTitle(List<String> lstTitle) {
		this.lstTitle = lstTitle;
	}

	public List<List<String>> getLstData() {
		return lstData;
	}

	public void setLstData(List<List<String>> lstData) {
		this.lstData = lstData;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public int getTypeView() {
		return typeView;
	}

	public void setTypeView(int typeView) {
		this.typeView = typeView;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    attributeMgr = (AttributeMgr)context.getBean("attributeMgr");
	}

	@Override
	public String execute(){
		resetToken(result);
		lstColumnType = new ArrayList<StatusBean>();
		lstColumnValueType = new ArrayList<StatusBean>();
		StatusBean ctNumber = new StatusBean();
		ctNumber.setValue(Long.valueOf(AttributeColumnType.NUMBER.getValue()));
		ctNumber.setName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.column.type.number"));
		lstColumnType.add(ctNumber);
		StatusBean ctChar = new StatusBean();
		ctChar.setValue(Long.valueOf(AttributeColumnType.CHARACTER.getValue()));
		ctChar.setName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.column.type.char"));
		lstColumnType.add(ctChar);
		StatusBean ctDate = new StatusBean();
		ctDate.setValue(Long.valueOf(AttributeColumnType.DATE_TIME.getValue()));
		ctDate.setName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.column.type.date"));
		lstColumnType.add(ctDate);
		StatusBean cvtManual = new StatusBean();
		cvtManual.setValue(Long.valueOf(AttributeColumnValue.SELF_INPUT.getValue()));
		cvtManual.setName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.column.value.type.manual"));
		lstColumnValueType.add(cvtManual);
		StatusBean cvtList = new StatusBean();
		cvtList.setValue(Long.valueOf(AttributeColumnValue.EXIST_BEFORE.getValue()));
		cvtList.setName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.column.value.type.list"));
		lstColumnValueType.add(cvtList);
		status = ActiveType.RUNNING.getValue();
		KPaging<Attribute> kPaging = new KPaging<Attribute>();
		kPaging.setPage(0);
		kPaging.setPageSize(20);
			/**
			 * @author tientv
			ObjectVO<Attribute> attrVO = attributeMgr.getListAttribute(kPaging,getTableName(type), null, null,null, ActiveType.RUNNING);
			if(attrVO!= null){
				lstAttribute = attrVO.getLstObject();
			}*/
		lstColumnName = getListColumnName(lstAttribute,-1);
		return SUCCESS;
	}

	/**
	 * Search.
	 *
	 * @return the string
	 */
	public String search(){
		result.put("page", page);
		result.put("max", max);
//		try{
			ObjectVO<Attribute> attrVO = null;
			KPaging<Attribute> kPaging = new KPaging<Attribute>();
			AttributeColumnType aType = null;
			if(columnType != null && columnType == -2){
				aType = null;
			}else{
				aType = AttributeColumnType.parseValue(columnType);
			}
			/**
			 @author tientv
			 if(listAll == 1){
				kPaging.setPage(0);
				kPaging.setPageSize(20);
				attrVO = attributeMgr.getListAttribute(kPaging,getTableName(type), code, name,aType, ActiveType.parseValue(status));
			} else {
				kPaging.setPage(page-1);
				kPaging.setPageSize(max);
				attrVO = attributeMgr.getListAttribute(kPaging,getTableName(type), code, name,aType, ActiveType.parseValue(status));
			}		*/
			if(attrVO!= null){
				result.put("total", attrVO.getkPaging().getTotalRows());
	    	    result.put("rows", attrVO.getLstObject());
			}

//		}catch (Exception e) {
////			LogUtility.logError(e, e.getMessage());
//			Date startLogDate = DateUtil.now();
//			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributesManagerAction.search()"), createLogErrorStandard(startLogDate));
//		}
		return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 */
	public String saveOrUpdate(){
		resetToken(result);
		boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
	    	errMsg = ValidateUtil.validateField(code, "attribute.code", 40, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
	    	errMsg+= ValidateUtil.validateField(name, "attribute.name", 250, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
	    	if(StringUtil.isNullOrEmpty(errMsg)){
//	    		try{
	    			/**
	    			 @author tientv
	    			Attribute attr = attributeMgr.getAttributeByCode(getTableName(type),code);
	    			if(id!= null && id>0){
		    			if(attr!= null && !id.equals(attr.getId())){
		    				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "attribute.code");
		    			} else {
		    				attr.setAttributeCode(code);
		    				attr.setAttributeName(name);
		    				attr.setColumnType(AttributeColumnType.parseValue(columnType));
		    				attr.setColumnValueType(AttributeColumnValue.parseValue(columnValueType));
		    				attr.setStatus(ActiveType.parseValue(status));
		    				attr.setNote(note);
		    				attr.setColumnName(columnName);
		    				attr.setTableName(getTableName(type).getTableName());
		    				attr.setUpdateUser(currentUser.getUserName());
		    				attributeMgr.updateAttribute(attr, getLogInfoVO());
		    				error = false;
		    			}
		    		} else {
		    			if(attr!= null){
		    				errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "attribute.code");
		    			} else {
		    				attr = new Attribute();
		    				attr.setAttributeCode(code);
		    				attr.setAttributeName(name);
		    				attr.setColumnType(AttributeColumnType.parseValue(columnType));
		    				attr.setColumnValueType(AttributeColumnValue.parseValue(columnValueType));
		    				attr.setStatus(ActiveType.parseValue(status));
		    				attr.setNote(note);
		    				attr.setColumnName(columnName);
		    				attr.setTableName(getTableName(type).getTableName());
		    				attr.setCreateUser(currentUser.getUserName());
		    				attributeMgr.createAttribute(attr, getLogInfoVO());
		    				error = false;
		    			}
		    		}*/
//	    		}catch (Exception e) {
//	    			LogUtility.logError(e, e.getMessage());
//				}
	    	}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Gets the table name.
	 *
	 * @param form the form
	 * @return the table name
	 */
	private TableHasAttribute getTableName(int form){
		TableHasAttribute[] arrTableName = TableHasAttribute.values();
		for(int i=0;i<arrTableName.length;i++){
			if(arrTableName[i].ordinal() == form){
				return arrTableName[i];
			}
		}
		return null;
	}

	/**
	 * Gets the list comlumn name.
	 *
	 * @param lstUsedAttr the lst used attr
	 * @return the list comlumn name
	 */
	private List<StatusBean> getListColumnName(List<Attribute> lstUsedAttr,Integer columnType){
		List<StatusBean> lstAttr = new ArrayList<StatusBean>();
		int i = 0;
		int size = 19;
		if(columnType.equals(AttributeColumnType.CHARACTER.getValue())){
			size = 9;
		}else if(columnType.equals(AttributeColumnType.NUMBER.getValue())){
			i = 10;
			size = 15;
		}else if(columnType.equals(AttributeColumnType.DATE_TIME.getValue())){
			i = 16;
			size = 19;
		}
		for(int j =i;j<=size;j++){
			String colName = "USER" + String.valueOf((j+1));
			if(j+1 < 10){
				colName = "USER0" + String.valueOf((j+1));
			}
			StatusBean bean = new StatusBean();
			bean.setName(colName);
//			Boolean flag = true;
			if(lstUsedAttr != null && lstUsedAttr.size() > 0){
				for(int k = 0;k<lstUsedAttr.size();k++){
					/*if(colName.equals(lstUsedAttr.get(k).getColumnName())){
						flag = false;
						break;
					}*/
				}
			}
//			if(flag){
				lstAttr.add(bean);
//			}
		}
		return lstAttr;
	}

	/**
	 * Search value.
	 *
	 * @return the string
	 */
	public String searchValue(){
		result.put("page", page);
		result.put("max", max);
//		try{
			ObjectVO<AttributeDetail> attrVO = null;
			KPaging<AttributeDetail> kPaging = new KPaging<AttributeDetail>();
			kPaging.setPage(page-1);
			kPaging.setPageSize(max);
			/** @author tientv
			attrVO = attributeMgr.getListAttributeDetailByAttributeId(kPaging, id, ActiveType.RUNNING);
			if(attrVO!= null){
				result.put("total", attrVO.getkPaging().getTotalRows());
	    	    result.put("rows", attrVO.getLstObject());
			}*/

//		}catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
		return JSON;
	}

	/**
	 * Delete value.
	 *
	 * @return the string
	 */
	public String deleteValue(){
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
			    AttributeDetail attributeDetail = attributeMgr.getAttributeDetailById(detailId);
			    if(attributeDetail!= null){
			    	/*if(attributeMgr.checkAttributeDetailIsUsing(getTableName(type), detailId)){
			    		errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.is.used.1",
								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.value.is.used"));
			    	}else{
			    		attributeMgr.deleteAttributeDetail(attributeDetail, getLogInfoVO());
			    		error = false;
			    	}*/
			    }

			}catch (BusinessException e) {
//			    LogUtility.logError(e, e.getMessage());
			    Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributesManagerAction.deleteValue()"), createLogErrorStandard(startLogDate));
			}
	    }
	    result.put(ERROR, error);
	    result.put("errMsg", errMsg);
	    return JSON;
	}

	/**
	 * Save or update value.
	 *
	 * @return the string
	 */
	public String saveOrUpdateValue(){
		boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
	    	//TODO: validate
	    	if(StringUtil.isNullOrEmpty(errMsg)){
	    		try{
	    			Attribute attribute = attributeMgr.getAttributeById(id);
	    			if(attribute != null){
	    				AttributeDetail attrDetail = attributeMgr.getAttributeDetailByCode(id, detailValue);
	    				/** @author tientv
	    				 if(detailId!= null && detailId > 0){
	    					if(attrDetail != null && !detailId.equals(attrDetail.getId())){
	    						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.value",detailValue,attribute.getAttributeCode());
	    					}else{
	    						AttributeDetail attributeDetail = attributeMgr.getAttributeDetailById(detailId);
	    						if(attributeDetail!= null){
	    							attributeDetail.setAttributeDetailValue(detailValue);
	    							attributeDetail.setAttributeDetailName(detailName);
	    							attributeMgr.updateAttributeDetail(attributeDetail, getLogInfoVO());
	    							error = false;
	    						}
	    					}
	    				} else {
	    					if(attrDetail != null){
    							errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.value",detailValue,attribute.getAttributeCode());
	    					}else{
	    						AttributeDetail attributeDetail = new AttributeDetail();
	    						attributeDetail.setAttributeDetailValue(detailValue);
	    						attributeDetail.setAttributeDetailName(detailName);
	    						attributeDetail.setAttribute(attribute);
	    						attributeMgr.createAttributeDetail(attributeDetail, getLogInfoVO());
	    						error = false;
	    					}
	    				}*/
	    			}
	    		}catch (BusinessException e) {
//	    			LogUtility.logError(e, e.getMessage());
	    			Date startLogDate = DateUtil.now();
					LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributesManagerAction.saveOrUpdateValue()"), createLogErrorStandard(startLogDate));
				}
	    	}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Delete attribute.
	 *
	 * @return the string
	 */
	public String deleteAttribute(){
	    boolean error = true;
	    List<Long> lstDeleted = new ArrayList<Long>();
	    List<Attribute> lstAttribute = new ArrayList<Attribute>();
	    if(currentUser!= null){
			try{
//			   List<Attribute> lstIdUsing = attributeMgr.checkIsUsingByOther(lstId);
//			   if(lstIdUsing!=null && lstIdUsing.size()>0){
//				   for(int i=0;i<lstIdUsing.size();i++){
//					   errMsg += lstIdUsing.get(i).getAttributeCode() +" , ";
//				   }
//				   if(!StringUtil.isNullOrEmpty(errMsg)) errMsg = errMsg.substring(0, errMsg.length()-3);
//				   for(int i=0;i<lstId.size();i++){
//					   Boolean flag=true;
//					   for(int j=0;j<lstIdUsing.size();j++){
//						   if(lstId.get(i).equals(lstIdUsing.get(j).getId()))
//							   flag=false;
//					   }
//					   if(flag){//Khong co trong list using
//						   Attribute attr = attributeMgr.getAttributeById(lstId.get(i));
//						   attributeMgr.deleteAttribute(attr, getLogInfoVO());
//						   lstDeleted.add(lstId.get(i));
//						   error = false;
//					   }else{
//						   error = true;
//						   break;
//					   }
//				   }
//			   }else{
//				   for(int i=0;i<lstId.size();i++){
//					   Attribute attr = attributeMgr.getAttributeById(lstId.get(i));
//					   attributeMgr.deleteAttribute(attr, getLogInfoVO());
//					   lstDeleted.add(lstId.get(i));
//				   }
//				   error = false;
//			   }
//			   if(!StringUtil.isNullOrEmpty(errMsg)){
//				   errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.value.is.used", errMsg);
//			   }
			   for(int i=0;i<lstId.size();i++){
				   Attribute attr = attributeMgr.getAttributeById(lstId.get(i));
				   lstDeleted.add(lstId.get(i));
				   if(attr != null){
					   lstAttribute.add(attr);
				   }
			   }
			   /**
			   @author tientv
			   if(lstAttribute != null && lstAttribute.size() > 0){
				   attributeMgr.deleteAttributeValue(lstAttribute, getLogInfoVO());
				   error = false;
			   }*/
			   result.put("lstDeleted", lstDeleted);
			}catch (BusinessException e) {
//			    LogUtility.logError(e, e.getMessage());
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributesManagerAction.deleteAttribute()"), createLogErrorStandard(startLogDate));
			    result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
	    }
	    result.put(ERROR, error);
	    return JSON;
	}

	/**
	 * Export attribute.
	 *
	 * @return the string
	 */
	public String exportAttribute(){
		TableHasAttribute table = getTableName(type);
		List<String> lstKeyText = getListKeyName(table);
		List<Attribute> lstAttr = new ArrayList<Attribute>();
		String fileName = lstKeyText.get(0);
		lstTitle = new ArrayList<String>();
		Date startLogDate = DateUtil.now();
		for(int i =1;i<lstKeyText.size();i++){
			lstTitle.add(lstKeyText.get(i));
		}
		try{
			if(lstId!= null && lstId.size()>0){
				for(int i=0;i<lstId.size();i++){
					Attribute attr = attributeMgr.getAttributeById(lstId.get(i));
					if(attr!= null){
						lstAttr.add(attr);
						lstTitle.add(attr.getAttributeCode());
					}
				}
			}
			/**
			 @author tientv
			 if(listAll==1){
				lstData = attributeMgr.exportAttribute(table, lstAttr);
			} else {
				lstData = new ArrayList<List<String>>();
			}*/
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/")
				+ Configuration.getExcelTemplatePathCatalog() + ConstantManager.TEMPLATE_ATTRIBUTE_EXPORT;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputFile = new StringBuilder(DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_EXCEL_FILE)).append("_").append(fileName).toString();
			String outputPath = Configuration.getStoreRealPath() + outputFile;
			outputPath = outputPath.replace('/', File.separatorChar);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("lstTitle", lstTitle);
			params.put("lstData", lstData);
			XLSTransformer transformer = new XLSTransformer();
			try {
				transformer.transformXLS(templateFileName, params, outputPath);
			} catch (ParsePropertyException e) {
//				LogUtility.logError(e, e.getMessage());
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributesManagerAction.exportAttribute()"), createLogErrorStandard(startLogDate));
			} catch (InvalidFormatException e) {
//				LogUtility.logError(e, e.getMessage());
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributesManagerAction.exportAttribute()"), createLogErrorStandard(startLogDate));
			} catch (IOException e) {
//				LogUtility.logError(e, e.getMessage());
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributesManagerAction.exportAttribute()"), createLogErrorStandard(startLogDate));
			}
			String outputDownload = Configuration.getExportExcelPath() + outputFile;
			outputDownload = outputDownload.replace('/', File.separatorChar);
			result.put("downloadLink",outputDownload);
		}catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributesManagerAction.exportAttribute()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Gets the list key name.
	 *
	 * @param table the table
	 * @return the list key name
	 */
	private List<String> getListKeyName(TableHasAttribute table){
		List<String> lstKeyName = new ArrayList<String>();
		if(TableHasAttribute.CUSTOMER.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.customer.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.code"));
		}else if(TableHasAttribute.DISPLAY_GROUP.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.display.group.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.code"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.quota.group.code"));
		}else if(TableHasAttribute.DISPLAY_PROGRAM.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.display.program.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.code"));
		}else if(TableHasAttribute.DISPLAY_PROGRAM_LEVEL.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.display.program.level.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.code"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.level.ma.dinh.muc"));
		}else if(TableHasAttribute.FOCUS_PROGRAM.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.focus.program.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.code"));
		}else if(TableHasAttribute.INCENTIVE_PROGRAM.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.incentive.program.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "incentive.program.code"));
		}else if(TableHasAttribute.INCENTIVE_PROGRAM_LEVEL.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.incentive.program_level.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "incentive.program.code"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.level.code"));
		}else if(TableHasAttribute.PRODUCT.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.product.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.product.code"));
		}else if(TableHasAttribute.PROMOTION_PROGRAM.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.promotion.program.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.code"));
		}else if(TableHasAttribute.STAFF.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.staff.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.staff.code"));
		}else if(TableHasAttribute.DP_PAY_PERIOD_RESULT.equals(table)){
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.dp.pay.period.filename"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.code"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sp.create.order.customer.is.empty"));
			lstKeyName.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "db.pay.period.code"));
		}
		return lstKeyName;
	}

	/**
	 * Gets the list message key name.
	 *
	 * @param table the table
	 * @return the list message key name
	 */
	private List<String> getListMessageKeyName(TableHasAttribute table){
		List<String> lstKeyName = new ArrayList<String>();
		if(TableHasAttribute.CUSTOMER.equals(table)){
			lstKeyName.add("catalog.customer.code");
		}else if(TableHasAttribute.DISPLAY_GROUP.equals(table)){
			lstKeyName.add("catalog.display.program.code");
			lstKeyName.add("catalog.display.program.quota.group.code");
		}else if(TableHasAttribute.DISPLAY_PROGRAM.equals(table)){
			lstKeyName.add("catalog.display.program.code");
		}else if(TableHasAttribute.DISPLAY_PROGRAM_LEVEL.equals(table)){
			lstKeyName.add("catalog.display.program.level");
			lstKeyName.add("catalog.display.program.level.code");
		}else if(TableHasAttribute.FOCUS_PROGRAM.equals(table)){
			lstKeyName.add("catalog.focus.program.code");
		}else if(TableHasAttribute.INCENTIVE_PROGRAM.equals(table)){
			lstKeyName.add("incentive.program.code");
		}else if(TableHasAttribute.INCENTIVE_PROGRAM_LEVEL.equals(table)){
			lstKeyName.add("incentive.program.level.code");
			lstKeyName.add("catalog.display.program.level.code");
		}else if(TableHasAttribute.PRODUCT.equals(table)){
			lstKeyName.add("catalog.product.code");
		}else if(TableHasAttribute.PROMOTION_PROGRAM.equals(table)){
			lstKeyName.add("catalog.promotion.code");
		}else if(TableHasAttribute.STAFF.equals(table)){
			lstKeyName.add("catalog.staff.code");
		}else if(TableHasAttribute.DP_PAY_PERIOD_RESULT.equals(table)){
			lstKeyName.add("catalog.unit.tree.code");
			lstKeyName.add("sp.create.order.customer.is.empty");
			lstKeyName.add("db.pay.period.code");
		}
		return lstKeyName;
	}

	public Boolean checkNumber(String str){
		if(str.equals("")) return true;
		try{
			Float test = Float.valueOf(str);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}

	/**
	 * Import excel.
	 *
	 * @return the string
	 */
	public String importExcel() {
		/** @author tientv
		isError = true;
		totalItem = 0;
		String message = "";
		List<List<String>> lstFails = new ArrayList<List<String>>();
		lstData = getExcelDataWithHeader(excelFile, excelFileContentType, errMsg,21);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				List<String> lstHeader = lstData.get(0);
				List<Attribute> lstAttribute = new ArrayList<Attribute>();
				List<String> lstAttr = new ArrayList<String>();
				int numCol = 0;
				TableHasAttribute tableType =  getTableName(type);
				String errMsgHeader = "";
				for(int i=0;i<lstHeader.size();i++){
					if(!StringUtil.isNullOrEmpty(lstHeader.get(i))){
						numCol++;
						if(i>=tableType.getTableKey().length){
							lstAttr.add(lstHeader.get(i).toUpperCase());
							try{

								Attribute attr = attributeMgr.getAttributeByCode(tableType,lstHeader.get(i).toUpperCase());
								if(attr!= null){
									if(ActiveType.RUNNING.equals(attr.getStatus())){
										lstAttribute.add(attr);
									}else{
										errMsgHeader += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.value.status.stopped", lstHeader.get(i).toUpperCase())+"\n";
									}
								}
							}catch (Exception e) {

							}
						}
					} else {
						break;
					}
				}
				List<Integer> index = new ArrayList<Integer>();
				lstHeader = lstHeader.subList(0, numCol);
				List<String> lstInvalidAttr = null;

				try {
					lstInvalidAttr = attributeMgr.checkAttributeIsNotExists(tableType, lstAttr);
				} catch (Exception e1) {
					errMsg =Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.invalid.file")+"\n";
					isError = false;
					return SUCCESS;
				}
				String errMsgAttr = "";
				if(lstInvalidAttr!= null && lstInvalidAttr.size()>0){
					StringBuilder sbInvAttr = new StringBuilder();
					for(int k=0;k<lstInvalidAttr.size();k++){
						sbInvAttr.append(lstInvalidAttr.get(k));
						if(k<lstInvalidAttr.size()-1){
							sbInvAttr.append(", ");
						}
						for(int i=0;i<lstHeader.size();i++){
							if(lstInvalidAttr.get(k).equals(lstHeader.get(i).toUpperCase())){
								index.add(i);
								break;
							}
						}
					}
					errMsgAttr = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.not.exist", sbInvAttr.toString())+"\n";
				}


				List<List<AttributeDetail>> arrListAD = new ArrayList<List<AttributeDetail>>();
				for(int i =0;i<lstAttribute.size();i++){
					if(AttributeColumnValue.EXIST_BEFORE.equals(lstAttribute.get(i).getColumnValueType())){
						ObjectVO<AttributeDetail> objAD = attributeMgr.getListAttributeDetailByAttributeId(null, lstAttribute.get(i).getId(), ActiveType.RUNNING);
						if(objAD !=null && objAD.getLstObject().size()>0){
							arrListAD.add(objAD.getLstObject());
						}else{
							arrListAD.add(null);
						}
					}else{
						arrListAD.add(null);
					}
				}
				for(int i=1;i<lstData.size();i++){
 					List<String> indexRemove = new ArrayList<String>();
					List<String> row = lstData.get(i);
					if(row!= null && row.size()>0){
						row = row.subList(0, numCol);
						lstData.set(i, row);
						for(int n = 0;n < index.size();n++){
							int m = 0;
							if(n == 0){
								m = index.get(n).intValue();
							}else{
								m = index.get(n).intValue() - n;
							}
							indexRemove.add(row.get(m));
							row.remove(m);
						}
						message = "";
						totalItem++;
						String[] arrKey = tableType.getTableKey();
						List<String> lstMsgKey = getListMessageKeyName(tableType);
						List<String> lstValue = new ArrayList<String>();
						List<List<String>> lstValueKey = new ArrayList<List<String>>();
						int j=0;
						for(j=0;j<arrKey.length;j++){
							message+= ValidateUtil.validateField(row.get(0), lstMsgKey.get(j), 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
							lstValue.add(row.get(j));
						}
						lstValueKey.add(lstValue);
						List<List<String>> lstFailData = attributeMgr.checkKeyIsNotExists(tableType, lstValueKey);
						if(lstFailData!= null && lstFailData.size()>0 && lstFailData.get(0)!= null && lstFailData.get(0).size()>0){
							String keys="";
							for(int k=0;k<lstFailData.get(0).size();k++){
								if(!StringUtil.isNullOrEmpty(lstFailData.get(0).get(k))){
									keys+=lstFailData.get(0).get(k)+" , ";
								}
							}
							if(keys.length()>0){
								keys=keys.substring(0, keys.length()-3);
								message += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_TOGETHER_DB,keys)+"\n";
							}
						}
						if(lstValueKey!= null && lstValueKey.size()>0 && lstValueKey.get(0)!= null && lstValueKey.get(0).size()>0){
							if(attributeMgr.checkObjectIsValidate(tableType, lstValueKey.get(0)) == 0 || attributeMgr.checkObjectIsValidate(tableType, lstValueKey.get(0)) == 1){
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.not.waiting", lstValueKey.get(0).get(0))+"\n";
							}else if(attributeMgr.checkObjectIsValidate(tableType, lstValueKey.get(0)) == 3){
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.pause", lstValueKey.get(0).get(1))+"\n";
							}else if(attributeMgr.checkObjectIsValidate(tableType, lstValueKey.get(0)) == 5){
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.not.waiting", lstValueKey.get(0).get(0))+"\n";
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.pause", lstValueKey.get(0).get(1))+"\n";
							}else if(attributeMgr.checkObjectIsValidate(tableType, lstValueKey.get(0)) == 6){
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.not.waiting", lstValueKey.get(0).get(0))+"\n";
							}
						}

						if(lstValueKey!= null && lstValueKey.size()>0 && lstValueKey.get(0)!= null && lstValueKey.get(0).size()>0){
							for(int n = 0;n < lstValueKey.get(0).size()-1;n++){
								row.remove(0);
							}
						}
						for(int k=0 ; k<lstAttribute.size(); k++){
							if(AttributeColumnValue.EXIST_BEFORE.equals(lstAttribute.get(k).getColumnValueType())){
								List<AttributeDetail> listAD = arrListAD.get(k);
								if(listAD!=null){
									Boolean flagErr=true;
									for(int m=0 ; m<listAD.size() ; m++){

										 if(listAD.get(m).getAttributeDetailValue().equals(row.get(k+1))){
											flagErr=false;
											break;
										}
									}
									if(flagErr){
										if(!StringUtil.isNullOrEmpty(row.get(k+1))){
											message += ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXISTS_BEFORE, lstAttribute.get(k).getAttributeCode())+"\n";
										}
									}
								}
							}else{

								if(AttributeColumnType.NUMBER.equals(lstAttribute.get(k).getColumnType())){
									if(!checkNumber(row.get(k+1))){
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.value.float.invalid", lstAttribute.get(k).getAttributeCode())+"\n";
									}
								}else if(AttributeColumnType.DATE_TIME.equals(lstAttribute.get(k).getColumnType())){
									if(DateUtil.checkInvalidFormatDate(row.get(k+1))){
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.value.date.invalid", lstAttribute.get(k).getAttributeCode())+"\n";
									}
								}
							}
						}
						if(!StringUtil.isNullOrEmpty(errMsgHeader)){
							message += errMsgHeader;
						}
						if(!StringUtil.isNullOrEmpty(errMsgAttr)){
							message += errMsgAttr;
						}
						if(lstValueKey!= null && lstValueKey.size()>0 && lstValueKey.get(0)!= null && lstValueKey.get(0).size()>0){
							for(int n = 0;n < lstValueKey.get(0).size()-1;n++){
								row.add(n,lstValueKey.get(0).get(n));
							}
						}
						for(int n = 0;n < index.size();n++){
							row.add(index.get(n).intValue(), indexRemove.get(n));
						}

						 @author tientv
						if(typeView == IMPORT_EXCEL){
							if(StringUtil.isNullOrEmpty(message)){
								List<List<String>> data = new ArrayList<List<String>>();
								data.add(row);
								attributeMgr.importAttribute(tableType, lstAttribute, data, getLogInfoVO());
							} else {
								row.add(message);
								lstFails.add(row);
							}
						} else {
							message = StringUtil.convertHTMLBreakLine(message);
							if(!StringUtil.isNullOrEmpty(message)){
								row.add(message);
							} else {
								row.add(ConstantManager.NOT_ERROR_TEXT);
							}
							lstData.set(i, row);
						}

					}
				}
				lstHeader.add(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.error")+"\n");
				//Export error
				if(typeView == IMPORT_EXCEL){
					if(lstFails.size()>0){
						numFail = lstFails.size();
						String templateName = ConstantManager.TEMPLATE_ATTRIBUTE_FAIL;
						String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getFailDataPath() + templateName;
						templateFileName = templateFileName.replace('/', File.separatorChar);
						String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + templateName;
						String outputFileName = Configuration.getStoreRealPath()+ outputName;
						outputFileName = outputFileName.replace('/', File.separatorChar);
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("header", lstHeader);
						params.put("report", lstFails);
						XLSTransformer transformer = new XLSTransformer();
						try {
							transformer.transformXLS(templateFileName, params, outputFileName);
						} catch (ParsePropertyException e) {
							LogUtility.logError(e, e.getMessage());
						} catch (InvalidFormatException e) {
							LogUtility.logError(e, e.getMessage());
						} catch (IOException e) {
							LogUtility.logError(e, e.getMessage());
						}
						fileNameFail = Configuration.getExportExcelPath() +  outputName;
					}
				} else {
					lstTitle = lstHeader;
					lstData.remove(0);
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		} */
		return SUCCESS;
	}

	public String getListColumnNameByType(){
//		try{
			/**
			@author tientv
			KPaging<Attribute> kPaging = new KPaging<Attribute>();
			kPaging.setPage(0);
			kPaging.setPageSize(20);
			ObjectVO<Attribute> attrVO = attributeMgr.getListAttribute(kPaging,getTableName(type), null, null,AttributeColumnType.parseValue(columnType),null);
			if(attrVO!= null){
				lstAttribute = attrVO.getLstObject();
			}*/
//		}catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
		lstColumnName = getListColumnName(lstAttribute,columnType);
		result.put("lstColumnName", lstColumnName);
		return JSON;
	}
}

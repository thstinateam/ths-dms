package ths.dms.action.catalog;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerTypeCatalogAction.
 */
public class CustomerTypeCatalogAction extends AbstractAction {	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -701060161120400556L;

	/** The id. */
	private Long id;
	
	/** The code. */
	private String code;
	
	/** The name. */
	private String name;
				
	/** The p id. */
	private Long parentId;	
	
	/** The lst channel type. */
	private List<ChannelType> lstChannelType;
		
	/** The channel type mgr. */
	private ChannelTypeMgr channelTypeMgr;
	
	/** The all item. */
	private int allItem;
	
	/** The status. */
	private int status;
	
	/** The sku level. */
	private String skuLevel;
	
	
	/** The sale amount. */
	private String saleAmount;
	
	private String order;
	
	private String sort;
	
	/**
	 * Gets the sku level.
	 *
	 * @return the sku level
	 */
	public String getSkuLevel() {
		return skuLevel;
	}

	/**
	 * Sets the sku level.
	 *
	 * @param skuLevel the new sku level
	 */
	public void setSkuLevel(String skuLevel) {
		this.skuLevel = skuLevel;
	}

	/**
	 * Gets the sale amount.
	 *
	 * @return the sale amount
	 */
	public String getSaleAmount() {
		return saleAmount;
	}

	/**
	 * Sets the sale amount.
	 *
	 * @param saleAmount the new sale amount
	 */
	public void setSaleAmount(String saleAmount) {
		this.saleAmount = saleAmount;
	}

	/**
	 * getId.
	 *
	 * @return the id
	 */
	public Long getId() {
	    return id;
	}

	/**
	 * setId.
	 *
	 * @param id the id to set
	 */
	public void setId(Long id) {
	    this.id = id;
	}

	/**
	 * getCode.
	 *
	 * @return the code
	 */
	public String getCode() {
	    return code;
	}

	/**
	 * setCode.
	 *
	 * @param code the code to set
	 */
	public void setCode(String code) {
	    this.code = code;
	}

	/**
	 * getName.
	 *
	 * @return the name
	 */
	public String getName() {
	    return name;
	}

	/**
	 * setName.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
	    this.name = name;
	}	

	/**
	 * Gets the parent id.
	 *
	 * @return the parentId
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * Sets the parent id.
	 *
	 * @param parentId the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}	

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    channelTypeMgr = (ChannelTypeMgr)context.getBean("channelTypeMgr");
	}
	
	/**
	 * Gets the lst channel type.
	 *
	 * @return the lstChannelType
	 */
	public List<ChannelType> getLstChannelType() {
		return lstChannelType;
	}

	/**
	 * Sets the lst channel type.
	 *
	 * @param lstChannelType the lstChannelType to set
	 */
	public void setLstChannelType(List<ChannelType> lstChannelType) {
		this.lstChannelType = lstChannelType;
	}

	/**
	 * Gets the all item.
	 *
	 * @return the allItem
	 */
	public int getAllItem() {
		return allItem;
	}

	/**
	 * Sets the all item.
	 *
	 * @param allItem the allItem to set
	 */
	public void setAllItem(int allItem) {
		this.allItem = allItem;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {
		resetToken(result);
		status = ActiveType.RUNNING.getValue();
		try{
			mapControl = getMapControlToken();
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.CUSTOMER);
			filter.setStatus(ActiveType.RUNNING);
			ObjectVO<ChannelType> channelTypeVO = channelTypeMgr.getListChannelType(filter,null);
			if(channelTypeVO!= null){
				lstChannelType = channelTypeVO.getLstObject();
			}
			mapControl = getMapControlToken();
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());			
		}
	    return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author hungtx , phuocdh2 modified sort ,order
	 * @since 18/07/2012 
	 * 
	 * @modify hunglm16
	 * @since 14/10/2015
	 * @description ra soat code
	 */
	public String search() {
		result.put("total", 0);
		result.put("rows", new ArrayList<ChannelType>());
		try {
			KPaging<ChannelType> kPaging = new KPaging<ChannelType>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			if (parentId != null && parentId.intValue() == ConstantManager.SELECT_ALL) {
				parentId = null;
			}
			ObjectVO<ChannelType> lstReasonGroup = null;

			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.CUSTOMER);
			filter.setStatus(ActiveType.parseValue(status));
			filter.setChannelTypeCode(code);
			filter.setChannelTypeName(name);
			filter.setParentId(parentId);
			filter.setOrder(order);
			filter.setSort(sort);
			if (!StringUtil.isNullOrEmpty(skuLevel)) {
				filter.setSku(new BigDecimal(skuLevel));
			}
			if (!StringUtil.isNullOrEmpty(saleAmount)) {
				filter.setSaleAmount(new BigDecimal(saleAmount));
			}
			if (allItem != 0) {
				filter.setIsGetParentAndChild(true);
			}
			lstReasonGroup = channelTypeMgr.getListChannelType(filter, kPaging);
			if (lstReasonGroup != null) {
				result.put("total", lstReasonGroup.getkPaging().getTotalRows());
				result.put("rows", lstReasonGroup.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.catalog.CustomerTypeCatalogAction.search", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 18/07/2012 
	 */
	private Integer changeCus;
	//private CustomerMgr customerMgr;
	private PromotionProgramMgr promotionProgramMgr;
	
	public Integer getChangeCus() {
		return changeCus;
	}

	public void setChangeCus(Integer changeCus) {
		this.changeCus = changeCus;
	}

	/**
	 * Modify vuongmq; Tao moi cap nhat loai KH
	 * @author vuongmq
	 * @return String
	 * @since 16/09/2015 
	 */
	public String saveOrUpdate() {
	    resetToken(result);
	    boolean error = true;
		if (currentUser != null) {
			try {
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(code, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(name, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
			    ChannelType channelType = channelTypeMgr.getChannelTypeByCode(code, ChannelTypeType.CUSTOMER);
			    ChannelType parentChannelType = null;
				if (parentId != null) {
			    	parentChannelType = channelTypeMgr.getChannelTypeById(parentId);
			    }		    
				if (id != null && id > 0) {
					if (channelType != null) {
						if (!id.equals(channelType.getId())) {
					    	result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.customer.type.code"));
					    } else if(ActiveType.STOPPED.getValue() == status && !channelTypeMgr.isAllChildStoped(channelType.getId(), ChannelTypeType.CUSTOMER)){
					    	result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.type.all.child.stopped"));
					    } else if(ActiveType.RUNNING.getValue() == status && 
					    		((channelType.getParentChannelType()!= null && ActiveType.STOPPED.equals(channelType.getParentChannelType().getStatus()))
					    				|| (parentChannelType!= null && ActiveType.STOPPED.equals(parentChannelType.getStatus())))) {
					    	result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.type.parent.stopped"));
					    }
	//				    else if(parentChannelType != null && channelTypeMgr.checkAncestor(channelType.getId(),parentChannelType.getId())){
	//						result.put("errMsg",  ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_CORRECT, false, "catalog.customer.type.parentcode"));
	//				    }
					    else {
							if (status != 1) {
						    	promotionProgramMgr = (PromotionProgramMgr)context.getBean("promotionProgramMgr");
						    	List<PromotionProgram> lstPro = new ArrayList<PromotionProgram>();
						    	lstPro = promotionProgramMgr.getListPromotionProgramByCustomerType(id);
						    	String mes = "";
								if (lstPro != null && lstPro.size() > 0) {
									for (int i = 0, sz = lstPro.size(); i < sz; i++) {
										if (!mes.isEmpty()) {
											mes += ", ";
										}
										mes += lstPro.get(i).getPromotionProgramCode();
									}
						    		result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "channeltype.updateCusType.promotion.error",lstPro.size(),mes));
						    		result.put(ERROR, error);
									return JSON;
						    	}
					    	}
					    	/*if(changeCus != null){
					    		customerMgr = (CustomerMgr)context.getBean("customerMgr");
					    		ParamsDAOGetListCustomer filter = new ParamsDAOGetListCustomer();
								filter.setCustomerTypeId(id);
								ObjectVO<Customer> lstCus = customerMgr.getListCustomer(filter);
								if(changeCus == 0){
									if(lstCus.getLstObject().size() >0){
										result.put("hasMessageListCustomer", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"channeltype.updateCusType.error",lstCus.getLstObject().size()));
										error = false;
										return JSON;
									}
								}else if(changeCus ==1){
									if(lstCus.getLstObject().size() >0){
										for(int i=0;i<lstCus.getLstObject().size();i++){
											Customer c = lstCus.getLstObject().get(i);
											c.setChannelType(null);
											customerMgr.updateCustomer(c, null, null, null, null, getLogInfoVO());
										}
									}
								}
							}*/
					    	if (!StringUtil.isNullOrEmpty(skuLevel) && !ValidateUtil.validateNumber(skuLevel)) {
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog_customer_type_standard_sku_is_not_number"));
					    		result.put(ERROR, error);
								return JSON;
							}
							if (!StringUtil.isNullOrEmpty(saleAmount) && !ValidateUtil.validateNumber(saleAmount)) {
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog_customer_type_target_ds_is_not_number"));
					    		result.put(ERROR, error);
								return JSON;
							}
							 //channelType.setChannelTypeCode(code);
							 channelType.setChannelTypeName(name);
							 channelType.setStatus(ActiveType.parseValue(status));
	//						 channelType.setParentChannelType(parentChannelType);
							 channelType.setType(ChannelTypeType.CUSTOMER);
							 if (!StringUtil.isNullOrEmpty(skuLevel)) {
								 channelType.setSku(new BigDecimal(skuLevel));
							 } else {
								 channelType.setSku(BigDecimal.ZERO);
							 }
							 if (!StringUtil.isNullOrEmpty(saleAmount)) {
								 channelType.setSaleAmount(new BigDecimal(saleAmount));
							 } else {
								 channelType.setSaleAmount(BigDecimal.ZERO);
							 }
							 //channelTypeMgr.updateChannelType(channelType,getLogInfoVO());
							 channelTypeMgr.updateCustomerType(channelType, getLogInfoVO());
							 error = false;
					    }
					}    
			    } else {
					if (channelType != null) {
					    result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.code.exist",
							Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.type.code"), code));
					} else {
						if (!StringUtil.isNullOrEmpty(skuLevel) && !ValidateUtil.validateNumber(skuLevel)) {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog_customer_type_standard_sku_is_not_number"));
				    		result.put(ERROR, error);
							return JSON;
						}
						if (!StringUtil.isNullOrEmpty(saleAmount) && !ValidateUtil.validateNumber(saleAmount)) {
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog_customer_type_target_ds_is_not_number"));
				    		result.put(ERROR, error);
							return JSON;
						}
					    ChannelType newChannelType = new ChannelType();
					    newChannelType.setChannelTypeCode(code);
					    newChannelType.setChannelTypeName(name);
					    newChannelType.setStatus(ActiveType.parseValue(status));
					    newChannelType.setParentChannelType(parentChannelType);
					    newChannelType.setType(ChannelTypeType.CUSTOMER);
					    if (!StringUtil.isNullOrEmpty(skuLevel)) {
					    	newChannelType.setSku(new BigDecimal(skuLevel));
						} else {
							newChannelType.setSku(BigDecimal.ZERO);
						}
					    if (!StringUtil.isNullOrEmpty(saleAmount)) {
					    	newChannelType.setSaleAmount(new BigDecimal(saleAmount));
					    } else {
					    	newChannelType.setSaleAmount(BigDecimal.ZERO);
					    }
					    newChannelType = channelTypeMgr.createChannelType(newChannelType,getLogInfoVO());
						if (newChannelType != null) {
							error = false;
						}
					}
			    }
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
	    }
	    result.put(ERROR, error);	    
	    return JSON;
	}
	
	/**
	 * Delete.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 18/07/2012
	 */
	public String delete(){
	    
	    resetToken(result);
	    boolean error = true;
	    if(currentUser!= null){
		try{
		    ChannelType channelType = channelTypeMgr.getChannelTypeByCode(code, ChannelTypeType.CUSTOMER);
		    if(channelType!= null){
				Boolean isUsed = channelTypeMgr.isUsingByOthers(channelType.getId(), ChannelTypeType.CUSTOMER);
				if(isUsed!= null && isUsed == false){
					Boolean hasChild = channelTypeMgr.checkChannelTypeHasAnyChild(channelType.getId());
					if(hasChild){
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.type.parent.delete"));
					} else {
						channelType.setStatus(ActiveType.DELETED);
					    channelTypeMgr.updateChannelType(channelType,getLogInfoVO());
					    error = false;
					}					
				} else {
				    result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.is.used.1",
						Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.type")));
				}
		    } 
		   		    
		}catch (Exception e) {
		    LogUtility.logError(e, e.getMessage());
		}
	    }
	    result.put(ERROR, error);
	    return JSON;
	}
	

	public String update() {
		if (currentUser != null) {
			try {
				ChannelType ct = new ChannelType();
				if(id!=null && id>0){
					ct = channelTypeMgr.getChannelTypeById(Long.valueOf(id));
					result.put("channelType", ct);
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		return JSON;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

}

package ths.dms.action.catalog;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.ShopProductMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopProduct;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ShopChannelType;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopProductFilter;
import ths.dms.core.entities.enumtype.ShopProductType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class CategoryTypeShopCatalogAction extends AbstractAction{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	private File excelFile;
	private String excelFileContentType;
	private List<ProductInfo> lstCategoryType;
	private ProductInfoMgr productInfoMgr;
	private ShopProductMgr shopProductMgr;
	private StaffMgr staffMgr;
	private String shopCode;
	private String shopName;
	private Long categoryId;
	private String categoryCode;
	private ShopProduct shopProduct;
	private ShopMgr shopMgr;
	private Integer stockMin;
	private Integer stockMax;
	private Integer dateGo;
	private String dateSale;
	private String growth;
	private Integer status;
	private String typeButton;
	private Long categoryType;
	private List<CellBean> listBeans;
	private Integer typeView;
	private boolean view;
	private List<String> lstcategory;
	private List<String> listCategory;
	private Integer shopObjectType;

	public Integer getShopObjectType() {
		return shopObjectType;
	}

	public void setShopObjectType(Integer shopObjectType) {
		this.shopObjectType = shopObjectType;
	}
	
	private Integer staffRoleType;

	public Integer getStaffRoleType() {
		return staffRoleType;
	}

	public void setStaffRoleType(Integer staffRoleType) {
		this.staffRoleType = staffRoleType;
	}
	

	public List<String> getLstcategory() {
		return lstcategory;
	}

	public void setLstcategory(List<String> lstcategory) {
		this.lstcategory = lstcategory;
	}

	public List<String> getListCategory() {
		return listCategory;
	}

	public void setListCategory(List<String> listCategory) {
		this.listCategory = listCategory;
	}

	public Integer getTypeView() {
		return typeView;
	}

	public void setTypeView(Integer typeView) {
		this.typeView = typeView;
	}

	public Long getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(Long categoryType) {
		this.categoryType = categoryType;
	}

	public Integer getStockMin() {
		return stockMin;
	}

	public void setStockMin(Integer stockMin) {
		this.stockMin = stockMin;
	}

	public Integer getStockMax() {
		return stockMax;
	}

	public void setStockMax(Integer stockMax) {
		this.stockMax = stockMax;
	}

	public Integer getDateGo() {
		return dateGo;
	}

	public void setDateGo(Integer dateGo) {
		this.dateGo = dateGo;
	}

	public String getDateSale() {
		return dateSale;
	}

	public void setDateSale(String dateSale) {
		this.dateSale = dateSale;
	}

	public String getGrowth() {
		return growth;
	}

	public void setGrowth(String growth) {
		this.growth = growth;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public ShopProduct getShopProduct() {
		return shopProduct;
	}

	public void setShopProduct(ShopProduct shopProduct) {
		this.shopProduct = shopProduct;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public List<ProductInfo> getLstCategoryType() {
		return lstCategoryType;
	}

	public void setLstCategoryType(List<ProductInfo> lstCategoryType) {
		this.lstCategoryType = lstCategoryType;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}


	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	    shopProductMgr = (ShopProductMgr)context.getBean("shopProductMgr");
	    shopMgr = (ShopMgr)context.getBean("shopMgr");
	    staffMgr =(StaffMgr) context.getBean("staffMgr");
	}
	
	@Override
	public String execute() {	
		resetToken(result);
		try {
			Staff staff=getStaffByCurrentUser();
			if(staff!=null && staff.getShop()!=null && staff.getShop().getType()!=null){
//				shopObjectType = staff.getShop().getType().getObjectType();
				shopCode = staff.getShop().getShopCode();
				shopName = staff.getShop().getShopName();
			}
			/*ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT,true);
			lstCategoryType = tmp.getLstObject();
			mapControl = getMapControlToken();*/
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		
	    return SUCCESS;
	}
	
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012
	 * 
	 *  @author hunglm16
	 *  @since JUNE 19, 2014
	 *  @description Phan quyen NPP, Phan quyen VNM
	 */
	public String search(){
	    return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012 
	 * 
	 *  @author hunglm16
	 *  @since JUNE 19, 2014
	 *  @description Phan quyen NPP, Phan quyen VNM
	 */
	public String saveOrUpdate(){
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(dateSale, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
			    shopProduct = shopProductMgr.getShopProductById(categoryId);
			    if(categoryId!= null && categoryId > 0){
					if(shopProduct!= null){
						if(!StringUtil.isNullOrEmpty(shopCode)){
							Staff ownerStaff = null;
							if(currentUser != null && currentUser.getUserName() != null) {
								ownerStaff = staffMgr.getStaffByCode(currentUser.getUserName());
							}
							if(ownerStaff != null && ownerStaff.getShop() != null) {
								if (!checkShopInOrgAccessByCode(shopCode)) {
									//error
									result.put(ERROR, error);
								    result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_AREA));	    
								    return JSON;
								}
							}
							Shop sTmp =shopMgr.getShopByCode(shopCode);
							
							if(sTmp!= null){
								if(ActiveType.RUNNING.equals(sTmp.getStatus())){
									if(categoryType != null && categoryType != -1){
										ProductInfo pTmp =productInfoMgr.getProductInfoById(categoryType);
										if(pTmp!= null){
											ObjectVO<ShopProduct> lstCategory = shopProductMgr.getListShopProduct(null, shopCode, null, categoryType, null, ActiveType.RUNNING,ShopProductType.CAT,null);
											if((!shopCode.equals(shopProduct.getShop().getShopCode()) || !categoryType.equals(shopProduct.getCat().getId())) && lstCategory.getLstObject().size() > 0 ){
												errMsg= Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.category.product.code.exist",
														Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.category.code"));
											}else{
												shopProduct.setShop(sTmp);
												shopProduct.setCat(pTmp);
												shopProduct.setMinsf(stockMin);
												shopProduct.setMaxsf(stockMax);
												shopProduct.setLead(dateGo);
												if(!StringUtil.isNullOrEmpty(dateSale)){
													shopProduct.setCalendarD(dateSale);
												}
												shopProduct.setPercentage(Float.parseFloat(growth));
												shopProduct.setType(ShopProductType.CAT);
												if(status != null && status != ConstantManager.NOT_STATUS){
													shopProduct.setStatus(ActiveType.parseValue(status));
												}
												shopProduct.setUpdateUser(currentUser.getUserName());
												shopProductMgr.updateShopProduct(shopProduct,getLogInfoVO());
												error = false;
											}
										}
									}
								}else{
									errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, null,"catalog.focus.program.shop.code");
								}
							}else{
								errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, null,"catalog.focus.program.shop.code");
							}
						}else{
							errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null,"catalog.focus.program.shop.code");
						}
					}    
			    } else {
			    	shopProduct = new ShopProduct();
					if(!StringUtil.isNullOrEmpty(shopCode)){
						Staff ownerStaff = null;
						if(currentUser != null && currentUser.getUserName() != null) {
							ownerStaff = staffMgr.getStaffByCode(currentUser.getUserName());
						}
						if(ownerStaff != null && ownerStaff.getShop() != null) {
							if (!checkShopInOrgAccessByCode(shopCode)) {
								//error
								result.put(ERROR, error);
							    result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_AREA));	    
							    return JSON;
							}
						}
						Shop sTmp =shopMgr.getShopByCode(shopCode);
						if(sTmp!= null){
							if(ActiveType.RUNNING.equals(sTmp.getStatus())){
								shopProduct.setShop(sTmp);
								if(categoryType != null && categoryType != -1){
									ProductInfo pTmp =productInfoMgr.getProductInfoById(categoryType);
									if(pTmp!= null){
										if(ActiveType.RUNNING.equals(pTmp.getStatus())){
											ObjectVO<ShopProduct> lstCategory = shopProductMgr.getListShopProduct(null, shopCode, null, categoryType, null, null,ShopProductType.CAT,null);
											if(lstCategory.getLstObject().size() > 0 ){
												errMsg= Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.category.product.code.exist",
														Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.category.code"));
											}else{
												shopProduct.setCat(pTmp);
												shopProduct.setMinsf(stockMin);
												shopProduct.setMaxsf(stockMax);
												shopProduct.setLead(dateGo);
												if(!StringUtil.isNullOrEmpty(dateSale)){
													shopProduct.setCalendarD(dateSale);
												}
												shopProduct.setPercentage(Float.parseFloat(growth));
												shopProduct.setType(ShopProductType.CAT);
												if(status != null && status != ConstantManager.NOT_STATUS){
													shopProduct.setStatus(ActiveType.parseValue(status));
												}
												shopProduct.setCreateUser(currentUser.getUserName());
												shopProduct = shopProductMgr.createShopProduct(shopProduct,getLogInfoVO());
												if(shopProduct!= null){
													error = false;
												}
											}
										}else{
											errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, null,"catalog.category.code");
										}
									} else {
										errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "sale.plan.qm.excel.shop.product.infor");
									}
								}
							}else{
								errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, null,"catalog.focus.program.shop.code");
							}
						}else{
							errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, null,"catalog.focus.program.shop.code");
						}
					}else{
						errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null,"catalog.focus.program.shop.code");
					}
				}
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}		
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}
	
	/**
	 * @author hunglm16
	 * @since JUNE 12, 2014
	 * @description Import Excel Tồn kho chuẩn theo sản phẩm
	 * 
	 *  @author hunglm16
	 *  @since JUNE 19, 2014
	 *  @description Phan quyen NPP, Phan quyen VNM
	 * */
	public String importExcelTKCTN(){
		//IMPORT EXCEL TON KHO CHUAN THEO NGHANH
		/*resetToken(result);
		isError = true;
		totalItem = 0;
		try{
        	String message = "";
        	String msg = "";
            List<CellBean> lstFails = new ArrayList<CellBean>();
            List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 8);
            if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
            	boolean isUpdate = false;
				boolean isDelete = false;
				String value = "";
				List<ShopProduct> lstDel = new ArrayList<ShopProduct>();
				ShopProduct temp = new ShopProduct();
				ShopProduct tempExits = new ShopProduct();
				Integer numberInt = 0;
				
				for(int i=0; i<lstData.size(); i++){
					message = "";
					msg ="";
					totalItem++;
					temp = new ShopProduct();
					tempExits = new ShopProduct();
					List<String> row = lstData.get(i);
					isUpdate = false;
					isDelete = false;
                    if(row!= null && row.size() >=7){
                    	//Khoi tao gia tri ban dau
						temp.setType(ShopProductType.CAT);
						//Ma Don Vi
						value = row.get(0);
						if(!StringUtil.isNullOrEmpty(value)){
							Shop sTmp = shopMgr.getShopByCode(value.trim());
							temp.setShop(sTmp);									
							if(sTmp != null){
								if(ActiveType.RUNNING.equals(sTmp.getStatus())){
									if(sTmp.getType()!=null && ChannelTypeType.SHOP.getValue().equals(sTmp.getType().getType().getValue())){
										if (!checkShopInOrgAccessByCode(value)) {
											msg +=  ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_AREA);
										}
										else{
											temp.setShop(sTmp);
										}
									}else{												
										msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_INVALID_TYPE_SHOP, true,"catalog.focus.program.shop.code");
									}
								}else{
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true,"catalog.focus.program.shop.code");
								}
							}else{
								msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true,"catalog.focus.program.shop.code");
							}
						}else{
							msg += "Mã đơn vị không được để trống";
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += msg;
							message += "\n";
						}
						
						//Ma Nganh Hang
						value = row.get(1);
						msg ="";
						if(!StringUtil.isNullOrEmpty(value)){
							ProductInfo pTmp = productInfoMgr.getProductInfoByCode(value, ProductType.CAT,null,null);
							temp.setCat(pTmp);
							if(pTmp != null && temp.getShop() != null) {
								ShopProduct shopProductTmp = shopProductMgr.getShopProductByCat(temp.getShop().getId(), pTmp.getId());
								if(shopProductTmp != null) {//update
									isUpdate = true;
									//temp = shopProductTmp;
									if(productInfoMgr.isEquimentCat(pTmp.getProductInfoCode())){
										msg += "Mã ngành hàng nhập vào không được là ngành hàng thiết bị";
									}else{
										if(!ActiveType.RUNNING.equals(pTmp.getStatus())){
											msg += "Mã ngành hàng không còn hoạt động";
										}
									}
								} else {
									if(!productInfoMgr.isEquimentCat(pTmp.getProductInfoCode())){
										if(ActiveType.RUNNING.equals(pTmp.getStatus())){
											temp.setCat(pTmp);
										}else{
											msg += "Mã ngành hàng không còn hoạt động";
										}
									}else{
										msg += "Mã ngành hàng nhập vào không được là ngành hàng thiết bị";
									}
								}
							} else {
								if(pTmp != null){
									if(!productInfoMgr.isEquimentCat(pTmp.getProductInfoCode())){
										if(ActiveType.RUNNING.equals(pTmp.getStatus())){
											temp.setCat(pTmp);
										}else{
											msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true,"catalog.category.code");
										}
									}else{
										msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.excel.error.categorycode.equip");
									}
								}else{
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true,"catalog.category.code");
								}
							}
						}else{
							msg += "Mã Nghành không được để trống";
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += msg;
							message += "\n";
						}
						//Ton kho Min
						value = row.get(2);
						msg ="";
						if(!StringUtil.isNullOrEmpty(value)){
							msg += ValidateUtil.validateField(value.trim(), "catalog.minSF.code", 4, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if(StringUtil.isNullOrEmpty(msg)){
								numberInt = Integer.parseInt(value);
								if(numberInt<=0){
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_ZERO,true,"catalog.minSF.code");
								}else{
									temp.setMinsf(numberInt);
								}
							}
						}else{
							msg += "Tồn kho Min không được để trống";
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += msg;
							message += "\n";
						}
						
						//Ton kho max
						value = row.get(3);
						msg ="";
						if(!StringUtil.isNullOrEmpty(value)){
							msg += ValidateUtil.validateField(value.trim(), "catalog.maxSF.code", 4, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if(StringUtil.isNullOrEmpty(msg)){
								numberInt = Integer.parseInt(value);
								if(numberInt<=0){
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_ZERO,true,"catalog.maxSF.code");
								}else{
									temp.setMaxsf(numberInt);
									if(temp.getMinsf()!=null && temp.getMinsf() > numberInt){
										msg += "Tồn kho Min phải nhỏ hơn Tồn kho Max";
									}
								}
							}
						}else{
							msg += "Tồn kho Max không được để trống";
							msg += "\n";
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += msg;
							message += "\n";
						}
						
						//Ngay di duong
						value = row.get(4);
						msg ="";
						if(!StringUtil.isNullOrEmpty(value)){
							msg += ValidateUtil.validateField(value.trim(), "catalog.daygo.code", 3, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if(StringUtil.isNullOrEmpty(msg)){
								numberInt = Integer.parseInt(value);
								if(numberInt<=0){
									msg += ValidateUtil.getErrorMsg(ConstantManager.ERR_CATALOG_DAYGO);
								}else{
									temp.setLead(numberInt);
									if(temp.getMaxsf()!=null && temp.getMaxsf() < numberInt){
										msg += "Ngày đi đường không được lớn hơn Tồn kho Max";
									}
								}
							}
						}else{
							msg += "Ngày đi đường không được để trống";
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += msg;
							message += "\n";
						}
						
						//Ngay ban hang
						value = row.get(5);
						msg ="";
						if(!StringUtil.isNullOrEmpty(value)){
							if(value.trim().length()!=7){
								msg += "Ngày bán hàng bao gồm 7 ký tự là số thể hiện các ngày bán trong tuần";
								msg += "\n";
							}else{
								Pattern p = Pattern.compile("[0-7]+");
								Matcher m = p.matcher(value);
								if(m.matches()== false){
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID_FORMAT_CHARACTER,true,"catalog.daysale.code","catalog.daysale.format");
								} else if( checkSaleDayDuplicateAndOrder(value)==false){
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID_FORMAT,true,"catalog.daysale.code");
								} else{
									temp.setCalendarD(value);
								}
							}
						}else{
							msg += "Ngày bán hàng không được để trống";
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += msg;
							message += "\n";
						}
						
						//Ty Le Tang Truong
						value = row.get(6);
						msg = "";
						msg = ValidateUtil.validateField(value, "catalog.growth.code", null, ConstantManager.ERR_REQUIRE);
						if (StringUtil.isNullOrEmpty(msg)) {
							try {
								Float testvalue = Float.valueOf(value);
								DecimalFormat df = new DecimalFormat("#.##");
								testvalue = Float.valueOf(df.format(testvalue));
								if(testvalue <= 0 ){
									msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_POSSITIVE_NUMBER, true, "catalog.growth.code");
								} else {
									if(ValidateUtil.validateNumberEx(testvalue.toString(),3,null)== false){
										msg +=  ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_FLOAT_FORMAT_PRE, true, "catalog.growth.code", "catalog.growth.code.format.pre");
									} else{
										temp.setPercentage(testvalue);
									}
								}
							} catch (Exception e) {
								msg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_NUMBER, true, "catalog.growth.code");
							}
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += msg;
							message += "\n";
						}
						
						// Trang Thai
						value = row.get(7);
						msg ="";
						if(!StringUtil.isNullOrEmpty(value)){
							if(!"D".equals(value.trim().toUpperCase())){
								msg = "Trạng thái nhận 2 giá trị là Để trống hoặc D là xóa";
							}else if(isUpdate == true){
								temp.setStatus(ActiveType.DELETED);
								isDelete = true;
							}else if(isUpdate == false && StringUtil.isNullOrEmpty(msg)){
								msg += R.getResource("common.product.category.shop.not.exists")+"\n";
							}
						}else{
							//Kiem tra trung ngành hàng trong cung 1 don vi
							if(i>0){
								for(int j=0; j<i; j++){
									List<String> rowPr = lstData.get(j);
									if(rowPr!= null && rowPr.size() >= 8){
										if(!StringUtil.isNullOrEmpty(row.get(0)) && !StringUtil.isNullOrEmpty(rowPr.get(0)) 
												&& row.get(0).trim().toUpperCase().equals(rowPr.get(0).trim().toUpperCase())
												&& !StringUtil.isNullOrEmpty(row.get(1)) && !StringUtil.isNullOrEmpty(rowPr.get(1)) 
												&& row.get(1).trim().toUpperCase().equals(rowPr.get(1).trim().toUpperCase())
//												&& !StringUtil.isNullOrEmpty(row.get(5)) && !StringUtil.isNullOrEmpty(rowPr.get(5)) 
//												&& row.get(5).trim().toUpperCase().equals(rowPr.get(5).trim().toUpperCase())
										){
											msg += "Dòng thứ " + String.valueOf(i+1) +" và dòng thứ " + String.valueOf(j+1) + " trùng Ngành hàng trong cùng một Đơn vị";
											break;
										}										
									}
								}
							}
							if(!StringUtil.isNullOrEmpty(msg)){
								message += msg;
								message += "\n";
							}
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += "\n";
							message += msg;
						}
						
						if(StringUtil.isNullOrEmpty(message)){
							//Kiem tra duoi DB
							tempExits = shopProductMgr.getShopProductByImport(row.get(0), row.get(1), row.get(5), 1);//flag = 1: Nghanh hang
							if(tempExits!=null){
								if(!isDelete){
									temp.setId(tempExits.getId());
									//temp.setType(tempExits.getType());
									temp.setCat(tempExits.getCat());
									temp.setRegion(tempExits.getRegion());
									temp.setCalendarW(tempExits.getCalendarW());
									temp.setCreateDate(tempExits.getCreateDate());
									temp.setCreateUser(tempExits.getCreateUser());
									isUpdate = true;
								}
								else{
									temp.setId(tempExits.getId());
								}
							}
						}
						
						//XU LY TRONG TRUONG HOP THEM MOI
						msg = "";
						if(StringUtil.isNullOrEmpty(message) && !isUpdate) {
							if(temp.getShop() != null && temp.getProduct() != null){
								ObjectVO<ShopProduct> lstCategory = shopProductMgr.getListShopProduct(null, temp.getShop().getShopCode(), null, null, temp.getProduct().getProductCode(), ActiveType.RUNNING,ShopProductType.PRODUCT,null);
								if(lstCategory.getLstObject().size() > 0 ){
									msg += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.shop.product.code.not.exist",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.code"));
								}
							}
						}
						if(!StringUtil.isNullOrEmpty(msg)){
							message += msg;
							message += "\n";
						}
						
						//XU LY TRONG TRUONG HOP THOA MAN CAC DIEU KIEN
						if(StringUtil.isNullOrEmpty(message)){
							temp.setStatus(ActiveType.RUNNING);
							if(!isUpdate) {
								//Insert
								temp.setCreateUser(currentUser.getUserName());
								temp.setCreateDate(new Date());
								shopProductMgr.createShopProduct(temp,getLogInfoVO());
							} else {
								//Update
								if(isDelete){
									lstDel.add(temp);
								}else{
									temp.setUpdateUser(currentUser.getUserName());
									temp.setUpdateDate(new Date());
									shopProductMgr.updateShopProduct(temp, getLogInfoVO());
								}
							}
						}else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
                    }
				}
				if(lstDel!=null && lstDel.size()>0){
					ShopProductFilter filter = new ShopProductFilter();
					filter.setUserName(currentUser.getUserName());
					shopProductMgr.deteteListShopProduct(lstDel, getLogInfoVO(), filter);
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_CATEGORY_SHOP_FAIL);
            }else{
            	 isError = true;
                 errMsg = "Tập tin Excel chưa nhập dữ liệu Import";
            }
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
            isError = false;
		}*/
		return SUCCESS;
	}
	
	public ProductInfoMgr getProductInfoMgr() {
		return productInfoMgr;
	}

	public void setProductInfoMgr(ProductInfoMgr productInfoMgr) {
		this.productInfoMgr = productInfoMgr;
	}

	public ShopProductMgr getShopProductMgr() {
		return shopProductMgr;
	}

	public void setShopProductMgr(ShopProductMgr shopProductMgr) {
		this.shopProductMgr = shopProductMgr;
	}

	public StaffMgr getStaffMgr() {
		return staffMgr;
	}

	public void setStaffMgr(StaffMgr staffMgr) {
		this.staffMgr = staffMgr;
	}

	public ShopMgr getShopMgr() {
		return shopMgr;
	}

	public void setShopMgr(ShopMgr shopMgr) {
		this.shopMgr = shopMgr;
	}

	public String getTypeButton() {
		return typeButton;
	}

	public void setTypeButton(String typeButton) {
		this.typeButton = typeButton;
	}

	public List<CellBean> getListBeans() {
		return listBeans;
	}

	public void setListBeans(List<CellBean> listBeans) {
		this.listBeans = listBeans;
	}

	/**
	 * Delete category.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String deleteCategory(){
	    
	    resetToken(result);
	    boolean error = true;
	    try{
			if(categoryId != null && categoryId > 0){
				shopProduct = shopProductMgr.getShopProductById(categoryId);
				if(shopProduct!= null){
					shopProduct.setStatus(ActiveType.DELETED);
					shopProductMgr.updateShopProduct(shopProduct,getLogInfoVO());
					error = false;
				}
			}
		}catch (Exception e) {
		    LogUtility.logError(e, e.getMessage());
		}
	    result.put(ERROR, error);
	    return JSON;
	}

	
	public boolean checkSaleDayDuplicate(String value) {
		Map<Character, Boolean> saleDayCharMap = new HashMap<Character, Boolean>();
		for (int i = 0; i< value.length(); ++i){
			char ch = value.charAt(i);
			if(ch != '0') {
				if(saleDayCharMap.get(ch) == null) {
					saleDayCharMap.put(ch, true);
				} else {
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean checkSaleDayDuplicateAndOrder(String value) {
		Map<Character, Boolean> saleDayCharMap = new HashMap<Character, Boolean>();
		char lastOrder = '0';
		for (int i = 0; i< value.length(); ++i){
			char ch = value.charAt(i);
			if(ch < lastOrder) {
				return false;
			}
			if(ch != '0') {
				if(saleDayCharMap.get(ch) == null) {
					saleDayCharMap.put(ch, true);
				} else {
					return false;
				}
			}
			lastOrder = ch;
		}
		return true;
	}
	
	/**
	 * @author hunglm16
	 * @since JUNE 19, 2014
	 * @description Update Phan quyen cho chuc nang
	 * */
	public String exportExcel() {
	   /* try{
	    	if(lstcategory != null  && lstcategory.size() >0) {
	    		String s = lstcategory.get(0);
	    		if (StringUtil.isNullOrEmpty(s) || "0".equals(s)) {
	    			lstcategory = null;
		    	}
	    	}
			ActiveType temp = null;
	    	if(status != null && status != ConstantManager.NOT_STATUS){
	    		temp = ActiveType.parseValue(status);
	    	}
	    	Boolean flag = true;
	    	if(currentUser!= null){
				StaffMgr staffMgr = (StaffMgr)context.getBean("staffMgr");
				if(staffMgr!= null){
					staff = staffMgr.getStaffByCode(currentUser.getUserName());
					if(staff!= null && staff.getShop()!= null){
						if(!ShopObjectType.VNM.getValue().equals(staff.getShop().getType().getObjectType().intValue())
								&& !ShopObjectType.GT.getValue().equals(staff.getShop().getType().getObjectType().intValue())
						){
							flag = false;
						}
					}
				}
			}
	    	
	    	List<Long> lstPShopId = new ArrayList<Long>();
	    	for(ShopToken shop:currentUser.getListShop()){
	    		lstPShopId.add(shop.getShopId());
	    	}
			ObjectVO<ShopProduct> lstCategory = shopProductMgr.getListShopProduct1(null, shopCode, shopName, lstcategory, null, temp, ShopProductType.CAT, lstPShopId, flag);
			List<ShopProduct> lst = lstCategory.getLstObject();
			listBeans = new ArrayList<CellBean>();
			for (int i=0; i<lst.size();i++)
			{
				CellBean add = new CellBean();
				add.setContent1(lst.get(i).getShop().getShopCode());
				add.setContent2(lst.get(i).getCat().getProductInfoCode());
				add.setContent3(lst.get(i).getMinsf().toString());
				add.setContent4(lst.get(i).getMaxsf().toString());
				add.setContent5(lst.get(i).getLead().toString());
				add.setContent6(lst.get(i).getRegion());//vung
				add.setContent7(lst.get(i).getCalendarD().toString());
				add.setContent8(lst.get(i).getCalendarW()); //tuan mua hang
				add.setContent9(lst.get(i).getPercentage().toString());
				listBeans.add(add);
			}
			if(lst.isEmpty()){
				result.put("hasData", false);
				return JSON;
			}
			//session.setAttribute(ConstantManager.USER_SESSION_KEY,listBeans);
			exportExcelDataNotSession(listBeans,ConstantManager.EXPORT_CATALOG_CATEGORY_TYPE_SHOP_CAT_TEMPLATE);
			result.put(ERROR, false);
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }*/
	    return JSON;
	}

	public void setView(boolean view) {
		this.view = view;
	}

	public boolean isView() {
		return view;
	}
}

package ths.dms.action.catalog;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.SaleLevelCatMgr;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.SaleLevelCat;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
//import ths.dms.core.entities.ProductLevel;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.SaleLevelBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class LevelIncomeCatalogAction extends AbstractAction {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	//LEVEL-INCOME
	private File excelFile;
	private String excelFileContentType;
	private Long categoryType;
	private Integer status;
	//private List<ProductLevel> lstLevel;
	private List<ProductInfo> lstCategory;
	private SaleLevelCatMgr saleLevelCatMgr;
	private Integer levelId;
	private String categoryName;
	private BigDecimal fromIncome;
	private BigDecimal toIncome;
	private Long levelIncomeId;
	private SaleLevelCat levelIncome;
	private ProductInfoMgr productInfoMgr;
	private String levelName;
	private String levelCode;
	private List<SaleLevelBean> listSaleLevel;
	private List<CellBean> listBeans;
	private Integer typeView;
	private List<String> lstcategory;
	private List<String> listcategory;
	private String fromAmountFilter;
	private String toAmountFilter;
	private String order;
	private String sort;
	
	public String getFromAmountFilter() {
		return fromAmountFilter;
	}

	public void setFromAmountFilter(String fromAmountFilter) {
		this.fromAmountFilter = fromAmountFilter;
	}

	public String getToAmountFilter() {
		return toAmountFilter;
	}

	public void setToAmountFilter(String toAmountFilter) {
		this.toAmountFilter = toAmountFilter;
	}

	public List<String> getLstcategory() {
		return lstcategory;
	}

	public void setLstcategory(List<String> lstcategory) {
		this.lstcategory = lstcategory;
	}

	public List<String> getListcategory() {
		return listcategory;
	}

	public void setListcategory(List<String> listcategory) {
		this.listcategory = listcategory;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public List<SaleLevelBean> getListSaleLevel() {
		return listSaleLevel;
	}

	public void setListSaleLevel(List<SaleLevelBean> listSaleLevel) {
		this.listSaleLevel = listSaleLevel;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public SaleLevelCat getLevelIncome() {
		return levelIncome;
	}

	public void setLevelIncome(SaleLevelCat levelIncome) {
		this.levelIncome = levelIncome;
	}

	public Long getLevelIncomeId() {
		return levelIncomeId;
	}

	public void setLevelIncomeId(Long levelIncomeId) {
		this.levelIncomeId = levelIncomeId;
	}

	public BigDecimal getFromIncome() {
		return fromIncome;
	}

	public void setFromIncome(BigDecimal fromIncome) {
		this.fromIncome = fromIncome;
	}

	public BigDecimal getToIncome() {
		return toIncome;
	}

	public void setToIncome(BigDecimal toIncome) {
		this.toIncome = toIncome;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getLevelId() {
		return levelId;
	}

	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}

	public List<ProductInfo> getLstCategory() {
		return lstCategory;
	}

	public void setLstCategory(List<ProductInfo> lstCategory) {
		this.lstCategory = lstCategory;
	}

	//	public List<ProductLevel> getLstLevel() {
	//		return lstLevel;
	//	}
	//
	//	public void setLstLevel(List<ProductLevel> lstLevel) {
	//		this.lstLevel = lstLevel;
	//	}

	public Long getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(Long categoryType) {
		this.categoryType = categoryType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		saleLevelCatMgr = (SaleLevelCatMgr) context.getBean("saleLevelCatMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		try {
			ObjectVO<ProductInfo> lstCategoryTmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true,sort,order);
			lstCategory = lstCategoryTmp.getLstObject();
			List<String> lstLevelIncome = new ArrayList<String>();
			lstLevelIncome = saleLevelCatMgr.getListSaleLevel(null, null, null, ActiveType.RUNNING);
			listSaleLevel = new ArrayList<SaleLevelBean>();
			if (lstLevelIncome != null && lstLevelIncome.size() > 0) {
				for (int i = 0; i < lstLevelIncome.size(); i++) {
					SaleLevelBean saleLevelBean = new SaleLevelBean();
					saleLevelBean.setSaleLevel(lstLevelIncome.get(i));
					listSaleLevel.add(saleLevelBean);
				}
			}
			mapControl = getMapControlToken();
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}

		return SUCCESS;
	}

	/**
	 * Search.
	 * 
	 * @return the string
	 * @author lamnh ,phuocdh2 modified : sort ,order grid result
	 * @since 18/07/2012
	 */
	public String search() {
		
		result.put("page", page);
		result.put("rows", rows);
		try {
			KPaging<SaleLevelCat> kPaging = new KPaging<SaleLevelCat>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			if (categoryType != null && categoryType == -1) {
				categoryType = null;
			}
			ActiveType temp = null;
			if (status != null && status != ConstantManager.NOT_STATUS) {
				temp = ActiveType.parseValue(status);
			}
			listcategory = new ArrayList<String>();
			if (lstcategory != null && lstcategory.size() > 0 && !lstcategory.get(0).equalsIgnoreCase("0") && !lstcategory.get(0).equalsIgnoreCase("")) {
				String s = lstcategory.get(0);
				String[] s1 = s.split(",");

				for (int i = 0; i < s1.length; i++) {
					listcategory.add(s1[i]);
				}
			}
	
			//phuocdh2 modified
			if (!StringUtil.isNullOrEmpty(sort)) {
				if ( "cat.productInfoCode".equals(sort) ) {
					sort = " s.product_info_code ";
				} else if ( "saleLevelCode".equals(sort) ) {
					sort = " sc.sale_level_code ";
				} else if ( "saleLevelName".equals(sort) ) {
					sort = " sc.sale_level_name ";
				} else if ( "fromAmountString".equals(sort) ) {
					sort = " nvl(sc.from_amount,0) ";
				} else if ( "toAmountString".equals(sort) ) {
					sort = " nvl(sc.to_amount,0) ";
				} else if ( "status".equals(sort) ) {
					sort = "sc.status";
				}
			}
			BigDecimal toAmt = null;
			BigDecimal fromAmt = null;
			if (!StringUtil.isNullOrEmpty(fromAmountFilter)) {
				fromAmt = new BigDecimal(fromAmountFilter);
			}
			if (!StringUtil.isNullOrEmpty(toAmountFilter)) {
				toAmt = new BigDecimal(toAmountFilter);
			}
			ObjectVO<SaleLevelCat> lstLevelIncome = saleLevelCatMgr.getListSaleLevelCatByProductInfo(kPaging, listcategory, levelCode, levelName, fromAmt, toAmt, temp,sort,order);

			if (lstLevelIncome != null) {
				result.put("total", lstLevelIncome.getkPaging().getTotalRows());
				result.put("rows", lstLevelIncome.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Save or update.
	 * 
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012
	 */
	public String saveOrUpdate() {

		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(levelName, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(levelCode, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				levelIncome = saleLevelCatMgr.getSaleLevelCatById(levelIncomeId);
				if (levelIncomeId != null && levelIncomeId > 0) {
					if (levelIncome != null) {
						//						if(categoryType != null && categoryType != -1){
						//							ProductInfo pTmp = productInfoMgr.getProductInfoById(categoryType);
						//							levelIncome.setCat(pTmp);
						//							if(levelCode != null && !saleLevelCatMgr.checkIfRecordExist(categoryType, levelCode, levelIncomeId)){
						levelIncome.setSaleLevelName(levelName);
						//								levelIncome.setSaleLevelCode(levelCode);
						if (status != null && status != ConstantManager.NOT_STATUS) {
							levelIncome.setStatus(ActiveType.parseValue(status));
						}
						levelIncome.setFromAmount(fromIncome);
						levelIncome.setToAmount(toIncome);
						levelIncome.setUpdateDate(commonMgr.getSysDate());
						saleLevelCatMgr.updateSaleLevelCat(levelIncome, getLogInfoVO());
						error = false;
						//							}else{
						//								errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST,null,"catalog.level.code");
						//							}
						//						}else{
						//							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,null,"catalog.category.code");
						//						}
					}
				} else {
					levelIncome = new SaleLevelCat();
					if (categoryType != null && categoryType != -1) {
						ProductInfo pTmp = productInfoMgr.getProductInfoById(categoryType);
						levelIncome.setCat(pTmp);
						if (levelCode != null && !saleLevelCatMgr.checkIfRecordExist(categoryType, levelCode, null)) {
							levelIncome.setSaleLevelName(levelName);
							levelIncome.setSaleLevelCode(levelCode.toUpperCase());
							if (status != null && status != ConstantManager.NOT_STATUS) {
								levelIncome.setStatus(ActiveType.parseValue(status));
							}
							levelIncome.setFromAmount(fromIncome);
							levelIncome.setToAmount(toIncome);
							levelIncome.setCreateDate(commonMgr.getSysDate());
							saleLevelCatMgr.createSaleLevelCat(levelIncome, getLogInfoVO());
							error = false;
						} else {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.level.code.standard");
						}
					} else {
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.level.code.standard");
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Import excel file.
	 * 
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String importExcelFile() {
		isError = true;
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 4);
		// phan xem Excel -------------------------------------------------------------------
		listBeans = new ArrayList<CellBean>();
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				for (int i = 0; i < lstData.size(); i++) {
					CellBean add = new CellBean();
					add.setContent1(lstData.get(i).get(0));
					add.setContent2(lstData.get(i).get(1));
					add.setContent3(lstData.get(i).get(2));
					add.setContent4(lstData.get(i).get(3));
					listBeans.add(add);
				}
			} catch (Exception ee) {
				result.put(ERROR, "catalog_customer_error_upload");
			}
		}
		// End view Excel File ----------------------------------------------------------
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						message = "";
						totalItem++;
						SaleLevelCat temp = new SaleLevelCat();
						List<String> row = lstData.get(i);
						ProductInfo pTmp = null;
						//Mã ngành hàng
						if (row.size() > 0) {
							String value = row.get(0);
							if (!StringUtil.isNullOrEmpty(value)) {
								pTmp = productInfoMgr.getProductInfoByCode(value, ProductType.CAT, null, null);
								if (pTmp != null) {
									if (ActiveType.RUNNING.equals(pTmp.getStatus())) {
										temp.setCat(pTmp);
									} else {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.category.code");
									}
								} else {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.category.code");
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.category.code");
							}
						}
						//Mức
						if (row.size() > 1) {
							String value = row.get(1);
							String checkMessage = ValidateUtil.validateField(value, "catalog.level.code", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(checkMessage)) {
								if (pTmp != null) {
									if (!saleLevelCatMgr.checkIfRecordExist(pTmp.getId(), value, null)) {
										//temp.setSaleLevel(value);
									} else {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_GENERAL, true, "catalog.level.code", "catalog.categoryvn");
									}
								}
							} else {
								message += checkMessage;
							}
						}
						//DS từ
						if (row.size() > 2) {
							String value = row.get(2);
							String checkMessage = ValidateUtil.validateField(value, "catalog.toamount.code", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (StringUtil.isNullOrEmpty(checkMessage)) {
								try {
									BigDecimal testvalue = new BigDecimal(value);
									if (testvalue.compareTo(BigDecimal.ZERO) != 1) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_POSSITIVE_NUMBER, true, "catalog.toamount.code");
									} else {
										temp.setFromAmount(BigDecimal.valueOf(Double.valueOf(value)));
									}
								} catch (Exception e) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_INT, true, "catalog.toamount.code");
								}
							} else {
								message += checkMessage;
							}
						}
						//Đến DS
						if (row.size() > 3) {
							String value = row.get(3);
							if (!StringUtil.isNullOrEmpty(value)) {
								String checkMessage = ValidateUtil.validateField(value, "catalog.fromamount.code", 20, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
								if (StringUtil.isNullOrEmpty(checkMessage)) {
									try {
										BigDecimal testvalue = new BigDecimal(value);
										if (testvalue.compareTo(BigDecimal.ZERO) != 1) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_POSSITIVE_NUMBER, true, "catalog.fromamount.code");
										} else {
											temp.setToAmount(BigDecimal.valueOf(Double.valueOf(value)));
										}
									} catch (Exception e) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_INT, true, "catalog.fromamount.code");
									}
								} else {
									message += checkMessage;
								}
							}
						}
						if (temp.getFromAmount() != null && temp.getToAmount() != null) {
							if (temp.getFromAmount().intValue() > temp.getToAmount().intValue()) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.fromamount.code.greater");
							}
						}
						if (typeView == 1) {
							if (StringUtil.isNullOrEmpty(message))
								listBeans.get(i).setContent5("OK");
							else {
								message = StringUtil.convertHTMLBreakLine(message);
								listBeans.get(i).setContent5(message);
							}
							if (i == (lstData.size() - 1))
								return "viewExcel";
						} else {
							if (StringUtil.isNullOrEmpty(message)) {
								try {
									saleLevelCatMgr.createSaleLevelCat(temp, getLogInfoVO());
								} catch (BusinessException be) {
									LogUtility.logError(be, be.getMessage());
								}
							} else {
								lstFails.add(StringUtil.addFailBean(row, message));
							}
						}
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_CATEGORY_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Delete level income.
	 * 
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String deleteLevelIncome() {

		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				if (levelIncomeId != null && levelIncomeId > 0) {
					levelIncome = saleLevelCatMgr.getSaleLevelCatById(levelIncomeId);
					if (levelIncome != null) {
						levelIncome.setStatus(ActiveType.DELETED);
						saleLevelCatMgr.updateSaleLevelCat(levelIncome, getLogInfoVO());
						error = false;
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		return JSON;
	}

	/**
	 * View detail.
	 * 
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String viewDetail() {
		resetToken(result);
		try {
			if (levelIncomeId != null && levelIncomeId > 0) {
				levelIncome = saleLevelCatMgr.getSaleLevelCatById(levelIncomeId);
				if (levelIncome != null && levelIncome.getCat() != null) {
					categoryType = levelIncome.getCat().getId();
					status = levelIncome.getStatus().getValue();
				}
			}
			ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true, null, null); //phuocdh2 modified them sort
			lstCategory = tmp.getLstObject();
			ProductInfo pf = productInfoMgr.getProductInfoById(categoryType);
			if (pf != null && !pf.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
				lstCategory.add(0, pf);
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return LIST;
	}

	//cuongnd

	public String exportExcel() {
		result.put("page", page);
		result.put("max", max);
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			if (categoryType != null && categoryType == -1) {
				categoryType = null;
			}
			ActiveType temp = null;
			if (status != null && status != ConstantManager.NOT_STATUS) {
				temp = ActiveType.parseValue(status);
			}
			if (!StringUtil.isNullOrEmpty(levelName) && levelName.equals("-1")) {
				levelName = null;
			}
			ObjectVO<SaleLevelCat> lstLevelIncome = saleLevelCatMgr.getListSaleLevelCat(null, categoryType, categoryName, levelName, temp);
			List<SaleLevelCat> lst = lstLevelIncome.getLstObject();
			listBeans = new ArrayList<CellBean>();
			for (int i = 0; i < lst.size(); i++) {
				CellBean add = new CellBean();
				add.setContent1(lst.get(i).getCat().getProductInfoCode());
				//add.setContent2(lst.get(i).getSaleLevel());
				add.setContent3(lst.get(i).getFromAmountString());
				add.setContent4(lst.get(i).getToAmountString());
				listBeans.add(add);
			}
			//session.setAttribute(ConstantManager.USER_SESSION_KEY,listBeans);
			exportExcelDataNotSession(listBeans, ConstantManager.TEMPLATE_LEVEL_INCOME_EXPORT);
			result.put(ERROR, false);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * The Class SaleLevelVO.
	 * 
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public class SaleLevelVO {
		private String saleLevel;

		public String getSaleLevel() {
			return saleLevel;
		}

		public void setSaleLevel(String saleLevel) {
			this.saleLevel = saleLevel;
		}
	}

	private String typeButton;

	public SaleLevelCatMgr getSaleLevelCatMgr() {
		return saleLevelCatMgr;
	}

	public void setSaleLevelCatMgr(SaleLevelCatMgr saleLevelCatMgr) {
		this.saleLevelCatMgr = saleLevelCatMgr;
	}

	public ProductInfoMgr getProductInfoMgr() {
		return productInfoMgr;
	}

	public void setProductInfoMgr(ProductInfoMgr productInfoMgr) {
		this.productInfoMgr = productInfoMgr;
	}

	public List<CellBean> getListBeans() {
		return listBeans;
	}

	public void setListBeans(List<CellBean> listBeans) {
		this.listBeans = listBeans;
	}

	public String getTypeButton() {
		return typeButton;
	}

	public void setTypeButton(String typeButton) {
		this.typeButton = typeButton;
	}

	public void setTypeView(Integer typeView) {
		this.typeView = typeView;
	}

	public Integer getTypeView() {
		return typeView;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
}

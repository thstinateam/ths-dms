/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.Date;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ApParamVO;
import ths.dms.core.entities.vo.BankVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class ApparamCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	private ApParamMgr apParamMgr;

	private String apParamCode;
	private String apParamName;
	private String type;
	private String description;
	private Integer status;
	private Long shopId;
	private String shopCode;
	private Shop shop;
	private Long idParam;
	private String value;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
	}

	@Override
	public String execute() {
		try {
			resetToken(result);
			if (currentUser != null && currentUser.getShopRoot() != null) {
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				if(shop != null){
					shopId = shop.getId();
					shopCode = shop.getShopCode();
				}
			} else {
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NO_DATA)); // loi du lieu khong ton tai shop
				return JSON;
			}
			lstActiveType = apParamMgr.getListActiveType(ApParamType.STATUS_TYPE);
		} catch (BusinessException e) {
			Date startDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", " ths.dms.action.catalog.ApparamCatalogAction.execute()"), createLogErrorStandard(startDate));
		}
		return SUCCESS;
	}

	/**
	 * Search.
	 * @return the string
	 * @author phuongvm
	 * @since 06/10/2014
	 */
	public String search() {
		result.put("page", page);
	    result.put("max", max);
		try {
			ApParamFilter filter = new ApParamFilter();
			if(!StringUtil.isNullOrEmpty(apParamCode)){
				filter.setApParamCode(apParamCode);
			}
			if(!StringUtil.isNullOrEmpty(apParamName)){
				filter.setApParamName(apParamName);
			}
			if(!StringUtil.isNullOrEmpty(description)){
				filter.setDescription(description);
			}
			if(!StringUtil.isNullOrEmpty(type)){
				filter.setTypeStr(type);
			}
			if(status != null &&  status>-1){
				ActiveType st = ActiveType.parseValue(status);
				filter.setStatus(st);
			}
			KPaging<ApParamVO> paging = new KPaging<ApParamVO>();
			paging.setPageSize(max);
			paging.setPage(page-1);
			filter.setPagingVO(paging);
			/** sort Grid*/
			filter.setOrder(order);
			filter.setSort(sort);
			ObjectVO<ApParamVO> vo = apParamMgr.getListApParamByFilterEx(filter);
			if (vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<BankVO>());
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, "ApparamCatalogAction.search()"+ e.getMessage());
			Date startDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ApparamCatalogAction.search()"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Save or update. them moi va cap nhat tham so
	 * @return the string
	 * @author phuongvm
	 * @since 07/10/2014
	 */
	public String saveOrUpdate() {
		resetToken(result);
		try {
			if (currentUser.getUserName() != null) {
				errMsg = ValidateUtil.validateField(apParamName, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(apParamCode, "catalog.pram.code", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(value, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(description, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(type, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				if (idParam != null && idParam > 0) {
					// cap nhat tham số
					ApParam ap = apParamMgr.getApParamById(idParam);
					if (ap != null) {
						/** khong cho sua ma tham so**/
						ap.setApParamName(apParamName);
						ap.setValue(value);
						ap.setDescription(description);
						if (!StringUtil.isNullOrEmpty(type)) {
							ApParamType typeAP = ApParamType.parseValue(type.toUpperCase());
							if (typeAP != null) {
								ap.setType(typeAP);
							} else {
								ap.setTypeStr(type);
							}
						} else {
							ap.setType(null);
						}
						if( status != null){
							ActiveType st = ActiveType.parseValue(status);
							ap.setStatus(st);
						}
						apParamMgr.updateApParamEx(ap, getLogInfoVO());
					}
				} else {
					// them moi
					ApParam ap = null;
					if(!StringUtil.isNullOrEmpty(apParamCode)){
						ap = apParamMgr.getApParamByCodeEx(apParamCode, ActiveType.RUNNING);
						if(ap != null){
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.pram.code")); // Ma tham so da ton tai trong he thong
							return JSON;
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.pram.code")); // Ban chua nhap gia tri cho truong Ma tham so
						return JSON;
					}
					ap = new ApParam();
					ap.setApParamCode(apParamCode);
					ap.setApParamName(apParamName);
					ap.setValue(value);
					ap.setDescription(description);
					if (!StringUtil.isNullOrEmpty(type)) {
						ApParamType typeAP = ApParamType.parseValue(type.toUpperCase());
						if (typeAP != null) {
							ap.setType(typeAP);
						} else {
							ap.setTypeStr(type);
						}
					}
					if( status != null){
						ActiveType st = ActiveType.parseValue(status);
						ap.setStatus(st);
					}
					ap = apParamMgr.createApParamEx(ap,  getLogInfoVO());
					if (ap == null) {
						result.put(ERROR, true);
						result.put("errMsg",  R.getResource("catalog.param.err.create")); // loi tao tham so
						return JSON;
					}
				}
			} else{
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("catalog.bank.err.user"));
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, "ApparamCatalogAction.saveOrUpdate()"+ e.getMessage());
			Date startDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.ApparamCatalogAction.saveOrUpdate()"), createLogErrorStandard(startDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ApParamMgr getApParamMgr() {
		return apParamMgr;
	}

	public void setApParamMgr(ApParamMgr apParamMgr) {
		this.apParamMgr = apParamMgr;
	}

	public String getApParamCode() {
		return apParamCode;
	}

	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}

	public String getApParamName() {
		return apParamName;
	}

	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getIdParam() {
		return idParam;
	}

	public void setIdParam(Long idParam) {
		this.idParam = idParam;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}

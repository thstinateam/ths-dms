package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.ValidateUtil;

public class BrandCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	/** The id. */
	private Long id;

	/** The code. */
	private String code;

	/** The name. */
	private String name;

	/** The note. */
	private String note;

	/** The status. */
	private String status;

	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;

	private ProductMgr productMgr;

	private CommonMgr commonMgr;

	private List<Long> lstProducts;

	private Long productId;





	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public List<Long> getLstProducts() {
		return lstProducts;
	}

	public void setLstProducts(List<Long> lstProducts) {
		this.lstProducts = lstProducts;
	}

	/**
	 * getId
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * setId
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * getCode
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * setCode
	 *
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * getName
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * setName
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * getNote
	 *
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * setNote
	 *
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * getStatus
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * setStatus
	 *
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {
		status = ActiveType.RUNNING.getValue().toString();
		resetToken(result);
		return SUCCESS;
	}

	/**
	 * Search.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 18/07/2012
	 */
	public String search() {

		result.put("page", page);
	    result.put("rows", rows);
//		try {
			/*KPaging<ProductInfo> kPaging = new KPaging<ProductInfo>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			ObjectVO<ProductInfo> lstReasonGroup = productInfoMgr.getListProductInfo(kPaging, code, name, note,
							ActiveType.RUNNING,ProductType.BRAND,true);
			if (lstReasonGroup != null) {
				result.put("total", lstReasonGroup.getkPaging().getTotalRows());
				result.put("rows", lstReasonGroup.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<ProductInfo>());
			}*/
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
		return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 18/07/2012
	 */
	public String saveOrUpdate() {

		// resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				ProductInfo productInfo = productInfoMgr.getProductInfoByCode(
						code, ProductType.BRAND,null,true);
				if (id != null && id > 0) {
					if (productInfo != null) {
						if (id.intValue() != productInfo.getId().intValue()) {
							result.put("errMsg", ValidateUtil
									.getErrorMsgQuickly(
											ConstantManager.ERR_EXIST, null,
											"catalog.brand.code"));
						} else {
							productInfo.setProductInfoCode(code);
							productInfo.setProductInfoName(name);
							productInfo.setDescription(note);
							productInfo.setStatus(ActiveType.parseValue(Integer
									.valueOf(status)));
							productInfo.setType(ProductType.BRAND);
							productInfoMgr.updateProductInfo(productInfo,
									getLogInfoVO());
							error = false;
						}
					}
				} else {
					if (productInfo != null) {
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(
								ConstantManager.ERR_EXIST, null,
								"catalog.brand.code"));
					} else {
						ProductInfo newProductInfo = new ProductInfo();
						newProductInfo.setProductInfoCode(code);
						newProductInfo.setProductInfoName(name);
						newProductInfo.setDescription(note);
						newProductInfo.setStatus(ActiveType.parseValue(Integer
								.valueOf(status)));
						newProductInfo.setType(ProductType.BRAND);
						newProductInfo = productInfoMgr.createProductInfo(
								newProductInfo, getLogInfoVO());
						if (newProductInfo != null) {
							error = false;
						}
					}
				}
			} catch (BusinessException e) {
//				LogUtility.logError(e, e.getMessage());
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.BrandCatalogAction.saveOrUpdate()"), createLogErrorStandard(startLogDate));
			}
		}
		result.put(ERROR, error);
		return JSON;
	}

	/**
	 * @author tientv11
	 * Danh sach san pham thuoc Brand
	 */
	public String loadProduct(){
		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<Product>());
			KPaging<Product> kPaging = new KPaging<Product>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			ProductFilter filter = new ProductFilter();
			filter.setBrandId(id);
			filter.setkPaging(kPaging);
			filter.setStatus(ActiveType.RUNNING);
			ObjectVO<Product> objectVO = productMgr.getListProductEx(filter);
			if (objectVO != null) {
				result.put("total", objectVO.getkPaging().getTotalRows());
				result.put("rows", objectVO.getLstObject());
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.BrandCatalogAction.loadProduct()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Set Brand_id = null cua san pham
	 * @author tientv11
	 *
	 */

	public String resetBrandProduct(){
		result.put(ERROR, true);
		Date sysDate = DateUtil.now();
		try {
			sysDate = commonMgr.getSysDate();
			Product pr = productMgr.getProductById(productId);
			if(pr == null){
				return JSON;
			}
			pr.setBrand(null);
			pr.setUpdateDate(sysDate);
			pr.setUpdateUser(currentUser.getUserName().toUpperCase());
			commonMgr.updateEntity(pr);
			result.put(ERROR, false);
		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.BrandCatalogAction.resetBrandProduct()"), createLogErrorStandard(sysDate));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}


	/**
	 * Set Thiet lap nhan hang cho san pham
	 * @author tientv11
	 *
	 */

	public String addBrandProduct(){
		result.put(ERROR, true);
		try {
			List<Product> PRODUCTS = new ArrayList<Product>();
			Date sysDate = commonMgr.getSysDate();
			ProductInfo productInfo = commonMgr.getEntityById(ProductInfo.class, id);
			if(productInfo == null){
				return JSON;
			}
			for(Long productId : lstProducts){
				Product pr = productMgr.getProductById(productId);
				pr.setBrand(productInfo);
				pr.setUpdateDate(sysDate);
				pr.setUpdateUser(currentUser.getUserName().toUpperCase());
				PRODUCTS.add(pr);
			}
			commonMgr.updateListEntity(PRODUCTS);
			result.put(ERROR, false);
		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.BrandCatalogAction.addBrandProduct()"), createLogErrorStandard(startLogDate));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}



	/**
	 * Delete brand.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 18/07/2012
	 */
	public String deleteBrand() {

		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				ProductInfo productInfo = productInfoMgr.getProductInfoByCode(
						code, ProductType.BRAND,null,true);
				if (productInfo != null) {
					if (productInfoMgr.isUsingByOthers(productInfo.getId())) {
						result.put("errMsg", ValidateUtil.getErrorMsg(
								ConstantManager.ERR_IS_USED_DB, Configuration
										.getResourceString(
												ConstantManager.VI_LANGUAGE,
												"catalog.brand")));
					} else {
						productInfo.setStatus(ActiveType.DELETED);
						productInfoMgr.updateProductInfo(productInfo,
								getLogInfoVO());
						error = false;
					}
				}

			} catch (BusinessException e) {
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.BrandCatalogAction.addBrandProduct()"), createLogErrorStandard(startLogDate));
//				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		return JSON;
	}

}

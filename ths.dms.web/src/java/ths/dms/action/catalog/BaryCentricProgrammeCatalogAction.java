package ths.dms.action.catalog;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.FocusProgramMgr;
import ths.dms.core.business.LogMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.entities.ActionAudit;
import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.FocusChannelMap;
import ths.dms.core.entities.FocusChannelMapProduct;
import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActionAuditType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.FocusProgramFilter;
import ths.dms.core.entities.enumtype.HTBHFilter;
import ths.dms.core.entities.enumtype.HTBHVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.vo.NewTreeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.JsPromotionShopTreeNode;
import ths.dms.web.bean.TreeNode;
import ths.dms.web.business.common.TreeUtility;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.VSARole;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class BaryCentricProgrammeCatalogAction extends AbstractAction {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8051124597792850084L;

	/** The focus program mgr. */
	private FocusProgramMgr focusProgramMgr;

	/** The shop mgr. */
	private ShopMgr shopMgr;

	private ApParamMgr apParamMgr;
	/** The product mgr. */
	private ProductMgr productMgr;

	private CommonMgr commonMgr;

	/** The id. */
	private Long id;

	private Boolean isAdmin;

	private Integer userHO;

	public Integer getUserHO() {
		return userHO;
	}

	public void setUserHO(Integer userHO) {
		this.userHO = userHO;
	}

	private Long excelFCId;

	/** The shop id. */
	private Long shopId;

	/** The code. */
	private String code;

	/** The name. */
	private String name;

	/** The shop code. */
	private String shopCode;

	/** The shop name. */
	private String shopName;

	/** The from date. */
	private String fromDate;

	/** The to date. */
	private String toDate;

	/** The excel file. */
	private File excelFile;

	/** The excel file content type. */
	private String excelFileContentType;

	/** The lst shop. */
	private List<Shop> lstRegion;

	/** The lst area. */
	private List<Shop> lstArea;

	/** The shop map id. */
	private Long shopMapId;

	/** The region id. */
	private Long regionId;

	/** The area id. */
	private Long areaId;

	/** The channel map id. */
	private Long channelMapId;

	/** The staff type code. */
	private String staffTypeCode;

	/** The staff type id. */
	private Long staffTypeId;

	/** The cat code. */
	private String catCode;

	/** The sub cat code. */
	private String subCatCode;

	/** The product code. */
	private String productCode;

	/** The product id. */
	private Long productId;

	/** The lst staff type. */
	private List<FocusChannelMap> lstStaffType;

	private int excelType;

	private Integer status;

	/** The lst shop id. */
	private List<Long> lstShopId;

	/** The lst product id. */
	private List<Long> lstProductId;

	/** The channel type id. */
	private Long channelTypeId;

	/** The lst channel type. */
	private List<ApParam> lstChannelType;
	private List<ApParam> lstType;

	/** The permission edit. */
	private boolean permissionEdit;

	/** The tab order. */
	private String tabOrder;

	/** The type. */
	/*private int type;*/
	private Integer type;

	private Integer numUpdate;
	private List<Long> listShopId;
	private LogMgr logMgr;

	private Boolean isUpdate;
	private Long focusMapProductId;
	private String focusProductType;
	private List<FocusChannelMap> lstFocusChannelMap;
	private String importProgram;
	private Integer subContentId;

	private Integer userHo;

	private String promotionProgramCode;
	private String promotionProgramName;
	private Long focusId;
	private List<TreeNode> searchUnitTree;

	public Integer getUserHo() {
		return userHo;
	}

	public void setUserHo(Integer userHo) {
		this.userHo = userHo;
	}

	public Integer getSubContentId() {
		return subContentId;
	}

	public void setSubContentId(Integer subContentId) {
		this.subContentId = subContentId;
	}

	public List<FocusChannelMap> getLstFocusChannelMap() {
		return lstFocusChannelMap;
	}

	public void setLstFocusChannelMap(List<FocusChannelMap> lstFocusChannelMap) {
		this.lstFocusChannelMap = lstFocusChannelMap;
	}

	public String getFocusProductType() {
		return focusProductType;
	}

	public void setFocusProductType(String focusProductType) {
		this.focusProductType = focusProductType;
	}

	public Boolean getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(Boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	public Long getFocusMapProductId() {
		return focusMapProductId;
	}

	public void setFocusMapProductId(Long focusMapProductId) {
		this.focusMapProductId = focusMapProductId;
	}

	/**
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * @return the shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public Long getExcelFCId() {
		return excelFCId;
	}

	public void setExcelFCId(Long excelFCId) {
		this.excelFCId = excelFCId;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * @param shopCode the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the excelFile
	 */
	public File getExcelFile() {
		return excelFile;
	}

	/**
	 * @param excelFile the excelFile to set
	 */
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	/**
	 * @return the excelFileContentType
	 */
	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	/**
	 * @param excelFileContentType the excelFileContentType to set
	 */
	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/**
	 * @return the lstRegion
	 */
	public List<Shop> getLstRegion() {
		return lstRegion;
	}

	/**
	 * @param lstRegion the lstRegion to set
	 */
	public void setLstRegion(List<Shop> lstRegion) {
		this.lstRegion = lstRegion;
	}

	/**
	 * @return the lstArea
	 */
	public List<Shop> getLstArea() {
		return lstArea;
	}

	/**
	 * @param lstArea the lstArea to set
	 */
	public void setLstArea(List<Shop> lstArea) {
		this.lstArea = lstArea;
	}

	/**
	 * @return the shopMapId
	 */
	public Long getShopMapId() {
		return shopMapId;
	}

	/**
	 * @param shopMapId the shopMapId to set
	 */
	public void setShopMapId(Long shopMapId) {
		this.shopMapId = shopMapId;
	}

	/**
	 * @return the regionId
	 */
	public Long getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the areaId
	 */
	public Long getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId the areaId to set
	 */
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the channelMapId
	 */
	public Long getChannelMapId() {
		return channelMapId;
	}

	/**
	 * @param channelMapId the channelMapId to set
	 */
	public void setChannelMapId(Long channelMapId) {
		this.channelMapId = channelMapId;
	}

	/**
	 * @return the staffTypeCode
	 */
	public String getStaffTypeCode() {
		return staffTypeCode;
	}

	/**
	 * @param staffTypeCode the staffTypeCode to set
	 */
	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	/**
	 * @return the staffTypeId
	 */
	public Long getStaffTypeId() {
		return staffTypeId;
	}

	/**
	 * @param staffTypeId the staffTypeId to set
	 */
	public void setStaffTypeId(Long staffTypeId) {
		this.staffTypeId = staffTypeId;
	}

	/**
	 * @return the catCode
	 */
	public String getCatCode() {
		return catCode;
	}

	/**
	 * @param catCode the catCode to set
	 */
	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	/**
	 * @return the subCatCode
	 */
	public String getSubCatCode() {
		return subCatCode;
	}

	/**
	 * @param subCatCode the subCatCode to set
	 */
	public void setSubCatCode(String subCatCode) {
		this.subCatCode = subCatCode;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * @return the lstStaffType
	 */
	public List<FocusChannelMap> getLstStaffType() {
		return lstStaffType;
	}

	/**
	 * @param lstStaffType the lstStaffType to set
	 */
	public void setLstStaffType(List<FocusChannelMap> lstStaffType) {
		this.lstStaffType = lstStaffType;
	}

	/**
	 * @return the excelType
	 */
	public int getExcelType() {
		return excelType;
	}

	/**
	 * @param excelType the excelType to set
	 */
	public void setExcelType(int excelType) {
		this.excelType = excelType;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the lstShopId
	 */
	public List<Long> getLstShopId() {
		return lstShopId;
	}

	/**
	 * @param lstShopId the lstShopId to set
	 */
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	/**
	 * @return the lstProductId
	 */
	public List<Long> getLstProductId() {
		return lstProductId;
	}

	/**
	 * @param lstProductId the lstProductId to set
	 */
	public void setLstProductId(List<Long> lstProductId) {
		this.lstProductId = lstProductId;
	}

	/**
	 * @return the channelTypeId
	 */
	public Long getChannelTypeId() {
		return channelTypeId;
	}

	/**
	 * @param channelTypeId the channelTypeId to set
	 */
	public void setChannelTypeId(Long channelTypeId) {
		this.channelTypeId = channelTypeId;
	}


	public List<ApParam> getLstChannelType() {
		return lstChannelType;
	}

	public void setLstChannelType(List<ApParam> lstChannelType) {
		this.lstChannelType = lstChannelType;
	}

	public List<ApParam> getLstType() {
		return lstType;
	}

	public void setLstType(List<ApParam> lstType) {
		this.lstType = lstType;
	}

	/**
	 * @return the permissionEdit
	 */
	public boolean isPermissionEdit() {
		return permissionEdit;
	}

	/**
	 * @param permissionEdit the permissionEdit to set
	 */
	public void setPermissionEdit(boolean permissionEdit) {
		this.permissionEdit = permissionEdit;
	}

	/**
	 * @return the tabOrder
	 */
	public String getTabOrder() {
		return tabOrder;
	}

	/**
	 * @param tabOrder the tabOrder to set
	 */
	public void setTabOrder(String tabOrder) {
		this.tabOrder = tabOrder;
	}



	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @return the numUpdate
	 */
	public Integer getNumUpdate() {
		return numUpdate;
	}

	/**
	 * @param numUpdate the numUpdate to set
	 */
	public void setNumUpdate(Integer numUpdate) {
		this.numUpdate = numUpdate;
	}

	public List<Long> getListShopId() {
		return listShopId;
	}

	public void setListShopId(List<Long> listShopId) {
		this.listShopId = listShopId;
	}

	public String getImportProgram() {
		return importProgram;
	}

	public void setImportProgram(String importProgram) {
		this.importProgram = importProgram;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		focusProgramMgr = (FocusProgramMgr)context.getBean("focusProgramMgr");
		shopMgr = (ShopMgr)context.getBean("shopMgr");
		productMgr = (ProductMgr)context.getBean("productMgr");
		apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
		logMgr = (LogMgr)context.getBean("logMgr");
		commonMgr = (CommonMgr)context.getBean("commonMgr");
		if (StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType())) {
			isAdmin = true;
		} else {
			isAdmin = false;
		}
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		status = ActiveType.RUNNING.getValue();
		if (checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) {
			userHo = 1;
		} else {
			userHo = 0;
		}
		resetToken(result);
		ShopToken shToken = currentUser.getShopRoot();
		if (shToken != null) {
			shopId = shToken.getShopId();
		} else {
			shopId = 0L;
		}
		return SUCCESS;
	}

	/**
	 * Search.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 *
	 * @modify hunglm16
	 * @since 15/11/2015
	 */
	public String search() throws Exception {
		result.put("total", 0);
		result.put("rows", new ArrayList<FocusProgram>());
		try {
			if (currentUser == null || currentUser.getShopRoot() == null) {
				return JSON;
			}
			KPaging<FocusProgram> kPaging = new KPaging<FocusProgram>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date fDate = null;
			Date tDate = null;
			FocusProgramFilter fpf = new FocusProgramFilter();
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				fpf.setFromDate(fDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				fpf.setToDate(tDate);
			}
			if ((!StringUtil.isNullOrEmpty(fromDate) && fDate == null) || (!StringUtil.isNullOrEmpty(toDate) && tDate == null)) {
				return JSON;
			}
			if (shopId != null && shopId > 0l) {
				fpf.setFlagAll(false);
				if (currentUser.getShopRoot().getShopId().equals(shopId) || getMapShopChild().get(shopId) != null) {
					fpf.setShopId(shopId);
				} else {
					return JSON;
				}
			} else {
				fpf.setFlagAll(true);
				fpf.setShopId(currentUser.getShopRoot().getShopId());
			}
			fpf.setFocusProgramCode(code);
			fpf.setFocusProgramName(name);
			if (status != null && status > ActiveType.DELETED.getValue()) {
				fpf.setStatus(ActiveType.parseValue(status));
			}
			fpf.setStrListShopId(getStrListShopId());

			fpf.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			fpf.setRollId(currentUser.getRoleToken().getRoleId());
			fpf.setShopRootId(currentUser.getShopRoot().getShopId());
			ObjectVO<FocusProgram> focusProgramVO = focusProgramMgr.getListFocusProgram(kPaging, fpf, null);
			if (focusProgramVO != null) {
				result.put("total", focusProgramVO.getkPaging().getTotalRows());
				result.put("rows", focusProgramVO.getLstObject());
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.search", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * Import excel.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	private String importFocusProgram() throws Exception {
		resetToken(result);

		if (currentUser == null || currentUser.getShopRoot() == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION));
			return JSON;
		}

		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = true;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,4);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				for (int i=0;i<lstData.size();i++) {
					if (lstData.get(i)!= null && lstData.get(i).size() > 0) {
						message = "";
						totalItem++;
						FocusProgram focusProgram = new FocusProgram();
						List<String> row = lstData.get(i);
						boolean isUpdate = false;
						FocusProgram program = null;
						//code
						if (row.size() > 0) {
							String value = row.get(0);
							focusProgram.setFocusProgramCode(value);
							String msg = ValidateUtil.validateField(value, "catalog.focus.program.code", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								program = focusProgramMgr.getFocusProgramByCode(value);
								if (program != null) {
									if (program.getStatus().getValue().equals(ActiveType.WAITING.getValue())) {
										isUpdate = true;
									} else {
										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED2, value) +"\n";
									}
								}
							}
						}
						//name
						if (row.size() > 1) {
							String value = row.get(1);
							focusProgram.setFocusProgramName(value);
							message += ValidateUtil.validateField(value, "catalog.focus.program.name", 100, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH);
						}
						//fromDate
						if (row.size() > 2) {
							String value = row.get(2);
							Date fromDate = null;
							// bo try catch vi da try catch xu li nhung ham nho ben trong
//							try {
							if (!StringUtil.isNullOrEmpty(value)) {
								fromDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true,"catalog.display.program.fromDate");
							}
//							} catch (Exception e) {
//								LogUtility.logError(e, e.getMessage());
//							}
							focusProgram.setFromDate(fromDate);
							message+= ValidateUtil.getErrorMsgForInvalidFormatDate(value,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.fromDate"),null);
						}
						//toDate
						if(row.size() > 3){
							String value = row.get(3);
							Date toDate = null;
//							try {
								if (!StringUtil.isNullOrEmpty(value)) {
									toDate = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY);
								}
//							} catch (Exception e) {
//								LogUtility.logError(e, e.getMessage());
//							}
							focusProgram.setToDate(toDate);
							message+= ValidateUtil.getErrorMsgForInvalidFormatDate(value,Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.toDate"),null);
						}
						message += ValidateUtil.getErrorMsgForInvalidToDate(focusProgram.getFromDate(), focusProgram.getToDate());
						if(isView == 0){
							if (StringUtil.isNullOrEmpty(message)){
								//CRM Update
								if(isUpdate){
									program.setFocusProgramName(focusProgram.getFocusProgramName());
									program.setUpdateUser(currentUser.getUserName());
									program.setFromDate(focusProgram.getFromDate());
									program.setToDate(focusProgram.getToDate());
									focusProgramMgr.updateFocusProgram(program,getLogInfoVO());
								}else{
									focusProgram.setCreateUser(currentUser.getUserName());
									focusProgram.setStatus(ActiveType.WAITING);
									focusProgramMgr.createFocusProgram(focusProgram,getLogInfoVO());
								}
							} else {
								lstFails.add(StringUtil.addFailBean(row, message));
							}
							typeView = false;
						}else{
							if(lstView.size()<100){
								if (StringUtil.isNullOrEmpty(message)){
									message = "OK";
								}
								message = StringUtil.convertHTMLBreakLine(message);
								lstView.add(StringUtil.addFailBean(row,message));
							}
							typeView = true;
						}
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_FOCUS_PROGRAM_FAIL);
			} catch (BusinessException e) {
//				LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.importFocusProgram: " + e.getMessage());
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.importFocusProgram()"), createLogErrorStandard(startLogDate));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Save info.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String saveInfo() throws Exception {
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if (currentUser != null && currentUser.getShopRoot() != null) {
	    	try {
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(name, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(code, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				FocusProgram focusProgram = focusProgramMgr.getFocusProgramByCode(code);
	    		if (id != null && id > 0) {
	    			if (focusProgram != null) {
	    				if (id.intValue() != focusProgram.getId().intValue()) {
	    					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, code);
		    			 } else {
		    				 /*if (isActivePermission()) {//active httm
		    					 if(ActiveType.WAITING.equals(focusProgram.getStatus())) {//chi dc sua trang thai

		    					 } else if(ActiveType.RUNNING.equals(focusProgram.getStatus())) {

	    						 }
		    				 } else {
		    					 //httm
		    					 if (ActiveType.WAITING.equals(focusProgram.getStatus())) {
		    						 if (!ActiveType.WAITING.getValue().equals(status)) {
		    							 result.put(ERROR, true);
		    							 result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED2, code));
		    							 return JSON;
		    						 }
		    					 }
		    				 }*/
		    				 if (ActiveType.WAITING.getValue().equals(status) && ActiveType.RUNNING.equals(focusProgram.getStatus())) {//Dang hoat dong khong the update thanh du thao
	    						 result.put(ERROR, true);
		    					 result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.promotion.error.running.to.waiting"));
		    					 return JSON;
	    					 }
//		    				 String errCode = focusProgramMgr.checkUpdateFocusProgramForActive(id);
//		    				 if(ActiveType.RUNNING.getValue().equals(status)  && ActiveType.WAITING.getValue().equals(focusProgram.getStatus().getValue())){
//			    				 if(StringUtil.isNullOrEmpty(errCode) == false && errCode.equalsIgnoreCase(ConstantManager.ERR_ACTIVE_PROGRAM_OK) == false){
//			    					 result.put(ERROR, true);
//			    					 result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, errCode));
//			    					 return JSON;
//			    				 }
//		    				 }
							//kiem tra update tu du thao len hoat dong neu chua day du du~ lieu ( dvtg , sp ... ) thi bao loi.
		    				 try {
		    					 if (ActiveType.RUNNING.getValue().equals(status) && ActiveType.WAITING.equals(focusProgram.getStatus())) {
		    						 if (!focusProgramMgr.checkUpdateFocusProgramForActive(id).equals(ExceptionCode.ERR_ACTIVE_PROGRAM_OK)){
										 result.put(ERROR, true);
										 result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "focus.no.shop.product"));
										 return JSON;
									 }
			    				 }
		    				 } catch (IllegalArgumentException e) {
		    					 result.put(ERROR, true);
				    			 result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ERR_ACTIVE_FOCUS_PROGRAM_ALL"));
				    			 return JSON;
		    				 }

		    				 focusProgram.setFocusProgramCode(code);
		    				 focusProgram.setFocusProgramName(name);
		    				 focusProgram.setStatus(ActiveType.parseValue(status));
		    				 if (!StringUtil.isNullOrEmpty(fromDate)) {
		    					 focusProgram.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
		    				 }
		    				 if (!StringUtil.isNullOrEmpty(toDate)) {
		    					 focusProgram.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
		    				 } else {
		    					 focusProgram.setToDate(null);
		    				 }
		    				 focusProgram.setUpdateDate(DateUtil.now());
		    				 focusProgram.setUpdateUser(currentUser.getUserName());
//		    				 saveAttributeData(focusProgram);
		    				 focusProgramMgr.updateFocusProgram(focusProgram,getLogInfoVO());
		    				 error = false;
		    				 result.put("id", id);
		    			 }
	    			}
	    		} else {
	    			if (focusProgram!= null) {
	    				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, code);
	    			} else {
	    				if (!ActiveType.WAITING.getValue().equals(status)) {
	    					result.put(ERROR, true);
	    					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED2, code));
	    					return JSON;
	    				}
	    				FocusProgram newFocusProgram = new FocusProgram();
	    				newFocusProgram.setFocusProgramCode(code);
	    				newFocusProgram.setCreateUser(currentUser.getUserName());
	    				newFocusProgram.setFocusProgramName(name);
	    				newFocusProgram.setStatus(ActiveType.parseValue(status));
	    				if (!StringUtil.isNullOrEmpty(fromDate)) {
	    					newFocusProgram.setFromDate(DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY));
	    				}
	    				if (!StringUtil.isNullOrEmpty(toDate)) {
	    					newFocusProgram.setToDate(DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY));
	    				}
//	    				saveAttributeData(newFocusProgram);
	    				newFocusProgram = focusProgramMgr.createFocusProgram(newFocusProgram,getLogInfoVO());
	    				if (newFocusProgram!= null) {
	    					error = false;
	    					result.put("id", newFocusProgram.getId());
	    				}
	    			}
	    		}
	    	} catch (BusinessException e) {
//	    		LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.saveInfo: " + e.getMessage());
	    		Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.saveInfo()"), createLogErrorStandard(startLogDate));

			}
	    }
	    result.put(ERROR, error);
	    if (error && StringUtil.isNullOrEmpty(errMsg)) {
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Search shop.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String searchShop() throws Exception {
	    result.put("page", page);
	    result.put("max", max);
	    try{
//	    	Shop ownerShop = (Shop) request.getSession().getAttribute(ConstantManager.SESSION_SHOP);
//			KPaging<FocusShopMap> kPaging = new KPaging<FocusShopMap>();
//			kPaging.setPageSize(max);
//			kPaging.setPage(page-1);
//			ObjectVO<FocusShopMap> focusShopMapVO = focusProgramMgr.getListFocusShopMap(kPaging, id,code,name,ActiveType.parseValue(status), ownerShop == null ? null : ownerShop.getId())	;
//			if(focusShopMapVO!= null){
//			    result.put("total", focusShopMapVO.getkPaging().getTotalRows());
//			    result.put("rows", focusShopMapVO.getLstObject());
//			}
	    	if (currentUser == null || currentUser.getShopRoot() == null) {
    			result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "catalog.focus.program.update.htbh.perimission"));
				return ERROR;
    		}

	    	List<JsPromotionShopTreeNode> lstStaffTypeTree = new ArrayList<JsPromotionShopTreeNode>();
			JsPromotionShopTreeNode bean = new JsPromotionShopTreeNode();
	    	NewTreeVO<Shop,ProgrameExtentVO> shopTree = shopMgr.getShopTreeInFocusProgram(id, shopCode, shopName, currentUser.getShopRoot().getShopId());
	    	if (shopTree.getObject() == null && shopTree.getListChildren() != null) {
				for (int i = 0; i < shopTree.getListChildren().size(); i++) {
					JsPromotionShopTreeNode tmp = new JsPromotionShopTreeNode();
					tmp = getJsTreeNode(tmp, shopTree.getListChildren().get(i));
					 //tmp.setState(ConstantManager.JSTREE_STATE_OPEN);
					lstStaffTypeTree.add(tmp);
				}
			} else {
				bean = getJsTreeNode(bean, shopTree);
				bean.setState(ConstantManager.JSTREE_STATE_OPEN);
				lstStaffTypeTree.add(bean);
			}
	    	result.put("rows", lstStaffTypeTree);
	    }catch (BusinessException e) {
//	    	LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.searchShop: " + e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.searchShop()"), createLogErrorStandard(startLogDate));

	    }
	    return JSON;
	}

	/**
	 * Save shop.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String saveShop() throws Exception {
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null && lstShopId!= null && lstShopId.size() > 0 && id!= null && id > 0){
	    	try{
	    		//check shop co phai VNM ko http://192.168.1.92:8000/mantis/view.php?id=3794
	    		/*for(int i = 0; i < lstShopId.size(); i++) {
	    			Shop chooseShop = shopMgr.getShopById(lstShopId.get(i));
	    			if(chooseShop == null || ActiveType.DELETED.equals(chooseShop.getStatus())) {
	    				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    				result.put(ERROR, true);
	    				result.put("errMsg", errMsg);
	    				return JSON;
	    			} else if(chooseShop.getType() != null && chooseShop.getType().getChannelTypeCode().equals(ShopChannelType.VNM.getValue())) {
	    				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.focus.program.shop.not.vnm");
	    				result.put(ERROR, true);
	    				result.put("errMsg", errMsg);
	    				return JSON;
	    			}
	    		}*/
	    		Shop sh = null;
	    		for (Long idt : lstShopId) {
	    			sh = shopMgr.getShopById(idt);
	    			if (sh == null) {
	    				result.put(ERROR, true);
	    				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "stock.issued.out.shop.null"));
	    				return JSON;
	    			}
	    			if (!checkShopInOrgAccessById(idt)) {
	    				result.put(ERROR, true);
	    				result.put("errMsg", R.getResource("common.permission2", sh.getShopCode()));
	    				return JSON;
	    			}
	    		}
	    		focusProgramMgr.createListFocusShopMapEx(id,lstShopId,ActiveType.parseValue(status),getLogInfoVO());
		    	error = false;
	    	}catch (BusinessException e) {
//	    		LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.saveShop: " + e.getMessage());
	    		Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.saveShop()"), createLogErrorStandard(startLogDate));
	    	}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Delete shop.
	 * @modifier: phut -> sua delete - > delete status
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String deleteShop() throws Exception {
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
	    	try{
	    		FocusShopMap focusShopMap = focusProgramMgr.getFocusShopMapById(shopMapId);
	    		if (focusShopMap != null) {
	    			if (!checkShopInOrgAccessById(focusShopMap.getShop().getId())) {
	    				result.put(ERROR, true);
	    				result.put("errMsg", R.getResource("common.permission2", focusShopMap.getShop().getShopCode()));
	    				return JSON;
	    			}
	    			focusShopMap.setStatus(ActiveType.DELETED);
	    			focusProgramMgr.updateFocusShopMap(focusShopMap, getLogInfoVO());
	    			error = false;
	    		}
	    	}catch (Exception e) {
	    		LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.deleteShop: " + e.getMessage());
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Search product.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String searchProduct() throws Exception {
	    result.put("page", page);
	    result.put("max", max);
		try {
			KPaging<FocusChannelMapProduct> kPaging = new KPaging<FocusChannelMapProduct>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			ObjectVO<FocusChannelMapProduct> focusChannelMapProductVO = focusProgramMgr.getListFocusChannelMapProduct(kPaging, code, name, id, null);
			if (focusChannelMapProductVO != null) {
				int size = focusChannelMapProductVO.getLstObject().size();
				for (int i = 0; i < size; i++) {
					FocusChannelMapProduct tmp = focusChannelMapProductVO.getLstObject().get(i);
					if (tmp.getFocusChannelMap() != null) {
						tmp.setCreateUser(tmp.getFocusChannelMap().getSaleTypeCode());
					}
				}
			    result.put("total", focusChannelMapProductVO.getkPaging().getTotalRows());
			    result.put("rows", focusChannelMapProductVO.getLstObject());
			}
		} catch (BusinessException e) {
//	    	LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.searchProduct: " + e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.searchProduct()"), createLogErrorStandard(startLogDate));
	    }
	    return JSON;
	}

	/**
	 * Save product.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String saveProduct() throws Exception {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		try {
			if (currentUser != null) {
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(staffTypeCode, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(focusProductType, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				Date sysdate = commonMgr.getSysDate();
				if (isUpdate != null && isUpdate) {
					FocusChannelMapProduct obj = focusProgramMgr.getFocusChannelMapProductById(focusMapProductId);
					if (obj != null) {
						ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, id, staffTypeCode, ActiveType.RUNNING);
						if (focusChannelMapVO != null && focusChannelMapVO.getLstObject() != null && focusChannelMapVO.getLstObject().size() > 0) {
							obj.setFocusChannelMap(focusChannelMapVO.getLstObject().get(0));
						}
						ApParam focusType = apParamMgr.getApParamByCode(focusProductType, ApParamType.FOCUS_PRODUCT_TYPE);
						if (focusType != null) {
							obj.setType(focusProductType);
						}
						obj.setUpdateUser(currentUser.getUserName());
						obj.setUpdateDate(sysdate);
						focusProgramMgr.updateFocusChannelMapProduct(obj, getLogInfoVO());
						error = false;
					}
				} else {
					if (lstProductId != null && lstProductId.size() > 0 && id != null && id > 0) {
						FocusChannelMap focusChannelMap = focusProgramMgr.getFocusChannelMapById(channelTypeId);
						String apParamCode = null;
						if (focusChannelMap != null) {
							apParamCode = focusChannelMap.getSaleTypeCode();
						}
						ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, id, apParamCode, ActiveType.RUNNING);
						if (focusChannelMapVO == null || focusChannelMapVO.getLstObject() == null || focusChannelMapVO.getLstObject().size() == 0) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_CORRECT, null, "catalog.sale.type");
						} else if (type != null && type == -2) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.sale.focus.program.type");
						} else {
							ApParam focusType = apParamMgr.getApParamById(Long.valueOf(type.toString()));
							if (focusType != null) {
								focusProgramMgr.createListFocusChannelMapProduct(lstProductId, id, apParamCode, getLogInfoVO(), focusType.getApParamCode());
								error = false;
							}
						}
					}
				}
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.saveProduct: " + e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.saveProduct()"), createLogErrorStandard(startLogDate));
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Delete product.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 02/08/2012
	 */
	public String deleteProduct() throws Exception {
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
		if (currentUser != null) {
			try {
	    		FocusChannelMapProduct focusChannelMapProduct = focusProgramMgr.getFocusChannelMapProductById(productId);
	    		if (focusChannelMapProduct != null) {
	    			focusProgramMgr.deleteFocusChannelMapProduct(focusChannelMapProduct, getLogInfoVO());
	    			error = false;
	    		}
	    	}catch (Exception e) {
	    		LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.deleteProduct: " + e.getMessage());
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Import shop.
	 *
	 * @return the string
	 * @author hungtx,tientv11
	 * @since Aug 25, 2012
	 */
	public String importShop() throws Exception {
		resetToken(result);
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = false;
	    List<Long> lstShop = this.getListShopChildId();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,2);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				for(int i=0;i<lstData.size();i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
						FocusShopMap focusShopMap = new FocusShopMap();
						FocusProgram focusProgram = null;
						List<String> row = lstData.get(i);

						//code
//						if(row.size() > 0){
//							String value = row.get(0);
//							if(!StringUtil.isNullOrEmpty(value)){
//								focusProgram = focusProgramMgr.getFocusProgramById(excelFCId);//focusProgramMgr.getFocusProgramByCode(excelFCId);
//								if(focusProgram == null){
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true, "catalog.focus.program.code");
//								}else if(!ActiveType.WAITING.getValue().equals(focusProgram.getStatus().getValue())){
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_IN_WAITING,true,"catalog.focus.program.code");
//								}
//							} else {
//								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,true,"catalog.focus.program.code");
//							}
//							focusShopMap.setFocusProgram(focusProgram);
//						}
						focusProgram = focusProgramMgr.getFocusProgramById(id);
						focusShopMap.setFocusProgram(focusProgram);
						/*String parentShopCode = null;
						if(session!= null && session.getAttribute(ConstantManager.SESSION_SHOP)!= null){
					    	parentShopCode = ((Shop)session.getAttribute(ConstantManager.SESSION_SHOP)).getShopCode();
					    }*/
						//shop code
						Shop shop = null;
						if(row.size() > 0){
							String value = row.get(0);
							if(!StringUtil.isNullOrEmpty(value)){
								shop = shopMgr.getShopByCode(value);
								if(shop == null){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB,true,"catalog.focus.program.shop.code");
								} /*else if(focusProgram != null && focusProgramMgr.checkIfAncestorInFp(shop.getId(), focusProgram.getId())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.focus.program.shop.not.in.ex");
									message += endline(isView);
								}
								//Neu ton tai thi cap nhat.
								/*else if(focusProgram!= null && focusProgramMgr.checkIfFocusShopMapExist(focusProgram.getId(), value, null)){
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.shop.exist", value);
									message += "\n";

								}*/else if(ActiveType.STOPPED.getValue().equals(shop.getStatus().getValue())){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE,true,"catalog.focus.program.shop.code");
								} else if (!lstShop.contains(shop.getId())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.shop.not.belong.area");
								}
//								} else if (!checkShopInOrgAccessById(shop.getId())) {
//									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.undefined.imp");
//								}
								/*else if(!StringUtil.isNullOrEmpty(parentShopCode) && !shopMgr.checkAncestor(parentShopCode, shop.getShopCode())){
									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_SHOP,shop.getShopCode(),currentUser.getUserName()) + "\n";

								} else if(focusProgram!= null && focusProgramMgr.checkShopFamilyInFocusProgram(shop.getId(), focusProgram.getId())){
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.shop.in.focus.program",value) + "\n";

								}*/
								/*else if(focusProgram!= null && !focusProgramMgr.checkShopAncestorInFocusProgram(focusProgram.getId(),shop.getId())){
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.focus.program.shop.not.in.area") + "\n";

								} else if(shop.getType().getChannelTypeCode().equals(ShopChannelType.VNM.getValue())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.focus.program.shop.not.vnm") + "\n";
								}*/
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE,true,"catalog.focus.program.shop.code");
							}
							focusShopMap.setShop(shop);
						}
						/*
						 if(!StringUtil.isNullOrEmpty(message)){
						 	-->shop!=null, shop ok. FocusProgram!=null, focusProgram ok. Co shopId, co focusProgramId.
						 	isExits la gi hieu roi.
						 	co isExits de viet code cho toi uu.
						 	Chua co cai focusShopMap voi shopId do thi isExist=true.
						 	Chua co focusShopMap thi minh moi set data va tao moi:
						 	if(isExists){
									focusShopMap.setCreateDate(commonMgr.getSysDate());
									focusShopMap.setCreateUser(currentUser.getUserName());
									focusShopMap.setStatus(ActiveType.RUNNING);
									focusProgramMgr.createFocusShopMap(focusShopMap, getLogInfoVO(),false);
							}
						 	Neu co cai focusShopMap do roi thi khoi fai lam gi nua.
						 }
						 */
						boolean isExists = false;
						if(shop!=null && focusProgram!=null && focusProgramMgr.checkIfFocusShopMapExist(focusProgram.getId(), shop.getShopCode(), null)){
							isExists = true;
						}
						if(isExists && focusProgram.getId()!=null && !StringUtil.isNullOrEmpty(shop.getShopCode())){
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.shop.is.exists") + "\n";
						}
						if(StringUtil.isNullOrEmpty(message)){
							boolean flag1 = focusProgramMgr.isExistChildJoinProgram(shop.getShopCode(),id);
							if(flag1){
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.focus.program.shop.con.is.exists") + "\n";
							}
						}
						if (StringUtil.isNullOrEmpty(message)){
								focusShopMap.setCreateDate(commonMgr.getSysDate());
								focusShopMap.setCreateUser(currentUser.getUserName());
								focusShopMap.setStatus(ActiveType.RUNNING);
								focusProgramMgr.createFocusShopMap(focusShopMap, getLogInfoVO());
								importProgram = "shop";
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_FOCUS_PROGRAM_SHOP_FAIL);
			} catch (BusinessException e) {
//				LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.importShop: " + e.getMessage());
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.importShop()"), createLogErrorStandard(startLogDate));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Import staff.
	 *
	 * @return the string
	 * @author hungtx
	 * @since Aug 25, 2012
	 *
	 * @modify hunglm16
	 * @since 03/11/2015
	 * @description ra soat va toi uu lai ham ke thua
	 */
	private String importStaff() throws Exception {
		int maxSizeColumn = 3;//So cot se doc tu file import excel
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
		typeView = true;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, maxSizeColumn);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				for (int i = 0, sizei = lstData.size(); i < sizei; i++) {
					//Xu ly bo qua cac dong du lieu rong
					if (lstData.get(i) == null || lstData.get(i).isEmpty()) {
						continue;
					}
					boolean flagContinue = true;//Kiem tra cot du lieu rong
					for (int ic = 0; ic < maxSizeColumn; ic ++) {
						if (!StringUtil.isNullOrEmpty(lstData.get(i).get(ic))) {
							flagContinue = false;
							break;
						}
					}
					if (flagContinue) {
						continue;
					}
					message = "";
					totalItem++;
					FocusProgram focusProgram = focusProgramMgr.getFocusProgramById(id);
					List<String> row = lstData.get(i);
					if (focusProgram != null) {
						ApParam channelType = null;
						if (row.size() > 0) {
							String value = row.get(0);
							if (!StringUtil.isNullOrEmpty(value)) {
								channelType = apParamMgr.getApParamByCode(value, ApParamType.STAFF_SALE_TYPE);
								if (channelType == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.sale.type.code");
								} else if (!ActiveType.RUNNING.equals(channelType.getStatus())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.sale.type.code");
								} else if (focusProgram != null) {
									ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, focusProgram.getId(), value, ActiveType.RUNNING);
									if (!(focusChannelMapVO != null && focusChannelMapVO.getLstObject() != null && focusChannelMapVO.getLstObject().size() > 0)) {
										message += R.getResource("catalog.focus.program.sale.type.not.in.focusprogram") + "\n";
									}
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.sale.type.code");
							}
						}
						//Ma San Pham
						Product product = null;
						String productCode = null;
						if (row.size() > 1) {
							productCode = row.get(1);
							if (!StringUtil.isNullOrEmpty(productCode)) {
								product = productMgr.getProductByCode(productCode);
								if (product == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.product.code");
								} else if (ActiveType.STOPPED.getValue().equals(product.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.product.code");
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.product.code");
							}
						}
						ApParam apParam = null;
						if (row.size() > 2) {
							String value = row.get(2).trim();
							String msg = ValidateUtil.validateField(value, "catalog.focus.program.product.type", 20, ConstantManager.ERR_REQUIRE);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								apParam = apParamMgr.getApParamByCode(value, ApParamType.FOCUS_PRODUCT_TYPE);
								if (apParam == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.product.type");
								} else {
									if (!ActiveType.RUNNING.equals(apParam.getStatus())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.product.type");
									}
								}
							}
						}
						if (isView == 0) {
							if (StringUtil.isNullOrEmpty(message)) {
								List<Long> lstProduct = new ArrayList<Long>();
								lstProduct.add(product.getId());
								focusProgramMgr.createListFocusChannelMapProduct(lstProduct, focusProgram.getId(), channelType.getApParamCode(), getLogInfoVO(), apParam.getApParamCode());
							} else {
								lstFails.add(StringUtil.addFailBean(row, message));
							}
							typeView = false;
						} else {
							if (lstView.size() < 100) {
								if (StringUtil.isNullOrEmpty(message)) {
									message = "OK";
								}
								message = StringUtil.convertHTMLBreakLine(message);
								lstView.add(StringUtil.addFailBean(row, message));
							}
							typeView = true;
						}
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_FOCUS_PROGRAM_STAFF_FAIL);
			} catch (BusinessException e) {
				LogUtility.logErrorStandard(e, "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.importStaff", createLogErrorStandard(actionStartTime));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Import excel.
	 *
	 * @return the string
	 * @author hungtx
	 * @since Aug 25, 2012
	 */
	public String importExcel() throws Exception {
		if (excelType == 1) {
			return importFocusProgram();
		} else if (excelType == 2) {
			return importShop();
		}
		return importStaff();
	}

	public String getListExcelData() throws Exception {
		if(excelType == 1){
			return exportFocusProgram();
		} else if(excelType == 2){
			return exportFocusShopMap();
		}
		return exportStaff();
	}

	public String exportFocusProgram() throws Exception {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

	    	Date fDate = null;
			Date tDate = null;
			FocusProgramFilter fpf = new FocusProgramFilter();
			if(!StringUtil.isNullOrEmpty(fromDate)){
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				fpf.setFromDate(fDate);
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				fpf.setToDate(tDate);
			}

			fpf.setFocusProgramCode(code);
			fpf.setFocusProgramName(name);
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shopTm = shopMgr.getShopByCode(shopCode);
				if (shopTm != null) {
					fpf.setShopId(shopTm.getId());
				}
			}
			fpf.setStatus(ActiveType.parseValue(status));
			StaffRole role = null;
			if (checkRoles(VSARole.VNM_SALESONLINE_HO)) {
				role = StaffRole.VNM_SALESONLINE_HO;
			} else if (checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) {
				role = StaffRole.VNM_SALESONLINE_DISTRIBUTOR;
			}

			fpf.setStaffRootId(currentUser.getStaffRoot().getStaffId());
			fpf.setRollId(currentUser.getRoleToken().getRoleId());
			fpf.setShopRootId(currentUser.getShopRoot().getShopId());

			ObjectVO<FocusProgram> focusProgramVO = focusProgramMgr.getListFocusProgram(null, fpf, role);
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if(focusProgramVO != null && focusProgramVO.getLstObject() != null){
				int size = focusProgramVO.getLstObject().size();
				for(int i=0;i<size;i++){
					FocusProgram temp = focusProgramVO.getLstObject().get(i);
					CellBean cellBean = new CellBean();
					cellBean.setContent1(temp.getFocusProgramCode());
					cellBean.setContent2(temp.getFocusProgramName());
					if(temp.getFromDate() != null){
						cellBean.setContent3(DateUtil.toDateString(temp.getFromDate(), ConstantManager.FULL_DATE_FORMAT));
					}
					if(temp.getToDate() != null){
						cellBean.setContent4(DateUtil.toDateString(temp.getToDate(), ConstantManager.FULL_DATE_FORMAT));
					}
					if(temp.getStatus() != null){
						if(ActiveType.RUNNING.equals(temp.getStatus())){
							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.name"));
						}else if(ActiveType.STOPPED.equals(temp.getStatus())){
							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"pause.status.name"));
						}else if(ActiveType.WAITING.equals(temp.getStatus())){
							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.waiting"));
						}
					}
					lstFails.add(cellBean);
				}
			}
			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_FOCUS_EXPORT);
//			String filePath = exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_FOCUS_EXPORT);
//			if (!StringUtil.isNullOrEmpty(filePath)) {
//				MemcachedUtils.putValueToMemcached(reportToken, filePath, retrieveReportMemcachedTimeout());
//			}
	    }catch (BusinessException e) {
	    	result.put(ERROR, true);
//	    	LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.exportFocusProgram: " + e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.exportFocusProgram()"), createLogErrorStandard(startLogDate));
	    }
	    return JSON;
	}

	public String exportShop() throws Exception {
		try{
			Date fDate = null;
			Date tDate = null;
			if(!StringUtil.isNullOrEmpty(fromDate)){
				fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			}
			ObjectVO<FocusShopMap> focusShopMapVO = focusProgramMgr.getListFocusShopMapV2(null, code, name, shopCode, shopName, fDate, tDate,ActiveType.parseValue(status));

			List<CellBean> lstFails = new ArrayList<CellBean>();
			if(focusShopMapVO != null && focusShopMapVO.getLstObject() != null){
				int size = focusShopMapVO.getLstObject().size();
				for(int i=0;i<size;i++){
					FocusShopMap temp = focusShopMapVO.getLstObject().get(i);
					CellBean cellBean = new CellBean();
					if(temp.getFocusProgram() != null){
						cellBean.setContent1(temp.getFocusProgram().getFocusProgramCode());
					}
					if(temp.getShop() != null){
						cellBean.setContent2(temp.getShop().getShopCode());
						cellBean.setContent3(temp.getShop().getShopName());
					}
					if(temp.getStatus() != null){
						if(ActiveType.RUNNING.equals(temp.getStatus())){
							cellBean.setContent4(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.name"));
						}else if(ActiveType.STOPPED.equals(temp.getStatus())){
							cellBean.setContent4(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"pause.status.name"));
						}
					}
					lstFails.add(cellBean);
				}
			}
			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_FOCUS_SHOP_EXPORT);
	    }catch (BusinessException e) {
	    	result.put(ERROR, true);
//	    	LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.exportShop: " + e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.exportShop()"), createLogErrorStandard(startLogDate));
	    }
	    return JSON;
	}

	public String exportStaff() throws Exception {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			List<CellBean> lstFails = new ArrayList<CellBean>();
			ObjectVO<FocusChannelMapProduct> lstFCMP = focusProgramMgr.getListFocusChannelMapProductEx(null, code, name, id, null , null);
			for(FocusChannelMapProduct fcmp : lstFCMP.getLstObject()) {
				CellBean cellBean = new CellBean();
				if(fcmp.getFocusChannelMap() != null){
					cellBean.setContent1(fcmp.getFocusChannelMap().getSaleTypeCode());
				}
				cellBean.setContent2(fcmp.getProduct() != null ? fcmp.getProduct().getProductCode() : null);
				cellBean.setContent3(fcmp.getType());
				lstFails.add(cellBean);
			}

			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_FOCUS_STAFF_EXPORT);
//			String filePath = exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_FOCUS_STAFF_EXPORT);
//			if (!StringUtil.isNullOrEmpty(filePath)) {
//				MemcachedUtils.putValueToMemcached(reportToken, filePath, retrieveReportMemcachedTimeout());
//			}
	    }catch (BusinessException e) {
	    	result.put(ERROR, true);
//	    	LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.exportStaff: " + e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.exportStaff()"), createLogErrorStandard(startLogDate));
	    }
	    return JSON;
	}

	public String searchInfoChange() throws Exception {
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<ActionAudit> kPaging = new KPaging<ActionAudit>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			Date fDate = null;
			Date tDate = null;
			if(!StringUtil.isNullOrEmpty(fromDate)){
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(fromDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.fromDate"), null);
				if(StringUtil.isNullOrEmpty(checkMessage)){
					fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else{
					return JSON;
				}
			}
			if(!StringUtil.isNullOrEmpty(toDate)){
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(toDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.display.program.toDate"), null);
				if(StringUtil.isNullOrEmpty(checkMessage)){
					tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else{
					return JSON;
				}
			}

			if(!DateUtil.compareDate(fDate, tDate)){
				return JSON;
			}

			if(id!= null){
				ObjectVO<ActionAudit> lstActionAudit = logMgr.getListActionAudit(kPaging,null,id,ActionAuditType.FOCUS_PROGRAM,fDate,tDate);
				if(lstActionAudit!= null){
				   	result.put("total", lstActionAudit.getkPaging().getTotalRows());
				   	result.put("rows", lstActionAudit.getLstObject());
			    }
			}

	    }catch (BusinessException e) {
//	    	LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.searchInfoChange: " + e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.searchInfoChange()"), createLogErrorStandard(startLogDate));
	    }
	    return JSON;
	}


	public String searchInfoChangeDetail() throws Exception {
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<ActionAuditDetail> kPaging = new KPaging<ActionAuditDetail>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			if(id!=null && id!=0){
				ObjectVO<ActionAuditDetail> lstActionAuditDetail = logMgr.getListActionAuditDetail(kPaging,id);
			    if(lstActionAuditDetail!= null){
				   	result.put("total", lstActionAuditDetail.getkPaging().getTotalRows());
				   	result.put("rows", lstActionAuditDetail.getLstObject());
			    }
			}
	    }catch (BusinessException e) {
//	    	LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.searchInfoChangeDetail: " + e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.searchInfoChangeDetail()"), createLogErrorStandard(startLogDate));
	    }
	    return JSON;
	}

	public String saveFocusChanelMap(){
		try {

		} catch (Exception e) {
		}
		return JSON;
	}

	/**
	 * Copy focus program.
	 *
	 * @return the string
	 * @author hungtt
	 * @since May 27, 2013
	 */
	public String copyFocusProgram() throws Exception {
		try {
			FocusProgram fp = focusProgramMgr.getFocusProgramByCode(code);
			if (fp != null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_FOCUS_PROGRAM_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.focus.program.exists")));
				return JSON;
			}
			FocusProgram pp = focusProgramMgr.copyFocusProgram(id, getLogInfoVO(), code, name);
			if (pp != null) {
				result.put("focusId", pp.getId());
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.copyFocusProgram: " + e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.copyFocusProgram()"), createLogErrorStandard(startLogDate));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);

		}
		return JSON;
	}

	/**
	 * Gets the js tree node.
	 *
	 * @param node the node
	 * @param channelType the channel type
	 * @return the js tree node
	 * @author hungtt
	 * @since Jun 3, 2013
	 */
	private JsPromotionShopTreeNode getJsTreeNode(JsPromotionShopTreeNode node,
			NewTreeVO<Shop, ProgrameExtentVO> channelType) throws Exception {
		if (channelType != null && channelType.getObject() != null) {
			node.setData(channelType.getObject().getShopCode()+"-"+channelType.getObject().getShopName());
			node.setId(channelType.getObject().getId());
			node.setIsShop(channelType.getDetail().getIsNPP());
			List<JsPromotionShopTreeNode> lstChild = new ArrayList<JsPromotionShopTreeNode>();
			if (channelType.getListChildren() != null
					&& channelType.getListChildren().size() > 0) {
				for (int i = 0; i < channelType.getListChildren().size(); i++) {
					JsPromotionShopTreeNode subBean = new JsPromotionShopTreeNode();
					subBean = getJsTreeNode(subBean, channelType
							.getListChildren().get(i));
					lstChild.add(subBean);
				}
				node.setChildren(lstChild);
				//node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			}
		}
		return node;
	}

	/**
	 * Creates the focus program.
	 *
	 * @return the string
	 * @author hungtt
	 * @since Jun 3, 2013
	 */
	public String createFocusProgram() throws Exception {
		try {
			List<FocusShopMap> listFSM = new ArrayList<FocusShopMap>();
			if (listShopId != null) {
				for (int i = 0; i < listShopId.size(); i++) {
					FocusShopMap fsm = new FocusShopMap();
					Shop s = shopMgr.getShopById(listShopId.get(i));
					FocusProgram pp = focusProgramMgr.getFocusProgramById(id);
					if(pp != null && pp.getStatus().getValue() != ActiveType.WAITING.getValue()){
						result.put(ERROR, true);
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "focus.program.incorrect"));
						return JSON;
					}
					if(s == null){
						result.put(ERROR, true);
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.not.exist"));
						return JSON;
					}
					if(s.getStatus().getValue() != ActiveType.RUNNING.getValue()){
						result.put(ERROR, true);
						result.put("errMsg",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.incorrect"));
						return JSON;
					}
					fsm.setShop(s);
					fsm.setFocusProgram(pp);
					fsm.setCreateDate(DateUtil.now());
					if(currentUser != null)
					{
						fsm.setCreateUser(currentUser.getUserName());
					}
					if (fsm != null) {
						listFSM.add(fsm);
					}

				}
				focusProgramMgr.createListFocusShopMap(listFSM,getLogInfoVO());
			}
		}
		catch(BusinessException e)
		{
//			LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.createFocusProgram: " + e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.createFocusProgram()"), createLogErrorStandard(startLogDate));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	public String deleteFocusShopMap() throws Exception {
		try {
			if (currentUser == null || currentUser.getShopRoot() == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_PERMISSION, ""));
				return JSON;
			}
			Shop s = shopMgr.getShopById(shopId);
			if (s != null) {
    			if (!checkShopInOrgAccessById(shopId)) {
    				result.put(ERROR, true);
    				result.put("errMsg", R.getResource("common.permission2", s.getShopCode()));
    				return JSON;
    			}
				focusProgramMgr.deleteFocusShopMap(shopId, id, currentUser.getShopRoot().getShopId(), getLogInfoVO());
				shopCode = (s != null) ? s.getShopCode() : "";
				result.put("shopCode", shopCode);
			} else {
				result.put(ERROR, true);
				result.put("promotionNull", true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PROMOTION_NOT_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"promotion.shop.not.exist")));
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.deleteFocusShopMap: " + e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.deleteFocusShopMap()"), createLogErrorStandard(startLogDate));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);
		}
		return JSON;
	}

	/**
	 * Export focus shop map.
	 *
	 * @return the string
	 * @author hungtt
	 * @since Jun 4, 2013
	 */
	public String exportFocusShopMap() throws Exception {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}

			ObjectVO<FocusShopMap> objectFSM = focusProgramMgr.getListFocusShopMap(null, id, shopCode, shopName, ActiveType.RUNNING, currentUser.getShopRoot().getShopId());
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if (objectFSM != null && objectFSM.getLstObject() != null) {
				int size = objectFSM.getLstObject().size();
				for (int i = 0; i < size; i++) {
					FocusShopMap temp = objectFSM.getLstObject().get(i);
					CellBean cellBean = new CellBean();
					if (temp.getShop() != null) {
						cellBean.setContent1(temp.getShop().getShopCode());
					}

					lstFails.add(cellBean);
				}
			}

			exportExcelDataNotSession(lstFails, ConstantManager.TEMPLATE_CATALOG_FOCUS_SHOP_MAP_EXPORT);
//			String filePath = exportExcelDataNotSession(lstFails, ConstantManager.TEMPLATE_CATALOG_FOCUS_SHOP_MAP_EXPORT);
//			if (!StringUtil.isNullOrEmpty(filePath)) {
//				MemcachedUtils.putValueToMemcached(reportToken, filePath, retrieveReportMemcachedTimeout());
//			}
		} catch (BusinessException e) {
			result.put(ERROR, true);
//			LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.exportFocusShopMap: " + e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.exportFocusShopMap()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	/**
	 * Tra ve man hinh chi tiet cua CTTT(Bao gom them moi va cap nhat)
	 * @return getChangedForm
	 * @author thongnm
	 *
	 * @modify hunglm16
	 * @since 15/11/2015
	 * @description Bo sung kiem tra quyen
	 */
	public String getChangedForm() throws Exception {
		resetToken(result);
		try{
			if (id != null && id > 0) {
				FocusProgramFilter fpf = new FocusProgramFilter();
				fpf.setId(id);
				fpf.setShopId(currentUser.getShopRoot().getShopId());
				fpf.setStaffRootId(currentUser.getStaffRoot().getStaffId());
				fpf.setRollId(currentUser.getRoleToken().getRoleId());
				fpf.setShopRootId(currentUser.getShopRoot().getShopId());
				if (focusProgramMgr.countFocusProgram(fpf) == 0) {
					return PAGE_NOT_PERMISSION;
				}
			}
			if(checkRoles(VSARole.VNM_SALESONLINE_HO)) roleType = 1;
			else if(checkRoles(VSARole.VNM_SALESONLINE_GS)) roleType = 2;
			else if(checkRoles(VSARole.VNM_SALESONLINE_DISTRIBUTOR)) roleType =3;
			else roleType = 0;

			if (id != null && id > 0) {
				focusProgram = focusProgramMgr.getFocusProgramById(id);
				if (focusProgram != null) {
					code = focusProgram.getFocusProgramCode();
					name = focusProgram.getFocusProgramName();
					if (focusProgram.getFromDate() != null) {
						fromDate = DateUtil.toDateString(focusProgram.getFromDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					if (focusProgram.getToDate() != null) {
						toDate = DateUtil.toDateString(focusProgram.getToDate(), DateUtil.DATE_FORMAT_DDMMYYYY);
					}
					status = focusProgram.getStatus().getValue();
				}
			} else {
				status = ActiveType.RUNNING.getValue();
//				fromDate = DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_STR);
//				toDate = DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_STR);
			}
			ObjectVO<FocusChannelMap> focusChannelMapVO = focusProgramMgr.getListFocusChannelMap(null, id, null, ActiveType.RUNNING);
			if (focusChannelMapVO != null) {
				lstFocusChannelMap = focusChannelMapVO.getLstObject();
			}
			lstType = apParamMgr.getListApParam(ApParamType.FOCUS_PRODUCT_TYPE, ActiveType.RUNNING);

			permissionEdit = false;
			if ((isCommercialSupportPermission() && (id == null || (id!= null && ActiveType.WAITING.getValue() == status)))
					|| (isActivePermission() && id!= null && ActiveType.STOPPED.getValue() != status)) {
				permissionEdit = true;
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, "BaryCentricProgrammeCatalogAction.getChangedForm: " + e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.getChangedForm()"), createLogErrorStandard(startLogDate));
		}
		return SUCCESS;
	}

	/*****************************	THONGNM ****************************************/

	private Integer roleType ;
	private FocusProgram focusProgram;
	private Long focusProgramId ;
	private List<Long> lstApParamId;

	/**
	 * Tim kiem HTBH cua CTTT
	 * @return searchHTBH
	 * @author tungmt
	 *
	 * Ra soat chuc nang
	 * @modify hunglm16
	 * @sice 02/11/2015
	 */
	public String searchHTBH() throws Exception {
		try {
			result.put("page", page);
			result.put("rows", rows);
			result.put("total", 0);
			result.put("rows", new ArrayList<HTBHVO>());
			if (page == 0) {
				page = 1;
			}
			if (rows == 0) {
				rows = 10;
			}
			HTBHFilter filter = new HTBHFilter();
			filter.setCode(code);
			filter.setName(name);
			filter.setFocusProgramId(focusProgramId);
			filter.setStatus(ActiveType.RUNNING);

			if (permissionEdit) {
				ObjectVO<HTBHVO> vo = focusProgramMgr.getListHTBHVO(filter, null);
				if (vo != null) {
					result.put("total", vo.getLstObject().size());
					result.put("rows", vo.getLstObject());
				}
			} else {
				if (focusProgramId != null && focusProgramId > 0) {
					KPaging<HTBHVO> kPaging = new KPaging<>();
					kPaging.setPageSize(max);
					kPaging.setPage(page - 1);
					ObjectVO<HTBHVO> vo = focusProgramMgr.getListHTBHVO(filter, kPaging);
					if (vo != null) {
						result.put("total", vo.getkPaging().getTotalRows());
						result.put("rows", vo.getLstObject());
					}
				}
			}
		} catch (BusinessException e) {
			LogUtility.logErrorStandard(e, "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.searchHTBH", createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}
	/**
	 * Luu HTBH cua CTTT
	 * @return searchHTBH
	 * @author thongnm
	 */
	public String saveHTBH() throws Exception {
	    resetToken(result);
    	try{
			//CHECK QUEN NHO DOI LAI LA HO VA TON TAI DON VI CUA USER DANG NHAP
			/*boolean isPermission = checkRoles(VSARole.VNM_SALESONLINE_HO);
			if (isPermission == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "catalog.focus.program.update.htbh.perimission"));
				return ERROR;
			}*/
			/*boolean isExistShop = checkExistShopOfUserLogin();
			if (isExistShop == false) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_BELONG_ANY_SHOP));
				return ERROR;
			}*/
    		if (currentUser == null || currentUser.getShopRoot() == null) {
    			result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "catalog.focus.program.update.htbh.perimission"));
				return ERROR;
    		}
			FocusProgram focusProgram = focusProgramMgr.getFocusProgramById(focusProgramId);
			if (focusProgram == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_DATA_CORRECT));
				return ERROR;
			}

			List<FocusChannelMap> lstFocusChannelMapOlds = focusProgramMgr.getListFocusChannelMap(null, focusProgramId, null, ActiveType.RUNNING).getLstObject() ;
			List<String> lstApParamCode = new ArrayList<String>();
			List<FocusChannelMap> lstFocusChannelMapsUpdate = new ArrayList<FocusChannelMap>();
			List<FocusChannelMap> lstFocusChannelMapsCreate = new ArrayList<FocusChannelMap>();

			ApParam apParam = null;
			if (lstApParamId != null && lstApParamId.size() > 0) {
				for (int i =0, n = lstApParamId.size(); i< n ; ++i) {
					apParam = apParamMgr.getApParamById(lstApParamId.get(i));
					lstApParamCode.add(apParam.getApParamCode());
				}
			}

			Date sysdate = commonMgr.getSysDate();

			if (lstFocusChannelMapOlds != null && lstFocusChannelMapOlds.size() > 0) {
				for (FocusChannelMap focusChannelMap : lstFocusChannelMapOlds) {
					String saleTypeCode = focusChannelMap.getSaleTypeCode();
					if (lstApParamCode.contains(focusChannelMap.getSaleTypeCode())) {
						focusChannelMap.setUpdateDate(sysdate);
						focusChannelMap.setUpdateUser(currentUser.getUserName());
						lstFocusChannelMapsUpdate.add(focusChannelMap);
						lstApParamCode.remove(saleTypeCode);
					} else {
						focusChannelMap.setStatus(ActiveType.DELETED);
						focusChannelMap.setUpdateDate(sysdate);
						focusChannelMap.setUpdateUser(currentUser.getUserName());
						lstFocusChannelMapsUpdate.add(focusChannelMap);
					}
				}
			}
			FocusChannelMap focusChannelMap = null;
			for (String apParamCode : lstApParamCode) {
				apParam = apParamMgr.getApParamByCodeEx(apParamCode, ActiveType.RUNNING);
				focusChannelMap = new FocusChannelMap();
				focusChannelMap.setFocusProgram(focusProgram);
				focusChannelMap.setSaleTypeCode(apParam.getApParamCode());
				focusChannelMap.setStatus(ActiveType.RUNNING);
				focusChannelMap.setCreateDate(sysdate);
				focusChannelMap.setCreateUser(currentUser.getUserName());
				lstFocusChannelMapsCreate.add(focusChannelMap);
			}
			focusProgramMgr.createListFocusChannelMap(lstFocusChannelMapsUpdate, lstFocusChannelMapsCreate, getLogInfoVO());
    	} catch (BusinessException e) {
    		result.put(ERROR, true);
    		result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
//			LogUtility.logError(e, "Error BaryCentricProgrammeCatalogAction.saveHTBH: " + e.getMessage());
    		Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.saveHTBH()"), createLogErrorStandard(startLogDate));
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * tra ve cau truc cay don vi tim kiem
	 *
	 * @author trietptm
	 * @return dinh dang du lieu tra ve
	 * @since 25/06/2015
	 */
	public String buildSearchUnitTree() {
		TreeUtility treeUtility = new TreeUtility(context);
		searchUnitTree = treeUtility.buildUnitTree(currentUser.getShopRoot().getShopId(), true, currentUser.getStaffRoot().getStaffId(), currentUser.getRoleToken().getRoleId(), currentUser.getShopRoot().getShopId());
		return JSON;
	}

	/**
	 * Search shop dialog.
	 *
	 * @return the string
	 * @author hungtt
	 * @since May 29, 2013
	 */
	public String searchShopDialog() throws Exception {
		List<JsPromotionShopTreeNode> lstStaffTypeTree = new ArrayList<JsPromotionShopTreeNode>();
		JsPromotionShopTreeNode bean = new JsPromotionShopTreeNode();
		try{
			if (currentUser == null || currentUser.getShopRoot() == null) {
    			result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_PERMISSION, false, "catalog.focus.program.update.htbh.perimission"));
				return ERROR;
    		}

		    ChannelTypeMgr channelTypeMgr = (ChannelTypeMgr)context.getBean("channelTypeMgr");
			if (channelTypeMgr != null) {
				if (shopId == null || !checkShopInOrgAccessById(shopId)) {
					shopId = currentUser.getShopRoot().getShopId();
				}
		    	NewTreeVO<Shop, ProgrameExtentVO> shopTree = new NewTreeVO<Shop, ProgrameExtentVO>();
		    	shopTree = shopMgr.getShopTreeVOForFocusProgramEx(shopId, null, promotionProgramCode, promotionProgramName, ActiveType.RUNNING, focusId, getStrListShopId());

				if (shopTree.getObject() == null && shopTree.getListChildren() != null) {
					for (int i = 0; i < shopTree.getListChildren().size(); i++) {
						JsPromotionShopTreeNode tmp = new JsPromotionShopTreeNode();
						tmp = getJsTreeNodeFocus(tmp, shopTree.getListChildren().get(i), promotionProgramCode, promotionProgramName);
						tmp.setState(ConstantManager.JSTREE_STATE_CLOSE);
						lstStaffTypeTree.add(tmp);
					}
				} else {
					bean = getJsTreeNodeFocus(bean, shopTree, promotionProgramCode, promotionProgramName);
					bean.setState(ConstantManager.JSTREE_STATE_OPEN);
					lstStaffTypeTree.add(bean);
				}
				int length = 0;
				if (lstStaffTypeTree != null && lstStaffTypeTree.size() > 0) {
					try {
						length = (lstStaffTypeTree.get(0).getChildren() != null) ? lstStaffTypeTree.get(0).getChildren().size() : 0;
					} catch (NullPointerException npe) {
						length = 0;
					}
				}
				result.put("solg", length);
				result.put("rows", lstStaffTypeTree);
		    }
		} catch (BusinessException e) {
//		    LogUtility.logError(e, "Error BaryCentricProgrammeCatalogAction.searchShopDialog: " + e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.BaryCentricProgrammeCatalogAction.searchShopDialog()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * Gets the js tree node focus.
	 *
	 * @param node the node
	 * @param channelType the channel type
	 * @param promotionCode the promotion code
	 * @param promotionName the promotion name
	 * @return the js tree node focus
	 * @author hungtt
	 * @since May 29, 2013
	 */
	private JsPromotionShopTreeNode getJsTreeNodeFocus(JsPromotionShopTreeNode node, NewTreeVO<Shop,ProgrameExtentVO> channelType, String promotionCode, String promotionName){
		if (channelType != null && channelType.getObject() != null) {
	    	node.setData(channelType.getObject().getShopCode() + " - " + channelType.getObject().getShopName());
	    	node.setId(channelType.getObject().getId());
			if (channelType.getDetail() != null) {
	    		node.setIsJoin(channelType.getDetail().getIsJoin());
	    		node.setIsShop(channelType.getDetail().getIsNPP());
	    	}
	    	node.setParentId((channelType.getObject().getParentShop() != null )?channelType.getObject().getParentShop().getId():null);
			try {
				node.setCountNode(channelType.getListChildren().size());
			} catch (NullPointerException npe) {
				node.setCountNode(0);
			}
			if (!StringUtil.isNullOrEmpty(promotionCode) || !StringUtil.isNullOrEmpty(promotionName)) {
				if (channelType.getListChildren() != null && channelType.getListChildren().size() > 0) {
					node.setState(ConstantManager.JSTREE_STATE_OPEN);
				}
			} else {
				if (channelType.getListChildren() != null && channelType.getListChildren().size() > 0) {
					node.setState(ConstantManager.JSTREE_STATE_CLOSE);
				}
			}
		    List<JsPromotionShopTreeNode> lstChild = new ArrayList<JsPromotionShopTreeNode>();
			if (channelType.getListChildren() != null && channelType.getListChildren().size() > 0) {
				for (int i = 0; i < channelType.getListChildren().size(); i++) {
					JsPromotionShopTreeNode subBean = new JsPromotionShopTreeNode();
					subBean = getJsTreeNodeFocus(subBean, channelType.getListChildren().get(i), promotionCode, promotionName);
					lstChild.add(subBean);
				}
				node.setChildren(lstChild);
				if (!StringUtil.isNullOrEmpty(promotionCode) || !StringUtil.isNullOrEmpty(promotionName)) {
					if (channelType.getListChildren() != null && channelType.getListChildren().size() > 0) {
						node.setState(ConstantManager.JSTREE_STATE_OPEN);
					}
				}
				// node.setState(ConstantManager.JSTREE_STATE_CLOSE);
			}
	    }
	    return node;
	}

	/*****************************	GETTER-SETTER ****************************************/
	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	public FocusProgram getFocusProgram() {
		return focusProgram;
	}

	public void setFocusProgram(FocusProgram focusProgram) {
		this.focusProgram = focusProgram;
	}

	public Long getFocusProgramId() {
		return focusProgramId;
	}

	public void setFocusProgramId(Long focusProgramId) {
		this.focusProgramId = focusProgramId;
	}
	public List<Long> getLstApParamId() {
		return lstApParamId;
	}

	public void setLstApParamId(List<Long> lstApParamId) {
		this.lstApParamId = lstApParamId;
	}

	public String checkFileImport(List<List<String>> lstData, int j) throws Exception {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).equals(row2.get(0))
							&& row1.get(1).equals(row2.get(1))) {
							if (StringUtil.isNullOrEmpty(message)) {
								message = R.getResource("shop.product.row") + (j + 2) + ", " + (k + 2);
							} else {
								message += ", " + (k + 2);
							}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " " + R.getResource("shop.product.duplicate");
			}
		}
		return message;
	}

	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}

	public String getPromotionProgramName() {
		return promotionProgramName;
	}

	public void setPromotionProgramName(String promotionProgramName) {
		this.promotionProgramName = promotionProgramName;
	}

	public Long getFocusId() {
		return focusId;
	}

	public void setFocusId(Long focusId) {
		this.focusId = focusId;
	}

	public List<TreeNode> getSearchUnitTree() {
		return searchUnitTree;
	}

	public void setSearchUnitTree(List<TreeNode> searchUnitTree) {
		this.searchUnitTree = searchUnitTree;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
}


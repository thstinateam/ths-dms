/*
 * 
 */
package ths.dms.action.catalog;

import ths.dms.web.action.general.AbstractAction;

public class DeliveryGroupCatalogAction extends AbstractAction {	

//	/** The Constant serialVersionUID. */
//	private static final long serialVersionUID = -2564315336392532591L;
//
//	/** The id. */
//	private Long id;
//	
//	/** The code. */
//	private String code;
//	
//	/** The name. */
//	private String name;
//				
//	/** The staff code. */
//	private String staffCode;
//		
//	/** The staff name. */
//	private String staffName;
//	
//	/** The customer code. */
//	private String customerCode;
//	
//	/** The customer name. */
//	private String customerName;
//	
//	/** The shop id. */
//	private Long shopId;
//	
//	/** The shop mgr. */
//	private ShopMgr shopMgr;
//	
//	/** The group transfer mgr. */
//	private GroupTransferMgr groupTransferMgr;
//	
//	/** The staff mgr. */
//	private StaffMgr staffMgr;
//	
//	/** The customer mgr. */
//	private CustomerMgr customerMgr;
//
//	/** The shop code. */
//	private String shopCode;
//	
//	/** The shop name. */
//	private String shopName;
//	
//	/** The excel type. */
//	private int excelType;
//	
//	/** The excel file. */
//	private File excelFile;
//	
//	/** The excel file content type. */
//	private String excelFileContentType;
//	
//	/** The status. */
//	private int status;
//	
//	/** The get all. */
//	private boolean getAll;
//	
//	private String staffTypeCode;
//	
//	/**
//	 * getId
//	 * @return the id
//	 */
//	public Long getId() {
//	    return id;
//	}
//
//	/**
//	 * setId
//	 * @param id the id to set
//	 */
//	public void setId(Long id) {
//	    this.id = id;
//	}
//
//	/**
//	 * getCode
//	 * @return the code
//	 */
//	public String getCode() {
//	    return code;
//	}
//
//	/**
//	 * setCode
//	 * @param code the code to set
//	 */
//	public void setCode(String code) {
//	    this.code = code;
//	}
//
//	/**
//	 * getName
//	 * @return the name
//	 */
//	public String getName() {
//	    return name;
//	}
//
//	/**
//	 * setName
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//	    this.name = name;
//	}		
//
//	/**
//	 * @return the staffCode
//	 */
//	public String getStaffCode() {
//		return staffCode;
//	}
//
//	/**
//	 * @param staffCode the staffCode to set
//	 */
//	public void setStaffCode(String staffCode) {
//		this.staffCode = staffCode;
//	}
//
//	/**
//	 * @return the customerCode
//	 */
//	public String getCustomerCode() {
//		return customerCode;
//	}
//
//	/**
//	 * @param customerCode the customerCode to set
//	 */
//	public void setCustomerCode(String customerCode) {
//		this.customerCode = customerCode;
//	}
//
//	/**
//	 * @return the shopId
//	 */
//	public Long getShopId() {
//		return shopId;
//	}
//
//	/**
//	 * @param shopId the shopId to set
//	 */
//	public void setShopId(Long shopId) {
//		this.shopId = shopId;
//	}
//
//	/**
//	 * @return the shopCode
//	 */
//	public String getShopCode() {
//		return shopCode;
//	}
//
//	/**
//	 * @param shopCode the shopCode to set
//	 */
//	public void setShopCode(String shopCode) {
//		this.shopCode = shopCode;
//	}
//
//	/**
//	 * @return the shopName
//	 */
//	public String getShopName() {
//		return shopName;
//	}
//
//	/**
//	 * @param shopName the shopName to set
//	 */
//	public void setShopName(String shopName) {
//		this.shopName = shopName;
//	}
//
//	/**
//	 * @return the staffName
//	 */
//	public String getStaffName() {
//		return staffName;
//	}
//
//	/**
//	 * @param staffName the staffName to set
//	 */
//	public void setStaffName(String staffName) {
//		this.staffName = staffName;
//	}
//
//	/**
//	 * @return the customerName
//	 */
//	public String getCustomerName() {
//		return customerName;
//	}
//
//	/**
//	 * @param customerName the customerName to set
//	 */
//	public void setCustomerName(String customerName) {
//		this.customerName = customerName;
//	}
//
//	/**
//	 * @return the excelType
//	 */
//	public int getExcelType() {
//		return excelType;
//	}
//
//	/**
//	 * @param excelType the excelType to set
//	 */
//	public void setExcelType(int excelType) {
//		this.excelType = excelType;
//	}
//
//	/**
//	 * @return the excelFile
//	 */
//	public File getExcelFile() {
//		return excelFile;
//	}
//
//	/**
//	 * @param excelFile the excelFile to set
//	 */
//	public void setExcelFile(File excelFile) {
//		this.excelFile = excelFile;
//	}
//
//	/**
//	 * @return the excelFileContentType
//	 */
//	public String getExcelFileContentType() {
//		return excelFileContentType;
//	}
//
//	/**
//	 * @param excelFileContentType the excelFileContentType to set
//	 */
//	public void setExcelFileContentType(String excelFileContentType) {
//		this.excelFileContentType = excelFileContentType;
//	}
//
//	/**
//	 * @return the status
//	 */
//	public int getStatus() {
//		return status;
//	}
//
//	/**
//	 * @param status the status to set
//	 */
//	public void setStatus(int status) {
//		this.status = status;
//	}
//
//	/**
//	 * @return the getAll
//	 */
//	public boolean isGetAll() {
//		return getAll;
//	}
//
//	/**
//	 * @param getAll the getAll to set
//	 */
//	public void setGetAll(boolean getAll) {
//		this.getAll = getAll;
//	}
//
//	public String getStaffTypeCode() {
//		return staffTypeCode;
//	}
//
//	public void setStaffTypeCode(String staffTypeCode) {
//		this.staffTypeCode = staffTypeCode;
//	}
//
//	/* (non-Javadoc)
//	 * @see ths.dms.web.action.general.AbstractAction#prepare()
//	 */
//	@Override
//	public void prepare() throws Exception {
//	    super.prepare();
//	    shopMgr = (ShopMgr)context.getBean("shopMgr");
//	    groupTransferMgr = (GroupTransferMgr)context.getBean("groupTransferMgr");
//	    staffMgr = (StaffMgr)context.getBean("staffMgr");
//	    customerMgr = (CustomerMgr)context.getBean("customerMgr");
//	}
//	
//
//	/* (non-Javadoc)
//	 * @see com.opensymphony.xwork2.ActionSupport#execute()
//	 */
//	@Override
//	public String execute() {
//		resetToken(result);
//		status = ActiveType.RUNNING.getValue();
//		if(shopId == null){
//			Staff staff = null;
//			try {
//				staff = staffMgr.getStaffByCode(currentUser.getUserName());
//			} catch (BusinessException e) {
//				LogUtility.logError(e, e.getMessage());
//			}			
//			if(staff!= null && staff.getShop()!= null){
//				shopId = staff.getShop().getId();		
//			}
//		}
//	    return SUCCESS;
//	}
//	
//	/**
//	 * Search.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since 25/07/2012 
//	 */
//	public String search(){
//	    
//	    result.put("page", page);
//	    result.put("max", max);
//	    try{
//		KPaging<GroupTransfer> kPaging = new KPaging<GroupTransfer>();
//		kPaging.setPageSize(max);
//		kPaging.setPage(page-1);
//		if(shopId < 1){
//			shopId = null;
//		}
//		String currentShopCode = null;
//		if(shopId == null){
//			Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());			
//			if(staff!= null && staff.getShop()!= null){
//				currentShopCode = staff.getShop().getShopCode();					
//			}
//			if(!StringUtil.isNullOrEmpty(currentShopCode)){
//				Shop shop = shopMgr.getShopByCode(currentShopCode);
//				if(shop!= null){
//					shopId = shop.getId();
//				}
//			} 
//		}
//		ObjectVO<GroupTransfer> groupTransferVO = groupTransferMgr.getListGroupTransfer(kPaging,shopId, code, name, staffCode, customerCode, ActiveType.parseValue(status),shopCode,getAll);
//		if(groupTransferVO!= null){
//		    result.put("total", groupTransferVO.getkPaging().getTotalRows());
//		    result.put("rows", groupTransferVO.getLstObject());		
//		}    		
//	    }catch (Exception e) {
//	    	LogUtility.logError(e, e.getMessage());
//	    }
//	    return JSON;
//	}
//		
//	/**
//	 * Gets the change form.
//	 *
//	 * @return the change form
//	 * @author hungtx
//	 * @since 26/07/2012
//	 */
//	public String getChangeForm(){
//		resetToken(result);
//		try{
//			status = ActiveType.RUNNING.getValue();
//			if(id!= null && id > 0){ // update
//				GroupTransfer groupTransfer = groupTransferMgr.getGroupTransferById(id);
//				if(groupTransfer!= null){
//					code = groupTransfer.getGroupTransferCode();					
//					name = groupTransfer.getGroupTransferName();
//					status = groupTransfer.getStatus().getValue();
//					if(groupTransfer.getShop()!= null){
//						shopCode = groupTransfer.getShop().getShopCode();
//						shopName = groupTransfer.getShop().getShopName();
//					}
//					if(groupTransfer.getStaff()!= null){
//						staffCode = groupTransfer.getStaff().getStaffCode();
//						staffName = groupTransfer.getStaff().getStaffName();
//					}
//				}
//			} else if(!StringUtil.isNullOrEmpty(shopCode)){ // create new
//				
//					Shop shop =  shopMgr.getShopByCode(shopCode);
//					if(shop!= null){
//						shopName = shop.getShopName();
//					}			
//			}
//		}catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}		
//		return SUCCESS;
//	}
//	
//	/**
//	 * Save info.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since 26/07/2012
//	 */
//	public String saveInfo(){
//		
//	    resetToken(result);
//	    boolean error = true;
//	    String errMsg = "";
//	    if(currentUser!= null){
//	    	try{
//	    		Shop ownerShop =  (Shop) request.getSession().getAttribute(ConstantManager.SESSION_SHOP);
//	    		if(ownerShop != null) {
//	    			if(!shopMgr.checkAncestor(ownerShop.getShopCode(), shopCode)) {
//	    				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_TO_USER, shopCode);
//	    				result.put(ERROR, error);
//	    				result.put("errMsg", errMsg);
//	    				return JSON;
//	    			}
//	    		} else {
//	    			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_TO_USER, shopCode);
//	    			result.put(ERROR, error);
//    				result.put("errMsg", errMsg);
//    				return JSON;
//	    		}
//	    		if(!staffMgr.checkStaffInShopOldFamily(staffCode, shopCode)) {
//	    			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, staffCode, shopCode);
//	    			result.put(ERROR, error);
//    				result.put("errMsg", errMsg);
//    				return JSON;
//	    		}
//	    		if(StringUtil.isNullOrEmpty(errMsg)){
//		    		Shop shop = shopMgr.getShopByCode(shopCode);	    		
//					Staff staff = staffMgr.getStaffByCode(staffCode);
//					if(shop== null){
//						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST,null, "catalog.focus.program.shop.code");
//					
//					} else if(!ActiveType.RUNNING.equals(shop.getStatus())) {
//						errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_ACTIVE, shopCode);
//					
//					} else if(staff == null){
//						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST,null, "catalog.staff.code");					
//					
//					} else if(!ActiveType.RUNNING.equals(staff.getStatus())) {
//						errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_ACTIVE, staffCode);
//					
//					} else if(staff.getStaffType()!= null && !StaffObjectType.NVGH.getValue().equals(staff.getStaffType().getObjectType())){
//						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.delivery.group.staff",staffCode);
//					}else if(!shop.getId().equals(staff.getShop().getId())){
//						errMsg =ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, staff.getStaffCode(), shopCode);
//					} else{
//						GroupTransfer groupTransfer = groupTransferMgr.getGroupTransferByCode(code,shop.getId());					
//			    		if(id!= null && id > 0){
//			    			if(groupTransfer!= null){
//			    				if(id.intValue() != groupTransfer.getId().intValue()){
//			    					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, code);
//				    			 } else {
//				    				 groupTransfer.setGroupTransferCode(code);
//				    				 groupTransfer.setGroupTransferName(name);
//				    				 groupTransfer.setShop(shop);
//				    				 groupTransfer.setStaff(staff);
//				    				 groupTransfer.setCreateUser(getCurrentUser().getUserName());
//				    				 groupTransfer.setStatus(ActiveType.parseValue(status));				    				 
//				    				 groupTransferMgr.updateGroupTransfer(groupTransfer,getLogInfoVO());
//				    				 error = false;
//				    				 result.put("id", id);
//				    				 result.put("shopCode", groupTransfer.getShop().getShopCode());
//				    			 }
//			    			}
//			    			
//			    		} else {
//			    			if(groupTransfer!= null){
//			    				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, code);		    				
//			    			} else {
//			    				 GroupTransfer newGroupTransfer = new GroupTransfer();
//			    				 newGroupTransfer.setGroupTransferCode(code);
//			    				 newGroupTransfer.setGroupTransferName(name);
//			    				 newGroupTransfer.setShop(shop);
//			    				 newGroupTransfer.setStaff(staff);
//			    				 newGroupTransfer.setCreateUser(getCurrentUser().getUserName());
//			    				 newGroupTransfer.setStatus(ActiveType.parseValue(status));
//			    				 newGroupTransfer = groupTransferMgr.createGroupTransfer(newGroupTransfer,getLogInfoVO());
//			    				 if(newGroupTransfer!= null){
//			    					 error = false;
//			    					 result.put("id", newGroupTransfer.getId());
//			    					 result.put("shopCode", newGroupTransfer.getShop().getShopCode());
//			    				 }
//			    			}
//			    		}
//					}	 
//	    		}
//	    	}catch (Exception e) {
//	    		LogUtility.logError(e, e.getMessage());	    		
//			}	    	
//	    }	    
//	    result.put(ERROR, error);
//	    if(error && StringUtil.isNullOrEmpty(errMsg)){
//	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//	    }
//	    result.put("errMsg", errMsg);
//		return JSON;
//	}
//	
//	/**
//	 * Gets the list customers.
//	 *
//	 * @return the list customers
//	 * @author hungtx
//	 * @since 26/07/2012
//	 */
//	public String getListCustomers(){
//		
//	    result.put("page", page);
//	    result.put("max", max);
//	    try{
//			KPaging<Customer> kPaging = new KPaging<Customer>();
//			kPaging.setPageSize(max);
//			kPaging.setPage(page-1);
//			ObjectVO<Customer> customerVO = customerMgr.getListCustomer(kPaging, null, null, null, null, null, null, null, null, null, id, null, null, null,shopCode,null,null,null);			
//			if(customerVO!= null){
//			    result.put("total", customerVO.getkPaging().getTotalRows());
//			    result.put("rows", customerVO.getLstObject());	
//			}    		
//	    }catch (Exception e) {
//	    	LogUtility.logError(e, e.getMessage());
//	    }
//	    return JSON;
//	}
//	
//	/**
//	 * Adds the customer.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since 26/07/2012
//	 */
//	public String addCustomer(){
//		
//	    resetToken(result);
//	    boolean error = true;
//	    String errMsg = "";
//	    if(currentUser!= null){
//	    	try{
//	    		Customer customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(shopCode, customerCode));	    		
//	    		if(customer == null){
//	    			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, customerCode);
//	    		}else if(!customer.getStatus().equals(ActiveType.RUNNING)){
//	    			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.customer.status.not.active", customer.getShortCode());
//	    		}else {
//	    			GroupTransfer groupTransfer = groupTransferMgr.getGroupTransferById(id);
//	    			//customer.setGroupTransfer(groupTransfer);
//	    			customerMgr.updateCustomer(customer,getLogInfoVO());
//	    			error = false;
//	    		}
//	    	}catch (Exception e) {
//	    		LogUtility.logError(e, e.getMessage());
//			}
//	    }
//	    result.put(ERROR, error);
//	    if(error && StringUtil.isNullOrEmpty(errMsg)){
//	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//	    }
//	    result.put("errMsg", errMsg);
//		return JSON;
//	}
//	
//	/**
//	 * Delete customer.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * 
//	 */
//	public String deleteCustomer(){
//		
//	    resetToken(result);
//	    boolean error = true;
//	    String errMsg = "";
//	    if(currentUser!= null){
//	    	try{
//	    		Customer customer = customerMgr.getCustomerById(id);	    		
//	    		if(customer!= null){
//	    			//customer.setGroupTransfer(null);
//	    			customerMgr.updateCustomer(customer,getLogInfoVO());
//	    			error = false;
//	    		}
//	    	}catch (Exception e) {
//	    		LogUtility.logError(e, e.getMessage());
//			}
//	    }
//	    result.put(ERROR, error);
//	    if(error && StringUtil.isNullOrEmpty(errMsg)){
//	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//	    }
//	    result.put("errMsg", errMsg);
//		return JSON;
//	}
//	
//	/**
//	 * Delete.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since 26/07/2012
//	 */
//	public String delete(){
//		
//	    resetToken(result);
//	    boolean error = true;
//	    String errMsg = "";
//	    if(currentUser!= null){
//	    	try{
//	    		Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());
//	    		if(staff != null && staff.getShop() != null){
//	    			if(groupTransferMgr.countNumGroupTransfer(staff.getShop().getId()) > 1){
//	    				GroupTransfer groupTransfer = groupTransferMgr.getGroupTransferById(id);	
//	    				if(groupTransfer != null && groupTransferMgr.isUsingByOthers(groupTransfer.getId())) {
//	    					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED_DB, groupTransfer.getGroupTransferCode());
//	    				}
//	    				if(StringUtil.isNullOrEmpty(errMsg) && groupTransfer!= null){
//	    					groupTransfer.setStatus(ActiveType.DELETED);
//	    					groupTransferMgr.updateGroupTransfer(groupTransfer,getLogInfoVO());
//	    					error = false;
//	    				}
//	    			}else{
//	    				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.delivery.group.one");
//	    			}
//	    		}
//	    	}catch (Exception e) {
//	    		LogUtility.logError(e, e.getMessage());
//	    		errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//			}
//	    }
//	    result.put(ERROR, error);
//	    if(error && StringUtil.isNullOrEmpty(errMsg)){
//	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//	    }
//	    result.put("errMsg", errMsg);
//		return JSON;
//	}
//	
//	/**
//	 * Import excel.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since Aug 24, 2012
//	 */
//	public String importExcel(){
//		if(excelType == 1){
//			return importGroupTransfer();
//		}
//		return importCustomer();
//	}
//	
//	/**
//	 * Import group transfer.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since Aug 24, 2012
//	 */
//	private String importGroupTransfer() {
//		isError = true;
//		totalItem = 0;
//		String message = "";		
//		lstView = new ArrayList<CellBean>();
//	    typeView = true;
//		List<CellBean> lstFails = new ArrayList<CellBean>();
//		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,4);
//		Shop ownerShop =  (Shop) request.getSession().getAttribute(ConstantManager.SESSION_SHOP);
//		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
//			try {
//				for(int i=0;i<lstData.size();i++){
//					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
//						message = "";
//						totalItem++;
//						GroupTransfer groupTransfer = new GroupTransfer();
//						groupTransfer.setCreateUser(currentUser.getUserName());
//						groupTransfer.setStatus(ActiveType.RUNNING);
//						List<String> row = lstData.get(i);
//						
//						// shop code
//						Shop shop = null;
//						String shopCode = "";
//						if(row.size() > 0){
//							String value = row.get(0);	
//							shopCode = value;
//							String checkMes = ValidateUtil.validateField(value, "catalog.focus.program.shop.code", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
//							if(StringUtil.isNullOrEmpty(checkMes)){
//								shop = shopMgr.getShopByCode(value);
//								if(shop == null){
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "catalog.focus.program.shop.code");										
//								} else if(!ActiveType.RUNNING.equals(shop.getStatus())){										
////									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");										
//									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_ACTIVE, value);
//									message += "\n";
//								} else{
//									if(ownerShop != null) {
//										if(!shopMgr.checkAncestor(ownerShop.getShopCode(), value)) {
//											message += ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_TO_USER, value);
//											message += "\n";
//										}
//									} else {
//										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_TO_USER, value);
//										message += "\n";
//									}
//								}
//								
//							}else{
//								message += checkMes;
//							}							
//							groupTransfer.setShop(shop);
//						}						
//						// group transfer code
//						if(row.size() > 1){
//							String value = row.get(1);
//							groupTransfer.setGroupTransferCode(value);							
//							String msg = ValidateUtil.validateField(value, "catalog.delivery.group.code", 20, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
//							if(StringUtil.isNullOrEmpty(msg)){
//								if(shop!= null && groupTransferMgr.getGroupTransferByCode(groupTransfer.getGroupTransferCode(),shop.getId())!= null){
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, true, "catalog.delivery.group.code");									
//								}
//							} else {
//								message += msg;
//							}
//						}						
//						// group transfer name
//						if(row.size() > 2){
//							String value = row.get(2);
//							groupTransfer.setGroupTransferName(value);
//							message += ValidateUtil.validateField(value, "catalog.delivery.group.name", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH);
//						}
//						
//						//staff code
//						if(row.size() > 3){
//							String value = row.get(3);
////							if(groupTransfer.getShop() != null) {
////								if(!staffMgr.checkStaffInShopOldFamily(value, groupTransfer.getShop().getShopCode())) {
////									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, value, groupTransfer.getShop().getShopCode());
////									message += "\n";
////								}
////							}
//							
//							String msg = ValidateUtil.validateField(value, "catalog.staff.code", 10, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH);
//							if(StringUtil.isNullOrEmpty(msg)){
//								Staff staff = staffMgr.getStaffByCode(value);
//								if(staff != null){
//									if(!StringUtil.isNullOrEmpty(shopCode) && !shopCode.equals(staff.getShop().getShopCode())){
//										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_BELONG_SHOP, value, shopCode);
//										message += "\n";
//									}
//									if(!ActiveType.RUNNING.equals(staff.getStatus())) {
//										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_STAFF_NOT_ACTIVE, value);
//										message += "\n";
//									}else if(staff.getStaffType()!= null && !StaffObjectType.NVGH.getValue().equals(staff.getStaffType().getObjectType())){
//										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.delivery.group.staff",value);
//										message += "\n";
//									}
//									groupTransfer.setStaff(staff);
//								} else {
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.staff.code");									
//								}
//							} else{
//								message+= msg;
//							}
//						}
//						if(isView == 0){
//							if (StringUtil.isNullOrEmpty(message)) {
//								groupTransferMgr.createGroupTransfer(groupTransfer,getLogInfoVO());	
//							} else {
//								lstFails.add(StringUtil.addFailBean(row, message));
//							}
//							typeView = false;
//						}else{
//							if(lstView.size()<100){
//								if(StringUtil.isNullOrEmpty(message)){
//						            message = "OK";
//						        } else{
//						        	message = StringUtil.convertHTMLBreakLine(message);
//						        }
//								lstView.add(StringUtil.addFailBean(row, message));
//							}
//							typeView = true;
//						}
//					}
//				}				
//				//Export error
//				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_GROUP_TRANSFER_FAIL);
//			} catch (Exception e) {
//				LogUtility.logError(e, e.getMessage());
//				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//			}
//		}
//		if (StringUtil.isNullOrEmpty(errMsg)) {
//			isError = false;
//		}
//		return SUCCESS;
//	}
//	
//	/**
//	 * Import customer.
//	 *
//	 * @return the string
//	 * @author hungtx
//	 * @since Aug 24, 2012
//	 */
//	private String importCustomer() {
//		isError = true;
//		totalItem = 0;
//		String message = "";	
//		lstView = new ArrayList<CellBean>();
//	    typeView = true;
//		List<CellBean> lstFails = new ArrayList<CellBean>();
//		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,3);		
//		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
//			try {
//				for(int i=0;i<lstData.size();i++){
//					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
//						message = "";
//						totalItem++;
//						GroupTransfer groupTransfer = new GroupTransfer();
//						groupTransfer.setStatus(ActiveType.RUNNING);
//						groupTransfer.setCreateUser(currentUser.getUserName());
//						Customer customer = null; 
//						List<String> row = lstData.get(i);
//						
//						// shop
//						Shop shop = null;	
//						if(row.size() > 2){
//							String value = row.get(2);									
//							String msg = ValidateUtil.validateField(value, "catalog.focus.program.shop.code", 50, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
//							if(!StringUtil.isNullOrEmpty(msg)){
//								message += msg;
//							}else{
//								shop = shopMgr.getShopByCode(value);
//								if(shop == null){
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.shop.code");										
//								} else if(!ActiveType.RUNNING.equals(shop.getStatus())){										
//									message += ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_ACTIVE, value);
//									message += "\n";
//								}
//							}							
//							groupTransfer.setShop(shop);
//						}
//						
//						// group transfer code
//						if(row.size() > 0){
//							String value = row.get(0);
//							groupTransfer.setGroupTransferCode(value);
//							String msg = ValidateUtil.validateField(value, "catalog.delivery.group.code", 20, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE,ConstantManager.ERR_MAX_LENGTH);
//							if(StringUtil.isNullOrEmpty(msg)){
//								if(shop!= null){
//									groupTransfer = groupTransferMgr.getGroupTransferByCode(groupTransfer.getGroupTransferCode(),shop.getId());
//								}								
//								if(groupTransfer== null){
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.delivery.group.code");									
//								}else if(!groupTransfer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())){
//									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.delivery.group.code");
//								}
//							} else {
//								message+=msg;
//							}
//						}
//						
//						// customer code
//						if(row.size() > 1){
//							String value = row.get(1);
//							String customerCode = "";
//							if(groupTransfer!= null && groupTransfer.getShop() != null){
//								customerCode = StringUtil.getFullCode(groupTransfer.getShop().getShopCode(), value); 
//							}
//							customer = customerMgr.getCustomerByCode(customerCode);
////							if(customer== null){		
////								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.code");									
////							
////							} else if(!ActiveType.RUNNING.equals(customer.getStatus())) {
////								message += ValidateUtil.getErrorMsg(ConstantManager.ERR_CUSTOMER_NOT_ACTIVE, value);
////								message += "\n";
////							
////							}else if(customer.getGroupTransfer()!= null && groupTransfer!= null){
////								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.delivery.group.customer.code.exist",value,customer.getGroupTransfer().getGroupTransferCode());
////								message += "\n";
////							
////							} else {
////								customer.setGroupTransfer(groupTransfer);
////							}
//						}
//						if(isView == 0){
//							if (StringUtil.isNullOrEmpty(message)) {
//								customerMgr.updateCustomer(customer,getLogInfoVO());								
//							} else {
//								lstFails.add(StringUtil.addFailBean(row, message));
//							}
//							typeView = false;
//						}else{
//							if(lstView.size()<100){
//								if(StringUtil.isNullOrEmpty(message)){
//						            message = "OK";
//						        } else{
//						        	message = StringUtil.convertHTMLBreakLine(message);
//						        }
//								lstView.add(StringUtil.addFailBean(row, message));
//							}
//							typeView = true;
//						}
//					}
//				}				
//				//Export error
//				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_GROUP_TRANSFER_CUSTOMER_FAIL);
//			} catch (Exception e) {
//				LogUtility.logError(e, e.getMessage());
//				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
//			}
//		}		
//		if (StringUtil.isNullOrEmpty(errMsg)) {
//			isError = false;
//		}
//		return SUCCESS;
//	}
//	/**
//	 * Search staff.
//	 *
//	 * @return the string
//	 * @author thongnm
//	 * @since 11/12/2012
//	 */
//	public String searchStaff(){		
//	    result.put("page", page);
//	    result.put("max", max);
//	    try{
//			KPaging<Staff> kPaging = new KPaging<Staff>();
//			kPaging.setPageSize(max);
//			kPaging.setPage(page-1);
//			if(!StringUtil.isNullOrEmpty(shopCode)){					
//				StaffObjectType staffType = null;
//				if(!StringUtil.isNullOrEmpty(staffTypeCode)){
//					staffType = StaffObjectType.parseValue(Integer.valueOf(staffTypeCode));					
//				}	
//				ObjectVO<Staff> staffVO = staffMgr.getListStaff(kPaging, code, null, name, null, ActiveType.RUNNING,shopCode,staffType, null,true,null,null,null);
//				if(staffVO!= null){
//					result.put("total", staffVO.getkPaging().getTotalRows());
//					   result.put("rows", staffVO.getLstObject());		
//				}
//			}			  		
//	    }catch (Exception e) {
//	    	LogUtility.logError(e, e.getMessage());
//	    }
//	    return JSON;
//	}
//	
//	public String getListExcelData(){
//		if(excelType == 1){
//			return exportGroupTransfer();
//		}
//		return exportCustomer();
//	}
//	
//	public String exportGroupTransfer(){
//		try{
//	    	if(shopId < 1){
//				shopId = null;
//			}
//			String currentShopCode = null;
//			if(shopId == null){
//				Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());			
//				if(staff!= null && staff.getShop()!= null){
//					currentShopCode = staff.getShop().getShopCode();					
//				}
//				if(!StringUtil.isNullOrEmpty(currentShopCode)){
//					Shop shop = shopMgr.getShopByCode(currentShopCode);
//					if(shop!= null){
//						shopId = shop.getId();
//					}
//				} 
//			}
//			ObjectVO<GroupTransfer> groupTransferVO = groupTransferMgr.getListGroupTransfer(null,shopId, code, name, staffCode, customerCode, ActiveType.parseValue(status),shopCode,getAll);
//			List<CellBean> lstFails = new ArrayList<CellBean>();
//			if(groupTransferVO != null && groupTransferVO.getLstObject() != null){
//				int size = groupTransferVO.getLstObject().size();
//				for(int i=0;i<size;i++){
//					GroupTransfer temp = groupTransferVO.getLstObject().get(i);
//					CellBean cellBean = new CellBean();
//					if(temp.getShop() !=null){
//						cellBean.setContent1(temp.getShop().getShopCode());
//					}
//					cellBean.setContent2(temp.getGroupTransferCode());
//					cellBean.setContent3(temp.getGroupTransferName());
//					if(temp.getStaff() != null){
//						cellBean.setContent4(temp.getStaff().getStaffCode());
//					}
//					if(temp.getStatus() != null){
//						if(ActiveType.RUNNING.equals(temp.getStatus())){
//							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.name"));
//						}else if(ActiveType.STOPPED.equals(temp.getStatus())){
//							cellBean.setContent5(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"pause.status.name"));
//						}
//					}
//					lstFails.add(cellBean);
//				}
//			}
//			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_DELIVERY_EXPORT);
//	    }catch (Exception e) {
//	    	result.put(ERROR, true);
//	    	LogUtility.logError(e, e.getMessage());
//	    }
//	    return JSON;
//	}
//	
//	public String exportCustomer(){
//		try{
//			if(shopId < 1){
//				shopId = null;
//			}
//			String currentShopCode = null;
//			if(shopId == null){
//				Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());			
//				if(staff!= null && staff.getShop()!= null){
//					currentShopCode = staff.getShop().getShopCode();					
//				}
//				if(!StringUtil.isNullOrEmpty(currentShopCode)){
//					Shop shop = shopMgr.getShopByCode(currentShopCode);
//					if(shop!= null){
//						shopId = shop.getId();
//					}
//				} 
//			}
//			ObjectVO<CustomerGTVO> CustomerGTVO = customerMgr.getListCustomerGTVOByGroupTransfer(null, shopId, code, name, staffCode, customerCode, ActiveType.parseValue(status), shopCode, getAll, customerName);
//			List<CellBean> lstFails = new ArrayList<CellBean>();
//			if(CustomerGTVO != null && CustomerGTVO.getLstObject() != null){
//				int size = CustomerGTVO.getLstObject().size();
//				for(int i=0;i<size;i++){
//					CustomerGTVO temp = CustomerGTVO.getLstObject().get(i);
//					CellBean cellBean = new CellBean();
//					cellBean.setContent1(temp.getGroupTransferCode());
//					cellBean.setContent2(temp.getGroupTransferName());
//					if(temp.getShortCode() != null){
//						cellBean.setContent3(temp.getShortCode());
//					}
//					if(temp.getCustomerName() != null){
//						cellBean.setContent4(temp.getCustomerName());
//					}
//					if(temp.getShopCode()!= null){
//						cellBean.setContent5(temp.getShopCode());
//					}
//					lstFails.add(cellBean);
//				}
//			}
//			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_DELIVERY_CUSTOMER_EXPORT);
//	    }catch (Exception e) {
//	    	result.put(ERROR, true);
//	    	LogUtility.logError(e, e.getMessage());
//	    }
//	    return JSON;
//	}
}

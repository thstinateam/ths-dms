package ths.dms.action.catalog;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class TotalMoneyQuotaCatalogAction extends AbstractAction {	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -140226918459707182L;

	/** The id. */
	private Long id;
	
	/** The code. */
	private String code;
	
	/** The name. */
	private String name;
		
	/** The sale amount. */
	private String saleAmount;
	
	/** The status. */
	private int status;	
	
		/** The channel type mgr. */
	private ChannelTypeMgr channelTypeMgr;
	
	/** The lst channel type. */
	private List<ChannelType> lstChannelType;
	
	/** The excel file. */
	private File excelFile;
	
	/** The excel file content type. */
	private String excelFileContentType;

	/**
	 * getId
	 * @return the id
	 */
	public Long getId() {
	    return id;
	}

	/**
	 * setId
	 * @param id the id to set
	 */
	public void setId(Long id) {
	    this.id = id;
	}

	/**
	 * getCode
	 * @return the code
	 */
	public String getCode() {
	    return code;
	}

	/**
	 * setCode
	 * @param code the code to set
	 */
	public void setCode(String code) {
	    this.code = code;
	}

	/**
	 * getName
	 * @return the name
	 */
	public String getName() {
	    return name;
	}

	/**
	 * setName
	 * @param name the name to set
	 */
	public void setName(String name) {
	    this.name = name;
	}

	

	/**
	 * getStatus
	 * @return the status
	 */
	public int getStatus() {
	    return status;
	}

	/**
	 * setStatus
	 * @param status the status to set
	 */
	public void setStatus(int status) {
	    this.status = status;
	}

	
	/**
	 * @return the lstChannelType
	 */
	public List<ChannelType> getLstChannelType() {
		return lstChannelType;
	}

	/**
	 * @param lstChannelType the lstChannelType to set
	 */
	public void setLstChannelType(List<ChannelType> lstChannelType) {
		this.lstChannelType = lstChannelType;
	}

	/**
	 * @return the excelFile
	 */
	public File getExcelFile() {
		return excelFile;
	}

	/**
	 * @param excelFile the excelFile to set
	 */
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	/**
	 * @return the excelFileContentType
	 */
	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	/**
	 * @param excelFileContentType the excelFileContentType to set
	 */
	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/**
	 * @return the saleAmount
	 */
	public String getSaleAmount() {
		return saleAmount;
	}

	/**
	 * @param saleAmount the saleAmount to set
	 */
	public void setSaleAmount(String saleAmount) {
		this.saleAmount = saleAmount;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    channelTypeMgr = (ChannelTypeMgr)context.getBean("channelTypeMgr");
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {	
	    status = ActiveType.RUNNING.getValue();
	    try{
	    	ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.CUSTOMER);							
	    	filter.setStatus(ActiveType.RUNNING);	
	    	ObjectVO<ChannelType> channelTypeVO = channelTypeMgr.getListChannelType(filter,null);
	    	if(channelTypeVO!= null){
	    		lstChannelType = channelTypeVO.getLstObject();
	    	}
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
		}
	    resetToken(result);
	    return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 07/08/2012 
	 */
	public String search() {		
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<ChannelType> kPaging = new KPaging<ChannelType>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			if(String.valueOf(ConstantManager.SELECT_ALL).equals(code)){
				code = null;
			}
			BigDecimal sAmount =  new BigDecimal(-1);;
			if(!StringUtil.isNullOrEmpty(saleAmount) && ValidateUtil.validateNumber(saleAmount)){
				sAmount= new BigDecimal(saleAmount);
			} else if(!StringUtil.isNullOrEmpty(saleAmount) && !ValidateUtil.validateNumber(saleAmount)){
				return JSON;
			}
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.CUSTOMER);							
	    	filter.setStatus(ActiveType.RUNNING);
	    	filter.setChannelTypeCode(code);
	    	filter.setChannelTypeName(name);
	    	filter.setStatusAmount(ActiveType.parseValue(status));
	    	filter.setSaleAmount(sAmount);
			ObjectVO<ChannelType> channelTypeVO = channelTypeMgr.getListChannelType(filter,kPaging);
			if (channelTypeVO != null) {
				result.put("total", channelTypeVO.getkPaging().getTotalRows());
				result.put("rows", channelTypeVO.getLstObject());
			} 

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 07/08/2012 
	 */
	public String saveOrUpdate(){
	    
	    resetToken(result);
	    boolean error = true;
	    if(currentUser!= null){
		try{
		    ChannelType channelType = channelTypeMgr.getChannelTypeByCode(code, ChannelTypeType.CUSTOMER);
		    if(id!= null && id > 0){
				if(channelType!= null){
					if(!StringUtil.isNullOrEmpty(saleAmount)){
				    	channelType.setSaleAmount(new BigDecimal(saleAmount));			    	

					}
			    	/*channelType.setStatusAmount(ActiveType.parseValue(Integer.valueOf(status)));
			    	channelTypeMgr.updateChannelType(channelType,getLogInfoVO());
			    	error = false;*/				    
				}    
			} else {
				/*if(channelType!= null && channelType.getStatusAmount()!= null){
					result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_CUSTOMER_EXIST_SKU));				    
				} else if(channelType!= null){
					if(!StringUtil.isNullOrEmpty(saleAmount)){
				    	channelType.setSaleAmount(new BigDecimal(saleAmount));
				    }				    
				    //channelType.setStatusAmount(ActiveType.parseValue(Integer.valueOf(status)));
				    channelTypeMgr.updateChannelType(channelType,getLogInfoVO());
				    error = false;
				    
				}*/
		    }		    
		}catch (Exception e) {
		    LogUtility.logError(e, e.getMessage());
		}		
	    }
	    result.put(ERROR, error);	    
	    return JSON;
	}
	
	/**
	 * Delete reason group.
	 *
	 * @return the string
	 * @author hungtx
	 * @since 07/08/2012 
	 */
	public String delete(){
	    
	    resetToken(result);
	    boolean error = true;
	    if(currentUser!= null){
		try{
		   ChannelType channelType = channelTypeMgr.getChannelTypeById(id);
		   if(channelType!= null){
			   channelType.setSaleAmount(null);
			  // channelType.setStatusAmount(null);
			   channelTypeMgr.updateChannelType(channelType,getLogInfoVO());
			   error = false;
		   }
		}catch (Exception e) {
		    LogUtility.logError(e, e.getMessage());
		}
	    }
	    result.put(ERROR, error);
	    return JSON;
	}
	
	/**
	 * Import excel.
	 *
	 * @return the string
	 */
	public String importExcel() {
		isError = true;
		errMsg = ValidateUtil.validateExcelFile(excelFile, excelFileContentType);
		totalItem = 0;
		String message = "";
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,2);		
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				for(int i=0;i<lstData.size();i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
						ChannelType channelType = null;
						List<String> row = lstData.get(i);
						
						// channel type code
						if(row.size() > 0){
							String value = row.get(0);
							channelType = channelTypeMgr.getChannelTypeByCode(value, ChannelTypeType.CUSTOMER);
							if(channelType== null){
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, null, "catalog.customer.type.code");									
							}else if(ActiveType.STOPPED.getValue().equals(channelType.getStatus().getValue())){
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, null, "catalog.customer.type.code");									
							}/*else if(channelType.getStatusAmount()!= null){
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.customer.type.code");									
							}*/
						}
						
						// sku amount
						BigDecimal skuAmount = null;
						if(row.size() > 1){
							String value = row.get(1);
							String msg = ValidateUtil.validateField(value, "catalog.toal.money.quota.level", 17, ConstantManager.ERR_REQUIRE,ConstantManager.ERR_MAX_LENGTH,ConstantManager.ERR_INTEGER);
							if(!StringUtil.isNullOrEmpty(msg)){
								message += msg;
							} else {
								try{
									skuAmount = new BigDecimal(value);
									if(skuAmount.longValue() <= 0){
										skuAmount = null;
									}										
								}catch (Exception e) {}
							}							
							if(skuAmount == null){
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_INT, null, "catalog.toal.money.quota.level");									
							}
						}
						if (StringUtil.isNullOrEmpty(message)) {
							channelType.setSaleAmount(skuAmount);
							//channelType.setStatusAmount(ActiveType.RUNNING);
							channelTypeMgr.updateChannelType(channelType,getLogInfoVO());
						} else {
							lstFails.add(StringUtil.addFailBean(row, message));
						}						
					}
				}				
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_TOTAL_MONEY_QUOTA_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}		
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

}

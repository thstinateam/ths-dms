/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.catalog;

import java.util.Date;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.vo.SettingsVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * Settings Catalog Action
 * @author duongdt3
 * @since 27/11/2015 
 */
public class SettingsCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	private static final long MIN_DISTANCE_CHECK_CUSTOMER = 0;
	private static final long MAX_DISTANCE_CHECK_CUSTOMER = 10000000;

	private static final long MIN_PROBLEM_ATTACH_SIZE_MAX = 1;
	private static final long MAX_PROBLEM_ATTACH_SIZE_MAX = 1024 * 100;

	private static final long MAX_LENGHT_PROBLEM_FILE_TYPE_UPLOAD = 200;

	private static final long MIN_TIME_REQUEST_POSITION = 10;
	private static final long MAX_TIME_REQUEST_POSITION = 60000;

	private static final long MIN_TIME_REQUEST_SYNC_DATA = 1;
	private static final long MAX_TIME_REQUEST_SYNC_DATA = 6000;
	private ApParamMgr apParamMgr;
	
	private String apParamCode;
	private String apParamName;
	private String type;
	private String description;
	private Integer status;
	private Long shopId;
	private String shopCode;
	private Shop shop; 
	
	private SettingsVO settingsVo;
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
	}

	@Override
	public String execute() {
		Date startLogDate = DateUtil.now();
		try {
			resetToken(result);
			
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			
			settingsVo = new SettingsVO();
			ApParam appHourAutoCloseDate = apParamMgr.getApParamByCodeEx(Constant.SYS_SHOP_LOCK_EXEC_TIME, ActiveType.RUNNING);
			if (appHourAutoCloseDate != null) {
				settingsVo.setHourAutoCloseDateId(appHourAutoCloseDate.getId());
				settingsVo.setHourAutoCloseDateValue(appHourAutoCloseDate.getValue());
			}
			
			ApParam appDistanceCheckCustomer = apParamMgr.getApParamByCodeEx(Constant.SYS_CHECK_DUPLICATE_CUSTOMER_DISTANCE, ActiveType.RUNNING);
			if (appDistanceCheckCustomer != null) {
				settingsVo.setDistanceCheckCustomerId(appDistanceCheckCustomer.getId());
				settingsVo.setDistanceCheckCustomerValue(Long.valueOf(appDistanceCheckCustomer.getValue()));
			}
			settingsVo.setMinDistanceCheckCustomerValue(MIN_DISTANCE_CHECK_CUSTOMER);
			settingsVo.setMaxDistanceCheckCustomerValue(MAX_DISTANCE_CHECK_CUSTOMER);
			
			ApParam appProblemAttachSizeMax = apParamMgr.getApParamByCodeEx(Constant.MAX_SIZE_UPLOAD_ATTACH, ActiveType.RUNNING);
			if (appProblemAttachSizeMax != null) {
				settingsVo.setProblemAttachSizeMaxId(appProblemAttachSizeMax.getId());
				settingsVo.setProblemAttachSizeMaxValue(Long.valueOf(appProblemAttachSizeMax.getValue()));
			}
			settingsVo.setMinProblemAttachSizeMaxValue(MIN_PROBLEM_ATTACH_SIZE_MAX);
			settingsVo.setMaxProblemAttachSizeMaxValue(MAX_PROBLEM_ATTACH_SIZE_MAX);
			
			ApParam appProblemFileTypeUpload = apParamMgr.getApParamByCodeEx(Constant.WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH, ActiveType.RUNNING);
			if (appProblemFileTypeUpload != null) {
				settingsVo.setProblemFileTypeUploadId(appProblemFileTypeUpload.getId());
				settingsVo.setProblemFileTypeUploadValue(appProblemFileTypeUpload.getValue());
			}
			settingsVo.setMaxLenghtProblemFileTypeUploadValue(MAX_LENGHT_PROBLEM_FILE_TYPE_UPLOAD);
			
			ApParam appTimeRequestPosition = apParamMgr.getApParamByCodeEx(Constant.SYS_INTERVAL_GPS_FUSED_POSITION, ActiveType.RUNNING);
			if (appProblemFileTypeUpload != null) {
				settingsVo.setTimeRequestPositionId(appTimeRequestPosition.getId());
				settingsVo.setTimeRequestPositionValue(Long.valueOf(appTimeRequestPosition.getValue()));
			}
			settingsVo.setMinTimeRequestPositionValue(MIN_TIME_REQUEST_POSITION);
			settingsVo.setMaxTimeRequestPositionValue(MAX_TIME_REQUEST_POSITION);
			
			ApParam appTimeRequestSyncData = apParamMgr.getApParamByCodeEx(Constant.SYS_TIME_SYNC_TO_SERVER, ActiveType.RUNNING);
			if (appProblemFileTypeUpload != null) {
				settingsVo.setTimeRequestSyncDataId(appTimeRequestSyncData.getId());
				settingsVo.setTimeRequestSyncDataValue(Long.valueOf(appTimeRequestSyncData.getValue()));
			}
			settingsVo.setMinTimeRequestSyncDataValue(MIN_TIME_REQUEST_SYNC_DATA);
			settingsVo.setMaxTimeRequestSyncDataValue(MAX_TIME_REQUEST_SYNC_DATA);
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SettingsCatalogAction.execute()"), createLogErrorStandard(startLogDate));
		}
		return SUCCESS;
	}

	/**
	 * update cap nhat cau hinh
	 * @return the string
	 * @author duongdt3
	 * @since 26/11/2015
	 */
	public String update() {
		resetToken(result);
		Date startLogDate = DateUtil.now();
		try {
			if (currentUser == null || currentUser.getShopRoot() == null || currentUser.getRoleToken() == null) {
				result.put(ERROR, true);
				result.put(ERR_MSG, R.getResource("common.cms.undefined"));
				return JSON;
			}
			
			if (settingsVo != null) {
				errMsg = "";
				
				if (StringUtil.isNullOrEmpty(errMsg) && (settingsVo.getDistanceCheckCustomerId() == null || settingsVo.getDistanceCheckCustomerId() <= 0 
						|| settingsVo.getDistanceCheckCustomerValue() == null || settingsVo.getDistanceCheckCustomerValue() < MIN_DISTANCE_CHECK_CUSTOMER 
						|| settingsVo.getDistanceCheckCustomerValue() > MAX_DISTANCE_CHECK_CUSTOMER)) {
					errMsg = R.getResource("common.not.date", R.getResource("jsp.settings.system.distance.check.customer"));
				}
				
				if (StringUtil.isNullOrEmpty(errMsg) && (settingsVo.getHourAutoCloseDateId() == null || settingsVo.getHourAutoCloseDateId() <= 0 
						|| StringUtil.isNullOrEmpty(settingsVo.getHourAutoCloseDateValue()))
						|| settingsVo.getHourAutoCloseDateValue().length() > MAX_LENGHT_PROBLEM_FILE_TYPE_UPLOAD) {
					errMsg = R.getResource("common.not.date", R.getResource("jsp.settings.system.hour.auto.close.date"));
				}

				if (StringUtil.isNullOrEmpty(errMsg) && !ValidateUtil.isValidDate(settingsVo.getHourAutoCloseDateValue(), "HH:mm")){
					errMsg = R.getResource("common.not.date", R.getResource("jsp.settings.system.hour.auto.close.date"));
				}
				
				if (StringUtil.isNullOrEmpty(errMsg) && (settingsVo.getProblemAttachSizeMaxId() == null || settingsVo.getProblemAttachSizeMaxId() <= 0 
						|| settingsVo.getProblemAttachSizeMaxValue() == null || settingsVo.getProblemAttachSizeMaxValue() < MIN_PROBLEM_ATTACH_SIZE_MAX 
						|| settingsVo.getProblemAttachSizeMaxValue() > MAX_PROBLEM_ATTACH_SIZE_MAX)) {
					errMsg = R.getResource("common.not.date", R.getResource("jsp.settings.system.problem.attach.size.max"));
				}
				
				if (StringUtil.isNullOrEmpty(errMsg) && (settingsVo.getProblemFileTypeUploadId() == null || settingsVo.getProblemFileTypeUploadId() <= 0 
						|| StringUtil.isNullOrEmpty(settingsVo.getProblemFileTypeUploadValue()))) {
					errMsg = R.getResource("common.not.date", R.getResource("jsp.settings.system.problem.file.type.upload"));
				}
				
				if (StringUtil.isNullOrEmpty(errMsg)){
					errMsg = ValidateUtil.getErrorMsgOfSpecialCharInSpecial(settingsVo.getProblemFileTypeUploadValue(), R.getResource("jsp.settings.system.problem.file.type.upload"));
				}
				
				if (StringUtil.isNullOrEmpty(errMsg) && (settingsVo.getTimeRequestPositionId() == null || settingsVo.getTimeRequestPositionId() <= 0 
						|| settingsVo.getTimeRequestPositionValue() == null || settingsVo.getTimeRequestPositionValue() < MIN_TIME_REQUEST_POSITION 
						|| settingsVo.getTimeRequestPositionValue() > MAX_TIME_REQUEST_POSITION)) {
					errMsg = R.getResource("common.not.date", R.getResource("jsp.settings.system.tablet.time.request.position"));
				}
				
				if (StringUtil.isNullOrEmpty(errMsg) && (settingsVo.getTimeRequestSyncDataId() == null || settingsVo.getTimeRequestSyncDataId() <= 0 
						|| settingsVo.getTimeRequestSyncDataValue() == null || settingsVo.getTimeRequestSyncDataValue() < MIN_TIME_REQUEST_SYNC_DATA 
						|| settingsVo.getTimeRequestSyncDataValue() > MAX_TIME_REQUEST_SYNC_DATA)) {
					errMsg = R.getResource("common.not.date", R.getResource("jsp.settings.system.tablet.time.request.sync.data"));
				}
				
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				
				updateParam(settingsVo.getDistanceCheckCustomerId(), String.valueOf(settingsVo.getDistanceCheckCustomerValue()));
				updateParam(settingsVo.getHourAutoCloseDateId(), String.valueOf(settingsVo.getHourAutoCloseDateValue()));
				updateParam(settingsVo.getProblemAttachSizeMaxId(), String.valueOf(settingsVo.getProblemAttachSizeMaxValue()));
				updateParam(settingsVo.getProblemFileTypeUploadId(), String.valueOf(settingsVo.getProblemFileTypeUploadValue()));
				updateParam(settingsVo.getTimeRequestPositionId(), String.valueOf(settingsVo.getTimeRequestPositionValue()));
				updateParam(settingsVo.getTimeRequestSyncDataId(), String.valueOf(settingsVo.getTimeRequestSyncDataValue()));
				
			} else{
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SettingsCatalogAction.update()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	private void updateParam(Long id, String value) throws BusinessException{
		if (id != null && id > 0) {
			// cap nhat tham so
			ApParam ap = apParamMgr.getApParamById(id);
			if (ap != null) {
				ap.setValue(value);
				apParamMgr.updateApParamEx(ap, getLogInfoVO());
			}
		}
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ApParamMgr getApParamMgr() {
		return apParamMgr;
	}

	public void setApParamMgr(ApParamMgr apParamMgr) {
		this.apParamMgr = apParamMgr;
	}

	public String getApParamCode() {
		return apParamCode;
	}

	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}

	public String getApParamName() {
		return apParamName;
	}

	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SettingsVO getSettingsVo() {
		return settingsVo;
	}
	
	public void setSettingsVo(SettingsVO settingsVo) {
		this.settingsVo = settingsVo;
	}
}

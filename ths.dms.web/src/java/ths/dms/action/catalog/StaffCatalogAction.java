package ths.dms.action.catalog;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.AreaMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.LogMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ActionAudit;
import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Area;
import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.vo.AreaVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class StaffCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5259937519678404511L;

	// STAFF
	
	private ApParamMgr apParamMgr;
	
	private String startDate;
	private String endDate;
	private File excelFile;
	private String excelFileContentType;
	private Integer productId;
	private String productCode;
	private ShopMgr shopMgr;
	private String shopCode;
	private Integer status;
	private Integer saleType;
	private ProductInfoMgr productInfoMgr;
	private Product product;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private String staffPhone;
	private Long staffType;
	private String idCard;
	private String idDate;
	private String idPlace;
	private String email;
	private Long areaId;
	private List<ApParam> lstsaleGroup;
	private String saleGroup;
	private String street;
	private String address;
	private List<ChannelType> lstStaffType;
	private ChannelTypeMgr channelTypeMgr;
	private StaffMgr staffMgr;
	private AreaMgr areaMgr;
	private Staff staff;
	private String provinceCode;
	private String districtCode;
	private String wardCode;
	private String shopName;
	private List<ProductInfo> lstCategory;
	private Long categoryId;
	private ProductInfo productInfo;
	private SaleOrderMgr saleOrderMgr;
	private Integer excelType;
	private LogMgr logMgr;
	private Long actionGeneralLogId;
	private Integer staffObject;
	private List<ApParam> lstSaleType;
	private List<ApParam> lstWorkState;
	private String staffOwnerCode;
	private Integer staffObjectType;
//	private Long saleType;
	private String saleTypeCode;
	private String workStateCode;
	private List<Long> lstCategoryId;
	private List<StaffSaleCat> lstStaffSaleCat;
	private Integer gender;
	private String workStartDate;
	private String education;
	private String position;
	private String staffTelephone;
	private Integer supervisorStaffValue;
	private List<Long> lstStaffId;
	private int roleViewDetailStaff;
	private List<Long> lstDeleteCategoryId;
	private Integer isSupervisorStaff;
	private String staffGenerateCode;
	private String shopCodeHidden;
	private Boolean isSaleStaff = false;
	private Staff newStaff;
	private List<Area> areas;
	private Area area;	
	private Staff staffSignIn;
	private String areaCode;
	
	private List<ProductInfo>lstProductInfo;
	
	private List<AreaVO> lstProvince;
	private List<AreaVO> lstDistrict;
	private List<AreaVO> lstPrecinct;
	
	/** vuongmq; Oct 09, 2014, them shop*/
	private Shop shop;
	
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getWorkStateCode() {
		return workStateCode;
	}

	public void setWorkStateCode(String workStateCode) {
		this.workStateCode = workStateCode;
	}

	public List<ApParam> getLstWorkState() {
		return lstWorkState;
	}

	public void setLstWorkState(List<ApParam> lstWorkState) {
		this.lstWorkState = lstWorkState;
	}

	public Integer getStaffObjectType() {
		return staffObjectType;
	}

	public void setStaffObjectType(Integer staffObjectType) {
		this.staffObjectType = staffObjectType;
	}

	public void setStaffSignIn(Staff staffSignIn) {
		this.staffSignIn = staffSignIn;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public List<ProductInfo> getLstProductInfo() {
		return lstProductInfo;
	}

	public void setLstProductInfo(List<ProductInfo> lstProductInfo) {
		this.lstProductInfo = lstProductInfo;
	}
	
	public Area getArea() {
		return area;
	}
	
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Staff getStaffSignIn() {
		return staffSignIn;
	}

	public void setStaffSigIn(Staff staffSigIn) {
		this.staffSignIn = staffSigIn;
	}

	public Staff getNewStaff() {
		return newStaff;
	}

	public void setNewStaff(Staff newStaff) {
		this.newStaff = newStaff;
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public Boolean getIsSaleStaff() {
		return isSaleStaff;
	}

	public void setIsSaleStaff(Boolean isSaleStaff) {
		this.isSaleStaff = isSaleStaff;
	}

	public String getShopCodeHidden() {
		return shopCodeHidden;
	}

	public void setShopCodeHidden(String shopCodeHidden) {
		this.shopCodeHidden = shopCodeHidden;
	}

	public String getStaffGenerateCode() {
		return staffGenerateCode;
	}

	public void setStaffGenerateCode(String staffGenerateCode) {
		this.staffGenerateCode = staffGenerateCode;
	}

	public Integer getIsSupervisorStaff() {
		return isSupervisorStaff;
	}

	public void setIsSupervisorStaff(Integer isSupervisorStaff) {
		this.isSupervisorStaff = isSupervisorStaff;
	}

	public List<Long> getLstDeleteCategoryId() {
		return lstDeleteCategoryId;
	}

	public void setLstDeleteCategoryId(List<Long> lstDeleteCategoryId) {
		this.lstDeleteCategoryId = lstDeleteCategoryId;
	}

	public List<Long> getLstStaffId() {
		return lstStaffId;
	}

	public void setLstStaffId(List<Long> lstStaffId) {
		this.lstStaffId = lstStaffId;
	}

	public Integer getSupervisorStaffValue() {
		return supervisorStaffValue;
	}

	public void setSupervisorStaffValue(Integer supervisorStaffValue) {
		this.supervisorStaffValue = supervisorStaffValue;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(String workStartDate) {
		this.workStartDate = workStartDate;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getStaffTelephone() {
		return staffTelephone;
	}

	public void setStaffTelephone(String staffTelephone) {
		this.staffTelephone = staffTelephone;
	}

	public List<StaffSaleCat> getLstStaffSaleCat() {
		return lstStaffSaleCat;
	}

	public void setLstStaffSaleCat(List<StaffSaleCat> lstStaffSaleCat) {
		this.lstStaffSaleCat = lstStaffSaleCat;
	}

	public List<Long> getLstCategoryId() {
		return lstCategoryId;
	}

	public void setLstCategoryId(List<Long> lstCategoryId) {
		this.lstCategoryId = lstCategoryId;
	}

	public String getSaleTypeCode() {
		return saleTypeCode;
	}

	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}

	public String getStaffOwnerCode() {
		return staffOwnerCode;
	}

	public void setStaffOwnerCode(String staffOwnerCode) {
		this.staffOwnerCode = staffOwnerCode;
	}

	public List<ApParam> getLstSaleType() {
		return lstSaleType;
	}

	public void setLstSaleType(List<ApParam> lstSaleType) {
		this.lstSaleType = lstSaleType;
	}

	public Integer getStaffObject() {
		return staffObject;
	}

	public void setStaffObject(Integer staffObject) {
		this.staffObject = staffObject;
	}

	public Long getActionGeneralLogId() {
		return actionGeneralLogId;
	}

	public void setActionGeneralLogId(Long actionGeneralLogId) {
		this.actionGeneralLogId = actionGeneralLogId;
	}

	public Integer getExcelType() {
		return excelType;
	}

	public void setExcelType(Integer excelType) {
		this.excelType = excelType;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffPhone() {
		return staffPhone;
	}

	public void setStaffPhone(String staffPhone) {
		this.staffPhone = staffPhone;
	}

	public Long getStaffType() {
		return staffType;
	}

	public void setStaffType(Long staffType) {
		this.staffType = staffType;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getIdDate() {
		return idDate;
	}

	public void setIdDate(String idDate) {
		this.idDate = idDate;
	}

	public String getIdPlace() {
		return idPlace;
	}

	public void setIdPlace(String idPlace) {
		this.idPlace = idPlace;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<ChannelType> getLstStaffType() {
		return lstStaffType;
	}

	public void setLstStaffType(List<ChannelType> lstStaffType) {
		this.lstStaffType = lstStaffType;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public List<ProductInfo> getLstCategory() {
		return lstCategory;
	}

	public void setLstCategory(List<ProductInfo> lstCategory) {
		this.lstCategory = lstCategory;
	}

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public int getRoleViewDetailStaff() {
		return roleViewDetailStaff;
	}

	public void setRoleViewDetailStaff(int roleViewDetailStaff) {
		this.roleViewDetailStaff = roleViewDetailStaff;
	}

	public List<AreaVO> getLstProvince() {
		return lstProvince;
	}

	public void setLstProvince(List<AreaVO> lstProvince) {
		this.lstProvince = lstProvince;
	}

	public List<AreaVO> getLstDistrict() {
		return lstDistrict;
	}

	public void setLstDistrict(List<AreaVO> lstDistrict) {
		this.lstDistrict = lstDistrict;
	}

	public List<AreaVO> getLstPrecinct() {
		return lstPrecinct;
	}

	public void setLstPrecinct(List<AreaVO> lstPrecinct) {
		this.lstPrecinct = lstPrecinct;
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		areaMgr = (AreaMgr) context.getBean("areaMgr");
		saleOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		logMgr = (LogMgr) context.getBean("logMgr");
		apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
	}

	/**
	 * @author vuongmq
	 * @since Oct 09, 2014
	 * @description: lay shop chon theo vai tro
	 */
	@Override
	public String execute() {
		resetToken(result);
		try {
			if(currentUser != null && currentUser.getShopRoot() != null){
	    		//staff = staffMgr.getStaffByCode(currentUser.getUserName());
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.STAFF);							
	    	filter.setStatus(ActiveType.RUNNING);	    	
			ObjectVO<ChannelType> lstStaffTypeTmp = channelTypeMgr.getListChannelType(filter,null);
			lstStaffType = lstStaffTypeTmp.getLstObject();
			lstSaleType = apParamMgr.getListApParam(ApParamType.STAFF_SALE_TYPE, ActiveType.RUNNING);
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.execute()" +e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * Search.
	 * 
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012
	 * @update: vuongmq; Oct 09,2014
	 */
	public String search() {
		
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Staff> kPaging = new KPaging<Staff>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);			
			//if(StringUtil.isNullOrEmpty(shopCodeHidden) || !checkAncestor(shopCodeHidden)){
			if(StringUtil.isNullOrEmpty(shopCodeHidden) || !checkShopPermission(shopCodeHidden)){
				result.put("total",0);
				result.put("rows", new ArrayList<Staff>());
			}
			List<Integer> lstChannelObjectType = new ArrayList<Integer>();
			lstChannelObjectType.add(1);
			lstChannelObjectType.add(2);
			lstChannelObjectType.add(3);
			lstChannelObjectType.add(4);			
			StaffFilter filter = new StaffFilter();
			filter.setkPaging(kPaging);
			filter.setStaffCode(staffCode);
			filter.setStaffName(staffName);
			filter.setStatus(ActiveType.parseValue(status));
			filter.setShopCode(shopCodeHidden);
			filter.setChannelTypeType(ChannelTypeType.STAFF);
			filter.setLstChannelObjectType(lstChannelObjectType);
			ObjectVO<Staff> lstStaff = staffMgr.getListStaffEx2(filter);
			if (lstStaff != null) {
				result.put("total", lstStaff.getkPaging().getTotalRows());
				result.put("rows", lstStaff.getLstObject());
			}else {
				result.put("total",0);
				result.put("rows", new ArrayList<Staff>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.search()" + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * @author tientv
	 * @update: vuongmq; Oct 09,2014
	 */
	public String showArea(){
		try {
			area = areaMgr.getAreaByCode(areaCode);
			if(area==null){
				return JSON;
			}
			areas = areaMgr.getListSubArea(area.getId(), ActiveType.RUNNING, true);
			result.put("areas",areas);			
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.showArea()" + e.getMessage());
		}
		return JSON;
	}
	
	
	/**
	 * Search supervisor.
	 * @return the string
	 * @author lamnh
	 * @since Oct 16, 2012
	 */
	public String searchSupervisor() {
		
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Staff> kPaging = new KPaging<Staff>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<Staff> lstStaff = staffMgr.getListStaffByOwnerId(kPaging, staffId, shopCode, shopName, staffCode, staffName);			
			if (lstStaff != null) {
				result.put("total", lstStaff.getkPaging().getTotalRows());
				result.put("rows", lstStaff.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.searchSupervisor()" + e.getMessage());
		}
		return JSON;
	}

	/**
	 * Search category.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 * @update: vuongmq; Oct 09,2014
	 */
	public String searchCategory() {
		
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<StaffSaleCat> kPaging = new KPaging<StaffSaleCat>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<StaffSaleCat> lstCategory = staffMgr.getListStaffSaleCat(kPaging, staffId);
			if (lstCategory != null) {
				result.put("total", lstCategory.getkPaging().getTotalRows());
				result.put("rows", lstCategory.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.searchCategory()" +e.getMessage());
		}
		return JSON;
	}

	/**
	 * Search history.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 * @update: vuongmq; Oct 09,2014
	 */
	public String searchHistory() {
		
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date startTemp = null;
			Date endTemp = null;
			if (!StringUtil.isNullOrEmpty(startDate)) {
				startTemp = ths.dms.web.utils.DateUtil.parse(startDate,ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(endDate)) {
				endTemp = ths.dms.web.utils.DateUtil.parse(endDate,ConstantManager.FULL_DATE_FORMAT);
			}
			SaleOrderFilter<SaleOrder> filter = new SaleOrderFilter<SaleOrder>();
			filter.setIsCompareEqual(true);
			filter.setShopCode(shopCode);
			filter.setStaffId(staffId);
			filter.setFromDate(startTemp);
			filter.setToDate(endTemp);
			filter.setApproved(SaleOrderStatus.APPROVED);
			filter.setGetBothINandSO(true);
			ObjectVO<SaleOrder> lstPrice = saleOrderMgr.getListSaleOrder(filter, kPaging);
			if (lstPrice != null) {
				result.put("total", lstPrice.getkPaging().getTotalRows());
				result.put("rows", lstPrice.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.searchHistory()" + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Search changed.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String searchChanged() {
		
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<ActionAudit> kPaging = new KPaging<ActionAudit>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			staff = staffMgr.getStaffById(staffId);
			if(staff != null){
				staffCode = staff.getStaffCode();
			}else{
				staffCode = "-1";
			}
			Date startTemp = null;
			Date endTemp = null;
			if (!StringUtil.isNullOrEmpty(startDate)) {
				startTemp = ths.dms.web.utils.DateUtil.parse(startDate,ConstantManager.FULL_DATE_FORMAT);
			}
			if (!StringUtil.isNullOrEmpty(endDate)) {
				endTemp = ths.dms.web.utils.DateUtil.parse(endDate,ConstantManager.FULL_DATE_FORMAT);
			}
			ObjectVO<ActionAudit> lstChanged = logMgr.getListActionGeneralLog(kPaging, null, null,staffId, startTemp, endTemp);
			if (lstChanged != null) {
				result.put("total", lstChanged.getkPaging().getTotalRows());
				result.put("rows", lstChanged.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.searchChanged()" +e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * Search detail changed.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String searchDetailChanged() {
		
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<ActionAuditDetail> kPaging = new KPaging<ActionAuditDetail>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<ActionAuditDetail> lstCategory = logMgr.getListActionGeneralLogDetail(kPaging, actionGeneralLogId);
			if (lstCategory != null) {
				result.put("total", lstCategory.getkPaging().getTotalRows());
				result.put("rows", lstCategory.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.searchDetailChanged()"+ e.getMessage());
		}
		return JSON;
	}

	/**
	 * View detail.
	 *
	 * @return the string
	 * @author lamnh
	 * @throws BusinessException 
	 * @since Oct 1, 2012
	 */
	public String viewDetail(){
		/*try{
			resetToken(result);
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.STAFF);							
	    	filter.setStatus(ActiveType.RUNNING);
	    	List<Integer> lstObjectType = new ArrayList<Integer>();
	    	lstObjectType.add(StaffObjectType.NVGH.getValue());
	    	lstObjectType.add(StaffObjectType.NVTT.getValue());
	    	filter.setLstObjectType(lstObjectType);
	    	ObjectVO<ChannelType> lstStaffTypeTmp = channelTypeMgr.getListChannelType(filter,null); 
	    	lstStaffType = lstStaffTypeTmp.getLstObject();
	    	lstSaleType = apParamMgr.getListApParam(ApParamType.STAFF_SALE_TYPE, ActiveType.RUNNING);
	    	lstWorkState = apParamMgr.getListApParam(ApParamType.WORK_STATE_TYPE, ActiveType.RUNNING);
			lstsaleGroup = apParamMgr.getListApParam(ApParamType.STAFF_SALE_GROUP, ActiveType.RUNNING);
			
			lstProvince = areaMgr.getListAreaByType(null, AreaType.PROVINCE.getValue(), ActiveType.RUNNING.getValue());
			ObjectVO<Area> areaVO = areaMgr.getListArea(null, null, null, null, ActiveType.RUNNING,null,null, null,null, null,null, AreaType.PROVINCE,null);
			if(areaVO!=null){
				areas = areaVO.getLstObject();
			}
			if(staffId==null || staffId==0){		
				//Lấy địa bàn của user NPP đăng nhập
				staffSignIn = getStaffByCurrentUser();
				Area areaStaff = null;
				if(staffSignIn != null && staffSignIn.getArea() != null){
					areaStaff = staffSignIn.getArea();
					areaId = areaStaff.getId();
					provinceCode = areaStaff.getProvince();
					districtCode = areaStaff.getDistrict();
					wardCode = areaStaff.getAreaCode();
					if (areaStaff.getParentArea() != null && areaStaff.getParentArea().getParentArea() != null) {
						Area areaTmp = areaStaff.getParentArea().getParentArea();
						lstDistrict = areaMgr.getListAreaByType(areaTmp.getId(),
								AreaType.DISTRICT.getValue(), ActiveType.RUNNING.getValue());
						provinceCode = areaTmp.getAreaCode();
					}
					if (areaStaff.getParentArea() != null ) {
						Area areaTmp = areaStaff.getParentArea();
						lstPrecinct = areaMgr.getListAreaByType(areaTmp.getId(),
								AreaType.WARD.getValue(), ActiveType.RUNNING.getValue());
						districtCode = areaTmp.getAreaCode();
					}
				}
				return LIST;
			}
			staff = staffMgr.getStaffById(staffId);
			if(staff==null){
				return LIST;
			}
			Staff userShop = getStaffByCurrentUser();
			//if(userShop==null || !checkAncestor(staff.getShop().getShopCode())){
			if(userShop==null || !checkShopPermission(staff.getShop().getShopCode())){
				return PAGE_NOT_PERMISSION;
			}				
			status = staff.getStatus().getValue();
			if(staff.getStaffType() != null){
				if(StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType())){
					supervisorStaffValue = 1;
				}else{
					supervisorStaffValue = 0;
				}
			}
			if (staff.getShop() != null) {
				shopCode = staff.getShop().getShopCode();
				shopName = staff.getShop().getShopName();
			}
			
			if (staff.getStaffType() != null) {
				staffType = staff.getStaffType().getId();
				if (StaffObjectType.NVGH.getValue().equals(staff.getStaffType().getObjectType())) {
					staffObjectType = 4;
				} else if (StaffObjectType.NVTT.getValue().equals(staff.getStaffType().getObjectType())) {
					staffObjectType = 3;
				}
				
				if(StaffObjectType.NVBH.getValue().equals(staff.getStaffType().getObjectType()) || StaffObjectType.NVVS.getValue().equals(staff.getStaffType().getObjectType())){
					isSaleStaff = true;
				}
			}
			if (staff.getSaleTypeCode() != null) {
				saleTypeCode = staff.getSaleTypeCode();
			}
			if (staff.getWorkStateCode() != null) {
				workStateCode = staff.getWorkStateCode();
			}
			Area area = staff.getArea();
			Area areaTmp;
			if(area != null){
				areaId = area.getId();
				provinceCode = staff.getArea().getProvince();
				districtCode = staff.getArea().getDistrict();
				wardCode = staff.getArea().getAreaCode();
				if (area.getParentArea() != null && area.getParentArea().getParentArea() != null) {
					areaTmp = area.getParentArea().getParentArea();
					lstDistrict = areaMgr.getListAreaByType(areaTmp.getId(),
							AreaType.DISTRICT.getValue(), ActiveType.RUNNING.getValue());
					provinceCode = areaTmp.getAreaCode();
				}
				if (area.getParentArea() != null ) {
					areaTmp = area.getParentArea();
					lstPrecinct = areaMgr.getListAreaByType(areaTmp.getId(),
							AreaType.WARD.getValue(), ActiveType.RUNNING.getValue());
					districtCode = areaTmp.getAreaCode();
				}
			}
			filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.STAFF);							
	    	filter.setStatus(ActiveType.RUNNING);
	    	lstStaffTypeTmp = channelTypeMgr.getListChannelType(filter,null); 
	    	if(lstStaffTypeTmp!=null){
	    		lstStaffType = lstStaffTypeTmp.getLstObject();
	    	}else{
	    		lstStaffType = new ArrayList<ChannelType>();
	    	}	    		
	    	ObjectVO<ProductInfo> lstProductInfoTmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT,false);
			if(lstProductInfoTmp!=null){
				lstProductInfo = lstProductInfoTmp.getLstObject();
			}else{
				lstProductInfo = new ArrayList<ProductInfo>();
			}
			
		}catch(Exception ex){
			LogUtility.logError(ex, "StaffCatalogAction.viewDetail()" + ex.getMessage());
		}*/
		return LIST;		
		
	}

	/**
	 * Load staff info.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String loadStaffInfo() {  // load thong tin nhan vien
		/*try {
			if (staffId != null && staffId != 0) {
				staff = staffMgr.getStaffById(staffId);
				if (staff.getShop() != null) {
					shopCode = staff.getShop().getShopCode();
					shopName = staff.getShop().getShopName();
				}
				if (staff.getStaffType() != null) {
					staffType = staff.getStaffType().getId();
					if(StaffObjectType.NVBH.getValue().equals(staff.getStaffType().getObjectType()) || StaffObjectType.NVVS.getValue().equals(staff.getStaffType().getObjectType())){
						isSaleStaff = true;
					}
				}
				if (staff.getSaleTypeCode() != null) {
					saleTypeCode = staff.getSaleTypeCode();
				}
				
			}
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.STAFF);							
	    	filter.setStatus(ActiveType.RUNNING);
			ObjectVO<ChannelType> lstStaffTypeTmp = channelTypeMgr.getListChannelType(filter,null);
			lstStaffType = lstStaffTypeTmp.getLstObject();
			lstSaleType = apParamMgr.getListApParam(ApParamType.STAFF_SALE_TYPE, ActiveType.RUNNING);
			
			lstsaleGroup = apParamMgr.getListApParam(ApParamType.STAFF_SALE_GROUP, ActiveType.RUNNING);
			//CuongND-------------------------
			loadAttributeData(staff,TableHasAttribute.STAFF); //return    lstAttribute
			// --------------------------
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.loadStaffInfo()" + e.getMessage());
		}*/
		return LIST;
	}
	
	/**
	 * Load staff supervisor.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 16, 2012
	 */
	public String loadStaffSupervisor() {
		try {
			if (staffId != null && staffId != 0) {
				staff = staffMgr.getStaffById(staffId);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.loadStaffSupervisor()" + e.getMessage());
		}
		return LIST;
	}

	/**
	 * Load staff category.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String loadStaffCategory() {
		try {
			if (staffId != null && staffId != 0) {
				staff = staffMgr.getStaffById(staffId);
				if (staff.getShop() != null) {
					shopCode = staff.getShop().getShopCode();
					shopName = staff.getShop().getShopName();
				}
				/*ObjectVO<ProductInfo> lstCategoryTmp = productInfoMgr
						.getListProductInfo(null, null, null, null, ActiveType.RUNNING,
								ProductType.CAT,true);
				lstCategory = lstCategoryTmp.getLstObject();*/
				ObjectVO<StaffSaleCat> lstStaffSaleCatTmp = staffMgr.getListStaffSaleCat(null, staffId);
				lstStaffSaleCat = lstStaffSaleCatTmp.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.loadStaffCategory()" + e.getMessage());
		}
		return LIST;
	}
	
	public boolean checkEqualCategory(Long productInfoId, List<StaffSaleCat> lsStaffSaleCate) {
		for (StaffSaleCat staffSaleCat : lsStaffSaleCate) {
			if(staffSaleCat.getCat() != null && staffSaleCat.getCat().getId().equals(productInfoId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Load staff changed.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String loadStaffChanged() {
		try {
			if (staffId != null && staffId != 0) {
				staff = staffMgr.getStaffById(staffId);
				if (staff.getShop() != null) {
					shopCode = staff.getShop().getShopCode();
					shopName = staff.getShop().getShopName();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.loadStaffChanged()" + e.getMessage());
		}
		return LIST;
	}

	/**
	 * Load staff history.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String loadStaffHistory() {
		try {
			if (staffId != null && staffId != 0) {
				staff = staffMgr.getStaffById(staffId);
				if (staff.getShop() != null) {
					shopCode = staff.getShop().getShopCode();
					shopName = staff.getShop().getShopName();
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "StaffCatalogAction.loadStaffHistory()" +e.getMessage());
		}
		return LIST;
	}
	
	/**
	 * Import excel file.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String importExcelFile(){
		if(excelType == 1){
			return importExcelStaffInfo();
		}
		return importExcelStaffCategory();
	}
	
	/**
	 * Import excel staff info.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String importExcelStaffInfo() {
		return SUCCESS;
	}
	
	/**
	 * Import excel staff category.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String importExcelStaffCategory() {
		isError = true;
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = true;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,2);		
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				for(int i=0;i<lstData.size();i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
						StaffSaleCat temp = new StaffSaleCat();
						List<String> row = lstData.get(i);
						
						// Mã NV
						if (row.size() >0) {
							if(!StringUtil.isNullOrEmpty(row.get(0))){
								Staff staff = staffMgr.getStaffByCode(row.get(0));
								if(staff != null){
									if(ActiveType.RUNNING.equals(staff.getStatus())){
										temp.setStaff(staff);
										// Mã ngành hàng
										if (row.size() >1) {
											if(!StringUtil.isNullOrEmpty((row.get(1)))){
												ProductInfo productInfo = productInfoMgr.getProductInfoByCode(row.get(1), ProductType.CAT,null,null);
												if(productInfo != null){
													if(ActiveType.RUNNING.equals(productInfo.getStatus())){
														if(!staffMgr.checkIfRecordExist(staff.getId(), productInfo.getId(), null)){
															temp.setCat(productInfo);
														}else{
															message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.exist",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.category.code"));
															message += "\n";
														}
													}else{
														message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.in.active",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.category.code"));
														message += "\n";
													}
												}else{
													message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.code.not.exist",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.category.code"));
													message += "\n";
												}
											}else{
												message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.category.code"));
												message += "\n";
											}
										}
									}else{
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.status.in.active",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.staff.code"));
										message += "\n";
									}
								}else{
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.code.not.exist",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.staff.code"));
									message += "\n";
								}
							}else{
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.required",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.staff.code"));
								message += "\n";
							}
						}
						if(isView == 0){
							if (StringUtil.isNullOrEmpty(message)) {
								try{
									 staffMgr.createStaffSaleCat(temp,getLogInfoVO());
								 }catch (Exception e) {
									 LogUtility.logError(e,e.getMessage());
								 }
							} else {
								lstFails.add(StringUtil.addFailBean(row,message));
							}
							typeView = false;
						} else{
							if(lstView.size()<100){
								if(StringUtil.isNullOrEmpty(message)){
						            message = "OK";
						        } else{
						        	message = StringUtil.convertHTMLBreakLine(message);
						        }
								lstView.add(StringUtil.addFailBean(row, message));
							}
							typeView = true;
						}
					}
				}				
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_STAFF_CATEGORY_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, "StaffCatalogAction.importExcelStaffCategory()"+ e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	public String changeStaff(){
		resetToken(result);	
		try{
			Shop curShop = null;
			if(session.getAttribute(ConstantManager.SESSION_SHOP)!= null){
				curShop = (Shop)session.getAttribute(ConstantManager.SESSION_SHOP);
			}
			
			staffSignIn = getStaffByCurrentUser();
			if(staffSignIn==null){
				resultJSON(true,ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_AREA));
				return JSON;
			}
			if(areaId == null || areaId <= 0){
				resultJSON(true,ValidateUtil.getErrorMsg(ConstantManager.ERR_REQUIRE, Configuration.getResourceString("vi", "catalog.area.tree")));
				return JSON;
			}
			area = areaMgr.getAreaById(areaId);
			if(area==null){
				return JSON;
			} else if(area != null) {
				if (!area.getType().equals(AreaType.WARD)) {
					result.put(ERROR, true);
					errMsg= Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.area.tree.not.ward",Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.area.tree"));
				    result.put("errMsg", errMsg);	    
				    return JSON;
				}
			}			
			staff = staffMgr.getStaffByCodeAndShopId1(staffCode, curShop.getId());
			if (staffId != null && staffId > 0) {
				if(staff==null){
					return JSON;
				}
				boolean flag = Update();
				if(!flag){
					return JSON;
				}
			}else if (staff != null) {
				resultJSON(true,ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST,Configuration.getResourceString("vi", "catalog.staff.code"),staffCode));
				return JSON;				
			} else{				
				boolean flag = Insert(curShop.getId());
				if(!flag){
					return JSON;
				}
			}
			result.put("staffId", staff.getId());
			result.put(ERROR,false);
		}catch(Exception ex){
			LogUtility.logError(ex, "StaffCatalogAction.changeStaff()"+ ex.getMessage());
			resultJSON(true,ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	private void resultJSON(boolean error,String message){
		result.put(ERROR, error);
		result.put("errMsg", message);
	}
	private boolean Update()throws Exception{
		/**Neu la nhan vien ban hang va Presales thi chi cap nhat :
		 * Hinh thuc ban hang, nhom ban hang, thong tin nganh hang.*/
		/*isSaleStaff = false;
		if (staff.getStaffType() != null) {
			staffType = staff.getStaffType().getId();
			if(StaffObjectType.NVBH.getValue().equals(staff.getStaffType().getObjectType()) || StaffObjectType.NVVS.getValue().equals(staff.getStaffType().getObjectType())){
				isSaleStaff = true;
			}
		}		
		if(isSaleStaff){
			if (!StringUtil.isNullOrEmpty(saleGroup)) {
				ApParam apSaleGr = apParamMgr.getApParamById(Long.parseLong(saleGroup));
				if (apSaleGr!=null){
					staff.setSaleGroup(apSaleGr.getApParamCode());
				}
			}
			if (!StringUtil.isNullOrEmpty(saleTypeCode)) {
				ApParam apSaleType = apParamMgr.getApParamById(Long.parseLong(saleTypeCode));
				if (apSaleType!=null){
					staff.setSaleTypeCode(apSaleType.getApParamCode());
				}
			}
			if (!StringUtil.isNullOrEmpty(workStateCode)) {
				ApParam apWorkStateType = apParamMgr.getApParamById(Long.parseLong(workStateCode));
				if (apWorkStateType!=null){
					staff.setWorkStateCode(apWorkStateType.getApParamCode());
				}
			}
			staffMgr.updateStaff(staff,getLogInfoVO());
			return true;
		}
		staff.setStaffName(staffName);
		staff.setArea(area);
		if (!StringUtil.isNullOrEmpty(idDate)) {
			staff.setIdnoDate(DateUtil.parse(idDate, ConstantManager.FULL_DATE_FORMAT));
		}
		staff.setIdnoPlace(idPlace);
		if(!StringUtil.isNullOrEmpty(staffPhone) && staffMgr.checkIfStaffExists(null, staffPhone, null, staff.getId())){
			resultJSON(true,ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST,Configuration.getResourceString("vi", "catalog.mobilephone")));			    
			return false;
		}
		staff.setMobilephone(staffPhone);
		if(!StringUtil.isNullOrEmpty(email) && staffMgr.checkIfStaffExists(email, null, null, staff.getId())){
			resultJSON(true, ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST,Configuration.getResourceString("vi", "catalog.email.code")));			    
			return false;
		}
		staff.setEmail(email);														
		staff.setStreet(street);
		if (!StringUtil.isNullOrEmpty(saleGroup)) {
			ApParam apSaleGr = apParamMgr.getApParamById(Long.parseLong(saleGroup));
			if (apSaleGr!=null){
				staff.setSaleGroup(apSaleGr.getApParamCode());
			}
		}
		staff.setAddress(address);
		if(gender != null && gender != -1){
			staff.setGender(GenderType.parseValue(gender));															
		}
		if(!StringUtil.isNullOrEmpty(workStartDate)){
			staff.setStartWorkingDay(DateUtil.parse(workStartDate, ConstantManager.FULL_DATE_FORMAT));
			
		}
		staff.setEducation(education);
		if(!StringUtil.isNullOrEmpty(idCard) && staffMgr.checkIfStaffExists(null, null, idCard, staff.getId())){			
			resultJSON(true,Configuration.getResourceString("vi", "common.exist",Configuration.getResourceString("vi", "catalog.idcard")));
			return false;
		}
		staff.setIdno(idCard);
		if (!StringUtil.isNullOrEmpty(idDate)) {
			staff.setIdnoDate(DateUtil.parse(idDate, DateUtil.DATE_FORMAT_STR));
		}
		staff.setPosition(position);														
		staff.setPhone(staffTelephone);			
		if (staffType != null && staffType != -1) {
			ChannelType staffObjectType = channelTypeMgr.getChannelTypeById(staffType);																	
			staff.setStaffType(staffObjectType);
		}
		if(ActiveType.parseValue(status)!=null){
			staff.setStatus(ActiveType.parseValue(status));
		}		
		if (!StringUtil.isNullOrEmpty(saleTypeCode)) {
			ApParam apSaleType = apParamMgr.getApParamById(Long.parseLong(saleTypeCode));
			if (apSaleType!=null){
				staff.setSaleTypeCode(apSaleType.getApParamCode());
			}
		}
		
		if (!StringUtil.isNullOrEmpty(workStateCode)) {
			ApParam apWorkStateType = apParamMgr.getApParamById(Long.parseLong(workStateCode));
			if (apWorkStateType!=null){
				staff.setWorkStateCode(apWorkStateType.getApParamCode());
			}
		}
		
		staff.setUpdateUser(staffSignIn.getStaffCode());		
		staffMgr.updateStaff(staff,getLogInfoVO());*/
		return true;
	}
	private boolean Insert(Long shopId) throws Exception{
		/*Staff newStaff = new Staff();		
		newStaff.setArea(area);
		newStaff.setShop(staffSignIn.getShop());
		//newStaff.setStaffCode(staffMgr.generateStaffCode());
		newStaff.setStaffCode(staffCode);
		
		Staff staff_tmp = staffMgr.getStaffByCodeAndShopId1(newStaff.getStaffCode(), shopId);
		if(staff_tmp != null){
			if(staffMgr.checkIfRecordExist(staff_tmp.getId(), null, null)){
				resultJSON(true,Configuration.getResourceString("vi", "common.exist",Configuration.getResourceString("vi", "catalog.staff.code")));
				return false;
			}
		}
		newStaff.setStaffName(staffName);								
		if(!StringUtil.isNullOrEmpty(idCard) && staffMgr.checkIfStaffExists(null, null, idCard, null)){			
			resultJSON(true,Configuration.getResourceString("vi", "common.exist",Configuration.getResourceString("vi", "catalog.idcard")));
			return false;
		}
		newStaff.setIdno(idCard);
		if (!StringUtil.isNullOrEmpty(idDate)) {
			newStaff.setIdnoDate(DateUtil.parse(idDate, DateUtil.DATE_FORMAT_STR));
		}
		newStaff.setIdnoPlace(idPlace);
		if(!StringUtil.isNullOrEmpty(staffPhone) && staffMgr.checkIfStaffExists(null, staffPhone, null,null)){			
			resultJSON(true,Configuration.getResourceString("vi", "common.exist",Configuration.getResourceString("vi", "catalog.mobilephone")));
		    return false;
		}
		newStaff.setMobilephone(staffPhone);
		if(!StringUtil.isNullOrEmpty(email) && staffMgr.checkIfStaffExists(email, null, null, null)){			
			resultJSON(true,Configuration.getResourceString("vi", "common.exist",Configuration.getResourceString("vi", "catalog.email.code")));
			return false;
		}
		newStaff.setEmail(email);														
		newStaff.setStreet(street);
		if (!StringUtil.isNullOrEmpty(saleGroup)) {
			ApParam apSaleGroup = apParamMgr.getApParamById(Long.parseLong(saleGroup));
			if (apSaleGroup!=null){
				newStaff.setSaleGroup(apSaleGroup.getApParamCode());
			}
		}
		newStaff.setAddress(address);
		newStaff.setGender(GenderType.parseValue(gender));
		if(!StringUtil.isNullOrEmpty(workStartDate)){
			newStaff.setStartWorkingDay(DateUtil.parse(workStartDate, DateUtil.DATE_FORMAT_STR));			
		}
		newStaff.setEducation(education);														
		newStaff.setPosition(position);														
		newStaff.setPhone(staffTelephone);
		if (staffType != null && staffType != -1) {
			ChannelType apStaffType = channelTypeMgr.getChannelTypeById(staffType);
			newStaff.setStaffType(apStaffType);															
		}
		if(status != null && status != ConstantManager.NOT_STATUS){
			newStaff.setStatus(ActiveType.parseValue(status));
		}
		if (!StringUtil.isNullOrEmpty(saleTypeCode)) {
			ApParam apSaleType = apParamMgr.getApParamById(Long.parseLong(saleTypeCode));
			if (apSaleType!=null){
				staff.setSaleTypeCode(apSaleType.getApParamCode());
			}
		}
		newStaff.setCreateUser(staffSignIn.getStaffCode());	
		staff = staffMgr.createStaff(newStaff,getLogInfoVO());	*/	
		return true;
	}

	/**
	 * Change staff info.
	 *
	 * @return the string
	 * @author lamnh,tientv
	 * @see search keyword //CRM Update
	 * @since Oct 1, 2012
	 */
	public String changeStaffInfo() {
	    return JSON;
	}
	
	public Integer getSaleType() {
		return saleType;
	}

	public void setSaleType(Integer saleType) {
		this.saleType = saleType;
	}

	public String getSaleGroup() {
		return saleGroup;
	}

	public void setSaleGroup(String saleGroup) {
		this.saleGroup = saleGroup;
	}

	/**
	 * Change staff supervisor.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 16, 2012
	 */
	public String changeStaffSupervisor(){
		
/*		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			if (staffId != null && staffId > 0) {
				try {
					staff = staffMgr.getStaffById(staffId);
					if (staff != null) {
						if (lstStaffId != null && lstStaffId.size() > 0) {
							for(int i=0;i<lstStaffId.size();i++){
								Staff temp = staffMgr.getStaffById(lstStaffId.get(i));
								if(temp != null && temp.getStaffType() != null && StaffObjectType.NVBH.getValue().equals(temp.getStaffType().getObjectType())){
									temp.setStaffOwner(staff);
									staffMgr.updateStaff(temp, getLogInfoVO());
								}
								error = false;
							}
						}
					}
				} catch (Exception e) {
					LogUtility.logError(e, "StaffCatalogAction.changeStaffSupervisor()"+ e.getMessage());
				}
			}
		}
		result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);*/	    
	    return JSON;
	}

	/**
	 * Change staff category.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String changeStaffCategory() {
		
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			if (staffId != null && staffId > 0) {
				try {
					staff = staffMgr.getStaffById(staffId);
					if (staff != null) {
						if (lstDeleteCategoryId != null && lstDeleteCategoryId.size() > 0) {
							for(int i=0;i<lstDeleteCategoryId.size();i++){
								StaffSaleCat ssc = staffMgr.getStaffSaleCat(staffId, lstDeleteCategoryId.get(i));
								staffMgr.deleteStaffSaleCat(ssc,getLogInfoVO());
								error = false;
							}
						}
						if (lstCategoryId != null && lstCategoryId.size() > 0) {
							error = true;
							for(int i=0;i<lstCategoryId.size();i++){
								productInfo = productInfoMgr.getProductInfoById(lstCategoryId.get(i));
								StaffSaleCat ssc = new StaffSaleCat();
								ssc.setCat(productInfo);
								ssc.setStaff(staff);
								staffMgr.createStaffSaleCat(ssc,getLogInfoVO());
								error = false;
								
							}
						}else{
							error = false;
						}
					}
				} catch (Exception e) {
					LogUtility.logError(e, "StaffCatalogAction.changeStaffCategory()"+ e.getMessage());
				}
			}
		}
		result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}

	/**
	 * Delete category.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String deleteCategory() {
		
		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				if (categoryId != null && categoryId != 0) {
					StaffSaleCat ssc = staffMgr.getStaffSaleCatById(categoryId);
					if (ssc != null) {
						staffMgr.deleteStaffSaleCat(ssc,getLogInfoVO());
						error = false;
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, "StaffCatalogAction.deleteCategory()"+ e.getMessage());
			}
		}
		result.put(ERROR, error);
		return JSON;
	}
	
	/**
	 * Search staff dialog.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 16, 2012
	 */
	public String searchStaffDialog(){
		
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<Staff> kPaging = new KPaging<Staff>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			Long shopId = null;
//			if(StringUtil.isNullOrEmpty(shopCode) && currentUser!= null) {
//				Staff staff = staffMgr.getStaffByCode(currentUser.getUserName());	
//				if(staff != null && staff.getShop() != null) {
//					shopId = staff.getShop().getId();
//				}
//			} else {
//				Shop shop = shopMgr.getShopByCode(shopCode);
//				if(shop != null) {
//					shopId = shop.getId();
//				}
//			}
			if(staffId != null && staffId !=0){
				Staff tmp = staffMgr.getStaffById(staffId);
				if(tmp != null && tmp.getShop() != null){
					shopId = tmp.getShop().getId();
				}
			}
//			ObjectVO<Staff> lstStaff = staffMgr.getListStaff(kPaging,staffCode,null,staffName,null,ActiveType.RUNNING,shopCode,null,staffId);
//			ObjectVO<Staff> lstStaff = staffMgr.getListStaffNotManagedByOwnerId(kPaging, staffId, shopCode, shopName, staffCode, staffName);
			ObjectVO<Staff> lstStaff = staffMgr.getListStaffNotManagedByOwnerId(kPaging, staffId, shopCode, shopName, staffCode, staffName, shopId);
	    	if(lstStaff!= null){
		    	result.put("total", lstStaff.getkPaging().getTotalRows());
		    	result.put("rows", lstStaff.getLstObject());		
	    	}		
	    }catch (Exception e) {
	    	LogUtility.logError(e, "StaffCatalogAction.searchStaffDialog()"+ e.getMessage());
	    }
	    return JSON;
	}
	
	/**
	 * Delete supervisor.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 16, 2012
	 */
	public String deleteSupervisor() {
		
		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				if (staffId != null && staffId != 0) {
					Staff staff = staffMgr.getStaffById(staffId);
					if (staff != null) {
						staff.setStaffOwner(null);
						staffMgr.updateStaff(staff, getLogInfoVO());
						error = false;
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, "StaffCatalogAction.deleteSupervisor()"+ e.getMessage());
			}
		}
		result.put(ERROR, error);
		return JSON;
	}
	
	/**
	 * Check exist superivsor.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 27, 2012
	 */
	public String checkExistSupervisor(){
	    return JSON;
	}
	public String exportExcel()
	{
		return JSON;
	}
	public void setListBeans(List<CellBean> listBeans) {
		this.listBeans = listBeans;
	}

	public List<CellBean> getListBeans() {
		return listBeans;
	}
	private List<CellBean> listBeans;
	private List<Attribute> lstAtt;
	public void setLstAtt(List<Attribute> lstAtt) {
		this.lstAtt = lstAtt;
	}

	public List<Attribute> getLstAtt() {
		return lstAtt;
	}

	public void setLstsaleGroup(List<ApParam> lstsaleGroup) {
		this.lstsaleGroup = lstsaleGroup;
	}

	public List<ApParam> getLstsaleGroup() {
		return lstsaleGroup;
	}
	private List<Long> lstCatId;
	private List<Long> lstDelete;
	public List<Long> getLstCatId() {
		return lstCatId;
	}
	public void setLstCatId(List<Long> lstCatId) {
		this.lstCatId = lstCatId;
	}
	
	

	public List<Long> getLstDelete() {
		return lstDelete;
	}

	public void setLstDelete(List<Long> lstDelete) {
		this.lstDelete = lstDelete;
	}

	public String getListSaleCatByStaffId(){		
		try {
			ObjectVO<StaffSaleCat> temp = staffMgr.getListStaffSaleCat(null, staffId);
			if (temp!=null){
				lstStaffSaleCat = temp.getLstObject();
				result.put("lst", lstStaffSaleCat);
			}
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	public String saveStaffSaleCat(){
		resetToken(result);
		try {
				if(lstDelete!=null){
					for(int i=0;i<lstDelete.size();i++){
						StaffSaleCat obj = staffMgr.getStaffSaleCat(staffId, lstDelete.get(i));
						staffMgr.deleteStaffSaleCat(obj,getLogInfoVO());
					}
				}
				if(lstCatId!=null){
					for(int j=0;j<lstCatId.size();j++){
						StaffSaleCat obj = new StaffSaleCat();//staffMgr.getStaffSaleCat(staffId, lstCatId.get(j));
						Staff idStaff = staffMgr.getStaffById(staffId);
						obj.setStaff(idStaff);
						ProductInfo idCat = productInfoMgr.getProductInfoById(lstCatId.get(j));
						obj.setCat(idCat);
						if(obj!=null){
							staffMgr.createStaffSaleCat(obj, getLogInfoVO());
						}
					}
				}
				
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
}

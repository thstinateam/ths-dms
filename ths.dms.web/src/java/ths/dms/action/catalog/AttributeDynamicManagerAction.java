package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.AttributeMgr;
import ths.dms.core.business.CustomerAttributeMgr;
import ths.dms.core.business.ProductAttributeMgr;
import ths.dms.core.business.StaffAttributeMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.CustomerAttributeDetail;
import ths.dms.core.entities.CustomerAttributeEnum;
import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.ProductAttributeEnum;
import ths.dms.core.entities.StaffAttributeDetail;
import ths.dms.core.entities.StaffAttributeEnum;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;


// TODO: Auto-generated Javadoc
/**
 * The Class AttributeDynamicManagerAction.
 * @description: Quan ly thuoc tinh dong.
 * @author liemtpt
 * @since 21/01/2015
 */
public class AttributeDynamicManagerAction extends AbstractAction{


	private static final long serialVersionUID = 1L;
	/** The Constant CUSTOMER_OBJECT. */
	private static final int CUSTOMER_OBJECT = 1;
	/** The Constant PRODUCT_OBJECT. */
	private static final int PRODUCT_OBJECT = 2;

	/** The Constant STAFF_OBJECT. */
	private static final int STAFF_OBJECT = 3;

	/** The attribute code. */
	private String attributeCode;

	/** The attribute name. */
	private String attributeName;

	/** The attribute id. */
	private Long attributeId;

	/** The value type. */
	private Integer valueType;

	/** The status. */
	private Integer status;

	/** The data length. */
	private String dataLength;

	/** The max value. */
	private String maxValue;

	/** The min value. */
	private String minValue;


	/** The apply object. */
	private Integer applyObject;

	/** The enum id. */
	private Long enumId;
	/** The attribute dynamic vo. */
	private AttributeDynamicVO attributeDynamicVO;
	/** The attribute enum vo. */
	private AttributeEnumVO attributeEnumVO;

	/** The attribute mgr. */
	private AttributeMgr attributeMgr;

	/** The customer attribute mgr. */
	private CustomerAttributeMgr customerAttributeMgr;

	/** The product attribute mgr. */
	private ProductAttributeMgr productAttributeMgr;

	/** The staff attribute mgr. */
	private StaffAttributeMgr staffAttributeMgr;

	/** The apparam mgr**/
	private ApParamMgr apParamMgr;

	/** The lst attribute customer detail. */
	private List<AttributeDetailVO> lstAttributeCustomerDetail;

	/** The lst attribute ap param. */
	private List<ApParam> lstAttributeApParam;

	/** list dynamic attribute. */
	private List<AttributeEnumVO> lstAttributteDetailDynamic;


	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		attributeMgr = (AttributeMgr)context.getBean("attributeMgr");
		productAttributeMgr = (ProductAttributeMgr)context.getBean("productAttributeMgr");
		staffAttributeMgr = (StaffAttributeMgr)context.getBean("staffAttributeMgr");
		customerAttributeMgr = (CustomerAttributeMgr)context.getBean("customerAttributeMgr");
		apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
	    super.prepare();
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {
		resetToken(result);
		try{
			lstAttributeApParam = apParamMgr.getListApParam(ApParamType.DMS_CORE_ATTR, ActiveType.RUNNING);
		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeDynamicManagerAction.execute()"), createLogErrorStandard(startLogDate));
		}
		return SUCCESS;
	}

	/**
	 * search.
	 *
	 * @return the string
	 * @author liemtpt
	 * @description: load danh sach va tim kiem thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	public String search() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<AttributeDynamicVO> kPaging = new KPaging<AttributeDynamicVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Integer CHOOSE_ALL = -1;
			AttributeDynamicFilter filter = new AttributeDynamicFilter();
			filter.setAttributeCode(attributeCode);
			filter.setAttributeName(attributeName);
			filter.setType(valueType);
			filter.setApplyObject(applyObject);
			if(status!=null && !CHOOSE_ALL.equals(status)){
				filter.setStatus(status);
			}
			/**Sort grid*/
			filter.setOrder(order);
			filter.setSort(sort);
			ObjectVO<AttributeDynamicVO> lstAttribute = attributeMgr.getListAttributeDynamicVO(filter, kPaging);
			if (lstAttribute != null) {
				result.put("total", lstAttribute.getkPaging().getTotalRows());
				result.put("rows", lstAttribute.getLstObject());
			}else {
				result.put("total",0);
				result.put("rows", new ArrayList<AttributeDynamicVO>());
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeDynamicManagerAction.search()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * load attribute detail.
	 *
	 * @return the string
	 * @author liemtpt
	 * @description: load danh sach gia tri thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	public String loadAttributeDetail(){
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<AttributeEnumVO> kPaging = new KPaging<AttributeEnumVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			AttributeEnumFilter filter = new AttributeEnumFilter();
			filter.setAttributeId(attributeId);
			filter.setApplyObject(applyObject);
			/**Sort grid*/
			filter.setOrder(order);
			filter.setSort(sort);
			ObjectVO<AttributeEnumVO> lstAttrDetail= attributeMgr.getListAttributeDetailDynamicVO(filter, kPaging);
			if (attributeId != null && attributeId > 0){
				lstAttributteDetailDynamic = lstAttrDetail.getLstObject();
				result.put("total", lstAttrDetail.getkPaging().getTotalRows());
				result.put("rows", lstAttributteDetailDynamic);

			}else{
				result.put("total",0);
				result.put("rows", new ArrayList<AttributeEnumVO>());
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e,e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeDynamicManagerAction.loadAttributeDetail()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return JSON;
	}

	/**
	 * saveAttribute: Them moi hay cap nhat doi tuong.
	 *
	 * @return the string
	 * @author liemtpt
	 * @description: tao moi hay cap nhat thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	public String saveAttribute() {
		try {
			result.put(ERROR, true);
			staff = getStaffByCurrentUser();
			if (staff == null ) {
				return JSON;
			}
			if(attributeDynamicVO == null){
				return JSON;
			}
			Long defaultAttrId = 0l;
			AttributeDynamicFilter filter = new AttributeDynamicFilter();
			Integer ObjectTmp = attributeDynamicVO.getApplyObject();
			if( ObjectTmp !=null){
				filter.setApplyObject(ObjectTmp);
				if(attributeDynamicVO.getAttributeId()!=null && attributeDynamicVO.getAttributeId().equals(defaultAttrId)){
					filter.setAttributeCode(attributeDynamicVO.getAttributeCode());
					Boolean flagCheck = attributeMgr.checkExistAttributeDynamic(filter);
					if(flagCheck){
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST,false, "attribute.code");
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
				}
				if(ObjectTmp.equals(CUSTOMER_OBJECT)){
					/*if(attributeDynamicVO.getStatus()!=null && ActiveType.STOPPED.getValue().equals(attributeDynamicVO.getStatus())){
						// Neu cap nhat trang thai tu Hoat đong sang Tam ngung thi check validate
						if(attributeDynamicVO.getAttributeId()!=null){
							Boolean flagCheckPromotion = customerAttributeMgr.checkExistPromotion(attributeDynamicVO.getAttributeId());
							if(flagCheckPromotion){
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"attribute.exits.promotion.is.running");
								result.put(ERROR, true);
								result.put("errMsg", errMsg);
								return JSON;
							}
						}
					}*/
					customerAttributeMgr.saveOrUpdateCustomerAttribute(attributeDynamicVO, getLogInfoVO());
				}else if(ObjectTmp.equals(PRODUCT_OBJECT)){
					productAttributeMgr.saveOrUpdateProductAttribute(attributeDynamicVO, getLogInfoVO());
				}else if(ObjectTmp.equals(STAFF_OBJECT)){
					staffAttributeMgr.saveOrUpdateStaffAttribute(attributeDynamicVO, getLogInfoVO());
				}
			}
			result.put(ERROR, false);
		} catch (BusinessException e) {
//			LogUtility.logError(ex, "AttributeDynamicManagerAction.saveAttribute() - " + ex.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeDynamicManagerAction.saveAttribute()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * save attribute detail.
	 *
	 * @return the string
	 * @author liemtpt
	 * @description: tao moi hay cap nhat gia tri thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	public String saveAttributeDetail(){
		try {
			result.put(ERROR, true);
			staff = getStaffByCurrentUser();
			if (staff == null ) {
				return JSON;
			}
			if(attributeEnumVO == null){
				return JSON;
			}
			Long defaultAttrDetailId = 0l;
			AttributeEnumFilter filter = new AttributeEnumFilter();
			Integer ObjectTmp = attributeEnumVO.getApplyObject();
			if( ObjectTmp !=null){
				filter.setApplyObject(ObjectTmp);
				if(attributeEnumVO.getEnumId()!=null && attributeEnumVO.getEnumId().equals(defaultAttrDetailId)){
					filter.setCode(attributeEnumVO.getEnumCode());
					Boolean flagCheck = attributeMgr.checkExistAttributeEnumDynamic(filter);
					if(flagCheck){
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST,false, "attribute.enum.code");
						result.put(ERROR, true);
						result.put("errMsg", errMsg);
						return JSON;
					}
				}
				if(ObjectTmp.equals(CUSTOMER_OBJECT)){
					customerAttributeMgr.saveOrUpdateCustomerAttributeEnum(attributeEnumVO, getLogInfoVO());
				}else if(ObjectTmp.equals(PRODUCT_OBJECT)){
					productAttributeMgr.saveOrUpdateProductAttributeEnum(attributeEnumVO, getLogInfoVO());
				}else if(ObjectTmp.equals(STAFF_OBJECT)){
					staffAttributeMgr.saveOrUpdateStaffAttributeEnum(attributeEnumVO, getLogInfoVO());
				}
			}
			result.put(ERROR, false);
		} catch (BusinessException e) {
//			LogUtility.logError(ex, "AttributeDynamicManagerAction.saveAttribute() - " + ex.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeDynamicManagerAction.saveAttributeDetail()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * get list attribute detail by enum id.
	 *
	 * @return the list attribute detail by enum id
	 * @author liemtpt
	 * @description: lay danh sach xem enumId co ton tai trong bang XXX_atribute_detail khong (XXX_: customer or staff or product)
	 * @createDate 22/01/2015
	 */
	public String getListAttributeDetailByEnumId() {
		try {
			if(applyObject!=null){
				if(enumId!=null){
					if(applyObject.equals(CUSTOMER_OBJECT)){
						List<CustomerAttributeDetail> lst = customerAttributeMgr.getListCustomerAttributeByEnumId(enumId);
						if (lst != null) {
							result.put("lstAttDetail", lst);
						}else {
							result.put("rows", new ArrayList<CustomerAttributeDetail>());
						}
					}else if(applyObject.equals(PRODUCT_OBJECT)){
						List<ProductAttributeDetail> lst = productAttributeMgr.getListProductAttributeByEnumId(enumId);
						if (lst != null) {
							result.put("lstAttDetail", lst);
						}else {
							result.put("rows", new ArrayList<ProductAttributeDetail>());
						}
					}else if(applyObject.equals(STAFF_OBJECT)){
						List<StaffAttributeDetail> lst = staffAttributeMgr.getListStaffAttributeByEnumId(enumId);
						if (lst != null) {
							result.put("lstAttDetail", lst);
						}else {
							result.put("rows", new ArrayList<StaffAttributeDetail>());
						}
					}
				}
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeDynamicManagerAction.getListAttributeDetailByEnumId()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}

	/**
	 * delete attribute detail: Xoa gia tri.
	 *
	 * @return the string
	 * @createDate 22/01/2015
	 * @author liemtpt
	 */
	public String deleteAttributeDetail(){
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
				if(applyObject != null){
					if(enumId!=null){
						if(applyObject.equals(CUSTOMER_OBJECT)){
							List<CustomerAttributeDetail> lst = customerAttributeMgr.getListCustomerAttributeByEnumId(enumId);
							if(lst!=null && lst.size()>0){
								for(CustomerAttributeDetail attr : lst){
									attr.setStatus(ActiveType.DELETED);
									customerAttributeMgr.updateCustomerAttributeDetail(attr, getLogInfoVO());
								}
							}
							CustomerAttributeEnum att = customerAttributeMgr.getCustomerAttributeEnumById(enumId);
							if(att != null){
								att.setStatus(ActiveType.DELETED);
								customerAttributeMgr.updateCustomerAttributeEnum(att,getLogInfoVO());
								error = false;
							}
						}else if(applyObject.equals(PRODUCT_OBJECT)){
							List<ProductAttributeDetail> lst = productAttributeMgr.getListProductAttributeByEnumId(enumId);
							if(lst!=null && lst.size()>0){
								for(ProductAttributeDetail attr : lst){
									attr.setStatus(ActiveType.DELETED);
									productAttributeMgr.updateProductAttributeDetail(attr, getLogInfoVO());
								}
							}
							ProductAttributeEnum att = productAttributeMgr.getProductAttributeEnumById(enumId);
							if(att != null){
								att.setStatus(ActiveType.DELETED);
								productAttributeMgr.updateProductAttributeEnum(att,getLogInfoVO());
								error = false;
							}
						}else if(applyObject.equals(STAFF_OBJECT)){
							List<StaffAttributeDetail> lst = staffAttributeMgr.getListStaffAttributeByEnumId(enumId);
							if(lst!=null && lst.size()>0){
								for(StaffAttributeDetail attr : lst){
									attr.setStatus(ActiveType.DELETED);
									staffAttributeMgr.updateStaffAttributeDetail(attr, getLogInfoVO());
								}
							}
							StaffAttributeEnum att = staffAttributeMgr.getStaffAttributeEnumById(enumId);
							if(att != null){
								att.setStatus(ActiveType.DELETED);
								staffAttributeMgr.updateStaffAttributeEnum(att,getLogInfoVO());
								error = false;
							}
						}
					}
				}
			} catch (BusinessException e) {
			    LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "vnm.web.action.AttributeDynamicManagerAction.deleteAttributeDetail()"), createLogErrorStandard(actionStartTime));
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);
	    return JSON;
	}

	/**
	 * Gets the lst attribute ap param.
	 *
	 * @return the lst attribute ap param
	 */
	public List<ApParam> getLstAttributeApParam() {
		return lstAttributeApParam;
	}

	/**
	 * Sets the lst attribute ap param.
	 *
	 * @param lstAttributeApParam the new lst attribute ap param
	 */
	public void setLstAttributeApParam(List<ApParam> lstAttributeApParam) {
		this.lstAttributeApParam = lstAttributeApParam;
	}

	/**
	 * Gets the attribute code.
	 *
	 * @return the attribute code
	 */
	public String getAttributeCode() {
		return attributeCode;
	}

	/**
	 * Sets the attribute code.
	 *
	 * @param attributeCode the new attribute code
	 */
	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	/**
	 * Gets the attribute name.
	 *
	 * @return the attribute name
	 */
	public String getAttributeName() {
		return attributeName;
	}

	/**
	 * Sets the attribute name.
	 *
	 * @param attributeName the new attribute name
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	/**
	 * Gets the attribute id.
	 *
	 * @return the attribute id
	 */
	public Long getAttributeId() {
		return attributeId;
	}

	/**
	 * Sets the attribute id.
	 *
	 * @param attributeId the new attribute id
	 */
	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}


	/**
	 * Gets the value type.
	 *
	 * @return the value type
	 */
	public Integer getValueType() {
		return valueType;
	}

	/**
	 * Sets the value type.
	 *
	 * @param valueType the new value type
	 */
	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the apply object.
	 *
	 * @return the apply object
	 */
	public Integer getApplyObject() {
		return applyObject;
	}

	/**
	 * Sets the apply object.
	 *
	 * @param applyObject the new apply object
	 */
	public void setApplyObject(Integer applyObject) {
		this.applyObject = applyObject;
	}

	/**
	 * Gets the lst attributte detail dynamic.
	 *
	 * @return the lst attributte detail dynamic
	 */
	public List<AttributeEnumVO> getLstAttributteDetailDynamic() {
		return lstAttributteDetailDynamic;
	}

	/**
	 * Sets the lst attributte detail dynamic.
	 *
	 * @param lstAttributteDetailDynamic the new lst attributte detail dynamic
	 */
	public void setLstAttributteDetailDynamic(
			List<AttributeEnumVO> lstAttributteDetailDynamic) {
		this.lstAttributteDetailDynamic = lstAttributteDetailDynamic;
	}

	/**
	 * Gets the attribute dynamic vo.
	 *
	 * @return the attribute dynamic vo
	 */
	public AttributeDynamicVO getAttributeDynamicVO() {
		return attributeDynamicVO;
	}

	/**
	 * Sets the attribute dynamic vo.
	 *
	 * @param attributeDynamicVO the new attribute dynamic vo
	 */
	public void setAttributeDynamicVO(AttributeDynamicVO attributeDynamicVO) {
		this.attributeDynamicVO = attributeDynamicVO;
	}

	/**
	 * Gets the lst attribute customer detail.
	 *
	 * @return the lst attribute customer detail
	 */
	public List<AttributeDetailVO> getLstAttributeCustomerDetail() {
		return lstAttributeCustomerDetail;
	}

	/**
	 * Sets the lst attribute customer detail.
	 *
	 * @param lstAttributeCustomerDetail the new lst attribute customer detail
	 */
	public void setLstAttributeCustomerDetail(
			List<AttributeDetailVO> lstAttributeCustomerDetail) {
		this.lstAttributeCustomerDetail = lstAttributeCustomerDetail;
	}

	/**
	 * Gets the attribute enum vo.
	 *
	 * @return the attribute enum vo
	 */
	public AttributeEnumVO getAttributeEnumVO() {
		return attributeEnumVO;
	}

	/**
	 * Sets the attribute enum vo.
	 *
	 * @param attributeEnumVO the new attribute enum vo
	 */
	public void setAttributeEnumVO(AttributeEnumVO attributeEnumVO) {
		this.attributeEnumVO = attributeEnumVO;
	}

	/**
	 * Gets the enum id.
	 *
	 * @return the enum id
	 */
	public Long getEnumId() {
		return enumId;
	}

	/**
	 * Sets the enum id.
	 *
	 * @param enumId the new enum id
	 */
	public void setEnumId(Long enumId) {
		this.enumId = enumId;
	}

	/**
	 * Gets the data length.
	 *
	 * @return the data length
	 */
	public String getDataLength() {
		return dataLength;
	}

	/**
	 * Sets the data length.
	 *
	 * @param dataLength the new data length
	 */
	public void setDataLength(String dataLength) {
		this.dataLength = dataLength;
	}

	/**
	 * Gets the max value.
	 *
	 * @return the max value
	 */
	public String getMaxValue() {
		return maxValue;
	}

	/**
	 * Sets the max value.
	 *
	 * @param maxValue the new max value
	 */
	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * Gets the min value.
	 *
	 * @return the min value
	 */
	public String getMinValue() {
		return minValue;
	}

	/**
	 * Sets the min value.
	 *
	 * @param minValue the new min value
	 */
	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}
}

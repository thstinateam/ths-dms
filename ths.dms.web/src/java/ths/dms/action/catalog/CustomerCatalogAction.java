/*
 * 
 */
package ths.dms.action.catalog;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.AreaMgr;
import ths.dms.core.business.AttributeMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.LogMgr;
import ths.dms.core.business.OrganizationMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.SaleLevelCatMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.SuperviserMgr;
import ths.dms.core.entities.ActionAudit;
import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Area;
import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerCatLevel;
import ths.dms.core.entities.Debit;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.SaleLevelCat;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailFilter;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.IsAttribute;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ParamsDAOGetListCustomer;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.RoutingGeneralFilter;
import ths.dms.core.entities.vo.AreaVO;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.CustomerCatLevelImportVO;
import ths.dms.core.entities.vo.CustomerExportVO;
import ths.dms.core.entities.vo.DuplicatedCustomerVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.memcached.MemcachedUtils;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.StatusBean;
import ths.dms.web.business.common.DynamicAttributeHelper;
import ths.dms.web.business.common.DynamicAttributeValidator;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.VSARole;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * The Class CustomerCatalogAction.
 */
public class CustomerCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2565770666803210401L;

	/** The Constant IMPORT_EXCEL_CUSTOMER. */
	private static final int IMPORT_EXCEL_CUSTOMER = 1;

	/** The Constant IMPORT_EXCEL_CAT. */
	private static final int IMPORT_EXCEL_CAT = 2;

	/** The customer mgr. */
	private CustomerMgr customerMgr;

	/** The area mgr. */
	private AreaMgr areaMgr;

	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;

	/** The sales order mgr. */
	private SaleOrderMgr salesOrderMgr;

	/** The sale level cat mgr. */
	private SaleLevelCatMgr saleLevelCatMgr;

	/** The log mgr. */
	private LogMgr logMgr;

	private SuperviserMgr superviserMgr;
	
	private AttributeMgr attributeMgr;
	
	/** The id. */
	private Long id;

	/** The shop id. */
	private Long shopId;

	/** The shop code. */
	private String shopCode;

	/** shopId hidden for load tree */
	private Long shopIdHidden;

	/** The customer code. */
	private String customerCode;

	/** The customer name. */
	private String customerName;

	/** The province code. */
	private String provinceCode;

	/** The district code. */
	private String districtCode;

	/** The address. */
	private String address;

	/** The street. */
	private String street;

	/** The phone. */
	private String phone;

	/** The mobile phone. */
	private String mobilePhone;

	/** The customer type. */
	private String customerType;

	/** The status. */
	private int status;

	/** The region. */
	private String region;

	/** The loyalty. */
	private String loyalty;

	/** The group transfer. */
	private String deliveryCode;

	/** The cashier id. */
	private Long cashierId;

	/** The lst customer type. */
	private List<ChannelType> lstCustomerType;

	/** The lst shop. */
	private List<Shop> lstRegion;

	/** The lst staff. */
	private List<Staff> lstStaff;
	private List<StaffVO> lstStaffVO;

	/** The excel file. */
	private File excelFile;

	/** The excel file content type. */
	private String excelFileContentType;

	/** The max debit. */
	private BigDecimal maxDebit;

	/** The debit date. */
	private Integer debitDate;

	/** The ward code. */
	private String wardCode;

	/** The contact. */
	private String contact;

	/** The id number. */
	private String idNumber;

	/** The email. */
	private String email;

	/** The shop name. */
	private String shopName;

	/** The province name. */
	private String provinceName;

	/** The district name. */
	private String districtName;

	/** The ward name. */
	private String wardName;

	/** The invoice conpany name. */
	private String invoiceConpanyName;

	/** The invoice outlet name. */
	private String invoiceOutletName;

	/** The cust delivery addr. */
	private String custDeliveryAddr;

	/** The invoice tax. */
	private String invoiceTax;

	/** The payment type. */
	private int paymentType;

	/** The invoice number account. */
	private String invoiceNumberAccount;

	/** The invoice name bank. */
	private String invoiceNameBank;
	
	private String invoiceNameBranchBank;
	
	private String bankAccountOwner;

	/** The lst cat. */
	private List<ProductInfo> lstCat;

	/** The from date. */
	private String fromDate;

	/** The to date. */
	private String toDate;

	/** The sale level cat id. */
	private Long saleLevelCatId;

	/** The sale level cat. */
	private Long saleLevelCat;

	/** The sale level. */
	private String saleLevel;

	/** The excel type. */
	private int excelType;

	/** The house name. */
	private String addressNumber;

	/** The lat. */
	private String lat;

	/** The lng. */
	private String lng;

	/** The from seach date. */
	private String fromSeachDate;

	/** The to seach date. */
	private String toSeachDate;

	/** The shop code like. */
	private String shopCodeLike;

	/** The APPLY_DEBIT_LIMITED. */
	private Integer applyDebitLimited;

	private Long customerId;

	private List<MediaItem> listMediaItem;

	private List<MediaItem> listMediaItemStore;

	private List<MediaItem> listMediaItemDisplay;

	private List<MediaItem> listMediaItemSalePoint;

	private MediaItem mediaItem;

	private MediaItem mediaItemStore;

	private MediaItem mediaItemDisplay;

	private MediaItem mediaItemSalePoint;

	private int objectType;

	private int roleViewImageCustomer;

	private List<ApParam> lstLoyalty;
	private List<ApParam> lstLocation;
	private List<ApParam> lstDisplay;
	private String display;
	private boolean hasEditPermission;
	private List<CellBean> listCell;
	private List<CellBean> listSubcat;
	private List<Long> lstCustomerId;

	private int statusCustomer;
	private BigDecimal maxDebitNow;
	private Long debitDateNow;
	private Long cashier;
	private Long areaId;
	private String cashierCode;
	private String locationCode;
	List<ChannelType> listCustomerType;
	List<Staff> listDeliverStaff;
	List<Staff> listCashierStaff;
	private Date sysDate;

	private boolean createNew;
	private List<String> lstTitle;
	private List<AreaVO> lstProvince;
	private List<AreaVO> lstDistrict;
	private List<AreaVO> lstPrecinct;
	private List<StatusBean> lstStatus;
	
	private List<RoutingVO> listRouting;
	private Long routingId;
	private String birthDayStr;
	private Integer tansuat;
	private String startDateStr;
	private String endDateStr;

	private Integer isVat;

	private String cusShopCode;
	private OrganizationMgr organizationMgr;

	private List<ApParam> salePositionOptions;
	private Integer customerSalePosition;
	
	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}

	public String getCusShopCode() {
		return cusShopCode;
	}

	public void setCusShopCode(String cusShopCode) {
		this.cusShopCode = cusShopCode;
	}

	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

	public Integer getIsVat() {
		return isVat;
	}

	public void setIsVat(Integer isVat) {
		this.isVat = isVat;
	}

	public List<String> getLstTitle() {
		return lstTitle;
	}

	public void setLstTitle(List<String> lstTitle) {
		this.lstTitle = lstTitle;
	}

	public boolean isCreateNew() {
		return createNew;
	}

	public void setCreateNew(boolean createNew) {
		this.createNew = createNew;
	}

	public String getCashierCode() {
		return cashierCode;
	}

	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}

	public List<Staff> getListCashierStaff() {
		return listCashierStaff;
	}

	public void setListCashierStaff(List<Staff> listCashierStaff) {
		this.listCashierStaff = listCashierStaff;
	}

	public List<Staff> getListDeliverStaff() {
		return listDeliverStaff;
	}

	public void setListDeliverStaff(List<Staff> listDeliverStaff) {
		this.listDeliverStaff = listDeliverStaff;
	}

	public List<ChannelType> getListCustomerType() {
		return listCustomerType;
	}

	public void setListCustomerType(List<ChannelType> listCustomerType) {
		this.listCustomerType = listCustomerType;
	}

	public int getStatusCustomer() {
		return statusCustomer;
	}

	public void setStatusCustomer(int statusCustomer) {
		this.statusCustomer = statusCustomer;
	}

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	/**
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the shopId
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * @param shopId
	 *            the shopId to set
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * @param shopCode
	 *            the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @param customerCode
	 *            the customerCode to set
	 */
	public void setCustomerCode(String code) {
		this.customerCode = code;
	}

	/**
	 * @return the customerName
	 */
	public String getName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setName(String name) {
		this.customerName = name;
	}

	/**
	 * @return the provinceCode
	 */
	public String getProvinceCode() {
		return provinceCode;
	}

	/**
	 * @param provinceCode
	 *            the provinceCode to set
	 */
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	 * @return the districtCode
	 */
	public String getDistrictCode() {
		return districtCode;
	}

	/**
	 * @param districtCode
	 *            the districtCode to set
	 */
	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone
	 *            the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the loyalty
	 */
	public String getLoyalty() {
		return loyalty;
	}

	/**
	 * @param loyalty
	 *            the loyalty to set
	 */
	public void setLoyalty(String loyalty) {
		this.loyalty = loyalty;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	/**
	 * @return the cashierId
	 */
	public Long getCashierId() {
		return cashierId;
	}

	/**
	 * @param cashierId
	 *            the cashierId to set
	 */
	public void setCashierId(Long cashierId) {
		this.cashierId = cashierId;
	}

	/**
	 * @return the lstCustomerType
	 */
	public List<ChannelType> getLstCustomerType() {
		return lstCustomerType;
	}

	/**
	 * @param lstCustomerType
	 *            the lstCustomerType to set
	 */
	public void setLstCustomerType(List<ChannelType> lstCustomerType) {
		this.lstCustomerType = lstCustomerType;
	}

	//	/**
	//	 * @return the lstGroupTransfer
	//	 */
	//	public List<GroupTransfer> getLstGroupTransfer() {
	//		return lstGroupTransfer;
	//	}
	//
	//	/**
	//	 * @param lstGroupTransfer the lstGroupTransfer to set
	//	 */
	//	public void setLstGroupTransfer(List<GroupTransfer> lstGroupTransfer) {
	//		this.lstGroupTransfer = lstGroupTransfer;
	//	}

	/**
	 * @return the lstStaff
	 */
	public List<Staff> getLstStaff() {
		return lstStaff;
	}

	/**
	 * @param lstStaff
	 *            the lstStaff to set
	 */
	public void setLstStaff(List<Staff> lstStaff) {
		this.lstStaff = lstStaff;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the lstRegion
	 */
	public List<Shop> getLstRegion() {
		return lstRegion;
	}

	/**
	 * @param lstRegion
	 *            the lstRegion to set
	 */
	public void setLstRegion(List<Shop> lstRegion) {
		this.lstRegion = lstRegion;
	}

	/**
	 * @return the excelFile
	 */
	public File getExcelFile() {
		return excelFile;
	}

	/**
	 * @param excelFile
	 *            the excelFile to set
	 */
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	/**
	 * @return the excelFileContentType
	 */
	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	/**
	 * @param excelFileContentType
	 *            the excelFileContentType to set
	 */
	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	/**
	 * @return the maxDebit
	 */
	public BigDecimal getMaxDebit() {
		return maxDebit;
	}

	/**
	 * @param maxDebit
	 *            the maxDebit to set
	 */
	public void setMaxDebit(BigDecimal maxDebit) {
		this.maxDebit = maxDebit;
	}

	/**
	 * @return the debitDate
	 */
	public Integer getDebitDate() {
		return debitDate;
	}

	/**
	 * @param debitDate
	 *            the debitDate to set
	 */
	public void setDebitDate(Integer debitDate) {
		this.debitDate = debitDate;
	}

	/**
	 * @return the wardCode
	 */
	public String getWardCode() {
		return wardCode;
	}

	/**
	 * @param wardCode
	 *            the wardCode to set
	 */
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	/**
	 * @return the contact
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @return the idNumber
	 */
	public String getIdNumber() {
		return idNumber;
	}

	/**
	 * @param idNumber
	 *            the idNumber to set
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName
	 *            the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the provinceName
	 */
	public String getProvinceName() {
		return provinceName;
	}

	/**
	 * @param provinceName
	 *            the provinceName to set
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	/**
	 * @return the districtName
	 */
	public String getDistrictName() {
		return districtName;
	}

	/**
	 * @param districtName
	 *            the districtName to set
	 */
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	/**
	 * @return the wardName
	 */
	public String getWardName() {
		return wardName;
	}

	/**
	 * @param wardName
	 *            the wardName to set
	 */
	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	/**
	 * @return the invoiceConpanyName
	 */
	public String getInvoiceConpanyName() {
		return invoiceConpanyName;
	}

	/**
	 * @param invoiceConpanyName
	 *            the invoiceConpanyName to set
	 */
	public void setInvoiceConpanyName(String invoiceConpanyName) {
		this.invoiceConpanyName = invoiceConpanyName;
	}

	/**
	 * @return the invoiceOutletName
	 */
	public String getInvoiceOutletName() {
		return invoiceOutletName;
	}

	/**
	 * @param invoiceOutletName
	 *            the invoiceOutletName to set
	 */
	public void setInvoiceOutletName(String invoiceOutletName) {
		this.invoiceOutletName = invoiceOutletName;
	}

	public List<StatusBean> getLstStatus() {
		return lstStatus;
	}

	public void setLstStatus(List<StatusBean> lstStatus) {
		this.lstStatus = lstStatus;
	}

	/**
	 * @return the invoiceTax
	 */
	public String getInvoiceTax() {
		return invoiceTax;
	}

	/**
	 * @param invoiceTax
	 *            the invoiceTax to set
	 */
	public void setInvoiceTax(String invoiceTax) {
		this.invoiceTax = invoiceTax;
	}

	/**
	 * @return the paymentType
	 */
	public int getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            the paymentType to set
	 */
	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the invoiceNumberAccount
	 */
	public String getInvoiceNumberAccount() {
		return invoiceNumberAccount;
	}

	/**
	 * @param invoiceNumberAccount
	 *            the invoiceNumberAccount to set
	 */
	public void setInvoiceNumberAccount(String invoiceNumberAccount) {
		this.invoiceNumberAccount = invoiceNumberAccount;
	}

	/**
	 * @return the invoiceNameBank
	 */
	public String getInvoiceNameBank() {
		return invoiceNameBank;
	}

	/**
	 * @param invoiceNameBank
	 *            the invoiceNameBank to set
	 */
	public void setInvoiceNameBank(String invoiceNameBank) {
		this.invoiceNameBank = invoiceNameBank;
	}

	/**
	 * @return the custDeliveryAddr
	 */
	public String getCustDeliveryAddr() {
		return custDeliveryAddr;
	}

	/**
	 * @param custDeliveryAddr
	 *            the custDeliveryAddr to set
	 */
	public void setCustDeliveryAddr(String custDeliveryAddr) {
		this.custDeliveryAddr = custDeliveryAddr;
	}

	/**
	 * @return the lstCat
	 */
	public List<ProductInfo> getLstCat() {
		return lstCat;
	}

	/**
	 * @param lstCat
	 *            the lstCat to set
	 */
	public void setLstCat(List<ProductInfo> lstCat) {
		this.lstCat = lstCat;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate
	 *            the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate
	 *            the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the saleLevelCatId
	 */
	public Long getSaleLevelCatId() {
		return saleLevelCatId;
	}

	/**
	 * @param saleLevelCatId
	 *            the saleLevelCatId to set
	 */
	public void setSaleLevelCatId(Long saleLevelCatId) {
		this.saleLevelCatId = saleLevelCatId;
	}

	/**
	 * @return the saleLevelCat
	 */
	public Long getSaleLevelCat() {
		return saleLevelCat;
	}

	/**
	 * @param saleLevelCat
	 *            the saleLevelCat to set
	 */
	public void setSaleLevelCat(Long saleLevelCat) {
		this.saleLevelCat = saleLevelCat;
	}

	/**
	 * @return the saleLevel
	 */
	public String getSaleLevel() {
		return saleLevel;
	}

	/**
	 * @param saleLevel
	 *            the saleLevel to set
	 */
	public void setSaleLevel(String saleLevel) {
		this.saleLevel = saleLevel;
	}

	/**
	 * @return the excelType
	 */
	public int getExcelType() {
		return excelType;
	}

	/**
	 * @param excelType
	 *            the excelType to set
	 */
	public void setExcelType(int excelType) {
		this.excelType = excelType;
	}

	/**
	 * @return the lat
	 */
	public String getLat() {
		return lat;
	}

	/**
	 * @param lat
	 *            the lat to set
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}

	/**
	 * @return the lng
	 */
	public String getLng() {
		return lng;
	}

	/**
	 * @param lng
	 *            the lng to set
	 */
	public void setLng(String lng) {
		this.lng = lng;
	}

	/**
	 * @return the houseNumber
	 */
	public String getAddressNumber() {
		return addressNumber;
	}

	/**
	 * @param houseNumber
	 *            the houseNumber to set
	 */
	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	/**
	 * @return the fromSeachDate
	 */
	public String getFromSeachDate() {
		return fromSeachDate;
	}

	/**
	 * @param fromSeachDate
	 *            the fromSeachDate to set
	 */
	public void setFromSeachDate(String fromSeachDate) {
		this.fromSeachDate = fromSeachDate;
	}

	/**
	 * @return the toSeachDate
	 */
	public String getToSeachDate() {
		return toSeachDate;
	}

	/**
	 * @param toSeachDate
	 *            the toSeachDate to set
	 */
	public void setToSeachDate(String toSeachDate) {
		this.toSeachDate = toSeachDate;
	}

	/**
	 * @return the shopCodeLike
	 */
	public String getShopCodeLike() {
		return shopCodeLike;
	}

	/**
	 * @param shopCodeLike
	 *            the shopCodeLike to set
	 */
	public void setShopCodeLike(String shopCodeLike) {
		this.shopCodeLike = shopCodeLike;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public List<MediaItem> getListMediaItemStore() {
		return listMediaItemStore;
	}

	public void setListMediaItemStore(List<MediaItem> listMediaItemStore) {
		this.listMediaItemStore = listMediaItemStore;
	}

	public List<MediaItem> getListMediaItemDisplay() {
		return listMediaItemDisplay;
	}

	public void setListMediaItemDisplay(List<MediaItem> listMediaItemDisplay) {
		this.listMediaItemDisplay = listMediaItemDisplay;
	}

	public List<MediaItem> getListMediaItemSalePoint() {
		return listMediaItemSalePoint;
	}

	public void setListMediaItemSalePoint(List<MediaItem> listMediaItemSalePoint) {
		this.listMediaItemSalePoint = listMediaItemSalePoint;
	}

	public MediaItem getMediaItemStore() {
		return mediaItemStore;
	}

	public void setMediaItemStore(MediaItem mediaItemStore) {
		this.mediaItemStore = mediaItemStore;
	}

	public List<CellBean> getListSubcat() {
		return listSubcat;
	}

	public void setListSubcat(List<CellBean> listSubcat) {
		this.listSubcat = listSubcat;
	}

	public MediaItem getMediaItemDisplay() {
		return mediaItemDisplay;
	}

	public void setMediaItemDisplay(MediaItem mediaItemDisplay) {
		this.mediaItemDisplay = mediaItemDisplay;
	}

	public MediaItem getMediaItemSalePoint() {
		return mediaItemSalePoint;
	}

	public void setMediaItemSalePoint(MediaItem mediaItemSalePoint) {
		this.mediaItemSalePoint = mediaItemSalePoint;
	}

	public List<CellBean> getListCell() {
		return listCell;
	}

	public void setListCell(List<CellBean> listCell) {
		this.listCell = listCell;
	}

	public int getObjectType() {
		return objectType;
	}

	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}

	public List<MediaItem> getListMediaItem() {
		return listMediaItem;
	}

	public void setListMediaItem(List<MediaItem> listMediaItem) {
		this.listMediaItem = listMediaItem;
	}

	public MediaItem getMediaItem() {
		return mediaItem;
	}

	public void setMediaItem(MediaItem mediaItem) {
		this.mediaItem = mediaItem;
	}

	public int getRoleViewImageCustomer() {
		return roleViewImageCustomer;
	}

	public void setRoleViewImageCustomer(int roleViewImageCustomer) {
		this.roleViewImageCustomer = roleViewImageCustomer;
	}

	/**
	 * @return the lstLoyalty
	 */
	public List<ApParam> getLstLoyalty() {
		return lstLoyalty;
	}

	/**
	 * @param lstLoyalty
	 *            the lstLoyalty to set
	 */
	public void setLstLoyalty(List<ApParam> lstLoyalty) {
		this.lstLoyalty = lstLoyalty;
	}

	/**
	 * @return the lstLocation
	 */
	public List<ApParam> getLstLocation() {
		return lstLocation;
	}

	/**
	 * @param lstLocation
	 *            the lstLocation to set
	 */
	public void setLstLocation(List<ApParam> lstLocation) {
		this.lstLocation = lstLocation;
	}

	/**
	 * @return the lstDisplay
	 */
	public List<ApParam> getLstDisplay() {
		return lstDisplay;
	}

	/**
	 * @param lstDisplay
	 *            the lstDisplay to set
	 */
	public void setLstDisplay(List<ApParam> lstDisplay) {
		this.lstDisplay = lstDisplay;
	}

	/**
	 * @return the display
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * @param display
	 *            the display to set
	 */
	public void setDisplay(String display) {
		this.display = display;
	}

	public Integer getApplyDebitLimited() {
		return applyDebitLimited;
	}

	public void setApplyDebitLimited(Integer applyDebitLimited) {
		this.applyDebitLimited = applyDebitLimited;
	}

	public List<Attribute> getLstAttribute() {
		return lstAttribute;
	}

	public void setLstAttribute(List<Attribute> lstAttribute) {
		this.lstAttribute = lstAttribute;
	}

	public List<AttributeDetailVO> getLstAttributeDetail() {
		return lstAttributeDetail;
	}

	public void setLstAttributeDetail(List<AttributeDetailVO> lstAttributeDetail) {
		this.lstAttributeDetail = lstAttributeDetail;
	}

	public BigDecimal getMaxDebitNow() {
		return maxDebitNow;
	}

	public void setMaxDebitNow(BigDecimal maxDebitNow) {
		this.maxDebitNow = maxDebitNow;
	}

	public Long getDebitDateNow() {
		return debitDateNow;
	}

	public void setDebitDateNow(Long debitDateNow) {
		this.debitDateNow = debitDateNow;
	}

	/**
	 * @return the hasEditPermission
	 */
	public boolean isHasEditPermission() {
		return hasEditPermission;
	}

	/**
	 * @param hasEditPermission
	 *            the hasEditPermission to set
	 */
	public void setHasEditPermission(boolean hasEditPermission) {
		this.hasEditPermission = hasEditPermission;
	}

	public Long getCashier() {
		return cashier;
	}

	public void setCashier(Long cashier) {
		this.cashier = cashier;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public Long getAreaId() {
		return areaId;
	}

	public List<AreaVO> getLstProvince() {
		return lstProvince;
	}

	public void setLstProvince(List<AreaVO> lstProvince) {
		this.lstProvince = lstProvince;
	}

	public List<AreaVO> getLstDistrict() {
		return lstDistrict;
	}

	public void setLstDistrict(List<AreaVO> lstDistrict) {
		this.lstDistrict = lstDistrict;
	}

	public List<AreaVO> getLstPrecinct() {
		return lstPrecinct;
	}

	public void setLstPrecinct(List<AreaVO> lstPrecinct) {
		this.lstPrecinct = lstPrecinct;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		super.prepare();
		customerMgr = (CustomerMgr) context.getBean("customerMgr");
		areaMgr = (AreaMgr) context.getBean("areaMgr");
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		salesOrderMgr = (SaleOrderMgr) context.getBean("saleOrderMgr");
		saleLevelCatMgr = (SaleLevelCatMgr) context.getBean("saleLevelCatMgr");
		logMgr = (LogMgr) context.getBean("logMgr");
		superviserMgr = (SuperviserMgr) context.getBean("superviserMgr");
		organizationMgr = (OrganizationMgr) context.getBean("organizationMgr");
		attributeMgr = (AttributeMgr) context.getBean("attributeMgr");
		dynamicAttributeHelper = (DynamicAttributeHelper) context.getBean("dynamicAttributeHelper");
		 
		lstStatus = new ArrayList<StatusBean>();
		StatusBean pausedStatus = new StatusBean(Long.valueOf(ActiveType.STOPPED.getValue()), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "pause.status.name"));
		StatusBean actionStatus = new StatusBean(Long.valueOf(ActiveType.RUNNING.getValue()), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.name"));
		StatusBean rejectStatus = new StatusBean(Long.valueOf(ActiveType.REJECTED.getValue()), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.reject"));
		StatusBean waitingStatus = new StatusBean(Long.valueOf(ActiveType.WAITING.getValue()), Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "action.status.waiting"));
		lstStatus.add(actionStatus);
		lstStatus.add(pausedStatus);
		lstStatus.add(rejectStatus);
		lstStatus.add(waitingStatus);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() throws Exception {
		try {
			hasEditPermission = Configuration.getSavePermission();
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.CUSTOMER);
			filter.setStatus(ActiveType.RUNNING);
			filter.setIsOrderByChannelTypeName(false);
			ObjectVO<ChannelType> customerTypeVO = channelTypeMgr.getListChannelType(filter, null);

			ShopToken shToken = currentUser.getShopRoot();
			shopId = null;
			shopCode = null;
			if (shToken != null) {
				shopId = shToken.getShopId();
				shopCode = shToken.getShopCode();
			}
			
			if (customerTypeVO != null) {
				lstCustomerType = customerTypeVO.getLstObject();
			}
			if (lstCustomerType == null) {
				lstCustomerType = new ArrayList<ChannelType>();
			}
			//			ObjectVO<GroupTransfer> groupTransferVO = groupTransferMgr.getListGroupTransferByShop(null, parentShopCode, parentShopCode); // CRM khong co bang GROUP_TRANSFER
			//			if(groupTransferVO!= null){
			//				lstGroupTransfer = groupTransferVO.getLstObject();
			////				for(int i=0;i<lstGroupTransfer.size();i++){//Huynp4
			////					lstGroupTransfer.get(i).setGroupTransferName(lstGroupTransfer.get(i).getGroupTransferCode()+"-"+lstGroupTransfer.get(i).getGroupTransferName());
			////				}
			//			} 
			//			lstGroupTransfer = new ArrayList<GroupTransfer>();		

			/*
			 * ShopFilter shopFilter = new ShopFilter();
			 * shopFilter.setStatus(ActiveType.RUNNING);
			 * shopFilter.setShopTypeId(Long.valueOf("9")); ObjectVO<Shop>
			 * shopVO = shopMgr.getListShop(shopFilter); if (shopVO != null) {
			 * lstRegion = shopVO.getLstObject(); }
			 */

			/*
			 * StaffPrsmFilter<StaffVO> staffFilter = new
			 * StaffPrsmFilter<StaffVO>();
			 * staffFilter.setObjectType(StaffObjectType.NVTT.getValue());
			 * staffFilter.setStatus(ActiveType.RUNNING.getValue()); if
			 * (currentUser.getListUser() != null &&
			 * !currentUser.getListUser().isEmpty()) {
			 * staffFilter.setParentStaffId
			 * (currentUser.getStaffRoot().getStaffId()); }
			 * staffFilter.setShopId(shopId); if (currentUser.getListShop() !=
			 * null && !currentUser.getListShop().isEmpty()) {
			 * staffFilter.setIsLstChildStaffRoot(true); } else {
			 * staffFilter.setIsLstChildStaffRoot(false); } ObjectVO<StaffVO>
			 * staffVO = staffMgr.searchListStaffVOByFilter(staffFilter); if
			 * (staffVO != null) { lstStaffVO = staffVO.getLstObject(); } if
			 * (lstStaffVO == null) { lstStaffVO = new ArrayList<StaffVO>(); }
			 */

			/*
			 * status = ActiveType.RUNNING.getValue(); lstLoyalty =
			 * apParamMgr.getListApParam(ApParamType.LOYALTY,
			 * ActiveType.RUNNING); if(lstLoyalty==null){ lstLoyalty = new
			 * ArrayList<ApParam>(); }
			 */
			//			else//Huynp4
			//			{
			//				for(int i=0;i<lstLoyalty.size();i++){
			//					lstLoyalty.get(i).setApParamName(lstLoyalty.get(i).getApParamCode()+"-"+lstLoyalty.get(i).getApParamName());
			//				}
			//			}
			/*
			 * lstLocation = apParamMgr.getListApParam(ApParamType.LOCATION,
			 * ActiveType.RUNNING); if(lstLocation==null){ lstLocation = new
			 * ArrayList<ApParam>(); }
			 */
			//			else//Huynp4
			//			{
			//				for(int i=0;i<lstLocation.size();i++){
			//					lstLocation.get(i).setApParamName(lstLocation.get(i).getApParamCode()+"-"+lstLocation.get(i).getApParamName());
			//				}
			//			}
			/*
			 * lstDisplay = apParamMgr.getListApParam(ApParamType.DISPLAY,
			 * ActiveType.RUNNING); if(lstDisplay==null){ lstDisplay = new
			 * ArrayList<ApParam>(); }
			 */
			//			else//Huynp4
			//			{
			//				for(int i=0;i<lstDisplay.size();i++){
			//					lstDisplay.get(i).setApParamName(lstDisplay.get(i).getApParamCode()+"-"+lstDisplay.get(i).getApParamName());
			//				}
			//			}


			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		resetToken(result);
		return SUCCESS;
	}

	/**
	 * Search customer.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since 03/08/2012
	 * 
	 * @author hunglm16
	 * @since February 20, 2014
	 * @description Update authorization for GSNPP
	 */
	public String search() throws Exception {
		result.put("page", page);
		result.put("rows", rows);
		try {
			KPaging<Customer> kPaging = new KPaging<Customer>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);

			ObjectVO<Customer> customerVO = searchCustomer(shopCode, kPaging);

			if (customerVO != null) {
				if (customerVO.getkPaging() != null) {
					result.put("total", customerVO.getkPaging().getTotalRows());
				}
				result.put("rows", customerVO.getLstObject());
			} else {
				customerVO = new ObjectVO<Customer>();
				customerVO.setLstObject(new ArrayList<Customer>());
				result.put("total", 0);
				result.put("rows", customerVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/*
	 * Tim kiem khach hang
	 */
	private ObjectVO<Customer> searchCustomer(String shopCode, KPaging<Customer> kPaging) throws BusinessException {
		Long shopId = getCurrentShopId(shopCode);
		ParamsDAOGetListCustomer params = getCustomerSearchParam(shopId);
		params.setkPaging(kPaging);
		ObjectVO<Customer> customerVO = customerMgr.getListCustomer(params);
		return customerVO;
	}
	
	/*
	 * tao doi tuong chua tham so tim kiem khach hang
	 */
	private ParamsDAOGetListCustomer getCustomerSearchParam(Long shopId) {
		ParamsDAOGetListCustomer params = new ParamsDAOGetListCustomer();
		params.setShortCode(customerCode);
		params.setCustomerName(customerName);
		params.setAllSubShop(true);
		params.setMobiPhone(phone);
		params.setStatus(ActiveType.parseValue(status));
		params.setAddress(address);
		params.setShopId(shopId);
		if (!StringUtil.isNullOrEmpty(customerType)) {
			params.setCustomerTypeCode(customerType);
		}
		params.setStrShopId(getStrListShopId());
		params.setOrder(order);
		
		if (!StringUtil.isNullOrEmpty(sort)) {
			final String ORDER_BY_SHOP_NAME = "shopName";
			final String ORDER_BY_CUSTOMER_CREATE_USER = "createUser";
			final String ORDER_BY_CUSTOMER_ROUTES = "routes";
			if ("shortCode".equals(sort)) {
				sort = "c.short_code";
			} else if ("customerName".equals(sort)) {
				sort = "c.customer_name";
			} else if ("channelType".equals(sort)) {
				sort = "ct.channel_type_name";
			} else if (ORDER_BY_SHOP_NAME.equalsIgnoreCase(sort)) {
				sort = "s.shop_name";
			} else if (ORDER_BY_CUSTOMER_CREATE_USER.equalsIgnoreCase(sort)) {
				sort = "c.create_user";
			} else if (ORDER_BY_CUSTOMER_ROUTES.equalsIgnoreCase(sort)) {
				sort = ORDER_BY_CUSTOMER_ROUTES;
			} else {
				sort = "c." + sort;
			}
		}
		
		params.setSort(sort);
		params.setTansuat(tansuat);
		return params;
	}
	
	

	/**
	 * Import excel.
	 * 
	 * @return the string
	 * @author hungtx
	 */
	private String importCustomer() throws Exception {
		resetToken(result);
		isError = true;
		totalItem = 0;
		String strWard = "";
		String message = "";
		listCell = new ArrayList<CellBean>();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		
		List<Object> dynamicAttributeVOs = getCustomerDynamicAttributeVO();
		final int TOTAL_DYNAMIC_ATTRIBUTE = dynamicAttributeVOs != null ? dynamicAttributeVOs.size() : 0;
		final int START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX = 35;
		
		int totalStaticCol = 35;
		final int startDataRowIndex = 2;
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, totalStaticCol + TOTAL_DYNAMIC_ATTRIBUTE, startDataRowIndex);
		List<Long> lstShopChild = getListShopChildId() ;
		
		ShopToken shToken = currentUser.getShopRoot();
		Shop shop = null;
		if (shToken != null) {
			shop = shopMgr.getShopById(shToken.getShopId());
		}

		if (shop == null) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		} else if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				List<ApParam> availableCustomerSalePositions = this.getCustomerSalePostionOptions();
				Boolean isHasErrorRow = false;
				for (int i = 0, size = lstData.size(); i < size; i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0 ) {//&& lstData.get(i).size() >= 28
						message = "";
						Customer customer = new Customer();
						customer.setShop(shop);
						List<String> row = lstData.get(i);
						int totalColumnInRow = row.size();
						Boolean isCreate = true;
						Shop sTmp = shop;
						totalItem++;

						final int shopCodeColumnIndex = 1;
						if (totalColumnInRow > shopCodeColumnIndex) {//Ma NPP
							String value = row.get(shopCodeColumnIndex);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.shopcode", 40, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								sTmp = shopMgr.getShopByCode(value);
								if (sTmp == null) {
									sTmp = shop;//
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.shop.code");
								} else {
									if (lstShopChild != null && !lstShopChild.contains(sTmp.getId())) {
										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_AREA);
									} else if (!ShopSpecificType.NPP.equals(sTmp.getType().getSpecificType())) {
										message += R.getResource("common.cms.shop.islevel5.undefined");
									}else if (!ActiveType.RUNNING.equals(sTmp.getStatus())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
									} else {
										customer.setShop(sTmp);
									}
								}
							} else {
								message += msg;
							}
						}
						
						//short code
						final int customerShortCodeColumnIndex = 2;
						if (totalColumnInRow > customerShortCodeColumnIndex) {
							String value = row.get(customerShortCodeColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "catalog.customer.import.customercode", 13, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (StringUtil.isNullOrEmpty(msg)) {
									customer = customerMgr.getCustomerByCode(StringUtil.getFullCode(sTmp.getShopCode(), value));
									if (customer == null) {
										isCreate = true;
										customer = new Customer();
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.import.customercode");
									} else {
										isCreate = false;
									}
									customer.setShop(sTmp);
								} else {
									message += msg;
								}
							} else{
								isCreate = true;
								customer = new Customer();
								customer.setShop(sTmp);
							}
						}
						
						//customer name
						final int customerNameColumnIndex = 3;
						if (totalColumnInRow > customerNameColumnIndex) {
							String value = row.get(customerNameColumnIndex);
							customer.setCustomerName(value);
							message += ValidateUtil.validateField(value, "catalog.customer.import.customername", 250, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
						}
						
						//customer type
						final int customerTypeColumnIndex = 4;
						if (totalColumnInRow > customerTypeColumnIndex) {
							String value = row.get(customerTypeColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								ChannelType channelType = null;
								try {
									channelType = channelTypeMgr.getChannelTypeByCode(value, ChannelTypeType.CUSTOMER);
								} catch (Exception e) {
									LogUtility.logError(e, e.getMessage());
								}
								if (channelType == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.type.code");
								} else if (ActiveType.STOPPED.getValue() == channelType.getStatus().getValue()) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.customer.type.code");
								}
								customer.setChannelType(channelType);
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.customer.type.code");
							}
						}
						
						//status
						final int customerStatusColumnIndex = 5;
						if (totalColumnInRow > customerStatusColumnIndex) {
							String value = row.get(customerStatusColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.status.active"))) {
									customer.setStatus(ActiveType.RUNNING);									
								} else if (value.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.status.stoped"))){
									customer.setStatus(ActiveType.STOPPED);									
								}
							} else{
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.customer.import.status");
							}
						}
						
						//contact
						final int customerContactColumnIndex = 6;
						if (totalColumnInRow > customerContactColumnIndex) {
							String value = row.get(customerContactColumnIndex);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.contactname", 250, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
							if (StringUtil.isNullOrEmpty(msg)) {
								customer.setContactName(value);
							} else {
								message += msg;
							}
						}
						
						//email
						final int customerEmailColumnIndex = 7;
						if (totalColumnInRow > customerEmailColumnIndex) {
							String value = row.get(customerEmailColumnIndex);
							String msg = ValidateUtil.validateField(value, "catalog.email.code", 50, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								customer.setEmail(value);
								if (!StringUtil.isNullOrEmpty(customer.getEmail()) && !ValidateUtil.validateEmailAddress(customer.getEmail())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID_EMAIL, true, "catalog.email.code");
								}
							} else {
								message += msg;
							}
						}
						
						//phone
						final int customerPhoneColumnIndex = 8;
						if (totalColumnInRow > customerPhoneColumnIndex) {
							String value = row.get(customerPhoneColumnIndex);
							customer.setPhone(value);
							String msg = ValidateUtil.validateField(value, "catalog.telephone", 30, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} 
						}
						
						//mobile phone
						final int customerMobiPhoneColumnIndex = 9;
						if (totalColumnInRow > customerMobiPhoneColumnIndex) {
							String value = row.get(customerMobiPhoneColumnIndex);
							customer.setMobiphone(value);
							String msg = ValidateUtil.validateField(value,"catalog.mobilephone", 30, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						//fax
						final int customerFaxColumnIndex = 10;
						if (totalColumnInRow > customerFaxColumnIndex) {
							String value = row.get(customerFaxColumnIndex);
							customer.setFax(value);
							String msg = ValidateUtil.validateField(value, "catalog_unit_fax", 20, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} 
						}

						// area code
						final int areaCodeColumnIndex = 11;
						if (totalColumnInRow > areaCodeColumnIndex) {
							String value = row.get(areaCodeColumnIndex);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.areaid", 30, ConstantManager.ERR_MAX_LENGTH,ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
							if (StringUtil.isNullOrEmpty(msg)) {
								String[] lstStr = value.split("-");
								String areaCode = value.split("-")[0];
								if (!StringUtil.isNullOrEmpty(areaCode)) {
									areaCode = areaCode.trim();
								}
								if (lstStr.length > 1)
									strWard = lstStr[1]; //ten phuong`

								Area area = areaMgr.getAreaByCode(areaCode);
								customer.setArea(area);
								if (area == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.precinct");
								} else {
									if (area.getType() == null || !AreaType.WARD.equals(area.getType())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.excel.areaisward");
									} else {
										if (!ActiveType.RUNNING.equals(area.getStatus())) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.customer.precinct");
										}
									}
								}
							} else {
								message += msg;
							}
						}

						String address = "";

						//street
						final int streetColumnIndex = 12;
						if (totalColumnInRow > streetColumnIndex) {
							String value = row.get(streetColumnIndex);
							customer.setStreet(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.street", 200, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						//so nha
						final int houseNumberColumnIndex = 13;
						if (totalColumnInRow > houseNumberColumnIndex) {
							String value = row.get(houseNumberColumnIndex);
							customer.setHousenumber(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.housenumber", 100, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						//address
						if (customer.getArea() != null) {
							Area area = customer.getArea();
							address = !StringUtil.isNullOrEmpty(customer.getHousenumber()) ? customer.getHousenumber()+", " : "" ;
							address += !StringUtil.isNullOrEmpty(customer.getStreet()) ? customer.getStreet()+ ", " : "";
							address += area.getPrecinctName()+", " + area.getDistrictName()+", " + area.getProvinceName();
							customer.setAddress(address);
						}
						
						//ngay sinh
						final int birthdayColumnIndex = 14;
						if (totalColumnInRow > birthdayColumnIndex) {
							String value = row.get(birthdayColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value.trim(), "catalog.customer.ngaysinh", false);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									customer.setBirthDay(DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY));
								}
							}
						}
						
						//tan suat
						final int frequencyColumnIndex = 15;
						if (totalColumnInRow > frequencyColumnIndex) {
							String value = row.get(frequencyColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "catalog.customer.tansuat", 2, ConstantManager.ERR_MAX_LENGTH,ConstantManager.ERR_INTEGER);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									Integer tansuat = Integer.parseInt(value);
									if (tansuat < 0) {
										message += R.getResource("imp.tuyen.clmn.error.tanSuat");
									}
									customer.setFrequency(Integer.parseInt(value));
								}
							}
						}
						
						//routing
						final int routingColumnIndex = 16;
						if (totalColumnInRow > routingColumnIndex) {
							String value = row.get(routingColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "catalog.customer.tuyen.giao.hang", 100, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else if (customer.getShop() != null) {
									Routing routing = superviserMgr.getRoutingByCode(customer.getShop().getId(), value);
									if (routing == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.tuyen.giao.hang");
									} else {
										customer.setRouting(routing);
									}
								}
							}
						}
						
						//lat
						final int latColumnIndex = 17;
						if (totalColumnInRow > latColumnIndex) {
							String value = row.get(latColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value,"catalog.label.lat", 30,ConstantManager.ERR_FLOAT_NUMBER, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									customer.setLat(Double.parseDouble(value));
								}
							}
						}
						
						//lng
						final int lngColumnIndex = 18;
						if (totalColumnInRow > lngColumnIndex) {
							String value = row.get(lngColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value,"catalog.label.lng", 30,ConstantManager.ERR_FLOAT_NUMBER, ConstantManager.ERR_MAX_LENGTH);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
								} else {
									customer.setLng(Double.parseDouble(value));
								}
							}
						}
						
						//nhan vien giao hang
						final int deliveryStaffColumnIndex = 19;
						if (totalColumnInRow > deliveryStaffColumnIndex) {
							String value = row.get(deliveryStaffColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								Staff staff = null;
								try {
									staff = staffMgr.getStaffByCodeAndShopId(value, customer.getShop().getId());
								} catch (Exception e) {
									LogUtility.logError(e, e.getMessage());
								}
								if (staff == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.import.deliverstaffid");
								} else {
									//Check staff is deliver sale
									if (!StaffSpecificType.NVGH.equals(staff.getStaffType().getSpecificType())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, true, "catalog.customer.import.isnotdeliverstaff", value);
									} else {
										if (staff.getShop() != null && customer.getShop() != null && !staff.getShop().getId().equals(customer.getShop().getId())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, true, "common.catalog.not.in.shop", staff.getStaffCode(), shop.getShopCode());
										} else {
											if (staff.getStatus() == null || !staff.getStatus().equals(ActiveType.RUNNING)) {
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.customer.import.deliverstaffid");
											}
										}
									}
								}
								customer.setDeliver(staff);
							}
						}
						
						//cashier
						final int cashierStaffColumnIndex = 20;
						if (totalColumnInRow > cashierStaffColumnIndex) {
							String value = row.get(cashierStaffColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								Staff staff = null;
								try {
									staff = staffMgr.getStaffByCodeAndShopId(value, customer.getShop().getId());
								} catch (Exception e) {
									LogUtility.logError(e, e.getMessage());
								}
								if (staff == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.import.cashierstaffid");
								} else {
									if (!StaffSpecificType.NVTT.equals(staff.getStaffType().getSpecificType())) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, true, "catalog.customer.import.isnotcashierstaff", value);
									} else {
										if (staff.getShop() != null && customer.getShop() != null && !staff.getShop().getId().equals(customer.getShop().getId())) {
											message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, true, "common.catalog.not.in.shop", staff.getStaffCode(), shop.getShopCode());
										} else {
											if (staff.getStatus() == null || !staff.getStatus().equals(ActiveType.RUNNING)) {
												message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.customer.import.cashierstaffid");
											}
										}
									}
								}
								customer.setCashierStaff(staff);
							}
						}
						
						boolean flagApplyDebitLimited = false;
						// max debit amount: Muc no
						final int limitDebitColumnIndex = 21;
						if (totalColumnInRow > limitDebitColumnIndex) {
							String value = row.get(limitDebitColumnIndex);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.maxdebitamount", 20, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								if (!StringUtil.isNullOrEmpty(value)) {
									flagApplyDebitLimited = true;
									try {
										BigDecimal tmp = new BigDecimal(value);
										if (tmp.longValue() >= 0) {
											customer.setMaxDebitAmount(tmp);
										}
									} catch (Exception e) {
										// pass through
									}
								} else {
									if (!isCreate) {
										customer.setMaxDebitAmount(null);
									}
								}
							}
						}

						// debit date: Han no
						final int expiredDebitDateColumnIndex = 22;
						if (totalColumnInRow > expiredDebitDateColumnIndex) {
							String value = row.get(expiredDebitDateColumnIndex);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.maxdebitdate", 3, ConstantManager.ERR_INTEGER, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								if (!StringUtil.isNullOrEmpty(value)) {
									flagApplyDebitLimited = true;
									Integer __value = null;
									try {
										__value = Integer.valueOf(value);
										customer.setMaxDebitDate(__value);
									} catch (Exception e) {
									}
								} else {
									if (!isCreate) {
										customer.setMaxDebitDate(null);
									}
								}
							}
						}

						if (flagApplyDebitLimited && customer.getMaxDebitAmount() != null && (!customer.getMaxDebitAmount().equals(BigDecimal.valueOf(-1)) || !customer.getMaxDebitDate().equals(-1))) {
							customer.setApplyDebitLimited(1);
						} else {
							customer.setApplyDebitLimited(null);
						}
						
						/*
						 * vi tri ban hang
						 */
						final int salePositionColumnIndex = expiredDebitDateColumnIndex + 1;
						if (totalColumnInRow > salePositionColumnIndex) {
							String value = row.get(salePositionColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								value = value.trim();
								ApParam validSalePosition = null;
								for (int j = 0, sizej = availableCustomerSalePositions.size(); j < sizej; j++) {
									validSalePosition = availableCustomerSalePositions.get(j);
									if (value.equals(validSalePosition.getApParamName())) {
										break;
									}
								}
								if (validSalePosition != null) {
									customer.setSalePosition(validSalePosition);									
								} else {
									message += R.getResource("catalog.customer.import.sale.position.invalid", value);
								}
							}
						}
						
						//ma so thue
						final int taxCodeColumnIndex = salePositionColumnIndex + 1;
						if (totalColumnInRow > taxCodeColumnIndex) {
							String value = row.get(taxCodeColumnIndex);
							customer.setInvoiceTax(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.taxnumber", 20, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						// account number
						final int bankAccountNumberColumnIndex = taxCodeColumnIndex + 1;
						if (totalColumnInRow > bankAccountNumberColumnIndex) {
							String value = row.get(bankAccountNumberColumnIndex);
							customer.setInvoiceNumberAccount(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.invoicennumber", 50, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						//ten don vi
						final int bankAccountColumnIndex = bankAccountNumberColumnIndex + 1;
						if (totalColumnInRow > bankAccountColumnIndex) {
							String value = row.get(bankAccountColumnIndex);
							customer.setInvoiceConpanyName(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.invoiceoutletname", 250, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}

						// ng dai dien
						final int representationPersonColumnIndex = bankAccountColumnIndex + 1;
						if (totalColumnInRow > representationPersonColumnIndex) {
							String value = row.get(representationPersonColumnIndex);
							customer.setInvoiceOutletName(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.contactname", 250, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}

						//ten ngan hang
						final int bankNameColumnIndex = representationPersonColumnIndex + 1;
						if (totalColumnInRow > bankNameColumnIndex) {
							String value = row.get(bankNameColumnIndex);
							customer.setInvoiceNameBank(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.bankname", 250, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						//ten chi nhanh ngan hang
						final int bankBranchNameColumnIndex = bankNameColumnIndex + 1;
						if (totalColumnInRow > bankBranchNameColumnIndex) {
							String value = row.get(bankBranchNameColumnIndex);
							customer.setInvoiceNameBranchBank(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.chinhanh", 250, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						//ten chu tk
						final int bankAccountOwnerNameColumnIndex = bankBranchNameColumnIndex + 1;
						if (totalColumnInRow > bankAccountOwnerNameColumnIndex) {
							String value = row.get(bankAccountOwnerNameColumnIndex);
							customer.setBankAccountOwner(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.chutk", 250, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						//dia chi hoa don
						final int invoiceAddressColumnIndex = bankAccountOwnerNameColumnIndex + 1;
						if (totalColumnInRow > invoiceAddressColumnIndex) {
							String value = row.get(invoiceAddressColumnIndex);
							customer.setDeliveryAddress(value);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.billaddress", 100, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							}
						}
						
						//open date
						final int customerOpenDateColumnIndex = invoiceAddressColumnIndex + 1;
						if (totalColumnInRow > customerOpenDateColumnIndex) {
							String value = row.get(customerOpenDateColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value.trim(), "catalog.customer.import.opendate", false);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									customer.setOpenDate(null);
								} else {
									customer.setOpenDate(DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY));
								}
							}
						}
						
						//close date
						final int customerCloseDateColumnIndex = customerOpenDateColumnIndex + 1;
						if (totalColumnInRow > customerCloseDateColumnIndex) {
							String value = row.get(customerCloseDateColumnIndex);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.getErrorMsgForInvalidFormatDate(value.trim(), "catalog.customer.import.closedate", false);
								if (!StringUtil.isNullOrEmpty(msg)) {
									message += msg;
									customer.setCloseDate(null);
								} else {
									customer.setCloseDate(DateUtil.parse(value.trim(), DateUtil.DATE_FORMAT_DDMMYYYY));
								}
							}
						}
						//Co in hoa don VAT
						final int isPrintVATAddressColumnIndex = customerCloseDateColumnIndex + 1;
						if (totalColumnInRow > isPrintVATAddressColumnIndex) {
							String value = row.get(isPrintVATAddressColumnIndex);
							if (value != null && value.trim().equals("1")) {
								customer.setIsVat(IsAttribute.IS);
							} else {
								customer.setIsVat(IsAttribute.UN);
							}
						}

						lstAttributeId = new ArrayList<Long>();
						lstAttributeValue = new ArrayList<String>();
						lstAttributeColumnValueType = new ArrayList<Integer>();
						List<String> dynamicAttributeColumnDataInImportFile = new ArrayList<String>();
						for (int j = START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX, lastDataColumnIndex = START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX + TOTAL_DYNAMIC_ATTRIBUTE
								; j <= lastDataColumnIndex; j++) {
							int dynamicAttributeSequence = j - START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX;
							if (totalColumnInRow > j) {
								String cellData = row.get(j);
								dynamicAttributeColumnDataInImportFile.add(cellData);
								AttributeDynamicVO dynamicAttributeVO = TOTAL_DYNAMIC_ATTRIBUTE > dynamicAttributeSequence ? (AttributeDynamicVO) dynamicAttributeVOs.get(dynamicAttributeSequence) : null;
								String errMsg = DynamicAttributeValidator.validateDynamicAttributeColumnData(row, j, dynamicAttributeVO);
								if (!StringUtil.isNullOrEmpty(errMsg)) {
									message += errMsg + ConstantManager.NEW_LINE_CHARACTER;
								}
								if (dynamicAttributeVO != null) {
									lstAttributeId.add(dynamicAttributeVO.getAttributeId());
									lstAttributeValue.add(DynamicAttributeValidator.getInputDynamicAttributeValue(row, j, dynamicAttributeVO));
									lstAttributeColumnValueType.add(dynamicAttributeVO.getType());
								}
							}
						}
						
						if (StringUtil.isNullOrEmpty(message)) {
							if (isCreate) {
								customerMgr.createCustomer(customer, null, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, getLogInfoVO());
							} else {
								customerMgr.updateCustomer(customer, null, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, getLogInfoVO());
							}
						} else {
							isHasErrorRow = true;
							if (message.substring(message.length() - 1).equals("\n")) {
								message = message.substring(0, message.length() - 1);
							}
							message = message.replace(ConstantManager.NEW_LINE_CHARACTER + ConstantManager.NEW_LINE_CHARACTER, ConstantManager.NEW_LINE_CHARACTER);
							CellBean failBean = StringUtil.addFailBean(row, message);
							failBean.setLstDynamic(dynamicAttributeColumnDataInImportFile);
							lstFails.add(failBean);
						}
					}
				}
				//Export error
				if (isHasErrorRow) {
					Map<String, Object> beans = initCustomerImportTempateHeader();
					beans.put("lstData", lstFails);
					beans.put("dynamicAttribute", dynamicAttributeVOs);
					beans.put("dynamicAttributeHeader", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.import.template.header.dynamic.attribute"));
					fileNameFail = exportErrorCustomerImportFile(beans, Configuration.getFailDataPath(),
										dynamicAttributeVOs.isEmpty() ? ConstantManager.TEMPLATE_CATALOG_CUSTOMER_FAIL_WITHOUT_DYNAMIC_ATTRIBUTE : ConstantManager.TEMPLATE_CATALOG_CUSTOMER_FAIL);
				}
				numFail = lstFails.size();
			} catch (Exception e) {
				throw e;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	
	
	private Map<String, Object> initCustomerImportTempateHeader(){
		Map<String, Object> fileHeaderParams = new HashMap<String, Object>();
		fileHeaderParams.put("stt", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.stt"));
		fileHeaderParams.put("shopCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.shopcode"));
		fileHeaderParams.put("customerCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.customercode"));
		fileHeaderParams.put("cutomerName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.customername"));
		fileHeaderParams.put("customerTypeId", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.customertypeid"));
		fileHeaderParams.put("status", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.status"));
		fileHeaderParams.put("contactName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.contactname"));
		fileHeaderParams.put("phone", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_phone"));
		fileHeaderParams.put("mobile", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_mobile"));
		fileHeaderParams.put("fax", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_fax"));
		fileHeaderParams.put("email", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "organization_email"));
		fileHeaderParams.put("lat", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.label.lat").toString());
		fileHeaderParams.put("lng", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.label.lng").toString());
		fileHeaderParams.put("areaId", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.areaid"));
		fileHeaderParams.put("street", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.street"));
		fileHeaderParams.put("houseNumber", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.housenumber"));
		fileHeaderParams.put("birthDay", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.ngaysinh"));
		fileHeaderParams.put("tansuat", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.tansuat"));
		fileHeaderParams.put("routingCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.tuyen.giao.hang"));
		fileHeaderParams.put("saleStaffId", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.salestaffid"));
		fileHeaderParams.put("deliverierStaff", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.deliverstaffid"));
		fileHeaderParams.put("cashierStaffId", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.cashierstaffid"));
		fileHeaderParams.put("maxDebitAmount", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.maxdebitamount"));
		fileHeaderParams.put("maxdebitDate", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.maxdebitdate"));
		fileHeaderParams.put("taxNumber", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.taxnumber"));
		fileHeaderParams.put("invoiceNumber", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.invoicennumber"));
		fileHeaderParams.put("invoiceOutletName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.invoiceoutletname"));
		fileHeaderParams.put("contactName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.contactname"));
		fileHeaderParams.put("bankName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.bankname"));
		fileHeaderParams.put("branchBankName", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.chinhanh"));
		fileHeaderParams.put("bankAccountOwner", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.chutk"));
		fileHeaderParams.put("isVAT", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.isvat"));
		fileHeaderParams.put("billAddress", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.billaddress"));
		fileHeaderParams.put("createDate", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.opendate"));
		fileHeaderParams.put("closeDate", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.closedate"));
		fileHeaderParams.put("customerInfo", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.customerinfo"));
		fileHeaderParams.put("saleInfo", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.saleinfo"));
		fileHeaderParams.put("billInfo", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.billinfo"));
		fileHeaderParams.put("cat", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.cat"));
		fileHeaderParams.put("level", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.level.code"));
		fileHeaderParams.put("mota", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.s.mo.ta"));
		fileHeaderParams.put("salePosition", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.template.header.sale.position"));
		fileHeaderParams.put("dynamicAttributeHeader", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.import.template.header.dynamic.attribute"));
		fileHeaderParams.put("error", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "customer.import.template.header.error"));
		return fileHeaderParams;
	}
	
	/*
	 * export file import khach hang loi
	 */
	private String exportErrorCustomerImportFile(Map<String, Object> beans, String importFailTemplateDirectory, String importFailTemplateFileName) throws Exception {
		return exportExcelJxlsNotAuthentication(beans, importFailTemplateDirectory, importFailTemplateFileName);
	}
	
	public String getListStaff(){
		try {
			StaffFilter staffFilter = new StaffFilter();
			staffFilter.setStatus(ActiveType.RUNNING);
			staffFilter.setShopCode(shopCode);
			staffFilter.setSpecType(StaffSpecificType.NVGH);
			staffFilter.setIsGetShopOnly(true);
			listDeliverStaff = staffMgr.getListStaff(staffFilter).getLstObject();
			if (listDeliverStaff == null) {
				listDeliverStaff = new ArrayList<Staff>();
			}
			staffFilter.setSpecType(StaffSpecificType.NVTT);
			listCashierStaff = staffMgr.getListStaff(staffFilter).getLstObject();
			if (listCashierStaff == null) {
				listCashierStaff = new ArrayList<Staff>();
			}
			listRouting = new ArrayList<RoutingVO>();
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				RoutingGeneralFilter<RoutingVO> filter = new RoutingGeneralFilter<RoutingVO>();
				filter.setShopId(shop.getId());
				filter.setStatus(ActiveType.RUNNING.getValue());
				listRouting = superviserMgr.searchListRoutingVOByFilter(filter).getLstObject();
			}
			
			result.put("listDeliverStaff",listDeliverStaff);
			result.put("listCashierStaff",listCashierStaff);
			result.put("listRouting",listRouting);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		return JSON;
	}

	/**
	 * Gets the changed form.
	 * 
	 * @return the changed form
	 * @throws Exception
	 * 
	 * @author hunglm16
	 * @since February 10, 2014
	 * @description Update authorization for GSNPP
	 */
	public String getChangedForm() throws Exception {
		resetToken(result);
		ObjectVO<ProductInfo> lstProductInfoTmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, true, null, null);
		lstCat = lstProductInfoTmp.getLstObject();
		hasEditPermission = Configuration.getSavePermission();
		Customer customer = null;
		ChannelTypeFilter listCustomerTypeFilter = new ChannelTypeFilter();
		listCustomerTypeFilter.setType(ChannelTypeType.CUSTOMER);
		listCustomerTypeFilter.setStatus(ActiveType.RUNNING);

		ShopToken shToken = currentUser.getShopRoot();
		Shop shop = null;
		if (shToken != null) {
			shop = shopMgr.getShopById(shToken.getShopId());
			if (shop != null) {
				shopCode = shop.getShopCode();
				if (shop.getType() != null && shop.getType().getSpecificType() != null && shop.getType().getSpecificType().equals(ShopSpecificType.NPP)) {
					cusShopCode = shop.getShopCode(); 
				}
			}
		}

		listCustomerType = channelTypeMgr.getListChannelType(listCustomerTypeFilter, null).getLstObject();
		lstProvince = areaMgr.getListAreaByType(null, AreaType.PROVINCE.getValue(), ActiveType.RUNNING.getValue());
		createNew = true;
		if (id != null && id > 0) {
			customer = customerMgr.getCustomerById(id);
			if (customer != null) {
				if (customer.getShop() == null || super.getMapShopChild().get(customer.getShop().getId()) == null) {
					return PAGE_NOT_PERMISSION;
				}
				//TODO for tab1
				createNew = false;
				customerCode = customer.getShortCode();
				customerName = customer.getCustomerName();
				contact = customer.getContactName();
				idNumber = customer.getIdno();
				phone = customer.getPhone();
				mobilePhone = customer.getMobiphone();
				email = customer.getEmail();
				status = customer.getStatus().getValue();
				street = customer.getStreet();
				address = customer.getAddress();
				statusCustomer = customer.getStatus().getValue();
				shopCode = customer.getShop().getShopCode();
				routingId = customer.getRouting() != null ? customer.getRouting().getId() : null;
				birthDayStr = customer.getBirthDay() != null ? DateUtil.toDateString(customer.getBirthDay()) : "";
				tansuat = customer.getFrequency();
				startDateStr = customer.getOpenDate() != null ? DateUtil.toDateString(customer.getOpenDate()) : "";
				endDateStr = customer.getCloseDate() != null ? DateUtil.toDateString(customer.getCloseDate()) : "";
				Area area = customer.getArea();
				Area areaTmp = null;
				if (area != null) {
					areaId = area.getId();
					provinceCode = area.getProvince();
					provinceName = area.getProvinceName();
					districtCode = area.getDistrict();
					districtName = area.getDistrictName();
					wardCode = area.getAreaCode();
					wardName = area.getAreaName();
					if (area.getParentArea() != null && area.getParentArea().getParentArea() != null) {
						areaTmp = area.getParentArea().getParentArea();
						lstDistrict = areaMgr.getListAreaByType(areaTmp.getId(), AreaType.DISTRICT.getValue(), ActiveType.RUNNING.getValue());
						provinceCode = areaTmp.getAreaCode();
						provinceName = areaTmp.getAreaName();
					}
					if (area.getParentArea() != null) {
						areaTmp = area.getParentArea();
						lstPrecinct = areaMgr.getListAreaByType(areaTmp.getId(), AreaType.WARD.getValue(), ActiveType.RUNNING.getValue());
						districtCode = areaTmp.getAreaCode();
						districtName = areaTmp.getAreaName();
					}
				}
				addressNumber = customer.getHousenumber();
				//TODO for tab2
				if (customer.getChannelType() != null) {
					customerType = customer.getChannelType().getChannelTypeCode();
				}
				deliveryCode = customer.getDeliver() == null ? null : customer.getDeliver().getStaffCode();
				cashierCode = customer.getCashierStaff() == null ? null : customer.getCashierStaff().getStaffCode();
				applyDebitLimited = customer.getApplyDebitLimited();
				maxDebit = customer.getMaxDebitAmount();
				debitDate = customer.getMaxDebitDate();
				DebitMgr debitMgr = (DebitMgr) context.getBean("debitMgr");
				DebitDetail debitDetail = debitMgr.getDebitCustomerRemain(customer.getId());
				if (debitDetail != null) {
					Date createdebit = debitDetail.getCreateDate();
					Date sysday = commonMgr.getSysDate();
					debitDateNow = DateUtil.dateDiff(sysday, createdebit);
				}
				Debit debit = debitMgr.getDebit(customer.getId(), DebitOwnerType.CUSTOMER);
				if (debit != null) {
					maxDebitNow = debit.getTotalDebit();
				} else {
					maxDebitNow = BigDecimal.ZERO;
				}
				invoiceConpanyName = customer.getInvoiceConpanyName();
				invoiceOutletName = customer.getInvoiceOutletName();
				custDeliveryAddr = customer.getDeliveryAddress();
				invoiceTax = customer.getInvoiceTax();
				invoiceNumberAccount = customer.getInvoiceNumberAccount();
				invoiceNameBank = customer.getInvoiceNameBank();
				invoiceNameBranchBank = customer.getInvoiceNameBranchBank();
				bankAccountOwner = customer.getBankAccountOwner();
				lat = customer.getLat() != null ? String.valueOf(customer.getLat()) : "";
				lng = customer.getLng() != null ? String.valueOf(customer.getLng()) : "";
				//TODO for tab 4
				if (customer.getIsVat() != null) {
					isVat = customer.getIsVat().getValue();
				} else {
					isVat = IsAttribute.UN.getValue();
				}
				
				ApParam currentCustomerSalePosition = customer.getSalePosition();
				if (currentCustomerSalePosition != null && !StringUtil.isNullOrEmpty(currentCustomerSalePosition.getValue())) {
					customerSalePosition = Integer.valueOf(currentCustomerSalePosition.getValue());
				}
			} else {
				return PAGE_NOT_PERMISSION;
			}
		}
		if (customer != null) {
			shop = customer.getShop();
			RoutingGeneralFilter<RoutingVO> filter = new RoutingGeneralFilter<RoutingVO>();
			filter.setShopId(shop.getId());
			filter.setStatus(ActiveType.RUNNING.getValue());
			listRouting = superviserMgr.searchListRoutingVOByFilter(filter).getLstObject();
		}
		StaffFilter staffFilter = new StaffFilter();
		staffFilter.setStatus(ActiveType.RUNNING);
		staffFilter.setShopCode(shop == null ? null : shop.getShopCode());
		staffFilter.setSpecType(StaffSpecificType.NVGH);
		staffFilter.setIsGetShopOnly(true);
		listDeliverStaff = staffMgr.getListStaff(staffFilter).getLstObject();
		staffFilter.setSpecType(StaffSpecificType.NVTT);
		listCashierStaff = staffMgr.getListStaff(staffFilter).getLstObject();
		salePositionOptions = this.getCustomerSalePostionOptions();
		loadAttributeDynamic();		
		return SUCCESS;
	}
	
	/**
	 * Lay danh sach cac option vi tri ban hang cua khach hang
	 * 
	 * @author tuannd20
	 * @return danh sach cac vi tri khach hang dang hoat dong
	 * @throws BusinessException
	 * @since 20/08/2015
	 */
	private List<ApParam> getCustomerSalePostionOptions() throws BusinessException {
		return apParamMgr.getListApParam(ApParamType.CUSTOMER_SALE_POSITION, ActiveType.RUNNING);
	}
	
	public String getListRoutingForShop() {
		result.put("listRouting", new ArrayList<RoutingVO>());
		try {
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				Shop shop = shopMgr.getShopByCode(shopCode);
				if (shop == null) {
					return JSON;
				}
				shopId = shop.getId();
			}
			RoutingGeneralFilter<RoutingVO> filter = new RoutingGeneralFilter<RoutingVO>();
			filter.setShopId(shopId);
			filter.setStatus(ActiveType.RUNNING.getValue());
			result.put("listRouting", superviserMgr.searchListRoutingVOByFilter(filter).getLstObject());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public String loadAttributeDynamic() {
		result.put("page", page);
		result.put("max", max);
		AttributeMgr attributeMgr = (AttributeMgr) context.getBean("attributeMgr");
		try {
//			Integer CHOOSE_ALL = -1;
			AttributeDynamicFilter filter = new AttributeDynamicFilter();
//			filter.setAttributeCode(attributeCode);
//			filter.setAttributeName(attributeName);
			filter.setStatus(1);
			filter.setApplyObject(1);
//			if(!CHOOSE_ALL.equals(status)){
//				filter.setStatus(status);
//			}
			/**Sort grid*/
			filter.setOrder(order);
			filter.setSort(sort);
			ObjectVO<AttributeDynamicVO> lstAttribute = attributeMgr.getListAttributeDynamicVO(filter, null);
			if (lstAttribute != null) {
				lstAttributeDynamic = lstAttribute.getLstObject();
				AttributeEnumFilter enumFilter = new AttributeEnumFilter();
				AttributeDetailFilter detailFilter = new AttributeDetailFilter();
				for(AttributeDynamicVO vo : lstAttributeDynamic){
					enumFilter.setAttributeId(vo.getAttributeId());
					enumFilter.setStatus(ActiveType.RUNNING);
					enumFilter.setApplyObject(1);
					detailFilter.setAttributeId(vo.getAttributeId());
					detailFilter.setStatus(ActiveType.RUNNING);
					detailFilter.setCustomerID(id);
					if(AttributeColumnType.CHOICE.getValue().equals(vo.getType())
						|| AttributeColumnType.MULTI_CHOICE.getValue().equals(vo.getType())){					
						vo.setAttributeEnumVOs(attributeMgr.getListAttributeDetailDynamicVO(enumFilter, null).getLstObject());										
					}				
					if(detailFilter.getCustomerID() != null){
						vo.setAttributeDetailVOs(attributeMgr.getListAttributeDetailVO(detailFilter, null).getLstObject());
					}
				}
//				result.put("total", lstAttribute.getkPaging().getTotalRows());
//				result.put("rows", lstAttribute.getLstObject());
			}else {
//				result.put("total",0);
//				result.put("rows", new ArrayList<AttributeDynamicVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public String checkSaleOrder() throws Exception {
		final int maxNumSale = 5;
		resetToken(result);
		String errMsg = "";
		if (currentUser != null) {
			try {
				ShopToken shToken = currentUser.getShopRoot();
				Shop shop = null;
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					shop = shopMgr.getShopByCode(shopCode);
				}
				if (shToken != null && shop == null) {
					shop = shopMgr.getShopById(shToken.getShopId());
				}

				if (shop == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				} else if (super.getMapShopChild().get(shop.getId()) == null) {
					errMsg = R.getResource("common.shop.not.belong.area");
				} else if (!ShopSpecificType.NPP.equals(shop.getType().getSpecificType())) {
					errMsg = R.getResource("common.cms.shop.islevel5.undefined");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(idNumber) && !ValidateUtil.validateNumber(idNumber)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NUMBER, false, "common.id.number");
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(customerName, "catalog.customer.import.customername", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(contact, "catalog.customer.import.contactname", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(idNumber, "common.id.number", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(phone, "catalog.customer.phone", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(mobilePhone, "catalog.customer.mobile", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(email)) {
					errMsg = ValidateUtil.validateField(email, "common.email", null, ConstantManager.ERR_INVALID_EMAIL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(addressNumber, "catalog.customer.house.number", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(street, "catalog.customer.street", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					ActiveType customerStatus = ActiveType.parseValue(status);
					if (id != null && id > 0) {
						Customer customer = customerMgr.getCustomerById(id);
						if (customer != null) {
							if(!customer.getShop().getId().equals(shop.getId())){
								result.put(ERROR, true);
								result.put("errMsg", "Khách hàng không thuộc quyền xử lý user hiện tại!");
								return JSON;
							} else {
								if(customerStatus.equals(ActiveType.STOPPED)) {
									List<SaleOrder> lstCustomer = saleOrderMgr.getListCustomerNotApproved(id);
									if(lstCustomer != null && lstCustomer.size() > 0) {
										StringBuilder sb = new StringBuilder("Khách hàng vẫn còn các giao dịch sau chưa xử lý xong: ");
										int numSale = 0;
										for (SaleOrder saleOrder : lstCustomer) {
											if(numSale == 0) {
												sb.append(saleOrder.getOrderNumber());
											} else if(numSale < maxNumSale) {
												sb.append(", ");
												sb.append(saleOrder.getOrderNumber());
//											} else if(numSale >= maxNumSale) {
											} else {
												sb.append("...");
												break;
											}
											numSale++;
										}
										sb.append(" Những giao dịch này sẽ không được duyệt nếu khách hàng ngưng hoạt động. Bạn có muốn tiếp tục không?");
										result.put(ERROR, true);
										result.put("errMsg", sb.toString());
										return JSON;
									} else {
										result.put(ERROR, false);
									}
								} else {
									result.put(ERROR, false);
								}
							}
						} else {
							result.put(ERROR, true);
							result.put("errMsg", "Khách hàng không tồn tại");
							return JSON;
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", "Chưa chọn khách hàng");
						return JSON;
					}
				} else {
					result.put(ERROR, true);
					result.put("errMsg", errMsg);
					return JSON;
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		return JSON;
	}

	/**
	 * Save info.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since 03/08/2012
	 * 
	 * @author hunglm16
	 * @since February 20, 2014
	 * @description Update authorization for GSNPP
	 */
	public String saveInfo() throws Exception {
		final int maxNumSale = 5;
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				ShopToken shToken = currentUser.getShopRoot();
				Shop shop = null;
				if (!StringUtil.isNullOrEmpty(shopCode)) {
					shop = shopMgr.getShopByCode(shopCode);
				}
				if (shToken != null && shop == null) {
					shop = shopMgr.getShopById(shToken.getShopId());
				}

				if (shop == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				} else if (super.getMapShopChild().get(shop.getId()) == null) {
					errMsg = R.getResource("common.shop.not.belong.area");
				} else if (!ShopSpecificType.NPP.equals(shop.getType().getSpecificType())) {
					errMsg = R.getResource("common.cms.shop.islevel5.undefined");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(idNumber) && !ValidateUtil.validateNumber(idNumber)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NUMBER, false, "common.id.number");
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(customerName, "catalog.customer.import.customername", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(contact, "catalog.customer.import.contactname", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(idNumber, "common.id.number", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(phone, "catalog.customer.phone", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(mobilePhone, "catalog.customer.mobile", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(email)) {
					errMsg = ValidateUtil.validateField(email, "common.email", null, ConstantManager.ERR_INVALID_EMAIL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(addressNumber, "catalog.customer.house.number", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(street, "catalog.customer.street", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					ActiveType customerStatus = ActiveType.parseValue(status);
					if (id != null && id > 0) {
						Customer customer = customerMgr.getCustomerById(id);
						if (customer != null) {
							/*List<SaleOrder> lstCustomer = saleOrderMgr.getListCustomerNotApproved(id);
							if(lstCustomer != null && lstCustomer.size() > 0) {
								StringBuilder sb = new StringBuilder("Khách hàng vẫn còn các giao dịch sau chưa xử lý xong: ");
								int numSale = 0;
								for (SaleOrder saleOrder : lstCustomer) {
									if(numSale == 0) {
										sb.append(saleOrder.getOrderNumber());
									} else if(numSale < maxNumSale) {
										sb.append(", ");
										sb.append(saleOrder.getOrderNumber());
									} else if(numSale >= maxNumSale) {
										sb.append("...");
										break;
									}
									numSale++;
								}
								sb.append(" Những giao dịch này sẽ không được duyệt nếu khách hàng ngưng hoạt động. Bạn có muốn tiếp tục không?");
								result.put(ERROR, true);
								result.put("errMsg", sb.toString());
								return JSON;
							}*/
							Staff u = staffMgr.getStaffByCode(currentUser.getUserName());
							//		    				if(customer.getShop()!= null && u!=null && customer.getShop().getId()!= u.getShop().getId()){
							if (customer.getShop() != null && u != null && (!shopMgr.isChild(shop.getId(),customer.getShop().getId()) &&  !customer.getShop().getId().equals(shop.getId()))) {
								result.put(ERROR, error);
								errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.customermanager.error.notInCurrentShop");
								result.put("errMsg", errMsg);
								return JSON;
							}
							//		    				if(!ActiveType.RUNNING.getValue().equals(customer.getStatus().getValue())){
							//		    					result.put(ERROR, error);
							//		    					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"comon.catalog.customer.not.active");
							//		    				    result.put("errMsg", errMsg);
							//		    					return JSON;
							//		    				}
							if (customer.getShop() != null && customer.getShop().getId() != shop.getId() && ActiveType.STOPPED.getValue() == shop.getStatus().getValue()) {
								errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
							} else {
								customer.setShop(shop);
								customer.setCustomerName(customerName);
								customer.setContactName(contact);
								customer.setIdno(idNumber);
								if (!StringUtil.isNullOrEmpty(idNumber) && customerMgr.checkIfCustomerExists(null, null, null, idNumber, id, null)) {
									result.put(ERROR, true);
									errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.exist", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.id.number"));
									result.put("errMsg", errMsg);
									return JSON;
								}
								customer.setPhone(phone);
								customer.setMobiphone(mobilePhone);
								customer.setEmail(email);
								ActiveType currentCustomerStatus = customer.getStatus();
								if ((ActiveType.REJECTED == currentCustomerStatus || ActiveType.WAITING == currentCustomerStatus)
										&& currentCustomerStatus != customerStatus) {
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("catalog.customer.edit.status.rejected.or.waiting.not.allow"));
									return JSON;
								}
								boolean isValidNewCustomerStatus = (currentCustomerStatus == customerStatus)
																|| (currentCustomerStatus == ActiveType.STOPPED && customerStatus == ActiveType.RUNNING)
																|| (currentCustomerStatus == ActiveType.RUNNING && customerStatus == ActiveType.STOPPED);
								if (!isValidNewCustomerStatus) {
									result.put(ERROR, true);
									result.put("errMsg", R.getResource("catalog.customer.edit.status.running.or.waiting.invalid"));
									return JSON;
								}
								customer.setStatus(customerStatus);									
								customer.setMaxDebitAmount(maxDebit);
								customer.setMaxDebitDate(debitDate);
								customer.setApplyDebitLimited(applyDebitLimited);
								Date birthDay = !StringUtil.isNullOrEmpty(birthDayStr) ? DateUtil.parse(birthDayStr, DateUtil.DATE_FORMAT_DDMMYYYY) : null;
								Date startDate = !StringUtil.isNullOrEmpty(startDateStr) ? DateUtil.parse(startDateStr, DateUtil.DATE_FORMAT_DDMMYYYY) : null;
								Date endDate = !StringUtil.isNullOrEmpty(endDateStr) ? DateUtil.parse(endDateStr, DateUtil.DATE_FORMAT_DDMMYYYY) : null;
								customer.setBirthDay(birthDay);
								customer.setOpenDate(startDate);
								customer.setCloseDate(endDate);
								customer.setFrequency(tansuat);
								customer.setInvoiceNameBranchBank(invoiceNameBranchBank);
								customer.setBankAccountOwner(bankAccountOwner);
								if (routingId != null) {
									Routing routing = superviserMgr.getRoutingById(routingId);
									customer.setRouting(routing);
								} else {
									customer.setRouting(null);
								}
								
								Area area = areaMgr.getAreaById(areaId);
								if (area != null) {
									address = "";
									if (!StringUtil.isNullOrEmpty(addressNumber)) {
										customer.setHousenumber(addressNumber);
										address = address.trim() + addressNumber;
									} else {
										customer.setHousenumber(null);
									}
									if (!StringUtil.isNullOrEmpty(street)) {
										customer.setStreet(street);
										address = address.trim() + " " + street;
									} else {
										customer.setStreet(null);
									}
									
									if (!StringUtil.isNullOrEmpty(address)) {
										//address = address.trim() + ", "+ area.getAreaName() +", "+ area.getDistrictName() +", "+ area.getProvinceName();
										address = address.trim() + ", " + area.getAreaName() + (area != null ? ", " + area.getParentArea().getAreaName() : "")
												+ (area.getParentArea() != null && area.getParentArea().getParentArea() != null ? ", " + area.getParentArea().getParentArea().getAreaName() : "");
									} else {
										address = area.getAreaName() + ", " + area.getDistrictName() + ", " + area.getProvinceName();
									}
									customer.setAddress(address.trim());
									customer.setArea(area);
								}

								customer.setUpdateUser(getCurrentUser().getUserName());
								customer.setUpdateDate(commonMgr.getSysDate());
								if (!StringUtil.isNullOrEmpty(lat) && !StringUtil.isNullOrEmpty(lng) && !lat.equals("null") && !lng.equals("null")) {
									Double __lat = Double.valueOf(lat);
									Double __lng = Double.valueOf(lng);
									customer.setLat(__lat);
									customer.setLng(__lng);
								} else {
									// xoa vi tri KH
									customer.setLat(null);
									customer.setLng(null);
								}
								String eMsg = this.saveProperty(customer);
								if (!StringUtil.isNullOrEmpty(eMsg)) {
									result.put(ERROR, true);
									result.put("errMsg", eMsg);
									return JSON;
								}
								customerMgr.updateCustomer(customer, currentUser != null ? currentUser.getUserName() : null, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, getLogInfoVO());
								error = false;
								result.put("id", id);
								result.put("code", customer.getShortCode());
							}
						}

					} else {
						if (ActiveType.STOPPED.getValue() == shop.getStatus().getValue()) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
						} else {
							Customer newCustomer = new Customer();
							newCustomer.setCustomerName(customerName);
							newCustomer.setContactName(contact);
							newCustomer.setIdno(idNumber);
							newCustomer.setPhone(phone);
							newCustomer.setMobiphone(mobilePhone);
							newCustomer.setEmail(email);
							if (ActiveType.RUNNING != customerStatus && ActiveType.STOPPED != customerStatus) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("catalog.customer.create.invalid.status"));
								return JSON;
							}
							newCustomer.setStatus(ActiveType.parseValue(status));
							newCustomer.setMaxDebitAmount(maxDebit);
							newCustomer.setMaxDebitDate(debitDate);
							//newCustomer.setShortCode(customerCode);
							newCustomer.setApplyDebitLimited(applyDebitLimited);
							Date birthDay = !StringUtil.isNullOrEmpty(birthDayStr) ? DateUtil.parse(birthDayStr, DateUtil.DATE_FORMAT_DDMMYYYY) : null;
							Date startDate = !StringUtil.isNullOrEmpty(startDateStr) ? DateUtil.parse(startDateStr, DateUtil.DATE_FORMAT_DDMMYYYY) : null;
							Date endDate = !StringUtil.isNullOrEmpty(endDateStr) ? DateUtil.parse(endDateStr, DateUtil.DATE_FORMAT_DDMMYYYY) : null;
							newCustomer.setBirthDay(birthDay);
							newCustomer.setOpenDate(startDate);
							newCustomer.setCloseDate(endDate);
							newCustomer.setFrequency(tansuat);
							newCustomer.setInvoiceNameBranchBank(invoiceNameBranchBank);
							newCustomer.setBankAccountOwner(bankAccountOwner);
							if (routingId != null) {
								Routing routing = superviserMgr.getRoutingById(routingId);
								newCustomer.setRouting(routing);
							}
							Area area = areaMgr.getAreaById(areaId);
							if (area == null) {
								result.put(ERROR, true);
								result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true, "catalog.area.tree"));
								return JSON;
							}
							if (!AreaType.WARD.getValue().equals(area.getType().getValue())) {
								result.put(ERROR, true);
								result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.unit.tree.area.is.ward"));
								return JSON;
							}
							newCustomer.setArea(area);
							if (area != null) {
								address = "";
								if (!StringUtil.isNullOrEmpty(addressNumber)) {
									newCustomer.setHousenumber(addressNumber);
									address = address.trim() + addressNumber;
								}
								if (!StringUtil.isNullOrEmpty(street)) {
									newCustomer.setStreet(street);
									address = address.trim() + " " + street;
								}
								if (!StringUtil.isNullOrEmpty(address)) {
									address = address.trim() + ", " + area.getAreaName() + ", " + area.getDistrictName() + ", " + area.getProvinceName();
								} else {
									address = area.getAreaName() + ", " + area.getDistrictName() + ", " + area.getProvinceName();
								}
								newCustomer.setAddress(address.trim());
								newCustomer.setArea(area);
							}
							newCustomer.setCreateUser(getCurrentUser().getUserName());
							newCustomer.setShop(shop);

							if (!StringUtil.isNullOrEmpty(lat) && !StringUtil.isNullOrEmpty(lng)) {
								Double __lat = Double.valueOf(lat);
								Double __lng = Double.valueOf(lng);
								newCustomer.setLat(__lat);
								newCustomer.setLng(__lng);

							}
							String eMsg = this.saveProperty(newCustomer);
							if (!StringUtil.isNullOrEmpty(eMsg)) {
								result.put(ERROR, true);
								result.put("errMsg", eMsg);
								return JSON;
							}
							newCustomer = customerMgr.createCustomer(newCustomer, currentUser != null ? currentUser.getUserName() : null, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, getLogInfoVO());
							if (newCustomer != null) {
								error = false;
								id = newCustomer.getId();//de them vao property
								result.put("id", newCustomer.getId());
								result.put("code", newCustomer.getShortCode());
							}
						}
					}
				}

			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Xoa vi tri KH
	 * @author vuongmq
	 * @return String
	 * @since 09/10/2015
	 */
	public String deleteCustomerPosition() {
		resetToken(result);
		Date startLogDate = DateUtil.now();
		result.put(ERROR, false);
		try {
			if (currentUser != null && !StringUtil.isNullOrEmpty(currentUser.getUserName())) {
				if (id != null && id > 0) {
					Customer customer = customerMgr.getCustomerById(id);
					if (customer != null) {
						if (customer.getShop() != null) {
							if (super.getMapShopChild().get(customer.getShop().getId()) == null) {
                                result.put(ERROR, true);
                                result.put("errMsg", R.getResource("common.cms.shop.undefined"));
                                return JSON;
							}
							if (customer.getLat() == null && customer.getLng() == null) {
								result.put(ERROR, true);
                                result.put("errMsg", R.getResource("catalog.customer.position.null"));
                                return JSON;
							}
							// xoa vi tri KH
							customer.setLat(null);
							customer.setLng(null);
							customer.setUpdateDate(DateUtil.now());
							customer.setUpdateUser(currentUser.getUserName());
							customerMgr.deleteCustomerPosition(customer, getLogInfoVO());
						} else {
							result.put(ERROR, true);
                            result.put("errMsg", R.getResource("sp.create.order.customer.is.not.in.shop"));
                            return JSON;
						}
					} else {
						result.put(ERROR, true);
                        result.put("errMsg", R.getResource("comon.not.exist", R.getResource("ss.manager.customer.title")));
                        return JSON;
					}
				} else {
					result.put(ERROR, true);
                    result.put("errMsg", R.getResource("comon.not.exist", R.getResource("ss.manager.customer.title")));
                    return JSON;
				}
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CustomerCatalogAction.deletePositionCustomer()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
            result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	/**
	 * Lay doi tuong config vi tri ban hang theo gia tri config
	 * 
	 * @author tuannd20
	 * @param salePositionValue gia tri config vi tri ban hang
	 * @return doi tuong config thong tin vi tri ban hang
	 * @throws BusinessException 
	 * @since 20/08/2015
	 */
	private ApParam getCustomerSalePositionConfig(Integer salePositionValue) throws BusinessException {
		ApParamFilter filter = new ApParamFilter();
		filter.setStatus(ActiveType.RUNNING);
		filter.setType(ApParamType.CUSTOMER_SALE_POSITION);
		if (salePositionValue != null) {
			filter.setValue(salePositionValue.toString());			
		}
		List<ApParam> apParams = apParamMgr.getListApParamByFilter(filter);
		return apParams != null && apParams.size() > 0 ? apParams.get(0) : null;
	}

	/**
	 * Save info.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since 03/08/2012
	 * 
	 * @author hunglm16
	 * @since November 17,2014
	 * @description Bo sung IsVAT (In hóa đơn VAT)
	 */
	public String saveProperty(Customer customer) {
		String errMsg = "";
		try {
			if (customer != null) {
				ChannelType custType = channelTypeMgr.getChannelTypeByCode(customerType, ChannelTypeType.CUSTOMER);
				if (custType == null) {
					return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.customer.type");
				}
				if (!ActiveType.RUNNING.getValue().equals(custType.getStatus().getValue())) {
					return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.customer.type");
				}
				Staff deliveryStaff = null;
				if (!StringUtil.isNullOrEmpty(deliveryCode)) {
					deliveryStaff = staffMgr.getStaffByCodeAndShopId(deliveryCode, customer.getShop().getId());
					if (deliveryStaff == null) {
						return ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "NVGH");
					}
					if (!ActiveType.RUNNING.getValue().equals(deliveryStaff.getStatus().getValue())) {
						return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "Mã NVGH");
					}
				}
				Staff cashierStaff = null;
				if (!StringUtil.isNullOrEmpty(cashierCode)) {
					cashierStaff = staffMgr.getStaffByCodeAndShopId(cashierCode, customer.getShop().getId());
					if (cashierStaff == null) {
						return ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST_DB, "NVTT");
					}
					if (!ActiveType.RUNNING.getValue().equals(cashierStaff.getStatus().getValue())) {
						return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "Mã NVTT");
					}
				}
				
				if (!IsAttribute.checkIsRecode(isVat)) {
					return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.catalog.customer.isVat.undefined");
				}
				customer.setIsVat(IsAttribute.parseValue(isVat));
				customer.setChannelType(custType);
				customer.setDeliver(deliveryStaff);
				customer.setCashierStaff(cashierStaff);
				customer.setApplyDebitLimited(applyDebitLimited);
				customer.setMaxDebitAmount(maxDebit);
				customer.setMaxDebitDate(debitDate);
				customer.setInvoiceConpanyName(invoiceConpanyName);
				customer.setInvoiceOutletName(invoiceOutletName);
				customer.setDeliveryAddress(custDeliveryAddr);
				customer.setInvoiceTax(invoiceTax);
				customer.setInvoiceNumberAccount(invoiceNumberAccount);
				customer.setInvoiceNameBank(invoiceNameBank);
				
				if (customerSalePosition != null) {
					ApParam customerSalePositionConfig = this.getCustomerSalePositionConfig(customerSalePosition);
					if (customerSalePositionConfig != null) {
						customer.setSalePosition(customerSalePositionConfig);
					} else {
						return R.getResource("customer.info.sale.position.value.invalid");
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof IllegalArgumentException) {
				LogUtility.logError(e, e.getMessage());
				errMsg = e.getMessage();
			} else {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		return errMsg;
	}

	/**
	 * Save info.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since 03/08/2012
	 * 
	 * @author hunglm16
	 * @since February 20, 2014
	 * @description Update authorization for GSNPP
	 */
	public String saveProductInfo() throws Exception {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				ShopToken shToken = currentUser.getShopRoot();
				Shop shop = null;
				if (shToken != null) {
					shop = shopMgr.getShopById(shToken.getShopId());
				}

				if (shop == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(idNumber) && !ValidateUtil.validateNumber(idNumber)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NUMBER, false, "common.id.number");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(mobilePhone) && !ValidateUtil.validateNumber(mobilePhone)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NUMBER, false, "common.mobiphone");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(idNumber) && customerMgr.checkIfCustomerExists(null, null, null, idNumber, id, null)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "common.id.number");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(email) && customerMgr.checkIfCustomerExists(email, null, null, null, id, null)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "common.email");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(mobilePhone) && customerMgr.checkIfCustomerExists(null, mobilePhone, null, null, id, null)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "common.mobiphone");
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(customerName, "catalog.customer.import.customername", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(contact, "catalog.customer.import.contactname", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(idNumber, "common.id.number", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(phone, "catalog.customer.phone", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(mobilePhone, "catalog.customer.mobile", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(email)) {
					errMsg = ValidateUtil.validateField(email, "common.email", null, ConstantManager.ERR_INVALID_EMAIL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(addressNumber, "catalog.customer.house.number", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(street, "catalog.customer.street", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(address, "catalog.address", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					AreaType areaType = null;
					if (!StringUtil.isNullOrEmpty(provinceCode)) {
						areaType = AreaType.PROVINCE;
						if (!StringUtil.isNullOrEmpty(districtCode)) {
							areaType = AreaType.DISTRICT;
							if (!StringUtil.isNullOrEmpty(wardCode)) {
								areaType = AreaType.WARD;
							}
						}
					}
					Area area = null;
					ObjectVO<Area> areaVO = areaMgr.getListArea(null, null, null, null, null, provinceCode, null, districtCode, null, wardCode, null, areaType, null,null,null);
					if (areaVO != null && areaVO.getLstObject() != null && areaVO.getLstObject().size() > 0) {
						area = areaVO.getLstObject().get(0);
					}

					if (id != null && id > 0) {
						Customer customer = customerMgr.getCustomerById(id);
						if (customer != null) {
							if (customer.getShop() != null && customer.getShop().getId() != shop.getId() && ActiveType.STOPPED.getValue() == shop.getStatus().getValue()) {
								errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
							} else {
								customer.setShop(shop);
								customer.setCustomerName(customerName);
								customer.setContactName(contact);
								customer.setIdno(idNumber);
								customer.setPhone(phone);
								customer.setMobiphone(mobilePhone);
								customer.setEmail(email);
								customer.setStatus(ActiveType.parseValue(status));
								customer.setArea(area);
								customer.setStreet(street);
								customer.setAddress(address);
								customer.setUpdateUser(getCurrentUser().getUserName());
								customer.setUpdateDate(commonMgr.getSysDate());
								customer.setHousenumber(addressNumber);
								customerMgr.updateCustomer(customer, null, null, null, null, getLogInfoVO());
								error = false;
								result.put("id", id);
								result.put("code", customer.getShortCode());
							}
						}

					} else {
						if (ActiveType.STOPPED.getValue() == shop.getStatus().getValue()) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
						} else {
							Customer newCustomer = new Customer();
							newCustomer.setCustomerName(customerName);
							newCustomer.setContactName(contact);
							newCustomer.setIdno(idNumber);
							newCustomer.setPhone(phone);
							newCustomer.setMobiphone(mobilePhone);
							newCustomer.setEmail(email);
							newCustomer.setStatus(ActiveType.parseValue(status));
							newCustomer.setArea(area);
							newCustomer.setStreet(street);
							newCustomer.setAddress(address);
							newCustomer.setCreateUser(getCurrentUser().getUserName());
							newCustomer.setShop(shop);
							newCustomer.setHousenumber(addressNumber);
							newCustomer = customerMgr.createCustomer(newCustomer, null, null, null, null, getLogInfoVO());
							if (newCustomer != null) {
								error = false;
								result.put("id", newCustomer.getId());
								result.put("code", newCustomer.getShortCode());
							}
						}
					}
				}

			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Save bill.
	 * 
	 * @return the string
	 */
	public String saveInvoice() {

		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(invoiceTax) && !ValidateUtil.validateNumber(invoiceTax)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NUMBER, false, "common.tax.code");
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(invoiceNumberAccount) && !ValidateUtil.validateNumber(invoiceNumberAccount)) {
					errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NUMBER, false, "common.account.number");
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					Customer customer = customerMgr.getCustomerById(id);
					if (customer != null) {
						if (!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.customer");
						} else {
							customer.setInvoiceConpanyName(invoiceConpanyName);
							customer.setInvoiceOutletName(invoiceOutletName);
							customer.setDeliveryAddress(custDeliveryAddr);
							customer.setInvoiceTax(invoiceTax);
							customer.setInvoiceNumberAccount(invoiceNumberAccount);
							customer.setInvoiceNameBank(invoiceNameBank);
							customerMgr.updateCustomer(customer, null, null, null, null, getLogInfoVO());
							error = false;
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Search sale order.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since 06/08/2012
	 */
	public String searchSaleOrder() {

		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(fromDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.fromDate"), null);
				if (StringUtil.isNullOrEmpty(checkMessage)) {
					fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					return JSON;
				}
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(toDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.toDate"), null);
				if (StringUtil.isNullOrEmpty(checkMessage)) {
					tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					return JSON;
				}
			}
			ObjectVO<SaleOrder> salesOrderVO = salesOrderMgr.getListSaleOrder(kPaging, shopCode, id, null, fDate, tDate, SaleOrderStatus.APPROVED, OrderType.IN, null);
			if (salesOrderVO != null) {
				result.put("total", salesOrderVO.getkPaging().getTotalRows());
				result.put("rows", salesOrderVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Search sale level cat.
	 * 
	 * @return the string
	 * @author hungtx
	 */
	public String searchSaleLevelCat() {

		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<CustomerCatLevel> kPaging = new KPaging<CustomerCatLevel>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<CustomerCatLevel> saleLevelCatVO = customerMgr.getListCustomerCatLevel(kPaging, id, null, true);
			if (saleLevelCatVO != null) {
				result.put("total", saleLevelCatVO.getkPaging().getTotalRows());
				result.put("rows", saleLevelCatVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Save level cat.
	 * 
	 * @return the string
	 * @author hungtx
	 */
	public String saveLevelCat() {

		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				boolean exist = customerMgr.checkIfCustomerCatLevelRecordExist(id, saleLevelCat, saleLevelCatId);
				SaleLevelCat saleLevel = saleLevelCatMgr.getSaleLevelCatById(saleLevelCat);
				CustomerCatLevel cusCatLevel = null;
				if (saleLevel != null && saleLevel.getCat() != null) {
					cusCatLevel = customerMgr.getCustomerCatLevel(id, saleLevel.getCat().getId());
				}
				if (exist) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.level.cat.exist");
				} else if (((saleLevelCatId == null || saleLevelCatId == 0) && cusCatLevel != null)
						|| (saleLevelCatId != null && cusCatLevel != null && cusCatLevel.getSaleLevelCat() != null && cusCatLevel.getSaleLevelCat().getId().equals(saleLevelCatId))) {
					errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.cat.exist");
				} else {
					Customer customer = customerMgr.getCustomerById(id);
					if (customer != null) {
						if (!customer.getStatus().getValue().equals(ActiveType.RUNNING.getValue())) {
							errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, false, "catalog.customer");
						} else {
							if (saleLevelCatId != null && saleLevelCatId > 0) {
								CustomerCatLevel customerCatLevel = customerMgr.getCustomerCatLevelById(saleLevelCatId);
								if (customerCatLevel != null) {
									customerCatLevel.setSaleLevelCat(saleLevel);
									customerMgr.updateCustomerCatLevel(customerCatLevel, getLogInfoVO());
									error = false;
									result.put("id", saleLevelCatId);
								}
							} else {
								CustomerCatLevel customerCatLevel = new CustomerCatLevel();
								customerCatLevel.setSaleLevelCat(saleLevel);
								customerCatLevel.setCustomer(customer);
								customerCatLevel = customerMgr.createCustomerCatLevel(customerCatLevel, getLogInfoVO());
								if (customerCatLevel != null) {
									error = false;
									result.put("id", customerCatLevel.getId());
								}
							}
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Delete level cat.
	 * 
	 * @return the string
	 * @author hungtx
	 */
	public String deleteLevelCat() {

		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				CustomerCatLevel catLevel = customerMgr.getCustomerCatLevelById(saleLevelCatId);
				if (catLevel != null) {
					customerMgr.deleteCustomerCatLevel(catLevel, getLogInfoVO());
					error = false;
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Search info change.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since Aug 24, 2012
	 */
	public String searchInfoChange() {

		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<ActionAudit> kPaging = new KPaging<ActionAudit>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			Date fDate = null;
			Date tDate = null;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(fromDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.fromDate"), null);
				if (StringUtil.isNullOrEmpty(checkMessage)) {
					fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					return JSON;
				}
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(toDate, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.display.program.toDate"), null);
				if (StringUtil.isNullOrEmpty(checkMessage)) {
					tDate = DateUtil.parse(toDate, DateUtil.DATE_FORMAT_DDMMYYYY);
				} else {
					return JSON;
				}
			}
			if (id != null) {
				ObjectVO<ActionAudit> actionGeneralLogVO = logMgr.getListActionGeneralLog(kPaging, null, id, null, fDate, tDate);
				if (actionGeneralLogVO != null) {
					result.put("total", actionGeneralLogVO.getkPaging().getTotalRows());
					result.put("rows", actionGeneralLogVO.getLstObject());
				}
			}

		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Search info change detail.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since Aug 24, 2012
	 */
	public String searchInfoChangeDetail() {

		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<ActionAuditDetail> kPaging = new KPaging<ActionAuditDetail>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			ObjectVO<ActionAuditDetail> cctionGeneralLogDetailVO = logMgr.getListActionGeneralLogDetail(kPaging, id);
			if (cctionGeneralLogDetailVO != null) {
				result.put("total", cctionGeneralLogDetailVO.getkPaging().getTotalRows());
				result.put("rows", cctionGeneralLogDetailVO.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Import level cat.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since Aug 27, 2012
	 */
	private String importLevelCat() throws Exception {
		resetToken(result);
		isError = true;
		totalItem = 0;
		String message = "";
		listSubcat = new ArrayList<CellBean>();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, 4);
		Shop shop = null;
		if (StringUtil.isNullOrEmpty(errMsg)) {
			ShopToken shToken = currentUser.getShopRoot();
			if (shToken != null) {
				shop = shopMgr.getShopById(shToken.getShopId());
			}

			if (shop == null) {
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				for (int i = 0; i < lstData.size(); i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						message = "";
						totalItem++;
						Customer temp = null;
						List<String> row = lstData.get(i);

						// customer code
						if (row.size() > 0) {
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "catalog.customer.code", 3, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (!StringUtil.isNullOrEmpty(msg)) {
								message += msg;
							} else {
								String customerCode = "";
								if (shop != null) {
									customerCode = StringUtil.getFullCode(shop.getShopCode(), value);
								}
								if (!StringUtil.isNullOrEmpty(value)) {
									try {
										temp = customerMgr.getCustomerByCode(customerCode);
									} catch (Exception e) {
										LogUtility.logError(e, e.getMessage());
									}
									if (temp == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.code");
									} else if (shop != null && (temp.getShop() == null || (temp.getShop() != null && !shop.getId().equals(temp.getShop().getId())))) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.customer.not.exist.shop", temp.getShortCode(), shop.getShopCode()) + "\n";
									} else if (ActiveType.STOPPED.getValue().equals(temp.getStatus().getValue())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.customer.code");
									}
								}
							}
						}

						ProductInfo cat = null;
						// cat
						if (row.size() > 1) {
							String value = row.get(1);
							if (StringUtil.isNullOrEmpty(value)) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.customer.cat");
							} else {
								cat = productInfoMgr.getProductInfoByCode(value, ProductType.CAT, null, null);
								if (cat == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.cat");
								} else if (ActiveType.STOPPED.getValue().equals(cat.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.customer.cat");
								} else if (temp != null && customerMgr.getCustomerCatLevel(temp.getId(), cat.getId()) != null) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.cat.exist");
									message += "\n";
								}
							}

						}

						//level
						SaleLevelCat saleLevelCat = null;
						if (row.size() > 2) {
							String value = row.get(2);
							if (StringUtil.isNullOrEmpty(value)) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, "catalog.level.code");
							} else if (cat != null) {
								saleLevelCat = saleLevelCatMgr.getSaleLevelCatByCatAndSaleLevel(cat.getId(), value);
								if (saleLevelCat == null) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.level.code");
								} else if (ActiveType.STOPPED.getValue().equals(saleLevelCat.getStatus().getValue())) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.level.code");
								} else if (temp != null && customerMgr.checkIfCustomerCatLevelRecordExist(temp.getId(), saleLevelCat.getId(), null)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.level.cat.exist");
									message += "\n";
								}
							}
						}
						if (isView == 0) {
							typeView = false;
							if (StringUtil.isNullOrEmpty(message)) {
								CustomerCatLevel customerCatLevel = new CustomerCatLevel();
								customerCatLevel.setSaleLevelCat(saleLevelCat);
								customerCatLevel.setCustomer(temp);
								customerCatLevel = customerMgr.createCustomerCatLevel(customerCatLevel, getLogInfoVO());
							} else {
								lstFails.add(StringUtil.addFailBean(row, message));
							}
						} else {
							typeView = true;
							lstFails.add(StringUtil.addFailBean(row, message));
							listSubcat.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_CUSTOMER_SUBCAT_FAIL);
			} catch (Exception e) {
				//LogUtility.logError(e, e.getMessage());
				//errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				throw e;
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Import excel.
	 * 
	 * @return the string
	 * @author hungtx
	 * @since Aug 27, 2012
	 */
	public String importExcel() throws Exception {
		try {
			switch (excelType) {
			case IMPORT_EXCEL_CUSTOMER:
				return importCustomer();
			case IMPORT_EXCEL_CAT:
				return importLevelCat();
			default:
				break;
			}
			return importLevelCat();
		} catch (Exception ex) {
			LogUtility.logError(ex, "CustomerCatalogAction.importExcel - " + ex.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		isError = true;
		return SUCCESS;
	}

	/**
	 * Xem hinh anh khach hang.
	 * 
	 * @return the string
	 * @author tungmt
	 * @since Oct 9, 2012
	 */
	public String customerImageView() {
		try {
			if (checkRoles(VSARole.VNM_SALESONLINE_GS))
				roleViewImageCustomer = 1;
			else {
				roleViewImageCustomer = 0;
				return SUCCESS;
			}

			Customer customer = customerMgr.getCustomerById(customerId);
			sysDate = commonMgr.getSysDate();
			if (customer != null) {
				customerCode = customer.getShortCode();
				customerName = customer.getCustomerName();
				listMediaItemStore = superviserMgr.getListMediaItemByObjectIdAndObjectType(customer.getId(), MediaObjectType.IMAGE_STORE);
				if (listMediaItemStore != null && listMediaItemStore.size() > 0)
					mediaItemStore = listMediaItemStore.get(0);

				listMediaItemDisplay = superviserMgr.getListMediaItemByObjectIdAndObjectType(customer.getId(), MediaObjectType.IMAGE_DISPLAY);
				if (listMediaItemDisplay != null && listMediaItemDisplay.size() > 0)
					mediaItemDisplay = listMediaItemDisplay.get(0);

				listMediaItemSalePoint = superviserMgr.getListMediaItemByObjectIdAndObjectType(customer.getId(), MediaObjectType.IMAGE_SALE_POINT);
				if (listMediaItemSalePoint != null && listMediaItemSalePoint.size() > 0)
					mediaItemSalePoint = listMediaItemSalePoint.get(0);

				//Hien thi thong bao neu chua co hinh nao
				status = 1;
				if (mediaItemSalePoint == null && mediaItemDisplay == null && mediaItemStore == null)
					status = 0;
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.manager.customer.title")));
				return ERROR;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ManageCustomer.execute: " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		result.put(ERROR, false);
		return SUCCESS;
	}

	/**
	 * Xem hinh anh khach hang.
	 * 
	 * @return the string
	 * @author tungmt
	 * @since Oct 9, 2012
	 */
	public String customerImageViewDetail() {
		try {
			if (!checkRoles(VSARole.VNM_SALESONLINE_GS)) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.view.image.title")));
				return ERROR;
			}

			Customer customer = customerMgr.getCustomerById(customerId);
			MediaObjectType mot = MediaObjectType.parseValue(objectType);
			if (customer != null) {
				listMediaItem = superviserMgr.getListMediaItemByObjectIdAndObjectType(customer.getId(), mot);
				if (listMediaItem != null && listMediaItem.size() > 0)
					mediaItem = listMediaItem.get(0);

				if (mediaItem == null) {
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.manager.customer.image.title")));
					return ERROR;
				}
				result.put("list", listMediaItem);
				result.put("item", mediaItem);
			} else {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_NOT_EXIST, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.manager.customer.title")));
				return ERROR;
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ManageCustomer.execute: " + e.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		result.put(ERROR, false);
		return JSON;
	}

	public void setShopIdHidden(Long shopIdHidden) {
		this.shopIdHidden = shopIdHidden;
	}

	public Long getShopIdHidden() {
		return shopIdHidden;
	}

	/**
	 * exportExecl customer.
	 * 
	 * @return the string
	 * @author huynp4
	 * @since 07/01/2013
	 */
	public String exportExeclCustomer() throws Exception {
		try {
//			ShopToken shToken = (currentUser!=null)?currentUser.getShopRoot():null;
//			
//
//			if(shopCode == null || shopCode.isEmpty()) {
//				shToken = currentUser.getShopRoot();
//			} else {
//				shToken = getShopTockenInOrgAccessByCode(shopCode);
//			}
//			Shop currentShop = null;
//			if (shToken != null) {
//				currentShop = shopMgr.getShopById(shToken.getShopId());
//			}
//			shopId = currentShop != null ? currentShop.getId() : null;
//
//			ParamsDAOGetListCustomer params = new ParamsDAOGetListCustomer();
//			//params.setkPaging(kPaging);
//			params.setShortCode(customerCode);
//			params.setCustomerName(customerName);
//			params.setAllSubShop(true);
//			params.setMobiPhone(phone);
//			params.setStatus(ActiveType.parseValue(status));
//			params.setAddress(address);
//			params.setShopId(shopId);
//			if (!StringUtil.isNullOrEmpty(customerType)) {
//				params.setCustomerTypeCode(customerType);
//			}
			
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			page=1;
			rows=65000;
			search();
			List<Customer> lstCustomer =(List<Customer>) result.get("rows") ;

			List<CellBean> lstFails = new ArrayList<CellBean>();
			if (lstCustomer == null || lstCustomer.isEmpty()) {
				result.put("errMsg", R.getResource("err.msg.not.get.all"));
				result.put("hasData", false);
			} else {
				//Put du lieu				
				 {
//					int size = Math.min(lstCustomer.size(),64995);
					int size = lstCustomer.size();
					
					String[] strStatus=new String[]{R.getResource("jsp.common.status.stoped"), R.getResource("jsp.common.status.active"), R.getResource("jsp.common.status.draft"), R.getResource("common.status.reject")};
					String[] strVAT = new String[]{R.getResource("catalog.customer.is.vat"),R.getResource("catalog.customer.not.vat")};
					for (int i = 0; i < size; i++) {
						Customer temp = lstCustomer.get(i);//i=65000
						CellBean cellBean = new CellBean();
						//						if(temp.getShop() != null){
						//							cellBean.setContent1(temp.getShop().getShopCode());
						//						}
						if (temp.getShop() != null) {
							cellBean.setContent2(temp.getShop().getShopCode());
						} else {
							cellBean.setContent2("");
						}
						cellBean.setContent1(String.valueOf(i+1));
						cellBean.setContent3(temp.getShortCode());
						cellBean.setContent4(temp.getCustomerName());
						if (temp.getChannelType() != null) {
							cellBean.setContent5(temp.getChannelType().getChannelTypeCode());
						}
						
						cellBean.setContent6((temp.getStatus().getValue()!=null && temp.getStatus().getValue()<strStatus.length)?strStatus[temp.getStatus().getValue()]:"");
						cellBean.setContent7(temp.getContactName());
						cellBean.setContent8(temp.getEmail());
						cellBean.setContent9(temp.getPhone());
						cellBean.setContent10(temp.getMobiphone());
						cellBean.setContent11(temp.getFax());
						if (temp.getArea() != null) {
							cellBean.setContent12(temp.getArea().getAreaCode());
						}
						cellBean.setContent13(temp.getStreet());
						cellBean.setContent14(temp.getHousenumber());
						cellBean.setContent15((temp.getBirthDay() != null) ? DateUtil.toDateString(temp.getBirthDay()) : null);
						cellBean.setContent16(temp.getFrequency() != null ? temp.getFrequency().toString() : "");
						cellBean.setContent17(temp.getRouting() != null ? temp.getRouting().getRoutingCode() : "");
						if (temp.getLat() != null) {
							cellBean.setContent18(temp.getLat().toString());
						}
						if (temp.getLng() != null) {
							cellBean.setContent19(temp.getLng().toString());
						}
						if (temp.getDeliver() != null) {
							cellBean.setContent20(temp.getDeliver().getStaffCode());
						}
						if (temp.getCashierStaff() != null) {
							cellBean.setContent21(temp.getCashierStaff().getStaffCode());
						}
						cellBean.setContent22((temp.getMaxDebitAmount() != null) ? temp.getMaxDebitAmount().toString() : null);
						cellBean.setContent23((temp.getMaxDebitDate() != null) ? temp.getMaxDebitDate().toString() : null);
						cellBean.setContent24(temp.getInvoiceTax());
						cellBean.setContent25(temp.getInvoiceNumberAccount());
						cellBean.setContent26(temp.getInvoiceOutletName());
						cellBean.setContent27(temp.getInvoiceConpanyName());
						cellBean.setContent28(temp.getInvoiceNameBank());
						cellBean.setContent29(temp.getInvoiceNameBranchBank());
						cellBean.setContent30(temp.getBankAccountOwner());
						cellBean.setContent31(temp.getDeliveryAddress());
						cellBean.setContent32((temp.getOpenDate() != null) ? DateUtil.toDateString(temp.getOpenDate()) : null);
						cellBean.setContent33((temp.getCloseDate() != null) ? DateUtil.toDateString(temp.getCloseDate()) : null);
						cellBean.setContent34((temp.getIsVat()!=null && temp.getIsVat().getValue()<strVAT.length)?strVAT[temp.getIsVat().getValue()]:"");

						cellBean.setContent35(temp.getShop().getShopName());
						String regionStr = "";
						String areaStr = "";
						String wardCode = "";
						String ward = "";
						String districtCode = "";
						String district = "";
						String proCode = "";
						String pro = "";
						String cusType = "";
						String status = "";
						
						if (temp.getShop() != null && temp.getShop().getParentShop() != null) {
							areaStr = temp.getShop().getParentShop().getShopName();
						}
						if (temp.getShop() != null && temp.getShop().getParentShop() != null && temp.getShop().getParentShop().getParentShop() != null) {
							regionStr = temp.getShop().getParentShop().getParentShop().getShopName();
						}
						if (temp.getArea() != null) {
							Area a = temp.getArea();
							wardCode = a.getPrecinct();
							ward = a.getPrecinctName();
							districtCode = a.getDistrict();
							district = a.getDistrictName();
							proCode = a.getProvince();
							pro = a.getProvinceName();
						}
						if (temp.getChannelType() != null) {
							cusType = temp.getChannelType().getChannelTypeName();
						}
						if (ActiveType.RUNNING.equals(temp.getStatus())) {
							status = R.getResource("jsp.common.status.active");
						} else if (ActiveType.STOPPED.equals(temp.getStatus())) {
							status = R.getResource("jsp.common.status.stoped");
						}
						
						cellBean.setContent36(regionStr);
						cellBean.setContent37(areaStr);
						cellBean.setContent38(wardCode);
						cellBean.setContent39(ward);
						cellBean.setContent40(districtCode);
						cellBean.setContent41(district);
						cellBean.setContent42(proCode);
						cellBean.setContent43(pro);
						cellBean.setContent44(cusType);
						cellBean.setContent45(status);
						
						lstFails.add(cellBean);
					}
				}
				//				session.setAttribute(ConstantManager.USER_SESSION_KEY,lstFails);
				//				result.put(ERROR, false);
				if (lstFails.size() == 0) {
					CellBean cellBean = new CellBean();

					cellBean.setContent1(R.getResource("price.manage.err.export.isnull"));
					cellBean.setContent2("");
					cellBean.setContent3("");
					cellBean.setContent4("");
					cellBean.setContent5("");
					cellBean.setContent6("");
					cellBean.setContent7("");
					cellBean.setContent8("");
					cellBean.setContent9("");
					cellBean.setContent10("");
					cellBean.setContent11("");
					cellBean.setContent12("");
					cellBean.setContent13("");
					cellBean.setContent14("");
					cellBean.setContent15("");
					cellBean.setContent16("");
					cellBean.setContent17("");
					cellBean.setContent18("");
					cellBean.setContent19("");
					cellBean.setContent20("");
					cellBean.setContent21("");
					cellBean.setContent22("");
					cellBean.setContent23("");
					lstFails.add(cellBean);
				}
				
				Map<String, Object> param = new HashMap<String, Object>();
				param = initCustomerImportTempateHeader(); 
				param.put("lstData", lstFails);
				exportExcelJxls(param, Configuration.getFailDataPath(), ConstantManager.TEMPLATE_CATALOG_CUSTOMER_EXPORT);
				
//				exportExcelDataNotSessionXLS(param,lstFails, ConstantManager.TEMPLATE_CATALOG_CUSTOMER_EXPORT_JASPER);
//				exportExcelDataNotSession(lstFails, ConstantManager.TEMPLATE_CATALOG_CUSTOMER_EXPORT);
			}

		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "CustomerCatalogAction.exportExcelCustomer - " + e.getMessage());
		}
		return JSON;
	}
	
	/**
	 * ham xu ly request export khach hang
	 * @author tuannd20
	 * @return
	 * @since 04/09/2015
	 */
	public String exportCustomerHandler() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			List<CustomerExportVO> customers = getCustomerWithDynamicAttributeInfo();
			setCustomerFieldText(customers);
			String outputPath = exportCustomer(customers);
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
			result.put("errMsg", R.getResource("system.error"));
		}
		return JSON;
	}
	
	/*
	 * Tim kiem khach hang voi thong tin thuoc tinh dong
	 */
	private List<CustomerExportVO> getCustomerWithDynamicAttributeInfo() throws BusinessException {
		Long shopId = getCurrentShopId(shopCode);
		ParamsDAOGetListCustomer params = getCustomerSearchParam(shopId);
		return customerMgr.getCustomerWithDynamicAttributeInfo(params);
	}
	
	/*
	 * set text cho cac field cua VO
	 */
	private void setCustomerFieldText(List<CustomerExportVO> customers) {
		for (CustomerExportVO customerExportVO : customers) {
			setCustomerStatusText(customerExportVO);
		}
	}
	
	/*
	 * set text cho truong trang thai
	 */
	private void setCustomerStatusText(CustomerExportVO customerExportVO) {
		String statusDescription = "";
		if (ActiveType.RUNNING.getValue().toString().equals(customerExportVO.getStatus())) {
			statusDescription = R.getResource("jsp.common.status.active");
		} else if (ActiveType.STOPPED.getValue().toString().equals(customerExportVO.getStatus())) {
			statusDescription = R.getResource("jsp.common.status.stoped");
		} else if (ActiveType.WAITING.getValue().toString().equals(customerExportVO.getStatus())) {
			statusDescription = R.getResource("jsp.common.status.draft");
		} else if (ActiveType.REJECTED.getValue().toString().equals(customerExportVO.getStatus())) {
			statusDescription = R.getResource("jsp.common.status.rejected");
		}
		
		customerExportVO.setStatus(statusDescription);
	}

	/*
	 * Export khach hang
	 */
	private String exportCustomer(List<CustomerExportVO> customers) throws Exception {
		List<Object> attributeDynamicVO = getCustomerDynamicAttributeVO();
		Map<String, Object> param = initCustomerImportTempateHeader();
		param.put("customers", customers);
		param.put("dynamicAttribute", attributeDynamicVO);
		String outputPath = exportExcelJxls(param, Configuration.getExcelTemplatePathCatalog(), ConstantManager.TEMPLATE_CATALOG_CUSTOMER_EXPORT);
		return outputPath;
	}

	/**
	 * exportExecl customer category.
	 * 
	 * @return the string
	 * @author huynp4
	 * @since 08/01/2013
	 */
	public String exportExeclCustomerCategory() throws Exception {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			ShopToken shToken = currentUser.getShopRoot();
			Shop sh = null;
			if (shToken != null) {
				sh = shopMgr.getShopById(shToken.getShopId());
			}

			ObjectVO<CustomerCatLevelImportVO> customerCatLevelImportVO = customerMgr.getCustomerCatLevelImportVO(null, customerCode, customerName, null, phone, mobilePhone, null, ActiveType.parseValue(status), null, loyalty, null, cashierId,
					address, street, sh.getShopCode(), shopCodeLike, display, region);

			if (customerCatLevelImportVO == null) {
				result.put("errMsg", R.getResource("err.msg.not.get.all"));
			} else {
				//Put du lieu
				List<CellBean> lstFails = new ArrayList<CellBean>();
				if (customerCatLevelImportVO != null && customerCatLevelImportVO.getLstObject() != null) {
					int size = customerCatLevelImportVO.getLstObject().size();
					for (int i = 0; i < size; i++) {
						CustomerCatLevelImportVO temp = customerCatLevelImportVO.getLstObject().get(i);
						CellBean cellBean = new CellBean();

						cellBean.setContent1(temp.getCustomerCode());
						cellBean.setContent2(temp.getProductInfoName());
						cellBean.setContent3(temp.getSaleLevel());
						//						cellBean.setContent4(temp.getShopCode());					

						lstFails.add(cellBean);
					}
				}
				if (lstFails.size() == 0) {
					CellBean cellBean = new CellBean();

					cellBean.setContent1("Không có dữ liệu");
					cellBean.setContent2("");
					cellBean.setContent3("");
					//					cellBean.setContent4("");					

					lstFails.add(cellBean);
				}
				exportExcelDataNotSession(lstFails, ConstantManager.TEMPLATE_CATALOG_CATOGORY_CUSTOMER_EXPORT);
				//				session.setAttribute(ConstantManager.USER_SESSION_KEY,lstFails);
				//				result.put(ERROR, false);
			}

		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "CustomerCatalogAction.exportCustomerCategory - " + e.getMessage());
		}
		return JSON;
	}

	public String getAreaInfo() throws Exception {
		try {
			Area area = areaMgr.getAreaById(areaId);
			if (area == null || area.getProvinceName() == null || area.getDistrictName() == null || area.getPrecinctName() == null) {
				//loi
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.area.customer.combobox.tree.error"));
			} else {
				result.put("provinceCode", area.getProvince());
				result.put("provinceName", area.getProvinceName());
				result.put("districtCode", area.getDistrict());
				result.put("districtName", area.getDistrictName());
				result.put("precinctCode", area.getPrecinct());
				result.put("precinctName", area.getPrecinctName());
				result.put("lat", lat);
				result.put("lng", lng);
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "CustomerCatalogAction.getAreaInfo - " + e.getMessage());
		}
		return JSON;
	}

	public String exportTemplateExeclCustomer() throws Exception {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			List<Area> lstArea = areaMgr.getListAreaFull();
			lstTitle = new ArrayList<String>();

			if (lstArea == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("err.msg.not.get.all"));
			} else {
				//Put du lieu
				List<Object> areaBeans = new ArrayList<Object>();
				List<List<Object>> excelExportBeans = new ArrayList<List<Object>>();
				if (lstArea != null) {
					int size = Math.min(lstArea.size(), ConstantManager.MAX_NUM_ROW_EXPORT);
					
					String provinceCode = "";
					String districtCode = "";
					for (int i = 0; i < size; i++) {
						CellBean cellBean = new CellBean();
						if (!provinceCode.equals(lstArea.get(i).getProvince())) {
							cellBean.setContent1(lstArea.get(i).getProvince());
							cellBean.setContent2(lstArea.get(i).getProvinceName());
							provinceCode = lstArea.get(i).getProvince();
						}

						if (!districtCode.equals(lstArea.get(i).getDistrict())) {
							cellBean.setContent3(lstArea.get(i).getDistrict());
							cellBean.setContent4(lstArea.get(i).getDistrictName());
							districtCode = lstArea.get(i).getDistrict();
						}

						cellBean.setContent5(lstArea.get(i).getPrecinct());
						cellBean.setContent6(lstArea.get(i).getPrecinctName());
						areaBeans.add(cellBean);
					}
				}

				excelExportBeans.add(areaBeans);
				excelExportBeans.add(getCustomerDynamicAttributeVO());
				//tạo header đa ngôn ngữ
				Map<String, Object> fileHeaderParams = initCustomerImportTempateHeader();
				
				String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_CATALOG_CUSTOMMER;
				exportExcelDataTemplate(excelExportBeans, ConstantManager.TEMPLATE_CATALOG_CUSTOMMER, outputName, fileHeaderParams);
				
			}

		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "CustomerCatalogAction.exportTemplateExcelCustomer - " + e.getMessage());
		}
		return JSON;
	}
	
	/*
	 * Lay danh sach thuoc tinh dong fill vao file excel
	 */
	private List<Object> getCustomerDynamicAttributeVO() throws BusinessException {
		return dynamicAttributeHelper.getCustomerDynamicAttributeVO(ActiveType.RUNNING);
	}

	/**export template nganh hang
	 * @author cuonglt3
	 * @return
	 * @throws Exception
	 */
	public String exportTemplateExcelCategory() throws Exception{
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			Map<String, Object> fileHeaderParams = new HashMap<String, Object>();
			fileHeaderParams.put("customerCode", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.customercode"));
			fileHeaderParams.put("category", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.categoryvn"));
			fileHeaderParams.put("level", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.level.code"));
			fileHeaderParams.put("require", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ss.require.label"));
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.customer.import.category");
			exportExcelDataTemplate(null, ConstantManager.TEMPLATE_CATALOG_CATEGORY, outputName, fileHeaderParams);
			
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "CustomerCatalogAction.exportTemplateExcelCategory - " + e.getMessage());
		}
		return JSON;
	}

	/**export template excel
	 * @author cuonglt3
	 * @param lstData
	 * @param tempFileName
	 * @param outputName
	 * @param fileHeaderParams
	 * @throws Exception
	 */
	private void exportExcelDataTemplate(List<List<Object>> lstData, String tempFileName, String outputName, Map<String, Object> fileHeaderParams) throws Exception {
		try {
			int dataSize = lstData != null ? lstData.size() : 0;
			if ((dataSize > 0 && tempFileName == ConstantManager.TEMPLATE_CATALOG_CUSTOMMER) || tempFileName == ConstantManager.TEMPLATE_CATALOG_CATEGORY) {
				Map<String, Object> params = new HashMap<String, Object>();
				if (tempFileName == ConstantManager.TEMPLATE_CATALOG_CUSTOMMER) {
					params.put("report", lstData.get(0));
					final int DYNAMIC_ATTRIBUTE_INDEX = 1;
					if (dataSize > DYNAMIC_ATTRIBUTE_INDEX) {
						params.put("dynamicAttribute", lstData.get(DYNAMIC_ATTRIBUTE_INDEX));
					}
					params.put("customerType", getListCustomerTypeForTemplate());
					params.put("lstTitle", lstTitle);
					params.put("salePositionOptions", getJoinedCustomerSalePositionOptions());
				}
				if (fileHeaderParams != null && fileHeaderParams.size() > 0) {
					params.putAll(fileHeaderParams);
				}
				
				String outputPath = exportExcelJxls(params, Configuration.getExcelTemplatePathCatalog(), tempFileName);
				result.put(LIST, outputPath);
				MemcachedUtils.putValueToMemcached(retrieveReportToken(reportCode), outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put("hasData", false);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private String getJoinedCustomerSalePositionOptions() throws BusinessException {
		List<ApParam> customerSalePostionOptions = this.getCustomerSalePostionOptions();
		String joinedCustomerSalePosition = "";
		if (customerSalePostionOptions != null) {
			for (int i = 0, size = customerSalePostionOptions.size(); i < size && customerSalePostionOptions.get(i) != null; i++) {
				ApParam option = customerSalePostionOptions.get(i);
				joinedCustomerSalePosition += "," + option.getApParamName();
			}
			if (joinedCustomerSalePosition.length() != 0) {
				joinedCustomerSalePosition = joinedCustomerSalePosition.substring(1);
			}
		}
		return joinedCustomerSalePosition;
	}

	/** lay danh sach loai khach hang
	 * @author cuonglt3
	 * @return
	 * @throws BusinessException
	 */
	private String getListCustomerTypeForTemplate() throws BusinessException{
		String stringlstCustomerType = "";
		List<ChannelTypeVO> lstChannelType =  channelTypeMgr.getListChannelTypeVO();
		if (lstChannelType != null) {
			for (int i = 0, size = lstChannelType.size(); i < size && lstChannelType.get(i) != null; i++) {
				stringlstCustomerType += (i != 0 ? ", " : "") + lstChannelType.get(i).getCodeChannelType();
			}
		}
		return stringlstCustomerType;
	}


	/**
	 * Lay danh sach dia ban
	 * 
	 * @author lacnv1
	 * @since Apr 21, 2014
	 */
	public String getListSubArea() throws Exception {
		try {
			if (areaId == null || objectType == 0) {
				return JSON;
			}
			lstPrecinct = areaMgr.getListAreaByType(areaId, objectType, ActiveType.RUNNING.getValue());
		} catch (Exception ex) {
			LogUtility.logError(ex, "CustomerCatalogAction.getListSubArea - " + ex.getMessage());
		}
		return JSON;
	}
	
	public String applyCustomer() throws Exception {
		resetToken(result);
		boolean error = true;
		String errMsg = "";
		if (currentUser != null) {
			try {
				ShopToken shToken = currentUser.getShopRoot();
				Shop shop = null;
				if (shToken != null) {
					shop = shopMgr.getShopById(shToken.getShopId());
				}

				if (shop == null) {
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				}
				if (lstCustomerId != null) {
					for (int i = 0, size = lstCustomerId.size(); i < size; i++) {
						Customer customer = customerMgr.getCustomerById(lstCustomerId.get(i));
						if (customer != null) {
							if (status == ActiveType.RUNNING.getValue()) {
								customer.setStatus(ActiveType.RUNNING);
							} else if (status == ActiveType.REJECTED.getValue()) {
								customer.setStatus(ActiveType.REJECTED);
							} else if (status == ActiveType.DELETED.getValue()) {
								customer.setStatus(ActiveType.DELETED);
							}
							commonMgr.updateEntity(customer);
							error = false;
						}
					}
				}  
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		result.put(ERROR, error);
		if (error && StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		}
		result.put("errMsg", errMsg);
		return JSON;
	}
	
	/**
	 * Ham xu ly request duyet khach hang moi
	 * 
	 * @author tuannd20
	 * @return
	 * @since 28/08/2015
	 */
	public String approveNewCustomerHandler() {
		resetToken(result);
		result.put(ERROR, false);
		
		if (currentUser == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		
		if (customerId == null) {
			result.put(ERROR, true);
			result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.id.empty"));
			return JSON;
		}
		
		try {
			Customer customer = commonMgr.getEntityById(Customer.class, customerId);
			if (customer == null || customer.getShop() == null || !this.isLoginUserManageShop(customer.getShop().getId())) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.not.exist.or.customer.belong.to.another.shop"));
				return JSON;
			}
			
			if (ActiveType.WAITING != customer.getStatus()) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.deleted.or.running"));
				return JSON;
			}
			
			if (customer.getLat() == null || customer.getLng() == null) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.lat.lng.invalid", customer.getCustomerName()));
				return JSON;
			}
			
			List<DuplicatedCustomerVO> duplicatedCustomer = approveNewCustomer(customer.getId(), getLogInfoVO());
			result.put("duplicatedCustomer", duplicatedCustomer);
		} catch (BusinessException e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
		}
		
		return JSON;
	}
	
	/*
	 * Duyet khach hang moi. Tra ve danh sach khach hang trung (neu co)
	 */
	private List<DuplicatedCustomerVO> approveNewCustomer(Long customerId, LogInfoVO logInfo) throws BusinessException {
		return customerMgr.approveNewCustomer(customerId, logInfo);
	}
	
	/**
	 * Ham xu ly request tao khach hang
	 * @author tuannd20
	 * @return
	 * @since 31/08/2015
	 */
	public String createNewCustomerHandler() {
		resetToken(result);
		result.put(ERROR, false);
		
		if (currentUser == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		
		if (customerId == null) {
			result.put(ERROR, true);
			result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.id.empty"));
			return JSON;
		}
		
		errMsg = ValidateUtil.validateField(customerCode, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE);
		if (!StringUtil.isNullOrEmpty(errMsg)) {
			result.put("errMsg", errMsg);
			result.put(ERROR, true);
			return JSON;
		}
		
		try {
			Customer customer = commonMgr.getEntityById(Customer.class, customerId);
			if (customer == null || customer.getShop() == null || !this.isLoginUserManageShop(customer.getShop().getId())) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.not.exist.or.customer.belong.to.another.shop"));
				return JSON;
			}
			
			if (ActiveType.WAITING != customer.getStatus()) {
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.deleted.or.running"));
				return JSON;
			}
			
			String errorMsg = null;
			if (StringUtil.isNullOrEmpty(customerCode)) {
				createNewCustomer(customerId);
			} else {
				errorMsg = createNewCustomerWithCode(customer, customerCode);
			}
			
			if (!StringUtil.isNullOrEmpty(errorMsg)) {
				result.put(ERROR, true);
				result.put("errMsg", errorMsg);
			}
		} catch (BusinessException e) {
			result.put(ERROR, true);
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	/*
	 * Tao moi khach hang voi ma khach hang tu sinh
	 */
	private void createNewCustomer(Long customerId) throws BusinessException {
		customerMgr.createNewCustomer(customerId, getLogInfoVO());
	}
	
	/*
	 * Tao moi khach hang voi ma khach hang chi dinh
	 */
	private String createNewCustomerWithCode(Customer customer, String customerShortCode) throws BusinessException {
		String errorMsg = null;
		String shopCode = customer.getShop() != null ? customer.getShop().getShopCode() : "";
		String customerCode = generateCustomerCode(customerShortCode, shopCode);
		
		Customer existCustomer = customerMgr.getCustomerByCode(customerCode);
		boolean existCustomerWithGivenCodeInSameShop = existCustomer != null && existCustomer.getShop() != null && customer.getShop() != null
											&& existCustomer.getShop().getId() != null
											&& existCustomer.getShop().getId().compareTo(customer.getShop().getId()) == 0;
		if (existCustomerWithGivenCodeInSameShop) {
			errorMsg = R.getResource("customer.manage.approve.new.customer.create.with.exist.code", customerShortCode);
		} else {
			customer.setCustomerCode(customerCode);
			customer.setShortCode(customerShortCode);
			customer.setStatus(ActiveType.RUNNING);
			customer.setUpdateDate(commonMgr.getSysDate());
			customer.setUpdateUser(getLogInfoVO().getStaffCode());
			commonMgr.updateEntity(customer);			
		}
		return errorMsg;
	}
	
	/*
	 * sinh ma khach hang dua tren ma shortCode & ma shopCode
	 */
	private String generateCustomerCode(String customerShortCode, String shopCode) {
		final String DELIMITER = "_";
		return customerShortCode + DELIMITER + shopCode;
	}
	
	/**
	 * Ham xu ly request huy khach hang 
	 * @author tuannd20
	 * @return
	 * @since 31/08/2015
	 */
	public String deleteNewCustomerHandler() {
		resetToken(result);
		result.put(ERROR, false);
		
		if (currentUser == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		
		if (lstCustomerId == null || lstCustomerId.isEmpty()) {
			result.put(ERROR, true);
			result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.id.empty"));
			return JSON;
		}
		
		Map<Long, String> errorCustomer = new HashMap<Long, String>();
		for (Long customerId : lstCustomerId) {
			Customer customer = null;
			try {
				customer = commonMgr.getEntityById(Customer.class, customerId);
				boolean isError = false;
				if (customer == null || customer.getShop() == null || !this.isLoginUserManageShop(customer.getShop().getId())) {
					errorCustomer.put(customerId, R.getResource("customer.manage.approve.new.customer.customer.not.exist.or.customer.belong.to.another.shop", customer.getCustomerName()));
					isError = true;
				}
				
				if (!isError && (ActiveType.RUNNING == customer.getStatus() || ActiveType.STOPPED == customer.getStatus())) {
					errorCustomer.put(customerId, R.getResource("customer.manage.approve.new.customer.delete.customer.stopped.or.running", customer.getCustomerName()));
					isError = true;
				}
				
				if (!isError) {
					deleteNewCustomer(customer);
				}
			} catch (BusinessException e) {
				errorCustomer.put(customerId, R.getResource("customer.manage.delete.new.customer.error", customer != null ? customer.getCustomerName() : ""));
				LogUtility.logError(e, e.getMessage());
			}
		}
		if (!errorCustomer.isEmpty()) {
			result.put(ERROR, true);
			result.put("errorCustomer", errorCustomer);
		}
		return JSON;
	}
	
	/*
	 * Xoa khach hang do nhan vien ban hang tao moi
	 */
	private void deleteNewCustomer(Customer customer) throws BusinessException {
		customer.setStatus(ActiveType.DELETED);
		customer.setUpdateDate(commonMgr.getSysDate());
		customer.setUpdateUser(getLogInfoVO().getStaffCode());
		commonMgr.updateEntity(customer);
	}
	
	/**
	 * Ham xu ly request tu choi khach hang 
	 * @author tuannd20
	 * @return
	 * @since 31/08/2015
	 */
	public String denyNewCustomerHandler() {
		resetToken(result);
		result.put(ERROR, false);
		
		if (currentUser == null) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		
		if (lstCustomerId == null || lstCustomerId.isEmpty()) {
			result.put(ERROR, true);
			result.put("errMsg", R.getResource("customer.manage.approve.new.customer.customer.id.empty"));
			return JSON;
		}
		
		Map<Long, String> errorCustomer = new HashMap<Long, String>();
		for (Long customerId : lstCustomerId) {
			Customer customer = null;
			try {
				customer = commonMgr.getEntityById(Customer.class, customerId);
				boolean isError = false;
				if (customer == null || customer.getShop() == null || !this.isLoginUserManageShop(customer.getShop().getId())) {
					errorCustomer.put(customerId, R.getResource("customer.manage.approve.new.customer.customer.not.exist.or.customer.belong.to.another.shop", customer.getCustomerName()));
					isError = true;
				}
				
				if (!isError && ActiveType.WAITING != customer.getStatus()) {
					errorCustomer.put(customerId, R.getResource("customer.manage.approve.new.customer.customer.deleted.or.running", customer.getCustomerName()));
					isError = true;
				}
				
				if (!isError) {
					denyNewCustomer(customer);					
				}
			} catch (BusinessException e) {
				errorCustomer.put(customerId, R.getResource("customer.manage.deny.new.customer.error", customer != null ? customer.getCustomerName() : ""));
				LogUtility.logError(e, e.getMessage());
			}
		}
		if (!errorCustomer.isEmpty()) {
			result.put(ERROR, true);
			result.put("errorCustomer", errorCustomer);
		}
		return JSON;
	}
	
	/*
	 * Tu choi khach hang do nhan vien ban hang tao
	 */
	private void denyNewCustomer(Customer customer) throws BusinessException {
		customer.setStatus(ActiveType.REJECTED);
		customer.setUpdateDate(commonMgr.getSysDate());
		customer.setUpdateUser(getLogInfoVO().getStaffCode());
		commonMgr.updateEntity(customer);
	}

	public List<StaffVO> getLstStaffVO() {
		return lstStaffVO;
	}

	public void setLstStaffVO(List<StaffVO> lstStaffVO) {
		this.lstStaffVO = lstStaffVO;
	}
	public String existsCode() throws Exception {
		ShopToken shop1 = getShopTockenInOrgAccessByCode(shopCode);
		if(shop1!=null){
			result.put("isExists", customerMgr.existsCode(id, shop1.getShopId(), customerCode));
		}
		return JSON;
	}

	public List<RoutingVO> getListRouting() {
		return listRouting;
	}

	public void setListRouting(List<RoutingVO> listRouting) {
		this.listRouting = listRouting;
	}

	public Long getRoutingId() {
		return routingId;
	}

	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}

	

	public Integer getTansuat() {
		return tansuat;
	}

	public void setTansuat(Integer tansuat) {
		this.tansuat = tansuat;
	}



	public String getBirthDayStr() {
		return birthDayStr;
	}

	public void setBirthDayStr(String birthDayStr) {
		this.birthDayStr = birthDayStr;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getInvoiceNameBranchBank() {
		return invoiceNameBranchBank;
	}

	public void setInvoiceNameBranchBank(String invoiceNameBranchBank) {
		this.invoiceNameBranchBank = invoiceNameBranchBank;
	}

	public String getBankAccountOwner() {
		return bankAccountOwner;
	}

	public void setBankAccountOwner(String bankAccountOwner) {
		this.bankAccountOwner = bankAccountOwner;
	}

	public List<ApParam> getSalePositionOptions() {
		return salePositionOptions;
	}

	public void setSalePositionOptions(List<ApParam> salePositionOptions) {
		this.salePositionOptions = salePositionOptions;
	}

	public Integer getCustomerSalePosition() {
		return customerSalePosition;
	}

	public void setCustomerSalePosition(
			Integer currentCustomerSalePosition) {
		this.customerSalePosition = currentCustomerSalePosition;
	}
}

package ths.dms.action.catalog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.commons.io.FileUtils;

import ths.dms.core.business.ReportManagerMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.Report;
import ths.dms.core.entities.ReportShopMap;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.DefaultType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ObjectReportType;
import ths.dms.core.entities.enumtype.StaffRoleType;
import ths.dms.core.entities.filter.ReportFilter;
import ths.dms.core.entities.vo.ObjectVO;



public class ReportManagerAction extends AbstractAction {

	/**
	 * Khai bao thuoc tinh chung
	 * @author vuongmq
	 */
	private static final long serialVersionUID = 1L;
	
	private ShopMgr shopMgr;
	private StaffMgr staffMgr;
	private ReportManagerMgr reportManagerMgr;
	
	/// end vuongmq
	private Shop shop;
	private Long shopId;
	private Staff staffSign;
	private Integer shopType;
	
	private Long reportId;
	private String reportCode;
	private String reportName;

	private String nameFile;
	private File reportFile;
	private Integer checkDefault;
	private Integer checkDefault1;
	private Integer typeReport;
	private Long reportIdDefault;
	private Long reportUrlId;
	private Long shopIdReport;
	private String urlVNM;
	private String urlNPP;
	
	/**
	 * Khai bao cac phuong thuc
	 * @author vuongmq
	 */
	
	@Override
	public void prepare(){
		try {			
			super.prepare();
			staffMgr = (StaffMgr)context.getBean("staffMgr");
			shopMgr =(ShopMgr) context.getBean("shopMgr");
			reportManagerMgr =(ReportManagerMgr) context.getBean("reportManagerMgr");
			staffSign = getStaffByCurrentUser();
				Shop shopChoose = (Shop)request.getSession().getAttribute(ConstantManager.SESSION_SHOP_CHOOSE);
				if (currentUser != null && currentUser.getUserName() != null) {
					staff = staffMgr.getStaffByCode(currentUser.getUserName());
					shopId = currentUser.getShopRoot().getShopId();
					shop = shopMgr.getShopById(shopId);
//					shopType = shop.getType().getObjectType();
				}
				if(shopChoose!=null){
					shop = shopChoose;
					shopId = shop.getId();
					//request.getSession().setAttribute(ConstantManager.SESSION_SHOP,shop);
				}
			
		} catch (Exception e) {	
			LogUtility.logError(e, "ReportManagerAction.prepare - " + e.getMessage());
		}		
	}

	@Override
	public String execute() {
		resetToken(result);
		List<String> lstUrl = getListSubconponentByStringInCMS();
		urlVNM = "";
		urlNPP = "";
		if(lstUrl!=null && lstUrl.size()>0){
			for(String url :lstUrl){
				if(url.equals("/catalog/report-manager/search")){
					urlVNM = "1";
				}else if(url.equals("/catalog/report-manager/search")){
					urlNPP = "1";
				}
			}
		}
		return SUCCESS;
	}
	
	public String search() {
		return JSON;
	}
	
	public String updateNameReport(){
		return JSON;
	}
	
	public String importFileReport() {
		try {
			errMsg = "";
			if(StringUtil.isNullOrEmpty(errMsg) && currentUser == null){
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.manager.err.loginSystem ");
			}
			if(!StringUtil.isNullOrEmpty(reportCode)){
				if(StringUtil.isNullOrEmpty(errMsg)){
					if( reportManagerMgr.getReportByCode(reportCode.toUpperCase()) != null) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.manager.err.report.code.exits");
					}
				}
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.manager.err.report.code");
			}
			if(StringUtil.isNullOrEmpty(errMsg) && StringUtil.isNullOrEmpty(reportName)){
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"rpt.manager.err.report.name");
			}
			if (reportFile != null ) {
				if (reportFile.exists() && !StringUtil.isNullOrEmpty(nameFile)){
					String name = nameFile.substring(nameFile.lastIndexOf("."));
					if(!name.toUpperCase().equals(".JRXML")) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"rpt.manager.err.report.namefile");
					}
				}
			}
			if(errMsg.length()>0){
				result.put(ERROR, true);
				result.put("errMsg", errMsg); 
				return SUCCESS;
			}
			//String path = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getVatTemplatePath();\
			Report rp = new Report();
			rp.setReportCode(reportCode.toUpperCase());
			rp.setReportName(reportName);
			rp.setCreateDate(DateUtil.now());
			rp.setCreateUser(getCurrentUser().getUserName());
			rp = reportManagerMgr.createReport(rp);
			if(rp == null) {
				result.put(ERROR, true);
				errMsg  = "Tạo không thành công Report !";
				return SUCCESS;
			} 
			if (reportFile != null ) {
				if (reportFile.exists() && !StringUtil.isNullOrEmpty(nameFile)){
					String name = nameFile.substring(0, nameFile.length() - 6);
					name = FileUtility.getSafeFileName(name);
					/*File a = new File("C:/vuong/rpt_bchn_thdhhh.jrxml");
					String output = "C:/temp/";
					String nameRe = "rpt_bchn_thdhhh.jrxml";*/
					FileUtils.getTempDirectory();
					String fileName = name + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_FILE_EXPORT);
					String fileType = nameFile.substring(nameFile.lastIndexOf('.'));
					String nameJrxml = fileName + fileType;
					String nameReport = FileUtility.createFileReport(reportFile, Configuration.getReportTemplatePath(), nameJrxml);
					if(!StringUtil.isNullOrEmpty(nameReport)) {
						Boolean flag = FileUtility.compilerJasperReportInJava(Configuration.getReportTemplatePath() + nameReport, Configuration.getReportTemplatePath(), nameReport);
						if(!flag) {
							result.put(ERROR, true);
							errMsg  = "Compiler file .jrxml không thành công !";
							return SUCCESS;
						}
					} else {
						result.put(ERROR, true);
						errMsg  = "Tạo file .jrxml không thành công !";
						return SUCCESS;
					}
					// tao report Url;
					ReportUrl rptUrl = new ReportUrl();
					DefaultType type = DefaultType.NOTDEFAULT;
					if(checkDefault!= null) {
						if(checkDefault == 1) { // nếu dialog check thì giá trị bàng 1, ko check la bang null, vi checkbox co check moi gui du lieu len duoc, ngc lai la null
							type = DefaultType.DEFAULT;
						}
					}
					rptUrl.setIsDefault(type);
					rptUrl.setObjectId(rp.getId());
					rptUrl.setType(ObjectReportType.VNM); // neu co chon Url thi la type mac dinh file goc
					rptUrl.setUrl(nameReport); // luu ten Url: tenfile.jrxml 
					rptUrl.setCreateDate(DateUtil.now());
					rptUrl.setCreateUser(getCurrentUser().getUserName());
					rptUrl = reportManagerMgr.createReportUrl(rptUrl);
					if(rptUrl == null) {
						result.put(ERROR, true);
						errMsg  = "Tạo không thành công ReportUrl !";
						return SUCCESS;
					}
				}
			}// end if(reportFile != null)
		} catch (Exception e) {
			LogUtility.logError(e, "ReportManagerAction.importFileReport()" + e.getMessage());
			result.put(ERROR, true);
			errMsg  = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			//result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM)); 
		}
		return SUCCESS;
	}
	
	public String importFileReportDetail() {
		try {
			errMsg = "";
			if(StringUtil.isNullOrEmpty(errMsg) && currentUser == null){
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "rpt.manager.err.loginSystem ");
			}
			if (reportFile != null ) {
				if (reportFile.exists() && !StringUtil.isNullOrEmpty(nameFile)){
					String name = nameFile.substring(nameFile.lastIndexOf("."));
					if(!name.toUpperCase().equals(".JRXML")) {
						errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"rpt.manager.err.report.namefile");
					}
				}
			}
			if(errMsg.length()>0){
				result.put(ERROR, true);
				result.put("errMsg", errMsg); 
				return SUCCESS;
			}
			if(checkDefault != 0) {
				if(checkDefault != -1) {
					// kiem tra reportIdDefault co Default thi cap nhat lai khong default
					// Truong hop nay: thi la file goc co 1 default, file NPP co 1 default voi ma : reportIdDefault
					ReportUrl urlDefault = null;
					if (typeReport != null && shopIdReport != null && reportId!= null) {
						ReportFilter filter = new ReportFilter();
						filter.setTypeReport(ObjectReportType.parseValue(typeReport));
						filter.setShopId(shopIdReport);
						filter.setReportId(reportId);
						urlDefault = reportManagerMgr.getDefaultReportUrl(filter);
					} else if(reportIdDefault != null) {
						urlDefault = reportManagerMgr.getReportUrlById(reportIdDefault);
					}
					if(urlDefault != null && DefaultType.DEFAULT.equals(urlDefault.getIsDefault())) {
						urlDefault.setIsDefault(DefaultType.NOTDEFAULT);
						urlDefault.setUpdateDate(DateUtil.now());
						urlDefault.setUpdateUser(getCurrentUser().getUserName());
						reportManagerMgr.updateReportUrl(urlDefault);
					}
					//la doi file lam default: update reportUrl: is_default, lay checkDefault: dung Id cua reportUrl
					ReportUrl Url = reportManagerMgr.getReportUrlById(new Long(checkDefault));
					Url.setIsDefault(DefaultType.DEFAULT);
					Url.setUpdateDate(DateUtil.now());
					Url.setUpdateUser(getCurrentUser().getUserName());
					reportManagerMgr.updateReportUrl(Url);
				}// la bang -1: khong Update reportUrl: is_default 
			} else {
				// la bang 0: file moi lam default, kiem tra reportIdDefault co Default thi cap nhat lai khong default
				ReportUrl urlDefault = null;
				if (typeReport != null && shopIdReport != null && reportId!= null) {
					ReportFilter filter = new ReportFilter();
					filter.setTypeReport(ObjectReportType.parseValue(typeReport));
					filter.setShopId(shopIdReport);
					filter.setReportId(reportId);
					urlDefault = reportManagerMgr.getDefaultReportUrl(filter);
				} else if(reportIdDefault != null) {
					urlDefault = reportManagerMgr.getReportUrlById(reportIdDefault);
				}
				if(urlDefault != null && DefaultType.DEFAULT.equals(urlDefault.getIsDefault())) {
					urlDefault.setIsDefault(DefaultType.NOTDEFAULT);
					urlDefault.setUpdateDate(DateUtil.now());
					urlDefault.setUpdateUser(getCurrentUser().getUserName());
					reportManagerMgr.updateReportUrl(urlDefault);
				}
			}
			// create reportUrl: is_default
			if(reportFile != null) {
				if(reportFile.exists() && !StringUtil.isNullOrEmpty(nameFile)){
					// cap nhat them file ReportUrl Cho Report(id)
					// cat chuoi file .zip reportCode
					String name = nameFile.substring(0, nameFile.length() - 6);
					name = FileUtility.getSafeFileName(name);
					/*File a = new File("C:/vuong/rpt_bchn_thdhhh.jrxml");
					String output = "C:/temp/";
					String nameRe = "rpt_bchn_thdhhh.jrxml";*/
					String fileName = name + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_FILE_EXPORT);
					String fileType = nameFile.substring(nameFile.lastIndexOf('.'));
					String nameJrxml = fileName + fileType;
					String nameReport = FileUtility.createFileReport(reportFile, Configuration.getReportTemplatePath(), nameJrxml);
					if(!StringUtil.isNullOrEmpty(nameReport)) {
						Boolean flag = FileUtility.compilerJasperReportInJava(Configuration.getReportTemplatePath() + nameReport, Configuration.getReportTemplatePath(), nameReport);
						if(!flag) {
							result.put(ERROR, true);
							errMsg  = "Compiler file .jrxml không thành công !";
							return SUCCESS;
						}
					} else {
						result.put(ERROR, true);
						errMsg  = "Tạo file .jrxml không thành công !";
						return SUCCESS;
					}
					// tao report Url;
					ReportUrl rptUrl = new ReportUrl();
					DefaultType type = DefaultType.NOTDEFAULT;
					if(checkDefault == 0){ // truong hop importFileReportDetail = 0 thi la lay lam defaullt
						type = DefaultType.DEFAULT;
					}
					rptUrl.setIsDefault(type);
					rptUrl.setUrl(nameReport); // luu ten Url: tenfile.jrxml
					rptUrl.setCreateDate(DateUtil.now());
					rptUrl.setCreateUser(getCurrentUser().getUserName());
					Report rp = reportManagerMgr.getReportById(reportId);
					if(typeReport == 0 ) { 
						//tao ReportUrl map reportId
						rptUrl.setType(ObjectReportType.VNM);
						rptUrl.setObjectId(rp.getId());
						rptUrl = reportManagerMgr.createReportUrl(rptUrl);
						if(rptUrl == null) {
							result.put(ERROR, true);
							errMsg  = "Tạo không thành công ReportUrl !";
							return SUCCESS;
						}
					} else { // NPP
						ReportShopMap kt = reportManagerMgr.getReportShopMapByShopIdReportId(shopIdReport, reportId);
						if(kt == null) {
							//tao reportShopMap
							ReportShopMap rptShopMap = new ReportShopMap();
							Shop sh = shopMgr.getShopById(shopIdReport);
							rptShopMap.setShopId(sh);
							rptShopMap.setReportId(rp);
							rptShopMap.setCreateDate(DateUtil.now());
							rptShopMap.setCreateUser(getCurrentUser().getUserName());
							rptShopMap = reportManagerMgr.createReportShopMap(rptShopMap);
							if(rptShopMap == null) {
								result.put(ERROR, true);
								errMsg  = "Tạo không thành công ReportShopMap !";
							}
							rptUrl.setType(ObjectReportType.NPP);
							rptUrl.setObjectId(rptShopMap.getId());
							//tao reportUrl map reportShopMapId
							rptUrl = reportManagerMgr.createReportUrl(rptUrl);
							if(rptUrl == null) {
								result.put(ERROR, true);
								errMsg  = "Tạo không thành công ReportUrl !";
								return SUCCESS;
							}
						} else {
							rptUrl.setType(ObjectReportType.NPP);
							rptUrl.setObjectId(kt.getId());
							//tao reportUrl map reportShopMapId
							rptUrl = reportManagerMgr.createReportUrl(rptUrl);
							if(rptUrl == null) {
								result.put(ERROR, true);
								errMsg  = "Tạo không thành công ReportUrl !";
								return SUCCESS;
							}
						}
					}
					return SUCCESS;
				}
			}// end if(reportFile != null)
		} catch (Exception e) {
			LogUtility.logError(e, "ReportManagerAction.importFileReportDetail()" + e.getMessage());
			result.put(ERROR, true);
			errMsg  = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			//result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM)); 
		}
		return SUCCESS;
	}
	public String editReport() {
		result.put("page", page);
		result.put("rows", rows);
		try {
			KPaging<ReportUrl> kPaging = new KPaging<ReportUrl>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			ReportFilter filter = new ReportFilter();
			filter.setReportId(reportId);
			// lay theo type File: typeReport 0:  file Gốc, 1: file NPP, 
			if(typeReport != null) {
				if(typeReport == 0)	{
					filter.setTypeReport(ObjectReportType.VNM);
				} else {
					filter.setTypeReport(ObjectReportType.NPP);
					filter.setShopId(shopId);
				}
			}
			ObjectVO<ReportUrl> vo = reportManagerMgr.getListReportUrl(kPaging, filter);
			if(vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0 );
				result.put("rows", new ArrayList<ReportUrl>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ReportManagerAction.editReport()" + e.getMessage());
			
		}
		return JSON;
	}
	
	public static Boolean compilerJasperReportInJava( String strFileJrxml, String outputFolder,  String nameJrxml) {
		try {
			File jasper = new File(strFileJrxml);
			if(jasper.exists()){
				int idx = nameJrxml.lastIndexOf(".jrxml");
				nameJrxml = nameJrxml.substring(0,idx);
				String nameJasper = nameJrxml + ".jasper";
				String strFileJasper = outputFolder + nameJasper;
				JasperCompileManager.compileReportToFile(strFileJrxml, strFileJasper);
			} else {
				return false;
			}
		} catch (JRException e) {
			LogUtility.logError(e, "compilerJasperReportInJava " + e.getMessage());
		}
		return true;
	}
	public static void main(String [] args) {
		String nameFile1 = "asaaaa.jrxml";
		nameFile1 =	nameFile1.substring(nameFile1.lastIndexOf("."));
		if(!nameFile1.toUpperCase().equals(".JRXML")) {
			System.out.print("loi ne");
		} else {
			System.out.print("OK");
		}
		//String a = javaCompilerJasperReport();
		//System.out.println("HIIIIIIIIII: " + a);
		/*String source = "C:/tmp/vat 15_06_20140616175754.jrxml";
		FileUtility.getSafeFileName(source);
		int idx = source.lastIndexOf("/");
		String name = source.substring(idx +1, source.length());
		System.out.println("HIIIIIIIIII:" + name);*/
		/*File a = new File("C:/vuong/rpt_bchn_thdhhh.jrxml");
		String output = "C:/temp/";
		String nameRe = "rpt_bchn_thdhhh.jrxml";
		String nameReport = createFileReport(a, output, nameRe);
		if(!StringUtil.isNullOrEmpty(nameReport)) {
			Boolean flag = compilerJasperReportInJava(output + nameReport, output, nameReport);
			if(!flag) {
				System.out.println(" Compiler file .jrxml không thành công !");
			}
		} else {
			System.out.println(" Tạo file .jrxml không thành công !");
		}*/
		
		
	}
	public static String createFileReport(File file, String outputFolder, String nameReport) {
		nameReport = nameReport.substring(0, nameReport.lastIndexOf("."));
		String fileName = nameReport + "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_FILE_EXPORT);
		String fileType = file.getName().substring(file.getName().lastIndexOf('.'));
		try {
			File newFile = new File(outputFolder + File.separator + fileName+ fileType);
			new File(newFile.getParent()).mkdirs();
	        if(!newFile.createNewFile()){
	         	newFile.delete();
	         	newFile.createNewFile();
	        }
	        if (newFile.exists()) {
		        FileInputStream from = null;
		        FileOutputStream to = null;
		        from = new FileInputStream(file);
		        to = new FileOutputStream(newFile);
		        byte[] buffer = new byte[2048];
		        int bytesRead;
		
		        while ((bytesRead = from.read(buffer)) > 0) {
		            to.write(buffer, 0, bytesRead); // write
		        }
		        from.close();
		        to.close();
			}else {
				return "";
			}
		 } catch (Exception e) {
			 LogUtility.logError(e, "createFileReport " + e.getMessage());
		 }
		 return fileName + fileType; // ten file khong duoi
	}
	/**
	 * Khai bao GETTER/SETTER
	 * @author vuongmq
	 */
	
	public String getReportCode() {
		return reportCode;
	}
	public File getReportFile() {
		return reportFile;
	}

	public void setReportFile(File reportFile) {
		this.reportFile = reportFile;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaffSign() {
		return staffSign;
	}
	public void setStaffSign(Staff staffSign) {
		this.staffSign = staffSign;
	}
	
	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getCheckDefault() {
		return checkDefault;
	}

	public void setCheckDefault(Integer checkDefault) {
		this.checkDefault = checkDefault;
	}

	public Integer getTypeReport() {
		return typeReport;
	}

	public void setTypeReport(Integer typeReport) {
		this.typeReport = typeReport;
	}

	public Long getReportIdDefault() {
		return reportIdDefault;
	}

	public void setReportIdDefault(Long reportIdDefault) {
		this.reportIdDefault = reportIdDefault;
	}

	public Long getReportUrlId() {
		return reportUrlId;
	}

	public void setReportUrlId(Long reportUrlId) {
		this.reportUrlId = reportUrlId;
	}

	public Long getShopIdReport() {
		return shopIdReport;
	}

	public void setShopIdReport(Long shopIdReport) {
		this.shopIdReport = shopIdReport;
	}

	public Integer getCheckDefault1() {
		return checkDefault1;
	}

	public void setCheckDefault1(Integer checkDefault1) {
		this.checkDefault1 = checkDefault1;
	}

	public String getUrlVNM() {
		return urlVNM;
	}

	public void setUrlVNM(String urlVNM) {
		this.urlVNM = urlVNM;
	}

	public String getUrlNPP() {
		return urlNPP;
	}

	public void setUrlNPP(String urlNPP) {
		this.urlNPP = urlNPP;
	}
	

	
}

package ths.dms.action.catalog;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.bean.JETreeNode;
import ths.dms.web.bean.JETreeNodeAttr;
import ths.dms.web.bean.TreeNode;
import ths.dms.web.business.common.DynamicAttributeHelper;
import ths.dms.web.business.common.DynamicAttributeValidator;
import ths.dms.web.business.common.TreeUtility;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.BusinessUtils;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.Unicode2English;
import ths.dms.web.utils.ValidateUtil;
import ths.dms.web.utils.report.excel.ExcelPOIProcessUtils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.ServletActionContext;

import viettel.passport.client.ShopToken;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.AreaMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.OrganizationMgr;
import ths.dms.core.business.ProductAttributeMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffAttributeMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Area;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.Organization;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopType;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.GenderType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrganizationFilter;
import ths.dms.core.entities.enumtype.OrganizationNodeType;
import ths.dms.core.entities.enumtype.OrganizationUnitTypeFilter;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.WorkingDateProcedureType;
import ths.dms.core.entities.enumtype.WorkingDateType;
import ths.dms.core.entities.filter.OrganizationSystemFilter;
import ths.dms.core.entities.filter.StaffAttributeFilter;
import ths.dms.core.entities.vo.AreaVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrganizationUnitTypeVO;
import ths.dms.core.entities.vo.OrganizationVO;
import ths.dms.core.entities.vo.ProductAttributeVO;
import ths.dms.core.entities.vo.ShopTreeVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffAttributeVO;
import ths.dms.core.entities.vo.StaffExportVO;
import ths.dms.core.entities.vo.StaffTreeVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.filter.UnitFilter;
import ths.dms.core.memcached.MemcachedUtils;

public class UnitTreeCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;
	private final Integer STAFF_CODE_MAX_LENGTH = 50;
	
	private ChannelTypeMgr channelTypeMgr;
	private StaffMgr staffMgr;
	private AreaMgr areaMgr;
	private ShopMgr shopMgr;
	private CommonMgr commonMgr;
	private ApParamMgr apParamMgr;
	private ProductInfoMgr productInfoMgr;
	private OrganizationMgr organizationMgr;
	private ExceptionDayMgr exceptionDayMgr;
	
	private List<StaffType> lstStaffTypeView;//danh sach loai nhan vien view theo cay to chuc
	private ProductAttributeMgr productAttributeMgr;
	private List<ProductAttributeVO> lstProductAttributes;
	private StaffAttributeMgr staffAttributeMgr;
	private List<StaffAttributeVO> lstStaffAttributes;
	private Long nodeTypeId;
	private Integer nodeType;
	private Integer staffGroupSearch;
	private Integer excelType;
	private Long groupId;
	private Integer statusCheckbox;
	private String saleTypeCode;
	private String subCatStr;
	private Integer gender;
	private String workStartDate;
	private String education;
	private String position;
	private String staffTelephone;
	private Long staffType;
	private Long staffSpecificType;
	private String staffTypeName;
	private String staffCode;
	private Long staffId;
	private Integer staffCreateType;
	private String shopCodeHidden;
	private File excelFile;
	private String excelFileContentType;
	private Long shopId;
	private Long shId;
	private String staffName;
	private Integer status;
	private Long staffGroupId;
	private Integer staffObjectType;
	private String province;
	private String district;
	private String ward;
	private String orderProduct;
	private String subStaff;
	private ShopType shopTypeEle;
	private Long organizationId;
	private String abbreviation;

	private String saleGroup;
	private Staff staffSignIn;
	private String areaCode;
	private Staff staff;
	private String houseNumber;
	private String provinceCode;
	private String districtCode;
	private String wardCode;
	private Area area;
	private String idCard;
	private String idDate;
	private String idPlace;
	private String email;
	private String street;

	private String staffPhone;
	private Shop shop;

	private Boolean isUpdate;
	private String parentShopCode;
	private Shop parentShop;
	private Long parentShopId;
	private Boolean allowSelectParentShop;
	private String shopCode;
	private String shopName;
	private Long shopType;
	private String phoneNumber;
	private String mobileNumber;
	private String faxNumber;
	private String accountNumber;
	private String bankName;
	private String taxNumber;
	private String addressBillTo;
	private String contactName;
	private String addressShipTo;
	private String fromMonth;

	private Long areaId;
	private String address;
	private String lat;
	private String lng;
	private Long id;
	private Long idSearch;
	private String parentStaffId;
	private Boolean isCheckExistRowsInShop;

	private UnitFilter unitFilter;
	private Long orgId;
	private StaffTreeVO staffTreeVO;

	private List<CellBean> listBeans; //list import excel
	private List<ShopTreeVO> lstNVGSShop;
	private List<String> lstStaffAddId;//danh sach nhan vien them vao group moi
	private List<Long> lstStaffShopAddId;//danh sach nhan vien them vao group moi
	private List<ApParam> lstsaleGroup; //danh sach nhom ban hang
	private List<String> lstStaffDeleteId;//danh sach nhan vien xoa khoi nhom
	private List<ApParam> lstSaleType;//danh sach hinh thuc ban hang
	private List<ChannelType> lstStaffType;//danh sach loai nhan vien
	private List<Area> areas;//danh sach don vi
	private List<JETreeNode> lstUnitTree;
	private List<Long> lstParentStaffId;
	private List<OrganizationUnitTypeVO> lstOrgUnitType;
	private List<OrganizationUnitTypeVO> lstOrgStaffType;
	private List<TreeNode> searchUnitTree;
	private List<TreeNode> searchSubCatTree;
	private List<ApParam> lstProductType;
	private List<CellBean> listCell;
	@Override
	public void prepare() throws Exception {
		super.prepare();
		shopMgr = (ShopMgr) context.getBean("shopMgr");
		channelTypeMgr = (ChannelTypeMgr) context.getBean("channelTypeMgr");
		staffMgr = (StaffMgr) context.getBean("staffMgr");
		areaMgr = (AreaMgr) context.getBean("areaMgr");
		commonMgr = (CommonMgr) context.getBean("commonMgr");
		apParamMgr = (ApParamMgr) context.getBean("apParamMgr");
		organizationMgr = (OrganizationMgr) context.getBean("organizationMgr");
		productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
		setProductAttributeMgr((ProductAttributeMgr) context.getBean("productAttributeMgr"));
		staffAttributeMgr = (StaffAttributeMgr) context.getBean("staffAttributeMgr");
		exceptionDayMgr = (ExceptionDayMgr)context.getBean("exceptionDayMgr");
		/*
		 * if(currentUser!=null){ shopId=currentUser.getShopRoot().getShopId();
		 * }
		 */
		organizationMgr = (OrganizationMgr) context.getBean("organizationMgr");
		dynamicAttributeHelper = (DynamicAttributeHelper) context.getBean("dynamicAttributeHelper");
		/*
		 * if(currentUser!=null){ shopId=currentUser.getShopRoot().getShopId();
		 * }
		 */
	}

	@Override
	public String execute() {
		resetToken(result);
		//kiem tra xem trong bang shop co ton tai dong du lieu nao khong
		isCheckExistRowsInShop = false;
		ShopFilter filter = new ShopFilter();
		filter.setStatus(ActiveType.RUNNING);
		ObjectVO<Shop> lstShop = null;
		try {
			lstShop = shopMgr.getListShop(filter);
			if (lstShop != null && lstShop.getLstObject() != null && lstShop.getLstObject().size() > 0) {
				isCheckExistRowsInShop = true;
			}
		} catch (BusinessException e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "Error UnitTreeCatalogAction.saveTree: " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return SUCCESS;
	}

	public String saveTree() {
		resetToken(result);
		result.put(ERROR, false);
		try {
			staffMgr.saveTree(lstStaffAddId, lstStaffShopAddId, getLogInfoVO());
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "Error UnitTreeCatalogAction.saveTree: " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Search Shop
	 * 
	 * @author thongnm
	 * @return the string
	 */
	public String searchShop() {
		result.put("page", page);
		result.put("rows", rows);
		try {
			KPaging<ShopVO> kPaging = new KPaging<ShopVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			setPriviledgeInfo(unitFilter);
			unitFilter.setStrShopId(getStrListShopId());
			ObjectVO<ShopVO> foundShops = shopMgr.findShopVOBy(kPaging, unitFilter);
			if (foundShops != null) {
				result.put("total", foundShops.getkPaging().getTotalRows());
				result.put("rows", foundShops.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<ShopVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * tra ve trang tim kiem don vi
	 * 
	 * @author tuannd20
	 * @return ket qua trang thai tim kiem
	 * @since 26/02/2015
	 */
	public String fetchUnitSearchPage() {
		try {
			/*
			 * khoi tao danh sach loai don vi
			 */
			OrganizationUnitTypeFilter orgUnitTypeFilter = new OrganizationUnitTypeFilter();
			orgUnitTypeFilter.setNodeType(OrganizationNodeType.SHOP);
			orgUnitTypeFilter.setStatusShopType(ActiveType.RUNNING);
			KPaging<OrganizationUnitTypeVO> kPaging = null;
			ObjectVO<OrganizationUnitTypeVO> orgUnitTypeObjectVO = organizationMgr.getListOrganizationUnitType(orgUnitTypeFilter, kPaging);
			if (orgUnitTypeObjectVO != null) {
				lstOrgUnitType = orgUnitTypeObjectVO.getLstObject();
			}
			
			/*
			 * khoi tao danh sach loai nhan vien
			 */
			orgUnitTypeFilter.setNodeType(OrganizationNodeType.STAFF);
			orgUnitTypeObjectVO = organizationMgr.getListOrganizationUnitType(orgUnitTypeFilter, kPaging);
			if (orgUnitTypeObjectVO != null) {
				lstOrgStaffType = orgUnitTypeObjectVO.getLstObject();
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * tra ve cau truc cay don vi tim kiem
	 * 
	 * @author tuannd20
	 * @return dinh dang du lieu tra ve
	 * @since 02/03/2015
	 */
	public String buildSearchUnitTree() {
		TreeUtility treeUtility = new TreeUtility(context, new TreeUtility.ITreeDataFilter() {
			@Override
			public List<Shop> filterTreeData(List<Shop> shopsOnTree) {
				if (shopsOnTree != null) {
					List<Shop> filteredShops = new ArrayList<Shop>();
					for (Shop shop : shopsOnTree) {
						if (shop != null && isLoginUserManageShop(shop.getId())) {
							filteredShops.add(shop);							
						}
					}
					return filteredShops;
				}
				return null;
			}
		});
		
		List<Long> rootShopIds = new ArrayList<Long>();
		if (currentUser != null && currentUser.getShopRoot() != null) {
			rootShopIds.add(currentUser.getShopRoot().getShopId());
		}
		searchUnitTree = treeUtility.buildUnitTree(rootShopIds);
		return JSON;
	}
	
	/**
	 * tra ve cau truc cay theo nganh hang
	 * 
	 * @author hoanv25
	 * @return dinh dang du lieu tra ve
	 * @since Auggust 19,2015
	 */
	public String buildSearchSubCatTree() {
		TreeUtility treeUtility = new TreeUtility(context);
		searchSubCatTree = treeUtility.buildSubCatTree();
		return JSON;
	}
//	/**
//	 * tao file template dung de import nhan vien
//	 * @author tuannd20
//	 * @return doi tuong chua duong dan toi file template
//	 * @since 28/03/2015
//	 */
//	public String downloadImportStaffTemplateFile() {
//		String importTemplateFileName = this.createImportStaffTemplateFile();
//		String outputPath = Configuration.getExportExcelPath() + importTemplateFileName;
//		result.put(REPORT_PATH, outputPath);
//		result.put(LIST, outputPath);
//		result.put(ERROR, false);
//		result.put("hasData", true);
//		return JSON;
//	}
//	
//	/**
//	 * Tao file mau import nhan vien
//	 * @author tuannd20
//	 * @return Ten file template moi duoc tao
//	 * @since 28/03/2015
//	 */
//	private String createImportStaffTemplateFile() {
//		String importTemplateFileName = null;
//		
//		FileInputStream fileInputStream = null;
//		FileOutputStream fileOutputStream = null;
//		XSSFWorkbook workbook = null;
//		OPCPackage opcPackage = null;
//		try {
//			/*
//			 * tao file template moi tu file template mau
//			 */
//			final String resourceDirectoryPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
//			final String sourceTemplateFileName = ConstantManager.IMPORT_STAFF_TEMPATE_FILE_NAME + FileExtension.XLSX.getValue();
//			final String sourceTemplateFileAbsoluteName = resourceDirectoryPath + File.separator + sourceTemplateFileName;
//			
//			importTemplateFileName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.file_name") 
//								+ "_" + DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
//			final String importTemplateFileAbsolutePath = Configuration.getStoreRealPath() + File.separator + importTemplateFileName;
//			FileUtility.copyFile(sourceTemplateFileAbsoluteName, importTemplateFileAbsolutePath);
//			
//			/*
//			 * ghi du lieu
//			 */
//			fileInputStream = new FileInputStream(importTemplateFileAbsolutePath);
//			opcPackage = OPCPackage.open(fileInputStream);
//			workbook = new XSSFWorkbook(opcPackage);
//			
//			/*
//			 * ghi header sheet import
//			 */
//			int IMPORT_SHEET_INDEX = 0;
//			this.writeStaffImportSheetHeader(workbook, IMPORT_SHEET_INDEX);
//			
//			/*
//			 * khoi tao danh sach loai don vi
//			 */
//			OrganizationUnitTypeFilter orgUnitTypeFilter = new OrganizationUnitTypeFilter();
//			orgUnitTypeFilter.setNodeType(OrganizationNodeType.STAFF);
//			orgUnitTypeFilter.setStatusShopType(ActiveType.RUNNING);
//			ObjectVO<OrganizationUnitTypeVO> orgUnitTypeObjectVO = organizationMgr.getListOrganizationUnitType(orgUnitTypeFilter, null);
//			List<OrganizationUnitTypeVO> staffTypes = null;
//			if (orgUnitTypeObjectVO != null) {
//				staffTypes = orgUnitTypeObjectVO.getLstObject();
//			}
//			if (staffTypes != null && staffTypes.size() > 0) {
//				int DATA_SHEET_INDEX = 2;
//				this.writeStaffTypeDataTo(workbook, DATA_SHEET_INDEX, staffTypes);
//			}
//			
//			/*
//			 * khoi tao sheet du lieu dia ban
//			 */
//			List<AreaVO> areasInfo = areaMgr.retrieveAreaWithParentAreaInfo(ActiveType.RUNNING);
//			if (areasInfo != null && areasInfo.size() > 0) {
//				int AREA_DATA_SHEET_INDEX = 1;
//				this.writeAreaDataSheet(workbook, AREA_DATA_SHEET_INDEX, areasInfo);				
//			}
//			
//			fileOutputStream = new FileOutputStream(importTemplateFileAbsolutePath);
//			workbook.write(fileOutputStream);
//			fileOutputStream.close();
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
//		finally {
//			IOUtils.closeQuietly(fileInputStream);
//			IOUtils.closeQuietly(fileOutputStream);
//			IOUtils.closeQuietly(opcPackage);
//		}
//		
//		return importTemplateFileName;
//	}
//	
//	/**
//	 * Ghi header sheet import
//	 * @author tuannd20
//	 * @param workbook Workbook ghi du lieu
//	 * @param importSheetIndex Vi tri sheet ghi du lieu
//	 * @since 28/03/2015
//	 */
//	private void writeStaffImportSheetHeader(XSSFWorkbook workbook, int importSheetIndex) {
//		XSSFSheet importSheet = workbook.getSheetAt(importSheetIndex);
//		if (importSheet != null) {
//			workbook.setSheetName(importSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.sheet_name"));
//			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
//			final int HEADER_ROW_INDEX = 0;
//			int writingColumnIndex = 0;
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.no"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.shop_code"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.shop_name"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.staff_code"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.staff_name"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.staff_type"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.gender"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.start_working_time"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.phone"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.mobile"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.email"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.precint_code"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.area"));
//			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.street_house_number"));
//		}
//	}
//	
//	/**
//	 * Ghi thong tin dia ban
//	 * @author tuannd20
//	 * @param workbook Workbook ghi du lieu
//	 * @param areaDataSheetIndex Vi tri sheet ghi du lieu
//	 * @param areasInfo Danh sach thong tin dia ban
//	 * @since 28/03/2015
//	 */
//	private void writeAreaDataSheet(XSSFWorkbook workbook, int areaDataSheetIndex, List<AreaVO> areasInfo) {
//		XSSFSheet areaDataSheet = workbook.getSheetAt(areaDataSheetIndex);
//		if (areaDataSheet != null) {
//			workbook.setSheetName(areaDataSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.area_sheet_name"));
//			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
//			/*
//			 * ghi thong tin header
//			 */
//			final int HEADER_ROW_INDEX = 0;
//			int writingColumnIndex = 0;
//			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.search_unit.search_grid.no"));
//			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.country"));
//			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.province"));
//			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.district"));
//			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.precinct"));
//			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.status"));
//
//			/*
//			 * ghi du lieu
//			 */
//			int START_WRITE_ROW = 1;
//			for (int i = 0, size = areasInfo.size(), writingRowIndex = START_WRITE_ROW; i < size; i++, writingRowIndex++) {
//				AreaVO areaVO = areasInfo.get(i);
//				writingColumnIndex = 0;
//				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, i + 1);
//				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, areaVO.getCountryCode() + " - " + areaVO.getCountryName());
//				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, areaVO.getProvinceCode() + " - " + areaVO.getProvinceName());
//				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, areaVO.getDistrictCode() + " - " + areaVO.getDistrictName());
//				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, areaVO.getPrecinctCode() + " - " + areaVO.getPrecinctName());
//				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, BusinessUtils.decodeStatus(areaVO.getStatus()));
//			}			
//		}
//	}
//
//	/**
//	 * ghi thong tin "Loai nhan vien" vao file de hien thi combobox chon tren file
//	 * @author tuannd20
//	 * @param workbook Workbook ghi du lieu
//	 * @param dataSheetIndex Vi tri sheet ghi du lieu
//	 * @param staffTypeNames Danh sach ten Loai nhan vien se ghi vao file
//	 * @since 28/03/2015
//	 */
//	private void writeStaffTypeDataTo(XSSFWorkbook workbook, int dataSheetIndex, List<OrganizationUnitTypeVO> staffTypes) {
//		XSSFSheet dataSheet = workbook.getSheetAt(dataSheetIndex);
//		if (dataSheet != null) {
//			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
//			for (int i = 0, size = staffTypes.size(); i < size; i++) {
//				/*XSSFRow row = dataSheet.getRow(i) != null ? dataSheet.getRow(i) : dataSheet.createRow(i);
//				XSSFCell cell = row.getCell(0) != null ? row.getCell(0) : row.createCell(0);
//				cell.setCellValue(staffTypes.get(i).getName());*/
//				excelProcessUtil.writeCellData(dataSheet, i, 0, staffTypes.get(i).getName());
//			}
//		}
//	}
	
	/**
	 * tao file template dung de import nhan vien
	 * 
	 * @author tuannd20
	 * @return Doi tuong chua duong dan toi file template
	 * @since 28/03/2015
	 */
	public String downloadImportStaffTemplateFile() {
		if (excelType == 1) {
			return this.exportTemplateExcelNV();
		} else if (excelType == 2) {
			return this.exportTemplateExcelNH();
		}
		return JSON;
	}

	/**
	 * tao file template import nhan vien
	 * 
	 * @author hoanv25
	 * @return Doi tuong chua duong dan toi file template
	 * @since Auggust 17,2015
	 */
	public String exportTemplateExcelNV() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			String importTemplateFileName = this.createImportStaffTemplateFile();
			String outputPath = Configuration.getExportExcelPath() + importTemplateFileName;
			result.put(REPORT_PATH, outputPath);
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "UnitTreeCatalogAction.exportTemplateExcelNV - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * tao file template dung de import nganh hang
	 * 
	 * @author hoanv25
	 * @return Doi tuong chua duong dan toi file template
	 * @since Auggust 17,2015
	 */
	public String exportTemplateExcelNH() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			Map<String, Object> params = new HashMap<String, Object>();
			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + ConstantManager.TEMPLATE_SUB_CAT_UNIT;
			String outputPath = exportExcelDataTemplate(params, ConstantManager.TEMPLATE_SUB_CAT_UNIT, outputName);
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			LogUtility.logError(e, "UnitTreeCatalogAction.exportTemplateExcelNV - " + e.getMessage());
		}
		return JSON;
	}

	/**
	 * tao file import
	 * 
	 * @author hoanv25
	 * @return 
	 * @since Auggust 17,2015
	 */
	private String exportExcelDataTemplate(Map<String, Object> lstParam, String tempFileName, String outputName) throws Exception {
		try {
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathKeyShop() + tempFileName;
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			Map<String, Object> params = new HashMap<String, Object>();
			if (lstParam != null && lstParam.size() > 0) {
				params.putAll(lstParam);
			}

			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			return Configuration.getExportExcelPath() + outputName;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Tao file template dung de import don vi
	 * 
	 * @author tuannd20
	 * @return Doi tuong chua duong dan toi file template
	 * @since 30/03/2015
	 */
	public String downloadImportShopTemplateFile() {
		String reportToken = retrieveReportToken(reportCode);
		if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
			result.put(ERROR, true);
			result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
			return JSON;
		}
		String importTemplateFileName = this.createImportShopTemplateFile();
		String outputPath = Configuration.getExportExcelPath() + importTemplateFileName;
		result.put(REPORT_PATH, outputPath);
		result.put(LIST, outputPath);
		result.put(ERROR, false);
		result.put("hasData", true);
		try {
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Tao file mau import don vi
	 * 
	 * @author tuannd20
	 * @return Ten file template moi duoc tao
	 * @since 30/03/2015
	 */
	private String createImportShopTemplateFile() {
		String importTemplateFileName = null;

		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		XSSFWorkbook workbook = null;
		OPCPackage opcPackage = null;
		try {
			/*
			 * tao file template moi tu file template mau
			 */
			String importTemplateFileAbsolutePath = this.generateShopTemplateImportFileFromOriginalFile();
			importTemplateFileName = Paths.get(importTemplateFileAbsolutePath).getFileName().toString();

			/*
			 * ghi du lieu
			 */
			fileInputStream = new FileInputStream(importTemplateFileAbsolutePath);
			opcPackage = OPCPackage.open(fileInputStream);
			workbook = new XSSFWorkbook(opcPackage);

			/*
			 * ghi header sheet import
			 */
			int IMPORT_SHEET_INDEX = 0;
			this.writeShopImportSheetHeader(workbook, IMPORT_SHEET_INDEX);

			/*
			 * khoi tao danh sach loai nhan vien
			 */
			this.initializeTempDataSheetOfShopTemplateImportFile(workbook);

			/*
			 * khoi tao sheet du lieu dia ban
			 */
			this.initializeAreaSheetOfTemplateImportFile(workbook);

			fileOutputStream = new FileOutputStream(importTemplateFileAbsolutePath);
			workbook.write(fileOutputStream);
			fileOutputStream.close();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		} finally {
			IOUtils.closeQuietly(fileInputStream);
			IOUtils.closeQuietly(fileOutputStream);
			IOUtils.closeQuietly(opcPackage);
		}

		return importTemplateFileName;
	}
	
	/**
	 * Khoi tao sheet du lieu tam cua file mau dung de import don vi
	 * 
	 * @author tuannd20
	 * @return XSSFWorkbook File ghi du lieu
	 * @throws BusinessException
	 * @since 30/03/2015
	 */
	private void initializeTempDataSheetOfShopTemplateImportFile(XSSFWorkbook workbook) throws BusinessException {
		OrganizationUnitTypeFilter orgUnitTypeFilter = new OrganizationUnitTypeFilter();
		orgUnitTypeFilter.setNodeType(OrganizationNodeType.SHOP);
		orgUnitTypeFilter.setStatusShopType(ActiveType.RUNNING);
		ObjectVO<OrganizationUnitTypeVO> orgUnitTypeObjectVO = organizationMgr.getListOrganizationUnitType(orgUnitTypeFilter, null);
		List<OrganizationUnitTypeVO> staffTypes = null;
		if (orgUnitTypeObjectVO != null) {
			staffTypes = orgUnitTypeObjectVO.getLstObject();
		}
		if (staffTypes != null && staffTypes.size() > 0) {
			int DATA_SHEET_INDEX = 2;
			this.writeStaffTypeDataTo(workbook, DATA_SHEET_INDEX, staffTypes);
		}		
	}

	/**
	 * Ghi header sheet import
	 * 
	 * @author tuannd20
	 * @param workbook Workbook ghi du lieu
	 * @param importSheetIndex Vi tri sheet ghi du lieu
	 * @since 30/03/2015
	 */
	private void writeShopImportSheetHeader(XSSFWorkbook workbook, int importSheetIndex) {
		XSSFSheet importSheet = workbook.getSheetAt(importSheetIndex);
		if (importSheet != null) {
			workbook.setSheetName(importSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.sheet_name"));
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			final int HEADER_ROW_INDEX = 0;
			int writingColumnIndex = 0;
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.no"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.shop_code"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.shop_name"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.short_hand_name"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.shop_type"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.parent_shop_code"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.parent_shop_name"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.phone"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.mobile"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.fax"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.email"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.tax_code"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.represent_person_name"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.invoice_address"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.delivery_address"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.ward_code"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.area"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.house_number_street"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.sheet.map_position"));
		}
	}

	/**
	 * Ghi header sheet import
	 * 
	 * @author tuannd20
	 * @return String Duong dan tuyet doi toi file template tra ve client
	 * @throws IOException 
	 * @since 30/03/2015
	 */
	private String generateShopTemplateImportFileFromOriginalFile() throws IOException {
		final String resourceDirectoryPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
		final String sourceTemplateFileName = ConstantManager.IMPORT_SHOP_TEMPATE_FILE_NAME + FileExtension.XLSX.getValue();
		final String sourceTemplateFileAbsoluteName = resourceDirectoryPath + File.separator + sourceTemplateFileName;

		String importTemplateFileName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_shop.template_file.import.file_name") + "_"
				+ DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSX.getValue();
		final String importTemplateFileAbsolutePath = Configuration.getStoreRealPath() + File.separator + importTemplateFileName;
		FileUtility.copyFile(sourceTemplateFileAbsoluteName, importTemplateFileAbsolutePath);
		return importTemplateFileAbsolutePath;
	}

	/**
	 * Tao file mau import nhan vien
	 * 
	 * @author tuannd20
	 * @return Ten file template moi duoc tao
	 * @since 28/03/2015
	 */
	private String createImportStaffTemplateFile() {
		String importTemplateFileName = null;

		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		XSSFWorkbook workbook = null;
		OPCPackage opcPackage = null;
		try {
			/*
			 * tao file template moi tu file template mau
			 */
			String importTemplateFileAbsolutePath = this.generateStaffTemplateImportFileFromOriginalFile();
			importTemplateFileName = Paths.get(importTemplateFileAbsolutePath).getFileName().toString();

			/*
			 * ghi du lieu
			 */
			fileInputStream = new FileInputStream(importTemplateFileAbsolutePath);
			opcPackage = OPCPackage.open(fileInputStream);
			workbook = new XSSFWorkbook(opcPackage);

			/*
			 * ghi header sheet import
			 */
			int IMPORT_SHEET_INDEX = 0;
			this.writeStaffImportSheetHeader(workbook, IMPORT_SHEET_INDEX);

			/*
			 * khoi tao danh sach loai nhan vien, hinh thuc ban hang, thuoc tinh dong
			 */
			this.initializeTempDataSheetOfStaffTemplateImportFile(workbook);

			/*
			 * khoi tao sheet du lieu dia ban
			 */
			this.initializeAreaSheetOfTemplateImportFile(workbook);

			fileOutputStream = new FileOutputStream(importTemplateFileAbsolutePath);
			workbook.write(fileOutputStream);
			fileOutputStream.close();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		} finally {
			IOUtils.closeQuietly(fileInputStream);
			IOUtils.closeQuietly(fileOutputStream);
			IOUtils.closeQuietly(opcPackage);
		}

		return importTemplateFileName;
	}
	
	/**
	 * Khoi tao sheet du lieu tam cua file mau dung de import nhan vien
	 * 
	 * @author tuannd20
	 * @return XSSFWorkbook File ghi du lieu
	 * @throws BusinessException
	 * @since 28/03/2015
	 */
	private void initializeTempDataSheetOfStaffTemplateImportFile(XSSFWorkbook workbook) throws BusinessException {
		writeStaffTypeData(workbook);
		writeStaffSaleFormData(workbook);
		writeDynamicAttributeMasterData(workbook);
	}

	private void writeStaffTypeData(XSSFWorkbook workbook) throws BusinessException {
		OrganizationUnitTypeFilter orgUnitTypeFilter = new OrganizationUnitTypeFilter();
		orgUnitTypeFilter.setNodeType(OrganizationNodeType.STAFF);
		orgUnitTypeFilter.setStatusShopType(ActiveType.RUNNING);
		ObjectVO<OrganizationUnitTypeVO> orgUnitTypeObjectVO = organizationMgr.getListOrganizationUnitType(orgUnitTypeFilter, null);
		List<OrganizationUnitTypeVO> staffTypes = null;
		if (orgUnitTypeObjectVO != null) {
			staffTypes = orgUnitTypeObjectVO.getLstObject();
		}
		if (staffTypes != null && staffTypes.size() > 0) {
			int DATA_SHEET_INDEX = 2;
			this.writeStaffTypeDataTo(workbook, DATA_SHEET_INDEX, staffTypes);
		}
	}
	
	private void writeStaffSaleFormData(XSSFWorkbook workbook) throws BusinessException {
		final int DATA_SHEET_INDEX = 2;
		XSSFSheet dataSheet = workbook.getSheetAt(DATA_SHEET_INDEX);
		if (dataSheet == null) {
			return;
		}
		
		List<ApParam> staffSaleForms = getStaffSaleForm();
		if (staffSaleForms == null || staffSaleForms.isEmpty()) {
			return;
		}
		final int startWriteColumn = 1, startWriteRow = 0;
		int writingColumn = startWriteColumn, writingRow = startWriteRow;
		ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
		for (ApParam staffSaleForm: staffSaleForms) {
			excelProcessUtil.writeCellData(dataSheet, writingRow++, writingColumn, codeNameDisplay(staffSaleForm.getApParamCode(), staffSaleForm.getDescription()));
		}
	}
	
	private void writeDynamicAttributeMasterData(XSSFWorkbook workbook) throws BusinessException {
		List<Object> staffDynamicAttributeVO = getStaffDynamicAttributeVO();
		if (staffDynamicAttributeVO == null) {
			return;
		}
		final int DATA_SHEET_INDEX = 2;
		XSSFSheet dataSheet = workbook.getSheetAt(DATA_SHEET_INDEX);
		if (dataSheet == null) {
			return;
		}
		ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
		final int startWriteColumn = 2, startWriteRow = 3;
		int writingColumn = startWriteColumn, writingRow = startWriteRow;
		for (int i = 0, size = staffDynamicAttributeVO.size(); i < size; i++) {
			Object object = staffDynamicAttributeVO.get(i);
			if (object != null && object instanceof AttributeDynamicVO) {
				writingColumn = startWriteColumn;
				AttributeDynamicVO dynamicAttributeVO = (AttributeDynamicVO)object;
				excelProcessUtil.writeCellData(dataSheet, writingRow, writingColumn++, dynamicAttributeVO.getAttributeName());
				excelProcessUtil.writeCellData(dataSheet, writingRow, writingColumn++, dynamicAttributeVO.getType());
				List<AttributeDetailVO> attributeDetailVOs = dynamicAttributeVO.getAttributeDetailVOs();
				if (attributeDetailVOs != null) {
					int numDetails = attributeDetailVOs.size();
					excelProcessUtil.writeCellData(dataSheet, writingRow, writingColumn++, numDetails);
					excelProcessUtil.writeCellData(dataSheet, writingRow, writingColumn++, dynamicAttributeVO.getMandatory());
					for (int j = 0; j < numDetails; j++) {
						AttributeDetailVO attributeDetailVO = attributeDetailVOs.get(j);
						if (attributeDetailVO == null) {
							continue;
						}
						excelProcessUtil.writeCellData(dataSheet, writingRow, writingColumn++, attributeDetailVO.getName());
					}
				}
				writingRow++;
			}
		}
	}
	
	private List<Object> getStaffDynamicAttributeVO() throws BusinessException {
		return dynamicAttributeHelper.getStaffDynamicAttributeVO(ActiveType.RUNNING);
	}

	/**
	 * Khoi tao sheet dia ban tham chieu cua file mau dung de import nhan vien
	 * 
	 * @author tuannd20
	 * @return XSSFWorkbook File ghi du lieu
	 * @throws BusinessException 
	 * @since 28/03/2015
	 */
	private void initializeAreaSheetOfTemplateImportFile(XSSFWorkbook workbook) throws BusinessException {
		List<AreaVO> areasInfo = areaMgr.retrieveAreaWithParentAreaInfo(ActiveType.RUNNING);
		if (areasInfo != null && areasInfo.size() > 0) {
			int AREA_DATA_SHEET_INDEX = 1;
			this.writeAreaDataSheet(workbook, AREA_DATA_SHEET_INDEX, areasInfo);
		}
	}

	/**
	 * Ghi header sheet import
	 * 
	 * @author tuannd20
	 * @return String Duong dan tuyet doi toi file template tra ve client
	 * @throws IOException 
	 * @since 28/03/2015
	 */
	private String generateStaffTemplateImportFileFromOriginalFile() throws IOException {
		final String resourceDirectoryPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
		final String sourceTemplateFileName = ConstantManager.IMPORT_STAFF_TEMPATE_FILE_NAME + FileExtension.XLSM.getValue();
		final String sourceTemplateFileAbsoluteName = resourceDirectoryPath + File.separator + sourceTemplateFileName;

		String importTemplateFileName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.file_name") + "_"
				+ DateUtil.toDateString(DateUtil.now(),DateUtil.DATE_FORMAT_EXCEL_FILE) + FileExtension.XLSM.getValue();
		final String importTemplateFileAbsolutePath = Configuration.getStoreRealPath() + File.separator + importTemplateFileName;
		FileUtility.copyFile(sourceTemplateFileAbsoluteName, importTemplateFileAbsolutePath);
		return importTemplateFileAbsolutePath;
	}

	/**
	 * Ghi header sheet import
	 * 
	 * @author tuannd20
	 * @param workbook Workbook ghi du lieu
	 * @param importSheetIndex Vi tri sheet ghi du lieu
	 * @since 28/03/2015
	 */
	private void writeStaffImportSheetHeader(XSSFWorkbook workbook, int importSheetIndex) {
		XSSFSheet importSheet = workbook.getSheetAt(importSheetIndex);
		if (importSheet != null) {
			workbook.setSheetName(importSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.sheet_name"));
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			final int HEADER_ROW_INDEX = 0;
			int writingColumnIndex = 0;
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.no"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.shop_code"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.shop_name"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.staff_code"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.staff_name"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.staff_type"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.gender"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.start_working_time"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.phone"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.mobile"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.email"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.precint_code"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.area"));
			excelProcessUtil.writeCellData(importSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.street_house_number"));
		}
	}

	/**
	* Ghi thong tin dia ban
	* @author tuannd20
	* @param workbook Workbook ghi du lieu
	* @param areaDataSheetIndex Vi tri sheet ghi du lieu
	* @param areasInfo Danh sach thong tin dia ban
	* @since 28/03/2015
	*/
	private void writeAreaDataSheet(XSSFWorkbook workbook, int areaDataSheetIndex, List<AreaVO> areasInfo) {
		XSSFSheet areaDataSheet = workbook.getSheetAt(areaDataSheetIndex);
		if (areaDataSheet != null) {
			workbook.setSheetName(areaDataSheetIndex, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.import.sheet.area_sheet_name"));
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			/*
			* ghi thong tin header
			*/
			final int HEADER_ROW_INDEX = 0;
			int writingColumnIndex = 0;
			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.search_unit.search_grid.no"));
			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.country"));
			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.province"));
			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.district"));
			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.precinct"));
			excelProcessUtil.writeCellData(areaDataSheet, HEADER_ROW_INDEX, writingColumnIndex++, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import_staff.template_file.area.status"));
			
			/*
			* ghi du lieu
			*/
			int START_WRITE_ROW = 1;
			for (int i = 0, size = areasInfo.size(), writingRowIndex = START_WRITE_ROW; i < size; i++, writingRowIndex++) {
				AreaVO areaVO = areasInfo.get(i);
				writingColumnIndex = 0;
				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, i + 1);
				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, areaVO.getCountryCode() + " - " + areaVO.getCountryName());
				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, areaVO.getProvinceCode() + " - " + areaVO.getProvinceName());
				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, areaVO.getDistrictCode() + " - " + areaVO.getDistrictName());
				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, areaVO.getPrecinctCode() + " - " + areaVO.getPrecinctName());
				excelProcessUtil.writeCellData(areaDataSheet, writingRowIndex, writingColumnIndex++, BusinessUtils.decodeStatus(areaVO.getStatus()));
			}
		}
	}

	/**
	 * ghi thong tin "Loai nhan vien" vao file de hien thi combobox chon tren file
	 * 
	 * @author tuannd20
	 * @param workbook Workbook ghi du lieu
	 * @param dataSheetIndex Vi tri sheet ghi du lieu
	 * @param staffTypeNames Danh sach ten Loai nhan vien se ghi vao file
	 * @since 28/03/2015
	 */
	private void writeStaffTypeDataTo(XSSFWorkbook workbook, int dataSheetIndex, List<OrganizationUnitTypeVO> staffTypes) {
		XSSFSheet dataSheet = workbook.getSheetAt(dataSheetIndex);
		if (dataSheet != null) {
			ExcelPOIProcessUtils excelProcessUtil = new ExcelPOIProcessUtils();
			for (int i = 0, size = staffTypes.size(); i < size; i++) {
				excelProcessUtil.writeCellData(dataSheet, i, 0, staffTypes.get(i).getName());
			}
		}
	}


	public String showShopInfo() {
		try {
			shop = shopMgr.getShopById(shId);
			shopTypeEle = new ShopType();
			shopTypeEle = organizationMgr.getShopTypeById(shopType);
			if (shopTypeEle == null) {
				result.put(ERROR, true);
				result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "organization_shop_is_not_shop_type"));
				return ERROR;
			}
			//Xu ly XSS
//			shopTypeEle.setName(StringUtil.escapeHtml4ByStringSingle(shopTypeEle.getName()));
//			shopTypeEle.setPrefix(StringUtil.escapeHtml4ByStringSingle(shopTypeEle.getPrefix()));
//			shopTypeEle.setDescription(StringUtil.escapeHtml4ByStringSingle(shopTypeEle.getDescription()));
			//shop.setType(shopTypeEle);//p+
			if (shop != null && organizationId == null) {
				//Cap nhat
				parentShop = shop.getParentShop();
				//				allowSelectParentShop = (ShopObjectType.NPP.getValue().equals(shop.getType().getObjectType()) || ShopObjectType.VUNG.getValue().equals(shop.getType().getObjectType()));
				isUpdate = true;
			} else {
				//Them moi
				shop = null; // p+ clear testbox
				parentShop = shopMgr.getShopById(parentShopId);
				allowSelectParentShop = (parentShop == null);
				isUpdate = false;
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * get list unit tree
	 * 
	 * @author
	 * @modifined liemtpt
	 * @return the string
	 */
	public String getListUnitTree() {
		try {
			lstUnitTree = new ArrayList<JETreeNode>();
			Shop currentShop = null;
			if (currentUser != null && currentUser.getShopRoot() != null) {
				currentShop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());				
			}
			if (currentShop != null) {
				if (idSearch != null) {
					Shop shop = shopMgr.getShopById(idSearch);
					List<Shop> lstShop = getListShopParent(shop, currentShop.getId());

					JETreeNode bean = new JETreeNode();
					bean.setId(currentShop.getId().toString());
					bean.setState(ConstantManager.JSTREE_STATE_OPEN);
					JETreeNodeAttr attrNode = new JETreeNodeAttr();
					attrNode.setShop(converterShopTreeVO(currentShop, 1));
					bean.setAttributes(attrNode);
					if (ActiveType.RUNNING.equals(currentShop.getStatus())) {
						bean.setText(currentShop.getShopCode() + " - " + currentShop.getShopName());
					} else {
						bean.setText(currentShop.getShopCode() + " - " + currentShop.getShopName());
					}
					if (currentShop.getType() != null && currentShop.getType().getIconUrl() != null && currentShop.getType().getIconUrl().length() > 0) {
						bean.setIconCls(Configuration.getImgServerPath() + currentShop.getType().getIconUrl());
					}
					lstUnitTree.add(bean);

					if (lstShop.size() > 0) {
						List<JETreeNode> lstT = getTreeNodeByShopParentX(shopMgr, lstShop, lstShop.size() - 1, (staffId != null));
						bean.setChildren(lstT);
					}
					if (bean.getChildren() == null || bean.getChildren().size() == 0) {
						bean.setIconCls("triconline");
					}
				} else if (id != null) {
					Shop shop = shopMgr.getShopById(id);
//					if (shop != null && checkShopInOrgAccessByCode(shop.getShopCode())) {
						lstUnitTree = getTreeNodeByShop(shop.getId());
//					}
				} else {
					JETreeNode cur = new JETreeNode();
					cur.setId(currentShop.getId().toString());
					cur.setState(ConstantManager.JSTREE_STATE_OPEN);
					JETreeNodeAttr attrNode = new JETreeNodeAttr();
					attrNode.setShop(converterShopTreeVO(currentShop, 1));
					cur.setAttributes(attrNode);
					cur.setText(currentShop.getShopCode() + " - " + currentShop.getShopName());
					cur.setChildren(getTreeNodeByShop(currentShop.getId()));
					
					if (currentShop.getType() != null && currentShop.getType().getIconUrl() != null && currentShop.getType().getIconUrl().length() > 0) {
						cur.setIconCls(Configuration.getImgServerPath() + currentShop.getType().getIconUrl());
					}
					lstUnitTree.add(cur);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	private List<JETreeNode> buildTreeFromShops(List<Shop> ancestorAndSiblingShops) {
		List<JETreeNode> tree = new ArrayList<JETreeNode>();
		Set<Long> puttedShopOnTree = new HashSet<Long>();
		for (Shop shop : ancestorAndSiblingShops) {
			if (puttedShopOnTree.contains(shop.getId())) {
				continue;
			}
			puttedShopOnTree.add(shop.getId());
			
			JETreeNode cur = buildTreeNodeBean(shop);
			cur.setChildren(buildTreeNodeChildrens(shop, ancestorAndSiblingShops, puttedShopOnTree));
			
			JETreeNodeAttr attrNode = new JETreeNodeAttr();
			attrNode.setShop(converterShopTreeVO(shop, 0));
			cur.setAttributes(attrNode);
			tree.add(cur);
		}
		return tree;
	}
	
	private List<JETreeNode> buildTreeNodeChildrens(Shop parentShop, List<Shop> ancestorAndSiblingShops, Set<Long> puttedShopOnTree) {
		List<JETreeNode> children = new ArrayList<JETreeNode>();
		for (Shop shop : ancestorAndSiblingShops) {
			if (puttedShopOnTree.contains(shop.getId())) {
				continue;
			}
			if (!shop.getParentShop().getId().equals(parentShop.getId())) {
				continue;
			}
			
			puttedShopOnTree.add(shop.getId());
			
			JETreeNode cur = buildTreeNodeBean(shop);
			cur.setChildren(buildTreeNodeChildrens(shop, ancestorAndSiblingShops, puttedShopOnTree));
			children.add(cur);
		}
		return children;
	}

	/**
	 * get list context menu by organization id
	 * @since 25/02/2015
	 * @author liemtpt
	 * @return the string
	 * @description lay danh sach shop va staff hien thi right click context menu
	 */
	public String getListContextMenuByOrgId() {
		if (orgId != null) {
			try {
				List<OrganizationVO> lstMenuContext = organizationMgr.getListOrganizationById4ContextMenu(orgId);
				if (lstMenuContext != null && lstMenuContext.size() > 0) {
					result.put("listMenuContext", lstMenuContext);
				} else {
					result.put("listMenuContext", new ArrayList<OrganizationVO>());
				}
			} catch (BusinessException e) {
				LogUtility.logError(e, "UnitTreeCatalogAction.getListContextMenuByOrgId()" + e.getMessage());
			}
		}
		return JSON;
	}
	/**
	 * check chose shop exits staff type id
	 * @since 05/03/2015
	 * @author liemtpt
	 * @return the string
	 * @description kiem tra don vi dich ton tai loai chuc vu nay moi duoc them
	 */
	public String checkChoseShopExitsStaffTypeId(){
		if (shopId != null && staffId != null) {
			try {
				Shop shop = shopMgr.getShopById(shopId);
				if(shop.getOrganization()==null){
					return JSON;
				}
				List<Staff> lst = organizationMgr.getListStaffExitsStaffTypeIdInOrganization(shop.getOrganization().getId(), staffId);
				if (lst != null && lst.size() > 0) {
					result.put("listExits", lst);
				} else {
					result.put("listExits", new ArrayList<Staff>());
				}
			} catch (BusinessException e) {
				LogUtility.logError(e, "UnitTreeCatalogAction.checkChoseShopExitsStaffTypeId() " + e.getMessage());
			}
		}
		return JSON;
	}
	/**
	 * suspended staff
	 * @since 03/02/2015
	 * @author liemtpt
	 * @return the string
	 * @description Tam ngung nhan vien
	 */
	public String suspendedStaff() {
		resetToken(result);
		result.put(ERROR, true);
		try {
			if (staffTreeVO == null) {
				return JSON;
			}
			if (staffTreeVO.getId() != null) {
				Staff staff = staffMgr.getStaffById(staffTreeVO.getId());
				if (staff != null) {
					if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) 
							&& getMapUserId().get(staff.getId()) == null) {
						result.put("errMsg", R.getResource("catalog.staff.code.permission"));
						return JSON;
					}
					if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) 
							&& !this.checkParentOrganization(staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId()), staff)) {
						result.put("errMsg", R.getResource("catalog.unit.tree.create.staff.belong.org"));
						return JSON;
					}
					staff.setStatus(ActiveType.STOPPED);
					staffMgr.updateStaff(staff, getLogInfoVO());
				}
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "Error UnitTreeCatalogAction.suspendedStaff() " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * moved staff
	 * @since 03/02/2015
	 * @author liemtpt
	 * @return the string
	 * @description chuyen don vi
	 */
	public String movedStaff() {
		resetToken(result);
		result.put(ERROR, true);
		try {
			if (shopId == null || staffId == null) {
				return JSON;
			}
			
			Staff staff = staffMgr.getStaffById(staffId);
			Shop shop = shopMgr.getShopById(shopId);
			if(shop.getOrganization() == null){
				return JSON;
			}
			Organization organization = organizationMgr.getOrganizationById(shop.getOrganization().getId());
			if (staff != null && organization != null && shop!= null) {
//				staff.setOrganization(organization);
				if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) 
						&& getMapUserId().get(staff.getId()) == null) {
					result.put("errMsg", R.getResource("catalog.staff.code.permission"));
					return JSON;
				}
				if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) 
						&& !this.checkParentOrganization(staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId()), staff)) {
					result.put("errMsg", R.getResource("catalog.unit.tree.create.staff.belong.org"));
					return JSON;
				}
				if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType())
						&& getMapShopChild().get(shop.getId()) == null) {
					result.put("errMsg", R.getResource("common.shop.not.belong.area"));
					return JSON;
				}
				staff.setStatus(ActiveType.RUNNING);
				staff.setShop(shop);
				staffMgr.updateStaff(staff, getLogInfoVO());
			}
			result.put(ERROR, false);
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "Error UnitTreeCatalogAction.movedStaff() " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}
	
	private List<JETreeNode> getTreeNodeByShopParentX(ShopMgr shopMgr, List<Shop> lstShop, Integer level, boolean isStaffSearching) throws Exception {
		List<JETreeNode> lstUnitTree = new ArrayList<JETreeNode>();
		Shop shT = lstShop.get(level);
		if (level.equals(0)) {
			if (shT != null) {
				lstUnitTree = getTreeNodeByShop(lstShop.get(level).getId());
			}
			return lstUnitTree;
		}

		lstUnitTree = getTreeNodeByShop(shT.getId());
		List<JETreeNode> lstUnitTreeChild = getTreeNodeByShopParentX(shopMgr, lstShop, level - 1, isStaffSearching);
		Shop sh = lstShop.get(level - 1);
		ShopTreeVO attSh = null;
		for (JETreeNode temp : lstUnitTree) {
			attSh = temp.getAttributes().getShop();
			if (attSh == null) {
				continue;
			}
			if (attSh.getId().equals(sh.getId())) {
				temp.setChildren(lstUnitTreeChild);
				if (level == 1 && !isStaffSearching) {
					temp.setState(ConstantManager.JSTREE_STATE_CLOSE);
				} else {
					temp.setState(ConstantManager.JSTREE_STATE_OPEN);
				}
				break;
			}
		}
		return lstUnitTree;
	}

	public List<Shop> getListShopParent(Shop shop, Long shopIdRoot) {
		List<Shop> lstShop = new ArrayList<Shop>();
		try {
			while (shop != null) {
				// Chi lay den cha la shopIdRoot
				if (shopIdRoot != null && shopIdRoot.equals(shop.getId())) {
					lstShop.add(shop);
					shop = shop.getParentShop();
					break;
				} else {
					lstShop.add(shop);
					shop = shop.getParentShop();
				}
			}
		} catch (Exception e) {
		}
		return lstShop;
	}

	/**
	 * converter shop tree vo
	 * @since 03/02/2015
	 * @author 
	 * @modifined liemtpt
	 * @return the string
	 * @description Chuyen don vi theo cau truc VO de su ly node tren cay
	 */
	public ShopTreeVO converterShopTreeVO(Shop shop, Integer parent) {
		ShopTreeVO st = new ShopTreeVO();
		try {
			if (shop != null) {
				st.setId(shop.getId());
				st.setShopCode(shop.getShopCode());
				st.setShopName(shop.getShopName());
				st.setType(shop.getType());
				st.setStatus(shop.getStatus());
				st.setOrganizationId(shop.getOrganization().getId());
				if (parent == 1 && shop.getParentShop() != null) {
					st.setParentShop(converterShopTreeVO(shop.getParentShop(), 0));
				} else {
					st.setParentShop(null);
				}
			}
		} catch (Exception e) {
		}
		return st;
	}
	/**
	 * converter staff tree vo
	 * @author 
	 * @ since 03/02/2015
	 * @modifined liemtpt
	 * @return the string
	 * @description Chuyen nhan vien theo cau truc VO de su ly node tren cay
	 */
	public StaffTreeVO converterStaffTreeVO(Staff staff) {
		StaffTreeVO st = new StaffTreeVO();
		try {
			if (staff != null) {
				st.setId(staff.getId());
				st.setShop(converterShopTreeVO(staff.getShop(), 1));
				st.setStaffCode(staff.getStaffCode());
				st.setStaffName(staff.getStaffName());
				st.setStaffType(staff.getStaffType());
				st.setStatus(staff.getStatus());
				st.setOrganizationId(staff.getOrganization().getId());
			}
		} catch (Exception e) {
		}
		return st;
	}

	/**
	 * get staff name
	 * 
	 * @author
	 * @modifined liemtpt
	 * @Description lay ten nhan vien theo prefix va name
	 * @return the string
	 */
	public String getStaffName(Staff staff) {
		String result = "";
		if (staff != null) {
			if (staff.getStaffType() != null && staff.getStaffType().getPrefix() != null) {
				result = staff.getStaffType().getPrefix() + " - " + staff.getStaffName();
			} else {
				result = staff.getStaffName();
			}
		}
		return result;
	}

	public String getIconForStaff(Staff staff) {
		String icon = "";
		return icon;
	}

	/**
	 * parse staff
	 * 
	 * @author
	 * @modifined liemtpt
	 * @return the string
	 */
	public List<JETreeNode> parseStaff(List<Staff> lstStaff) {
		List<JETreeNode> lstBean = new ArrayList<JETreeNode>();
		for (Staff staff : lstStaff) {
			if (staff != null) {
				JETreeNode node = new JETreeNode();
				JETreeNodeAttr attr = new JETreeNodeAttr();
				attr.setStaff(converterStaffTreeVO(staff));
				node.setAttributes(attr);
				node.setId("s" + staff.getId());
				node.setState(ConstantManager.JSTREE_STATE_LEAF);
				node.setText(getStaffName(staff));
				if (staff.getStaffType() != null && staff.getStaffType().getIconUrl() != null && staff.getStaffType().getIconUrl().length() > 0) {
					node.setIconCls(Configuration.getImgServerPath() + staff.getStaffType().getIconUrl());
				}
				if (ActiveType.RUNNING.equals(staff.getStatus())) {
					if (staff.getStaffType() != null && staff.getStaffType().getPrefix() != null) {
						node.setText(staff.getStaffType().getPrefix() + " - " + staff.getStaffName());
					} else {
						node.setText(staff.getStaffName());

					}
				} else {
					if (staff.getStaffType() != null && staff.getStaffType().getPrefix() != null) {
						node.setText(staff.getStaffType().getPrefix() + " - " + staff.getStaffName());
					} else {
						node.setText(staff.getStaffName());

					}
				}
				lstBean.add(node);
			}
		}
		return lstBean;
	}

	/**
	 * get tree node by shop
	 * 
	 * @author
	 * @modifined liemtpt
	 * @Description lay danh sach node cho cay don vi theo shop
	 * @return the string
	 */
	public List<JETreeNode> getTreeNodeByShop(Long id) {
		List<JETreeNode> lstUnitTree = new ArrayList<JETreeNode>();
		ShopFilter filter = new ShopFilter();
		try {
			StaffFilter filterStaff = new StaffFilter();
			filterStaff.setShopId(id);
			filterStaff.setIsStaffTypeId(true);
			List<Staff> lstStaff = staffMgr.getListStaffByShop(filterStaff);
			if (lstStaff != null) {
				lstStaff = filterPriviledgedStaffs(lstStaff, true);
				List<JETreeNode> lstNodeStaff = parseStaff(lstStaff);
				for (JETreeNode temp : lstNodeStaff) {
					lstUnitTree.add(temp);
				}
			}
			Shop shop = shopMgr.getShopById(id);
			if (shop != null) {
				filter = new ShopFilter();
				filter.setIsGetOneChildLevel(true);
				filter.setLstParentId(Arrays.asList(id));
				filter.setIsShopTypeId(true);
				ObjectVO<Shop> lstSubShop = shopMgr.getListShop(filter);
				List<Shop> shops = lstSubShop.getLstObject();
				shops = filterPriviledgedShops(shops, true);
				List<JETreeNode> lstNodeShop = parseShop(shops);
				for (JETreeNode temp : lstNodeShop) {
					if (temp.getAttributes() != null && temp.getAttributes().getShop() != null) {
						lstUnitTree.add(temp);
					}
				}
			}
		} catch (Exception e) {

		}
		return lstUnitTree;
	}
	
	/**
	 * parse shop
	 * 
	 * @author
	 * @modifined liemtpt
	 * @return the string
	 */
	public List<JETreeNode> parseShop(List<Shop> lst) {
		List<JETreeNode> lstBean = new ArrayList<JETreeNode>();
		for (Shop shop : lst) {
			if (shop != null) {
				JETreeNode bean = buildTreeNodeBean(shop);
				lstBean.add(bean);
			}
		}
		return lstBean;
	}

	private JETreeNode buildTreeNodeBean(Shop shop) {
		JETreeNode bean = new JETreeNode();
		JETreeNodeAttr attr = new JETreeNodeAttr();
		attr.setShop(converterShopTreeVO(shop, 1));
		bean.setId(shop.getId().toString());
		bean.setAttributes(attr);
		if (shop.getType() != null && shop.getType().getIconUrl() != null && shop.getType().getIconUrl().length() > 0) {
			bean.setIconCls(Configuration.getImgServerPath() + shop.getType().getIconUrl());
		}
		if (ActiveType.RUNNING.equals(shop.getStatus())) {
			if (shop.getType() != null && shop.getType().getPrefix() != null) {
				bean.setText(shop.getType().getPrefix() + " - " + shop.getShopName());
			} else {
				bean.setText(shop.getShopName());
			}
		} else {
			if (shop.getType() != null && shop.getType().getPrefix() != null) {
				bean.setText(shop.getType().getPrefix() + " - " + shop.getShopName());
			} else {
				bean.setText(shop.getShopName());
			}
		}
		bean.setState(ConstantManager.JSTREE_STATE_CLOSE);
		return bean;
	}

	/***************************** PHUOCDH2 BEGIN ***************************************/
	/**
	 * addOrUpdateShopInfo: thuc hien them moi , cap nhat thong tin don vi
	 * 
	 * @return the string
	 * @author phuocdh2
	 * @description: thuc hien them moi, cap nhat don vi tu cay don vi
	 * @createDate 26/02/2015
	 * 
	 * @update hoanv25
	 * @description: Tu dong tao lich lam viec khi them moi don vi
	 * @since August 14,2015
	 */
	public String addOrUpdateShopInfo() {
		resetToken(result);
		errMsg = "";
		try {
			errMsg = ValidateUtil.validateField(shopCode, "catalog.unit.tree.code", 100, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(shopName, "catalog.unit.tree.name", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(abbreviation, "organization_abbreviation_name", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(phoneNumber, "organization_phone", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(mobileNumber, "organization_mobile", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(faxNumber, "organization_fax", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			}
			if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(email)) {
				errMsg = ValidateUtil.validateField(email, "organization_email", null, ConstantManager.ERR_INVALID_EMAIL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(taxNumber, "organization_tax_Number", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(contactName, "organization_contact_Name", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(addressShipTo, "organization_address_ShipTo", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(address, "organization_address", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return ERROR;
			}
			Staff userLogin = staffMgr.getStaffByCode(currentUser.getUserName());
			Shop shop = shopMgr.getShopByCode(shopCode);
			/** cap nhat shop */
			if (shopId != null && shopId > 0) {
				if (shop != null) {
					errMsg = validateInput();
					if (!StringUtil.isNullOrEmpty(errMsg)) {
						result.put("errMsg", errMsg);
						result.put(ERROR, true);
						return ERROR;
					}
					if (shop.getParentShop() != null && !ActiveType.RUNNING.equals(shop.getParentShop().getStatus()) && ActiveType.RUNNING.getValue().equals(status)) {//cha khong hoat dong ma con hoat dong
						result.put(ERROR, true);
						errMsg = R.getResource("organization_shop_have_stopped_sub_shop");
						result.put("errMsg", errMsg);
						return ERROR;
					}
					if (userLogin == null || userLogin.getShop() == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_AREA));
						return ERROR;
					}
					if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) && getMapShopChild().get(shop.getId()) == null) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("common.permission.change"));
						return ERROR;
					}
					shop.setUpdateDate(commonMgr.getSysDate());
					shop.setUpdateUser(currentUser.getUserName());
					shop.setShopName(shopName);
					ShopType shopTypeEle = new ShopType();
					shopTypeEle = organizationMgr.getShopTypeById(shopType);
					if (shopTypeEle == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "organization_shop_is_not_shop_type"));
						return ERROR;
					}
					shop.setType(shopTypeEle);
					if (ActiveType.STOPPED.getValue().equals(status)) {
						if (shopMgr.checkShopHaveActiveSubShop(ActiveType.RUNNING, shopId)) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("organization_shop_have_active_sub_shop"));
							return ERROR;
						}
						if (shopMgr.checkShopHaveActiveStaff(ActiveType.RUNNING, shopId)) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("organization_shop_have_active_staff"));
							return ERROR;
						}
						// vuongmq; 18/03/2016; khong check KH tam ngung
						/*if (shopMgr.checkShopHaveActiveCustomer(ActiveType.RUNNING, shopId)) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("organization_shop_have_active_customer"));
							return ERROR;
						}*/						
					} else if (ActiveType.RUNNING.getValue().equals(status)) {
						if (shop.getParentShop() != null && !ActiveType.RUNNING.equals(shop.getParentShop().getStatus())) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("organization_shop_have_stopped_sub_shop"));
							return ERROR;
						}						
					} else {
						if (ActiveType.parseValue(status) == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_PERMISSION_CHANGE));
							return ERROR;
						}
					}
					shop.setAbbreviation(abbreviation);
					shop.setStatus(ActiveType.parseValue(status));
					//p++
					shop.setPhone(phoneNumber);
					ShopFilter filter = new ShopFilter();
					shop.setMobiphone(mobileNumber);					
					shop.setFax(faxNumber);
					shop.setEmail(email);
					shop.setInvoiceNumberAccount(accountNumber);
					shop.setInvoiceBankName(bankName);
					shop.setTaxNum(taxNumber);
					filter = new ShopFilter();
					filter.setShopId(shopId);
					filter.setTaxNum(taxNumber);
					if (!StringUtil.isNullOrEmpty(taxNumber) && shopMgr.checkIfShopExists(filter)) {
						result.put(ERROR, true);
						errMsg = R.getResource("common.exist", R.getResource("common.tax.code"));
						result.put("errMsg", errMsg);
						return ERROR;
					}
					shop.setBillTo(addressBillTo);
					shop.setContactName(contactName);
					shop.setShipTo(addressShipTo);
					if (areaId == -2 && areaId != null) { // chon all
						shop.setArea(null);
					} else {
						Area area = areaMgr.getAreaById(areaId);
						if (area == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "catalog.area.tree"));
							return ERROR;
						}
						if (!AreaType.WARD.equals(area.getType())) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("catalog.unit.tree.area.is.ward"));
							return ERROR;
						}
						shop.setArea(area);
						shop.setAreaCode(area.getAreaCode());
					}
					/**
					 * vuongmq; 05/03/2015;cap nhat shop; xoa vi tri lat,lng thi van cap nhat lai
					 * vuongmq; 18/03/2016;cap nhat shop; xoa vi tri lat,lng.
					 */
					if (lat != null && !StringUtil.isNullOrEmpty(lat)) {
						shop.setLat(Float.valueOf(lat));
					} else {
						shop.setLat(null);
					}
					if (lng != null && !StringUtil.isNullOrEmpty(lng)) {
						shop.setLng(Float.valueOf(lng));
					} else {
						shop.setLng(null);
					}

					shop.setAddress(address);
					/*boolean isCancelWW = false;
					if (isCancelWW) {
						shopMgr.updateShopWithCancelSalerTrainingPlan(shop, getLogInfoVO());
					} else {
						shopMgr.updateShop(shop, getLogInfoVO());
					}*/
					shopMgr.updateShop(shop, getLogInfoVO());
					result.put("shopId", shopId);
					if (shop.getParentShop() != null) {
						result.put("parentShopId", shop.getParentShop().getId());
						//this.setSessionChangeShopRoot(currentUser.getShopRoot());
					} else {
						result.put("parentShopId", shop.getParentShop());
						//this.setSessionChangeShopRoot(currentUser.getShopRoot());
					}
				}
			} else {
				/** them moi shop */
				if (shop != null) {
					result.put(ERROR, true);
					errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_EXIST, R.getResource("catalog.unit.tree.code.value", shopCode));
					result.put("errMsg", errMsg);
					return ERROR;

				} else {
					errMsg = validateInput();
					if (!StringUtil.isNullOrEmpty(errMsg)) {
						result.put("errMsg", errMsg);
						result.put(ERROR, true);
						return ERROR;
					}
					Shop newShop = new Shop();
					Shop parentShop = shopMgr.getShopByCode(parentShopCode);
					if (parentShop == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "catalog.unit.tree.parent.code"));
						return ERROR;
					} else if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) && getMapShopChild().get(parentShop.getId()) == null) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("common.permission.change"));
						return ERROR;
					} else if (!ActiveType.RUNNING.equals(parentShop.getStatus())) {
						errMsg = R.getResource("catalog.unit.update.parent.shop.not.running");
						result.put("errMsg", errMsg);
						result.put(ERROR, true);
						return ERROR;
					}
					newShop.setParentShop(parentShop);
					newShop.setCreateUser(currentUser.getUserName());
					newShop.setShopCode(shopCode);
					newShop.setShopName(shopName);
					ShopType shopTypeEle = new ShopType();
					shopTypeEle = organizationMgr.getShopTypeById(shopType);
					newShop.setAbbreviation(abbreviation);
					if (shopTypeEle == null) {
						result.put(ERROR, true);
						result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "catalog.shop.type.tree"));
						return ERROR;
					}
					newShop.setType(shopTypeEle);//p+
					Organization org = new Organization();
					if (organizationId != null) {
						org = organizationMgr.getOrganizationById(organizationId);
						if (org == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, false, "organization_shop_is_not_organization_type"));
							return ERROR;
						}
						newShop.setOrganization(org);
					}
					if (parentShop.getType() == null) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("catelog.parentshop.type.is.not.avail"));
						return ERROR;
					}
					if (status != null && status != ConstantManager.NOT_STATUS) {
						newShop.setStatus(ActiveType.parseValue(status));
					}
					if (status != ActiveType.RUNNING.getValue() && status != ActiveType.STOPPED.getValue()) {
						result.put(ERROR, true);
						result.put("errMsg", R.getResource("catalog.unit.tree.status"));
						return ERROR;
					}
					//p++
					newShop.setPhone(phoneNumber);
					newShop.setMobiphone(mobileNumber);
					ShopFilter filter = new ShopFilter();					
					newShop.setFax(faxNumber);
					newShop.setEmail(email);
					newShop.setTaxNum(taxNumber);
					filter = new ShopFilter();
					filter.setShopId(shopId);
					filter.setTaxNum(taxNumber);
					if (!StringUtil.isNullOrEmpty(taxNumber) && shopMgr.checkIfShopExists(filter)) {
						result.put(ERROR, true);
						errMsg = R.getResource("common.exist", R.getResource("common.tax.code"));
						result.put("errMsg", errMsg);
						return ERROR;
					}
					newShop.setBillTo(addressBillTo);
					newShop.setContactName(contactName);
					newShop.setShipTo(addressShipTo);

					if (areaId == -2 && areaId != null) { // chon all
						newShop.setArea(null);
					} else {
						Area area = areaMgr.getAreaById(areaId);
						if (area == null) {
							result.put(ERROR, true);
							result.put("errMsg", ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, false, "catalog.area.tree"));
							return ERROR;
						}
						if (!AreaType.WARD.equals(area.getType())) {
							result.put(ERROR, true);
							result.put("errMsg", R.getResource("catalog.unit.tree.area.is.ward"));
							return ERROR;
						}
						newShop.setArea(area);
						newShop.setAreaCode(area.getAreaCode());
					}
					newShop.setAddress(address);
					if (!StringUtil.isNullOrEmpty(lat)) {
						newShop.setLat(Float.valueOf(lat));
					}
					if (!StringUtil.isNullOrEmpty(lat)) {
						newShop.setLng(Float.valueOf(lng));
					}
					//newShop.setShopChannel(1);
					newShop = shopMgr.createShop(newShop, getLogInfoVO());
					if (newShop != null) {
						Shop shopTemp = null;
						shopTemp = shopMgr.getShopById(newShop.getId());
						fromMonth = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
						Integer value = exceptionDayMgr.createWorkDateCopyShopParent(shopTemp, WorkingDateType.CHI_DINH_DON_VI, fromMonth, null, null, null, getLogInfoVO());
						if (value != null && value == WorkingDateType.DE_QUY.getValue()) {
							/** +Nếu đang sử dụng lịch theo lịch cha: */
							Date fromMonthDate = DateUtil.parse(fromMonth, DateUtil.DATE_FORMAT_DDMMYYYY);
							exceptionDayMgr.callProceduceWorkingDate(shopTemp.getId(), fromMonthDate, null, WorkingDateProcedureType.THIET_LAP_RIENG);
						}
						setShopId2Session(newShop.getId());
						result.put("shopId", newShop.getId());
						//this.setSessionChangeShopRoot(currentUser.getShopRoot());
					}
					if (newShop.getParentShop() != null) {
						result.put("parentShopId", newShop.getParentShop().getId());
						//this.setSessionChangeShopRoot(currentUser.getShopRoot());
					} else {
						result.put("parentShopId", newShop.getParentShop());
						//this.setSessionChangeShopRoot(currentUser.getShopRoot());
					}
				}
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logError(e, "Error UnitTreeCatalogAction.addOrUpdateShopInfo: " + e.getMessage());
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		result.put(ERROR, false);
		return SUCCESS;
	}

	/***************************** PHUOCDH2 END **************************************/
	/**
	 * kiem tra du lieu dau vao (fix xss)
	 * @author trietptm
	 * @return thong bao loi
	 */
	private String validateInput() {
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(shopName, "catalog.promotion.group.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(phoneNumber, "organization_phone", 20, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(mobileNumber, "organization_mobile", 20, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(faxNumber, "organization_fax", 20, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(email)) {
			errMsg = ValidateUtil.validateField(email, "organization_email", 40, ConstantManager.ERR_INVALID_EMAIL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(taxNumber, "organization_tax_Number", 20, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(contactName, "organization_contact_Name", 200, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(addressBillTo, "organization_address_BillTo", 200, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(addressShipTo, "organization_address_ShipTo", 200, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			errMsg = ValidateUtil.validateField(address, "organization_address", 200, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, ConstantManager.ERR_MAX_LENGTH);
		}
		return errMsg;
	}
	
	/**
	 * Search Staff.
	 * 
	 * @return the string
	 * @author nhanlt
	 * @since 16/05/2013
	 */
	public String searchStaffGroup() {

		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<StaffVO> kPaging = new KPaging<StaffVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			setPriviledgeInfo(unitFilter);
			ObjectVO<StaffVO> foundStaffs = staffMgr.findStaffVOBy(kPaging, unitFilter);
			if (foundStaffs != null) {
				result.put("total", foundStaffs.getkPaging().getTotalRows());
				result.put("rows", foundStaffs.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<StaffVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	/**
	 * Search Staff.
	 * 
	 * @return the string
	 * @author nhanlt
	 * @since 16/05/2013
	 */
	public String searchStaff() {
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<Staff> kPaging = new KPaging<Staff>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			StaffFilter filter = new StaffFilter();
			filter.setkPaging(kPaging);
			if (shopId != null && shopId > 0) {
				filter.setShopId(shopId);
			} else {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			}
			if (status != null && status >= 0) {
				filter.setStatus(ActiveType.parseValue(status));
			}
			filter.setStaffCode(staffCode);
			filter.setStaffName(staffName);
			ObjectVO<Staff> vo = staffMgr.getListStaffOfShop(filter);
			if (vo != null) {
				result.put("total", vo.getkPaging().getTotalRows());
				result.put("rows", vo.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<Staff>());
			}
		} catch (Exception ex) {
			LogUtility.logError(ex, "UnitTreeCatalog.searchStaff - " + ex.getMessage());
		}
		return JSON;
	}

	public String loadSearchStaff() {
		try {
			if (currentUser != null) {
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
			}
			ChannelTypeFilter filter = new ChannelTypeFilter();
			filter.setType(ChannelTypeType.STAFF);
			filter.setStatus(ActiveType.RUNNING);
			ObjectVO<ChannelType> lstStaffTypeTmp = channelTypeMgr.getListChannelType(filter, null);
			lstStaffType = lstStaffTypeTmp.getLstObject();
			lstSaleType = apParamMgr.getListApParam(ApParamType.STAFF_SALE_TYPE, ActiveType.RUNNING);
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * @author vuongmq
	 * @date Feb 26,2015 View detail staff
	 * 
	 * @update hoanv25 - bo sung them hinh thuc ban hang va nganh hang con
	 * @since August 14,2015
	 */
	public String viewStaffDetail() {
		try {
			Staff userShop = getStaffByCurrentUser();
			if (userShop == null) {
				return PAGE_NOT_PERMISSION;
			}
			/**Lay danh sach hinh thuc ban hang - hoanv25 August 14/2015*/
			lstProductType = getStaffSaleForm();
			/** vuongmq; 26/02/2015; lay dah sach loai nhan vien */
			OrganizationSystemFilter<StaffType> filter = new OrganizationSystemFilter<StaffType>();
			List<StaffType> lstStaffTypeViewTmp = organizationMgr.getListStaffType(filter);
			lstStaffTypeView = new ArrayList<StaffType>();
			if (lstStaffTypeViewTmp != null && !lstStaffTypeViewTmp.isEmpty()) {
				for (StaffType value: lstStaffTypeViewTmp) {
					value.setName(StringUtil.escapeHtml4ByStringSingle(value.getName()));
					lstStaffTypeView.add(value);
				}
			}
			//lstStaffTypeView = organizationMgr.getListStaffType(filter);
			/** vuongmq; 26/02/2015; lay dah sach dia ban */
			ObjectVO<Area> areaVO = new ObjectVO<Area>();
			areaVO = areaMgr.getListArea(null, null, null, null, ActiveType.RUNNING, null, null, null, null, null, null, AreaType.PROVINCE, null,null,null);
			if (areaVO != null) {
				areas = areaVO.getLstObject();
			}
			if (staffId != null) {
				staff = staffMgr.getStaffById(staffId);
				if (staff == null) {
					return LIST;
				}
				/** vuongmq; 26/02/2015; lay dah sach thuoc tinh mo rong */
				StaffAttributeFilter filterAttr = new StaffAttributeFilter();
				filterAttr.setStaffId(staffId);
				lstStaffAttributes = staffAttributeMgr.getListStaffAttributeVOFilter(filterAttr);

				if (staff.getShop() != null) {
					shopCode = staff.getShop().getShopCode();
				}
				if (staff.getArea() != null) {
					province = staff.getArea().getProvinceName();
					district = staff.getArea().getDistrictName();
					ward = staff.getArea().getPrecinctName();
				}
				if (staff.getStaffType() != null) {
					staffType = staff.getStaffType().getId();
					if (staff.getStaffType().getSpecificType() != null) {
						staffSpecificType =  staff.getStaffType().getSpecificType().getValue().longValue();						
					}
				}
				if (staff.getSaleTypeCode() != null) {
					saleTypeCode = staff.getSaleTypeCode();
				}
				ObjectVO<StaffSaleCat> vo = staffMgr.getListStaffSaleCat(null, staff.getId());
				subCatStr = "";
				if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
					for (StaffSaleCat ssc : vo.getLstObject()) {
						subCatStr += subCatStr.equals("") ? ssc.getCat().getId() : "," + ssc.getCat().getId();
					}
				}
			} else {
				StaffAttributeFilter filterAttr = new StaffAttributeFilter();
				lstStaffAttributes = staffAttributeMgr.getListStaffAttributeVOFilter(filterAttr);
			}
		} catch (Exception e) {
			LogUtility.logError(e, "UnitTreeCatalogaction.viewDetail()" + e.getMessage());
		}
		return LIST;
	}

	private List<ApParam> getStaffSaleForm() throws BusinessException {
		ObjectVO<ApParam> tmp = productInfoMgr.getListShopProductInfoType(null, ActiveType.PRODUCTTYPE);
		return tmp != null ? tmp.getLstObject() : null;
	}
	
	public Boolean checkParentOrganization (Staff root, Staff staff) {
		try {
			if (root == null || staff == null || root.getOrganization() == null || staff.getOrganization() == null) {
				return false;
			}
			OrganizationFilter filter = new OrganizationFilter();
			filter.setParentOrgId(root.getOrganization().getId());
			filter.setOrganizationId(staff.getOrganization().getId());
			return organizationMgr.checkParentOrg(filter);
		} catch (BusinessException e) {
			LogUtility.logError(e, "UnitTreeCatalogaction.checkParentOrganization()" + e.getMessage());
		}
		return false;
	}

	/**
	 * Change staff info.
	 * 
	 * @return the string
	 * @author nhanlt
	 * @see search keyword //CRM Update
	 * @since Oct 1, 2012
	 */
	public String saveOrUpdateStaff() {
		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(staffCode, "catalog.staff.code", STAFF_CODE_MAX_LENGTH, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(staffName, "catalog.staff.name", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(houseNumber, "catalog.housenumber", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(staffPhone, "organization_mobile", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(staffTelephone, "organization_phone", null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER);
				}
				if (StringUtil.isNullOrEmpty(errMsg) && !StringUtil.isNullOrEmpty(email)) {
					errMsg = ValidateUtil.validateField(email, "catalog.email.code", null, ConstantManager.ERR_INVALID_EMAIL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				staff = staffMgr.getStaffByCode(staffCode);
				/** cap nhat nhan vien */
				if (staffId != null && staffId > 0) {
					if (staff != null) {
						if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType())) {
							if (getMapUserId().get(staff.getId()) == null) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("common.permission.change"));
								return JSON;
							}
							if (!this.checkParentOrganization(staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId()), staff)) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("catalog.unit.tree.create.staff.belong.org"));
								return JSON;
							}
						}
						Shop sTmp = shopMgr.getShopById(shopId);
						if (sTmp != null) {
							//staff.setShop(sTmp); //khong cap nhat shop
							if (status != null && status != ConstantManager.NOT_STATUS) {							
								if (ActiveType.STOPPED.equals(staff.getStatus()) && ActiveType.RUNNING.getValue().equals(status)) {
									if (ActiveType.STOPPED.equals(sTmp.getStatus())) {
										result.put(ERROR, true);
										errMsg = R.getResource("catalog_unit_chuyen_nhan_vien_tam_ngung_sang_hoat_dong");
										result.put("errMsg", errMsg);
										return JSON;
									}
								}
								staff.setStatus(ActiveType.parseValue(status));
							}
							staff.setStaffName(staffName);
							Area area = areaMgr.getAreaById(areaId);
							if (area != null) {
								if (!area.getType().equals(AreaType.WARD)) {
									result.put(ERROR, true);
									errMsg = R.getResource("catalog.area.tree.not.ward", R.getResource("catalog.area.tree"));
									result.put("errMsg", errMsg);
									return JSON;
								}
								staff.setArea(area);
								staff.setAddress(this.valueAreaAddress(houseNumber, area));
							}
							staff.setHousenumber(houseNumber);
							staff.setMobilephone(staffPhone);
							staff.setPhone(staffTelephone);
							staff.setEmail(email);
							if (gender != null && gender != -1) {
								staff.setGender(GenderType.parseValue(gender));
							} else {
								staff.setGender(null);
							}
							staff.setStartWorkingDay(null);
							if (!StringUtil.isNullOrEmpty(workStartDate)) {
								staff.setStartWorkingDay(ths.dms.web.utils.DateUtil.parse(workStartDate, ConstantManager.FULL_DATE_FORMAT));
							}
							staff.setSaleTypeCode(null);
							if (!StringUtil.isNullOrEmpty(orderProduct)) {
								staff.setSaleTypeCode(orderProduct);
							}
							staff.setUpdateUser(currentUser.getUserName());
							// Xu ly update nganh hang cn 
							//xoa tat ca staff_sale_cat
							ObjectVO<StaffSaleCat> vo = staffMgr.getListStaffSaleCat(null, staff.getId());
							if (vo != null && vo.getLstObject() != null && vo.getLstObject().size() > 0) {
								for (StaffSaleCat ssc : vo.getLstObject()) {
									staffMgr.deleteStaffSaleCat(ssc, getLogInfoVO());
								}
							}
							
							//parse thanh list can them
							List<Long> lstUpdate = new ArrayList<Long>();
							if (subStaff != null && subStaff.length() >0){
								String[] arrCreateStr = subStaff.split(",");
								for (String value : arrCreateStr) {
									if (StringUtil.isNumberInt(value.trim())) {
										lstUpdate.add(Long.valueOf(value.trim()));
									}
								}
							}							
							if (lstUpdate != null && lstUpdate.size() > 0) {
//								ObjectVO<StaffVO> listCheckDelSub = staffMgr.listCheckDelSubStaff(staff.getId());
//								if (listCheckDelSub != null && listCheckDelSub.getLstObject().size() > 0) {
//									List<Long> lstCheckDeleteUpdate = new ArrayList<Long>();
//									for (int j = 0, s = listCheckDelSub.getLstObject().size(); j < s; j++) {
//										if (lstUpdate.contains(listCheckDelSub.getLstObject().get(j).getCatId())) {
//											lstCheckDeleteUpdate.add(listCheckDelSub.getLstObject().get(j).getId());
//										}
//									}
//									if (lstCheckDeleteUpdate != null && lstCheckDeleteUpdate.size() > 0) {
//										for (int k = 0, s = listCheckDelSub.getLstObject().size(); k < s; k++) {
//											StaffSaleCat idCatBys = staffMgr.getStaffSaleCatById(lstCheckDeleteUpdate.get(k));
//											staffMgr.deleteStaffSaleCat(idCatBys, null);
//										}
//									}
//								}
								
								//them staff_sale_cat moi
								for (int i = 0, sz = lstUpdate.size(); i < sz; i++) {
									ProductInfo lstSubId = productInfoMgr.getProductInfoById(lstUpdate.get(i));
									//Kiem tra xem co sub nao thay doi khong
									StaffSaleCat filter = new StaffSaleCat();
									filter.setStaff(staff);
									if (lstSubId != null) {
										filter.setCat(lstSubId);
									}
									ObjectVO<StaffVO> listCheckSub = staffMgr.listCheckSubStaff(filter);
									if (listCheckSub != null && listCheckSub.getLstObject().size() > 0) {
									} else {
										StaffSaleCat staffSaleCat = new StaffSaleCat();
										staffSaleCat.setStaff(staff);
										if (lstSubId != null) {
											staffSaleCat.setCat(lstSubId);
										}
										staffSaleCat = staffMgr.createStaffSaleCat(staffSaleCat, null);
									}
								}
							}
							staff = staffMgr.saveOrUpdateStaff(staff, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, getLogInfoVO());
							if (staff != null) {
								result.put("shopId", staff.getShop().getId());
							}
							error = false;
							result.put("staff", staff);
							result.put("staffId", staff.getId());
							result.put("status", staff.getStatus().getValue());
						} else {
							errMsg = R.getResource("common.catalog.code.not.exist", R.getResource("catalog.focus.program.shop.code"));
						}
					}
				} else {
					/** them moi nhan vien */
					if (staff != null) {
						errMsg = R.getResource("common.code.exist", R.getResource("catalog.staff.code"), staffCode);
					} else {
						Staff newStaff = new Staff();
						/* Shop sTmp = shopMgr.getShopByCode(shopCode); */
						Shop sTmp = shopMgr.getShopById(shopId);
						if (sTmp != null) {
							if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) && getMapShopChild().get(sTmp.getId()) == null) {
								result.put(ERROR, true);
								result.put("errMsg", R.getResource("common.permission.change"));
								return JSON;
							}
							if (ActiveType.RUNNING.equals(sTmp.getStatus())) {
								newStaff.setShop(sTmp);
								newStaff.setStaffCode(staffCode);
								Staff staffTmp = staffMgr.getStaffByCode(newStaff.getStaffCode());
								if (staffTmp != null) {
									if (staffMgr.checkIfRecordExist(staffTmp.getId(), null, null)) {
										result.put(ERROR, true);
										errMsg = R.getResource("common.exist", R.getResource("catalog.staff.code"));
										result.put("errMsg", errMsg);
										return JSON;
									}
								}
								newStaff.setStaffName(staffName);
								newStaff.setNameText(Unicode2English.codau2khongdau(staffName));
								if (status != null && status != ConstantManager.NOT_STATUS) {
									newStaff.setStatus(ActiveType.parseValue(status));
								}
								Area area = areaMgr.getAreaById(areaId);
								if (area != null) {
									if (!area.getType().equals(AreaType.WARD)) {
										result.put(ERROR, true);
										errMsg = R.getResource("catalog.area.tree.not.ward", R.getResource("catalog.area.tree"));
										result.put("errMsg", errMsg);
										return JSON;
									}
									newStaff.setArea(area);
									newStaff.setAddress(this.valueAreaAddress(houseNumber, area));//longnh15 change
								}
								newStaff.setHousenumber(houseNumber);
								if (!StringUtil.isNullOrEmpty(staffPhone) && staffMgr.checkIfStaffExists(null, staffPhone, null, null)) {
									result.put(ERROR, true);
									errMsg = R.getResource("common.exist", R.getResource("catalog.mobilephone"));
									result.put("errMsg", errMsg);
									return JSON;
								}
								newStaff.setMobilephone(staffPhone);
								newStaff.setPhone(staffTelephone);
								if (!StringUtil.isNullOrEmpty(email) && staffMgr.checkIfStaffExists(email, null, null, null)) {
									result.put(ERROR, true);
									errMsg = R.getResource("common.exist", R.getResource("catalog.email.code"));
									result.put("errMsg", errMsg);
									return JSON;
								}
								newStaff.setEmail(email);
								if (gender != null && gender != -1) {
									newStaff.setGender(GenderType.parseValue(gender));
								} else {
									newStaff.setGender(null);
								}
								if (!StringUtil.isNullOrEmpty(workStartDate)) {
									newStaff.setStartWorkingDay(ths.dms.web.utils.DateUtil.parse(workStartDate, ConstantManager.FULL_DATE_FORMAT));
								}
								if (!StringUtil.isNullOrEmpty(orderProduct)) {
									newStaff.setSaleTypeCode(orderProduct);
								}
								if (OrganizationNodeType.STAFF.getValue().equals(nodeType)) {
									StaffType staffTypeTmp = organizationMgr.getStaffTypeById(nodeTypeId);
									newStaff.setStaffType(staffTypeTmp);
									Organization org = organizationMgr.getOrganizationById(orgId);
									newStaff.setOrganization(org);
									if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) && !this.checkParentOrganization(staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId()), newStaff)) {
										result.put(ERROR, true);
										result.put("errMsg", R.getResource("catalog.unit.tree.create.staff.belong.org"));
										return JSON;
									}
								}
								newStaff.setCreateUser(currentUser.getUserName());
								newStaff = staffMgr.saveOrUpdateStaff(newStaff, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, getLogInfoVO());
								if (newStaff != null) {
									result.put("shopId", newStaff.getShop().getId());
									setUserId2Session(newStaff.getId());
									// Xu ly them nganh hang cn 
									List<Long> lstCreate = new ArrayList<Long>();
									if (subStaff != null && subStaff.length() > 0) {
										String[] arrCreateStr = subStaff.split(",");
										for (String value : arrCreateStr) {
											if (StringUtil.isNumberInt(value)) {
												lstCreate.add(Long.valueOf(value));
											}
										}
										if (lstCreate != null && lstCreate.size() > 0) {
											for (int i = 0, sz = lstCreate.size(); i < sz; i++) {
												ProductInfo lstSubId = productInfoMgr.getProductInfoById(lstCreate.get(i));
												StaffSaleCat staffSaleCat = new StaffSaleCat();
												staffSaleCat.setStaff(newStaff);
												if (lstSubId != null) {
													staffSaleCat.setCat(lstSubId);
												}
												staffSaleCat = staffMgr.createStaffSaleCat(staffSaleCat, null);
											}
										}
									}
									result.put("staff", newStaff);
									result.put("staffId", newStaff.getId());
									result.put("status", newStaff.getStatus().getValue());
									error = false;
								}
							} else {
								errMsg = R.getResource("common.catalog.status.in.active", R.getResource("catalog.focus.program.shop.code"));
							}
						} else {
							errMsg = R.getResource("common.catalog.code.not.exist", R.getResource("catalog.focus.program.shop.code"));
						}
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, "UnitTreeCatalogaction.saveOrUpdateStaff()" + e.getMessage());
				result.put(ERROR, true);
				result.put("errMsg", R.getResource("system.error"));
				return JSON;
			}
		}
		result.put(ERROR, error);
		result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * @author vuongmq
	 * @date 04/03/2015
	 * @param houseNumber
	 * @param area
	 * @return Lay luu dia chi Address; noi chuoi tu: so nha, phuong, huyen,
	 *         tinh;
	 */
	private String valueAreaAddress(String houseNumber, Area area) {
		StringBuilder addressStr = new StringBuilder();
		if (houseNumber != null && houseNumber.length() > 0) {
			addressStr.append(houseNumber);
		}
		if (area.getPrecinctName() != null && area.getPrecinctName().length() > 0) {
			if (addressStr.length() > 0) {
				String endAdd = addressStr.substring(addressStr.length() - 1);
				if (",".equals(endAdd)) {
					addressStr.append(" ").append(area.getPrecinctName());
				} else {
					addressStr.append(", ").append(area.getPrecinctName());
				}
			} else {
				addressStr.append(area.getPrecinctName());
			}
		}
		if (area.getDistrictName() != null && area.getDistrictName().length() > 0) {
			if (addressStr.length() > 0) {
				String endAdd = addressStr.substring(addressStr.length() - 1);
				if (",".equals(endAdd)) {
					addressStr.append(" ").append(area.getDistrictName());
				} else {
					addressStr.append(", ").append(area.getDistrictName());
				}
			} else {
				addressStr.append(area.getDistrictName());
			}
		}
		if (area.getProvinceName() != null && area.getProvinceName().length() > 0) {
			if (addressStr.length() > 0) {
				String endAdd = addressStr.substring(addressStr.length() - 1);
				if (",".equals(endAdd)) {
					addressStr.append(" ").append(area.getProvinceName());
				} else {
					addressStr.append(", ").append(area.getProvinceName());
				}
			} else {
				addressStr.append(area.getProvinceName());
			}
		}
		return addressStr.toString();
	}

	public Boolean checkParentStaffMap(Long staffId, List<Long> parentStaffMapId) {
		try {
			Staff staff = staffMgr.getStaffById(staffId);
			if (staff != null) {
				StaffFilter filter = new StaffFilter();
				filter.setIsGetAllChild(true);
				filter.setStaffId(staff.getId());
				ObjectVO<StaffVO> vo = staffMgr.getListStaffVO(filter);
				if (vo != null && vo.getLstObject() != null) {
					for (StaffVO sv : vo.getLstObject()) {
						for (Long id : parentStaffMapId) {
							if (id.equals(sv.getStaffId())) {
								result.put("errMsg", R.getResource("catalog.unit.tree.parent.belong.staff", sv.getStaffCode()));
								result.put(ERROR, true);
								return false;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return true;
	}

	/**
	 * Import Excel.
	 * 
	 * @return the string
	 * @author nhanlt
	 * @throws Exception
	 * @since 16/05/2013
	 */
	public String importExcelFile() throws Exception {
		/* return importExcelCatalogStaff(); */
		resetToken(result);
		isError = true;
		totalItem = 0;
		if (excelType == 1) {//nhan vien
			return importExcelCatalogStaff();
		} else if (excelType == 2) {//sub cat product
			return importExcelCatalogSubOfStaff();
		}
		return SUCCESS;
	}

	private static Integer PRODUCT_TOTAL_COLS = 2;

	/**
	 * Import excel sub of staff catalog.
	 * 
	 * @return the string
	 * @author hoanv25
	 * @throws Exception
	 * @since Auggust 17, 2015
	 */
	public String importExcelCatalogSubOfStaff() throws Exception {
		resetToken(result);
		isError = true;
		totalItem = 0;
		String message = "";
		listCell = new ArrayList<CellBean>();
		List<CellBean> lstFails = new ArrayList<CellBean>();
		int maxColumnSize = 9;
		List<List<String>> lstData = getExcelDataEx(excelFile, excelFileContentType, errMsg, maxColumnSize);
		List<Long> lstShopChild = getListShopChildId();

		ShopToken shToken = currentUser.getShopRoot();
		Shop shop = null;
		if (shToken != null) {
			shop = shopMgr.getShopById(shToken.getShopId());
		}

		if (shop == null) {
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
		} else if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
			try {
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					if (lstData.get(i) != null && lstData.get(i).size() > 0) {
						List<String> row = lstData.get(i);
						boolean flag1 = false; 
						//Flag1: Co Hieu kiem tra bo qua cac dong de trong tat ca cac gia tri Import 
						for (int c1 = 0; c1 < maxColumnSize; c1++) {
							if (!StringUtil.isNullOrEmpty(row.get(c1))) {
								flag1 = true;
							}
						}
						if (!flag1) {
							continue;
						}
						message = "";
						StaffSaleCat staffsale = new StaffSaleCat();
						
						Boolean isCreate = true;
						Shop sTmp = shop;
						StaffFilter filter = new StaffFilter();
						if (i == lstData.size() - 1) {
							String name = row.get(2);
							String name1 = row.get(4);
							if (name.equals("X") && name1.equals(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "Bắt buộc"))) {
								break;
							} else {
								totalItem++;
							}
						} else {
							totalItem++;
						}

						if (row.size() > 1) {//Ma NPP
							String value = row.get(0);
							String msg = ValidateUtil.validateField(value, "catalog.customer.import.shopcode", 40, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
							if (StringUtil.isNullOrEmpty(msg)) {
								sTmp = shopMgr.getShopByCode(value);
								if (sTmp == null) {
									sTmp = shop;//
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.focus.program.shop.code");
								} else {
									if (lstShopChild != null && !lstShopChild.contains(sTmp.getId())) {
										message += ValidateUtil.getErrorMsg(ConstantManager.ERR_SHOP_NOT_BELONG_AREA);
									} else if (!ShopSpecificType.NPP.equals(sTmp.getType().getSpecificType())) {
										message += R.getResource("common.cms.shop.islevel5.undefined");
									} else if (!ActiveType.RUNNING.equals(sTmp.getStatus())) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true, "catalog.focus.program.shop.code");
									}
								}
							} else {
								message += msg;
							}
						}

						//Ma nvbh
						if (row.size() > 3) {
							String value = row.get(2);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "catalog.customer.import.salestaffid", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (StringUtil.isNullOrEmpty(msg)) {
									Staff staff = staffMgr.getStaffByCode(value);
									if (staff == null) {
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.import.salestaffid");
									} else {
										filter.setStaffId(staff.getId());
										staffsale.setStaff(staff);
									}
								} else {
									message += msg;
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.customer.import.salestaffid");
							}
						}

						//Ma nganh hang
						if (row.size() > 6) {
							String value = row.get(5);
							if (!StringUtil.isNullOrEmpty(value)) {
								String msg = ValidateUtil.validateField(value, "catalog.quota.group.sub.cat.code", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
								if (StringUtil.isNullOrEmpty(msg)) {
									ProductInfo productCat = productInfoMgr.getProductInfoByCodeCat(value, ProductType.SUB_CAT, ActiveType.isValidValue(PRODUCT_TOTAL_COLS));
									if (productCat != null) {
										ProductInfo product = productInfoMgr.getProductInfoByCode(value, ProductType.SUB_CAT, productCat.getProductInfoCode(), ActiveType.isValidValue(PRODUCT_TOTAL_COLS));
										if (product == null) {
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.quota.group.sub.cat.code");
										} else {
											filter.setCatId(product.getId());
											staffsale.setCat(product);
										}
									}
								} else {
									message += msg;
								}
							} else {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true, "catalog.quota.group.sub.cat.code");
							}
						}
						//Kiem tra hanh dong
						if (row.size() > 8) {
							String value = row.get(7);
							if (value.equals("0")) {
								isCreate = false;
							} else {
								isCreate = true;
							}
						}
						if (StringUtil.isNullOrEmpty(message)) {
							if (isCreate) {
								ObjectVO<StaffVO> listEquipCategoryCode = staffMgr.listEquipCategoryCode(filter);
								if (listEquipCategoryCode != null && listEquipCategoryCode.getLstObject() != null && !listEquipCategoryCode.getLstObject().isEmpty()) {

								} else {
									staffMgr.createStaffSaleCat(staffsale, null);
								}
							} else {
								//xoa dong
								ObjectVO<StaffVO> listEquipCategoryCode = staffMgr.listEquipCategoryCode(filter);
								if (listEquipCategoryCode != null && listEquipCategoryCode.getLstObject() != null && !listEquipCategoryCode.getLstObject().isEmpty()) {
									staffMgr.deleteStaffSaleCat(staffsale, null);
								}
							}
						} else {
							if (message.substring(message.length() - 1).equals("\n")) {
								message = message.substring(0, message.length() - 1);
							}
							lstFails.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_SUB_UNIT_FAIL);
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}
	
	/**
	 * Validate duplicate data.
	 * 
	 * @return the string
	 * @author nhanlt6
	 * @since June 06, 2013
	 */
	public String checkDuplicateStaff(List<List<String>> lstData, int k) {//check trung khoa
		String mes = "";
		if (lstData.get(k) == null || lstData.get(k).size() < 3) {
			mes += "1";//R.getResource( "import.excel.invalid.data");//Du lieu khong hop le
		} else {
			String shopCode = lstData.get(k).get(1).toLowerCase().trim();
			String staffCode = lstData.get(k).get(3).toLowerCase().trim();
			String staffName = lstData.get(k).get(4).toLowerCase().trim();
			String type = lstData.get(k).get(5).toLowerCase().trim();
			if (StringUtil.isNullOrEmpty(staffName) || StringUtil.isNullOrEmpty(staffCode) || StringUtil.isNullOrEmpty(shopCode) || StringUtil.isNullOrEmpty(type)) {
			} else {
				for (int i = 0; i < lstData.size(); i++) {
					if (i != k && lstData.get(i) != null && lstData.get(i).size() >= 3) {
						List<String> row = lstData.get(i);
						if (shopCode.equals(row.get(1).toLowerCase().trim()) && staffCode.equals(row.get(3).toLowerCase().trim()) && staffName.equals(row.get(4).toLowerCase().trim()) 
								&& type.equals(row.get(5).toLowerCase().trim())) {
							mes += (i + 2) + " , ";
						}
					}
				}
				if (mes.length() > 0)
					mes = R.getResource("customerdebit.batch.import.duplicate") + mes.substring(0, mes.length() - 3);
			}
		}
		return mes;
	}

	/**
	 * Import excel staff catalog.
	 * 
	 * @return the string
	 * @author nhanlt6 
	 * @since May 14, 2013 
	 */
	public String importExcelCatalogStaff() {
		isError = true; 
		totalItem = 0; 
		String message = ""; 
		String checkMessage = ""; 
		lstView = new ArrayList<CellBean>(); 
		typeView =true; 
		List<CellBean> lstFails = new ArrayList<CellBean>();
		
		try {
			List<ApParam> availableStaffSaleForm = getStaffSaleForm();
			List<Object> dynamicAttributeVOs = getStaffDynamicAttributeVO();
			final int TOTAL_DYNAMIC_ATTRIBUTE = dynamicAttributeVOs != null ? dynamicAttributeVOs.size() : 0;
			final int START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX = 15;
			
			int totalStaticCol = 15;
			final int startDataRowIndex = 1;
			int totalImportColumn = totalStaticCol + TOTAL_DYNAMIC_ATTRIBUTE;
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, totalImportColumn, startDataRowIndex); 
			if ( StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) { 
				List<String> row = lstData.get(lstData.size() - 1);
				String value = null; 
				int totalColumnInRow = row.size();
				if (totalColumnInRow >= 2) { 
					int sz1 = totalColumnInRow; int n = 0; 
					for (int i = 2; i < sz1;i++) { 
						if (StringUtil.isNullOrEmpty(row.get(i))) { 
							n++; 
							} 
						} 
						value = R.getResource("ss.require.label"); 
						if (n == sz1 - 2 && value.equalsIgnoreCase(row.get(1).trim()) && "x".equalsIgnoreCase(row.get(0).trim())) {
							lstData.remove(lstData.size() - 1); 
						} 
				} 
				Staff staff = null;
				Staff temp = null; 
				Shop sTmp = null; 
				Date date = null; 
				Area area = null; 
				value = null; 
				row = null; 
				Date now = DateUtil.now(); 
				String shopRow1 = null; // ma don vi
				Boolean flagShopType = false;
				Boolean isUpdateAction = false;
				Boolean isTypeEqual = false;
				Boolean isParentShop = false; // ma don vi co hop le
				for (int i = 0, sz = lstData.size(); i < sz; i++) { 
					flagShopType = false;
					isUpdateAction = false;
					isTypeEqual = false;
					isParentShop = false;
					row = lstData.get(i);
					totalColumnInRow = row.size();
					totalItem++; 
					message = "";
					checkMessage = ""; // 
					String mesTemp = checkDuplicateStaff(lstData,i); //
					if (!StringUtil.isNullOrEmpty(mesTemp) && !mesTemp.equals("1")) { //
						message += mesTemp; // 
					} 
					if (StringUtil.isNullOrEmpty(message) && row != null && totalColumnInRow == totalImportColumn) {
						temp = new Staff(); // Mã DV
						temp.setCreateUser(currentUser.getUserName()); 
						if (totalColumnInRow > 0) {
							value = row.get(1); //x 
							shopRow1 = row.get(1);
							if (!StringUtil.isNullOrEmpty(value)) {
								message += ValidateUtil.validateField(value, "unit.tree.shop.code",40, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_CORE, ConstantManager.ERR_MAX_LENGTH);
								sTmp = shopMgr.getShopByCode(value); 
								if (sTmp == null) {
									message += R.getResource("catalog.unit.tree.not.exist"); 
								} else if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType())
										&& getMapShopChild().get(sTmp.getId()) == null) {
									message += R.getResource("common.shop.not.belong.area");
								} else if (!ActiveType.RUNNING.equals(sTmp.getStatus())) {
									message +=  R.getResource("catalog.unit.tree.shop.stopped.create.staff") ;
								} else { 
									temp.setShop(sTmp);
									isParentShop = true; // ma don vi  cap nhat hop le	
								} 
							} else { 
								message += R.getResource("common.required",R.getResource("unit.tree.shop.code")); 
							} 
						} 
						//Ma NV
						if ( totalColumnInRow > 3 ) { 
							value = row.get(3); 
							if ( !StringUtil.isNullOrEmpty(value) ) { 
								message += ValidateUtil.validateField(value, "catalog.staff.code", STAFF_CODE_MAX_LENGTH, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_CORE, ConstantManager.ERR_MAX_LENGTH);
								if ( StringUtil.isNullOrEmpty(message) ) { 
									staff = staffMgr.getStaffByCode(value); 
									if ( staff != null ) {
										if(!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) 
												&& getMapUserId().get(staff.getId()) == null) {
											message += R.getResource("catalog.staff.code.permission");
										}
										isUpdateAction = true;
										//temp = staffMgr.getStaffByCode(value); 
										temp =staff;
										if ( isParentShop && sTmp != null) {
											if (!temp.getShop().getId().equals(sTmp.getId())) {
												message += "Mã đơn vị của "+ temp.getStaffCode() + " - " + temp.getStaffName() + " đang là "+ temp.getShop().getShopCode() +". Vui lòng kiểm tra lại dữ liệu . ";
											}
										}
									} else { 
										temp.setStaffCode(value); 
									} 
								} 
							} else { 
								message += R.getResource("common.required", R.getResource("catalog.staff.code"));
							}
						} 
						// Tên NV 
						if ( totalColumnInRow > 4 ) { 
							value = row.get(4); 
							if ( !StringUtil.isNullOrEmpty(value) ) { 
								message += ValidateUtil.validateField(value, "catalog.staff.name", 200, null, null, ConstantManager.ERR_MAX_LENGTH);
								temp.setStaffName(value); 
							} else { 
								message += R.getResource("common.required", R.getResource("catalog.staff.name")); 
							} 
						}
						// Mã Loại NV 
						if ( totalColumnInRow > 5 ) { 
							value = row.get(5);
							if ( !StringUtil.isNullOrEmpty(value) ) {
								if ( isUpdateAction ) { // cap nhat
									StaffType type1 = new StaffType();
									OrganizationFilter filter1 = new OrganizationFilter();
									filter1.setTypeName(value);
									type1 = organizationMgr.getStaffTypeByCondition(filter1);
									if ( type1 == null ) { 
										message += R.getResource("common.catalog.code.not.exist",R.getResource("catalog.staff.type.code")); 
									} else {
										if (ActiveType.RUNNING.equals( type1.getStatus()) ) {
											if ( staff != null ) {
												//Nếu staff khong co StaffType thi cap nhat StaffType la type1
												if(staff.getStaffType()==null){
													temp.setStaffType(type1);
												}
												else if ( type1.getId().equals(staff.getStaffType().getId()) ) {
													isTypeEqual = true;
												} else {
													message += "Loại nhân viên hiện tại đang là "+ type1.getName() +" . Vui lòng kiểm tra lại dữ liệu "; 
													message += "\n";
												}
											} else {
												message += " Loại nhân viên đang thuộc đơn vị không có trong hệ thống "; 
												message += "\n";
											}
										} else {
											message += " Loại nhân viên đang tạm ngưng " ;
											message += "\n";
										}
									}
								} else { // them moi
									StaffType type = new StaffType();
									OrganizationFilter filter = new OrganizationFilter();
									filter.setTypeName(value);
									type = organizationMgr.getStaffTypeByCondition(filter);
									if ( type == null ) { 
										message += R.getResource("common.catalog.code.not.exist",R.getResource("catalog.staff.type.code")); 
									} else { 
										// check loai nhan vien co thuoc don vi
										if ( ActiveType.RUNNING.equals(type.getStatus()) ) {
											sTmp = shopMgr.getShopByCode(row.get(1)); // lay don vi roi lay org_id cua Loai don vi
											if ( !StringUtil.isNullOrEmpty(row.get(1)) && sTmp != null ) { // -1
												if ( sTmp.getOrganization() != null ) { 
													if (ActiveType.RUNNING.equals(sTmp.getStatus())) {
														List<OrganizationVO> lstMenuContext = organizationMgr.getListOrganizationById4ContextMenu(sTmp.getOrganization().getId());// va org = null
														for (int j = 0, size = lstMenuContext.size(); j < size; j++) {
															if ( type.getId().equals(lstMenuContext.get(j).getTypeId()) && lstMenuContext.get(j).getNodeType() == OrganizationNodeType.STAFF.getValue() ){ // loai nhan vien fix sau
																flagShopType = true;
																temp.setOrganization(organizationMgr.getOrganizationById(lstMenuContext.get(j).getId()));
															}
														}
														if ( !flagShopType ){
															message += "Không tạo được "+ type.getName() +" bên dưới đơn vị "+ sTmp.getShopCode() +" - " +sTmp.getShopName() + " . ";
															message += "\n";
														} else {
															temp.setStaffType(type);
														}
													} else {
														message += " Loại nhân viên thuộc đơn vị đang ở trạng thái tạm ngưng .";
													}
												} else {
													message +=  "Lỗi dữ liệu , đơn vị có org_id = null ";
												}
												
											}else {
												message += " Loại nhân viên thuộc đơn vị không tồn tại ";
												message += "\n";
											}
										} else {
											message += "Loại nhân viên đang tạm ngưng .";
											message += "\n";
										}
									}
								}
							} else { 
								message += R.getResource("common.required",R.getResource("catalog.staff.type.code"));
								message += "\n"; 
							}
						}
						//gioi tinh 
						if (totalColumnInRow > 6) { 
							value = row.get(6); 
							if ( !StringUtil.isNullOrEmpty(value) ) { 
								try { 
									if (Integer.valueOf(value) == 0 || Integer.valueOf(value) == 1) {
										temp.setGender(GenderType.parseValue(Integer.valueOf(value))); 
									} else { 
									message += R.getResource("common.not.date",R.getResource("catalog.gender.value")); 
									message += "\n"; } 
								} catch (Exception e) { 
									message += R.getResource("common.not.date",R.getResource("catalog.gender.value")); 
									message += "\n"; 
								}
							} 
						} //ngay bat dau lam viec 
						if (totalColumnInRow > 7) { 
							value = row.get(7); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								try { 
									if (DateUtil.checkInvalidFormatDate(value)) { 
										message += R.getResource("common.not.date",R.getResource("catalog.workStartDate.value")); 
										message += "\n"; 
									} else { 
										date = DateUtil.parse(value,ConstantManager.FULL_DATE_FORMAT); 
										if (date != null) { 
											if (DateUtil.compareDateWithoutTime(date, now) > 0) { 
												message += R.getResource("common.date.greater.currentdate",R.getResource("catalog.workStartDate.value")); 
												message += "\n"; 
											}
											temp.setStartWorkingDay(date); 
										} else { 
											message += R.getResource("common.not.date", R.getResource("catalog.workStartDate.value")); 
											message += "\n"; 
										} 
									} 
								} catch (Exception e) { 
									message += R.getResource("common.not.date", R.getResource("catalog.workStartDate.value")); 
									message += "\n"; 
								} 
							}
						}
						//so co dinh 
						if (totalColumnInRow > 8) { 
							value = row.get(8); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								message += ValidateUtil.validateField(value, "catalog.telephone", 200,null,ConstantManager.ERR_MAX_LENGTH, null);
								temp.setPhone(value); 
							} 
						}
						 //MobilePhone 
						if (	totalColumnInRow > 9	) { 
							value = row.get(9); 
							message += ValidateUtil.validateField(value, "catalog.mobilephone",200,null,ConstantManager.ERR_MAX_LENGTH, null); 
							temp.setMobilephone(value); 
						}
						// Email 
						if (totalColumnInRow > 10) { 
							value = row.get(10); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								if (!StringUtil.validateEmail(value)) {
									message += ValidateUtil.validateField(value, "catalog.email.code", 200,null,ConstantManager.ERR_MAX_LENGTH, null); 
									message += R.getResource("common.not.date",R.getResource("catalog.email.code")); 
									message += "\n"; 
									
								} else {
									temp.setEmail(value); 
								}	
							}
						}
						//Mã Phường/Xã 
						if(totalColumnInRow > 11) {
							value = row.get(11);
							if ( !StringUtil.isNullOrEmpty(value) ) {
								message += ValidateUtil.validateField(value, "catalog.ward.code",40,null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_CORE, ConstantManager.ERR_MAX_LENGTH); 
								area = areaMgr.getAreaByCode(value); 
								if (area != null && area.getType() != null) { 
									if ( area.getType().getValue().equals(AreaType.WARD.getValue())) {
										temp.setArea(area); 
									} else { 
										message += R.getResource("common.catalog.code.not.exist",R.getResource("catalog.ward.code")); message += "\n"; 
									}				
								} else {
									message += R.getResource("common.catalog.code.not.exist.or.pending",R.getResource("catalog.ward.code")); 
									message += "\n"; 
								}
							} 
						} 
						// Địa chỉ 
						if (totalColumnInRow > 13) { 
							value = row.get(13); 
							message += ValidateUtil.validateField(value, "catalog.address", 200,null,ConstantManager.ERR_MAX_LENGTH, null); 
							message += "\n";
							message += ValidateUtil.getErrorMsgOfSpecialCharInAddress(value,"catalog.address"); //check special char 
							temp.setAddress(value); 
						}
						
						final int staffSaleFormColumnIndex = 14;
						if (totalColumnInRow > staffSaleFormColumnIndex) {
							String cellValue = row.get(staffSaleFormColumnIndex);
							StaffType importingStaffType = temp.getStaffType();
							boolean isCellEmpty = StringUtil.isNullOrEmpty(cellValue);
							if (!isCellEmpty && importingStaffType != null && StaffSpecificType.STAFF == importingStaffType.getSpecificType()) {
								String[] codeName = cellValue.split("-");
								String importSaleFormCode = codeName.length > 0 ? codeName[0] : null;
								importSaleFormCode = importSaleFormCode != null ? importSaleFormCode.trim().toUpperCase() : importSaleFormCode;
								boolean isValidImportSaleFormCode = false;
								for (ApParam staffSaleForm: availableStaffSaleForm) {
									if (!StringUtil.isNullOrEmpty(staffSaleForm.getApParamCode()) && staffSaleForm.getApParamCode().equals(importSaleFormCode)) {
										isValidImportSaleFormCode = true;
										break;
									}
								}
								if (isValidImportSaleFormCode) {
									temp.setSaleTypeCode(importSaleFormCode);
								} else {
									message += R.getResource("catalog.import.staff.staff.sale.form.invalid") + ConstantManager.NEW_LINE_CHARACTER;
								}
							} else if (!isCellEmpty) {
								message += R.getResource("catalog.import.staff.staff.sale.form.only.avaible.for.saleman") + ConstantManager.NEW_LINE_CHARACTER; 
							}
						}
						
						lstAttributeId = new ArrayList<Long>();
						lstAttributeValue = new ArrayList<String>();
						lstAttributeColumnValueType = new ArrayList<Integer>();
						List<String> dynamicAttributeColumnDataInImportFile = new ArrayList<String>();
						for (int j = START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX, lastDataColumnIndex = START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX + TOTAL_DYNAMIC_ATTRIBUTE
								; j <= lastDataColumnIndex; j++) {
							int dynamicAttributeSequence = j - START_DYNAMIC_ATTRIBUTE_COLUMN_INDEX;
							if (totalColumnInRow > j) {
								String cellData = row.get(j);
								dynamicAttributeColumnDataInImportFile.add(cellData);
								AttributeDynamicVO dynamicAttributeVO = TOTAL_DYNAMIC_ATTRIBUTE > dynamicAttributeSequence ? (AttributeDynamicVO) dynamicAttributeVOs.get(dynamicAttributeSequence) : null;
								String errMsg = DynamicAttributeValidator.validateDynamicAttributeColumnData(row, j, dynamicAttributeVO);
								if (!StringUtil.isNullOrEmpty(errMsg)) {
									message += errMsg + ConstantManager.NEW_LINE_CHARACTER;
								}
								if (dynamicAttributeVO != null) {
									lstAttributeId.add(dynamicAttributeVO.getAttributeId());
									lstAttributeValue.add(DynamicAttributeValidator.getInputDynamicAttributeValue(row, j, dynamicAttributeVO));
									lstAttributeColumnValueType.add(dynamicAttributeVO.getType());
								}
							}
						}
						
						/*if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType()) 
								&& !this.checkParentOrganization(staffMgr.getStaffById(currentUser.getStaffRoot().getStaffId()), staff)) {
							message += R.getResource("catalog.unit.tree.create.staff.belong.org");
						}*/
						if (isView != null && isView == 0) { 
							if (StringUtil.isNullOrEmpty(message)) { 
								try {
									if (!isUpdateAction) {
										//staffMgr.createStaff(temp, getLogInfoVO());
										staffMgr.saveOrUpdateStaff(temp, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, getLogInfoVO());
									} else {
										//staffMgr.updateStaff(temp, getLogInfoVO());
										staffMgr.saveOrUpdateStaff(temp, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, getLogInfoVO());
									}
								} catch (Exception e) {
									LogUtility.logError(e, e.getMessage()); 
								}
							} else {
								CellBean failBean = StringUtil.addFailBean(row, message);
								failBean.setLstDynamic(dynamicAttributeColumnDataInImportFile);
								lstFails.add(failBean);
							} 
							typeView = false; 
					    } else { 
					    	if (lstView.size() < 100) { 
					    		if (StringUtil.isNullOrEmpty(message)) { 
					    			message = "OK"; 
					    		} else {
					    			message = StringUtil.convertHTMLBreakLine(message); 
					    		}
					    		lstView.add(StringUtil.addFailBean(row, message)); 
					    	} 
					    	typeView = true;
					    } 
					} else { 
						lstFails.add(StringUtil.addFailBean(row, message)); 
					} 
				}
				
				if (!lstFails.isEmpty()) {
					Map<String, Object> beans = new HashMap<String, Object>();
					beans.put("report", lstFails);
					beans.put("dynamicAttribute", dynamicAttributeVOs);
					fileNameFail = exportExcelJxlsNotAuthentication(beans, Configuration.getFailDataPath(), 
											dynamicAttributeVOs.isEmpty() ? ConstantManager.TEMPLATE_CATALOG_STAFF_FAIL_WITHOUT_DYNAMIC_ATTRIBUTE : ConstantManager.TEMPLATE_CATALOG_STAFF_FAIL_NEW);
				}
				
				numFail = lstFails.size();
			} else {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import.no.data");
			}
		} catch (Exception e) { 
			LogUtility.logError(e, e.getMessage()); 
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM); 
		}
		
		
		if (StringUtil.isNullOrEmpty(errMsg)) { 
			isError = false; 
		}
		return SUCCESS;
	}

	/**
	 * kiem tra cac dong shop trung trong excel
	 * 
	 * @return the string
	 * @author phuocdh2
	 * @since Mar 29, 2015
	 */
	public String checkDuplicateShop(List<List<String>> lstData, int k) {//check trung khoa
		String mes = "";
		if (lstData.get(k) == null || lstData.get(k).size() < 3) {
			mes += "1";//R.getResource( "import.excel.invalid.data");//Du lieu khong hop le
		} else {
			String shopCode = lstData.get(k).get(1).toLowerCase().trim();
			String abba = lstData.get(k).get(3).toLowerCase().trim();
			String type = lstData.get(k).get(4).toLowerCase().trim();
			String parentShopCode = lstData.get(k).get(5).toLowerCase().trim();
			if (StringUtil.isNullOrEmpty(shopCode) || StringUtil.isNullOrEmpty(abba) || StringUtil.isNullOrEmpty(parentShopCode) || StringUtil.isNullOrEmpty(type)) {
			} else {
				for (int i = 0; i < lstData.size(); i++) {
					if (i != k && lstData.get(i) != null && lstData.get(i).size() >= 3) {
						List<String> row = lstData.get(i);
						if (shopCode.equals(row.get(1).toLowerCase().trim()) && abba.equals(row.get(3).toLowerCase().trim()) 
								&& type.equals(row.get(4).toLowerCase().trim()) 
								&& parentShopCode.equals(row.get(5).toLowerCase().trim())) {
							mes += (i + 2) + " , ";
						}
					}
				}
				if (mes.length() > 0)
					mes = R.getResource("customerdebit.batch.import.duplicate") + mes.substring(0, mes.length() - 3);
			}
		}
		return mes;
	}
	/**
	 * Import danh sach don vi theo mau vao cay don vi
	 * @return the string
	 * @author phuocdh2
	 * @since 29 Mar, 2015
	 */
	public String importShopExcel() {
		resetToken(result);
		isError = true; 
		totalItem = 0; 
		String message = ""; 
		String checkMessage = ""; 
		lstView = new ArrayList<CellBean>(); 
		typeView =true; 
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile,excelFileContentType, errMsg, 19); 
		if ( StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) { 
			try { 
				List<String> row = lstData.get(lstData.size() - 1); 
				String value = null; 
				if (row.size()>= 2) { 
					int sz1 = row.size(); int n = 0; 
					for (int i = 2; i < sz1;i++) { 
						if (StringUtil.isNullOrEmpty(row.get(i))) { 
							n++; 
							} 
						} 
						value = R.getResource("ss.require.label"); 
						if (n == sz1 - 2 && value.equalsIgnoreCase(row.get(1).trim()) && "x".equalsIgnoreCase(row.get(0).trim())) {
							lstData.remove(lstData.size() - 1); 
						} 
				} 
				Shop temp = null; 
				Shop sTmp = null; 
				Area area = null; 
				value = null; 
				row = null; 
				String shopRow1 = null; // ma don vi
				Boolean flagShopType = false;
				//Boolean isUpdateAction = false;
				Boolean isTypeEqual = false;
				Boolean isParentShop = false;
 				for (int i = 0, sz = lstData.size(); i < sz; i++) { 
					Boolean isUpdateAction = false;
					row = lstData.get(i); 
					totalItem++; 
					message = "";
					checkMessage = ""; // 
					String mesTemp = checkDuplicateShop(lstData,i); 
					if (!StringUtil.isNullOrEmpty(mesTemp) && !mesTemp.equals("1")){ //
						message += mesTemp; // 
					} 
					if (StringUtil.isNullOrEmpty(message) && row != null && row.size() == 19) { 
						temp = new Shop(); // Mã Don vi cha
						temp.setCreateUser(currentUser.getUserName()); 
						if (row.size() > 5) {
							value = row.get(5); //x 
							shopRow1 = row.get(5);
							if (!StringUtil.isNullOrEmpty(value)) {
								message += ValidateUtil.validateField(value, "catalog.unit.tree.parent.code", 40, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_CORE, ConstantManager.ERR_MAX_LENGTH);
								sTmp = shopMgr.getShopByCode(value); 
								if (sTmp == null) {
									message += "Mã đơn vị cha không có trong hệ thống ."; 
								} else { 
									if (!StaffSpecificType.VIETTEL_ADMIN.getValue().equals(currentUser.getStaffRoot().getObjectType())
											&& getMapShopChild().get(sTmp.getId()) == null) {
										message += R.getResource("common.shop.not.belong.area");
									} else if (!ActiveType.RUNNING.equals(sTmp.getStatus())){ // f shop cha het hoat dong
										message += " Đơn vị cha đang tạm ngưng, không thể tạo mới đơn vị ."; 
									} else { 
										temp.setParentShop(sTmp);
										isParentShop = true; 
									} 
								} 
							} else { 
								message += R.getResource("common.required",R.getResource("catalog.unit.tree.parent.code")); 
								message += "\n"; 
							} 
						}
						//Ma Don vi
						if (row.size() > 1) { 
							value = row.get(1); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								message += ValidateUtil.validateField(value, "catalog.unit.tree.code", 40,null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_CORE, ConstantManager.ERR_MAX_LENGTH);
								shop = shopMgr.getShopByCode(value); 
								if (shop != null) {
									isUpdateAction = true;
									temp = shopMgr.getShopByCode(value); 
									if ( isParentShop && sTmp != null && !temp.getParentShop().getId().equals(sTmp.getId()) ) {
										message += "Mã đơn vị cha của "+ temp.getShopCode() + " - " + temp.getShopName() + " đang là "+ temp.getParentShop().getShopCode() +". Vui lòng kiểm tra lại dữ liệu";
									}
								} else { 
									temp.setShopCode(value); 
								} 
				 
							}  else {
								message += R.getResource("common.required", R.getResource("catalog.unit.tree.code")); 
								
							}
						} 
						// Tên Don vi
						if ( row.size() > 2) { 
							value = row.get(2); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								message +=  ValidateUtil.validateField(value, "catalog.unit.tree.name", 200,null,null, ConstantManager.ERR_MAX_LENGTH);
								temp.setShopName(value); 
							} else { 
								message += R.getResource("common.required", R.getResource("catalog.unit.tree.name")); 
							} 
						}
						// ten viet tat
						if ( row.size() > 3) { 
							value = row.get(3); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								message += ValidateUtil.validateField(value, "catalog.product.ten.viet.tat", 200,null,null,ConstantManager.ERR_MAX_LENGTH);
								temp.setAbbreviation(value); 
							}
						}
						// Mã Loại Don vi 
						if (row.size() > 4) { 
							value = row.get(4);
							if ( !StringUtil.isNullOrEmpty(value) ) {
								message +=  ValidateUtil.validateField(value, "unit_tree.search_unit.shop_type", 200, null,null, ConstantManager.ERR_MAX_LENGTH);
								if ( isUpdateAction ) { // cap nhat
									ShopType type1 = new ShopType();
									OrganizationFilter filter1 = new OrganizationFilter();
									filter1.setTypeName(value);
									type1 = organizationMgr.getShopTypeByCondition(filter1);
									if ( type1 == null ) { 
										message += R.getResource("common.catalog.code.not.exist",R.getResource("unit_tree.search_unit.shop_type")); 
									} else {
										if (ActiveType.RUNNING.equals( type1.getStatus()) ) {
											if (shop != null) {
												if ( type1.getId().equals(shop.getType().getId()) ) {
													isTypeEqual = true;
												} else {
													message += "Loại đơn vị hiện tại của "+ shop.getShopCode() + " - " + shop.getShopName() +" đang là "+ shop.getType().getName()+". Vui lòng kiểm tra lại dữ liệu "; 
													message += "\n";
												}
											} else {
												message += "Loại đơn vị thuộc đơn vị cha không có trong hệ thống"; 
												message += "\n";
											}
										} else {
											message += " Loại đơn vị đang tạm ngưng " ;
											message += "\n";
										}
									}
								} else { // them moi
									ShopType type = new ShopType();
									OrganizationFilter filter = new OrganizationFilter();
									filter.setTypeName(value);
									type = organizationMgr.getShopTypeByCondition(filter);
									if ( type == null ) { 
										message += R.getResource("common.catalog.code.not.exist",R.getResource("unit_tree.search_unit.shop_type")); 
									} else { // check loai don vi con co thuoc don vi
										if (ActiveType.RUNNING.equals( type.getStatus()) ) {
											if ( !StringUtil.isNullOrEmpty(row.get(5)) && shopMgr.getShopByCode(row.get(5)) != null ) {
												sTmp = shopMgr.getShopByCode(row.get(5));
												if (ActiveType.RUNNING.equals(sTmp.getStatus())) {
													List<OrganizationVO> lstMenuContext = organizationMgr.getListOrganizationById4ContextMenu(sTmp.getOrganization().getId());// va org = null
													for ( int j = 0, size = lstMenuContext.size(); j < size; j++ ) {
														if ( type.getId().equals(lstMenuContext.get(j).getTypeId()) && lstMenuContext.get(j).getNodeType() == OrganizationNodeType.SHOP.getValue() ){ // loai don vi 
															flagShopType = true;
															temp.setOrganization(organizationMgr.getOrganizationById(lstMenuContext.get(j).getId()));
														}
													}
													if ( !flagShopType ) {
														message += " Không tạo được "+ type.getName() +" bên dưới đơn vị "+ sTmp.getShopCode() +" - " +sTmp.getShopName();
														message += "\n";
													} else {
														temp.setType(type);
													}
												} else {
													message += "Loại đơn vị thuộc đơn vị cha đang tạm ngưng " ;
												}
											} else {
												message += " Loại đơn vị thuộc đơn vị cha không tồn tại trong hệ thống ";
												message += "\n";
											}
										} else {
											message += " Loại đơn vị đang tạm ngưng " ;
											message += "\n";
										}
									}
								}
							} else { 
								message += R.getResource("common.required",R.getResource("unit_tree.search_unit.shop_type"));
								message += "\n"; 
							}
						}
						//so co dinh 
						if ( row.size() > 7 ) { 
							value = row.get(7); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								message += ValidateUtil.validateField(value, "catalog.telephone", 200,null,null, ConstantManager.ERR_MAX_LENGTH); 
								temp.setPhone(value); 
							} 
						}
						 //MobilePhone 
						if ( row.size() > 8	) { 
							value = row.get(8); 
							message += ValidateUtil.validateField(value, "catalog.mobilephone", 200,null,null, ConstantManager.ERR_MAX_LENGTH); 
							temp.setMobiphone(value); 
						} 
						 // fax 
						if ( row.size() > 9	) { 
							value = row.get(9); 
							message += ValidateUtil.validateField(value, "catalog.unit.tree.fax.number", 200,null,null, ConstantManager.ERR_MAX_LENGTH); 
							if (!StringUtil.isNullOrEmpty(value)) { 
							} 
							temp.setFax(value); 
						} 
						// Email 
						if ( row.size() > 10 ) { 
							value = row.get(10); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								if (!StringUtil.validateEmail(value)) {
									message += R.getResource("common.not.date",R.getResource("catalog.email.code")); 
									message += "\n"; 
								} 
								else {
									//message += ValidateUtil.validateField(value, "catalog.email.code", 200, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME,ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INVALID_FORMAT_CHARACTER); 
									//longnh15: Da check ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME ben tren, neu check nua loi @
									message += ValidateUtil.validateField(value, "catalog.email.code", 200,ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INVALID_FORMAT_CHARACTER);
									temp.setEmail(value); 
								}	
							}
						}
						//ma so thue 
						if (row.size() > 11) { 
							value = row.get(11); 
							if (!StringUtil.isNullOrEmpty(value)) {
								final int fieldMaxLength = 20;
								message += ValidateUtil.validateField(value, "catalog.tax.number.code", fieldMaxLength, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH, ConstantManager.ERR_INTEGER);
								temp.setTaxNum(value); 
							} 
						}
						//nguoi lien he 
						if (row.size() > 12) { 
							value = row.get(12); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								message += ValidateUtil.validateField(value, "catalog.unit.tree.contact.name", 200,null,null, ConstantManager.ERR_MAX_LENGTH); 
								temp.setContactName(value); 
							} 
						}
						//dia chi hoa don
						if (row.size() > 13) { 
							value = row.get(13); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								message += ValidateUtil.validateField(value, "catalog.unit.tree.bill.to", 200,null,null, ConstantManager.ERR_MAX_LENGTH); 
								temp.setBillTo(value); 
							} 
						}
						//dia chi giao hang 
						if (row.size() > 14) { 
							value = row.get(14); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								message += ValidateUtil.validateField(value, "catalog.unit.tree.ship.to", 200,null,null, ConstantManager.ERR_MAX_LENGTH); 
								temp.setShipTo(value); 
							} 
						}
						//Mã Phường/Xã 
						if ( row.size() > 15 ) {
							value = row.get(15);
							if ( !StringUtil.isNullOrEmpty(value) ) {
								message +=  ValidateUtil.validateField(value, "catalog.ward.code", 40, null,ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_CORE, ConstantManager.ERR_MAX_LENGTH);
								area = areaMgr.getAreaByCode(value); 
								if (area != null && area.getType() != null) { 
									if ( area.getType().getValue().equals(AreaType.WARD.getValue())) {
										temp.setArea(area); 
									} else { 
										message += R.getResource("common.catalog.code.not.exist",R.getResource("catalog.ward.code")); 
										message += "\n"; 
									}				
								} else {
									message += R.getResource("common.catalog.code.not.exist",R.getResource("catalog.ward.code")); 
									message += "\n"; 
								}
							}
						} 
						// so nha, duong 
						if ( row.size() > 17 ) { 
							value = row.get(17); 
							message += ValidateUtil.validateField(value, "catalog.address", 200,null,null, ConstantManager.ERR_MAX_LENGTH); 
							temp.setAddress(value); 
						} 
						// vi tri lat,lag 
						if (row.size() > 18) { 
							value = row.get(18); 
							if (!StringUtil.isNullOrEmpty(value)) { 
								String[] str2 = value.split(",");
								if ( str2.length == 2 ) {
									try {
										Float lat = Float.parseFloat(str2[0]);
										Float lng = Float.parseFloat(str2[1]);
										if (lat > -90 && lat < 90) {
											temp.setLat(lat);
										} else {
											message += "vị trí lat phải nằm trong giá trị -90 tới 90";
										}
										if (lat > -180 && lat <180) {
											temp.setLng(lng);
										} else {
											message += "vị trí lat phải nằm trong giá trị -180 tới 180";
										}
										
									} catch (Exception e) {
										message += "vị trí lat , lng không đúng";
									}
								} else {
									message += "vị trí lat,lng định dạng x,y ";
								}
							}
						} 
						if (isView == 0) { 
							if ( StringUtil.isNullOrEmpty(message) ) { 
								if ( !isUpdateAction ) {
									try {
										Shop sh = shopMgr.createShop(temp, getLogInfoVO()); 
										fromMonth = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_DDMMYYYY);
										Integer workingDateType = exceptionDayMgr.createWorkDateCopyShopParent(sh, WorkingDateType.CHI_DINH_DON_VI, fromMonth, null, null, null, getLogInfoVO());
										if (workingDateType != null && workingDateType == WorkingDateType.DE_QUY.getValue()) {
											/** +Nếu đang sử dụng lịch theo lịch cha: */
											Date fromMonthDate = DateUtil.parse(fromMonth, DateUtil.DATE_FORMAT_DDMMYYYY);
											exceptionDayMgr.callProceduceWorkingDate(sh.getId(), fromMonthDate, null, WorkingDateProcedureType.THIET_LAP_RIENG);
										}
									} catch (Exception e) {
											LogUtility.logError(e, e.getMessage()); 
									} 
								} else {
									try {
										shopMgr.updateShop(temp, getLogInfoVO()); 
									} catch (Exception e) {
											LogUtility.logError(e, e.getMessage()); 
									} 
								}
							} else {
										lstFails.add(StringUtil.addFailBean(row, message)); 
							} 
							typeView = false; 
					    } else { 
					    	if (lstView.size() < 100) { 
					    		if (StringUtil.isNullOrEmpty(message)) { 
					    			message = "OK"; 
					    		} else {
					    			message = StringUtil.convertHTMLBreakLine(message); 
					    		}
					    		lstView.add(StringUtil.addFailBean(row, message)); 
					    	} 
					    	typeView = true;
					    } 
					}
					else if (!StringUtil.isNullOrEmpty(message))
					{
						//longnh15 bao loi duplicate
						lstFails.add(StringUtil.addFailBean(row, message));
					}
				}
				getOutputFailExcelFile(lstFails,ConstantManager.TEMPLATE_CATALOG_UNIT_INFO_FAIL_NEW);
			} catch (Exception e) { 
				LogUtility.logError(e, e.getMessage()); 
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM); 
			} 
		} else {
			//errMsg = "Không có dữ liệu import.";
			errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "unit_tree.import.no.data");
			

		}
		if (StringUtil.isNullOrEmpty(errMsg)) { 
			isError = false; 
		}
		return SUCCESS;
	}
	/**
	 * 
	 * Ham export danh sach don vi theo ket qua tim kiem
	 * @author phuocdh2
	 * @return the string
	 * @since 24-Mar-2015
	 */
	public String exportShopExcel() {
		result.put("page", page);
		result.put("rows", rows);
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			
			KPaging<ShopVO> kPaging = new KPaging<ShopVO>();
			kPaging.setPageSize(1000000);
			kPaging.setPage(0);
			setPriviledgeInfo(unitFilter);
			ObjectVO<ShopVO> vo = shopMgr.findShopVOExport(kPaging, unitFilter);
			List<ShopVO> lst = vo.getLstObject();
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			}
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
			StringBuilder sb = new StringBuilder(folder).append(ConstantManager.TEMPLATE_UNITS_EXPORT_FILE_NAME).append(FileExtension.XLS.getValue());
			String templateFileName = sb.toString();
			templateFileName = templateFileName.replace('/', File.separatorChar);
			sb = new StringBuilder(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"template_units_export_file_name")).append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append(FileExtension.XLS.getValue());
			String outputName = sb.toString();
			sb = null;
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			Map<String, Object> beans = new HashMap<String, Object>();
			beans.put("lstStaff", lst);
			//phuocdh2 modified
			beans.put("STT", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"baocao.dtbh.dt3.sheet1.Header.stt"));
			beans.put("maDonVi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock_manage_ma_don_vi"));
			beans.put("tenDonVi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock_manage_ten_don_vi"));
			beans.put("tenVietTat", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.product.ten.viet.tat"));
			beans.put("loaiDonVi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"organization_shop_Type"));
			beans.put("maDonViCha", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.unit.tree.parent.code"));
			beans.put("tenDonViCha", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_ten_don_vi_cha"));
			beans.put("phone", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_phone"));
			beans.put("mobile", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_mobile"));
			beans.put("fax", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_fax"));
			beans.put("email", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_email"));
			beans.put("maSoThue", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_ma_so_thue"));
			beans.put("nguoiLienHe", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_nguoi_lien_he"));
			beans.put("diaChiHoaDon", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_dia_chi_hoa_don"));
			beans.put("diaChiGiaoHang", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_dia_chi_giao_hang"));
			beans.put("email", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_email"));
			beans.put("maPhuongXa", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_ma_phuong_xa"));
			beans.put("diaBan", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_dia_ban"));
			beans.put("soNhaDuong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_so_nha_duong"));
			beans.put("viTriBanDo", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_vi_tri_ban_do"));
			//beans.put("tenSheet",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_ten_Sheet"));
			String tenSheet=Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_units_ten_Sheet");
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			resultWorkbook.setSheetName(0, tenSheet);
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * Export excel staff info.
	 * 
	 * @return the string
	 * @author nhanlt
	 * @since Oct 27, 2012
	 */
	public String exportExcel() throws Exception {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			KPaging<StaffExportVO> paging = new KPaging<StaffExportVO>();
			paging.setPageSize(10000);
			paging.setPage(0);
			StaffFilter filter = new StaffFilter();
			filter.setExPaging(paging);
			/*
			 * if (shopId != null && shopId > 0) { filter.setShopId(shopId); }
			 * else { filter.setShopId(currentUser.getShopRoot().getShopId()); }
			 */
			if (shId != null && shId > 0) {
				filter.setShopId(shId);
			} else {
				filter.setShopId(currentUser.getShopRoot().getShopId());
			}
			if (status != null && status >= 0) {
				filter.setStatus(ActiveType.parseValue(status));
			}
			if (staffTypeName != null && !"-1".equals(staffTypeName)) {
				filter.setStaffTypeName(staffTypeName);
			}
			filter.setStaffCode(staffCode);
			filter.setStaffName(staffName);
			setPriviledgeInfo(filter);
			List<StaffExportVO> lst = staffMgr.getListStaffWithDynamicAttribute(filter);
			
			//Gender multi language
			String gt_nam = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_gt_nam");
			String gt_nu = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_gt_nu");
			
			//Status multi language
			String status_active = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.status.active");
			String status_stopped = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.status.stopped");
			
			if (lst == null || lst.size() == 0) {
				result.put(ERROR, false);
				result.put("hasData", false);
				return JSON;
			} 
			else {
				//Gender and Status multi language
				for (StaffExportVO staff : lst) {
					if (staff.getGender()!= null) {
						staff.setGenderStr(gt_nu);
						if (staff.getGender() == 1) {
							staff.setGenderStr(gt_nam);							
						}
					} 
					staff.setStatusStr(status_stopped);
					if (staff.getStatus() != null && staff.getStatus() == 1) {
						staff.setStatusStr(status_active);
					}
				}
			}
			String folder = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog();
			StringBuilder sb = new StringBuilder(folder).append(ConstantManager.TEMPLATE_STAFF_EXPORT_FILE_NAME).append(FileExtension.XLS.getValue());
			String templateFileName = sb.toString();
			templateFileName = templateFileName.replace('/', File.separatorChar);

			sb = new StringBuilder(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"template_staff_export_file_name")).append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE)).append(FileExtension.XLS.getValue());
			String outputName = sb.toString();
			sb = null;
			String exportFileName = Configuration.getStoreRealPath() + outputName;

			Map<String, Object> beans = new HashMap<String, Object>();

			beans.put("lstStaff", lst);
			beans.put("dynamicAttribute", getStaffDynamicAttributeVO());
			//phuocdh2 modified
			beans.put("STT", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"baocao.dtbh.dt3.sheet1.Header.stt"));
			beans.put("maDonVi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock_manage_ma_don_vi"));
			beans.put("tenDonVi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock_manage_ten_don_vi"));
			beans.put("maNhanVien", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"unit_tree.search_staff.staff_code"));
			beans.put("tenNhanVien", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"unit_tree.search_staff.staff_name"));
			beans.put("tenDonVi", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock_manage_ten_don_vi"));
			beans.put("loaiNhanVien", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_loai_nhan_vien"));
			beans.put("gioiTinh", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_gioi_tinh"));
			beans.put("ngayBatDauLam", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_ngay_bat_dau_lam"));
			beans.put("phone", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_phone"));
			beans.put("mobile", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_email"));
			beans.put("email", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_mobile"));
			beans.put("maPhuongXa", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_ma_phuong_xa"));
			beans.put("diaBan", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_dia_ban"));
			beans.put("soNhaDuong", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_so_nha_duong"));
			beans.put("trangThai", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_trang_thai"));
			beans.put("staffSaleForm", Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_tree_staff_sale_form"));
			//beans.put("tenSheet",Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog_unit_ten_Sheet"));
			String tenSheet = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"template_staff_ten_Sheet");
			inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			resultWorkbook.setSheetName(0, tenSheet);
			os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			result.put(ERROR, false);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception ex) {
			LogUtility.logError(ex, "UnitTreeCatalog.exportExcel - " + ex.getMessage());
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			IOUtils.closeQuietly(inputStream);
			IOUtils.closeQuietly(os);
		}
		return JSON;
	}

	/**
	 * Export Excel to file.
	 * 
	 * @author nhanlt
	 * @since 16/05/2013
	 */
	public void exportExcelStaffData(List<CellBean> lstData, String tempFileName) {
		try {
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathCatalog() + tempFileName;
			templateFileName = templateFileName.replace('/', File.separatorChar);

			String outputName = DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_EXCEL_FILE) + "_" + tempFileName;
			String exportFileName = Configuration.getStoreRealPath() + outputName;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report", lstData);
			InputStream inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
			XLSTransformer transformer = new XLSTransformer();
			org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, params);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFileName));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(LIST, outputPath);
		} catch (ParsePropertyException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (InvalidFormatException e) {
			LogUtility.logError(e, e.getMessage());
		} catch (IOException e) {
			LogUtility.logError(e, e.getMessage());
		}
	}

	public Integer getStaffGroupSearch() {
		return staffGroupSearch;
	}

	public void setStaffGroupSearch(Integer staffGroupSearch) {
		this.staffGroupSearch = staffGroupSearch;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Integer getStatusCheckbox() {
		return statusCheckbox;
	}

	public void setStatusCheckbox(Integer statusCheckbox) {
		this.statusCheckbox = statusCheckbox;
	}

	public String getSaleTypeCode() {
		return saleTypeCode;
	}

	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(String workStartDate) {
		this.workStartDate = workStartDate;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getStaffTelephone() {
		return staffTelephone;
	}

	public void setStaffTelephone(String staffTelephone) {
		this.staffTelephone = staffTelephone;
	}

	public Long getStaffType() {
		return staffType;
	}

	public void setStaffType(Long staffType) {
		this.staffType = staffType;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Integer getStaffCreateType() {
		return staffCreateType;
	}

	public void setStaffCreateType(Integer staffCreateType) {
		this.staffCreateType = staffCreateType;
	}

	public String getShopCodeHidden() {
		return shopCodeHidden;
	}

	public void setShopCodeHidden(String shopCodeHidden) {
		this.shopCodeHidden = shopCodeHidden;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getStaffGroupId() {
		return staffGroupId;
	}

	public void setStaffGroupId(Long staffGroupId) {
		this.staffGroupId = staffGroupId;
	}

	public Integer getStaffObjectType() {
		return staffObjectType;
	}

	public void setStaffObjectType(Integer staffObjectType) {
		this.staffObjectType = staffObjectType;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getSaleGroup() {
		return saleGroup;
	}

	public void setSaleGroup(String saleGroup) {
		this.saleGroup = saleGroup;
	}

	public Staff getStaffSignIn() {
		return staffSignIn;
	}

	public void setStaffSignIn(Staff staffSignIn) {
		this.staffSignIn = staffSignIn;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getIdDate() {
		return idDate;
	}

	public void setIdDate(String idDate) {
		this.idDate = idDate;
	}

	public String getIdPlace() {
		return idPlace;
	}

	public void setIdPlace(String idPlace) {
		this.idPlace = idPlace;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStaffPhone() {
		return staffPhone;
	}

	public void setStaffPhone(String staffPhone) {
		this.staffPhone = staffPhone;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Boolean getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(Boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	public String getParentShopCode() {
		return parentShopCode;
	}

	public void setParentShopCode(String parentShopCode) {
		this.parentShopCode = parentShopCode;
	}

	public Shop getParentShop() {
		return parentShop;
	}

	public void setParentShop(Shop parentShop) {
		this.parentShop = parentShop;
	}

	public Long getParentShopId() {
		return parentShopId;
	}

	public void setParentShopId(Long parentShopId) {
		this.parentShopId = parentShopId;
	}

	public Boolean getAllowSelectParentShop() {
		return allowSelectParentShop;
	}

	public void setAllowSelectParentShop(Boolean allowSelectParentShop) {
		this.allowSelectParentShop = allowSelectParentShop;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getAddressBillTo() {
		return addressBillTo;
	}

	public void setAddressBillTo(String addressBillTo) {
		this.addressBillTo = addressBillTo;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getAddressShipTo() {
		return addressShipTo;
	}

	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public List<CellBean> getListBeans() {
		return listBeans;
	}

	public void setListBeans(List<CellBean> listBeans) {
		this.listBeans = listBeans;
	}

	public List<ShopTreeVO> getLstNVGSShop() {
		return lstNVGSShop;
	}

	public void setLstNVGSShop(List<ShopTreeVO> lstNVGSShop) {
		this.lstNVGSShop = lstNVGSShop;
	}

	public List<String> getLstStaffAddId() {
		return lstStaffAddId;
	}

	public void setLstStaffAddId(List<String> lstStaffAddId) {
		this.lstStaffAddId = lstStaffAddId;
	}

	public List<ApParam> getLstsaleGroup() {
		return lstsaleGroup;
	}

	public void setLstsaleGroup(List<ApParam> lstsaleGroup) {
		this.lstsaleGroup = lstsaleGroup;
	}

	public List<String> getLstStaffDeleteId() {
		return lstStaffDeleteId;
	}

	public void setLstStaffDeleteId(List<String> lstStaffDeleteId) {
		this.lstStaffDeleteId = lstStaffDeleteId;
	}

	public List<ApParam> getLstSaleType() {
		return lstSaleType;
	}

	public void setLstSaleType(List<ApParam> lstSaleType) {
		this.lstSaleType = lstSaleType;
	}

	public List<ChannelType> getLstStaffType() {
		return lstStaffType;
	}

	public void setLstStaffType(List<ChannelType> lstStaffType) {
		this.lstStaffType = lstStaffType;
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public List<JETreeNode> getLstUnitTree() {
		return lstUnitTree;
	}

	public void setLstUnitTree(List<JETreeNode> lstUnitTree) {
		this.lstUnitTree = lstUnitTree;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdSearch() {
		return idSearch;
	}

	public void setIdSearch(Long idSearch) {
		this.idSearch = idSearch;
	}

	public String getParentStaffId() {
		return parentStaffId;
	}

	public void setParentStaffId(String parentStaffId) {
		this.parentStaffId = parentStaffId;
	}

	public List<Long> getLstParentStaffId() {
		return lstParentStaffId;
	}

	public void setLstParentStaffId(List<Long> lstParentStaffId) {
		this.lstParentStaffId = lstParentStaffId;
	}

	public List<Long> getLstStaffShopAddId() {
		return lstStaffShopAddId;
	}

	public void setLstStaffShopAddId(List<Long> lstStaffShopAddId) {
		this.lstStaffShopAddId = lstStaffShopAddId;
	}

	public Long getShId() {
		return shId;
	}

	public void setShId(Long shId) {
		this.shId = shId;
	}

	public Long getShopType() {
		return shopType;
	}

	public void setShopType(Long shopType) {
		this.shopType = shopType;
	}

	public OrganizationMgr getOrganizationMgr() {
		return organizationMgr;
	}

	public void setOrganizationMgr(OrganizationMgr organizationMgr) {
		this.organizationMgr = organizationMgr;
	}

	public ShopType getShopTypeEle() {
		return shopTypeEle;
	}

	public void setShopTypeEle(ShopType shopTypeEle) {
		this.shopTypeEle = shopTypeEle;
	}

	public Boolean getIsCheckExistRowsInShop() {
		return isCheckExistRowsInShop;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public void setIsCheckExistRowsInShop(Boolean isCheckExistRowsInShop) {
		this.isCheckExistRowsInShop = isCheckExistRowsInShop;
	}

	public ApParamMgr getApParamMgr() {
		return apParamMgr;
	}

	public void setApParamMgr(ApParamMgr apParamMgr) {
		this.apParamMgr = apParamMgr;
	}

	public List<ProductAttributeVO> getLstProductAttributes() {
		return lstProductAttributes;
	}

	public void setLstProductAttributes(List<ProductAttributeVO> lstProductAttributes) {
		this.lstProductAttributes = lstProductAttributes;
	}

	public List<StaffType> getLstStaffTypeView() {
		return lstStaffTypeView;
	}

	public void setLstStaffTypeView(List<StaffType> lstStaffTypeView) {
		this.lstStaffTypeView = lstStaffTypeView;
	}

	public List<StaffAttributeVO> getLstStaffAttributes() {
		return lstStaffAttributes;
	}

	public void setLstStaffAttributes(List<StaffAttributeVO> lstStaffAttributes) {
		this.lstStaffAttributes = lstStaffAttributes;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public UnitFilter getUnitFilter() {
		return unitFilter;
	}

	public void setUnitFilter(UnitFilter unitFilter) {
		this.unitFilter = unitFilter;
	}

	public List<OrganizationUnitTypeVO> getLstOrgUnitType() {
		return lstOrgUnitType;
	}

	public void setLstOrgUnitType(List<OrganizationUnitTypeVO> lstOrgUnitType) {
		this.lstOrgUnitType = lstOrgUnitType;
	}

	/***************************** GETTER-SETTER ****************************************/
	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Long getNodeTypeId() {
		return nodeTypeId;
	}

	public void setNodeTypeId(Long nodeTypeId) {
		this.nodeTypeId = nodeTypeId;
	}

	public Integer getNodeType() {
		return nodeType;
	}

	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}

	public List<TreeNode> getSearchUnitTree() {
		return searchUnitTree;
	}

	public void setSearchUnitTree(List<TreeNode> searchUnitTree) {
		this.searchUnitTree = searchUnitTree;
	}

	public StaffTreeVO getStaffTreeVO() {
		return staffTreeVO;
	}

	public void setStaffTreeVO(StaffTreeVO staffTreeVO) {
		this.staffTreeVO = staffTreeVO;
	}

	public List<OrganizationUnitTypeVO> getLstOrgStaffType() {
		return lstOrgStaffType;
	}

	public void setLstOrgStaffType(List<OrganizationUnitTypeVO> lstOrgStaffType) {
		this.lstOrgStaffType = lstOrgStaffType;
	}

	public ProductAttributeMgr getProductAttributeMgr() {
		return productAttributeMgr;
	}

	public void setProductAttributeMgr(ProductAttributeMgr productAttributeMgr) {
		this.productAttributeMgr = productAttributeMgr;
	}

	public String getStaffTypeName() {
		return staffTypeName;
	}

	public void setStaffTypeName(String staffTypeName) {
		this.staffTypeName = staffTypeName;
	}

	public List<ApParam> getLstProductType() {
		return lstProductType;
	}

	public void setLstProductType(List<ApParam> lstProductType) {
		this.lstProductType = lstProductType;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public Integer getExcelType() {
		return excelType;
	}

	public void setExcelType(Integer excelType) {
		this.excelType = excelType;
	}

	public List<CellBean> getListCell() {
		return listCell;
	}

	public void setListCell(List<CellBean> listCell) {
		this.listCell = listCell;
	}

	public String getOrderProduct() {
		return orderProduct;
	}

	public void setOrderProduct(String orderProduct) {
		this.orderProduct = orderProduct;
	}

	public List<TreeNode> getSearchSubCatTree() {
		return searchSubCatTree;
	}

	public void setSearchSubCatTree(List<TreeNode> searchSubCatTree) {
		this.searchSubCatTree = searchSubCatTree;
	}

	public String getSubStaff() {
		return subStaff;
	}

	public void setSubStaff(String subStaff) {
		this.subStaff = subStaff;
	}

	public String getSubCatStr() {
		return subCatStr;
	}

	public void setSubCatStr(String subCatStr) {
		this.subCatStr = subCatStr;
	}

	public Long getStaffSpecificType() {
		return staffSpecificType;
	}

	public void setStaffSpecificType(Long staffSpecificType) {
		this.staffSpecificType = staffSpecificType;
	}	
}
package ths.dms.action.catalog;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.ApParamMgr;
import ths.dms.core.business.CarMgr;
import ths.dms.core.business.ChannelTypeMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class CarTypeCatalogAction extends AbstractAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	private Shop shop;
	private Long id;
	private String shopCode;
	private String number;
	private Float gross;
	private String type;
	private String category;
	private Long label;
	private String origin;
	private Integer status;
	private ChannelTypeMgr channelTypeMgr;
	private String carFileError;
	private File excelFile;
	private String excelFileContentType;
	private CarMgr carMgr;
	private ShopMgr shopMgr;
	private List<ChannelType> lstCarLabel;
	private ApParamMgr apParamMgr;
	private List<ApParam> lstCarType;
	private List<ApParam> lstCarCategory;
	private List<ApParam> lstCarOrigin;
	private Staff staff;
	private StaffMgr staffMgr;
	private Boolean isAll;

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Boolean getIsAll() {
		return isAll;
	}

	public void setIsAll(Boolean isAll) {
		this.isAll = isAll;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public List<ChannelType> getLstCarLabel() {
		return lstCarLabel;
	}

	public void setLstCarLabel(List<ChannelType> lstCarLabel) {
		this.lstCarLabel = lstCarLabel;
	}

	public List<ApParam> getLstCarType() {
		return lstCarType;
	}

	public void setLstCarType(List<ApParam> lstCarType) {
		this.lstCarType = lstCarType;
	}

	public List<ApParam> getLstCarCategory() {
		return lstCarCategory;
	}

	public void setLstCarCategory(List<ApParam> lstCarCategory) {
		this.lstCarCategory = lstCarCategory;
	}

	public List<ApParam> getLstCarOrigin() {
		return lstCarOrigin;
	}

	public void setLstCarOrigin(List<ApParam> lstCarOrigin) {
		this.lstCarOrigin = lstCarOrigin;
	}

	public String getCarFileError() {
		return carFileError;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public void setCarFileError(String carFileError) {
		this.carFileError = carFileError;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Float getGross() {
		return gross;
	}

	public void setGross(Float gross) {
		this.gross = gross;
	}

	public Long getLabel() {
		return label;
	}

	public void setLabel(Long label) {
		this.label = label;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    carMgr = (CarMgr)context.getBean("carMgr");
	    shopMgr = (ShopMgr)context.getBean("shopMgr");
	    apParamMgr = (ApParamMgr)context.getBean("apParamMgr");
	    channelTypeMgr = (ChannelTypeMgr)context.getBean("channelTypeMgr");
	    staffMgr = (StaffMgr)context.getBean("staffMgr");
	}

	/**
	 * @author vuongmq
	 * @since Oct 09,2014
	 * @description: load theo shop chon vai tro
	 */
	@Override
	public String execute() {
		resetToken(result);
	    if(currentUser != null){
			try {
				//staff = staffMgr.getStaffByCode(currentUser.getUserName());
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				lstCarType = apParamMgr.getListApParam(ApParamType.CARTYPE, ActiveType.RUNNING);
				lstCarCategory = apParamMgr.getListApParam(ApParamType.CARCAT, ActiveType.RUNNING);
				lstCarOrigin = apParamMgr.getListApParam(ApParamType.CARORG, ActiveType.RUNNING);

				ChannelTypeFilter filter = new ChannelTypeFilter();
				filter.setType(ChannelTypeType.CAR_LABEL);
				filter.setStatus(ActiveType.RUNNING);

				ObjectVO<ChannelType> tmp1 = channelTypeMgr.getListChannelType(filter,null);
				if(tmp1 != null){
					lstCarLabel = tmp1.getLstObject();
				}
				//shopCode = staff.getShop().getShopCode();
				shopCode = shop.getShopCode();
			} catch (BusinessException e) {
//				LogUtility.logError(e, "CarTypeCatalogAction.execute()" + e.getMessage());
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.execute()"), createLogErrorStandard(startLogDate));
			}
	    }

	    return SUCCESS;
	}

	/**
	 * Search.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012
	 */
	public String search(){

	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<Car> kPaging = new KPaging<Car>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
			Shop shop = null;
			if(currentUser != null && currentUser.getUserName() != null){
				/*staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
					id = shop.getId();
				}*/
				shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				if(shop == null) {
					return JSON;
				} else {
					id = shop.getId();
				}
			}

			if(!StringUtil.isNullOrEmpty(shopCode)){
				Shop sTmp = shopMgr.getShopByCode(shopCode);
				if(sTmp != null){
					//if(!checkAncestor(shopCode)){
					if(!checkShopPermission(shopCode)){
						return JSON;
					}
					id = sTmp.getId();
				}else{
					id = 0L;
				}
			} else{
				isAll = true;
			}
			if(label != null && label == -1){
				label = null;
			}
			ActiveType tmp = null;
	    	if(status != null && status != ConstantManager.NOT_STATUS){
	    		tmp = ActiveType.parseValue(status);
	    	}
	    	if(!StringUtil.isNullOrEmpty(type) && type.equals("-1")){
	    		type = null;
	    	}
	    	if(!StringUtil.isNullOrEmpty(category) && category.equals("-1")){
	    		category = null;
	    	}
	    	if(!StringUtil.isNullOrEmpty(origin) && origin.equals("-1")){
	    		origin = null;
	    	}
			ObjectVO<Car> lstCar = carMgr.getListCar(kPaging, id, type, number,label, gross,category,origin, tmp,isAll);
			if(lstCar!= null){
				result.put("total", lstCar.getkPaging().getTotalRows());
				result.put("rows", lstCar.getLstObject());
			}
	    }catch (BusinessException e) {
//	    	LogUtility.logError(e, "CarTypeCatalogAction.search()" +e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.search()"), createLogErrorStandard(startLogDate));
	    }
	    return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012
	 */
	public String saveOrUpdate(){

	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(number, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(category, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(origin, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
				Staff staffSign = getStaffByCurrentUser();
				if(staffSign==null){
					return JSON;
				}
				shopCode = staffSign.getShop().getShopCode();
			    Car car = carMgr.getCarById(id);
			    if(id!= null && id > 0){
					if(car!= null){
						if(!StringUtil.isNullOrEmpty(shopCode)){
							Shop shop =shopMgr.getShopByCode(shopCode);
							if(shop!= null){
								if(ActiveType.RUNNING.equals(shop.getStatus())){
									//if(checkAncestor(shopCode)){
									if(checkShopPermission(shopCode)){
										car.setShop(shop);
										if(!StringUtil.isNullOrEmpty(number)){
											if (car.getCarNumber().equals(number)) {
												if(!carMgr.checkIfRecordExists(number,id)){
													car.setCarNumber(number);
													if(!type.equals("-1")){
														car.setType(type.toUpperCase());
													}
													ChannelType bTmp = channelTypeMgr.getChannelTypeById(label);
													car.setLabel(bTmp);
													car.setGross(gross);
													if(!category.equals("-1")){
														car.setCategory(category.toUpperCase());
													}
													if(!origin.equals("-1")){
														car.setOrigin(origin);
													}
													car.setStatus(ActiveType.parseValue(status));
													carMgr.updateCar(car,getLogInfoVO());
													error = false;
												}else{
													errMsg= Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.carnumber.code.can.not.change");
												}
											} else {
												errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.carnumber.code");
											}
										}else{
											errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.carnumber.code");
										}
									} else{
										errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_SHOP,shopCode,currentUser.getUserName());
									}
								}else{
									errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, null, "catalog.focus.program.shop.code");
								}
							}else{
								errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, null, "catalog.focus.program.shop.code");
							}
						}else{
							errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.focus.program.shop.code");
						}
					}
			    } else {
					Car newCar = new Car();
					if(!StringUtil.isNullOrEmpty(shopCode)){
						Shop shop =shopMgr.getShopByCode(shopCode);
						if(shop!= null){
							if(ActiveType.RUNNING.equals(shop.getStatus())){
								//if(checkAncestor(shopCode)){
								if(checkShopPermission(shopCode)){
									newCar.setShop(shop);
									if(!StringUtil.isNullOrEmpty(number)){
										if(!carMgr.checkIfRecordExists(number, null)){
											newCar.setCarNumber(number);
											if(!type.equals("-1")){
												newCar.setType(type);
											}
											ChannelType bTmp = channelTypeMgr.getChannelTypeById(label);
											newCar.setLabel(bTmp);
											newCar.setGross(gross);
											if(!category.equals("-1")){
												newCar.setCategory(category);
											}
											if(!origin.equals("-1")){
												newCar.setOrigin(origin);
											}
											newCar.setStatus(ActiveType.parseValue(status));
											newCar = carMgr.createCar(newCar,getLogInfoVO());
											if(newCar!= null){
												error = false;
											}
										}else{
											errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, null, "catalog.carnumber.code");
										}
									}else{
										errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.carnumber.code");
									}
								} else{
									errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_SHOP,shopCode,currentUser.getUserName());
								}
							}else{
								errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, null, "catalog.focus.program.shop.code");
							}
						}else{
							errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, null, "catalog.focus.program.shop.code");
						}
					}
					else{
						errMsg= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, null, "catalog.focus.program.shop.code");
					}
				}
			}catch (BusinessException e) {
//			    LogUtility.logError(e, "CarTypeCatalogAction.saveOrUpdate()" +e.getMessage());
			 	Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.search()"), createLogErrorStandard(startLogDate));
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	 errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
	    return JSON;
	}

	/**
	 * Import excel file.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String importExcelFile() {
		isError = true;
		totalItem = 0;
		String message = "";
		lstView = new ArrayList<CellBean>();
	    typeView = true;
		List<CellBean> lstFails = new ArrayList<CellBean>();
		List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg,7);
		if (StringUtil.isNullOrEmpty(errMsg) && lstData!= null && lstData.size() > 0) {
			try {
				for(int i=0;i<lstData.size();i++){
					if(lstData.get(i)!= null && lstData.get(i).size() > 0){
						message = "";
						totalItem++;
						Car temp = new Car();
						List<String> row = lstData.get(i);

						//Mã NPP
						if (row.size() >0) {
							String value = row.get(0);
							try {
								if (!StringUtil.isNullOrEmpty(value)) {
									Shop shopTmp = shopMgr.getShopByCode(value);
									if(shopTmp != null){
										if(shopTmp.getType() != null && shopTmp.getType().getId() != ConstantManager.TYPE_SHOP){
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_SHOP_CODE, true,"catalog.focus.program.shop.code");
										}
										if(!ActiveType.RUNNING.equals(shopTmp.getStatus())){
											message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_STATUS_INACTIVE, true,"catalog.focus.program.shop.code");
										}
										//if(!checkAncestor(value)){
										if(!checkShopPermission(value)){
											message+= ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_SS_NOT_BELONG_SHOPUSER, true, "catalog.focus.program.shop.code");
										}
										temp.setShop(shopTmp);
									}else{
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST, true,"catalog.focus.program.shop.code");
									}
								}else{
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true,"catalog.focus.program.shop.code");
								}
							} catch (BusinessException e) {
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_SHOP_CODE, true,"catalog.focus.program.shop.code");
							}
						}
						//Loại xe
						if (row.size() >1) {
							String value = row.get(1);
							if(!StringUtil.isNullOrEmpty(value)){
								ApParam oTmp = apParamMgr.getApParamByCode(value, ApParamType.CARTYPE);
								temp.setType(value.toUpperCase());
								if(oTmp == null){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_EXIST_DB, true,"catalog.car.type.code");
								}
							}
						}

						//Số xe
						if (row.size() >2) {
							String value = row.get(2);
							if(!StringUtil.isNullOrEmpty(value)){
								if(!ValidateUtil.validateCarNumber(value)){
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.invalid.format.tf.car.number",value);
								}
								if(carMgr.checkIfRecordExists(value, null)){
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST, true,"catalog.carnumber.code");
								}else{
									temp.setCarNumber(value);
								}
							}else{
								message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true,"catalog.carnumber.code");
							}
						}
						//Hiệu xe
						if (row.size() >3) {
							String value = row.get(3);
							if(!StringUtil.isNullOrEmpty(value)){
								ChannelType bTmp  = channelTypeMgr.getChannelTypeByCode(value, ChannelTypeType.CAR_LABEL);
								if(bTmp != null){
									temp.setLabel(bTmp);
								}
							}
						}
						//Tải trọng
						if (row.size() >4) {
							String value = row.get(4);
							if(!StringUtil.isNullOrEmpty(value)){
								try {
									temp.setGross(Float.parseFloat(value));
									if(Integer.valueOf(value) < 0){
										message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_INT, true,"catalog.cargross.code");
									}
								} catch (NumberFormatException e) {
									message += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_INT, true,"catalog.cargross.code");
								}
							}
						}
						//Chủng loại
						if (row.size() >5) {
							String value = row.get(5);
							ApParam oTmp = null;
							try {
								if (!StringUtil.isNullOrEmpty(value)) {
									oTmp = apParamMgr.getApParamByCode(value, ApParamType.CARCAT);
								}
							} catch (BusinessException e) {
//								LogUtility.logError(e, e.getMessage());
								Date startLogDate = DateUtil.now();
								LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.importExcelFile()"), createLogErrorStandard(startLogDate));
							}
							if(oTmp != null){
								temp.setCategory(value.toUpperCase());
							}
						}
						//Nguồn gốc
						if (row.size() >6) {
							String value = row.get(6);
							ApParam oTmp = null;
							try {
								if (!StringUtil.isNullOrEmpty(value)) {
									oTmp = apParamMgr.getApParamByCode(value, ApParamType.CARORG);
								}
							} catch (BusinessException e) {
//								LogUtility.logError(e, e.getMessage());
								Date startLogDate = DateUtil.now();
								LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.importExcelFile()"), createLogErrorStandard(startLogDate));
							}
							if(oTmp != null){
								temp.setOrigin(value.toUpperCase());
							}
						}
						if(isView == 0){
							if (StringUtil.isNullOrEmpty(message)) {
								try{
									carMgr.createCar(temp,getLogInfoVO());
								}catch (BusinessException e) {
//									LogUtility.logError(be,be.getMessage());
									Date startLogDate = DateUtil.now();
									LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.importExcelFile()"), createLogErrorStandard(startLogDate));
								}
							} else {
								lstFails.add(StringUtil.addFailBean(row,message));
							}
							typeView = false;
						}else{
							typeView = true;
							if(lstView.size()<100){
								if(StringUtil.isNullOrEmpty(message)){
									message = "OK";
								}
								message = StringUtil.convertHTMLBreakLine(message);
								lstView.add(StringUtil.addFailBean(row,message));
							}
						}
					}
				}
				//Export error
				getOutputFailExcelFile(lstFails, ConstantManager.TEMPLATE_CATALOG_CAR_FAIL);
			} catch (BusinessException e) {
//				LogUtility.logError(e, "CarTypeCatalogAction.importExcelFile()" +e.getMessage());
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.importExcelFile()"), createLogErrorStandard(startLogDate));
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			}
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	/**
	 * Gets the list parent code.
	 *
	 * @return the list parent code
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String getListParentCode(){

		if(id != null){
			try {
				ChannelTypeFilter filter = new ChannelTypeFilter();
				filter.setType(ChannelTypeType.CAR);
				filter.setStatus(ActiveType.RUNNING);
				ObjectVO<ChannelType> tmp = channelTypeMgr.getListChannelType(filter,null);
				if(tmp != null){
					List<ChannelType> parentTmp = tmp.getLstObject();
					result.put("lstParentCode", parentTmp);
				}
			} catch (BusinessException e) {
//				LogUtility.logError(e, "CarTypeCatalogAction.getListParentCode()" +e.getMessage());
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.getListParentCode()"), createLogErrorStandard(startLogDate));
			}
		}
		return JSON;
	}

	/**
	 * Delete car type.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String deleteCarType(){

	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
			    Car car = carMgr.getCarById(id);
			    if(car!= null){
			    	if(carMgr.isUsingByOthers(id)){
			    		errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.is.used",
								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.car.type.code"));
			    	}else{
			    		carMgr.deleteCar(car,getLogInfoVO());
			    		error = false;
			    	}
			    }
			}catch (BusinessException e) {
				Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.deleteCarType()"), createLogErrorStandard(startLogDate));
//			    LogUtility.logError(e, "CarTypeCatalogAction.deleteCarType()" + e.getMessage());
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);
	    return JSON;
	}
	public String getListExcelData(){
	    try{
	    	Shop shop = null;
			if(currentUser != null && currentUser.getUserName() != null){
				staff = staffMgr.getStaffByCode(currentUser.getUserName());
				if(staff != null && staff.getShop() != null){
					shop = staff.getShop();
					id = shop.getId();
				}
			}
			if(shop == null) {
				return JSON;
			}
			if(!StringUtil.isNullOrEmpty(shopCode)){
				Shop sTmp = shopMgr.getShopByCode(shopCode);
				if(sTmp != null){
					//if(!checkAncestor(shopCode)){
					if(!checkShopPermission(shopCode)){
						return JSON;
					}
					id = sTmp.getId();
				}else{
					id = 0L;
				}
			} else{
				isAll = true;
			}
			if(label != null && label == -1){
				label = null;
			}
			ActiveType tmp = null;
	    	if(status != null && status != ConstantManager.NOT_STATUS){
	    		tmp = ActiveType.parseValue(status);
	    	}
	    	if(!StringUtil.isNullOrEmpty(type) && type.equals("-1")){
	    		type = null;
	    	}
	    	if(!StringUtil.isNullOrEmpty(category) && category.equals("-1")){
	    		category = null;
	    	}
	    	if(!StringUtil.isNullOrEmpty(origin) && origin.equals("-1")){
	    		origin = null;
	    	}
			ObjectVO<Car> lstCar = carMgr.getListCar(null, id, type, number,label, gross,category,origin, tmp,isAll);
			List<CellBean> lstFails = new ArrayList<CellBean>();
			if(lstCar != null && lstCar.getLstObject() != null){
				int size = lstCar.getLstObject().size();
				for(int i=0;i<size;i++){
					Car temp = lstCar.getLstObject().get(i);
					CellBean cellBean = new CellBean();
					if(temp.getShop() != null){
						cellBean.setContent1(temp.getShop().getShopCode());
					}
					cellBean.setContent2(temp.getType());
					cellBean.setContent3(temp.getCarNumber());
					if(temp.getLabel() != null){
						cellBean.setContent4(temp.getLabel().getChannelTypeCode());
					}
					if(temp.getGross() != null){
						cellBean.setContent5(String.valueOf(temp.getGross()));
					}
					cellBean.setContent6(temp.getOrigin());
					cellBean.setContent7(temp.getCategory());
					if(temp.getStatus() != null){
						if(ActiveType.RUNNING.equals(temp.getStatus())){
							cellBean.setContent8(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"action.status.name"));
						}else if(ActiveType.STOPPED.equals(temp.getStatus())){
							cellBean.setContent8(Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"pause.status.name"));
						}
					}
					lstFails.add(cellBean);
				}
			}
			exportExcelDataNotSession(lstFails,ConstantManager.TEMPLATE_CATALOG_CAR_EXPORT);
	    }catch (BusinessException e) {
	    	result.put(ERROR, true);
//	    	LogUtility.logError(e, "CarTypeCatalogAction.getListExcelData()" +e.getMessage());
	    	Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.web.action.general.CarTypeCatalogAction.getListExcelData()"), createLogErrorStandard(startLogDate));
	    }
	    return JSON;
	}
}

package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.business.AttributeMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.AttributeDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailFilter;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeFilter;
import ths.dms.core.entities.enumtype.AttributeVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;


public class AttributeCustomerManagerAction extends AbstractAction{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String attributeCode;
	private String attributeName;
	private Long attributeId;
	private Long id;
	private Long idDetail;
	private Integer valueType;
	private List<AttributeColumnType> lstValueType;
	private Integer status;
	private String note;
	private String attributeCodePop;
	private String attributeNamePop;
	private Integer valueTypePop;
	private Integer statusPop;
	private String notePop;
	private String objectPop;
	private String attributeDetailCode;
	private String attributeDetailName;
	private Integer statusDetail;
	private String attributeDetailCodePop;
	private String attributeDetailNamePop;
	private Integer statusDetailPop;

	private AttributeMgr attributeMgr;
	private List<AttributeDetailVO> lstAttributeCustomerDetail;
	private List<ApParam> lstAttributeApParam;
	private String lstAttributeCM;
	private String lstObject;

	private Boolean showObjectDMSCORE = false;

	public String getLstObject() {
		return lstObject;
	}

	public void setLstObject(String lstObject) {
		this.lstObject = lstObject;
	}

	public String getObjectPop() {
		return objectPop;
	}

	public void setObjectPop(String objectPop) {
		this.objectPop = objectPop;
	}

	public Boolean getShowObjectDMSCORE() {
		return showObjectDMSCORE;
	}

	public void setShowObjectDMSCORE(Boolean showObjectDMSCORE) {
		this.showObjectDMSCORE = showObjectDMSCORE;
	}

	public List<AttributeColumnType> getLstValueType() {
		return lstValueType;
	}

	public void setLstValueType(List<AttributeColumnType> lstValueType) {
		this.lstValueType = lstValueType;
	}

	public String getLstAttributeCM() {
		return lstAttributeCM;
	}

	public void setLstAttributeCM(String lstAttributeCM) {
		this.lstAttributeCM = lstAttributeCM;
	}

	public Long getIdDetail() {
		return idDetail;
	}

	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}

	public String getAttributeCode() {
		return attributeCode;
	}

	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}


	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public Integer getValueType() {
		return valueType;
	}

	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttributeCodePop() {
		return attributeCodePop;
	}

	public void setAttributeCodePop(String attributeCodePop) {
		this.attributeCodePop = attributeCodePop;
	}

	public String getAttributeNamePop() {
		return attributeNamePop;
	}

	public void setAttributeNamePop(String attributeNamePop) {
		this.attributeNamePop = attributeNamePop;
	}

	public Integer getValueTypePop() {
		return valueTypePop;
	}

	public void setValueTypePop(Integer valueTypePop) {
		this.valueTypePop = valueTypePop;
	}

	public Integer getStatusPop() {
		return statusPop;
	}

	public void setStatusPop(Integer statusPop) {
		this.statusPop = statusPop;
	}

	public String getNotePop() {
		return notePop;
	}

	public void setNotePop(String notePop) {
		this.notePop = notePop;
	}

	public String getAttributeDetailCode() {
		return attributeDetailCode;
	}

	public void setAttributeDetailCode(String attributeDetailCode) {
		this.attributeDetailCode = attributeDetailCode;
	}

	public String getAttributeDetailName() {
		return attributeDetailName;
	}

	public void setAttributeDetailName(String attributeDetailName) {
		this.attributeDetailName = attributeDetailName;
	}

	public Integer getStatusDetail() {
		return statusDetail;
	}

	public void setStatusDetail(Integer statusDetail) {
		this.statusDetail = statusDetail;
	}

	public List<AttributeDetailVO> getLstAttributeCustomerDetail() {
		return lstAttributeCustomerDetail;
	}

	public void setLstAttributeCustomerDetail(
			List<AttributeDetailVO> lstAttributeCustomerDetail) {
		this.lstAttributeCustomerDetail = lstAttributeCustomerDetail;
	}


	public String getAttributeDetailCodePop() {
		return attributeDetailCodePop;
	}

	public void setAttributeDetailCodePop(String attributeDetailCodePop) {
		this.attributeDetailCodePop = attributeDetailCodePop;
	}

	public String getAttributeDetailNamePop() {
		return attributeDetailNamePop;
	}

	public void setAttributeDetailNamePop(String attributeDetailNamePop) {
		this.attributeDetailNamePop = attributeDetailNamePop;
	}

	public Integer getStatusDetailPop() {
		return statusDetailPop;
	}

	public void setStatusDetailPop(Integer statusDetailPop) {
		this.statusDetailPop = statusDetailPop;
	}

	@Override
	public void prepare() throws Exception {
		attributeMgr = (AttributeMgr)context.getBean("attributeMgr");
	    super.prepare();
	}

	@Override
	public String execute(){
		resetToken(result);
		mapControl = getMapControlToken();
		if (showObjectDMSCORE) {
			showObject();
		}

		return SUCCESS;
	}


	public String search() {

		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<AttributeVO> kPaging = new KPaging<AttributeVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			AttributeFilter filter = new AttributeFilter();
			filter.setAttributeCode(attributeCode);
			filter.setAttributeName(attributeName);
//			filter.setValueType(AttributeColumnType.parseValue(valueType));
			lstValueType = new ArrayList<AttributeColumnType>();
			if (lstAttributeCM != null) {
				String[] lstAttributeCMTemp = lstAttributeCM.split(",");
				for (String i : lstAttributeCMTemp) {
					if (!StringUtil.isNullOrEmpty(i) && Integer.valueOf(i) != -1L) {
						lstValueType.add(AttributeColumnType.parseValue(Integer.valueOf(i)));
					}
				}
				filter.setLstValueType(lstValueType);
			} else {
				filter.setLstValueType(null);
			}
			filter.setListObject(lstObject);
			filter.setStatus(ActiveType.parseValue(status));
			ObjectVO<AttributeVO> lstAttribute = attributeMgr.getListAttributeVO(filter, kPaging);
			if (lstAttribute != null) {
				result.put("total", lstAttribute.getkPaging().getTotalRows());
				result.put("rows", lstAttribute.getLstObject());
			}else {
				result.put("total",0);
				result.put("rows", new ArrayList<AttributeVO>());
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeCustomerManagerAction.search()"), createLogErrorStandard(startLogDate));
		}
		return JSON;
	}
	public String loadAttributeDetail(){
		result.put("page", page);
		result.put("max", max);
		try {
			KPaging<AttributeDetailVO> kPaging = new KPaging<AttributeDetailVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			AttributeDetailFilter filter = new AttributeDetailFilter();
			filter.setAttributeId(attributeId);
			ObjectVO<AttributeDetailVO> lstAttrDetail= attributeMgr.getListAttributeDetailVO(filter, kPaging);
			if (attributeId != null && attributeId > 0){
				lstAttributeCustomerDetail = lstAttrDetail.getLstObject();
				result.put("total", lstAttrDetail.getkPaging().getTotalRows());
				result.put("rows", lstAttributeCustomerDetail);

			}else{
				result.put("total",0);
				result.put("rows", new ArrayList<AttributeDetailVO>());
			}
		} catch (BusinessException e) {
//			LogUtility.logError(e,e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeCustomerManagerAction.loadAttributeDetail()"), createLogErrorStandard(startLogDate));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return ERROR;
		}
		return JSON;
	}
	public String saveAttribute(){
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try {
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(attributeNamePop, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(attributeCodePop, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(notePop, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
		    	if(id!= null && id > 0){
		    		Attribute attribute = attributeMgr.getAttributeById(id);
		    		if(attribute!= null){
		    			attribute.setAttributeName(attributeNamePop);
		    			attribute.setStatus(ActiveType.parseValue(statusPop));
		    			attribute.setNote(notePop);
		    			attribute.setUpdateUser(getCurrentUser().getUserName());
		    			attribute.setUpdateDate(DateUtil.now());
		    			attributeMgr.updateAttribute(attribute);
			    		error = false;
			    		result.put("id", id);
			    		result.put("code", attribute.getAttributeCode());
		    		}

		    	} else {
		    		String tableNameTmp = "CUSTOMER";
		    		Attribute att = attributeMgr.getAttributeByCode(tableNameTmp,attributeCodePop);
		    		if(att!=null){
		    			result.put(ERROR, true);
		    			result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_ATTRIBUTE_NOT_EXISTS,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.customer.code.not.exist")));
		    			return JSON;
		    		}
		    		Attribute newAttribute = new Attribute();
		    		newAttribute.setAttributeCode(attributeCodePop.toUpperCase());
		    		newAttribute.setAttributeName(attributeNamePop);
		    		newAttribute.setValueType(AttributeColumnType.parseValue(valueTypePop));
		    		newAttribute.setStatus(ActiveType.parseValue(statusPop));
		    		newAttribute.setNote(notePop);
		    		newAttribute.setTableName(tableNameTmp);
		    		newAttribute.setCreateUser(getCurrentUser().getUserName());
		    		newAttribute.setCreateDate(DateUtil.now());
		    		newAttribute.setVisible(1);
		    		newAttribute = attributeMgr.createAttribute(newAttribute);
		    		if(newAttribute!= null){
		    			error = false;
		    			result.put("id", newAttribute.getId());
		    			result.put("code", newAttribute.getAttributeCode());
		    		}
		    	}

	    	}catch (BusinessException e) {
//	    		LogUtility.logError(e, e.getMessage());
	    		Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeCustomerManagerAction.saveAttribute()"), createLogErrorStandard(startLogDate));
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}

	/**
	 * Lay code param DMSCORE
	 * @author tulv2
	 * @since 17.09.2014
	 * */
	/*private String getApParamCodeDMSCORE(){
		String result = "CUSTOMER";
		try {
			ApParamFilter filter = new ApParamFilter();
			filter.setType(ApParamType.DMS_CORE_ATTR);
			filter.setStatus(ActiveType.RUNNING);
			filter.setValue(objectPop);
			List<ApParam> apParams = apParamMgr.getListApParamByFilter(filter);
			result = apParams.get(0).getApParamCode();
		} catch (BusinessException e) {
			LogUtility.logError(e, e.getMessage());
		}

		return result;
	}*/

	public String saveAttributeDetail(){
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
	    	try{
	    		if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(attributeDetailNamePop, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(attributeDetailCodePop, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
		    	if(idDetail!= null && idDetail > 0){
		    		AttributeDetail attributeDetail = attributeMgr.getAttributeDetailById(idDetail);
		    		if(attributeDetail!= null){
		    			attributeDetail.setAttributeDetailName(attributeDetailNamePop);
		    			attributeDetail.setStatus(ActiveType.parseValue(statusDetailPop));
		    			attributeMgr.updateAttributeDetail(attributeDetail);
			    		error = false;
			    		result.put("idDetail", idDetail);
			    		result.put("code", attributeDetail.getAttributeDetailCode());
		    		}

		    	} else {
		    		Attribute attribute = attributeMgr.getAttributeById(attributeId);
		    		if(attribute==null){
		    			return JSON;
		    		}
		    		AttributeDetail detail = attributeMgr.getAttributeDetailByCode(attribute.getId(), attributeDetailCodePop);
		    		if(detail!=null){
		    			result.put(ERROR, true);
		    			result.put("errMsg",ValidateUtil.getErrorMsg(ConstantManager.ERR_ATTRIBUTE_DETAIL_NOT_EXISTS,Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "attribute.customer.detail.code.not.exist")));
		    			return JSON;
		    		}
		    		AttributeDetail newAttributeDetail = new AttributeDetail();
		    		newAttributeDetail.setAttributeDetailCode(attributeDetailCodePop.toUpperCase());
		    		newAttributeDetail.setAttributeDetailName(attributeDetailNamePop);
		    		newAttributeDetail.setStatus(ActiveType.parseValue(statusDetailPop));
		    		newAttributeDetail.setAttribute(attribute);
		    		newAttributeDetail = attributeMgr.createAttributeDetail(newAttributeDetail);
		    		if(newAttributeDetail!= null){
		    			error = false;
		    			result.put("idDetail", newAttributeDetail.getId());
		    			result.put("code", newAttributeDetail.getAttributeDetailCode());
		    		}
		    	}

	    	}catch (Exception e) {
//	    		LogUtility.logError(e, e.getMessage());
	    		Date startLogDate = DateUtil.now();
				LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeCustomerManagerAction.saveAttributeDetail()"), createLogErrorStandard(startLogDate));
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);
		return JSON;
	}

	public List<ApParam> getLstAttributeApParam() {
		return lstAttributeApParam;
	}

	public void setLstAttributeApParam(List<ApParam> lstAttributeApParam) {
		this.lstAttributeApParam = lstAttributeApParam;
	}

	/**
	 * Lay danh sach ApParam theo dieu kien loc filter.
	 * @author tulv2
	 * @since 17.09.2014
	 * */
	private void showObject() {
		ApParamFilter filter = new ApParamFilter();
		filter.setType(ApParamType.DMS_CORE_ATTR);
		filter.setStatus(ActiveType.RUNNING);
		try {
			lstAttributeApParam = apParamMgr.getListApParamByFilter(filter);
		} catch (BusinessException e) {
//			LogUtility.logError(e, e.getMessage());
			Date startLogDate = DateUtil.now();
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.AttributeCustomerManagerAction.showObject()"), createLogErrorStandard(startLogDate));
		}
	}
}

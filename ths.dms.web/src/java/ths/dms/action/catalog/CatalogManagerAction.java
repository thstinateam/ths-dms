/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.CatalogFilter;
import ths.dms.core.entities.filter.StaffTypeFilter;
import ths.dms.core.entities.vo.CatalogVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.JsTreeNode;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.NumberUtil;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * quan ly danh muc
 * @author trietptm
 * @since Nov 17, 2015
 * */
public class CatalogManagerAction extends AbstractAction {
	private static final long serialVersionUID = 1L;

	ProductInfoMgr productInfoMgr;
	ProductMgr productMgr;
	private String type;
	private String catCode;
	private String catName;
	private String catDescript;
	private String oldCatCode;
	private Boolean isEdit;
	private String pdCodeName;
	private List<ProductInfo> lstCat;
	private List<JsTreeNode> lstCatalogTree;
	private Long parentCatId;
	private Long staffTypeId;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		currentShop = getCurrentShop();
		staff = getStaffByCurrentUser();
		productInfoMgr = (ProductInfoMgr) context.getBean("productInfoMgr");
		productMgr = (ProductMgr) context.getBean("productMgr");
	}

	@Override
	public String execute() throws Exception {
		generateToken();
//		try {
			staff = getStaffByCurrentUser();
//		} catch (Exception e) {
//			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.CatalogManagerAction.execute"), createLogErrorStandard(actionStartTime));
//		}
		return SUCCESS;
	}

	/**
	 * load cay danh muc
	 * @author trietptm
	 * @return String
	 * @since Nov 23, 2015
	 */
	public String getCatalogTree() {
		try {
			lstCatalogTree = new ArrayList<JsTreeNode>();

			Shop currentShop = getCurrentShop();
			if (currentShop == null) {
				return JSON;
			}
			// san pham
			JsTreeNode productNode = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.san.pham"), "");
			productNode.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.san.pham"));
			productNode.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<JsTreeNode> lstProductChildNode = new ArrayList<JsTreeNode>();
			JsTreeNode unit = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.don.vi.tinh"), ApParamType.UOM.getValue());
			lstProductChildNode.add(unit);
			JsTreeNode category = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.nganh.hang"), ProductType.CAT.getValue().toString());
			lstProductChildNode.add(category);
			JsTreeNode subCategory = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.nganh.hang.con"), ProductType.SUB_CAT.getValue().toString());
			lstProductChildNode.add(subCategory);
			JsTreeNode brand = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.nhan.hieu"), ProductType.BRAND.getValue().toString());
			lstProductChildNode.add(brand);
			JsTreeNode flavor = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.huong.vi"), ProductType.FLAVOUR.getValue().toString());
			lstProductChildNode.add(flavor);
			JsTreeNode packing = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.dong.goi"), ProductType.PACKING.getValue().toString());
			lstProductChildNode.add(packing);
			JsTreeNode expirtDate = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.expiry.date"), ApParamType.NGAY_THANG_NAM.getValue());
			lstProductChildNode.add(expirtDate);
			productNode.setChildren(lstProductChildNode);
			lstCatalogTree.add(productNode);
			// van de
			JsTreeNode problems = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.van.de.title"), "");
			problems.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.van.de.title"));
			problems.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<JsTreeNode> lstProblemsChildNode = new ArrayList<JsTreeNode>();
			JsTreeNode problemType = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.van.de.list.title"), ApParamType.FEEDBACK_TYPE.getValue());
			lstProblemsChildNode.add(problemType);
			problems.setChildren(lstProblemsChildNode);
			lstCatalogTree.add(problems);
			//khach hang
			JsTreeNode customer = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.customer"), "");
			customer.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.customer"));
			customer.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<JsTreeNode> lstCustomerChildNode = new ArrayList<JsTreeNode>();
			JsTreeNode customerPosition = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.customer.position"), ApParamType.CUSTOMER_SALE_POSITION.getValue());
			lstCustomerChildNode.add(customerPosition);
			customer.setChildren(lstCustomerChildNode);
			lstCatalogTree.add(customer);
			//nhan vien
			JsTreeNode staff = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.staff"), "");
			staff.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.staff"));
			staff.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<JsTreeNode> lstStaffChildNode = new ArrayList<JsTreeNode>();
			JsTreeNode staffType = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.staff.type.sale"), ApParamType.STAFF_SALE_TYPE.getValue());
			lstStaffChildNode.add(staffType);
			JsTreeNode positionSuperviseStaffType = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.staff.type.position.supervise.tree"), ApParamType.STAFF_TYPE_SHOW_SUP.getValue());
			lstStaffChildNode.add(positionSuperviseStaffType);
			staff.setChildren(lstStaffChildNode);
			lstCatalogTree.add(staff);
			// chuong trinh trong tam
			JsTreeNode focusProgram = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.cttt"), "");
			focusProgram.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.cttt"));
			focusProgram.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<JsTreeNode> lstFocusProgramChildNode = new ArrayList<JsTreeNode>();
			JsTreeNode focusProductType = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.cttt.focus.product"), ApParamType.FOCUS_PRODUCT_TYPE.getValue());
			lstFocusProgramChildNode.add(focusProductType);
			focusProgram.setChildren(lstFocusProgramChildNode);
			lstCatalogTree.add(focusProgram);
			// nhap hang
			JsTreeNode importProduct = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.import.product"), "");
			importProduct.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.import.product"));
			importProduct.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<JsTreeNode> lstImportProductChildNode = new ArrayList<JsTreeNode>();
			JsTreeNode reasonUpdateInvoice = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.reason.update.invoice"), ApParamType.PO_REASON_INVOICE.getValue());
			lstImportProductChildNode.add(reasonUpdateInvoice);
			importProduct.setChildren(lstImportProductChildNode);
			lstCatalogTree.add(importProduct);
			// nhap hang
			JsTreeNode order = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.order"), "");
			order.setData(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.order"));
			order.setState(ConstantManager.JSTREE_STATE_OPEN);
			List<JsTreeNode> lstOrderChildNode = new ArrayList<JsTreeNode>();
			JsTreeNode orderPriority = new JsTreeNode(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.order.priority"), ApParamType.ORDER_PIRITY.getValue());
			lstOrderChildNode.add(orderPriority);
			order.setChildren(lstOrderChildNode);
			lstCatalogTree.add(order);

		} catch (BusinessException ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.action.catalog.CatalogManagerAction.getCatalogTree"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}

		return JSON;
	}

	/**
	 *
	 * load chi tiet danh sach tung danh muc
	 * @author trietptm
	 * @return String
	 * @since Nov 23, 2015
	 */
	public String loadShopCat() {
		result.put("page", page);
		result.put("max", max);
		result.put("total", 0);
		result.put("rows", new ArrayList<ProductInfo>());
		try {
			KPaging<CatalogVO> kPaging = new KPaging<CatalogVO>();
			kPaging.setPage(page - 1);
			kPaging.setPageSize(max);
			CatalogFilter filter = new CatalogFilter();
			filter.setkPaging(kPaging);
			if (staff != null && staff.getShop() != null) {
				filter.setShopId(staff.getShop().getId());
			}
			if (StringUtil.isNullOrEmpty(type)) {
				type = ApParamType.UOM.getValue();
			}
			filter.setType(type);
			filter.setText(pdCodeName);
			ObjectVO<CatalogVO> lstObj = productInfoMgr.getListCatalogByFilter(filter);
			if (ProductType.SUB_CAT.getValue().toString().equals(type)) {
				ObjectVO<ProductInfo> lstCategoryTmp = productInfoMgr.getListProductInfoEx(null, null, null, ActiveType.RUNNING, ProductType.CAT, null, true);
				List<ProductInfo> listCat = lstCategoryTmp.getLstObject();
				if (listCat != null && listCat.size() > 0) {
					result.put("lstCat", listCat);
				} else {
					result.put("lstCat", new ArrayList<ProductInfo>());
				}
			} else if (ApParamType.STAFF_TYPE_SHOW_SUP.getValue().equals(type)) {
				StaffTypeFilter staffTypeFilter = new StaffTypeFilter();
				staffTypeFilter.setStatus(ActiveType.RUNNING);
				List<StaffType> lstStaffType = staffMgr.getListAllStaffType(staffTypeFilter);
				if (lstStaffType != null && lstStaffType.size() > 0) {
					result.put("lstStaffType", lstStaffType);
				} else {
					result.put("lstStaffType", new ArrayList<ProductInfo>());
				}
			} else {
				result.put("lstCat", new ArrayList<ProductInfo>());
			}
			if (lstObj != null && lstObj.getLstObject() != null && lstObj.getLstObject().size() > 0) {
				result.put("total", lstObj.getkPaging().getTotalRows());
				result.put("rows", lstObj.getLstObject());
			} else {
				result.put("total", 0);
				result.put("rows", new ArrayList<ProductInfo>());
			}
		} catch (BusinessException ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.action.catalog.CatalogManagerAction.loadShopCat"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 * Xu ly luu tao moi, thay doi
	 * @author trietptm
	 * @return String
	 * @since Nov 23, 2015
	 */
	public String saveShopCat() {
		resetToken(result);
		result.put(ERROR, false);
		try {
			if (isEdit && StringUtil.isNullOrEmpty(oldCatCode)) {
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.data.invalid");
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(catCode, "cat.manage.grid.tt.code", 20, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(catName, "cat.manage.grid.tt.name", 200, ConstantManager.ERR_REQUIRE, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, ConstantManager.ERR_MAX_LENGTH);
			}
			if (StringUtil.isNullOrEmpty(errMsg)) {
				errMsg = ValidateUtil.validateField(catDescript, "jsp.khuyenmai.ghichu", 1000, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, ConstantManager.ERR_MAX_LENGTH);
			}
			if (!StringUtil.isNullOrEmpty(errMsg)) {
				result.put("errMsg", errMsg);
				result.put(ERROR, true);
				return JSON;
			}
			if (ApParamType.UOM.getValue().equals(type) || ApParamType.NGAY_THANG_NAM.getValue().equals(type)
					|| ApParamType.FEEDBACK_TYPE.getValue().equals(type) || ApParamType.CUSTOMER_SALE_POSITION.getValue().equals(type)
					|| ApParamType.STAFF_SALE_TYPE.getValue().equals(type) || ApParamType.FOCUS_PRODUCT_TYPE.getValue().equals(type)
					|| ApParamType.PO_REASON_INVOICE.getValue().equals(type) || ApParamType.ORDER_PIRITY.getValue().equals(type)
					|| ApParamType.STAFF_TYPE_SHOW_SUP.getValue().equals(type)) {
				// ApParam
				if (ApParamType.STAFF_TYPE_SHOW_SUP.getValue().equals(type)) {
					if (staffTypeId != null) {
						StaffTypeFilter staffTypeFilter = new StaffTypeFilter();
						staffTypeFilter.setId(staffTypeId);
						List<StaffType> lstStaffType = staffMgr.getListAllStaffType(staffTypeFilter);
						if (lstStaffType == null || lstStaffType.size() == 0) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.data.invalid"));
							return JSON;
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.data.invalid"));
						return JSON;
					}
				}

				ApParamType apParamType = ApParamType.parseValue(type);
				if (isEdit) {
					ApParam apParamOld = apParamMgr.getApParamByCode(oldCatCode, apParamType);
					if (apParamOld == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.unit.error.not.exist", catCode));
						return JSON;
					}
					apParamOld.setApParamName(catName);
					if (ApParamType.STAFF_TYPE_SHOW_SUP.getValue().equals(type)) {
						apParamOld.setValue(staffTypeId.toString());
					} else if (!ApParamType.FOCUS_PRODUCT_TYPE.getValue().equals(type)) {
						apParamOld.setValue(catName);
					}
					apParamOld.setType(apParamType);
					apParamOld.setDescription(catDescript);
					apParamOld.setStatus(ActiveType.RUNNING);
					apParamMgr.updateApParam(apParamOld, getLogInfoVO());
				} else {
					ApParam apParam = apParamMgr.getApParamByCode(catCode, apParamType);
					if (apParam != null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.error.exist.code", catCode));
						return JSON;
					}
					ApParam apParamSave = new ApParam();
					apParamSave.setApParamCode(catCode.toUpperCase());
					apParamSave.setApParamName(catName);
					if (ApParamType.STAFF_TYPE_SHOW_SUP.getValue().equals(type)) {
						apParamSave.setValue(staffTypeId.toString());
					} else if (ApParamType.FOCUS_PRODUCT_TYPE.getValue().equals(type)) {
						Long value = null;
						List<ApParam> lstApParam = apParamMgr.getListApParam(ApParamType.FOCUS_PRODUCT_TYPE, null);
						if (lstApParam != null) {
							for (ApParam app : lstApParam) {
								if (app != null && app.getValue() != null) {
									value = NumberUtil.tryParseLong(app.getValue());
									if (value != null) {
										break;
									}
								}
							}
						}
						if (value == null) {
							value = 0L;
						}
						value++;
						apParamSave.setValue(value.toString());
					} else {
						apParamSave.setValue(catName);
					}
					apParamSave.setType(apParamType);
					apParamSave.setDescription(catDescript);
					apParamSave.setStatus(ActiveType.RUNNING);
					apParamMgr.createApParam(apParamSave, getLogInfoVO());
				}
			} else if (ProductType.CAT.getValue().toString().equals(type) || ProductType.SUB_CAT.getValue().toString().equals(type)
					|| ProductType.BRAND.getValue().toString().equals(type) || ProductType.FLAVOUR.getValue().toString().equals(type)
					|| ProductType.PACKING.getValue().toString().equals(type)) {
				// ProductInfo
				ProductInfo parentPdInfo = null;
				ProductInfo productInfoSave = null;
				ProductType productType = ProductType.parseValue(Integer.valueOf(type));
				if (ProductType.SUB_CAT.getValue().toString().equals(type)) {
					if (parentCatId != null) {
						parentPdInfo = productInfoMgr.getProductInfoById(parentCatId);
						if (parentPdInfo == null) {
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.cat.error.not.exist"));
							return JSON;
						}
					} else {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.data.invalid"));
						return JSON;
					}
				}
				if (!catCode.equalsIgnoreCase(oldCatCode)) {
					ProductInfo pdInfo = productInfoMgr.getProductInfoByWithNotSubCat(catCode, productType, true);
					if (pdInfo != null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.error.exist.code", catCode));
						return JSON;
					}
				}
				if (isEdit) {
					ProductInfo productInfoOld = productInfoMgr.getProductInfoByWithNotSubCat(oldCatCode, productType, true);
					if (productInfoOld == null) {
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.error.not.exist.code", catCode));
						return JSON;
					}
					productInfoSave = productInfoOld;
					productInfoSave.setProductInfoCode(catCode.toUpperCase());
					productInfoSave.setProductInfoName(catName);
					productInfoSave.setDescription(catDescript);
					productInfoSave.setStatus(ActiveType.RUNNING);
					productInfoSave.setType(productType);
					productInfoMgr.updateProductInfo(productInfoSave, parentPdInfo, getLogInfoVO());
				} else {
					productInfoSave = new ProductInfo();
					productInfoSave.setProductInfoCode(catCode.toUpperCase());
					productInfoSave.setProductInfoName(catName);
					productInfoSave.setDescription(catDescript);
					productInfoSave.setStatus(ActiveType.RUNNING);
					productInfoSave.setType(productType);
					productInfoMgr.createProductInfo(productInfoSave, parentPdInfo, getLogInfoVO());
				}
			}
		} catch (BusinessException ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.action.catalog.CatalogManagerAction.saveShopCat"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	/**
	 *
	 * Xu ly xoa danh muc
	 * @author trietptm
	 * @return
	 * @since Nov 23, 2015
	 */
	public String deleteShopCat() {
		resetToken(result);
		try {
			if (ApParamType.STAFF_TYPE_SHOW_SUP.getValue().equals(type)) {
				ApParamType apParamType = ApParamType.parseValue(type);
				ApParam apParam = apParamMgr.getApParamByCode(catCode, apParamType);
				if (apParam != null) {
					apParam.setStatus(ActiveType.DELETED);
					apParamMgr.updateApParam(apParam, getLogInfoVO());
				}
				/*else {
					result.put(ERROR, true);
					result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "cat.manage.error.not.exist.code", catCode));
					return JSON;
				}*/
			} else if (ProductType.CAT.getValue().toString().equals(type) || ProductType.SUB_CAT.getValue().toString().equals(type)
					|| ProductType.BRAND.getValue().toString().equals(type) || ProductType.FLAVOUR.getValue().toString().equals(type)
					|| ProductType.PACKING.getValue().toString().equals(type)) {
				String catName = "";
				if (ProductType.CAT.getValue().toString().equals(type)) {
					catName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.nganh.hang");
				} else if (ProductType.SUB_CAT.getValue().toString().equals(type)) {
					catName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.nganh.hang.con");
				} else if (ProductType.BRAND.getValue().toString().equals(type)) {
					catName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.nhan.hieu");
				} else if (ProductType.FLAVOUR.getValue().toString().equals(type)) {
					catName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.huong.vi");
				} else if (ProductType.PACKING.getValue().toString().equals(type)) {
					catName = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "khai.bao.danh.muc.dong.goi");
				}

				ProductType pdType = ProductType.parseValue(Integer.valueOf(type));
				ProductInfo pdInfo = productInfoMgr.getProductInfoByWithNotSubCat(catCode, pdType, true);
				if (pdInfo != null) { // xet san pham dang thuoc nganh hang cha
					List<Product> lstPd = productMgr.checkIfExistsProductByProductInfo(pdType, pdInfo.getId());
					if (lstPd != null && lstPd.size() > 0) { // neu nganh hang co san pham dang su dung
						result.put(ERROR, true);
						result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.check.ton.tai.sp.dang.su.dung.cat", pdInfo.getProductInfoName(), catName));
						return JSON;
					}
					if (ProductType.CAT.getValue().toString().equals(type)) { // xet nganh hang con
						List<ProductInfo> listSubCat = productInfoMgr.getListSubCatByCatId(pdInfo.getId(), ActiveType.RUNNING);
						if (listSubCat != null && listSubCat.size() > 0) {// neu con nganh hang con su dung nganh hang nay
							result.put(ERROR, true);
							result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.check.ton.tai.subcat.dang.su.dung.cat", pdInfo.getProductInfoName(), catName));
							return JSON;
						}
					}
					productInfoMgr.changeStatusDeleteProductInfo(pdInfo, getLogInfoVO());
				}
			}
		} catch (BusinessException ex) {
			LogUtility.logErrorStandard(ex, R.getResource("web.log.message.error", "ths.dms.action.catalog.CatalogManagerAction.deleteShopCat"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		}
		return JSON;
	}

	//getter - setter

	public String getOldCatCode() {
		return oldCatCode;
	}

	public void setOldCatCode(String oldCatCode) {
		this.oldCatCode = oldCatCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getCatDescript() {
		return catDescript;
	}

	public void setCatDescript(String catDescript) {
		this.catDescript = catDescript;
	}

	public Boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}

	public String getPdCodeName() {
		return pdCodeName;
	}

	public void setPdCodeName(String pdCodeName) {
		this.pdCodeName = pdCodeName;
	}

	public List<ProductInfo> getLstCat() {
		return lstCat;
	}

	public void setLstCat(List<ProductInfo> lstCat) {
		this.lstCat = lstCat;
	}

	public Long getParentCatId() {
		return parentCatId;
	}

	public void setParentCatId(Long parentCatId) {
		this.parentCatId = parentCatId;
	}

	public List<JsTreeNode> getLstCatalogTree() {
		return lstCatalogTree;
	}

	public void setLstCatalogTree(List<JsTreeNode> lstCatalogTree) {
		this.lstCatalogTree = lstCatalogTree;
	}

	public Long getStaffTypeId() {
		return staffTypeId;
	}

	public void setStaffTypeId(Long staffTypeId) {
		this.staffTypeId = staffTypeId;
	}
}

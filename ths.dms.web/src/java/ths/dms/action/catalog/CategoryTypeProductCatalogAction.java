package ths.dms.action.catalog;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.ShopProductMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopProduct;
import ths.dms.core.entities.Staff;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;

public class CategoryTypeProductCatalogAction extends AbstractAction{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	private File excelFile;
	private String excelFileContentType;
	private List<ProductInfo> lstCategoryType;
	private ProductInfoMgr productInfoMgr;
	private ShopProductMgr shopProductMgr;
	private StaffMgr staffMgr;
	private String shopCode;
	private String shopName;
	private Long categoryId;
	private String categoryCode;
	private ShopProduct shopProduct;
	private ShopMgr shopMgr;
	private Integer stockMin;
	private Integer stockMax;
	private Integer dateGo;
	private String dateSale;
	private Float growth;
	private Integer status;
	private Long categoryType;
	private Long productId;
	private String productCode;
	private ProductMgr productMgr;
	private Integer staffRoleType;
	private Integer shopObjectType;

	public Integer getShopObjectType() {
		return shopObjectType;
	}

	public void setShopObjectType(Integer shopObjectType) {
		this.shopObjectType = shopObjectType;
	}

	public Integer getStaffRoleType() {
		return staffRoleType;
	}

	public void setStaffRoleType(Integer staffRoleType) {
		this.staffRoleType = staffRoleType;
	}

	public ShopProduct getShopProduct() {
		return shopProduct;
	}

	public void setShopProduct(ShopProduct shopProduct) {
		this.shopProduct = shopProduct;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Long getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(Long categoryType) {
		this.categoryType = categoryType;
	}

	public Integer getStockMin() {
		return stockMin;
	}

	public void setStockMin(Integer stockMin) {
		this.stockMin = stockMin;
	}

	public Integer getStockMax() {
		return stockMax;
	}

	public void setStockMax(Integer stockMax) {
		this.stockMax = stockMax;
	}

	public Integer getDateGo() {
		return dateGo;
	}

	public void setDateGo(Integer dateGo) {
		this.dateGo = dateGo;
	}

	public String getDateSale() {
		return dateSale;
	}

	public void setDateSale(String dateSale) {
		this.dateSale = dateSale;
	}

	public Float getGrowth() {
		return growth;
	}

	public void setGrowth(Float growth) {
		this.growth = growth;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public List<ProductInfo> getLstCategoryType() {
		return lstCategoryType;
	}

	public void setLstCategoryType(List<ProductInfo> lstCategoryType) {
		this.lstCategoryType = lstCategoryType;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}


	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	    shopProductMgr = (ShopProductMgr)context.getBean("shopProductMgr");
	    shopMgr = (ShopMgr)context.getBean("shopMgr");
	    productMgr = (ProductMgr)context.getBean("productMgr");
	    staffMgr =(StaffMgr) context.getBean("staffMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
//		try {
			Staff staff=getStaffByCurrentUser();
			if(staff!=null && staff.getShop()!=null && staff.getShop().getType()!=null){
//				shopObjectType = staff.getShop().getType().getObjectType();
				shopCode = staff.getShop().getShopCode();
				shopName = staff.getShop().getShopName();
			}
			/*ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfo(null, null, null, null, null, ProductType.CAT,true);
			lstCategoryType = tmp.getLstObject();
			mapControl = getMapControlToken();*/
//		} catch (Exception e) {
//			LogUtility.logError(e, e.getMessage());
//		}
	    return SUCCESS;
	}

	private Shop shopForNPP(){
		try{
			if(currentUser!=null && currentUser.getShopRoot()!=null){
				return shopMgr.getShopById(currentUser.getShopRoot().getShopId());
			}
		}catch(BusinessException e) {
//			LogUtility.logError(e, "Error ManageRouteCreateAction");
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.CategoryTypeProductCatalogAction.shopForNPP()"), createLogErrorStandard(actionStartTime));
		}
		return null;
	}

	/**
	 * Search.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012
	 *
	 *  @author hunglm16
	 *  @since JUNE 19, 2014
	 *  @description Phan quyen NPP, Phan quyen VNM
	 */
	public String search(){
	    return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author lamnh
	 * @since 18/07/2012
	 *
	 * @author hunglm16
	 *  @since JUNE 19, 2014
	 *  @description Phan quyen NPP, Phan quyen VNM
	 */
	public String saveOrUpdate(){
		return SUCCESS;
	}

	/**
	 * @author hunglm16
	 * @since JUNE 19, 2014
	 * @description Update Phan quyen cho chuc nang
	 * */
	public String export() {
		return SUCCESS;
	}

	/**
	 * Delete category.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String deleteCategory(){

	   /* resetToken(result);
	    boolean error = true;
	    if(currentUser!= null){
			try{
				//Kiem tra quyen thuc hien chuc nang
				Shop shop = null;
				if(currentUser!=null && currentUser.getShopRoot()!=null){
					shop = shopMgr.getShopById(currentUser.getShopRoot().getShopId());
				}
	    		if(shop!= null && shop.getType()!=null){
	    			if(!ShopObjectType.VNM.getValue().equals(shop.getType().getObjectType())){
	    				result.put(ERROR, error);
					    result.put("errMsg", "Không được quyền thực hiện chức năng");
					    return JSON;
	    			}
	    		}
				if(productId != null && productId > 0){
					shopProduct = shopProductMgr.getShopProductById(productId);
					if(shopProduct!= null){
						shopProduct.setStatus(ActiveType.DELETED);
						shopProductMgr.updateShopProduct(shopProduct,getLogInfoVO());
						error = false;
					}
				}
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}
	    }
	    result.put(ERROR, error);*/
	    return JSON;
	}

	/**
	 * View detail.
	 *
	 * @return the string
	 * @author lamnh
	 * @throws BusinessException
	 * @since Oct 1, 2012
	 */
	public String viewDetail() throws BusinessException{
		resetToken(result);
		if(productId != null && productId >0){
			shopProduct = shopProductMgr.getShopProductById(productId);
			if(shopProduct != null && shopProduct.getCat() != null){
				categoryType = shopProduct.getCat().getId();
			}
			if(shopProduct != null) {
				if (!checkShopInOrgAccessByCode(shopProduct.getShop().getShopCode())) {
					throw new BusinessException();
				}
			}
		}
		/*ObjectVO<ProductInfo> tmp = productInfoMgr.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT,true);
		loadAttributeData(shopProduct,TableHasAttribute.PRODUCT);
		lstCategoryType = tmp.getLstObject();*/
		return LIST;
	}
	public boolean checkSaleDayDuplicate(String value) {
		Map<Character, Boolean> saleDayCharMap = new HashMap<Character, Boolean>();
		for (int i = 0; i< value.length(); ++i){
			char ch = value.charAt(i);
			if(saleDayCharMap.get(ch) == null) {
				saleDayCharMap.put(ch, true);
			} else {
				return false;
			}
		}
		return true;
	}

	public boolean checkSaleDayDuplicateAndOrder(String value) {
		Map<Character, Boolean> saleDayCharMap = new HashMap<Character, Boolean>();
		char lastOrder = '0';
		for (int i = 0; i< value.length(); ++i){
			char ch = value.charAt(i);
			if(ch < lastOrder) {
				return false;
			}
			if(ch != '0') {
				if(saleDayCharMap.get(ch) == null) {
					saleDayCharMap.put(ch, true);
				} else {
					return false;
				}
			}
			lastOrder = ch;
		}
		return true;
	}
	public String checkFileImport(List<List<String>> lstData, int j) {
		String message = "";
		if (lstData.get(j) != null) {
			List<String> row1 = lstData.get(j);
			message = "";
			for (int k = 0; k < lstData.size(); k++) {
				if (lstData.get(k) != null && k != j) {
					List<String> row2 = lstData.get(k);
					if (row1.get(0).equals(row2.get(0))
							&& row1.get(1).equals(row2.get(1))) {
							if (StringUtil.isNullOrEmpty(message)) {
								message = Configuration.getResourceString(
										ConstantManager.VI_LANGUAGE,
										"shop.product.row")
										+ (j + 2) + ", " + (k + 2);
							} else {
								message += ", " + (k + 2);
							}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(message)) {
				message += " "
						+ Configuration.getResourceString(
								ConstantManager.VI_LANGUAGE,
								"shop.product.duplicate");
			}
		}
		return message;
	}
}

package ths.dms.action.catalog;

import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.vo.ObjectVO;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class PackingCatalogAction extends AbstractAction {	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2128120975032582873L;

	/** The id. */
	private Integer id;
	
	/** The code. */
	private String code;
	
	/** The name. */
	private String name;
	
	/** The note. */
	private String note;
	
	/** The status. */
	private String status;	
			
	/** The product info mgr. */
	private ProductInfoMgr productInfoMgr;

	/**
	 * getId
	 * @return the id
	 */
	public Integer getId() {
	    return id;
	}

	/**
	 * setId
	 * @param id the id to set
	 */
	public void setId(Integer id) {
	    this.id = id;
	}

	/**
	 * getCode
	 * @return the code
	 */
	public String getCode() {
	    return code;
	}

	/**
	 * setCode
	 * @param code the code to set
	 */
	public void setCode(String code) {
	    this.code = code;
	}

	/**
	 * getName
	 * @return the name
	 */
	public String getName() {
	    return name;
	}

	/**
	 * setName
	 * @param name the name to set
	 */
	public void setName(String name) {
	    this.name = name;
	}

	/**
	 * getNote
	 * @return the note
	 */
	public String getNote() {
	    return note;
	}

	/**
	 * setNote
	 * @param note the note to set
	 */
	public void setNote(String note) {
	    this.note = note;
	}

	/**
	 * getStatus
	 * @return the status
	 */
	public String getStatus() {
	    return status;
	}

	/**
	 * setStatus
	 * @param status the status to set
	 */
	public void setStatus(String status) {
	    this.status = status;
	}

	/* (non-Javadoc)
	 * @see ths.dms.web.action.general.AbstractAction#prepare()
	 */
	@Override
	public void prepare() throws Exception {
	    super.prepare();
	    productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute() {	
	    status = ActiveType.RUNNING.getValue().toString();
	    resetToken(result);
	    return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String search(){
	    
	    result.put("page", page);
	    result.put("max", max);
	    try{
			KPaging<ProductInfo> kPaging = new KPaging<ProductInfo>();
			kPaging.setPageSize(max);
			kPaging.setPage(page-1);
	    	/*ObjectVO<ProductInfo> lstPacking = productInfoMgr.getListProductInfo(
	    						kPaging, code, name, note,
	    						ActiveType.parseValue(Integer.valueOf(status)),ProductType.PACKING,true);
	    	if(lstPacking!= null){
		    	result.put("total", lstPacking.getkPaging().getTotalRows());
		    	result.put("rows", lstPacking.getLstObject());		
	    	}*/		
	    }catch (Exception e) {
	    	LogUtility.logError(e, e.getMessage());
	    }
	    return JSON;
	}

	/**
	 * Save or update.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String saveOrUpdate(){
	    
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(code, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(name, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (StringUtil.isNullOrEmpty(errMsg)) {
					errMsg = ValidateUtil.validateField(note, null, null, ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL);
				}
				if (!StringUtil.isNullOrEmpty(errMsg)) {
					result.put("errMsg", errMsg);
					result.put(ERROR, true);
					return JSON;
				}
			    ProductInfo productInfo = productInfoMgr.getProductInfoByCode(code, ProductType.PACKING,null,null);
			    if(id!= null && id > 0){
					if(productInfo!= null){
//					    if(ActiveType.STOPPED.getValue().intValue() == Integer.valueOf(status).intValue() 
//						    && productInfoMgr.isUsingByOthers(productInfo.getId())){
//					    	errMsg= Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.is.used",
//									Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.packing"));
//					    } else if(id.intValue() != productInfo.getId().intValue()){
//					    	errMsg= Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.code.exist",
//									Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.packing.code"),code);
//					    } else{
						 productInfo.setProductInfoCode(code);
						 productInfo.setProductInfoName(name);
						 productInfo.setDescription(note);
						 productInfo.setStatus(ActiveType.parseValue(Integer.valueOf(status)));
						 productInfo.setType(ProductType.PACKING);
						 productInfoMgr.updateProductInfo(productInfo,getLogInfoVO());
						 error = false;
//					    }
					}    
			    } else {
					if(productInfo!= null){
						errMsg = ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST,null,"catalog.packing.code");
					} else {
					    ProductInfo newProductInfo = new ProductInfo();
					    newProductInfo.setProductInfoCode(code);
					    newProductInfo.setProductInfoName(name);
					    newProductInfo.setDescription(note);
					    newProductInfo.setStatus(ActiveType.parseValue(Integer.valueOf(status)));
					    newProductInfo.setType(ProductType.PACKING);
					    newProductInfo = productInfoMgr.createProductInfo(newProductInfo,getLogInfoVO());
					    if(newProductInfo!= null){
					    	error = false;
					    }
					}
			    }		    
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}		
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}
	
	/**
	 * Delete packing.
	 *
	 * @return the string
	 * @author lamnh
	 * @since Oct 1, 2012
	 */
	public String deletePacking(){
	    
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
			    ProductInfo productInfo = productInfoMgr.getProductInfoByCode(code, ProductType.PACKING,null,null);
			    if(productInfo!= null){
					if(productInfoMgr.isUsingByOthers(productInfo.getId())){
						errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_IS_USED_DB,
								Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "catalog.packing.code"));								
					}else{
						productInfo.setStatus(ActiveType.DELETED);
					    productInfoMgr.updateProductInfo(productInfo,getLogInfoVO());
					    error = false;
					}			    	
			    } 
			   		    
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}

}

package ths.dms.action.catalog;

import java.util.ArrayList;
import java.util.List;

import ths.dms.core.business.PoMgr;
import ths.dms.core.business.ProductInfoMgr;
import ths.dms.core.entities.PoAutoGroup;
import ths.dms.core.entities.PoAutoGroupDetail;
import ths.dms.core.entities.PoAutoGroupShopMap;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailType;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailVO;
import ths.dms.core.entities.enumtype.PoAutoGroupFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupShopMapFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupVO;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoAutoGroupShopMapVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;

import ths.dms.helper.Configuration;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

public class GroupProductPOAutoNPPCatalog extends AbstractAction{

	/**
	 * 
	 */
	private Long id;
	private String shopCode;
	private String shopName;
	private String groupCode;
	private String groupName;
	private String status;
	private PoMgr poMgr;
	private Long idGroup;//idPoAutoGroupShopMap
	private Integer objectType;
	private Long groupDetailId;
	private PoAutoGroupDetail poAutoGroupDetail;
	private PoAutoGroup poAutoGroup;
	private Integer statusValue;
	private String code;
	private String name;
	private Integer poAutoGroupId;
	private List<String> listOjectId;
	private List<String> lstCategoryId;
	private List<String> listShopId;
	
	
	
	public List<String> getListShopId() {
		return listShopId;
	}

	public void setListShopId(List<String> listShopId) {
		this.listShopId = listShopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public List<String> getLstCategoryId() {
		return lstCategoryId;
	}

	public void setLstCategoryId(List<String> lstCategoryId) {
		this.lstCategoryId = lstCategoryId;
	}

	public List<String> getListOjectId() {
		return listOjectId;
	}

	public void setListOjectId(List<String> listOjectId) {
		this.listOjectId = listOjectId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPoAutoGroupId() {
		return poAutoGroupId;
	}

	public void setPoAutoGroupId(Integer poAutoGroupId) {
		this.poAutoGroupId = poAutoGroupId;
	}

	public PoAutoGroupDetail getPoAutoGroupDetail() {
		return poAutoGroupDetail;
	}

	public void setPoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) {
		this.poAutoGroupDetail = poAutoGroupDetail;
	}

	public Long getGroupDetailId() {
		return groupDetailId;
	}

	public void setGroupDetailId(Long groupDetailId) {
		this.groupDetailId = groupDetailId;
	}

	public Long getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(Long idGroup) {
		this.idGroup = idGroup;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public PoMgr getPoMgr() {
		return poMgr;
	}

	public void setPoMgr(PoMgr poMgr) {
		this.poMgr = poMgr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Integer getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(Integer statusValue) {
		this.statusValue = statusValue;
	}

	private ProductInfoMgr productInfoMgr;
	private static final long serialVersionUID = 1L;
	
	@Override
	public void prepare() throws Exception {	
		super.prepare();		
		poMgr =(PoMgr)context.getBean("poMgr");
		productInfoMgr = (ProductInfoMgr)context.getBean("productInfoMgr");
	}
	
	@Override
	public String execute() throws Exception {	
		status = ActiveType.RUNNING.getValue().toString();
		resetToken(result);
		return SUCCESS;
	}
	
	/**
	 * Search.
	 *
	 * @return the string
	 * @author liemtpt
	 * @since Aug 14, 2013
	 */
	public String search() {
		result.put("page", page);
	    result.put("rows", rows);
		try {
			KPaging<PoAutoGroupShopMapVO> kPaging = new KPaging<PoAutoGroupShopMapVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			PoAutoGroupShopMapFilter filter = new PoAutoGroupShopMapFilter();
			filter.setShopCode(shopCode);
			filter.setShopName(shopName);
			filter.setkPaging(kPaging);
//			ObjectVO<Shop>lstShop = shopMgr.getListShopNotInPoGroup(filter);
			ObjectVO<PoAutoGroupShopMapVO> lstShop = poMgr.getListShopInPoGroup(filter,ActiveType.parseValue(Integer.valueOf(status)));
			if (lstShop != null) {
				result.put("total", lstShop.getkPaging().getTotalRows());
				result.put("rows", lstShop.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<PoAutoGroupShopMapVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	public String getListShopNotInPoGroup(){//Dung trong man hinh them nha phan phoi.
		result.put("page", page);
	    result.put("rows", rows);
		try {
			KPaging<Shop> kPaging = new KPaging<Shop>();
			Long shopId = null;
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			if(currentUser != null){
				Staff s = staffMgr.getStaffByCode(currentUser.getUserName());
				if(s != null && s.getShop() != null){
						shopId = s.getShop().getId();
				}
			}
			ShopFilter filter = new ShopFilter();
			if(!StringUtil.isNullOrEmpty(shopCode)){
				filter.setShopCode(shopCode);
			}
			if(!StringUtil.isNullOrEmpty(shopName)){
				filter.setShopName(shopName);
			}
			filter.setkPaging(kPaging);
			ObjectVO<Shop> lstShopNotInPoGroup = shopMgr.getListShopNotInPoGroup(filter, shopId);
			if (lstShopNotInPoGroup != null) {
				result.put("total", lstShopNotInPoGroup.getkPaging().getTotalRows());
				result.put("rows", lstShopNotInPoGroup.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<Shop>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
public String addPOAutoGroupShopMap(){
	    
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
//				poAutoGroup = poMgr.getPoAutoGroupById(id);
//			    if(poAutoGroup != null){
			    	if(listShopId != null && listShopId.size() > 0){
			    		for(int i = 0 ; i< listShopId.size() ; i++){
			    			PoAutoGroupShopMap poAutoGroupShopMap = new PoAutoGroupShopMap();
			    			poAutoGroupShopMap.setShop(shopMgr.getShopById(Long.parseLong(listShopId.get(i))));
			    			poAutoGroupShopMap.setStatus(ActiveType.RUNNING);
			    			poAutoGroupShopMap.setCreateUser(currentUser.getUserName());
			    			poAutoGroupShopMap.setCreateDate(DateUtil.now());
			    			poMgr.createPoAutoGroupShopMap(poAutoGroupShopMap, getLogInfoVO());
			    		}
			    		error = false;
			    	}
//			    }
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}		
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}

	/**
	 * Search.
	 *
	 * @return the string
	 * @author liemtpt
	 * @since Aug 14, 2013
	 */
	public String searchGroupProduct() {

		result.put("page", page);
	    result.put("rows", rows);
		try {
			KPaging<PoAutoGroupVO> kPaging = new KPaging<PoAutoGroupVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			PoAutoGroupFilter filter = new PoAutoGroupFilter();
			filter.setPoAutoGroupShopMapId(id);
			//sontt comment status di, vi status chi dung cho tim kiem thang nha phan phoi o phia tren thoi. 
			//filter.setStatus(ApprovalStatus.parseValue(Integer.valueOf(status)));
			
			ObjectVO<PoAutoGroupVO> lstPoGroup = poMgr.getPoAutoGroupVO(filter, kPaging);
			if (lstPoGroup != null) {
				result.put("total", lstPoGroup.getkPaging().getTotalRows());
				result.put("rows", lstPoGroup.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<PoAutoGroupVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}

	public String searchListProduct() {

		result.put("page", page);
	    result.put("rows", rows);
		try {
			KPaging<PoAutoGroupDetailVO> kPaging = new KPaging<PoAutoGroupDetailVO>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			PoAutoGroupDetailFilter filter = new PoAutoGroupDetailFilter();
			if(idGroup != null && idGroup > 0){
				filter.setPoAutoGroupId(idGroup);
			}
			if(objectType!=null){
				filter.setObjectType(objectType);
			}
			
			ObjectVO<PoAutoGroupDetailVO> lstPoGroupDetail = poMgr.getListPoAutoGroupDetailVO(filter, kPaging);
			if (lstPoGroupDetail != null) {
				result.put("total", lstPoGroupDetail.getkPaging().getTotalRows());
				result.put("rows", lstPoGroupDetail.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<PoAutoGroupDetailVO>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	
	public String searchListSector(){
		result.put("page", page);
	    result.put("rows", rows);
	    
		try {
			
			KPaging<ProductInfoVOEx> kPaging = new KPaging<ProductInfoVOEx>();
			kPaging.setPageSize(rows);
			kPaging.setPage(page - 1);
			PoAutoGroupDetailFilter filter = new PoAutoGroupDetailFilter();
			boolean sector = false;

			if(idGroup != null && idGroup > 0){
				filter.setPoAutoGroupId(idGroup);
				poAutoGroupId = Integer.valueOf(idGroup.intValue());
			}
			if(objectType!=null && objectType == 1){
				filter.setObjectType(objectType);
				sector = true;
			}
			if(objectType!=null && objectType == 0){
				filter.setObjectType(objectType);
				sector = false;
			}
			ObjectVO<ProductInfoVOEx> lstPoGroupDetail = poMgr.getListProductInfoCanBeAddedNPP(1, name, code, sector, kPaging);
			if (lstPoGroupDetail != null) {
				result.put("total", lstPoGroupDetail.getkPaging().getTotalRows());
				result.put("rows", lstPoGroupDetail.getLstObject());
			}else{
				result.put("total", 0);
				result.put("rows", new ArrayList<ProductInfoVOEx>());
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return JSON;
	}
	public String deleteProduct() {

		resetToken(result);
		boolean error = true;
		if (currentUser != null) {
			try {
				if (groupDetailId != null && groupDetailId > 0) {
					poAutoGroupDetail = null;
					poAutoGroupDetail = poMgr.getPoAutoGroupDetailById(groupDetailId);
					if (poAutoGroupDetail != null) {
						//poAutoGroupDetail.setStatus(ActiveType.DELETED);
						poMgr.deletePoAutoGroupDetail(poAutoGroupDetail);
						error = false;
					}
				}
			} catch (Exception e) {
				LogUtility.logError(e, e.getMessage());
			}
		}
		result.put(ERROR, error);
		return JSON;
	}
	
public String saveOrUpdate(){
	    
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    List<String> lst = new ArrayList<String>() ;
		if (listOjectId != null) {
			for(int j = 0 ; j< listOjectId.size() ; j ++){
				lst.add(listOjectId.get(j));
			}
		}
	    if(currentUser!= null){
			try{
				poAutoGroup = poMgr.getPoAutoGroupById(id);
			    if(poAutoGroup != null){
			    	if(lst != null && lst.size() > 0){
			    		for(int i = 0 ; i< lst.size() ; i++){
			    			ProductInfo pf = productInfoMgr.getProductInfoById(Long.parseLong(lst.get(i)));
//			    			if(pf != null){
//			    				ObjectVO<ProductInfo> objectVO = productInfoMgr.getListProductInfo(null, pf.getProductInfoCode(), pf.getProductInfoName(), null, ActiveType.RUNNING, ProductType.PRODUCT, null);
//				    			if(objectVO != null && objectVO.getLstObject().size() >0){
//				    				for(int j = 0;j< objectVO.getLstObject().size();j++){
//						    			poAutoGroupDetail.setObjectId(objectVO.getLstObject().get(j).getId());
//				    				}
//				    			}else{
//					    			poAutoGroupDetail.setObjectId(Long.parseLong(lst.get(i)));
//				    			}
			    				poAutoGroupDetail = new PoAutoGroupDetail();
				    			poAutoGroupDetail.setObjectId(Long.parseLong(lst.get(i)));
				    			
				    			poAutoGroupDetail.setPoAutoGroup(poAutoGroup);
						    	poAutoGroupDetail.setObjectType(PoAutoGroupDetailType.parseValue(objectType));
				    			poAutoGroupDetail.setCreateUser(currentUser.getUserName());
				    			poAutoGroupDetail.setCreateDate(DateUtil.now());
				    			poMgr.createPoAutoGroupDetail(poAutoGroupDetail);
//			    			}
//			    			
//			    			if(pf != null && pf.getType().getValue() == ProductType.PRODUCT.getValue()){
//			    				poAutoGroupDetail = new PoAutoGroupDetail();
//				    			poAutoGroupDetail.setPoAutoGroup(poAutoGroup);
//						    	poAutoGroupDetail.setObjectType(PoAutoGroupDetailType.parseValue(objectType));
//				    			poAutoGroupDetail.setObjectId(Long.parseLong(lst.get(i)));
//				    			poAutoGroupDetail.setCreateUser(currentUser.getUserName());
//				    			poAutoGroupDetail.setCreateDate(DateUtil.now());
//				    			poMgr.createPoAutoGroupDetail(poAutoGroupDetail);
//			    			}
			    		}
			    		error = false;
			    	}
			    }
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}		
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}
	/**
	 * Creates the group po auto.
	 *
	 * @return the string
	 * @author liemtpt
	 * @since Aug 14, 2013
	 */
	public String createGroupPOAuto()
	{
		try
		{
			groupCode = groupCode.toUpperCase();
			PoAutoGroup poAutoGroup = poMgr.getPoAutoGroupByCode(groupCode);
			if (poAutoGroup != null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "grouppoauto.error.duplicate"));
				return JSON;
			}
			poAutoGroup = new PoAutoGroup();
			poAutoGroup.setGroupCode(groupCode);
			poAutoGroup.setGroupName(groupName);
			ActiveType at = ActiveType.parseValue(statusValue);
			poAutoGroup.setStatus(at);
			poAutoGroup.setPoAutoGroupShopMap(poMgr.getPoAutoGroupShopMapById(id));
			poAutoGroup.setCreateUser(currentUser.getUserName());
			poAutoGroup.setCreateDate(DateUtil.now());
			poMgr.createPoAutoGroup(poAutoGroup);
		}
		catch(Exception e)
		{
			 result.put(ERROR, true);	    
			 LogUtility.logError(e, e.getMessage());
			 errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			 result.put("errMsg", errMsg);

		}
		return JSON;
	}
	
	public String updateGroupPOAuto()
	{
		try
		{
			PoAutoGroup poAutoGroup = poMgr.getPoAutoGroupById(id);
			if(poAutoGroup != null)
			{
				boolean flag = false;
				flag = poMgr.isExistsInfoOfPoAutoGroup(poAutoGroup);
				ActiveType at = ActiveType.parseValue(statusValue);
				if(flag == true && at.getValue() == ActiveType.RUNNING.getValue())
				{
					result.put(ERROR, true);
					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_GROUP_PO_AUTO_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.group.po.auto.exitst")));
				}
				else
				{
					poAutoGroup.setGroupName(groupName);
					poAutoGroup.setStatus(at);
					poAutoGroup.setUpdateUser(currentUser.getUserName());
					poAutoGroup.setUpdateDate(DateUtil.now());
					poMgr.updatePoAutoGroup(poAutoGroup);
				}

			}
			else
			{
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				result.put(ERROR, true);
				result.put("errMsg", errMsg);

			}
		}
		catch(Exception e)
		{
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);

		}
		return JSON;
	}
	public String updatePoAutoGroupShopMap()
	{
		try
		{
			PoAutoGroupShopMap poAutoGroupShopMap = poMgr.getPoAutoGroupShopMapById(id);
			if(poAutoGroupShopMap != null)
			{
//				boolean flag = false;
//				flag = poMgr.isExistsInfoOfPoAutoGroup(poAutoGroup);
				ActiveType at = ActiveType.parseValue(statusValue);
//				if(flag == true && at.getValue() == ActiveType.RUNNING.getValue())
//				{
//					result.put(ERROR, true);
//					result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_GROUP_PO_AUTO_EXISTS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"catalog.group.po.auto.exitst")));
//				}
//				else
//				{
					poAutoGroupShopMap.setStatus(at);
					poAutoGroupShopMap.setUpdateUser(currentUser.getUserName());
					poAutoGroupShopMap.setUpdateDate(DateUtil.now());
					poMgr.updatePoAutoGroupShopMap(poAutoGroupShopMap, getLogInfoVO());
//				}

			}
			else
			{
				errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
				result.put(ERROR, true);
				result.put("errMsg", errMsg);

			}
		}
		catch(Exception e)
		{
			LogUtility.logError(e, e.getMessage());
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			result.put(ERROR, true);
			result.put("errMsg", errMsg);

		}
		return JSON;
	}
	
public String addPOAutoGroupDetail(){
	    
	    resetToken(result);
	    boolean error = true;
	    String errMsg = "";
	    if(currentUser!= null){
			try{
				poAutoGroup = poMgr.getPoAutoGroupById(id);
			    if(poAutoGroup != null){
			    	if(lstCategoryId != null && lstCategoryId.size() > 0){
			    		for(int i = 0 ; i< lstCategoryId.size() ; i++){
			    			poAutoGroupDetail = new PoAutoGroupDetail();
			    			poAutoGroupDetail.setPoAutoGroup(poAutoGroup);
					    	poAutoGroupDetail.setObjectType(PoAutoGroupDetailType.CAT);
			    			poAutoGroupDetail.setObjectId(Long.parseLong(lstCategoryId.get(i)));
			    			poAutoGroupDetail.setCreateUser(currentUser.getUserName());
			    			poAutoGroupDetail.setStatus(ActiveType.RUNNING);
			    			poAutoGroupDetail.setCreateDate(DateUtil.now());
			    			poMgr.createPoAutoGroupDetail(poAutoGroupDetail);
			    		}
			    		error = false;
			    	}
			    }
			}catch (Exception e) {
			    LogUtility.logError(e, e.getMessage());
			}		
	    }
	    result.put(ERROR, error);
	    if(error && StringUtil.isNullOrEmpty(errMsg)){
	    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
	    }
	    result.put("errMsg", errMsg);	    
	    return JSON;
	}



public String addPOAutoGroupDetailSubCat(){
    
    resetToken(result);
    boolean error = true;
    String errMsg = "";
    if(currentUser!= null){
		try{
			poAutoGroup = poMgr.getPoAutoGroupById(id);
		    if(poAutoGroup != null){
		    	if(lstCategoryId != null && lstCategoryId.size() > 0){
		    		for(int i = 0 ; i< lstCategoryId.size() ; i++){
		    			poAutoGroupDetail = new PoAutoGroupDetail();
		    			poAutoGroupDetail.setPoAutoGroup(poAutoGroup);
				    	poAutoGroupDetail.setObjectType(PoAutoGroupDetailType.SUB_CAT);
		    			poAutoGroupDetail.setObjectId(Long.parseLong(lstCategoryId.get(i)));		
		    			poAutoGroupDetail.setCreateUser(currentUser.getUserName());
		    			poAutoGroupDetail.setStatus(ActiveType.RUNNING);
		    			poAutoGroupDetail.setCreateDate(DateUtil.now());
		    			poMgr.createPoAutoGroupDetail(poAutoGroupDetail);
		    		}
		    		error = false;
		    	}
		    }
		}catch (Exception e) {
		    LogUtility.logError(e, e.getMessage());
		}		
    }
    result.put(ERROR, error);
    if(error && StringUtil.isNullOrEmpty(errMsg)){
    	errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "system.error");
    }
    result.put("errMsg", errMsg);	    
    return JSON;
}
public String deleteCategoryRow() {

	resetToken(result);
	boolean error = true;
	if (currentUser != null) {
		try {
			if (groupDetailId != null && groupDetailId > 0) {
				poAutoGroupDetail = null;
				poAutoGroupDetail = poMgr.getPoAutoGroupDetailById(groupDetailId);
				if (poAutoGroupDetail != null) {
					poMgr.deletePoAutoGroupDetail(poAutoGroupDetail);
					error = false;
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}
	result.put(ERROR, error);
	return JSON;
}
public String deleteSubCategoryRow() {

	resetToken(result);
	boolean error = true;
	if (currentUser != null) {
		try {
			if (groupDetailId != null && groupDetailId > 0) {
				poAutoGroupDetail = null;
				poAutoGroupDetail = poMgr.getPoAutoGroupDetailById(groupDetailId);
				if (poAutoGroupDetail != null) {
					//poAutoGroupDetail.setStatus(ActiveType.DELETED);
					poMgr.deletePoAutoGroupDetail(poAutoGroupDetail);
					error = false;
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}
	result.put(ERROR, error);
	return JSON;
}

}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.action.catalog;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.action.general.AbstractAction;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.DateUtil;
import ths.dms.web.utils.FileUtility;
import ths.dms.web.utils.LogUtility;
import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.business.ProductMgr;
import ths.dms.core.entities.PriceSaleIn;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.filter.PriceFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PriceVO;
import ths.dms.core.memcached.MemcachedUtils;

/**
 * 
 * Quan ly gia sale in
 * 
 * @author trietptm
 * @since Jan 20, 2016
 */
public class SaleInPriceManagerAction extends AbstractAction {
	private static final long serialVersionUID = -2128120975032582873L;

	private ProductMgr productMgr;
	private Long shopId;
	private Integer status;
	private String fromDateStr;
	private String toDateStr;
	private String shopCode;
	private String productCode;
	private String productName;
	private Long id;
	private File excelFile;
	private String excelFileContentType;

	@Override
	public void prepare() throws Exception {
		super.prepare();
		productMgr = (ProductMgr) context.getBean("productMgr");
	}

	@Override
	public String execute() {
		resetToken(result);
		//		try {
		//
		//		} catch (Exception e) {
		//			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SaleInPriceManagerAction.execute()"), createLogErrorStandard(actionStartTime));
		//		}
		return SUCCESS;
	}

	/**
	 * 
	 * Xu ly load dau tien
	 * 
	 * @author trietptm
	 * @return
	 * @since Jan 20, 2016
	 */
	public String infoManager() {
		resetToken(result);
		try {
			if (currentUser.getShopRoot() != null) {
				shopId = currentUser.getShopRoot().getShopId();
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SaleInPriceManagerAction.infoManager()"), createLogErrorStandard(actionStartTime));
		}
		return SUCCESS;
	}

	/**
	 * 
	 * Xu ly tim kiem
	 * 
	 * @author trietptm
	 * @return String
	 * @since Jan 20, 2016
	 */
	public String search() {

		try {
			result.put("total", 0);
			result.put("rows", new ArrayList<PriceVO>());
			PriceFilter<PriceVO> filter = new PriceFilter<PriceVO>();
			KPaging<PriceVO> kPaging = new KPaging<PriceVO>();
			kPaging.setPageSize(max);
			kPaging.setPage(page - 1);
			filter.setkPaging(kPaging);

			//Ma san pham			
			if (!StringUtil.isNullOrEmpty(productCode)) {
				filter.setProductCode(productCode);
			}
			//Ten san pham
			if (!StringUtil.isNullOrEmpty(productName)) {
				filter.setProductName(productName);
			}
			//Ma Don vi
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(shopCode);
			}
			//From date - to date
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			//Status
			if (status != null && status > -1) {
				filter.setStatus(status);
			}

			ObjectVO<PriceVO> data = productMgr.searchPriceSaleInVOByFilter(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				result.put("total", data.getkPaging().getTotalRows());
				result.put("rows", data.getLstObject());
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SaleInPriceManagerAction.search()"), createLogErrorStandard(actionStartTime));
		}
		return JSON;
	}

	/**
	 * 
	 * export excel price sale in
	 * 
	 * @author trietptm
	 * @return String
	 * @since Jan 22, 2016
	 */
	public String exportPriceProduct() {
		InputStream inputStream = null;
		OutputStream os = null;
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			PriceFilter<PriceVO> filter = new PriceFilter<PriceVO>();

			//Ma san pham			
			if (!StringUtil.isNullOrEmpty(productCode)) {
				filter.setProductCode(productCode);
			}
			//Ten san pham
			if (!StringUtil.isNullOrEmpty(productName)) {
				filter.setProductName(productName);
			}
			//Ma Don vi
			if (!StringUtil.isNullOrEmpty(shopCode)) {
				filter.setShopCode(shopCode);
			}
			//From date - to date
			if (!StringUtil.isNullOrEmpty(fromDateStr)) {
				filter.setFromDate(DateUtil.parse(fromDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			if (!StringUtil.isNullOrEmpty(toDateStr)) {
				filter.setToDate(DateUtil.parse(toDateStr, DateUtil.DATE_FORMAT_DDMMYYYY));
			}
			//Status
			if (status != null && status > -1) {
				filter.setStatus(status);
			}

			//Status multi language
			String status_active = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.active");
			String status_stopped = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.status.stopped");
			String status_waitting = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "jsp.common.status.draft");

			ObjectVO<PriceVO> data = productMgr.searchPriceSaleInVOByFilter(filter);
			if (data != null && !data.getLstObject().isEmpty()) {
				Map<String, Object> beans = new HashMap<String, Object>();
				beans.put("lstData", data.getLstObject());
				for (PriceVO priceV : data.getLstObject()) {
					priceV.setStatusStr(status_stopped);
					if (priceV.getStatus() != null && priceV.getStatus() == ActiveType.RUNNING.getValue()) {
						priceV.setStatusStr(status_active);
					} else if (priceV.getStatus() != null && priceV.getStatus() == ActiveType.WAITING.getValue()) {
						priceV.setStatusStr(status_waitting);
					}
				}

				String fileTemplate = ConstantManager.PRICE_SALE_IN_EXPORT_TEMPLATE;
				String fileName = ConstantManager.PRICE_SALE_IN_EXPORT_FILENAME;
				String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathPrice() + fileTemplate;
				templateFileName = templateFileName.replace('/', File.separatorChar);

				String outputName = fileName + genExportFileSuffix() + FileExtension.XLSX.getValue();
				String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);

				inputStream = new BufferedInputStream(new FileInputStream(templateFileName));
				XLSTransformer transformer = new XLSTransformer();
				Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
				os = new BufferedOutputStream(new FileOutputStream(exportFileName));
				resultWorkbook.write(os);
				os.flush();
				String outputPath = Configuration.getExportExcelPath() + outputName;
				result.put(ERROR, false);
				result.put("path", outputPath);
				MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
			} else {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.export.isnull"));
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SaleInPriceManagerAction.exportPriceProduct()"), createLogErrorStandard(actionStartTime));
			result.put(ERROR, true);
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
			if (os != null) {
				IOUtils.closeQuietly(os);
			}
		}
		return JSON;
	}

	/**
	 * 
	 * Xu ly tam ngung gia
	 * 
	 * @author trietptm
	 * @return String
	 * @since Jan 21, 2016
	 */
	public String stopPrice() {
		resetToken(result);
		try {
			if (id == null || id < 0) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.data.invalid"));
				return JSON;
			}
			PriceSaleIn priceSaleIn = productMgr.getPriceSaleInById(id);
			if (priceSaleIn != null && ActiveType.RUNNING.getValue().equals(priceSaleIn.getStatus().getValue())) {
				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(currentUser.getUserName());
				productMgr.stopPriceSaleIn(priceSaleIn, logInfo);
			} else if (priceSaleIn == null) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.in.price.stop.data.not.exist"));
				return JSON;
			}
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SaleInPriceManagerAction.stopPrice()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		result.put(ERROR, false);
		return JSON;
	}

	/**
	 * 
	 * download template import
	 * 
	 * @author trietptm
	 * @return String
	 * @since Jan 22, 2016
	 */
	public String downloadImportPriceTemplateFile() {
		try {
			String reportToken = retrieveReportToken(reportCode);
			if (ths.dms.web.utils.StringUtil.isNullOrEmpty(reportToken)) {
				result.put(ERROR, true);
				result.put("errMsg", Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.invalid.token"));
				return JSON;
			}
			String fileNameTemplate = ConstantManager.PRICE_SALE_IN_IMPORT_TEMPLATE;
			String templateFileName = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getExcelTemplatePathPrice() + fileNameTemplate + FileExtension.XLSX.getValue();
			templateFileName = templateFileName.replace('/', File.separatorChar);
			String outputName = fileNameTemplate + "_" + genExportFileSuffix() + FileExtension.XLSX.getValue();
			String exportFileName = (Configuration.getStoreRealPath() + outputName).replace('/', File.separatorChar);
			FileUtility.copyFile(templateFileName, exportFileName);
			String outputPath = Configuration.getExportExcelPath() + outputName;
			result.put(REPORT_PATH, outputPath);
			result.put(LIST, outputPath);
			result.put(ERROR, false);
			result.put("hasData", true);
			MemcachedUtils.putValueToMemcached(reportToken, outputPath, retrieveReportMemcachedTimeout());
		} catch (Exception e) {
			result.put(ERROR, true);
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SaleInPriceManagerAction.downloadImportPriceTemplateFile()"), createLogErrorStandard(actionStartTime));
			result.put("errMsg", ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM));
			return JSON;
		}
		return JSON;
	}

	/**
	 * 
	 * Import gia sale in
	 * 
	 * @author trietptm
	 * @return String
	 * @since Jan 22, 2016
	 */
	public String importPriceProduct() {
		resetToken(result);
		totalItem = 0;
		String message = "";
		String value = "";
		try {
			int NUM_COLUMN = 5;
			List<List<String>> lstData = getExcelData(excelFile, excelFileContentType, errMsg, NUM_COLUMN);
			if (StringUtil.isNullOrEmpty(errMsg) && lstData != null && lstData.size() > 0) {
				lstView = new ArrayList<CellBean>();
				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(currentUser.getUserName());
				logInfo.setStartDate(commonMgr.getSysDate());
				for (int i = 0, sz = lstData.size(); i < sz; i++) {
					Shop shop = null;
					Product product = null;
					PriceSaleIn priceInsUpd = null;
					List<String> row = lstData.get(i);
					if (row != null) {
						priceInsUpd = new PriceSaleIn();
						status = -1;
						message = "";
						totalItem++;

						//Check Ma Don Vi
						value = row.get(0).trim();
						if (StringUtil.isNullOrEmpty(value)) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.isShop.isUnDefined");
							message += "\n";
						} else {
							shop = shopMgr.getShopByCode(value);
							if (shop != null && shop.getId() > 0) {
								if (shop.getType() != null && (shop.getType().getSpecificType() == null || !ShopSpecificType.NPP.getValue().equals(shop.getType().getSpecificType().getValue()))) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.cms.shop.islevel5.undefined");
									message += "\n";
								} else if (!ActiveType.RUNNING.equals(shop.getStatus())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.isShop.isStop");
									message += "\n";
								} else {
									priceInsUpd.setShop(shop);
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.isShop.isUnDefined");
								message += "\n";
							}
						}

						//Check Ma San Pham
						value = row.get(1).trim();
						if (StringUtil.isNullOrEmpty(value)) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.product.isnull");
							message += "\n";
						} else {
							product = productMgr.getProductByCode(value);
							if (product != null && product.getId() > 0) {
								if (!ActiveType.RUNNING.equals(product.getStatus())) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.product.isStop");
									message += "\n";
								} else {
									priceInsUpd.setProduct(product);
								}
							} else {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.product.isundefined");
								message += "\n";
							}
						}

						if (StringUtil.isNullOrEmpty(row.get(2).trim()) && StringUtil.isNullOrEmpty(row.get(3).trim())) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.in.price.err.imp.package.retail.is.null");
							message += "\n";
						} else {
							//Check Gia thung VAT
							value = row.get(2).trim();
							if (StringUtil.isNullOrEmpty(value)) {
								priceInsUpd.setPackagePrice(BigDecimal.ZERO);
							} else {
								value = value.replaceAll(",", "").trim();
								if (value.length() > 22 || !StringUtil.isNumberInt(value)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.in.price.err.imp.price.package.isOk");
									message += "\n";
								} else {
									BigDecimal packagePrice = new BigDecimal(value);
									if (packagePrice.compareTo(BigDecimal.ZERO) < 0) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.in.price.err.imp.price.package.isOk");
										message += "\n";
									} else {
										priceInsUpd.setPackagePrice(packagePrice);
									}
								}
							}

							//Check Gia Le VAT
							value = row.get(3).trim();
							if (StringUtil.isNullOrEmpty(value)) {
								priceInsUpd.setRetailPrice(BigDecimal.ZERO);
							} else {
								value = value.replaceAll(",", "").trim();
								if (value.length() > 22 || !StringUtil.isNumberInt(value)) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.in.price.err.imp.price.isOk");
									message += "\n";
								} else {
									BigDecimal price = new BigDecimal(value);
									if (price.compareTo(BigDecimal.ZERO) < 0) {
										message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "sale.in.price.err.imp.price.isOk");
										message += "\n";
									} else {
										priceInsUpd.setRetailPrice(price);
									}
								}
							}
						}

						//Check Phan Tram VAT
						value = row.get(4).trim();
						if (StringUtil.isNullOrEmpty(value)) {
							message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.vat.isNull");
							message += "\n";
						} else {
							if (!StringUtil.isFloat(value)) {
								message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.vat.isOk");
								message += "\n";
							} else {
								Float vat = Float.valueOf(value);
								if (vat < 0 || vat > 100) {
									message += Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "price.manage.err.imp.vat.isOk");
									message += "\n";
								}
								priceInsUpd.setVat(StringUtil.precisionFloat(2, vat));
							}
						}

						//Xy ly ghi du lieu DB
						if (StringUtil.isNullOrEmpty(message.trim())) {
							calculatPriceNotVAT(priceInsUpd);
							priceInsUpd.setFromDate(logInfo.getStartDate());
							priceInsUpd.setStatus(ActiveType.RUNNING);
							productMgr.createAndUpdateOldPriceSaleIn(priceInsUpd, logInfo);
						} else {
							lstView.add(StringUtil.addFailBean(row, message));
						}
					}
				}
				if (lstView != null && lstView.size() > 0) {
					//Export error
					getOutputFailExcelFile(lstView, ConstantManager.PRICE_SALE_IN_IMPORT_FAIL_TEMPLATE);
				}
			} else {
				isError = true;
				errMsg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "not.data.excel");
			}
		} catch (Exception e) {
			LogUtility.logErrorStandard(e, R.getResource("web.log.message.error", "ths.dms.action.catalog.SaleInPriceManagerAction.importPriceProduct()"), createLogErrorStandard(actionStartTime));
			errMsg = ValidateUtil.getErrorMsg(ConstantManager.ERR_SYSTEM);
			isError = true;
		}
		if (StringUtil.isNullOrEmpty(errMsg)) {
			isError = false;
		}
		return SUCCESS;
	}

	private void calculatPriceNotVAT(PriceSaleIn priceInsUpd) {
		//Gia dong goi chua vat
		Float pricePackageNotVat = priceInsUpd.getPackagePrice().floatValue() * Float.valueOf(100);
		if (pricePackageNotVat > 0) {
			pricePackageNotVat = pricePackageNotVat / Float.valueOf(Float.valueOf(100) + priceInsUpd.getVat());
			priceInsUpd.setPackagePriceNotVat(StringUtil.precisionBigDecimal(0, BigDecimal.valueOf(pricePackageNotVat)));
		} else {
			priceInsUpd.setPackagePrice(BigDecimal.ZERO);
			priceInsUpd.setPackagePriceNotVat(BigDecimal.ZERO);
		}
		//Gia le chua vat
		Float priceNotVat = priceInsUpd.getRetailPrice().floatValue() * Float.valueOf(100);
		if (priceNotVat > 0) {
			priceNotVat = priceNotVat / Float.valueOf(Float.valueOf(100) + priceInsUpd.getVat());
			priceInsUpd.setRetailPriceNotVat(StringUtil.precisionBigDecimal(0, BigDecimal.valueOf(priceNotVat)));
		} else {
			priceInsUpd.setRetailPrice(BigDecimal.ZERO);
			priceInsUpd.setRetailPriceNotVat(BigDecimal.ZERO);
		}
	}

	////////GETTER/SETTER

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFromDateStr() {
		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}
}

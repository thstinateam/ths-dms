package ths.dms.helper;

//import ths.dms.core.entities.enumtype.FunctionType;
// TODO: Auto-generated Javadoc

/**
 * The Class LogInfo.
 *
 * @author tientv11
 * @since 27/03/2014
 * Doi tuong ghi log ung dung
 */
public class LogInfo {
	
	/** The app code. */
	private String appCode; /** Ma ung dung. */
	
	private String ipClientAddress;	/** Ip cua nguoi dung. */
	
	private String startTime;		/** Thoi gian bat dau. */
	
	private String endTime;		/** Thoi gian ket thuc. */
	
	private String loginUserCode;		/** Username dang nhap he thong. */
	
	private String functionCode;
	
	/** The function type. */
	//private FunctionType functionType;		/** loai tac dong. */	
	
	private Long objectId;		/** Id cua doi tuong tac dong. */
	
	private String description; /**
  * Thong tin chi tiet loi.
  *
  * @return the app code
  */

	public String getAppCode() {
		return appCode;
	}

	/**
	 * Sets the app code.
	 *
	 * @param appCode the new app code
	 */
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	/**
	 * Gets the ip client address.
	 *
	 * @return the ip client address
	 */
	public String getIpClientAddress() {
		return ipClientAddress;
	}

	/**
	 * Sets the ip client address.
	 *
	 * @param ipClientAddress the new ip client address
	 */
	public void setIpClientAddress(String ipClientAddress) {
		this.ipClientAddress = ipClientAddress;
	}

	/**
	 * Gets the start time.
	 *
	 * @return the start time
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 *
	 * @param startTime the new start time
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 *
	 * @return the end time
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 *
	 * @param endTime the new end time
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * Gets the login user code.
	 *
	 * @return the login user code
	 */
	public String getLoginUserCode() {
		return loginUserCode;
	}

	/**
	 * Sets the login user code.
	 *
	 * @param loginUserCode the new login user code
	 */
	public void setLoginUserCode(String loginUserCode) {
		this.loginUserCode = loginUserCode;
	}

	/**
	 * Gets the function code.
	 *
	 * @return the function code
	 */
	public String getFunctionCode() {
		return functionCode;
	}

	/**
	 * Sets the function code.
	 *
	 * @param functionCode the new function code
	 */
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}

	/**
	 * Gets the function type.
	 *
	 * @return the function type
	 */
	/*public FunctionType getFunctionType() {
		return functionType;
	}

	*//**
	 * Sets the function type.
	 *
	 * @param functionType the new function type
	 *//*
	public void setFunctionType(FunctionType functionType) {
		this.functionType = functionType;
	}*/

	/**
	 * Gets the object id.
	 *
	 * @return the object id
	 */
	public Long getObjectId() {
		return objectId;
	}

	/**
	 * Sets the object id.
	 *
	 * @param objectId the new object id
	 */
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		if(description==null){
			description = "N/A";
		}
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
	
	
	
}

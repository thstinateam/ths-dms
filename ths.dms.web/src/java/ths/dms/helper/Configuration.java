package ths.dms.helper;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;


/**
 * The Class Configuration.
 */
public class Configuration {

	public static final int FILE_VERSION_NUMBER = AppSetting
			.getIntegerValue("svn-revision-number");

	public Configuration() {
	}
	
	/**
	 *  Lay ma host domain
	 * 
	 * @author hunglm16
	 * @return
	 * @since August 27,2015
	 */
	public static String getHostDomain() {
		return AppSetting.getStringValue("hotsdomain");
	}
	
	/**
	 * Gets the img server path.
	 * 
	 * @return the Application
	 */
	public static String getDomainCode() {
		return AppSetting.getStringValue("domainCode");
	}

	/**
	 * Gets the resource bundle.
	 * 
	 * @param language
	 *            the language
	 * @return the resource bundle
	 */
	public static ResourceBundle getResourceBundle(String language) {
		return ResourceBundle.getBundle(AppSetting
				.getStringValue("resource-bundle"), new Locale(language));
	}
	
	

	/**
	 * Gets the resource string.
	 * 
	 * @param language
	 *            the language
	 * @param key
	 *            the key
	 * @param params
	 *            the params
	 * @return the resource string
	 */
	public static String getResourceString(String language, String key,
			Object... params) {
		HttpServletRequest request = null;
		request = ServletActionContext.getRequest();		
		if(request!=null){
			HttpSession session = request.getSession(true);
			if(session.getAttribute("lang")!=null){
				language = (String)session.getAttribute("lang");
			}
		}
		ResourceBundle rs = getResourceBundle(language);
		String text = rs.getString(key);
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				if (params[i] != null) {
					text = text.replace("{" + i + "}", params[i].toString());
				}
			}
		}

		return text;
	}
	
	public static String getResourceString(String language, Boolean hasSuffix, String key,
			Object... params) {
		ResourceBundle rs = getResourceBundle(language);
		String text = rs.getString(key);
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				if (params[i] != null) {
					text = text.replace("{" + i + "}", params[i].toString());
				}
			}
		}

		if(hasSuffix!= null && hasSuffix){
			text += "\n";
		}
		return text;
	}

	/**
	 * Gets the css server path.
	 * 
	 * @return the css server path
	 */
	public static String getCssServerPath() {
		return AppSetting.getStringValue("css-server-path");
	}

	/**
	 * Gets the css server path.
	 * 
	 * @param cssFile
	 *            the css file
	 * @return the css server path
	 */
	public static String getCssServerPath(String cssFile) {
		return AppSetting.getStringValue("css-server-path") + cssFile + "?v="
				+ FILE_VERSION_NUMBER;
	}
	
	/**
	 * Gets the img server path.
	 * 
	 * @return the img server path
	 */
	public static String getImgServerDocPath() {
		return AppSetting.getStringValue("img-server-doc-path");
	}
	
	/**
	 * Gets the static server path.
	 * 
	 * @param staticFile
	 *            the static file
	 * @return the static server path
	 */
	public static String getImgServerDocPath(String staticFile) {
		return AppSetting.getStringValue("img-server-doc-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER; 
	}
	

	/**
	 * Gets the img server path.
	 * 
	 * @return the img server path
	 */
	public static String getImgServerPath() {
		return AppSetting.getStringValue("img-server-path");
	}
	
	/**
	 * Gets the static server path.
	 * 
	 * @param staticFile
	 *            the static file
	 * @return the static server path
	 */
	public static String getImgServerPath(String staticFile) {
		return AppSetting.getStringValue("img-server-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER; 
	}
	
	/**
	 * Gets the img server path.
	 * 
	 * @return the img server path
	 */
	public static String getImgServerSOPath() {
		return AppSetting.getStringValue("img-server-so-path");
	}
	
	
	
	/**
	 * Gets the static server path.
	 * 
	 * @param staticFile
	 *            the static file
	 * @return the static server path
	 */
	public static String getImgServerSOPath(String staticFile) {
		return AppSetting.getStringValue("img-server-so-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER; 
	}
	
	/**
	 * Gets the img server path.
	 * 
	 * @return the img server path
	 */
	public static String getImgServerProductPath() {
		return AppSetting.getStringValue("img-server-product-path");
	}
	
	
	
	/**
	 * Gets the static server path.
	 * 
	 * @param staticFile
	 *            the static file
	 * @return the static server path
	 */
	public static String getImgServerProductPath(String staticFile) {
		return AppSetting.getStringValue("img-server-product-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER; 
	}

	/**
	 * Gets the static server path.
	 * 
	 * @return the static server path
	 */
	public static String getStaticServerPath() {
		return AppSetting.getStringValue("static-server-path");
	}

	/**
	 * Gets the static server path.
	 * 
	 * @param staticFile
	 *            the static file
	 * @return the static server path
	 */
	public static String getStaticServerPath(String staticFile) {
		return AppSetting.getStringValue("static-server-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER;
	}

	/**
	 * Gets the static avatar server path.
	 * 
	 * @return the static avatar server path
	 */
	public static String getStaticAvatarServerPath() {
		return AppSetting.getStringValue("static-avatar-server-path");
	}

	/**
	 * Gets the client app name.
	 * 
	 * @return the client app name
	 */
	public static String getClientAppName() {
		return AppSetting.getStringValue("app-name");
	}

	/**
	 * Gets the vBD key api.
	 * 
	 * @return the vBD key api
	 */
	public static String getVBDKeyApi() {
		return AppSetting.getStringValue("vbd-key-api");
	}

	/**
	 * Gets the max image upload size byte.
	 * 
	 * @return the max file upload size byte
	 */
	public static long getMaxFileUploadSizeByte() {
		return AppSetting.getLongValue("max-file-upload-size-byte");
	}

	/**
	 * get store static path to upload file
	 * 
	 * @return store static path
	 */
	public static String getStoreStaticPath() {
		return AppSetting.getStringValue("image-real-path");
	}

	/**
	 * Gets the core server path.
	 *
	 * @return the core server path
	 */
	public static String getCoreServerPath() {
		return AppSetting.getStringValue("core-server-path");
	}
	
	/**
	 * get pdf image to upload file
	 * @return convert pdf to image
	 * */
	public static String getPdfImagePath() {
		return AppSetting.getStringValue("pdf-real-path");
	}
	/**
	 * Gets the excel template path.
	 *
	 * @return the excel template path
	 */
	public static String getExcelTemplatePath() {
		return AppSetting.getStringValue("excel-template-path");
	}
	
	/**
	 * Gets the excel file supprt.
	 *
	 * @return the excel file supprt
	 */
	public static String getExcelFileSupprt(){
	    return AppSetting.getStringValue("excel-file-support");
	}
	
	/**
	 * Gets the excel header file supprt.
	 *
	 * @return the excel header file supprt
	 */
	public static String getExcelHeaderFileSupprt(){
	    return AppSetting.getStringValue("excel-header-file-support");
	}
	
	/**
	 * Gets the excel upload max size byte.
	 *
	 * @return the excel upload max size byte
	 */
	public static long getExcelUploadMaxSizeByte(){
	    return AppSetting.getLongValue("excel-upload-max-size-byte");
	}
	
	public static String getExcelTemplatePathCatalog() {
		return AppSetting.getStringValue("excel-template-path-catalog");
	}
	
	public static String getExcelTemplatePathPrice() {
		return AppSetting.getStringValue("excel-template-path-price");
	}
	
	public static String getExcelTemplatePathProduct() {
		return AppSetting.getStringValue("excel-template-path-product");
	}
	
	public static String getExcelTemplatePathSalePlan() {
		return AppSetting.getStringValue("excel-template-path-sale-plan");
	}
	
	public static String getExcelTemplatePathSuperviseCustomer() {
		return AppSetting.getStringValue("excel-template-path-supervise-customer");
	}	
	
	public static String getExcelTemplatePathSaleMTReport() {
		return AppSetting.getStringValue("excel-template-path-salemt-report");
	}	
	
	public static String getExcelTemplatePathSuperviseShop() {
		return AppSetting.getStringValue("excel-template-path-supervise-shop");
	}
	
	public static String getExcelTemplatePathKeyShop() {
		return AppSetting.getStringValue("excel-template-path-key-shop");
	}
	
	/**
	 * Gets the fail data path.
	 *
	 * @return the fail data path
	 */
	public static String getFailDataPath() {
		return AppSetting.getStringValue("fail-data-path");
	}
	
	/**
	 * Gets the store real path.
	 *
	 * @return the store real path
	 */
	public static String getStoreRealPath() {
		return AppSetting.getStringValue("store-real-path");
	}
	
	/**
	 * Gets the store real path.
	 *
	 * @return the PO Auto real path
	 */
	public static String getPORealPath() {
		return AppSetting.getStringValue("xml-real-path");
	}
	
	/**
	 * Gets the excel template path po.
	 * 
	 * @return the excel template path po
	 */
	public static String getExcelTemplatePathPO() {
		return AppSetting.getStringValue("excel-template-path-po");
	}
	
	/**
	 * Gets the export excel path.
	 *
	 * @return the export excel path
	 */
	public static String getExportExcelPath() {
		return AppSetting.getStringValue("export-excel-path");
	}
	public static String getPayrollPath(){
		return AppSetting.getStringValue("payroll-data-path");
	}
	
	/**
	 * Gets the excel template path stock.
	 * 
	 * @return the excel template path stock
	 */
	public static String getExcelTemplatePathStock() {
		return AppSetting.getStringValue("excel-template-path-stock");
	}
	public static String getExcelTemplatePathDPPayeriod(){
		return AppSetting.getStringValue("excel-template-path-dppayperiod");
	}
	public static String getExcelTemplatePathCustomerDebit(){
		return AppSetting.getStringValue("excel-template-path-customer-debit");
	}
	public static String getExcelTemplatePathShopDebit(){
		return AppSetting.getStringValue("excel-template-path-shop-debit");
	}
	public static String getImageRealPath() {
		return AppSetting.getStringValue("image-real-path");
	}
	
	public static String getImageRealPath(String staticFile) {
		return AppSetting.getStringValue("image-real-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER; 
	}
	
	public static String getImageRealProdcutPath() {
		return AppSetting.getStringValue("image-real-product-path");
	}
	
	public static String getImageRealProductPath(String staticFile) {
		return AppSetting.getStringValue("image-real-product-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER; 
	}
	
	public static String getImageRealDocPath() {
		return AppSetting.getStringValue("image-real-doc-path");
	}
	
	public static String getImageRealDocPath(String staticFile) {
		return AppSetting.getStringValue("image-real-doc-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER; 
	}
	
	public static String getImageRealSOPath() {
		return AppSetting.getStringValue("image-real-so-path");
	}
	
	public static String getImageRealSOPath(String staticFile) {
		return AppSetting.getStringValue("image-real-so-path") + staticFile
				+ "?v=" + FILE_VERSION_NUMBER; 
	}
	
	public static int getMaxImageUploadSize() {
        return AppSetting.getIntegerValue("max-image-upload-size");
    }
	
	public static String getImageUploadFileSupport() {
        return AppSetting.getStringValue("image-upload-file-support");
    }
	
	public static String getVideoUploadFileSupport() {
        return AppSetting.getStringValue("video-upload-file-support");
    }
	
	public static String getGoogleMapsKey(){
		return AppSetting.getStringValue("google-map-key");
	}
	
	/**
	 * Gets the excel template path sale-product.
	 * 
	 * @return the excel template path sale-product
	 */
	public static String getExcelTemplatePathSaleProduct(){
		return AppSetting.getStringValue("excel-template-path-sale-product");
	}
	
	public static String getVietbandoKey(){
		return AppSetting.getStringValue("vietbando-key");
	}
	public static String getViettelMapKey() {
		return AppSetting.getStringValue("viettelmap-key");
	}
	public static int getMaxNodeOnTree() {
        return AppSetting.getIntegerValue("max-node-on-tree");
    }
	public static boolean getSavePermission() {
        return AppSetting.getBooleanValue("save-permission");
    }
	
	public static String getExcelTemplatePathPayRoll() {
		return AppSetting.getStringValue("excel-template-path-payroll");
	}
	
	public static long getMaxNumberCellReport() {
		return AppSetting.getLongValue("max-number-cell-report");
	}
	
	/**
	 * Gets the excel template path po.
	 * 
	 * @return the excel template path po
	 */
	public static String getVatTemplatePath() {
		return AppSetting.getStringValue("vat-data-path");
	}
	// luu file report zip len server
	public static String getReportTemplatePath() {
		return AppSetting.getStringValue("report-server-zip-path");
	}
	public static String getReportRealPath() {
		return AppSetting.getStringValue("report-real-zip-path");
	}
	
	public static String getPathSaleMTSync() {
		return AppSetting.getStringValue("path-salemt-sync");
	}
	
	public static String getRealImageDocumentPath() {
		return AppSetting.getStringValue("image-real-document-path");
	}
	public static String getRealImageDocumentPath(String fileName) {
		return AppSetting.getStringValue("image-real-document-path") + fileName;
	}
	public static String getServerImageDocumentPath() {
		return AppSetting.getStringValue("image-server-document-path");
	}
	public static String getServerImageDocumentPath(String fileName) {
		return AppSetting.getStringValue("image-server-document-path") + fileName
		+ "?v=" + FILE_VERSION_NUMBER;
	}
	
	public static String getRealVideoPath() {
		return AppSetting.getStringValue("image-real-video-path");
	}
	public static String getRealVideoPath(String fileName) {
		return AppSetting.getStringValue("image-real-video-path") + fileName;
	}
	public static String getServerVideoPath() {
		return AppSetting.getStringValue("image-server-video-path");
	}
	public static String getServerVideoPath(String fileName) {
		return AppSetting.getStringValue("image-server-video-path") + fileName
		+ "?v=" + FILE_VERSION_NUMBER;
	}
	public static String getStoreImportDownloadPath() {
		return AppSetting.getStringValue("store-import-download-path");
	}
	public static String getStoreImportFailDownloadPath() {
		return AppSetting.getStringValue("store-import-fail-download-path");
	}
	
	/**
	 * So ngay mac dinh cho khoang tu ngay den ngay ngan
	 * @author hunglm16
	 * @return (int) gia tri mac dinh cua so ngay xuat bao cao
	 * @since 19/10/2015
	 */
	public static String getNumberDayShortDefaultReport() {
		return AppSetting.getStringValue("number-day-short-default-report");
	}
	
	/**
	 * So ngay mac dinh cho khoang tu ngay den ngay trung binh
	 * @author hunglm16
	 * @return (int) gia tri mac dinh cua so ngay xuat bao cao
	 * @since 12/11/2015
	 */
	public static String getNumberDayMediumDefaultReport() {
		return AppSetting.getStringValue("number-day-medium-default-report");
	}
	
	/**
	 * So ngay mac dinh cho khoang tu ngay den ngay dai
	 * @author hunglm16
	 * @return (int) gia tri mac dinh cua so ngay xuat bao cao
	 * @since 12/11/2015
	 */
	public static String getNumberDayLongDefaultReport() {
		return AppSetting.getStringValue("number-day-long-default-report");
	}

	/**
	 * Duong dan tro vao thu muc bieu mau bao cao HO
	 * @author hunglm16
	 * @return (String) duong dan tro vao thu muc bieu mau bao cao HO
	 * @since 19/10/2015
	 */
	public static String getReportHoTemplatePath() {
		return AppSetting.getStringValue("report-ho-template-path");
	}
	
	/**
	 * Duong dan tro vao thu muc bieu mau bao cao GS
	 * @author hunglm16
	 * @return (String) duong dan tro vao thu muc bieu mau bao cao HO
	 * @since 19/10/2015
	 */
	public static String getReportSupperivseTemplatePath() {
		return AppSetting.getStringValue("report-supervise-template-path");
	}
	
	/**
	 * Xu ly getUploadFileParamPath
	 * @author vuongmq
	 * @return String
	 * @since Nov 26, 2015
	 */
	public static String getUploadFileParamPath() {
		return AppSetting.getStringValue("upload-file-param-support");
	}
	
	/**
	 * 
	 * Xu ly lay duong dan file import
	 * @author tamvnm
	 * @return duong dan file import
	 * @since Jan 20, 2016
	 */
	public static String getStoreImportRealPath() {
		return AppSetting.getStringValue("store-import-real-path");
	}
	
	/**
	 * 
	 * Xu ly lay duong dan file import
	 * @author tamvnm
	 * @return duong dan file import
	 * @since Jan 20, 2016
	 */
	public static String getStoreImportEquipRealPath() {
		return AppSetting.getStringValue("store-import-equip-real-path");
	}
	
	/**
	 * lay tap tin cua bien ban thiet bi
	 * 
	 * @author hunglm16
	 * @return the file equip server doc path
	 */
	public static String getFileEquipServerDocPath() {
		return AppSetting.getStringValue("file-equip-server-doc-path");
	}
	
	/**
	 * lay tap tin cua bien ban thiet bi
	 * 
	 * @author hunglm16
	 * @return the file equip server doc path
	 */
	public static String getPrefixFileEquipServerDocPath() {
		return AppSetting.getStringValue("prefix-file-equip-server-doc-path");
	}
	
	/**
	 * lay hinh anh mac dinh cho tap tin bien ban bao mat
	 * 
	 * @author hunglm16
	 * @return the file equip server doc path
	 */
	public static String getEquipThumbUrlAttachFile() {
		return AppSetting.getStringValue("equip-thumbUrl-attach-file");
	}
	
	public static int getMaxLengthListFileEquipUpload() {
        return AppSetting.getIntegerValue("max-length-list-file-equip-upload");
    }
	
	public static String getEquipUploadFileSupport() {
        return AppSetting.getStringValue("equip-upload-file-support");
    }
	
	public static int getMaxFileEquipUploadSize() {
        return AppSetting.getIntegerValue("max-file-equip-upload-size");
    }
	
	public static String getReportTemplateImportPhysicalPath() {
		return AppSetting.getStringValue("report-template-import-physical-path");
	}
	
	public static String getReportTemplateImportDownloadPath() {
		return AppSetting.getStringValue("report-template-import-download-path");
	}
	
	public static String getExcelTemplatePathEquipment(){
		return AppSetting.getStringValue("excel-template-path-equipment-report");
	}
	
	/**
	 * Lay duong dan template bao cao
	 * 
	 * @author nhutnn
	 * @return duong dan template bao cao
	 * @since 06/10/2015
	 */
	public static String getTemplatePathEquipmentReport() {
		return AppSetting.getStringValue("template-path-equip-report");
	}
	
	public static String getEquipmentPrintTemplatePath() {
		return AppSetting.getStringValue("equipment-print-template-path");
	}
	
	public static String getMailSMTPHost() {
		return AppSetting.getStringValue("mail-smtp-host");
	}
	public static String getMailSMTPSocketPort() {
		return AppSetting.getStringValue("mail-smtp-socket-port");
	}
	public static String getMailSMTPSocketClass() {
		return AppSetting.getStringValue("mail-smtp-socket-class");
	}
	public static String getMailSMTPAuth() {
		return AppSetting.getStringValue("mail-smtp-auth");
	}
	public static String getMailSMTPPort() {
		return AppSetting.getStringValue("mail-smtp-port");
	}
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.helper;

import ths.dms.web.constant.ConstantManager;

/**
 * Thuc hien cho cac thao tac lay message_vi
 * @author hunglm16
 * @since October 09,2015
 */
public class R {
	/**
	 * Lay thong tin trong message_vi.properties
	 * @author hunglm16
	 * @param key
	 * @return
	 * @since October 09,2015
	 */
	public static String getResource(String key) {
		try {
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, key);
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * Lay thong tin trong message_vi.properties co chua tham so replace
	 * @author hunglm16
	 * @param key
	 * @param params
	 * @return
	 * @since October 09,2015
	 */
	public static String getResource(String key, Object... params) {
		try {
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, key, params);
		} catch (Exception e) {
			return "";
		}
	}
}

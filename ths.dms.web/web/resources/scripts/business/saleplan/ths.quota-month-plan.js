var QuotaMonthPlan = {
	_xhrSave : null,
	_skuMap:null,
	lstProductId:null,
	lstQty:null,
	_searchMonth:null,
	_mapCheckProduct: new Map(),
	isReset:false,
	_isYearPeriodChange: false,
	params:function(allocate){
		var data = new Object();
		var shopCode = $('#shopCode').val().trim();
		var yearPeriod = $('#yearPeriod').val().trim();
		var numPeriod = $('#numPeriod').combobox('getValue');
		var yearPeriodUnder = $('#yearPeriodUnder').val().trim();
		var numPeriodUnder = $('#numPeriodUnder').combobox('getValue');
		
		var productCode = $('#productId').val().trim();
		var shopCodeUnder = $('#shopCodeUnder').val().trim();
		var categoryCode = '';
		$('#category').each(function(){
		    if(categoryCode.length > 0){
		    	categoryCode+=',';
		    }
		    categoryCode += $(this).val();
		});
		data.categoryCode = categoryCode;
		
		if (allocate != undefined && allocate != null && allocate == true) {
			if (shopCodeUnder.length > 0) {
				data.shopCode = shopCodeUnder;
			}
			if (yearPeriodUnder.length > 0) {
				data.yearPeriod = yearPeriodUnder;
			}
			if(numPeriodUnder.length>0){
				data.numPeriod = numPeriodUnder;
			}
		} else {
			if (shopCode.length > 0) {			
				data.shopCode = shopCode;
			}
			if (yearPeriod.length > 0) {
				data.yearPeriod = yearPeriod;
			}
			if (numPeriod.length > 0) {
				data.numPeriod = numPeriod;
			}
		}
		if (productCode.length > 0) {
			data.productCode = productCode;
		}
		try {
			data.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		return data;
	},	
	searchShopOnDialog : function(callback, arrParam) {
		CommonSearch.openSearchStyle1EasyUIDialog(sale_plan_unit_code, sale_plan_unit_name,
				sale_plan_search_unit, "/sale-plan/quota-month/shop-search", 
				callback, "shopCode","shopName", arrParam);
		return false;
	},
	search:function(){		
		var msg = "";
		$('.ErrorMsgStyle').html('').hide();		
		var msg = Utils.getMessageOfRequireCheck('shopCode',sale_plan_unit_code);
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();			
			return false;
		}		
		var params = QuotaMonthPlan.params();
		params.typeSearch = 1;
		$('#dg').datagrid('load',params);	
		$('#shopCodeUnder').val($('#shopCode').val().trim());
		QuotaMonthPlan.showTotalQuantityAndAmount(params);
		QuotaMonthPlan.isReset = false;
		return false;		
	},	
	execute:function(){
		var msg = "";
		$('.ErrorMsgStyle').html('').hide();		
		var msg = Utils.getMessageOfRequireCheck('shopCodeUnder', sale_plan_unit_allocate);
		var yearPeriodSearch = $('#yearPeriod').val().trim();
		var yearPeriodCreate = $('#yearPeriodUnder').val().trim();
		var numPeriodSearch = $('#numPeriod').combobox('getValue');
		var numPeriodCreate = $('#numPeriodUnder').combobox('getValue');
		
		var title = '';		

		if (yearPeriodCreate != yearPeriodSearch || numPeriodSearch != numPeriodCreate) {
			title = sale_plan_error_search_14;
		} else {
			var shopCodeUnder = $('#shopCodeUnder').val().trim();
			var shopCode = $('#shopCode').val().trim();
			if (shopCodeUnder.length > 0 && shopCode.length > 0 && shopCodeUnder.toUpperCase() != shopCode.toUpperCase()) {
				title = sale_plan_error_search_11;
			}
		}
		var lstProduct = new Array(),lstQuantity = new Array(), lstAmount= new Array();	
		$('.skuItem').each(function(){
			var index = $(this).attr('index');
				var rows = $('#dg').datagrid('getRows')[index];
				if (rows != null) {
					var quantity = $(this).val().trim().replace(/,/g,'');
					var tmpQuantity=getQuantity($(this).val().trim().replace(/,/g,''),rows.convfact);;
					var productcode=rows.productCode;
					var tmpAmount=parseFloat(Number($('#amount'+productcode).val().trim().replace(/,/g,'')));
					var amount = $('#amount'+productcode).val().trim().replace(/,/g,'');
					if (isNaN(quantity)) {
						$(this).focus();
						msg = sale_plan_error_input_01;
						return false;
					}
					if (isNaN(amount)) {
						$('#amount'+index).focus();
						msg = sale_plan_error_input_02;
						return false;
					}
					if (quantity!=null && quantity!='')
					{
						if (amount==null || amount=='')
						{
							lstQuantity.push(tmpQuantity);
							lstAmount.push(0);
							lstProduct.push(rows.productId);
						}
						else
						{
							lstQuantity.push(tmpQuantity);
							lstAmount.push(tmpAmount);
							lstProduct.push(rows.productId);
						}
					}
					else
					{
						if (amount!=null && amount!='')
						{
							lstQuantity.push(0);
							lstAmount.push(tmpAmount);
							lstProduct.push(rows.productId);
						}
					}
				}				
		});		
		if(msg.length==0 && lstProduct.length==0){
			msg = sale_plan_error_search_05;
		}
		if(msg.length > 0){
			$('#mainErr').html(msg).show();
			return false;
		}
		
		var data = QuotaMonthPlan.params(true);
		data.lstProduct = lstProduct;
		data.lstQuantity = lstQuantity;	
		data.lstAmount=lstAmount;
		Utils.addOrSaveData(data, "/sale-plan/quota-month/allocation", QuotaMonthPlan._xhrSave, 'errMsgSearch',function(d){					
		},'loading2',null,null,title);
		return false;
		
	},
	showTotalQuantityAndAmount : function(params) {
		$.ajax({
			type : "POST",
			url : "/sale-plan/quota-month/footer",
			data :($.param(params,true)),
			dataType: "json",
			success : function(data) {
				if(data!=null && data.footer!=null){
					$('#dg').datagrid('reloadFooter',[
					 	{shopCode:"_",spMonth:"_",productName: '<b>'+sale_plan_tongcong+'</b>',quantity: data.footer.quantity + "_" , amount: data.footer.amount + "_"},
					 ]);
				}else{
					$('#dg').datagrid('reloadFooter',[
					     {shopCode:"_",spMonth:"_",productName: '<b>'+sale_plan_tongcong+'</b>',quantity: 0 + "_" , amount: 0 + "_"},
					 ]);
				} 
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#dg').datagrid('reloadFooter',[
				     {shopCode:"_",spMonth:"_",productName: '<b>'+sale_plan_tongcong+'</b>',quantity: 0 + "_" , amount: 0 + "_"},
				 ]);
			}
		});
	},	
	reset:function(){
		$('.ErrorMsgStyle').html('').hide();		
		enableWidthAuthorize('execute');
		enableWidthAuthorize('reset');
		enableWidthAuthorize('btnImportExcel');
		$('#productId').val('');		
		$('.skuItem').each(function(){
			$(this).val('');
			$(this).attr('old-quantity','');
			$('#amount'+$(this).attr('id')).html('0');
			$('#amount'+$(this).attr('id')).val('');
			$('#amount'+$(this).attr('id')).attr('old-amount','');
			$(this).attr('reset',1);
		});
		QuotaMonthPlan.isReset = true;
		$('#footerQuantity').html("0");
		$('#footerAmount').html("0");
		return false;
	},	
	/**
	 * download file template import gia san pham
	 * @author longnh15
	 * @since 06/05/2015
	 */
	downloadImportSalePlanTemplateFile: function() {
		var url = "/sale-plan/quota-month/downloadTemplateExcelfile";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
	qtyFocused:function(selector){
		$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		var quantityAdd = 0;
		var amount = 0;
		if(rows!=null){								
			quantity = $(selector).val().trim().replace(/,/g, '');
			if(!isNaN(quantity) && quantity != ''){				
				quantityAdd = parseInt(quantity);
				CreateSalePlan._quantityPlan -= quantityAdd;
				
				amount = rows.price * quantity;
				CreateSalePlan._amountPlan -= amount;
			}
		}
	},
	qtyChanged:function(selector) {
		$('.ErrorMsgStyle').html('').hide();
		var dg_index = $(selector).attr('index');
		var rows = $('#dg').datagrid('getRows')[dg_index];
		if (rows != null) {
			if ($(selector).val() != null && $(selector).val() != '') {
				quantity = getQuantity($(selector).val().trim().replace(/,/g, ''),rows.convfact);
				if(isNaN(quantity) || (parseInt(quantity)<0)){				
					setTimeout(function(){
						$(selector).focus();
					}, 20);
					$('#errMsgSearch').html(sale_plan_error_input_03).show();
					return false;
				}
				var oddQuantity = Number($(selector).attr('old-quantity'));
				if (oddQuantity != quantity) {
					$('#amount' + rows.productCode).val(formatFloatValue(rows.price * quantity,2));
					$(selector).val(formatFloatValue(quantity));
					var oddAmount = $('#amount' + rows.productCode).attr('old-amount');
					oddAmount = oddAmount && oddAmount != 'null' ? Number(oddAmount) : 0;
					var footerQuantity = Number(Utils.returnMoneyValue($('#footerQuantity').html()));
					var footerAmount = Number(Utils.returnMoneyValue($('#footerAmount').html()));
					if (oddQuantity < quantity) {
						footerQuantity += quantity - oddQuantity;
						footerAmount += (rows.price * quantity) - oddAmount;
					} else {
						footerQuantity -= oddQuantity - quantity;
						footerAmount -= oddAmount - (rows.price * quantity);
					}
					$(selector).attr('old-quantity',quantity);
					$('#amount'+rows.productCode).attr('old-amount',Number(rows.price * quantity));
					
					$('#footerQuantity').html(formatCurrency(footerQuantity));
					$('#footerAmount').html(formatFloatValue(footerAmount,2));
				}
			} else {
				var oddQuantity = Number($(selector).attr('old-quantity'));
				var footerQuantity = Number(Utils.returnMoneyValue($('#footerQuantity').html()));
				footerQuantity -= oddQuantity;
				$(selector).attr('old-quantity', '');
				$('#footerQuantity').html(formatCurrency(footerQuantity));
			}
		}
	},
	amtChanged:function(selector){
		$('.ErrorMsgStyle').html('').hide();
		var dg_index = $(selector).attr('index');
		var rows = $('#dg').datagrid('getRows')[dg_index];
		if(rows!=null){								
			var tempAmount=$(selector).val().replace(/,/g, '').trim();
			var amount = Number(tempAmount);
			if(isNaN(amount) || (parseFloat(amount)<0)){				
				setTimeout(function(){
					$(selector).focus();
				}, 2000);
				$('#errMsgSearch').html(sale_plan_error_input_03).show();
				return false;
			}
			if (/^\d*\.?\d{0,6}$/.test(tempAmount)==false) {
				setTimeout(function(){
					$(selector).focus();
				}, 2000);
				$('#errMsgSearch').html(sale_plan_error_input_03).show();
				return false;
			}
			if (tempAmount.trim().length>0)
			{
				$('#amount'+rows.productCode).val(formatFloatValue(amount,2));
			}
			var oddAmount = Number($('#amount'+rows.productCode).attr('old-amount').replace(/,/g, '').trim());
			var footerAmount = Number(Utils.returnMoneyValue($('#footerAmount').html()));
			if(oddAmount<amount){
				footerAmount += amount - oddAmount;
			}else{
				footerAmount -= oddAmount - amount;
			}
			$(selector).attr('old-amount',amount);
			$('#footerAmount').html(formatFloatValue(footerAmount,2));
		}
	},
	checkInputDate:function(MM,yyyy){		
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = (now.getMonth()+1).toString().length>1?(now.getMonth()+1):'0'+(now.getMonth()+1);
		var currentDate = cMonth+'/'+cYear;
		$('.ErrorMsgStyle').html('').hide();		
		var x = MM+'/'+yyyy;
		if (MM != cMonth && Utils.compareMonth(x,currentDate)){
			$('#addSKU').addClass('BtnGeneralDStyle');
			$('#group_insert_execute').addClass('BtnGeneralDStyle');
			$('#reset').addClass('BtnGeneralDStyle');
			$('#btnImportExcel').addClass('BtnGeneralDStyle');
			$('#btnViewExcel').addClass('BtnGeneralDStyle');
			disabled('addSKU');
			disabled('execute');
			disabled('reset');
			disabled('btnImportExcel');
			disabled('btnViewExcel');
			$('.skuItem').each(function(){
				$(this).attr('disabled','disabled');
			});
			$('#mainErr').html(sale_plan_error_search_03).show();
			return false;
		}else{			
			$('#addSKU').removeClass('BtnGeneralDStyle');
			$('#group_insert_execute').removeClass('BtnGeneralDStyle');
			$('#reset').removeClass('BtnGeneralDStyle');
			$('#btnImportExcel').removeClass('BtnGeneralDStyle');
			$('#btnViewExcel').removeClass('BtnGeneralDStyle');
			enableWidthAuthorize('addSKU');
			enableWidthAuthorize('execute');
			enableWidthAuthorize('reset');
			enableWidthAuthorize('btnImportExcel');
			enableWidthAuthorize('btnViewExcel');
			$('.skuItem').each(function(){
				$(this).removeAttr('disabled');
			});
			$('#errMsgSearch').html('').hide();
			return true;
		}
	},
	exportExcel:function(){
		var msg = "";
		$('.ErrorMsgStyle').html('').hide();		
		var msg = Utils.getMessageOfRequireCheck('shopCode',sale_plan_unit_code);
		if(msg.length > 0){
			$('#mainErr').html(msg).show();			
			return false;
		}		
		var kData = $.param(QuotaMonthPlan.params(),true);
		$.messager.confirm(sale_plan_confirm, sale_plan_excel_export, function(r){
			if (r){
				$('#divOverlay').show();		
				StockIssued.xhrExport = $.ajax({
					type : "POST",
					url : "/sale-plan/quota-month/export",
					data :(kData),
					dataType: "json",
					success : function(data) {
						$('#divOverlay').hide();
						StockIssued.xhrExport = null;
						if(data.error!=undefined && data.error!=null && !data.error) {
							var filePath = ReportUtils.buildReportFilePath(data.view);
							window.location.href = filePath;
							setTimeout(function(){ 
			                    CommonSearch.deleteFileExcelExport(data.view);
							},2000);
						} else {
							$('#mainErr').html(data.errMsg).show();					
						}
					},
					error:function(XMLHttpRequest, textStatus, errorDivThrown) {
						StockIssued.xhrExport = null;
					}
				});
			}
		});		
		return false;	
	},
	isValidateDate:function(searchMonth){
		var searchMonth = $(searchMonth).val().trim();
		if(searchMonth.length>0){
			if(searchMonth.length == 10 && !Utils.isDate(searchMonth)){
				return false;
			}else if(searchMonth.length == 7 && !Utils.isDate('01/'+searchMonth)){
				return false;
			}
		}
		return true;
	},
	searchProductOnDialog : function(callback, arrParam) {
		CommonSearch.openSearchProductOnDialog1(sale_plan_product, sale_plan_productname,
				sale_plan_search_product, "/commons/product/search-product-with-valid-price", callback,
				"productCode", "productName", arrParam);
		return false;
	}, beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		$('#errExcelMsg').html('').hide();
		$('#successMsg1').html('').hide();
		showLoadingIcon();
		return true;
	},
	importExcel:function(){
 		$('.SuccessMsgStyle').html('').hide();
        $('.ErrorMsgStyle').html('').hide();
        
        Utils.importExcelUtils(function(data){
        	  QuotaMonthPlan.search();
        	  $('.SuccessMsgStyle').html('').hide();
              $('.ErrorMsgStyle').html('').hide();
              $('#excelFile').val("").change();
              $('#fakefilepc').val("").change();
              if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
            	  $('#errExcelMsg').html(data.message.trim()).change().show();
            	  var tm = setTimeout(function(){
            		  $('#errExcelMsg').html(data.message.trim()).change().show();
                      clearTimeout(tm);
                }, 1500);   
                    
              } else {
            	  var tm = setTimeout(function(){
            		  $('.SuccessMsgStyle').html(sale_plan_save_success).show();
                    clearTimeout(tm);
                }, 1500);        
            	 
              }
                        
        }, 'importFrm', 'excelFile', 'errExcelMsg');
 	},
	afterImportExcel: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {
	    	$("#responseDiv").html(responseText);
	    	var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
    		if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    		$('#fakefilepc').val('');
				$('#excelFile').val('');
				return ;
	    	}
	    	else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
	    		var numFail = parseInt($('#numFail').html().trim());
	    		var fileNameFail = $('#fileNameFail').html();
	    		var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    		if(numFail > 0){
	    			mes+= ' <a href="'+ fileNameFail +'">sale_plan_view_error</a>';
	    		}
	    		$('#errExcelMsg').html(mes).show();
	    		$("#dg").datagrid("reload");
	    	}
	    	$('#fakefilepc').val('');
			$('#excelFile').val('');
	    }	
	}, 
	yearPeriodUnderChange: function(){
		if (!QuotaMonthPlan._isYearPeriodChange) {
			var par = {};
	 		par.year = $('#yearPeriodUnder').val();
	 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
	 			if (data != null && data.lstCycle != null) {
	 				Utils.bindPeriodCbx('#numPeriodUnder', data.lstCycle, data.currentNum, 206, function (){
	 					QuotaMonthPlan.validatePeriod();
	 				});
	 				QuotaMonthPlan.validatePeriod();
	 			}
	 		});
		}
		QuotaMonthPlan._isYearPeriodChange = false;
	},
	initCycleInfo: function() {
 		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 206);
 				Utils.bindPeriodCbx('#numPeriodUnder', jQuery.extend(true, [], data.lstCycle), data.currentNum, 206, function (){
 					QuotaMonthPlan.validatePeriod();
 				});
 			}
 		});
 	},
	validatePeriod: function(){
		if ($('#yearPeriodUnder').val() != null && $('#numPeriodUnder').combobox('getValue') != null) {
			var curYearPeriod = Number($('#curYearPeriod').val().trim());
			var curNumPeriod = Number($('#curNumPeriod').val().trim());
			var yearPeriodCreate = Number($('#yearPeriodUnder').val().trim());
			var numPeriodCreate = Number($('#numPeriodUnder').combobox('getValue'));
			
			if(yearPeriodCreate < curYearPeriod || (yearPeriodCreate == curYearPeriod && numPeriodCreate < curNumPeriod)){
				$('#mainErr').html('<s:text name="sale_plan_mainerr"/>');
				
				$('#btnImport').attr('disabled','disabled');
				$('#btnImport').addClass('BtnGeneralDStyle');
				
				$('#fakefilepc').attr('disabled','disabled');
				$('#fakefilepc').addClass('BtnGeneralDStyle');
				
				$('#group_insert_execute').attr('disabled','disabled');
				$('#group_insert_execute').addClass('BtnGeneralDStyle');
				
				$('#reset').attr('disabled','disabled');
				$('#reset').addClass('BtnGeneralDStyle');
			} else {
				$('#btnImport').removeAttr('disabled');
				$('#btnImport').removeClass('BtnGeneralDStyle');
				
				$('#fakefilepc').removeAttr('disabled');
				$('#fakefilepc').removeClass('BtnGeneralDStyle');
				
				$('#group_insert_execute').removeAttr('disabled');
				$('#group_insert_execute').removeClass('BtnGeneralDStyle');
				
				$('#reset').removeAttr('disabled');
				$('#reset').removeClass('BtnGeneralDStyle');
			}
		}
	},
	changeShop: function(){
		$('#shopCodeUnder').val($('#shopCode').val());
		$('#shopCodeUnder').change();
	},
	changeNumPeriod: function(){
		$('#numPeriodUnder').combobox('setValue', $('#numPeriod').combobox('getValue'))
	}
};
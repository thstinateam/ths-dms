var CreateSalePlan = {
	scroll:null,
	totalPrice:0,
	totalAmount:0,
	_quantityPlan:0,
	_amountPlan:0,
	lstCTTB : null,
	_skuMap : null,
	lstCTTB_temp : null,
	lstDelete : new Map(),
	_isYearPeriodChange: false,
	search:function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#loading').css('visibility','visible');
		var data = new Object();
		var shopCode = $('#shopCode').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var productCode = $('#productCode').val().trim();
		var dateSearch = $('#dateSearch').val().trim();
		var categoryCode = $('#category').val().trim();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('staffCode',sale_plan_nvbh_code);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dateSearch',sale_plan_month_allocate);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$('#loading').css('visibility','hidden');
			return false;
		}
		CreateSalePlan._quantityPlan = 0;
		CreateSalePlan._amountPlan = 0;
		data.shopCode = shopCode;
		data.staffCode = staffCode;
		data.productCode = productCode;
		data.dateSearch = dateSearch;
		data.categoryCode = categoryCode;
		var kData = $.param(data,true);
		$.ajax({
			type : "POST",
			url : "/sale-plan/create/search",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$('#loading').css('visibility','hidden');
				$("#divTable").html(data);
				$('#staffCodeUnder').val(staffCode);
				scroll = $('#scrollCreate').jScrollPane().data().jsp;
				$('.ScrollSection').jScrollPane().data().jsp;
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						$('#loading').css('visibility','hidden');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	/**
	 * download file template import gia san pham
	 * @author longnh15
	 * @since 06/05/2015
	 */
	downloadImportSalePlanTemplateFile: function() {
		var url = "/sale-plan/create/downloadTemplateExcelfile";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
	qtyFocused:function(selector){
		$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		var quantityAdd = 0;
		var amount = 0;
		if(rows!=null){								
			quantity = $(selector).val().replace(/,/g, '').trim();
			if(!isNaN(quantity) && quantity != ''){				
				quantityAdd = parseInt(quantity);
				CreateSalePlan._quantityPlan -= quantityAdd;
				
				amount = rows.price * quantity;
				CreateSalePlan._amountPlan -= amount;
			}
		}
	},
	amtFocused:function(selector){
		//$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		var amountAdd = 0;
		if(rows!=null){								
			var tempAmount=$(selector).val().replace(/,/g, '').trim();
			var amount = Number(tempAmount);
			if(!isNaN(amount) && amount != ''){				
				amountAdd = parseFloat(amount);
				CreateSalePlan._amountPlan -= amountAdd;
			}
		}
	},
	qtyChanged:function(selector){
		$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		var amount = 0;
		if (rows != null) {
			quantity = $(selector).val().replace(/,/g, '').trim();
			if (quantity != '') {
				if(isNaN(quantity)){				
					setTimeout(function() {
						$(selector).focus();
					}, 20);
					$('#errMsg').html(sale_plan_error_input_01).show();
					return false;
				}
				var oddQuantity = Number($(selector).attr('old-quantity'));
				quantity = Number(quantity);
				if (oddQuantity != quantity) {
					amount = rows.price * quantity;
					var oddAmount = $('#amount' + dg_index).attr('old-amount');
					oddAmount = oddAmount && oddAmount != 'null' ? Number(oddAmount) : 0;
					if (oddQuantity < quantity) {
						CreateSalePlan._quantityPlan += quantity - oddQuantity;
						CreateSalePlan._amountPlan += amount - oddAmount;
					} else {
						CreateSalePlan._quantityPlan -= oddQuantity - quantity;
						CreateSalePlan._amountPlan -= oddAmount - amount;
					}
//					CreateSalePlan._quantityPlan += quantityAdd;
//					CreateSalePlan._amountPlan += amount;
					$('#amount'+dg_index).attr('old-amount', amount);
					$('#amount'+dg_index).val(formatCurrency(amount));
					$(selector).val(formatCurrency(quantity));
					$(selector).attr('old-quantity',quantity);
				}
			} else {
				var oddQuantity = Number($(selector).attr('old-quantity'));
				CreateSalePlan._quantityPlan -= oddQuantity;
				$(selector).attr('old-quantity', '');
			}
		}
		var totalQuantity = 0;
		var totalAmount = 0;
		var rows = $('#salePlanGrid').datagrid('getRows')[0];
		totalQuantity = rows.totalQuantity + CreateSalePlan._quantityPlan;
		totalAmount = rows.totalAmount + CreateSalePlan._amountPlan;
    	$('#salePlanGrid').datagrid('reloadFooter',[
		 	{staffCode:"_",month:"_",productName: '<b>'+sale_plan_tongcong+'</b>',staffQuantity: totalQuantity + "_" , amount: totalAmount + "_"},
		 ]);
	},
	amtChanged:function(selector){
		$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		if(rows!=null){	
			var tempAmount=$(selector).val().replace(/,/g, '').trim();
			var amount = Number(tempAmount);
			if(isNaN(amount)){				
				setTimeout(function(){
					$(selector).focus();
				}, 2000);
				$('#errMsg').html(sale_plan_error_input_02).show();
				return false;
			}
			if (/^\d*\.?\d{0,6}$/.test(tempAmount)==false) {
				setTimeout(function(){
					$(selector).focus();
				}, 2000);
				$('#errMsg').html(sale_plan_error_input_02).show();
				return false;
			}
			amount = parseFloat(amount);
//			CreateSalePlan._amountPlan += amount;
			var oddAmount = Number($(selector).attr('old-amount').replace(/,/g, '').trim());
			if (oddAmount < amount) {
				CreateSalePlan._amountPlan += amount - oddAmount;
			} else {
				CreateSalePlan._amountPlan -= oddAmount - amount;
			}
			$(selector).attr('old-amount', amount);
		}
		var totalQuantity = 0;
		var totalAmount = 0;
		var rows = $('#salePlanGrid').datagrid('getRows')[0];
		totalQuantity = rows.totalQuantity + CreateSalePlan._quantityPlan;
		totalAmount = rows.totalAmount + CreateSalePlan._amountPlan;
    	$('#salePlanGrid').datagrid('reloadFooter',[
		 	{staffCode:"_",month:"_",productName: '<b>'+sale_plan_tongcong+'</b>',staffQuantity: totalQuantity + "_" , amount: totalAmount + "_"},
		 ]);
	},
	searchForGSNPP:function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#salePlanGrid').datagrid('reloadFooter',[
		    {staffCode:"_",month:"_",productName: '<b>'+sale_plan_tongcong+'</b>',staffQuantity: 0 + "_" , amount: 0 + "_"},
		]);
		var staffCode = $('#staffCode').val().trim();
		var productCode = $('#productCode').val().trim();
		var yearPeriod = $('#yearPeriod').val().trim();
		var numPeriod = $('#numPeriod').combobox('getValue');
		var msg = '';
		if (staffCode == -1) {
			msg = sale_plan_error_search_01;	
			$('#errMsg').html(msg).show();
			return false;
		}
		if (yearPeriod == '') {
			msg = sale_plan_error_search_13;	
			$('#errMsg').html(msg).show();
			return false;
		}
		if (numPeriod == '') {
			msg = sale_plan_error_search_12;	
			$('#errMsg').html(msg).show();
			return false;
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$('#loading').css('visibility','hidden');
			return false;
		}
		var lstCategoryCode = CreateSalePlan.lstCTTB;
		var lstCode = '';
		for (var i=0;i < lstCategoryCode.length;i++) {
			if (i == 0) {
				 lstCode += lstCategoryCode[i];
			} else {
				lstCode += "," + lstCategoryCode[i];
			}
		}
		CreateSalePlan._quantityPlan = 0;
		CreateSalePlan._amountPlan = 0;
		$('#salePlanGrid').datagrid('load',{shopId: $('#curShopId').val(), page : 1, yearPeriod: yearPeriod, numPeriod: numPeriod, staffCode: staffCode, productCode: productCode, lstCategoryCode: lstCategoryCode, lstCode : lstCode});
	},
	deleteRow:function(id){
		jConfirm(sale_plan_confirm_01, sale_plan_confirm, function (ret) {			
			if (ret == true) {
				var _id = 'productId_' + id;
				CreateSalePlan.lstDelete.put(_id,$('#'+ _id).val());
				$("#ruleItem_" + id).remove();
				CreateSalePlan.sumQuantityAndMoney();
				$('.ScrollSection').jScrollPane().data().jsp;
				scroll = $('#scrollCreate').jScrollPane().data().jsp;
				$('.ColsTd1').each(function(index) {
					$(this).html(index+1);
				});
			}
		});
	},
	resetAll:function(){
		$('.skuItem').each(function(){
			$(this).val('');
			$(this).html('0').show();
			$(this).attr('old-quantity','');
			var dg_index = $(this).attr('index');
			var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
			if(rows!=null){								
				quantity = $(this).val().trim();
				if(isNaN(quantity)){				
					setTimeout(function(){
						$(selector).focus();
					}, 20);
					$('#errMsg').html(sale_plan_error_input_01).show();
					return false;
				}			
				$('#amount'+dg_index).html('0');
				$('#amount'+dg_index).val('');
				$('#amount' + dg_index).attr('old-amount','');
			}
		});
		CreateSalePlan._quantityPlan = 0;
		CreateSalePlan._amountPlan = 0;
		$('#salePlanGrid').datagrid('reloadFooter',[
		 	{staffCode:"_",month:"_",productName: '<b>'+sale_plan_tongcong+'</b>',staffQuantity: 0 + "_" , amount: 0 + "_"},
		 ]);
		
	},
	distributeProduct:function(){
		var msg = '';
		$('#errExcelMsg').html('').hide();
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('dateCreate',sale_plan_month_allocate);
		if(msg.length == 0){
			var staffCodeUnder = $('#staffCodeUnder').val();
			if (staffCodeUnder == -1) {
				msg = sale_plan_error_search_02;	
				$('#errExcelMsg').html(msg).show();
				return false;
			}
		}
		if(msg.length == 0){
			if($('#dateCreate').val().trim() != ''){
				var currentTime = new Date();
				var day = currentTime.getDate();
				var month = currentTime.getMonth() + 1;
				var year = currentTime.getFullYear();
				var dateCreate = day+"/"+$('#dateCreate').val().trim();
				if(!Utils.compareDate(day + '/' + month + '/' + year,dateCreate)){
					msg = sale_plan_error_search_03;				
				}
			}
		}
		if(msg.length > 0){
			$('#errExcelMsg').html(msg).show();
			return false;
		}
		var createMonth = $('#dateCreate').val().trim();
		var searchMonth = $('#dateSearch').val().trim();
		if(searchMonth.length>0){
			if(searchMonth.length == 10){
				searchMonth = $('#dateSearch').val().split('/')[1]+'/'+$('#dateSearch').val().split('/')[2];
			}
		}
		if(createMonth.length>0){
			var MM = '';
			var yyyy = '';
			if($('#dateCreate').val().length == 7){
				MM = $('#dateCreate').val().split("/")[0];
				yyyy = $('#dateCreate').val().split("/")[1];	
			}else if($('#dateCreate').val().length == 10){
				MM = $('#dateCreate').val().split("/")[1];
				yyyy = $('#dateCreate').val().split("/")[2];
				createMonth = MM+'/'+yyyy;
			}		
		}
		var title = '';
		if(createMonth != searchMonth){
			title = sale_plan_error_search_04;
		}else{
			title = 'Bạn có chắc chắn muốn thực hiện phân bổ?';
		}
		var listId = new Array();
		var listQuantity = new Array();
		var lstIndex= new Array();
		var zeroQuantity  = false;
		$('.RuleItemClass').each(function(index) {
			var flag = true;
			if($("#amount_"+$(this).attr("data")).is(':disabled')){
				flag = false;
			}
			if(flag == true){
				var qtt = '';
				if($("#amount_"+$(this).attr("data")).val() != '' && $("#amount_"+$(this).attr("data")).val() != undefined){
					qtt = $("#amount_"+$(this).attr("data")).val().trim().replace(/^0+/, '');
				}
				if(qtt == '' || parseInt(qtt) == 0){
					$('#errExcelMsg').html(sale_plan_error_input_03).show();
					$("#amount_"+$(this).attr("data")).focus();
					zeroQuantity = true;
					return false;
				}else{
					listId.push($("#productId_"+$(this).attr("data")).val().trim());
					listQuantity.push(qtt);
				}
			}
		});
		if(zeroQuantity){
			return false;
		}
		if(listQuantity.length==0){
			$('#errExcelMsg').html(sale_plan_error_search_05).show();
			return false;
		}
		var data = new Object();
		data.dateSearch = $('#dateCreate').val().trim();
		data.staffCode = $('#staffCodeUnder').val().trim();
		data.listId = listId;
		data.listQuantity = listQuantity;
		var lstDelete= new Array();
		for(var i=0;i<CreateSalePlan.lstDelete.size();++i){
			lstDelete.push(CreateSalePlan.lstDelete.get(CreateSalePlan.lstDelete.keyArray[i]));
		}
		data.lstDelete = lstDelete;
		$.messager.confirm('Xác nhận', title, function(r){
			if (r){
				Utils.saveData(data, '/sale-plan/create/distributeproduct', null, 'errExcelMsg', function(data) {
					if(data.error == false){
						$('#staffCode').val($('#staffCodeUnder').val().trim());
						CreateSalePlan.search();
					}else{
						$('#errExcelMsg').html(data.errMsg).show();
					}
					CreateSalePlan.lstDelete = new Map();
				});
			}
		});		
		return false;
	},
	/**
	 * Import Lap ke hoach tieu thu thang
	 * 
	 * @author hunglm16
	 * @description Cap nhat ham chung CMS
	 * */
	importExcel:function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		
		Utils.importExcelUtils(function(data){
			if (Number($('#staffCode').val()).toString() !=="NaN" && Number($('#staffCode').val()) > 0) {
				CreateSalePlan.searchForGSNPP();
			}
			//$('#excelFile').val("").change();
			//$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
				 var tm = setTimeout(function(){
	           		  $('#errExcelMsg').html(data.message.trim()).change().show();
	                  clearTimeout(tm);
	               }, 1500);
			} else {
				$('#successMsg').html(sale_plan_save_success).show();
				var tm = setTimeout(function() {
					$('#successMsg').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
		return false;
	},	
	getPrice:function(obj,price,total){
		var priceUnit = Utils.returnMoneyValue($('#'+price).html());
		var t = 0;
		if(obj.val() == ''){
			t = 0;
		}else{
			t = parseInt(obj.val(), 0);
		}
		$('#'+total).html(formatCurrency(parseInt(priceUnit)*t));
		if($('#'+total).html() == 'NaN'){
			$('#'+total).text(0);
		}
		CreateSalePlan.sumQuantityAndMoney();
	},
	sumQuantityAndMoney:function(){
		var totalAmount = 0;
		var totalMoney = 0;
		$('.quantity').each(function(){
			var amount = 0;
			if($(this).val() == ''){
				amount = 0;
			}else{
				amount = parseInt($(this).val(), 0);
			}
			totalAmount+=amount;
		});	
		$('#totalAmount').html(formatCurrency(totalAmount));
		if($('#totalAmount').html() == 'NaN'){
			$('#totalAmount').text(0);
		}
		$('.amount').each(function(){
			var money = 0;
			if($(this).html() == ''){
				money = 0;
			}else{
				money = parseInt(Utils.returnMoneyValue($(this).html()), 0);
			}
			totalMoney+=money;
		});
		$('#totalMoney').html(formatCurrency(totalMoney));
		if($('#totalMoney').html() == 'NaN'){
			$('#totalMoney').text(0);
		}
	},
	
	distributeProductNew:function(){
		var msg = '';
		$('#errExcelMsg').html('').hide();
		$('#errMsg').html('').hide();

		if(msg.length == 0){
			var staffCodeUnder = $('#staffCodeUnder').val().trim();
			if (staffCodeUnder == -1) {
				msg = sale_plan_error_search_06;	
				$('#errExcelMsg').html(msg).show();
				return false;
			}
		}

		if(msg.length > 0){
			$('#errExcelMsg').html(msg).show();
			return false;
		}
		var yearPeriodSearch = $('#yearPeriod').val().trim();
		var numPeriodSearch = $('#numPeriod').combobox('getValue');
		var yearPeriodCreate = $('#yearPeriodUnder').val().trim();
		var numPeriodCreate = $('#numPeriodUnder').combobox('getValue');
		
		var title = '';
		if(yearPeriodCreate != yearPeriodSearch || numPeriodSearch != numPeriodCreate){
			title = sale_plan_error_search_14;
		}else{
			title = sale_plan_error_search_07;
		}
		var listId = new Array();
		var listQuantity = new Array();
		var listAmount = new Array();
		var zeroQuantity  = false;
		$('.skuItem').each(function(){
			var index = $(this).attr('index');
				var rows = $('#salePlanGrid').datagrid('getRows')[index];
				if(rows!=null){		
					var quantity = $(this).val().trim().replace(/,/g,'');
					var amount=$('#amount'+index).val().trim().replace(/,/g,'');
					if(isNaN(quantity)){
						$(this).focus();
						msg = sale_plan_error_input_01;
						return false;
					}			
					if(isNaN(amount)){
						$('#amount'+index).focus();
						msg = sale_plan_error_input_02;
						return false;
					}
					if (quantity!=null && quantity!='')
					{
						if (amount==null || amount=='')
						{
							listQuantity.push(quantity);
							listAmount.push(0);
							listId.push(rows.productId);
						}
						else
						{
							listQuantity.push(quantity);
							listAmount.push(amount);
							listId.push(rows.productId);
						}
					}
					else
					{
						if (amount!=null && amount!='')
						{
							listQuantity.push(0);
							listAmount.push(amount);
							listId.push(rows.productId);
						}
					}
				}				
		});	
		if(zeroQuantity){
			return false;
		}
		if(listQuantity.length==0){
			$('#errExcelMsg').html(sale_plan_error_search_05).show();
			return false;
		}
		var data = new Object();
		data.yearPeriod = $('#yearPeriodUnder').val().trim();
		data.numPeriod = $('#numPeriodUnder').combobox('getValue');
		data.staffCode = $('#staffCodeUnder').val().trim();
		data.shopId = $('#curShopId').val();
		data.listId = listId;
		data.listQuantity = listQuantity;
		data.listAmount = listAmount;
		$.messager.confirm('Xác nhận', title, function(r){
			if (r){
				Utils.saveData(data, '/sale-plan/create/distributeproduct', null, 'errExcelMsg', function(data) {
					if(data.error == false){
						$('#staffCode').val($('#staffCodeUnder').val());
						CreateSalePlan._quantityPlan = 0;
						CreateSalePlan._amountPlan = 0;
						CreateSalePlan.searchForGSNPP();
						$('#staffCode').change();
					}else{
						$('#errExcelMsg').html(data.errMsg).show();
					}
				});
			}
		});		
		return false;
	},
	//xuat file excel
	exportExcel:function(){
		var msg = "";
		var staffCodeUnder = $('#staffCodeUnder').val().trim();
		var yearPeriodCreate = $('#yearPeriodUnder').val().trim();
		var numPeriodCreate = $('#numPeriodUnder').combobox('getValue');
		var staffCode = $('#staffCode').val().trim();
		var productCode = $('#productCode').val().trim();
		var yearPeriod = $('#yearPeriod').val().trim();
		var numPeriod = $('#numPeriod').combobox('getValue');
		var lstCategoryCode = CreateSalePlan.lstCTTB;
		var lstCode = '';
		for (var i=0;i < lstCategoryCode.length;i++) {
			if (i == 0) {
				 lstCode += lstCategoryCode[i];
			} else {
				lstCode += "," + lstCategoryCode[i];
			}
		}
		$('.ErrorMsgStyle').html('').hide();		
		if ($('#staffCode').val() == -1) {
			msg = sale_plan_error_search_08;
			$('#staffCode').focus();
		}

		if(msg.length > 0){
			$('#mainErr').html(msg).show();			
			return false;
		}
		var data = new Object();
		data.shopId = $('#curShopId').val();
		data.staffCode = staffCode;
		data.yearPeriod = yearPeriod;
		data.numPeriod = numPeriod;
		if(productCode.length>0){
			data.productCode = productCode;
		}
		if(staffCodeUnder.length>0){
			data.staffCodeUnder = staffCodeUnder;
		}
		data.lstCode = lstCode;
		data.yearPeriodCreate = yearPeriodCreate;
		data.numPeriodCreate = numPeriodCreate;
		try {
			data.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(data,true);
		$.messager.confirm(sale_plan_confirm, sale_plan_excel_export, function(r){
			if (r){
				$('#divOverlay').show();		
				StockIssued.xhrExport = $.ajax({
					type : "POST",
					url : "/sale-plan/create/export",
					data :(kData),
					dataType: "json",
					success : function(data) {
						$('#divOverlay').hide();
						StockIssued.xhrExport = null;
						if(data.error!=undefined && data.error!=null && !data.error) {
							var filePath = ReportUtils.buildReportFilePath(data.view);
							window.location.href = filePath;
							setTimeout(function() {
								//Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.view);
							}, 2000);
						} else {
							$('#mainErr').html(data.errMsg).show();					
						}
					},
					error: function(XMLHttpRequest, textStatus, errorDivThrown) {
						StockIssued.xhrExport = null;
					}
				});
			}
		});		
		return false;	
	},

	 /**
	 * Xu ly khi thay doi don vi
	 * @author vuongmq
	 * @since 31/08/2015
	 */
	changeShop: function(shopId) {
		if (shopId != undefined && shopId != null && $('#curShopId').val() != shopId) {
			$.getJSON('/sale-plan/create/get-list-staff?shopId=' + shopId, function(list) {
				$('#staffCode').html('');
				if (list != undefined && list != null && list.length > 0) {
					if (list.length > 1) { 
						$('#staffCode').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode(sale_plan_choose_staff) + '</option>');
					}
					for (var i = 0; i < list.length; i++) {
						$('#staffCode').append('<option value = "' + Utils.XSSEncode(list[i].staffCode) + '">' + Utils.XSSEncode(list[i].staffCode + " - " + list[i].staffName) + '</option>');  
					}
				} else {
					$('#staffCode').append('<option value = "-1">' + Utils.XSSEncode(sale_plan_no_staff) + '</option>');
				}
				$('#staffCode').change();
				$('#staffCodeUnder').html('');
				if (list != undefined && list != null && list.length > 0) {
					if (list.length > 1) { 
						$('#staffCodeUnder').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode(sale_plan_choose_staff) + '</option>');
					}
					for (var i = 0; i < list.length; i++) {
						$('#staffCodeUnder').append('<option value = "' + Utils.XSSEncode(list[i].staffCode) + '">' + Utils.XSSEncode(list[i].staffName) + '</option>');  
					}
				} else {
					$('#staffCodeUnder').append('<option value = "-1">' + Utils.XSSEncode(sale_plan_no_staff) + '</option>');
				}
				$('#staffCodeUnder').change();
			});

		}
	},

	/**
	 * validate period
	 */
	validatePeriod: function() {
		if ($('#yearPeriodUnder').val() != null && $('#numPeriodUnder').combobox('getValue') != null) {
			var curYearPeriod = Number($('#curYearPeriod').val().trim());
			var curNumPeriod = Number($('#curNumPeriod').val().trim());
			var yearPeriodCreate = Number($('#yearPeriodUnder').val().trim());
			var numPeriodCreate = Number($('#numPeriodUnder').combobox('getValue'));
			
			if (yearPeriodCreate < curYearPeriod || (yearPeriodCreate == curYearPeriod && numPeriodCreate < curNumPeriod)) {
				$('#btnView').attr('disabled','disabled');
				$('#btnView').addClass('BtnGeneralDStyle');
				
				$('#btnImport').attr('disabled','disabled');
				$('#btnImport').addClass('BtnGeneralDStyle');
				
				$('#fakefilepc').attr('disabled','disabled');
				$('#fakefilepc').addClass('BtnGeneralDStyle');
				
				$('#addProductBtn').attr('disabled','disabled');
				$('#addProductBtn').addClass('BtnGeneralDStyle');
				
				$('#group_insert_btnDistribute').attr('disabled','disabled');
				$('#group_insert_btnDistribute').addClass('BtnGeneralDStyle');
				
				$('#btnReset').attr('disabled','disabled');
				$('#btnReset').addClass('BtnGeneralDStyle');
			}else{
				$('#btnView').removeAttr('disabled');
				$('#btnView').removeClass('BtnGeneralDStyle');
				
				
				$('#btnImport').removeAttr('disabled');
				$('#btnImport').removeClass('BtnGeneralDStyle');
				
				$('#fakefilepc').removeAttr('disabled');
				$('#fakefilepc').removeClass('BtnGeneralDStyle');
				
				$('#addProductBtn').removeAttr('disabled');
				$('#addProductBtn').removeClass('BtnGeneralDStyle');
				
				$('#group_insert_btnDistribute').removeAttr('disabled');
				$('#group_insert_btnDistribute').removeClass('BtnGeneralDStyle');
				
				$('#btnReset').removeAttr('disabled');
				$('#btnReset').removeClass('BtnGeneralDStyle');
			}
		}
	},
	/**
	 * load du lieu cho chu ky
	 */
	initCycleInfo: function() {
 		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 206);
 				Utils.bindPeriodCbx('#numPeriodUnder', jQuery.extend(true, [], data.lstCycle), data.currentNum, 206, function (){
 					CreateSalePlan.validatePeriod();
 				});
 			}
 		});
 	},
 	/**
	 * Xu ly khi thay doi so chu ky
	 */
	changeNumPeriod: function(){
		$('#numPeriodUnder').combobox('setValue', $('#numPeriod').combobox('getValue'))
	},
	/**
	 * Xu ly khi thay doi nhan vien
	 */
	changeStaff: function(){
		$('#staffCodeUnder').val($('#staffCode').val());
		$('#staffCodeUnder').change();
	},
	/**
	 * Xu ly khi thay doi nam chu ky ben duoi
	 */
	yearPeriodUnderChange: function() {
		if (!CreateSalePlan._isYearPeriodChange) {
			var par = {};
	 		par.year = $('#yearPeriodUnder').val();
	 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data) {
	 			if (data != null && data.lstCycle != null) {
	 				Utils.bindPeriodCbx('#numPeriodUnder', data.lstCycle, data.currentNum, 206, function (){
	 					CreateSalePlan.validatePeriod();
	 				});
	 				CreateSalePlan.validatePeriod();
	 			}
	 		});
		}
		CreateSalePlan._isYearPeriodChange = false;
	},

};
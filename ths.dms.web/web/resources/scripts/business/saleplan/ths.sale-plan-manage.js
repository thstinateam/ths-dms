var SalePlanMgr = {	
	initURL:function(){
		var URL='/sale-plan/manage/search?searchShop=&searchMonth=&productCode=&saleStaffCode=';
		return URL;
	},
	search:function(){
		$('#errMsg').html('').hide();
		var shopCode = $('#searchShop').val().trim();
		var searchShop = '';
		if(shopCode.length>0 && shopCode.indexOf('-')>=0){
			var arrShop = shopCode.split('-'); 
			searchShop = arrShop[0].trim();
		}else if(shopCode.length>0){
			searchShop = shopCode;
		}
		var searchMonth = $('#searchMonth').val().trim();
		var productCode = $('#productCode').val().trim();
		var saleStaffCode = $('#saleStaffCode').val().trim();
		var msg = Utils.getMessageOfRequireCheck('saleStaffCode', 'Mã nhân viên bán hàng');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('searchMonth', 'Tháng');	
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('searchMonth','Tháng', Utils._DATE_MM_YYYY);
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var URL='searchShop='+encodeChar(searchShop)+ '&searchMonth='+encodeChar(searchMonth)+ '&productCode='+encodeChar(productCode)+ '&saleStaffCode=' +encodeChar(saleStaffCode);
		$('#grid').setGridParam({url:'/sale-plan/manage/search?'+URL,page:1}).trigger("reloadGrid");
		var userData = $('#grid').jqGrid('getGridParam', 'userData');
		$.getJSON('/sale-plan/manage/payment?' + URL,function(data){
			if(data.amount==null || data.amount==undefined){
				$('#_totalAmount').html('0');
			}else{
				$('#_totalAmount').html(formatCurrency(data.amount));
			}			
		});
		return false;
	},
	searchShop:function(){		
		var shopCode = $('#searchShop').val().trim();
		var searchShop = '';
		if(shopCode.length>0 && shopCode.indexOf('-')>=0){
			var arrShop = shopCode.split('-'); 
			searchShop = arrShop[0].trim();
		}else if(shopCode.length>0){
			searchShop = shopCode;
		}
		$.getJSON('/sale-plan/manage/search-shop?searchShop='+encodeChar(searchShop),function(data){
			if(data!=null && data !=undefined){
				shopCode = data.shop;
				if(shopCode !=null && shopCode.length>0){
					$('#searchShop').val(data.shop);
				}			}
			
		});
	},
	getShopCode:function(){
		var shopCode = $('#searchShop').val().trim();
		var searchShop = '';
		if(shopCode.length>0 && shopCode.indexOf('-')>=0){
			var arrShop = shopCode.split('-'); 
			searchShop = arrShop[0].trim();
		}else if(shopCode.length>0){
			searchShop = shopCode;
		}
		return searchShop.trim();
	}
};
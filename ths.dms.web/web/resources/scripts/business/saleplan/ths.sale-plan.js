/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/saleplan/vnm.create-sale-plan.js
 */
var CreateSalePlan = {
	scroll:null,
	totalPrice:0,
	totalAmount:0,
	_quantityPlan:0,
	_amountPlan:0,
	lstCTTB : null,
	_skuMap : null,
	lstCTTB_temp : null,
	lstDelete : new Map(),
	_isYearPeriodChange: false,
	search:function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#loading').css('visibility','visible');
		var data = new Object();
		var shopCode = $('#shopCode').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var productCode = $('#productCode').val().trim();
		var dateSearch = $('#dateSearch').val().trim();
		var categoryCode = $('#category').val().trim();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('staffCode',sale_plan_nvbh_code);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dateSearch',sale_plan_month_allocate);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$('#loading').css('visibility','hidden');
			return false;
		}
		CreateSalePlan._quantityPlan = 0;
		CreateSalePlan._amountPlan = 0;
		data.shopCode = shopCode;
		data.staffCode = staffCode;
		data.productCode = productCode;
		data.dateSearch = dateSearch;
		data.categoryCode = categoryCode;
		var kData = $.param(data,true);
		$.ajax({
			type : "POST",
			url : "/sale-plan/create/search",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$('#loading').css('visibility','hidden');
				$("#divTable").html(data);
				$('#staffCodeUnder').val(staffCode);
				scroll = $('#scrollCreate').jScrollPane().data().jsp;
				$('.ScrollSection').jScrollPane().data().jsp;
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						$('#loading').css('visibility','hidden');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	/**
	 * download file template import gia san pham
	 * @author longnh15
	 * @since 06/05/2015
	 */
	downloadImportSalePlanTemplateFile: function() {
		var url = "/sale-plan/create/downloadTemplateExcelfile";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
	qtyFocused:function(selector){
		$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		var quantityAdd = 0;
		var amount = 0;
		if(rows!=null){								
			quantity = $(selector).val().replace(/,/g, '').trim();
			if(!isNaN(quantity) && quantity != ''){				
				quantityAdd = parseInt(quantity);
				CreateSalePlan._quantityPlan -= quantityAdd;
				
				amount = rows.price * quantity;
				CreateSalePlan._amountPlan -= amount;
			}
		}
	},
	amtFocused:function(selector){
		//$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		var amountAdd = 0;
		if(rows!=null){								
			var tempAmount=$(selector).val().replace(/,/g, '').trim();
			var amount = Number(tempAmount);
			if(!isNaN(amount) && amount != ''){				
				amountAdd = parseFloat(amount);
				CreateSalePlan._amountPlan -= amountAdd;
			}
		}
	},
	qtyChanged:function(selector){
		$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		var amount = 0;
		if (rows != null) {
			quantity = $(selector).val().replace(/,/g, '').trim();
			if (quantity != '') {
				if(isNaN(quantity)){				
					setTimeout(function() {
						$(selector).focus();
					}, 20);
					$('#errMsg').html(sale_plan_error_input_01).show();
					return false;
				}
				var oddQuantity = Number($(selector).attr('old-quantity'));
				quantity = Number(quantity);
				if (oddQuantity != quantity) {
					amount = rows.price * quantity;
					var oddAmount = $('#amount' + dg_index).attr('old-amount');
					oddAmount = oddAmount && oddAmount != 'null' ? Number(oddAmount) : 0;
					if (oddQuantity < quantity) {
						CreateSalePlan._quantityPlan += quantity - oddQuantity;
						CreateSalePlan._amountPlan += amount - oddAmount;
					} else {
						CreateSalePlan._quantityPlan -= oddQuantity - quantity;
						CreateSalePlan._amountPlan -= oddAmount - amount;
					}
//					CreateSalePlan._quantityPlan += quantityAdd;
//					CreateSalePlan._amountPlan += amount;
					$('#amount'+dg_index).attr('old-amount', amount);
					$('#amount'+dg_index).val(formatCurrency(amount));
					$(selector).val(formatCurrency(quantity));
					$(selector).attr('old-quantity',quantity);
				}
			} else {
				var oddQuantity = Number($(selector).attr('old-quantity'));
				CreateSalePlan._quantityPlan -= oddQuantity;
				$(selector).attr('old-quantity', '');
			}
		}
		var totalQuantity = 0;
		var totalAmount = 0;
		var rows = $('#salePlanGrid').datagrid('getRows')[0];
		totalQuantity = rows.totalQuantity + CreateSalePlan._quantityPlan;
		totalAmount = rows.totalAmount + CreateSalePlan._amountPlan;
    	$('#salePlanGrid').datagrid('reloadFooter',[
		 	{staffCode:"_",month:"_",productName: '<b>'+sale_plan_tongcong+'</b>',staffQuantity: totalQuantity + "_" , amount: totalAmount + "_"},
		 ]);
	},
	amtChanged:function(selector){
		$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		if(rows!=null){	
			var tempAmount=$(selector).val().replace(/,/g, '').trim();
			var amount = Number(tempAmount);
			if(isNaN(amount)){				
				setTimeout(function(){
					$(selector).focus();
				}, 2000);
				$('#errMsg').html(sale_plan_error_input_02).show();
				return false;
			}
			if (/^\d*\.?\d{0,6}$/.test(tempAmount)==false) {
				setTimeout(function(){
					$(selector).focus();
				}, 2000);
				$('#errMsg').html(sale_plan_error_input_02).show();
				return false;
			}
			amount = parseFloat(amount);
//			CreateSalePlan._amountPlan += amount;
			var oddAmount = Number($(selector).attr('old-amount').replace(/,/g, '').trim());
			if (oddAmount < amount) {
				CreateSalePlan._amountPlan += amount - oddAmount;
			} else {
				CreateSalePlan._amountPlan -= oddAmount - amount;
			}
			$(selector).attr('old-amount', amount);
		}
		var totalQuantity = 0;
		var totalAmount = 0;
		var rows = $('#salePlanGrid').datagrid('getRows')[0];
		totalQuantity = rows.totalQuantity + CreateSalePlan._quantityPlan;
		totalAmount = rows.totalAmount + CreateSalePlan._amountPlan;
    	$('#salePlanGrid').datagrid('reloadFooter',[
		 	{staffCode:"_",month:"_",productName: '<b>'+sale_plan_tongcong+'</b>',staffQuantity: totalQuantity + "_" , amount: totalAmount + "_"},
		 ]);
	},
	searchForGSNPP:function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#salePlanGrid').datagrid('reloadFooter',[
		    {staffCode:"_",month:"_",productName: '<b>'+sale_plan_tongcong+'</b>',staffQuantity: 0 + "_" , amount: 0 + "_"},
		]);
		var staffCode = $('#staffCode').val().trim();
		var productCode = $('#productCode').val().trim();
		var yearPeriod = $('#yearPeriod').val().trim();
		var numPeriod = $('#numPeriod').combobox('getValue');
		var msg = '';
		if (staffCode == -1) {
			msg = sale_plan_error_search_01;	
			$('#errMsg').html(msg).show();
			return false;
		}
		if (yearPeriod == '') {
			msg = sale_plan_error_search_13;	
			$('#errMsg').html(msg).show();
			return false;
		}
		if (numPeriod == '') {
			msg = sale_plan_error_search_12;	
			$('#errMsg').html(msg).show();
			return false;
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$('#loading').css('visibility','hidden');
			return false;
		}
		var lstCategoryCode = CreateSalePlan.lstCTTB;
		var lstCode = '';
		for (var i=0;i < lstCategoryCode.length;i++) {
			if (i == 0) {
				 lstCode += lstCategoryCode[i];
			} else {
				lstCode += "," + lstCategoryCode[i];
			}
		}
		CreateSalePlan._quantityPlan = 0;
		CreateSalePlan._amountPlan = 0;
		$('#salePlanGrid').datagrid('load',{shopId: $('#curShopId').val(), page : 1, yearPeriod: yearPeriod, numPeriod: numPeriod, staffCode: staffCode, productCode: productCode, lstCategoryCode: lstCategoryCode, lstCode : lstCode});
	},
	deleteRow:function(id){
		jConfirm(sale_plan_confirm_01, sale_plan_confirm, function (ret) {			
			if (ret == true) {
				var _id = 'productId_' + id;
				CreateSalePlan.lstDelete.put(_id,$('#'+ _id).val());
				$("#ruleItem_" + id).remove();
				CreateSalePlan.sumQuantityAndMoney();
				$('.ScrollSection').jScrollPane().data().jsp;
				scroll = $('#scrollCreate').jScrollPane().data().jsp;
				$('.ColsTd1').each(function(index) {
					$(this).html(index+1);
				});
			}
		});
	},
	resetAll:function(){
		$('.skuItem').each(function(){
			$(this).val('');
			$(this).html('0').show();
			$(this).attr('old-quantity','');
			var dg_index = $(this).attr('index');
			var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
			if(rows!=null){								
				quantity = $(this).val().trim();
				if(isNaN(quantity)){				
					setTimeout(function(){
						$(selector).focus();
					}, 20);
					$('#errMsg').html(sale_plan_error_input_01).show();
					return false;
				}			
				$('#amount'+dg_index).html('0');
				$('#amount'+dg_index).val('');
				$('#amount' + dg_index).attr('old-amount','');
			}
		});
		CreateSalePlan._quantityPlan = 0;
		CreateSalePlan._amountPlan = 0;
		$('#salePlanGrid').datagrid('reloadFooter',[
		 	{staffCode:"_",month:"_",productName: '<b>'+sale_plan_tongcong+'</b>',staffQuantity: 0 + "_" , amount: 0 + "_"},
		 ]);
		
	},
	distributeProduct:function(){
		var msg = '';
		$('#errExcelMsg').html('').hide();
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('dateCreate',sale_plan_month_allocate);
		if(msg.length == 0){
			var staffCodeUnder = $('#staffCodeUnder').val();
			if (staffCodeUnder == -1) {
				msg = sale_plan_error_search_02;	
				$('#errExcelMsg').html(msg).show();
				return false;
			}
		}
		if(msg.length == 0){
			if($('#dateCreate').val().trim() != ''){
				var currentTime = new Date();
				var day = currentTime.getDate();
				var month = currentTime.getMonth() + 1;
				var year = currentTime.getFullYear();
				var dateCreate = day+"/"+$('#dateCreate').val().trim();
				if(!Utils.compareDate(day + '/' + month + '/' + year,dateCreate)){
					msg = sale_plan_error_search_03;				
				}
			}
		}
		if(msg.length > 0){
			$('#errExcelMsg').html(msg).show();
			return false;
		}
		var createMonth = $('#dateCreate').val().trim();
		var searchMonth = $('#dateSearch').val().trim();
		if(searchMonth.length>0){
			if(searchMonth.length == 10){
				searchMonth = $('#dateSearch').val().split('/')[1]+'/'+$('#dateSearch').val().split('/')[2];
			}
		}
		if(createMonth.length>0){
			var MM = '';
			var yyyy = '';
			if($('#dateCreate').val().length == 7){
				MM = $('#dateCreate').val().split("/")[0];
				yyyy = $('#dateCreate').val().split("/")[1];	
			}else if($('#dateCreate').val().length == 10){
				MM = $('#dateCreate').val().split("/")[1];
				yyyy = $('#dateCreate').val().split("/")[2];
				createMonth = MM+'/'+yyyy;
			}		
		}
		var title = '';
		if(createMonth != searchMonth){
			title = sale_plan_error_search_04;
		}else{
			title = 'Bạn có chắc chắn muốn thực hiện phân bổ?';
		}
		var listId = new Array();
		var listQuantity = new Array();
		var lstIndex= new Array();
		var zeroQuantity  = false;
		$('.RuleItemClass').each(function(index) {
			var flag = true;
			if($("#amount_"+$(this).attr("data")).is(':disabled')){
				flag = false;
			}
			if(flag == true){
				var qtt = '';
				if($("#amount_"+$(this).attr("data")).val() != '' && $("#amount_"+$(this).attr("data")).val() != undefined){
					qtt = $("#amount_"+$(this).attr("data")).val().trim().replace(/^0+/, '');
				}
				if(qtt == '' || parseInt(qtt) == 0){
					$('#errExcelMsg').html(sale_plan_error_input_03).show();
					$("#amount_"+$(this).attr("data")).focus();
					zeroQuantity = true;
					return false;
				}else{
					listId.push($("#productId_"+$(this).attr("data")).val().trim());
					listQuantity.push(qtt);
				}
			}
		});
		if(zeroQuantity){
			return false;
		}
		if(listQuantity.length==0){
			$('#errExcelMsg').html(sale_plan_error_search_05).show();
			return false;
		}
		var data = new Object();
		data.dateSearch = $('#dateCreate').val().trim();
		data.staffCode = $('#staffCodeUnder').val().trim();
		data.listId = listId;
		data.listQuantity = listQuantity;
		var lstDelete= new Array();
		for(var i=0;i<CreateSalePlan.lstDelete.size();++i){
			lstDelete.push(CreateSalePlan.lstDelete.get(CreateSalePlan.lstDelete.keyArray[i]));
		}
		data.lstDelete = lstDelete;
		$.messager.confirm('Xác nhận', title, function(r){
			if (r){
				Utils.saveData(data, '/sale-plan/create/distributeproduct', null, 'errExcelMsg', function(data) {
					if(data.error == false){
						$('#staffCode').val($('#staffCodeUnder').val().trim());
						CreateSalePlan.search();
					}else{
						$('#errExcelMsg').html(data.errMsg).show();
					}
					CreateSalePlan.lstDelete = new Map();
				});
			}
		});		
		return false;
	},
	/**
	 * Import Lap ke hoach tieu thu thang
	 * 
	 * @author hunglm16
	 * @description Cap nhat ham chung CMS
	 * */
	importExcel:function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		
		Utils.importExcelUtils(function(data){
			if (Number($('#staffCode').val()).toString() !=="NaN" && Number($('#staffCode').val()) > 0) {
				CreateSalePlan.searchForGSNPP();
			}
			//$('#excelFile').val("").change();
			//$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
				 var tm = setTimeout(function(){
	           		  $('#errExcelMsg').html(data.message.trim()).change().show();
	                  clearTimeout(tm);
	               }, 1500);
			} else {
				$('#successMsg').html(sale_plan_save_success).show();
				var tm = setTimeout(function() {
					$('#successMsg').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
		return false;
	},	
	getPrice:function(obj,price,total){
		var priceUnit = Utils.returnMoneyValue($('#'+price).html());
		var t = 0;
		if(obj.val() == ''){
			t = 0;
		}else{
			t = parseInt(obj.val(), 0);
		}
		$('#'+total).html(formatCurrency(parseInt(priceUnit)*t));
		if($('#'+total).html() == 'NaN'){
			$('#'+total).text(0);
		}
		CreateSalePlan.sumQuantityAndMoney();
	},
	sumQuantityAndMoney:function(){
		var totalAmount = 0;
		var totalMoney = 0;
		$('.quantity').each(function(){
			var amount = 0;
			if($(this).val() == ''){
				amount = 0;
			}else{
				amount = parseInt($(this).val(), 0);
			}
			totalAmount+=amount;
		});	
		$('#totalAmount').html(formatCurrency(totalAmount));
		if($('#totalAmount').html() == 'NaN'){
			$('#totalAmount').text(0);
		}
		$('.amount').each(function(){
			var money = 0;
			if($(this).html() == ''){
				money = 0;
			}else{
				money = parseInt(Utils.returnMoneyValue($(this).html()), 0);
			}
			totalMoney+=money;
		});
		$('#totalMoney').html(formatCurrency(totalMoney));
		if($('#totalMoney').html() == 'NaN'){
			$('#totalMoney').text(0);
		}
	},
	
	distributeProductNew:function(){
		var msg = '';
		$('#errExcelMsg').html('').hide();
		$('#errMsg').html('').hide();

		if(msg.length == 0){
			var staffCodeUnder = $('#staffCodeUnder').val().trim();
			if (staffCodeUnder == -1) {
				msg = sale_plan_error_search_06;	
				$('#errExcelMsg').html(msg).show();
				return false;
			}
		}

		if(msg.length > 0){
			$('#errExcelMsg').html(msg).show();
			return false;
		}
		var yearPeriodSearch = $('#yearPeriod').val().trim();
		var numPeriodSearch = $('#numPeriod').combobox('getValue');
		var yearPeriodCreate = $('#yearPeriodUnder').val().trim();
		var numPeriodCreate = $('#numPeriodUnder').combobox('getValue');
		
		var title = '';
		if(yearPeriodCreate != yearPeriodSearch || numPeriodSearch != numPeriodCreate){
			title = sale_plan_error_search_14;
		}else{
			title = sale_plan_error_search_07;
		}
		var listId = new Array();
		var listQuantity = new Array();
		var listAmount = new Array();
		var zeroQuantity  = false;
		$('.skuItem').each(function(){
			var index = $(this).attr('index');
				var rows = $('#salePlanGrid').datagrid('getRows')[index];
				if(rows!=null){		
					var quantity = $(this).val().trim().replace(/,/g,'');
					var amount=$('#amount'+index).val().trim().replace(/,/g,'');
					if(isNaN(quantity)){
						$(this).focus();
						msg = sale_plan_error_input_01;
						return false;
					}			
					if(isNaN(amount)){
						$('#amount'+index).focus();
						msg = sale_plan_error_input_02;
						return false;
					}
					if (quantity!=null && quantity!='')
					{
						if (amount==null || amount=='')
						{
							listQuantity.push(quantity);
							listAmount.push(0);
							listId.push(rows.productId);
						}
						else
						{
							listQuantity.push(quantity);
							listAmount.push(amount);
							listId.push(rows.productId);
						}
					}
					else
					{
						if (amount!=null && amount!='')
						{
							listQuantity.push(0);
							listAmount.push(amount);
							listId.push(rows.productId);
						}
					}
				}				
		});	
		if(zeroQuantity){
			return false;
		}
		if(listQuantity.length==0){
			$('#errExcelMsg').html(sale_plan_error_search_05).show();
			return false;
		}
		var data = new Object();
		data.yearPeriod = $('#yearPeriodUnder').val().trim();
		data.numPeriod = $('#numPeriodUnder').combobox('getValue');
		data.staffCode = $('#staffCodeUnder').val().trim();
		data.shopId = $('#curShopId').val();
		data.listId = listId;
		data.listQuantity = listQuantity;
		data.listAmount = listAmount;
		$.messager.confirm('Xác nhận', title, function(r){
			if (r){
				Utils.saveData(data, '/sale-plan/create/distributeproduct', null, 'errExcelMsg', function(data) {
					if(data.error == false){
						$('#staffCode').val($('#staffCodeUnder').val());
						CreateSalePlan._quantityPlan = 0;
						CreateSalePlan._amountPlan = 0;
						CreateSalePlan.searchForGSNPP();
						$('#staffCode').change();
					}else{
						$('#errExcelMsg').html(data.errMsg).show();
					}
				});
			}
		});		
		return false;
	},
	//xuat file excel
	exportExcel:function(){
		var msg = "";
		var staffCodeUnder = $('#staffCodeUnder').val().trim();
		var yearPeriodCreate = $('#yearPeriodUnder').val().trim();
		var numPeriodCreate = $('#numPeriodUnder').combobox('getValue');
		var staffCode = $('#staffCode').val().trim();
		var productCode = $('#productCode').val().trim();
		var yearPeriod = $('#yearPeriod').val().trim();
		var numPeriod = $('#numPeriod').combobox('getValue');
		var lstCategoryCode = CreateSalePlan.lstCTTB;
		var lstCode = '';
		for (var i=0;i < lstCategoryCode.length;i++) {
			if (i == 0) {
				 lstCode += lstCategoryCode[i];
			} else {
				lstCode += "," + lstCategoryCode[i];
			}
		}
		$('.ErrorMsgStyle').html('').hide();		
		if ($('#staffCode').val() == -1) {
			msg = sale_plan_error_search_08;
			$('#staffCode').focus();
		}

		if(msg.length > 0){
			$('#mainErr').html(msg).show();			
			return false;
		}
		var data = new Object();
		data.shopId = $('#curShopId').val();
		data.staffCode = staffCode;
		data.yearPeriod = yearPeriod;
		data.numPeriod = numPeriod;
		if(productCode.length>0){
			data.productCode = productCode;
		}
		if(staffCodeUnder.length>0){
			data.staffCodeUnder = staffCodeUnder;
		}
		data.lstCode = lstCode;
		data.yearPeriodCreate = yearPeriodCreate;
		data.numPeriodCreate = numPeriodCreate;
		try {
			data.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(data,true);
		$.messager.confirm(sale_plan_confirm, sale_plan_excel_export, function(r){
			if (r){
				$('#divOverlay').show();		
				StockIssued.xhrExport = $.ajax({
					type : "POST",
					url : "/sale-plan/create/export",
					data :(kData),
					dataType: "json",
					success : function(data) {
						$('#divOverlay').hide();
						StockIssued.xhrExport = null;
						if(data.error!=undefined && data.error!=null && !data.error) {
							var filePath = ReportUtils.buildReportFilePath(data.view);
							window.location.href = filePath;
							setTimeout(function() {
								//Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.view);
							}, 2000);
						} else {
							$('#mainErr').html(data.errMsg).show();					
						}
					},
					error: function(XMLHttpRequest, textStatus, errorDivThrown) {
						StockIssued.xhrExport = null;
					}
				});
			}
		});		
		return false;	
	},

	 /**
	 * Xu ly khi thay doi don vi
	 * @author vuongmq
	 * @since 31/08/2015
	 */
	changeShop: function(shopId) {
		if (shopId != undefined && shopId != null && $('#curShopId').val() != shopId) {
			$.getJSON('/sale-plan/create/get-list-staff?shopId=' + shopId, function(list) {
				$('#staffCode').html('');
				if (list != undefined && list != null && list.length > 0) {
					if (list.length > 1) { 
						$('#staffCode').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode(sale_plan_choose_staff) + '</option>');
					}
					for (var i = 0; i < list.length; i++) {
						$('#staffCode').append('<option value = "' + Utils.XSSEncode(list[i].staffCode) + '">' + Utils.XSSEncode(list[i].staffCode + " - " + list[i].staffName) + '</option>');  
					}
				} else {
					$('#staffCode').append('<option value = "-1">' + Utils.XSSEncode(sale_plan_no_staff) + '</option>');
				}
				$('#staffCode').change();
				$('#staffCodeUnder').html('');
				if (list != undefined && list != null && list.length > 0) {
					if (list.length > 1) { 
						$('#staffCodeUnder').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode(sale_plan_choose_staff) + '</option>');
					}
					for (var i = 0; i < list.length; i++) {
						$('#staffCodeUnder').append('<option value = "' + Utils.XSSEncode(list[i].staffCode) + '">' + Utils.XSSEncode(list[i].staffName) + '</option>');  
					}
				} else {
					$('#staffCodeUnder').append('<option value = "-1">' + Utils.XSSEncode(sale_plan_no_staff) + '</option>');
				}
				$('#staffCodeUnder').change();
			});

		}
	},

	/**
	 * validate period
	 */
	validatePeriod: function() {
		if ($('#yearPeriodUnder').val() != null && $('#numPeriodUnder').combobox('getValue') != null) {
			var curYearPeriod = Number($('#curYearPeriod').val().trim());
			var curNumPeriod = Number($('#curNumPeriod').val().trim());
			var yearPeriodCreate = Number($('#yearPeriodUnder').val().trim());
			var numPeriodCreate = Number($('#numPeriodUnder').combobox('getValue'));
			
			if (yearPeriodCreate < curYearPeriod || (yearPeriodCreate == curYearPeriod && numPeriodCreate < curNumPeriod)) {
				$('#btnView').attr('disabled','disabled');
				$('#btnView').addClass('BtnGeneralDStyle');
				
				$('#btnImport').attr('disabled','disabled');
				$('#btnImport').addClass('BtnGeneralDStyle');
				
				$('#fakefilepc').attr('disabled','disabled');
				$('#fakefilepc').addClass('BtnGeneralDStyle');
				
				$('#addProductBtn').attr('disabled','disabled');
				$('#addProductBtn').addClass('BtnGeneralDStyle');
				
				$('#group_insert_btnDistribute').attr('disabled','disabled');
				$('#group_insert_btnDistribute').addClass('BtnGeneralDStyle');
				
				$('#btnReset').attr('disabled','disabled');
				$('#btnReset').addClass('BtnGeneralDStyle');
			}else{
				$('#btnView').removeAttr('disabled');
				$('#btnView').removeClass('BtnGeneralDStyle');
				
				
				$('#btnImport').removeAttr('disabled');
				$('#btnImport').removeClass('BtnGeneralDStyle');
				
				$('#fakefilepc').removeAttr('disabled');
				$('#fakefilepc').removeClass('BtnGeneralDStyle');
				
				$('#addProductBtn').removeAttr('disabled');
				$('#addProductBtn').removeClass('BtnGeneralDStyle');
				
				$('#group_insert_btnDistribute').removeAttr('disabled');
				$('#group_insert_btnDistribute').removeClass('BtnGeneralDStyle');
				
				$('#btnReset').removeAttr('disabled');
				$('#btnReset').removeClass('BtnGeneralDStyle');
			}
		}
	},
	/**
	 * load du lieu cho chu ky
	 */
	initCycleInfo: function() {
 		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 206);
 				Utils.bindPeriodCbx('#numPeriodUnder', jQuery.extend(true, [], data.lstCycle), data.currentNum, 206, function (){
 					CreateSalePlan.validatePeriod();
 				});
 			}
 		});
 	},
 	/**
	 * Xu ly khi thay doi so chu ky
	 */
	changeNumPeriod: function(){
		$('#numPeriodUnder').combobox('setValue', $('#numPeriod').combobox('getValue'))
	},
	/**
	 * Xu ly khi thay doi nhan vien
	 */
	changeStaff: function(){
		$('#staffCodeUnder').val($('#staffCode').val());
		$('#staffCodeUnder').change();
	},
	/**
	 * Xu ly khi thay doi nam chu ky ben duoi
	 */
	yearPeriodUnderChange: function() {
		if (!CreateSalePlan._isYearPeriodChange) {
			var par = {};
	 		par.year = $('#yearPeriodUnder').val();
	 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data) {
	 			if (data != null && data.lstCycle != null) {
	 				Utils.bindPeriodCbx('#numPeriodUnder', data.lstCycle, data.currentNum, 206, function (){
	 					CreateSalePlan.validatePeriod();
	 				});
	 				CreateSalePlan.validatePeriod();
	 			}
	 		});
		}
		CreateSalePlan._isYearPeriodChange = false;
	},

};
/*
 * END OF FILE - /web/web/resources/scripts/business/saleplan/vnm.create-sale-plan.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/saleplan/vnm.sale-day-plan.js
 */
var SaleDayPlan = {
	_daysInMonth: [31,28,31,30,31,30,31,31,30,31,30,31],
	_dayInWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
	_VIEW_BY_YEAR: 0,
	_VIEW_BY_MONTH: 1,
	_xhrSave : null,
	_hideErrExcelMsg: 0,
	viewTabMonth:0,
	getDaysInMonth: function(month,year){
	    if ((month==1)&&(year%4==0)&&((year%100!=0)||(year%400==0))){
	      return 29;
	    }else{
	      return SaleDayPlan._daysInMonth[month];
	    }
	},
	getCalendarByMonth: function(month,year,viewType){
		$('#dataBox').hide();
		var vt = SaleDayPlan._VIEW_BY_YEAR;
		if(viewType!= null && viewType!= undefined){
			vt = viewType;
		}
		var html = new Array();
		if(vt == SaleDayPlan._VIEW_BY_YEAR){
			html.push('<h5>Tháng '+ month + '</h5>');
			html.push('<p id="numExDay_'+ month +'" class="Warning3Style"></p>');
			html.push('<div class="GeneralTable Table21Section">');
		} else {
			html.push('<div class="GeneralTable Table22Section">');
		}
		html.push('<table width="100%" border="0" cellspacing="0" cellpadding="0">');
		html.push('<thead>');
		html.push('<tr>');
		html.push('<th>CN</th>');
		html.push('<th>Th2</th>');
		html.push('<th>Th3</th>');
		html.push('<th>Th4</th>');
		html.push('<th>Th5</th>');
		html.push('<th>Th6</th>');
		html.push('<th class="ColsTh7 ColsThEnd">Th7</th>');
		html.push('</tr>');
		html.push('</thead>');
		html.push('<tbody>');		
		// insert day
		var firstDayDate=new Date(year,month-1,1);
        var firstDay=firstDayDate.getDay();
        var maxCell = 42;
        // get days of previous month
        var preYear = year;
        if(firstDay > 0){
        	html.push('<tr>');
        	var preMonth = month - 1;        	
        	if(preMonth < 1){
        		preMonth = 12;
        		preYear = year - 1;
        	}
        	for(var i=0;i<firstDay;i++){
        		var pd = SaleDayPlan.getDaysInMonth(preMonth-1, preYear) - firstDay + i + 1;
        		html.push('<td class="ExtCols">'+ pd +'</td>');
        	}
        }
        
        //get days in month
        var dOM = SaleDayPlan.getDaysInMonth(month-1, preYear) + firstDay;
        for (var j=firstDay;j<dOM;j++){
        	var holClass = '';
        	if (j%7==0){
        		html.push('<tr>');
        		holClass = ' SunCols';
        		
        	}
        	html.push('<td class="DayInfo'+ holClass +'" id="tm_'+(j-firstDay+1)+'_'+ month +'" onclick="SaleDayPlan.showDate(event,'+(j-firstDay+1)+','+ month +','+ year +',this,'+ j%7 +')">'+ (j-firstDay+1) +'</td>');        	        	
        	if (j%7==6){
        		html.push('</tr>');
        	}
        }
        
        // get days of next month   
        if(dOM < (7*5)){
        	maxCell = 7*5;
        }
        if(dOM != (7*5)){
        	var nxtMonth = month + 1;
        	var nxtYear = year;
        	if(nxtMonth > 12){
        		nxtMonth = 1;
        		nxtYear = year + 1;
        	}
        	var k = 0;
        	for(var i=dOM;i<maxCell;i++){
        		k++;
        		//var pd = SaleDayPlan.getDaysInMonth(nxtMonth-1, nxtYear) - firstDay + i + 1;
        		html.push('<td class="ExtCols">'+ k +'</td>');
        	}
        	html.push('</tr>');
        } 
		//////////////		
		html.push('</tbody>');    
		html.push('</table">');
		html.push('</div">');
		return html.join("");
	},
	loadCalendar: function(){
		var year = $('#currentYear').val().trim();
		var month = $('#currentMonth').val().trim();
		for(var i=1;i<=12;i++){
			$('#m' + i).html(SaleDayPlan.getCalendarByMonth(i, year));
		}
		$('#monthContent').html(SaleDayPlan.getCalendarByMonth(i, year,SaleDayPlan._VIEW_BY_MONTH));
		SaleDayPlan.getSaleDayInfo(0, year);	
		SaleDayPlan.getSaleDayInfo(month, year);
		SaleDayPlan.setCurrentDate();
	},
	currentYearChanged: function(){
		var year = $('#currentYear').val().trim();
		$('#yearText').text(year);
		SaleDayPlan.loadCalendar();
//		SaleDayPlan.currentMonthChanged();
	},
	showYearTab: function(){
		SaleDayPlan.viewTabMonth = 0;
		$('#currentYear').val($('#currentYearMonthView').val().trim());
		$('#currentYear').change();
		$('#viewByMonth').removeClass('Active');
		$('#viewByYear').addClass('Active');
		$('#viewMonth').hide();
		$('#viewYear').show();
	},
	showMonthTab: function(){
		SaleDayPlan.viewTabMonth = 1;
		$('#currentYearMonthView').val($('#currentYear').val().trim());
		$('#currentYearMonthView').change();
		$('#viewByYear').removeClass('Active');
		$('#viewByMonth').addClass('Active');
		$('#viewYear').hide();
		$('#viewMonth').show();		
		var year = $('#currentYear').val().trim();
		var month = $('#currentMonth').val().trim();
		$('#monthContent').html(SaleDayPlan.getCalendarByMonth(month, year,SaleDayPlan._VIEW_BY_MONTH));
		//TODO
		SaleDayPlan.getSaleDayInfo(month, year);
		SaleDayPlan.setCurrentDate();
	},
	currentMonthChanged: function(){
		var year = $('#currentYearMonthView').val().trim();
		var month = $('#currentMonth').val().trim();
		$('#monthContent').html(SaleDayPlan.getCalendarByMonth(month, year,SaleDayPlan._VIEW_BY_MONTH));		
		SaleDayPlan.getSaleDayInfo(month, year);
		SaleDayPlan.setCurrentDate();
	},
	showDate: function(event,day,month,year,obj,dow){
		if (dow != 0) {
			$('#errMsg').html('').hide();
			$('#errExcelMsg').html('').hide();
			$('#dataBox').show();
			var left = event.pageX;
			var top = event.pageY;
			var w_box = 260;
			var h_box = 140;
			if ($('#divBtnUpdateSaleDay').is(':hidden')) {
				h_box = 108;
			}
			top = top - h_box;
			left = left - (w_box / 2);
			$('#dataBox .BoxEditState').css({
				'left' : left,
				'top' : top
			});
			var id = "tm_" + day + '_' + month;
			if (day < 10) {
				day = '0' + day;
			}
			if (month < 10) {
				month = '0' + month;
			}
			var dateString = SaleDayPlan._dayInWeek[dow] + ' ' + day + '/' + month + '/' + year;
			var urlTmp = '/rest/catalog/check-system-time.json?time=' + day + '/' + month + '/' + year;
			$.getJSON(urlTmp, function(data) {
				if (data != undefined && data.value == 1) {
					$('#divBtnUpdateSaleDay').show();
				} else {
					$('#divBtnUpdateSaleDay').hide();
				}
			});
			$('#dateValue').val(dateString);
			if ($('#' + id).hasClass('OffCols')) {
				setSelectBoxValue('dateType', 1);
				setSelectBoxValue('reasonGroup', $('#' + id).attr('data'));
			} else {
				setSelectBoxValue('dateType', 0);
				setSelectBoxValue('reasonGroup', -2);
			}
		}
	},
	saveDateInfo: function(){
		var msg = '';
		var dateValue = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('txtShopCode','Đơn vị');
		if(msg.length  > 0){	
			$('#txtShopCode').focus();
			$('#errMsg').html(msg).show();
			return false;
		}
		msg = Utils.getMessageOfRequireCheck('dateValue','Ngày');
		if(msg.length  == 0){			
			var arrItems = $('#dateValue').val().trim().split(' ');
			if(arrItems.length == 1){
				dateValue = arrItems[0].trim();
			} else {
				dateValue = arrItems[1].trim();
			}
			if(!Utils.isDate(dateValue, '/')){
				msg = format(msgErr_invalid_format_date,'Ngày');
				$('#dateValue').focus();
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('dateType','Trạng thái',true);
			}
			if(msg.length == 0 && $('#dateType').val().trim()== '1'){
				msg = Utils.getMessageOfRequireCheck('reasonGroup','Lý do',true);
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			if(msg.length == 0){
				arrDays = dateValue.split('/');
				if(arrDays.length > 0){
					var selDate = new Date(arrDays[1]+ '/' + arrDays[0] + '/' + arrDays[2]);
					//var day = parseInt(arrDays[0]);
					if(selDate.getDay() == 0){
						msg = 'Chủ nhật là ngày nghỉ. Bạn không thao tác được trên ngày này';
						$('#dateValue').focus();
					}
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.dateValue = dateValue;
		dataModel.dateType = $('#dateType').val().trim();
		dataModel.reasonCode = $('#reasonGroup').val().trim();		
		dataModel.shopCodeStr = $('#txtShopCode').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/sale-plan/day/save", SaleDayPlan._xhrSave, null, null, function(data){
			if(!data.error){
				var arrDate = $('#dateValue').val().trim().split('/');
				SaleDayPlan.getSaleDayInfo(arrDate[1], arrDate[2]);
				$('#dataBox').hide();
			}
		});		
		return false;
	},
	getSaleDayInfo: function(month,year){
		if (SaleDayPlan._hideErrExcelMsg <= 0) {
			$('#errExcelMsg').hide();
		} else {
			SaleDayPlan._hideErrExcelMsg--;
		}
		if(/^0\d/.test(month)){
			month = month.replace('0','');
		}
		if(parseInt(month) == 0){
			$('.DayInfo').each(function(){
				$(this).removeClass('OffCols');
				$(this).removeAttr('data');
			});
		} else {
			$('#m'+parseInt(month)+' .DayInfo,#monthContent .DayInfo').each(function(){
				$(this).removeClass('OffCols');
				$(this).removeAttr('data');
			});
		}
		$('#numWorkDayByMonth').text('');
		var shopCode = $('#txtShopCode').val().trim();
		$.getJSON('/rest/catalog/sale-day/except-day/'+month+ '/' + year +'/list.json?shopCode='+shopCode, function(data){						
			for(var i=0;i<data.length-1;i++){	
				var id = data[i].content1;
				var value = data[i].content2;
				if($('#viewYear #' + id)!= null){
					$('#viewYear #' + id).addClass('OffCols');
					$('#viewYear #' + id).attr('data',value);
				}
				if($('#viewMonth #' + id)!= null){
					$('#viewMonth #' + id).addClass('OffCols');
					$('#viewMonth #' + id).attr('data',value);
				}
			}
			if(data[data.length-1] != null){
				var objExDay = data[data.length-1];
				var arrDay = [objExDay.content1,objExDay.content2,objExDay.content3,objExDay.content4,objExDay.content5,objExDay.content6,objExDay.content7,objExDay.content8,objExDay.content9,objExDay.content10,objExDay.content11,objExDay.content12];
				if(parseInt(month) == 0){
					for(var i=0;i<arrDay.length;i++){
						var numWorkDay = SaleDayPlan.getDaysInMonth(i,year) - SaleDayPlan.getSundaysInMonth((i+1), year).length - arrDay[i]; 
						if($('#viewYear #numExDay_' + (i+1)).html()!= null && numWorkDay> 0){
							$('#viewYear #numExDay_' + (i+1)).html('(Có '+ numWorkDay +' ngày làm việc)');
						} else if($('#viewYear #numExDay_' + (i+1)).html()!= null){
							$('#viewYear #numExDay_' + (i+1)).html('');						
						}					
					}
				}else{
					var numWorkDay = SaleDayPlan.getDaysInMonth(month-1,year) - SaleDayPlan.getSundaysInMonth(month, year).length - arrDay[month-1];
					if(numWorkDay > 0){
						$('#viewYear #numExDay_' + (month)).html('(Có '+ numWorkDay +' ngày làm việc)');
						$('#numWorkDayByMonth').text('(Có '+ numWorkDay +' ngày làm việc)');
					} else {
						$('#viewYear #numExDay_' + (month)).html('');
					}
				}
				
			}
		});
	},
	setCurrentDate: function(){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(parseInt($('#currentYear').val().trim()) == year){
			var id="tm_" + day + '' + month;
			$('#viewYear #' + id).addClass('NowCols');
			$('#viewMonth #' + id).addClass('NowCols');
		}
	},
	getSundaysInMonth: function(month,year){		
		var days = new Date(year,month,0 ).getDate();
		var fDate = new Date( month +'/01/'+ year );
		var sundays = [ 8 - fDate.getDay() ];
		for ( var i = sundays[0] + 7; i <= days; i += 7 ) {
			sundays.push( i );
		}
		if(fDate.getDay() == 0){
			sundays.push(1);
		}
		return sundays;
	},
	exportExcel:function(){
		$('.ErrorMsgStyle').html('').hide();
		$('#errMsg').html('').hide();	
		$('#errExcelMsg').html('').hide();
		var month = 0;
		var year = 0;
		if(SaleDayPlan.viewTabMonth == 0){
        	 year = $('#currentYear').val().trim();
        }else{
        	 year = $('#currentYearMonthView').val().trim();
    		 month = $('#currentMonth').val().trim();
        }
		var msg = Utils.getMessageOfRequireCheck('txtShopCode','Mã đơn vị');
		if(msg.length > 0){
			$('#txtShopCode').focus();
			$('#errMsgSearch').html(msg).show();			
			return false;
		}	
		var params = new Object();
		params.month = month;
		params.year = year;
		params.shopCode = $('#txtShopCode').val().trim();
		var url = "/sale-plan/day/export-excel";
		showLoadingIcon();
		ReportUtils.exportReport(url,params,'errExcelMsg');
 		return false;
 	},
 	importExcel:function(){
 		$('.SuccessMsgStyle').html('').hide();
        $('.ErrorMsgStyle').html('').hide();
        
        Utils.importExcelUtils(function(data){
              $('#excelFile').val("").change();
              $('#fakefilepc').val("").change();
              if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
            	  var arrDate = $('#dateValue').val().trim().split('/');
					SaleDayPlan.getSaleDayInfo(arrDate[1], arrDate[2]);
                    var tm = setTimeout(function(){
                  	$('#errExcelMsg').html(data.message.trim()).change().show();
                        clearTimeout(tm);
                  }, 1500); 
              }else{
                    $('#successMsg').html("Lưu dữ liệu thành công").show();
                    var tm = setTimeout(function(){
                          //Load lai danh sach quyen
                          $('#successMsg').html("").hide();
                          clearTimeout(tm);
                    }, 1500);
                    if(SaleDayPlan.viewTabMonth == 0){
                    	var year = $('#currentYear').val().trim();
                		SaleDayPlan.getSaleDayInfo(0, year);	
                    }else{
                    	var year = $('#currentYearMonthView').val().trim();
                		var month = $('#currentMonth').val().trim();
                		SaleDayPlan.getSaleDayInfo(month, year);
                    }
              }
        }, 'importFrm', 'excelFile', 'errExcelMsg');
 	},
 	fillCodeForInputTextByDialogSearch2 : function (txt, id, code, name, isShow) {
 		$('.ErrorMsgStyle').html('').hide();
		if (isShow == undefined || isShow == null) {
			$('#'+txt.trim()).val(code+' - '+name);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.code == isShow) {
			$('#'+txt.trim()).val(code);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.name == isShow) {
			$('#'+txt.trim()).val(name);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.id == isShow) {
			$('#'+txt.trim()).val(id);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else {
			$('#'+txt.trim()).val("");
			$('#'+txt.trim()).attr(txt + '_0', 0);
		}
		$('#common-dialog-search-tree-in-grid-choose-single').dialog("close");
		if(SaleDayPlan.viewTabMonth == 1){
			SaleDayPlan.currentMonthChanged();
		}else{
			SaleDayPlan.loadCalendar();
		}
	},
 	afterImportExcel: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();	
		if (statusText == 'success') {
	    	$("#responseDiv").html(responseText);
	    	var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
    		if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    		$('#fakefilepc').val('');
				$('#excelFile').val('');
				return ;
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
	    		var numFail = parseInt($('#numFail').html().trim());
	    		var fileNameFail = $('#fileNameFail').html();
	    		var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    		if(numFail > 0){
	    			mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    		}
	    		$('#errExcelMsg').html(mes).show();
	    		SaleDayPlan._hideErrExcelMsg = 3;
	    		$('#currentYear').change();
	    	}
	    	$('#fakefilepc').val('');
			$('#excelFile').val('');
	    }
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/saleplan/vnm.sale-day-plan.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/saleplan/vnm.quota-month-plan.js
 */
var QuotaMonthPlan = {
	_xhrSave : null,
	_skuMap:null,
	lstProductId:null,
	lstQty:null,
	_searchMonth:null,
	_mapCheckProduct: new Map(),
	isReset:false,
	_isYearPeriodChange: false,
	params:function(allocate){
		var data = new Object();
		var shopCode = $('#shopCode').val().trim();
		var yearPeriod = $('#yearPeriod').val().trim();
		var numPeriod = $('#numPeriod').combobox('getValue');
		var yearPeriodUnder = $('#yearPeriodUnder').val().trim();
		var numPeriodUnder = $('#numPeriodUnder').combobox('getValue');
		
		var productCode = $('#productId').val().trim();
		var shopCodeUnder = $('#shopCodeUnder').val().trim();
		var categoryCode = '';
		$('#category').each(function(){
		    if(categoryCode.length > 0){
		    	categoryCode+=',';
		    }
		    categoryCode += $(this).val();
		});
		data.categoryCode = categoryCode;
		
		if (allocate != undefined && allocate != null && allocate == true) {
			if (shopCodeUnder.length > 0) {
				data.shopCode = shopCodeUnder;
			}
			if (yearPeriodUnder.length > 0) {
				data.yearPeriod = yearPeriodUnder;
			}
			if(numPeriodUnder.length>0){
				data.numPeriod = numPeriodUnder;
			}
		} else {
			if (shopCode.length > 0) {			
				data.shopCode = shopCode;
			}
			if (yearPeriod.length > 0) {
				data.yearPeriod = yearPeriod;
			}
			if (numPeriod.length > 0) {
				data.numPeriod = numPeriod;
			}
		}
		if (productCode.length > 0) {
			data.productCode = productCode;
		}
		try {
			data.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		return data;
	},	
	searchShopOnDialog : function(callback, arrParam) {
		CommonSearch.openSearchStyle1EasyUIDialog(sale_plan_unit_code, sale_plan_unit_name,
				sale_plan_search_unit, "/sale-plan/quota-month/shop-search", 
				callback, "shopCode","shopName", arrParam);
		return false;
	},
	search:function(){		
		var msg = "";
		$('.ErrorMsgStyle').html('').hide();		
		var msg = Utils.getMessageOfRequireCheck('shopCode',sale_plan_unit_code);
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();			
			return false;
		}		
		var params = QuotaMonthPlan.params();
		params.typeSearch = 1;
		$('#dg').datagrid('load',params);	
		$('#shopCodeUnder').val($('#shopCode').val().trim());
		QuotaMonthPlan.showTotalQuantityAndAmount(params);
		QuotaMonthPlan.isReset = false;
		return false;		
	},	
	execute:function(){
		var msg = "";
		$('.ErrorMsgStyle').html('').hide();		
		var msg = Utils.getMessageOfRequireCheck('shopCodeUnder', sale_plan_unit_allocate);
		var yearPeriodSearch = $('#yearPeriod').val().trim();
		var yearPeriodCreate = $('#yearPeriodUnder').val().trim();
		var numPeriodSearch = $('#numPeriod').combobox('getValue');
		var numPeriodCreate = $('#numPeriodUnder').combobox('getValue');
		
		var title = '';		

		if (yearPeriodCreate != yearPeriodSearch || numPeriodSearch != numPeriodCreate) {
			title = sale_plan_error_search_14;
		} else {
			var shopCodeUnder = $('#shopCodeUnder').val().trim();
			var shopCode = $('#shopCode').val().trim();
			if (shopCodeUnder.length > 0 && shopCode.length > 0 && shopCodeUnder.toUpperCase() != shopCode.toUpperCase()) {
				title = sale_plan_error_search_11;
			}
		}
		var lstProduct = new Array(),lstQuantity = new Array(), lstAmount= new Array();	
		$('.skuItem').each(function(){
			var index = $(this).attr('index');
				var rows = $('#dg').datagrid('getRows')[index];
				if (rows != null) {
					var quantity = $(this).val().trim().replace(/,/g,'');
					var tmpQuantity=getQuantity($(this).val().trim().replace(/,/g,''),rows.convfact);;
					var productcode=rows.productCode;
					var tmpAmount=parseFloat(Number($('#amount'+productcode).val().trim().replace(/,/g,'')));
					var amount = $('#amount'+productcode).val().trim().replace(/,/g,'');
					if (isNaN(quantity)) {
						$(this).focus();
						msg = sale_plan_error_input_01;
						return false;
					}
					if (isNaN(amount)) {
						$('#amount'+index).focus();
						msg = sale_plan_error_input_02;
						return false;
					}
					if (quantity!=null && quantity!='')
					{
						if (amount==null || amount=='')
						{
							lstQuantity.push(tmpQuantity);
							lstAmount.push(0);
							lstProduct.push(rows.productId);
						}
						else
						{
							lstQuantity.push(tmpQuantity);
							lstAmount.push(tmpAmount);
							lstProduct.push(rows.productId);
						}
					}
					else
					{
						if (amount!=null && amount!='')
						{
							lstQuantity.push(0);
							lstAmount.push(tmpAmount);
							lstProduct.push(rows.productId);
						}
					}
				}				
		});		
		if(msg.length==0 && lstProduct.length==0){
			msg = sale_plan_error_search_05;
		}
		if(msg.length > 0){
			$('#mainErr').html(msg).show();
			return false;
		}
		
		var data = QuotaMonthPlan.params(true);
		data.lstProduct = lstProduct;
		data.lstQuantity = lstQuantity;	
		data.lstAmount=lstAmount;
		Utils.addOrSaveData(data, "/sale-plan/quota-month/allocation", QuotaMonthPlan._xhrSave, 'errMsgSearch',function(d){					
		},'loading2',null,null,title);
		return false;
		
	},
	showTotalQuantityAndAmount : function(params) {
		$.ajax({
			type : "POST",
			url : "/sale-plan/quota-month/footer",
			data :($.param(params,true)),
			dataType: "json",
			success : function(data) {
				if(data!=null && data.footer!=null){
					$('#dg').datagrid('reloadFooter',[
					 	{shopCode:"_",spMonth:"_",productName: '<b>'+sale_plan_tongcong+'</b>',quantity: data.footer.quantity + "_" , amount: data.footer.amount + "_"},
					 ]);
				}else{
					$('#dg').datagrid('reloadFooter',[
					     {shopCode:"_",spMonth:"_",productName: '<b>'+sale_plan_tongcong+'</b>',quantity: 0 + "_" , amount: 0 + "_"},
					 ]);
				} 
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#dg').datagrid('reloadFooter',[
				     {shopCode:"_",spMonth:"_",productName: '<b>'+sale_plan_tongcong+'</b>',quantity: 0 + "_" , amount: 0 + "_"},
				 ]);
			}
		});
	},	
	reset:function(){
		$('.ErrorMsgStyle').html('').hide();		
		enableWidthAuthorize('execute');
		enableWidthAuthorize('reset');
		enableWidthAuthorize('btnImportExcel');
		$('#productId').val('');		
		$('.skuItem').each(function(){
			$(this).val('');
			$(this).attr('old-quantity','');
			$('#amount'+$(this).attr('id')).html('0');
			$('#amount'+$(this).attr('id')).val('');
			$('#amount'+$(this).attr('id')).attr('old-amount','');
			$(this).attr('reset',1);
		});
		QuotaMonthPlan.isReset = true;
		$('#footerQuantity').html("0");
		$('#footerAmount').html("0");
		return false;
	},	
	/**
	 * download file template import gia san pham
	 * @author longnh15
	 * @since 06/05/2015
	 */
	downloadImportSalePlanTemplateFile: function() {
		var url = "/sale-plan/quota-month/downloadTemplateExcelfile";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
	qtyFocused:function(selector){
		$('.ErrorMsgStyle').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#salePlanGrid').datagrid('getRows')[dg_index];
		var quantityAdd = 0;
		var amount = 0;
		if(rows!=null){								
			quantity = $(selector).val().trim().replace(/,/g, '');
			if(!isNaN(quantity) && quantity != ''){				
				quantityAdd = parseInt(quantity);
				CreateSalePlan._quantityPlan -= quantityAdd;
				
				amount = rows.price * quantity;
				CreateSalePlan._amountPlan -= amount;
			}
		}
	},
	qtyChanged:function(selector) {
		$('.ErrorMsgStyle').html('').hide();
		var dg_index = $(selector).attr('index');
		var rows = $('#dg').datagrid('getRows')[dg_index];
		if (rows != null) {
			if ($(selector).val() != null && $(selector).val() != '') {
				quantity = getQuantity($(selector).val().trim().replace(/,/g, ''),rows.convfact);
				if(isNaN(quantity) || (parseInt(quantity)<0)){				
					setTimeout(function(){
						$(selector).focus();
					}, 20);
					$('#errMsgSearch').html(sale_plan_error_input_03).show();
					return false;
				}
				var oddQuantity = Number($(selector).attr('old-quantity'));
				if (oddQuantity != quantity) {
					$('#amount' + rows.productCode).val(formatFloatValue(rows.price * quantity,2));
					$(selector).val(formatFloatValue(quantity));
					var oddAmount = $('#amount' + rows.productCode).attr('old-amount');
					oddAmount = oddAmount && oddAmount != 'null' ? Number(oddAmount) : 0;
					var footerQuantity = Number(Utils.returnMoneyValue($('#footerQuantity').html()));
					var footerAmount = Number(Utils.returnMoneyValue($('#footerAmount').html()));
					if (oddQuantity < quantity) {
						footerQuantity += quantity - oddQuantity;
						footerAmount += (rows.price * quantity) - oddAmount;
					} else {
						footerQuantity -= oddQuantity - quantity;
						footerAmount -= oddAmount - (rows.price * quantity);
					}
					$(selector).attr('old-quantity',quantity);
					$('#amount'+rows.productCode).attr('old-amount',Number(rows.price * quantity));
					
					$('#footerQuantity').html(formatCurrency(footerQuantity));
					$('#footerAmount').html(formatFloatValue(footerAmount,2));
				}
			} else {
				var oddQuantity = Number($(selector).attr('old-quantity'));
				var footerQuantity = Number(Utils.returnMoneyValue($('#footerQuantity').html()));
				footerQuantity -= oddQuantity;
				$(selector).attr('old-quantity', '');
				$('#footerQuantity').html(formatCurrency(footerQuantity));
			}
		}
	},
	amtChanged:function(selector){
		$('.ErrorMsgStyle').html('').hide();
		var dg_index = $(selector).attr('index');
		var rows = $('#dg').datagrid('getRows')[dg_index];
		if(rows!=null){								
			var tempAmount=$(selector).val().replace(/,/g, '').trim();
			var amount = Number(tempAmount);
			if(isNaN(amount) || (parseFloat(amount)<0)){				
				setTimeout(function(){
					$(selector).focus();
				}, 2000);
				$('#errMsgSearch').html(sale_plan_error_input_03).show();
				return false;
			}
			if (/^\d*\.?\d{0,6}$/.test(tempAmount)==false) {
				setTimeout(function(){
					$(selector).focus();
				}, 2000);
				$('#errMsgSearch').html(sale_plan_error_input_03).show();
				return false;
			}
			if (tempAmount.trim().length>0)
			{
				$('#amount'+rows.productCode).val(formatFloatValue(amount,2));
			}
			var oddAmount = Number($('#amount'+rows.productCode).attr('old-amount').replace(/,/g, '').trim());
			var footerAmount = Number(Utils.returnMoneyValue($('#footerAmount').html()));
			if(oddAmount<amount){
				footerAmount += amount - oddAmount;
			}else{
				footerAmount -= oddAmount - amount;
			}
			$(selector).attr('old-amount',amount);
			$('#footerAmount').html(formatFloatValue(footerAmount,2));
		}
	},
	checkInputDate:function(MM,yyyy){		
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = (now.getMonth()+1).toString().length>1?(now.getMonth()+1):'0'+(now.getMonth()+1);
		var currentDate = cMonth+'/'+cYear;
		$('.ErrorMsgStyle').html('').hide();		
		var x = MM+'/'+yyyy;
		if (MM != cMonth && Utils.compareMonth(x,currentDate)){
			$('#addSKU').addClass('BtnGeneralDStyle');
			$('#group_insert_execute').addClass('BtnGeneralDStyle');
			$('#reset').addClass('BtnGeneralDStyle');
			$('#btnImportExcel').addClass('BtnGeneralDStyle');
			$('#btnViewExcel').addClass('BtnGeneralDStyle');
			disabled('addSKU');
			disabled('execute');
			disabled('reset');
			disabled('btnImportExcel');
			disabled('btnViewExcel');
			$('.skuItem').each(function(){
				$(this).attr('disabled','disabled');
			});
			$('#mainErr').html(sale_plan_error_search_03).show();
			return false;
		}else{			
			$('#addSKU').removeClass('BtnGeneralDStyle');
			$('#group_insert_execute').removeClass('BtnGeneralDStyle');
			$('#reset').removeClass('BtnGeneralDStyle');
			$('#btnImportExcel').removeClass('BtnGeneralDStyle');
			$('#btnViewExcel').removeClass('BtnGeneralDStyle');
			enableWidthAuthorize('addSKU');
			enableWidthAuthorize('execute');
			enableWidthAuthorize('reset');
			enableWidthAuthorize('btnImportExcel');
			enableWidthAuthorize('btnViewExcel');
			$('.skuItem').each(function(){
				$(this).removeAttr('disabled');
			});
			$('#errMsgSearch').html('').hide();
			return true;
		}
	},
	exportExcel:function(){
		var msg = "";
		$('.ErrorMsgStyle').html('').hide();		
		var msg = Utils.getMessageOfRequireCheck('shopCode',sale_plan_unit_code);
		if(msg.length > 0){
			$('#mainErr').html(msg).show();			
			return false;
		}		
		var kData = $.param(QuotaMonthPlan.params(),true);
		$.messager.confirm(sale_plan_confirm, sale_plan_excel_export, function(r){
			if (r){
				$('#divOverlay').show();		
				StockIssued.xhrExport = $.ajax({
					type : "POST",
					url : "/sale-plan/quota-month/export",
					data :(kData),
					dataType: "json",
					success : function(data) {
						$('#divOverlay').hide();
						StockIssued.xhrExport = null;
						if(data.error!=undefined && data.error!=null && !data.error) {
							var filePath = ReportUtils.buildReportFilePath(data.view);
							window.location.href = filePath;
							setTimeout(function(){ 
			                    CommonSearch.deleteFileExcelExport(data.view);
							},2000);
						} else {
							$('#mainErr').html(data.errMsg).show();					
						}
					},
					error:function(XMLHttpRequest, textStatus, errorDivThrown) {
						StockIssued.xhrExport = null;
					}
				});
			}
		});		
		return false;	
	},
	isValidateDate:function(searchMonth){
		var searchMonth = $(searchMonth).val().trim();
		if(searchMonth.length>0){
			if(searchMonth.length == 10 && !Utils.isDate(searchMonth)){
				return false;
			}else if(searchMonth.length == 7 && !Utils.isDate('01/'+searchMonth)){
				return false;
			}
		}
		return true;
	},
	searchProductOnDialog : function(callback, arrParam) {
		CommonSearch.openSearchProductOnDialog1(sale_plan_product, sale_plan_productname,
				sale_plan_search_product, "/commons/product/search-product-with-valid-price", callback,
				"productCode", "productName", arrParam);
		return false;
	}, beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		$('#errExcelMsg').html('').hide();
		$('#successMsg1').html('').hide();
		showLoadingIcon();
		return true;
	},
	importExcel:function(){
 		$('.SuccessMsgStyle').html('').hide();
        $('.ErrorMsgStyle').html('').hide();
        
        Utils.importExcelUtils(function(data){
        	  QuotaMonthPlan.search();
        	  $('.SuccessMsgStyle').html('').hide();
              $('.ErrorMsgStyle').html('').hide();
              $('#excelFile').val("").change();
              $('#fakefilepc').val("").change();
              if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
            	  $('#errExcelMsg').html(data.message.trim()).change().show();
            	  var tm = setTimeout(function(){
            		  $('#errExcelMsg').html(data.message.trim()).change().show();
                      clearTimeout(tm);
                }, 1500);   
                    
              } else {
            	  var tm = setTimeout(function(){
            		  $('.SuccessMsgStyle').html(sale_plan_save_success).show();
                    clearTimeout(tm);
                }, 1500);        
            	 
              }
                        
        }, 'importFrm', 'excelFile', 'errExcelMsg');
 	},
	afterImportExcel: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {
	    	$("#responseDiv").html(responseText);
	    	var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
    		if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    		$('#fakefilepc').val('');
				$('#excelFile').val('');
				return ;
	    	}
	    	else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
	    		var numFail = parseInt($('#numFail').html().trim());
	    		var fileNameFail = $('#fileNameFail').html();
	    		var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    		if(numFail > 0){
	    			mes+= ' <a href="'+ fileNameFail +'">sale_plan_view_error</a>';
	    		}
	    		$('#errExcelMsg').html(mes).show();
	    		$("#dg").datagrid("reload");
	    	}
	    	$('#fakefilepc').val('');
			$('#excelFile').val('');
	    }	
	}, 
	yearPeriodUnderChange: function(){
		if (!QuotaMonthPlan._isYearPeriodChange) {
			var par = {};
	 		par.year = $('#yearPeriodUnder').val();
	 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
	 			if (data != null && data.lstCycle != null) {
	 				Utils.bindPeriodCbx('#numPeriodUnder', data.lstCycle, data.currentNum, 206, function (){
	 					QuotaMonthPlan.validatePeriod();
	 				});
	 				QuotaMonthPlan.validatePeriod();
	 			}
	 		});
		}
		QuotaMonthPlan._isYearPeriodChange = false;
	},
	initCycleInfo: function() {
 		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 206);
 				Utils.bindPeriodCbx('#numPeriodUnder', jQuery.extend(true, [], data.lstCycle), data.currentNum, 206, function (){
 					QuotaMonthPlan.validatePeriod();
 				});
 			}
 		});
 	},
	validatePeriod: function(){
		if ($('#yearPeriodUnder').val() != null && $('#numPeriodUnder').combobox('getValue') != null) {
			var curYearPeriod = Number($('#curYearPeriod').val().trim());
			var curNumPeriod = Number($('#curNumPeriod').val().trim());
			var yearPeriodCreate = Number($('#yearPeriodUnder').val().trim());
			var numPeriodCreate = Number($('#numPeriodUnder').combobox('getValue'));
			
			if(yearPeriodCreate < curYearPeriod || (yearPeriodCreate == curYearPeriod && numPeriodCreate < curNumPeriod)){
				$('#mainErr').html('<s:text name="sale_plan_mainerr"/>');
				
				$('#btnImport').attr('disabled','disabled');
				$('#btnImport').addClass('BtnGeneralDStyle');
				
				$('#fakefilepc').attr('disabled','disabled');
				$('#fakefilepc').addClass('BtnGeneralDStyle');
				
				$('#group_insert_execute').attr('disabled','disabled');
				$('#group_insert_execute').addClass('BtnGeneralDStyle');
				
				$('#reset').attr('disabled','disabled');
				$('#reset').addClass('BtnGeneralDStyle');
			} else {
				$('#btnImport').removeAttr('disabled');
				$('#btnImport').removeClass('BtnGeneralDStyle');
				
				$('#fakefilepc').removeAttr('disabled');
				$('#fakefilepc').removeClass('BtnGeneralDStyle');
				
				$('#group_insert_execute').removeAttr('disabled');
				$('#group_insert_execute').removeClass('BtnGeneralDStyle');
				
				$('#reset').removeAttr('disabled');
				$('#reset').removeClass('BtnGeneralDStyle');
			}
		}
	},
	changeShop: function(){
		$('#shopCodeUnder').val($('#shopCode').val());
		$('#shopCodeUnder').change();
	},
	changeNumPeriod: function(){
		$('#numPeriodUnder').combobox('setValue', $('#numPeriod').combobox('getValue'))
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/saleplan/vnm.quota-month-plan.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/saleplan/vnm.sale-plan-manage.js
 */
var SalePlanMgr = {	
	initURL:function(){
		var URL='/sale-plan/manage/search?searchShop=&searchMonth=&productCode=&saleStaffCode=';
		return URL;
	},
	search:function(){
		$('#errMsg').html('').hide();
		var shopCode = $('#searchShop').val().trim();
		var searchShop = '';
		if(shopCode.length>0 && shopCode.indexOf('-')>=0){
			var arrShop = shopCode.split('-'); 
			searchShop = arrShop[0].trim();
		}else if(shopCode.length>0){
			searchShop = shopCode;
		}
		var searchMonth = $('#searchMonth').val().trim();
		var productCode = $('#productCode').val().trim();
		var saleStaffCode = $('#saleStaffCode').val().trim();
		var msg = Utils.getMessageOfRequireCheck('saleStaffCode', 'Mã nhân viên bán hàng');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('searchMonth', 'Tháng');	
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('searchMonth','Tháng', Utils._DATE_MM_YYYY);
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var URL='searchShop='+encodeChar(searchShop)+ '&searchMonth='+encodeChar(searchMonth)+ '&productCode='+encodeChar(productCode)+ '&saleStaffCode=' +encodeChar(saleStaffCode);
		$('#grid').setGridParam({url:'/sale-plan/manage/search?'+URL,page:1}).trigger("reloadGrid");
		var userData = $('#grid').jqGrid('getGridParam', 'userData');
		$.getJSON('/sale-plan/manage/payment?' + URL,function(data){
			if(data.amount==null || data.amount==undefined){
				$('#_totalAmount').html('0');
			}else{
				$('#_totalAmount').html(formatCurrency(data.amount));
			}			
		});
		return false;
	},
	searchShop:function(){		
		var shopCode = $('#searchShop').val().trim();
		var searchShop = '';
		if(shopCode.length>0 && shopCode.indexOf('-')>=0){
			var arrShop = shopCode.split('-'); 
			searchShop = arrShop[0].trim();
		}else if(shopCode.length>0){
			searchShop = shopCode;
		}
		$.getJSON('/sale-plan/manage/search-shop?searchShop='+encodeChar(searchShop),function(data){
			if(data!=null && data !=undefined){
				shopCode = data.shop;
				if(shopCode !=null && shopCode.length>0){
					$('#searchShop').val(data.shop);
				}			}
			
		});
	},
	getShopCode:function(){
		var shopCode = $('#searchShop').val().trim();
		var searchShop = '';
		if(shopCode.length>0 && shopCode.indexOf('-')>=0){
			var arrShop = shopCode.split('-'); 
			searchShop = arrShop[0].trim();
		}else if(shopCode.length>0){
			searchShop = shopCode;
		}
		return searchShop.trim();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/saleplan/vnm.sale-plan-manage.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/kpi/vnm.set-target-kpi.js
 */
var TargetKpi = {
	_MAX_CYCLE_NUM: 13,
	_lstStaffInfo: [],
	getSearchParams: function() {
		var params = {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		params.yearPeriod = $('#yearPeriod').val().trim();
		params.numPeriod = $('#numPeriod').combobox('getValue');
		return params;
	},
	search: function() {
		hideAllMessage();
		TargetKpi._lstStaffInfo = [];
		var par = TargetKpi.getSearchParams();
		Utils.getHtmlDataByAjax(par, '/kpi/set-target/search', function(data){
			$('#gridContainer').html(data);
		});
	},
	bindFormatInput: function(){
		$('#gridContainer input.kpi').each(function(index, item){
			var id = $(this).attr("id");
			Utils.bindFormatOntextfieldCurrencyFor(id, Utils._TF_NUMBER_DOT);
			$(this).bind('blur', function(){
				var kpiId = $(this).attr('kpi');
				if (!Utils.isEmpty(kpiId)) {
					var numRow = $('#gridContainer input.kpi[kpi="' + kpiId + '"]').length;
					var total = 0;
					$('#gridContainer input.kpi[kpi="' + kpiId + '"]').each(function(index, item){
						var val = $(this).val().trim().replace(/,/g, "");
						if (!Utils.isEmpty(val) && !isNaN(val)) {
							total += Number(val);
						}
					});
					if (total == 0) {
						$('#' + kpiId + '-avg').html(total);
					} else {
						$('#' + kpiId + '-avg').html(formatFloatValue(total/numRow, 2));
					}
				}
				
			});
		});
	},
	setKpi: function() {
//		$('#errExcelMsg').html('').hide();
		hideAllMessage();
		var date = new Date();
		var curYear = $('#curYearPeriod').val().trim();
		var curPeriod = $('#curNumPeriod').val().trim();
		var year = $('#yearPeriodUnder').val().trim();
		var period = $('#numPeriodUnder').combobox('getValue').trim();
		var errMsg = '';
		if (parseInt(period) < 0 || parseInt(period) > TargetKpi._MAX_CYCLE_NUM){
			errMsg = 'Chu kỳ không hợp lệ. Vui lòng chọn lại';
		}
		if (errMsg == '' && !Utils.isEmpty(curYear) && !isNaN(curYear) && parseInt(year) < parseInt(curYear)){
			errMsg = 'Năm phải lớn hơn hay bằng năm hiện tại. Vui lòng chọn lại';
		}
		if (errMsg == '' && !Utils.isEmpty(curYear) && !isNaN(curYear) && parseInt(year) == parseInt(curYear) && parseInt(period) < parseInt(curPeriod)){
			errMsg = 'Chu kỳ phải lớn hơn hay bằng chu kỳ hiện tại. Vui lòng chọn lại';
		}
		if (TargetKpi._lstStaffInfo == null || TargetKpi._lstStaffInfo.length == 0){
			errMsg = 'Danh sách nhân viên rỗng.'
		}
		if (errMsg.length > 0){
			$('#errExcelMsg').html(errMsg).show();
			return;
		}
		var lstData = [];
		var objData = {};
		for (var i = 0, n = TargetKpi._lstStaffInfo.length; i < n; i++){
			var st = TargetKpi._lstStaffInfo[i];
			var obj = {};
			var lstKpiData = [];
			objData = {};
			objData.staffId = st.staffId;
			objData.year = year;
			objData.period = period;
			$('#gridContainer input[st=' + st.staffId + ']').each(function(index, item){
				var id = $(this).attr("id");
				obj = {};
				obj.key = $(this).attr("kpi");
				obj.value = $(this).val().replace(/,/g, "");
				lstKpiData.push(obj);
			});
			objData.lstKpiData = lstKpiData;
			lstData.push(objData);
		}
		var dataModel = {};
		convertToSimpleObject(dataModel, lstData, 'lstKpiSaveInfo');
		Utils.addOrSaveData(dataModel, '/kpi/set-target/save-info', null, 'errExcelMsg', function(){
			
		});
		
	},
	downloadTemplate: function() {
		hideAllMessage();
 		var param = {};
 		try {
 			param.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
 		Utils.getJSONDataByAjax(param, '/kpi/set-target/download-template', function(data) {
 			if (data.error) {
 				$('#errExcelMsg').html(data.errMsg).show();
 			} else if (data.view != null && data.view.length > 0) {
 				var filePath = ReportUtils.buildReportFilePath(data.view);
 				window.location.href = filePath;
 			}
 			
 		}, null, null);
 	},
 	importExcel: function() {
 		var params = {};
 		$('.SuccessMsgStyle').html('').hide();
 		$('.ErrorMsgStyle').html('').hide();
 		
		Utils.importExcelUtils(function(data) {
			TargetKpi.search();
			$('#errExcelMsg').val("").change();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				$('#successMsg').html(jsp_common_save_success).show();
				var tm = setTimeout(function(){
					//Load lai danh sach quyen
					$('#successMsg').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
 	},
 	exportExcel: function() {
 		var params = TargetKpi.getSearchParams();
 		ReportUtils.exportReport('/kpi/set-target/exportExcel',params,'errMsg');
 	},
 	initCycleInfo: function() {
 		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 113);
 				Utils.bindPeriodCbx('#numPeriodUnder', jQuery.extend(true, [], data.lstCycle), data.currentNum, 113);
 			}
 		});
 	},

	/**
	 * khi change yearPeriodUnder Lay chu ky cua combobox Under(numPeriodUnder) 
	 * @author vuongmq
	 * @since 13/10/2015 
	 */
 	initCycleInfoUnder: function() {
 		var par = {};
 		par.year = $('#yearPeriodUnder').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriodUnder', data.lstCycle, data.currentNum, 113);
 			}
 		});
 	},
 	setCurrentYear: function(year) {
 		$('#yearPeriod').val(year);
 		$('#yearPeriod').change();
 		$('#yearPeriodUnder').val(year);
 		$('#yearPeriodUnder').change();
 	}
 	
}
/*
 * END OF FILE - /web/web/resources/scripts/business/kpi/vnm.set-target-kpi.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/aso/vnm.aso-target.js
 */
var AsoTarget = {
	_MAX_CYCLE_NUM: 13,
	_lstStaffInfo: [],
	_TYPE_ASO_SKU: 1, 
	_TYPE_ASO_SUB_CAT: 2,
	_paramSearch: null,

	/**
	 * Lay params khi nhan buttom search
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	getSearchParams: function() {
		var params = {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		params.yearPeriod = $('#yearPeriod').val().trim();
		params.numPeriod = $('#numPeriod').combobox('getValue');
		params.staffCode = $('#staffCode').val();
		params.routingId = $('#routingId').val();
		params.type = $('#typeAso').val();
		AsoTarget._paramSearch = params;
		return params;
	},

	/**
	 * Search danh sach phan bo aso theo loai
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	search: function() {
		hideAllMessage();
		AsoTarget._lstStaffInfo = [];
		var par = AsoTarget.getSearchParams();
		Utils.getHtmlDataByAjax(par, '/aso/search', function(data){
			$('#gridContainer').html(data);
		});
	},

	/**
	 * Bind input chi tieu cua Aso
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	bindFormatInput: function(){
		$('#gridContainer input.aso').each(function(index, item){
			var id = $(this).attr("id");
			Utils.bindFormatOntextfieldCurrencyFor(id, Utils._TF_NUMBER_DOT);
			$(this).bind('blur', function(){
				var value = $('#' + id).val();
				if (!Utils.isEmpty(value) && !isNaN(value)) {
					if (Number(value) < 0) {
						$('#errExcelMsg').html('Vui lòng nhập giá trị lớn hơn 0');
					} else {
						hideAllMessage();
					}
				}
			});
		});
	},

	/**
	 * Confirm khi nhan phan bo aso
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	confirmSaveAso: function(){
		hideAllMessage();
		var errMsg = '';
		var nvbh = $('#staffCodeUnder').val();
		if (Utils.isEmpty(nvbh) || nvbh == activeType.DELETE) {
			errMsg = 'Không có nhân viên bán hàng';
		}
		if (errMsg.length == 0) {
			if (Utils.isEmpty($('#routingIdUnder').val())) {
				errMsg = 'Không có tuyến của nhân viên bán hàng';
			}
		}
		if (errMsg.length > 0) {
			$('#errExcelMsg').html(errMsg).show();
			return false;
		}
		var type = $('#curTypeAso').val();
		var mesDiaLog = '';
		if (type == AsoTarget._TYPE_ASO_SKU) {
			mesDiaLog = 'Bạn có muốn phân bổ aso theo SKU không?';
		} else {
			mesDiaLog = 'Bạn có muốn phân bổ aso theo ngành hàng con không?';
		}
		$.messager.confirm('Xác nhận', mesDiaLog, function(r) {  
			if (r) {
				var params = new Object();
				params.shopCode = $('#curShopCodeAso').val();
				params.yearPeriod = $('#yearPeriodUnder').val().trim();
				params.numPeriod = $('#numPeriodUnder').combobox('getValue');
				params.staffCode = $('#staffCodeUnder').val();
				params.type = type;
				Utils.getJSONDataByAjaxNotOverlay(params, '/aso/checkAsoPlan', function(data) {
					if (data != undefined && data != null && data.flag != null) {
						if (data.flag == activeType.WAITING) {
							$.messager.confirm('Xác nhận', data.errMsg, function(r) {  
								if (r) {
									AsoTarget.setAso();
								}
							});
						} else if (data.flag == activeType.STOPPED) {
							$.messager.alert('Thông báo', data.errMsg, 'info');
						} else {
							AsoTarget.setAso();
						}
					}
				});
			}
		});
	},

	/**
	 * Phan bo aso
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	setAso: function() {
		hideAllMessage();
		var date = new Date();
		var curYear = $('#curYearPeriod').val().trim();
		var curPeriod = $('#curNumPeriod').val().trim();
		var year = $('#yearPeriodUnder').val().trim();
		var period = $('#numPeriodUnder').combobox('getValue').trim();
		var errMsg = '';
		if (parseInt(period) < 0 || parseInt(period) > AsoTarget._MAX_CYCLE_NUM){
			errMsg = 'Chu kỳ không hợp lệ. Vui lòng chọn lại';
		}
		if (errMsg == '' && !Utils.isEmpty(curYear) && !isNaN(curYear) && parseInt(year) < parseInt(curYear)) {
			errMsg = 'Năm phải lớn hơn hay bằng năm hiện tại. Vui lòng chọn lại';
		}
		if (errMsg == '' && !Utils.isEmpty(curYear) && !isNaN(curYear) && parseInt(year) == parseInt(curYear) && parseInt(period) < parseInt(curPeriod)) {
			errMsg = 'Chu kỳ phải lớn hơn hay bằng chu kỳ hiện tại. Vui lòng chọn lại';
		}
		var type = $('#curTypeAso').val();
		var errType = '';
		if (type == AsoTarget._TYPE_ASO_SKU) {
			errType = 'SKU';
		} else {
			errType = 'Ngành hàng con';
		}
		if (AsoTarget._lstStaffInfo == null || AsoTarget._lstStaffInfo.length == 0) {
			errMsg = 'Danh sách ' + errType + ' rỗng.'
		}
		var lstData = [];
		var i = 1;
		$('#gridContainer input.aso').each(function(index, item){
			var id = $(this).attr("id");
			obj = {};
			obj.key = id;
			var valueInput = Utils.returnMoneyValue($(this).val());
			if (isNaN(valueInput)) {
				errMsg = 'Dòng thứ ' + i + ' giá trị chỉ tiêu không phải là số';
				$('#errExcelMsg').html(errMsg).show();
				return false;
			}
			obj.value = valueInput;
			lstData.push(obj);
			i++;
		});
		if (errMsg.length > 0) {
			$('#errExcelMsg').html(errMsg).show();
			return false;
		}
		
		var dataModel = {};
		convertToSimpleObject(dataModel, lstData, 'lstAsoPlanObject');
		dataModel.shopCode = $('#curShopCodeAso').val();
		dataModel.yearPeriod = $('#yearPeriodUnder').val().trim();
		dataModel.numPeriod = $('#numPeriodUnder').combobox('getValue');
		dataModel.staffCode = $('#staffCodeUnder').val();
		dataModel.routingId = $('#routingIdUnder').val();
		dataModel.type = type;
		Utils.saveData(dataModel, '/aso/save-info', null, 'errExcelMsg');
	},

	/**
	 * reset phan bo loai Aso
	 * @author vuongmq
	 * @since 22/10/2015
	 */
	resetAso: function(){
		hideAllMessage();
		var errMsg = '';
		var nvbh = $('#staffCodeUnder').val();
		if (Utils.isEmpty(nvbh) || nvbh == activeType.DELETE) {
			errMsg = 'Không có nhân viên bán hàng';
		}
		if (errMsg.length == 0) {
			if (Utils.isEmpty($('#routingIdUnder').val())) {
				errMsg = 'Không có tuyến của nhân viên bán hàng';
			}
		}
		var type = $('#curTypeAso').val();
		var errType = '';
		if (type == AsoTarget._TYPE_ASO_SKU) {
			errType = 'SKU';
		} else {
			errType = 'Ngành hàng con';
		}
		if (errMsg.length == 0) {
			if (AsoTarget._lstStaffInfo == null || AsoTarget._lstStaffInfo.length == 0) {
				errMsg = 'Danh sách ' + errType + ' rỗng.'
			}
		}
		if (errMsg.length > 0) {
			$('#errExcelMsg').html(errMsg).show();
			return false;
		}
		var message = 'Bạn có muốn reset phân bổ loại ' + errType + ' về 0?';
		var dataModel = {};
		dataModel.shopCode = $('#curShopCodeAso').val();
		dataModel.yearPeriod = $('#yearPeriodUnder').val().trim();
		dataModel.numPeriod = $('#numPeriodUnder').combobox('getValue');
		dataModel.staffCode = $('#staffCodeUnder').val();
		dataModel.routingId = $('#routingIdUnder').val();
		dataModel.type = type;
		Utils.addOrSaveData(dataModel, '/aso/reset-aso', null, 'errExcelMsg', function(data){
			var tm = setTimeout(function(){
				//Load lai danh sach ASO
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				AsoTarget.search();
			 }, 1500);
		}, null, null, null, message);
	},

	/**
	 * imporrt confirm loai Aso
	 * @author vuongmq
	 * @since 22/10/2015
	 */
	openImportConfirm: function(){
		hideAllMessage();
		var excelURI = $('#excelFile').val();
 		if (excelURI == undefined || excelURI == null || excelURI.trim().length == 0) {
 			$('#errExcelMsg').html('Vui lòng chọn tập tin Excel').show();
 			return false;
 		}
		$('#popupImportAso').dialog({
			title: 'Chọn loại import',
			width: 350,
			height:'auto',
			onOpen: function(){
				$('#rbTypeSku').prop("checked", true);
			}
		});
		$('#popupImportAso').dialog('open');
	},

	/**
	 * import excel ASO
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	importExcel: function() {
 		var typeRadio = $('input:radio[name=importRadio]:checked').val();
 		if (typeRadio == null || typeRadio == '') {
 			$('#errExcelMsg').html('Vui lòng chọn loại phân bổ aso').show();
 			return false;
 		}
 		$('#typeId').val(typeRadio);
 		$('#popupImportAso').dialog('close');
 		var params = {};
 		hideAllMessage();
		Utils.importExcelUtils(function(data) {
			$('#errExcelMsg').val("").change();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
			} else {
				$('#successMsg').html(jsp_common_save_success).show();
				var tm = setTimeout(function(){
					//Load lai danh sach ASO
					$('#successMsg').html("").hide();
					clearTimeout(tm);
					AsoTarget.search();
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg', true);
 	},

	/**
	 * Export excel ASO
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	exportExcel: function() {
 		hideAllMessage();
		var errMsg = '';
		var nvbh = $('#staffCodeUnder').val();
		if (Utils.isEmpty(nvbh) || nvbh == activeType.DELETE) {
			errMsg = 'Không có nhân viên bán hàng';
		}
		if (errMsg.length == 0) {
			if (Utils.isEmpty($('#routingIdUnder').val())) {
				errMsg = 'Không có tuyến của nhân viên bán hàng';
			}
		}
		if (errMsg.length > 0) {
			$('#errExcelMsg').html(errMsg).show();
			return false;
		}
 		var params = AsoTarget._paramSearch;
 		ReportUtils.exportReport('/aso/exportExcel', params, 'errExcelMsg');
 	},

 	/**
	 * Lay danh sach cycle theo nam header
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	initCycleInfo: function() {
 		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 113, function(rec) {
 					if (rec != undefined && rec != null && rec.num != undefined && rec.num != null) {
 						AsoTarget.changeNumPeriodAso(rec.num);
 					}
 				});
 				Utils.bindPeriodCbx('#numPeriodUnder', jQuery.extend(true, [], data.lstCycle), data.currentNum, 113);
 			}
 		});
 	},

	/**
	 * khi change yearPeriodUnder Lay chu ky cua combobox Under(numPeriodUnder) 
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	initCycleInfoUnder: function() {
 		var par = {};
 		par.year = $('#yearPeriodUnder').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriodUnder', data.lstCycle, data.currentNum, 113);
 			}
 		});
 	},

 	/**
	 * set year
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	setCurrentYear: function(year) {
 		$('#yearPeriod').val(year);
 		$('#yearPeriod').change();
 		$('#yearPeriodUnder').val(year);
 		$('#yearPeriodUnder').change();
 	},
 	
 	 /**
	 * Xu ly khi thay doi don vi; lay danh sach 
	 * @author vuongmq
	 * @since 19/10/2015
	 */
	changeShop: function(shopId) {
		if (shopId != undefined && shopId != null) {
			$.getJSON('/aso/get-list-staff?shopId=' + shopId, function(list) {
				$('#staffCode').html('');
				if (list != undefined && list != null && list.length > 0) {
					if (list.length > 1) { 
						$('#staffCode').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode(sale_plan_choose_staff) + '</option>');
					}
					for (var i = 0; i < list.length; i++) {
						$('#staffCode').append('<option value = "' + Utils.XSSEncode(list[i].staffCode) + '">' + Utils.XSSEncode(list[i].staffCode + " - " + list[i].staffName) + '</option>');  
					}
				} else {
					$('#staffCode').append('<option value = "-1">' + Utils.XSSEncode(sale_plan_no_staff) + '</option>');
				}
				$('#staffCode').change();
				$('#staffCodeUnder').html('');
				if (list != undefined && list != null && list.length > 0) {
					if (list.length > 1) { 
						$('#staffCodeUnder').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode(sale_plan_choose_staff) + '</option>');
					}
					for (var i = 0; i < list.length; i++) {
						$('#staffCodeUnder').append('<option value = "' + Utils.XSSEncode(list[i].staffCode) + '">' + Utils.XSSEncode(list[i].staffName) + '</option>');  
					}
				} else {
					$('#staffCodeUnder').append('<option value = "-1">' + Utils.XSSEncode(sale_plan_no_staff) + '</option>');
				}
				$('#staffCodeUnder').change();
			});
		}
	},
	
	/**
	 * Xu ly khi thay doi nhan vien 
	 * @author vuongmq
	 * @since 19/10/2015
	 */
	changeStaff: function(typeView){
		if (typeView == activeType.STOPPED) {
			var staffCode = $('#staffCode').val();
			$('#staffCodeUnder').val(staffCode);
			$('#staffCodeUnder').change();
			// load tuyen cua NVBH
			$('#routingStaff').html('');
			$('#routingId').val('');
			$.getJSON('/aso/get-routing-staff?staffCode=' + staffCode, function(data) {
				if (data != undefined && data != null && data.routingId > 0) {
					$('#routingStaff').html(Utils.XSSEncode(data.routingStaff));
					$('#routingId').val(data.routingId);
				}
			});
		} else {
			var staffCode = $('#staffCodeUnder').val();
			// load tuyen cua NVBH Under
			$('#routingStaffUnder').html('');
			$('#routingIdUnder').val('');
			$.getJSON('/aso/get-routing-staff?staffCode=' + staffCode, function(data) {
				if (data != undefined && data != null && data.routingId > 0) {
					$('#routingStaffUnder').html(Utils.XSSEncode(data.routingStaff));
					$('#routingIdUnder').val(data.routingId);
				}
			});
		}
	},

	/**
	 * Xu ly khi thay doi numPeriod (numPeriodUnder thay doi theo)
	 * @author vuongmq
	 * @since 26/10/2015
	 */
	changeNumPeriodAso: function(numPeriod){
		if (numPeriod == undefined || numPeriod == null) {
			numPeriod = $('#numPeriod').combobox('getValue');
		}
		$('#numPeriodUnder').combobox('setValue', numPeriod);
	},
}
/*
 * END OF FILE - /web/web/resources/scripts/business/aso/vnm.aso-target.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

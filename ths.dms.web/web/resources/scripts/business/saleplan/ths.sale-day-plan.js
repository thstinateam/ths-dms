var SaleDayPlan = {
	_daysInMonth: [31,28,31,30,31,30,31,31,30,31,30,31],
	_dayInWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
	_VIEW_BY_YEAR: 0,
	_VIEW_BY_MONTH: 1,
	_xhrSave : null,
	_hideErrExcelMsg: 0,
	viewTabMonth:0,
	getDaysInMonth: function(month,year){
	    if ((month==1)&&(year%4==0)&&((year%100!=0)||(year%400==0))){
	      return 29;
	    }else{
	      return SaleDayPlan._daysInMonth[month];
	    }
	},
	getCalendarByMonth: function(month,year,viewType){
		$('#dataBox').hide();
		var vt = SaleDayPlan._VIEW_BY_YEAR;
		if(viewType!= null && viewType!= undefined){
			vt = viewType;
		}
		var html = new Array();
		if(vt == SaleDayPlan._VIEW_BY_YEAR){
			html.push('<h5>Tháng '+ month + '</h5>');
			html.push('<p id="numExDay_'+ month +'" class="Warning3Style"></p>');
			html.push('<div class="GeneralTable Table21Section">');
		} else {
			html.push('<div class="GeneralTable Table22Section">');
		}
		html.push('<table width="100%" border="0" cellspacing="0" cellpadding="0">');
		html.push('<thead>');
		html.push('<tr>');
		html.push('<th>CN</th>');
		html.push('<th>Th2</th>');
		html.push('<th>Th3</th>');
		html.push('<th>Th4</th>');
		html.push('<th>Th5</th>');
		html.push('<th>Th6</th>');
		html.push('<th class="ColsTh7 ColsThEnd">Th7</th>');
		html.push('</tr>');
		html.push('</thead>');
		html.push('<tbody>');		
		// insert day
		var firstDayDate=new Date(year,month-1,1);
        var firstDay=firstDayDate.getDay();
        var maxCell = 42;
        // get days of previous month
        var preYear = year;
        if(firstDay > 0){
        	html.push('<tr>');
        	var preMonth = month - 1;        	
        	if(preMonth < 1){
        		preMonth = 12;
        		preYear = year - 1;
        	}
        	for(var i=0;i<firstDay;i++){
        		var pd = SaleDayPlan.getDaysInMonth(preMonth-1, preYear) - firstDay + i + 1;
        		html.push('<td class="ExtCols">'+ pd +'</td>');
        	}
        }
        
        //get days in month
        var dOM = SaleDayPlan.getDaysInMonth(month-1, preYear) + firstDay;
        for (var j=firstDay;j<dOM;j++){
        	var holClass = '';
        	if (j%7==0){
        		html.push('<tr>');
        		holClass = ' SunCols';
        		
        	}
        	html.push('<td class="DayInfo'+ holClass +'" id="tm_'+(j-firstDay+1)+'_'+ month +'" onclick="SaleDayPlan.showDate(event,'+(j-firstDay+1)+','+ month +','+ year +',this,'+ j%7 +')">'+ (j-firstDay+1) +'</td>');        	        	
        	if (j%7==6){
        		html.push('</tr>');
        	}
        }
        
        // get days of next month   
        if(dOM < (7*5)){
        	maxCell = 7*5;
        }
        if(dOM != (7*5)){
        	var nxtMonth = month + 1;
        	var nxtYear = year;
        	if(nxtMonth > 12){
        		nxtMonth = 1;
        		nxtYear = year + 1;
        	}
        	var k = 0;
        	for(var i=dOM;i<maxCell;i++){
        		k++;
        		//var pd = SaleDayPlan.getDaysInMonth(nxtMonth-1, nxtYear) - firstDay + i + 1;
        		html.push('<td class="ExtCols">'+ k +'</td>');
        	}
        	html.push('</tr>');
        } 
		//////////////		
		html.push('</tbody>');    
		html.push('</table">');
		html.push('</div">');
		return html.join("");
	},
	loadCalendar: function(){
		var year = $('#currentYear').val().trim();
		var month = $('#currentMonth').val().trim();
		for(var i=1;i<=12;i++){
			$('#m' + i).html(SaleDayPlan.getCalendarByMonth(i, year));
		}
		$('#monthContent').html(SaleDayPlan.getCalendarByMonth(i, year,SaleDayPlan._VIEW_BY_MONTH));
		SaleDayPlan.getSaleDayInfo(0, year);	
		SaleDayPlan.getSaleDayInfo(month, year);
		SaleDayPlan.setCurrentDate();
	},
	currentYearChanged: function(){
		var year = $('#currentYear').val().trim();
		$('#yearText').text(year);
		SaleDayPlan.loadCalendar();
//		SaleDayPlan.currentMonthChanged();
	},
	showYearTab: function(){
		SaleDayPlan.viewTabMonth = 0;
		$('#currentYear').val($('#currentYearMonthView').val().trim());
		$('#currentYear').change();
		$('#viewByMonth').removeClass('Active');
		$('#viewByYear').addClass('Active');
		$('#viewMonth').hide();
		$('#viewYear').show();
	},
	showMonthTab: function(){
		SaleDayPlan.viewTabMonth = 1;
		$('#currentYearMonthView').val($('#currentYear').val().trim());
		$('#currentYearMonthView').change();
		$('#viewByYear').removeClass('Active');
		$('#viewByMonth').addClass('Active');
		$('#viewYear').hide();
		$('#viewMonth').show();		
		var year = $('#currentYear').val().trim();
		var month = $('#currentMonth').val().trim();
		$('#monthContent').html(SaleDayPlan.getCalendarByMonth(month, year,SaleDayPlan._VIEW_BY_MONTH));
		//TODO
		SaleDayPlan.getSaleDayInfo(month, year);
		SaleDayPlan.setCurrentDate();
	},
	currentMonthChanged: function(){
		var year = $('#currentYearMonthView').val().trim();
		var month = $('#currentMonth').val().trim();
		$('#monthContent').html(SaleDayPlan.getCalendarByMonth(month, year,SaleDayPlan._VIEW_BY_MONTH));		
		SaleDayPlan.getSaleDayInfo(month, year);
		SaleDayPlan.setCurrentDate();
	},
	showDate: function(event,day,month,year,obj,dow){
		if (dow != 0) {
			$('#errMsg').html('').hide();
			$('#errExcelMsg').html('').hide();
			$('#dataBox').show();
			var left = event.pageX;
			var top = event.pageY;
			var w_box = 260;
			var h_box = 140;
			if ($('#divBtnUpdateSaleDay').is(':hidden')) {
				h_box = 108;
			}
			top = top - h_box;
			left = left - (w_box / 2);
			$('#dataBox .BoxEditState').css({
				'left' : left,
				'top' : top
			});
			var id = "tm_" + day + '_' + month;
			if (day < 10) {
				day = '0' + day;
			}
			if (month < 10) {
				month = '0' + month;
			}
			var dateString = SaleDayPlan._dayInWeek[dow] + ' ' + day + '/' + month + '/' + year;
			var urlTmp = '/rest/catalog/check-system-time.json?time=' + day + '/' + month + '/' + year;
			$.getJSON(urlTmp, function(data) {
				if (data != undefined && data.value == 1) {
					$('#divBtnUpdateSaleDay').show();
				} else {
					$('#divBtnUpdateSaleDay').hide();
				}
			});
			$('#dateValue').val(dateString);
			if ($('#' + id).hasClass('OffCols')) {
				setSelectBoxValue('dateType', 1);
				setSelectBoxValue('reasonGroup', $('#' + id).attr('data'));
			} else {
				setSelectBoxValue('dateType', 0);
				setSelectBoxValue('reasonGroup', -2);
			}
		}
	},
	saveDateInfo: function(){
		var msg = '';
		var dateValue = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('txtShopCode','Đơn vị');
		if(msg.length  > 0){	
			$('#txtShopCode').focus();
			$('#errMsg').html(msg).show();
			return false;
		}
		msg = Utils.getMessageOfRequireCheck('dateValue','Ngày');
		if(msg.length  == 0){			
			var arrItems = $('#dateValue').val().trim().split(' ');
			if(arrItems.length == 1){
				dateValue = arrItems[0].trim();
			} else {
				dateValue = arrItems[1].trim();
			}
			if(!Utils.isDate(dateValue, '/')){
				msg = format(msgErr_invalid_format_date,'Ngày');
				$('#dateValue').focus();
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('dateType','Trạng thái',true);
			}
			if(msg.length == 0 && $('#dateType').val().trim()== '1'){
				msg = Utils.getMessageOfRequireCheck('reasonGroup','Lý do',true);
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			if(msg.length == 0){
				arrDays = dateValue.split('/');
				if(arrDays.length > 0){
					var selDate = new Date(arrDays[1]+ '/' + arrDays[0] + '/' + arrDays[2]);
					//var day = parseInt(arrDays[0]);
					if(selDate.getDay() == 0){
						msg = 'Chủ nhật là ngày nghỉ. Bạn không thao tác được trên ngày này';
						$('#dateValue').focus();
					}
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.dateValue = dateValue;
		dataModel.dateType = $('#dateType').val().trim();
		dataModel.reasonCode = $('#reasonGroup').val().trim();		
		dataModel.shopCodeStr = $('#txtShopCode').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/sale-plan/day/save", SaleDayPlan._xhrSave, null, null, function(data){
			if(!data.error){
				var arrDate = $('#dateValue').val().trim().split('/');
				SaleDayPlan.getSaleDayInfo(arrDate[1], arrDate[2]);
				$('#dataBox').hide();
			}
		});		
		return false;
	},
	getSaleDayInfo: function(month,year){
		if (SaleDayPlan._hideErrExcelMsg <= 0) {
			$('#errExcelMsg').hide();
		} else {
			SaleDayPlan._hideErrExcelMsg--;
		}
		if(/^0\d/.test(month)){
			month = month.replace('0','');
		}
		if(parseInt(month) == 0){
			$('.DayInfo').each(function(){
				$(this).removeClass('OffCols');
				$(this).removeAttr('data');
			});
		} else {
			$('#m'+parseInt(month)+' .DayInfo,#monthContent .DayInfo').each(function(){
				$(this).removeClass('OffCols');
				$(this).removeAttr('data');
			});
		}
		$('#numWorkDayByMonth').text('');
		var shopCode = $('#txtShopCode').val().trim();
		$.getJSON('/rest/catalog/sale-day/except-day/'+month+ '/' + year +'/list.json?shopCode='+shopCode, function(data){						
			for(var i=0;i<data.length-1;i++){	
				var id = data[i].content1;
				var value = data[i].content2;
				if($('#viewYear #' + id)!= null){
					$('#viewYear #' + id).addClass('OffCols');
					$('#viewYear #' + id).attr('data',value);
				}
				if($('#viewMonth #' + id)!= null){
					$('#viewMonth #' + id).addClass('OffCols');
					$('#viewMonth #' + id).attr('data',value);
				}
			}
			if(data[data.length-1] != null){
				var objExDay = data[data.length-1];
				var arrDay = [objExDay.content1,objExDay.content2,objExDay.content3,objExDay.content4,objExDay.content5,objExDay.content6,objExDay.content7,objExDay.content8,objExDay.content9,objExDay.content10,objExDay.content11,objExDay.content12];
				if(parseInt(month) == 0){
					for(var i=0;i<arrDay.length;i++){
						var numWorkDay = SaleDayPlan.getDaysInMonth(i,year) - SaleDayPlan.getSundaysInMonth((i+1), year).length - arrDay[i]; 
						if($('#viewYear #numExDay_' + (i+1)).html()!= null && numWorkDay> 0){
							$('#viewYear #numExDay_' + (i+1)).html('(Có '+ numWorkDay +' ngày làm việc)');
						} else if($('#viewYear #numExDay_' + (i+1)).html()!= null){
							$('#viewYear #numExDay_' + (i+1)).html('');						
						}					
					}
				}else{
					var numWorkDay = SaleDayPlan.getDaysInMonth(month-1,year) - SaleDayPlan.getSundaysInMonth(month, year).length - arrDay[month-1];
					if(numWorkDay > 0){
						$('#viewYear #numExDay_' + (month)).html('(Có '+ numWorkDay +' ngày làm việc)');
						$('#numWorkDayByMonth').text('(Có '+ numWorkDay +' ngày làm việc)');
					} else {
						$('#viewYear #numExDay_' + (month)).html('');
					}
				}
				
			}
		});
	},
	setCurrentDate: function(){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(parseInt($('#currentYear').val().trim()) == year){
			var id="tm_" + day + '' + month;
			$('#viewYear #' + id).addClass('NowCols');
			$('#viewMonth #' + id).addClass('NowCols');
		}
	},
	getSundaysInMonth: function(month,year){		
		var days = new Date(year,month,0 ).getDate();
		var fDate = new Date( month +'/01/'+ year );
		var sundays = [ 8 - fDate.getDay() ];
		for ( var i = sundays[0] + 7; i <= days; i += 7 ) {
			sundays.push( i );
		}
		if(fDate.getDay() == 0){
			sundays.push(1);
		}
		return sundays;
	},
	exportExcel:function(){
		$('.ErrorMsgStyle').html('').hide();
		$('#errMsg').html('').hide();	
		$('#errExcelMsg').html('').hide();
		var month = 0;
		var year = 0;
		if(SaleDayPlan.viewTabMonth == 0){
        	 year = $('#currentYear').val().trim();
        }else{
        	 year = $('#currentYearMonthView').val().trim();
    		 month = $('#currentMonth').val().trim();
        }
		var msg = Utils.getMessageOfRequireCheck('txtShopCode','Mã đơn vị');
		if(msg.length > 0){
			$('#txtShopCode').focus();
			$('#errMsgSearch').html(msg).show();			
			return false;
		}	
		var params = new Object();
		params.month = month;
		params.year = year;
		params.shopCode = $('#txtShopCode').val().trim();
		var url = "/sale-plan/day/export-excel";
		showLoadingIcon();
		ReportUtils.exportReport(url,params,'errExcelMsg');
 		return false;
 	},
 	importExcel:function(){
 		$('.SuccessMsgStyle').html('').hide();
        $('.ErrorMsgStyle').html('').hide();
        
        Utils.importExcelUtils(function(data){
              $('#excelFile').val("").change();
              $('#fakefilepc').val("").change();
              if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
            	  var arrDate = $('#dateValue').val().trim().split('/');
					SaleDayPlan.getSaleDayInfo(arrDate[1], arrDate[2]);
                    var tm = setTimeout(function(){
                  	$('#errExcelMsg').html(data.message.trim()).change().show();
                        clearTimeout(tm);
                  }, 1500); 
              }else{
                    $('#successMsg').html("Lưu dữ liệu thành công").show();
                    var tm = setTimeout(function(){
                          //Load lai danh sach quyen
                          $('#successMsg').html("").hide();
                          clearTimeout(tm);
                    }, 1500);
                    if(SaleDayPlan.viewTabMonth == 0){
                    	var year = $('#currentYear').val().trim();
                		SaleDayPlan.getSaleDayInfo(0, year);	
                    }else{
                    	var year = $('#currentYearMonthView').val().trim();
                		var month = $('#currentMonth').val().trim();
                		SaleDayPlan.getSaleDayInfo(month, year);
                    }
              }
        }, 'importFrm', 'excelFile', 'errExcelMsg');
 	},
 	fillCodeForInputTextByDialogSearch2 : function (txt, id, code, name, isShow) {
 		$('.ErrorMsgStyle').html('').hide();
		if (isShow == undefined || isShow == null) {
			$('#'+txt.trim()).val(code+' - '+name);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.code == isShow) {
			$('#'+txt.trim()).val(code);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.name == isShow) {
			$('#'+txt.trim()).val(name);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.id == isShow) {
			$('#'+txt.trim()).val(id);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else {
			$('#'+txt.trim()).val("");
			$('#'+txt.trim()).attr(txt + '_0', 0);
		}
		$('#common-dialog-search-tree-in-grid-choose-single').dialog("close");
		if(SaleDayPlan.viewTabMonth == 1){
			SaleDayPlan.currentMonthChanged();
		}else{
			SaleDayPlan.loadCalendar();
		}
	},
 	afterImportExcel: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();	
		if (statusText == 'success') {
	    	$("#responseDiv").html(responseText);
	    	var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
    		if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    		$('#fakefilepc').val('');
				$('#excelFile').val('');
				return ;
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
	    		var numFail = parseInt($('#numFail').html().trim());
	    		var fileNameFail = $('#fileNameFail').html();
	    		var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    		if(numFail > 0){
	    			mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    		}
	    		$('#errExcelMsg').html(mes).show();
	    		SaleDayPlan._hideErrExcelMsg = 3;
	    		$('#currentYear').change();
	    	}
	    	$('#fakefilepc').val('');
			$('#excelFile').val('');
	    }
	}
};
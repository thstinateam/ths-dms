var ViettelMap = {
	_DEF_LAT: 10.79418775,
	_DEF_LNG: 106.65682978,
	_DEF_LAT_BIG:  15.744675,
	_DEF_LNG_BIG: 107.515136,
	_DEF_ALL_ZOOM: 4,
	_DEF_ZOOM: 10,
	_DEF_ZOOM_BIG: 6,
	_map: null,
	_marker: null,
	_listMarker: null,
	_indexMarker: 0,
	sellerPositionMap: null,
	visitResultMap:null,
	_currentInfoWindow:null,
	_listOverlay:null,
	clearOverlays: function() { 
		if(ViettelMap._listOverlay != null && ViettelMap._listOverlay != undefined) {
			if(ViettelMap._listOverlay instanceof Map) {
				for(var i = 0; i < ViettelMap._listOverlay.size(); i++) {
					var obj = ViettelMap._listOverlay.valArray[i];
					obj.setMap(null);
				}
			} else if(ViettelMap._listOverlay instanceof Array) {
				for(var i = 0; i < ViettelMap._listOverlay.length; i++) {
					var obj = ViettelMap._listOverlay[i];
					obj.setMap(null);
				}
			}
		}
		ViettelMap._listOverlay = null;
	},
	fitOverLay: function() {
		var bound = null;
		if(ViettelMap._listOverlay != null && ViettelMap._listOverlay != undefined) {
			if(ViettelMap._listOverlay instanceof Map) {
				for(var i = 0; i < ViettelMap._listOverlay.size(); i++) {
					var obj = ViettelMap._listOverlay.get(ViettelMap._listOverlay.keyArray[i]);
					if(obj instanceof viettel.LabelMarker) {
						if(ViettelMap.isValidLatLng(obj.getPosition().lat(), obj.getPosition().lng())) {
							var latlng = new viettel.LatLng(obj.getPosition().lat(), obj.getPosition().lng());
							bound = ViettelMap.getBound(bound, latlng);
						}
					} else {
						if(ViettelMap.isValidLatLng(obj.lat, obj.lng)) {
							var latlng = new viettel.LatLng(obj.lat, obj.lng);
							bound = ViettelMap.getBound(bound, latlng);
						}
					}
				}
			} else if(ViettelMap._listOverlay instanceof Array) {
				for(var i = 0; i < ViettelMap._listOverlay.length; i++) {
					var obj = ViettelMap._listOverlay[i];
					if(obj instanceof viettel.LabelMarker) {
						if(ViettelMap.isValidLatLng(obj.getPosition().lat(), obj.getPosition().lng())) {
							var latlng = new viettel.LatLng(obj.getPosition().lat(), obj.getPosition().lng());
							bound = ViettelMap.getBound(bound, latlng);
						}
					} else {
						if(ViettelMap.isValidLatLng(obj.lat, obj.lng)) {
							var latlng = new viettel.LatLng(obj.lat, obj.lng);
							bound = ViettelMap.getBound(bound, latlng);
						}
					}
				}
			}
		}
		if(bound != null) {
			ViettelMap._map.fitBounds(bound);
		}
	},
	getBound: function(bound, latlng) {
		if(bound == null || bound == undefined) {
			bound = new viettel.LatLngBounds(latlng, latlng);
		} else {
			var lat = latlng.lat();
			var lng = latlng.lng();
			
          	var swLat = bound.getSouthWest().lat();
          	var swLng = bound.getSouthWest().lng();
          	var neLat = bound.getNorthEast().lat();
          	var neLng = bound.getNorthEast().lng();
          
			if(bound.getSouthWest().lat() > lat) {
				swLat = lat;
			}
			if(bound.getSouthWest().lng() > lng) {
				swLng = lng;
			}
			if(bound.getNorthEast().lat() < lat) {
				neLat = lat;
			}
			if(bound.getNorthEast().lng() < lng) {
				neLng = lng;
			}
			
			var sw = new viettel.LatLng(swLat, swLng);
			var ne = new viettel.LatLng(neLat, neLng);
			
			bound = new viettel.LatLngBounds(sw, ne);
		}
		return bound;
	},
	CalculateDistance:function(i,k){
		var n,l,h;var j;var c;var e;var a;var m;
		var d=i.lat();
		var b=i.lng();
		var g=k.lat();
		var f=k.lng();
		j=d*(Math.PI/180);
		c=b*(Math.PI/180);
		e=g*(Math.PI/180);
		a=f*(Math.PI/180);
		n=b-f;m=n*(Math.PI/180);
		h=Math.sin(j)*Math.sin(e)+Math.cos(j)*Math.cos(e)*Math.cos(m);
		h=Math.acos(h);
		l=h*180/Math.PI;
		l=l*60*1.1515;
		l=l*1.609344*1000;
		return Math.round(l);
	},
	CalculateDistanceFormat : function(distance) {
		if (distance >= 1000) {
			return roundNumber((distance/1000),2);
		}
	},
	addMarkerStaff:function(point){
		var map = ViettelMap._map;
		if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new viettel.LatLng(point.lat, point.lng);
			var info="<div id='info"+point.staffId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
			var title=point.staffName;
			var image=point.image;
			var accuracy = point.accuracy;
			var createTime = point.createTime;
			
			var level = ViettelMap._map.getZoom();
			var markerContent = '';
			var markerTop = "<div id='top"+point.staffId+"' class='Top' style='text-align : center;position: relative;top: -15px;'></div>";
			var markerBotoom = "<div id='bottom' style='position: relative; bottom: -28px;'></div>";
			if(level >= 13) {
				markerTop = "<div id='top"+point.staffId+"' class='Top' style='text-align:center;position: relative;top: -15px;'><strong style='color:red;font-size: 12px;' class='titleShop'>" + Utils.XSSEncode(point.shopCode) + "</strong></div>";
			} else {
				markerTop = "<div id='top"+point.staffId+"' class='Top' style='display:none; text-align: center;position: relative;top: -15px;'><strong style='color:red;font-size: 12px;' class='titleShop'>" + Utils.XSSEncode(point.shopCode) + "</strong></div>";
			}
			if(level >= 16) {
				markerBotoom = "<div id='bottom"+point.staffId+"' class='Bottom' style='position: relative; bottom: -28px; text-align:center;'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(point.staffName) + "</strong></div>";
			} else {
				markerBotoom = "<div id='bottom"+point.staffId+"' class='Bottom' style='position: relative; bottom: -28px; text-align:center; display:none;'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(point.staffName) + "</strong></div>";
			}
			markerContent = "<div id='marker"+point.staffId+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'>"+markerTop+markerBotoom+"</div>";
			
			
			var marker = new viettel.LabelMarker({
				icon:{
					url : image,
					size : {height : 39, width : 20},
					scaledSize : {height : 39, width : 20}
				},
				position : pt,
				map : ViettelMap._map,
				labelContent : markerContent,
				labelClass : "MarkerLabel",
				labelVisible : true,
				draggable : false,
				labelAnchor : new viettel.Point(25, 0)
			});
			marker.staffId = point.staffId;
			marker.lat = point.lat;
			marker.lng = point.lng;
			marker.accuracy = point.accuracy;
			marker.createTime = point.createTime;
			
			$('#marker'+point.staffId).parent().prev().css('z-index', 10000000);
			
			$('#marker'+point.staffId).parent().prev().bind('click', function() {
				ViettelMap.showWindowInfo(point.id, point.lat, point.lng, point.accuracy, point.createTime);
			});
			
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
		ViettelMap.hideShowTitleMarker();
	},
	// vuongmq; 19/08/2015; Lay them DK staffId
	addMarkerCust: function(point, staffId) {
		var map = ViettelMap._map;
		if (ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new viettel.LatLng(point.lat, point.lng);
			
			var image = '';
			if (point.image == 'Customers1Status') { //co don hang
				image = '/resources/images/Mappin/icon_circle_blue.png';
			} else if(point.image == 'Customers2Status') { //chua ghe tham
				image = '/resources/images/Mappin/icon_circle_green.png';
			} else if(point.image == 'Customers3Status') { //dang ghe tham
				image = '/resources/images/Mappin/icon_circle_orange.png';
			} else if(point.image == 'Customers4Status') { //Ghe tham khong co don dat hang
				image = '/resources/images/Mappin/icon_circle_red.png';
			} else if(point.image == 'Customers5Status') { //kh ngoai tuyen
				image = '/resources/images/Mappin/icon_circle_yellow.png';
			}
			
			var marker = null;
			
			if (point.ordinalVisit>9) {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 41px; top: -24px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else if(0<=point.ordinalVisit<=9) {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 47px; top: -25px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 47px; top: -25px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			}
			
			marker.customerId = point.id;
			marker.lat = point.lat;
			marker.lng = point.lng;
			
			viettel.Events.addListener(marker, "click", function(evt) {
				//SuperviseSales.showCustomerDetail(this.customerId);
				SuperviseSales.showCustomerDetail(this.customerId, staffId); // vuongmq; 19/08/2015; Lay them DK staffId
			});
			
			if (ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
	},
	addMarkerCustKA:function(point){
		var map = ViettelMap._map;
		if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new viettel.LatLng(point.lat, point.lng);
			
			var image = '';
			if(point.image == 'Customers1Status') {
				image = '/resources/images/Mappin/icon_circle_blue.png';
			} else if(point.image == 'Customers2Status') {
				image = '/resources/images/Mappin/icon_circle_green.png';
			} else if(point.image == 'Customers3Status') {
				image = '/resources/images/Mappin/icon_circle_orange.png';
			} else if(point.image == 'Customers4Status') {
				image = '/resources/images/Mappin/icon_circle_red.png';
			} else if(point.image == 'Customers5Status') {
				image = '/resources/images/Mappin/icon_circle_yellow.png';
			}
			
			var marker = null;
			
			if(point.ordinalVisit>9) {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 41px; top: -24px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else if(0<=point.ordinalVisit<=9) {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 45px; top: -25px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 47px; top: -25px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			}
			
			
			marker.customerId = point.id;
			marker.lat = point.lat;
			marker.lng = point.lng;
			
			viettel.Events.addListener(marker, "click", function(evt) {
				SupperviseSalesKA.showCustomerDetail(this.customerId);
			});
			
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
	},
	addMarkerCustMT:function(point){
		var map = ViettelMap._map;
		if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new viettel.LatLng(point.lat, point.lng);
			
			var image = '';
			if(point.image == 'Customers1Status') {
				image = '/resources/images/Mappin/icon_circle_blue.png';
			} else if(point.image == 'Customers2Status') {
				image = '/resources/images/Mappin/icon_circle_green.png';
			} else if(point.image == 'Customers3Status') {
				image = '/resources/images/Mappin/icon_circle_orange.png';
			} else if(point.image == 'Customers4Status') {
				image = '/resources/images/Mappin/icon_circle_red.png';
			} else if(point.image == 'Customers5Status') {
				image = '/resources/images/Mappin/icon_circle_yellow.png';
			}
			
			var marker = null;
			
			if(point.ordinalVisit>9) {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 41px; top: -24px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else if(0<=point.ordinalVisit<=9) {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 45px; top: -25px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else {
				marker = new viettel.LabelMarker({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 47px; top: -25px;">'+point.ordinalVisit+'</span>'+'<span style="position: relative; left: 35px; top: -17px;" class="CustomerTimeVisit">'+point.ordinalAndTimeVisitReal+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			}
			
			
			marker.customerId = point.id;
			marker.lat = point.lat;
			marker.lng = point.lng;
			
			viettel.Events.addListener(marker, "click", function(evt) {
				SupperviseSalesMT.showCustomerDetail(this.customerId);
			});
			
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
	},
	addMarkerCustomerKARouting:function(point){
		var map = Vietbando._map;
		if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new viettel.LatLng(point.lat, point.lng);
			var marker = null;
			if(isNaN(point.ordinalVisit) && point.ordinalVisit.length == 1) {
				marker = new viettel.LabelMarker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 47px; top: -25px;">'+point.ordinalVisit+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else if(!isNaN(point.ordinalVisit) && point.ordinalVisit <10) {
				marker = new viettel.LabelMarker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 45px; top: -25px;">'+point.ordinalVisit+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else {
				marker = new viettel.LabelMarker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 41px; top: -25px;">'+point.ordinalVisit+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			}
			marker.customerId = point.customerId;
			marker.lat = point.lat;
			marker.lng = point.lng;
			viettel.Events.addListener(marker, "click", function(evt) {
				SetRouting.showCustomerDetail(this.customerId);
			});
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
	},
	addMarkerCustomerMTRouting:function(point){
		var map = Vietbando._map;
		if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new viettel.LatLng(point.lat, point.lng);
			var marker = null;
			if(isNaN(point.ordinalVisit) && point.ordinalVisit.length == 1) {
				marker = new viettel.LabelMarker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 47px; top: -25px;">'+point.ordinalVisit+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else if(!isNaN(point.ordinalVisit) && point.ordinalVisit <10) {
				marker = new viettel.LabelMarker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 45px; top: -25px;">'+point.ordinalVisit+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			} else {
				marker = new viettel.LabelMarker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					labelContent : '<span style="position: inherit; left: 41px; top: -25px;">'+point.ordinalVisit+'</span>',
					labelClass : "CustomersStatus", 
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(50, 0)
				});
			}
			marker.customerId = point.customerId;
			marker.lat = point.lat;
			marker.lng = point.lng;
			viettel.Events.addListener(marker, "click", function(evt) {
				MTSetRouting.showMTCustomerDetail(this.customerId);
			});
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
	},
	addMarkerImages:function(lat,lng, custLat, custLng){
		ViettelMap._listOverlay = null;
		if(ViettelMap.isValidLatLng(lat, lng)) {
			if(ViettelMap.isValidLatLng(custLat, custLng)) {
				var pt = new viettel.LatLng(lat, lng);
				var cusPt = new viettel.LatLng(custLat, custLng);
				var distance = ViettelMap.CalculateDistance(pt, cusPt);
				var distanceStr = '';
				if(Number(distance) < 1000) {
					distanceStr = formatCurrency(distance) + ' m';
				} else {
					distance = Number(distance) / 1000;
					distanceStr = formatCurrency(distance) + ' km';
				}
				var title = $('#titlePopup').html();
				if(title.indexOf(distanceStr) == -1) {
					$('#titlePopup').html(title + ' - ' + Utils.XSSEncode(distanceStr));
				}
				var marker = new viettel.LabelMarker({
					position : pt,
					labelContent : '<span style="position: inherit; left: -31px; font-size: 12px;font-weight: bold; top: -49px; width: 121px !important; text-align: center">'+Utils.XSSEncode(distanceStr)+'</span>',
					icon:{
						url : '/resources/images/Mappin/icon_circle_image_green.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					map : ViettelMap._map,
					labelClass : "MarkerLabel",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(25, 0)
				});
				
				if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
					ViettelMap._listOverlay = new Array();
					ViettelMap._listOverlay.push(marker);
				} else {
					ViettelMap._listOverlay.push(marker);
				}
				
				var custMarker = new viettel.LabelMarker({
					position : cusPt,
					icon:{
						url : '/resources/images/Mappin/icon_circle_red.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					map : ViettelMap._map,
					labelClass : "MarkerLabel",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(25, 0)
				});
				
				if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
					ViettelMap._listOverlay = new Array();
					ViettelMap._listOverlay.push(custMarker);
				} else {
					ViettelMap._listOverlay.push(custMarker);
				}
			} else {
				var pt = new viettel.LatLng(lat, lng);
				var marker = new viettel.LabelMarker({
					position : pt,
					icon:{
						url : '/resources/images/Mappin/icon_circle_image_green.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					map : ViettelMap._map,
					labelClass : "MarkerLabel",
					labelVisible : true,
					draggable : false,
					labelAnchor : new viettel.Point(25, 0)
				});
				
				if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
					ViettelMap._listOverlay = new Array();
					ViettelMap._listOverlay.push(marker);
				} else {
					ViettelMap._listOverlay.push(marker);
				}
			}
		} else if(ViettelMap.isValidLatLng(custLat, custLng)) {
			var cusPt = new viettel.LatLng(custLat, custLng);
			var custMarker = new viettel.LabelMarker({
				position : cusPt,
				icon:{
					url : '/resources/images/Mappin/icon_circle_red.png',
					size : {height : 32, width : 32},
					scaledSize : {height : 32, width : 32}
				},
				map : ViettelMap._map,
				labelClass : "MarkerLabel",
				labelVisible : true,
				draggable : false,
				labelAnchor : new viettel.Point(25, 0)
			});
			
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(custMarker);
			} else {
				ViettelMap._listOverlay.push(custMarker);
			}
		}
		ViettelMap.fitOverLay();
	},
	addMutilMarkerStaff:function(){
		var map = ViettelMap._map;
		if(ViettelMap._listMarker!=undefined && ViettelMap._listMarker!=null){
			var flag=0;
			var index=0;
			SuperviseSales._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<ViettelMap._listMarker.valArray.length;i++,index++,j++){
					if(j>100) break;
					var point = ViettelMap._listMarker.valArray[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new viettel.LatLng(point.lat, point.lng);
						var info="<div id='info"+point.staffId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
						var title=point.staffName;
						var image=point.image;
						var accuracy = point.accuracy;
						var createTime = point.createTime;
						
						var level = ViettelMap._map.getZoom();
						var markerContent = ''; 
						var markerTop = "<div id='top"+point.id+"' class='Top' style='text-align:center;position: relative;top: -15px;'></div>";
						var markerBotoom = "<div id='bottom' style='position: relative; bottom: -28px;text-align:center;'></div>";
						if(level >= 13) {
							markerTop = "<div id='top"+point.id+"' class='Top' style='text-align:center;position: relative;top: -15px;'><strong style='color:red;font-size: 12px;' class='titleShop'>" + Utils.XSSEncode(point.shopCode) + "</strong></div>";
						} else {
							markerTop = "<div id='top"+point.id+"' class='Top' style='display:none;text-align:center;position: relative;top: -15px;'><strong style='color:red;font-size: 12px;' class='titleShop'>" + Utils.XSSEncode(point.shopCode) + "</strong></div>";
						}
						if(level >= 16) {
							markerBotoom = "<div id='bottom"+point.id+"' class='Bottom' style='position: relative; bottom: -28px; text-align:center;'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(point.staffName) + "</strong></div>";
						} else {
							markerBotoom = "<div id='bottom"+point.id+"' class='Bottom' style='position: relative; bottom: -28px; display:none;text-align:center;'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(point.staffName) + "</strong></div>";
						}
						markerContent = "<div id='marker"+point.id+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'>"+markerTop+markerBotoom+"</div>";
						
						
						var marker = new viettel.LabelMarker({
							icon:{
								url : image,
								size : {height : 39, width : 20},
								scaledSize : {height : 39, width : 20}
							},
							position : pt,
							map : ViettelMap._map,
							labelContent : markerContent,
							labelClass : "MarkerLabel",
							labelVisible : true,
							draggable : false,
							labelAnchor : new viettel.Point(25, 0)
						});
						marker.staffId = point.staffId;
						marker.lat = point.lat;
						marker.lng = point.lng;
						marker.accuracy = point.accuracy;
						marker.createTime = point.createTime;
						marker.id=point.id;
						$('#marker'+point.id).parent().prev().css('z-index', 10000000);
						$('#marker'+point.id).parent().prev().bind('click', point, function(e) {
							var point = e.data;
							ViettelMap.showWindowInfo(point.id, point.lat, point.lng, point.accuracy, point.createTime, point); 
						});
						
						if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
							ViettelMap._listOverlay = new Array();
							ViettelMap._listOverlay.push(marker);
						} else {
							ViettelMap._listOverlay.push(marker);
						}
						
						if(SuperviseSales._lstNodeExcep.get(marker.id) != null) {
							$('#marker'+marker.id).parent().parent().hide();
						}
					}
				}
				if(index>=ViettelMap._listMarker.valArray.length){
					ViettelMap.hideShowTitleMarker();
					if(SuperviseSales._notFitOverlay==undefined || SuperviseSales._notFitOverlay==null){
						ViettelMap.fitOverLay();
					}else if(SuperviseSales._idStaffSelected!=null && SuperviseSales._idStaffSelect==SuperviseSales._idStaffSelected ){
						SuperviseSales.moveToStaff(SuperviseSales._idStaffSelect);
					}
					SuperviseSales._notFitOverlay=null;
					window.clearInterval(SuperviseSales._itv);
				}
				if(SuperviseSales._idStaffSelect != null || SuperviseSales._idStaffSelected != null) {
					
				} else {
					ViettelMap.fitOverLay();
				}
				ViettelMap.hideShowTitleMarker();
			},300); 
		}
	},
	loadBigMap: function(objId, lat, lng, zoom,callback){
		ViettelMap._map = new viettel.Map(document.getElementById(objId));
		var map = ViettelMap._map; 
		var showMarker = true;			 
		if(!ViettelMap.isValidLatLng(lat,lng)){
			lat = ViettelMap._DEF_LAT_BIG;
			lng = ViettelMap._DEF_LNG_BIG;
			showMarker = false;
			if(zoom == null || zoom == undefined) {
				zoom = ViettelMap._DEF_ZOOM_BIG;
			}
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			zoom = ViettelMap._DEF_ZOOM_BIG;
		}
		var pt = new viettel.LatLng(lat, lng);
	    map.setCenter(pt);
	    map.setZoom(zoom);
	    var mapOptions = {
		    	panZoomControl: enable,
	    		panZoomControlOptions : {
	    			position: viettel.ControlPosition.RIGHT_TOP
	    		}
	    	};
    	map.setOptions(mapOptions);
	    viettel.Events.addListener(map, 'zoom_changed', function() {
	    	ViettelMap.hideShowTitleMarker();
	    });
		 return map;
	},
	/**SangTN Thay doi load ban do phan quan ly khach hang*/
	loadBigMapAndShowMarker: function(objId, lat, lng, zoom, callback){
		
		ViettelMap._map = new viettel.Map(document.getElementById(objId));
		
		var map = ViettelMap._map; 
		var showMarker = true;
		if(!ViettelMap.isValidLatLng(lat,lng)){
			showMarker = false;
			lat = ViettelMap._DEF_LAT_BIG;
			lng = ViettelMap._DEF_LNG_BIG;
			if(zoom == null || zoom == undefined) {
				zoom = ViettelMap._DEF_ZOOM_BIG;
			}
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			zoom = ViettelMap._DEF_ZOOM_BIG;
		}
		var pt = new viettel.LatLng(lat, lng); 
	    map.setCenter(pt);
	    map.setZoom(zoom);
	    var mapOptions = {
		    	panZoomControl: enable,
	    		panZoomControlOptions : {
	    			position: viettel.ControlPosition.RIGHT_TOP
	    		}
	    	};
    	map.setOptions(mapOptions);
    	
    	if(showMarker){
    		if (ViettelMap._marker != null){
             	ViettelMap._marker.setMap(null);
             }
             ViettelMap._marker = new viettel.Marker({
             	position: pt,
             	map: ViettelMap._map
             });	
    	}    	 
    	
	    viettel.Events.addListener(map, 'zoom_changed', function() {
	    	ViettelMap.hideShowTitleMarker();
	    });
	    if(callback != undefined && callback != null){
	    	viettel.Events.addListener(map, 'click', function(evt) {
		    	callback.call(this, evt);
		    });
	    }
	    
		return map;
	},
	loadMapForImages: function(objId, lat, lng, zoom,type, custLat, custLng){
		if(!ViettelMap.isValidLatLng(lat,lng)){
			lat = ViettelMap._DEF_LAT_BIG;
			lng = ViettelMap._DEF_LNG_BIG;
			showMarker = false;
			if(zoom == null || zoom == undefined) {
				zoom = ViettelMap._DEF_ZOOM_BIG;
			}
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			zoom = ViettelMap._DEF_ZOOM_BIG;
		}
		ViettelMap.clearOverlays();
		$('.fancybox-inner #bigMap').html('');
		var __object = $('.fancybox-inner #bigMap')[0];
		ViettelMap._map = new viettel.Map(__object);
		ViettelMap._map.setOptions({
			overviewMapControl : false,
			center: new viettel.LatLng(lat, lng),
			zoom: zoom
		});
		var map = ViettelMap._map; 
		//var showMarker = true;
		ViettelMap.addMarkerImages(lat,lng, custLat, custLng);
		//var pt = new viettel.LatLng(lat, lng);
	    //map.setCenter(pt);
	    //map.setZoom(zoom);
	    if(type != undefined && type != null && type == 1){//bản đồ mở rộng
	    	var mapOptions = {
		    		panZoomControl: enable,
		    		panZoomControlOptions : {
		    			position: viettel.ControlPosition.RIGHT_TOP
		    		}
		    	};
	    	map.setOptions(mapOptions);
	    }
		return map;
	},	
	loadMapForImagesMT: function(objId, lat, lng, zoom,type, custLat, custLng, typeUpload){
		if(!ViettelMap.isValidLatLng(lat,lng)){
			lat = ViettelMap._DEF_LAT_BIG;
			lng = ViettelMap._DEF_LNG_BIG;
			showMarker = false;
			if(zoom == null || zoom == undefined) {
				zoom = ViettelMap._DEF_ZOOM_BIG;
			}
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			zoom = ViettelMap._DEF_ZOOM_BIG;
		}
		ViettelMap.clearOverlays();
		$('.fancybox-inner #bigMap').html('');
		var __object = $('.fancybox-inner #bigMap')[0];
		ViettelMap._map = new viettel.Map(__object);
		ViettelMap._map.setOptions({
			overviewMapControl : false,
			center: new viettel.LatLng(lat, lng),
			zoom: zoom
		});
		var map = ViettelMap._map; 
		//var showMarker = true;
		ViettelMap.addMarkerImagesMT(lat,lng, custLat, custLng, typeUpload);
		//var pt = new viettel.LatLng(lat, lng);
	    //map.setCenter(pt);
	    //map.setZoom(zoom);
	    if(type != undefined && type != null && type == 1){//bản đồ mở rộng
	    	var mapOptions = {
		    		panZoomControl: enable,
		    		panZoomControlOptions : {
		    			position: viettel.ControlPosition.RIGHT_TOP
		    		}
		    	};
	    	map.setOptions(mapOptions);
	    }
		return map;
	},

	hideShowTitleMarker:function(){
		if(ViettelMap._map.getZoom()>=13){
    		$('.Top').show();
    		if(ViettelMap._map.getZoom()>=16){
	    		$('.Bottom').show();
	    	}else{
	    		$('.Bottom').hide();
	    	}
    	}else{
    		$('.Top').hide();
    		$('.Bottom').hide();
    	}
	},
	loadMapImage: function(objId, lat, lng, zoom){
		ViettelMap._map = new viettel.Map(document.getElementById(objId));
		 var map = ViettelMap._map; 
		 var showMarker = true;			 
		 if(!ViettelMap.isValidLatLng(lat,lng)){
			 lat = ViettelMap._DEF_LAT;
			 lng = ViettelMap._DEF_LNG;
			 showMarker = false;
			 if(zoom == null || zoom == undefined) {
				 zoom = ViettelMap._DEF_ALL_ZOOM;
			 }
		 } else if(zoom == undefined || zoom == 0 || zoom == null){
			 zoom = ViettelMap._DEF_ZOOM;
		 }
		 var pt = new viettel.LatLng(lat, lng);
		 var mapOptions = {
				 center : pt,
				 zoom : zoom,
				 panZoomControl: enable,
				 panZoomControlOptions : {
					 position : viettel.ControlPosition.RIGHT_TOP
				 }
			 };
	     map.setOptions(mapOptions);
	    
	     if(showMarker){
	    	 ViettelMap._marker = new viettel.Marker({
	    		 position: pt,
	    		 map: map
	    	 });
	     }
		 return map;
	},
	loadMapWithMultiMarkerKARouting : function(objId, lstPoint, centerPtLat, centerPtLng, zoom, callback) {
		ViettelMap.clearOverlays();
		$('#'+objId).html('');
		ViettelMap._map = new viettel.Map(document.getElementById(objId));
		var map = ViettelMap._map;
		ViettelMap.clearOverlays();
		if(ViettelMap._currentInfoWindow != null) {
			ViettelMap._currentInfoWindow.close();
		}
		ViettelMap.fitOverLay();
		var lat=0;
		var lng=0;
	    if(!ViettelMap.isValidLatLng(centerPtLat,centerPtLng)){
	    	centerPtLat = ViettelMap._DEF_LAT;
	    	centerPtLng = ViettelMap._DEF_LNG;
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			 zoom = ViettelMap._DEF_ZOOM;
		}
	    var centerPt = new viettel.LatLng(centerPtLat, centerPtLng);
		map.setCenter(centerPt);
		map.setZoom(zoom);
	    //map.addControl(new VSmallZoomControl());
	    
		if(lstPoint!=undefined && lstPoint!=null){
			for(var i = 0; i < lstPoint.length; i++) {
				var point = lstPoint[i];
				ViettelMap.addMarkerCustomerKARouting(point);
			}
		}
		setTimeout(function() {
			ViettelMap.fitOverLay();
		}, 10);
		
		return map;
	},
	loadMapWithMultiMarkerMTRouting : function(objId, lstPoint, centerPtLat, centerPtLng, zoom, callback) 
	{
		ViettelMap.clearOverlays();
		$('#'+objId).html('');
		ViettelMap._map = new viettel.Map(document.getElementById(objId));
		var map = ViettelMap._map;
		ViettelMap.clearOverlays();
		if(ViettelMap._currentInfoWindow != null) {
			ViettelMap._currentInfoWindow.close();
		}
		ViettelMap.fitOverLay();
		var lat=0;
		var lng=0;
	    if(!ViettelMap.isValidLatLng(centerPtLat,centerPtLng)){
	    	centerPtLat = ViettelMap._DEF_LAT;
	    	centerPtLng = ViettelMap._DEF_LNG;
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			 zoom = ViettelMap._DEF_ZOOM;
		}
	    var centerPt = new viettel.LatLng(centerPtLat, centerPtLng);
		map.setCenter(centerPt);
		map.setZoom(zoom);
	    //map.addControl(new VSmallZoomControl());
		
		if(lstPoint!=undefined && lstPoint!=null){
			for(var i = 0; i < lstPoint.length; i++) {
				var point = lstPoint[i];
				ViettelMap.addMarkerCustomerMTRouting(point);
			}
		}
		setTimeout(function() {
			ViettelMap.fitOverLay();
		}, 10);
		
		return map;
	},
	showWindowInfo: function(objectId,lat,lng,cur,time) {		
		var pt = new viettel.LatLng(lat, lng);
		if (objectId != '') {
			/*** Begin Tam thoi; chi cho hien thi StaffSpecType.SUPERVISOR; StaffSpecType.STAFF*/
			var temp = SuperviseSales._lstStaffPosition.get(objectId);
			if (temp != null) {
				if (temp.roleType != StaffSpecType.SUPERVISOR && temp.roleType != StaffSpecType.STAFF) {
					return false;
				}
			}
			/*** End Tam thoi; chi cho hien thi StaffSpecType.SUPERVISOR; StaffSpecType.STAFF*/
			if (ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			var infoWindow = new viettel.InfoWindow({
				content: "<div style='width:285px; height:35px; text-align: center;'><img style='position: relative;top:20px;' src='/resources/images/loading-small.gif'/></div><br/>",
				position: pt,
				maxWidth: 290
			});
			ViettelMap._currentInfoWindow = infoWindow;
			infoWindow.open(ViettelMap._map);
			var temp = SuperviseSales._lstStaffPosition.get(objectId);
			if (temp != null) {
				SuperviseSales._idStaffSelect=objectId;
				if (temp.roleType == StaffSpecType.MANAGER) { //TBHV; Vuongmq; 11/09/2015; chua su dung
					//SuperviseSales.showDialogTBHVInfo(temp.staffId,temp.staffCode,temp.staffName,temp.createTime,temp.accuracy,pt,temp.staffOwnerId);
				} else if (temp.roleType == StaffSpecType.SUPERVISOR) { // NVGS
					SuperviseSales.showDialogNVGSInfo(temp.staffId,temp.staffCode,temp.staffName,temp.createTime,temp.accuracy,pt,temp.staffOwnerId);
				} else if (temp.roleType == StaffSpecType.STAFF) { // NVBH
					SuperviseSales.showDialogNVBHInfo(temp.staffId,temp.staffCode,temp.staffName,temp.createTime,temp.accuracy,pt,temp.staffOwnerId);
				}
			}
		}
	},
	addMarker: function(lat,lng){
		if(!ViettelMap.isValidLatLng(lat,lng)){
			return;
		}
		var latlng = new viettel.LatLng(lat, lng);
        if (ViettelMap._marker != null){
        	ViettelMap._marker.setMap(null);
        }
        ViettelMap._marker = new viettel.Marker({
        	position: latlng,
        	map: ViettelMap._map
        });
        ViettelMap._map.setCenter(latlng);
	},
	addMarkerNotCenter: function(lat,lng){
		if(!ViettelMap.isValidLatLng(lat,lng)){
			return;
		}
		var latlng = new viettel.LatLng(lat, lng);
		
		if (ViettelMap._marker != null){
			ViettelMap._marker.setMap(null);
        }
        ViettelMap._marker = new viettel.Marker({
        	position: latlng,
        	map: ViettelMap._map
        });
	},
	isValidLatLng: function(lat,lng){
		if(lat==undefined || lng == undefined || lat== null || lng== null || lat == 0.0 || lng ==0.0){
			return false;
		}
		if (!Number(lat) || !Number(lng)) {
			return false;
		}
		return true;
	},
	loadBigMapEx: function(objId, lat, lng, zoom,callback, clickable, isNotLoadMap){
		if (isNotLoadMap == undefined || isNotLoadMap == null || !isNotLoadMap) {
			ViettelMap._map = new viettel.Map(document.getElementById(objId));
		}
		if (ViettelMap._marker != null){
        	ViettelMap._marker.setMap(null);
        }
		var map = ViettelMap._map; 
		var showMarker = true;
		if(!ViettelMap.isValidLatLng(lat,lng)){
			lat = 16.1060393;
			lng = 107.1976608455;
			showMarker = false;
			if(zoom == null || zoom == undefined) {
				zoom = ViettelMap._DEF_ZOOM_LV4;
			}
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			zoom = ViettelMap._DEF_ALL_ZOOM;
		}
		var pt = new viettel.LatLng(lat, lng);
		map.setCenter(pt);
		map.setZoom(zoom);
	    var mapOptions = {
		    	panZoomControl: enable,
	    		panZoomControlOptions : {
	    			position: viettel.ControlPosition.RIGHT_TOP
	    		}
	    	};
    	map.setOptions(mapOptions);
    	var marker = new viettel.Marker({
        	position: pt,
        	map: showMarker ? ViettelMap._map : null
        });
    	ViettelMap._marker = marker;
    	if(showMarker){
			marker.lat = lat;
			marker.lng = lng;
		    if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
	    }
	    viettel.Events.addListener(map, 'zoom_changed', function() {	    	
	    });
	    if (isNotLoadMap == undefined || isNotLoadMap == null || !isNotLoadMap) {
	    viettel.Events.addListener(map, "click", function(evt) {
            var lat = evt.latLng.lat();
            var lng = evt.latLng.lng();
            if (clickable != undefined && clickable != null && clickable &&
            		callback != undefined && callback != null) {
            	var pt = new viettel.LatLng(lat, lng);
            	ViettelMap._marker.setMap(null);
            	var marker = new viettel.Marker({
                	position: pt,
                	map: ViettelMap._map
                });
            	ViettelMap._marker = marker;
                callback(lat, lng);
            }
	    });
	    }
		return map;
	},
	addMutilMarkerStaffForSetOrder:function(){
		var map = ViettelMap._map;
		var lstPoint = new Array();
		if(SuperviseManageRouteCreate._listMarker!=undefined && SuperviseManageRouteCreate._listMarker!=null && SuperviseManageRouteCreate._listMarker.length >0){
			lstPoint = SuperviseManageRouteCreate._listMarker;
			var flag=0;
			var index=0;
			SuperviseManageRouteCreate._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<lstPoint.length;i++,index++,j++){
					if(j>100) break;
					var point = lstPoint[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new viettel.LatLng(point.lat, point.lng);
						var custId=point.customerId;
						var info="<strong>" + Utils.XSSEncode(point.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(point.customerName) + "</strong><br/>";
						info += "Địa chỉ: ";
						if(point.address != null)
							info += point.address;
						var ttgt = "";
						if(point.ttgt != undefined && point.ttgt != null){
							ttgt=point.ttgt;
						}
						var image="/resources/images/Mappin/icon_circle_red.png";
						//map.addOverlay(new MyOverlay(custId, pt, image, title,info));
						var markerContent = '<span id="marker'+custId+'" style="color: white; font-weight: bold; z-index: 10000001; position: relative; left: 22px; top: -23px;" class="StaffPositionOridnalVisit" ' + 
							'onclick="ViettelMap.showDialogCustomerInfoEx(' + point.lat + ',' + point.lng + ',\'' + Utils.XSSEncode(point.customerCode) + '\',\'' + Utils.XSSEncode(point.customerName) + '\',\'' + Utils.XSSEncode(point.address) + '\');">' + ttgt + '</span>' + 
							'<span style="color: blue; font-weight: bold; z-index: 10000001; position: relative; left: 6px; top: -45px;">'+ Utils.XSSEncode(point.customerCode) +'</span>';
						if(ttgt > 9){
							markerContent = '<span id="marker'+custId+'" style="color: white; font-weight: bold; z-index: 10000001; position: relative; left: 18px; top: -23px;" class="StaffPositionOridnalVisit"' + 
							'onclick="ViettelMap.showDialogCustomerInfoEx(' + point.lat + ',' + point.lng + ',\'' + Utils.XSSEncode(point.customerCode) + '\',\'' + Utils.XSSEncode(point.customerName) + '\',\'' + Utils.XSSEncode(point.address) + '\');">' + ttgt + '</span>' + 
							'<span style="color: blue; font-weight: bold; z-index: 10000001; position: relative; left: 0px; top: -45px;">'+ Utils.XSSEncode(point.customerCode) +'</span>';
						}
						var marker = new viettel.LabelMarker({
							icon:{
								url : image,
								size : {height : 32, width : 32},
								scaledSize : {height : 32, width : 32}
							},
							position : pt,
							map : ViettelMap._map,
							labelContent : markerContent,
							labelClass : "MarkerLabel",
							labelVisible : true,
							draggable : false,
							labelAnchor : new viettel.Point(25, 0)
						});
						
						$('#marker'+point.customerId).parent().prev().css('z-index', 10000000);
						
						$('#marker'+point.customerId).parent().prev().bind('click', point, function(e) {
							var point = e.data;
							ViettelMap.showDialogCustomerInfo(point);
						});
						
						if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
							ViettelMap._listOverlay = new Array();
							ViettelMap._listOverlay.push(marker);
						} else {
							ViettelMap._listOverlay.push(marker);
						}
					}
				}
				if(index>=lstPoint.length){
					ViettelMap.hideShowTitleMarker();
					window.clearInterval(SuperviseManageRouteCreate._itv);
				}
				ViettelMap.fitOverLay();
				ViettelMap.hideShowTitleMarker();
			},300); 
		}
	},
	showDialogCustomerInfo: function(data) {
		if (ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		var pt = new viettel.LatLng(data.lat, data.lng);
		var info="<strong>" + Utils.XSSEncode(data.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(data.customerName) + "</strong><br/>";
		info += "Địa chỉ: ";
		if(data.address != null)
			info += data.address;
		
		var infoWindow = new viettel.InfoWindow({
			content: info,
			maxWidth: 200,
			position: pt
		});										
		ViettelMap._currentInfoWindow = infoWindow;
		infoWindow.open(ViettelMap._map);
		
		ViettelMap._map.setCenter(pt);
		//$('.olPopup .olPopupContent').css('height','30');
		$('.olPopup .olPopupContent').css('font-size','1em');
//		ViettelMap.fitOverLay();
	},
	TBHVInfoContent : '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Mã TBHV:</dt><dd>{0}</dd>'
					  +'<dt>Tên TBHV:</dt><dd>{1}</dd><dt>Đơn vị:</dt><dd>{2}</dd><dt>Đang đi trên tuyến cùng:</dt>'
					  +'<dd>{3}</dd><dt>Cập nhật vị trí cuối:</dt><dd>{4}</dd>'
					  +'<dt>Độ chính xác:</dt><dd>{5}</dd></dl></div>',
	GSNPPInfoContent : '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Mã GSNPP:</dt><dd>{0}</dd>'
					  +'<dt>Tên GSNPP:</dt><dd>{1}</dd><dt>Đơn vị:</dt><dd>{2}</dd><dt>Đang đi trên tuyến cùng:</dt>'
					  +'<dd>{3}</dd><dt>Cập nhật vị trí cuối:</dt><dd>{4}</dd>'
					  +'<dt>Độ chính xác:</dt><dd>{5}</dd></dl></div>',
	NVBHInfoContent : '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Mã NVBH:</dt><dd>{0}</dd>'
					  +'<dt>Tên NVBH:</dt><dd>{1}</dd><dt>Đơn vị:</dt><dd>{2}</dd><dt>Doanh số ngày/KH:</dt>'
					  +'<dd>{3}</dd><dt>Điểm trong tuyến:</dt><dd>{4}</dd>'
					  +'<dt>Điểm doanh số:</dt><dd>{5}</dd><dt>Cập nhật vị trí cuối cùng:</dt><dd>{6}</dd>'
					  +'<dt>Độ chính xác:</dt><dd>{7}</dd></dl></div>',
	CustomerInfoContent : '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Mã điểm lẻ:</dt><dd>{0}</dd>'
					  +'<dt>Tên điểm lẻ:</dt><dd>{1}</dd></dl></div>',
	CustomerDetail :    '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Tên KH:</dt><dd>{0}</dd>'
		  				+'<dt>Thứ tự:</dt><dd>{1}</dd><dt>Địa chỉ:</dt><dd>{2}</dd><dt>Di Động:</dt>'
		  				+'<dd>{3}</dd><dt>Cố định:</dt><dd>{4}</dd>'
						  
};
var VTMapUtil = {
	loadedResources:new Array(),
	OVERLAY_ZINDEX : 9,
	OVERLAY_ZINDEX_DEFAULT : 1,
	getBrowserName : function() {
		var browserName = "";
		var ua = navigator.userAgent.toLowerCase();
		if (ua.indexOf("opera") != -1)
			browserName = "opera";
		else if (ua.indexOf("msie") != -1)
			browserName = "msie";
		else if (ua.indexOf("safari") != -1)
			browserName = "safari";
		else if (ua.indexOf("mozilla") != -1) {
			if (ua.indexOf("firefox") != -1)
				browserName = "firefox";
			else
				browserName = "mozilla";
		}
	},
	loadMapResource:function(callback){
		VTMapUtil.loadResource(vtm_url, 'js',callback);
	},
	loadResource:function(filename, filetype, callback){
		if ($.inArray(filename, VTMapUtil.loadedResources) == -1){
			if (filetype=="js"){ // if filename is a external JavaScript file
				var fileref=document.createElement('script');
				fileref.setAttribute("type","text/javascript");
				fileref.setAttribute("src", filename);
			}
			if (typeof fileref != "undefined"){
				if(callback != undefined){
					if(fileref.readyState){
						fileref.onreadystatechange = function (){
							if (fileref.readyState == 'loaded' || fileref.readyState == 'complete'){
								callback();
							}
						};
					} else {
						fileref.onload = callback;
					}
				}
				document.getElementsByTagName("head")[0].appendChild(fileref);
			}
			VTMapUtil.loadedResources.push(filename);
		} else {
			if(callback != undefined){
				callback();
			}
		}
	}
};







function TBHVMapInfo(staffCode,staffName,shopCode,GSNPPStr,lastPosTime,accuracy,GSNPPArr,lat,lng,imgUrl){
	this.staffCode = staffCode;
	this.staffName = staffName;
	this.shopCode = shopCode;
	this.GSNPPStr = GSNPPStr;
	this.lastPosTime = lastPosTime;
	this.accuracy = accuracy;
	this.GSNPPArr = GSNPPArr;
	this.lat = lat;
	this.lng = lng;
	this.imgUrl = imgUrl;
};

function GSNPPMapInfo(staffCode,staffName,shopCode,NVBHStr,lastPosTime,accuracy,lat,lng,imgUrl){
	this.staffCode = staffCode;
	this.staffName = staffName;
	this.shopCode = shopCode;
	this.NVBHStr = NVBHStr;
	this.lastPosTime = lastPosTime;
	this.accuracy = accuracy;
	this.lat = lat;
	this.lng = lng;
	this.imgUrl = imgUrl;
};

function NVBHMapInfo(staffCode,staffName,shopCode,revenue,channelPoint,revenuePoint,lastPosTime,accuracy,lat,lng,imgUrl){
	this.staffCode = staffCode;
	this.staffName = staffName;
	this.shopCode = shopCode;
	this.revenue = revenue;
	this.channelPoint = channelPoint;
	this.revenuePoint = revenuePoint;
	this.lastPosTime = lastPosTime;
	this.accuracy = accuracy;
	this.lat = lat;
	this.lng = lng;
	this.imgUrl = imgUrl;
};

function CustomerInfo(customerCode,customerName,lat,lng,imgUrl){
	this.customerCode = shopCode;
	this.customerName = shopName;
	this.lat = lat;
	this.lng = lng;
	this.imgUrl = imgUrl;
};

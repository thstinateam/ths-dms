var CommonSearch = {
	_currentSearchCallback : null,
	_arrParams : null,
	_listMultiChoose : null,
	_mapCheckProduct:new Map(),
	_xhrReport:null,
	_listCustomer:null,

	deleteFileExcelExport:function(excelFileName){
        
	},
	deleteFileByPath:function(lstPath){
	},
	showProductInfo : function(productCode, productName, convfactNumber,
			stockNumber, priceValue, total, promotionProgram, isSaleProduct, giaThung, isAllowShowPrice) {
		$('#showProductInfoContainer').show();
		$('#showProductInfo').dialog({
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
				$('#showProductInfo #productCode').text(Utils.XSSEncode(productCode));
				$('#showProductInfo #productName').text(Utils.XSSEncode(productName));
				$('#showProductInfo #convfactNumber').text(convfactNumber);
				$('#showProductInfo #stockNumber').text(Utils.XSSEncode(stockNumber));
				if (giaThung != null && giaThung != undefined) {
					$('#showProductInfo #priceValue').text(priceValue + " / " + giaThung);
				} else {
					$('#showProductInfo #priceValue').text(priceValue);
				}
				if (!isAllowShowPrice) {
					$('#showProductInfo #priceValue').hide();
					$('#showProductInfo #lblPrice').hide();
				}
				$('#showProductInfo #totalQuantity').text(total);
				if (isSaleProduct !== null && isSaleProduct !== undefined && isSaleProduct) {
					$('#showProductInfo #promotionProgram').show();
					$('#showProductInfo #promotionProgram').prev().show();
					if (promotionProgram == null || promotionProgram == undefined || promotionProgram.length == 0) {
						$('#showProductInfo #promotionProgram').text('');
					} else {
						$('#showProductInfo #promotionProgram').text(Utils.XSSEncode(promotionProgram));
					}
				} else {
					$('#showProductInfo #promotionProgram').hide();
					$('#showProductInfo #promotionProgram').prev().hide();
				}
	        },
	        onClose:function() {
	        	$('#showProductInfoContainer').hide();
	        }
		});
	},
	getSearchStyleShopGridUrl : function(url, code, name, arrParam) {		
		var searchUrl = '';
		searchUrl = url;
		var curDate = new Date();
		var curTime = curDate.getTime();
		searchUrl+= '?curTime=' + encodeChar(curTime);
		if (arrParam != null && arrParam != undefined) {
			for ( var i = 0; i < arrParam.length; i++) {
				searchUrl += "&" + arrParam[i].name + "=";
				if(arrParam[i].value != null && arrParam[i].value != undefined){
					searchUrl = searchUrl + arrParam[i].value;
				}
			}
		}
		searchUrl+= "&promotionProgramCode=" + encodeChar(code) + "&promotionProgramName=" + encodeChar(name);
		return searchUrl;
	},
	searchCustomerByShopOnDialog : function(callback, arrParam) {
		CommonSearch.openSearchStyle1EasyUIDialog(create_order_ma_kh , create_order_ten_kh,
				create_order_tt_tim_kiem, "/commons/customer-in-shop/search", callback,
				"shortCode", "customerName", arrParam,"id");
		return false;
	},
	openSearchStyle1EasyUIDialog : function(codeText, nameText, title, url, callback, codeFieldText, nameFieldText, arrParam,idFieldText,addressText, addressFieldText) {
		$('.easyui-dialog #btnSearchStyle1').unbind('click');		
		CommonSearch._arrParams = null;
		CommonSearch._currentSearchCallback = null;
		$('#searchStyle1EasyUIDialogDiv').show();
		var html = $('#searchStyle1EasyUIDialogDiv').html();
		$('#searchStyle1EasyUIDialog').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	        	
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});								
				$('.easyui-dialog #seachStyle1Code').focus();
				$('.easyui-dialog #searchStyle1Url').val(url);
				
				$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);
				$('.easyui-dialog #searchStyle1IdText').val(idFieldText);
				if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
					$('.easyui-dialog #searchStyle1AddressText').val(addressFieldText);
					$('.easyui-dialog #seachStyle1AddressLabel').show();
					$('.easyui-dialog #seachStyle1Address').show();					
				}
				$('.easyui-dialog #btnSearchStyle1').css('margin-left', 270);
				$('.easyui-dialog #btnSearchStyle1').css('margin-top', 5);				
				CommonSearch._currentSearchCallback = callback;
				CommonSearch._arrParams = arrParam;
				var codeField = 'code';
				var nameField = 'name';
				var idField = 'id';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				if (idFieldText != null&& idFieldText != undefined && idFieldText.length > 0) {
					idField = idFieldText;
				}
				Utils.bindAutoSearch();
				$('.easyui-dialog #seachStyle1CodeLabel').html(codeText);
				$('.easyui-dialog #seachStyle1NameLabel').html(nameText);
				$('.easyui-dialog #searchStyle1Grid').show();
				$('.easyui-dialog #searchStyle1Grid').datagrid({
					url : CommonSearch.getSearchStyle1GridUrl(url, '', '', arrParam),
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					pageSize : 10,
					pageList: [10,20,30,40,50],
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
					queryParams:{
						page:1
					},
					width : ($('#searchStyle1EasyUIDialog').width() - 40),
				    columns:[[  
				        {field:codeField,title:codeText,align:'left', width:130, sortable : false,resizable : false,
				        	formatter: function(val, row) {
				        		if (val != undefined && val != null) {
				        			return Utils.XSSEncode(val);
				        		}
				        		return '';
				        	}
				        },  
				        {field:nameField,title:nameText,align:'left', width:$('#searchStyle1EasyUIDialog').width() - 46 - 50 - 130 - 65, sortable : false,resizable : false,
				        	formatter: function(val, row) {
				        		if (val != undefined && val != null) {
				        			return Utils.XSSEncode(val);
				        		}
				        		return '';
				        	}
				        },				        
				        {field:'select', title:'Chọn', width:65, align:'center',sortable : false,resizable : false,formatter : CommonSearchFormatter.selectCellIconFormatterEasyUIDialog},
				        {field :idField,hidden : true},
				    ]],
				    onLoadSuccess :function(){
				    	 tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 $('.datagrid-header-rownumber').html(jsp_common_numerical_order);
			    		 updateRownumWidthForDataGrid('.easyui-dialog');
			    		 $('.easyui-dialog .datagrid-header-rownumber').css('width', '50px');
			    		 $('.easyui-dialog .datagrid-cell-rownumber').css('width', '50px');
			    		 setTimeout(function(){
				    			CommonSearchEasyUI.fitEasyDialog();
				    		},1200);
			    		 $(window).resize();
				    }
				});
				$('.easyui-dialog #btnSearchStyle1').bind('click',function(event) {
					if(!$(this).is(':hidden')){
						var code = $('.easyui-dialog #seachStyle1Code').val().trim();
						var name = $('.easyui-dialog #seachStyle1Name').val().trim();
						var address = $('.easyui-dialog #seachStyle1Address').val().trim();
						$('.easyui-dialog #searchStyle1Grid').datagrid('load',{code :code, name: name,address:address});						
						$('.easyui-dialog #seachStyle1Code').focus();
					}					
				});
				$('.easyui-dialog #btnClose').bind('click',function(event) {
					$('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");
					$('.easyui-dialog #searchStyle1Grid').datagrid('reload',{pageNumber:1});
					$('.easyui-dialog').dialog('close');
				});
	        }
		});
	},
	openSearchStyleCustomerShopEasyUIDialog : function(codeText, nameText, title, url, callback, codeFieldText, nameFieldText, arrParam,idFieldText,addressText, addressFieldText) {
		CommonSearch._arrParams = null;
		CommonSearch._currentSearchCallback = null;
		$('#searchStyleCustomerDialogDiv').css("visibility", "visible");
		var html = $('#searchStyleCustomerDialog').html();
		$('#searchStyleCustomerDialog').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 600,
	        height : 'auto',
	        onOpen: function(){	        	
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});								
				$('.easyui-dialog #seachStyleCustomerCode').focus();
				$('.easyui-dialog #searchStyleCustomerUrl').val(url);
				$('.easyui-dialog #errMsgSearchCustomer').html("").hide();
				
				$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);
				$('.easyui-dialog #searchStyle1IdText').val(idFieldText);
				Utils.bindFormatOnTextfield('searchStyleQuantity',Utils._TF_NUMBER,null);
		    	Utils.formatCurrencyFor('searchStyleQuantity');
				if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
					$('.easyui-dialog #searchStyle1AddressText').val(addressFieldText);
					$('.easyui-dialog #seachStyle1AddressLabel').show();
					$('.easyui-dialog #seachStyleCustomerAddress').show();					
				}
				CommonSearch._currentSearchCallback = callback;
				CommonSearch._arrParams = arrParam;
				var codeField = 'code';
				var nameField = 'name';
				var idField = 'id';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				if (idFieldText != null&& idFieldText != undefined && idFieldText.length > 0) {
					idField = idFieldText;
				}
				$('.easyui-dialog #seachStyle1CodeLabel').html(codeText);
				$('.easyui-dialog #seachStyle1NameLabel').html(nameText);
				$('#searchStyleCustomerGrid').show();
				$('#searchStyleCustomerGrid').datagrid({
					url : CommonSearch.getSearchStyleCustomerMapGridUrl(url, '', '','', arrParam),
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageNumber: 1,
					scrollbarSize : 0,
					fitColumns:true,
					pageList  : [10,20,30],
					width : ($('#searchStyleCustomerContainerGrid').width()),
				    columns:[[  
				        {field:'shortCode',title:codeText,align:'left', width:120, sortable : false,resizable : false,formatter:function(value,row,index){
				        	return Utils.XSSEncode(row.shortCode);
				        }},  
				        {field:'customerName',title:nameText,align:'left', width:170, sortable : false,resizable : false,formatter:function(value,row,index){
				        	return Utils.XSSEncode(row.customerName);
				        }},  
				        {field:'address', title:addressText,width:250, align:'left',sortable : false,resizable : false,formatter : function(value,row,index){
				        	return Utils.XSSEncode(row.address);
				        }},
				        {field: 'id', checkbox:true, align:'center', width:80,sortable : false,resizable : false}
				    ]],
				    onLoadSuccess :function(data){
				    	 $('.datagrid-header-rownumber').html(jsp_common_numerical_order);
				    	 tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 if(data.rows.length == 0){
			    			 $('.datagrid-header-check input').removeAttr('checked');
			    		 }
					    	var originalSize = $('.easyui-dialog .datagrid-header-row td[field=shortCode] div').width();
					    	if(originalSize != null && originalSize != undefined){
					    		PromotionCatalog._listCustomerDialogSize.push(originalSize);
					    	}
					    	Utils.updateRownumWidthForJqGridEX1('.easyui-dialog ','shortCode',PromotionCatalog._listCustomerDialogSize,true);
			    		 setTimeout(function(){
				    			CommonSearchEasyUI.fitEasyDialog();
				    		},1000);
			    		 $('input[name="id"]').each(function(){
			 				var temp = PromotionCatalog._listCustomer.get($(this).val());
			 				if(temp!=null) $(this).attr('checked','checked');
			 			});
			 	    	var length = 0;
			 	    	$('input[name="id"]').each(function(){
			 	    		if($(this).is(':checked')){
			 	    			++length;
			 	    		}	    		
			 	    	});	    	
			 	    	if(data.rows.length==length){
			 	    		$('.datagrid-header-check input').attr('checked','checked');
			 	    	}else{
							$('.datagrid-header-check input').removeAttr('checked');
			 	    	}
			 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
			 	    		$('.datagrid-header-check input').removeAttr('checked');
			 	    	}
				    },
				    onCheck:function(i,r){
				    	PromotionCatalog._listCustomer.put(r.id,r);
				    },
				    onUncheck:function(i,r){
				    	PromotionCatalog._listCustomer.remove(r.id);
				    },
				    onCheckAll:function(r){
				    	for(i=0;i<r.length;i++){
				    		PromotionCatalog._listCustomer.put(r[i].id,r[i]);
				    	}
				    },
				    onUncheckAll:function(r){
				    	for(i=0;i<r.length;i++){
				    		PromotionCatalog._listCustomer.remove(r[i].id);
				    	}
				    }
				}); 
				$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
				$('.easyui-dialog #btnSearchStyleCustomer').bind('click',function(event) {
						$('.easyui-dialog #errMsgSearchCustomer').html('').hide();
						var code = $('.easyui-dialog #seachStyleCustomerCode').val().trim();
						var name = $('.easyui-dialog #seachStyleCustomerName').val().trim();
						var address = $('.easyui-dialog #seachStyleCustomerAddress').val().trim();
						var url = CommonSearch.getSearchStyleCustomerMapGridUrl($('.easyui-dialog #searchStyleCustomerUrl').val(), code, name,address, CommonSearch._arrParams);
						$('.easyui-dialog #searchStyleCustomerGrid').datagrid({url:url,pageNumber:1});						
						$('.easyui-dialog #seachStyleCustomerCode').focus();
				});
				$('.easyui-dialog #btnSaveCustomer').bind('click',function(event) {
					var quantityMax = $('.easyui-dialog #seachStyleQuantity').val();
					var lstIdCustomer = new Array();
					for(i=0;i<PromotionCatalog._listCustomer.keyArray.length;i++){
						var temp = PromotionCatalog._listCustomer.get(PromotionCatalog._listCustomer.keyArray[i]);
						if(temp!=null){
							lstIdCustomer.push(temp.id);
						}
					}
					var s = $('#errMsgSearchCustomer').html();
					var flag = true;
					if(s.length > 0){
						var value = quantityMax;
						if(value != null && value != undefined){
							value = value * 1;
							if(isNaN(value)){
								$('#errMsgSearchCustomer').show();
								return false;
							}
						}
					}else{
						var value = quantityMax;
						if(value != null && value != undefined){
							value = value * 1;
							if(isNaN(value)){
								$('#errMsgSearchCustomer').show();
								return false;
							}
						}
					}
					var params = new Object();
					params.quantityMax = quantityMax;
					params.listCustomerId =lstIdCustomer;
					params.shopId = $('#shopMapId').val();
					params.promotionId = $('#promotionShopMap').val();
					if(lstIdCustomer == undefined || lstIdCustomer == null || lstIdCustomer.length <=0){
						$('.easyui-dialog #errMsgSearchCustomer').html("Bạn chưa chọn KH để cập nhật số suất.").show();
						return false;
					}
					Utils.addOrSaveData(params, '/catalog/promotion/createcustomershop', null, 'errMsgCustomer', function(data){
						if(data != null && data.error == undefined && data.errMsg == undefined){
							PromotionCatalog.searchCustomerShopMap($('#promotionShopMap').val(),$('#shopMapId').val(),data.shopCode);
							PromotionCatalog._listCustomer = new Map();
							$('#errMsgSearchCustomer').html("").hide();
							$('#searchStyleCustomerDialog').dialog('close');
							$('#errMsgCustomer').html("").hide();
							$('#successMsgCustomer').html('Lưu dữ liệu thành công.').show();
							try {
								$("#exGrid").treegrid("reload");
							} catch (e) {}
							var tm = setTimeout(function(){
								$('#successMsgCustomer').html('').hide();
								clearTimeout(tm); 
							}, 3000);
						}
					}, null, null, null, "Bạn có muốn thêm KH không ?", null);
					//$('.easyui-dialog').dialog('close');
				});
				$('.easyui-dialog #seachStyleCustomerCode').bind('keyup',function(event){
					if(event.keyCode == 13){
						$('.easyui-dialog #errMsgSearchCustomer').html('').hide();
						var code = $('.easyui-dialog #seachStyleCustomerCode').val().trim();
						var name = $('.easyui-dialog #seachStyleCustomerName').val().trim();
						var address = $('.easyui-dialog #seachStyleCustomerAddress').val().trim();
						var url = CommonSearch.getSearchStyleCustomerMapGridUrl($('.easyui-dialog #searchStyleCustomerUrl').val(), code, name,address, CommonSearch._arrParams);
						$('.easyui-dialog #searchStyleCustomerGrid').datagrid({url:url,pageNumber:1});
						$('.easyui-dialog #seachStyleCustomerCode').focus();
					}
				});
				$('.easyui-dialog #seachStyleCustomerName').bind('keyup',function(event){
					if(event.keyCode == 13){
						$('.easyui-dialog #errMsgSearchCustomer').html('').hide();
						var code = $('.easyui-dialog #seachStyleCustomerCode').val().trim();
						var name = $('.easyui-dialog #seachStyleCustomerName').val().trim();
						var address = $('.easyui-dialog #seachStyleCustomerAddress').val().trim();
						var url = CommonSearch.getSearchStyleCustomerMapGridUrl($('.easyui-dialog #searchStyleCustomerUrl').val(), code, name,address, CommonSearch._arrParams);
						$('.easyui-dialog #searchStyleCustomerGrid').datagrid({url:url,pageNumber:1});
						$('.easyui-dialog #seachStyleCustomerCode').focus();
					}
				});
				$('.easyui-dialog #seachStyleCustomerAddress').bind('keyup',function(event){
					if(event.keyCode == 13){
						$('.easyui-dialog #errMsgSearchCustomer').html('').hide();
						var code = $('.easyui-dialog #seachStyleCustomerCode').val().trim();
						var name = $('.easyui-dialog #seachStyleCustomerName').val().trim();
						var address = $('.easyui-dialog #seachStyleCustomerAddress').val().trim();
						var url = CommonSearch.getSearchStyleCustomerMapGridUrl($('.easyui-dialog #searchStyleCustomerUrl').val(), code, name,address, CommonSearch._arrParams);
						$('.easyui-dialog #searchStyleCustomerGrid').datagrid({url:url,pageNumber:1});
						$('.easyui-dialog #seachStyleCustomerCode').focus();
					}
				});
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('#searchStyleCustomerDialog').html(html);
	        	$('#searchStyleCustomerDialogDiv').hide();
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #seachStyle1Address').val('');
	        	$('.easyui-dialog #btnSaveCustomer').unbind('click');
	        	$('.easyui-dialog #errMsgSearchCustomer').html('').hide();
	        	PromotionCatalog._listCustomer = new Map();
	        }
	    });
		return false;
	},
	openSearchStyle1EasyUICreateUpdateGroupPOAuto : function(codeText, nameText, title, codeFieldText, nameFieldText, searchDialog,searchDiv,arrayParam) {
		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		$('#' +searchDiv).css("visibility", "visible");
		var html = $('#' +searchDiv).html();
		$('#' +searchDialog).dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height :'auto',
	        onOpen: function(){
	        	//@CuongND : my try Code <reset tabindex>
	        	var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
				$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);
				
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				if(arrayParam != null && arrayParam[0] == 1){
					var code = arrayParam[2];
					var name = arrayParam[3];
					var status = arrayParam[4];
					$('.easyui-dialog #seachStyle1Code').attr('disabled','disabled');
					$('.easyui-dialog #seachStyle1Code').val(code);
					$('.easyui-dialog #seachStyle1Name').val(name);
					$('.easyui-dialog #seachStyle1Name').focus();
					$('.easyui-dialog #statusDialogUpdate option[value=' +status+']').attr('selected','selected');
					var value = $('.easyui-dialog #statusDialogUpdate option[value=' +status+']').html();
					$('.easyui-dialog .CustomStyleSelectBoxInner').html(value);
				}
				if(arrayParam != null && arrayParam[0] == 0){
					var value = $('.easyui-dialog #statusDialog option[selected=selected]').html();
					$('.easyui-dialog .CustomStyleSelectBoxInner').html(value);
					$('.easyui-dialog #seachStyle1Code').removeAttr('disabled').focus();
				}
			
				$('.easyui-dialog #__btnSave').bind('click',function(event) {
					var s = -1;
					var code = $('.easyui-dialog #seachStyle1Code').val().trim();
					var name = $('.easyui-dialog #seachStyle1Name').val().trim();
					var msg = "";
					if(code == undefined || code == "" || code == null){
						msg = "Bạn chưa nhập giá trị cho trường Mã nhóm.";
						$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					if(msg.length == 0){
						msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Code', 'Mã nhóm', Utils._CODE, null);
						if(msg.length >0){
							$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
							$('.easyui-dialog #seachStyle1Code').focus();
							return false;
						}
					}
					if(msg.length == 0){
						if(name == undefined || name == "" || name == null){
							msg = "Bạn chưa nhập giá trị cho trường Tên nhóm.";
							$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
							$('.easyui-dialog #seachStyle1Name').focus();
							return false;
						}
					}
					if(msg.length == 0){
						msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Name', 'Tên nhóm', Utils._NAME, null);
						if(msg.length >0){
							$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
							$('.easyui-dialog #seachStyle1Name').focus();
							return false;
						}
					}
					if(arrayParam != null){
						s = arrayParam[0];
					}
					if(s == 0){
						var params = new Object();
						params.groupCode = code;
						params.groupName = name;
						params.statusValue = $('.easyui-dialog #statusDialog').val();
						Utils.addOrSaveData(params, '/catalog/group-po/creategrouppo', null, 'errMsgSearchCreate', function(data){
							$('#' +searchDialog).dialog('close'); 
							GroupPOAuto.search();
						}, null, null, null, "Bạn có muốn thêm nhóm mới không ?", null);
					}
					
				});
				$('.easyui-dialog #__btnSave1').bind('click',function(event) {
					var s = -1;
					var code = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').val().trim();
					var name = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').val().trim();
					var msg = "";
					if(code == undefined || code == "" || code == null){
						msg = "Bạn chưa nhập giá trị cho trường Mã nhóm.";
						$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
						$('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').focus();
						return false;
					}
					if(msg.length == 0){
						msg = Utils.getMessageOfSpecialCharactersValidateEx1('#searchStyle1EasyUIDialogUpdate #seachStyle1Code', 'Mã nhóm', Utils._CODE, null);
						if(msg.length >0){
							$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
							$('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').focus();
							return false;
						}
					}
					if(msg.length == 0){
						if(name == undefined || name == "" || name == null){
							msg = "Bạn chưa nhập giá trị cho trường Tên nhóm.";
							$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
							$('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').focus();
							return false;
						}
					}
					if(msg.length == 0){
						msg = Utils.getMessageOfSpecialCharactersValidateEx1('#searchStyle1EasyUIDialogUpdate #seachStyle1Name', 'Tên nhóm', Utils._NAME, null);
						if(msg.length >0){
							$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
							$('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').focus();
							return false;
						}
					}
					if(arrayParam != null){
						s = arrayParam[0];
					}
					if(s == 1){
						var params = new Object();
						params.id = arrayParam[1];
						var name = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').val().trim();
						params.groupName = name;
						params.statusValue = $('.easyui-dialog #statusDialogUpdate').val();
						Utils.addOrSaveData(params, '/catalog/group-po/updategrouppo', null, 'errMsgSearchUpdate', function(data){
							$('#' +searchDialog).dialog('close'); 
							GroupPOAuto.search();
						}, null, null, null, "Bạn có muốn cập nhật thông tin nhóm không ?", null);
					}
					
				});
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();
	        },
	        onClose : function(){
	        	$('#' +searchDiv).html(html);
	        	$('#' +searchDiv).css("visibility", "hidden");
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #__btnSave').unbind('click');
	        	$('.easyui-dialog #__btnSave1').unbind('click');
	        	$('.easyui-dialog #errMsgSearchCreate').html('').hide();
	        	$('.easyui-dialog #errMsgSearchUpdate').html('').hide();	
	        }
	    });
		return false;
	},
	getSearchStyleCustomerMapGridUrl : function(url, code, name,address, arrParam) {		
		var searchUrl = '';
		searchUrl = url;
		var curDate = new Date();
		var curTime = curDate.getTime();
		searchUrl+= '?curTime=' + encodeChar(curTime);
		if (arrParam != null && arrParam != undefined) {
			for ( var i = 0; i < arrParam.length; i++) {
				searchUrl += "&" + encodeChar(arrParam[i].name) + "=" + encodeChar(arrParam[i].value);
			}
		}
		searchUrl+= "&customerCode=" + encodeChar(code) + "&customerName=" + encodeChar(name) + "&address=" + encodeChar(address);
		return searchUrl;
	},
	recursionFindParentShopCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
    	$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getParentShop',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.parentShopId != null && data.parentShopId != undefined){
					$('#check_' +data.parentShopId).attr('disabled','disabled').removeAttr('checked');
					CommonSearch.recursionFindParentShopCheck(data.parentShopId);
				}
			}
    	});
	},
	recursionFindParentShopUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getParentShop',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.parentShopId != null && data.parentShopId != undefined){
					var flag = true;
					$('.class_' +data.parentShopId).each(function(){
						if($(this).is(':checked') || $(this).is(':disabled')){
							flag = false;
						}
					});
					if(flag){
						$('#check_' +data.parentShopId).removeAttr('checked').removeAttr('disabled');
						CommonSearch.recursionFindParentShopUnCheck(data.parentShopId);
					}
					else{
						CommonSearch.recursionFindParentShopUnCheck();
					}
				}
			}
    	});
	}
}; // End commonSearch

var ExportActionLog = {
	CUSTOMER : 0,
	STAFF: 1,
	SHOP: 2,
	PRODUCT: 3,
	CUSTOMERCATLEVEL:4,
	STAFFCATLEVEL: 5,
	PROMOTION_PROGRAM: 6,
    INCENTIVE_PROGRAM: 7,
    FOCUS_PROGRAM: 8,
	DISPLAY_PROGRAM: 9,
	
	exportActionLog :function(objectId, type, fDate, tDate, errMsgId){
		var params = new Object();
		params.objectIdActionLog = objectId;
		params.fromDateActionLog = fDate;
		params.toDateActionLog = tDate;
		params.typeActionLog = type;
		$('#' +errMsgId).html('').hide();
		var msg = 'Bạn có muốn xuất file xem thông tin thay đổi?';
		Utils.addOrSaveData(params, '/commons/export/actionlog',null, errMsgId, function(data) {
			window.location.href = data.view;
			setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
                CommonSearch.deleteFileExcelExport(data.view);
			},2000);
		}, null, null, null, msg);
		return false;
	}
};
var CommonSearchEasyUI = {
	_currentSearchCallback : null,
	_arrParams : null,
	_mapMutilSelect : null,
	_isOpenDialog : null,
	_latbk: '',
	_lngbk: '',
	fitEasyDialog:function(objectId){
		if(objectId!=undefined && objectId!=null && objectId!=''){
			objectId="#" +objectId;
		}else objectId=".easyui-dialog";
		$(objectId).each(function(){
			try{
				if($(this).parent().css('display')!='none'){
					var hDialog=parseInt($(this).parent().height());
					var hWindow=parseInt($(window).height());
					if(hDialog>=hWindow){//dialog dài hơn window
						$(this).parent().css('top', document.documentElement.scrollTop);
					}else{
						var distant=hWindow-hDialog;
						distant=distant/2+document.documentElement.scrollTop;
						$(this).parent().css('top', distant);
//						$(this).parent().css('position', 'fixed');
					}
				}
			}catch(e){}
		});
	},
	hiddenTabIndex : function() {
		//@CuongND : my try Code <reset tabindex>
		var tabindex = -1;
		$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
			if (this.type != 'hidden') {
				$(this).attr("tabindex", tabindex);
				tabindex -=1;
			}
		});
		//end <reset tabindex>
	},
	showTabIndex : function (parentDiv) {
		var parentId = '';
		if(parentDiv!= null && parentDiv!= undefined){
			parentId = parentDiv;
		}
		var tabindex = 1;
		var tabindexObject = parentId +' .InputTextStyle';
		tabindexObject += parentId +' select';
		tabindexObject += parentId +' input';
		tabindexObject += parentId +' button';
		$(tabindexObject).each(function () {
			if (this.type != 'hidden') {
				$(this).attr("tabindex", '');
			}
			tabindex ++;
		});	
	},
	getSearchStyleEasyUIGridUrl : function(url,arrParam) {
		var searchUrl = '';
		searchUrl = url;
		var curDate = new Date();
		var curTime = curDate.getTime();
		searchUrl+= '?curTime=' + encodeChar(curTime);
		if (arrParam != null && arrParam != undefined) {
			for ( var i = 0; i < arrParam.length; i++) {
				searchUrl += "&" + encodeChar(arrParam[i].name) + "=" + encodeChar(arrParam[i].value);
			}
		}
		return searchUrl;
	},
	getResultSearchStyle2EasyUIDialog : function(rowIndex) {
		var grid = $('#searchStyle2EasyUIGrid');
		grid.datagrid('unselectAll');
		grid.datagrid('selectRow',rowIndex);
		var row = grid.datagrid('getSelected');		
		if (CommonSearchEasyUI._currentSearchCallback != null) {
			CommonSearchEasyUI._currentSearchCallback.call(this, row);
			$('#searchStyle2EasyUIDialog').dialog('close');
		}
		return false;
	},
	openSearchStyle2EasyUIDialog : function(codeText, nameText, title, url, callback, codeFieldText, nameFieldText, arrParam) {
		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		var html = $('#searchStyle2EasyUIDialogDiv').html();
		$('#searchStyle2EasyUIDialog').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height :'auto',
	        onOpen: function(){
	        	//@CuongND : my try Code <reset tabindex>
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				//end <reset tabindex>
				$('#searchStyle2EasyUICode').focus();
				$('#searchStyle2EasyUIUrl').val(url);
				$('#searchStyle2EasyUICodeText').val(codeFieldText);
				$('#searchStyle2EasyUINameText').val(nameFieldText);
				CommonSearchEasyUI._currentSearchCallback = callback;
				CommonSearchEasyUI._arrParams = arrParam;
				CommonSearchEasyUI._isOpenDialog = true;
				
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				
				$('#seachStyle2EasyUICodeLabel').html(codeText);
				$('#seachStyle2EasyUINameLabel').html(nameText);
				Utils.bindAutoButtonEx('#searchStyle2EasyUIDialog','btnSearchStyle2EasyUI');
				$('#searchStyle2EasyUIGrid').show();
				$('#searchStyle2EasyUIGrid').datagrid({
					url : CommonSearchEasyUI.getSearchStyleEasyUIGridUrl(url, arrParam),
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					pageList  : [10,20,30],
					width : ($('#searchStyle2EasyUIContainerGrid').width()),
				    columns:[[  
				        {field:codeField,title:codeText,align:'left', width:150, sortable : false,resizable : false, formatter:function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          }},  
				        {field:nameField,title:nameText,align:'left', width:400, sortable : false,resizable : false, formatter:function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          }},  
				        {field:'select', align:'center',sortable : false,resizable : false,formatter : CommonSearchEasyUIFormatter.selectSearchStyle2EasyUIDialog},
				        {field:'id',hidden : true},
				    ]],
				    onLoadSuccess :function(){
				    	var easyDiv = '#searchStyle2EasyUIContainerGrid ';
				    	$(easyDiv+'.datagrid-header-rownumber').html(jsp_common_numerical_order);
				    	tabindex = 1;
			    		$('#searchStyle2EasyUIDialog input,#searchStyle2EasyUIDialog select,#searchStyle2EasyUIDialog button').each(function () {
				    		if (this.type != 'hidden') {
					    	    $(this).attr("tabindex", '');
								tabindex++;
				    		}
						});
			    		updateRownumWidthForDataGrid('#searchStyle2EasyUIContainerGrid');
			    		setTimeout(function(){
			    			CommonSearchEasyUI.fitEasyDialog('searchStyle2EasyUIDialog');
			    		},1000);
				    }
				});  
				$('#btnSearchStyle2EasyUI').bind('click',function(event) {
					var code = $('#searchStyle2EasyUICode').val().trim();
					var name = $('#searchStyle2EasyUIName').val().trim();
					$('#searchStyle2EasyUIGrid').datagrid('load',{code :code, name: name});
					$('#searchStyle2EasyUICode').focus();
				});
				$('#btnSearchStyle2EasyUIUpdate').hide();
				$('#btnSearchStyle2EasyUIClose').show();
	        },
	        onClose : function(){
	        	$('#searchStyle2EasyUIDialog').dialog('destroy');
	        },
	        onDestroy :function() {
	        	var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				CommonSearchEasyUI._currentSearchCallback = null;
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();			
	        	$('#searchStyle2EasyUIDialogDiv').html(html);
	        	CommonSearchEasyUI._isOpenDialog = false;
	        }
	    });
		return false;
	},

	openSearchStyle2EasyUIMutilSelectDialog : function(codeText, nameText, title, url, callback, codeFieldText, nameFieldText, arrParam) {
		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		CommonSearchEasyUI._mapMutilSelect = new Map();
		var html = $('#searchStyle2EasyUIDialogDiv').html();
		$('#searchStyle2EasyUIDialog').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height :'auto',
	        onOpen: function(){
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				$('#searchStyle2EasyUICode').focus();
				$('#searchStyle2EasyUIUrl').val(url);
				$('#searchStyle2EasyUICodeText').val(Utils.XSSEncode(codeFieldText));
				$('#searchStyle2EasyUINameText').val(Utils.XSSEncode(nameFieldText));
				CommonSearchEasyUI._currentSearchCallback = callback;
				CommonSearchEasyUI._arrParams = arrParam;
				CommonSearchEasyUI._isOpenDialog = true;
				
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				
				$('#seachStyle2EasyUICodeLabel').html(codeText);
				$('#seachStyle2EasyUINameLabel').html(nameText);
				Utils.bindAutoButtonEx('#searchStyle2EasyUIDialog','btnSearchStyle2EasyUI');
				$('#searchStyle2EasyUIGrid').show();
				var url1 = CommonSearchEasyUI.getSearchStyleEasyUIGridUrl(url, arrParam);
				$('#searchStyle2EasyUIGrid').datagrid({
					url : url1,
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					scrollbarSize : 0,
					pageList  : [10,20,30],
					width : ($('#searchStyle2EasyUIContainerGrid').width()),
				    columns:[[  
				        {field:codeField,title:codeText,align:'left', width:150, sortable : false,resizable : false, formatter:function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          }},  
				        {field:nameField,title:nameText,align:'left', width:356, sortable : false,resizable : false, formatter:function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          }},  
				        {field:'id',checkbox:true, align:'center',sortable : false,resizable : false},
				    ]],
				    onSelect : function(rowIndex, rowData) {
				    	var selectedId = rowData['id'];
				    	CommonSearchEasyUI._mapMutilSelect.put(selectedId, rowData);
				    },
				    onUnselect : function(rowIndex, rowData) {
				    	var selectedId = rowData['id'];
				    	CommonSearchEasyUI._mapMutilSelect.remove(selectedId);
				    },
				    onSelectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['id'];
						    	CommonSearchEasyUI._mapMutilSelect.put(selectedId, row);
				    		}
				    	}
				    },
				    onUnselectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['id'];
						    	CommonSearchEasyUI._mapMutilSelect.remove(selectedId);
				    		}
				    	}
				    },
				    onLoadSuccess :function(){
				    	Utils.bindAutoButtonEx('.easyui-dialog','btnSearchStyle2EasyUI');
				    	var easyDiv = '#searchStyle2EasyUIContainerGrid ';
				    	$(easyDiv+'.datagrid-header-rownumber').html(jsp_common_numerical_order);
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=id]').each(function() {
			    			var selectedId = this.value;
			    			if(CommonSearchEasyUI._mapMutilSelect.get(selectedId) != null || CommonSearchEasyUI._mapMutilSelect.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#searchStyle2EasyUIGrid').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
				    	tabindex = 1;
			    		$('#searchStyle2EasyUIDialog input,#searchStyle2EasyUIDialog select,#searchStyle2EasyUIDialog button').each(function () {
				    		if (this.type != 'hidden') {
					    	    $(this).attr("tabindex", '');
								tabindex++;
				    		}
						});
			    		updateRownumWidthForDataGrid('#searchStyle2EasyUIContainerGrid');
			    		$(easyDiv+'.datagrid-header-rownumber').css('width', '50px');
				    	$(easyDiv+'.datagrid-cell-rownumber').css('width', '50px');
			    		setTimeout(function(){
			    			CommonSearchEasyUI.fitEasyDialog('searchStyle2EasyUIDialog');
			    		},1000);
				    }
				});
				$('#btnSearchStyle2EasyUI').unbind('click');
				$('#btnSearchStyle2EasyUI').bind('click',function(event) {
					var code = $('#searchStyle2EasyUICode').val().trim();
					var name = $('#searchStyle2EasyUIName').val().trim();
					$('#searchStyle2EasyUIGrid').datagrid('load',{code :code, name: name});
					$('#searchStyle2EasyUICode').focus();
				});
				$('#btnSearchStyle2EasyUIUpdate').show();
				$('#btnSearchStyle2EasyUIClose').hide();
				$('#btnSearchStyle2EasyUIUpdate').unbind('click');
				$('#btnSearchStyle2EasyUIUpdate').bind('click', function(event) {
					$('#errMsgSearchStyle2EasyUI').html('').hide();
        			if(CommonSearchEasyUI._mapMutilSelect == null || CommonSearchEasyUI._mapMutilSelect.size() <= 0) {
    	        		if (codeFieldText == 'shopCode'){
    	        			$('#errMsgSearchStyle2EasyUI').html('Bạn chưa chọn đơn vị nào. Yêu cầu chọn').show();
    	        		}else if(codeFieldText == 'staffCode'){
    	        			$('#errMsgSearchStyle2EasyUI').html('Bạn chưa chọn nhân viên nào. Yêu cầu chọn').show();
    	        		}else if(codeFieldText == 'productCode'){
    	        			$('#errMsgSearchStyle2EasyUI').html('Bạn chưa chọn sản phẩm nào. Yêu cầu chọn').show();
    	        		}else if(codeFieldText == 'productInfoCode'){
    	        			$('#errMsgSearchStyle2EasyUI').html('Bạn chưa chọn ngành hàng nào. Yêu cầu chọn').show();
    	        		}else{
    	        			$('#errMsgSearchStyle2EasyUI').html('Bạn chưa chọn mục nào. Yêu cầu chọn').show();
    	        		}
    	        		return false;
    	        	}
    	        	if (CommonSearchEasyUI._currentSearchCallback != null) {
    	        		CommonSearchEasyUI._currentSearchCallback.call(this, CommonSearchEasyUI._mapMutilSelect.valArray);
    	    			$('#searchStyle2EasyUIDialog').dialog('close');
    	    		}
				});
	        },
	        onClose : function(){
	        	$('#searchStyle2EasyUIDialog').dialog('destroy');
	        },
	        onDestroy :function() {
	        	var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				CommonSearchEasyUI._currentSearchCallback = null;
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();			
	        	$('#searchStyle2EasyUIDialogDiv').html(html);
	        	CommonSearchEasyUI._isOpenDialog = false;
	        }
	    });
		return false;
	},
	searchNVUnitTreeDialog : function(callback, arrParam) {
		CommonSearchEasyUI.openSearchStyle2EasyUIMutilSelectDialog("Mã NV", "Tên NV", "Thêm nhân viên", "/commons/unittree/search-staff",
				callback,"staffCode", "staffName", arrParam);
		return false;
	},
	searchProductDialog : function(callback, arrParam, url) {
		CommonSearchEasyUI.openSearchStyle2EasyUIMutilSelectDialog("Mã sản phẩm", "Tên sản phẩm", "Thêm sản phẩm", url,
				callback,"productCode", "productName", arrParam);
		return false;
	},
	viewBigMap : function(arrParam, title) {
		var html = $('#viewBigMapDiv').html();
		$('#viewBigMap').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 950,
	        height :580,
	        onOpen: function(){
	        	$("#viewBigMap").addClass("easyui-dialog");
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
	        	var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
					//load map
					var lat = arrParam.lat;
					var lng = arrParam.lng;
					var zoom=null;
					if(lat!='' && lng!=''){
						zoom=10;
					}
					VTMapUtil.loadMapResource(function() {
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longitude) {
							$('#lat').val(latitude);
							$('#lng').val(longitude);
						}, true);
					});
					clearTimeout(tm);
				}, 500);
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});			
	        },
        	onClose : function(){
        		$('#viewBigMap').dialog('close');
        		var lat = $('#lat').val();
				var lng = $('#lng').val();
				$('#mapContainerUnitTree').html('');
				VTMapUtil.loadMapResource(function() {
					ViettelMap.loadBigMapEx('mapContainerUnitTree', lat, lng, null, function(latitude, longitude) {
						$('#lat').val(latitude);
						$('#lng').val(longitude);
					}, true);
				});
				$('#viewBigMap').dialog('destroy');
				$('#viewBigMapDiv').html(html);
 	        }
		});
		return false;
		
	},
	viewBigMapShopTree : function(arrParam, title) {
		CommonSearchEasyUI._latbk = arrParam.lat;
		CommonSearchEasyUI._lngbk = arrParam.lng;
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 950,
	        height :580,
	        onOpen: function(){
	        	$("#viewBigMap").addClass("easyui-dialog");
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
	        	var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
					//load map
					var lat = arrParam.lat;
					var lng = arrParam.lng;
					var zoom=null;
					if(lat!='' && lng!=''){
						zoom=12;
					}
					VTMapUtil.loadMapResource(function() {
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longitude) {
							$('#lat').val(latitude);
							$('#lng').val(longitude);
						}, true);
					});
					clearTimeout(tm);
				}, 1500);
				$('.easyui-dialog #imgBtnDeleteLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnDeleteLatLng').bind('click',function(){
	        		$('#lat').val('');
					$('#lng').val('');
	        		ViettelMap.clearOverlays();
	        		ViettelMap._listOverlay = new Array(); 
	        		if (ViettelMap._marker != null){
	                	ViettelMap._marker.setMap(null);
	                }
	                
	        	});
	        	$('.easyui-dialog #imgBtnUpdateLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnUpdateLatLng').bind('click',function(){
	        		var lat = '';
					var lng = '';
					lat = $('#lat').val();
					lng = $('#lng').val();
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							$('#lat').val(latitude);
							$('#lng').val(longitude);
						}, true);
		    		});
		    		$('#viewBigMap').dialog('close');
	        	});
	        	$('.easyui-dialog #imgBtnCancelLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnCancelLatLng').bind('click',function(){
	        		var zoom = 12;
					if(UnitTreeCatalog.latbk.trim().length==0 || UnitTreeCatalog.lngbk.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', CommonSearchEasyUI._latbk, CommonSearchEasyUI._lngbk, zoom, function(latitude, longtitude) {
							$('#lat').val(latitude);
							$('#lng').val(longitude);
						}, true);
		    		});
	        	});
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});			
	        },
        	onClose : function(){
        		var lat = $('#lat').val();
				var lng = $('#lng').val();
				$('#mapContainerUnitTree').html('');
				VTMapUtil.loadMapResource(function() {
					ViettelMap.loadBigMapEx('mapContainerUnitTree', lat, lng, null, function(latitude, longitude) {
						$('#lat').val(latitude);
						$('#lng').val(longitude);
					}, true);
				});
				$('#viewBigMap').html(html);
 	        }
		});
		return false;
	}
};
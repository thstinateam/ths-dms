var EasyUiCommon = {
	editIndex :undefined,
	editIndexDetail :undefined,
	_xhrDel : null,
	_xhrSave : null,
	_mapCheckProduct: new Map(),
	_mapNewDataSubGrid1: new Map(),
	_mapNewDataSubGrid2: new Map(),
	_lstProductData: new Map(),
	_lstProduct : null,
	inputId:null,
	field:null,
	type:null,
	apParamCode:null,
	productCode : '',
	productName : null,
	_lstSize :null,
	pCode: '',
	pName: '',
	pCodeRep: '',
	pQuantity: 0,
	REQUIRE_PRODUCT: 1,
	QUANTITY_PRODUCT: 2,
	AMOUNT_PRODUCT: 3,
	DETAIL_PRODUCT: 4,
	AUTOCOMPLETE_ONE: 1,
	AUTOCOMPLETE_MUL: 2,
	isMulti:null,
	typeChange:0,
	indexDeleted:[],
	_lstProductSelected:[],
	_mapProductOnSecondDialog: new Map(),
	_mapProductCodeStr1: new Map(),
	_mapProductCodeStr2: new Map(),
	_lstProductExpand:[],
	_lstProductOnChange:[],
	hasChange:false,
	isExceptZ:false,
	totalRowDelete:0,
	_totalRowGrid:0,
	codeByGroup:'',
	endEditing : function(gridId,field){
		if (EasyUiCommon.editIndex == undefined){
			return true;
		} 
		if(gridId != null && gridId != undefined){
			if ($('#'+gridId).datagrid('validateRow', EasyUiCommon.editIndex)){  
				$('#'+gridId).datagrid('endEdit', EasyUiCommon.editIndex);  
				EasyUiCommon.editIndex = undefined;  
				return true;  
			}else{
				return false;
			}
		}else{
			return true;
		}
	},
	endEditingDetail : function(gridId,field){
		if (EasyUiCommon.editIndexDetail == undefined){
			return true;
		} 
		if(gridId != null && gridId != undefined){
			if ($('#'+gridId).datagrid('validateRow', EasyUiCommon.editIndexDetail)){ 
				$('#'+gridId).datagrid('endEdit', EasyUiCommon.editIndexDetail);  
				EasyUiCommon.editIndexDetail = undefined;  
				return true;  
			}else{
				return false;
			}
		}else{
			return true;
		}
	},
	appendRowOnGrid:function(gridId){
		var type = $('#apParamCode').val().trim();
		$('#grid').datagrid('selectRow',0);
		var data = $('#grid').datagrid('getRows');
		var isAppend = false;
		if(data.length == 0){
			$('#'+gridId).datagrid('appendRow',{});
			isAppend = true;
		}else{
				if(type == 'ZV01' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0 ){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV02' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV04' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV05' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV03' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].quantity != null && data[data.length-1].quantity > 0){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV06' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].amount != null && data[data.length-1].amount > 0){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV07' ){
					if(data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0
						|| EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
					}
				}else if(type == 'ZV08' ){
					if(data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0
						|| EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
					}
				}else if(type == 'ZV10' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0
						|| EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
					}
				}else if(type == 'ZV11' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0
						|| EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
					}
				}else if(type == 'ZV09' || type == 'ZV23'){
					if(data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						||EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
						EasyUiCommon._mapNewDataSubGrid2.put(data.length-1,new Array());
					}
				}else if(type == 'ZV12' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						||EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
						EasyUiCommon._mapNewDataSubGrid2.put(data.length-1,new Array());
					}
				}else if(type == 'ZV15' ){
					if(data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV18' ){
					if(data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV13' || type == 'ZV16' ){
					if(data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0
						||data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV14' || type == 'ZV17' ){
					if(data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0
						||data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV19' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV20' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV21' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						||data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV22' ){
					if(data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						||EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#'+gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
						EasyUiCommon._mapNewDataSubGrid2.put(data.length-1,new Array());
					}
				}
		}
		if(isAppend){
			EasyUiCommon._totalRowGrid++;
		}
		$('#grid').datagrid('selectRow',$('#grid').datagrid('getRows').length-1);
	},
	loadCommonDataGrid:function(type,permission){
		EasyUiCommon.indexDeleted = [];
		EasyUiCommon._mapNewDataSubGrid1 = new Map();
		EasyUiCommon._mapNewDataSubGrid2 = new Map();		
		EasyUiCommon._mapProductCodeStr1 = new Map();
		EasyUiCommon._mapProductCodeStr2 = new Map();
		var urlComboGrid = '';
		var url = '';
		if(type == 'ZV01'){
			url = PromotionCatalog.getGridZV01Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid01020405('grid',url,'productCode','productName','quantity','discountPercent','Mã SP','Tên SP','SL mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV02'){
			url = PromotionCatalog.getGridZV01Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid01020405('grid',url,'productCode','productName','quantity','discountAmount','Mã SP','Tên SP','SL mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV04'){
			url = PromotionCatalog.getGridZV01Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid01020405('grid',url,'productCode','productName','amount','discountPercent','Mã SP','Tên SP','TT mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV05'){
			url = PromotionCatalog.getGridZV01Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid01020405('grid',url,'productCode','productName','amount','discountAmount','Mã SP','Tên SP','TT mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV03'){
			url = PromotionCatalog.getGridZV03Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid0306('grid',url,'productCode','productName','quantity','freeProductCodeStr','Mã SP','Tên SP','SL mua','SP khuyến mãi',urlComboGrid,type,'saleQty','Số lượng',permission);
		}else if(type == 'ZV06'){
			url = PromotionCatalog.getGridZV03Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid0306('grid',url,'productCode','productName','amount','freeProductCodeStr','Mã SP','Tên SP','TT mua','SP khuyến mãi',urlComboGrid,type,'saleQty','Số lượng',permission);
		}else if(type == 'ZV07'){
			url = PromotionCatalog.getGridZV07Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid07081011('grid',url,'quantity','discountPercent','SL mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV08'){
			url = PromotionCatalog.getGridZV07Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid07081011('grid',url,'quantity','discountAmount','SL mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV10'){
			url = PromotionCatalog.getGridZV07Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid07081011('grid',url,'amount','discountPercent','TT mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV11'){
			url = PromotionCatalog.getGridZV07Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid07081011('grid',url,'amount','discountAmount','TT mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV09'){
			url = PromotionCatalog.getGridZV09Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid0912('grid',url,'quantity','SL mua',urlComboGrid,type,permission);
		}else if(type == 'ZV12'){
			url = PromotionCatalog.getGridZV09Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid0912('grid',url,'amount','TT mua',urlComboGrid,type,permission);
		}else if(type == 'ZV15'){
			url = PromotionCatalog.getGridZV09Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid1518('grid',url,urlComboGrid,type,permission);
		}else if(type == 'ZV18'){
			url = PromotionCatalog.getGridZV09Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid1518('grid',url,urlComboGrid,type,permission);
		}else if(type == 'ZV13'){
			url = PromotionCatalog.getGridZV13Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid13141617('grid',url,'discountPercent','% KM tiền',urlComboGrid,type,'quantity','Số lượng',permission);
		}else if(type == 'ZV14'){
			url = PromotionCatalog.getGridZV13Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid13141617('grid',url,'discountAmount','TT khuyến mại',urlComboGrid,type,'quantity','Số lượng',permission);
		}else if(type == 'ZV16'){
			url = PromotionCatalog.getGridZV13Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid13141617('grid',url,'discountPercent','% KM tiền',urlComboGrid,type,'amount','TT mua',permission);
		}else if(type == 'ZV17'){
			url = PromotionCatalog.getGridZV13Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid13141617('grid',url,'discountAmount','TT khuyến mại',urlComboGrid,type,'amount','TT mua',permission);
		}else if(type == 'ZV19'){
			url = PromotionCatalog.getGridZV19Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid1920('grid',url,'amount','discountPercent','TT mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV20'){
			url = PromotionCatalog.getGridZV19Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid1920('grid',url,'amount','discountAmount','TT mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV21'){
			url = PromotionCatalog.getGridZV21Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid21('grid',url,'amount','freeProductCodeStr','TT mua','SP khuyến mãi',urlComboGrid,type,'quantity','SL khuyến mãi',permission);
		}
	},
	changeCommonGrid :function(type,permission){
		$('#divOverlay').show();
		$('#errMsgProduct').html('').hide();
		$('#errExcelMsg').html('').hide();	
		var lstString1 = new Array();
		var lstString2 = new Array();
		var lstString3 = new Array();
		var lstString4 = new Array();
		var lstNumber = [];
		var lstCode = [];
		var size = $('#grid').datagrid('getRows').length;
		EasyUiCommon.totalRowDelete = 0;
		//////DETAIL FOR SUBGRID
		for(var i= 0;i<size;i++){
//			var indexDetailErr = 0;
			var str = i.toString();
			if(type == 'ZV03' || type == 'ZV06'){
				var sizeDetail = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
					sizeDetail = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					if(sizeDetail > 0){
						lstString2.push(sizeDetail);
					}
					for(var k = 0;k<sizeDetail;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						a2.push(objDetail[k].productCode);
						if(objDetail[k].quantity == '' || objDetail[k].quantity == undefined){
							objDetail[k].quantity = 0;
						}
						a2.push(objDetail[k].quantity);
						lstString3.push(a2);
					}
				}
			}else if(type == 'ZV07' || type == 'ZV08' || type == 'ZV10' || type == 'ZV11' ){
				var sizeDetail = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
					sizeDetail = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					if(sizeDetail > 0){
						lstString2.push(sizeDetail);
					}
					for(var k = 0;k<sizeDetail;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						a2.push(objDetail[k].productCode);
						if(objDetail[k].required == '' || objDetail[k].required == undefined){
							objDetail[k].required = 0;
						}
						a2.push(objDetail[k].required);
						lstString3.push(a2);
					}
				}
			}else if(type == 'ZV09' || type == 'ZV12' || type == 'ZV15' || type == 'ZV18' ){
				var sizeDetail1 = 0;
				var sizeDetail2 = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid2.get(i) != null){
					sizeDetail1 = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					sizeDetail2 = EasyUiCommon._mapNewDataSubGrid2.get(i).length;
					var sizeIndex = [];
					if(sizeDetail1 > 0 && sizeDetail2 > 0){
						sizeIndex.push(sizeDetail1);
						sizeIndex.push(sizeDetail2);
					}else{
						sizeIndex.push(0);
						sizeIndex.push(0);
					}
					lstString2.push(sizeIndex);
					for(var k = 0;k<sizeDetail1;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						if(type == 'ZV15' || type == 'ZV18'){
							if(i == 0){
								lstCode.push(objDetail[k].productCode.trim());
							}else{
								if(lstCode.indexOf(objDetail[k].productCode.trim()) == -1){
									$('#errMsgProduct').html('Sản phẩm mua ở các dòng bắt buộc phải giống nhau.').show();
									break;
								}
							}
						}
						a2.push(objDetail[k].productCode);
						if(type == 'ZV09' || type == 'ZV12'){
							if(objDetail[k].required == '' || objDetail[k].required == undefined){
								objDetail[k].required = 0;
							}
							a2.push(objDetail[k].required);
						}else if(type == 'ZV15'){
							if(objDetail[k].quantity == '' || objDetail[k].quantity == undefined){
								objDetail[k].quantity = 0;
							}
							a2.push(objDetail[k].quantity);
						}else if(type == 'ZV18'){
							if(objDetail[k].amount == '' || objDetail[k].amount == undefined){
								objDetail[k].amount = 0;
							}
							a2.push(objDetail[k].amount);
						}
						lstString3.push(a2);
					}
					for(var j = 0;j<sizeDetail2;j++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid2.get(i);
						var a2 = [];
						if(objDetail[j].productCode == '' || objDetail[j].productCode == undefined){
							objDetail[j].productCode = 0;
						}
						a2.push(objDetail[j].productCode);
						if(objDetail[j].quantity == '' || objDetail[j].quantity == undefined){
							objDetail[j].quantity = 0;
						}
						a2.push(objDetail[j].quantity);
						lstString4.push(a2);
					}
				}
			}else if(type == 'ZV13' || type == 'ZV14' || type == 'ZV16' || type == 'ZV17' ){
				var sizeDetail = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
					sizeDetail = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					if(lstString2.length > 0 && (lstString2.indexOf(sizeDetail) == -1 || lstString2.indexOf(parseInt(sizeDetail)) == -1)){
						$('#errMsgProduct').html('Sản phẩm mua ở các dòng bắt buộc phải giống nhau.').show();
						break;
					}
					if(sizeDetail > 0){
						lstString2.push(sizeDetail);
					}
					for(var k = 0;k<sizeDetail;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						if(i == 0){
							lstCode.push(objDetail[k].productCode.trim());
						}else{
							if(lstCode.indexOf(objDetail[k].productCode.trim()) == -1){
								$('#errMsgProduct').html('Sản phẩm mua ở các dòng bắt buộc phải giống nhau.').show();
								break;
							}
						}
						a2.push(objDetail[k].productCode);
						if(type == 'ZV13' || type == 'ZV14'){
							if(objDetail[k].quantity == '' || objDetail[k].quantity == undefined){
								objDetail[k].quantity = 0;
							}
							a2.push(objDetail[k].quantity);
						}else{
							if(objDetail[k].amount == '' || objDetail[k].amount == undefined){
								objDetail[k].amount = 0;
							}
							a2.push(objDetail[k].amount);
						} 
						lstString3.push(a2);
					}
				}
			}else if(type == 'ZV21'){
				var sizeDetail = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && EasyUiCommon._mapNewDataSubGrid1.get(i) != null){
					sizeDetail = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					if(sizeDetail > 0){
						lstString2.push(sizeDetail);
					}
					for(var k = 0;k<sizeDetail;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						a2.push(objDetail[k].productCode);
						if(objDetail[k].quantity == '' || objDetail[k].quantity == undefined){
							objDetail[k].quantity = 0;
						}
						a2.push(objDetail[k].quantity);
						lstString3.push(a2);
					}
				}
			}
		}
		
		//////HEADER FOR GRID
		if($('#errMsgProduct').html() != '' && $('#errMsgProduct').html().length > 0 ){
			$('#divOverlay').hide();
			return false;
		}
		var indexErr = 0;
		var indexConfirm = 0;
		var isConfirm = false;
		for(var i= 0;i<size;i++){
			var str = i.toString();
			if(i == 0){
				$('#grid').datagrid('acceptChanges');
				EasyUiCommon.formatLabelAfterChange(type);
			}
			var objHeader = $('#grid').datagrid('getData').rows;
			if($('#errMsgProduct').html() == '' || $('#errMsgProduct').html().length == 0 ){
				indexErr = i +1;
				if(type == 'ZV01' || type == 'ZV02' || type == 'ZV04' || type == 'ZV05'){
					if($.inArray(str,EasyUiCommon.indexDeleted) == -1){
						if(objHeader[i].productCode != '' && objHeader[i].productCode != undefined ){
							var numExist = 0;
							if(lstString1.indexOf(objHeader[i].productCode) > -1){
								numExist = 1;
							}
							lstString1.push(objHeader[i].productCode);
							if(type == 'ZV01'){
								if(numExist == 1){
									if(lstString2.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstString2.indexOf(objHeader[i].quantity) > -1){
										numExist = 2;
									}
								}
								if(numExist == 2){
									$('#errMsgProduct').html(' Sản phẩm ' + Utils.XSSEncode(objHeader[i].productCode) +' với số lượng mua này đã tồn tại.').show();
								}
								lstString2.push(objHeader[i].quantity);
								lstString3.push(objHeader[i].discountPercent);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV02'){
								if(numExist == 1){
									if(lstString2.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstString2.indexOf(objHeader[i].quantity) > -1){
										numExist = 2;
									}
								}
								if(numExist == 2){
									$('#errMsgProduct').html(' Sản phẩm ' + Utils.XSSEncode(objHeader[i].productCode) + ' với số lượng mua này đã tồn tại.').show();
								}
								lstString2.push(objHeader[i].quantity);
								lstString3.push(objHeader[i].discountAmount);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
							}else if(type == 'ZV04'){
								if(numExist == 1){
									if(lstString2.indexOf(parseInt(objHeader[i].amount)) > -1 || lstString2.indexOf(objHeader[i].amount) > -1){
										numExist = 2;
									}
								}
								if(numExist == 2){
									$('#errMsgProduct').html(' Sản phẩm ' + Utils.XSSEncode(objHeader[i].productCode) + ' với TT mua này đã tồn tại.').show();
								}
								lstString2.push(objHeader[i].amount);
								lstString3.push(objHeader[i].discountPercent);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV05'){
								if(numExist == 1){
									if(lstString2.indexOf(parseInt(objHeader[i].amount)) > -1 || lstString2.indexOf(objHeader[i].amount) > -1){
										numExist = 2;
									}
								}
								if(numExist == 2){
									$('#errMsgProduct').html(' Sản phẩm '+ Utils.XSSEncode(objHeader[i].productCode) + ' với TT mua này đã tồn tại.').show();
								}
								lstString2.push(objHeader[i].amount);
								lstString3.push(objHeader[i].discountAmount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].amount != '' &&  objHeader[i].discountAmount != '' && parseInt(objHeader[i].amount) < parseInt(objHeader[i].discountAmount)){
									isConfirm = true;
									indexConfirm = indexErr;
								}
							}
						}else{
							if(type == 'ZV01'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV02'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV04'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV05'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}
						}
					}
				}else if(type == 'ZV03' || type == 'ZV06'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1)){// && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
						if(objHeader[i].productCode != '' && objHeader[i].productCode != undefined ){
							var a1= [];
							a1.push(objHeader[i].productCode);
							if(type == 'ZV03'){
								a1.push(objHeader[i].quantity);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(EasyUiCommon._mapProductCodeStr1.get(i) == null || EasyUiCommon._mapProductCodeStr1.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng '+ indexErr).show();
								}
								for(var j=0;j<lstString1.length;j++){
									if(lstString1[j][0] == a1[0]&&lstString1[j][1] == a1[1]){
										$('#errMsgProduct').html('SP khuyến mãi và số lượng ở dòng '+ indexErr+' đã bị trùng').show();
									}
								}
							}else if(type == 'ZV06'){
								a1.push(objHeader[i].amount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(EasyUiCommon._mapProductCodeStr1.get(i) == null || EasyUiCommon._mapProductCodeStr1.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng '+ indexErr).show();
								}
								for(var j=0;j<lstString1.length;j++){
									if(lstString1[j][0] == a1[0]&&lstString1[j][1] == a1[1]){
										$('#errMsgProduct').html('SP khuyến mãi và tổng tiền ở dòng '+ indexErr+' đã bị trùng').show();
									}
								}
							}
							lstString1.push(a1);
						}else{
							if(type == 'ZV03'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (EasyUiCommon._mapProductCodeStr1.get(i) != null && EasyUiCommon._mapProductCodeStr1.get(i).length > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV06'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (EasyUiCommon._mapProductCodeStr1.get(i) != null && EasyUiCommon._mapProductCodeStr1.get(i).length > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}
						}
					}
				}else if(type == 'ZV07' || type == 'ZV08' ||type == 'ZV10' || type == 'ZV11'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
						if(EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
							var a1= [];
							if(type == 'ZV07'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstNumber.indexOf(objHeader[i].quantity) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].quantity);
								a1.push(objHeader[i].quantity);
								a1.push(objHeader[i].discountPercent);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV08'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstNumber.indexOf(objHeader[i].quantity) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].quantity);
								a1.push(objHeader[i].quantity);
								a1.push(objHeader[i].discountAmount);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
							}else if(type == 'ZV10'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].amount)) > -1 || lstNumber.indexOf(objHeader[i].amount) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].amount);
								a1.push(objHeader[i].amount);
								a1.push(objHeader[i].discountPercent);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV11'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].amount)) > -1 || lstNumber.indexOf(objHeader[i].amount) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].amount);
								a1.push(objHeader[i].amount);
								a1.push(objHeader[i].discountAmount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
							}
							lstString1.push(a1);
						}else{
							if(type == 'ZV07'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV08'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV10'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV11'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}
						}
					}
				}else if(type == 'ZV09' || type == 'ZV12' ||type == 'ZV15' || type == 'ZV18'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1)){
						if(EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
							var a1= [];
							if(type == 'ZV09'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstNumber.indexOf(objHeader[i].quantity) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].quantity);
								a1.push(objHeader[i].quantity);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(EasyUiCommon._mapNewDataSubGrid2.get(i) == null || EasyUiCommon._mapNewDataSubGrid2.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng '+ indexErr).show();
								}
								lstString1.push(a1);
							}else if(type == 'ZV12'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].amount)) > -1 || lstNumber.indexOf(objHeader[i].amount) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].amount);
								a1.push(objHeader[i].amount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(EasyUiCommon._mapNewDataSubGrid2.get(i) == null || EasyUiCommon._mapNewDataSubGrid2.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng '+ indexErr).show();
								}
								lstString1.push(a1);
							}else if(type == 'ZV15'){
								if(EasyUiCommon._mapNewDataSubGrid2.get(i) == null || EasyUiCommon._mapNewDataSubGrid2.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV18'){
								if(EasyUiCommon._mapNewDataSubGrid2.get(i) == null || EasyUiCommon._mapNewDataSubGrid2.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng '+ indexErr).show();
								}
							}
						}else{
							if(type == 'ZV09'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (EasyUiCommon._mapNewDataSubGrid2.get(i) != null && EasyUiCommon._mapNewDataSubGrid2.get(i).length > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV12'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (EasyUiCommon._mapNewDataSubGrid2.get(i) != null && EasyUiCommon._mapNewDataSubGrid2.get(i).length > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV15'){
								if(EasyUiCommon._mapNewDataSubGrid1.get(i) == null || EasyUiCommon._mapNewDataSubGrid1.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV18'){
								if(EasyUiCommon._mapNewDataSubGrid1.get(i) == null || EasyUiCommon._mapNewDataSubGrid1.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}
						}
					}
				}else if(type == 'ZV13' || type == 'ZV14' ||type == 'ZV16' || type == 'ZV17'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1)){
						if(EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
							var a1= [];
							if(type == 'ZV13'){
								a1.push(objHeader[i].discountPercent);
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV14'){
								a1.push(objHeader[i].discountAmount);
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
							}else if(type == 'ZV16'){
								a1.push(objHeader[i].discountPercent);
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV17'){
								a1.push(objHeader[i].discountAmount);
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
							}
							lstString1.push(a1);
						}else{
							if(type == 'ZV13'){
								if(objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV14'){
								if(objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV16'){
								if(objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV17'){
								if(objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng '+ indexErr).show();
								}
							}
						}
					}
				}else if(type == 'ZV19' || type == 'ZV20'){
					if($.inArray(str,EasyUiCommon.indexDeleted) == -1){
						if(objHeader[i].amount != '' && objHeader[i].amount > 0){
							lstString1.push(objHeader[i].amount);
							if(type == 'ZV19'){
								lstString2.push(objHeader[i].discountPercent);
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng '+ indexErr+' phải nhỏ hơn 100').show();
								}
								for(var j=i+1;j<size;j++){
									if(objHeader[i].amount == objHeader[j].amount){
										$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ (i+1)+' trùng với tổng tiền mua ở dòng '+(j+1)).show();
										$('#successMsgProduct').html('').hide();
									}
								}
							}else if(type == 'ZV20'){
								lstString2.push(objHeader[i].discountAmount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng '+ indexErr+' phải lớn hơn 0').show();
								}
								for(var j=i+1;j<size;j++){
									if(objHeader[i].amount == objHeader[j].amount){
										$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ (i+1)+' trùng với tổng tiền mua ở dòng '+(j+1)).show();
										$('#successMsgProduct').html('').hide();
									}
								}
								if(objHeader[i].amount != '' &&  objHeader[i].discountAmount != '' && parseInt(objHeader[i].amount) < parseInt(objHeader[i].discountAmount)){
									isConfirm = true;
									indexConfirm = indexErr;
								}
							}
						}else{
							if(type == 'ZV19'){
								if(objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0){
									$('#errMsgProduct').html('Vui lòng nhập tổng tiền mua ở dòng '+ indexErr).show();
								}
							}else if(type == 'ZV20'){
								if(objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0){
									$('#errMsgProduct').html('Vui lòng nhập tổng tiền mua ở dòng '+ indexErr).show();
								}
							}
						}
					}
				}else if(type == 'ZV21'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
						if(objHeader[i].amount != '' && objHeader[i].amount > 0){
							lstString1.push(objHeader[i].amount);
							if(EasyUiCommon._mapNewDataSubGrid1.get(i) == null || EasyUiCommon._mapNewDataSubGrid1.get(i).length == 0){
								$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng '+ indexErr).show();
							}
						}else{
							if(EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
								$('#errMsgProduct').html('Vui lòng nhập TT mua ở dòng '+ indexErr).show();
							}
						}
						for(var j=i+1;j<size;j++){
							if(objHeader[i].amount == objHeader[j].amount){
								$('#errMsgProduct').html('Tổng tiền mua ở dòng '+ (i+1)+' trùng với tổng tiền mua ở dòng '+(j+1)).show();
								$('#successMsgProduct').html('').hide();
							}
						}
					}
				}
			}
		}
		if($('#errMsgProduct').html() != '' && $('#errMsgProduct').html().length > 0 ){
			$('#grid').datagrid('selectRow',indexErr - 1);
			$('#divOverlay').hide();
			return false;
		}
//		console.log(lstString1);
//		console.log(lstString2);
//		console.log(lstString3);
//		console.log(lstString4);
//		if(type == 'ZV13' ||type == 'ZV14' ||type == 'ZV15' || type == 'ZV16' ||type == 'ZV17' ||type == 'ZV18'){
//		}
		var dataModel = new Object();		
		dataModel.promotionId = $('#promotionId').val().trim();
		dataModel.lstString1 = lstString1;
		dataModel.lstString2 = lstString2;
		dataModel.lstString3 = lstString3;
		if(type == 'ZV09' ||type == 'ZV12' ||type == 'ZV15' ||type == 'ZV18'){
			dataModel.lstString4 = lstString4;
		}
		$('#divOverlay').hide();
		if(isConfirm){
			$.messager.confirm('Xác nhận', 'Bạn nhập tổng tiền khuyến mại lớn hơn tổng tiền mua ở dòng '+indexConfirm+'. Bạn có muốn cập nhật hay không?', function(r){
				if (r){
					Utils.saveData(dataModel, "/catalog/promotion/change-common-data-grid", EasyUiCommon._xhrSave, null,function(data){
						EasyUiCommon.loadCommonDataGrid(type,permission);
						$('#successMsgProduct').html('Cập nhật thành công.').show();
						setTimeout(function() { $("#successMsgProduct").hide(); }, 3000);
					});
				}
			});	
		}else{
			Utils.addOrSaveData(dataModel, '/catalog/promotion/change-common-data-grid', EasyUiCommon._xhrSave,null, function(data){
				EasyUiCommon.loadCommonDataGrid(type,permission);
				$('#successMsgProduct').html('Cập nhật thành công.').show();
				setTimeout(function() { $("#successMsgProduct").hide(); }, 3000);
			});
		}
		return false;
	},
	reloadNewDataForSubGrid: function(gridId, gridIndex,gridDetailId){
		var size = $('#'+gridId).datagrid('getRows').length;
		for(var m = 0;m<size;m++){
			$('#'+gridId).datagrid('collapseRow',m);
			$('#'+gridId).datagrid('expandRow',m);
		}
		EasyUiCommon.loadLocalDataForSubGrid(gridDetailId,gridIndex);		
	},
	loadLocalDataForSubGrid: function(gridDetailId,gridIndex){
		var newData = {};
		if(gridDetailId != undefined){
			if(gridDetailId.substring(0,3) == 'ddv'){
				newData = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
			}else{
				newData = EasyUiCommon._mapNewDataSubGrid2.get(gridIndex);
			}
		}else{
			newData = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
		}
		if(newData!= undefined && newData!= null && newData.length > 0){
			for(var i=0;i<newData.length;i++){
				var numRow = $('#'+gridDetailId).datagrid('getRows').length;
				$('#'+gridDetailId).datagrid('insertRow',{index: numRow-1, row:newData[i]});
			}
		}
	},
	clearTmpRowInSubGrid: function(datagridView){
		$('.datagrid-row-detail .datagrid-view'+ datagridView +' .datagrid-body table tbody').each(function(){
			var arrSubChild = $(this).children();
			var rowIndex = -1;
			arrSubChild.each(function(){
			    if(!$(this).hasClass('datagrid-row') || $(this).attr('id').split('-')[3]!=datagridView || $(this).attr('datagrid-row-index')== rowIndex){
			        $(this).remove();
			    } else {
			        rowIndex = $(this).attr('datagrid-row-index');
			    }    
			});
		});		
		
	},
	updateRownumWidthForJqGridEX1:function(parentId,colReduceWidth){	
		var pId = '';
		if(parentId!= null && parentId!= undefined){
			pId = parentId + ' ';
		}
		var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
		var widthSTT = $('.easyui-dialog .datagrid-cell-rownumber').width();
		var s = $('.easyui-dialog .datagrid-header-row td[field='+colReduceWidth+'] div').width();
		if(EasyUiCommon._lstSize != null && EasyUiCommon._lstSize != undefined){ 
			s = EasyUiCommon._lstSize[0];
		}
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9;
			}
			var value = extWidth - widthSTT;
			s = s - value;
			$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
			$(pId + '.datagrid-header-rownumber').css('width',extWidth);
			$(parentId + ' .datagrid-row').each(function(){
				$(parentId + ' .datagrid-header-row td[field='+colReduceWidth+'] div').width(s);
				$(parentId + ' .datagrid-row td[field='+colReduceWidth+'] div').width(s);
			});
			
		}
	},
	loadAutoCompleteForProduct: function(filter){	//filter: desObj, pType, type	
		var choosePro = 0;
		var desObj = filter.desObj;
		var template = '';
		var gridDetailId = '';
		if(filter.gridId != null && filter.gridId != undefined){
			gridDetailId = filter.gridId;
		}
		if(filter.pType == EasyUiCommon.REQUIRE_PRODUCT){
			template = '<h3 style="float:left">${productCode}</h3><span style="float:right"><label style="font-size:10px;font-style:italic">BB mua:</label><input style="margin-top:7px" type="checkbox"/></span></br><p>${productName}</p>';
		} else if(filter.pType == EasyUiCommon.QUANTITY_PRODUCT){
			template = '<h3 style="float:left">${productCode}</h3><span style="float:right"><label style="font-size:10px;font-style:italic">SL mua:</label><input style="float:right !important;width:30px" class="InputTextStyle " type="text"/></span></br><p>${productName}</p>';
		} else if(filter.pType == EasyUiCommon.AMOUNT_PRODUCT){
			template = '<h3 style="float:left">${productCode}</h3><span style="float:right"><label style="font-size:10px;font-style:italic">TT mua:</label><input style="float:right !important;width:30px" class="InputTextStyle " type="text"/></span></br><p>${productName}</p>';
		} else if(filter.pType == EasyUiCommon.DETAIL_PRODUCT){
			template = '<h3 style="float:left">${productCode}</h3></br><p>${productName}</p>';
		}
		if(desObj != null && desObj != undefined){
			desObj.kendoAutoComplete({	 
				highlightFirst: true,
                filter: "startswith",
                placeholder: "Chọn sản phẩm...",
                separator: ", ",
                dataSource: {
                    type: "json",
                    serverFiltering: true,
                    serverPaging: true,
                    pageSize: 20,
                    transport: {
                        read: "/catalog/product/autocomplete?isExceptZCat=true"
                    }
                },
                dataTextField: "productCode",
                template: template,
                change: function(e){        
            		var value = this.element.val();
            		var arrVal = value.split(',');
            		var fVal = '';
            		if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && filter.pType == EasyUiCommon.REQUIRE_PRODUCT){
            			if(arrVal!= null && arrVal.length  > 0){
            				for(var i=0;i<arrVal.length;i++){
            					if(arrVal[i] != undefined){
            						var fSubVal = arrVal[i].trim().replace(/\*/g,'');
            						if(pCodeRep!= null && fSubVal == pCode && fVal.indexOf(fSubVal) == -1){
            							fVal += pCodeRep + ', ';
            						} else if(arrVal[i].trim().length > 0 && fVal.indexOf(fSubVal) == -1) {
            							fVal += arrVal[i].trim() + ', ';
            						}
            					}
            				}
            				if(gridDetailId != ''){
            					EasyUiCommon.appendRowKendo(gridDetailId,filter.pType,filter.index);
            				}
            				this.element.val(fVal);
            			}
            		} else 
            			if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && (filter.pType == EasyUiCommon.QUANTITY_PRODUCT || filter.pType == EasyUiCommon.AMOUNT_PRODUCT))
            		{
            			if(arrVal!= null && arrVal.length  > 0){
            				for(var i=0;i<arrVal.length;i++){
            					if(arrVal[i] != undefined){
            						var fSubVal = arrVal[i].replace(/\(|\)/g,' ').trim().split(' ')[0];                    			
            						if(pCodeRep!= null && fSubVal == pCode && fVal.indexOf(fSubVal) == -1){
            							fVal += pCodeRep + ', ';
            						} else if(arrVal[i].trim().length > 0 && fVal.indexOf(fSubVal) == -1) {
            							if(arrVal[i].trim().indexOf("(") > -1 && arrVal[i].trim().indexOf(")") > -1){
            								fVal += arrVal[i].trim() + ', ';
            							}
            						}
            					}
            				}
            				if(gridDetailId != ''){
            					EasyUiCommon.appendRowKendo(gridDetailId,filter.pType,filter.index);
            				}
        					this.element.val(fVal);
            			}
            		} 
            			else{
            				if(choosePro == 1){
            					this.element.val(pCode);
                    			this.element.parent().parent().parent().parent().parent().parent().parent().parent().children().first().next().children().children().children().children().children().children().val(pName);
                    			this.element.parent().parent().parent().parent().parent().parent().parent().parent().children().first().next().children().children().children().children().children().children().attr('disabled','disabled');
                    			$('#errMsgProduct').html('Mã sản phẩm không hợp lệ').hide();
            				}
            				else{
            					this.element.val('');
            					this.element.parent().parent().parent().parent().parent().parent().parent().parent().children().first().next().children().children().children().children().children().children().val('');
                    			this.element.parent().parent().parent().parent().parent().parent().parent().parent().children().first().next().children().children().children().children().children().children().attr('enabled','enabled');
            					var html = $('#errMsgProduct').html();
            					if(html!=undefined && html!=null)
            						$('#errMsgProduct').html('Mã sản phẩm không hợp lệ').show();                   			
            				}           				
            		}
                },
                dataBound: function(e) {
                	$(window).bind('keyup', function(evt){
                		if(evt.keyCode == 17){
                			if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && filter.pType == EasyUiCommon.REQUIRE_PRODUCT){
                				var chkItem =  $('.k-state-focused input[type=checkbox]');
                    			if(chkItem.is(':checked')){
                    				chkItem.removeAttr('checked');
                    			} else {
                    				chkItem.attr('checked','checked');
                    			}
                			} else if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && (filter.pType == EasyUiCommon.QUANTITY_PRODUCT || filter.pType == EasyUiCommon.AMOUNT_PRODUCT)){
                				setTimeout(function(){
                					$('.k-state-focused input').focus();
                				},500);
                			}               			
                		}
                	
                	});  
                },
                close: function(e){
                	$(window).unbind('keyup');	                            	
                },	                             
                select: function(e){
                	choosePro = 1;
                	pCode = $('.k-state-focused h3').html();
                	pName = $('.k-state-focused p').html();
                		pCodeRep = null;
                    	if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && filter.pType == EasyUiCommon.REQUIRE_PRODUCT){
                    		if($('.k-state-focused input[type=checkbox]').is(':checked')){
                    			EasyUiCommon.pQuantity = 1;
                        		pCodeRep = pCode + '*';
                        	}else{
                        		EasyUiCommon.pQuantity = 0;
                        	}
                    	} else if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && (filter.pType == EasyUiCommon.QUANTITY_PRODUCT || filter.pType == EasyUiCommon.AMOUNT_PRODUCT)){
                    		pCode = e.item.children().first().html();
                    		//var numProduct = $('.k-state-focused input').val().trim();
                    		var numProduct = $(e.item.children()[1]).children().last().val().trim();
                    		if(numProduct.length == 0){
                    			numProduct = 0;
                    		}
                    		EasyUiCommon.pQuantity = numProduct;
                    		pCodeRep = Utils.XSSEncode(pCode) + '(' + numProduct + ')';
                    	} else{
                    		pCode = Utils.XSSEncode(e.item.children().first().text());
                        	pName = Utils.XSSEncode(e.item.children().last().text());
                    	}
                	
                }                
            });			
			setTimeout(function(){
				desObj.css('width',desObj.parent().width()-6);
				desObj.parent().css('padding','0px !important');	
			},500);
		}
	},
	appendRowKendo:function(gridDetailId,type,index){
		var row = {};
		var i = $('#'+gridDetailId).datagrid('getRows').length-1; 
		$('#'+gridDetailId).datagrid('deleteRow', i); 
		if(type == EasyUiCommon.REQUIRE_PRODUCT){
			row = {productCode:pCode,
					productName:pName,
					required:EasyUiCommon.pQuantity};    			
		}else if(type == EasyUiCommon.QUANTITY_PRODUCT){
			row = {productCode:pCode,
					productName:pName,
					quantity:EasyUiCommon.pQuantity}; 
		}else if(type == EasyUiCommon.AMOUNT_PRODUCT){
			row = {productCode:pCode,
					productName:pName,
					amount:EasyUiCommon.pQuantity}; 
		}
		if(pCode != '' && pCode != null && pCode != undefined){
			var size = 0;
			var productCode = '';
			var isExist = false;
			if(gridDetailId.substring(0,3) == 'ddv'){
				if(EasyUiCommon._mapNewDataSubGrid1.size() > 0){
					size = EasyUiCommon._mapNewDataSubGrid1.get(index).length;
					for(var count = 0;count< size;count++){
						productCode = EasyUiCommon._mapNewDataSubGrid1.get(index)[count].productCode;
						if(pCode == productCode){
							isExist = true;
						}
					}
				}
			}else{
				if(EasyUiCommon._mapNewDataSubGrid2.size() > 0){
					size = EasyUiCommon._mapNewDataSubGrid2.get(index).length;
					for(var count = 0;count< size;count++){
						productCode = EasyUiCommon._mapNewDataSubGrid2.get(index)[count].productCode;
						if(pCode == productCode){
							isExist = true;
						}
					}
				}
			}
			if(!isExist){
				$('#'+gridDetailId).datagrid('appendRow',row);
				if(gridDetailId.substring(0,3) == 'ddv'){
					var arrValues = EasyUiCommon._mapNewDataSubGrid1.get(index);
					if(arrValues == undefined || arrValues == null || arrValues.length == 0){
						arrValues = new Array();
					}
					arrValues.push(row);
					EasyUiCommon._mapNewDataSubGrid1.put(index,arrValues);
				}else{
					var arrValues = EasyUiCommon._mapNewDataSubGrid2.get(index);
					if(arrValues == undefined || arrValues == null || arrValues.length == 0){
						arrValues = new Array();
					}
					arrValues.push(row);
					EasyUiCommon._mapNewDataSubGrid2.put(index,arrValues);
				}
			}
		}
		$('#'+gridDetailId).datagrid('appendRow',{});
		setTimeout(function(){
			$('#'+gridDetailId).datagrid('resize');
			$('#grid').datagrid('resize');
		},500);
	},
	dataGrid01020405 : function(gridId,url,field1,field2,field3,field4,title1,title2,title3,title4,urlComboGrid,type,permission){
		var _field1 = 'productCode';
		var _field2 = 'productName';
		var _title1 = 'Mã SP';
		var _title2 = 'Tên SP';
		if(field1 != null && field1 != undefined){
			_field1 = field1;
		}
		if(field2 != null && field2 != undefined){
			_field2 = field2;
		}
		if(title1 != null && title1 != undefined){
			_title1 = title1;
		}
		if(title2 != null && title2 != undefined){
			_title2 = title2;
		}
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>"; 
		}
		if(field3 != null && field3 != undefined && field4 != null && field4 != undefined ){
			$('#'+gridId).datagrid({
				url : url,
				pageList  : [10,20,30],
				width:$('.GridSection').width() -20,
				height:'auto',
				scrollbarSize : 18,
				rownumbers:true,
				columns:[[  
			          {field: _field1,title: _title1, width:200,align:'left',sortable : false,resizable : false,
			        	  editor:{
				        	  type:'text'
			          	  },
			          	formatter:function(value){
				    		return Utils.XSSEncode(value);	
				    	}
			          },
			          {field: _field2,title: _title2, width:300,align:'left',sortable : false,resizable : false,
			        	  editor:{
				        	  type:'text'
			          	  },
			          	  styler: function(value,row,index){
 							  return 'font-size:14;font-weight:bold';
			          	  },
			          	  formatter:function(value){
				    		return Utils.XSSEncode(value);	
			          	  }
			          },
			          {field: field3,title: title3, width:150,align:'right',sortable : false,resizable : false,
			        	  editor:{ 
								type:'numberbox',
					    		options:{
					    			groupSeparator: ',',
					    			max:'5'
					    		}
				    		}, 
				    		formatter:function(value){
					    		return formatCurrency(value);	
					    	}
			          },
			          {field: field4,title: title4, width:150,align:'right',sortable : false,resizable : false,
			        	  editor:{ 
								type:'numberbox',
					    		options:{
					    			groupSeparator: ',',
					    			max:'5'
					    		}
				    		}, 
				    		formatter:function(value){
					    		return formatCurrency(value);	
					    	}
			          },
			          {field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
			        	  formatter: function(value,row,index){
			        		  if(permission == 'true'){
			        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
			        		  }else{
			        			  return '';
			        		  }
			        	  }
			          },
			          ]],
			      onSelect:function(index,row){
			    	  if(permission == 'true'){
		    			  if (EasyUiCommon.endEditing(gridId)){
		    				  $('#'+gridId).datagrid('beginEdit', index);  
		    				  EasyUiCommon.editIndex = index;
		    				  $('#'+gridId).datagrid('resize');
		    			  }
			        	  var ed = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field1});
			        	  var ed2 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field2});
			        	  var ed3 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field3});
			        	  var ed4 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field4});
			        	  if(ed != null && ed != undefined){
		        			  $(ed2.target).attr('disabled','disabled');
		        			  var filter = {};
		        			  filter.desObj = $(ed.target);
		        			  filter.type = EasyUiCommon.AUTOCOMPLETE_MUL;
		        			  filter.pType = EasyUiCommon.DETAIL_PRODUCT;
		        			  EasyUiCommon.loadAutoCompleteForProduct(filter);	
			        	  }
			        	  if(ed3 != null && ed3 != undefined){
			        		  $(ed3.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER && $(ed.target).val() != ""){	
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  if(ed4 != null && ed4 != undefined){
			        		  $(ed4.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER && $(ed.target).val() != ""){		
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
				    	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
		        	  }
			      },
		          method : 'GET',
		          onLoadSuccess:function(){
		        	  $('.datagrid-header-rownumber').html(jsp_common_numerical_order);
		        	  if(permission == 'true'){
		        		  var size = $('#'+gridId).datagrid('getRows').length; 
		        		  EasyUiCommon._totalRowGrid = size;
		        		  EasyUiCommon.appendRowOnGrid(gridId);
		        	  }
		        	  $('#'+gridId).datagrid('resize');
		          }
			});
		}
	},
	dataGrid0306 : function(gridId,url,field1,field2,field3,field4,title1,title2,title3,title4,urlComboGrid,type,fieldDetail,titleDetail,permission){
		var _field1 = 'productCode';
		var _field2 = 'productName';
		var _title1 = 'Mã SP';
		var _title2 = 'Tên SP';
		if(field1 != null && field1 != undefined){
			_field1 = field1;
		}
		if(field2 != null && field2 != undefined){
			_field2 = field2;
		}
		if(title1 != null && title1 != undefined){
			_title1 = title1;
		}
		if(title2 != null && title2 != undefined){
			_title2 = title2;
		}
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field3 != null && field3 != undefined && field4 != null && field4 != undefined ){
			$('#'+gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				scrollbarSize : 0,
				rownumbers:true,
				columns:[[  
				{field: _field1,title: _title1, width:200,align:'left',sortable : false,resizable : false,
					  editor:{
						  type:'text'
					  },
					  formatter:function(value){
				    		return Utils.XSSEncode(value);	
			          }
				},
				{field: _field2,title: _title2, width:200,align:'left',sortable : false,resizable : false,
					  editor:{
			        	  type:'text',
		          	  },
		          	  styler: function(value,row,index){
						  return 'font-size:14;font-weight:bold';
		          	  },
		          	  formatter:function(value){
		          		  	return Utils.XSSEncode(value);	
		          	  }
				},
				{field: field3,title: title3, width:100,align:'right',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: field4,title: title4, width:300,align:'left',sortable : false,resizable : false,
					formatter: function(value,row,index){
						return EasyUiCommon.stylerOnRow(index,value,type);
					}
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }
				}
	            ]],
	            onSelect:function(index,row){
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
	        		  if (EasyUiCommon.endEditing(gridId)){  
	        			  $('#'+gridId).datagrid('beginEdit', index);  
	        			  EasyUiCommon.editIndex = index;
	        			  $('#'+gridId).datagrid('resize');
	        		  } 
	        		  EasyUiCommon.formatLabelAfterChange(type);
		        	  var ed = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field1});
		        	  var ed2 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field2});
		        	  if(ed != null && ed != undefined){
		        		  $(ed2.target).attr('disabled','disabled');
	        			  var filter = {};
	        			  filter.desObj = $(ed.target);
	        			  filter.type = EasyUiCommon.AUTOCOMPLETE_MUL;
	        			  filter.pType = EasyUiCommon.DETAIL_PRODUCT;
	        			  EasyUiCommon.loadAutoCompleteForProduct(filter);
		        	  }
		        	  var ed3 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field3});
		        	  var ed4 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field4});
		        	  if(ed3 != null && ed3 != undefined){
		        		  $(ed3.target).bind('keyup',function(event){
		        			  if(event.keyCode == keyCodes.ENTER){		   				
		        				  EasyUiCommon.appendRowOnGrid(gridId);
		        			  }
		        		  });
		        	  }
		        	  if(ed4 != null && ed4 != undefined){
		        		  $(ed4.target).bind('focus',function(event){
		        			  EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',false);
		        		  });
		        	  }
		        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
		              $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
		              $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
		              $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            },
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		var size = $('#'+gridId).datagrid('getRows').length; 
	            		EasyUiCommon._totalRowGrid = size;
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
	            	$('#'+gridId).datagrid('resize');
	            }
			});
		}
	},
	dataGrid07081011 : function(gridId,url,field2,field3,title2,title3,urlComboGrid,type,permission){
		var _field1 = 'productCodeStr';
		var _title1 = 'Sản phẩm mua';
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field2 != null && field2 != undefined && field3 != null && field3 != undefined ){
			$('#'+gridId).datagrid({
				url : url,
				total:100,
				width: $('.GridSection').width() -20,
				scrollbarSize : 0,
				rownumbers:true,
				columns:[[  
				{field: _field1,title: _title1, width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type);
					  }
				},
				{field: field2,title: title2, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: field3,title: title3, width:100,align:'right',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }
				},
	            ]],
	            onSelect:function(index,row){
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
		        		  if (EasyUiCommon.endEditing(gridId)){  
		        			  $('#'+gridId).datagrid('beginEdit', index);  
		        			  EasyUiCommon.editIndex = index;  
		        			  $('#'+gridId).datagrid('resize');
		        		  } 
		        		  EasyUiCommon.formatLabelAfterChange(type);
			        	  var ed = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field1});
			        	  if(ed != null && ed != undefined){
			        		  $(ed.target).bind('focus',function(event){
			        			  EasyUiCommon.showGridDialog(index,'required','BB mua','checkbox',false);
			        		  });
			        	  }
			        	  var ed2 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field2});
			        	  var ed3 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field3});
			        	  if(ed2 != null && ed2 != undefined){
			        		  $(ed2.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  if(ed3 != null && ed3 != undefined){
			        		  $(ed3.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
			        	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
			        	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
			        	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            },
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		var size = $('#'+gridId).datagrid('getRows').length; 
	            		EasyUiCommon._totalRowGrid = size;
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
	            	$('#'+gridId).datagrid('resize');
	            }
			});
		}
	},
	dataGrid0912 : function(gridId,url,field,title,urlComboGrid,type,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field != null && field != undefined){
			$('#'+gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				fitColumns:true,
				rownumbers:true,
				scrollbarSize : 0,
				columns:[[  
				{field: 'productCodeStr',title: 'Sản phẩm mua', width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type);
					  }
				},
				{field: field,title: title, width:100,align:'right',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: 'freeProductCodeStr',title: 'Sản phẩm khuyến mãi', width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type,true);
					  }
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
		        		  if(permission == 'true'){
		        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('"+index+"',true)\"><img title ='Xóa' src='/resources/images/icon-delete.png'/></a>";
		        		  }else{
		        			  return '';
		        		  }
		        	  }
				},
	            ]],
	            onSelect : function (index,row){
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
	        		    if (EasyUiCommon.endEditing(gridId)){  
	            			$('#'+gridId).datagrid('beginEdit', index);  
	            			EasyUiCommon.editIndex = index; 
	            			$('#'+gridId).datagrid('resize');
	            		}  
	        		    EasyUiCommon.formatLabelAfterChange(type);
//	            		var ed = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: 'productCodeStr'});
//	            		if(ed != null && ed != undefined){
//		        		    $(ed.target).bind('focus',function(event){
//		        		    	EasyUiCommon.showGridDialog(EasyUiCommon.editIndex,'required','BB mua','checkbox',false);
//			        		  });
//	            		}
	            		var ed2 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field});
	            		if(ed2 != null && ed2 != undefined){
	            			$(ed2.target).bind('keyup',function(event){
		        			    if(event.keyCode == keyCodes.ENTER){		   				
		        			    	EasyUiCommon.appendRowOnGrid(gridId);
		        			    }
		        		    });
	            		}
//	            		var ed3 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: 'freeProductCodeStr'});
//	            		if(ed3 != null && ed3 != undefined){
//	            			$(ed3.target).bind('focus',function(event){
//	            				EasyUiCommon.showGridDialog(EasyUiCommon.editIndex,'saleQty','Số lượng','numberbox',true);
//	            			});
//	            		}
	            		$('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
				    	$('.datagrid-view td[field="amount"] input').attr('maxlength','17');
				    	$('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
				    	$('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            } ,
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		var size = $('#'+gridId).datagrid('getRows').length; 
	            		EasyUiCommon._totalRowGrid = size;
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
            		$('#'+gridId).datagrid('resize');
            		$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
            		$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
	            },
	            onAfterEdit: function(){
	            	$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
            		$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
	            },
	            onCancelEdit: function(){
	            	$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
            		$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
	            },
	            onBeforeEdit: function(){
	            	$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
            		$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
	            }
			});
		}
	},
	dataGrid1518 : function(gridId,url,urlComboGrid,type,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		$('#'+gridId).datagrid({
			url : url,
			total:100,
			width: $('.GridSection').width() -20,
			scrollbarSize : 0,
			rownumbers:true,
			columns:[[  
			{field: 'productCodeStr',title: 'Sản phẩm mua', width:200,align:'left',sortable : false,resizable : false,
				  formatter: function(value,row,index){
					  return EasyUiCommon.stylerOnRow(index,value,type);
				  }
			},
			{field: 'freeProductCodeStr',title: 'Sản phẩm khuyến mãi', width:200,align:'left',sortable : false,resizable : false,
				  formatter: function(value,row,index){
					  return EasyUiCommon.stylerOnRow(index,value,type,true);
				  }
			},
			{field: 'hiddenField', hidden:true,
				editor:{
					type:'text'
				}
			},
			{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }
			},
            ]],
            onSelect : function (index,row){ 
            	$('#divDialogSearch').hide();
            	if(permission == 'true'){
        		    if (EasyUiCommon.endEditing(gridId)){  
            			$('#'+gridId).datagrid('beginEdit', index);  
            			EasyUiCommon.editIndex = index; 
            			$('#'+gridId).datagrid('resize');
            		}  
        		    EasyUiCommon.formatLabelAfterChange(type);
            		var ed = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: 'productCodeStr'});
            		if(ed != null && ed != undefined){
            			if(type != null && type == 'ZV15'){
            				$(ed.target).bind('focus',function(event){
            					EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',false);
	            			});
            			}else{
            				$(ed.target).bind('focus',function(event){
            					EasyUiCommon.showGridDialog(index,'amount','TT mua','numberbox',false);
            				});
            			}
            		}
            		var ed2 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: 'freeProductCodeStr'});
            		if(ed2 != null && ed2 != undefined){
            			$(ed2.target).bind('focus',function(event){
            				EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',true);
            			});
            		}
            		$('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
			    	$('.datagrid-view td[field="amount"] input').attr('maxlength','17');
			    	$('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
			    	$('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
            	}
            } ,
			method : 'GET',
            onLoadSuccess:function(){
            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
            	if(permission == 'true'){
            		var size = $('#'+gridId).datagrid('getRows').length; 
            		EasyUiCommon._totalRowGrid = size;
            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
            	} else {
            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
            	}
        		$('#'+gridId).datagrid('resize');
            }
		});
	},
	dataGrid13141617 : function(gridId,url,field2,title2,urlComboGrid,type,fieldDetail,titleDetail,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		var _field1 = 'productCodeStr';
		var _title1 = 'Sản phẩm mua';
		if(field2 != null && field2 != undefined){
			$('#'+gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				scrollbarSize : 18,
				rownumbers:true,
				columns:[[  
				{field: _field1,title: _title1, width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type);
					  }
				},
				{field: field2,title: title2, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }
				},
	            ]],
	            onSelect:function(index,row){
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
		        		  if (EasyUiCommon.endEditing(gridId)){  
		        			  $('#'+gridId).datagrid('beginEdit', index);  
		        			  EasyUiCommon.editIndex = index; 
		        			  $('#'+gridId).datagrid('resize');
		        		  } 
		        		  EasyUiCommon.formatLabelAfterChange(type);
			        	  var ed = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field1});
			        	  if(ed != null && ed != undefined){
//			        		  $(ed.target).focusToEnd();
			        		  EasyUiCommon.eventWhenPressTAB('numberItem');
			        		  if(type == 'ZV13' || type == 'ZV14'){
			        			  $(ed.target).bind('focus',function(event){
			        				  EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',false);
			        			  });
			        		  }else if(type == 'ZV16' || type == 'ZV17'){
			        			  $(ed.target).bind('focus',function(event){
			        				  EasyUiCommon.showGridDialog(index,'amount','TT mua','numberbox',false);
			        			  });
			        		  }
			        	  }
			        	  var ed2 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field2});
			        	  if(ed2 != null && ed2 != undefined){
			        		  $(ed2.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
				    	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            },
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		var size = $('#'+gridId).datagrid('getRows').length; 
	            		EasyUiCommon._totalRowGrid = size;
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
	        		$('#'+gridId).datagrid('resize');
	            }
			});
		}
	},
	dataGrid1920 : function(gridId,url,field1,field2,title1,title2,urlComboGrid,type,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field1 != null && field1 != undefined && field2 != null && field2 != undefined){
			$('#'+gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				//height:'auto',
				rownumbers:true,
				scrollbarSize : 18,
				columns:[[  
				{field: field1,title: title1, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ',',
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: field2,title: title2, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }},
	            ]],
	            onSelect : function (index,row){
	            	if(permission == 'true'){
		        		  if (EasyUiCommon.endEditing(gridId)){  
		        			  $('#'+gridId).datagrid('beginEdit', index);  
		        			  EasyUiCommon.editIndex = index;  
		        			  $('#'+gridId).datagrid('resize');
		        		  } 
			        	  var ed = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field1});
			        	  if(ed != null && ed != undefined){
			        		  $(ed.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  var ed2 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field2});
			        	  if(ed2 != null && ed2 != undefined){
			        		  $(ed2.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
				    	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            },
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		$('#'+gridId).datagrid('appendRow',{});
	            	}
	            	$('#'+gridId).datagrid('resize');
	            },
			});
		}
	},
	dataGrid21 : function(gridId,url,field1,field2,title1,title2,urlComboGrid,type,fieldDetail,titleDetail,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field1 != null && field1 != undefined && field2 != null && field2 != undefined){
			$('#'+gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				scrollbarSize : 18,
				rownumbers:true,
				columns:[[  
				{field: field1,title: title1, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: field2,title: title2, width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type);
					  }
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }},
	            ]],
	            onSelect : function (index,row){ 
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
	        		  if (EasyUiCommon.endEditing(gridId)){  
	        			  $('#'+gridId).datagrid('beginEdit', index);  
	        			  EasyUiCommon.editIndex = index; 
	        			  $('#'+gridId).datagrid('resize');
	        		  }  
	        		  EasyUiCommon.formatLabelAfterChange(type);
		        	  var ed = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field1});
		        	  if(ed != null && ed != undefined){
		        		  $(ed.target).focusToEnd();
		        		  $(ed.target).bind('keyup',function(event){
		        			  if(event.keyCode == keyCodes.ENTER){		   				
		        				  EasyUiCommon.appendRowOnGrid(gridId);
		        			  }
		        		  });
		        	  }
		        	  var ed2 = $('#'+gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field2});
		        	  if(ed2 != null && ed2 != undefined){
		        		  $(ed2.target).bind('focus',function(event){
	        				  EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',false);
	        			  });
		        	  }
		        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
			    	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
			    	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
			    	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            } ,
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
	        		$('#'+gridId).datagrid('resize');
	            }
			});
		}
	},
	showGridAutocomplete : function(gridId,indexGrid,inputId,_comboGridField3,_comboGridTitle3,type,apParamCode,inputValue,isTwoDetail) {
		$('#indexDialog').val(indexGrid);
		EasyUiCommon.inputId = inputId;
		EasyUiCommon.field = _comboGridField3;
		EasyUiCommon.title = _comboGridTitle3;
		EasyUiCommon.type = type;
		EasyUiCommon.apParamCode = apParamCode;
		EasyUiCommon.isMulti = isTwoDetail;
		if(inputValue == undefined){
			inputValue = '';
		}
		$('#errMsgUIDialog').html('').hide();
		$('#searchGrid').datagrid({
			url : '/catalog/product/search?status=1&isExceptZCat=true&isPriceValid=true&productCode='+inputValue+'&lstProductCode='+EasyUiCommon._lstProductSelected,
			autoRowHeight : false,
			view: bufferview,
			pageSize:10,  
			rownumbers : true, 
			height: 150,
			fitColumns:true,
			singleSelect:true,
			queryParams:{
		 	},
		 	scrollbarSize : 18,
			width : ($('#gridDialog').width()),
		    columns:[[  
		        {field:'productCode',title:'Mã SP',align:'left', width:150, sortable : false,resizable : false,fixed:true, formatter:function(value){
		        	return Utils.XSSEncode(value);
		        }},  
		        {field:'productName',title:'Tên SP',align:'left', width:250, sortable : false,resizable : false,fixed:true, formatter:function(value){
		        	return Utils.XSSEncode(value);
		        }},
		        {field: EasyUiCommon.field,title: EasyUiCommon.title, width:100,align:'left',sortable : false,resizable : false,fixed:true,
		        	editor:{
		        		type:EasyUiCommon.type
		        	},
	            	formatter:function(value,row){
	            		if(type == 'checkbox'){
	            			return '<input class="checkItem" type="checkbox" id="'+EasyUiCommon.field+'_'+row.id+'" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'checkItem\',\'searchGrid\')"/>';
	            		}else{
	            			return '<input class="numberItem" id="'+EasyUiCommon.field+'_'+row.id+'" onfocus="return Utils.bindFormatOnTextfield(\''+_comboGridField3+'_'+row.id+'\',Utils._TF_NUMBER,null);" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'numberItem\',\'searchGrid\')"/>';
	            		}
	            	}},
		        {field :'id',hidden : true},
		    ]],
		    onLoadSuccess :function(){
		    	$('#searchGrid').datagrid('selectRow', 0);
		    	$('.datagrid-pager').html('').hide();
		    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
		    	var originalSize = $('.datagrid-header-row td[field=productCode] div').width();
		    	if(originalSize != null && originalSize != undefined){
		    		EasyUiCommon._lstSize.push(originalSize);
		    	}
	    		$(window).bind('keyup',function(event){
	    			if(event.keyCode == keyCodes.ENTER){
	    				var fVal = '';
	    				var data = $('#searchGrid').datagrid('getSelected');
	    				var arrVal = $(EasyUiCommon.inputId).val().split(",");
	    				if(EasyUiCommon.type == 'checkbox'){
							if(arrVal!= null && arrVal.length  > 0){
								for(var i=0;i<arrVal.length;i++){
									if(arrVal[i] != undefined){
										if(arrVal[i].trim() != '' && arrVal[i].trim().length >= 6){
											fVal += arrVal[i].trim() + ', ';
										}else{
											if(fVal.indexOf(data.productCode) == -1){
												arrVal[i] = '';
												arrVal[i] += data.productCode;
												var required = 0;
												if($('#'+EasyUiCommon.field+'_'+data.id).is(":checked")){
													arrVal[i] += "*";
													required = 1;
												}else{
													required = 0;	
												}
												fVal += arrVal[i].trim() + ', ';
												var row = {};
												row = {productCode:data.productCode,
														productName:data.productName,
														required:required
												}; 
												EasyUiCommon._lstProductSelected.push(data.productCode);
												if(EasyUiCommon.isMulti){
													var arrValues = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
													if(arrValues == undefined || arrValues == null || arrValues.length == 0){
														arrValues = new Array();
													}
													arrValues.push(row);
													EasyUiCommon._mapNewDataSubGrid2.put($('#indexDialog').val(),arrValues);
												}else{
													var arrValues = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
													if(arrValues == undefined || arrValues == null || arrValues.length == 0){
														arrValues = new Array();
													}
													arrValues.push(row);
													EasyUiCommon._mapNewDataSubGrid1.put($('#indexDialog').val(),arrValues);
												}
											}
										}
									}
								}
							}
	    				}else{
							if(arrVal!= null && arrVal.length  > 0){
								for(var i=0;i<arrVal.length;i++){
									if(arrVal[i] != undefined){
										if(arrVal[i].trim().indexOf("(") > -1 && arrVal[i].trim().indexOf(")") > -1){
											fVal += arrVal[i].trim() + ', ';
										}else{
											if(fVal.indexOf(data.productCode) == -1){
												arrVal[i] = '';
												arrVal[i] += data.productCode;
												arrVal[i] += "("+$('#'+EasyUiCommon.field+'_'+data.id).val()+")";
												fVal += arrVal[i].trim() + ', ';
												var row = {};
												if(EasyUiCommon.field == 'saleQty'){
													row = {productCode:data.productCode,
															productName:data.productName,
															quantity:$('#'+EasyUiCommon.field+'_'+data.id).val()
													}; 
												}else{
													row = {productCode:data.productCode,
															productName:data.productName,
															amount:$('#'+EasyUiCommon.field+'_'+data.id).val()
													};
												}
												EasyUiCommon._lstProductSelected.push(data.productCode);
												if(EasyUiCommon.isMulti){
													var arrValues = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
													if(arrValues == undefined || arrValues == null || arrValues.length == 0){
														arrValues = new Array();
													}
													arrValues.push(row);
													EasyUiCommon._mapNewDataSubGrid2.put($('#indexDialog').val(),arrValues);
												}else{
													var arrValues = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
													if(arrValues == undefined || arrValues == null || arrValues.length == 0){
														arrValues = new Array();
													}
													arrValues.push(row);
													EasyUiCommon._mapNewDataSubGrid1.put($('#indexDialog').val(),arrValues);
												}
											}
										}
									}
								}
							}
	    				}
	    				$(EasyUiCommon.inputId).val(fVal);
	    				$(EasyUiCommon.inputId).focusToEnd();
	    				$('#divDialogSearch').hide();
	    				return false;
	    			}else if(event.keyCode == keyCodes.ESCAPE){
	    				$(EasyUiCommon.inputId).focusToEnd();
	    				$('#divDialogSearch').hide();
	    				return false;
	    			}
	    		});
		    }
		});  
		return false;
	},
	showGridDialog:function(gridIndex,_comboGridField3,_comboGridTitle3,type,isTwoDetail,isPromotionProduct){
		EasyUiCommon.isExceptZ = !isPromotionProduct;
		$('#indexDialog').val(gridIndex);
		$('#errMsgUIDialogUpdate').html('').hide();
		EasyUiCommon.isMulti = isTwoDetail;
		var title = '';
		
		var permissionUser = $('#permissionValue').val().trim();  //thachnn them 2 dong nay de fix loi
		var promotionStatus = $('#promotionStatus').val().trim(); //
		
		if(_comboGridField3 == 'saleQty'){
			EasyUiCommon.typeChange = EasyUiCommon.QUANTITY_PRODUCT;
		}else if(_comboGridField3 == 'amount'){
			EasyUiCommon.typeChange = EasyUiCommon.AMOUNT_PRODUCT;
		}else if(_comboGridField3 == 'required'){
			EasyUiCommon.typeChange = EasyUiCommon.REQUIRE_PRODUCT;
		}
		
			if(promotionStatus != 2){
				$('#productDialogUpdate .BtnCenterSection').html('<button id="btnChange" class="BtnGeneralStyle" onclick="$(\'#productDialogUpdate\').window(\'close\');">Đóng</button>');
			}
		
		if(permissionUser == 'true'){
			if(promotionStatus == 2){
				title = '<a href="javascript:void(0);" onclick="EasyUiCommon.showTreeGridDialog('+isPromotionProduct+');"><img title="Thêm mới" src="/resources/images/icon-add.png"></a>';
			}else{
				title = '';
			}
		}else{
			title = '';
		}
		$('#productDialogUpdate').dialog({  
	        title: 'Thông tin SPKM',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	EasyUiCommon._lstProductOnChange = [];
	        	EasyUiCommon.hasChange = false;
	        	$('.k-list-container').each(function(){
            	    $(this).removeAttr('id');
            	});
	        	$('.easyui-dialog #searchGridUpdate').datagrid({
					url : '/catalog/product/search?status=1&isExceptZCat='+EasyUiCommon.isExceptZ+'&isPriceValid=true&productCode=01AB03'+'&lstProductCode='+EasyUiCommon._lstProductSelected,
					autoRowHeight : false,
					pageSize:10,  
					rownumbers : true, 
					height: 160,
					fitColumns:true,
					queryParams:{
				 	},
					width : ($('.easyui-dialog #gridDialogUpdate').width()),
				    columns:[[  
				        {field:'productCode',title:'Mã SP',align:'left', width:100, sortable : false,resizable : false,fixed:true,
				        	editor:{
				        		type:'text'
				        	},
				        	formatter:function(value,row,index){
				        		if(value != null && value != undefined && value.length > 0){
				        			return '<input id="productCode_'+index+'" value="'+value+'" disabled="disabled" style="width:75px"/>';
				        		}else{
				        			return '<input id="productCode_'+index+'" style="width:75px" />';
				        		}
				        	}
				        },  
				        {field:'productName',title:'Tên SP',align:'left', width:250, sortable : false,resizable : false,fixed:true, formatter: function(value){
				        	return Utils.XSSEncode(value);
				        }},
				        {field: _comboGridField3,title: _comboGridTitle3, width:80,align:'left',sortable : false,resizable : false,fixed:true,
				        	editor:{
				        		type:type
				        	},
			            	formatter:function(value,row,index){
			            		if(type == 'checkbox'){
			            			if(row.required == 1){
			            				return '<input class="checkDialog" type="checkbox" checked="checked"/>';
			            			}else{
			            				return '<input class="checkDialog" type="checkbox" />';
			            			}
			            		}else{
			            			if(_comboGridField3 == 'amount'){
			            				return '<input id="numberItem_'+index+'" value="'+row.amount+'" style="width:80px;" class="numberDialog" onkeypress="NextAndPrevTextField(event,this,\'numberDialog\')" onfocus="return Utils.bindFormatOnTextfield(\'numberItem_'+index+'\','+Utils._TF_NUMBER+');" maxlength="17"/>';
			            			}else{
			            				return '<input id="numberItem_'+index+'" value="'+row.quantity+'" style="width:80px;" class="numberDialog" onkeypress="NextAndPrevTextField(event,this,\'numberDialog\')" onfocus="return Utils.bindFormatOnTextfield(\'numberItem_'+index+'\','+Utils._TF_NUMBER+');" maxlength="9"/>';
			            			}
			            		}
			            	}
				        },
				        {field: 'remove',title:title, width:50,align:'center',sortable : false,resizable : false,
							formatter: function(value,row,index){
								if(permissionUser == 'true'){
									if(promotionStatus == 2){
										return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnDialog('.easyui-dialog #searchGridUpdate','" + index + "','"+isTwoDetail+"')\"><img title ='Xóa' src='/resources/images/icon-delete.png'/></a>";
									}else{
										return '';
									}
								}else{
									return '';
								}
							}
						},
				    ]],
				    onLoadSuccess :function(){
				    	var newData = {};
				    	if(isTwoDetail){
				    		newData = EasyUiCommon._mapNewDataSubGrid2.get(gridIndex);
				    	}else{
				    		newData = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
				    	}
			    		if(newData!= undefined && newData!= null && newData.length > 0){
			    			for(var i=0;i<newData.length;i++){
			    				$('.easyui-dialog #searchGridUpdate').datagrid('insertRow',{index: i, row:newData[i]});
			    			}
			    		}
				    	$('.datagrid-pager').html('').hide();
				    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
				    	var originalSize = $('.easyui-dialog .datagrid-header-row td[field=productCode] div').width();
				    	if(originalSize != null && originalSize != undefined){
				    		EasyUiCommon._lstSize.push(originalSize);
				    	}
				    	if(newData!= undefined && newData!= null && newData.length > 0){
				    		EasyUiCommon.insertRowOnDialog(newData.length);
				    	}else{
				    		EasyUiCommon.insertRowOnDialog(0);
				    	}
				    	if(_comboGridField3 == 'required'){
				    		$('.checkDialog').each(function(){
				    			$(this).focus();
				    			return false;
				    		});
				    	}else{
				    		$('.numberDialog').each(function(){
				    			$(this).focusToEnd();
				    			return false;
				    		});
				    	}
				    	EasyUiCommon.updateRownumWidthForJqGridEX1('.easyui-dialog #gridDialog','productCode');
				    }
				});  
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	if(!EasyUiCommon.hasChange){
	        		for(var i = 0; i < EasyUiCommon._lstProductOnChange.length ; i++){
	        			if(EasyUiCommon.isMulti){
	        				for(var j = 0;j < EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).length ; j++){
	        					if(EasyUiCommon._lstProductOnChange[i] == EasyUiCommon._mapNewDataSubGrid2.get(gridIndex)[j].productCode){
	        						EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).splice(j);
	        					}
	        				}
	        			}else{
	        				for(var j = 0;j < EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).length ; j++){
	        					if(EasyUiCommon._lstProductOnChange[i] == EasyUiCommon._mapNewDataSubGrid1.get(gridIndex)[j].productCode){
	        						EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).splice(j);
	        					}
	        				}
	        			}
	        		}
	        	}
	        	EasyUiCommon._mapCheckProduct = new Map();
	        	$('#gridDialogUpdate').html('<table id="searchGridUpdate" class="easyui-datagrid"></table><div class="Clear"></div>');
	        }
	    });
		return false;
	},
	autocompleteOnDialog:function(inputId,index){
		var gridIndex = $('#indexDialog').val();
		var template = '<h3 style="float:left">${productCode}</h3></br><p>${productName}</p>';
		inputId.kendoAutoComplete({	 
			highlightFirst: true,
            filter: "startswith",
            placeholder: "Nhập SP...",
            dataSource: {
                type: "json",
                serverFiltering: true,
                serverPaging: true,
                pageSize: 20,
                transport: {
                    read: "/catalog/product/autocomplete?isExceptZCat="+EasyUiCommon.isExceptZ
                }
            },
            dataTextField: "productCode",
            template: template,
            change: function(e){      
        		this.element.val(pCode);
        		this.element.parent().parent().parent().parent().children().first().next().children().html(pName);
        		var row = {};
        		if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
        			row = {
        					productCode : pCode,
        					productName : pName,
        					quantity : 0
        			};
        		}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
        			row = {
        					productCode : pCode,
        					productName : pName,
        					amount : 0
        			};
        		}else{
        			row = {
        					productCode : pCode,
        					productName : pName,
        					required : 0
        			};
        		}
        		if(EasyUiCommon.isMulti){
        			var isExist = false;
        			if(EasyUiCommon._mapNewDataSubGrid2.get(gridIndex) != null){
        				for(var i = 0;i < EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).length ; i++){
        					if(pCode == EasyUiCommon._mapNewDataSubGrid2.get(gridIndex)[i].productCode){
        						isExist = true;
        						break;
        					}
        				}
        			}
        			if(!isExist){
        				if(EasyUiCommon._mapNewDataSubGrid2.get(gridIndex) != null){
        					EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).push(row);
        				}else{
        					var arrValue = new Array();
        					arrValue.push(row);
        					EasyUiCommon._mapNewDataSubGrid2.put(gridIndex,arrValue);
        				}				
        			}
        		}else{
        			var isExist = false;
        			if(EasyUiCommon._mapNewDataSubGrid1.get(gridIndex) != null){
        				for(var i = 0;i < EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).length ; i++){
        					if(pCode == EasyUiCommon._mapNewDataSubGrid1.get(gridIndex)[i].productCode){
        						isExist = true;
        						break;
        					}
        				}
        			}
        			if(!isExist){
        				if(EasyUiCommon._mapNewDataSubGrid1.get(gridIndex) != null){
        					EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).push(row);
        				}else{
        					var arrValue = new Array();
        					arrValue.push(row);
        					EasyUiCommon._mapNewDataSubGrid1.put(gridIndex,arrValue);
        				}
        			}
        		}
        		EasyUiCommon._lstProductOnChange.push(pCode);
            },
            dataBound: function(e) {
            },
            close: function(e){
            	$(window).unbind('keyup');	
            },	  
            open: function(e) {
    			var text = inputId.attr('id')+'-list';
    			$('#'+text).css('width','350px');
    			inputId.parent().css('padding','0px !important');	
            },
            select: function(e){
            	pCode = e.item.children().first().text();
            	pName = e.item.children().last().text();
            	var lstIndex = [];
        		$('.easyui-dialog .k-autocomplete').each(function(){
        			lstIndex.push($(this).children().first().attr('id').split("_")[1]);
            	});
        		var hasNewRow = true;
        		for(var i = 0 ; i < lstIndex.length ; i++){
        			if(parseInt(lstIndex[i]) == index + 1){
        				hasNewRow = false;
        				break;
        			}
        		}
        		if(hasNewRow){
        			EasyUiCommon.insertRowOnDialog(index+1);
        		}
            }                
        });
	},
	changeQuantity:function(){
		EasyUiCommon.hasChange = true;
		$('#errMsgUIDialogUpdate').html('').hide();
		var gridIndex = $('#indexDialog').val();
		var ed = null;
		var type = $('#apParamCode').val().trim();
		if(type == 'ZV03' || type == 'ZV06'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'productCode'});
		}else if(type == 'ZV07' || type == 'ZV08'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'quantity'});
		}else if(type == 'ZV10' || type == 'ZV11'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'amount'});
		}else if(type == 'ZV09'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'quantity'});
		}else if(type == 'ZV12'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'amount'});
		}else if(type == 'ZV15' || type == 'ZV18'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'hiddenField'});
		}else if(type == 'ZV13' || type == 'ZV16'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'discountPercent'});
		}else if(type == 'ZV14' || type == 'ZV17'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'discountAmount'});
		}else if(type == 'ZV21'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'amount'});
		}
		var lstTmp = null;
		var number = 0;
		var checkType = 0;
		if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
			lstTmp = new Array();
			checkType = 1;
			$('.checkDialog').each(function(){
				if($('#productCode_'+number).val() != ''&& $('#productCode_'+number).val().length > 0 && $(this).parent().parent().parent().children().first().next().children().text() != ''){
					lstTmp.push($('#productCode_'+number).val());
				}
				number++;
			});
		}else{
			checkType = 2;
			lstTmp = new Array();
			$('.numberDialog').each(function(){
				if($('#productCode_'+number).val() != ''&& $('#productCode_'+number).val().length > 0 && $(this).parent().parent().parent().children().first().next().children().text() != ''){
					lstTmp.push($('#productCode_'+number).val());
				}
				number++;
			});
		}
		var xx = 0;
		var flag = false;
		while(xx < lstTmp.length){
			for (var i = 0; i< lstTmp.length; i++){
				if (xx != i){
					if (lstTmp[xx] == lstTmp[i]){
						flag = true;
					}
				}
			}
			xx++;
		}
		if (flag == true){
			if (checkType == 1){
				$('#errMsgUIDialogUpdate').html(' SP là duy nhất trong nhóm SP mua.').show();
				return false;
			}else{
				$('#errMsgUIDialogUpdate').html(' SP là duy nhất trong nhóm SPKM.').show();
				return false;
			}
		}
		if(ed != null && ed != undefined){
			EasyUiCommon.inputId = ed.target;
			var lstProductCode = '';
			var first = true;
			var isErr = false;
			var num = 0;
			if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
				$('.checkDialog').each(function(){
					if($('#productCode_'+num).val() != ''&& $('#productCode_'+num).val().length > 0 && $(this).parent().parent().parent().children().first().next().children().text() != ''){
						var required = 0;
						if(!first){
							lstProductCode += ','; 
						}
						lstProductCode += $('#productCode_'+num).val();
						if($(this).is(":checked")){
							lstProductCode += "*";
							required = 1;
						}else{
							required = 0;
						}
						var row = {};
						row = {
								productCode:$('#productCode_'+num).val(),
								productName:$(this).parent().parent().parent().children().first().next().children().text(),
								required:required
						};
						var arrValues = null;
						if(!first){
							if(EasyUiCommon.isMulti){
								arrValues = EasyUiCommon._mapNewDataSubGrid2.get(gridIndex);
							}else{
								arrValues = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
							}
						}else{
							arrValues = new Array();
						}
						arrValues.push(row);
						if(EasyUiCommon.isMulti){
							EasyUiCommon._mapNewDataSubGrid2.put(gridIndex,arrValues);					
						}else{
							for(var i = 0;i < EasyUiCommon._mapNewDataSubGrid1.keyArray.length;i++){
								if(EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
									EasyUiCommon._mapNewDataSubGrid1.put(i,arrValues);
								}
							}
						}
						first = false;
					}
					num++;
				});
			}else{
				$('.numberDialog').each(function(){
					if($('#productCode_'+num).val() != ''&& $('#productCode_'+num).val().length > 0 && $(this).parent().parent().parent().children().first().next().children().text() != ''){
						if(this.value != '' && this.value > 0){
							if(!first){
								lstProductCode += ','; 
							}
							lstProductCode += $('#productCode_'+num).val();
							lstProductCode += "(" +this.value+ ")";
							var row = {};
							if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
								row = {
										productCode:$('#productCode_'+num).val(),
										productName:$(this).parent().parent().parent().children().first().next().children().text(),
										quantity:this.value
								}; 
							}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
								row = {
										productCode:$('#productCode_'+num).val(),
										productName:$(this).parent().parent().parent().children().first().next().children().text(),
										amount:this.value
								}; 
							}
							var arrValues = null;
							if(!first){
								if(EasyUiCommon.isMulti){
									arrValues = EasyUiCommon._mapNewDataSubGrid2.get(gridIndex);
								}else{
									arrValues = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
								}
							}else{
								arrValues = new Array();
							}
							arrValues.push(row);
							if(EasyUiCommon.isMulti){
								EasyUiCommon._mapNewDataSubGrid2.put(gridIndex,arrValues);					
							}else{
								EasyUiCommon._mapNewDataSubGrid1.put(gridIndex,arrValues);
							}
							first = false;
						}else{
							isErr = true;
							$('#errMsgUIDialogUpdate').html('Vui lòng nhập số lượng KM').show();
							return false;
						}
					}
					num++;
				});
				if(EasyUiCommon.isMulti){
					if(EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).length == 0){
						isErr = true;
						$('#errMsgUIDialogUpdate').html('Vui lòng chọn sản phẩm').show();
						return false;
					}
				}else{
					if(EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).length == 0){
						isErr = true;
						$('#errMsgUIDialogUpdate').html('Vui lòng chọn sản phẩm').show();
						return false;
					}
				}
			}
			if(!isErr){
				if(EasyUiCommon.isMulti){
					lstProductCode = EasyUiCommon.stylerOnRow(gridIndex,lstProductCode,type,true);
					EasyUiCommon._mapProductCodeStr2.put(gridIndex,lstProductCode);
					if(type == 'ZV09' || type == 'ZV12'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().last().prev().children().html(lstProductCode);
					}else{
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().last().prev().prev().children().html(lstProductCode);
					}
				}else{
					lstProductCode = EasyUiCommon.stylerOnRow(gridIndex,lstProductCode,type);
					EasyUiCommon._mapProductCodeStr1.put(gridIndex,lstProductCode);
					if(type == 'ZV03' || type == 'ZV06'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().parent().children().last().prev().children().html(lstProductCode);
					}else if(type == 'ZV07' || type == 'ZV08' || type == 'ZV09' || type == 'ZV10' || type == 'ZV11' || type == 'ZV12'){
						for(var i = 0;i < EasyUiCommon._mapNewDataSubGrid1.keyArray.length;i++){
							if(EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
								$('#grid').datagrid('beginEdit',i);
								if(type == 'ZV07' || type == 'ZV08'){
									ed = $('#grid').datagrid('getEditor', {index:i,field: 'quantity'});
								}else if(type == 'ZV10' || type == 'ZV11'){
									ed = $('#grid').datagrid('getEditor', {index:i,field: 'amount'});
								}else if(type == 'ZV09'){
									ed = $('#grid').datagrid('getEditor', {index:i,field: 'quantity'});
								}else if(type == 'ZV12'){
									ed = $('#grid').datagrid('getEditor', {index:i,field: 'amount'});
								}
								$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().children().html(lstProductCode);
							}
						}
						for(var i = 0; i < EasyUiCommon._mapProductCodeStr1.keyArray.length ; i++){
							EasyUiCommon._mapProductCodeStr1.put(parseInt(EasyUiCommon._mapProductCodeStr1.keyArray[i]),lstProductCode);
						}
					}else if(type == 'ZV15' || type == 'ZV18'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().children().html(lstProductCode);
					}else if(type == 'ZV13' || type == 'ZV16'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().children().html(lstProductCode);
					}else if(type == 'ZV14' || type == 'ZV17'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().children().html(lstProductCode);
					}else if(type == 'ZV21'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().next().children().html(lstProductCode);
					}
				}
				$('#productDialogUpdate').dialog('close');
			}
		}
	},
	autocompleteOnTextbox:function(event,value,gridId,index,inputId,type,typeAutocomplete,isTwoDetail){
		if(isTwoDetail == null || isTwoDetail == undefined){
			isTwoDetail = false;
		}
		var keyChar = String.fromCharCode(event.which);
		var namePattern = /^[A-Za-z0-9]*$/ ;
		if(namePattern.test(keyChar)){
			  $('#divDialogSearch').show();
			  var left = inputId.position().left;
			  var top = inputId.position().top;
			  var height_box = inputId.height();
			  left = left+$('.GridSection').position().left;
			  top = top+ height_box + $('.GridSection').position().top;
			  $('#productEasyUIDialog').css({'left':left,'top':top});
			  $('#typeProductValue').val(0);
			  var inputValue = '';
			  if(value == null || value == undefined){
				  value = '';
			  }
			  inputValue =  value + keyChar;
			  var size = inputValue.split(",").length;
			  if(size > 0){
				  inputValue = inputValue.split(",")[size-1];
			  }
			  if(typeAutocomplete == EasyUiCommon.QUANTITY_PRODUCT){
				  EasyUiCommon.showGridAutocomplete(gridId,index,inputId,'saleQty','SL mua','numberbox',type,inputValue.trim(),isTwoDetail);
			  }else if(typeAutocomplete == EasyUiCommon.AMOUNT_PRODUCT){
				  EasyUiCommon.showGridAutocomplete(gridId,index,inputId,'amount','TT mua','numberbox',type,inputValue.trim(),isTwoDetail);
			  }else if(typeAutocomplete == EasyUiCommon.REQUIRE_PRODUCT){
				  EasyUiCommon.showGridAutocomplete(gridId,index,inputId,'required','BB mua','checkbox',type,inputValue.trim(),isTwoDetail);
			  }
		}
	},
	loadDataOnGridSuccess:function(gridId,apParamCode){
		$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
		if(PromotionCatalog._isExcel == true){
			EasyUiCommon._lstProductSelected = new Array();
		}
		var size = $('#'+gridId).datagrid('getRows').length; 
    	if(size == 0){
    		$('#'+gridId).datagrid('appendRow',{});
    		EasyUiCommon._totalRowGrid++;
    	}else{
//    		$('#'+gridId).datagrid('appendRow',{});//Sontt comment dong nay khi fix bug! Khi list co du lieu thi ko add dong rong vao. Chi add dong
    		//rong vao khi grid ko co du lieu, hoac khi nguoi dung bam nut them moi cho grid.
    		if(apParamCode ==  'ZV07' || apParamCode ==  'ZV08' || apParamCode ==  'ZV10' || apParamCode ==  'ZV11' || apParamCode ==  'ZV09' || apParamCode ==  'ZV12'){
    			size = $('#'+gridId).datagrid('getRows').length;
    		}
    		for(var i = 0;i<size;i++){
    			var codeStr = '';
    			var nameStr = '';
    			var freeCodeStr = '';
    			var freeNameStr = '';
    			if(apParamCode ==  'ZV03' || apParamCode ==  'ZV06'){
    				codeStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].freeProductCodeStr);
    				nameStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].freeProductNameStr);
    				EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    			}else if(apParamCode ==  'ZV07' || apParamCode ==  'ZV08' || apParamCode ==  'ZV10' || apParamCode ==  'ZV11'){
    				codeStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].productCodeStr);
    				nameStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].productNameStr);
    				EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.REQUIRE_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    			}else if(apParamCode ==  'ZV09' || apParamCode ==  'ZV12'){
    				codeStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].productCodeStr);
    				nameStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].productNameStr);
    				freeCodeStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].freeProductCodeStr);
    				freeNameStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].freeProductNameStr);
    				EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.REQUIRE_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				EasyUiCommon.putDataForMap(freeCodeStr, freeNameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid2, i,true);
    			}else if(apParamCode ==  'ZV15' || apParamCode ==  'ZV18'){
    				codeStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].productCodeStr);
    				nameStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].productNameStr);
    				freeCodeStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].freeProductCodeStr);
    				freeNameStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].freeProductNameStr);
    				if(apParamCode ==  'ZV15'){
    					EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				}else{
    					EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.AMOUNT_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				}
    				EasyUiCommon.putDataForMap(freeCodeStr, freeNameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid2, i,true);
    			}else if(apParamCode ==  'ZV13' || apParamCode ==  'ZV14' || apParamCode ==  'ZV16' || apParamCode ==  'ZV17'){
    				codeStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].productCodeStr);
    				nameStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].productNameStr);
    				if(apParamCode ==  'ZV13' || apParamCode ==  'ZV14'){
    					EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				}else{
    					EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.AMOUNT_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				}
    			}else if(apParamCode ==  'ZV21'){
    				codeStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].freeProductCodeStr);
    				nameStr = Utils.XSSEncode($('#'+gridId).datagrid('getRows')[i].freeProductNameStr);
    				EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    			} 
    		}
    	}
	},
	insertRowOnDialog:function(index){
		var row = {};
		if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
			row = {
					productCode : '',
					productName : '',
					quantity : '',
			};
		}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
			row = {
					productCode : '',
					productName : '',
					amount : '',
			};
		}else {
			row = {
					productCode : '',
					productName : '',
					required : 0,
			};
		}
		$('.easyui-dialog #searchGridUpdate').datagrid('insertRow',{index: index, row:row});
		EasyUiCommon.autocompleteOnDialog($('#productCode_'+index),index);
	},
	eventOnTextField:function(e,selector,clazz,gridId){	
		var index = $('input.'+clazz).index(selector);	
		if((e.keyCode == keyCodes.ARROW_DOWN) ){	
			++index;
			var nextSelector = $('input.'+clazz).eq(index);
			if($(nextSelector).hasClass(clazz)){				
				setTimeout(function(){
					$('input.'+clazz).eq(index).focus();
				}, 30);
			}
		}else if(e.keyCode == keyCodes.ARROW_UP ){
			--index;
			var nextSelector = $('input.'+clazz).eq(index);
			if($(nextSelector).hasClass(clazz)){	
				setTimeout(function(){
					$('input.'+clazz).eq(index).focus();
				}, 30);
			}			
		}
		if(gridId != null && gridId != undefined){
			$('#'+gridId).datagrid('selectRow',index);
		}
	},
	eventWhenPressTAB:function(clazz){
		$(window).bind('keyup', function(evt){
    		if(evt.keyCode == keyCodes.TAB){
    			$('.'+clazz).each(function(){
    				$(this).focus();
    				return false;
    			});
    		}
	  });
	},
	putDataForMap:function(codeStr,nameStr,typeAutocomplete,_lstProductData,index,isTwoDetail){
		var type = $('#apParamCode').val().trim();
		var lstProductCode = '';
		if(codeStr != undefined ){
			if(isTwoDetail != undefined && isTwoDetail){
				lstProductCode = EasyUiCommon.stylerOnRow(index,codeStr,type,true);
				EasyUiCommon._mapProductCodeStr2.put(index,lstProductCode);
			}else{
				lstProductCode = EasyUiCommon.stylerOnRow(index,codeStr,type);
				EasyUiCommon._mapProductCodeStr1.put(index,lstProductCode);
				EasyUiCommon.productCode = lstProductCode;
			}
			var arrLst = codeStr.split(",");
			var arrLstName = nameStr.split(",");
			for(var k = 0;k < arrLst.length ;k++){
				var row = {};
				if(typeAutocomplete ==  EasyUiCommon.QUANTITY_PRODUCT){
					var x = arrLst[k].indexOf("(");
					var y = arrLst[k].indexOf(")");
					row = {productCode:arrLst[k].substring(0,x),
							productName:arrLstName[k],
							quantity:arrLst[k].substring(x+1,y)
					};
				}else if(typeAutocomplete ==  EasyUiCommon.AMOUNT_PRODUCT){
					var x = arrLst[k].indexOf("(");
					var y = arrLst[k].indexOf(")");
					row = {productCode:arrLst[k].substring(0,x),
							productName:arrLstName[k],
							amount:arrLst[k].substring(x+1,y)
					};
				}else if(typeAutocomplete ==  EasyUiCommon.REQUIRE_PRODUCT){
					var required = 0;
					var productCode = '';
					if(arrLst[k].indexOf("*") == -1){
						required = 0;
						productCode = arrLst[k]; 
					}else{
						required = 1;
						productCode = arrLst[k].split("*")[0];
					}
					row = {productCode:productCode,
							productName:arrLstName[k],
							required:required
					};
				}
				EasyUiCommon._lstProductSelected.push(row.productCode);
				var arrValues = _lstProductData.get(index);
				if(arrValues == undefined || arrValues == null || arrValues.length == 0){
					arrValues = new Array();
				}
				arrValues.push(row);
				_lstProductData.put(index,arrValues);
			}
		}else{
			if(type == 'ZV07' || type == 'ZV08' || type == 'ZV09' || type == 'ZV10' || type == 'ZV11' || type == 'ZV12'){
				if(isTwoDetail == undefined || !isTwoDetail){
					if(EasyUiCommon._mapProductCodeStr1.keyArray.length > 0){
						lstProductCode = EasyUiCommon.stylerOnRow(index,EasyUiCommon.codeByGroup,type);
						EasyUiCommon._mapProductCodeStr1.put(index,lstProductCode);
					} 
					if(_lstProductData.keyArray.length > 0){
						_lstProductData.put(index,_lstProductData.get(0));
					}
				}else{
					_lstProductData.put(index,new Array());
				}
			}else{
				var arrValues = new Array();
				_lstProductData.put(index,arrValues);
			}
		}
	},
	removeRowOnDialog:function(gridId,index,isTwoDetail){
		var tmp = new Map();
    	$.messager.confirm('Xác nhận', 'Bạn có muốn xóa dòng dữ liệu này?', function(r){
			if (r){
				$('#'+gridId).datagrid('deleteRow', index);
				var size = $('#'+gridId).datagrid('getRows').length; 
            	if(size == 0){
            		$('#'+gridId).datagrid('appendRow',{});
            	} else {
            		if(isTwoDetail == 'true'){
            			tmp = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
            			if(tmp != null){
            				tmp.splice(index,1);
            			}
            			EasyUiCommon.putValueOnRemove(tmp);
            			EasyUiCommon._mapNewDataSubGrid2.put($('#indexDialog').val(),tmp);
            		}else{
            			tmp = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
            			console.log(tmp[index]);
            			if(tmp != null){
            				tmp.splice(index,1);
            			}
            			EasyUiCommon.putValueOnRemove(tmp);
            			EasyUiCommon._mapNewDataSubGrid1.put($('#indexDialog').val(),tmp);
            		}
            	}
            	$('#'+gridId).datagrid('reload');
            	$('.k-list-container').each(function(){
            	    $(this).removeAttr('id');
            	});
			}
    	});
    },
    putValueOnRemove:function(tmp){
    	var i = 0;
		if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
			$('.numberDialog').each(function(){
				if(tmp != null && tmp[i] != undefined){
					tmp[i].quantity = this.value;
				}
				i++;
			});
		}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
			$('.numberDialog').each(function(){
				if(tmp != null && tmp[i] != undefined){
					tmp[i].amount = this.value;
				}
				i++;
			});
		}else if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
			$('.checkDialog').each(function(){
				if(tmp != null && tmp[i] != undefined){
					if($(this).is(":checked")){
						tmp[i].required = 1;
					}else{
						tmp[i].required = 0;
					}
				}
				i++;
			});
		}
    },
    removeRowOnGrid:function(index,isTwoDetail){
    	var type = $('#apParamCode').val().trim();
    	var tmp = new Map();
    	$('#errMsgProduct').html('').hide();
    	if(type == 'ZV01' || type == 'ZV02' ||type == 'ZV04' ||type == 'ZV05' ||type == 'ZV03' ||type == 'ZV06'){
			rows = $('#grid').datagrid('getRows');
			if(rows.length > 0){
				if(rows.length == 1||EasyUiCommon._totalRowGrid ==1){
					return;
				}
				if(rows[index].productCode == undefined || rows[index].productCode == null || rows[index].productCode == ''){
					return;
				}
			}
		}
    	$.messager.confirm('Xác nhận', 'Bạn có muốn xóa dòng dữ liệu này?', function(r){
			if (r){
				$('#grid').datagrid('deleteRow', index);
				EasyUiCommon._totalRowGrid--;
				//var data = $('#grid').datagrid('getRows');
				//$('#grid').datagrid('loadData',data);
				var obj = new Object();
				$('#grid').datagrid('getRows').splice(index, 0, obj);
				EasyUiCommon.indexDeleted.push(index);
				var size = $('#grid').datagrid('getRows').length; 
            	if(size == 0){
            		EasyUiCommon.appendRowOnGrid('grid');
            	} else {
            		if(!(type == 'ZV01' || type == 'ZV02' ||type == 'ZV04' ||type == 'ZV05' ||type == 'ZV19' ||type == 'ZV20')){
            			if(isTwoDetail){
            				tmp = EasyUiCommon._mapNewDataSubGrid2.get(index);
            				if(tmp != null){
//            					tmp.splice(0);
            					EasyUiCommon._mapNewDataSubGrid2.put(index,new Array());
            					//EasyUiCommon._mapNewDataSubGrid2.remove(index);
            				}
            				EasyUiCommon._mapProductCodeStr2.put(index,"");
            				//EasyUiCommon._mapProductCodeStr1.remove(index);
            				//EasyUiCommon.resetMapCodeStrAfterRemoveGrid(EasyUiCommon._mapNewDataSubGrid2
            					//	, EasyUiCommon._mapProductCodeStr1, type, isTwoDetail);
            			}
            			tmp = EasyUiCommon._mapNewDataSubGrid1.get(index);
            			if(tmp != null){ 
//            				tmp.splice(0);
            				EasyUiCommon._mapNewDataSubGrid1.put(index,new Array());
            				//EasyUiCommon._mapNewDataSubGrid1.remove(index);
            			}
            			EasyUiCommon._mapProductCodeStr1.put(index,"");
            			//EasyUiCommon._mapProductCodeStr1.remove(index);
            			//EasyUiCommon.resetMapCodeStrAfterRemoveGrid(EasyUiCommon._mapNewDataSubGrid1,
            				//	EasyUiCommon._mapProductCodeStr1, type, isTwoDetail);
            		}
            	} 
			}
    	});
    },
    /**
     * reset index cua MapSubGridData va MapProductCodeStringData khi xoa tren dataGrid
     * @param mapSubData
     * @param mapCodeData
     * @param type
     * @param isMulti
     * @author vuonghn
     */
    resetMapCodeStrAfterRemoveGrid:function(mapSubData,mapCodeData,type,isMulti){
    	var keySub = mapSubData.keySet();
    	var keyCode = mapCodeData.keySet();
    	var size = keySub.length;
    	mapSubData.keyArray = new Array();
    	mapCodeData.keyArray = new Array();
    	for(var i=0;i<size;i++){
    		mapSubData.keyArray.push(i);
    		mapCodeData.keyArray.push(i);
    		var first = true;
    		var valueSub = mapSubData.valSet()[i];
    		var lstProductCode = '';
    		for(var j=0;j<valueSub.length;j++){
    			if(!first){
        			lstProductCode+=',';
        		}
    			lstProductCode+=valueSub[j].productCode;
    			if(valueSub[j].quantity != undefined && valueSub[j].quantity !=null){
    				lstProductCode += "("+valueSub[j].quantity+")";
    			}else{
    				if(valueSub[j].required != undefined && valueSub[j].required !=null){
    					if(valueSub[j].required == 1){
    						lstProductCode += '*';
    					}
    				}
    			}
    			first = false;
    		}
    		lstProductCode = EasyUiCommon.stylerOnRow(i, lstProductCode, type, isMulti);
    		mapCodeData.put(i,lstProductCode);
    	}
    },
    cssForNumberBox:function(value){
    	var lstStr = '';
	    for(var k = 0;k<value.split(",").length;k++){
		   if(lstStr != ''){
			   lstStr += ", ";
		   }
		   var x = value.split(",")[k].indexOf("(");
		   var y = value.split(",")[k].indexOf(")");
		   if(value.split(",")[k].substring(0,x) != ''){
			   lstStr += value.split(",")[k].substring(0,x) + "(<span style='color:red'>" + EasyUiCommon.formatCurrency(value.split(",")[k].substring(x+1,y)) + "</span>)";
		   }
	    }
	    if(lstStr != '' && lstStr.split(",").length > 3){
			var len = lstStr.split(",")[0].length + lstStr.split(",")[1].length +lstStr.split(",")[2].length;
			lstStr = lstStr.substring(0,len+2) + " , <strong>...</strong>";
		}
	    return lstStr;
    },
    cssForCheckBox:function(value){
    	var lstStr = '';
	    for(var k = 0;k<value.split(",").length;k++){
		   if(lstStr != ''){
			   lstStr += ", ";
		   }
		   if(value.split(",")[k].indexOf("*") > -1){
			   x = value.split(",")[k].indexOf("*");
			   if(value.split(",")[k].substring(0,x) != ''){
				   lstStr += value.split(",")[k].substring(0,x) + "<span style='color:red'>*</span>";
			   }
		   }else{
			   lstStr += value.split(",")[k];
		   }
	    }
	    if(lstStr != '' && lstStr.split(",").length > 3){
			var len = lstStr.split(",")[0].length + lstStr.split(",")[1].length +lstStr.split(",")[2].length;
			lstStr = lstStr.substring(0,len+2) + " , <strong>...</strong>";
		}
	    return lstStr;
    },
    loadProductTreeGrid:function(param,isPromotionProduct){
		$('#loadingDialog').show();
		if(param == null || param == undefined || param == ''){
			param = '&code=&name=&type=&parentId=&typeObject=2&isPromotionProduct='+isPromotionProduct;
		}
		var title = 'Số lượng';
		var typeBox = 'numberbox';
		if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
			title = 'Số lượng';
			typeBox = 'numberbox';
		}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
			title = 'Tổng tiền';
			typeBox = 'numberbox';
		}else if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
			title = 'Bắt buộc mua';
			typeBox = 'checkbox';
		}
		var field = 'quantity';
		$('.easyui-dialog #treeGrid').treegrid({  
			url:  '/rest/catalog/product/tree-grid/0.json?id='+$('#promotionId').val() + param,
			width:($('.easyui-dialog #treeGridContainer').width()-20),  
			height:400,  
			idField: 'id',  
			treeField: 'text',
			method:'GET',
			checkOnSelect:false,
			singleSelect:false,
			rownumbers:true,
			columns:[[  
	          {field:'text',title:'Sản phẩm',resizable:false,width:400},
	          {field: field,title: title, width:180,align:'center',sortable : false,resizable : false,fixed:true,
	        	  editor:{
	        		  type : typeBox
	        	  },
	        	  formatter:function(value,row){
	        		  if(row.attributes.productTreeVO.type == 'PRODUCT'){
		        		  if(typeBox == 'numberbox'){
		        			  return '<input style="width:140px" class="numberSecondDialog" id="quantity_'+row.id+'" onfocus="return Utils.bindFormatOnTextfield(\'quantity_'+row.id+'\',Utils._TF_NUMBER,null);" onkeypress="NextAndPrevTextField(event,this,\'numberSecondDialog\')"/>';
		        		  }else{
		        			  return '<input style="width:140px" type="checkbox" class="checkSecondDialog" id="quantity_'+row.id+'" onkeypress="NextAndPrevTextField(event,this,\'checkSecondDialog\')"/>';
		        		  }
	        		  }
	        	  }
	          },
	          {field: 'ck',checkbox:true,
	          }
	          ]],
            onLoadSuccess:function(row,data){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('#loadingDialog').hide();
            	setTimeout(function(){
            		CommonSearchEasyUI.fitEasyDialog();
        	  	},1000);
            	$('.numberSecondDialog').each(function(){
            		EasyUiCommon.hideCheckBoxOnTree(this);
	        	    $(this).bind('keyup',function(event){
	        	        if(event.keyCode == keyCodes.ENTER){
	        	        	EasyUiCommon.selectOnSecondDialog();
	        	        }
	        	    });
	        	});
            	$('.checkSecondDialog').each(function(){
	        	    EasyUiCommon.hideCheckBoxOnTree(this);
	        	});
            },
            onExpand:function(row){
            	var param = '&code=&name=&typeObject=2&parentId='+row.id+'&type='+row.attributes.productTreeVO.type+'&isPromotionProduct='+isPromotionProduct;
            	setTimeout(function(){
	            	$.getJSON('/rest/catalog/product/tree-grid/0.json?id='+$('#promotionId').val() + param,function(data){
	            		var isExist = false;
	            		for(var i = 0; i < data.rows.length ; i++){
	            			if(EasyUiCommon._lstProductExpand.indexOf(data.rows[i].id) > -1){
	            				isExist = true;
	            				break;
	            			}else{
	            				EasyUiCommon._lstProductExpand.push(data.rows[i].id);
	            			}
	            		}
	            		if(!isExist){
            				$('#treeGrid').treegrid('append',{
            					parent: row.id,  
            					data: data.rows
            				});
	            		}
	        		});
            	},500);
            },
            onCollapse:function(row){
            },
            onCheck:function(row){
//        		var arr = row.attributes.productTreeVO.listChildren;
//        		EasyUiCommon.recursionCheck(arr,true);
            	if(row.attributes.productTreeVO.type == 'PRODUCT'){
            		 var id = row.attributes.productTreeVO.id;
            		 var text = row.attributes.productTreeVO.name;
            		 var code = text.split('-')[0];
            		 var name = text.split('-')[1];
            		 var insRow = {
    					 productCode:code,
    					 productName:name,
    					 quantity:$('#quantity_'+ id).val()
        			 };  
            		 var array = new Array();
            		 array.push(insRow);
            		 EasyUiCommon._mapProductOnSecondDialog.put(id,array);
            	}
		    },
		    onUncheck:function(row){
//		    	var arr = row.attributes.productTreeVO.listChildren;
//		    	EasyUiCommon.recursionUnCheck(arr);
		    	var id = row.attributes.productTreeVO.id;
		    	EasyUiCommon._mapProductOnSecondDialog.remove(id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
        			var id = row.attributes.productTreeVO.id;
	           		var text = row.attributes.productTreeVO.name;
	           		var code = text.split('-')[0];
	           		var name = text.split('-')[1];
	           		var insRow = {
	       					productCode:code,
	       					productName:name,
	       					quantity:$('#quantity_'+ id).val()
	           			};  
	           		var array = new Array();
	           		array.push(insRow);
	           		EasyUiCommon._mapProductOnSecondDialog.put(id,array);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		var id = row.attributes.productTreeVO.id;
		    		EasyUiCommon._mapProductOnSecondDialog.remove(id);
		    	}
		    },
		});
	},
	hideCheckBoxOnTree:function(input){
		var html = $(input).parent().parent().parent().parent().parent().parent().parent().parent().parent().children();
	    var htmlCat = html;
	    htmlCat = html.parent().parent().parent().parent().parent().parent().children();
	    var size = html.length;
	    var sizeCat = htmlCat.length;
	    for(var i = 0; i < size ; i++){
	    	if(i > 0){
	    		html = html.next();
	    	}
	    	html = html.first();
	    	var id = html.attr('id');
	    	if(id != undefined){
	    		var htmlSub = html;
	    		htmlSub.children().last().children().html('');
	    	}
	    }
	    for(var i = 0; i < sizeCat ; i++){
	    	if(i > 0){
	    		htmlCat = htmlCat.next();
	    	}
	    	htmlCat = htmlCat.first();
	    	var id = htmlCat.attr('id');
	    	if(id != undefined){
	    		var htmlCat1 = htmlCat;
	    		htmlCat1.children().last().children().html('');
	    	}
	    }
	},
	recursionCheck:function(arr,isHasTextBox){
		var size = 0;
		if(arr != null && arr != undefined){
			size = arr.length;
		}
		var lstArr = [];
		if(size == 0){
			return;
		}else{
			for(var i=0;i<size;i++){
				if(i < size){
					if(arr[i].type != 'PRODUCT'){
						$('.easyui-dialog #treeGrid').treegrid('select',arr[i].id);
						lstArr = arr[i].listChildren;
						EasyUiCommon.recursionCheck(lstArr,true);
					}else{
						if(isHasTextBox){
							if(i == 0){
								$('#quantity_'+arr[i].id).focus();
							}
						}
						$('.easyui-dialog #treeGrid').treegrid('select',arr[i].id);
					}
				}
			}
		}
	},
	recursionUnCheck:function(arr){
		var size = 0;
		if(arr != null && arr != undefined){
			size = arr.length;
		}
		var lstArr = [];
		if(size == 0){
			return;
		}else{
			for(var i=0;i<size;i++){
				if(i < size){
					if(arr[i].type != 'PRODUCT'){
						$('.easyui-dialog #treeGrid').treegrid('unselect',arr[i].id);
						lstArr = arr[i].listChildren;
						EasyUiCommon.recursionUnCheck(lstArr);
					}else{
						$('.easyui-dialog #treeGrid').treegrid('unselect',arr[i].id);
					}
				}
			}
		}
	},
	showTreeGridDialog:function(isPromotionProduct){
		$('#dialogProduct').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#errMsgProductDlg').html('').hide();
	        	EasyUiCommon._lstProductExpand = [];
	        	EasyUiCommon._mapProductOnSecondDialog = new Map();
	        	$('#productCodeDlg').focus();
	        	$('#productCodeDlg').val('');
	        	$('#productNameDlg').val('');
	        	$('#btnSeachProductTree').bind('click',function(){
	        		EasyUiCommon._lstProductExpand = [];
            		var param = '&code='+encodeChar($('.easyui-dialog #productCodeDlg').val().trim())+'&name='+encodeChar($('.easyui-dialog #productNameDlg').val().trim())+'&type=&parentId=&typeObject=2&isPromotionProduct='+isPromotionProduct;
        			EasyUiCommon.loadProductTreeGrid(param,isPromotionProduct);
        		});
            	$('#productCodeDlg,#productNameDlg').each(function(){
	        	    $(this).bind('keyup',function(event){
	        	        if(event.keyCode == keyCodes.ENTER){
	        	        	EasyUiCommon._lstProductExpand = [];
	        	        	var param = '&code='+encodeChar($('.easyui-dialog #productCodeDlg').val().trim())+'&name='+encodeChar($('.easyui-dialog #productNameDlg').val().trim())+'&type=&parentId=&typeObject=2&isPromotionProduct='+isPromotionProduct;
	            			EasyUiCommon.loadProductTreeGrid(param,isPromotionProduct);
	        	        }
	        	    });
	        	});
        		EasyUiCommon.loadProductTreeGrid(null,isPromotionProduct);
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#divProductContainer').hide();
	        }
	    });
	},
	selectOnSecondDialog:function(){
		$('#errMsgProductDlg').html('').hide();
		var msg = '';
		var newData = {};
    	if(EasyUiCommon.isMulti){
    		newData = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
    	}else{
    		newData = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
    	}
		var size = 0;
		if(newData!= undefined && newData!= null && newData.length > 0){
			size = newData.length;
		}
		var newDataOnSecondDialog = EasyUiCommon._mapProductOnSecondDialog.keyArray;
		var isErr = false;
		var id = 0;
		var lstArr = new Array();
		$('.easyui-dialog #searchGridUpdate').datagrid('deleteRow',size);
		if(newDataOnSecondDialog.length == 0){
			msg = 'Vui lòng chọn sản phẩm';
		}else{
			for(var i=size;i< size + newDataOnSecondDialog.length;i++){
				if(!isErr){
					var obj = EasyUiCommon._mapProductOnSecondDialog.get(newDataOnSecondDialog[i-size])[0];
					if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
						if($('#quantity_'+newDataOnSecondDialog[i-size]).val() != '' && $('#quantity_'+newDataOnSecondDialog[i-size]).val().trim() > 0){
							obj.quantity = $('#quantity_'+newDataOnSecondDialog[i-size]).val().trim();
						}else{
							msg = 'Vui lòng nhập số lượng';
							isErr = true;
							id = newDataOnSecondDialog[i-size];
							break;
						}
					}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
						if($('#quantity_'+newDataOnSecondDialog[i-size]).val() != '' && $('#quantity_'+newDataOnSecondDialog[i-size]).val().trim() > 0){
							obj.amount = $('#quantity_'+newDataOnSecondDialog[i-size]).val().trim();
						}else{
							msg = 'Vui lòng nhập tổng tiền';
							isErr = true;
							id = newDataOnSecondDialog[i-size];
							break;
						}
					}else if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
						if($('#quantity_'+newDataOnSecondDialog[i-size]).is(":checked")){
							obj.required = 1;
						}else{
							obj.required = 0;
						}
					}
					lstArr.push(obj);
				}
			}
		}
		if(msg.length > 0){
			$('#errMsgProductDlg').html(msg).show();
			EasyUiCommon.insertRowOnDialog(size);
			$('#quantity_'+id).focus();
		}else{
			for(var i = size; i< size + lstArr.length ; i++){
				$('.easyui-dialog #searchGridUpdate').datagrid('insertRow',{index: i, row:lstArr[i-size]});
				var arrValues = null;
				if(EasyUiCommon.isMulti){
					arrValues = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
				}else{
					arrValues = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
				}
				if(arrValues == undefined || arrValues == null || arrValues.length == 0){
					arrValues = new Array();
				}
				arrValues.push(lstArr[i-size]);
				if(EasyUiCommon.isMulti){
					EasyUiCommon._mapNewDataSubGrid2.put($('#indexDialog').val(),arrValues);
				}else{
					EasyUiCommon._mapNewDataSubGrid1.put($('#indexDialog').val(),arrValues);
				}
				EasyUiCommon._lstProductOnChange.push(lstArr[i-size].productCode);
			}
			if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
				$('.checkDialog').each(function(){
					$(this).focus();
				});
			}else{
				$('.numberDialog').each(function(){
					$(this).focusToEnd();
				});
			}
			if(EasyUiCommon.isMulti){
				EasyUiCommon.insertRowOnDialog(EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val()).length);
			}else{
				EasyUiCommon.insertRowOnDialog(EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val()).length);
			}
			$('#dialogProduct').dialog('close'); 
		}
	},
	stylerOnRow:function(index,value,type,isMulti){
		value = Utils.XSSEncode(value);
		var text = '<span style="color:white">.</span>';
		var img = '';
		var left = 0;
		if(type == 'ZV03' || type == 'ZV06'){
			left = $('.datagrid-view .datagrid-view2').width() - 118;
			img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',false,true)\"><img title='Thêm SP KM' src='/resources/images/icon-view.png'/></a>";
			if(value != '' && value != null && value != undefined){
				return EasyUiCommon.cssForNumberBox(value) + img;
			}
		}else if(type == 'ZV07' || type == 'ZV08'|| type == 'ZV10'|| type == 'ZV11'){
			left = $('.datagrid-view .datagrid-view2').children().first().children().children().children().children().children().first().children().width() - 18;
			img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','required','BB mua','checkbox',false,false)\"><img title='Thêm SP mua' src='/resources/images/icon-view.png'/></a>";
			if(value != '' && value != null && value != undefined){
				EasyUiCommon.codeByGroup = value;
				return EasyUiCommon.cssForCheckBox(value) + img;
			}
			if(index != 0 && EasyUiCommon.codeByGroup != ''){
				return EasyUiCommon.cssForCheckBox(EasyUiCommon.codeByGroup) + img;
			}
		}else if(type == 'ZV09' || type == 'ZV12'){
			if(isMulti != null && isMulti != undefined && isMulti){
//				left = $('.datagrid-view .datagrid-view2').width() - 118;
//				img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',true,true)\"><img src='/resources/images/icon-view.png'/></a>";
				var right = 10;
				img = "<a style=\"position: absolute; right: "+right+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',true,true)\"><img title='Thêm SP KM' src='/resources/images/icon-view.png'/></a>";
//				$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
				$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
				if(value != '' && value != null && value != undefined){
					return EasyUiCommon.cssForNumberBox(value) + img;
				}
			}else{
//				left = $('.datagrid-view .datagrid-view2').children().first().children().children().children().children().children().first().children().width() - 18;
//				img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','required','BB mua','checkbox',false,false)\"><img src='/resources/images/icon-view.png'/></a>";
				var right = 10;
				img = "<a style=\"position: absolute; right: "+right+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','required','BB mua','checkbox',false,false)\"><img title ='Thêm SP mua' src='/resources/images/icon-view.png'/></a>";
				$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
				$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
				if(value != '' && value != null && value != undefined){
					//EasyUiCommon.codeByGroup = value; //thachnn bo dong nay de fix bug
					return EasyUiCommon.cssForCheckBox(value) + img;
				}
				if(index != 0 && EasyUiCommon.codeByGroup != ''){
					return EasyUiCommon.cssForCheckBox(EasyUiCommon.codeByGroup) + img;
				}
			}
		}else if(type == 'ZV15' || type == 'ZV18'){
			if(isMulti != null && isMulti != undefined && isMulti){
				left = $('.datagrid-view .datagrid-view2').width() - 168;
				img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',true,true)\"><img title='Thêm SP KM' src='/resources/images/icon-view.png'/></a>";
			}else{
				left = $('.datagrid-view .datagrid-view2').children().first().children().children().children().children().children().first().children().width() - 18;
				if(type == 'ZV15'){
					img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',false,false)\"><img title='Thêm SP mua' src='/resources/images/icon-view.png'/></a>";
				}else{
					img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','amount','TT mua','numberbox',false,false)\"><img title='Thêm SP mua' src='/resources/images/icon-view.png'/></a>";
				}
			}
			if(value != '' && value != null && value != undefined){
				return EasyUiCommon.cssForNumberBox(value) + img;
			}
		}else if(type == 'ZV13' || type == 'ZV14'|| type == 'ZV16'|| type == 'ZV17'){
			left = $('.datagrid-view .datagrid-view2').children().first().children().children().children().children().children().first().children().width() - 18;
			if(type == 'ZV13' || type == 'ZV14'){
				img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',false,false)\"><img title='Thêm SP mua'  src='/resources/images/icon-view.png'/></a>";
			}else{
				img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','amount','TT mua','numberbox',false,false)\"><img title='Thêm SP mua'  src='/resources/images/icon-view.png'/></a>";
			}
			if(value != '' && value != null && value != undefined){
				return EasyUiCommon.cssForNumberBox(value) + img;
			}
		}else if(type == 'ZV21'){
			left = $('.datagrid-view .datagrid-view2').width() - 178;
			img = "<a style=\"position: absolute; left: "+left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',false,true)\"><img  title='Thêm SP KM' src='/resources/images/icon-view.png'/></a>";
			if(value != '' && value != null && value != undefined){
				return EasyUiCommon.cssForNumberBox(value) + img;
			}
		}
		return text + img;
	},
	formatLabelAfterChange:function(type){
		var count = 0;
		for(var x = 0; x < EasyUiCommon._mapProductCodeStr1.keyArray.length ; x++){
			if(EasyUiCommon._mapProductCodeStr1.get(x) != '' && EasyUiCommon._mapProductCodeStr1.get(x).length > 0){
				var html = $('.datagrid-view2 .datagrid-body table tbody').children();
				var indexValue = EasyUiCommon._mapProductCodeStr1.keyArray[x];
				for(var y = 0;y <= count ; y++){
					if(y > 0 ){
						html = html.next();
					}
				}
				if(type == 'ZV03' || type == 'ZV06'){
					html.first().children().last().prev().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV07' || type == 'ZV08' || type == 'ZV09' || type == 'ZV10' || type == 'ZV11' || type == 'ZV12'){
					html.first().children().first().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV15' || type == 'ZV18'){
					html.first().children().first().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV13' || type == 'ZV16'){
					html.first().children().first().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV14' || type == 'ZV17'){
					html.first().children().first().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV21'){
					html.first().children().first().next().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}
				count++;
			}
		}
		var count = 0;
		for(var x = 0; x < EasyUiCommon._mapProductCodeStr2.keyArray.length ; x++){
			if(EasyUiCommon._mapProductCodeStr2.get(x) != '' && EasyUiCommon._mapProductCodeStr2.get(x).length > 0){
				var html = $('.datagrid-view2 .datagrid-body table tbody').children();
				var indexValue = EasyUiCommon._mapProductCodeStr2.keyArray[x];
				for(var y = 0;y <= count ; y++){
					if(y > 0){
						html = html.next();
					}
				}
				if(type == 'ZV09' || type == 'ZV12'){
					html.first().children().last().prev().children().html(EasyUiCommon._mapProductCodeStr2.get(indexValue));
				}else{
					html.first().children().last().prev().prev().children().html(EasyUiCommon._mapProductCodeStr2.get(indexValue));
				}
				count++;
			}
		}
	},
	formatCurrency:function(num) {
		if(num == undefined || num == null) {
			return '';
		}
		num = num.toString().split('.');
		var ints = num[0].split('').reverse();
		for (var out=[],len=ints.length,i=0; i < len; i++) {
			if (i > 0 && (i % 3) === 0){
				out.push('.');	
			}
			out.push(ints[i]);
		}
		out = out.reverse() && out.join('');
		return out;
	}
};
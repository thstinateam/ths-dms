var Vietbando = {
	_DEF_LAT: 10.79418775,
	_DEF_LNG: 106.65682978,
	_DEF_LAT_BIG:  15.744675,
	_DEF_LNG_BIG: 107.515136,
	_DEF_ALL_ZOOM: 4,
	_DEF_ZOOM: 10,
	_DEF_ZOOM_BIG: 6,
	_map: null,
	_marker: null,
	_listMarker: null,
	_indexMarker: 0,
	sellerPositionMap: null,
	visitResultMap:null,
	_latUnitTree: null,
	_lngUnitTree: null,
	CalculateDistance:function(i,k){
		var n,l,h;var j;var c;var e;var a;var m;
		var d=i.latitude;
		var b=i.longitude;
		var g=k.latitude;
		var f=k.longitude;
		j=d*(Math.PI/180);
		c=b*(Math.PI/180);
		e=g*(Math.PI/180);
		a=f*(Math.PI/180);
		n=b-f;m=n*(Math.PI/180);
		h=Math.sin(j)*Math.sin(e)+Math.cos(j)*Math.cos(e)*Math.cos(m);
		h=Math.acos(h);
		l=h*180/Math.PI;
		l=l*60*1.1515;
		l=l*1.609344*1000;
		return Math.round(l);
	},
	CalculateDistanceFormat : function(distance) {
		if (distance >= 1000) {
			return roundNumber((distance/1000),2);
		}
	},
	loadMap: function(objId, lat, lng, zoom,callback, showMarker){
		 if (VBrowserIsCompatible()){
			 var isLatLngValidate = lat == null || lng == null || lat == 'null' || lng == 'null' || isNaN(lat) || isNaN(lng) ? false : true;
			 if(Vietbando._map==null || Vietbando._map==undefined){
				 Vietbando._map = new VMap(document.getElementById(objId));
			 }			 
			 var map = Vietbando._map; 
			 if(showMarker == null) {
				 showMarker = true;
			 }
			 //var showMarker = true;/*cho phep truyen bien nay vao*/			 
			 if(lat == null || lng == null || !Vietbando.isValidLatLng(lat,lng)){
				 lat = Vietbando._DEF_LAT;
				 lng = Vietbando._DEF_LNG;
				 showMarker = false;
				 zoom = Vietbando._DEF_ALL_ZOOM;
			 } else if(zoom == undefined || zoom == 0 || zoom == null){
				 zoom = Vietbando._DEF_ZOOM;
			 }
			 var pt = new VLatLng(lat, lng);
			 if(isLatLngValidate) {
				 Vietbando._map.setCenter(pt, zoom);
			 } else {
				 Vietbando._map.setCenter(new VLatLng(Vietbando._DEF_LAT, Vietbando._DEF_LNG), zoom);
			 }
		     Vietbando._map.addControl(new VSmallZoomControl());
		    
		     
		     if(showMarker){	
		    	 if (Vietbando._marker != null)
		          	   Vietbando._map.removeOverlay(Vietbando._marker);
		    	 if(isLatLngValidate) {
		    		 Vietbando._marker = new VMarker(pt, new VIcon());
		    	 } else {
		    		 Vietbando._marker = null;
		    	 }
		    	 
			     //marker.enableDragging();
//			     VEvent.addListener(marker, "dragend", function(overlay, pt){			           
//			           if (pt == null) return;
//			           if (callback != null) {
//			        	   callback.call(this, pt);
//					   }
//			     });
		    	 if(Vietbando._marker != null) {
		    		 Vietbando._map.addOverlay(Vietbando._marker);
		    	 }
		     }
		     if(objId!="sellerPosMapContainer"){
		    	 VEvent.addListener(Vietbando._map, 'click', function(overlay, pt) {		    	
		    		 if (pt == null) return;
		    		 var latlng = new VLatLng(pt.latitude, pt.longitude);
		    		 if (Vietbando._marker != null)
		    			 Vietbando._map.removeOverlay(Vietbando._marker);
		    		 Vietbando._marker = new VMarker(latlng);
		    		 //marker.enableDragging();
//	               VEvent.addListener(marker, "dragend", function(ovl, point){			           
//			           if (point == null) return;
//			           if (callback != null) {
//			        	   callback.call(this, point);
//					   }
//			     });
		    		 Vietbando._map.addOverlay(Vietbando._marker);
		    		 if (callback != null) {
		    			 callback.call(this, pt);
		    		 }
		    	 });
		     }else{//dang ky su kien khi click marker tren ban do giam sat vi tri NVBH
		    	 	VEvent.addListener(Vietbando._map, 'click', function(overlay, pt) {
						if (overlay != null) {// kick vao marker
							if (overlay.objectType == 7) {//TBHV
								var html =String.format(Vietbando.TBHVInfoContent,overlay.staffCode,overlay.staffName,overlay.shopCode,overlay.GSNPPStr,overlay.lastPosTime,overlay.accuracy);
								Vietbando.sellerPositionMap.openInfoWindow(pt,html);
							}else if (overlay.objectType == 5) {//GSNPP
								$.ajax({
									type : "POST",
									url : "/superviseshop/visit-result/getGSNPPdetail",
									data : {staffId:overlay.staffId,objectType:overlay.objectType},
									dataType : "json",
									success : function(data) {
										if (data.success)
									    {
											//lay danh sach NVBH di cung tuyen
											if (data.childInfo!=null && data.childInfo.length>0)
										    {
												var firstSubItem = true;
												for(var i=0;i<data.childInfo.length;i++){
													var subItem = data.childInfo[i];
													if(subItem.staffCode!=null && subItem.staffCode!=undefined && subItem.staffCode!=''){
														if(firstSubItem){
															overlay.NVBHStr = Utils.XSSEncode(subItem.staffCode);//NVBH Code
															firstSubItem = false;
														}else{
															overlay.NVBHStr = Utils.XSSEncode(overlay.NVBHStr) + ", " + Utils.XSSEncode(subItem.staffCode);
														}
													}
												}
										    }
											var html =String.format(Vietbando.GSNPPInfoContent,overlay.staffCode,overlay.staffName,overlay.shopCode,overlay.NVBHStr,overlay.lastPosTime,overlay.accuracy);
											Vietbando.sellerPositionMap.openInfoWindow(pt,html);
									    }
									},
									error:function(XMLHttpRequest, textStatus, errorThrown) {
										
									}
								});
							}else if (overlay.objectType == 1) {//NVBH
								$.ajax({
									type : "POST",
									url : "/superviseshop/visit-result/getNVBHdetail",
									data : {staffId:overlay.staffId,objectType:overlay.objectType},
									dataType : "json",
									success : function(data) {
										if (data.success)
									    {
											if (data.mainInfo!=null && data.mainInfo.length>0)
										    {
												var item = data.mainInfo[0];
												overlay.revenue = ((item.amount!=null && item.amount!=undefined)?item.amount:"") + ((item.plan!=null && item.plan!=undefined)?("/"+item.plan):"");
												overlay.channelPoint = (item.pointInRouting!=null && item.pointInRouting!=undefined)?item.pointInRouting:"";
												overlay.revenuePoint = (item.pointAmount!=null && item.pointAmount!=undefined)?item.pointAmount:"";
												overlay.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
										    }
											var html =String.format(Vietbando.NVBHInfoContent,overlay.staffCode,overlay.staffName,overlay.shopCode,overlay.revenue,overlay.channelPoint,overlay.revenuePoint,overlay.lastPosTime,overlay.accuracy);
											Vietbando.sellerPositionMap.openInfoWindow(pt,html);
									    }
									},
									error:function(XMLHttpRequest, textStatus, errorThrown) {
										
									}
								});
							}else if(overlay.objectType == -1){//Customer
								var html =String.format(Vietbando.CustomerInfoContent,overlay.staffCode,overlay.staffName);
								Vietbando.sellerPositionMap.openInfoWindow(pt,html);
							}
						}
					});
		    	 	Vietbando.sellerPositionMap = map;
		    	 	if (callback != null) {
		    	 		callback.call(this);
		    	 	}
		     }
		 }
		 //return map;
	},
	addMarkerStaff:function(point){
		var map = Vietbando._map;
		if(Vietbando.isValidLatLng(point.lat, point.lng)) {
			var pt = new VLatLng(point.lat, point.lng);
			var info="<div id='info"+point.staffId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
			var title=point.staffName;
			var image=point.image;
			var accuracy = point.accuracy;
			var createTime = point.createTime;
			map.addOverlay(new MyOwnOverlay(point.staffId, pt, image, title,info,point));
		}
	},
	addMarkerCust:function(point){
		var map = Vietbando._map;
		if(Vietbando.isValidLatLng(point.lat, point.lng)) {
			var pt = new VLatLng(point.lat, point.lng);
			map.addOverlay(new MyOverlayCust(point,pt));					
		}
	},
	addMarkerImages:function(lat,lng){
		if(Vietbando.isValidLatLng(lat, lng)) {
			Vietbando._map.clearOverlays();
			var pt = new VLatLng(lat, lng);
			Vietbando._map.setCenter(pt, 13);
			Vietbando._map.addOverlay(new MyOverlayImages(pt));					
		}
	},
	loadBigMap: function(objId, lat, lng, zoom,callback){
		 if (VBrowserIsCompatible()){
			 var isLatLngValidate = lat == null || lng == null || lat == 'null' || lng == 'null' || isNaN(lat) || isNaN(lng) ? false : true;
			 Vietbando._map = new VMap(document.getElementById(objId));
			 var map = Vietbando._map; 
			 var showMarker = true;			 
			 if(!Vietbando.isValidLatLng(lat,lng)){
				 lat = Vietbando._DEF_LAT_BIG;
				 lng = Vietbando._DEF_LNG_BIG;
				 showMarker = false;
				 zoom = Vietbando._DEF_ZOOM_BIG;
			 } else if(zoom == undefined || zoom == 0 || zoom == null){
				 zoom = Vietbando._DEF_ZOOM_BIG;
			 }
			 var pt = new VLatLng(lat, lng);
			 if(isLatLngValidate) {
				 map.setCenter(pt, zoom);
			 } else {
				 map.setCenter(new VLatLng(Vietbando._DEF_LAT_BIG, Vietbando._DEF_LNG_BIG), zoom);
			 }
		     map.addControl(new VLargeMapControl(), new VControlPosition(V_ANCHOR_TOP_RIGHT, new VSize(5, 5)));
		     $('.ZoomPinLeft').css('left','-50px');
		     if(showMarker){
		    	 if(isLatLngValidate) {
		    		 Vietbando._marker = new VMarker(pt, new VIcon());
		    		 map.addOverlay(Vietbando._marker);
		    	 }
		     }
		     if(callback != null) {
		    	 VEvent.addListener(Vietbando._map, 'click', function(overlay, pt) {		    	
			    	 if (pt == null) return;
		               var latlng = new VLatLng(pt.latitude, pt.longitude);
		               if (Vietbando._marker != null)
		            	   Vietbando._map.removeOverlay(Vietbando._marker);
		               Vietbando._marker = new VMarker(latlng);
		               Vietbando._map.addOverlay(Vietbando._marker);
		               if (callback != null) {
			        	   callback.call(this, pt);
					   }
			     });
		     }
		 }
		 return map;
	},
	//load ban do hien thi lo trinh disable event click tao marker
	loadBigMapResult: function(objId, lat, lng, zoom,callback){
		 if (VBrowserIsCompatible()){
			 Vietbando._map = new VMap(document.getElementById(objId));
			 var map = Vietbando._map; 
			 var showMarker = true;			 
			 if(!Vietbando.isValidLatLng(lat,lng)){
				 lat = Vietbando._DEF_LAT_BIG;
				 lng = Vietbando._DEF_LNG_BIG;
				 showMarker = false;
				 zoom = Vietbando._DEF_ZOOM_BIG;
			 } else if(zoom == undefined || zoom == 0 || zoom == null){
				 zoom = Vietbando._DEF_ZOOM_BIG;
			 }
			 var pt = new VLatLng(lat, lng);
		     map.setCenter(pt, zoom);
		     map.addControl(new VLargeMapControl());
		    
		     if(showMarker){
		    	 Vietbando._marker = new VMarker(pt, new VIcon());
			     map.addOverlay(Vietbando._marker);
		     }
		     
		     if(objId=="bigMapContainer"){
		    	 VEvent.addListener(Vietbando._map, 'click', function(overlay, pt) {
		    	 if (overlay != null && overlay.isCustomer == undefined) {// kick vao marker
						var html =String.format(Vietbando.CustomerDetail,overlay.customerName,overlay.seq,overlay.address,overlay.mobiphone,overlay.phone);
						Vietbando.visitResultMap.openInfoWindow(pt,html);
					}
		    	 });
		     }
		     
		 }
		 return map;
	},
	loadBigMapForUnitTree: function(objId, lat, lng, zoom,callback){
		 if (VBrowserIsCompatible()){
			 Vietbando._map = new VMap(document.getElementById(objId));		 
			 if(!Vietbando.isValidLatLng(lat,lng)){
				 lat = Vietbando._DEF_LAT_BIG;
				 lng = Vietbando._DEF_LNG_BIG;
				 zoom = Vietbando._DEF_ZOOM_BIG;
			 } else { 
				 if(zoom == undefined || zoom == 0 || zoom == null){
					 zoom = Vietbando._DEF_ZOOM_BIG;
				 }
				 var latlng = new VLatLng(lat, lng);
	    		 var marker = new VMarker(latlng);
	    		 Vietbando._map.addOverlay(marker);
			 }
			 var pt = new VLatLng(lat, lng);
			 Vietbando._map.setCenter(pt, zoom);
			 Vietbando._map.addControl(new VLargeMapControl());
		    
		     VEvent.addListener(Vietbando._map, 'click', function(overlay, pt) {		    	
		    	 if (pt == null) return;
	               var latlng = new VLatLng(pt.latitude, pt.longitude);
	               var marker = new VMarker(latlng);
	               Vietbando._map.clearOverlays();
		    	   Vietbando._map.addOverlay(marker);
	               if (callback != null) {
		        	   callback.call(this, pt);
				   }
		     });
		 }
		 return Vietbando._map;
	},
	loadMapForUnitTree : function(objId, lat, lng, zoom,callback, address){
		 if (VBrowserIsCompatible()){
			 Vietbando._map = new VMap(document.getElementById(objId)); 		 
			 if(!Vietbando.isValidLatLng(lat,lng)){
//				 if(address != undefined && address != null && address != '') {
//					 var client = new VClientGeocoder();
//			         client.getLatLng(address,'getlatlng');
//			         lat = Vietbando._latUnitTree;
//					 lng = Vietbando._lngUnitTree;
//				 } else {
					 lat = Vietbando._DEF_LAT;
					 lng = Vietbando._DEF_LNG;
					 zoom = Vietbando._DEF_ALL_ZOOM;
//				 }
			 } else{ 
				 if(zoom == undefined || zoom == 0 || zoom == null){
					 zoom = Vietbando._DEF_ZOOM;
				 }
				 var latlng = new VLatLng(lat, lng);
	    		 var marker = new VMarker(latlng);
	    		 Vietbando._map.addOverlay(marker);
			 }
			 var pt = new VLatLng(lat, lng);
			 Vietbando._map.setCenter(pt, zoom);
		     Vietbando._map.addControl(new VSmallZoomControl());
		     
	    	 VEvent.addListener(Vietbando._map, 'click', function(overlay, pt) {		    	
	    		 if (pt == null) return;
	    		 var latlng = new VLatLng(pt.latitude, pt.longitude);
	    		 var marker = new VMarker(latlng);
	    		 Vietbando._map.clearOverlays();
	    		 Vietbando._map.addOverlay(marker);
	    		 if (callback != null) {
	    			 callback.call(this, pt);
	    		 }
	    	 });
		 }
		 //return map;
	},
	addMarkerWhenAddress: function(address) {
	   if (VBrowserIsCompatible())
	   {
	       map = new VMap(document.getElementById('container'));
	       map.setCenter(new VLatLng(10.8152328, 106.680505), 4);

	       var client = new VClientGeocoder();
	       client.getLatLng(address, 'getlatlng');
	   }
	},
	loadMapImage: function(objId, lat, lng, zoom){
		 if (VBrowserIsCompatible()){
			 Vietbando._map = new VMap(document.getElementById(objId));
			 var map = Vietbando._map; 
			 var showMarker = true;			 
			 if(!Vietbando.isValidLatLng(lat,lng)){
				 lat = Vietbando._DEF_LAT;
				 lng = Vietbando._DEF_LNG;
				 showMarker = false;
				 zoom = Vietbando._DEF_ALL_ZOOM;
			 } else if(zoom == undefined || zoom == 0 || zoom == null){
				 zoom = Vietbando._DEF_ZOOM;
			 }
			 var pt = new VLatLng(lat, lng);
		     map.setCenter(pt, zoom);
		     map.addControl(new VSmallZoomControl());
		    
		     if(showMarker){
		    	 Vietbando._marker = new VMarker(pt, new VIcon());
			     map.addOverlay(Vietbando._marker);
			     Vietbando.addMarker(lat,lng);
		     }
		 }
		 return map;
	},
	loadMapWithMultiMarker : function(objId, lstPoint, centerPtLat, centerPtLng, zoom, callback) {
		if (VBrowserIsCompatible()){
			Vietbando._map = new VMap(document.getElementById(objId));
			var map = Vietbando._map; 
			var lat=0;
			var lng=0;
			VEvent.addListener(map, 'click', function(overlay, pt) { 
				$('#divUlDate').hide('slow'); });
		    if(!Vietbando.isValidLatLng(centerPtLat,centerPtLng)){
		    	centerPtLat = Vietbando._DEF_LAT;
		    	centerPtLng = Vietbando._DEF_LNG;
			} else if(zoom == undefined || zoom == 0 || zoom == null){
				 zoom = Vietbando._DEF_ZOOM;
			}
		    var centerPt = new VLatLng(centerPtLat, centerPtLng);
			map.setCenter(centerPt, zoom);
		    map.addControl(new VSmallZoomControl());
			Vietbando._listMarker=lstPoint;
			for(var i = 0; i < lstPoint.length; i++) {
				var point = lstPoint[i];
				if(Vietbando.isValidLatLng(point.lat, point.lng)) {
					var pt = new VLatLng(point.lat, point.lng);
					var custId=point.customerId;
					var info="<strong>" + Utils.XSSEncode(point.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(point.customerName) + "</strong><br/>";
					info += "Địa chỉ: ";
					if(point.address != null)//loctt - Oct24, 2013
						info += Utils.XSSEncode(point.address);
					var title= Utils.XSSEncode(point.customerCode);
					var image="/resources/images/Mappin/icon_circle_red.png";
					if(custId<0){ 
						image="/resources/images/Mappin/icon_circle_red.png";
						title="...";
					}
					map.addOverlay(new MyOverlay(custId, pt, image, title,info));
				}
			}
			Vietbando._map.fitOverlays();
		}
		return map;
	},
	loadMapWithMultiMarkerForSetOrder : function(objId, lstPoint, centerPtLat, centerPtLng, zoom, callback) {
		if (VBrowserIsCompatible()){
			Vietbando._map = new VMap(document.getElementById(objId));
			var map = Vietbando._map; 
			var lat=0;
			var lng=0;
		    if(!Vietbando.isValidLatLng(centerPtLat,centerPtLng)){
		    	centerPtLat = Vietbando._DEF_LAT;
		    	centerPtLng = Vietbando._DEF_LNG;
			} else if(zoom == undefined || zoom == 0 || zoom == null){
				 zoom = Vietbando._DEF_ZOOM;
			}
		    var centerPt = new VLatLng(centerPtLat, centerPtLng);
			map.setCenter(centerPt, zoom);
		    map.addControl(new VSmallZoomControl());
			Vietbando._listMarker=lstPoint;
			for(var i = 0; i < lstPoint.length; i++) {
				var point = lstPoint[i];
				if(Vietbando.isValidLatLng(point.lat, point.lng)) {
					var pt = new VLatLng(point.lat, point.lng);
					var custId=point.customerId;
					var info="<strong>" + point.customerCode + "</strong> - <strong>" + point.customerName + "</strong><br/>";
					info += "Địa chỉ: ";
					if(point.address!=null) //loctt - Oct24, 2013
			            info += point.address;
					var ttgt=point.ttgt;
					if(custId<0){ 
						ttgt="...";
					}
					map.addOverlay(new MyOverlayCust(custId, point.customerCode, pt, ttgt,info));					
				}
			}
			Vietbando._map.fitOverlays();
		}
		return map;
	},
	showWindowInfo4Supervise:function(objectId,lat,lng,info,cur,time){		
		var pt = new VLatLng(lat, lng);
		if(objectId>0){
			var temp = SuperviseSales._lstStaffPosition.get(objectId);
			SuperviseSales._idStaffSelect = objectId;
			SuperviseSales._idStaffSelected = objectId;
			if(temp!= null){
				Vietbando._map.openInfoWindow(pt,"<div style='width:100%;text-align: center;'><img style='position: relative;top:20px;' src='/resources/images/loading-small.gif'/></div><br/>");
				if(temp.roleType==7){
					SuperviseSales.showDialogTBHVInfo(temp.staffId,temp.staffCode,temp.staffName, temp.shopCode, temp.shopName,temp.createTime,temp.accuracy,pt);
				}else if(temp.roleType==5){
					SuperviseSales.showDialogNVGSInfo(temp.staffId,temp.staffCode,temp.staffName, temp.shopCode, temp.shopName, temp.createTime,temp.accuracy,pt);
				}else if(temp.roleType==1 || temp.roleType==2){
					SuperviseSales.showDialogNVBHInfo(temp.staffId,temp.staffCode,temp.staffName, temp.shopCode, temp.shopName, temp.createTime,temp.accuracy,pt);
				}
			}
		}
	},
	showWindowInfo:function(custId,lat,lng,info){		
		var pt = new VLatLng(lat, lng);
		var list=Vietbando._listMarker;
		if(custId>0){
			Vietbando._map.openInfoWindow(pt, info);
		}
		else if(Vietbando._indexMarker!=null){
			for(i=0;i<list.length;i++){
				if(custId==list[i].customerId){
					var listMerce=list[i].listMerce;
					if(Vietbando._indexMarker>=listMerce.length){
						Vietbando._indexMarker=0;
					}
					var infoMerce="<strong>" + Utils.XSSEncode(listMerce[Vietbando._indexMarker].customerCode) + "</strong> - <strong>" + Utils.XSSEncode(listMerce[Vietbando._indexMarker].customerName) + "</strong><br/>";						
					//loctt - Oct24, 2013 - begin
					var address = listMerce[Vietbando._indexMarker].address;
					infoMerce += "Địa chỉ: ";
					if(address != null)
						info += address;
					//loctt - Oct24, 2013 - end
					Vietbando._indexMarker++;
					var pixel = Vietbando._map.fromLatLngToDivPixel(pt);					
					pixel.y=pixel.y-40;
					pixel.x=pixel.x+5;
					pt= Vietbando._map.fromDivPixelToLatLng(pixel);
					Vietbando._map.openInfoWindow(pt, infoMerce);
				}
			}
		}
	},
	addMarker: function(lat,lng){
		if(!Vietbando.isValidLatLng(lat,lng)){
			return;
		}
		var latlng = new VLatLng(lat, lng);
        if (Vietbando._marker != null){
        	Vietbando._map.removeOverlay(Vietbando._marker);
        }
        Vietbando._marker = new VMarker(latlng);
        var point = new VLatLng(lat, lng);
        Vietbando._map.setCenter(point, Vietbando._DEF_ZOOM);
        Vietbando._map.addOverlay(Vietbando._marker);
	},
	addMarkerNotCenter: function(lat,lng){
		if(!Vietbando.isValidLatLng(lat,lng)){
			return;
		}
		var latlng = new VLatLng(lat, lng);
		if (Vietbando._marker != null){
        	Vietbando._map.removeOverlay(Vietbando._marker);
        	//Vietbando._map.clearOverlays();
        }
        Vietbando._marker = new VMarker(latlng);
        Vietbando._map.addOverlay(Vietbando._marker);
	},
	isValidLatLng: function(lat,lng){
		if(lat==undefined || lng == undefined || lat== null || lng== null || lat == 0.0 || lng ==0.0){
			return false;
		}
		return true;
	},
	addMutilMarkerStaff:function(){
		var map = Vietbando._map;
		if(Vietbando._listMarker!=undefined && Vietbando._listMarker!=null){
			var flag=0;
			var index=0;
			SuperviseSales._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<Vietbando._listMarker.valArray.length;i++,index++,j++){
					if(j>100) break;
					var point = Vietbando._listMarker.valArray[i];
					if(Vietbando.isValidLatLng(point.lat, point.lng)) {
						var pt = new VLatLng(point.lat, point.lng);
						var info="<div id='info"+point.staffId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
						var title=point.staffName;
						var image=point.image;
						var accuracy = point.accuracy;
						var createTime = point.createTime;
						map.addOverlay(new MyOwnOverlay(point.staffId, pt, image, title,info,point));
					}
				}
				if(index>=Vietbando._listMarker.valArray.length){
					Vietbando.hideShowTitleMarker();
					if(SuperviseSales._notFitOverlay==undefined || SuperviseSales._notFitOverlay==null){
						Vietbando._map.fitOverlays();
					}else if(SuperviseSales._idStaffSelected!=null && SuperviseSales._idStaffSelect==SuperviseSales._idStaffSelected ){
						SuperviseSales.moveToStaff(SuperviseSales._idStaffSelect);
					}
					SuperviseSales._notFitOverlay=null;
					window.clearInterval(SuperviseSales._itv);
				}
				Vietbando.hideShowTitleMarker();
			},300); 
		}
	},
	hideShowTitleMarker:function() {
		if(Vietbando._map.getLevel() >= 13) {
			$('.titleShop').show();
			if(Vietbando._map.getLevel() >= 16) {
				$('.titleMarker').show();
			} else {
				$('.titleMarker').hide();
			}
		} else {
			$('.titleShop').hide();
			$('.titleMarker').hide();
		}
	},
	TBHVInfoContent : '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Mã TBHV:</dt><dd>{0}</dd>'
					  +'<dt>Tên TBHV:</dt><dd>{1}</dd><dt>Đơn vị:</dt><dd>{2}</dd><dt>Đang đi trên tuyến cùng:</dt>'
					  +'<dd>{3}</dd><dt>Cập nhật vị trí cuối:</dt><dd>{4}</dd>'
					  +'<dt>Độ chính xác:</dt><dd>{5}</dd></dl></div>',
	GSNPPInfoContent : '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Mã GSNPP:</dt><dd>{0}</dd>'
					  +'<dt>Tên GSNPP:</dt><dd>{1}</dd><dt>Đơn vị:</dt><dd>{2}</dd><dt>Đang đi trên tuyến cùng:</dt>'
					  +'<dd>{3}</dd><dt>Cập nhật vị trí cuối:</dt><dd>{4}</dd>'
					  +'<dt>Độ chính xác:</dt><dd>{5}</dd></dl></div>',
	NVBHInfoContent : '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Mã NVBH:</dt><dd>{0}</dd>'
					  +'<dt>Tên NVBH:</dt><dd>{1}</dd><dt>Đơn vị:</dt><dd>{2}</dd><dt>Doanh số ngày/KH:</dt>'
					  +'<dd>{3}</dd><dt>Điểm trong tuyến:</dt><dd>{4}</dd>'
					  +'<dt>Điểm doanh số:</dt><dd>{5}</dd><dt>Cập nhật vị trí cuối cùng:</dt><dd>{6}</dd>'
					  +'<dt>Độ chính xác:</dt><dd>{7}</dd></dl></div>',
	CustomerInfoContent : '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Mã điểm lẻ:</dt><dd>{0}</dd>'
					  +'<dt>Tên điểm lẻ:</dt><dd>{1}</dd></dl></div>',
	CustomerDetail :    '<div class="EmployeeInfoBox"><dl class="Dl3Style FixFloat"><dt>Tên KH:</dt><dd>{0}</dd>'
		  				+'<dt>Thứ tự:</dt><dd>{1}</dd><dt>Địa chỉ:</dt><dd>{2}</dd><dt>Di Động:</dt>'
		  				+'<dd>{3}</dd><dt>Cố định:</dt><dd>{4}</dd>'
						  
};
var MapUtil = {
		loadedResources:new Array(),
		OVERLAY_ZINDEX : 9,
		OVERLAY_ZINDEX_DEFAULT : 1,
		getBrowserName : function() {
			var browserName = "";
			var ua = navigator.userAgent.toLowerCase();
			if (ua.indexOf("opera") != -1)
				browserName = "opera";
			else if (ua.indexOf("msie") != -1)
				browserName = "msie";
			else if (ua.indexOf("safari") != -1)
				browserName = "safari";
			else if (ua.indexOf("mozilla") != -1) {
				if (ua.indexOf("firefox") != -1)
					browserName = "firefox";
				else
					browserName = "mozilla";
			}
		},
		loadMapResource:function(callback){
			MapUtil.loadResource(vbd_url, 'js',callback);
		},
		loadResource:function(filename, filetype, callback){
			if ($.inArray(filename, MapUtil.loadedResources) == -1){
				if (filetype=="js"){ // if filename is a external JavaScript file
					var fileref=document.createElement('script');
					fileref.setAttribute("type","text/javascript");
					fileref.setAttribute("src", filename);
				}
				if (typeof fileref != "undefined"){
					if(callback != undefined){
						if(fileref.readyState){
							fileref.onreadystatechange = function (){
								if (fileref.readyState == 'loaded' || fileref.readyState == 'complete'){
									callback();
								}
							};
						} else {
							fileref.onload = callback;
						}
					}
					document.getElementsByTagName("head")[0].appendChild(fileref);
				}
				MapUtil.loadedResources.push(filename);
			} else {
				if(callback != undefined){
					callback();
				}
			}
		}
	};
function MyOverlayImages(latlng)
{
    this.latlng = latlng;
    this.latlngs = latlng;
    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;
    function initialize(map)
    {
    	this.mapObj = map;
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.className = "overlayImage";
        myDiv.style.position = "absolute";
        myDiv.style.textAlign = "center";
        myDiv.style.width = "20px";
        myDiv.innerHTML ="<img src='/resources/images/Mappin/red.png' />";
        map.getOverlayContainer().appendChild(myDiv);
        this.divObj = myDiv;
        this.redraw();
    }
    
    
    function remove()
    {
    	try{
        this.divObj.parentNode.removeChild(this.divObj);
    	}catch(e){}
    }

    function copy()
    {
        return new MyOverlay(this.latlng, this.img);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
	    this.divObj.style.left = (pt.x-this.divObj.clientWidth/2) + "px";
	    this.divObj.style.top = (pt.y-45) + "px";
    }

    function vType()
    {
        return 'MyOverlay';
    }
}
function MyOwnOverlay(objectId, latlng, img, title, info,object)
{
    this.latlng = latlng;
    this.latlngs = latlng;
    this.img = img;    

    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;
    this.info=info;
    this.objectId=objectId;
    this.title=title;
    this.object = object;

    function initialize(map)
    {
    	this.mapObj = map;
    	var level = map.getLevel();
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.className = "Overlay"+objectId;
        myDiv.style.position = "absolute";
        myDiv.style.textAlign = "center";
        myDiv.style.width = "100px";
        var lat=latlng.latitude;
        var lng=latlng.longitude;
        if(level>=13){
        	myDiv.innerHTML="<strong style='color:red;font-size: 12px;' class='titleShop'>" + Utils.XSSEncode(this.object.shopCode) + "</strong></br>";
        }else{
        	myDiv.innerHTML="<strong style='color:red;font-size: 12px;' class='titleShop' style='display:none'>" + Utils.XSSEncode(this.object.shopCode) + "</strong></br>";
        }
        myDiv.innerHTML += "<img id='img"+this.objectId+"' alt='" + this.title + "' src='" + this.img + "' onclick=\"Vietbando.showWindowInfo4Supervise("+objectId+","+lat+","+lng+",'"+this.object.accuracy+"','"+this.object.createTime+"');\"/></br>";
        if(level>=16){
        	myDiv.innerHTML+="<strong style='color:red;font-size: 12px;' class='titleMarker'>" + this.title + "</strong>";
        }else{
        	myDiv.innerHTML+="<strong style='color:red;font-size: 12px;' class='titleMarker' style='display:none'>" + this.title + "</strong>";
        }
        map.getOverlayContainer().appendChild(myDiv);
        this.divObj = myDiv;
        this.redraw();
    }
    
    
    function remove()
    {
    	try{
            this.divObj.parentNode.removeChild(this.divObj);
        }catch(e){}
    }

    function copy()
    {
        return new MyOverlay(this.latlng, this.img);
    }

    function redraw()
    {
    	if(this.mapObj.getLevel() >=13) {
    		$('#'+this.mapObj.id+'MyOverlay .titleShop').show();
    	} else {
    		$('#'+this.mapObj.id+'MyOverlay .titleShop').hide();
    	}
    	if(this.mapObj.getLevel() >= 16) {
    		$('#'+this.mapObj.id+'MyOverlay .titleMarker').show();
    	} else {
    		$('#'+this.mapObj.id+'MyOverlay .titleMarker').hide();
    	}
    	
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
	    this.divObj.style.left = (pt.x-this.divObj.clientWidth/2) + "px";
	    this.divObj.style.top = (pt.y-60) + "px";
    }

    function vType()
    {
        return 'MyOverlay';
    }
}
function MyOverlayCust(object,latlng)
{
    this.latlng = latlng;
    this.latlngs = latlng;
    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;
    this.object=object;
    function initialize(map)
    {
    	this.mapObj = map;
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.className = "overlayCust";
        myDiv.style.position = "absolute";
        myDiv.style.textAlign = "center";
        myDiv.style.width = "60px";
        //myDiv.innerHTML ='<div class="CustomersStatus '+this.object.image+'" onclick="SuperviseSales.showCustomerDetail('+this.object.id+');"><span>'+this.object.ordinalVisit+'</span></div>';
        var html = '<div class="CustomersStatus '+this.object.image+'" onclick="SuperviseSales.showCustomerDetail('+this.object.id+');">';
        html += '<span>'+this.object.ordinalVisit+'</span>';
        if(this.object.ordinalAndTimeVisitReal != null && this.object.ordinalAndTimeVisitReal != undefined) {
        	if(this.object.ordinalVisit == '') {
        		html += '<span class="CustomerTimeVisit" style="color:red; display:block; position: absolute; left: -12px; padding-top: 26px;font-size: 12px;">'+this.object.ordinalAndTimeVisitReal+'</span>';
        	} else {
        		html += '<span class="CustomerTimeVisit" style="color:red; display:block; position: absolute; left: -12px; padding-top: 10px;font-size: 12px;">'+this.object.ordinalAndTimeVisitReal+'</span>';
        	}
        }
        html +='</div>';
        myDiv.innerHTML =html;
        map.getOverlayContainer().appendChild(myDiv);
        this.divObj = myDiv;
        this.redraw();
    }
    
    
    function remove()
    {
    	try{
        this.divObj.parentNode.removeChild(this.divObj);
    	}catch(e){}
    }

    function copy()
    {
        return new MyOverlay(this.latlng, this.img);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
	    this.divObj.style.left = (pt.x-this.divObj.clientWidth/2) + "px";
	    this.divObj.style.top = (pt.y-22) + "px";
    }

    function vType()
    {
        return 'MyOverlay';
    }
}
function MyOverlay(latlng,strHTML){
    this.latlng = latlng;
    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;

    function initialize(map)
    {
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.style.position = "absolute";
        myDiv.innerHTML = strHTML;
        map.getOverlayContainer().appendChild(myDiv);
        this.mapObj = map;
        this.divObj = myDiv;
        this.redraw();
    }

    function remove()
    {
    	if(this.divObj.parentNode != null){
    		this.divObj.parentNode.removeChild(this.divObj);
    	}
    }

    function copy()
    {
        return new MyOverlay(this.latlng);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        this.divObj.style.left = pt.x + "px";
        this.divObj.style.top = pt.y  + "px";
    }

    function vType()
    {
        return 'MyOverlay';
    }
}
//customize Overlay cho module hien~ thi lo trinh
function MyRouteOverlay(latlng,strHTML){
    this.latlng = latlng;
    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;

    function initialize(map)
    {
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyRouteOverlay";
        myDiv.style.position = "absolute";
        myDiv.style.fontSize = "9";
        myDiv.style.fontWeight = "bold";
        myDiv.style.zIndex = "500";
        myDiv.style.color = "white";
        myDiv.style.fontFamily = "Arial";
        myDiv.innerHTML = strHTML;
        map.getOverlayContainer().appendChild(myDiv);
        this.mapObj = map;
        this.divObj = myDiv;
        this.redraw();
    }

    function remove()
    {
    	if(this.divObj.parentNode != null){
    		this.divObj.parentNode.removeChild(this.divObj);
    	}
    }

    function copy()
    {
        return new MyRouteOverlay(this.latlng);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        this.divObj.style.left = (pt.x - 12) + "px";
        this.divObj.style.top = (pt.y - 33) + "px";
    }

    function vType()
    {
        return 'MyRouteOverlay';
    }
}
//customize OverlayCustomer cho module hien~ thi lo trinh
function MyRouteOverlayCustomer(latlng,strHTML){
    this.latlng = latlng;
    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;

    function initialize(map)
    {
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyRouteOverlay";
        myDiv.style.position = "absolute";
        myDiv.style.fontSize = "9";
        myDiv.style.fontWeight = "bold";
        myDiv.style.zIndex = "500";
        myDiv.style.color = "black";
        myDiv.style.fontFamily = "Arial";
        myDiv.innerHTML = strHTML;
        map.getOverlayContainer().appendChild(myDiv);
        this.mapObj = map;
        this.divObj = myDiv;
        this.redraw();
    }

    function remove()
    {
    	if(this.divObj.parentNode != null){
    		this.divObj.parentNode.removeChild(this.divObj);
    	}
    }

    function copy()
    {
        return new MyRouteOverlay(this.latlng);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        this.divObj.style.left = (pt.x - 4) + "px";
        this.divObj.style.top = (pt.y - 19) + "px";
    }

    function vType()
    {
        return 'MyRouteOverlay';
    }
}
//customize Overlay cho module hien~ thi lo trinh
function MyDistanceOverlay(latlng,strHTML){
    this.latlng = latlng;
    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;

    function initialize(map)
    {
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyDistanceOverlay";
        myDiv.style.position = "absolute";
        myDiv.style.fontWeight = "bold";
        myDiv.style.zIndex = "1000";
        myDiv.style.color = "red";
        myDiv.style.fontFamily = "Arial";
        myDiv.innerHTML = strHTML;
        map.getOverlayContainer().appendChild(myDiv);
        this.mapObj = map;
        this.divObj = myDiv;
        this.redraw();
    }

    function remove()
    {
    	if(this.divObj.parentNode != null){
    		this.divObj.parentNode.removeChild(this.divObj);
    	}
    }

    function copy()
    {
        return new MyRouteOverlay(this.latlng);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        this.divObj.style.left = (pt.x - 10) + "px";
        this.divObj.style.top = (pt.y + 5) + "px";
    }

    function vType()
    {
        return 'MyDistanceOverlay';
    }
}

function TBHVMapInfo(staffCode,staffName,shopCode,GSNPPStr,lastPosTime,accuracy,GSNPPArr,lat,lng,imgUrl){
	this.staffCode = staffCode;
	this.staffName = staffName;
	this.shopCode = shopCode;
	this.GSNPPStr = GSNPPStr;
	this.lastPosTime = lastPosTime;
	this.accuracy = accuracy;
	this.GSNPPArr = GSNPPArr;
	this.lat = lat;
	this.lng = lng;
	this.imgUrl = imgUrl;
};

function GSNPPMapInfo(staffCode,staffName,shopCode,NVBHStr,lastPosTime,accuracy,lat,lng,imgUrl){
	this.staffCode = staffCode;
	this.staffName = staffName;
	this.shopCode = shopCode;
	this.NVBHStr = NVBHStr;
	this.lastPosTime = lastPosTime;
	this.accuracy = accuracy;
	this.lat = lat;
	this.lng = lng;
	this.imgUrl = imgUrl;
};

function NVBHMapInfo(staffCode,staffName,shopCode,revenue,channelPoint,revenuePoint,lastPosTime,accuracy,lat,lng,imgUrl){
	this.staffCode = staffCode;
	this.staffName = staffName;
	this.shopCode = shopCode;
	this.revenue = revenue;
	this.channelPoint = channelPoint;
	this.revenuePoint = revenuePoint;
	this.lastPosTime = lastPosTime;
	this.accuracy = accuracy;
	this.lat = lat;
	this.lng = lng;
	this.imgUrl = imgUrl;
};

function CustomerInfo(customerCode,customerName,lat,lng,imgUrl){
	this.customerCode = shopCode;
	this.customerName = shopName;
	this.lat = lat;
	this.lng = lng;
	this.imgUrl = imgUrl;
};

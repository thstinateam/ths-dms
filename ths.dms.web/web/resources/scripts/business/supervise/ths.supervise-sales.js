var SuperviseSales  = {
	_lstStaffPosition:null,
	_lstStaffException:null,
	_lstCust:null,
	_lstStaffWithTrainingPlan:null,
	_idStaffSelect:null,
	_idStaffSelected:null,
	_idShopSelect: null,
	_itv:null,
	_isSearch:null,
	_isLHL:null,
	_notFitOverlay:null,
	_isTheFirstLoad:true,
	_isNeedToReload:true,
	customerViewer: new Array(),
	selectedStaffId : null,
	_nodeType: {SHOP: 1, STAFF: 2},
	mapObject : null,
	_EXTENTION_VALUE: 2, // vuongMQ, lay convert Math.round, 2 gia tri cuoi thap phan
	_NGAY: 1, // vuongMQ, gia tri ngay
	_LUY_KE: 2, // vuongMQ, gia tri luy ke
	listPointViewer : new Array(),
	listPathView: new Array(),
	_childStaffMap: new Map(),//key is shop_id, value is list all child staff of shop
	_childNodeMap: new Map(),//key is shop_id, value is list all child node
	_amountType: {STAFF: '1', ROUTE: '2', SHOP: '3'},
	stText: "staff",
	sysDateFromDB: null,
	numPointLine: 0,
	calcRoute:function(start,end) {
		var directionsService = new viettel.RoutingService();
		var startPoint = new viettel.LatLng(start.lat, start.lng);
		var mapOptions = {
				zoom:12,
				center: startPoint
		};
		ViettelMap._map.setOptions(mapOptions);
		var directionsDisplay = new viettel.RoutingRender({
			map:ViettelMap._map,
			hideMarker : true
		});
		var endPoint = new viettel.LatLng(end.lat, end.lng);
		var request = {
				origin: startPoint,
				destination: endPoint,
				travelMode: viettel.TravelMode.MOTOR
		};
		directionsService.route(request, function(response, status) {
			if (status == viettel.RoutingStatus.OK) {
				directionsDisplay.setRoutingRequest(request);
			}
		});
		SuperviseSales._mapObject.push(directionsDisplay);
	},
	showContextMenu : function(t, type, staffId, code, name, staffOwnerId) {
		if (staffId != null && staffId != 'null') {
			//var temp = SuperviseSales._lstStaffPosition.get(staffId + '-' + staffOwnerId);
			var temp = SuperviseSales._lstStaffPosition.get(staffId);
			if (temp != null) {
				if (temp.roleType > StaffSpecType.RSM) { // > 6; roleType GSM chua dinh ngia
					/*$('#cmDoanhSoNgay').attr('onclick','SuperviseSales.showDialogDaySaleGDM(this,'+staffId+',\''+
							Utils.XSSEncode(temp.staffCode)+'\',\''+Utils.XSSEncode(temp.staffName)+'\')').show();
					$('#cmLuyKeThang').attr('onclick','SuperviseSales.showDialogMonthAmountPlan(this,'+type+','+staffId+')').show();
					$('#cmXemLoTrinh').hide();*/
				} else if (temp.roleType > StaffSpecType.RSM) { // > 6; roleType TBHV chua dinh ngia
					/*$('#cmDoanhSoNgay').attr('onclick','SuperviseSales.showDialogDaySaleTBHV(this,'+staffId+',\''+
							Utils.XSSEncode(temp.staffCode)+'\',\''+Utils.XSSEncode(temp.staffName)+'\')').show();
					$('#cmLuyKeThang').attr('onclick','SuperviseSales.showDialogMonthAmountPlan(this,'+type+','+staffId+')').show();
					$('#cmXemLoTrinh').hide();*/
				} else if (temp.roleType == StaffSpecType.SUPERVISOR) { // NVGS
					/*$('#cmDoanhSoNgay').attr('onclick','SuperviseSales.showDialogDaySaleNVGS(this,'+staffId+',\''+
							Utils.XSSEncode(temp.staffCode)+'\',\''+Utils.XSSEncode(temp.staffName)+'\')').show();
					$('#cmLuyKeThang').attr('onclick','SuperviseSales.showDialogMonthAmountPlan(this,'+type+','+staffId+')').show();*/
					$('#cmDoanhSoNgay').attr('onclick','SuperviseSales.showDialogDaySaleNVGS(this,' + staffId + ',\'' + Utils.XSSEncode(temp.staffCode) + '\',\'' + Utils.XSSEncode(temp.staffName) + '\',\'' + SuperviseSales._NGAY + '\');').show();
					$('#cmLuyKeThang').attr('onclick','SuperviseSales.showDialogDaySaleNVGS(this,' + staffId + ',\'' + Utils.XSSEncode(temp.staffCode) + '\',\'' + Utils.XSSEncode(temp.staffName) + '\',\'' + SuperviseSales._LUY_KE + '\');').show();
					$('#cmXemLoTrinh').hide();
				} else if (temp.roleType == StaffSpecType.STAFF) { // NVBH
					/*$('#cmDoanhSoNgay').attr('onclick','SuperviseSales.showDialogDaySaleNVBH(this,'+staffId+',\''+
							Utils.XSSEncode(temp.staffCode)+'\',\''+Utils.XSSEncode(temp.staffName)+'\')').show();*/
					$('#cmDoanhSoNgay').attr('onclick', 'SuperviseSales.showDialogDaySaleNVBH(this,' + staffId + ',\'' + Utils.XSSEncode(temp.staffCode) + '\',\'' + Utils.XSSEncode(temp.staffName) + '\',\'' + SuperviseSales._NGAY + '\');').show();
					$('#cmLuyKeThang').attr('onclick', 'SuperviseSales.showDialogDaySaleNVBH(this,' + staffId + ',\'' + Utils.XSSEncode(temp.staffCode) + '\',\'' + Utils.XSSEncode(temp.staffName) + '\',\'' + SuperviseSales._LUY_KE + '\');').show();
					$('#cmXemLoTrinh').attr('onclick', 'SuperviseSales.showListCustomerByVisitPlan(' + staffId + ',\'' + Utils.XSSEncode(name) + '\',' + temp.roleType + ');').show();
				}
				$('#cmDoanhSoNgay').attr('parentId',staffOwnerId);
				$('#cmLuyKeThang').attr('parentId',staffOwnerId);
				$('#cmXemLoTrinh').attr('parentId',staffOwnerId);
				$('#contextMenu').menu('show', {  
					  left: $(t).offset().left+47,  
					  top: $(t).offset().top  
				});
			}
		}
	},
	
	/**
	 * Mui ten view thong tin san luong, doanh so cua shop (all shop != NPP)
	 * @author vuongmq
	 * @param t: this
	 * @param type: cua shop la null
	 * @param shopId
	 * @param code
	 * @param name
	 * @since 21/09/2015
	 */
	showContextMenuShop: function(t, type, shopId, code, name) {
		$('#cmDoanhSoNgay').attr('onclick','SuperviseSales.showDialogDaySaleShop(this,' + shopId + ',\'' + Utils.XSSEncode(code) + '\',\'' + Utils.XSSEncode(name) + '\',\'' + SuperviseSales._NGAY + '\');').show();
		$('#cmLuyKeThang').attr('onclick','SuperviseSales.showDialogDaySaleShop(this,' + shopId + ',\'' + Utils.XSSEncode(code) + '\',\'' + Utils.XSSEncode(name) + '\',\'' + SuperviseSales._LUY_KE + '\');').show();
		$('#cmXemLoTrinh').hide();
		$('#contextMenu').menu('show', {  
			  left: $(t).offset().left+47,  
			  top: $(t).offset().top  
		});
	},
	
	getStaffName: function(r){
		var maxlength = 31;
		var str = "";
		if (r.attr.roleType == StaffSpecType.STAFF) {
//			str = '(NVBH) ' + r.attr.text;
			str = r.attr.text;
		} else if(r.attr.roleType == StaffSpecType.SUPERVISOR) {
//			str = '(GS) ' + r.attr.text;
			str = r.attr.text;
		} else if(r.attr.roleType == StaffSpecType.MANAGER) {
//			str = '(QL) ' + r.attr.text;
			str = r.attr.text;
		}
		var span = '<span title="' + Utils.XSSEncode(str) + '">';
		if (str.length > maxlength) {
			return span + Utils.XSSEncode(str.substring(0,maxlength)) + '...</span>';
		}
		return span + Utils.XSSEncode(str) + '</span>';
		/*var maxlength = 31;
		if(r.attr.roleType==8) maxlength=37;//VNM
		else if(r.attr.roleType==6) maxlength=35;//GDM
		else if(r.attr.roleType==7) maxlength=33;//TBHV
		else if(r.attr.roleType==5) maxlength=31;//NVGS
		var span='<span title="'+Utils.XSSEncode(r.attr.staffName)+'">';
		if(r.attr.staffName==undefined || r.attr.staffName==null){
			r.attr.staffName='';
		}
		if(r.attr.staffName.length > maxlength){
			return span+Utils.XSSEncode(r.attr.staffName.substring(0,maxlength-3))+'...</span>';
		}
		return span+Utils.XSSEncode(r.attr.staffName)+'</span>';*/
	},
	reloadTreeStaff: function(){
		$('#btCapNhatViTri').html('<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15"/>');
		$('#btCapNhatViTri').attr('disabled','disabled');
		SuperviseSales._notFitOverlay=1;
		SuperviseSales._idStaffSelected = SuperviseSales._idStaffSelect;
		SuperviseSales.getListStaffForShop();
	},
	loadTreeStaff:function(isSearch){
		if (isSearch != undefined && isSearch != null && isSearch == 1) {
			SuperviseSales._isSearch = 1;
			$('#treeGrid').treegrid('reload');
		} else {
			$('#treeGrid').treegrid({  
			    url:  '/supervise/sales/list-staff-for-shop-one-node',
		        height:'auto',  
		        idField: 'id',  
		        treeField: 'text',
		        width:500,
		        //scrollbarSize:0,
		        lines:true,
		        animate:true,
		        fitColumns:true,
			    columns:[[  
			        {field:'text',title:'Đơn vị - Nhân viên',width:300,formatter:function(v,r,i){
			        	var str ='';
			        	/*if(r.attr.staffOwnerId != undefined && r.attr.staffOwnerId!=null && r.attr.staffOwnerId==-1){//user đăng nhập
			        		str='<img class="refreshIcon" src="/resources/images/icon_user.png" height="15"/>';
			        	}else{
				        	if(SuperviseSales._lstNodeExcep.get(r.id)!=null){
				        		str='<input type="checkbox" onclick="SuperviseSales.onClickCheckboxNode(this);" value="'+r.id+'" id="cb'+r.id+'">';
				        	}else{
				        		str='<input checked="checked" type="checkbox" onclick="SuperviseSales.onClickCheckboxNode(this);" value="'+r.id+'" id="cb'+r.id+'">';
				        	}
				        	if(r.attr.roleType==1 || r.attr.roleType==2) str='';//nvbh ko hiện checkbox
				        	if(r.attr.roleType!=null && (r.attr.roleType==7 || r.attr.roleType==5 || r.attr.roleType==1 || r.attr.roleType==2) && r.attr.hhmm!=null){//TBHV,GSNPP,NVBH có tọa độ
			        			return str+'<a href="javascript:void(0)" onclick="SuperviseSales.moveToStaff(\''+r.id+'\');">'+SuperviseSales.getStaffName(r)+'</a>';
			        		}
			        	}*/	
			        	if (r.root) {//node root
//			        		str = '<img class="refreshIcon" src="/resources/images/icon_user.png" height="15"/>';
			        		if (!Utils.isEmpty(r.iconCls)) {
			        			str += '<img src="' + r.iconCls + '" width="15px" height="15" style="margin: 0px 5px 5px;" >'; //class="refreshIcon"
			        		}
			        	} else {
			        		if (r.nodeType == SuperviseSales._nodeType.STAFF) {
			        			if (r.checked != null && r.checked > 0) {
			        				str='<input checked="checked" type="checkbox" onclick="SuperviseSales.onClickCheckboxNode(this);" class="cb_'+r.parentID+'" value="'+r.id+'" id="staff-cb'+r.id+'">';
			        			} else {
			        				str='<input type="checkbox" onclick="SuperviseSales.onClickCheckboxNode(this);" class="cb_'+r.parentID+'" value="'+r.id+'" id="staff-cb'+r.id+'">';
			        			}
			        			
			        		} else {//SHOP
			        			if (r.checked != null && r.checked > 0) {
			        				str='<input checked="checked" type="checkbox" onclick="SuperviseSales.onClickCheckboxNode(this);" class="cb_'+r.parentID+'" value="'+r.id+'" id="cb'+r.id+'">';
			        			} else {
			        				str='<input type="checkbox" onclick="SuperviseSales.onClickCheckboxNode(this);" class="cb_'+r.parentID+'" value="'+r.id+'" id="cb'+r.id+'">';
			        			}
			        		}
			        		if (!Utils.isEmpty(r.iconCls)) {
			        			str += '<img src="' + r.iconCls + '" width="15px" height="15" style="margin: 0px 5px 5px;" >'; //class="refreshIcon"
			        		}
				        	if(!Utils.isEmpty(r.attr.hhmm)){//TBHV,GSNPP,NVBH có tọa độ
//				        			return str+'<a href="javascript:void(0)" onclick="SuperviseSales.moveToStaff(\''+r.id+'\');">'+SuperviseSales.getStaffName(r)+'</a>';
				        		return str + '<a href="javascript:void(0)" onclick="SuperviseSales.moveToStaff('+r.id+');">'+SuperviseSales.getStaffName(r)+'</a>';
			        		}
			        	}
			        	return str + Utils.XSSEncode(r.text);;
			        	
//			        	return Utils.XSSEncode(r.text);
			        }},
//			        {field:'shopCode',title:'Đơn vị',width:60,align:'left',formatter:function(v,r,i){
//		        		return Utils.XSSEncode(r.attr.shopCode);
//		        	}},
			        {field:'update',title:'Cập nhật',width:60,align:'center',formatter:function(v,r,i){
			        		/*if(r.attr.roleType!=null && (r.attr.roleType==1 || r.attr.roleType==2 || r.attr.roleType==5 || r.attr.roleType==7)){//TBHV,GSNPP,NVBH
			        			if(r.attr.hhmm!=null) return Utils.XSSEncode(r.attr.hhmm);
			        			else return '<span style="color:red">N/A</span>';
			        		}*/
				        	if (r.nodeType == SuperviseSales._nodeType.STAFF) {
				        		if (Utils.isEmpty(r.attr.hhmm)) {
				        			return '<span style="color:red">N/A</span>';
				        		} else {
				        			return Utils.XSSEncode(r.attr.hhmm);
				        		}
				        	}
			        		return '';
			        	}
			        },
			        {field:'edit',title:'',width:30,align:'left',formatter:function(v,r,i){
			        	/*if(r.attr.roleType==8) return '';
			        	var str= '<a href="javascript:void(0)" onclick="SuperviseSales.showContextMenu(this,'+r.attr.roleType+','+r.attr.staffId+',\''+
			        		Utils.XSSEncode(r.attr.staffCode)+'\',\''+Utils.XSSEncode(r.attr.staffName)+'\','+r.attr.staffOwnerId+');">';
			        	str+='<img src="/resources/images/icon_2.png",width:15, height="12"/></a>';
			        	return str;*/
			        	
			        	/**vuongmq; 27/08/2015; lay them menu load doanh so cho loai nodeType = 2*/
			        	if (r.nodeType == SuperviseSales._nodeType.SHOP) {
			        		if (r.attr.roleType == null || r.attr.roleType != SuperviseSales._nodeType.SHOP) { // khac NPP thi cho menu
			        			var str= '<a href="javascript:void(0)" onclick="SuperviseSales.showContextMenuShop(this,' + r.attr.roleType + ',' + r.attr.shopId + ',\'' + Utils.XSSEncode(r.attr.shopCode) + '\',\'' + Utils.XSSEncode(r.attr.shopName) + '\');">';
					        	str += '<img src="/resources/images/icon_2.png",width:15, height="12"/></a>';
					        	return str;
				        	}
			        	} else {
			        		if (r.attr.roleType == null || r.attr.roleType == StaffSpecType.VIETTEL_ADMIN) {
				        		return '';
				        	} else if (r.attr.roleType == StaffSpecType.SUPERVISOR || r.attr.roleType == StaffSpecType.STAFF) {
				        		// hien tai chi lay hai roleType nay
					        	var str= '<a href="javascript:void(0)" onclick="SuperviseSales.showContextMenu(this,' + r.attr.roleType + ',' + r.attr.staffId + ',\'' + Utils.XSSEncode(r.attr.staffCode) + '\',\'' + Utils.XSSEncode(r.attr.staffName) + '\',' + r.attr.staffOwnerId + ');">';
					        	str += '<img src="/resources/images/icon_2.png",width:15, height="12"/></a>';
					        	return str;
				        	}
			        	}
			        }}
			    ]],
			    rowStyler: function(r){
					if (r.attr != null && r.attr.isBold){
						return 'background:none repeat scroll 0 0 #FBEC88';
					}
				},
				onCollapse:function(r){
//					$('[node-id='+r.attr.staffId+']').next().remove();
					$('[node-id='+r.id+']').next().remove();	
				},
			    onBeforeLoad: function(n,p){
			    	if(p != undefined && p != null){
			    		$('.highlight').css('font-weight','normal');
//				    	p.lhl= !$('#cbLHL').is(':disabled') && $('#cbLHL').is(':checked')?1:0;
				    	if(SuperviseSales._isLHL==undefined || SuperviseSales._isLHL==null || SuperviseSales._isLHL!=1)
//				    		p.staffId = p.id;
				    		p.nodeId = p.id;
				    	else SuperviseSales._isLHL=null;
				    	/*if(($('#shopCode').val().trim()!='' || $('#shopName').val().trim()!='') && SuperviseSales._isSearch!=undefined && SuperviseSales._isSearch!=null && SuperviseSales._isSearch==1){
				    		p.shopCode=$('#shopCode').val().trim();
				    		p.shopName=$('#shopName').val().trim();
				    	}*/
				    	SuperviseSales._isSearch=null;
				    	
				    	//add param check
				    	if (n != null) {
				    		if ($('#cb' + n.id).length > 0 && $('#cb' + n.id).is(':checked')) {
					    		p.checked = 1;
					    	} else {
					    		p.checked = 0;
					    	}
				    	}
			    	}
			    },
			    onLoadSuccess: function(row, data){
			    	if (data != null && data.length > 0) {
			    		if (row == null) {//first load
			    			var node = data[0];
			    			SuperviseSales._childNodeMap.put(node.id, node.children);
			    			SuperviseSales._childStaffMap.put(node.id, node.lstAllStaff);
			    			
			    			if (node.children != null && node.children.length > 0) {
			    				for (var i = 0, n = node.children.length; i < n; i++) {
			    					var childNode = node.children[i];
			    					SuperviseSales._childStaffMap.put(childNode.id, childNode.lstAllStaff);
//			    					SuperviseSales._childNodeMap.put(childNode.id, childNode.children);
			    				}
			    			}
			    		} else {
			    			SuperviseSales._childNodeMap.put(row.id, data);
			    			for (var i = 0, n = data.length; i < n; i++) {
		    					var childNode = data[i];
		    					SuperviseSales._childStaffMap.put(childNode.id, childNode.lstAllStaff);
		    				}
//			    			SuperviseSales._childStaffMap.put(row.id, row.lstAllStaff);
			    			
			    		}
//			    		console.log("_childNodeMap Key: " + SuperviseSales._childNodeMap.keyArray.length);
//			    		console.log("_childStaffMap Key: " + SuperviseSales._childStaffMap.keyArray.length);
			    	}
			    	$('#promotionShopGrid .datagrid-view').css('max-height', '356px');
			    	$('#promotionShopGrid .datagrid-view').css('overflow-y', 'scroll');
			    }
			});
		}
	},
	checkAllChildNode: function(noteId) {
		var lstNote = SuperviseSales._childNodeMap.get(noteId);
		if (lstNote == null || lstNote.length == 0) {
			return;
		}
		/*if (lstNote != null && lstNote.length > 0) {
			for (k = 0, size = lstNote.length; k < size; k++) {
				var aNode = lstNote[k];
				if (aNode.nodeType == SuperviseSales._nodeType.SHOP) {
					$('#cb' + aNode.id).attr('checked', "checked");
				} else if (aNode.nodeType == SuperviseSales._nodeType.STAFF) {
					$('#staff-cb' + aNode.id).attr('checked', "checked");
				}
//				arguments.call(aNode.id);
				SuperviseSales.checkAllChildNode(aNode.id);
			}
		}*/
		SuperviseSales.checkAllListNode(lstNote);
	},
	checkAllListNode: function(lstNote) {
		if (lstNote == null || lstNote.length == 0) {
			return;
		}
		if (lstNote != null && lstNote.length > 0) {
			var lstChildNode = [];
			for (k = 0, size = lstNote.length; k < size; k++) {
				var aNode = lstNote[k];
				lstChildNode = SuperviseSales.addLstChildNode(aNode, lstChildNode);
				if (aNode.nodeType == SuperviseSales._nodeType.SHOP) {
					$('#cb' + aNode.id).attr('checked', "checked");
				} else if (aNode.nodeType == SuperviseSales._nodeType.STAFF) {
					$('#staff-cb' + aNode.id).attr('checked', "checked");
				}
//				SuperviseSales.checkAllChildNode(aNode.id);
			}
			SuperviseSales.checkAllListNode(lstChildNode);
		}
	},
	uncheckAllChildNode: function(noteId) {
		var lstNote = SuperviseSales._childNodeMap.get(noteId);
		if (lstNote == null || lstNote.length == 0) {
			return;
		}
		/*if (lstNote != null && lstNote.length > 0) {
			for (k = 0, size = lstNote.length; k < size; k++) {
				var aNode = lstNote[k];
				if (aNode.nodeType == SuperviseSales._nodeType.SHOP) {
					$('#cb' + aNode.id).removeAttr('checked')
				} else if (aNode.nodeType == SuperviseSales._nodeType.STAFF) {
					$('#staff-cb' + aNode.id).removeAttr('checked')
				}
				SuperviseSales.uncheckAllChildNode(aNode.id);
//				arguments.call(aNode.id);
			}
		}*/
		SuperviseSales.unCheckListNode(lstNote);
	},
	unCheckListNode: function(lstNote) {
		if (lstNote == null || lstNote.length == 0) {
			return;
		}
		if (lstNote != null && lstNote.length > 0) {
			var lstChildNode = [];
			for (k = 0, size = lstNote.length; k < size; k++) {
				var aNode = lstNote[k];
				lstChildNode = SuperviseSales.addLstChildNode(aNode, lstChildNode);
				if (aNode.nodeType == SuperviseSales._nodeType.SHOP) {
					$('#cb' + aNode.id).removeAttr('checked')
				} else if (aNode.nodeType == SuperviseSales._nodeType.STAFF) {
					$('#staff-cb' + aNode.id).removeAttr('checked')
				}
//				SuperviseSales.uncheckAllChildNode(aNode.id);
			}
			SuperviseSales.unCheckListNode(lstChildNode);
		}
	},
	addLstChildNode: function(aNode, lstChildNode) {
		if (SuperviseSales._childNodeMap.get(aNode.id) != null) {
			var lstNode = SuperviseSales._childNodeMap.get(aNode.id);
			if (lstNode.length > 0) {
				for (var m = 0, msize = lstNode.length; m < msize; m++) {
					lstChildNode.push(lstNode[m]);
				}
			}
		}
		return lstChildNode
	},
	
	onClickCheckboxNode:function(t, back){
		var nodeType = SuperviseSales._nodeType.SHOP;
		if ($(t).attr('id').indexOf(SuperviseSales.stText) >= 0) {
			nodeType = SuperviseSales._nodeType.STAFF;
		}
		var id = $(t).attr('value');
		if (isNaN(id)) {
			return;
		} else {
			id = Number(id);
		}
		var classParent = $(t).attr('class');
		var parentID  = [];
		parentID  = classParent.split('_');
		if (nodeType == SuperviseSales._nodeType.SHOP) {
			var lstChildStaff = SuperviseSales._childStaffMap.get(id);
			if ($(t).is(':checked')) {//xóa khỏi list excep
				if (lstChildStaff != null && lstChildStaff.length > 0) {
					for (var i = 0, n = lstChildStaff.length; i < n; i++) {
						SuperviseSales.handleCheckStaff(lstChildStaff[i]);
					}
				}
				var fullCheckChildren = true;
				$('.'+classParent).each(function(i,e){
					if($(e).is(':checked')) {
					} else {
						fullCheckChildren = false;
					}
				});
				if(fullCheckChildren) {
					$('#cb' + parentID[1]).attr('checked','checked');
				}
				//check all child notes
				SuperviseSales.checkAllChildNode(id);//tam thoi comment do stop script
				
			} else {
				if (lstChildStaff != null && lstChildStaff.length > 0) {
					for (var i = 0, n = lstChildStaff.length; i < n; i++) {
						SuperviseSales.handleUncheckStaff(lstChildStaff[i]);
					}
				}
				//uncheck all child notes
				if(!back) {
					SuperviseSales.uncheckAllChildNode(id);//tam thoi comment do stop script
				}
				$('#cb' + parentID[1]).removeAttr('checked');
			}
			SuperviseSales.onClickCheckboxNode('#cb' + parentID[1], true);
		} else if (nodeType == SuperviseSales._nodeType.STAFF) {
			var lengthExcept = SuperviseSales._lstNodeExcep.keyArray.length;
			var temp = SuperviseSales._lstStaffPosition.get(id);
			if(temp == null) return true;
		//	if(temp.staffOwnerId!=null) $('#cb'+temp.staffOwnerId).removeAttr('checked');
			if ($(t).is(':checked')) {//xóa khỏi list excep
				var fullCheckChildren = true;
				$('.'+classParent).each(function(i,e){
					if($(e).is(':checked')) {
					} else {
						fullCheckChildren = false;
					}
				});
				if(fullCheckChildren) {
					$('#cb' + parentID[1]).attr('checked','checked');
				}
				SuperviseSales.handleCheckStaff(temp);
			} else {
				SuperviseSales.handleUncheckStaff(temp);
				$('#cb' + parentID[1]).removeAttr('checked');
			}
			SuperviseSales.onClickCheckboxNode('#cb' + parentID[1], true);
		}
		
//		if(lengthExcept!=SuperviseSales._lstNodeExcep.keyArray.length) {
//			SuperviseSales.reloadMarker();
//			Vietbando._map.fitOverlays();
//		}
	},
	handleCheckStaff: function(node) {
		SuperviseSales._lstNodeExcep.remove(node.id);
		if($('#marker'+node.id).length != 0) {
			$('#marker'+node.id).parent().parent().show();
		}
//		SuperviseSales.removeListException(temp.id,temp.staffId);//trungtm6 comment
	},
	handleUncheckStaff: function(node) {
		/*var temp = SuperviseSales._lstStaffPosition.get(id);
		if(SuperviseSales._idStaffSelect == id || SuperviseSales._idStaffSelected == id) {
			SuperviseSales._idStaffSelected = null;
			SuperviseSales._idStaffSelect = null;
		}*/
		if(SuperviseSales._idStaffSelect == node.id || SuperviseSales._idStaffSelected == node.id) {
			SuperviseSales._idStaffSelected = null;
			SuperviseSales._idStaffSelect = null;
		}
		if(node != null) {
			SuperviseSales._lstNodeExcep.put(node.id,node);
			if($('#marker'+node.id).length != 0) {
				$('#marker'+node.id).parent().parent().hide();
			}
//			SuperviseSales.putListException(temp.id,temp.staffId);//trungtm6 comment
		}
	},
	
	putListException:function(id,staffOwnerId) {
		for(var i=0;i<SuperviseSales._lstStaffPosition.valArray.length;i++){
			var temp=SuperviseSales._lstStaffPosition.valArray[i];
			if(temp != null && temp.staffOwnerId == staffOwnerId){
				SuperviseSales._lstNodeExcep.put(temp.id,temp);
				$('#cb'+temp.id).removeAttr('checked');
				//if($('.overlay'+temp.staffId).length!=0) $('.overlay'+temp.staffId).hide();
				if($('#marker'+temp.id).length != 0) {
					$('#marker'+temp.id).parent().parent().hide();
				}
				if(temp.roleType > 5){//1->5 
					SuperviseSales.putListException(temp.id,temp.staffId);
				}
			}
		}
	},
	removeListException:function(id,staffOwnerId){
		for(var i=0;i<SuperviseSales._lstStaffPosition.valArray.length;i++){
			var temp=SuperviseSales._lstStaffPosition.valArray[i];
			if(temp!=null && temp.staffId != temp.staffOwnerId &&  temp.staffOwnerId==staffOwnerId){
				SuperviseSales._lstNodeExcep.remove(temp.id);
				$('#cb'+temp.id).attr('checked','checked');
				//if($('.overlay'+temp.staffId).length!=0) $('.overlay'+temp.staffId).show();
				if($('#marker'+temp.id).length != 0) {
					$('#marker'+temp.id).parent().parent().show();
				}
				if(temp.roleType>5){//1->5
					SuperviseSales.removeListException(temp.id,temp.staffId);
				}
			}
		}
	},
	getListStaffForShop:function(){
		try{
			var data = new Object();
			var shopIdSearch = $('#shopId').val().trim();
			data.shopIdSearch = shopIdSearch;
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			Utils.getHtmlDataByAjaxNotOverlay(data, '/supervise/sales/list-staff-for-shop',function(result) {
				if(!result.error){
					SuperviseSales._lstStaffPosition=new Map();
					var data = JSON.parse(result);
					var lstStaff = data.lstStaff;
					var lstParentStaff = data.lstParentStaff;
					var nSt = 0;
					if (lstStaff != null) {
						nSt = lstStaff.length;
					}
					if(lstStaff != null && nSt > 0){ 
						for(var i = 0; i < nSt; i++){
							var temp=lstStaff[i];
							temp.createTime=lstStaff[i].hhmm;//toTimeString(new Date(lstStaff[i].createTime));
							temp.countVisit=0;
							temp.staffOwnerId=lstStaff[i].staffOwnerId;
							if(parseInt(temp.roleType) == StaffSpecType.MANAGER){//TBHV
								temp.image="/resources/images/Mappin/blue.png";
							}else if(parseInt(temp.roleType) == StaffSpecType.SUPERVISOR){//GSNPP
								temp.image="/resources/images/Mappin/green.png";
							}else if(parseInt(temp.roleType) == StaffSpecType.STAFF){//NVBH
								temp.image="/resources/images/Mappin/red.png";
							}
							temp.id = temp.staffId;
							SuperviseSales._lstStaffPosition.put(temp.id, temp);
							
							SuperviseSales.reloadMarker();//trungtm6
							/*if(temp.lstStaffOwnerIdStr != null){
								var str=temp.lstStaffOwnerIdStr.split(',');
								if(str.length>1){
									for(var j =0;j< str.length;j++){
										var node=jQuery.extend(true, {}, temp);
										node.staffOwnerId=str[j];
										node.id=node.staffId+'-'+str[j];
										SuperviseSales._lstStaffPosition.put(node.id,node);
									}
								}else{
									temp.staffOwnerId=temp.lstStaffOwnerIdStr;
//									temp.id=temp.staffId+'-'+temp.staffOwnerId;
									temp.id = temp.staffId;
									SuperviseSales._lstStaffPosition.put(temp.id,temp);
								}
							} else if (parseInt(temp.roleType) == StaffSpecType.SUPERVISOR) {//GSNPP
								temp.staffOwnerId = temp.lstStaffOwnerIdStr;
//								temp.id = temp.staffId + '-' + '-1';
								temp.id = temp.staffId;
								SuperviseSales._lstStaffPosition.put(temp.id, temp);
							}*/
						}
					}
//					if(lstParentStaff!=null && lstParentStaff.length>0){
//						for(var i=0;i<lstParentStaff.length;i++){
//							SuperviseSales._lstParentStaff.put(lstParentStaff[i].parentStaffStr,1);
//						}
//					}
					
					//trungtm6 comment on 24/08/2015
//					SuperviseSales.getListStaffWithTrainingPlan(data.lstStaffTraining);
					$('#btCapNhatViTri').html(jsSaleSuperviseControlRefreshStaffPositionText);
					$('#btCapNhatViTri').removeAttr('disabled');
				}
			}, 'loading2', null);
		}catch(err){
			$('#loading2').hide();
		}
	},
	getListStaffWithTrainingPlan:function(lstStaff){
		if(lstStaff!=null && lstStaff.length>0){
			for(var i=0;i<lstStaff.length;i++){
				var temp = new Object();
				temp = lstStaff[i];
				for(var j=0,lengthStaff=SuperviseSales._lstStaffPosition.valArray.length;j<lengthStaff;j++){
					var staffTemp=SuperviseSales._lstStaffPosition.valArray[j];
					if(staffTemp!=null && temp.staffId==staffTemp.staffId){
						staffTemp.image="/resources/images/Mappin/red_star.png";
						staffTemp.countVisit=1;//co lick huan luyen trong ngay
					}
					if(staffTemp!=null && temp.staffOwnerId==staffTemp.staffId){
						staffTemp.countVisit=1;//co lick huan luyen trong ngay
					}
				}
			}
		}
		SuperviseSales.reloadMarker();
	},

	reloadMarker: function(){
		$('#xemLoTrinhDialog').hide();
		$('#btCapNhatViTri').html(jsSaleSuperviseControlRefreshStaffPositionText);
		$("#btCapNhatViTri").unbind( "click" );
		$('#btCapNhatViTri').bind('click', function() {
			SuperviseSales.reloadTreeStaff()
		}); 
		SuperviseSales.resetPathViewer();
		$('.NoteCustomersStatus').hide();
		try {
			if (SuperviseSales._itv != undefined && SuperviseSales._itv != null) {
				window.clearInterval(SuperviseSales._itv);
			}
		} catch(e) {}
		if (!$('#cbLHL').is(':disabled') && $('#cbLHL').is(':checked')) {
			SuperviseSales.filterListStaffForTrainingPlan();
		} else {
			SuperviseSales.filterListStaff(); //Add danh sách nhân viên vào _listMarker để load
		}
		ViettelMap.clearOverlays(); 
		if (ViettelMap._map != null) {
			ViettelMap.clearOverlays();
			if (ViettelMap._currentInfoWindow != undefined && ViettelMap._currentInfoWindow != null) {
				ViettelMap._currentInfoWindow.close();
			}
			ViettelMap.addMutilMarkerStaff();
		} else {
			var interval = window.setInterval(function() {
				if (ViettelMap._map != null) {
					ViettelMap.clearOverlays();
					if (ViettelMap._currentInfoWindow != undefined && ViettelMap._currentInfoWindow != null) {
						ViettelMap._currentInfoWindow.close();
					}
					ViettelMap.addMutilMarkerStaff();
					window.clearInterval(interval);
				}
			},500);
		}
		$('#divOverlay').hide();
	},
	filterListStaff:function(){
		ViettelMap._listMarker=new Map();
		var i = 0;
		if(SuperviseSales._lstStaffPosition!=null){
			if(!$('#cbNVBH').is(':disabled') && $('#cbNVBH').is(':checked')){
				for(i = 0; i < SuperviseSales._lstStaffPosition.valArray.length; i++){
					var temp = SuperviseSales._lstStaffPosition.valArray[i];
					if((temp.roleType == StaffSpecType.STAFF) && temp.lat!=null && temp.lng!=null){
						ViettelMap._listMarker.put(temp.id,temp);
					}
				}
			}
			if(!$('#cbGSNPP').is(':disabled') && $('#cbGSNPP').is(':checked')){
				for(i=0;i<SuperviseSales._lstStaffPosition.valArray.length;i++){
					var temp = SuperviseSales._lstStaffPosition.valArray[i];
					if(temp.roleType == StaffSpecType.SUPERVISOR && temp.lat!=null && temp.lng!=null){
						ViettelMap._listMarker.put(temp.id,temp);
					}
				}
			}
			if(!$('#cbTBHV').is(':disabled') && $('#cbTBHV').is(':checked')){
				for(i=0;i<SuperviseSales._lstStaffPosition.valArray.length;i++){
					var temp = SuperviseSales._lstStaffPosition.valArray[i];
					if(temp.roleType == StaffSpecType.MANAGER && temp.lat!=null && temp.lng!=null){
						ViettelMap._listMarker.put(temp.id,temp);
					}
				}
			}
		}
	},
	filterListStaffForTrainingPlan:function(){
		ViettelMap._listMarker=new Map();
		if(!$('#cbLHL').is(':disabled') && $('#cbLHL').is(':checked')){
			for(var i=0;i<SuperviseSales._lstStaffPosition.valArray.length;i++){
				var temp = SuperviseSales._lstStaffPosition.valArray[i];
				if(temp.lat!=null && temp.lng!=null 
						&& (temp.roleType==StaffRoleType.NVGS || temp.roleType==StaffRoleType.TBHV || temp.countVisit>0)){
					ViettelMap._listMarker.put(temp.id,temp);
				}
			}
		}
	},
	toggleListStaff:function(){
		if($('.StaffSelectBtmSection .OffStyle').length==1){
			$('#titleStaff').addClass('OnStyle');
			$('#titleStaff').removeClass('OffStyle');
			
		}else{
			$('#titleStaff').addClass('OffStyle');
			$('#titleStaff').removeClass('OnStyle');
		}
		$('#listStaff').toggle();
	},	
	moveToStaff: function(id){//di chuyen den marker cua nhan vien va hien thi info window
		ViettelMap._listMarker = new Map();
		if(id!= null && id!= undefined && SuperviseSales._lstStaffPosition != null && ViettelMap._listMarker != null){
//			
			var temp = ViettelMap._listMarker.get(id);
			if (temp != null) {
				if(ViettelMap.isValidLatLng(temp.lat, temp.lng)){
					$('.overlay'+temp.id).show();
					var pt = new viettel.LatLng(temp.lat, temp.lng);
					ViettelMap._map.setCenter(pt);
					ViettelMap._map.setZoom(14);
					if($('#marker'+temp.id).length!=0){ 
						$('#marker'+temp.id).parent().parent().show();
					}
					ViettelMap.showWindowInfo(temp.id, temp.lat, temp.lng, temp.accuracy, temp.createTime);
				}
				ViettelMap.addMarkerStaff(temp);
			} else {
				var temp = SuperviseSales._lstStaffPosition.get(id);
				if (temp != null) {
					ViettelMap._listMarker.put(temp.staffId, temp);
					if(temp!=null && ViettelMap.isValidLatLng(temp.lat, temp.lng)){
						if($('.overlay'+temp.id).length==0){
							ViettelMap.addMarkerStaff(temp);
						}
						var pt = new viettel.LatLng(temp.lat, temp.lng);
						ViettelMap._map.setCenter(pt);
						ViettelMap.showWindowInfo(temp.id, temp.lat, temp.lng, temp.accuracy, temp.createTime);
					}
				}
			}
		}
	},
	checkLatLngOnStaff:function(staffId){
		var temp = SuperviseSales._lstStaffPosition.get(staffId);
		if(temp!=null && temp.lat!=null && temp.lng!=null) return true;
		return false;
	},
	selectStaffOnUI:function(id,staffOwnerId){
		$('.easyui-dialog').dialog('close');
		SuperviseSales.moveToStaff(id+'-'+staffOwnerId);
	},
	showDialogDaySaleGDM:function(t,nvgsId, nvgsCode, nvgsName,refresh) {
		SuperviseSales._idStaffSelect=nvgsId;
		var parentId=$(t).attr('parentId');
		if(refresh==undefined || refresh==null){
			var title='<a parentId="'+parentId+'" style="position: absolute; right: 27px;" onclick="SuperviseSales.showDialogDaySaleGDM(this,'+nvgsId+',\''+
				Utils.XSSEncode(nvgsCode)+'\',\''+Utils.XSSEncode(nvgsName)+'\',1)">';
			title+='<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15"/></a>';
			$('#gdmSaleBody').html('');
			$('#gdmFooter').html('');
			$('#dialogDSNgayGDM').dialog({  
		        title: 'Doanh số bán hàng ngày: '+Utils.XSSEncode(nvgsCode)+' - ' +
		        	Utils.XSSEncode(nvgsName)+title,  
		        closed: false,  
		        cache: false,  
		        modal: true  
		    });
		}else{
			$('.panel-title .refreshIcon').attr('src','/resources/images/loading-small.gif');
		}
		$.getJSON('/supervise/sales/getDSNgayGDM?gdmId='+nvgsId, function(data) {
			if(SuperviseSales._idStaffSelect==nvgsId){
				$('.panel-title .refreshIcon').attr('src','/resources/images/icon_refreshnew.png');
				if(!data.error) {
					var lstNVBHSaleInfo = data.lstGDMSaleInfo;
					var htmlBody = '';
					var htmlFooter = '';
					var totalDHKH = 0;
					var totalDHTH = 0;
					var totalDSKH = 0;
					var totalDSTH = 0;
					var totalDSDuyet = 0;
					var totalDSLeft = 0;
					if($.isArray(lstNVBHSaleInfo)) {
						for(var i = 0; i < lstNVBHSaleInfo.length; i++) {
							var nvbhSaleInfo = lstNVBHSaleInfo[i];
							nvbhSaleInfo.dsKH=SuperviseSales.covertDSKH(nvbhSaleInfo.dsKH);
							htmlBody += '<tr>';
							htmlBody += '<td class="FirstTdStyle"><div class="AlignLCols">'+
								Utils.XSSEncode(nvbhSaleInfo.shopCode)+'</div></td>';
							htmlBody += '<td><div class="AlignLCols">'+
								Utils.XSSEncode(nvbhSaleInfo.staffCode)+'</div></td>';
							if(SuperviseSales.checkLatLngOnStaff(nvbhSaleInfo.staffId)){
								htmlBody += '<td><div class="AlignLCols"><a href="javascript:void(0);" onclick="return SuperviseSales.selectStaffOnUI('+
									nvbhSaleInfo.staffId+','+parentId+');">'+Utils.XSSEncode(nvbhSaleInfo.staffName)+'</a></div></td>';
							} else {
								htmlBody += '<td><div class="AlignLCols" >'+Utils.XSSEncode(nvbhSaleInfo.staffName)+'</div></td>';
							}
							htmlBody += '<td><div class="AlignCCols">'+(nvbhSaleInfo.dhKH == null ? 0 : nvbhSaleInfo.dhKH)+'</div></td>';
							htmlBody += '<td><div class="AlignCCols">'+(nvbhSaleInfo.dhTH == null ? 0 : nvbhSaleInfo.dhTH)+'</div></td>';
							htmlBody += '<td><div class="AlignRCols">'+formatCurrency(nvbhSaleInfo.dsKH)+'</div></td>';
							htmlBody += '<td><div class="AlignRCols">'+formatCurrency(nvbhSaleInfo.dsTH)+'</div></td>';
							htmlBody += '<td><div class="AlignRCols">'+formatCurrency(nvbhSaleInfo.dsDuyet)+'</div></td>';
							htmlBody += '<td><div class="AlignRCols">'+formatCurrency(nvbhSaleInfo.dsKH > nvbhSaleInfo.dsTH ? nvbhSaleInfo.dsKH - nvbhSaleInfo.dsTH : 0)+'</div></td>';
							htmlBody += '</tr>';
							totalDHKH += Number(nvbhSaleInfo.dhKH);
							totalDHTH += Number(nvbhSaleInfo.dhTH);
							totalDSKH += Number(nvbhSaleInfo.dsKH);
							totalDSDuyet += Number(nvbhSaleInfo.dsDuyet);
							totalDSTH += Number(nvbhSaleInfo.dsTH);
						}
						htmlFooter += '<tr>';
						htmlFooter += '<td colspan="3" class="FirstTdStyle"><div class="AlignRCols">Tổng cộng</div></td>';
						htmlFooter += '<td><div class="AlignCCols">'+totalDHKH+'</div></td>';
						htmlFooter += '<td><div class="AlignCCols">'+totalDHTH+'</div></td>';
						htmlFooter += '<td><div class="AlignRCols">'+formatCurrency(totalDSKH)+'</div></td>';
						htmlFooter += '<td><div class="AlignRCols">'+formatCurrency(totalDSTH)+'</div></td>';
						htmlFooter += '<td><div class="AlignRCols">'+formatCurrency(totalDSDuyet)+'</div></td>';
						htmlFooter += '<td><div class="AlignRCols">'+formatCurrency(totalDSKH > totalDSTH ? totalDSKH - totalDSTH : 0)+'</div></td>';
						htmlFooter += '</tr>';
						$('#gdmSaleBody').html(htmlBody);
						$('#gdmFooter').html(htmlFooter);
					}
				}
				
			}
		});
	},
	showDialogDaySaleTBHV:function(t,tbhvId, tbhvCode, tbhvName,refresh) {
		SuperviseSales._idStaffSelect=tbhvId;
		var parentId=$(t).attr('parentId');
		if(refresh==undefined || refresh==null){
			var title='<a style="position: absolute; right: 27px;" onclick="SuperviseSales.showDialogDaySaleTBHV(this,'+tbhvId+',\''+
				Utils.XSSEncode(tbhvCode)+'\',\''+Utils.XSSEncode(tbhvName)+'\',1)">';
			title+='<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15"/></a>';
			$('#tbhvSaleBody').html('');
			$('#tbhvSaleFooter').html('');
			$('#dialogDSNgayTBHV').dialog({  
		        title: 'Doanh số bán hàng ngày: '+Utils.XSSEncode(tbhvCode)+' - ' +Utils.XSSEncode(tbhvName)+title,  
		        closed: false,  
		        cache: false,  
		        modal: true  
		    }); 
		}else{
			$('.panel-title .refreshIcon').attr('src','/resources/images/loading-small.gif');
		}
		$.getJSON('/supervise/sales/getDSNgayTBHV?tbhvId='+tbhvId, function(data) {
			if(SuperviseSales._idStaffSelect==tbhvId){
				$('.panel-title .refreshIcon').attr('src','/resources/images/icon_refreshnew.png');
				if(refresh!=undefined && refresh!=null){
				}
				if(!data.error) {
					var htmlBody = '';
					var htmlFooter = '';
					var totalDSKH = 0;
					var totalDSTH = 0;
					var totalDuyet = 0;
					var totalDSLeft = 0;
					var lstNVGSSaleInfo = data.lstNVGSSaleInfo;
					if($.isArray(lstNVGSSaleInfo)) {
						for(var i = 0; i < lstNVGSSaleInfo.length; i++) {
							var nvgsSaleInfo = lstNVGSSaleInfo[i];
							htmlBody += '<tr>';
							htmlBody += '<td class="FirstTdStyle"><div class="AlignLCols">'+
								Utils.XSSEncode(nvgsSaleInfo.shopCode)+'</div></td>';
							if(SuperviseSales.checkLatLngOnStaff(nvgsSaleInfo.staffId)){
								htmlBody += '<td><div class="AlignLCols"><a href="javascript:void(0);" onclick="return SuperviseSales.selectStaffOnUI('+
									nvgsSaleInfo.staffId+','+parentId+');">'+Utils.XSSEncode(nvgsSaleInfo.staffName)+'</a></div></td>';
							} else {
								htmlBody += '<td><div class="AlignLCols">'+Utils.XSSEncode(nvgsSaleInfo.staffName)+'</div></td>';
							}
							htmlBody += '<td><div class="AlignRCols">'+formatCurrency(SuperviseSales.covertDSKH(nvgsSaleInfo.dayAmountPlan))+'</div></td>';
							htmlBody += '<td><div class="AlignRCols">'+formatCurrency(nvgsSaleInfo.dayAmount)+'</div></td>';
							htmlBody += '<td><div class="AlignRCols">'+formatCurrency(nvgsSaleInfo.dayAmountApproved)+'</div></td>';
							htmlBody += '<td><div class="AlignRCols">'+formatCurrency(nvgsSaleInfo.dayAmountPlan > nvgsSaleInfo.dayAmount ? nvgsSaleInfo.dayAmountPlan - nvgsSaleInfo.dayAmount : 0)+'</div></td>';
							htmlBody += '</tr>';
							totalDSKH += Number(SuperviseSales.covertDSKH(nvgsSaleInfo.dayAmountPlan));
							totalDSTH += Number(nvgsSaleInfo.dayAmount);
							totalDuyet += Number(nvgsSaleInfo.dayAmountApproved);
//							totalDSLeft += Number(nvgsSaleInfo.dayAmountLeft);
						}
						htmlFooter += '<tr>';
						htmlFooter += '<td colspan="2" class="FirstTdStyle"><div class="AlignRCols">Tổng cộng</div></td>';
						htmlFooter += '<td><div class="AlignRCols">'+formatCurrency(totalDSKH)+'</div></td>';
						htmlFooter += '<td><div class="AlignRCols">'+formatCurrency(totalDSTH)+'</div></td>';
						htmlFooter += '<td><div class="AlignRCols">'+formatCurrency(totalDuyet)+'</div></td>';
						htmlFooter += '<td><div class="AlignRCols">'+formatCurrency(totalDSKH > totalDSTH ? totalDSKH-totalDSTH : 0)+'</div></td>';
						htmlFooter += '</tr>';
					}
					$('#tbhvSaleBody').html(htmlBody);
					$('#tbhvSaleFooter').html(htmlFooter);
				}
				
			}
		});
	},

	/**
	 * Doanh so Ngay, Luy ke: cua NVGS cho bang tong hop RPT_STAFF_SALE
	 * them: typeAmount: 1: Ngay; 2: Luy ke
	 * @author vuongmq
	 * @since 21/08/2015 
	 */
	showDialogDaySaleNVGS: function(t, nvgsId, nvgsCode, nvgsName, typeAmount, refresh) {
		if (typeAmount == undefined || typeAmount == null) {
			typeAmount == SuperviseSales._NGAY;
		}
		SuperviseSales._idStaffSelect = nvgsId;
		var parentId = $(t).attr('parentId');
		if (refresh == undefined || refresh == null) {
			/*var title = '<a id="loadPopUpNVGS" style="position: absolute; right: 50px;" onclick="SuperviseSales.showDialogDaySaleNVGS(this,'+nvgsId+',\''+Utils.XSSEncode(nvgsCode)+'\',\''+Utils.XSSEncode(nvgsName)+'\',1)">';*/
			var title = '<a id="loadPopUpNVGS" style="position: absolute; right: 50px;" onclick="SuperviseSales.changeDaySaleNVGS(' + nvgsId + ',' + typeAmount + ');">';
			title += '<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15" title="Cập nhật dữ liệu"/></a>';
			$('#nvgsSaleBody').html('');
			//$('#nvgsFooter').html('');
			$('#dialogDSNgayNVGS').dialog({  
		        title: 'Doanh số bán hàng: ' + Utils.XSSEncode(nvgsCode) + ' - ' + Utils.XSSEncode(nvgsName) + title,  
		        closed: false,  
		        cache: false,  
		        modal: true,
		        onOpen: function() {
		        	/*Utils.initUnitCbx('cbxShop', {}, 200, function(){});*/
		        	var url = '/supervise/sales/show-shop-of-supervise';
		        	var params = new Object();
		        	params.nvgsId = nvgsId;
					SuperviseSales.loadShopSuperviseCbx('cbxShop', url, params, 205, function(data) {
						if (data != undefined && data != null) {
							SuperviseSales.changeDaySaleNVGS(nvgsId, typeAmount, data.id);
						}
					}, function (dataFail) {
						SuperviseSales.changeDaySaleNVGS(nvgsId, typeAmount);
					});
		        },  
		    });
		} else {
			$('.panel-title .refreshIcon').attr('src','/resources/images/loading-small.gif');
		}
		//SuperviseSales.changeDaySaleNVGS(nvgsId, typeAmount);
	},

	/**
	 * add HTML detail Doanh so Ngay, Luy ke: cua NVGS cho bang tong hop RPT_STAFF_SALE
	 * them: typeAmount: 1: Ngay; 2: Luy ke
	 * goi tu ham: showDialogDaySaleNVGS()
	 * @author vuongmq
	 * @since 21/08/2015
	 */
	changeDaySaleNVGS: function(nvgsId, typeAmount, shopId) {
		var url = '';
		if (shopId == undefined || shopId == null) {
			url = '/supervise/sales/getDSNgayNVGS?nvgsId=' + nvgsId;
		} else {
			url = '/supervise/sales/getDSNgayNVGS?nvgsId=' + nvgsId + '&shopIdSearch=' + shopId;
		}
		$('#divOverlay').show();
		$.getJSON(url, function(data) {
			$('#divOverlay').hide();
			if (SuperviseSales._idStaffSelect == nvgsId) {
				$('.panel-title .refreshIcon').attr('src','/resources/images/icon_refreshnew.png');
				if (!data.error) {
					var lstNVBHSaleInfo = data.lstNVBHSaleInfo;
					/** VuongMQ; 20/08/2015; load popup doanh so, san luong luy ke; ***/
					var html = '';
					var totalDSKeHoach = 0;
					var totalDSDuyet = 0;
					var totalDSTong = 0;
					var totalSLKeHoach = 0;
					var totalSLDuyet = 0;
					var totalSLTong = 0;
					if ($.isArray(lstNVBHSaleInfo)) {
						if (typeAmount == SuperviseSales._NGAY) {
							// check radio cho tung loai typeAmount Ngay
							$('#rNVGSNgay').prop('checked', true);
							for (var i = 0, len = lstNVBHSaleInfo.length; i < len; i++) {
								var nvbhSaleInfo = lstNVBHSaleInfo[i];
								var dsKeHoach = SuperviseSales.covertDSKH(nvbhSaleInfo.dayAmountPlan);
								var dsDuyet = SuperviseSales.covertDSKH(nvbhSaleInfo.dayAmountApproved);
								var dsTong = SuperviseSales.covertDSKH(nvbhSaleInfo.dayAmount);
								var tienDoDS = 0;
								if (dsKeHoach == 0) {
									tienDoDS = 0;
								} else if (dsTong == 0) {
									tienDoDS = 0;
								} else {
									tienDoDS = convertValueMath(Number(dsTong) * 100 / Number(dsKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
								}
								var slKeHoach = SuperviseSales.covertDSKH(nvbhSaleInfo.dayQuantityPlan);
								var slDuyet = SuperviseSales.covertDSKH(nvbhSaleInfo.dayQuantityApproved);
								var slTong = SuperviseSales.covertDSKH(nvbhSaleInfo.dayQuantity);
								var tienDoSL = 0;
								if (slKeHoach == 0) {
									tienDoSL = 0;
								} else if (slTong == 0) {
									tienDoSL = 0;
								} else {
									tienDoSL = convertValueMath(Number(slTong) * 100 / Number(slKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
								}
								html += '	<tr>';
								html += '	<td class="FirstTdStyle"><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(nvbhSaleInfo.staffCode)) + '</div></td>';
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(nvbhSaleInfo.staffName)) + '</div></td>';
								if (apConfig.sysSaleRoute == SuperviseSales._amountType.ROUTE) {
									html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(nvbhSaleInfo.routingCode)) + '</div></td>';
									html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(nvbhSaleInfo.routingName)) + '</div></td>';
								}
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsKeHoach) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(tienDoDS) + '%</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slKeHoach) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(tienDoSL) + '%</div></td>';
								html += '</tr>';
								totalDSKeHoach += Number(dsKeHoach);
								totalDSDuyet += Number(dsDuyet);
								totalDSTong += Number(dsTong);
								totalSLKeHoach += Number(slKeHoach);
								totalSLDuyet += Number(slDuyet);
								totalSLTong += Number(slTong);
							}
						} else if (typeAmount == SuperviseSales._LUY_KE) {
							// check radio cho tung loai typeAmount Luy Ke
							$('#rNVGSLuyKe').prop('checked', true);
							for (var i = 0, len = lstNVBHSaleInfo.length; i < len; i++) {
								var nvbhSaleInfo = lstNVBHSaleInfo[i];
								var dsKeHoach = SuperviseSales.covertDSKH(nvbhSaleInfo.monthAmountPlan);
								var dsDuyet = SuperviseSales.covertDSKH(nvbhSaleInfo.monthAmountApproved);
								var dsTong = SuperviseSales.covertDSKH(nvbhSaleInfo.monthAmount);
								var tienDoDS = 0;
								if (dsKeHoach == 0) {
									tienDoDS = 0;
								} else if (dsTong == 0) {
									tienDoDS = 0;
								} else {
									tienDoDS = convertValueMath(Number(dsTong) * 100 / Number(dsKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
								}
								var slKeHoach = SuperviseSales.covertDSKH(nvbhSaleInfo.monthQuantityPlan);
								var slDuyet = SuperviseSales.covertDSKH(nvbhSaleInfo.monthQuantityApproved);
								var slTong = SuperviseSales.covertDSKH(nvbhSaleInfo.monthQuantity);
								var tienDoSL = 0;
								if (slKeHoach == 0) {
									tienDoSL = 0;
								} else if (slTong == 0) {
									tienDoSL = 0;
								} else {
									tienDoSL = convertValueMath(Number(slTong) * 100 / Number(slKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
								}
								html += '	<tr>';
								html += '	<td class="FirstTdStyle"><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(nvbhSaleInfo.staffCode)) + '</div></td>';
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(nvbhSaleInfo.staffName)) + '</div></td>';
								if (apConfig.sysSaleRoute == SuperviseSales._amountType.ROUTE) {
									html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(nvbhSaleInfo.routingCode)) + '</div></td>';
									html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(nvbhSaleInfo.routingName)) + '</div></td>';
								}
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsKeHoach) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(tienDoDS) + '%</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slKeHoach) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(tienDoSL) + '%</div></td>';
								html += '</tr>';
								totalDSKeHoach += Number(dsKeHoach);
								totalDSDuyet += Number(dsDuyet);
								totalDSTong += Number(dsTong);
								totalSLKeHoach += Number(slKeHoach);
								totalSLDuyet += Number(slDuyet);
								totalSLTong += Number(slTong);
							}
						}
					}
					var totalTienDoDS = 0;
					if (totalDSKeHoach == 0) {
						totalTienDoDS = 0;
					} else if (totalDSTong == 0) {
						totalTienDoDS = 0;
					} else {
						totalTienDoDS = convertValueMath(Number(totalDSTong) * 100 / Number(totalDSKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					}
					var totalTienDoSL = 0;
					if (totalSLKeHoach == 0) {
						totalTienDoSL = 0;
					} else if (totalSLTong == 0) {
						totalTienDoSL = 0;
					} else {
						totalTienDoSL = convertValueMath(Number(totalSLTong) * 100 / Number(totalSLKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					}
					html += '<tr class="FooterTable">';
					if (apConfig.sysSaleRoute == SuperviseSales._amountType.ROUTE) {
						html += '	<td class="FirstTdStyle" colspan="4"><div class="AlignRCols">Tổng cộng</div></td>';
					} else {
						html += '	<td class="FirstTdStyle" colspan="2"><div class="AlignRCols">Tổng cộng</div></td>';
					}
					
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalDSKeHoach) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalDSDuyet) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalDSTong) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalTienDoDS) + '%</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalSLKeHoach) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalSLDuyet) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalSLTong) + '</div></td>';
					html += '	<td class="EndTdStyle"><div class="AlignRCols">' + formatCurrency(totalTienDoSL) + '%</div></td>';
					html += '</tr>';
					$('#nvgsSaleBody').html(html);
					$('#loadPopUpNVGS').attr('onclick', 'SuperviseSales.changeDaySaleNVGS(' + nvgsId + ',' + typeAmount + ',' + shopId + ');');
					$('#rNVGSNgay').attr('onclick', 'SuperviseSales.changeDaySaleNVGS(' + nvgsId + ',' + SuperviseSales._NGAY + ',' + shopId + ');');
					$('#rNVGSLuyKe').attr('onclick', 'SuperviseSales.changeDaySaleNVGS(' + nvgsId + ',' + SuperviseSales._LUY_KE + ',' + shopId + ');');
				}
				
			}
		});
	},

	/**
	 * Doanh so Ngay, Luy ke: cua NVBH cho bang tong hop RPT_STAFF_SALE_DETAIL
	 * typeAmount: 1: Ngay; 2: Luy ke
	 * @author vuongmq
	 * @since 20/08/2015 
	 */
	showDialogDaySaleNVBH: function(t, nvbhId, nvbhCode, nvbhName, typeAmount, refresh) {
		if (typeAmount == undefined || typeAmount == null) {
			typeAmount == SuperviseSales._NGAY;
		}
		SuperviseSales._idStaffSelect = nvbhId;
		if (refresh == undefined || refresh == null) {
			/*var title='<a id="loadPopUpNVBH" style="position: absolute; right: 50px;" onclick="SuperviseSales.showDialogDaySaleNVBH(this,'+nvbhId+',\''+
				Utils.XSSEncode(nvbhCode)+'\',\''+Utils.XSSEncode(nvbhName)+'\',\''+typeAmount+'\',1)">';*/
			var title = '<a id="loadPopUpNVBH" style="position: absolute; right: 50px;" onclick="SuperviseSales.changeDaySaleNVBH(' + nvbhId + ',' + typeAmount + ');">';
			title += '<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15" title="Cập nhật dữ liệu"/></a>';
			//$('#nvbhBody').html('');
			//$('#nvbhFooter').html('');
			$('#bodyNVBH').html('');
			$('#dialogDSNgayNVBH').dialog({  
		        title: 'Doanh số bán hàng: ' + Utils.XSSEncode(nvbhCode) + ' - ' + Utils.XSSEncode(nvbhName) + title,  
		        closed: false,  
		        cache: false,  
		        modal: true
		    });
		} else {
			$('.panel-title .refreshIcon').attr('src', '/resources/images/loading-small.gif');
		}
		SuperviseSales.changeDaySaleNVBH(nvbhId, typeAmount);
	},

	/**
	 * add HTML detail Doanh so Ngay, Luy ke: cua NVBH cho bang tong hop RPT_STAFF_SALE_DETAIL
	 * goi tu ham: showDialogDaySaleNVBH()
	 * them: tung loai typeAmount: 1: Ngay, 2: luy ke
	 * @author vuongmq
	 * @since 20/08/2015
	 */
	changeDaySaleNVBH: function(nvbhId, typeAmount) {
		$('#divOverlay').show();
		$.getJSON('/supervise/sales/getDSNgayNVBH?nvbhId=' + nvbhId, function(data) {
			$('#divOverlay').hide();
			if (SuperviseSales._idStaffSelect == nvbhId) {
				$('.panel-title .refreshIcon').attr('src', '/resources/images/icon_refreshnew.png');
				if (!data.error) {
					var lstCustomerSaleInfo = data.lstCustomerSaleInfo;
					/** VuongMQ; 20/08/2015; load popup doanh so, san luong luy ke; ***/
					var html = '';
					var totalDSDuyet = 0;
					var totalDSTong = 0;
					var totalSLDuyet = 0;
					var totalSLTong = 0;
					if ($.isArray(lstCustomerSaleInfo)) {
						if (typeAmount == SuperviseSales._NGAY) {
							// check radio cho tung loai typeAmount Ngay
							$('#rNVBHNgay').prop('checked', true);
							for (var i = 0, len = lstCustomerSaleInfo.length; i < len; i++) {
								var customerSaleInfo = lstCustomerSaleInfo[i];
								var dsDuyet = SuperviseSales.covertDSKH(customerSaleInfo.dayAmountApproved);
								var dsTong = SuperviseSales.covertDSKH(customerSaleInfo.dayAmount);
								var slDuyet = SuperviseSales.covertDSKH(customerSaleInfo.dayQuantityApproved);
								var slTong = SuperviseSales.covertDSKH(customerSaleInfo.dayQuantity);
								html += '<tr>';
								html += '	<td class="FirstTdStyle"><div class="AlignCCols">' + (i + 1) + '</div></td>';
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(customerSaleInfo.shortCode)) + '</div></td>';
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(customerSaleInfo.customerName)) + '</div></td>';
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(customerSaleInfo.address)) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slTong) + '</div></td>';
								html += '</tr>';							
								totalDSDuyet += Number(dsDuyet);
								totalDSTong += Number(dsTong);
								totalSLDuyet += Number(slDuyet);
								totalSLTong += Number(slTong);
							}
						} else if (typeAmount == SuperviseSales._LUY_KE) {
							// check radio cho tung loai typeAmount Luy Ke
							$('#rNVBHLuyKe').prop('checked', true);
							for (var i = 0, len = lstCustomerSaleInfo.length; i < len; i++) {
								var customerSaleInfo = lstCustomerSaleInfo[i];
								var dsDuyet = SuperviseSales.covertDSKH(customerSaleInfo.monthAmountApproved);
								var dsTong = SuperviseSales.covertDSKH(customerSaleInfo.monthAmount);
								var slDuyet = SuperviseSales.covertDSKH(customerSaleInfo.monthQuantityApproved);
								var slTong = SuperviseSales.covertDSKH(customerSaleInfo.monthQuantity);
								html += '<tr>';
								html += '	<td class="FirstTdStyle"><div class="AlignCCols">' + (i + 1) + '</div></td>';
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(customerSaleInfo.shortCode))  + '</div></td>';
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(customerSaleInfo.customerName))  + '</div></td>';
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(customerSaleInfo.address)) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slTong) + '</div></td>';
								html += '</tr>';							
								totalDSDuyet += Number(dsDuyet);
								totalDSTong += Number(dsTong);
								totalSLDuyet += Number(slDuyet);
								totalSLTong += Number(slTong);
							}
						}
					}
					html += '<tr class="FooterTable">';
					html += '	<td class="FirstTdStyle" colspan="4"><div class="AlignRCols">Tổng cộng</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalDSDuyet) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalDSTong) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalSLDuyet) + '</div></td>';
					html += '	<td class="EndTdStyle"><div class="AlignRCols">' + formatCurrency(totalSLTong) + '</div></td>';
					html += '</tr>';
					$('#bodyNVBH').html(html);
					$('#loadPopUpNVBH').attr('onclick', 'SuperviseSales.changeDaySaleNVBH(' + nvbhId + ',' + typeAmount + ');');
					$('#rNVBHNgay').attr('onclick', 'SuperviseSales.changeDaySaleNVBH(' + nvbhId + ',' + SuperviseSales._NGAY + ');');
					$('#rNVBHLuyKe').attr('onclick', 'SuperviseSales.changeDaySaleNVBH(' + nvbhId + ',' + SuperviseSales._LUY_KE + ');');
				}
			}
		});
	},
	
	/**
	 * Up shop parent Doanh so Ngay, Luy ke: cua Shop cho bang tong hop RPT_STAFF_SALE
	 * @author vuongmq
	 * @param parentShopId
	 * @since 23/09/2015 
	 */
	upShopParentAdd: function(shopId, typeAmount) {
		if (shopId != undefined && shopId != null && shopId != 'null') {
			var params= {};
			params.shopId = shopId;
			Utils.getJSONDataByAjaxNotOverlay(params, '/commons/view-shop-info' , function(data) {
				if (data != null && data.shop != undefined && data.shop != null) {
					var shop = data.shop;
					var parentShopId = shop.parentShopId;
					var parentShopCode = shop.parentShopCode;
					var parentShopName = shop.parentShopName;
					if (parentShopId != undefined && parentShopId != null && parentShopId != 'null') {
						$('#divUpShopParent').css('visibility', 'visible');
						$('#upShopParent').attr('onclick','SuperviseSales.showDialogDaySaleShop(null,\'' + parentShopId + '\',\'' + Utils.XSSEncode(parentShopCode) + '\',\'' + Utils.XSSEncode(parentShopName) + '\',\'' + typeAmount + '\');');
					} else {
						$('#divUpShopParent').css('visibility', 'hidden');
						$('#upShopParent').attr('onclick','');
					}
				}
			});
		}
	},

	/**
	 * Doanh so Ngay, Luy ke: cua Shop cho bang tong hop RPT_STAFF_SALE
	 * @author vuongmq
	 * @param t: this
	 * @param shopId
	 * @param shopCode
	 * @param shopName
	 * @param typeAmount
	 * @param refresh
	 * @since 21/09/2015 
	 */
	showDialogDaySaleShop: function(t, shopId, shopCode, shopName, typeAmount, refresh) {
		if (typeAmount == undefined || typeAmount == null) {
			typeAmount == SuperviseSales._NGAY;
		}
		SuperviseSales._idShopSelect = shopId;
		//var parentId = $(t).attr('parentId');
		if (refresh == undefined || refresh == null) {
			var title = '<a id="loadPopUpShop" style="position: absolute; right: 50px;" onclick="SuperviseSales.changeDaySaleShop(' + shopId + ',' + typeAmount + ');">';
			title += '<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15" title="Cập nhật dữ liệu"/></a>';
			$('#shopSaleBody').html('');
			$('#dialogDSNgayShop').dialog({  
		        title: 'Doanh số bán hàng: ' + Utils.XSSEncode(shopCode) + ' - ' + Utils.XSSEncode(shopName) + title,  
		        closed: false,  
		        cache: false,  
		        modal: true,
		        onOpen: function() {
		        },  
		    });
		} else {
			$('.panel-title .refreshIcon').attr('src','/resources/images/loading-small.gif');
		}
		SuperviseSales.changeDaySaleShop(shopId, typeAmount);
	},

	/**
	 * add HTML detail Doanh so Ngay, Luy ke: cua Shop cho bang tong hop RPT_STAFF_SALE
	 * goi tu ham: showDialogDaySaleShop()
	 * @author vuongmq
	 * @param shopId
	 * @param typeAmount
	 * @since 21/09/2015
	 */
	changeDaySaleShop: function(shopId, typeAmount) {
		// them nut up shop
		SuperviseSales.upShopParentAdd(shopId, typeAmount);
		var url = '/supervise/sales/getDSNgayShop?shopIdSearch=' + shopId;
		$('#divOverlay').show();
		$.getJSON(url, function(data) {
			$('#divOverlay').hide();
			if (SuperviseSales._idShopSelect == shopId) {
				$('.panel-title .refreshIcon').attr('src','/resources/images/icon_refreshnew.png');
				if (!data.error) {
					var lstShopSaleInfo = data.lstShopSaleInfo;
					var html = '';
					var totalDSKeHoach = 0;
					var totalDSDuyet = 0;
					var totalDSTong = 0;
					var totalSLKeHoach = 0;
					var totalSLDuyet = 0;
					var totalSLTong = 0;
					if ($.isArray(lstShopSaleInfo)) {
						if (typeAmount == SuperviseSales._NGAY) {
							// check radio cho tung loai typeAmount Ngay
							$('#rShopNgay').prop('checked', true);
							for (var i = 0, len = lstShopSaleInfo.length; i < len; i++) {
								var shopSaleInfo = lstShopSaleInfo[i];
								var dsKeHoach = SuperviseSales.covertDSKH(shopSaleInfo.dayAmountPlan);
								var dsDuyet = SuperviseSales.covertDSKH(shopSaleInfo.dayAmountApproved);
								var dsTong = SuperviseSales.covertDSKH(shopSaleInfo.dayAmount);
								var tienDoDS = 0;
								if (dsKeHoach == 0) {
									tienDoDS = 0;
								} else if (dsTong == 0) {
									tienDoDS = 0;
								} else {
									tienDoDS = convertValueMath(Number(dsTong) * 100 / Number(dsKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
								}
								var slKeHoach = SuperviseSales.covertDSKH(shopSaleInfo.dayQuantityPlan);
								var slDuyet = SuperviseSales.covertDSKH(shopSaleInfo.dayQuantityApproved);
								var slTong = SuperviseSales.covertDSKH(shopSaleInfo.dayQuantity);
								var tienDoSL = 0;
								if (slKeHoach == 0) {
									tienDoSL = 0;
								} else if (slTong == 0) {
									tienDoSL = 0;
								} else {
									tienDoSL = convertValueMath(Number(slTong) * 100 / Number(slKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
								}
								html += '	<tr>';
								if (shopSaleInfo.specificType != undefined && shopSaleInfo.specificType != null && shopSaleInfo.specificType == SuperviseSales._nodeType.SHOP) { // bang NPP
									html += '	<td class="FirstTdStyle"><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(shopSaleInfo.shopCode)) + '</div></td>';	
								} else {
									html += '	<td class="FirstTdStyle"><div class="AlignLCols"> <a href="javascript:void(0)" onclick="SuperviseSales.showDialogDaySaleShop(this,' + shopSaleInfo.shopId + ',\'' + Utils.XSSEncode(shopSaleInfo.shopCode) + '\',\'' + Utils.XSSEncode(shopSaleInfo.shopName) + '\',\'' + SuperviseSales._NGAY + '\');" >' + Utils.XSSEncode(SuperviseSales.covertValueStr(shopSaleInfo.shopCode)) + '</a></div></td>';
								}
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(shopSaleInfo.shopName)) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsKeHoach) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(tienDoDS) + '%</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slKeHoach) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(tienDoSL) + '%</div></td>';
								html += '</tr>';
								totalDSKeHoach += Number(dsKeHoach);
								totalDSDuyet += Number(dsDuyet);
								totalDSTong += Number(dsTong);
								totalSLKeHoach += Number(slKeHoach);
								totalSLDuyet += Number(slDuyet);
								totalSLTong += Number(slTong);
							}
						} else if (typeAmount == SuperviseSales._LUY_KE) {
							// check radio cho tung loai typeAmount Luy Ke
							$('#rShopLuyKe').prop('checked', true);
							for (var i = 0, len = lstShopSaleInfo.length; i < len; i++) {
								var shopSaleInfo = lstShopSaleInfo[i];
								var dsKeHoach = SuperviseSales.covertDSKH(shopSaleInfo.monthAmountPlan);
								var dsDuyet = SuperviseSales.covertDSKH(shopSaleInfo.monthAmountApproved);
								var dsTong = SuperviseSales.covertDSKH(shopSaleInfo.monthAmount);
								var tienDoDS = 0;
								if (dsKeHoach == 0) {
									tienDoDS = 0;
								} else if (dsTong == 0) {
									tienDoDS = 0;
								} else {
									tienDoDS = convertValueMath(Number(dsTong) * 100 / Number(dsKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
								}
								var slKeHoach = SuperviseSales.covertDSKH(shopSaleInfo.monthQuantityPlan);
								var slDuyet = SuperviseSales.covertDSKH(shopSaleInfo.monthQuantityApproved);
								var slTong = SuperviseSales.covertDSKH(shopSaleInfo.monthQuantity);
								var tienDoSL = 0;
								if (slKeHoach == 0) {
									tienDoSL = 0;
								} else if (slTong == 0) {
									tienDoSL = 0;
								} else {
									tienDoSL = convertValueMath(Number(slTong) * 100 / Number(slKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
								}
								html += '	<tr>';
								if (shopSaleInfo.specificType != undefined && shopSaleInfo.specificType != null && shopSaleInfo.specificType == SuperviseSales._nodeType.SHOP) { // bang NPP
									html += '	<td class="FirstTdStyle"><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(shopSaleInfo.shopCode)) + '</div></td>';
								} else {
									html += '	<td class="FirstTdStyle"><div class="AlignLCols"> <a href="javascript:void(0)" onclick="SuperviseSales.showDialogDaySaleShop(this,' + shopSaleInfo.shopId + ',\'' + Utils.XSSEncode(shopSaleInfo.shopCode) + '\',\'' + Utils.XSSEncode(shopSaleInfo.shopName) + '\',\'' + SuperviseSales._LUY_KE + '\');" >' + Utils.XSSEncode(SuperviseSales.covertValueStr(shopSaleInfo.shopCode)) + '</a></div></td>';
								}
								html += '	<td><div class="AlignLCols">' + Utils.XSSEncode(SuperviseSales.covertValueStr(shopSaleInfo.shopName)) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsKeHoach) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(dsTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(tienDoDS) + '%</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slKeHoach) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slDuyet) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(slTong) + '</div></td>';
								html += '	<td><div class="AlignRCols">' + formatCurrency(tienDoSL) + '%</div></td>';
								html += '</tr>';
								totalDSKeHoach += Number(dsKeHoach);
								totalDSDuyet += Number(dsDuyet);
								totalDSTong += Number(dsTong);
								totalSLKeHoach += Number(slKeHoach);
								totalSLDuyet += Number(slDuyet);
								totalSLTong += Number(slTong);
							}
						}
					}
					var totalTienDoDS = 0;
					if (totalDSKeHoach == 0) {
						totalTienDoDS = 0;
					} else if (totalDSTong == 0) {
						totalTienDoDS = 0;
					} else {
						totalTienDoDS = convertValueMath(Number(totalDSTong) * 100 / Number(totalDSKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					}
					var totalTienDoSL = 0;
					if (totalSLKeHoach == 0) {
						totalTienDoSL = 0;
					} else if (totalSLTong == 0) {
						totalTienDoSL = 0;
					} else {
						totalTienDoSL = convertValueMath(Number(totalSLTong) * 100 / Number(totalSLKeHoach), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					}
					html += '<tr class="FooterTable">';
					html += '	<td class="FirstTdStyle" colspan="2"><div class="AlignRCols">Tổng cộng</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalDSKeHoach) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalDSDuyet) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalDSTong) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalTienDoDS) + '%</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalSLKeHoach) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalSLDuyet) + '</div></td>';
					html += '	<td><div class="AlignRCols">' + formatCurrency(totalSLTong) + '</div></td>';
					html += '	<td class="EndTdStyle"><div class="AlignRCols">' + formatCurrency(totalTienDoSL) + '%</div></td>';
					html += '</tr>';
					$('#shopSaleBody').html(html);
					$('#loadPopUpShop').attr('onclick', 'SuperviseSales.changeDaySaleShop(' + shopId + ',' + typeAmount + ');');
					$('#rShopNgay').attr('onclick', 'SuperviseSales.changeDaySaleShop(' + shopId + ',' + SuperviseSales._NGAY + ');');
					$('#rShopLuyKe').attr('onclick', 'SuperviseSales.changeDaySaleShop(' + shopId + ',' + SuperviseSales._LUY_KE + ');');
					
				}
			}
		});
	},
	
	/** TIENTV */
	showDialogMonthAmountPlan:function(t,type,staffId,refresh){
		var params = new Object();
		params.staffId = staffId;
		params.type = type;
		SuperviseSales._idStaffSelect=staffId;
		if(refresh==undefined || refresh==null){
			$('#monthAmountPlan').dialog('open');
		}else{
			$('.panel-title .refreshIcon').attr('src','/resources/images/loading-small.gif');
		}
		$('#monthAmountPlanContainer').html('');
		var title='<a style="position: absolute; right: 27px;" onclick="SuperviseSales.showDialogMonthAmountPlan(this,'+type+','+staffId+',1)">';
		title+='<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15"/></a>';
		$('.panel-title').html(title);
		$.ajax({
			type : 'POST',
			url : '/supervise/sales/amount-plan',
			data :($.param(params, true)),
			dataType : 'html',
			success : function(result) {
				if(SuperviseSales._idStaffSelect==staffId){
					$('#monthAmountPlanContainer').html(result);
					if(type!=StaffRoleType.TBHM){
						$('#titleStaffType').html('GSNPP');
						$('#titleStaffCode').html('NPP');
						if(type==StaffRoleType.NVGS){
							$('#titleStaffCode').html('Mã NVBH');
							$('#titleStaffType').html('NVBH');
						}
					} else {
						$('#titleStaffType').html('TBHV');
						$('#titleStaffCode').html('Vùng');
						$('#titleTBHV').html('Mã TBHV');
					}
					var title='<a style="position: absolute; right: 27px;" onclick="SuperviseSales.showDialogMonthAmountPlan(this,'+type+','+staffId+',1)">';
					title+='<img class="refreshIcon" src="/resources/images/icon_refreshnew.png" height="15"/></a>';
					$('.panel-title').html($('#titleDialogAmount').val().trim() + title);
				}
			}
		});
	},
	showListCustomer: function(staffId, roleType){
		SuperviseSales.resetPathViewer();
//		SuperviseSales.reloadMarker();
		ViettelMap.clearOverlays();
		$('#listStaff').toggle(false);
		SuperviseSales._idStaffSelected = staffId;
		var params = new Object();
		params.staffId = staffId;
		params.checkDate = $('#startDate').val().trim();
		$('#divOverlay').show();
		$.ajax({
			type: 'POST',
			url: '/supervise/sales/visitplan-listcustomer',
			data:($.param(params, true)),
			dataType: 'json',
			success: function(result) {
				/*if (result != null && result.distance != null && result.distance >= 0) {
					$('#kmDriven').show();
					if (result.distance > 1000) {
						$('#resultKmDriven').html(formatCurrency(Math.round(result.distance/1000)));
						$('#resultKmDriven').attr('title', result.distance + " (m)");
						$('#titleResultKmDrivenId').html("(Km)");
					} else {
						$('#resultKmDriven').html(formatCurrency(result.distance));
						$('#titleResultKmDrivenId').html("(m)");
					}
				}*/
				SuperviseSales.customerViewer = new Array();
				SuperviseSales._lstCust = new Map();
				if (result.customers != undefined && result.customers != null) {
					var customers = result.customers;
					var cus = null;
					SuperviseSales.customerViewer = result.customers;
					$('.overlayCust').remove();	// xóa danh sách khách hàng
					for (i = 0; i < SuperviseSales._lstStaffPosition.valArray.length; i++) {
						var temp = SuperviseSales._lstStaffPosition.valArray[i];
						if (temp.staffId == staffId) {
							ViettelMap.addMarkerStaff(temp);
						}
					}
					if (customers == null || customers.length == 0) {
						$('.olPopupContent .MPContent .Text1Style').html(Utils.XSSEncode(jsSaleSuperviseSalerMarkerNoCustomerVisited));
					} else {
						var ordinalVisitReal = 1;
						for ( var i = 0; i < customers.length; ++i) {
							cus = new Object();
							cus = customers[i];
							var flagStatus=1;
							if(cus.isOr != null && cus.isOr == 1) {//kh ngoai tuyen
								cus.image = "Customers5Status";
								cus.ordinalVisit = '!';
							} else if (cus.count != null && cus.count >= 1) {//co don hang
								cus.image = "Customers1Status";
							} else if (cus.objectType == 0 && cus.endTime == null) {//dang ghe tham
								cus.image = "Customers3Status";
							} else if (cus.objectType == null) {//chua ghe tham
								cus.image = "Customers2Status";
							} else if (cus.objectType == 0 || cus.objectType == 1) {
								cus.image = "Customers4Status";
							} else {
								flagStatus = 0;
							}
							cus.ordinalAndTimeVisitReal = "";
							if (cus.objectType != null && (cus.objectType == 0 || cus.objectType == 1)) {
								if(cus.startTimeHHMM != null && cus.startTimeHHMM != '' && cus.startTimeHHMM != undefined) {
									cus.ordinalAndTimeVisitReal = ordinalVisitReal + '-' + cus.startTimeHHMM;
								} else {
									cus.ordinalAndTimeVisitReal = ordinalVisitReal;
								}
								ordinalVisitReal++;
							}
							
							if (cus.ordinalVisit == null) {
								cus.ordinalVisit = '';
							}
							if (flagStatus == 1) {
								if(SuperviseSales._lstCust.get(cus.id) == null || SuperviseSales._lstCust.get(cus.id) == undefined) {
									SuperviseSales._lstCust.put(cus.id, cus);
//									if (cus.lat > 0 && cus.lng > 0){
									if (cus.lat != null && cus.lng != null) {
										ViettelMap.addMarkerCust(cus, staffId); // vuongmq; 19/08/2015; Lay them DK staffId
									}
								}
							}
						}
						if (ViettelMap._currentInfoWindow!=null) {
							ViettelMap._currentInfoWindow.close(); //Đóng infowindow lại
						}
						ViettelMap.fitOverLay();
						ViettelMap.hideShowTitleMarker();
					}
					if (roleType == StaffSpecType.SUPERVISOR) {
						$("#item1").hide();
						$("#item2").hide();
						$("#item3").hide();
						$("#item4").hide();
						$("#item5").hide();
						$('.NoteCustomersStatus').show();
						// adjust element size and position
						try{
							$('div.NoteCustomersStatus').css('width', 'auto').css('left', '1000px');
						} catch (e) {
							
						}
					}else{
						$("#item1").show();
						$("#item2").show();
						$("#item3").show();
						$("#item4").show();
						$("#item5").show();
						$('.NoteCustomersStatus').show();
						// adjust element size and position
						try{
							$('div.NoteCustomersStatus').css('width', 'auto').css('left', '95px');
						} catch (e) {
							
						}
					}
				}
				
				var stType = SuperviseSales._lstStaffPosition.get(staffId);
				if (stType != null) {
					if (stType.roleType == StaffSpecType.SUPERVISOR) {
						SuperviseSales.showHideDescript(true);
					}else{
						SuperviseSales.showHideDescript(false);
					}
				}
				$('#divOverlay').hide();
			}
		});
	},
	showListCustomerByVisitPlan: function (staffId, staffName, roleType) {
		if (roleType == StaffSpecType.SUPERVISOR) {
			$('#cbTBHV').attr('checked', false);
			$('#cbNVBH').attr('checked', false);
			$('#cbGSNPP').attr('checked', true);
//			SuperviseSales.reloadMarker();
		} else if (roleType == StaffSpecType.STAFF) {
			$('#cbTBHV').attr('checked', false);
			$('#cbNVBH').attr('checked', true);
			$('#cbGSNPP').attr('checked', false);
//			SuperviseSales.reloadMarker();
		}
		SuperviseSales.id = staffId;
		$('input[name=kmType][value="0"]').click();
		$('#titleId').html(Utils.XSSEncode(msgTuyen7 + ' ' + staffName));
		$('#xemLoTrinhDialog').show();
		$("#btCapNhatViTri").unbind( "click" );
		$('#btCapNhatViTri').bind('click', function() {
			SuperviseSales.numKm = 0;
			SuperviseSales.calcShortestDistance(0);
		});
		$('#btCapNhatViTri').html(jsSaleSuperviseControlCalcDistanceText);
		
		$('#startDate').val(getCurrentDate());
		applyDateTimePicker("#startDate", null, null, null, null, null, null, null, null, null, null, function() {
			var currentTime = new Date(SuperviseSales.sysDateFromDB);
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			var startDate = $('#startDate').val().trim();
			if (!Utils.compareDate(startDate, day + '/' + month + '/' + year)) {
				$('#errMsg').html(msgCommonErr9).show();
				$('#startDate').focus();
			} else {
				$('#errMsg').html('').hide();
				SuperviseSales.numKm = 0;
				SuperviseSales.showListCustomer(SuperviseSales.id, roleType);
			}
		});
		SuperviseSales.numKm = 0;
		$('#startDate').val(getCurrentDate());
		SuperviseSales.showListCustomer(staffId, roleType);
	},
	// vuongmq; 19/08/2015; Lay them DK staffId
	showCustomerDetail:function (id, staffId) {
		if (ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		var customer = SuperviseSales._lstCust.get(id);
		if (customer != null) {
			//Vietbando._map.closeInfoWindow();
			/*$('#h2CustomerCode').html(Utils.XSSEncode(customer.shortCode) + ' - ' + Utils.XSSEncode(customer.customerName));
			$('#ddAddress').html('');
			if(customer.address!=null){
				$('#ddAddress').html(Utils.XSSEncode(customer.address));
			}
			$('#ddMobiPhone').html('');
			if(customer.mobilephone!=null){
				$('#ddMobiPhone').html(Utils.XSSEncode(customer.mobilephone));
			}
			$('#ddPhone').html('');
			if(customer.phone!=null){
				$('#ddPhone').html(Utils.XSSEncode(customer.phone));
			}
			$('#ddAmountPlan').html('');
			$('#ddAmount').html('');
			if(customer.amountPlan!=null){
				$('#ddAmountPlan').html(formatCurrency(roundNumber(customer.amountPlan/1000,0)));				
			}
			if(customer.amount!=null){
				$('#ddAmount').html(formatCurrency(roundNumber(customer.amount/1000,0)));
			}
			if(customer.dsDuyet!=null){
				$('#ddDuyet').html(formatCurrency(roundNumber(customer.dsDuyet/1000,0)));
			}
			$('#ddStartTime').html('');
			$('#ddEndTime').html('');
			if(customer.startTime!=null){
				$('#ddStartTime').html(customer.startTimeHHMM);				
			}
			if(customer.endTime!=null){
				$('#ddEndTime').html(customer.endTimeHHMM);
			}
			var pt = new viettel.LatLng(customer.lat, customer.lng);
			var html = $('#showCustomerDetail').html();
			//Vietbando._map.openInfoWindow(pt,html);

			if (ViettelMap._currentInfoWindow != null) {
				ViettelMap._currentInfoWindow.close();
			}
			var infoWindow = new viettel.InfoWindow({
				position : pt,
				content : html,
				maxWidth : 500,
				maxHeight : 400,
				//maxWidth : 350,
			});
			infoWindow.open(ViettelMap._map);
			ViettelMap._currentInfoWindow = infoWindow;*/
			var params = {nvbhId: staffId, customerId: id};
			var url = '/supervise/sales/show-amount-customer-of-staff';
			Utils.getJSONDataByAjaxNotOverlay(params, url, function (data) {
				if (data != undefined && data != null && data.customer != undefined && data.customer != null && data.customer.shortCode != undefined && data.customer.shortCode != null) {
					var cusAmount = data.customer;
					var html = SuperviseSales.viewHTMLCustomerOfStaff(cusAmount);
					var pt = new viettel.LatLng(cusAmount.lat, cusAmount.lng);
					if (ViettelMap._currentInfoWindow != null) {
						ViettelMap._currentInfoWindow.close();
					}
					var infoWindow = new viettel.InfoWindow({
						position : pt,
						content : html,
						maxWidth : 500,
						maxHeight : 400,
						//maxWidth : 350,
					});
					infoWindow.open(ViettelMap._map);
					ViettelMap._currentInfoWindow = infoWindow;
				}
			});
		}
	},
//	showDialogTBHVInfo: function(tbhvId, tbhvCode, tbhvName, shopCode, shopName, timeUpdate, dochinhxac,pt) {
	showDialogTBHVInfo: function(tbhvId, tbhvCode, tbhvName, timeUpdate, dochinhxac,pt) {
		$.getJSON('/supervise/sales/getTBHVInfo?tbhvId='+tbhvId, function(data) {
			if(!data.error){
				SuperviseSales._idStaffSelected = tbhvId;
				var dayAmountPlan = SuperviseSales.covertDSKH(data.dayAmountPlan);
				var dayAmount = data.dayAmount;
				var dayAmountApproved = data.dayAmountApproved;
				var dayTiendo = 0;
				if(dayAmountPlan ==0) {
					dayTiendo = 0;//loctt - Oct23, 2013 - fix 0011454 (change from 100 to 0)
				} else if(dayAmount == 0) {
					dayTiendo = 0;
				} else {
					dayTiendo = Math.ceil(Number(dayAmount) * 100 / Number(dayAmountPlan));
					if(dayTiendo != 0) {
						dayTiendo = dayTiendo - 1;
					}
				}
				if(isNaN(dayTiendo)) dayTiendo=0;
				var tienDoChuan=data.tienDoChuan;
				var monthAmountPlan = SuperviseSales.covertDSKH(data.monthAmountPlan); 
				var monthAmount = data.monthAmount;
				var monthAmountApproved = data.monthAmountApproved;
				var monthTiendo = 0;
				if(monthAmountPlan ==0) {
					monthTiendo = 100;
				} else if(monthAmount == 0) {
					monthTiendo = 0;
				} else {
					monthTiendo = Math.ceil(Number(monthAmount) * 100 / Number(monthAmountPlan));
					if(monthTiendo != 0 && (monthAmount % monthAmountPlan) != 0) {
						monthTiendo = monthTiendo - 1;
					}
				}
				if(monthAmountPlan == 0 && monthAmount == 0){
					monthTiendo = 0;
				}
				if(isNaN(monthTiendo)) monthTiendo=0;
				
				if(timeUpdate==undefined || timeUpdate==null || timeUpdate=='null') timeUpdate='';
				if(dochinhxac==undefined || dochinhxac==null || dochinhxac=='null') dochinhxac='';
				if(tienDoChuan==undefined || tienDoChuan==null || tienDoChuan=='null') tienDoChuan=0;
				var html = '<div class="MapPopupSection MapPopup1Section">';
				html += '<h2 class="Title2Style">'+Utils.XSSEncode(tbhvCode)+' - '+Utils.XSSEncode(tbhvName)+'</h2>';
				var temp = SuperviseSales._lstStaffPosition.get(tbhvId);
				if(temp!=null){
					html += '<h4 style="color: lightslategrey;">'+Utils.XSSEncode(temp.shopCode)+' - '+
						Utils.XSSEncode(temp.shopName)+'</h4>';
					tbhvName=temp.staffName;
				}
				html += '<div class="MPContent"><dl class="Dl1Style"><dt>Vị trí cập nhật lúc:</dt><dd>'+
					Utils.XSSEncode(timeUpdate)+'</dd><dt>Độ chính xác:</dt><dd>'+dochinhxac+' (m)</dd></dl>';
				html += '<div class="GeneralTable Table1Section">';
				html += '<table width="100%" border="0" cellspacing="0" cellpadding="0"><colgroup><col style="width:105px;" /><col style="width:87px;" /><col style="width:86px;" /><col style="width:53px;" /></colgroup><thead><tr><th class="FirstThStyle">Nội dung</th><th>Kế hoạch</th><th>Thực hiện</th><th>Duyệt</th><th class="EndThStyle">Tiến độ</th></tr></thead><tbody><tr><td class="FirstTdStyle"><div class="AlignLCols">';
				html += '	<a href="javascript:void(0)" onclick="SuperviseSales.showDialogDaySaleTBHV(this,'+tbhvId+',\''+
					Utils.XSSEncode(tbhvCode)+'\',\''+Utils.XSSEncode(tbhvName)+'\')">Doanh số ngày</a></div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(dayAmountPlan)+'</div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(dayAmount)+'</div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(dayAmountApproved)+'</div></td>';
				html += '<td><div class="AlignCCols">'+formatCurrency(dayTiendo)+'%</div></td></tr>';
				html += '<tr><td  class="FirstTdStyle"><div class="AlignLCols"><a href="javascript:void(0)" onclick="SuperviseSales.showDialogMonthAmountPlan(this,7,'+tbhvId+')">Lũy kế</a></div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(monthAmountPlan)+'</div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(monthAmount)+'</div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(monthAmountApproved)+'</div></td>';
				if(monthTiendo<tienDoChuan){
					html += '<td><div class="AlignCCols Color1Style">'+formatCurrency(monthTiendo)+'%</div></td>';
				}else{
					html += '<td><div class="AlignCCols">'+formatCurrency(monthTiendo)+'%</div></td>';
				}
				html += '</tr></tbody></table></div><div class="FixFloat ContentFunc"><p class="Text2Style" style="color:red">* Đơn vị (x 1000 vnđ)</p></div></div></div>';
				if(ViettelMap._currentInfoWindow != null) {
					ViettelMap._currentInfoWindow.close();
				}
							
				var infoWindow = new viettel.InfoWindow({
					position : pt,
					content : html,
					maxWidth : 350
				});
				infoWindow.open(ViettelMap._map);
				ViettelMap._currentInfoWindow = infoWindow;
			}
		});
	},
	showDialogNVGSInfo: function(nvgsId, nvgsCode, nvgsName, timeUpdate, dochinhxac,pt) {
		$.getJSON('/supervise/sales/getNVGSInfo?nvgsId='+nvgsId, function(data) {
			if (!data.error) {
				SuperviseSales._idStaffSelected = nvgsId;
				var dayAmountPlan = SuperviseSales.covertDSKH(data.dayAmountPlan);
				var dayAmountApproved = SuperviseSales.covertDSKH(data.dayAmountApproved);
				var dayAmount = SuperviseSales.covertDSKH(data.dayAmount);
				var dayQuantityPlan = SuperviseSales.covertDSKH(data.dayQuantityPlan);
				var dayQuantityApproved = SuperviseSales.covertDSKH(data.dayQuantityApproved);
				var dayQuantity = SuperviseSales.covertDSKH(data.dayQuantity);
				var phoneStaff = data.phoneStaff;
				if (!isNullOrEmpty(phoneStaff)) {
					phoneStaff = '<br /><span style="color: #333; font-weight: normal;">SĐT: </span><span style="color:red">' + Utils.XSSEncode(phoneStaff) + '</span>';
				}
				var tienDoNgayDS = 0;
				if (dayAmountPlan ==0) {
					tienDoNgayDS = 0;
				} else if (dayAmount == 0) {
					tienDoNgayDS = 0;
				} else {
					tienDoNgayDS = convertValueMath(Number(dayAmount) * 100 / Number(dayAmountPlan), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					//tienDoNgayDS = Math.ceil(Number(dayAmount) * 100 / Number(dayAmountPlan));
					/*if (tienDoNgayDS != 0) {
						tienDoNgayDS = tienDoNgayDS - 1;
					}*/
				}
				var tienDoNgaySL = 0;
				if (dayQuantityPlan ==0) {
					tienDoNgaySL = 0;
				} else if (dayQuantity == 0) {
					tienDoNgaySL = 0;
				} else {
					tienDoNgaySL = convertValueMath(Number(dayQuantity) * 100 / Number(dayQuantityPlan), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					//tienDoNgaySL = Math.ceil(Number(dayQuantity) * 100 / Number(dayQuantityPlan));
					/*if (tienDoNgaySL != 0) {
						tienDoNgaySL = tienDoNgaySL - 1;
					}*/
				}
				var monthAmountPlan = SuperviseSales.covertDSKH(data.monthAmountPlan); 
				var monthAmountApproved = SuperviseSales.covertDSKH(data.monthAmountApproved);
				var monthAmount = SuperviseSales.covertDSKH(data.monthAmount);
				var monthQuantityPlan = SuperviseSales.covertDSKH(data.monthQuantityPlan);
				var monthQuantityApproved = SuperviseSales.covertDSKH(data.monthQuantityApproved);
				var monthQuantity = SuperviseSales.covertDSKH(data.monthQuantity);
				var tienDoLuyKeDS = 0;
				if (monthAmountPlan ==0) {
					tienDoLuyKeDS = 0;
				} else if (monthAmount == 0) {
					tienDoLuyKeDS = 0;
				} else {
					tienDoLuyKeDS = convertValueMath(Number(monthAmount) * 100 / Number(monthAmountPlan), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					//tienDoLuyKeDS = Math.ceil(Number(monthAmount) * 100 / Number(monthAmountPlan));
					/*if (tienDoLuyKeDS != 0 && (monthAmount % monthAmountPlan) != 0) {
						tienDoLuyKeDS = tienDoLuyKeDS - 1;
					}*/
				}
				var tienDoLuyKeSL = 0;
				if (monthQuantityPlan ==0) {
					tienDoLuyKeSL = 0;
				} else if(monthQuantity == 0) {
					tienDoLuyKeSL = 0;
				} else {
					tienDoLuyKeSL = convertValueMath(Number(monthQuantity) * 100 / Number(monthQuantityPlan), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					//tienDoLuyKeSL = Math.ceil(Number(monthQuantity) * 100 / Number(monthQuantityPlan));
					/*if (tienDoLuyKeSL != 0 && (monthAmount % monthAmountPlan) != 0) {
						tienDoLuyKeSL = tienDoLuyKeSL - 1;
					}*/
				}
				timeUpdate = SuperviseSales.covertValueStr(timeUpdate);
				dochinhxac = SuperviseSales.covertValueStr(dochinhxac);
				var tienDoChuan = SuperviseSales.covertDSKH(data.tienDoChuan);
				var batPercent = SuperviseSales.covertDSKH(data.pin);
				var networkType = data.networkType;
				var networkStrength = data.networkSpeed;
				var netStr = '';
				if (networkType==undefined || networkType==null || networkType=='null') {
					networkType = '';
				}
				if (networkStrength==undefined || networkStrength==null || networkStrength=='null') {
					networkStrength = '';
				}
				if (!Utils.isEmpty(networkType)) {
					netStr = networkType;
				}
				if (!Utils.isEmpty(netStr)) {
					if (Number(networkStrength) == 0) {
						netStr = netStr+" - "+msgNetSignWeak;
					} else if (Number(networkStrength) == 1) {
						netStr = netStr+" - "+msgNetSignStrong;
					} else {
						netStr = netStr+" - "+msgNetSignVeryStrong;
					}
				}
				/*var html = '<div class="MapPopupSection MapPopup1Section">';
				html += '<h2 class="Title2Style">'+Utils.XSSEncode(nvgsCode)+' - '+
					Utils.XSSEncode(nvgsName)+'</h2>';
				var temp = SuperviseSales._lstStaffPosition.get(nvgsId);
				if(temp!=null){
					html += '<h4 style="color: lightslategrey;">'+
						Utils.XSSEncode(temp.shopCode)+' - '+Utils.XSSEncode(temp.shopName)+'</h4>';
					nvgsName=temp.staffName;
				}
				html += '<div class="MPContent"><dl class="Dl1Style"><dt>Vị trí cập nhật lúc:</dt><dd>'+
					Utils.XSSEncode(timeUpdate)+'</dd><dt>Độ chính xác:</dt><dd>'+dochinhxac+' (m)</dd></dl>';
				html += '<div class="GeneralTable Table1Section">';
				html += '<table width="100%" border="0" cellspacing="0" cellpadding="0"><colgroup><col style="width:105px;" /><col style="width:87px;" /><col style="width:86px;" /><col style="width:53px;" /></colgroup><thead><tr><th class="FirstThStyle">Nội dung</th><th>Kế hoạch</th><th>Thực hiện</th><th>Duyệt</th><th class="EndThStyle">Tiến độ</th></tr></thead><tbody><tr><td class="FirstTdStyle">';
				html += '	<div class="AlignLCols"><a href="javascript:void(0)" onclick="SuperviseSales.showDialogDaySaleNVGS(this,'+nvgsId+',\''+
					Utils.XSSEncode(nvgsCode)+'\',\''+Utils.XSSEncode(nvgsName)+'\')">Doanh số ngày</a></div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(dayAmountPlan)+'</div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(dayAmount)+'</div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(dayAmountApproved)+'</div></td>';
				html += '<td><div class="AlignCCols">'+formatCurrency(dayTiendo)+'%</div></td></tr>';
				html += '<tr><td  class="FirstTdStyle"><div class="AlignLCols"><a href="javascript:void(0)" onclick="SuperviseSales.showDialogMonthAmountPlan(this,5,'+nvgsId+')">Lũy kế tháng</a></div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(monthAmountPlan)+'</div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(monthAmount)+'</div></td>';
				html += '<td><div class="AlignRCols">'+formatCurrency(monthAmountApproved)+'</div></td>';
				if(monthTiendo<tienDoChuan){
					html += '<td><div class="AlignCCols Color1Style">'+formatCurrency(monthTiendo)+'%</div></td>';
				}else{
					html += '<td><div class="AlignCCols">'+formatCurrency(monthTiendo)+'%</div></td>';
				}
				html += '</tr></tbody></table></div><div class="FixFloat ContentFunc"><p class="Text2Style" style="color:red">* Đơn vị (x 1000 vnđ)</p></div></div></div>';*/
				//SangTN-VTMap
				//Vietbando._map.openInfoWindow(pt,html);
				var html = '<div class="MapPopupSection MapPopup4Section">';
				html += '<h2 class="Title2Style">' + Utils.XSSEncode(nvgsCode) + ' - ' + Utils.XSSEncode(nvgsName) + phoneStaff + '</h2>';
				var temp = SuperviseSales._lstStaffPosition.get(nvgsId);
				if (temp != null) {
					html += '<h4 style="color: lightslategrey;">'+Utils.XSSEncode(temp.shopCode)+' - '+Utils.XSSEncode(temp.shopName)+'</h4>';
					nvgsName = temp.staffName;
				}
				html += '<div class="MPContent"><dl class="Dl1Style">';
				html += '<dt>Vị trí cập nhật lúc: </dt>';
				html += '<dd>'+Utils.XSSEncode(timeUpdate)+'</dd>';
				html += '<dt>Độ chính xác: </dt>';
				html += '<dd>'+dochinhxac+' (m)<br /></dd>';
				html += '<dt><img src = "/resources/images/battery.png" width="30px" height="30px" /></dt>';
				html += '<dd>  '+Utils.XSSEncode(batPercent)+'%</dd>';
				if (!Utils.isEmpty(netStr)) {
					//html += '<dt><img src = "/resources/images/wifi.png" width="30px" height="30px" /></dt>';
					html += '<dd> '+Utils.XSSEncode(netStr)+'<br /></dd>';
				} else {
					html += '<br />';
				}
				/*html += '<dt>Đang ghé thăm: </dt>';
				html += '<dd>'+Utils.XSSEncode(custVisit)+', '+Utils.XSSEncode(custVisitAddr)+'<br /></dd>';
				html += '<dt>Địa chỉ: </dt>';
				html += '<dd>'+custVisitAddr+'</dd>';*/
				html += '</dl>';
				html += '<div class="GeneralTable Table1Section" style="width: 100%;" >';
				html += '	<table width="100%" border="0" cellspacing="0" cellpadding="0">';
				html += '		<colgroup>';
				html += '			<col style="width: 20%;" />';
				html += '			<col style="width: 15%;" />';
				html += '			<col style="width: 15%;" />';
				html += '			<col style="width: 15%;" />';
				html += '			<col style="width: 15%;" />';
				html += '			<col style="width: 12%;" />';
				html += '		</colgroup>';
				html += '		<thead>';
				html += '			<tr>';
				html += '				<th class="FirstThStyle" colspan="2" style="padding: 3px 0;">Nội dung</th>';
				html += '				<th style="padding: 3px 0;">Kế hoạch</th>';
				html += '				<th style="padding: 3px 0;">Thực hiện <br />(Đã duyệt)</th>';
				html += '				<th style="padding: 3px 0;">Thực hiện (Tổng)</th>';
				html += '				<th class="EndThStyle" style="padding: 3px 0;">Tiến độ</th>';
				html += '			</tr>';
				html += '		</thead>';
				html += '		<tbody>';
				html += '			<tr style="height: 22px;">';
				html += '				<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">';
				html += '					<div class="AlignLCols TextOnMap">';
				html += '						<a href="javascript:void(0)" onclick="SuperviseSales.showDialogDaySaleNVGS(this,'+nvgsId+',\''+Utils.XSSEncode(nvgsCode)+'\',\''+Utils.XSSEncode(nvgsName)+'\',\''+SuperviseSales._NGAY+'\')">Ngày hiện tại</a>';
				html += '					</div>';
				html += '				</td>';
				html += '				<td  style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Doanh số</div></td>';
				html += '				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" >'+formatCurrency(dayAmountPlan)+'</div></td>';
				html += '				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" >'+formatCurrency(dayAmountApproved)+'</div></td>';
				html += '				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" >'+formatCurrency(dayAmount)+'</div></td>';
				html += '				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" >'+formatCurrency(tienDoNgayDS)+'%</div></td>';					
				html += '			</tr>';
				html += '			<tr style="height: 22px;">';
				html += '				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Sản lượng</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(dayQuantityPlan)+'</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(dayQuantityApproved)+'</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(dayQuantity)+'</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(tienDoNgaySL)+'%</div></td>';
				html += '			</tr>';
				html += '			<tr style="height: 22px;">';
				html += '				<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">';
				//html += '					<div class="AlignLCols TextOnMap"><a href="javascript:void(0)" onclick="SuperviseSales.showDialogMonthAmountPlan(this, 5, '+nvgsId+')">Lũy kế tháng</a></div>';
				html += '					<div class="AlignLCols TextOnMap"><a href="javascript:void(0)" onclick="SuperviseSales.showDialogDaySaleNVGS(this,'+nvgsId+',\''+Utils.XSSEncode(nvgsCode)+'\',\''+Utils.XSSEncode(nvgsName)+'\',\''+SuperviseSales._LUY_KE+'\')">Lũy kế</a>';
				html += '				</td>';
				html += '				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Doanh số</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(monthAmountPlan)+'</div></td>';
				html += '		        <td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(monthAmountApproved)+'</div></td>';
				html += '		        <td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(monthAmount)+'</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(tienDoLuyKeDS)+'%</div></td>';
				html += '			</tr>';
				html += '			<tr style="height: 22px;">';
				html += '				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Sản lượng</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(monthQuantityPlan)+'</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(monthQuantityApproved)+'</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(monthQuantity)+'</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">'+formatCurrency(tienDoLuyKeSL)+'%</div></td>';
				html += '			</tr>';
				html += '		</tbody>';
				html += '	</table>';
				html += '</div>';
				html += '<div class="FixFloat ContentFunc"><p class="Text2Style" style="color:red">* Đơn vị (x 1000 vnđ)</p></div>';
				html += '</div></div>';
				if (ViettelMap._currentInfoWindow != null) {
					ViettelMap._currentInfoWindow.close();
				}
				var infoWindow = new viettel.InfoWindow({
					content: html,
					position: pt,
					maxWidth: 500,
					maxHeight: 400,
					//maxWidth: 350
				});								
				
				infoWindow.open(ViettelMap._map);
				ViettelMap._currentInfoWindow = infoWindow;				
			}
		});
	},
	showDialogNVBHInfo: function(nvbhId, nvbhCode, nvbhName, timeUpdate, dochinhxac, pt) {
		$.getJSON('/supervise/sales/getNVBHInfo?nvbhId=' + nvbhId, function(data) {
			if (!data.error) {
				SuperviseSales._idStaffSelected = nvbhId;
				var dayAmountPlan = SuperviseSales.covertDSKH(data.dayAmountPlan);
				var dayAmountApproved = SuperviseSales.covertDSKH(data.dayAmountApproved);
				var dayAmount = SuperviseSales.covertDSKH(data.dayAmount);
				var dayQuantityPlan = SuperviseSales.covertDSKH(data.dayQuantityPlan);
				var dayQuantityApproved = SuperviseSales.covertDSKH(data.dayQuantityApproved);
				var dayQuantity = SuperviseSales.covertDSKH(data.dayQuantity);
				var custVisit = SuperviseSales.covertValueStr(data.custVisit);
				var custVisitAddr = SuperviseSales.covertValueStr(data.custVisitAddr);
				var phoneStaff = data.phoneStaff;
				if (!isNullOrEmpty(phoneStaff)) {
					phoneStaff = '<br /><span style="color: #333; font-weight: normal;">SĐT: </span><span style="color:red">' + Utils.XSSEncode(phoneStaff) + '</span>';
				}
				var tienDoNgayDS = 0;
				if (dayAmountPlan == 0) {
					tienDoNgayDS = 0;
				} else if (dayAmount == 0) {
					tienDoNgayDS = 0;
				} else {
					tienDoNgayDS = convertValueMath(Number(dayAmount) * 100 / Number(dayAmountPlan), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					//tienDoNgayDS = Math.ceil(Number(dayAmount) * 100 / Number(dayAmountPlan));
					/*if (tienDoNgayDS != 0) {
						tienDoNgayDS = tienDoNgayDS - 1;
					}*/
				}
				var tienDoNgaySL = 0;
				if (dayQuantityPlan == 0) {
					tienDoNgaySL = 0;
				} else if (dayQuantity == 0) {
					tienDoNgaySL = 0;
				} else {
					tienDoNgaySL = convertValueMath(Number(dayQuantity) * 100 / Number(dayQuantityPlan), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					//tienDoNgaySL = Math.ceil(Number(dayQuantity) * 100 / Number(dayQuantityPlan));
					/*if (tienDoNgaySL != 0) {
						tienDoNgaySL = tienDoNgaySL - 1;
					}*/
				}
				var monthAmountPlan = SuperviseSales.covertDSKH(data.monthAmountPlan); 
				var monthAmountApproved = SuperviseSales.covertDSKH(data.monthAmountApproved);
				var monthAmount = SuperviseSales.covertDSKH(data.monthAmount);
				var monthQuantityPlan = SuperviseSales.covertDSKH(data.monthQuantityPlan);
				var monthQuantityApproved = SuperviseSales.covertDSKH(data.monthQuantityApproved);
				var monthQuantity = SuperviseSales.covertDSKH(data.monthQuantity);
				var tienDoLuyKeDS = 0;
				if (monthAmountPlan == 0) {
					tienDoLuyKeDS = 0;
				} else if (monthAmount == 0) {
					tienDoLuyKeDS = 0;
				} else {
					tienDoLuyKeDS = convertValueMath(Number(monthAmount) * 100 / Number(monthAmountPlan), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					//tienDoLuyKeDS = Math.ceil(Number(monthAmount) * 100 / Number(monthAmountPlan));
					/*if (tienDoLuyKeDS != 0 && (monthAmount % monthAmountPlan) != 0) {
						tienDoLuyKeDS = tienDoLuyKeDS - 1;
					}*/
				}
				var tienDoLuyKeSL = 0;
				if (monthQuantityPlan == 0) {
					tienDoLuyKeSL = 0;
				} else if (monthQuantity == 0) {
					tienDoLuyKeSL = 0;
				} else {
					tienDoLuyKeSL = convertValueMath(Number(monthQuantity) * 100 / Number(monthQuantityPlan), Utils._MATH_ROUND, SuperviseSales._EXTENTION_VALUE);
					//tienDoLuyKeSL = Math.ceil(Number(monthQuantity) * 100 / Number(monthQuantityPlan));
					/*if (tienDoLuyKeSL != 0 && (monthAmount % monthAmountPlan) != 0) {
						tienDoLuyKeSL = tienDoLuyKeSL - 1;
					}*/
				}
				timeUpdate = SuperviseSales.covertValueStr(timeUpdate);
				dochinhxac = SuperviseSales.covertValueStr(dochinhxac);
				var tienDoChuan = SuperviseSales.covertDSKH(data.tienDoChuan);
				var batPercent = SuperviseSales.covertDSKH(data.pin);
				var networkType = SuperviseSales.covertValueStr(data.networkType);
				var networkStrength = SuperviseSales.covertValueStr(data.networkSpeed);
				var netStr = '';
				if (!Utils.isEmpty(networkType)) {
					netStr = networkType;
				}
				if (!Utils.isEmpty(netStr)) {
					if (Number(networkStrength) == 0) {
						netStr = netStr + ' - ' + msgNetSignWeak;
					} else if (Number(networkStrength) == 1) {
						netStr = netStr + ' - ' + msgNetSignStrong;
					} else {
						netStr = netStr + ' - ' + msgNetSignVeryStrong;
					}
				}
				var html = '<div class="MapPopupSection MapPopup4Section">';
				html += '<h2 class="Title2Style">' + Utils.XSSEncode(nvbhCode) + ' - ' + Utils.XSSEncode(nvbhName) + phoneStaff + '</h2>';
				var temp = SuperviseSales._lstStaffPosition.get(nvbhId);
				if (temp != null) {
					html += '<h4 style="color: lightslategrey;">' + Utils.XSSEncode(temp.shopCode) + ' - ' + Utils.XSSEncode(temp.shopName) + '</h4>';
					nvbhName = temp.staffName;
				}
				html += '<div class="MPContent"><dl class="Dl1Style">';
				html += '<dt>Vị trí cập nhật lúc: </dt>';
				html += '<dd>' + Utils.XSSEncode(timeUpdate) + '</dd>';
				html += '<dt>Độ chính xác: </dt>';
				html += '<dd>' + dochinhxac + ' (m)<br /></dd>';
				html += '<dt><img src = "/resources/images/battery.png" width="30px" height="30px" /></dt>';
				html += '<dd>  ' + Utils.XSSEncode(batPercent) + '%</dd>';
				if (!Utils.isEmpty(netStr)) {
					//html += '<dt><img src = "/resources/images/wifi.png" width="30px" height="30px" /></dt>';
					html += '<dd> ' + Utils.XSSEncode(netStr) + '<br /></dd>';
				} else {
					html += '<br />';
				}
				html += '<dt>Đang ghé thăm: </dt>';
				html += '<dd>' + Utils.XSSEncode(custVisit) + ', ' + Utils.XSSEncode(custVisitAddr) + '<br /></dd>';
				/*html += '<dt>Địa chỉ:</dt>';
				html += '<dd>' + custVisitAddr + '</dd>';*/
				html += '</dl>';
				html += '<div class="GeneralTable Table1Section" style="width: 100%;" >';
				html += '	<table width="100%" border="0" cellspacing="0" cellpadding="0">';
				html += '		<colgroup>';
				html += '			<col style="width: 20%;" />';
				html += '			<col style="width: 15%;" />';
				html += '			<col style="width: 15%;" />';
				html += '			<col style="width: 15%;" />';
				html += '			<col style="width: 15%;" />';
				html += '			<col style="width: 12%;" />';
				html += '		</colgroup>';
				html += '		<thead>';
				html += '			<tr>';
				html += '				<th class="FirstThStyle" colspan="2" style="padding: 3px 0;">Nội dung</th>';
				html += '				<th style="padding: 3px 0;">Kế hoạch</th>';
				html += '				<th style="padding: 3px 0;">Thực hiện <br />(Đã duyệt)</th>';
				html += '				<th style="padding: 3px 0;">Thực hiện (Tổng)</th>';
				html += '				<th class="EndThStyle" style="padding: 3px 0;">Tiến độ</th>';
				html += '			</tr>';
				html += '		</thead>';
				html += '		<tbody>';
				html += '			<tr style="height: 22px;">';
				html += '				<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">';
				html += '					<div class="AlignLCols TextOnMap">';
				html += '						<a href="javascript:void(0)" onclick="SuperviseSales.showDialogDaySaleNVBH(this,' + nvbhId + ',\'' + Utils.XSSEncode(nvbhCode) + '\',\'' + Utils.XSSEncode(nvbhName) + '\',\'' + SuperviseSales._NGAY + '\')">Ngày hiện tại</a>';
				html += '					</div>';
				html += '				</td>';
				html += '				<td  style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Doanh số</div></td>';
				html += '				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" >' + formatCurrency(dayAmountPlan) + '</div></td>';
				html += '				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" >' + formatCurrency(dayAmountApproved) + '</div></td>';
				html += '				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" >' + formatCurrency(dayAmount) + '</div></td>';
				html += '				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" >' + formatCurrency(tienDoNgayDS) + '%</div></td>';					
				html += '			</tr>';
				html += '			<tr style="height: 22px;">';
				html += '				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Sản lượng</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(dayQuantityPlan) + '</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(dayQuantityApproved) + '</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(dayQuantity) + '</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(tienDoNgaySL) + '%</div></td>';
				html += '			</tr>';
				html += '			<tr style="height: 22px;">';
				html += '				<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">';
				html += '					<div class="AlignLCols TextOnMap">';
				html += '						<a href="javascript:void(0)" onclick="SuperviseSales.showDialogDaySaleNVBH(this,' + nvbhId + ',\'' + Utils.XSSEncode(nvbhCode) + '\',\'' + Utils.XSSEncode(nvbhName) + '\',\'' + SuperviseSales._LUY_KE + '\')">Lũy kế</a>';
				html += '					</div>';
				html += '				</td>';
				html += '				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Doanh số</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthAmountPlan) + '</div></td>';
				html += '		        <td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthAmountApproved) + '</div></td>';
				html += '		        <td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthAmount) + '</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(tienDoLuyKeDS) + '%</div></td>';
				html += '			</tr>';
				html += '			<tr style="height: 22px;">';
				html += '				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Sản lượng</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthQuantityPlan) + '</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthQuantityApproved) + '</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthQuantity) + '</div></td>';
				html += '				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(tienDoLuyKeSL) + '%</div></td>';
				html += '			</tr>';
				html += '		</tbody>';
				html += '	</table>';
				html += '</div>';
				html += '<div class="FixFloat ContentFunc"><p class="Text1Style"><a href="javascript:void(0)" onclick="SuperviseSales.showListCustomerByVisitPlan(' + nvbhId + ',\'' + Utils.XSSEncode(nvbhName) + '\',' + temp.roleType + ');" class="Sprite1">Xem lộ trình</a><p class="Text2Style" style="color:red">* Đơn vị (x 1000 vnđ)</p></div>';
				html += '</div></div>';
				if (ViettelMap._currentInfoWindow != null) {
					ViettelMap._currentInfoWindow.close();
				}
				var infoWindow = new viettel.InfoWindow({
					content: html,
					position: pt,
					maxWidth: 500,
					maxHeight: 400,
					//maxWidth: 350
				});								
				infoWindow.open(ViettelMap._map);
				ViettelMap._currentInfoWindow = infoWindow;
				/*if(SuperviseSales.customerViewer==null || SuperviseSales.customerViewer.length==0){
					$('.MPContent .Text1Style').html('Không có khách hàng viếng thăm');
				}*/
			}
		});
	},

	/**
	 * @author vuongmq
	 * @param value
	 * @return value, ngc lai: 0
	 * @since 18/08/2015 
	 */
	covertDSKH: function(value) {
		if (value == undefined || value == null || value < 0 ) {
			return 0;
		}
		return value;
	},
	
	/**
	 * @author vuongmq
	 * @param value
	 * @return String
	 * @since 18/08/2015 
	 */
	covertValueStr: function(value) {
		if (value == undefined || value == null) {
			return '';
		}
		return value;
	},
//	resetPathViewer:function(){
//		if (SuperviseSales.mapObject!=null) {
//			SuperviseSales.mapObject.setMap(null);
//			SuperviseSales.mapObject = null;
//			$('#item6').html('Vẽ lộ trình');
//			for(var i=0;i<SuperviseSales.listPointViewer.length;++i){
//				var obj = SuperviseSales.listPointViewer[i];
//				obj.setMap(null);
//			}
//    	}
//	},
	//HAM RESET LAI LO TRINH
	resetPathViewer:function(){
		$('#item6').html(Utils.XSSEncode(jsSaleSuperviseControlDrawPath));
		if (SuperviseSales._mapObject!=null && SuperviseSales._mapObject.length > 0) {
			for(var i = 0; i < SuperviseSales._mapObject.length; i++){
				SuperviseSales._mapObject[i].clear();
			}
			//SuperviseSales._mapObject = null;
		}
		SuperviseSales._mapObject = null;
		if(SuperviseSales.listPointViewer != null && SuperviseSales.listPointViewer.length >0){	
			for(var i=0;i<SuperviseSales.listPointViewer.length;++i){
				var obj = SuperviseSales.listPointViewer[i];
				obj.setMap(null);
			}
		}
		
	},
	//orignal
	/*showPathViewer: function(){
		if(SuperviseSales.mapObject==null){			
			$('#item6').html('Xóa lộ trình');
			var data = new Object();
			data.staffId = SuperviseSales.selectedStaffId;
			Utils.getJSONDataByAjaxNotOverlay(data,'/supervise/sales/show-direction-staff',function(data){
				SuperviseSales.customerViewer = data.list;
				if(SuperviseSales.customerViewer!=null && SuperviseSales.customerViewer.length>0){
					var listLatLngs = SuperviseSales.customerViewer;
					var points = new Array();
					SuperviseSales.listPointViewer = new Array();
					for(var i=0,size=listLatLngs.length;i<size;++i){
						var p = listLatLngs[i];
						points.push(new viettel.LatLng(parseFloat(p.lat), parseFloat(p.lng)));
						var pt = new viettel.LatLng(p.lat, p.lng);
						var urlImage = '/resources/images/Mappin/1378843002_circle_red.png';
						if(i==0){
							urlImage = '/resources/images/Mappin/1378843009_circle_blue.png';
						}						
						var marker = new viettel.LabelMarker({
							icon:{
								url : urlImage,
								size : {height : 10, width : 10},
								scaledSize : {height :10, width : 10}
							},
							position : pt,
							map : ViettelMap._map,						
							draggable : false,
							labelAnchor : new viettel.Point(25, 0)
						});
						SuperviseSales.listPointViewer.push(marker);
					}   
					if (SuperviseSales.mapObject!=null) {
						SuperviseSales.mapObject.setMap(null);
						SuperviseSales.mapObject = null;
	            	}				
					SuperviseSales.mapObject = new viettel.Polyline({
		                path: points,
		                strokeColor: "#CC0000",
		                strokeOpacity: 0.5,
		                strokeWeight: 3,
		                clickable: true,
		                map: ViettelMap._map
		            });					
				}
			});		
		}else{
			SuperviseSales.resetPathViewer();
		}
	},*/
	showPathViewer: function() {
		if (SuperviseSales._mapObject == null) {
			$('#divOverlay').show();
			SuperviseSales.isDraw = true;
			$('#item6').html(Utils.XSSEncode(jsSaleSuperviseSalerPathDeletePathText));
			var data = new Object();
			// data.staffId = SuperviseSales.selectedStaffId;
			data.staffId = SuperviseSales._idStaffSelected;
			data.checkDate = $('#startDate').val().trim();
			if ($('#fromTime').val() == '') {
				data.fromTime = '07:00';
			} else {
				data.fromTime = $('#fromTime').val().trim();
			}
			if ($('#toTime').val() == '') {
				data.toTime = '18:00';
			} else {
				data.toTime = $('#toTime').val().trim();
			}
			SuperviseSales.listPathView = new Array();
			Utils.getJSONDataByAjaxNotOverlay(data, '/supervise/sales/calc-shortest-distance', function(data) {
				SuperviseSales.customerViewer = data.list;
				SuperviseSales.listPointViewer = new Array();
				for (var i = 0, size = SuperviseSales.customerViewer.length; i < size; ++i) {
					var p = SuperviseSales.customerViewer[i];
					SuperviseSales.showDirectionStaff(p, i);
//					if (SuperviseSales.numPointLine == 0) {
//						var isPoint = false;
//						if (p.isOr == 1) {// kh ngoai tuyen
//							isPoint = true;
//						} else if (p.count != null && p.count >= 1) {// co don hang
//							isPoint = true;
//						} else if (p.objectType == 0 && p.endTime == null) {// dang ghe tham
//							isPoint = true;
//						} else if (p.objectType == 0 || p.objectType == 1) {
//							isPoint = true;
//						}
//						if (ViettelMap.isValidLatLng(p.lat, p.lng) && isPoint) {
//							SuperviseSales.showDirectionStaff(p, i);
//						}
//					} else {
//						SuperviseSales.showDirectionStaff(p, i);
//					}
				}
				if (SuperviseSales._mapObject != null && SuperviseSales._mapObject.length > 0) {
					for ( var i = 0; i < SuperviseSales._mapObject.length; i++) {
						SuperviseSales._mapObject[i].clear();
					}
					SuperviseSales._mapObject = null;
				}
				SuperviseSales._mapObject = new Array();
				for (var i = 0; i < SuperviseSales.listPathView.length - 1; i++) {
					if (SuperviseSales.listPathView.length > 1) {
						var row = SuperviseSales.listPathView[i];
						var nextRow = SuperviseSales.listPathView[i + 1];
						SuperviseSales.calcRoute(row, nextRow);
					}
				}
				$('#divOverlay').hide();
			});
		} else {
			SuperviseSales.resetPathViewer();
			SuperviseSales.isDraw = false;
		}
	},
//	showPathViewer: function(){
//		if(SuperviseSales.mapObject==null){			
//			$('#item6').html('Xóa lộ trình');
//			var data = new Object();
//			data.staffId = SuperviseSales.selectedStaffId;
//			Utils.getJSONDataByAjaxNotOverlay(data,'/supervise/sales/show-direction-staff',function(data){
//				SuperviseSales.customerViewer = data.list;
//				if(SuperviseSales.customerViewer!=null && SuperviseSales.customerViewer.length>0){
//					var listLatLngs = SuperviseSales.customerViewer;
//					var points = new Array();
//					SuperviseSales.listPointViewer = new Array();
//					for(var i=0,size=listLatLngs.length;i<size;++i){
//						var p = listLatLngs[i];
//						points.push(new viettel.LatLng(parseFloat(p.lat), parseFloat(p.lng)));
//						var pt = new viettel.LatLng(p.lat, p.lng);
//						var urlImage = '/resources/images/Mappin/1378843002_circle_red.png';
//						if(i==0){
//							urlImage = '/resources/images/Mappin/1378843009_circle_blue.png';
//						}						
//						/*var marker = new viettel.LabelMarker({
//							icon:{
//								url : urlImage,
//								size : {height : 10, width : 10},
//								scaledSize : {height :10, width : 10}
//							},
//							position : pt,
//							map : ViettelMap._map,						
//							draggable : false,
//							labelAnchor : new viettel.Point(25, 0)
//						});
//						SuperviseSales.listPointViewer.push(marker);*/
//						
//						SuperviseSales.showDirectionStaff(pt,i);
//					}   
//					if (SuperviseSales.mapObject!=null) {
//						SuperviseSales.mapObject.setMap(null);
//						SuperviseSales.mapObject = null;
//	            	}				
//					/*SuperviseSales.mapObject = new viettel.Polyline({
//		                path: points,
//		                strokeColor: "#CC0000",
//		                strokeOpacity: 0.5,
//		                strokeWeight: 3,
//		                clickable: true,
//		                map: ViettelMap._map
//		            });	*/				
//				}
//			});		
//		}else{
//			SuperviseSales.resetPathViewer();
//		}
//	},
	
	showDirectionStaff:function(p,i){
		var obj = new Object();
		obj.lat = p.lat;
		obj.lng = p.lng;
//		obj.time = p.timeHHMM;
		SuperviseSales.listPathView.push(obj);
		var pt = new viettel.LatLng(p.lat, p.lng);
		var urlImage = '/resources/images/Mappin/middle_point.png';
		var widthSize = 10;
		var heightSize = 10;
		if(i==0){
			urlImage = '/resources/images/Mappin/start_point.png';
			widthSize = 50;
			heightSize = 50;
		}	
		var marker = new viettel.LabelMarker({
			icon:{
				url : urlImage,
				size : {height : heightSize, width : widthSize},
				scaledSize : {height :heightSize, width : widthSize}
			},
			position : pt,
			map : ViettelMap._map,						
			draggable : false,
			labelAnchor : new viettel.Point(25, 0)
		});
		marker.lat = p.lat;
		marker.lng = p.lng;
		//marker.timeHHMM = p.timeHHMM;
		
		/*var timepart = p.createDate != null && p.createDate != undefined ? (p.createDate.indexOf('T') != -1 ? p.createDate.split('T') : (p.createDate.indexOf(' ') != -1 ? p.createDate.split(' ') : '')) : '';
		var timearr = timepart[1] != null && timepart[1] != undefined ? timepart[1].split(':') : '';
		marker.timeHHMM = (timearr[0] != null && timearr[0] != undefined ? timearr[0] : '') + ':' + (timearr[1] != null && timearr[1] != undefined ? timearr[1] : '');*/
		
		
		SuperviseSales.listPointViewer.push(marker);
		viettel.Events.addListener(marker, "click", function(evt) {
			marker.setTitle('title');
			var point = new viettel.LatLng(this.lat, this.lng);
//			var html = '<div style="margin-top: 5px; margin-bottom: 5px;">' + Utils.XSSEncode(jsSaleSuperviseSalerPathPointPopupText) + ': ' + this.timeHHMM + '</div>';
//			ViettelMap.showInfoWindow(point, html, 200);
		});
	},
	//end copy from dmslite
	
	/***
	* Lay HTML cua Popup KH
	* @author vuongmq
	* @param customer
	* @since 19/08/2015
	*/
	viewHTMLCustomerOfStaff: function(customer) {
		var diaChi = SuperviseSales.covertValueStr(customer.address);
		var diDong = SuperviseSales.covertValueStr(customer.mobiphone);
		var coDinh = SuperviseSales.covertValueStr(customer.phone);
		var dayAmountApproved = SuperviseSales.covertDSKH(customer.dayAmountApproved);
		var dayAmount = SuperviseSales.covertDSKH(customer.dayAmount);
		var dayQuantityApproved = SuperviseSales.covertDSKH(customer.dayQuantityApproved);
		var dayQuantity = SuperviseSales.covertDSKH(customer.dayQuantity);
		var monthAmountApproved = SuperviseSales.covertDSKH(customer.monthAmountApproved);
		var monthAmount = SuperviseSales.covertDSKH(customer.monthAmount);
		var monthQuantityApproved = SuperviseSales.covertDSKH(customer.monthQuantityApproved);
		var monthQuantity = SuperviseSales.covertDSKH(customer.monthQuantity);
		var dsGheTham = customer.lstDetailActionLog;
		var html = '';
		html += '	<div class="MapPopupSection MapPopup4Section">';
		html += '	<h2 class="Title2Style" id="h2CustomerCode">' + Utils.XSSEncode(customer.shortCode) + ' - ' + Utils.XSSEncode(customer.customerName) + '</h2>';
		html += '	<div class="MPContent">';
		html += '		<dl class="Dl1Style FixFloat">';
		html += '			<dt>Địa chỉ: </dt>';
		html += '			<dd id="ddAddress">' + diaChi + '</dd><br />';
		html += '			<dt>Di động: </dt>';
		html += '			<dd id="ddMobiPhone">' + diDong + '</dd><br />';
		html += '			<dt>Cố định: </dt>';
		html += '			<dd id="ddPhone">' + coDinh + '</dd>';
		html += '		</dl>';
		html += '		<div class="GeneralTable Table1Section" style="width: 100%;" >';
		html += '			<div id="tableDoanhSo" class="FixFloat" style="float: left; width: 70%">';
		html += '				<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		html += '	           	<colgroup>';
		html += '	           		<col style="width:77px;" />';
		html += '	           		<col style="width:55px;" />';
		html += '	           		<col style="width:70px;" />';
		html += '	           		<col style="width:70px;" />';
		html += '	           	</colgroup>';
		html += '	           	<thead>';
		html += '		           	<tr>';
		html += '			           	<th class="FirstThStyle EndTopThStyle" colspan="4" style="padding: 3px 0;">Doanh số</th>';
		html += '		           	</tr>';
		html += '					<tr>';
		html += '						<th class="FirstThStyle" colspan="2" style="padding: 3px 0;">Nội dung</th>';
		html += '						<th style="padding: 3px 0;">Đã duyệt</th>';
		html += '						<th style="padding: 3px 0;">Tổng</th>';
		html += '					</tr>';
		html += '				</thead>';
		html += '	               <tbody>';
		html += '	                <tr style="height: 22px;">';
		html += '						<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">';
		html += '							<div class="AlignLCols TextOnMap">Ngày hiện tại</div>';
		html += '						</td>';
		html += '						<td  style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Doanh số</div></td>';
		html += '						<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(dayAmountApproved) + '</div></td>';
		html += '						<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(dayAmount) + '</div></td>';
		html += '					</tr>';
		html += '					<tr style="height: 22px;">';
		html += '						<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Sản lượng</div></td>';
		html += '						<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(dayQuantityApproved) + '</div></td>';
		html += '						<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(dayQuantity) + '</div></td>';
		html += '						';
		html += '					</tr>';
		html += '					<tr style="height: 22px;">';
		html += '						<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">';
		html += '							<div class="AlignLCols TextOnMap">Lũy kế</div>';
		html += '						</td>';
		html += '						<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Doanh số</div></td>';
		html += '						<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthAmountApproved) + '</div></td>';
		html += '						<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthAmount) + '</div></td>';
		html += '					</tr>';
		html += '					<tr style="height: 22px;">';
		html += '						<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Sản lượng</div></td>';
		html += '						<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthQuantityApproved) + '</div></td>';
		html += '						<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;">' + formatCurrency(monthQuantity) + '</div></td>';
		html += '					</tr>';
		html += '	               </tbody>';
		html += '	           </table>';
		html += '           </div>';
		html += '           <div id="tableGheTham" class="FixFloat" style="float: left; width: 30%">';
		html += '				<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		html += '	           	<colgroup>';
		html += '	           		<col style="width:50px;" />';
		html += '	           		<col style="width:50px;" />';
		html += '	           	</colgroup>';
		html += '	           	<thead>';
		html += '		           	<tr>';
		html += '			           	<th class="FirstThStyle EndTopThStyle" colspan="2"style="padding: 3px 0;">Ghé thăm</th>';
		html += '		           	</tr>';
		html += '					<tr>';
		html += '						<th class="FirstThStyle"style="padding: 3px 0;">Bắt đầu</th>';
		html += '						<th style="padding: 3px 0;">Kết thúc</th>';
		html += '					</tr>';
		html += '				</thead>';
		html += '	               <tbody>';
		if (dsGheTham != undefined && dsGheTham != null && dsGheTham.length > 0) {
			for (var i = 0, len = dsGheTham.length; i < len; i++) {
				html += '	                <tr style="height: 22px;">';
				html += '						<td class="FirstTdStyle" style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;">' + dsGheTham[i].startTimeHHMM + '</div></td>';
				html += '						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;">' + dsGheTham[i].endTimeHHMM + '</div></td>';
				html += '					</tr>';
			};
		} else {
			html += '	                <tr style="height: 22px;">';
			html += '						<td class="FirstTdStyle" style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"></div></td>';
			html += '						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"></div></td>';
			html += '					</tr>';
		}
		html += '	               </tbody>';
		html += '	           </table>';
		html += '           </div>';
		html += '           <div class="Clear"></div>';
		html += '		   <div class="FixFloat ContentFunc">';
		html += '			   <p class="Text2Style" style="color:red">* Đơn vị (x 1000 vnđ)</p> ';
		html += '		   </div>';
		html += '		</div>';
		html += '		</div>';
		html += '	</div>';
		return html;
	},

	/***
	* Lay cobobox don vi ung voi giam sat quan ly NVBH
	* @author vuongmq
	* @since 21/08/2015
	*/
	loadShopSuperviseCbx: function(cbxId, url, params, cbxWidth, selectCall, callBackFail) {				
		var par = {};
		var wid = $('#' + cbxId).width();
		if (params != undefined && params != null){
			par = params;
		}
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		$.ajax({
			type : "POST",
			url: url,
			data : ($.param(par, true)),
			dataType : "json",
			success : function(data) {
				if (data != undefined && data != null && data.rows != null && data.rows.length > 0) {
					$('#' + cbxId).combobox({
						valueField: 'shopCode',
						textField:  'shopName',
						data: data.rows,
						panelWidth: wid,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
						},
						filter: function(q, row) {
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0);
						},	
				        onSelect: function(rec){
				        	if (selectCall != undefined && selectCall != null) {
				        		selectCall.call(this, rec);
				        	}
				        },
				        onLoadSuccess: function() {
				        	//choose the first item when load success
				        	var arr = $('#' + cbxId).combobox('getData');
				        	if (arr != null && arr.length > 0) {
				        		$('#' + cbxId).combobox('select', arr[0].shopCode);
				        	}
				        	
				        }
					});
				} else {
					$('#' + cbxId).combobox({
						valueField: 'shopCode',
						textField:  'shopName',
						data: data.rows,
						panelWidth: wid,
						formatter: function(row) {
							return '';
						},
					});
					if (callBackFail != undefined && callBackFail != null) {
		        		callBackFail.call(this, data);
		        	}
				}
			}
		});
	},
	/**
	 * @author trietptm
	 * lay danh sach vi tri cua NVBH (merge dms lite)
	 */
	calcShortestDistance : function(type) {
		$('#divOverlay').show();
		var params = new Object();
		params.staffId = SuperviseSales.id;
		if ($('#startDate').val() == '') {
			params.checkDate = getCurrentDate();
		} else {
			params.checkDate = $('#startDate').val().trim();
		}
		if ($('#fromTime').val() == '') {
			params.fromTime = '07:00';
		} else {
			params.fromTime = $('#fromTime').val().trim();
		}
		if ($('#toTime').val() == '') {
			params.toTime = '18:00';
		} else {
			params.toTime = $('#toTime').val().trim();
		}
		params.type = type;
		$.ajax({
			type: 'POST',
			url: '/supervise/sales/calc-shortest-distance',
			data: ($.param(params, true)),
			dataType: 'json',
			success: function(result) {
				if (result != null && result.list != null && result.list.length > 0) {
					var rows = result.list;
					$.each (rows, function (i) {
						if (i < rows.length - 1) {
							row = rows[i];
							nextRow = rows[i+1];
							SuperviseSales.calcShortestKm(row, nextRow);
						}
					});
				} else {
					$('#kmDriven').show();
					$('#resultKmDriven').html("0(Km)");
					$('#titleResultKmDrivenId').html("");
					$('#divOverlay').hide();
				}
			}
		});
	},
	/**
	 * @author trietptm
	 * tinh khoang cach giua 2 diem (merge dms lite)
	 */
	calcShortestKm: function(start,end) {
		var directionsService = new viettel.RoutingService();
		var startPoint = new viettel.LatLng(start.lat, start.lng);
		var endPoint = new viettel.LatLng(end.lat, end.lng);
		var request = {
			origin: startPoint,
			destination: endPoint,
			travelMode: viettel.TravelMode.MOTOR
		};
		directionsService.route(request, function(response, status) {
			if (status == viettel.RoutingStatus.OK) {
				SuperviseSales.numKm += response.length;  
				$('#kmDriven').show();
				if (SuperviseSales.numKm > 1000){
					$('#resultKmDriven').html(formatCurrency(SuperviseSales.numKm / 1000));
					$('#resultKmDriven').attr('title', SuperviseSales.numKm + " (m)");
					$('#titleResultKmDrivenId').html("(Km)");
				}else{
					$('#resultKmDriven').html(formatCurrency(SuperviseSales.numKm));
					$('#titleResultKmDrivenId').html("(m)");
				}
				$('#divOverlay').hide();
			}
		});
	},
	showHideDescript : function(isGS) {
		if(isGS == true || isGS=='true'){
			for(var i=1; i<6; i++){
				$('#item'+i).hide();
			}
		}else{
			for(var i=1; i<6; i++){
				$('#item'+i).show();
			}			
		}
	}
};
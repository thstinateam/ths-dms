var SupperviseLogisticsRoute  = {
	_lstCustomer:null,
	_lstCustomerStand:null,
	//-------------- Quản lý tuyến--------------------------
	hideAllTab:function(){
		$('.Active').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
		$('#container3').hide();
	},
	loadTab1:function(){
		SupperviseLogisticsRoute.hideAllTab();
		$('#tab1').addClass('Active');
		$('#container1').show();
	},
	loadTab2:function(){
		SupperviseLogisticsRoute.hideAllTab();
		$('#tab2').addClass('Active');
		$('#container2').show();
		if(ViettelMap._map==null){
			VTMapUtil.loadMapResource(function(){
				ViettelMap.loadBigMap('mapRoute',0,0,13,null);
				SupperviseLogisticsRoute.openMapCustomerRouting();
			});
		}else{
			SupperviseLogisticsRoute.openMapCustomerRouting();
		}
	},
	loadTab3:function(){
		SupperviseLogisticsRoute.hideAllTab();
		$('#tab3').addClass('Active');
		$('#container3').show();
	},
	loadAllCustomer:function(){
		var dataModel=new Object();
		dataModel.carId=$('#car').val();
		dataModel.deliveryDate=$('#date').val();
		$('#divOverlay').show();
		$('#imgOverlay').show();
		Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/list-all-customer-for-routing',function(data) {
			$('#divOverlay').hide();
			$('#imgOverlay').hide();
			if(!data.error){
				var lst=data.lst;
				if(data.nvgh!=undefined && data.nvgh!=null){
					$('#nvghId').val(data.nvgh);
				}else{
					$('#nvghId').val('');
				}
				if(lst.length>0){
					for(var i=0;i<lst.length;i++){
						SupperviseLogisticsRoute._lstCustomer.put(lst[i].customerId,lst[i]);
						SupperviseLogisticsRoute._lstCustomerStand.put(lst[i].customerId,jQuery.extend(true, {}, lst[i]));
					}
				}else{
					$('#btUpdate').hide();
				}
			}
		});
	},
	searchRoute:function(){
		SupperviseLogisticsRoute.loadTab1();
		$('#errMsg').hide();
		$('#errMsgSearch').hide();
		if($('#date').val().trim()==''){
			$('#errMsgSearch').html('Ngày không được để trống.').show();
			return;
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var date = $('#date').val().trim();
		if(Utils.compareDate(day + '/' + month + '/' + year,date)){
			$('#btUpdate').show();
		}else{
			$('#btUpdate').hide();
		}
		$('#carSearch').val($('#car').val());
		$('#dateSearch').val($('#date').val());
		SupperviseLogisticsRoute._lstCustomer=new Map();
		SupperviseLogisticsRoute._lstCustomerStand=new Map();
		SupperviseLogisticsRoute.loadAllCustomer();
		SupperviseLogisticsRoute.loadNVGH();
		SupperviseLogisticsRoute.loadGridCustomer();
	},
	loadNVGH:function(){
		var dataModel=new Object();
		dataModel.carId=$('#car').val();
		dataModel.deliveryDate=$('#date').val();
		$('#divOverlay').show();
		$('#imgOverlay').show();
		Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/get-list-nvgh',function(data) {
			$('#divOverlay').hide();
			$('#imgOverlay').hide();
			if(!data.error){
				var lst=data.lst;
				if(lst.length>0){
					var html='<option value="-1">-- Chọn nhân viên --</option>';
					for(var i=0;i<lst.length;i++){
						html += '<option value="'+lst[i].id+'">'+Utils.XSSEncode(lst[i].staffCode+' - '+lst[i].staffName)+'</option>';
					}
					$('#nvgh').html(html);
					if($('#nvghId').val()!=''){
						$('#nvgh').val($('#nvghId').val());
					}else{
						$('#nvgh').val(-1);
					}
					$('#nvgh').change();
				}
			}
		});
	},
	loadGridCustomer:function(){
		$("#grid").datagrid('load',{page:1,
			carId:$('#carSearch').val().trim(),
			deliveryDate:$('#dateSearch').val().trim()});
	},
	inputChange:function(id){
		var seq=$('#customer'+id).val();
		if(seq.trim()==''){
			seq=0;
		}
		if(!isNaN(seq)){
			seq=parseInt(seq);
			var temp=SupperviseLogisticsRoute._lstCustomer.get(id);
			temp.seq=seq;
		}
	},
	checkSeq:function(){
		for(var i=0,n=SupperviseLogisticsRoute._lstCustomer.valArray.length;i<n-1;i++){
			var seqI=SupperviseLogisticsRoute._lstCustomer.valArray[i].seq;
			if(seqI!=null && seqI!=0){
				for(var k=i+1,m=SupperviseLogisticsRoute._lstCustomer.valArray.length;k<m;k++){
					var seqK=SupperviseLogisticsRoute._lstCustomer.valArray[k].seq;
					if(seqK!=null && seqK!=0 && seqK==seqI){
						var custK=SupperviseLogisticsRoute._lstCustomer.valArray[k].shortCode;
						var custI=SupperviseLogisticsRoute._lstCustomer.valArray[i].shortCode;
						$('#errMsg').html('Thứ tự ghé thăm trùng nhau giữa 2 khách hàng '+custI+' và '+custK).show();
						return false;
					}
				}
			}
		}
		return true;
	},
	saveRoute:function(){
		$('#errMsg').hide();
		if(SupperviseLogisticsRoute._lstCustomer==null || SupperviseLogisticsRoute._lstCustomer.keyArray.length==0){
			$('#errMsg').html('Không có khách hàng để cập nhật').show();
			$('#btUpdate').hide();
			return;
		}
		if(!SupperviseLogisticsRoute.checkSeq()){
			return;
		}
		if($('#nvgh').val()=='-1' || $('#nvgh').val()==''){
			$('#errMsg').html('Bạn chưa chọn tài xế lái xe.').show();
			return;
		}
		var lstCustomerId=new Array();
		var lstId=new Array();
		var lstSeq=new Array();
		for(var i=0,n=SupperviseLogisticsRoute._lstCustomer.keyArray.length;i<n;i++){
			var cust=SupperviseLogisticsRoute._lstCustomer.valArray[i];
			var custStand=SupperviseLogisticsRoute._lstCustomerStand.get(cust.customerId);
			if(cust.seq!=custStand.seq || SupperviseLogisticsRoute._lstCustomer.valArray[i].id==null){
				lstCustomerId.push(cust.customerId);
				if(cust.id!=undefined && cust.id!=null){
					lstId.push(cust.id);
				}else{
					lstId.push(0);
				}
				if(cust.seq!=undefined && cust.seq!=null){
					lstSeq.push(cust.seq);
				}else{
					lstSeq.push(0);
				}
			}
		}
		var param = new Object();
		param.carId=$('#carSearch').val();
		param.deliveryDate=$('#dateSearch').val();
		param.lstCustomerId=lstCustomerId;
		param.lstId=lstId;
		param.lstSeq=lstSeq;
		param.nvghId=$('#nvgh').val().trim();
		Utils.addOrSaveData(param, '/supervise/logistics/update-route', null, 'errMsg', function(result){
			SupperviseLogisticsRoute.searchRoute();
		});
	},
	addMarkerRoutingCust:function(point){
		var pt = new viettel.LatLng(point.lat, point.lng);
		var custId=point.customerId;
		var seq=point.seq!=null&&point.seq!=0?point.seq:'';
		var title="<strong style='color:white;left: 25px;position: absolute;top: -23px;'>"+seq+"</strong>";
		var image="/resources/images/Mappin/icon_circle_blue.png";
		var marker = new viettel.LabelMarker({
			icon:{
				url : image,
				size : {height : 32, width : 32},
				scaledSize : {height : 32, width : 32}
			},
			position : pt,
			map : ViettelMap._map,
			labelContent : title,
			labelClass : "MarkerLabel",
			labelVisible : true,
			draggable : false,
			labelAnchor : new viettel.Point(30, 0)
		});
		marker.attr=point;
		viettel.Events.addListener(marker, "click", function(evt) {
			var html= Utils.XSSEncode(marker.attr.shortCode+' - '+marker.attr.customerName)+'</br>'+Utils.XSSEncode(marker.attr.address);
			SupperviseLogistics.openWindowInfo(html, marker.getPosition().lat(), marker.getPosition().lng());
		});
		
		if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
			ViettelMap._listOverlay = new Array();
			ViettelMap._listOverlay.push(marker);
		} else {
			ViettelMap._listOverlay.push(marker);
		}
	},
	openMapCustomerRouting: function(){
		ViettelMap.clearOverlays();
		var map = ViettelMap._map;
		var lstPoint = new Array();
		if(SupperviseLogisticsRoute._lstCustomer!=undefined && SupperviseLogisticsRoute._lstCustomer!=null){
			lstPoint = SupperviseLogisticsRoute._lstCustomer.valArray;
			for(var i=0;i<lstPoint.length;i++){
				var point = lstPoint[i];
				if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
					SupperviseLogisticsRoute.addMarkerRoutingCust(point);
				}
			}
			ViettelMap.fitOverLay();
		}
	}
};
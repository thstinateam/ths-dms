var SupperviseLogistics  = {
	_isSearch:null,
	_lstMarkerCust:new Array(),
	_lstMarkerShop:new Array(),
	_lstMarkerCar:new Array(),
	_lstMarkerCarRoute:new Array(),
	_lstMarkerRoutingCust:new Array(),
	_lstMarkerRoutingCarCust:new Array(),//khach hang trong tuyen
	_lstMarkerRoutingCustRoute:new Array(),//polyline tuyen khach hang
	_lstShopPosition:null,
	_lstCarPosition:null,
	_lstCustPosition:null,
	_lstShopCustPosition:null,
	_notFitOverlay:null,
	_itvCust:null,
	_itvCar:null,
	_currentShopId:null,
	_denyZoom:null,
	_itvProccessCar:null,
	_lstPolygon:new Array(),
	_currentCarLogId:0,
	_currentColor:null,
	a:new Array(),
	b:new Array(),
	_colorEx: ['#7FFFD4','#007FFF','#3D2B1F','#C41E3A','#7FFF00','#4B0082','#F0DC82','#FFBF00',
	         '#DF73FF','#FF0000','#0000FF','#8A2BE2','#A52A2A','#DEB887','#5F9EA0','#7FFF00','#D2691E',
	         '#6495ED','#DC143C','#00FFFF','#B8860B','#A9A9A9','#006400','#8B008B','#FF1493','#2F4F4F',
	         '#00CED1','#9400D3','#483D8B','#00BFFF','#696969','#ADFF2F','#4B0082','#CD5C5C','#DDA0DD'],
	_color: ['005A7E','D15849','583E6A','8B6B1B','0303B7','E7AE00','1248C6','445A20','659A8A','943700','00740D',
	         '9D617F','00670B','AF1DCD','C400BA','D21A86','9966CC'],
    clearOverlays:function(type){
    	if(type==undefined || type==null || type.indexOf('Polygon')!=-1){
	    	if(SupperviseLogistics._lstPolygon!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstPolygon.length; i++) {
					var obj = SupperviseLogistics._lstPolygon[i];
					obj.setMap(null);
				}
	    	}
	    	SupperviseLogistics._lstPolygon=new Array();
    	}
    	if(type==undefined || type==null || type.indexOf('Car')!=-1){
	    	if(SupperviseLogistics._lstMarkerCar!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerCar.length; i++) {
					var obj = SupperviseLogistics._lstMarkerCar[i];
					obj.setMap(null);
				}
	    	}
	    	SupperviseLogistics._lstMarkerCar=new Array();
    	}
    	if(type==undefined || type==null || type.indexOf('CarRoute')!=-1){
	    	if(SupperviseLogistics._lstMarkerCarRoute!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerCarRoute.length; i++) {
					var obj = SupperviseLogistics._lstMarkerCarRoute[i];
					obj.setMap(null);
				}
	    	}
	    	SupperviseLogistics._lstMarkerCarRoute=new Array();
    	}
    	if(type==undefined || type==null || type.indexOf('Shop')!=-1){
	    	if(SupperviseLogistics._lstMarkerShop!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerShop.length; i++) {
					var obj = SupperviseLogistics._lstMarkerShop[i];
					obj.setMap(null);
				}
	    	}
	    	SupperviseLogistics._lstMarkerShop=new Array();
    	}
    	if(type==undefined || type==null || type.indexOf('Customer')!=-1){
	    	if(SupperviseLogistics._lstMarkerCust!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerCust.length; i++) {
					var obj = SupperviseLogistics._lstMarkerCust[i];
					obj.setMap(null);
				}
	    	}
	    	SupperviseLogistics._lstMarkerCust=new Array();
    	}
    	if(type==undefined || type==null || type.indexOf('RoutingCust')!=-1){
	    	if(SupperviseLogistics._lstMarkerRoutingCustRoute!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerRoutingCustRoute.length; i++) {
					var obj = SupperviseLogistics._lstMarkerRoutingCustRoute[i];
					obj.setMap(null);
				}
	    	}
	    	SupperviseLogistics._lstMarkerRoutingCustRoute=new Array();
    	}
    	if(type==undefined || type==null || type.indexOf('RoutingCarCust')!=-1){
	    	if(SupperviseLogistics._lstMarkerRoutingCarCust!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerRoutingCarCust.length; i++) {
					var obj = SupperviseLogistics._lstMarkerRoutingCarCust[i];
					obj.setMap(null);
				}
	    	}
	    	SupperviseLogistics._lstMarkerRoutingCarCust=new Array();
    	}
    	
 	},
 	setMapOverlay:function(map,type){
 		if(type==undefined || type==null || type.indexOf('Polygon')!=-1){
	    	if(SupperviseLogistics._lstPolygon!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstPolygon.length; i++) {
					var obj = SupperviseLogistics._lstPolygon[i];
					obj.setMap(map);
				}
	    	}
    	}
    	if(type==undefined || type==null || type.indexOf('Car')!=-1){
	    	if(SupperviseLogistics._lstMarkerCar!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerCar.length; i++) {
					var obj = SupperviseLogistics._lstMarkerCar[i];
					obj.setMap(map);
				}
	    	}
    	}
    	if(type==undefined || type==null || type.indexOf('CarRoute')!=-1){
	    	if(SupperviseLogistics._lstMarkerCarRoute!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerCarRoute.length; i++) {
					var obj = SupperviseLogistics._lstMarkerCarRoute[i];
					obj.setMap(map);
				}
	    	}
    	}
    	if(type==undefined || type==null || type.indexOf('Shop')!=-1){
	    	if(SupperviseLogistics._lstMarkerShop!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerShop.length; i++) {
					var obj = SupperviseLogistics._lstMarkerShop[i];
					obj.setMap(map);
				}
	    	}
    	}
    	if(type==undefined || type==null || type.indexOf('Customer')!=-1){
	    	if(SupperviseLogistics._lstMarkerCust!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerCust.length; i++) {
					var obj = SupperviseLogistics._lstMarkerCust[i];
					obj.setMap(map);
				}
	    	}
    	}
    	if(type==undefined || type==null || type.indexOf('CarCust')!=-1){
	    	if(SupperviseLogistics._lstMarkerRoutingCarCust!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerRoutingCarCust.length; i++) {
					var obj = SupperviseLogistics._lstMarkerRoutingCarCust[i];
					obj.setMap(map);
				}
	    	}
    	}
    	if(type==undefined || type==null || type.indexOf('CustRoute')!=-1){
	    	if(SupperviseLogistics._lstMarkerRoutingCustRoute!=null){
		    	for(var i = 0; i < SupperviseLogistics._lstMarkerRoutingCustRoute.length; i++) {
					var obj = SupperviseLogistics._lstMarkerRoutingCustRoute[i];
					obj.setMap(map);
				}
	    	}
    	}
 	},
 	fitOverLay: function() {
		var bound = null;
		if(SupperviseLogistics._lstMarkerCust != null && SupperviseLogistics._lstMarkerCust != undefined) {
			for(var i = 0; i < SupperviseLogistics._lstMarkerCust.length; i++) {
				var obj = SupperviseLogistics._lstMarkerCust[i];
				if(obj instanceof viettel.LabelMarker || obj instanceof viettel.Marker) {
					if(ViettelMap.isValidLatLng(obj.getPosition().lat(), obj.getPosition().lng())) {
						var latlng = new viettel.LatLng(obj.getPosition().lat(), obj.getPosition().lng());
						bound = ViettelMap.getBound(bound, latlng);
					}
				}
			}
		}
		if(SupperviseLogistics._lstMarkerShop != null && SupperviseLogistics._lstMarkerShop != undefined) {
			for(var i = 0; i < SupperviseLogistics._lstMarkerShop.length; i++) {
				var obj = SupperviseLogistics._lstMarkerShop[i];
				if(obj instanceof viettel.LabelMarker || obj instanceof viettel.Marker) {
					if(ViettelMap.isValidLatLng(obj.getPosition().lat(), obj.getPosition().lng())) {
						var latlng = new viettel.LatLng(obj.getPosition().lat(), obj.getPosition().lng());
						bound = ViettelMap.getBound(bound, latlng);
					}
				}
			}
		}
		if(SupperviseLogistics._currentShopId!=null && !$('#cbCar').is(':disabled') && $('#cbCar').is(':checked')
			&& SupperviseLogistics._lstMarkerCar!=null && SupperviseLogistics._lstMarkerCar!=undefined){
			for(var i = 0; i < SupperviseLogistics._lstMarkerCar.length; i++) {
				var obj = SupperviseLogistics._lstMarkerCar[i];
				if(obj instanceof viettel.LabelMarker || obj instanceof viettel.Marker) {
					if(ViettelMap.isValidLatLng(obj.getPosition().lat(), obj.getPosition().lng())) {
						var latlng = new viettel.LatLng(obj.getPosition().lat(), obj.getPosition().lng());
						bound = ViettelMap.getBound(bound, latlng);
					}
				}
			}
		}
		if(SupperviseLogistics._currentShopId!=null && !$('#cbRoute').is(':disabled') && $('#cbRoute').is(':checked')
				&& SupperviseLogistics._lstMarkerRoutingCarCust!=null && SupperviseLogistics._lstMarkerRoutingCarCust!=undefined){
			for(var i = 0; i < SupperviseLogistics._lstMarkerRoutingCarCust.length; i++) {
				var obj = SupperviseLogistics._lstMarkerRoutingCarCust[i];
				if(obj instanceof viettel.LabelMarker || obj instanceof viettel.Marker) {
					if(ViettelMap.isValidLatLng(obj.getPosition().lat(), obj.getPosition().lng())) {
						var latlng = new viettel.LatLng(obj.getPosition().lat(), obj.getPosition().lng());
						bound = ViettelMap.getBound(bound, latlng);
					}
				}
			}
		}
		if(bound != null) {
			ViettelMap._map.fitBounds(bound);
		}
	},
 	addListOverlay:function(marker,type){
 		if(type=='CUSTOMER'){
 			SupperviseLogistics._lstMarkerCust.push(marker);
 		}else if(type=='SHOP'){
 			SupperviseLogistics._lstMarkerShop.push(marker);
 		}
 	},
 	loadTreeShop:function(isSearch){
 		if(isSearch!=undefined && isSearch!=null && isSearch==1){
			SupperviseLogistics._isSearch=1;
			$('#treeGrid').treegrid('reload');
		}else{
			$('#treeGrid').treegrid({  
			    url:  '/supervise/logistics/list-shop-car-one-node',
		        height:'auto',  
		        idField: 'id',  
		        treeField: 'text',
		        width:500,
		        //scrollbarSize:0,
		        lines:true,
		        animate:true,
		        fitColumns:true,
			    columns:[[  
			        {field:'text',title:'Đơn vị',width:350,formatter:function(v,r,i){
			        	var isRoot=0;
			        	if(r.attr.shopType!=null && r.attr.shopType==0){
			        		isRoot=1;
			        	}
			        	var str = Utils.XSSEncode(r.attr.shopCode+' - '+r.attr.shopName);
			        	str=SupperviseLogistics.getShopName(str);
			        	if(r.attr.shopType!=null && r.attr.shopType==3){
			        		return '<a class="Decoration" onclick="SupperviseLogistics.moveToShop('+r.attr.shopId+','+isRoot+',1);">'+str+'</a>';
			        	}else{
			        		return '<a class="Decoration" onclick="SupperviseLogistics.moveToShop('+r.attr.shopId+','+isRoot+');">'+str+'</a>';
			        	}
			        }},
			        {field:'car',title:'Số xe',width:100,align:'center',formatter:function(v,r,i){
			        	if(r.attr.countCar!=null && r.attr.countCar!=0){
			        		return Utils.XSSEncode(r.attr.countCar);
			        	}else{
			        		return '0';
			        	}
		        	}},
		        	{field:'warning',title:'Lấn vùng',width:120,align:'center',formatter:function(v,r,i){
		        		if(r.attr.countWarning!=null && r.attr.countWarning!=0){
		        			return Utils.XSSEncode(r.attr.countWarning);
			        	}else{
			        		return '0';
			        	}
		        	}},
		        	{field:'warningRouting',title:'Lấn tuyến',width:120,align:'center',formatter:function(v,r,i){
		        		if(r.attr.countRouting!=null && r.attr.countRouting!=0){
		        			return Utils.XSSEncode(r.attr.countRouting);
			        	}else{
			        		return '0';
			        	}
		        	}},
		        	{field:'edit',title:'',width:40,align:'center',formatter:function(v,r,i){
			        	var str= '<a href="javascript:void(0)" onclick="SupperviseLogistics.showContextMenu(this,'+r.attr.shopId+','+r.attr.shopType+');">';
			        	str+='<img src="/resources/images/icon_2.png",width:15, height="12"/></a>';
			        	return str;
			        }}
			    ]],
			    rowStyler: function(r){
			    	var str='';
			    	if((r.attr.countWarning!=null && r.attr.countWarning>0)
			    		|| (r.attr.countRouting!=null && r.attr.countRouting>0)){
			    		str='color:red;';
			    	}
					if (r.attr.isBold!=null && r.attr.isBold){
						return 'background:none repeat scroll 0 0 #FBEC88;'+str;
					}
					return str;
//			    	return 'color:red;';
				},
			    onBeforeLoad:function(n,p){
			    	if(p!=undefined && p!=null){
			    		$('.highlight').css('font-weight','normal');
				    	p.shopId=p.id;
				    	p.typeSup=$('input[name="rbSup"]:checked').val();
				    	if(($('#shopCode').val().trim()!='' || $('#shopName').val().trim()!='') && SupperviseLogistics._isSearch!=undefined && SupperviseLogistics._isSearch!=null && SupperviseLogistics._isSearch==1){
				    		p.shopCode=$('#shopCode').val().trim();
				    		p.shopName=$('#shopName').val().trim();
				    	}
				    	SupperviseLogistics._isSearch=null;
			    	}
			    }
			});
		}
 	},
 	showContextMenu:function(t,shopId,shopType){
 		if(shopId!=null && shopId!='null'){
 			if(shopType==null){
 				shopType = 3;
 			}
 			$('#cmLanTuyen').attr('onclick','SupperviseLogistics.openSearchCarEasyUIDialog('+shopId+','+shopType+')').show();
 			$('#cmLanVung').attr('onclick','SupperviseLogistics.openSearchOverlayEasyUIDialog('+shopId+','+shopType+')').show();
 			$('#cmKhongHetTuyen').attr('onclick','SupperviseLogistics.openSearchRoutingExistEasyUIDialog('+shopId+','+shopType+')').show();
 			$('#contextMenu').menu('show', {  
 				left: $(t).offset().left+47,  
 				top: $(t).offset().top  
 			});
 		}
 	},
 	getListShop:function(){
		try{
			var data = new Object();
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			Utils.getHtmlDataByAjax(data, '/supervise/logistics/list-shop-car',function(result) {
				if(!result.error){
					SupperviseLogistics._lstShopPosition=new Map();
					var data = JSON.parse(result);
					var lstShop = data.lstShop;
					if(lstShop!=null && lstShop.length>0){ 
						for(i=0;i<lstShop.length;i++){
							var temp=lstShop[i];
							if(temp.shopType==1){
								if(parseInt(temp.countWarning)==0){
									temp.image="/resources/images/Mappin/green.png";
								}else {
									temp.image="/resources/images/Mappin/red.png";
								}
							}else{
								if(parseInt(temp.countWarning)==0){
									temp.image="/resources/images/Mappin/car_green.png";
								}else {
									temp.image="/resources/images/Mappin/car_red.png";
								}
							}
							SupperviseLogistics._lstShopPosition.put(temp.shopId,temp);
						}
					}
					SupperviseLogistics.reloadMarker(1);
				}
			}, 'loading2', null);
		}catch(err){
			$('#loading2').hide();
		}
	},
	reloadMarker:function(type){//type: 1:shop , 2:car
		try{
			if(SupperviseLogistics._itv!=undefined && SupperviseLogistics._itv!=null)
				window.clearInterval(SupperviseLogistics._itv);
		}catch(e){}
		if(ViettelMap._map!=null){
			if(SupperviseLogistics._notFitOverlay==null){
				ViettelMap._map.setZoom(5);
			}
			ViettelMap._listMarker=new Map();
			SupperviseLogistics.fillListMarker(type);
			SupperviseLogistics.clearOverlays();
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			if(type==1){
				SupperviseLogistics.addMutilMarkerShop();
			}
		}else{
			var interval=window.setInterval( function() {
				if(ViettelMap._map!=null){
					ViettelMap._listMarker=new Map();
					SupperviseLogistics.fillListMarker(type);
					SupperviseLogistics.clearOverlays();
					if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
						ViettelMap._currentInfoWindow.close();
					}
					if(type==1){
						SupperviseLogistics.addMutilMarkerShop();
					}
					window.clearInterval(interval);
				}
			},500);
		}
	},
	fillListMarker:function(type){//type: 1:shop , 2:car
		if(type==1){//shop
			SupperviseLogistics._isLoadShop=1;
			if(SupperviseLogistics._lstShopPosition!=null){
				for(var i=0;i<SupperviseLogistics._lstShopPosition.valArray.length;i++){
					var temp = SupperviseLogistics._lstShopPosition.valArray[i];
					if(temp.lat!=null && temp.lng!=null){
						if((ViettelMap._map.getZoom()<=7 && temp.shopType==1)
								|| (ViettelMap._map.getZoom()>=8 && temp.shopType==2)){
							ViettelMap._listMarker.put(temp.shopId,temp);
						}
					}
				}
			}
		}else{//car
			SupperviseLogistics._isLoadShop=0;
			if(SupperviseLogistics._lstCarPosition!=null){
				for(var i=0;i<SupperviseLogistics._lstCarPosition.valArray.length;i++){
					var temp = SupperviseLogistics._lstCarPosition.valArray[i];
					if(temp.lat!=null && temp.lng!=null){
						ViettelMap._listMarker.put(temp.carId,temp);
					}
				}
			}
		}
	},
	moveToShop:function(shopId,root,ex){//ex mở dialog góc phải bên trên
		SupperviseLogistics._currentShopId=null;
		SupperviseLogistics._denyZoom=null;
		SupperviseLogistics._currentColor=null;
		SupperviseLogistics.changeCheckCarAndRoute(0);
		if(shopId!=undefined && shopId!=null){
			var temp = SupperviseLogistics._lstShopPosition.get(shopId);
			if(temp!=null){
				if(root!=undefined && root!=null && root==1){//root
					SupperviseLogistics.reloadMarker(1);
				}else{
					SupperviseLogistics._denyZoom=1;
					SupperviseLogistics.showOverlay(shopId);
				}
			}else{
				if(root!=undefined && root!=null && root==1){
					SupperviseLogistics.reloadMarker(1);
				}else{
					SupperviseLogistics._currentShopId=shopId;
					SupperviseLogistics._denyZoom=1;
					SupperviseLogistics.showOverlayWithCar(shopId);
					SupperviseLogistics.changeCheckCarAndRoute(1);
				}
			}
		}
	},
	changeCheckCarAndRoute:function(type){//1:on, 0:off
		if(type!=undefined && type==1){
			$('#cbCar').removeAttr('disabled');
			$('#cbCar').attr('checked','checked');
			$('#cbRoute').removeAttr('disabled');
			$('#cbRoute').attr('checked','checked');
			$('#cbCust').removeAttr('disabled');
			$('#cbCust').removeAttr('checked');
		}else{
			$('#cbCar').attr('disabled','disabled');
			$('#cbRoute').attr('disabled','disabled');
			$('#cbCust').attr('disabled','disabled');
		}
	},
	addMarkerShop:function(point){
		if(SupperviseLogistics._currentMarkerShop!=null){
			SupperviseLogistics._currentMarkerShop.setMap(null);
		}
		if($('#marker'+point.shopId).length>0){
			$('#marker'+point.shopId).parent().prev().click();
			return ;
		}
		if(ViettelMap.isValidLatLng(point.lat, point.lng)){
			var pt = new viettel.LatLng(point.lat, point.lng);
			var info="<div id='info"+point.shopId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
			var title=point.shopName;
			var image=point.image;
			
			var markerContent = "<div id='marker"+point.shopId+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'></div>";
			
			
			var marker = new viettel.LabelMarker({
				icon:{
					url : image,
					size : {height : 35, width : 25},
					scaledSize : {height : 35, width : 25}
				},
				position : pt,
				map : ViettelMap._map,
				labelContent : markerContent,
				labelClass : "MarkerLabel",
				labelVisible : true,
				draggable : false,
				labelAnchor : new viettel.Point(25, 0)
			});
			marker.shopId = point.shopId;
			marker.lat = point.lat;
			marker.lng = point.lng;
			ViettelMap._map.setCenter(pt);
			SupperviseLogistics._currentMarkerShop = marker;
			$('#marker'+point.shopId).parent().prev().css('z-index', 10000000);
			var parent=$('#marker'+point.shopId).parent().parent();
			var a=parent.css('top');
			parent.css('top',(parseInt(a.substring(0,a.length-2))+10)+'px');
			SupperviseLogistics.showWindowInfoShop(point.shopId);
			$('#marker'+point.shopId).parent().prev().bind('click', point, function(e) {
				var point = e.data;
				SupperviseLogistics.showWindowInfoShop(point.shopId); 
			});
			
		}
	},
	addMutilMarkerShop:function(){
		var map = ViettelMap._map;
		if(ViettelMap._listMarker!=undefined && ViettelMap._listMarker!=null){
			var flag=0;
			var index=0;
			SupperviseLogistics._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<ViettelMap._listMarker.valArray.length;i++,index++,j++){
					if(j>100) break;
					var point = ViettelMap._listMarker.valArray[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new viettel.LatLng(point.lat, point.lng);
						var info="<div id='info"+point.shopId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
						var title=point.shopName;
						var image=point.image;
						
						var markerContent = "<div id='marker"+point.shopId+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'></div>";
						
						
						var marker = new viettel.LabelMarker({
							icon:{
								url : image,
								size : {height : 35, width : 25},
								scaledSize : {height : 35, width : 25}
							},
							position : pt,
							map : ViettelMap._map,
							labelContent : markerContent,
							labelClass : "MarkerLabel",
							labelVisible : true,
							draggable : false,
							labelAnchor : new viettel.Point(25, 0)
						});
						marker.shopId = point.shopId;
						marker.lat = point.lat;
						marker.lng = point.lng;
						
						$('#marker'+point.shopId).parent().prev().css('z-index', 10000000);
						var parent=$('#marker'+point.shopId).parent().parent();
						$('#marker'+point.shopId).parent().prev().bind('click', point, function(e) {
							var point = e.data;
							SupperviseLogistics.showWindowInfoShop(point.shopId, point.lat, point.lng); 
						});
						SupperviseLogistics.addListOverlay(marker,'SHOP');
					}
				}
				if(index>=ViettelMap._listMarker.valArray.length){
					if(SupperviseLogistics._notFitOverlay==undefined || SupperviseLogistics._notFitOverlay==null){
						SupperviseLogistics.fitOverLay();
					}
					SupperviseLogistics._notFitOverlay=null;
					window.clearInterval(SupperviseLogistics._itv);
				}
			},300); 
		}
	},
	getListSubShop:function(shopId,lat,lng){
		var pt=SupperviseLogistics._lstShopPosition.get(shopId);
		if(pt!=null){
			$('#InfoWindow').html('<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15"/>');
			var str='?shopId='+pt.shopId;
			$.getJSON('/supervise/logistics/list-sub-shop-car'+str, function(data) {
				var strStyleRed = '';
				if(pt.countWarning!=null && pt.countWarning>0){
					strStyleRed=' style="color:red;" ';
				}
				var html = '<div id="InfoWindow" class="MapPopupSection MapPopup1Section">';
				html += '<h2 style="text-align:left" class="Title2Style">'+Utils.XSSEncode(pt.shopName)+'</h2>';
				html += '<div class="MPContent">';
				html += '<div class="GeneralTable Table1Section">';
				html += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
				html += '<colgroup><col style="width:120px;" /><col style="width:30px;" /><col style="width:30px;" /><col style="width:30px;" /><col style="width:30px;" /></colgroup>';
				html += '<thead><tr><th class="FirstThStyle">Đơn vị</th><th>Số xe</th><th>Lấn vùng</th><th>Lấn tuyến</th><th>Vùng phủ</th></tr></thead>';
				html += '<tbody>';
				if(data.lst!=null){
					var lst=data.lst;
					for(var i=0;i<lst.length;i++){
						var strStyleTemp='';
						if((lst[i].countWarning!=null && lst[i].countWarning>0)
								|| (lst[i].countRouting!=null && lst[i].countRouting>0)){
							strStyleTemp=' style="color:red;" ';
						}
						var aTag = '<a href="javascript:void(0)" onclick="SupperviseLogistics.getListSubShop('+lst[i].shopId+','+lat+','+lng+')">';
						aTag += Utils.XSSEncode(lst[i].shopCode+' - '+lst[i].shopName) +'</a>';
						html += '<tr '+strStyleTemp+'><td><div class="AlignLCols">'+aTag+'</div></td>';
						html += '<td><div class="AlignCCols">'+(lst[i].countCar!=null?lst[i].countCar:'0')+'</div></td>';
						html += '<td><div class="AlignCCols">'+(lst[i].countWarning!=null?lst[i].countWarning:'0')+'</div></td>';
						html += '<td><div class="AlignCCols">'+(lst[i].countRouting!=null?lst[i].countRouting:'0')+'</div></td>';
						html += '<td><div class="AlignCCols"><a href="javascript:void(0);" onclick="SupperviseLogistics.moveToShop('+lst[i].shopId+',0)"><img src="/resources/images/Mappin/overlay.png",width:15, height="15"/></a></div></td></tr>';
					}
				}
				html += '<tr '+strStyleRed+'><td><div class="AlignLCols">Tổng</div></td>';
				html += '<td><div class="AlignCCols">'+(pt.countCar!=null?pt.countCar:'0')+'</div></td>';
				html += '<td><div class="AlignCCols">'+(pt.countWarning!=null?pt.countWarning:'0')+'</div></td>';
				html += '<td><div class="AlignCCols">'+(pt.countRouting!=null?pt.countRouting:'0')+'</div></td>';
				html += '<td><div class="AlignCCols"><a href="javascript:void(0);" onclick="SupperviseLogistics.showOverlay('+shopId+')"><img src="/resources/images/Mappin/overlay.png",width:15, height="15"/></a></div></td></tr>';
				
				html += '</tbody></table></div></div></div>';
				SupperviseLogistics.openWindowInfo(html, lat, lng);
			});
		} else {
			SupperviseLogistics.moveToShop(shopId,0,1);
		}
	},
	showOverlay:function(shopId){
		var dataModel=new Object();
		dataModel.shopId=shopId;
		SupperviseLogistics.clearOverlays();
		$('#divOverlay').show();
		$('#imgOverlay').show();
		Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/list-cust-area',function(data) {
			$('#divOverlay').hide();
			$('#imgOverlay').hide();
			if(!data.error){
				var lst = data.lstCust;
				SupperviseLogistics._lstShopCustPosition=new Map();
				var shopId=0;
				var lstTemp=new Array();
				var bound=null;
				for(var i=0;i<lst.length;i++){
					if(shopId!=lst[i].shopId){
						if(shopId!=0){
							SupperviseLogistics._lstShopCustPosition.put(shopId,lstTemp);
						}
						shopId=lst[i].shopId;
						var lstTemp=new Array();
					}
//					if(lstTemp.length>300) continue;
					lstTemp.push(lst[i]);
					bound = ViettelMap.getBound(bound, new viettel.LatLng(lst[i].lat, lst[i].lng));
				}
				if(bound != null) {
					ViettelMap._map.fitBounds(bound);
				}
				if(shopId!=0){
					SupperviseLogistics._lstShopCustPosition.put(shopId,lstTemp);
				}
				var lstShop=SupperviseLogistics._lstShopCustPosition;
				var indexColor=0;
				for(var i=0;i<lstShop.keyArray.length;i++,indexColor++){
					SupperviseLogistics.a=new Array();
					for(var j=0,n=lstShop.valArray[i].length;j<n;j++){
						SupperviseLogistics.a.push({
							lat:lstShop.valArray[i][j].lat,
							lng:lstShop.valArray[i][j].lng
						});
					}
					SupperviseLogistics.drawOverlay(SupperviseLogistics.a,SupperviseLogistics._color[indexColor],lstShop.keyArray[i],1);
					if(indexColor>15) indexColor=-1;
				}
				if(SupperviseLogistics._notFitOverlay==undefined || SupperviseLogistics._notFitOverlay==null){
					SupperviseLogistics.fitOverLay();
				}
			}
		});
	},
	showOverlayWithCar:function(shopId,color){
		if(color==undefined || color==null){
			color=SupperviseLogistics._color[0];//màu mặc định khi chưa chọn màu
		}
		SupperviseLogistics._currentColor=color;
		var dataModel=new Object();
		dataModel.shopId=shopId;
		SupperviseLogistics.clearOverlays();
		$('#divOverlay').show();
		$('#imgOverlay').show();
		var temp = SupperviseLogistics._lstShopPosition.get(shopId);
		if(temp==null){//chọn shop NPP
			SupperviseLogistics._currentShopId=shopId;
		}
		SupperviseLogistics._lstCustPosition=new Array();
		Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/list-cust-area',function(data) {
			$('#divOverlay').hide();
			$('#imgOverlay').hide();
			if(!data.error){
				var lstArea = data.lstCust;
				//SupperviseLogistics.addMultiMarker(lst,color);
//				SupperviseLogistics._lstCustPosition=lst;
				SupperviseLogistics.drawOverlay(lstArea,color,shopId);
				SupperviseLogistics.showCarAndRoute(shopId);
				SupperviseLogistics.showRouteCustomer(shopId);
				if(SupperviseLogistics._notFitOverlay==undefined || SupperviseLogistics._notFitOverlay==null){
					SupperviseLogistics.fitOverLay();
				}
			}
		});
	},
	showCustShop:function(){
		if(SupperviseLogistics._currentShopId==undefined || SupperviseLogistics._currentShopId==null){
			return;
		}
		if(SupperviseLogistics._lstCustPosition!=null && SupperviseLogistics._lstCustPosition.length>0){
			SupperviseLogistics.addMultiMarker(SupperviseLogistics._lstCustPosition,SupperviseLogistics._currentColor);
		}else{
			var dataModel=new Object();
			dataModel.shopId=SupperviseLogistics._currentShopId;
			$('#divOverlay').show();
			$('#imgOverlay').show();
			SupperviseLogistics._lstCustPosition=new Array();
			Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/list-cust-shop',function(data) {
				$('#divOverlay').hide();
				$('#imgOverlay').hide();
				if(!data.error){
					var lst = data.lstCust;
					SupperviseLogistics._lstCustPosition=lst;
					SupperviseLogistics.addMultiMarker(SupperviseLogistics._lstCustPosition,SupperviseLogistics._currentColor);
				}
			});
		}
	},
	drawOverlay:function(b,color,shopId,onClick){
		var lst=new Array();
		for(var i=0;i< b.length;i++){
			lst.push(new viettel.LatLng(b[i].lat, b[i].lng));
		}
		var map=null;
		if($('#cbOverlay').is(':checked')){
			map=ViettelMap._map;
		}
		var polygon = new viettel.Polygon({
			paths: lst,
			strokeColor: ('#'+color),
			strokeOpacity: 0.3,
			strokeWeight: 0,
			fillColor: ('#'+color),
			fillOpacity: 0.3,
			clickable: true,
			map: map
		});
		polygon.shopId=shopId;
		polygon.color=color;
		SupperviseLogistics._lstPolygon.push(polygon);
		if(onClick!=undefined && onClick!=null ){
			viettel.Events.addListener(polygon, "click", function(evt) {
				SupperviseLogistics.showOverlayWithCar(polygon.shopId,polygon.color);
			});
		}
	},
	addMultiMarker:function(a,color){
		try{
			if(SupperviseLogistics._itvCust!=undefined && SupperviseLogistics._itvCust!=null)
				window.clearInterval(SupperviseLogistics._itv);
		}catch(e){}
		var flag=0;
		var index=0;
		SupperviseLogistics._itvCust =window.setInterval(function(){
			var j=0;
			for(var i=index,j=0;i<a.length;i++,index++,j++){
				if(j>100) break;
				if(ViettelMap.isValidLatLng(a[i].lat, a[i].lng)) {
					SupperviseLogistics.addMarker(a[i],color);
				}
			}
			if(index>=a.length){
				SupperviseLogistics._notFitOverlay=null;
				window.clearInterval(SupperviseLogistics._itvCust);
			}
		},300);
	},
	addMarker:function(point,color){
		var latlng = new viettel.LatLng(point.lat,point.lng);
		var marker = new viettel.Marker({
			icon:{
				url : '/resources/images/icons-color/'+color+'.png',
				size : {height : 10, width : 10},
				scaledSize : {height : 10, width : 10}
			},
			position: latlng,
			map: ViettelMap._map
		});
		marker.attr=point;
		viettel.Events.addListener(marker, "click", function(evt) {
			var html= Utils.XSSEncode(marker.attr.shortCode+' - '+marker.attr.customerName)+'</br>'+Utils.XSSEncode(marker.attr.address);
			SupperviseLogistics.openWindowInfo(html, marker.getPosition().lat(), marker.getPosition().lng());
		});
		SupperviseLogistics.addListOverlay(marker,'CUSTOMER');
	},
	showWindowInfoShop:function(shopId){		
		if(shopId!=null && shopId>0){
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			var pt=SupperviseLogistics._lstShopPosition.get(shopId);
			if(pt!=null) {
				var html = '<div id="InfoWindow" class="MapPopupSection MapPopup1Section">';
				html += '</div>';
				SupperviseLogistics.openWindowInfo(html, pt.lat, pt.lng);
				SupperviseLogistics.getListSubShop(shopId,pt.lat, pt.lng);
			}
		}
	},
	openWindowInfo:function(html,lat,lng){
		if(ViettelMap._currentInfoWindow != null) {
			ViettelMap._currentInfoWindow.close();
		}
		var infoWindow = new viettel.InfoWindow({
			content: html,
			position:  new viettel.LatLng(lat, lng)
		});
		infoWindow.open(ViettelMap._map);
		ViettelMap._currentInfoWindow = infoWindow;
		$('#InfoWindow').parent().css('width','');
		$('#InfoWindow').parent().css('overflow','');
	},
	showCarAndRoute:function(shopId){
		SupperviseLogistics.changeCheckCarAndRoute(1);
		//hiển thị lộ trình xe
		SupperviseLogistics.showCar(shopId);
	},
	showCar:function(shopId){
		var dataModel=new Object();
		dataModel.shopId=shopId;
		$('#divOverlay').show();
		$('#imgOverlay').show();
		SupperviseLogistics._lstCarPosition=new Map();
		SupperviseLogistics._currentCarLogId=0;
		Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/list-car',function(data) {
			$('#divOverlay').hide();
			$('#imgOverlay').hide();
			if(!data.error){
				var lstCar = data.lstCarPosition;
				var carId=0;
				var lstTemp=new Array();
				for(var i=0;i<lstCar.length;i++){
					if(SupperviseLogistics._currentCarLogId<lstCar[i].id){
						SupperviseLogistics._currentCarLogId=lstCar[i].id;
					}
					if(carId!=lstCar[i].carId){
						if(carId!=0){
							SupperviseLogistics._lstCarPosition.put(carId,lstTemp);
						}
						carId=lstCar[i].carId;
						var lstTemp=new Array();
					}
					lstTemp.push(lstCar[i]);
				}
				if(carId!=0){
					SupperviseLogistics._lstCarPosition.put(carId,lstTemp);
				}
				SupperviseLogistics.drawCar();
				SupperviseLogistics.proccessCar();
			}
		});
	},
	proccessCar:function(){
		if(SupperviseLogistics._itvCar!=null){
			window.clearInterval(SupperviseLogistics._itvCar);
		}
		SupperviseLogistics._itvCar=window.setInterval( function() {
			SupperviseLogistics.getAppendCar();
		},5000);
	},
	getAppendCar:function(){
		if(SupperviseLogistics._currentShopId==null){
			window.clearInterval(SupperviseLogistics._itvCar);
			return;
		}
		var dataModel=new Object();
		dataModel.shopId=SupperviseLogistics._currentShopId;
		dataModel.maxLogId=SupperviseLogistics._currentCarLogId;
		Utils.getJSONDataByAjaxNotOverlay(dataModel, '/supervise/logistics/list-car',function(data) {
			if(!data.error){
				var lstCar = data.lstCarPosition;
				var carId=0;
				var lstTemp=new Array();
				var lstCarPosition=new Map();
				for(var i=0;i<lstCar.length;i++){
					if(SupperviseLogistics._currentCarLogId<lstCar[i].id){
						SupperviseLogistics._currentCarLogId=lstCar[i].id;
					}
					if(carId!=lstCar[i].carId){
						if(carId!=0){
							lstCarPosition.put(carId,lstTemp);
						}
						carId=lstCar[i].carId;
						var lstTemp=new Array();
					}
					lstTemp.push(lstCar[i]);
				}
				if(carId!=0){
					lstCarPosition.put(carId,lstTemp);
				}
				for(var i=0,n=lstCarPosition.keyArray.length;i<n;i++){
					var carId=lstCarPosition.keyArray[i];
					//append thêm lộ trình xe
					for(var k=0;k<SupperviseLogistics._lstMarkerCarRoute.length;k++){
						if(SupperviseLogistics._lstMarkerCarRoute[k].carId==carId){
							var car=SupperviseLogistics._lstMarkerCarRoute[k];
							for(var j=0;j<lstCarPosition.valArray[i].length;j++){
								car.getPath().insertAt(0,new viettel.LatLng
									(lstCarPosition.valArray[i][j].lat,lstCarPosition.valArray[i][j].lng));
							}
							break;
						}
					}
					//đổi vị trí xe
					for(var k=0;k<SupperviseLogistics._lstMarkerCar.length;k++){
						if(SupperviseLogistics._lstMarkerCar[k].attr.carId==carId){
							var car=SupperviseLogistics._lstMarkerCar[k];
							var n=lstCarPosition.valArray[i].length-1;
							car.attr=lstCarPosition.valArray[i][n];
							car.setPosition(new viettel.LatLng
									(lstCarPosition.valArray[i][n].lat,lstCarPosition.valArray[i][n].lng));
							if(car.attr.isWarning!=null && car.attr.isWarning==1){
								car.setIcon('/resources/images/Mappin/icon_car_red.png');
							}else{
								car.setIcon('/resources/images/Mappin/icon_car_green.png');
							}
							if(car.attr.gpsSpeed==0){
								SupperviseLogistics.getRoutingCustStatus(car.attr.shopId);
							}
							break;
						}
					}
				}
			}
		});
	},
	drawCar:function(){
		var lstCar=SupperviseLogistics._lstCarPosition;
		if(lstCar!=null){
			var indexColor=0;
			var map=null;
			if($('#cbCar').is(':checked')){
				map=ViettelMap._map;
			}
			for(var i =0;i<lstCar.valArray.length;i++){
				if(indexColor>34){
					indexColor=0;
				}
				var car=lstCar.valArray[i];
				//vẽ lộ trình xe
				var points=new Array();
				var carCurrent=null;
				for(var k=0;k<car.length;k++){
					if(SupperviseLogistics.isValidLatLng(car[k].lat, car[k].lng)){
						points.push(new viettel.LatLng(parseFloat(car[k].lat), parseFloat(car[k].lng)));
						if(carCurrent==null) {
							carCurrent=car[k];
						}
					}
				}
				var temp = new viettel.Polyline({
					path: points,
					strokeColor: SupperviseLogistics._colorEx[indexColor],
					strokeOpacity: 1,
					strokeWeight: 4,
					clickable: false,
					map: map
				});
				indexColor++;
				temp.carId=lstCar.keyArray[i];
				SupperviseLogistics._lstMarkerCarRoute.push(temp);
				//vẽ xe
				if(carCurrent!=null){
					SupperviseLogistics.drawOneCar(carCurrent,map);
				}
			}
			if(SupperviseLogistics._notFitOverlay==undefined || SupperviseLogistics._notFitOverlay==null){
				SupperviseLogistics.fitOverLay();
			}
		}
	},
	drawOneCar:function(carCurrent,map){
		var latLng=new viettel.LatLng(carCurrent.lat, carCurrent.lng);
		var url='/resources/images/Mappin/icon_car_green.png';
		if(carCurrent.isWarning==1){
			url='/resources/images/Mappin/icon_car_red.png';
		}
		var markerCar = new viettel.Marker({
			icon:{
				url : url,
				size : {height : 30, width : 30},
				scaledSize : {height : 30, width : 30}
			},
			position: latLng,
			map: map
		});
		markerCar.attr= carCurrent;
		viettel.Events.addListener(markerCar, "click", function(evt) {
			SupperviseLogistics.showWindowInfoCar(markerCar);
		});
		SupperviseLogistics._lstMarkerCar.push(markerCar);
	},
	showWindowInfoCar:function(marker){
		var car = marker.attr;
		var color='';
		if(SupperviseLogistics._lstMarkerRoutingCustRoute!=null){
			for(var i=0;i<SupperviseLogistics._lstMarkerRoutingCustRoute.length;i++){
				if(car.carId==SupperviseLogistics._lstMarkerRoutingCustRoute[i].carId){
					color=SupperviseLogistics._lstMarkerRoutingCustRoute[i].color;
					break;
				}
			}
		}
		if(car!=null){
			var html='';
			var tinhTrang='<dd>Bình thường</dd>';
			if(car.isWarning==1){
				tinhTrang='<dd style="color:red;">Xe lấn vùng</dd>';
			}
			var conlai=0;
			if(car.tongSoDiemGiao!=undefined && car.tongSoDiemGiao!=null){
				if(car.diemDaGiaoTrongTuyen!=undefined && car.diemDaGiaoTrongTuyen!=null){
					conlai=car.tongSoDiemGiao-car.diemDaGiaoTrongTuyen;
				}else{
					conlai=car.tongSoDiemGiao;
				}
			}else{
				car.tongSoDiemGiao=0;
			}
			if(car.fuelPercent==undefined || car.fuelPercent==null){
				car.fuelPercent=0;
			}
			html += '<div id="InfoWindow" class="MapPopupSection MapPopup1Section">';
			html += '<h2 style="text-align:left" class="Title2Style">Xe: '+Utils.XSSEncode(car.carNumber)+'</h2>';
			html += '<h4 style="color: lightslategrey;font-size: 11px;">'+Utils.XSSEncode(car.shopCode!=null?car.shopCode:"")+'</h4>';
			html += '<h4 style="color: lightslategrey;font-size: 11px;">'+Utils.XSSEncode(car.createDate)+'</h4>';
			html += '<div class="MPContent">';
			html += '<dl class="Dl1Style">';
			html += '<dt>Tình trạng:</dt>'+tinhTrang;
			html += '</br><dt style="position: relative;">Tuyến khách hàng: ';
			html +='<hr size="5" style="position: absolute; left: 110px; width: 100px; height: 5px; background-color: '+color+'; top: 4px;border:none"/></dt>'; 
			html += '<hr style="color: lightslategrey;"/>';
			html += '<dt>Tài xế:</dt><dd>'+Utils.XSSEncode(car.staffCode)+'</dd>';
			html += '</br><dt>Số điểm đã giao:  </dt><dd>'+car.diemDaGiao+'</dd>';
			html += '</br><dt>Số điểm còn lại:  </dt><dd>'+conlai+'</dd>';
			html += '</br><dt>Xăng dầu còn lại:  </dt><dd>'+car.fuelPercent+' %</dd>';
			html += '</dl>';
			html += '</div></div>';
			SupperviseLogistics.openWindowInfo(html, marker.getPosition().lat(), marker.getPosition().lng());
		}
	},
	getRoutingCustStatus:function(shopId){
		var dataModel=new Object();
		dataModel.shopId=shopId;
		$('#divOverlay').show();
		$('#imgOverlay').show();
		Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/list-routing-cust-status',function(data) {
			$('#divOverlay').hide();
			$('#imgOverlay').hide();
			if(!data.error){
				var lstCust = data.lstRoutingCustStatus;
				for(var i=0,n=SupperviseLogistics._lstMarkerRoutingCarCust.length;i<n;i++){
					var cust=SupperviseLogistics._lstMarkerRoutingCarCust[i];
					for(var j=0;j<lstCust.length;j++){
						if(cust.attr.customerId==lstCust[j].customerId){
							cust.setIcon('/resources/images/Mappin/icon_circle_blue.png');
							break;
						}
					}
				}
			}
		});
	},
	showRouteCustomer:function(shopId){
		var dataModel=new Object();
		dataModel.shopId=shopId;
		$('#divOverlay').show();
		$('#imgOverlay').show();
		SupperviseLogistics._lstMarkerRoutingCust=new Map();
		SupperviseLogistics._currentCarLogId=0;
		Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/list-routing-cust',function(data) {
			$('#divOverlay').hide();
			$('#imgOverlay').hide();
			if(!data.error){
				var lstCust = data.lstRoutingCustPosition;
				var carId=0;
				var lstTemp=new Array();
				if(lstCust!=null && lstCust.length>0){
					for(var i=0;i<lstCust.length;i++){
						if(carId!=lstCust[i].carId){
							if(carId!=0){
								SupperviseLogistics._lstMarkerRoutingCust.put(carId,lstTemp);
							}
							carId=lstCust[i].carId;
							var lstTemp=new Array();
						}
						lstTemp.push(lstCust[i]);
					}
					if(carId!=0){
						SupperviseLogistics._lstMarkerRoutingCust.put(carId,lstTemp);
					}
					SupperviseLogistics.drawRoutingCust();
					SupperviseLogistics.getRoutingCustStatus(shopId);
				}
//				SupperviseLogistics.proccessRoutingCust();
			}
		});
	},
	drawRoutingCust:function(){
		var lstCust=SupperviseLogistics._lstMarkerRoutingCust;
		if(lstCust!=null){
			indexColor=34;
			var map=null;
			if($('#cbRoute').is(':checked')){
				map=ViettelMap._map;
			}
			for(var i =0;i<lstCust.valArray.length;i++){
				if(indexColor<0){
					indexColor=34;
				}
				var car=lstCust.valArray[i];
				//vẽ lộ trình khách hàng
				var points=new Array();
				for(var k=0;k<car.length;k++){
					if(car[k].seq!=null && car[k].seq!=0 && SupperviseLogistics.isValidLatLng(car[k].lat, car[k].lng)){
						points.push(new viettel.LatLng(parseFloat(car[k].lat), parseFloat(car[k].lng)));
					}
				}
				var temp = new viettel.Polyline({
					path: points,
					strokeColor: SupperviseLogistics._colorEx[indexColor],
					strokeOpacity: 1,
					strokeWeight: 4,
					clickable: false,
					map: map
				});
				temp.carId=lstCust.keyArray[i];
				temp.color=SupperviseLogistics._colorEx[indexColor];//luu lai màu tuyến của xe
				indexColor--;
				SupperviseLogistics._lstMarkerRoutingCustRoute.push(temp);
				//vẽ khách hàng của xe
				for(var j=0;j<car.length;j++){
					SupperviseLogistics.drawCustCar(car[j],map);
				}
			}
			if(SupperviseLogistics._notFitOverlay==undefined || SupperviseLogistics._notFitOverlay==null){
				SupperviseLogistics.fitOverLay();
			}
		}
	},
	drawCustCar:function(car,map){
//		var latLng=new viettel.LatLng(car.lat, car.lng);
//		var url='/resources/images/Mappin/icon_circle_blue.png';
//		var markerCar = new viettel.Marker({
//			icon:{
//				url : url,
//				size : {height : 20, width : 20},
//				scaledSize : {height : 20, width : 20}
//			},
//			position: latLng,
//			map: map
//		});
//		markerCar.attr= car;
//		viettel.Events.addListener(markerCar, "click", function(evt) {
//			SupperviseLogistics.showWindowInfoRoutingCarCust(markerCar);
//		});
//		SupperviseLogistics._lstMarkerRoutingCarCust.push(markerCar);
//		
		var pt = new viettel.LatLng(car.lat, car.lng);
//		var custId=point.customerId;
		var seq=car.seq!=null&&car.seq!=0?car.seq:'';
		var title="<strong class='carCustMarker' style='color:white;left: 16px;position: absolute;top: -20px;font-size: 14px;'>"+seq+"</strong>";
		var image="/resources/images/Mappin/icon_circle_green.png";
		var marker = new viettel.LabelMarker({
			icon:{
				url : image,
				size : {height : 20, width : 20},
				scaledSize : {height : 20, width : 20}
			},
			position : pt,
			map : ViettelMap._map,
			labelContent : title,
			labelClass : "MarkerLabel",
			labelVisible : true,
			draggable : false,
			labelAnchor : new viettel.Point(20, 0)
		});
		marker.attr=car;
		viettel.Events.addListener(marker, "click", function(evt) {
			var html=marker.attr.customerCode;
			SupperviseLogistics.openWindowInfo(html, marker.getPosition().lat(), marker.getPosition().lng());
		});
		SupperviseLogistics._lstMarkerRoutingCarCust.push(marker);
	},
	showWindowInfoRoutingCarCust:function(marker){
		var car = marker.attr;
		if(car!=null){
			var html=car.customerCode;
			SupperviseLogistics.openWindowInfo(html, marker.getPosition().lat(), marker.getPosition().lng());
		}
	},
 	toggleListShop:function(open){
		if(open!=undefined && open!=null){
			if(open==1){
				$('#titleStaff').addClass('OffStyle');
				$('#titleStaff').removeClass('OnStyle');
				$('#listStaff').show();
			}else{
				$('#titleStaff').addClass('OnStyle');
				$('#titleStaff').removeClass('OffStyle');
				$('#listStaff').hide();
			}
		}else{
			if($('.StaffSelectBtmSection .OffStyle').length==1){
				$('#titleStaff').addClass('OnStyle');
				$('#titleStaff').removeClass('OffStyle');
			}else{
				$('#titleStaff').addClass('OffStyle');
				$('#titleStaff').removeClass('OnStyle');
			}
			$('#listStaff').toggle();
		}
	},
 	toggleListCar:function(open){
		if(open!=undefined && open!=null){
			if(open==1){
				$('#titleCar').addClass('OffStyle');
				$('#titleCar').removeClass('OnStyle');
				$('#listCar').show();
			}else{
				$('#titleCar').addClass('OnStyle');
				$('#titleCar').removeClass('OffStyle');
				$('#listCar').hide();
			}
		}else{
			if($('#titleCar.OffStyle').length==1){
				$('#titleCar').addClass('OnStyle');
				$('#titleCar').removeClass('OffStyle');
			}else{
				$('#titleCar').addClass('OffStyle');
				$('#titleCar').removeClass('OnStyle');
			}
			$('#listCar').toggle();
		}
	},
	getShopName:function(shopName){
		var maxlength=25;
		var span='<span title="'+Utils.XSSEncode(shopName)+'">';
		if(shopName==undefined || shopName==null){
			shopName='';
		}
		if(shopName.length>maxlength){
			return span+Utils.XSSEncode(shopName.substring(0,maxlength-3))+'...</span>';
		}
		return span+Utils.XSSEncode(shopName)+'</span>';
	},
	cbOverlay:function(){
		if(SupperviseLogistics._lstPolygon!=null){
			var map=null;
			if($('#cbOverlay').is(':checked')){
				map=ViettelMap._map;
			}
			for(var i=0;i<SupperviseLogistics._lstPolygon.length;i++){
				SupperviseLogistics._lstPolygon[i].setMap(map);
			}
		}
	},
	cbCar:function(){
		if(SupperviseLogistics._lstMarkerCarRoute!=null){
			var map=null;
			if(!$('#cbCar').is(':disabled') && $('#cbCar').is(':checked')){
				map=ViettelMap._map;
			}
			if(map!=null && SupperviseLogistics._lstMarkerCar.length==0 && SupperviseLogistics._currentShopId!=null){
				SupperviseLogistics.showCar(SupperviseLogistics._currentShopId);
			}else{
				SupperviseLogistics.setMapOverlay(map,'Car;CarRoute');
			}
		}
	},
	cbRoute:function(){
		if(SupperviseLogistics._lstMarkerRoutingCustRoute!=null 
				|| SupperviseLogistics._lstMarkerRoutingCarCust!=null){
			var map=null;
			if(!$('#cbRoute').is(':disabled') && $('#cbRoute').is(':checked')){
				map=ViettelMap._map;
				$('.carCustMarker').parent().parent().show();
			}else{
				$('.carCustMarker').parent().parent().hide();
			}
			SupperviseLogistics.setMapOverlay(map,'CustRoute');
		}
	},
	cbCust:function(){
		if(!$('#cbCust').is(':disabled') && $('#cbCust').is(':checked')){
			SupperviseLogistics.showCustShop();
		}else{
			if(SupperviseLogistics._lstMarkerCust!=null){
				if(SupperviseLogistics._itvCust!=null){//xóa tiến trình tạo marker khách hàng nếu có
					window.clearInterval(SupperviseLogistics._itvCust);
				}
				SupperviseLogistics.setMapOverlay(null,'Customer');
			}
		}
	},
	isValidLatLng: function(lat,lng){
		if(lat==undefined || lng == undefined || lat== null || lng== null || lat == 0.0 || lng ==0.0 
				|| lat==-1 || lng==-1){
			return false;
		}
		if (!Number(lat) || !Number(lng)) {
			return false;
		}
		return true;
	},
	updateOverCustomer:function(shopId){
		var dataModel=new Object();
		dataModel.shopId=shopId;
		Utils.getJSONDataByAjax(dataModel, '/supervise/logistics/update-over-customer',function(data) {
			var lst=new Array();
			var b=data.lstCust;
			for(var i=0;i< b.length;i++){
				lst.push(new viettel.LatLng(b[i].lat, b[i].lng));
//				SupperviseLogistics.addMarker(b[i].lat,b[i].lng);
			}
			var polygon = new viettel.Polygon({
				paths: lst,
				strokeColor: "#FF0000",
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: "#FF0000",
				fillOpacity: 0.35,
				clickable: true,
				map: ViettelMap._map
			}); 
		});
	},
	openSearchCarEasyUIDialog: function(shopId,shopType) {
		var html = $('#searchEasyUIDialog').html();
		$('#searchEasyUIDialog').dialog({  
	        title: 'Danh sách xe lấn tuyến',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:510,
	        onOpen: function(){
	        	$('#warningMsg').show();
				$('.easyui-dialog #deliveryDate').focus();
				$('.easyui-dialog #shopId').val(shopId);
				
				Utils.bindAutoSearch();
				$('.easyui-dialog #grid').show();
				$('.easyui-dialog #grid').datagrid({
					url : '/supervise/logistics/list-car-kp',
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
					fitColumns:true,
					queryParams:{
						page:1,
						shopId:shopId
					},
					width : ($('#searchEasyUIDialog').width() - 40),
				    columns:[[  
//						{field:'mien',title:'Mã miền',align:'left', width:110, sortable : false,resizable : false, formatter: function(v,r,i){
//							return Utils.XSSEncode(v);
//						}},
//						{field:'vung',title:'Mã vùng',align:'left', width:110, sortable : false,resizable : false, formatter: function(v,r,i){
//							return Utils.XSSEncode(v);
//						}},
						{field:'shopCode',title:'NPP',align:'left', width:60, sortable : false,resizable : false, formatter: function(v,r,i){
							return Utils.XSSEncode(v);
						}},
						{field:'carNumber',title:'Số xe',align:'left', width:80, sortable : false,resizable : false, formatter: function(v, r,i){
				        	return Utils.XSSEncode(v);
						}},
				        {field:'staffCode',title:'Tài xế',align:'left', width:150, sortable : false,resizable : false, formatter: function(v, r, i){
				        	return Utils.XSSEncode(v);
						}},
						{field:'customerCode',title:'Khách hàng gần nhất',align:'left', width:150, sortable : false,resizable : false, formatter: function(v, r, i){
							return Utils.XSSEncode(v);
						}},
						{field:'distance',title:'Khoảng cách (m)',align:'right', width:80, sortable : false,resizable : false, formatter: function(v, r, i){
							if(v!=null && v==0){
								return '0';
							}
							return Utils.XSSEncode(v);
						}},
						{field:'createTime',title:'Thời gian',align:'center', width:50, sortable : false,resizable : false, formatter: function(v, r, i){
							return Utils.XSSEncode(v);
						}},
				    ]],
				    rowStyler: function(i,r){
						if(r.isWarning!=null && r.isWarning==1){
							return 'color:red';
						}
						return '';
					},
				    onLoadSuccess :function(){
			    		 $('.datagrid-header-rownumber').html('STT');	
			    		 updateRownumWidthForDataGrid('.easyui-dialog');
				    }
				});
				$('.easyui-dialog #btnSearchCar').bind('click',function(event) {
					if(!$(this).is(':hidden')){
						var deliveryDate = $('.easyui-dialog #deliveryDate').val().trim();
						var shopId = $('.easyui-dialog #shopId').val().trim();
						$('.easyui-dialog #grid').datagrid('load',{deliveryDate :deliveryDate,shopId:shopId});						
						$('.easyui-dialog #carNumber').focus();
					}
				});
	        },
	        onBeforeClose: function() {
	        	
	        },
	        onClose : function(){
	        	$('#searchContainerGrid').html('<table id="grid" class="easyui-datagrid" style="width: 520px;"></table><div id="searchStyle1Pager"></div>');
	        }
	    });
		
		return false;
	},
	openSearchOverlayEasyUIDialog: function(shopId,shopType) {
		var html = $('#searchOverlayEasyUIDialog').html();
		$('#searchOverlayEasyUIDialog').dialog({  
	        title: 'Danh sách xe lấn vùng',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:510,
	        onOpen: function(){
	        	$('#warningMsg').show();
				$('.easyui-dialog #deliveryDateOverlay').focus();
				$('.easyui-dialog #shopIdOverlay').val(shopId);
				
				Utils.bindAutoSearch();
				$('.easyui-dialog #gridOverlay').show();
				$('.easyui-dialog #gridOverlay').datagrid({
					url : '/supervise/logistics/list-car-vung-kp',
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
					fitColumns:true,
					queryParams:{
						page:1,
						shopId:shopId
					},
					width : ($('#searchOverlayEasyUIDialog').width() - 40),
				    columns:[[  
						{field:'shopCode',title:'NPP',align:'left', width:60, sortable : false,resizable : false, formatter: function(v,r,i){
							return Utils.XSSEncode(v);
						}},
						{field:'carNumber',title:'Số xe',align:'left', width:80, sortable : false,resizable : false, formatter: function(v, r,i){
				        	return Utils.XSSEncode(v);
						}},
				        {field:'staffCode',title:'Tài xế',align:'left', width:150, sortable : false,resizable : false, formatter: function(v, r, i){
				        	return Utils.XSSEncode(v);
						}},
						{field:'customerCode',title:'Khách hàng gần nhất',align:'left', width:150, sortable : false,resizable : false, formatter: function(v, r, i){
							return Utils.XSSEncode(v);
						}},
						{field:'createTime',title:'Thời gian',align:'center', width:50, sortable : false,resizable : false, formatter: function(v, r, i){
							return Utils.XSSEncode(v);
						}},
				    ]],
				    rowStyler: function(i,r){
						return '';
					},
				    onLoadSuccess :function(){
			    		 $('.datagrid-header-rownumber').html('STT');	
			    		 updateRownumWidthForDataGrid('.easyui-dialog');
				    }
				});
	        },
	        onBeforeClose: function() {
	        	
	        },
	        onClose : function(){
	        	$('#searchContainerGrid').html('<table id="grid" class="easyui-datagrid" style="width: 520px;"></table><div id="searchStyle1Pager"></div>');
	        }
	    });
		
		return false;
	},
	openSearchRoutingExistEasyUIDialog: function(shopId,shopType) {
		var html = $('#searchEasyRoutingUIDialog').html();
		$('#searchEasyRoutingUIDialog').dialog({  
	        title: 'Danh sách xe không đi hết tuyến',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:510,
	        onOpen: function(){
	        	$('#warningMsg').show();
				$('.easyui-dialog #deliveryDateRouting').focus();
				$('.easyui-dialog #shopId').val(shopId);
				
				Utils.bindAutoSearch();
				$('.easyui-dialog #gridRouting').show();
				$('.easyui-dialog #gridRouting').datagrid({
					url : '/supervise/logistics/list-routing-exist-kp',
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
					fitColumns:true,
					queryParams:{
						page:1,
						deliveryDate :$('.easyui-dialog #deliveryDateRouting').val(),
						shopId:shopId
					},
					width : ($('#searchEasyRoutingUIDialog').width() - 40),
				    columns:[[  
						{field:'shopCode',title:'NPP',align:'left', width:180, sortable : false,resizable : false, formatter: function(v,r,i){
							return Utils.XSSEncode(v);
						}},
						{field:'carNumber',title:'Số xe',align:'left', width:80, sortable : false,resizable : false, formatter: function(v, r,i){
				        	return Utils.XSSEncode(v);
						}},
				        {field:'driverCode',title:'Tài xế',align:'left', width:150, sortable : false,resizable : false, formatter: function(v, r, i){
				        	return Utils.XSSEncode(v);
						}},
						{field:'staffCode',title:'Nhân viên',align:'left', width:150, sortable : false,resizable : false, formatter: function(v, r, i){
				        	return Utils.XSSEncode(v);
						}},
						{field:'countWarning',title:'Số KH còn lại',align:'right', width:100, sortable : false,resizable : false, formatter: function(v, r, i){
							if(v==null || v==0){
								return '0';
							}
							return Utils.XSSEncode(v);
						}},
				    ]],
				    rowStyler: function(i,r){
						return '';
					},
				    onLoadSuccess :function(){
			    		 $('.datagrid-header-rownumber').html('STT');	
			    		 updateRownumWidthForDataGrid('.easyui-dialog');
				    }
				});
				$('.easyui-dialog #btnSearchRouting').bind('click',function(event) {
					if(!$(this).is(':hidden')){
						var deliveryDate = $('.easyui-dialog #deliveryDateRouting').val().trim();
						var shopId = $('.easyui-dialog #shopId').val().trim();
						$('.easyui-dialog #gridRouting').datagrid('load',{deliveryDate :deliveryDate,shopId:shopId});						
						$('.easyui-dialog #deliveryDateRouting').focus();
					}
				});
	        },
	        onBeforeClose: function() {
	        	
	        },
	        onClose : function(){
	        	$('#searchRoutingContainerGrid').html('<table id="gridRouting" class="easyui-datagrid" style="width: 520px;"></table><div id="searchStyle1Pager"></div>');
	        }
	    });
		
		return false;
	},
	//---------------------------------------------------------------------------------------------------	
	//---------------------------------------------------------------------------------------------------
	clickMap:function(){
		viettel.Events.addListener(ViettelMap._map, 'click', function(evt){ 
			var latlng = new viettel.LatLng(evt.latLng.lat(),evt.latLng.lng());
			var marker = new viettel.Marker({position: latlng,map: ViettelMap._map});
			viettel.Events.addListener(marker, "click", function(evt) {
				var map_box = new viettel.InfoWindow({
				position: marker.getPosition(),
				content: (marker.getPosition().lat()+","+marker.getPosition().lng()),
				maxWidth: 350
				});
				map_box.open(ViettelMap._map, marker);
				}); 
			var temp=new Object();temp.lat=evt.latLng.lat();temp.lng=evt.latLng.lng();
			SupperviseLogistics.a.push(temp);console.log(evt.latLng.lat()+','+evt.latLng.lng());
			SupperviseLogistics.checkPoint(evt.latLng.lat(),evt.latLng.lng());
		});
	},
	drawPolygon:function(){
		var lst=new Array();
		var b=SupperviseLogistics.b;
		for(var i=0;i< b.length;i++){
//			lst.push(new viettel.LatLng(b[i].lat, b[i].lng));
			SupperviseLogistics.addMarker(b[i].lat,b[i].lng);
		}
//		var polygon = new viettel.Polygon({
//			paths: lst,
//			strokeColor: "#FF0000",
//			strokeOpacity: 0.8,
//			strokeWeight: 2,
//			fillColor: "#FF0000",
//			fillOpacity: 0.35,
//			clickable: true,
//			map: ViettelMap._map
//		}); 
	},
	init:function(){
		SupperviseLogistics.a=new Array();
		var lat=10.772915;
		var lng=106.581867;
		for(var i =0;i<1000;i++){
			var temp = new Object();
			temp.lat=lat+Math.random();
			temp.lng=lng+Math.random();
			temp.goc=0;
			SupperviseLogistics.a.push(temp);
		}
	},
	goc:function(m1,m2){
		var t=0;
		var dx=m2.lat-m1.lat;
		var dy=m2.lng-m1.lng;
		if(dx==0 && dy==0){
			t=0;
		}else{
			t=dy/(Math.abs(dx)+Math.abs(dy));
		}
		if(dx<0){
			t=2-t;
		}else if (dy < 0) {
			t=4+t;
		}
		return t * 90;
	},
	direct:function(m1,m2,m3){
		var dx1=m2.lat-m1.lat;
		var dy1=m2.lng-m1.lng;
		var dx2=m3.lat-m1.lat;
		var dy2=m3.lng-m1.lng;
		var temp=dy2*dx1-dy1*dx2;
		if(temp>0) return 1;
		if(temp<0) return -1;
		if ((dx1*dx2 < 0) || (dy1*dy2 < 0)) return 1;
		else{
			if (Math.sqrt(dx1)+ Math.sqrt(dy1) >= Math.sqrt(dx2) + Math.sqrt(dy2)){
				return 0;
			}else{
				return 1;
			}
		}
	},
	swap:function(){
		var a=SupperviseLogistics.a;
		var i=0,j=0,t=0;
		var q=new Object();
		var g = new Array();
		g.push(0);
		for(i=1;i<a.length;i++){
			g.push(SupperviseLogistics.goc(a[0],a[i]));
			a[i].goc=g[i];
		}
		for(i=1;i<a.length-1;i++){
			for(j=i+1;j<a.length;j++){
				if(g[i]>g[j]){
					t=g[i];g[i]=g[j];g[j]=t;
					var lat=a[i].lat;a[i].lat=a[j].lat;a[j].lat=lat;
					var lng=a[i].lng;a[i].lng=a[j].lng;a[j].lng=lng;
					var goc=a[i].goc;a[i].goc=a[j].goc;a[j].goc=goc;
				}
			}
		}
	},
	graham:function(){
		var a=SupperviseLogistics.a;
		var i=0,min=0,k=0,t=0;
		min=0;
		for(i=1;i<a.length;i++){
			if(a[i].lng<a[min].lng) min=i;
		}
		for(i=1;i<a.length;i++){
			if((a[i].lng==a[min].lng) && (a[i].lat>a[min].lat)) min=i;
		}
		var q=jQuery.extend(true, {}, a[0]);
		a[0]=jQuery.extend(true, {}, a[min]);
		a[min]=jQuery.extend(true, {}, q);
		SupperviseLogistics.swap();
		var b=new Array();
		b.push(a[a.length-1]);
		for(i=0;i<2;i++){
			b.push(jQuery.extend(true, {}, a[i]));
		}
		
		m=2;
		for(i=2;i<a.length;i++){
			if(a[i].lat==a[i-1].lat && a[i].lng==a[i-1].lng) continue;
			while(SupperviseLogistics.direct(b[m-1],b[m],a[i])<=0){
				b.splice(m,1);
				m=m-1;
			}
			m=m+1;//inc(m,1);
//			b[m]=jQuery.extend(true, {}, a[i]);
			b[m]=new Object();
			b[m].lat=a[i].lat;
			b[m].lng=a[i].lng;
		}
		return b;
	},
	calcTime:function(){
		var a=new Date();
		SupperviseLogistics.graham();
		var b=new Date();
		console.log(b-a);
		SupperviseLogistics.drawPolygon();
	},
	checkPoint:function(lat,lng){
		var b=SupperviseLogistics.b;
		var point=new Object();
		point.lat=lat;
		point.lng=lng;
		point.goc=SupperviseLogistics.goc(b[1],point);
		if(b[0].goc<=point.goc && point.goc>=b[b.length-1].goc){
			if(SupperviseLogistics.direct(b[b.length-1],point,b[0])<=0){
				console.log("Trong");
			}else{
				console.log("Ngoài");
			}
		}else{
			for(var i=0;i<b.length-1;i++){
				if(b[i].goc<=point.goc && point.goc<=b[i+1].goc){
					if(SupperviseLogistics.direct(b[i],point,b[i+1])<=0){
						console.log("Trong");
					}else{
						console.log("Ngoài");
					}
				}
			}
		}
	},
	
	//--------------------  Báo cáo  -----------------------------------------------
	exportVT11:function(){
		$('#errMsg').html('').hide();
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if(fDate == '__/__/____'){ $('#fromDate').val(''); }
		if(tDate == '__/__/____'){ $('#toDate').val(''); }
		var msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0 && !Utils.compareDate(fDate,tDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length ==0){
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var arrFromDate = fDate.split("/");
		var arrToDate = tDate.split("/");
		if(msg.length ==0){
			if(arrFromDate[1]!= arrToDate[1] && arrFromDate[2]!= arrToDate[2]){
				msg = 'Từ ngày và Đến ngày bắt buộc phải cùng tháng. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(msg.length ==0 && lstShopId.length == 0) {
			msg = 'Bạn chưa chọn NPP. Vui lòng chọn NPP';
		} 
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = lstShopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		CommonSearch.exportExcelData(dataModel, '/supervise/logistics/bc-vt1-1/export', 'errMsg');
	},
	exportVT12:function(){
		$('#errMsg').html('').hide();
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if(fDate == '__/__/____'){ $('#fromDate').val(''); }
		if(tDate == '__/__/____'){ $('#toDate').val(''); }
		var msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0 && !Utils.compareDate(fDate,tDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length ==0){
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var arrFromDate = fDate.split("/");
		var arrToDate = tDate.split("/");
		if(msg.length ==0){
			if(arrFromDate[1]!= arrToDate[1] && arrFromDate[2]!= arrToDate[2]){
				msg = 'Từ ngày và Đến ngày bắt buộc phải cùng tháng. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(msg.length ==0 && lstShopId.length == 0) {
			msg = 'Bạn chưa chọn NPP. Vui lòng chọn NPP';
		} 
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = lstShopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		CommonSearch.exportExcelData(dataModel, '/supervise/logistics/bc-vt1-2/export', 'errMsg');
	},
	exportVT13:function(){
		$('#errMsg').html('').hide();
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if(fDate == '__/__/____'){ $('#fromDate').val(''); }
		if(tDate == '__/__/____'){ $('#toDate').val(''); }
		var msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0 && !Utils.compareDate(fDate,tDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length ==0){
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var arrFromDate = fDate.split("/");
		var arrToDate = tDate.split("/");
		if(msg.length ==0){
			if(arrFromDate[1]!= arrToDate[1] && arrFromDate[2]!= arrToDate[2]){
				msg = 'Từ ngày và Đến ngày bắt buộc phải cùng tháng. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(msg.length ==0 && lstShopId.length == 0) {
			msg = 'Bạn chưa chọn NPP. Vui lòng chọn NPP';
		} 
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = lstShopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		CommonSearch.exportExcelData(dataModel, '/supervise/logistics/bc-vt1-3/export', 'errMsg');
	},
	exportVT14:function(){
		var msg='';
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(msg.length ==0 && lstShopId.length == 0) {
			msg = 'Bạn chưa chọn NPP. Vui lòng chọn NPP';
		} 
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = lstShopId;
		dataModel.carNumber=$('#carNumber').val().trim();
		CommonSearch.exportExcelData(dataModel, '/supervise/logistics/bc-vt1-4/export', 'errMsg');
	},
	exportVT15:function(){
		$('#errMsg').html('').hide();
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if(fDate == '__/__/____'){ $('#fromDate').val(''); }
		if(tDate == '__/__/____'){ $('#toDate').val(''); }
		var msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0 && !Utils.compareDate(fDate,tDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length ==0){
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var arrFromDate = fDate.split("/");
		var arrToDate = tDate.split("/");
		if(msg.length ==0){
			if(arrFromDate[1]!= arrToDate[1] && arrFromDate[2]!= arrToDate[2]){
				msg = 'Từ ngày và Đến ngày bắt buộc phải cùng tháng. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(msg.length ==0 && lstShopId.length == 0) {
			msg = 'Bạn chưa chọn NPP. Vui lòng chọn NPP';
		} 
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = lstShopId;
		dataModel.carNumber=$('#carNumber').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		CommonSearch.exportExcelData(dataModel, '/supervise/logistics/bc-vt1-5/export', 'errMsg');
	},
	exportVT16:function(){
		$('#errMsg').html('').hide();
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if(fDate == '__/__/____'){ $('#fromDate').val(''); }
		if(tDate == '__/__/____'){ $('#toDate').val(''); }
		var msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0 && !Utils.compareDate(fDate,tDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length ==0){
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var arrFromDate = fDate.split("/");
		var arrToDate = tDate.split("/");
		if(msg.length ==0){
			if(arrFromDate[1]!= arrToDate[1] && arrFromDate[2]!= arrToDate[2]){
				msg = 'Từ ngày và Đến ngày bắt buộc phải cùng tháng. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(msg.length ==0 && lstShopId.length == 0) {
			msg = 'Bạn chưa chọn NPP. Vui lòng chọn NPP';
		} 
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = lstShopId;
		dataModel.carNumber=$('#carNumber').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		CommonSearch.exportExcelData(dataModel, '/supervise/logistics/bc-vt1-6/export', 'errMsg');
	}
};
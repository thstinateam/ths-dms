var SupperviseDevice  = {
	_isSearch:null,
	_lstShopPosition:null,
	_lstToolPosition:null,
	_currentMarkerShop:null,
	_allowDistance:15,
	_markerDistance:null,
	_markerTool:null,
	_notFitOverlay:null,
	_itv:null,
	_isLoadShop:null,
	_lstChipChange:null,
	_currentShopId:null,
	_itvProccessTool:null,
	clearOverlays:function(){
		if (SupperviseDevice._currentMarkerShop!=null) {
			SupperviseDevice._currentMarkerShop.setMap(null);
			SupperviseDevice._currentMarkerShop = null;
		}
		if (SupperviseDevice._markerDistance!=null) {
			SupperviseDevice._markerDistance.setMap(null);
			SupperviseDevice._markerDistance = null;
		}
		if (SupperviseDevice._markerTool!=null) {
			SupperviseDevice._markerTool.setMap(null);
			SupperviseDevice._markerTool = null;
		}
		ViettelMap.clearOverlays();
	},
	loadTreeShop:function(isSearch){
		if(isSearch!=undefined && isSearch!=null && isSearch==1){
			SupperviseDevice._isSearch=1;
			$('#treeGrid').treegrid('reload');
		}else{
			$('#treeGrid').treegrid({  
			    url:  '/supervise/device/list-shop-tool-one-node',
		        height:'auto',  
		        idField: 'id',  
		        treeField: 'text',
		        width:500,
		        //scrollbarSize:0,
		        lines:true,
		        animate:true,
		        fitColumns:true,
			    columns:[[  
			        {field:'text',title:'Đơn vị',width:350,formatter:function(v,r,i){
			        	var isRoot=0;
			        	if(r.attr.shopType!=null && r.attr.shopType==0){
			        		isRoot=1;
			        	}
			        	var str = Utils.XSSEncode(r.attr.shopCode+' - ' +r.attr.shopName);
			        	str=SupperviseDevice.getShopName(str);
			        	var styleTemp = '';
			        	if(r.attr.countWarning!=null && r.attr.countWarning>0){
			        		styleTemp = 'style="color:red"';
			        	}
			        	if(r.attr.shopType!=null && r.attr.shopType==3){
			        		return '<a class="Decoration" onclick="SupperviseDevice.moveToShop(' +r.attr.shopId+',' +isRoot+',1);" ' +styleTemp+' >' +Utils.XSSEncode(str)+'</a>';
			        	}else{
			        		return '<a class="Decoration" onclick="SupperviseDevice.moveToShop(' +r.attr.shopId+',' +isRoot+');" ' +styleTemp+' >' +Utils.XSSEncode(str)+'</a>';
			        	}
			        }},
			        {field:'tool',title:'Số tủ lạnh',width:100,align:'center',formatter:function(v,r,i){
			        	if(r.attr.countTool!=null && r.attr.countTool!=0){
			        		var str='onclick="SupperviseDevice.openSearchToolEasyUIDialog(false,' +r.attr.shopId+',' +r.attr.shopType+');"';
			        		if(r.attr.countWarning!=null && r.attr.countWarning>0){
			        			return '<a class="Decoration" ' +str+' style="color:red">' +Utils.XSSEncode(r.attr.countTool)+'</a>';
			        		}else{
			        			return '<a class="Decoration" ' +str+'>' +Utils.XSSEncode(r.attr.countTool)+'</a>';
			        		}
			        	}else{
			        		return '0';
			        	}
		        	}},
		        	{field:'warning',title:'Cảnh báo',width:80,align:'center',formatter:function(v,r,i){
		        		if(r.attr.countWarning!=null && r.attr.countWarning!=0){
		        			var str='onclick="SupperviseDevice.openSearchToolEasyUIDialog(true,' +r.attr.shopId+',' +r.attr.shopType+');"';
		        			if(r.attr.countWarning!=null && r.attr.countWarning>0){
			        			return '<a class="Decoration" ' +str+' style="color:red">' +Utils.XSSEncode(r.attr.countWarning)+'</a>';
			        		}else{
			        			return '<a class="Decoration" ' +str+'>' +Utils.XSSEncode(r.attr.countWarning)+'</a>';
			        		}
			        	}else{
			        		return '0';
			        	}
		        	}},
		        	{field:'edit',title:'',width:40,align:'left',formatter:function(v,r,i){
			        	var str= '<a href="javascript:void(0)" onclick="SupperviseDevice.showContextMenu(this,' +r.attr.shopId+',' +r.attr.shopType+');">';
			        	str+='<img src="/resources/images/icon_2.png",width:15, height="12"/></a>';
			        	return str;
			        }}
			    ]],
			    rowStyler: function(r){
					if (r.attr.isBold){
						return 'background:none repeat scroll 0 0 #FBEC88';
					}
				},
			    onBeforeLoad:function(n,p){
			    	if(p!=undefined && p!=null){
			    		$('.highlight').css('font-weight','normal');
				    	p.shopId=p.id;
				    	p.typeSup=$('input[name="rbSup"]:checked').val();
				    	if(($('#shopCode').val().trim()!='' || $('#shopName').val().trim()!='') && SupperviseDevice._isSearch!=undefined && SupperviseDevice._isSearch!=null && SupperviseDevice._isSearch==1){
				    		p.shopCode=$('#shopCode').val().trim();
				    		p.shopName=$('#shopName').val().trim();
				    	}
				    	SupperviseDevice._isSearch=null;
			    	}
			    }
			});
		}
	},
	showContextMenu:function(t,shopId,shopType){
		if(shopId!=null && shopId!='null'){
			if(shopType==null){
				shopType = 3;
			}
			$('#cmCountTool').attr('onclick','SupperviseDevice.openSearchToolEasyUIDialog(false,' +shopId+',' +shopType+')').show();
			$('#cmCountWarning').attr('onclick','SupperviseDevice.openSearchToolEasyUIDialog(true,' +shopId+',' +shopType+')').show();
			$('#contextMenu').menu('show', {  
				left: $(t).offset().left+47,  
				top: $(t).offset().top  
			});
		}
	},
	rbChange:function(){
		if(SupperviseDevice._itvProccessTool!=null){
			window.clearInterval(SupperviseDevice._itvProccessTool);
		}
		SupperviseDevice.clearOverlays();
		SupperviseDevice.toggleListShop(1);
		$('#treeGrid').treegrid('reload');
		SupperviseDevice.getListShop();
	},
	addMarkerShop:function(point){
		if(SupperviseDevice._currentMarkerShop!=null){
			SupperviseDevice._currentMarkerShop.setMap(null);
		}
		if($('#marker' +point.shopId).length>0){
			$('#marker' +point.shopId).parent().prev().click();
			return ;
		}
		if(ViettelMap.isValidLatLng(point.lat, point.lng)){
			var pt = new viettel.LatLng(point.lat, point.lng);
			var info="<div id='info" +point.shopId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
			var title=point.shopName;
			var image=point.image;
			
			var markerContent = "<div id='marker" +point.shopId+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'></div>";
			
			
			var marker = new viettel.LabelMarker({
				icon:{
					url : image,
					size : {height : 25, width : 25},
					scaledSize : {height : 25, width : 25}
				},
				position : pt,
				map : ViettelMap._map,
				labelContent : markerContent,
				labelClass : "MarkerLabel",
				labelVisible : true,
				draggable : false,
				labelAnchor : new viettel.Point(25, 0)
			});
			marker.shopId = point.shopId;
			marker.lat = point.lat;
			marker.lng = point.lng;
			ViettelMap._map.setCenter(pt);
			SupperviseDevice._currentMarkerShop = marker;
			$('#marker' +point.shopId).parent().prev().css('z-index', 10000000);
			var parent=$('#marker' +point.shopId).parent().parent();
			var a=parent.css('top');
			parent.css('top',(parseInt(a.substring(0,a.length-2))+10)+'px');
			SupperviseDevice.showWindowInfoShop(point.shopId);
			$('#marker' +point.shopId).parent().prev().bind('click', point, function(e) {
				var point = e.data;
				SupperviseDevice.showWindowInfoShop(point.shopId); 
			});
			
		}
	},
	addMarkerTool:function(point){
		if(SupperviseDevice._markerTool!=null){
			SupperviseDevice._markerTool.setMap(null);
		}
		if($('#markerT' +point.toolId).length>0){
			$('#markerT' +point.toolId).parent().prev().click();
			return ;
		}
		if(ViettelMap.isValidLatLng(point.lat, point.lng)){
			var pt = new viettel.LatLng(point.lat, point.lng);
			var info="<div id='info" +point.shopId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
			var title=point.shopName;
			var image=point.image;
			
			var markerContent = "<div id='markerT" +point.toolId+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'></div>";
			
			var marker = new viettel.LabelMarker({
				icon:{
					url : image,
					size : {height : 25, width : 25},
					scaledSize : {height : 25, width : 25}
				},
				position : pt,
				map : ViettelMap._map,
				labelContent : markerContent,
				labelClass : "MarkerLabel",
				labelVisible : true,
				draggable : false,
				labelAnchor : new viettel.Point(25, 0)
			});
			marker.point = point;
			ViettelMap._map.setCenter(pt);
			ViettelMap._map.setZoom(13);
			SupperviseDevice._markerTool = marker;
			$('#markerT' +point.toolId).parent().prev().css('z-index', 10000000);
			var parent=$('#markerT' +point.toolId).parent().parent();
			var a=parent.css('top');
			parent.css('top',(parseInt(a.substring(0,a.length-2))+10)+'px');
			console.log(point.toolId+'/' +a);
			$('#markerT' +point.toolId).parent().prev().bind('click', point, function(e) {
				var point = e.data;
				SupperviseDevice.showWindowInfoTool(point);
			});
			$('#markerT' +point.toolId).parent().prev().click();
			SupperviseDevice._lstToolPosition.put(point.toolId,point);
		}
	},
	proccessPosition:function(){
		if(SupperviseDevice._itvProccessTool!=null){
			window.clearInterval(SupperviseDevice._itvProccessTool);
		}
		SupperviseDevice._itvProccessTool=window.setInterval( function() {
			if(ViettelMap._map!=null && SupperviseDevice._currentShopId!=null){
				SupperviseDevice._notFitOverlay=1;
//				SupperviseDevice.moveToShop(SupperviseDevice._currentShopId,0,1);
				if($('#titleTool.OffStyle').length==1){
					SupperviseDevice.openSearchToolEasyUIDialogEx(true,SupperviseDevice._currentShopId,3);
				}
			}
		},60000);
	},
	moveToShop:function(shopId,root,ex){//ex mở dialog góc phải bên trên
		window.clearInterval(SupperviseDevice._itvProccessTool);
		SupperviseDevice._currentShopId=null;
		if(shopId!=undefined && shopId!=null){
			var temp = SupperviseDevice._lstShopPosition.get(shopId);
			if(temp!=null){
				if(root!=undefined && root!=null && root==1){//root
					SupperviseDevice.reloadMarker(1);
				}else{
					SupperviseDevice.addMarkerShop(temp);
				}
			}else{
				if(root!=undefined && root!=null && root==1){
					SupperviseDevice.reloadMarker(1);
				}else{
					SupperviseDevice._currentShopId=shopId;
					SupperviseDevice.getListToolForShop(shopId);
					if(ex!=undefined && ex!=null && ex==1){
						SupperviseDevice.openSearchToolEasyUIDialogEx(true,shopId,3);
					}else{
						SupperviseDevice.openSearchToolEasyUIDialog(false,shopId,3);
					}
//					SupperviseDevice.toggleListShop(0);
					SupperviseDevice.proccessPosition();
				}
			}
		}
	},
	moveToTool:function(toolId,lat,lng,i){
		$('#warningMsg').hide();
		if($('#markerT' +toolId).length==0){
			if(lat!=null && lat<=0 && lng!=null && lng<=0){
				$('#warningMsg').html('Không tìm thấy vị trí tủ lạnh').show();
				return;
			}
			$('#toolGrid').datagrid('selectRow',i);
			var point = $('#toolGrid').datagrid('getSelected');
			if(point!=null){
				if(point.isWarning!=null && parseInt(point.isWarning)==1){
					point.image="/resources/images/Mappin/icon_tool_red.png";
				}else {
					point.image="/resources/images/Mappin/icon_tool_green.png";
				}
				SupperviseDevice.addMarkerTool(point);
			}
		}else{
			$('#markerT' +toolId).parent().prev().click();
		}
		$('#searchToolEasyUIDialog').dialog('close');
	},
	getListToolForShop:function(shopId){
		try{
			var data = new Object();
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			data.shopId=shopId;
			data.typeSup=$('input[name="rbSup"]:checked').val();
			Utils.getHtmlDataByAjaxNotOverlay(data, '/supervise/device/list-tool',function(result) {
				if(!result.error){
					SupperviseDevice._lstToolPosition=new Map();
					var data = JSON.parse(result);
					var lstTool = data.lstTool;
					if(lstTool!=null && lstTool.length>0){ 
						for(i=0;i<lstTool.length;i++){
							var temp=lstTool[i];
							if(temp.isWarning!=null && parseInt(temp.isWarning)==1){
								temp.image="/resources/images/Mappin/icon_tool_red.png";
							}else {
								temp.image="/resources/images/Mappin/icon_tool_green.png";
							}
							SupperviseDevice._lstToolPosition.put(temp.toolId,temp);
						}
					}
					SupperviseDevice.reloadMarker(2);
				}
			}, 'loading2', null);
		}catch(err){
			$('#loading2').hide();
		}
	},
	toggleListShop:function(open){
		if(open!=undefined && open!=null){
			if(open==1){
				$('#titleStaff').addClass('OffStyle');
				$('#titleStaff').removeClass('OnStyle');
				$('#listStaff').show();
			}else{
				$('#titleStaff').addClass('OnStyle');
				$('#titleStaff').removeClass('OffStyle');
				$('#listStaff').hide();
			}
		}else{
			if($('.StaffSelectBtmSection .OffStyle').length==1){
				$('#titleStaff').addClass('OnStyle');
				$('#titleStaff').removeClass('OffStyle');
			}else{
				$('#titleStaff').addClass('OffStyle');
				$('#titleStaff').removeClass('OnStyle');
			}
			$('#listStaff').toggle();
		}
	},
	toggleListTool:function(open){
		if(open!=undefined && open!=null){
			if(open==1){
				$('#titleTool').addClass('OffStyle');
				$('#titleTool').removeClass('OnStyle');
				$('#listTool').show();
			}else{
				$('#titleTool').addClass('OnStyle');
				$('#titleTool').removeClass('OffStyle');
				$('#listTool').hide();
			}
		}else{
			if($('#titleTool.OffStyle').length==1){
				$('#titleTool').addClass('OnStyle');
				$('#titleTool').removeClass('OffStyle');
			}else{
				$('#titleTool').addClass('OffStyle');
				$('#titleTool').removeClass('OnStyle');
			}
			$('#listTool').toggle();
		}
	},
	getShopName:function(shopName){
		var maxlength=40;
		var span='<span title="' +Utils.XSSEncode(shopName)+'">';
		if(shopName==undefined || shopName==null){
			shopName='';
		}
		if(shopName.length>maxlength){
			return span+Utils.XSSEncode(shopName.substring(0,maxlength-3))+'...</span>';
		}
		return span+Utils.XSSEncode(shopName)+'</span>';
	},
	getListShop:function(){
		try{
			var data = new Object();
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			data.typeSup=$('input[name="rbSup"]:checked').val();
			Utils.getHtmlDataByAjax(data, '/supervise/device/list-shop-tool',function(result) {
				if(!result.error){
					SupperviseDevice._lstShopPosition=new Map();
					var data = JSON.parse(result);
					var lstShop = data.lstShop;
					if(lstShop!=null && lstShop.length>0){ 
						for(i=0;i<lstShop.length;i++){
							var temp=lstShop[i];
							if(parseInt(temp.countWarning)==0){
								temp.image="/resources/images/Mappin/icon_tool_green.png";
							}else {
								temp.image="/resources/images/Mappin/icon_tool_red.png";
							}
							SupperviseDevice._lstShopPosition.put(temp.shopId,temp);
						}
					}
					SupperviseDevice.reloadMarker(1);
				}
			}, 'loading2', null);
		}catch(err){
			$('#loading2').hide();
		}
	},
	refresh:function(){
		if(SupperviseDevice._currentShopId!=null){
			SupperviseDevice._notFitOverlay=1;
			SupperviseDevice.moveToShop(SupperviseDevice._currentShopId,0,1);
		}else{
			SupperviseDevice.getListShop();
			$('#treeGrid').treegrid('reload');
		}
	},
	reloadMarker:function(type){//type: 1:shop , 2:tool
		try{
			if(SupperviseDevice._itv!=undefined && SupperviseDevice._itv!=null)
				window.clearInterval(SupperviseDevice._itv);
		}catch(e){}
		if(ViettelMap._map!=null){
			if(SupperviseDevice._notFitOverlay==null){
				ViettelMap._map.setZoom(5);
			}
			ViettelMap._listMarker=new Map();
			SupperviseDevice.fillListMarker(type);
			SupperviseDevice.clearOverlays();
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			if(type==1){
				SupperviseDevice.addMutilMarkerShop();
			}else{
				SupperviseDevice.addMutilMarkerTool();
			}
		}else{
			var interval=window.setInterval( function() {
				if(ViettelMap._map!=null){
					ViettelMap._listMarker=new Map();
					SupperviseDevice.fillListMarker(type);
					SupperviseDevice.clearOverlays();
					if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
						ViettelMap._currentInfoWindow.close();
					}
					if(type==1){
						SupperviseDevice.addMutilMarkerShop();
					}else{
						SupperviseDevice.addMutilMarkerTool();
					}
					window.clearInterval(interval);
				}
			},500);
		}
	},
	fillListMarker:function(type){//type: 1:shop , 2:tool
		if(type==1){//shop
			SupperviseDevice._isLoadShop=1;
			if(SupperviseDevice._lstShopPosition!=null){
				for(var i=0;i<SupperviseDevice._lstShopPosition.valArray.length;i++){
					var temp = SupperviseDevice._lstShopPosition.valArray[i];
					if(temp.lat!=null && temp.lng!=null){
						if((ViettelMap._map.getZoom()<=7 && temp.shopType==1)
								|| (ViettelMap._map.getZoom()>=8 && temp.shopType==2)){
							ViettelMap._listMarker.put(temp.shopId,temp);
						}
					}
				}
			}
		}else{//tool
			SupperviseDevice._isLoadShop=0;
			if(SupperviseDevice._lstToolPosition!=null){
				for(var i=0;i<SupperviseDevice._lstToolPosition.valArray.length;i++){
					var temp = SupperviseDevice._lstToolPosition.valArray[i];
					if(temp.lat!=null && temp.lng!=null){
						ViettelMap._listMarker.put(temp.toolId,temp);
					}
				}
			}
		}
	},
	openWindowInfo:function(html,lat,lng){
		if(ViettelMap._currentInfoWindow != null) {
			ViettelMap._currentInfoWindow.close();
		}
		var infoWindow = new viettel.InfoWindow({
			content: html,
			position:  new viettel.LatLng(lat, lng)
		});
		infoWindow.open(ViettelMap._map);
		ViettelMap._currentInfoWindow = infoWindow;
		$('#InfoWindow').parent().css('width','');
		$('#InfoWindow').parent().css('overflow','');
	},
	showWindowInfoCustomer:function(pt){
		if(pt!=null) {
			var html = '<div id="InfoWindow" class="MapPopupSection MapPopup1Section">';
			html += '<h2 style="text-align:left" class="Title2Style">' +Utils.XSSEncode(pt.customerCode+' - ' +pt.customerName)+'</h2>';
			html += '<h4  style="color: lightslategrey;font-size: 11px;">' +Utils.XSSEncode(pt.customerCode+'-' +pt.customerName+', ' +pt.address)+'</h4>';
			SupperviseDevice.openWindowInfo(html, pt.latCustomer, pt.lngCustomer);
		}
	},
	viewDistance:function(t,toolId){
		if($(t).html()=='Xem bản đồ'){
			$(t).html('Bỏ xem bản đồ');
			var point=SupperviseDevice._lstToolPosition.get(toolId);
			if(point!=null){
				if(SupperviseDevice._currentMarkerShop!=null){
					SupperviseDevice._currentMarkerShop.setMap(null);
				}
				if(ViettelMap.isValidLatLng(point.latCustomer, point.lngCustomer)){
					var pt = new viettel.LatLng(point.latCustomer, point.lngCustomer);
					var info="<div id='info" +point.customerCode+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
					var image='/resources/images/Mappin/blue.png';
					
					var markerContent = "<div id='markerC" +point.customerCode+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'></div>";
					
					
					var marker = new viettel.LabelMarker({
						icon:{
							url : image,
							size : {height : 39, width : 20},
							scaledSize : {height : 39, width : 20}
						},
						position : pt,
						map : ViettelMap._map,
						labelContent : markerContent,
						labelClass : "MarkerLabel",
						labelVisible : true,
						draggable : false,
						labelAnchor : new viettel.Point(25, 0)
					});
					marker.shopId = point.shopId;
					marker.lat = point.lat;
					marker.lng = point.lng;
					SupperviseDevice._currentMarkerShop = marker;
					$('#markerC' +point.customerCode).parent().prev().css('z-index', 10000000);
					
					$('#markerC' +point.customerCode).parent().prev().bind('click', point, function(e) {
						var point = e.data;
						SupperviseDevice.showWindowInfoCustomer(point); 
					});
					if (SupperviseDevice._markerDistance!=null) {
						SupperviseDevice._markerDistance.setMap(null);
						SupperviseDevice._markerDistance = null;
					}
					var points = new Array();
					points.push(new viettel.LatLng(point.lat, point.lng));
					points.push(new viettel.LatLng(point.latCustomer, point.lngCustomer));
					SupperviseDevice._markerDistance = new viettel.Polyline({
						path: points,
						strokeColor: "#CC0000",
						strokeOpacity: 0.5,
						strokeWeight: 3,
						clickable: true,
						map: ViettelMap._map
					});	
				}
			}
		}else{
			$(t).html('Xem bản đồ');
			if (SupperviseDevice._currentMarkerShop!=null) {
				SupperviseDevice._currentMarkerShop.setMap(null);
				SupperviseDevice._currentMarkerShop = null;
			}
			if (SupperviseDevice._markerDistance!=null) {
				SupperviseDevice._markerDistance.setMap(null);
				SupperviseDevice._markerDistance = null;
			}
		}
	},
	showWindowInfoTool: function(pt) {
		if(pt!=null) {
			var strStyleRed = ' style="color:red" ';
			var html='';
			if($('input[name="rbSup"]:checked').val()=='0'){//vi tri
				html += '<div id="InfoWindow" class="MapPopupSection MapPopup1Section">';
				html += '<h2 style="text-align:left" class="Title2Style">Thiết bị: ' +Utils.XSSEncode(pt.toolCode)+'</h2>';
				html += '<h4 style="color: lightslategrey;font-size: 11px;">' +Utils.XSSEncode(pt.customerCode+'-' +pt.customerName+', ' +pt.address)+'</h4>';
				html += '<div class="MPContent">';
				html += '<dl class="Dl1Style">';
				if(pt.isWarning==1){
					html += '<dt ' +strStyleRed+'>Khoảng cách sai lệch:</dt><dd ' +strStyleRed+'>' +Utils.XSSEncode(pt.distance)+' (m)</dd>';
				}else{
					html += '<dt>Khoảng cách sai lệch:</dt><dd>' +Utils.XSSEncode(pt.distance)+' (m)</dd>';
				}
				if(pt.latCustomer!=null && pt.latCustomer>0 && pt.lngCustomer!=null && pt.lngCustomer>0){
					html += '<dt><a href="javascript:void(0)" onclick="SupperviseDevice.viewDistance(this,' +pt.toolId+');" >Xem bản đồ</a></dt>';
				}
				html += '<br/><dt>Vị trí cập nhật lúc:</dt><dd>' +Utils.XSSEncode(pt.createDate)+'</dd>';
				html += '<dt>Độ chính xác:</dt><dd>' +Utils.XSSEncode(pt.accuracy)+' (m)</dd>';
				html += '<hr style="color: lightslategrey;"/>';
				html += '<dt>Độ sai lệch cho phép:</dt><dd>' +Utils.XSSEncode(SupperviseDevice._allowDistance)+' (m)</dd>';
				html += '</dl>';
				html += '<div class="GeneralTable Table1Section">';
				html += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
				html += '<colgroup><col style="width:90px;" /><col style="width:50px;" /></colgroup>';
				html += '<thead><tr><th class="FirstThStyle">Thông tin thiết bị</th><th>Giá trị</th></tr></thead>';
				html += '<tbody>';
				html += '<tr><td><div class="AlignLCols">Năm sản xuất</div></td>';
				html += '<td><div class="AlignRCols">' +(pt.productYear!=null?pt.productYear:'')+'</div></td></tr>';
				
				html += '<tr><td><div class="AlignLCols">Thời gian bảo hành</div></td>';
				html += '<td><div class="AlignRCols">' +(pt.warranty!=null?pt.warranty:'')+'</div></td></tr>';
				html += '</tbody></table></div></div></div>';
			}else{//nhiet do
				html += '<div id="InfoWindow" class="MapPopupSection MapPopup1Section">';
				html += '<h2 style="text-align:left" class="Title2Style">Thiết bị: ' +Utils.XSSEncode(pt.toolCode)+'</h2>';
				html += '<h4 style="color: lightslategrey;font-size: 11px;">' +Utils.XSSEncode(pt.customerCode+'-' +pt.customerName+', ' +pt.address)+'</h4>';
				html += '<div class="MPContent">';
				html += '<dl class="Dl1Style">';
				if(pt.isWarning==1){
					html += '<dt ' +strStyleRed+'>Nhiệt độ hiện tại:</dt><dd ' +strStyleRed+'>' +Utils.XSSEncode(pt.temperature)+' &deg;C</dd>';
				}else{
					html += '<dt>Nhiệt độ hiện tại:</dt><dd>' +Utils.XSSEncode(pt.temperature)+' &deg;C</dd>';
				}
				html += '<br/><dt>Nhiệt độ cập nhật lúc:</dt><dd>' +Utils.XSSEncode(pt.createDate)+'</dd>';
				html += '<hr style="color: lightslategrey;"/>';
				html += '<dt>Khoảng nhiệt độ cho phép:</dt><dd>' +pt.minTemp+' &deg;C - ' +pt.maxTemp+' &deg;C</dd>';
				html += '</dl>';
				html += '<div class="GeneralTable Table1Section">';
				html += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
				html += '<colgroup><col style="width:90px;" /><col style="width:50px;" /></colgroup>';
				html += '<thead><tr><th class="FirstThStyle">Thông tin thiết bị</th><th>Giá trị</th></tr></thead>';
				html += '<tbody>';
				html += '<tr><td><div class="AlignLCols">Năm sản xuất</div></td>';
				html += '<td><div class="AlignRCols">' +(pt.productYear!=null?pt.productYear:'')+'</div></td></tr>';
				
				html += '<tr><td><div class="AlignLCols">Thời gian bảo hành</div></td>';
				html += '<td><div class="AlignRCols">' +(pt.warranty!=null?pt.warranty:'')+'</div></td></tr>';
				html += '</tbody></table></div></div></div>';
			}
			SupperviseDevice.openWindowInfo(html, pt.lat, pt.lng);
		}
	},
	addMutilMarkerShop:function(){
		var map = ViettelMap._map;
		if(ViettelMap._listMarker!=undefined && ViettelMap._listMarker!=null){
			var flag=0;
			var index=0;
			SupperviseDevice._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<ViettelMap._listMarker.valArray.length;i++,index++,j++){
					if(j>100) break;
					var point = ViettelMap._listMarker.valArray[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new viettel.LatLng(point.lat, point.lng);
						var info="<div id='info" +point.shopId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
						var title=point.shopName;
						var image=point.image;
						
						var markerContent = "<div id='marker" +point.shopId+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'></div>";
						
						
						var marker = new viettel.LabelMarker({
							icon:{
								url : image,
								size : {height : 25, width : 25},
								scaledSize : {height : 25, width : 25}
							},
							position : pt,
							map : ViettelMap._map,
							labelContent : markerContent,
							labelClass : "MarkerLabel",
							labelVisible : true,
							draggable : false,
							labelAnchor : new viettel.Point(25, 0)
						});
						marker.shopId = point.shopId;
						marker.lat = point.lat;
						marker.lng = point.lng;
						
						$('#marker' +point.shopId).parent().prev().css('z-index', 10000000);
						var parent=$('#marker' +point.shopId).parent().parent();
						var a=parent.css('top');
						parent.css('top',(parseInt(a.substring(0,a.length-2))+10)+'px');
						$('#marker' +point.shopId).parent().prev().bind('click', point, function(e) {
							var point = e.data;
							SupperviseDevice.showWindowInfoShop(point.shopId, point.lat, point.lng); 
						});
						
						
						if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
							ViettelMap._listOverlay = new Array();
							ViettelMap._listOverlay.push(marker);
						} else {
							ViettelMap._listOverlay.push(marker);
						}
						
					}
				}
				if(index>=ViettelMap._listMarker.valArray.length){
					if(SupperviseDevice._notFitOverlay==undefined || SupperviseDevice._notFitOverlay==null){
						ViettelMap.fitOverLay();
					}
					SupperviseDevice._notFitOverlay=null;
					window.clearInterval(SupperviseDevice._itv);
				}
			},300); 
		}
	},
	getListSubShop:function(shopId,lat,lng){
		var pt=SupperviseDevice._lstShopPosition.get(shopId);
		if(pt!=null){
			$('#InfoWindow').html('<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15"/>');
			var str='?shopId=' +pt.shopId+'&typeSup=' +$('input[name="rbSup"]:checked').val();
			$.getJSON('/supervise/device/list-sub-shop-tool' +str, function(data) {
				var strStyleRed = '';
				if(pt.countWarning!=null && pt.countWarning>0){
					strStyleRed=' style="color:red;" ';
				}
				var html = '<div id="InfoWindow" class="MapPopupSection MapPopup1Section">';
				html += '<h2 style="text-align:left" class="Title2Style">' +Utils.XSSEncode(pt.shopName)+'</h2>';
				html += '<div class="MPContent">';
				html += '<div class="GeneralTable Table1Section">';
				html += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
				html += '<colgroup><col style="width:120px;" /><col style="width:30px;" /><col style="width:30px;" /></colgroup>';
				html += '<thead><tr><th class="FirstThStyle">Đơn vị</th><th>Số tủ lạnh</th><th>Cảnh báo</th></tr></thead>';
				html += '<tbody>';
				if(data.lst!=null){
					var lst=data.lst;
					for(var i=0;i<lst.length;i++){
						var strStyleTemp='';
						if(lst[i].countWarning!=null && lst[i].countWarning>0){
							strStyleTemp=' style="color:red;" ';
						}
						var aTag = '<a href="javascript:void(0)" onclick="SupperviseDevice.getListSubShop(' +lst[i].shopId+',' +lat+',' +lng+')">';
						aTag += Utils.XSSEncode(lst[i].shopCode+' - ' +lst[i].shopName)+'</a>';
						html += '<tr ' +strStyleTemp+'><td><div class="AlignLCols">' +aTag+'</div></td>';
						html += '<td><div class="AlignCCols">' +(lst[i].countTool!=null?lst[i].countTool:'0')+'</div></td>';
						html += '<td><div class="AlignCCols">' +(lst[i].countWarning!=null?lst[i].countWarning:'0')+'</div></td></tr>';
					}
				}
				html += '<tr ' +strStyleRed+'><td><div class="AlignLCols">Tổng</div></td>';
				html += '<td><div class="AlignCCols">' +(pt.countTool!=null?pt.countTool:'0')+'</div></td>';
				html += '<td><div class="AlignCCols">' +(pt.countWarning!=null?pt.countWarning:'0')+'</div></td></tr>';
				
				html += '</tbody></table></div></div></div>';
				SupperviseDevice.openWindowInfo(html, lat, lng);
			});
		}else{
			SupperviseDevice.moveToShop(shopId,0,1);
		}
	},
	showWindowInfoShop:function(shopId){		
		if(shopId!=null && shopId>0){
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			var pt=SupperviseDevice._lstShopPosition.get(shopId);
			if(pt!=null) {
				var html = '<div id="InfoWindow" class="MapPopupSection MapPopup1Section">';
				html += '</div>';
				SupperviseDevice.openWindowInfo(html, pt.lat, pt.lng);
				SupperviseDevice.getListSubShop(shopId,pt.lat, pt.lng);
			}
		}
	},
	addMutilMarkerTool:function(){
		var map = ViettelMap._map;
		if(ViettelMap._listMarker!=undefined && ViettelMap._listMarker!=null){
			var flag=0;
			var index=0;
			SupperviseDevice._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<ViettelMap._listMarker.valArray.length;i++,index++,j++){
					if(j>100) break;
					var point = ViettelMap._listMarker.valArray[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new viettel.LatLng(point.lat, point.lng);
						var info="<div id='info" +point.shopId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
						var title=point.shopName;
						var image=point.image;
						
						var markerContent = "<div id='markerT" +point.toolId+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'></div>";
						
						
						var marker = new viettel.LabelMarker({
							icon:{
								url : image,
								size : {height : 25, width : 25},
								scaledSize : {height : 25, width : 25}
							},
							position : pt,
							map : ViettelMap._map,
							labelContent : markerContent,
							labelClass : "MarkerLabel",
							labelVisible : true,
							draggable : false,
							labelAnchor : new viettel.Point(25, 0)
						});
						marker.point = point;
						$('#markerT' +point.toolId).parent().prev().css('z-index', 10000000);
						var parent=$('#markerT' +point.toolId).parent().parent();
						var a=parent.css('top');
						parent.css('top',(parseInt(a.substring(0,a.length-2))+10)+'px');
						$('#markerT' +point.toolId).parent().prev().bind('click', point, function(e) {
							var point = e.data;
							SupperviseDevice.showWindowInfoTool(point);
						});
						
						
						if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
							ViettelMap._listOverlay = new Array();
							ViettelMap._listOverlay.push(marker);
						} else {
							ViettelMap._listOverlay.push(marker);
						}
						
					}
				}
				if(index>=ViettelMap._listMarker.valArray.length){
//					ViettelMap.hideShowTitleMarker();
					if(SupperviseDevice._notFitOverlay==undefined || SupperviseDevice._notFitOverlay==null){
						ViettelMap.fitOverLay();
					}
					SupperviseDevice._notFitOverlay=null;
					window.clearInterval(SupperviseDevice._itv);
				}
//				ViettelMap.hideShowTitleMarker();
			},300); 
		}
	},
	openSearchToolEasyUIDialog: function(isWarning,shopId,shopType) {//isWarning:true load cảnh báo
		var html = $('#searchToolEasyUIDialog').html();
		$('#searchToolEasyUIDialog').dialog({  
	        title: 'Danh sách thiết bị',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:510,
	        onOpen: function(){
	        	$('#warningMsg').show();
				$('.easyui-dialog #toolCode').focus();
				$('.easyui-dialog #shopId').val(shopId);
				$('.easyui-dialog #isWarning').val(isWarning);
				$('.easyui-dialog #btnSearchTool').css('margin-left', 270);
				$('.easyui-dialog #btnSearchTool').css('margin-top', 5);				
				
				Utils.bindAutoSearch();
				$('.easyui-dialog #toolGrid').show();
				$('.easyui-dialog #toolGrid').datagrid({
					url : '/supervise/device/list-tool-kp',
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
					fitColumns:true,
					queryParams:{
						page:1,
						shopId:shopId,
						isWarning:isWarning,
						typeSup:$('input[name="rbSup"]:checked').val()
					},
					width : ($('#searchToolEasyUIDialog').width() - 40),
				    columns:[[  
				        {field:'toolCode',title:'Mã thiết bị',align:'left', width:110, sortable : false,resizable : false, formatter: function(v, r,i){
				        	if(r.lat!=null && r.lng!=null){
					        	var lat=r.lat!=null?r.lat:0;
					        	var lng=r.lng!=null?r.lng:0;
								return '<a class="Decoration" href="javascript:void(0);" onclick="SupperviseDevice.moveToTool(' +r.toolId+',' +lat+',' +lng+',' +i+');">' +Utils.XSSEncode(v)+'</a>';
				        	}else{
				        		return Utils.XSSEncode(v);
				        	}
						}},
						{field:'mien',title:'Mã miền',align:'left', width:110, sortable : false,resizable : false, formatter: function(v,r,i){
							return Utils.XSSEncode(v);
						}},
						{field:'vung',title:'Mã vùng',align:'left', width:110, sortable : false,resizable : false, formatter: function(v,r,i){
							return Utils.XSSEncode(v);
						}},
						{field:'shopCode',title:'Mã NPP',align:'left', width:110, sortable : false,resizable : false, formatter: function(v,r,i){
							return Utils.XSSEncode(v);
						}},
				        {field:'customerCode',title:'Điểm lẻ',align:'left', width:$('#searchToolEasyUIDialog').width() - 230, sortable : false,resizable : false, formatter: function(v, r, i){
							return Utils.XSSEncode(r.customerCode + ' - ' +r.customerName+', ' +r.address);
						}},
				        {field :'toolId',hidden : true, formatter: function(v,r,i){
							return Utils.XSSEncode(v);
						}},
				    ]],
				    rowStyler: function(i,r){
						if(r.isWarning!=null && r.isWarning==1){
							return 'color:red';
						}
						return '';
					},
				    onLoadSuccess :function(){
			    		 $('.datagrid-header-rownumber').html('STT');	
			    		 updateRownumWidthForDataGrid('.easyui-dialog');
			    		 if(shopType!=undefined){
			    			 if(shopType>0){
			    				 $('#toolGrid').datagrid('hideColumn','mien');
			    			 }
			    			 if(shopType>1){
			    				 $('#toolGrid').datagrid('hideColumn','vung');
			    			 }
			    			 if(shopType>2){
			    				 $('#toolGrid').datagrid('hideColumn','shopCode');
			    			 }
			    		 }
				    }
				});
				$('.easyui-dialog #btnSearchTool').bind('click',function(event) {
					if(!$(this).is(':hidden')){
						var toolCode = $('.easyui-dialog #toolCode').val().trim();
						var customerCode = $('.easyui-dialog #customerCode').val().trim();
						var shopId = $('.easyui-dialog #shopId').val().trim();
						var isWarning = $('.easyui-dialog #isWarning').val().trim();
						$('.easyui-dialog #toolGrid').datagrid('load',{toolCode :toolCode, customerCode: customerCode
								,shopId:shopId, isWarning:isWarning,typeSup:$('input[name="rbSup"]:checked').val()});						
						$('.easyui-dialog #toolCode').focus();
					}
				});
				$('.easyui-dialog #btnClose').bind('click',function(event) {
					$('#searchToolEasyUIDialog.easyui-dialog').dialog('close');
				});
	        },
	        onBeforeClose: function() {
	        	
	        },
	        onClose : function(){
	        	$('#searchToolContainerGrid').html('<table id="toolGrid" class="easyui-datagrid" style="width: 520px;"></table><div id="searchStyle1Pager"></div>');
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #seachStyle1Address').val('');	        	
	        }
	    });
		
		return false;
	},
	openSearchToolEasyUIDialogEx: function(isWarning,shopId,shopType) {//isWarning:true load cảnh báo
		SupperviseDevice.toggleListTool(1);
		var width=$(window).width()-$('#promotionShopGrid').width()-100;
		if($('.StaffSelectBtmSection .OffStyle').length==0){
			width=$(window).width()-550;
		}
		$('#toolGridEx').datagrid({
			url : '/supervise/device/list-tool-kp',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			pageSize:10,
			scrollbarSize : 0,
			singleSelect:true,
			pageNumber:1,
			fitColumns:true,
			height:200,
			queryParams:{
				page:1,
				shopId:shopId,
				isWarning:isWarning,
				typeSup:$('input[name="rbSup"]:checked').val()
			},
			width : width,
		    columns:[[  
		        {field:'toolCode',title:'Mã thiết bị',align:'left', width:110, sortable : false,resizable : false, formatter: function(v, r,i){
		        	if(r.lat!=null && r.lng!=null){
			        	var lat=r.lat!=null?r.lat:0;
			        	var lng=r.lng!=null?r.lng:0;
						return '<a class="Decoration" href="javascript:void(0);" onclick="SupperviseDevice.moveToTool(' +r.toolId+',' +lat+',' +lng+',' +i+');">' +Utils.XSSEncode(v)+'</a>';
		        	}else{
		        		return Utils.XSSEncode(v);
		        	}
				}},
		        {field:'customerCode',title:'Điểm lẻ',align:'left', width:$('#searchToolEasyUIDialog').width() - 230, sortable : false,resizable : false, formatter: function(v, r, i){
					return Utils.XSSEncode(r.customerCode + ' - ' +r.customerName+', ' +r.address);
				}},
		        {field :'toolId',hidden : true, formatter: function(v,r,i){
					return Utils.XSSEncode(v);
				}},
		    ]],
		    rowStyler: function(i,r){
				if(r.isWarning!=null && r.isWarning==1){
					return 'color:red';
				}
				return '';
			},
		    onLoadSuccess :function(){
	    		 $('.datagrid-header-rownumber').html('STT');	
	    		 updateRownumWidthForDataGrid('.easyui-dialog');
		    }
		});
		return false;
	},
	hideAllTab:function(){
		$('.Active').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
		$('#container3').hide();
	},
	loadTab1:function(){
		SupperviseDevice.hideAllTab();
		$('#tab1').addClass('Active');
		$('#container1').show();
		SupperviseDevice.loadChipGridTemp();
	},
	loadTab2:function(){
		SupperviseDevice.hideAllTab();
		$('#tab2').addClass('Active');
		$('#container2').show();
		SupperviseDevice.loadChipGridPosition();
	},
	loadTab3:function(){
		SupperviseDevice.hideAllTab();
		$('#tab3').addClass('Active');
		$('#container3').show();
	},
	loadChipGridTemp:function(){
		$('#chipGridTemp').datagrid({
			url : '/supervise/device/list-chip',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			pageSize:10,
			scrollbarSize : 0,
			singleSelect:true,
			pageNumber:1,
			fitColumns:true,
			queryParams:{
				page:1
			},
			width : ($('#searchChipTempEasyUIDialog').width() - 40),
		    columns:[[  
		        {field:'chipCode',title:'Mã chíp',align:'left', width:110,editor:{type:'numberbox'}, sortable : false,resizable : false, formatter: function(value, rowData, rowIndex){
					return Utils.XSSEncode(value);
				}},
				{field:'minTemp',title:'Nhiệt độ từ',align:'right', width:70, sortable : false,resizable : false, formatter: function(value, rowData, rowIndex){
					if(value=='0') return value;
					else return Utils.XSSEncode(value);
				}},
				{field:'maxTemp',title:'Nhiệt độ đến',align:'right', width:70, sortable : false,resizable : false, formatter: function(value, rowData, rowIndex){
					if(value=='0') return value;
					else return Utils.XSSEncode(value);
				}},
				{field:'edit',title:'',align:'center', width:30, sortable : false,resizable : false, formatter: function(v, r, i){
					var str=r.chipId + ",'" + r.chipCode +"'," +r.minTemp+"," +r.maxTemp;
					return "<a href='javascript:void(0)' onclick=\"return SupperviseDevice.editChipTemp(" +str+");\"><img src='/resources/images/icon-edit.png'/></a>";
				}}
		    ]],
		    onLoadSuccess :function(){
	    		 $('.datagrid-header-rownumber').html('STT');	
	    		 updateRownumWidthForDataGrid('.easyui-dialog');
		    }
		});
	},
	searchChipTemp:function(){
		var msg = '';
		$('#errMsgSearch').html('').hide();
		if(msg.length == 0 && $('#minTempSearch').val().trim()!='' && isNaN($('#minTempSearch').val())){
			msg = 'Nhiệt độ từ phải là số thực.';
		}
		if(msg.length == 0 && $('#maxTempSearch').val().trim()!='' && isNaN($('#maxTempSearch').val())){
			msg = 'Nhiệt độ đến phải là số thực.';
		}
		if(msg.length == 0 && $('#minTempSearch').val().trim()!='' && $('#maxTempSearch').val().trim()!='' 
			&& parseFloat($('#minTempSearch').val()) > parseFloat($('#maxTempSearch').val())){
			msg = 'Nhiệt độ từ phải nhỏ hơn nhiệt độ đến.';
		}
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();
			return false;
		}
		var chipCode=$('#chipCodeSearchTemp').val();
		var minTemp = $('#minTempSearch').val();
		var maxTemp = $('#maxTempSearch').val();
		$('#chipGridTemp').datagrid('load',{chipCode :chipCode, minTemp: minTemp,maxTemp:maxTemp});
	},
	editChipTemp:function(chipId,chipCode,minTemp,maxTemp){
		$('#searchChipTempEasyUIDialog').show();
		$('#searchChipTempEasyUIDialog').dialog({
	        title: 'Thông tin chíp',
	        closed: false,
	        cache: false,
	        modal: true,
	        onOpen: function(){
				$('#minTemp').focus();
				$('#chipIdTemp').val(chipId);
				$('#chipCodeTemp').val(chipCode);
				$('#minTemp').val(minTemp);
				$('#maxTemp').val(maxTemp);
	        },
	        onBeforeClose: function() {
	        	
	        },
	        onClose : function(){
	        	$('#chipIdTemp').val('');
	        	$('#chipCodeTemp').val('');
	    		$('#minTemp').val('');
	    		$('#maxTemp').val('');
	        }
	    });
	},
	updateChipTemp:function(){
		var msg = '';
		$('#errMsgPopup1').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('minTemp','Nhiệt độ từ');
		}
		if(msg.length == 0 && isNaN($('#minTemp').val())){
			msg = 'Nhiệt độ từ phải là số thực.';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('maxTemp','Nhiệt độ đến');
		}
		if(msg.length == 0 && isNaN($('#maxTemp').val())){
			msg = 'Nhiệt độ đến phải là số thực.';
		}
		if(msg.length == 0 && parseFloat($('#minTemp').val()) > parseFloat($('#maxTemp').val())){
			msg = 'Nhiệt độ từ phải nhỏ hơn nhiệt độ đến.';
		}
		if(msg.length > 0){
			$('#errMsgPopup1').html(msg).show();
			return false;
		}
		var chipId = $('#chipIdTemp').val();
		var minTemp = $('#minTemp').val().trim();
		var maxTemp = $('#maxTemp').val().trim();
		if(chipId==undefined || chipId==''){
			return false;
		}
		var dataModel = new Object();
		dataModel.chipId = chipId;
		dataModel.minTemp = minTemp;
		dataModel.maxTemp = maxTemp;
		Utils.addOrSaveData(dataModel, "/supervise/device/update-chip-temp", null, 'errMsgPopup1', function(data){
			$('#successMsgPopup1').html('Lưu thành công').show();
			SupperviseDevice.searchChipTemp();
			setTimeout(function(){
				$('#successMsgPopup1').html('').hide();
				$('#searchChipTempEasyUIDialog').dialog('close');
			},3000);
		});	
	},
	loadChipGridPosition:function(){
		$('#chipGridPosition').datagrid({
			url : '/supervise/device/list-chip-for-customer',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			pageSize:10,
			scrollbarSize : 0,
			singleSelect:true,
			pageNumber:1,
			fitColumns:true,
			queryParams:{
				page:1
			},
			width : ($('#searchChipPositionEasyUIDialog').width() - 40),
		    columns:[[  
		        {field:'chipCode',title:'Mã chíp',align:'left', width:70,editor:{type:'numberbox'}, sortable : false,resizable : false, formatter: function(value, rowData, rowIndex){
					return Utils.XSSEncode(value);
				}},
				{field:'customerCode',title:'Mã khách hàng',align:'left', width:70, sortable : false,resizable : false, formatter: function(value, rowData, rowIndex){
					return Utils.XSSEncode(value);
				}},
				{field:'customerName',title:'Tên khách hàng',align:'left', width:150, sortable : false,resizable : false, formatter: function(value, rowData, rowIndex){
					return Utils.XSSEncode(value);
				}},
				{field:'edit',title:'',align:'center', width:30, sortable : false,resizable : false, formatter: function(v, r, i){
					var str=r.chipId + ",'" + r.chipCode +"'";
					return "<a href='javascript:void(0)' onclick=\"return SupperviseDevice.editChipPosition(" +str+");\"><img src='/resources/images/icon-edit.png'/></a>";
				}}
		    ]],
		    onLoadSuccess :function(){
	    		 $('.datagrid-header-rownumber').html('STT');	
	    		 updateRownumWidthForDataGrid('.easyui-dialog');
		    }
		});
	},
	searchChipPosition:function(){
		var msg = '';
		var chipCode=$('#chipCodeSearchPosition').val();
		var customerCode = $('#customerCodeSearchPosition').val();
		var customerName = $('#customerNameSearchPosition').val();
		$('#chipGridPosition').datagrid('load',{chipCode :chipCode, customerCode: customerCode,customerName:customerName});
	},
	editChipPosition:function(chipId,chipCode){
		$('#searchChipPositionEasyUIDialog').show();
		$('#searchChipPositionEasyUIDialog').dialog({
	        title: 'Thông tin chíp',
	        closed: false,
	        cache: false,
	        modal: true,
	        onOpen: function(){
				$('#latPosition').focus();
				$('#chipIdPosition').val(chipId);
				$('#chipCodePosition').val(chipCode);
	        },
	        onBeforeClose: function() {
	        	
	        },
	        onClose : function(){
	        	$('#chipIdPosition').val('');
	        	$('#chipCodePosition').val('');
	    		$('#latPosition').val('');
	    		$('#lngPosition').val('');
	        }
	    });
	},
	updateChipPosition:function(){
		var msg = '';
		$('#errMsgPopup2').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('latPosition','Lat');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('latPosition','Lat');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('lngPosition','Lng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('lngPosition','Lng');
		}
		if(msg.length > 0){
			$('#errMsgPopup2').html(msg).show();
			return false;
		}
		var chipId = $('#chipIdPosition').val();
		var lat = $('#latPosition').val().trim();
		var lng = $('#lngPosition').val().trim();
		if(chipId==undefined || chipId==''){
			return false;
		}
		var dataModel = new Object();
		dataModel.chipId = chipId;
		dataModel.lat = lat;
		dataModel.lng = lng;
		Utils.addOrSaveData(dataModel, "/supervise/device/update-chip-position", null, 'errMsgPopup2', function(data){
			$('#successMsgPopup2').html('Lưu thành công').show();
			SupperviseDevice.searchChipPosition();
			setTimeout(function(){
				$('#successMsgPopup2').html('').hide();
				$('#searchChipPositionEasyUIDialog').dialog('close');
			},1000);
		});	
	},
	updateConfig:function(){
		var msg = '';
		$('#errMsg1').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tempConfig','Nhiệt độ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('tempConfig','Nhiệt độ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('positionConfig','Vị trí');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('positionConfig','Vị trí');
		}
		if(msg.length > 0){
			$('#errMsg1').html(msg).show();
			return false;
		}
		var tempConfig = $('#tempConfig').val().trim();
		var positionConfig = $('#positionConfig').val().trim();
		var dataModel = new Object();
		dataModel.tempConfig = tempConfig;
		dataModel.positionConfig = positionConfig;
		Utils.addOrSaveData(dataModel, "/supervise/device/update-config", null, 'errMsg1', function(data){
			$('#successMsg1').html('Lưu thành công').show();
			setTimeout(function(){
				$('#successMsg1').html('').hide();
			},1000);
		});	
	},
	updateDistance:function(){
		var msg = '';
		$('#errMsg2').html('').hide();
		msg = Utils.getMessageOfRequireCheck('distanceConfig','Khoảng cách');
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('distanceConfig','Khoảng cách');
		}
		if(msg.length > 0){
			$('#errMsg2').html(msg).show();
			return false;
		}
		var distanceConfig = parseInt($('#distanceConfig').val().trim());
		var dataModel = new Object();
		dataModel.distanceConfig = distanceConfig;
		Utils.addOrSaveData(dataModel, "/supervise/device/update-config", null, 'errMsg2', function(data){
			$('#successMsg2').html('Lưu thành công').show();
			setTimeout(function(){
				$('#successMsg2').html('').hide();
			},3000);
		});	
	},
	exportTemperature:function(){
		$('#errMsg').html('').hide();
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if(fDate == '__/__/____'){ $('#fromDate').val(''); }
		if(tDate == '__/__/____'){ $('#toDate').val(''); }
		var msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0 && !Utils.compareDate(fDate,tDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length ==0){
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var arrFromDate = fDate.split("/");
		var arrToDate = tDate.split("/");
		if(msg.length ==0){
			if(arrFromDate[1]!= arrToDate[1] && arrFromDate[2]!= arrToDate[2]){
				msg = 'Từ ngày và Đến ngày bắt buộc phải cùng tháng. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var shopId=$('#shopId').val();
		if(shopId.trim()==''){
			msg = 'Bạn chưa chọn đơn vị.';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = shopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		CommonSearch.exportExcelData(dataModel, '/supervise/device/bc-temperature/export', 'errMsg');
	},
	exportPosition:function(){
		$('#errMsg').html('').hide();
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if(fDate == '__/__/____'){ $('#fromDate').val(''); }
		if(tDate == '__/__/____'){ $('#toDate').val(''); }
		var msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0 && !Utils.compareDate(fDate,tDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length ==0){
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var arrFromDate = fDate.split("/");
		var arrToDate = tDate.split("/");
		if(msg.length ==0){
			if(arrFromDate[1]!= arrToDate[1] && arrFromDate[2]!= arrToDate[2]){
				msg = 'Từ ngày và Đến ngày bắt buộc phải cùng tháng. Vui lòng nhập lại.';
				$('#fromDate').focus();
			}
		}
		var shopId=$('#shopId').val();
		if(shopId.trim()==''){
			msg = 'Bạn chưa chọn đơn vị.';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = shopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		CommonSearch.exportExcelData(dataModel, '/supervise/device/bc-position/export', 'errMsg');
	}
};
var SupperviseSalesStaff  = {
	_colors:[
		'#445A20',
		'#8B6B1B',
		'#D15849',
		'#AF1DCD',
		'#D21A86',
		'#0303B7',
		'#943700',
		'#005A7E',
		'#E7AE00',
		'#00670B',
		'#1248C6',
		'#00740D',
		'#C400BA',
		'#9D617F',
		'#583E6A',
		'#659A8A'
		],
	_iColorCurrent:0,
	_mapColorStaff:new Map(),
	
	_lstStaffListPosition:new Array(),
	_lstStaffPositionViewer:new Map(),
	
	_isSearch:null,
	_notFitOverlay:null,
	customerViewer: new Array(),	
	
	selectedStaffId : null,
	mapObject : null,
	arrayMObject : new Map(),	
	arrayLstPointStaffViewer : new Map(),//1 staff co 1 cac cham vi tri ==> list list
	arrayLstPointCustomerViewer : new Map(),
	dateCurrent:'',
	
	getStaffName:function(r){
		var maxlength=31;
		if(r.attr.roleType==8) maxlength=37;
		else if(r.attr.roleType==6) maxlength=35;
		else if(r.attr.roleType==7) maxlength=33;
		else if(r.attr.roleType==5) maxlength=31;
		var span='<span title="' +r.attr.staffName+'">';
		if(r.attr.staffName==undefined || r.attr.staffName==null){
			r.attr.staffName='';
		}
		if(r.attr.staffName.length>maxlength){
			return span+r.attr.staffName.substring(0,maxlength-3)+'...</span>';
		}
		return span+Utils.XSSEncode(r.attr.staffName)+'</span>';
	},
	reloadTreeStaff:function(){
		$('#btCapNhatViTri').html('<img class="refreshIcon" src="/resources/images/loading-small.gif" height="15"/>');
		$('#btCapNhatViTri').attr('disabled','disabled');
		SupperviseSalesStaff._notFitOverlay=1;
		SupperviseSalesStaff.resetPathViewer();
	},
	loadTreeStaff:function(isSearch){
	
		$('#divOverlay').show();
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var dateNow = cDate+'/' +cMonth+'/' +cYear;
		
		var dateValue = $('#day').val().trim();
		SupperviseSalesStaff._lstStaffPositionViewer = new Map();
		SupperviseSalesStaff.resetPathViewer();
		if(ViettelMap._map!=null){
			ViettelMap._map.setZoom(ViettelMap._DEF_ALL_ZOOM);
			var pt = new viettel.LatLng(null, null);
			ViettelMap._map.setCenter(pt);
			if(ViettelMap._currentInfoWindow != null) {
				ViettelMap._currentInfoWindow.close();
			}
		}
			
		
		var date = '';
		if(dateValue != null && dateValue != ''){
			date = dateValue;
		}else{
			date = dateNow;
		}
		SupperviseSalesStaff.dateCurrent = date;
		
		if(isSearch!=undefined && isSearch!=null && isSearch==1){
			SupperviseSalesStaff._isSearch=1;
		}
	
		$('#treeGrid').treegrid({  
			url:  '/supervise/sales/staff/list-staff-for-shop-one-node',
			queryParams:{dateTime:date},
	        height:'auto',  
	        idField: 'id',  
	        treeField: 'text',
	        width:500,
	        //scrollbarSize:0,
	        lines:true,
	        animate:true,
	        fitColumns:true,
		    columns:[[  
		        {field:'text',title:'Nhân viên',width:250,formatter:function(v,r,i){       	
		        	var linkStaffName = SupperviseSalesStaff.getStaffName(r);
		        	var str = '';
		        	var isViewer = false;
		        	if(SupperviseSalesStaff._lstStaffPositionViewer != undefined && SupperviseSalesStaff._lstStaffPositionViewer != null && SupperviseSalesStaff._lstStaffPositionViewer.valArray.length > 0){
		        		isViewer = (SupperviseSalesStaff._lstStaffPositionViewer.get(r.attr.staffId)!=null?true:false);
		        	}
		        	
		        	if(r.attr.roleType==1 || r.attr.roleType==2 || r.attr.roleType==5){			        		
		        		if(r.attr.isHaveTraining != undefined && r.attr.isHaveTraining != null){			        			
		        			str='<input type="checkbox" parentId="' +r.attr.staffOwnerId+'" onclick="SupperviseSalesStaff.onClickCheckboxStaff(this, ' + r.attr.isHaveTraining + ');" value="' +Utils.XSSEncode(r.attr.staffId)+'" id="cb' +r.attr.staffId+'">';
		        			if(isViewer){			        				
		        				str='<input checked="checked" type="checkbox" parentId="' +r.attr.staffOwnerId+'" onclick="SupperviseSalesStaff.onClickCheckboxStaff(this, ' + r.attr.isHaveTraining + ');" value="' +Utils.XSSEncode(r.attr.staffId)+'" id="cb' +r.attr.staffId+'">';
		        			}
		        			linkStaffName = '<a href="#" style="color:#0000FF" onClick="SupperviseSalesStaff.onClickCheckboxStaff($(\'#cb' +r.attr.staffId+ '\'), ' + r.attr.isHaveTraining + ', 1);">' + linkStaffName + '</a>';
		        		}else{
		        			str='<input type="checkbox" parentId="' +r.attr.staffOwnerId+'" onclick="SupperviseSalesStaff.onClickCheckboxStaff(this);" value="' +Utils.XSSEncode(r.attr.staffId)+'" id="cb' +r.attr.staffId+'">';
		        			if(isViewer){
		        				str='<input checked="checked" type="checkbox" parentId="' +r.attr.staffOwnerId+'" onclick="SupperviseSalesStaff.onClickCheckboxStaff(this);" value="' +Utils.XSSEncode(r.attr.staffId)+'" id="cb' +r.attr.staffId+'">';
		        			}
		        			linkStaffName = '<a href="#" style="color:#0000FF" onClick="SupperviseSalesStaff.onClickCheckboxStaff($(\'#cb' +r.attr.staffId+ '\'), null, 1);">' + linkStaffName + '</a>';
		        		}
		        					        		
		        		if(r.attr.hhmm != undefined && r.attr.hhmm != null){
		        			if(r.attr.isHaveTraining != undefined && r.attr.isHaveTraining != null && r.attr.isHaveTraining ==1){
		        				return str + '<span style="color:#0000FF">' + linkStaffName + '</span>&nbsp;<span style="color:red">*</span>';
		        			}else{
		        				return str + '<span style="color:#0000FF">' + linkStaffName + '</span>';
		        			}
		        		}			        					        		
		        	}			        	
		        	if(r.attr.isHaveTraining != undefined && r.attr.isHaveTraining != null && r.attr.isHaveTraining ==1){
		        		return str+SupperviseSalesStaff.getStaffName(r) + '&nbsp;<span style="color:red">*</span>';
		        	}else{
		        		return str+SupperviseSalesStaff.getStaffName(r);
		        	}
		        	
		        }},
		        {field:'shopCode',title:'Đơn vị',width:70,align:'left',formatter:function(v,r,i){
	        		return Utils.XSSEncode(r.attr.shopCode);
	        	}},
		        {field:'update',title:'Màu',width:70,align:'center',formatter:function(v,r,i){
		        	var isViewer = false;
		        	if(SupperviseSalesStaff._lstStaffPositionViewer != undefined && SupperviseSalesStaff._lstStaffPositionViewer != null && SupperviseSalesStaff._lstStaffPositionViewer.valArray.length > 0){
		        		isViewer = (SupperviseSalesStaff._lstStaffPositionViewer.get(r.attr.staffId)!=null?true:false);
		        	}
		        	if(isViewer){
		        		var colorStaff = SupperviseSalesStaff._mapColorStaff.get(r.attr.staffId);
		        		return '<p id="pMau' +r.attr.staffId+'" style="border-style:solid;border-width:1px;border-color:' + colorStaff + ';"></p>';
		        	}
	        		if(r.attr.roleType!=null && (r.attr.roleType==1 || r.attr.roleType==2 || r.attr.roleType==5)){//GSNPP,NVBH
	        			var staffId = r.attr.staffId;
	        			var color = SupperviseSalesStaff._colors[SupperviseSalesStaff._iColorCurrent];
	        			SupperviseSalesStaff._iColorCurrent++;
	        			if(SupperviseSalesStaff._iColorCurrent == 10){
	        				SupperviseSalesStaff._iColorCurrent = 0;
	        			}
	        			SupperviseSalesStaff._mapColorStaff.put(staffId, color);			        			
	        			return '<p hidden id="pMau' +r.attr.staffId+'" style="border-style:solid;border-width:1px;border-color:' + color + ';"></p>';
	        		}
	        		return '';
	        	}},
		        {field:'abc',hidden:true}
		    ]],
		    rowStyler: function(r){
				if (r.attr.isBold){
					return 'background:none repeat scroll 0 0 #FBEC88';
				}
			},
			onCollapse:function(r){
				if(r.attr.roleType!=undefined && r.attr.roleType!=null && r.attr.roleType!=7 && r.attr.roleType!=4)
					$('[node-id=' +r.attr.staffId+']').next().remove();
			},
		    onBeforeLoad:function(n,p){
	    		if(p!=undefined && p!=null){
		    		$('.highlight').css('font-weight','normal');
			    	p.lhl= !$('#cbLHL').is(':disabled') && $('#cbLHL').is(':checked')?1:0;
			    	p.dateTime = date;
			    	p.staffId=p.id;
			    	if(($('#shopCode').val().trim()!='' || $('#shopName').val().trim()!='') && SupperviseSalesStaff._isSearch!=undefined && SupperviseSalesStaff._isSearch!=null && SupperviseSalesStaff._isSearch==1){
			    		p.shopCode=$('#shopCode').val().trim();
			    		p.shopName=$('#shopName').val().trim();
			    	}
			    	SupperviseSalesStaff._isSearch=null;
		    	}
		    },
		    onLoadSuccess:function() {
		    	$('#divOverlay').hide();	
		    }
		});
	},
	

	onClickCheckboxStaff:function(t, isHaveTraining, isClickStaffName){		
		if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		
		if(isClickStaffName != undefined && isClickStaffName != null && isClickStaffName ==1){
			if(!$(t).is(':checked')){	
				$(t).attr('checked', 'checked');
			}else{
				$(t).removeAttr('checked');
			}
		}
		
		var id=$(t).attr('value');		 
		if($(t).is(':checked')){			
			var now = new Date();
			var cYear = now.getFullYear();
			var cMonth = now.getMonth() + 1;
			var cDate = now.getDate();
			var dateNow = cDate+'/' +cMonth+'/' +cYear;
			var dateValue = $('#day').val();
			var date = '';
			if(dateValue != null && dateValue != ''){
				date = dateValue;
			}else{
				date = dateNow;
			}
			var staffListPosition = null;
			var parentStaffId=$(t).attr('parentId');
			$('#divOverlay').show();
			$.ajax({
				type : 'GET',			
				url : '/supervise/sales/staff/visitplan-lststafflistposition',			
				data : {dateTime:date, idStaffSelected:id, parentStaffId:parentStaffId},
				dataType : 'json',
				success : function(result) {				
					if(result.staffListPosition!=undefined && result.staffListPosition!=null
							&& result.staffListPosition.lstStaffPositionVO != null
							&& result.staffListPosition.lstStaffPositionVO.length > 0){
						$('#pMau' + result.staffListPosition.staffId).show();
						staffListPosition = result.staffListPosition;
						staffListPosition.isHaveTraining = isHaveTraining;
						SupperviseSalesStaff._lstStaffPositionViewer.put(id, staffListPosition);
						SupperviseSalesStaff.showStaffListPositionHasVisitPlanAll(); 
					}				
					$('#divOverlay').hide();
				}
			});				
		}else{
			SupperviseSalesStaff._lstStaffPositionViewer.remove(id);//Remove staff in viewer			
			$('#pMau' + id).hide();
			SupperviseSalesStaff.showStaffListPositionHasVisitPlanAll(); 
		}		
	},

	toggleListStaff:function(){
		if($('.StaffSelectBtmSection .OffStyle').length==1){
			$('#titleStaff').addClass('OnStyle');
			$('#titleStaff').removeClass('OffStyle');
			
		}else{
			$('#titleStaff').addClass('OffStyle');
			$('#titleStaff').removeClass('OnStyle');
		}
		$('#listStaff').toggle();
	},	

	showStaffListPositionHasVisitPlanAll:function(){ 		
		ViettelMap.clearOverlays();
		SupperviseSalesStaff.resetPathViewer();
		$('#divOverlay').show();				
		var viewers = SupperviseSalesStaff._lstStaffPositionViewer.valArray;
		for(var iStaffPaint = 0; iStaffPaint< viewers.length; iStaffPaint++){
			var staffListPosition = viewers[iStaffPaint]; 
			var lstStaffPosition = staffListPosition.lstStaffPositionVO;				
			for(var i=0,size=lstStaffPosition.length;i<size;++i){					
				var staffPosition = null;				
				var ordinalVisitReal = 1;
				for(var i=0; i<lstStaffPosition.length; i++){
					staffPosition = new Object();
					staffPosition = lstStaffPosition[i];
					staffPosition.image="Customers1Status";
					
					if(isNullOrEmpty(staffPosition.createTime)) {
						staffPosition.ordinalAndTimeVisitReal = ordinalVisitReal;
					}else{
						staffPosition.ordinalAndTimeVisitReal = ordinalVisitReal+'.' +toTimeString(new Date(staffPosition.createTime));
					}
					ordinalVisitReal++;							
				}				
			}			
		}
		if(ViettelMap._currentInfoWindow != null) {
			ViettelMap._currentInfoWindow.close();
		}
		ViettelMap.hideShowTitleMarker();
		$('.NoteCustomersStatus').show();
		ViettelMap.fitOverLay();
		$('#divOverlay').hide();		
		SupperviseSalesStaff.showPathViewer(); 
	
	},	
	
	resetPathViewer:function(){		
		if(SupperviseSalesStaff.arrayMObject != null && SupperviseSalesStaff.arrayMObject.valArray.length > 0){
			for(var i = 0; i< SupperviseSalesStaff.arrayMObject.valArray.length; i++){
				if(SupperviseSalesStaff.arrayMObject.valArray[i] != null){
					SupperviseSalesStaff.arrayMObject.valArray[i].setMap(null);
					SupperviseSalesStaff.arrayMObject.valArray[i] = null;
				}
			}	
		}
		if(SupperviseSalesStaff.arrayLstPointStaffViewer != null && SupperviseSalesStaff.arrayLstPointStaffViewer.valArray.length > 0){
			for(var i = 0; i< SupperviseSalesStaff.arrayLstPointStaffViewer.valArray.length; i++){
				for(var j=0;j<SupperviseSalesStaff.arrayLstPointStaffViewer.valArray[i].length;++j){
					var obj = SupperviseSalesStaff.arrayLstPointStaffViewer.valArray[i][j];
					obj.setMap(null);
				}
			}
		}
		if(SupperviseSalesStaff.arrayLstPointCustomerViewer != null && SupperviseSalesStaff.arrayLstPointCustomerViewer.valArray.length > 0){
			for(var i = 0; i< SupperviseSalesStaff.arrayLstPointCustomerViewer.valArray.length; i++){
				for(var j=0;j<SupperviseSalesStaff.arrayLstPointCustomerViewer.valArray[i].length;++j){
					var obj = SupperviseSalesStaff.arrayLstPointCustomerViewer.valArray[i][j];
					obj.setMap(null);
				}
			}
		}
	},
	showPathViewer : function(){		
		var viewers = SupperviseSalesStaff._lstStaffPositionViewer.valArray;
		for(var iStaffPaint = 0; iStaffPaint< viewers.length; iStaffPaint++){
			var viewer = viewers[iStaffPaint]; 
			var colorStaff = SupperviseSalesStaff._mapColorStaff.get(viewer.staffId);
			var lstLatLngsStaff = viewer.lstStaffPositionVO;	
			
			var points = new Array();			
			var listPointStaffViewerTemp = new Array();			
			var listPointCustomerViewerTemp = new Array();
			
			//Ve rieng marker khach hang
			var indexCus = 1;
			for(var i=0,size=lstLatLngsStaff.length;i<size;++i){				
				var p = lstLatLngsStaff[i];
				points.push(new viettel.LatLng(parseFloat(p.lat), parseFloat(p.lng)));
				if(p.cusLat != undefined && p.cusLat != null && p.cusLat != 0 
						&& p.cusLng != undefined && p.cusLng != null && p.cusLng != 0){
					var pt = new viettel.LatLng(p.cusLat, p.cusLng);
					
					var label = '';
					if(p.isOr != undefined && p.isOr != null && p.isOr == 1){
						label = '<span style="position: relative; left: 21px; top: -26px; color:yellow; font-weight:bold;">' +indexCus+'</span>';
						if(indexCus >9 ){
							label = '<span style="position: relative; left: 16px; top: -26px; color:yellow; font-weight:bold;">' +indexCus+'</span>';
						}
					}else{
						label =  '<span style="position: relative; left: 21px; top: -26px; color:white; font-weight:bold;">' +indexCus+'</span>';
						if(indexCus >9 ){
							label = '<span style="position: relative; left: 16px; top: -26px; color:white; font-weight:bold;">' +indexCus+'</span>';
						}
					}
						
					var colorHex = colorStaff.substring(1, 7);
				
					var urlImage = '/resources/images/icons-color/' + colorHex + '.png';									
					var marker = new viettel.LabelMarker({
						icon:{
							url : urlImage,
							size : {height : 32, width : 32},
							scaledSize : {height :32, width : 32}
						},
						position : pt,
						labelContent : label,
						map : ViettelMap._map,						
						draggable : false,
						labelAnchor : new viettel.Point(25, 0)
					});
					
					
					indexCus++;
					listPointCustomerViewerTemp.push(marker); 
					SupperviseSalesStaff.registerCircleEvent(marker, p);
					
					if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
						ViettelMap._listOverlay = new Array();
						ViettelMap._listOverlay.push(marker);
					} else {
						ViettelMap._listOverlay.push(marker);
					}
				}				
			}		
			SupperviseSalesStaff.arrayLstPointCustomerViewer.put(viewer.staffId, listPointCustomerViewerTemp);
			ViettelMap.fitOverLay();
			//End ve marker khach hang
			
			points = new Array();	
			for(var i=0,size=lstLatLngsStaff.length;i<size;++i){
				var p = lstLatLngsStaff[i];
				points.push(new viettel.LatLng(parseFloat(p.lat), parseFloat(p.lng)));
				var pt = new viettel.LatLng(p.lat, p.lng);
				var urlImage = '/resources/images/Mappin/1378843002_circle_red.png';
				var sizeImage = {height : 7, width : 7}; 
				if(i==0){
					urlImage = '/resources/images/Mappin/1378843009_circle_blue.png';
				}
				
				var markerContent = '<span id="' +i+'marker' +p.staffId+'" style="position: relative; left: 30px; top: -10px; color: ' + colorStaff + '" class="StaffPositionOridnalVisit"></span>';
				
				if(i == lstLatLngsStaff.length - 1){
					if(viewer.roleType == 5){						
						urlImage ='/resources/images/Mappin/green.png';
						if(viewer.isHaveTraining != undefined && viewer.isHaveTraining != null && viewer.isHaveTraining == 1){
							urlImage ='/resources/images/Mappin/green_star.png';
						} 
					}else{ 
						if(viewer.roleType ==1 || viewer.roleType ==2 ){
							urlImage ='/resources/images/Mappin/red.png';
							if(viewer.isHaveTraining != undefined && viewer.isHaveTraining != null && viewer.isHaveTraining == 1){
								urlImage ='/resources/images/Mappin/red_star.png';
							}
						} 
					}							
					
					sizeImage = {height : 39, width : 20};
					
					var level = ViettelMap._map.getZoom();
					var markerTop = '';
					var markerBotoom = '';
					if(level >= 13) {
						markerTop = "<div id='top"+p.staffId+"' class='Top' style='text-align:center;position: relative;top: -15px;'><strong style='color:red;font-size: 12px;' class='titleShop'>" + p.staffName + "</strong></div>";
					} else {
						markerTop = "<div id='top"+p.staffId+"' class='Top' style='display:none; text-align: center;position: relative;top: -15px;'><strong style='color:red;font-size: 12px;' class='titleShop'>" + p.staffName + "</strong></div>";
					}
					markerBotoom = '<span style="position: relative; left: 7px; top: 18px; color: ' + colorStaff + '" class="StaffPositionOridnalVisit">' +p.ordinalVisit+'</span>';
					markerContent = "<div id='"+i+"marker"+p.staffId+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -41px;'>"+markerTop+"</div>";
				}
				//marker Cham diem tren bang do, doc lap voi mObject
				var marker = new viettel.LabelMarker({
					icon:{
						url : urlImage,
						size : sizeImage,
						scaledSize : sizeImage
					},
					labelContent : markerContent,
					position : pt,
					map : ViettelMap._map,						
					draggable : false,
					labelAnchor : new viettel.Point(25, 0)
				});				
				listPointStaffViewerTemp.push(marker);
				
				p.idx = i;
				
				if(i == lstLatLngsStaff.length - 1){
					$('#' +i+'marker' +p.staffId).parent().prev().css('z-index', 10000000);
					$('#' +i+'marker' +p.staffId).parent().prev().bind('click', viewer, SupperviseSalesStaff.showDialogNVBHInfo);
				} else {
					$('#' +i+'marker' +p.staffId).parent().prev().css('z-index', 10000000);
					
					$('#' +i+'marker' +p.staffId).parent().prev().bind('click', p, function(e) {
						var point = e.data;
						var timeStr = point.createTime == null || point.createTime == undefined ? '' : toTimeString(new Date(point.createTime));
						SupperviseSalesStaff.showTimeTooltip(point.idx,point.staffId, point.lat, point.lng, timeStr);
					});
				}
				
				if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
					ViettelMap._listOverlay = new Array();
					ViettelMap._listOverlay.push(marker);
				} else {
					ViettelMap._listOverlay.push(marker);
				}
			}		
			SupperviseSalesStaff.arrayLstPointStaffViewer.put(viewer.staffId, listPointStaffViewerTemp);
			
			var mObject = null;
			//mObject Ve lines voi points chuyen vao
			mObject = new viettel.Polyline({
                path: points,
                strokeColor: colorStaff,
                strokeOpacity: 0.5,
                strokeWeight: 3,
                clickable: true,
                map: ViettelMap._map
            });
			
			SupperviseSalesStaff.arrayMObject.put(viewer.staffId, mObject);
			ViettelMap.fitOverLay();
		}
		ViettelMap.fitOverLay();
	},
	showTimeTooltip : function(i, staffId, lat, lng, timeStr) {
		var pt = new viettel.LatLng(lat, lng);
		if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		var infoWindow = new viettel.InfoWindow({
			content: "<div style='width: 100%;'>"+'Thời gian cập nhập ' +'<span style="font-weight: bold;">' +timeStr+'</span>' +"</div>", 
			position: pt
		});
		ViettelMap._currentInfoWindow = infoWindow;
		infoWindow.open(ViettelMap._map);
	},

	showDialogNVBHInfo: function(e) {
		var data = e.data;
		if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		var pt = new viettel.LatLng(data.lat, data.lng);
		var html = '<div class="MapPopupSection MapPopup2Section">';
		html += '<h2 class="Title2Style">' +Utils.XSSEncode(data.staffCode)+' - ' +Utils.XSSEncode(data.staffName)+'</h2>';
		html += '<h4 style="color: lightslategrey;">' +Utils.XSSEncode(data.shopCode)+' - ' +Utils.XSSEncode(data.shopName)+'</h4>';
		html += '<div class="MPContent"><dl class="Dl1Style">';
		html += '<dt>Vị trí cập nhật lúc:</dt>';
		html += '<dd>' +toTimeString(new Date(data.createTime))+'</dd>';
		html += '<dt>Độ chính xác:</dt>';
		html += '<dd>' +Utils.XSSEncode(data.accuracy)+' (m)<br /></dd>';
		html += '</dl>';
		
		var infoWindow = new viettel.InfoWindow({
			content: html,
			maxWidth: 280,
			position: pt
		});										
		ViettelMap._currentInfoWindow = infoWindow;
		infoWindow.open(ViettelMap._map);
	},
	registerCircleEvent:function(marker, p){
		 viettel.Events.addListener(marker, 'click', function(evt) {
				if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
					
			var pt = new viettel.LatLng(p.cusLat, p.cusLng);
			var html = '<div class="MapPopupSection MapPopup2Section">';
			html += '<h2 class="Title2Style">' +Utils.XSSEncode(p.customerCode)+' - ' +Utils.XSSEncode(p.customerName)+'</h2>';
			html += '<div class="MPContent"><dl class="Dl1Style">';
			html += '<dt>Địa chỉ:&nbsp;</dt>';
			html += '<dd>' +Utils.XSSEncode(p.address)+'<br /></dd>';
			html += '<dt>Di động:&nbsp;</dt>';
			html += '<dd>' +Utils.XSSEncode(p.mobilephone)+'<br /></dd>';
			html += '<dt>Cố định:&nbsp;</dt>';
			html += '<dd>' +Utils.XSSEncode(p.phone)+'<br /></dd>';
			html += '</dl>';
			
			var infoWindow = new viettel.InfoWindow({
				content: html,
				maxWidth: 250,
				position: pt
			});										
			ViettelMap._currentInfoWindow = infoWindow;
			infoWindow.open(ViettelMap._map);			 
         });
	}
};
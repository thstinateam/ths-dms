/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manageroute-create.js
 */
var SuperviseManageRouteCreate = {
	_xhrDel : null,
	_itv:null,
	_divMap:'',
	_xhrSave : null,
	_rCheckedMap: null,
	_mapCustomer : null,
	_mapCustomerCoppy : null,
	_mapCustomerMap : null,
	_mapCustomerName : null,
	_mapCustomerAddress: null,
	_mapCustomerStartDate: null,
	_mapCustomerNameEndate: null,
	_mapCustomerDialog :null,
	_mapCustomerHiddent: null,
	_mapCustomerDelete: null,
	_flagCheckAll: null,
	_ADD: 0,
	_UPDATE: 1,
	_DELETE: 2,
	_listRoute: null,
	_listMarker:null,
	_zoom:4,//18,
	_centerLat:0,
	_centerLng:0,
	_index : 0,
	_altRByRow : null,
	_mapCustomerStand: null,
	_isView: null,
	_shopIdIsView: null,
	getGridUrl : function(staffSaleCode, routingCode, routingName) {
		return "/superviseshop/manageroute-create/searchroute?staffSaleCode=" + encodeChar(staffSaleCode) + "&routingCode=" + encodeChar(routingCode) + "&routingName=" + encodeChar(routingName);
	},
	resize: function() {
	},
	loadTreeRoute: function() {
		var shopCode = $('#shopCodeCB').combobox('getValue');
		TreeUtils.loadRoutingTree('routingTree', null ,null, function(data) {
			window.location.href= '/superviseshop/manageroute-create/editroute?routeId=' + data + '&shopCode=' + $('#shopCode').val();
		}, function(node) {
			if (node.text == activeStatusText) {
				$('#status').val(activeType.RUNNING);
			} else {
				$('#status').val(activeType.STOPPED);
			}
			$('#status').change();
			SuperviseManageRouteCreate.searchRoute();
		}, shopCode);
	},
	/**
	 * Tim kiem danh sach Tuyen
	 * 
	 * @author hunglm16
	 * @since October 6,2014
	 * */
	searchRoute : function() {
		$('.ErrorMsgStyle').html("").change();
		$("#grid").datagrid("load", {
			shopCode: $('#shopCodeCB').combobox('getValue'),
			staffSaleCode: $('#staffCode').val().trim(),
			routingCode: $('#routingCode').val().trim(),
			routingName: $('#routingName').val().trim(),
			status: $('#status').val()
		});
		return false;
	},
	/**
	 * Xoa Tuyen
	 * 
	 * @author hunglm16
	 * @since October 13,2014
	 * */
	deleteRouting: function(routingId, routingCode, staffCode){
		$('.ErrorMsgStyle').html('').hide();
		var dataModel = new Object();
		dataModel.routeId = routingId;
		dataModel.shopCode = $('#shopCode').val();
		var msg = formatString(create_route_confirm_delete_route, routingCode);
		Utils.addOrSaveData(dataModel, "/superviseshop/manageroute-create/deleteroute", null, 'errMsg', function(data) {
			if (data.error != undefined && data.error !=null && !data.error) {
				$('#routingTree').tree('reload');
				SuperviseManageRouteCreate.searchRoute();
				$("#successMsg").html(msgCommon1).show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html("").hide();
				 }, 1500);
			} else if (data.errMsg != undefined && data.errMsg !=null && data.errMsg.length > 0) {
				$('#errMsg').html(data.errMsg).show();
			}
		}, null, null, null, msg);
		
		return false;		
	},
	getShopCodeByShopCodeAndName : function(input) {
		var value = $('#' + input).val().trim().split("-");
		return value[0];
	},
	searchCustomerDialog : function(){
		var params= SuperviseManageRouteCreate.getCustomerGridUrl();
		$("#gridCustomer").datagrid('load', params);
		return false;
	},
	getCustomerGridUrl : function( shortCode, customerName) {
		var shortCodeDialog = $('#shortCodeDialog').val().trim();
		var customerNameDialog = $('#customerNameDialog').val().trim();
		var fDate = $('#startDate').val().trim();
		var tDate = $('#endDate').val().trim();
		var msg = '';
		if(msg.length == 0 && fDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', jsp_common_from_date);
		}
		if(msg.length == 0 && tDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', jsp_common_to_date);
		}
		if(msg.length == 0 && tDate.length >0 && tDate.length >0 && !Utils.compareDate(fDate, tDate)){
			msg = msgCommonErr4;
			$('#fromDate').focus();
		}
		var params= new Object();
		params.shopCode = $('#shopCodeCB').combobox('getValue');
		params.shortCodeDialog=shortCodeDialog;
		params.customerNameDialog=customerNameDialog;
		params.pageNumber=1;
		params.startDateStr = fDate;
		params.endDateStr = tDate;
		params.routeId = $('#routingId').val().trim();
//		params.shopCode = $('#shopCode').val();
		params.status = $('#listStatus').val().trim();
		if(SuperviseManageRouteCreate._mapCustomer!=null && SuperviseManageRouteCreate._mapCustomer.valArray.length>0){
			var lstValue=SuperviseManageRouteCreate._mapCustomer.valArray;
			var strExceptionCus = lstValue[0][10];
			for(var i=1;i<lstValue.length;i++){
				strExceptionCus+=',' +lstValue[i][10];//customerId
			}
			params.strExceptionCus=strExceptionCus;
		}
		return params;
	},
	hideAllTab: function(){
		$('#tabActive1').removeClass('Active');
		$('#tabActive2').removeClass('Active');
		$('#tabActive3').removeClass('Active');
		$('#tabActive4').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
		$('#container3').hide();
		$('#container4').hide();
	},
	showTab1: function(){
		$('.ErrorMsgStyle').html('').hide();
		SuperviseManageRouteCreate.hideAllTab();
		$('#tabActive1').addClass('Active');
		$('#container1').show();
		$('#treeContainer').css('min-height',$('.ContentSection').height() - 125);
		$('#group_edit_rt_dt_btnAddRouting').css('bottom', $('#treeContainer').height() - $('.ContentSection').height() + 100);
	},
	showTab2: function(){
		$('.ErrorMsgStyle').html('').hide();
		SuperviseManageRouteCreate.hideAllTab();
		$('#errMsg').html('').hide();
		$('#tabActive2').addClass('Active');
		$('#container2').show();
		$('#routingContainerGrid').html('<table id="routingGrid"></table>').show().change();
		/*Chen grid*/
		var params = new Object();
		params.routingId = $('#routingId').val();
		params.saleDate = $('#listDateNew').val();
		params.shopCode = $('#shopCode').val();
		SuperviseManageRouteSetOrder._filterSaleDate = $('#listDateNew').val();
		SuperviseManageRouteCreate.convertSeq();
		$('#routingGrid').datagrid({
			url :'/superviseshop/manageroute-create/order/search',
	        queryParams:params,
	        rownumbers : true,
	        pageNumber : 1,
	        singleSelect :true,
	        scrollbarSize: 0,
	        autoWidth: true,
	        autoRowHeight : true,
	        fitColumns : true,
			width : ($('#routingContainerGrid').width()),
			columns:[[  
				{field:'shortCode',title:jsp_common_cust_code,width:100, align:'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field:'customerName',title:jsp_common_cust_name,width:250, align:'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field:'address',title:jsp_common_address,width:350,align:'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field:	'TT',title: create_route_tt,width:100,align:'center', formatter: function(value, row, index) {//SuperviseManageRouteSetOrder._seqTmp
			    	if(row[SuperviseManageRouteSetOrder._seqTmp] != null && row[SuperviseManageRouteSetOrder._seqTmp] != null && row[SuperviseManageRouteSetOrder._seqTmp]!=0) {
			    		return '<input type="text" id="seq_' +row.customerId+'" onkeypress="NextAndPrevTextField(event,this,\'InputTextStyleSetOrder\')" class="seq InputTextStyleSetOrder" value="' +row[SuperviseManageRouteSetOrder._seqTmp]+'" maxlength="5" ></input>';
			    	} else {
			    		return '<input type="text" id="seq_' +row.customerId+'" onkeypress="NextAndPrevTextField(event,this,\'InputTextStyleSetOrder\')" class="seq InputTextStyleSetOrder" value="" maxlength="5"></input>';
			    	}
			    }},
			]],
	        onLoadSuccess :function(data){
	            $('.InputTextStyleSetOrder').each(function(){
	            	$(this).numberbox({ 
	 	                min:1,
	 	                max: data.total,
	 	                precision:0
	 	            });  
	    		});
		    	$('#routingContainerGrid .datagrid-header-rownumber').html(jsp_common_numerical_order);
		    	updateRownumWidthForDataGrid('#routingContainerGrid');
		    	SuperviseManageRouteSetOrder.resize();
	    	}
		});
		
		SuperviseManageRouteSetOrder.resize();
	 	$('#treeContainer').css('min-height',$('.ContentSection').height() - 125);
		$('#group_edit_rt_dt_btnAddRouting').css('bottom', $('#treeContainer').height() - $('.ContentSection').height() + 100);
	},
	showTab3: function(){
		$('.ErrorMsgStyle').html('').hide();
		SuperviseManageRouteCreate.hideAllTab();
		$('#errMsg').html('').hide();
		$('#tabActive3').addClass('Active');
		$('#container3').show();
		SuperviseManageRouteAssign.resetStaff();
		/*Chen grid*/
		$('#staffContainerGrid').html('<table id="staffGrid"></table>').show().change();
		$('#saleStaffId').focus();
		$('#fromDate').val($('#sysdateServer').val().trim());
		$('#toDate').val('');
		$('#flagFromDate').val($('#sysdateServer').val().trim());
		$('#flagToDate').val("");
		var params = new Object();
		params.routingId = $('#routingId').val();
		$('#staffGrid').datagrid({
			url :'/superviseshop/manageroute-create/assign/search-staff',
			checkOnSelect : false,
			singleSelect :true,
	        pagination : true,
	        queryParams:params,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        autoRowHeight : true,
	        fitColumns : true,
			width : ($('#staffContainerGrid').width()),
			columns:[[  
	        	{field: 'staffCode',title:jsp_common_staff_code, width:150,align:'left',sortable : false,resizable : false ,formatter:function(value,rowData){
	        		if (rowData.staff != undefined && rowData.staff != null) { 
	        			return Utils.XSSEncode(rowData.staff.staffCode);
	        		} else {
	        			return '';
	        		}
	        	}},  
	        	{field: 'staffName',title:jsp_common_staff_name, width:300,align:'left',sortable : false,resizable : false,formatter:function(value,rowData){
	        		if (rowData.staff != undefined && rowData.staff != null) { 
	        			return Utils.XSSEncode(rowData.staff.staffName);
	        		} else {
	        			return '';
	        		}
				}},  
	        	{field: 'fromDate',title:jsp_common_from_date, width:120,align:'left',sortable : false,resizable : false, formatter:function(value) {
	        		if(value != null && value != '') return $.datepicker.formatDate('dd/mm/yy', new Date(value));
	        		else return '';
	        		} 
	    		},
	    		{field: 'toDate',title:jsp_common_to_date, width:120,align:'left',sortable : false,resizable : false, formatter:function(value) {
	    			if(value != null && value != '') return $.datepicker.formatDate('dd/mm/yy', new Date(value));
	        		else return '';
	        		} 
	    		},
	        	{field: 'edit', width:70, align:'center', formatter : ManageRouteAssign.editStaffInRouting},
	        ]],
	        onLoadSuccess :function(){
		    	 $('#staffContainerGrid .datagrid-header-rownumber').html(jsp_common_numerical_order);
		    	 updateRownumWidthForDataGrid('#staffContainerGrid');
		    	 SuperviseManageRouteAssign.resize();
		    	//Phan quyen control
		 		var arrEdit =  $('#staffContainerGrid td[field="edit"]');
		 		if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
		 		  for (var i = 0, size = arrEdit.length; i < size; i++) {
		 		  	$(arrEdit[i]).prop("id", "group_edit_rt_dt_tab_nv_gr_td_edit_" + i);//Khai bao id danh cho phan quyen
		 			$(arrEdit[i]).addClass("cmsiscontrol");
		 		  }
		 		}
		 		Utils.functionAccessFillControl('staffContainerGrid', function(data){
		 			//Xu ly cac su kien lien quan den cac control phan quyen
		 			var arrTmpLength =  $('#staffContainerGrid [id^="group_edit_rt_dt_tab_nv_gr_td_edit_"]').length;
		 			var invisibleLenght = $('.isCMSInvisible[id^="group_edit_rt_dt_tab_nv_gr_td_edit_"]').length;
		 			if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#staffGrid').datagrid("showColumn", "edit");
					} else {
						$('#staffGrid').datagrid("hideColumn", "edit");
					}
		 		});
	    	}
		});
		
		$('#treeContainer').css('min-height',$('.ContentSection').height() - 125);
		$('#group_edit_rt_dt_btnAddRouting').css('bottom', $('#treeContainer').height() - $('.ContentSection').height() + 100);
	},
	
	showTab4: function(){
		$('.ErrorMsgStyle').html('').hide();
		SuperviseManageRouteCreate.hideAllTab();
		$('#errMsg').html('').hide();
		$('#tabActive4').addClass('Active');
		$('#container4').show();
		$('#divDate').html(SuperviseManageRouteCreate._divMap).show().change();
		SuperviseManageRouteSetOrder.openMapCustomerRouting();
	},
	
	changeShopSelectBox: function(){
		SuperviseManageRouteCreate._mapCustomer.clear();
		SuperviseManageRouteCreate._mapCustomerName.clear();
		$('#errMsg').html('').hide();
		$('#lstRoutingCustomerDetail').html('');
	},
	
	openAddCustomerDialog :function(isEdit) {
		$('#showDialogId').show();
		$('#errMsg').html('').hide();
		$('#idDivErrorInsert').html('').change();
		$('#errMsg2').html('').hide();
		$('#addCustomerDialog').dialog({
			title : create_route_add_cust_route,
			closed : false,
			onOpen : function() {
				SuperviseManageRouteCreate._mapCustomerDialog = new Map();
				$('#shortCodeDialog').focus();
				$("#gridCustomer").datagrid({
					url:'/superviseshop/manageroute-create/searchcustomer',
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					pageList: [10],
					scrollbarSize:0,
					queryParams:SuperviseManageRouteCreate.getCustomerGridUrl(),
					columns:[[	
					 	{field:'shortCode', title: jsp_common_cust_code, width: 80, sortable:false, resizable:false , align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {field:'customerName', title: jsp_common_cust_name, width: 151, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {field:'address', title: jsp_common_address,width: 300 ,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {field:'id', checkbox:true, align:'center', width: 60 , sortable:false,resizable:false}
					]],
					onCheck:function(i,r){
						var obj = SuperviseManageRouteCreate._mapCustomerDialog.get(r.shortCode);
						if(obj == null){
							var customerDialogObj = new Object();
							customerDialogObj.customerId = r.id;
							customerDialogObj.customerCode = r.customerCode;
							customerDialogObj.customerName =  r.customerName;
							customerDialogObj.address = r.address;
							customerDialogObj.lat = r.lat;
							customerDialogObj.lng = r.lng;
							SuperviseManageRouteCreate._mapCustomerDialog.put(r.shortCode,customerDialogObj);
						}
				    },
				    onUncheck:function(i,r){
				    	var obj = SuperviseManageRouteCreate._mapCustomerDialog.get(r.shortCode);
						if(obj != null) {
							SuperviseManageRouteCreate._mapCustomerDialog.remove(r.shortCode);
						}
				    },
				    onCheckAll:function(r){
				    	for(i=0;i<r.length;i++){
				    		var obj = SuperviseManageRouteCreate._mapCustomerDialog.get(r[i].shortCode);
							if(obj == null){
								var customerDialogObj = new Object();
								customerDialogObj.customerId = r[i].id;
								customerDialogObj.customerCode = r[i].customerCode;
								customerDialogObj.customerName =  r[i].customerName;
								customerDialogObj.address = r[i].address;
								customerDialogObj.lat = r[i].lat;
								customerDialogObj.lng = r[i].lng;
								SuperviseManageRouteCreate._mapCustomerDialog.put(r[i].shortCode,customerDialogObj);
							}
				    	}
				    	$('#checkAll').removeAttr('checked');
				    },
				    onUncheckAll:function(r){
				    	for(i=0;i<r.length;i++){
				    		var obj = SuperviseManageRouteCreate._mapCustomerDialog.get(r[i].shortCode);
							if(obj != null) {
								SuperviseManageRouteCreate._mapCustomerDialog.remove(r[i].shortCode);
							}
				    	}
				    },
					method : 'POST',
					onLoadSuccess: function(data){
						$('td[field=id] div.datagrid-header-check input').removeAttr('checked');
						var isAll = true;
						for(var i = 0; i < data.rows.length; i++) {
							if(SuperviseManageRouteCreate._mapCustomerDialog.get(data.rows[i].shortCode) != null) {
								$('#gridCustomer').datagrid('selectRow', i);
							} else {
								isAll = false;
							}
						}
						if(isAll) {
							$('td[field=id] div.datagrid-header-check input').attr('checked', 'checked');
						}
						$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
						$(window).resize();
						$('#shortCodeDialog').bind('keypress',Utils.unitCharactersAz094Code);
						$('#customerNameDialog').bind('keypress',Utils.unitCharactersAz094Name);
						$('#shortCodeDialog').bind('paste',function(e) {
							var tm = setTimeout(function() {
								var msg = Utils.getMessageOfSpecialCharactersValidate('shortCodeDialog','',Utils._CODE);
								if (msg.length > 0) {
									$('#seachStyle1Code').val('');
								}
								clearTimeout(tm);
							},50);
						});
						$('#customerNameDialog').bind('paste',function(e) {
							var tm = setTimeout(function() {
								var msg = Utils.getMessageOfSpecialCharactersValidate('customerNameDialog','',Utils._NAME);
								if (msg.length > 0) {
										$('#seachStyle1Name').val('');
									}
									clearTimeout(tm);
								},50);
							});
						$(window).resize();
					}
				});
			},
			onClose:function(){
				$('#shortCodeDialog').val('');
				$('#customerNameDialog').val('');
				$('#showDialogId').hide();
			}
		});
		return false;
	},
	changeCheckBoxCustomerDialog : function(isCheck,shortCode,customerId,customerCode,customerName,address,lat,lng) {
		var shortCode = shortCode;
		if(isCheck) {
			var obj = SuperviseManageRouteCreate._mapCustomerDialog.get(shortCode);
			if(obj == null){
				var customerDialogObj = new Object();
				customerDialogObj.customerId = customerId;
				customerDialogObj.customerCode = customerCode;
				customerDialogObj.customerName =  customerName;
				customerDialogObj.address = address;
				customerDialogObj.lat = lat;
				customerDialogObj.lng = lng;
				SuperviseManageRouteCreate._mapCustomerDialog.put(shortCode,customerDialogObj);
			}
		} else {
			var obj = SuperviseManageRouteCreate._mapCustomerDialog.get(shortCode);
			if(obj != null) {
				SuperviseManageRouteCreate._mapCustomerDialog.remove(shortCode);
			}
		}
		return false;
	},
	addCustomerDialog : function() {
		if(SuperviseManageRouteCreate._mapCustomerDialog.size()<= 0){
			$('#errMsg2').html(create_route_chua_chon_kh).show();
				setTimeout(function(){$('#errMsg2').html('').hide();},3000);
			return false;
		}
		var customerMapKey = SuperviseManageRouteCreate._mapCustomerDialog.keyArray;
		var customerMapValue = SuperviseManageRouteCreate._mapCustomerDialog.valArray;
		var result='-1';
		for( var i = 0; i < customerMapKey.length; i++ ) {
			result = ";" +  customerMapValue[i].customerId;
	    }
		$('.ErrorMsgStyle').html("").show().change();
		result = result.replace(/-1;/g, '');
		$('#shortCode').val("").show().change();
		$('#customerName').val("").show().change();
		$('#address').val("").show().change();
		$('#startDate').val("").show().change();
		$('#endDate').val("").show().change();
		var fDate = $('#startDate').val().trim();
		var tDate = $('#endDate').val().trim();
		var param = {};
		param.routeId = $('#routingId').val().trim();
		param.shortCode = $('#shortCode').val().trim();
		param.customerName = $('#customerName').val().trim();
		param.address = $('#address').val().trim();
		param.startDateStr = fDate;
		param.endDateStr = tDate;
		param.lstIdStr = result;
		
		Utils.getHtmlDataByAjax(param,'/superviseshop/manageroute-create/editroute-Insert-Customer', function(data){
			$('#container1').html(data);
			var tm = setTimeout(function() {
				$('#successMsg').html(msgCommon1);
			}, 700);
			SuperviseManageRouteCreate.showDefaulCustomerRowtingSearch();
			$('#addCustomerDialog').dialog('close');
		});
	},
	
	addCustomerDialogInsert : function() {
		$('#lstRoutingCustomerDetail').html('');
		SuperviseManageRouteCreate._index = 0;//Bien Index
		var mapCustomerSort = new Map();//Tao Map tam
		//lay danh sach khach hang vua chon
		var mapCustomerDialogKey = SuperviseManageRouteCreate._mapCustomerDialog.keyArray;
		var mapCustomerDialogValue = SuperviseManageRouteCreate._mapCustomerDialog.valArray;
		//Them danh sach khach hang hien dang co và trong Map() tam
		for(var i = 0; i< SuperviseManageRouteCreate._mapCustomer.keyArray.length; i++){
			mapCustomerSort.put(Utils.XSSEncode(SuperviseManageRouteCreate._mapCustomer.valArray[i][11]) + SuperviseManageRouteCreate._index.toString(), SuperviseManageRouteCreate._mapCustomer.valArray[i]);
			SuperviseManageRouteCreate._index = SuperviseManageRouteCreate._index + 1;
		}
		//Them danh sach khach hang vua chon vao Map() khach hang tam
		for( var i = 0; i < mapCustomerDialogKey.length; i++ ) {
			var shortCode =  mapCustomerDialogKey[i];
			var customerDialogObj = mapCustomerDialogValue[i];
			var customerName = Utils.XSSEncode(customerDialogObj.customerName);
			var customerId =  customerDialogObj.customerId;
			var obj = new Array();
			obj.push(Utils.XSSEncode(customerName));//0 - ten KH
			obj.push(0);//1 - Thu 2
			obj.push(0);//2 - Thu 3
			obj.push(0);//3 - Thu 4
			obj.push(0);//4 - Thu 5
			obj.push(0);//5 - Thu 6
			obj.push(0);//6 - Thu 7
			obj.push(0);//7 - Chu nhat
			obj.push(1);//8 - Tan suat
			obj.push(1);//9 - startWeek
			obj.push(customerDialogObj.customerId);//10 - Id KH
			obj.push( Utils.XSSEncode(shortCode));//11 - Short Code KH
			if(customerDialogObj.address!=undefined && customerDialogObj.address!=null && customerDialogObj.address.length>0 && customerDialogObj.address.trim().toLowerCase()!='null'){
				obj.push(customerDialogObj.address);//12 - dia Chi KH; vuongmq; 16/12/2015 (dau ' XSS -> &#39; loi trien khai) luc view len HTML da XSS
			}else{
				obj.push('');//12
			}
			obj.push(customerDialogObj.lat);//13
			obj.push(customerDialogObj.lng);//14
			obj.push("");//15 - id Routng Customer
			obj.push(0);//16 - checker
			obj.push($('#sysdateServer').val().trim());//17 - tu Ngay
			obj.push('');//18 - den Ngay
			obj.push(SuperviseManageRouteCreate._ADD);//19 - Tinh trang (Them, sua)
			obj.push(1);//20 - tinh trang Default cho tu Ngay, den Ngay
			obj.push(1);
			obj.push("");//Tinh trang coppy
			obj.push(1);//23 - W1
			obj.push(1);//24 - W1
			obj.push(1);//25 - W1
			obj.push(1);//26 - W1
			obj.push(0);//27 tansuat
			mapCustomerSort.put(Utils.XSSEncode(shortCode) + SuperviseManageRouteCreate._index.toString(), obj);
			SuperviseManageRouteCreate._mapCustomerName.put(customerId , Utils.XSSEncode(customerName));
			SuperviseManageRouteCreate._index = SuperviseManageRouteCreate._index + 1;
	    }
		//Dua danh sach khach hang hien dang co ve rong
		SuperviseManageRouteCreate._mapCustomer.clear();
		SuperviseManageRouteCreate._mapCustomer = new Map();
		//Sap xep khach hang trong mang tam
		var mapCustomerKeyNew = new Array(); 
		for(var i=0; i< mapCustomerSort.keyArray.length; i++){
			mapCustomerKeyNew.push(mapCustomerSort.keyArray[i]);
		}
		mapCustomerKeyNew = sortArray.quickSort(mapCustomerKeyNew);//Thuat toan sap xep
		//Gan lai danh sach vao Map() khach hang ban dau va Show() HTML
		for(var i=0; i< mapCustomerKeyNew.length; i++){
			var obj = mapCustomerSort.get(mapCustomerKeyNew[i]);
			if(obj !=undefined && obj!=null){
				SuperviseManageRouteCreate._mapCustomer.put(mapCustomerKeyNew[i], obj);
				var html = SuperviseManageRouteCreate.getHTMLCustomerRouting(mapCustomerKeyNew[i], obj);
				$('#lstRoutingCustomerDetail').append(html);			
			}
		}
		mapCustomerSort = new Map();//Dua danh sach khach hang tam ve rong
		SuperviseManageRouteCreate._mapCustomerDialog = new Map();
		//Xu ly defaul cho danh sach moi
		SuperviseManageRouteCreate.showOrderAndScrollBody();
		SuperviseManageRouteCreate.clearSearchBoxCustomer();
		SuperviseManageRouteCreate.showDefaulCustomerRowtingSearchInsert();
		$('#addCustomerDialog').dialog('close');
		return false;
	},
	clearSearchBoxCustomer :function(){
		$('#shortCode').val('');
		$('#customerName').val('');
		setSelectBoxValue('listStatus',-1);
	},
	initEditRoutingPage :function(){
		SuperviseManageRouteCreate._index = 0;
		var params = {};
		params.routeId = $('#routingId').val();
		Utils.getJSONDataByAjax(params,'/superviseshop/manageroute-create/getListCustomerByRoutingInShop', function(data){
			if(data.lstData!=undefined && data.lstData!=null && data.lstData.length>0){
				for(var i=0; i< data.lstData.length; i++){
					var obj = new Array();
					obj.push(data.lstData[i].customerName);//0 - ten KH
					obj.push(data.lstData[i].monday);//1
					obj.push(data.lstData[i].tuesday);//2
					obj.push(data.lstData[i].wednesday);//3
					obj.push(data.lstData[i].thursday);//4
					obj.push(data.lstData[i].friday);//5
					obj.push(data.lstData[i].saturday);//6 
					obj.push(data.lstData[i].sunday);//7
					obj.push(data.lstData[i].weekInterval);//8
					obj.push(data.lstData[i].startWeek);//9
					obj.push(data.lstData[i].customerId);//10
					obj.push(data.lstData[i].shortCode);//11
					obj.push(data.lstData[i].address);//12
					obj.push(data.lstData[i].lat);//13
					obj.push(data.lstData[i].lng);//14
					obj.push(data.lstData[i].routingCustomerId);//15
					obj.push(data.lstData[i].statusRowEvent);//16
					obj.push(data.lstData[i].startDate);//17
					obj.push(data.lstData[i].endDate);//18
					obj.push(SuperviseManageRouteCreate._UPDATE);//19
					if(isDel!=undefined && isDel!=null){
						obj.push(data.lstData[i].isDel);//20 isDel
					}else{
						obj.push(1);
					}
					obj.push(data.lstData[i].status);//21 tinh trang roucus
					
					SuperviseManageRouteCreate._mapCustomer.put(data.lstData[i].shortCode + SuperviseManageRouteCreate._index, obj);
					SuperviseManageRouteCreate._mapCustomerStand.put(data.lstData[i].shortCode + index, jQuery.extend(true, [], obj));
					//SuperviseManageRouteCreate._mapCustomerName.put(data.lstData[i].customerId , data.lstData[i].customerName);
					
					SuperviseManageRouteCreate._index = SuperviseManageRouteCreate._index + 1;				
				}
			}
		});
		
	},
	pushMapData : function(shortCode,customerName,monday,tuesday,wednesday,thursday,friday,saturday,sunday, weekInterval ,
			startWeek,routingCustomerId, customerId, customerCode,address,lat,lng, statusRowEvent, startDate, endDate, isDel, index, status){
		var obj = new Array();
		obj.push(customerName);//0 - ten KH
		obj.push(monday);//1
		obj.push(tuesday);//2
		obj.push(wednesday);//3
		obj.push(thursday);//4
		obj.push(friday);//5
		obj.push(saturday);//6 
		obj.push(sunday);//7
		obj.push(weekInterval);//8
		obj.push(startWeek);//9
		obj.push(customerId);//10
		obj.push(shortCode);//11
		obj.push(address);//12
		obj.push(lat);//13
		obj.push(lng);//14
		obj.push(routingCustomerId);//15
		obj.push(statusRowEvent);//16
		obj.push(startDate);//17
		obj.push(endDate);//18
		obj.push(SuperviseManageRouteCreate._UPDATE);//19
		if(isDel!=undefined && isDel!=null){
			obj.push(isDel);//20 isDel
		}else{
			obj.push(1);
		}
		obj.push(status);//21 tinh trang roucus
		SuperviseManageRouteCreate._mapCustomer.put(shortCode + index, obj);
		SuperviseManageRouteCreate._mapCustomerStand.put(shortCode + index, jQuery.extend(true, [], obj));
		SuperviseManageRouteCreate._mapCustomerName.put(customerId ,customerName);
	},
	showOrderAndScrollBody : function(){
		var runner = 0;
		$('.OrderNumber').each(function(){
			runner+=1;
			$(this).html(runner).show();
		});
		Utils.bindFormatOnTextfieldInputCss('WeekInterval', Utils._TF_NUMBER);
		Utils.bindFormatOnTextfieldInputCss('StartWeek', Utils._TF_NUMBER_CONVFACT);
		Utils.bindFormatCalenderOnTextfieldInputCss('StartWeek');
		$('#lstRoutingCustomerScroll').jScrollPane();
		$('#lstRoutingCustomerTableScroll').jScrollPane();
	},
	
	showDefaulCustomerRowtingSearch : function(){
		var arrfDate = $('input[name= "fromDateGrid"]');
		var i=0;
		for(i =0; i < arrfDate.length; i++){
			applyDateTimePicker('#' +arrfDate[i].id);
		}
		var arrtDate = $('input[name= "toDateGrid"]');
		for(i =0; i < arrtDate.length; i++){
			applyDateTimePicker('#' +arrtDate[i].id);
		}
		var arrisDel = $('input[name= "isDel"]');
		for(i =0; i < arrisDel.length; i++){
			if(arrisDel[i].value!=undefined && arrisDel[i].value!=null){
				if(parseInt(arrisDel[i].value.trim()) > 1){
	    			disableDateTimePicker('fDate_' +arrisDel[i].id.replace(/isDel_/g,''));
	    			disableDateTimePicker('tDate_' +arrisDel[i].id.replace(/isDel_/g,''));
				}else if(parseInt(arrisDel[i].value.trim()) < 1){
				    disableDateTimePicker('fDate_' +arrisDel[i].id.replace(/isDel_/g,''));
				}
			}
		}
		
		SuperviseManageRouteCreate._mapCustomer = new Map();
		SuperviseManageRouteCreate._mapCustomerStand = new Map();
		SuperviseManageRouteCreate._mapCustomerName = new Map();
		SuperviseManageRouteCreate.initEditRoutingPage();
	},
	
	showDefaulCustomerRowtingSearchInsert : function(){
		//Show cac gia tri mac dinh
		var arrfDate = $('input[name= "fromDateGrid"]');
		var i=0;
		for(i =0; i < arrfDate.length; i++){
			applyDateTimePicker('#' +arrfDate[i].id);
			var fDate = $('#' +arrfDate[i].id).val();
			if(fDate!=undefined && fDate!=null && fDate.length >0 && Utils.compareDate1(fDate.trim(), $('#sysdateServer').val().trim())){
				disableDateTimePicker(arrfDate[i].id);
			}
		}
		var arrtDate = $('input[name= "toDateGrid"]');
		for(i =0; i < arrtDate.length; i++){
			applyDateTimePicker('#' +arrtDate[i].id);
			var tDate = $('#' +arrtDate[i].id).val();
			if(tDate!=undefined && tDate!=null && tDate.trim().length >0 && Utils.compareDate1(tDate.trim(), $('#sysdateServer').val().trim())){
				var row = SuperviseManageRouteCreate._mapCustomer.get(arrtDate[i].id.substring(6).trim());
				if(row[22] == undefined || row[22] == null || row[22].length < 4){
					disableDateTimePicker(arrtDate[i].id);										
				}
			}
		}
		//Phan quyen control
		var arrEdit =  $('#gridCustomerContainerDiv [field="edit"]');
		if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
		  for (var i = 0, size = arrEdit.length; i < size; i++) {
		  	$(arrEdit[i]).prop("id", "group_edit_rt_dt_tab_kh_gr_td_edit_" + i);//Khai bao id danh cho phan quyen
			$(arrEdit[i]).addClass("cmsiscontrol");
		  }
		}
		Utils.functionAccessFillControl('gridCustomerContainerDiv', function(data){
			//Xu ly cac su kien lien quan den cac control phan quyen
//			var arrTmpLength =  $('#gridCustomerContainerDiv [id^="group_edit_rt_dt_tab_kh_gr_td_edit_"]').length;
//			var invisibleLenght = $('.isCMSInvisible[id^="group_edit_rt_dt_tab_kh_gr_td_edit_"]').length;
//			if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
//				
//			} else {
//				
//			}
		});
	},
	/**
	 * Tim kiem danh sach khach hang (routingcustomer)
	 * 
	 * @author hunglm16
	 * @since October 6,2014
	 * */
	searchCustomerRouting : function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var fDate = $('#startDate').val().trim();
		var tDate = $('#endDate').val().trim();
		if(msg.length == 0 && fDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', jsp_common_from_date);
		}
		if(msg.length == 0 && tDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', jsp_common_to_date);
		}
		if(msg.length == 0 && tDate.length >0 && tDate.length >0 && !Utils.compareDate(fDate, tDate)){
				msg = msgCommonErr4;
				$('#fromDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsgSearchRoucus').html(msg).show();
			return false;
		}
		var param = {};
		param.routeId = $('#routingId').val().trim();
		param.shortCode = $('#shortCode').val().trim();
		param.customerName = $('#customerName').val().trim();
		param.address = $('#address').val().trim();
		param.startDateStr = fDate;
		param.endDateStr = tDate;
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			param.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		param.status = $('#listStatus').val().trim();
		param.shopCode = $('#shopCode').val();
		Utils.getJSONDataByAjax(param,'/superviseshop/manageroute-create/editroute-search-Customer', function(data){
			SuperviseManageRouteCreate._mapCustomerName = new Map();
			SuperviseManageRouteCreate._mapCustomer = new Map();
			SuperviseManageRouteCreate._mapCustomerStand = new Map();
			SuperviseManageRouteCreate._mapCustomerMap = new Map();
			SuperviseManageRouteCreate._mapCustomerCoppy = new Map();
			SuperviseManageRouteCreate._altRByRow = new Array();
			
			$('#lstRoutingCustomerDetail').html('');
			$('#idDivErrorInsert').html('').change();
			
			SuperviseManageRouteCreate._index = 0;
			
			if (data.lstData != undefined && data.lstData != null && data.lstData.length > 0) {
				for(var i=0; i< data.lstData.length; i++){
					var obj = new Array();
					obj.push(data.lstData[i].customerName);//0 - ten KH
					obj.push(data.lstData[i].monday);//1
					obj.push(data.lstData[i].tuesday);//2
					obj.push(data.lstData[i].wednesday);//3
					obj.push(data.lstData[i].thursday);//4
					obj.push(data.lstData[i].friday);//5
					obj.push(data.lstData[i].saturday);//6 
					obj.push(data.lstData[i].sunday);//7
					obj.push(data.lstData[i].weekInterval);//8
					obj.push(data.lstData[i].startWeek);//9
					obj.push(data.lstData[i].customerId);//10
					obj.push(Utils.XSSEncode(data.lstData[i].shortCode));//11
					obj.push(data.lstData[i].address);//12
					obj.push(data.lstData[i].lat);//13
					obj.push(data.lstData[i].lng);//14
					obj.push(data.lstData[i].routingCustomerId);//15
					obj.push(data.lstData[i].statusRowEvent);//16
					obj.push(data.lstData[i].startDateStr);//17
					obj.push(data.lstData[i].endDateStr);//18
					obj.push(SuperviseManageRouteCreate._UPDATE);//19
					if(data.lstData[i].isDel!=undefined && data.lstData[i].isDel!=null){
						obj.push(data.lstData[i].isDel);//20 isDel
					}else{
						obj.push(1);
					}
					obj.push(data.lstData[i].status);//21 tinh trang roucus
					obj.push("");//22
					obj.push(data.lstData[i].week1);//23
					obj.push(data.lstData[i].week2);//24
					obj.push(data.lstData[i].week3);//25
					obj.push(data.lstData[i].week4);//26
					obj.push(data.lstData[i].tansuat != undefined && data.lstData[i].tansuat != null ? data.lstData[i].tansuat : '');//27 tan suat
					
					SuperviseManageRouteCreate._mapCustomer.put(data.lstData[i].shortCode + SuperviseManageRouteCreate._index, obj);
					SuperviseManageRouteCreate._mapCustomerStand.put(data.lstData[i].shortCode + SuperviseManageRouteCreate._index, jQuery.extend(true, [], obj));
					//Tao HTML
					var html = SuperviseManageRouteCreate.getHTMLCustomerRouting(data.lstData[i].shortCode + SuperviseManageRouteCreate._index, obj);
					
					$('#lstRoutingCustomerDetail').append(html);
					
					SuperviseManageRouteCreate._index = SuperviseManageRouteCreate._index + 1;			
				}	
			}
			$('#lstRoutingCustomerDetail').change().show();
			SuperviseManageRouteCreate.showOrderAndScrollBody();
			SuperviseManageRouteCreate.showDefaulCustomerRowtingSearchInsert();
		});
	},
	
	searchCustomerRoutingShortCode : function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var fDate = $('#startDate').val().trim();
		var tDate = $('#endDate').val().trim();
		if(msg.length == 0 && fDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', jsp_common_from_date);
		}
		if(msg.length == 0 && tDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', jsp_common_to_date);
		}
		if(msg.length == 0 && tDate.length >0 && tDate.length >0 && !Utils.compareDate(fDate, tDate)){
				msg = msgCommonErr4;
				$('#fromDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsgSearchRoucus').html(msg).show();
			return false;
		}
		var shortCode =  $('#shortCode').val().trim();
		var customerName =  $('#customerName').val().trim();
		var address =  $('#address').val().trim();
		var startDate =  $('#startDate').val().trim();
		var endDate =  $('#endDate').val().trim();
		var customerMapKey = SuperviseManageRouteCreate._mapCustomer.keyArray;
		var customerMapValue = SuperviseManageRouteCreate._mapCustomer.valArray;
		var arrayShortCode = new Array();
		if(customerMapKey == undefined || customerMapKey == null || customerMapKey.length == 0){
			//truong hop chua them du lieu
			$('#lstRoutingCustomerDetail').html('<tr><td colspan="15" class="NotData" style="height: 25px; text-align: center;">' + create_route_find_no_resul + '</td></tr>');
		}else{
			var flag = true;
			var arr = [];
			//truong hop da co du lieu
			for(var i in customerMapKey){
				arr = customerMapValue[i];
				flag = true;
				if(flag && shortCode.length > 0){ 
					if(arr[11] == undefined || arr[11]==null || arr[11].length == 0  || arr[11].toLowerCase().indexOf(shortCode.toLowerCase())<0){
						flag = false;
					}
				}
				
				if(flag && customerName.length > 0){ 
					if(arr[0] == undefined || arr[0]==null || arr[0].length == 0  || arr[0].toLowerCase().indexOf(customerName.toLowerCase())<0){
						flag = false;
					}
				}
				
				if(flag && address.length > 0){ 
					if(arr[12] == undefined || arr[12]==null || arr[12].length == 0  || arr[12].toLowerCase().indexOf(address.toLowerCase())<0){
						flag = false;
					}
				}
				
				if(flag && !Utils.communicationBetweenTwoTime(startDate, endDate, arr[17], arr[18])){
					flag = false;
				}
				
				
				if(flag){
					if(arr[19] != SuperviseManageRouteCreate._DELETE){
						arrayShortCode.push(customerMapKey[i]);
					}
				}
			}
			//Update giao dien
			if(arrayShortCode.length > 0 ){
				$('#lstRoutingCustomerDetail').html('');
				for( var i = 0; i < arrayShortCode.length; i++ ) {
					var obj =SuperviseManageRouteCreate._mapCustomer.get(arrayShortCode[i]);
					if( obj != null){
						var html = SuperviseManageRouteCreate.getHTMLCustomerRouting(arrayShortCode[i], obj);
						$('#lstRoutingCustomerDetail').append(html);
					}
			    }
			} else{
				$('#lstRoutingCustomerDetail').html('<tr><td colspan="15" class="NotData" style="height: 25px; text-align: center;">' + create_route_find_no_resul + '</td></tr>');
			}
		}
		
		SuperviseManageRouteCreate.showOrderAndScrollBody();
		SuperviseManageRouteCreate.showDefaulCustomerRowtingSearchInsert();
		return false;
	},
	
	
	
	getHTMLCustomerRouting :function(shortCode, obj){
		var html = new Array();
		SuperviseManageRouteCreate._altRByRow.push('#imgDel_' + Utils.XSSEncode(shortCode));
		if(obj[19]==0) {
			html.push('<tr id="tr_' + Utils.XSSEncode(shortCode)+'" style="color: #00CBFF">');
		} else{
			html.push('<tr id="tr_' + Utils.XSSEncode(shortCode)+'">');
		}
		html.push('<td style="text-align: center;" class="ColsTd1 AlignCCols OrderNumber"></td>'); //stt
		html.push('<td class="AlignLCols ShortCode">' + obj[11] +'</td>');  //Ma KH
		html.push('<td class="AlignLCols"><div class="Wordwrap">' + Utils.XSSEncode(obj[0]) +'</div></td>');  //TenKH
		html.push('<td class="AlignLCols"><div class="Wordwrap">' + Utils.XSSEncode(obj[12]) +'</div></td>');  //DiaChi
		html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="status_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' + Utils.XSSEncode(shortCode) + '\',1,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[21])+'/></td>');
		if(obj[20]!=undefined && obj[20]!=null && obj[20]!=1){
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled"  id="monday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode) + '\',1,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[1])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="tuesday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode) + '\',2,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[2])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="wednesday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode) + '\',3,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[3])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="thursday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode) + '\',4,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[4])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="friday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',5,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[5])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="saturday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',6,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[6])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="sunday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',7,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[7])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="w1_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',23,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[23])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="w2_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',24,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[24])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="w3_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',25,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[25])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" disabled="disabled" id="w4_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',26,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[26])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><span id="tansuat_' +  Utils.XSSEncode(shortCode) + '">' + obj[27] +'</span></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input style="width:75px;" type="text" class="InputTextStyle InputText1Style WeekInterval" maxlength="10" id="fDate_' + Utils.XSSEncode(shortCode)+'"  value="' +obj[17]+'" name = "fromDateGrid" onchange="SuperviseManageRouteCreate.changeByDateShortCode(\'' +Utils.XSSEncode(shortCode)+'\',this, 1)"/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input style="width:75px;" type="text" class="InputTextStyle InputText1Style WeekInterval" maxlength="10" id="tDate_' + Utils.XSSEncode(shortCode)+'"  value="' +obj[18]+'" name = "toDateGrid" onchange="SuperviseManageRouteCreate.changeByDateShortCode(\'' +Utils.XSSEncode(shortCode)+'\',this, 2)"/></td>');
			if(obj[20]!=0){
				html.push('<td class="EndCols AlignCCols" field="edit"></td>');
			}else{
				html.push('<td class="EndCols AlignCCols" id="td_shortCode" field="edit"><a id="imgDel_' + Utils.XSSEncode(shortCode)+'" title="' + jsp_common_sao_chep + '" href="javascript:void(0)" onclick="SuperviseManageRouteCreate.coppyCustomerRouting(\'' +Utils.XSSEncode(shortCode)+'\')"><img width="16" height="16" src="/resources/images/icon-coppy.png"/></a></td>');
			}
		}else{
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="monday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode) + '\',1,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[1])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="tuesday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode) + '\',2,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[2])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="wednesday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode) + '\',3,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[3])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="thursday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode) + '\',4,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[4])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="friday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',5,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[5])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="saturday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',6,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[6])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="sunday_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',7,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[7])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="w1_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',23,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[23])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="w2_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',24,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[24])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="w3_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',25,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[25])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input type="checkbox" id="w4_' +obj[10]+'" onchange="SuperviseManageRouteCreate.changeCheckBoxShortCode(\'' +Utils.XSSEncode(shortCode)+ '\',26,this.checked)"' + SuperviseManageRouteCreate.getCheck(obj[26])+'/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><span id="tansuat_' +  Utils.XSSEncode(shortCode) + '">' + obj[27] +'</span></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input style="width:75px;" type="text" class="InputTextStyle InputText1Style WeekInterval" maxlength="10" id="fDate_' + Utils.XSSEncode(shortCode)+'"  value="' +obj[17]+'" name = "fromDateGrid" onchange="SuperviseManageRouteCreate.changeByDateShortCode(\'' +Utils.XSSEncode(shortCode)+'\',this, 1)"/></td>');
			html.push('<td style="padding: 8px 5px; text-align: center;"><input style="width:75px;" type="text" class="InputTextStyle InputText1Style WeekInterval" maxlength="10" id="tDate_' + Utils.XSSEncode(shortCode)+'"  value="' +obj[18]+'" name = "toDateGrid" onchange="SuperviseManageRouteCreate.changeByDateShortCode(\'' +Utils.XSSEncode(shortCode)+'\',this, 2)"/></td>');
			html.push('<td class="EndCols AlignCCols" id="td_shortCode" field="edit"><a id="imgDel_' + Utils.XSSEncode(shortCode)+'" title="' + jsp_common_xoa + '" href="javascript:void(0)" onclick="SuperviseManageRouteCreate.deleteCustomerRouting(\'' +Utils.XSSEncode(shortCode)+'\',\'' +obj[15]+'\',\'' +obj[11]+'\')"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
		}
		html.push('</tr>');
		return html.join("");
	},
	getCheck : function(value){
		if(value ==1) return 'checked="checked"';
		else return '';
	},
	changeCheckBox : function(customerId, field, ischeck){
		var obj = SuperviseManageRouteCreate._mapCustomer.get(customerId);
		var i = Number(field);
		if(ischeck) {
			obj[field] = 1;
		} else {
			obj[field] = 0;
		}
	},
	changeCheckBoxShortCode : function(shortCode, field, ischeck){
		var obj = SuperviseManageRouteCreate._mapCustomer.get(shortCode);
		if (obj[20] != undefined && obj[20] != null && obj[20] == 1) {
			var i = Number(field);
			if(ischeck) {
				obj[field] = 1;
			} else {
				obj[field] = 0;
			}
		}
		SuperviseManageRouteCreate.calcTansuat(obj, shortCode);
		
	},
	calcTansuat:function(obj, shortCode){
		var week = 0;
		for (var i = 23 ; i <= 26 ; i++) {
			if (obj[i] == 1) {
				week++;
			}
		}
		var day = 0;
		for (var i = 1 ; i <= 7 ; i++) {
			if (obj[i] == 1) {
				day++;
			}
		}
		obj[27] = week * day;
		$('#tansuat_' + shortCode).html(Utils.XSSEncode(obj[27]));
	},	
	changeCheckBoxShortCodeW : function(shortCode, field, ischeck){
		var obj = SuperviseManageRouteCreate._mapCustomer.get(shortCode);
		var i = Number(field);
		if(ischeck) {
			obj[field] = 1;
		} else {
			var kq = 0;
			for ( var j = 23; j <= 26; j++) {
				if(obj[j] == undefined || obj[j] == null || obj[j]==0){
					kq ++;
				}
			}
			if(kq<4){
				$.messager.alert(jsp_common_thong_bao,create_route_ckgt_bat_buoc,null,function(){
					return false;
				});
			}
			obj[field] = 0;
		}
	},
	changeweekInterval : function(routingCustomerId, input){
		var obj = SuperviseManageRouteCreate._mapCustomer.get(routingCustomerId);
		var value = $(input).val();
		if(isNaN(value)){
			$.messager.alert(jsp_common_thong_bao,create_route_tan_suat_is_not_number,null,function(){
				obj[8] = '';
				$(input).val('');
				$(input).focus();
			});
		} else if((Number(value)<=0 ||Number(value) > 54)){
			$.messager.alert(jsp_common_thong_bao,create_route_tan_suat_0_54,null,function(){
				obj[8] = '';
				$(input).val('');
				$(input).focus();
			});
			return false;
		} else{
			obj[8] = value;
		}
	},
	
	changeweekIntervalShortCode : function(shortCode, input){
		var obj = SuperviseManageRouteCreate._mapCustomer.get(shortCode);
		var value = $(input).val();
		if(isNaN(value)){
			$.messager.alert(jsp_common_thong_bao,create_route_tan_suat_is_not_number,null,function(){
				obj[8] = '';
				$(input).val('');
				$(input).focus();
			});
		} else if((Number(value)<=0 ||Number(value) > 99)){
			$.messager.alert(jsp_common_thong_bao,create_route_tan_suat_0_100,null,function(){
				obj[8] = '';
				$(input).val('');
				$(input).focus();
			});
			return false;
		} else{
			obj[8] = value;
		}
	},
	
	changeByDate : function(routingCustomerId, input, flag){
		var obj = SuperviseManageRouteCreate._mapCustomer.get(routingCustomerId);
		var value = $(input).val();
		var fDate = $('#fDate_' +routingCustomerId).val().trim();
		var tDate = $('#tDate_' +routingCustomerId).val().trim();
		var fDateHiddent = $('#fDateHiddent_' +routingCustomerId).val().trim();
		var tDateHiddent = $('#tDateHiddent_' +routingCustomerId).val().trim();
		if(flag == 1){
			if(fDate!=null && fDate.length>0 && tDate!=null & tDate.length>0 && !Utils.compareDate(fDate, tDate)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr4,null,function(){
					obj[17] = '';
					$(input).val(fDateHiddent);
					$(input).focus();
				});
			}else if(fDate!=null && fDate.length>0 && Utils.compareCurrentDate(fDate)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr5,null,function(){
					obj[17] = '';
					$(input).val(fDateHiddent);
					$(input).focus();
				});
			}else if(fDate == null || fDate.length == 0){
				$.messager.alert(jsp_common_thong_bao,create_route_from_date_null,null,function(){
					obj[17] = '';
					$(input).val(fDateHiddent);
					$(input).focus();
				});
			}else{
				obj[17] = value;
			}
		} else {
			if(fDate!=null && fDate.length>0 && tDate!=null & tDate.length>0 && !Utils.compareDate1(fDate, tDate)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr4,null,function(){
					obj[18] = '';
					$(input).val(tDateHiddent);
					$(input).focus();
				});
			}else if(tDate!=null && fDate.length>0 && Utils.compareCurrentDate(tDate)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr4,null,function(){
					obj[18] = '';
					$(input).val(tDateHiddent);
					$(input).focus();
				});
			}else if(fDate==null || fDate.length==0){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr8,null,function(){
					obj[18] = '';
					$(input).val(tDateHiddent);
					$(input).focus();
				});
			}else{
				obj[18] = value;
			}
		}
	},
	
	changeByDateShortCode : function(shortCode, input, flag){
		var obj = SuperviseManageRouteCreate._mapCustomer.get(shortCode);
		var value = $(input).val();
		var sysdate = $('#sysdateServer').val().trim();
		var yesterdate = $('#yesterdateServer').val().trim();
		var fDate = $('#fDate_' +shortCode).val().trim();
		var tDate = $('#tDate_' +shortCode).val().trim();
		if(flag == 1){
			if(fDate!=null && fDate.length>0 && tDate!=null & tDate.length>0 && !Utils.compareDate(fDate, tDate)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr4,null,function(){
					$(input).val(obj[17]);
					$(input).focus();
				});
			}else if(fDate!=null && fDate.length>0 && !Utils.compareDate(sysdate, fDate)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr5,null,function(){
					$(input).val(obj[17]);
					$(input).focus();
				});
			}else if(fDate == null || fDate.length == 0){
				$.messager.alert(jsp_common_thong_bao,create_route_from_date_null,null,function(){
					$(input).val(obj[17]);
					$(input).focus();
				});
			}else{
				obj[17] = value;
			}
		} else {
			if(fDate!=null && fDate.length>0 && tDate!=null & tDate.length>0 && !Utils.compareDate(fDate, tDate)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr4,null,function(){
					$(input).val(obj[18]);
					$(input).focus();
				});
			}else {
				if(obj[22]!=undefined && obj[22]!=null && obj[22].trim().toLowerCase() === "coppy" && tDate!=null && tDate.length>0 && Utils.compareDate(tDate, yesterdate)){
					$.messager.alert(jsp_common_thong_bao,msgCommonErr11,null,function(){
						$(input).val(obj[18]);
						$(input).focus();
					});
				}else if(obj[22].trim().toLowerCase() != "coppy" && tDate!=null && tDate.length>0 && !Utils.compareDate(sysdate, tDate)){
					$.messager.alert(jsp_common_thong_bao,msgCommonErr6,null,function(){
						$(input).val(obj[18]);
						$(input).focus();
					});
				}else{
					obj[18] = value;
				}
			}
		}
	},
	
	changestartWeek : function(routingCustomerId, input){
		var obj = SuperviseManageRouteCreate._mapCustomer.get(routingCustomerId);
		var numWeek = getNumWeekOfYear($(input).val());
		if((Number(numWeek)<=0 ||Number(numWeek) >= 54)) {
			$.messager.alert(jsp_common_thong_bao,create_route_tuan_0_54,null,function(){
				obj[10] = '';
				$(input).val('');
				$(input).focus();
			});
			return false;
		}
		obj[10] = numWeek;
	},
	
	deleteCustomerRouting : function(shortCode, customerRoutingId, shortCopdeName){
		var msg = format(create_route_confirm_delete_cust, shortCopdeName);
		$.messager.confirm(jsp_common_xacnhan, msg, function(r){
			if (r){
				var delArray = SuperviseManageRouteCreate._mapCustomer.get(shortCode);
				//var isUpdate = $('#flagEdit').val().trim();
				//if(parseInt(delArray[20])!=0){
				if(delArray[22]==undefined || delArray[22] == null || delArray[22] !== "newcoppy"){
					if(SuperviseManageRouteCreate._mapCustomerDelete==null || SuperviseManageRouteCreate._mapCustomerDelete.length ==0){
						SuperviseManageRouteCreate._mapCustomerDelete = new Array();
					}
					if(delArray!=undefined && delArray!=null && delArray.length >0){
						if(delArray[15]==customerRoutingId){
							SuperviseManageRouteCreate._mapCustomerDelete.push(delArray[15]);
						}
					}
				}
				SuperviseManageRouteCreate._mapCustomerName.remove(shortCode);
				SuperviseManageRouteCreate._mapCustomer.remove(shortCode);
				$('#tr_' + shortCode).replaceWith('');
				SuperviseManageRouteCreate.showOrderAndScrollBody();
			}
		});	
	},
	deleteCustomerRoutingNew : function(shortCode, customerRoutingId){
		$('#errMsg').html('').hide();
		$('#idDivErrorInsert').html('').change();
		var msg = format(create_route_confirm_delete_cust, shortCode);
		$.messager.confirm(jsp_common_xacnhan, msg, function(r){
			if (r){
				//Xoa tuyen
				var fDate = $('#startDate').val().trim();
				var tDate = $('#endDate').val().trim();
				var param = {};
				param.routeId = $('#routingId').val().trim();
				param.shortCode = $('#shortCode').val().trim();
				param.customerName = $('#customerName').val().trim();
				param.address = $('#address').val().trim();
				param.fDateStr = fDate;
				param.tDateStr = tDate;
				param.routingCustomerId = customerRoutingId;
				if (SuperviseManageRouteCreate._shopIdIsView != null) {
					param.shopId = SuperviseManageRouteCreate._shopIdIsView;
				}
				Utils.getHtmlDataByAjax(param,'/superviseshop/manageroute-create/editroute-Delelet-Customer', function(data){
					$('#container1').html(data);
					SuperviseManageRouteCreate.showDefaulCustomerRowtingSearch();
				});
			}
		});	
	},
	loadTree: function(tree){
		if(tree.length <=0){
			return false;
		} else {
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			var html = '';
			for (var i= 0 ; i< tree.length; i++) {
				html += '<li><a href="/superviseshop/manageroute-create/editroute?routeId='
					 + tree[i].routingId+'"><span>' + tree[i].routingCode+'</span></a></li>';
			};
			$('#treeContainer').html(html);
			$('#treeContainer').jScrollPane();
		}
		return false;
	},
	checkChangeMap:function(key){
		var a=SuperviseManageRouteCreate._mapCustomerStand.get(key);
		var b=SuperviseManageRouteCreate._mapCustomer.get(key);
		if(a==null || b==null) return true;
		if(a[1]!=b[1]) return true;
		if(a[2]!=b[2]) return true;
		if(a[3]!=b[3]) return true;
		if(a[4]!=b[4]) return true;
		if(a[5]!=b[5]) return true;
		if(a[6]!=b[6]) return true;
		if(a[7]!=b[7]) return true;
		if(a[8]!=b[8]) return true;
		if(a[9]!=b[9]) return true;
		if(a[16]!=b[16]) return true;
		if(a[17]!=b[17]) return true;
		if(a[18]!=b[18]) return true;
		if(a[23]!=b[23]) return true;
		if(a[24]!=b[24]) return true;
		if(a[25]!=b[25]) return true;
		if(a[26]!=b[26]) return true;
		return false;
	},
	addOrSaveRouting :function (isEdits, nextPage){
		var flagEdit = $('#flagEdit').val();
		var isEdit = true;
		if(flagEdit!= undefined && flagEdit!=null && parseInt(flagEdit.trim())!=1){
			isEdit = false;
		}
		$('.ErrorMsgStyle').html('').hide();
		$('#idDivErrorInsert').html('').change();
		var msg = '';
		
		if(!isEdit) {
			if(msg.length ==0){
				msg = Utils.getMessageOfRequireCheck('routingCode',jsp_common_route_code);
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfSpecialCharactersValidate('routingCode',jsp_common_route_code, Utils._CODE);
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfRequireCheck('routingName',jsp_common_route_name);
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfSpecialCharactersValidate('routingName',jsp_common_route_name, Utils._NAME);
			}
		} else{
			if(msg.length ==0){
				msg = Utils.getMessageOfRequireCheck('routingName',jsp_common_route_name);
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfSpecialCharactersValidate('routingName',jsp_common_route_name, Utils._NAME);
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$('#group_edit_rt_dt_btnAOSRouting').focus();
			return false;
		}
		
		var lstRoutingCustomerAdd = new Array();
		var lstRoutingCustomerEdit = new Array();
		
		var customerMapKey = SuperviseManageRouteCreate._mapCustomer.keyArray;
		var customerMapValue = SuperviseManageRouteCreate._mapCustomer.valArray;
		var result1='';
		var result2='';
		var tDateTemp = '';
		var result='';
		//Validate dong giao nhau
		if(customerMapKey.length > 1) {
			var flag = -1;
			for( var i = 0; i < customerMapKey.length; i++ ) {
				for( var j = i + 1; j < customerMapKey.length; j++ ) {
					flag = -1;
					if(customerMapValue[i][17]==null || customerMapValue[i][17].trim().length ==0){
						flag = -2;
						break;
					}
					if(customerMapValue[i][11].trim() === customerMapValue[j][11]){
						//Neu f1 < f2 thi t1 phai nho hon f2
						if(Utils.compareDate1(customerMapValue[i][17], customerMapValue[j][17])){
							//Neu t1 is null or t1 >= t2 thì Error
							if(customerMapValue[i][18]==undefined || customerMapValue[i][18]==null || customerMapValue[i][18].length == 0 || !Utils.compareDate1(customerMapValue[i][18], customerMapValue[j][17])){
								flag = j;
								break;
							}
						}else if(Utils.compareDate1(customerMapValue[j][17], customerMapValue[i][17])){//neu f2< f1 thi t2 < f1
							//Neu t2 is null or t2 >= t1 thì Error
							if(customerMapValue[j][18]==undefined || customerMapValue[j][18]==null || customerMapValue[j][18].length == 0 || !Utils.compareDate1(customerMapValue[j][18], customerMapValue[i][17])){
								flag = j;
								break;
							}
						}else{
							flag = j;
							break;
						}
					}
				}
				if(flag>-1){
					$('#errMsg').html(formatString(create_route_time_line_and_line, [(i+1),(j+1)])).show();
					$('#group_edit_rt_dt_btnAOSRouting').focus();
					return false;
				}else if(flag < -1){
					$('#errMsg').html(formatString(create_route_line_is_not_require, i)).show();
					$('#group_edit_rt_dt_btnAOSRouting').focus();
				}
			}
		}
		
		for( var i = 0; i < customerMapKey.length; i++ ) {
			var obj = customerMapValue[i];
			var obj = customerMapValue[i];
			
			var kq = false;
			for ( var j = 23; j <= 26; j++) {
				if(obj[j] != undefined && obj[j] != null && obj[j]==1){
					kq = true;
					break;
				}
			}
			if(!kq){
				$('#errMsg').html(formatString(create_route_cust_not_ckgt, (i + 1))).show();
				$('#group_edit_rt_dt_btnAOSRouting').focus();
				return false;
			}
			
			if(obj[20] != undefined && obj[20] != null && (obj[20] < 2 || (obj[20] == 2 && obj[22]!=undefined && obj[22]!=null && obj[22].length >3))){
				result =  customerMapKey[i];
				for(var j = 1 ; j< obj.length ; ++j){
						if(obj[j]==undefined || obj[j] == null || obj[j].toString().trim().length == 0 || obj[j].toString().trim() === 'null'){
							result += ';K';
						}else{
							result += ';' + obj[j];
						}
				}
				if(!isEdit) {
					lstRoutingCustomerAdd.push(result);
				}else{
					if(obj[19]==0){
						lstRoutingCustomerAdd.push(result);
					}else if(SuperviseManageRouteCreate.checkChangeMap(customerMapKey[i])){
						lstRoutingCustomerEdit.push(result);
					}
				}
				result1 += "----" + result;
			}
	    }
		if (!isEdit) {
			if (lstRoutingCustomerAdd.length == 0) {
				$('#errMsg').html(create_route_have_not_cust_add).show();
				$('#group_edit_rt_dt_btnAOSRouting').focus();
				return false;
			}
		} else {
			if(lstRoutingCustomerEdit.length == 0 && lstRoutingCustomerAdd.length == 0){
				if(SuperviseManageRouteCreate._mapCustomerDelete == null || SuperviseManageRouteCreate._mapCustomerDelete.length == 0){
					$('#errMsg').html(create_route_no_route_is_change).show();
					$('#group_edit_rt_dt_btnAOSRouting').focus();
					return false;
				}
			}
		}
		var lstDeleteStr = "-1";
		if(SuperviseManageRouteCreate._mapCustomerDelete!=null && SuperviseManageRouteCreate._mapCustomerDelete.length>0){
			for(var i=0; i<SuperviseManageRouteCreate._mapCustomerDelete.length; i++){
				lstDeleteStr += ";" + SuperviseManageRouteCreate._mapCustomerDelete[i].toString();
			}
		}
		lstDeleteStr = lstDeleteStr.replace(/-1;/g,""); 
		lstDeleteStr = lstDeleteStr.replace(/-1/g,""); 
		
		var msg = "";
		var params = new Object();
		if (SuperviseManageRouteCreate._shopIdIsView != undefined && SuperviseManageRouteCreate._shopIdIsView != null) {
			params.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		if(!isEdit) {
			params.routingCode = $('#routingCode').val().trim();
			params.routingName = $('#routingName').val().trim();
			params.lstRoutingCustomerAdd = lstRoutingCustomerAdd;
			msg = create_route_confirm_create_route;
		} else{
			params.routeId = $('#routingId').val().trim();
			params.routingName = $('#routingName').val().trim();
			params.lstRoutingCustomerEdit = lstRoutingCustomerEdit;
			params.lstDeleteStr = lstDeleteStr;
			params.lstRoutingCustomerAdd = lstRoutingCustomerAdd;
			msg = formatString(create_route_confirm_update_route, '');
		}
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			params.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		params.shopCode = $('#shopCodeCB').combobox('getValue');
		if(nextPage==undefined || nextPage==null || nextPage==false){
			Utils.addOrSaveData(params, '/superviseshop/manageroute-create/addorsaverouting', null,  'errMsg', function(data) {
				if(data.tree!=undefined && data.tree!=null){
					SuperviseManageRouteCreate.loadTree(data.tree);
				}
				showSuccessMsg('successMsg',data,function(){
					SuperviseManageRouteCreate.searchCustomerRouting();
					//window.location.href = data.view;
				},1500,jsp_common_save_fail);
				
			}, null, null, null,msg, function(data){
				if(data.errorType!=undefined && data.errorType!=null && data.errorType == 1){
					$('#routingCode').focus();
				}
				if(data.error != undefined && data.error !=null && data.error){
					if(data.lstErr != undefined && data.lstErr !=null && data.lstErr.length >0){
						$('#idDivErrorInsert').html('').change();
						for(var i=0; i<data.lstErr.length; i++){
							if(data.lstErr[i].endDate!=undefined && data.lstErr[i].endDate!=null && data.lstErr[i].endDate.length>0){
								$('#idDivErrorInsert').append('<p id="errMsgInsert" style="background-position: left -370px; color: #FF6633;  font-size: 12px; margin: 5px 0 0 5px; padding: 2px 0 5px 29px;"> '
									+ formatString(create_route_cust_route_todate, [data.lstErr[i].customer.shortCode, data.lstErr[i].routing.routingCode, formatDate(data.lstErr[i].startDate), formatDate(data.lstErr[i].endDate)]) 
									+' </p>');
							}else{
								$('#idDivErrorInsert').append('<p id="errMsgInsert" style="background-position: left -370px; color: #FF6633;  font-size: 12px; margin: 5px 0 0 5px; padding: 2px 0 5px 29px;"> '
									+ formatString(create_route_cust_route_todate_null, [data.lstErr[i].customer.shortCode, data.lstErr[i].routing.routingCode, formatDate(data.lstErr[i].startDate)])
									+ ' </p>');
								
							}
						}
						$('#idDivErrorInsert').show().change();
					}
				}
				if(data.error != undefined && data.error !=null && !data.error && data.view!=undefined && data.view!=null){
					SuperviseManageRouteCreate.searchCustomerRouting();
					//window.location.href = data.view;
				}
			});
		}else{
			Utils.saveData(params, '/superviseshop/manageroute-create/addorsaverouting', null,  'errMsg', function(data) {
							showSuccessMsg('successMsg',data,function(){
								//window.location.href = data.view;
								SuperviseManageRouteCreate.searchCustomerRouting();
							},1000,jsp_common_save_fail);
						}, null, null, null, function(dateError){
							if(dateError.errorType == 1){
								$('#routingCode').focus();
							}
							if(data.error != undefined && data.error !=null && data.error){
								if(data.lstErr != undefined && data.lstErr !=null && data.lstErr.length >0){
									$('#idDivErrorInsert').html('').change();
									for(var i=0; i<data.lstErr.length; i++){
										if(data.lstErr[i].endDate!=undefined && data.lstErr[i].endDate!=null && data.lstErr[i].endDate.length>0){
											$('#idDivErrorInsert').append('<p id="errMsgInsert" style="background-position: left -370px; color: #FF6633;  font-size: 12px; margin: 5px 0 0 5px; padding: 2px 0 5px 29px;"> '
												+ formatString(create_route_cust_route_todate, [data.lstErr[i].customer.shortCode, data.lstErr[i].routing.routingCode, formatDate(data.lstErr[i].startDate), formatDate(data.lstErr[i].endDate)])
												+ ' </p>');
											
										}else{
											$('#idDivErrorInsert').append('<p id="errMsgInsert" style="background-position: left -370px; color: #FF6633;  font-size: 12px; margin: 5px 0 0 5px; padding: 2px 0 5px 29px;"> '
												+ formatString(create_route_cust_route_todate_null, [data.lstErr[i].customer.shortCode, data.lstErr[i].routing.routingCode, formatDate(data.lstErr[i].startDate)])
												+ ' </p>');
										}
									}
									$('#idDivErrorInsert').show().change();
								}
							}
							if(data.error != undefined && data.error !=null && !data.error && data.view!=undefined && data.view!=null){
								SuperviseManageRouteCreate.searchCustomerRouting();
								//window.location.href = data.view;
							}
						});
		}
		return false;
	},
	/**
	 * Chuyen trang them moi
	 * 
	 * @author hunglm16
	 * @since October 6,2014
	 * @description Cap nhat CMS
	 * */
	showDialogConfirm: function() {
		$.messager.confirm(jsp_common_xacnhan, create_route_confirm_direct_add_page, function(r){
			if (r){
				window.location.href = '/superviseshop/manageroute-create/addroute?shopCode=' + $('#shopCode').val();
			}
		});
		
	},
	//TUNGMT
	openMapCustomerRouting: function(){
		var customerMapKey = SuperviseManageRouteCreate._mapCustomerMap.keyArray;
		var customerMapValue = SuperviseManageRouteCreate._mapCustomerMap.valArray;
		SuperviseManageRouteCreate._listRoute=new Array();
		for( var i = 0; i < customerMapKey.length; i++ ) {
			var objCustomer = customerMapValue[i];
			if(objCustomer[20] < 2 && objCustomer[19]==1){
				var obj=new Object();
				obj.t2 = objCustomer[1];
				obj.t3 = objCustomer[2];
				obj.t4 = objCustomer[3];
				obj.t5 = objCustomer[4];
				obj.t6 = objCustomer[5];
				obj.t7 = objCustomer[6];
				obj.cn = objCustomer[7];
				obj.customerId = objCustomer[10];//Customer Id
				obj.customerCode =  objCustomer[11];//customer Short Code
				obj.customerName = objCustomer[0];//Customer Name
				obj.address = objCustomer[12];//Address Customer
				obj.lat = objCustomer[13];
				obj.lng = objCustomer[14];
				SuperviseManageRouteCreate._listRoute.push(obj);
			}
	    }
		SuperviseManageRouteCreate.fillListMarker("t2");
	},
	//HungLM16
	openMapCustomerRoutingNew: function(){
		var param = new Object();
		param.routeId = $('#routingId').val();
		Utils.getJSONDataByAjaxNotOverlay(param,'/superviseshop/manageroute-create/load-Map-By-RoutingCustomer', function(data){
			if(data.lstData!=undefined && data.lstData!=null && data.lstData.length>0){
				for(var i=0; i<data.lstData.length; i++){
					var obj = new Array();
					obj.push(data.lstData[i].customerName);//0 - ten KH
					obj.push(data.lstData[i].monday);//1
					obj.push(data.lstData[i].tuesday);//2
					obj.push(data.lstData[i].wednesday);//3
					obj.push(data.lstData[i].thursday);//4
					obj.push(data.lstData[i].friday);//5
					obj.push(data.lstData[i].saturday);//6 
					obj.push(data.lstData[i].sunday);//7
					obj.push(data.lstData[i].weekInterval);//8
					obj.push(data.lstData[i].startWeek);//9
					obj.push(data.lstData[i].customerId);//10
					obj.push(data.lstData[i].shortCode);//11
					obj.push(data.lstData[i].address);//12
					obj.push(data.lstData[i].lat);//13
					obj.push(data.lstData[i].lng);//14
					obj.push(data.lstData[i].routingCustomerId);//15
					obj.push(1);//16
					obj.push(data.lstData[i].startDateStr);//17
					obj.push(data.lstData[i].endDateStr);//18
					obj.push(SuperviseManageRouteCreate._UPDATE);//19
					
					SuperviseManageRouteCreate._mapCustomerMap.put(data.lstData[i].shortCode, obj);
				}
				
				var customerMapKey = SuperviseManageRouteCreate._mapCustomerMap.keyArray;
				var customerMapValue = SuperviseManageRouteCreate._mapCustomerMap.valArray;
				SuperviseManageRouteCreate._listRoute=new Array();
				for( var i = 0; i < customerMapKey.length; i++ ) {
					var objCustomer = customerMapValue[i];
					var obj=new Object();
					obj.t2 = objCustomer[1];
					obj.t3 = objCustomer[2];
					obj.t4 = objCustomer[3];
					obj.t5 = objCustomer[4];
					obj.t6 = objCustomer[5];
					obj.t7 = objCustomer[6];
					obj.cn = objCustomer[7];
					obj.customerId = objCustomer[10];//Customer Id
					obj.customerCode =  objCustomer[11];//customer Short Code
					obj.customerName = objCustomer[0];//Customer Name
					obj.address = objCustomer[12];//Address Customer
					obj.lat = objCustomer[13];
					obj.lng = objCustomer[14];
					SuperviseManageRouteCreate._listRoute.push(obj);
			    }
				SuperviseManageRouteCreate.fillListMarker("t2");
			}
		});
		
		
		
	},
	toggleDivUlDate:function(){
		$('#divUlDate').toggle('slow');
	},
	resetListRoute:function(){
		if(SuperviseManageRouteCreate._listRoute!=null){
			for(var i=0 ; i<SuperviseManageRouteCreate._listRoute.length ; i++){
				if(SuperviseManageRouteCreate._listRoute[i].customerId!=undefined
						&& SuperviseManageRouteCreate._listRoute[i].customerId!=null && SuperviseManageRouteCreate._listRoute[i].customerId <0)
					SuperviseManageRouteCreate._listRoute[i].customerId = SuperviseManageRouteCreate._listRoute[i].customerId * (-1);
			}
		}
	},

	fillListMarker:function(day){
		if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		ViettelMap.clearOverlays();
		if(SuperviseManageRouteCreate._itv!=undefined && SuperviseManageRouteCreate._itv!=null){
			window.clearInterval(SuperviseManageRouteCreate._itv);
		} 
		$('#divUlDate').hide('slow');
		SuperviseManageRouteCreate._listMarker = new Array();
		SuperviseManageRouteCreate.resetListRoute();
		var listCust=new Array();
		switch(day){
		case "t2":
			$('#strongDate').html(jsp_common_thu_hai);
			for(var i=0 ; i<SuperviseManageRouteCreate._listRoute.length ; i++){
				if(SuperviseManageRouteCreate._listRoute[i].t2==1){
					listCust.push(SuperviseManageRouteCreate._listRoute[i]);
				}
			}
			break;
		case "t3":
			$('#strongDate').html(jsp_common_thu_ba);
			for(var i=0 ; i<SuperviseManageRouteCreate._listRoute.length ; i++){
				if(SuperviseManageRouteCreate._listRoute[i].t3==1){
					listCust.push(SuperviseManageRouteCreate._listRoute[i]);
				}
			}
			break;
		case "t4":
			$('#strongDate').html(jsp_common_thu_bon);
			for(var i=0 ; i<SuperviseManageRouteCreate._listRoute.length ; i++){
				if(SuperviseManageRouteCreate._listRoute[i].t4==1){
					listCust.push(SuperviseManageRouteCreate._listRoute[i]);
				}
			}
			break;
		case "t5":
			$('#strongDate').html(jsp_common_thu_nam);
			for(var i=0 ; i<SuperviseManageRouteCreate._listRoute.length ; i++){
				if(SuperviseManageRouteCreate._listRoute[i].t5==1){
					listCust.push(SuperviseManageRouteCreate._listRoute[i]);
				}
			}
			break;
		case "t6":
			$('#strongDate').html(jsp_common_thu_sau);
			for(var i=0 ; i<SuperviseManageRouteCreate._listRoute.length ; i++){
				if(SuperviseManageRouteCreate._listRoute[i].t6==1){
					listCust.push(SuperviseManageRouteCreate._listRoute[i]);
				}
			}
			break;
		case "t7":
			$('#strongDate').html(jsp_common_thu_bay);
			for(var i=0 ; i<SuperviseManageRouteCreate._listRoute.length ; i++){
				if(SuperviseManageRouteCreate._listRoute[i].t7==1){
					listCust.push(SuperviseManageRouteCreate._listRoute[i]);
				}
			}
			break;
		case "cn":
			$('#strongDate').html(jsp_common_thu_chunhat);
			for(i=0 ; i<SuperviseManageRouteCreate._listRoute.length ; i++){
				if(SuperviseManageRouteCreate._listRoute[i].cn==1){
					listCust.push(SuperviseManageRouteCreate._listRoute[i]);
				}
			}
			break;
		default:
			break;
		}
		//merce cac diem gan nhau thanh 1
		for(var i=0 ; i<listCust.length ; i++){
			if(listCust[i].lat!=null && listCust[i].lat!=0 && listCust[i].lat!=undefined && !isNaN(listCust[i].lat) && listCust[i].lng!=null && listCust[i].lng!=0 && listCust[i].lng!=undefined && !isNaN(listCust[i].lng)){
				var flag=false;
				var kc=0;
				var listMerce=new Array();
				for(j=i+1; j<listCust.length ; j++){
					kc=SuperviseManageRouteCreate.CalculateDistance(listCust[i],listCust[j]);					
					if(kc<100){
						flag=true;
						listMerce.push(listCust[j]);
						listCust.splice(j, 1);
						j--;
					}
				}
				if(flag==true){
					listMerce.push(listCust[i]);//add them kh dang xet vao list
					listCust[i].customerId = -listCust[i].customerId;  // chuyen kh dang xet thanh kh dai dien
					listCust[i].listMerce=listMerce;//gan danh sach kh trong list vao kh dai dien
					SuperviseManageRouteCreate._listMarker.push(listCust[i]);
				}else{
					SuperviseManageRouteCreate._listMarker.push(listCust[i]);
				}
			}
			else{
				listCust.splice(i,1);
				i--;
			}
		}
		$('#errMsgMap').hide();
		if(SuperviseManageRouteCreate._itv!=undefined && SuperviseManageRouteCreate._itv!=null){
			SuperviseManageRouteCreate.addMutilMarkerStaff();
		}else{
			VTMapUtil.loadMapResource(function(){
				ViettelMap.loadBigMap('mapRoute',SuperviseManageRouteCreate._centerLat,SuperviseManageRouteCreate._centerLng, SuperviseManageRouteCreate._zoom, null);
				SuperviseManageRouteCreate.addMutilMarkerStaff();
			});
		}
	},
	addMutilMarkerStaff:function(){
		var map = ViettelMap._map;
		var lstPoint = new Array();
		if(SuperviseManageRouteCreate._listMarker!=undefined && SuperviseManageRouteCreate._listMarker!=null){
			lstPoint = SuperviseManageRouteCreate._listMarker;
			var flag=0;
			var index=0;
			SuperviseManageRouteCreate._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<lstPoint.length;i++,index++,j++){
					if(j>100) break;
					var point = lstPoint[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new viettel.LatLng(point.lat, point.lng);
						var custId=point.customerId;
						var info="<strong>" + Utils.XSSEncode(point.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(point.customerName) + "</strong><br/>";
						info += jsp_common_address + ": ";
						if(point.address != null)//loctt - Oct24, 2013
							info += point.address;
						var title=point.customerCode;
						var image="/resources/images/Mappin/icon_circle_red.png";
						if(custId<0){ 
							title="...";
						}
						var markerContent = '<span id="marker' +custId+'" style="position: relative; left: 30px; top: -10px; color: yellow" class="StaffPositionOridnalVisit"></span>';
						var marker = new viettel.LabelMarker({
							icon:{
								url : image,
								size : {height : 32, width : 32},
								scaledSize : {height : 32, width : 32}
							},
							position : pt,
							map : ViettelMap._map,
							labelContent : markerContent,
							labelClass : "MarkerLabel",
							labelVisible : true,
							draggable : false,
							labelAnchor : new viettel.Point(25, 0)
						});
						
						$('#marker' +point.customerId).parent().prev().css('z-index', 10000000);
						
						$('#marker' +point.customerId).parent().prev().bind('click', point, function(e) {
							var point = e.data;
							SuperviseManageRouteCreate.showDialogCustomerInfo(point);
						});
						
						if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
							ViettelMap._listOverlay = new Array();
							ViettelMap._listOverlay.push(marker);
						} else {
							ViettelMap._listOverlay.push(marker);
						}
					}
				}
				if(index>=lstPoint.length){
					ViettelMap.hideShowTitleMarker();
					window.clearInterval(SuperviseManageRouteCreate._itv);
				}
				ViettelMap.fitOverLay();
				ViettelMap.hideShowTitleMarker();
			}, 300); 
		}
	},
	addMutilMarkerStaffForSetOrder:function(){
		var map = ViettelMap._map;
		var lstPoint = new Array();
		if (SuperviseManageRouteCreate._listMarker !=undefined && SuperviseManageRouteCreate._listMarker !=null){
			lstPoint = SuperviseManageRouteCreate._listMarker;
			var flag=0;
			var index=0;
			SuperviseManageRouteCreate._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<lstPoint.length;i++,index++,j++){
					if(j>100) break;
					var point = lstPoint[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new viettel.LatLng(point.lat, point.lng);
						var custId=point.customerId;
						var info="<strong>" + Utils.XSSEncode(point.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(point.customerName) + "</strong><br/>";
						info += "Địa chỉ: ";
						if(point.address != null)
							info += point.address;
						var ttgt = "";
						if(point.ttgt != undefined && point.ttgt != null){
							ttgt=point.ttgt;
						}
						var image="/resources/images/Mappin/icon_circle_red.png";
						//map.addOverlay(new MyOverlay(custId, pt, image, title,info));
						var markerContent = '<span id="marker' +custId+'" style="color: white; font-weight: bold; z-index: 10000001; position: relative; left: 22px; top: -23px;" class="StaffPositionOridnalVisit" ' + 
							'onclick="SuperviseManageRouteCreate.showDialogCustomerInfoEx(' + point.lat + ',' + point.lng + ',\'' + Utils.XSSEncode(point.customerCode) + '\',\'' + Utils.XSSEncode(point.customerName) + '\',\'' + Utils.XSSEncode(point.address) + '\');">' + ttgt + '</span>' + 
							'<span style="color: blue; font-weight: bold; z-index: 10000001; position: relative; left: 6px; top: -45px;">' + Utils.XSSEncode(point.customerCode) +'</span>';
						if(ttgt > 9){
							markerContent = '<span id="marker' +custId+'" style="color: white; font-weight: bold; z-index: 10000001; position: relative; left: 18px; top: -23px;" class="StaffPositionOridnalVisit"' + 
							'onclick="SuperviseManageRouteCreate.showDialogCustomerInfoEx(' + point.lat + ',' + point.lng + ',\'' + Utils.XSSEncode(point.customerCode) + '\',\'' + Utils.XSSEncode(point.customerName) + '\',\'' + Utils.XSSEncode(point.address) + '\');">' + ttgt + '</span>' + 
							'<span style="color: blue; font-weight: bold; z-index: 10000001; position: relative; left: 0px; top: -45px;">' + Utils.XSSEncode(point.customerCode) +'</span>';
						}
						var marker = new viettel.LabelMarker({
							icon:{
								url : image,
								size : {height : 32, width : 32},
								scaledSize : {height : 32, width : 32}
							},
							position : pt,
							map : ViettelMap._map,
							labelContent : markerContent,
							labelClass : "MarkerLabel",
							labelVisible : true,
							draggable : false,
							labelAnchor : new viettel.Point(25, 0)
						});
						$('#marker' +point.customerId).parent().prev().css('z-index', 10000000);
						$('#marker' +point.customerId).parent().prev().bind('click', point, function(e) {
							var point = e.data;
							SuperviseManageRouteCreate.showDialogCustomerInfo(point);
						});
						if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
							ViettelMap._listOverlay = new Array();
							ViettelMap._listOverlay.push(marker);
						} else {
							ViettelMap._listOverlay.push(marker);
						}
					}
				}
				if (index >= lstPoint.length) {
					ViettelMap.hideShowTitleMarker();
					window.clearInterval(SuperviseManageRouteCreate._itv);
				}
				ViettelMap.fitOverLay();
				ViettelMap.hideShowTitleMarker();
			},300); 
		}
	},
	showDialogCustomerInfo: function(data) {
		if (ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		var pt = new viettel.LatLng(data.lat, data.lng);
		var info="<strong>" + Utils.XSSEncode(data.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(data.customerName) + "</strong><br/>";
		info += jsp_common_address + ": ";
		if(data.address != null)
			info += data.address;
		
		var infoWindow = new viettel.InfoWindow({
			content: info,
			maxWidth: 200,
			position: pt
		});										
		ViettelMap._currentInfoWindow = infoWindow;
		infoWindow.open(ViettelMap._map);
		
		ViettelMap._map.setCenter(pt);
		//$('.olPopup .olPopupContent').css('height','30');
		$('.olPopup .olPopupContent').css('font-size','1em');
//		ViettelMap.fitOverLay();
	},
	showDialogCustomerInfoEx: function(lat, lng, customerCode, customerName, address) {
		var data = new Object();
		data.lat = lat;
		data.lng = lng;
		data.customerCode = customerCode;
		data.customerName = customerName;
		data.address = address;
		if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		var pt = new viettel.LatLng(data.lat, data.lng);
		var info="<strong>" + Utils.XSSEncode(data.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(data.customerName) + "</strong><br/>";
		info += "Địa chỉ: ";
		if (data.address != null) {
			info += data.address;
		}
		
		var infoWindow = new viettel.InfoWindow({
			content: info,
			maxWidth: 200,
			position: pt
		});										
		ViettelMap._currentInfoWindow = infoWindow;
		infoWindow.open(ViettelMap._map);
		
		ViettelMap._map.setCenter(pt);
		//$('.olPopup .olPopupContent').css('height','30');
		$('.olPopup .olPopupContent').css('font-size','1em');
	},
	CalculateDistance:function(i,k){
		var n,l,h;
		var j;
		var c;
		var e;
		var a;
		var m;
		var d = i.lat;
		var b = i.lng;
		var g = k.lat;
		var f = k.lng;
		j = d * (Math.PI / 180);
		c = b * (Math.PI / 180);
		e = g * (Math.PI / 180);
		a = f * (Math.PI / 180);
		n = b - f;
		m = n * (Math.PI / 180);
		h = Math.sin(j)*Math.sin(e)+Math.cos(j)*Math.cos(e)*Math.cos(m);
		h = Math.acos(h);
		l = h * 180 / Math.PI;
		l = l * 60 * 1.1515;
		l = l * 1.609344 * 1000;
		return Math.round(l);
	},
	
	/**
	 * Import Tuyen
	 * 
	 * @author hunglm16 
	 * @author October 8,2014
	 * @description thay doi su dung ham chung
	 * */
	importTuyenByExcel:function(){
		$('.ErrorMsgStyle').html("").change().hide();
		$('.SuccessMsgStyle').html('').hide();
		Utils.importExcelUtils(function(data){
			TreeUtils.loadRoutingTree('routingTree', null, $('#routingId').val() ,function(data) {
				window.location.href= '/superviseshop/manageroute-create/editroute?routeId=' + data + '&shopCode=' + $('#shopCode').val();
			},  function(node) {
				if (node.text == activeStatusText) {
					window.location.href= '/superviseshop/manageroute-create/info?status=' + activeType.RUNNING + '&shopCode=' + $('#shopCode').val();
				} else {
					window.location.href= '/superviseshop/manageroute-create/info?status=' + activeType.STOPPED + '&shopCode=' + $('#shopCode').val();
				}
			}, $('#shopCode').val());
			SuperviseManageRouteCreate.searchRoute();
			$('#excelFile').val("").change();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message !=null && data.message.trim().length > 0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				$('.ErrorMsgStyle').html("").change().hide();
				$('#successMsg').html(msgCommon1).show();
				var tm = setTimeout(function(){
					//Load lai danh sach quyen
					$('.SuccessMsgStyle').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
	},
	exportTuyenByExcel:function(){	
		$('.ErrorMsgStyle').html("").change();
		var dataModel = new Object();
		
		var flagCheckAll = $('td[field=routingId] div.datagrid-header-check input[type=checkbox]')[0].checked;
		if (flagCheckAll) {
			dataModel.status = 1;
		} else if(SuperviseManageRouteCreate._rCheckedMap != undefined && SuperviseManageRouteCreate._rCheckedMap != null && SuperviseManageRouteCreate._rCheckedMap.size() > 0){
			dataModel.status = 0;
			var arrExport = SuperviseManageRouteCreate._rCheckedMap.valArray;
			var lstRoutingId = new Array();
			var idTemp = 0;
			for (var i=0; i< SuperviseManageRouteCreate._rCheckedMap.size(); i++) {
				if (idTemp != arrExport[i].routingId) {
					idTemp = arrExport[i].routingId;
					lstRoutingId.push(idTemp);
				}
			}
			dataModel.lstId = lstRoutingId;
		}
		
		var staffSaleCode = $('#staffCodeExport').val().trim();
		var routingCode = $('#routingCodeExport').val().trim();
		var routingName = $('#routingNameExport').val().trim();
		
		dataModel.routingCode = routingCode;
		dataModel.routingName = routingName;
		dataModel.staffSaleCode = staffSaleCode;
		dataModel.shopCode = $('#shopCode').val();
		ReportUtils.exportReport('/superviseshop/manageroute-create/export-tuyen-by-excel', dataModel);
	},
	searchRouteSetOrder: function(){
		$('.ErrorMsgStyle').html('').hide();
		$('#routingContainerGrid').html('<table id="routingGrid"></table>').show().change();
		/*Chen grid*/
		var params = new Object();
		params.routingId = $('#routingId').val();
		params.saleDate = $('#listDate').val();
		SuperviseManageRouteSetOrder._filterSaleDate = $('#listDate').val();
		SuperviseManageRouteSetOrder.convertSeq();
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			params.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		$('#routingGrid').datagrid({
			url :'/superviseshop/manageroute-create/order/search',
	        queryParams:params,
	        rownumbers : true,
	        pageNumber : 1,
	        singleSelect :true,
	        scrollbarSize: 0,
	        autoWidth: true,
	        autoRowHeight : true,
	        fitColumns : true,
			width : ($('#routingContainerGrid').width()),
			columns:[[  
				{field:'shortCode',title:jsp_common_cust_code,width:100, align:'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field:'customerName',title:jsp_common_cust_name,width:250, align:'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}}, 
			    {field:'address',title:jsp_common_address,width:350,align:'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field:	jsp_common_thu_hai,title:'TT',width:100,align:'center', formatter: function(value, row, index) {//SuperviseManageRouteSetOrder._seqTmp
			    	if(row[SuperviseManageRouteSetOrder._seqTmp] == null) {
			    		return '<input type="text" id="seq_' +row.customerId+'" onkeypress="NextAndPrevTextField(event,this,\'InputTextStyleSetOrder\')" class="seq InputTextStyleSetOrder" value="" maxlength="5"></input>';
			    	} else {
			    		return '<input type="text" id="seq_' +row.customerId+'" onkeypress="NextAndPrevTextField(event,this,\'InputTextStyleSetOrder\')" class="seq InputTextStyleSetOrder" value="' +row[SuperviseManageRouteSetOrder._seqTmp]+'" maxlength="5" ></input>';
			    	}
			    }},
			]],
	        onLoadSuccess :function(data) {
	            $('.InputTextStyleSetOrder').each(function(){
	            	$(this).numberbox({ 
	 	                min:1,
	 	                max: data.total,
	 	                precision:0
	 	            });  
	    		});
		    	$('#routingContainerGrid .datagrid-header-rownumber').html(jsp_common_numerical_order);
		    	updateRownumWidthForDataGrid('#routingContainerGrid');
		    	SuperviseManageRouteSetOrder.resize();
	    	}
		});
		
		var routingId = $('#routingId').val();
		var saleDate = $('#listDate').val();
		SuperviseManageRouteSetOrder.convertSeq();
		$('#routingGrid').datagrid('reload', {routingId: routingId,saleDate :saleDate});
		SuperviseManageRouteSetOrder._filterSaleDate = saleDate;
	},
	
	changeSelectBox: function(){
		$('.ErrorMsgStyle').html('').hide();
		if($('#tabActive2').hasClass('Active')) {
			SuperviseManageRouteCreate.searchRouteSetOrderNew();
		} else{
			SuperviseManageRouteSetOrder.openMapCustomerRouting();
		}
	},
	//Tab 3 - AddOrUpdateStaff
	addOrUpdateStaff: function(isUpdate, confirmMsg){
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		var saleStaffId = $('#saleStaffId').combobox('getValue').trim();
		if(msg.length == 0 && saleStaffId == 0) {
			msg = create_route_no_choose_staff_code;
			$('#saleStaffId').focus();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate',jsp_common_from_date);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate',jsp_common_from_date);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate',jsp_common_to_date);
		}
		
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();

		if (msg.length == 0 && !isUpdate && !Utils.compareDate(getNextBySysDateForNumber(0), fromDate)) {
			msg = msgCommonErr5;
		}
		
		if(msg.length ==0 && !Utils.compareDate(fromDate,toDate)){
			msg =msgCommonErr4;
			$('#fromDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsgStaff').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		if(isUpdate) dataModel.visitPlanId = SuperviseManageRouteAssign._visitPlanId;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			dataModel.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		dataModel.status = 1;
		dataModel.saleStaffId  = saleStaffId;
		dataModel.routingId = $('#routingId').val();
		if (confirmMsg != undefined && confirmMsg != null) {
			dataModel.isAllowDelete = true;
		}
		Utils.addOrSaveData(dataModel, "/superviseshop/manageroute-create/assign/addorupdate-staff", null, 'errMsgStaff',function(data){
			if(data.error ==undefined || data.error==null || !data.error){
				$('#staffGrid').datagrid('reload');				
				SuperviseManageRouteAssign.resetStaff();
			}
		}, null, null, null, confirmMsg, function(data) {
			if(data.error !=undefined && data.error!=null && data.error){
				if (data.confirmDelete != undefined && data.confirmDelete != null && data.confirmDelete) {//confirm
					SuperviseManageRouteCreate.addOrUpdateStaff(isUpdate, data.errMsg);
				}
			}
		});
		return false;
	},
	//Tab2 - updateListRoutingCustomer
	/**
	 * Cap nhat phan quyen voi CMS
	 * 
	 * @author hunglm16
	 * @since October 6,2014
	 * */
	updateListRoutingCustomer: function(){
		$('#errMsgRoucus').html('').hide();
		var arraySeq = SuperviseManageRouteSetOrder.getListSeq();
		//SuperviseManageRouteSetOrder.pushDataGridForMap();
		var rows = $('#routingGrid').datagrid('getRows');//SuperviseManageRouteSetOrder._listCustomerMap.keyArray;
		var flag = false;
		var i=0;
		
		if((arraySeq == null || rows == null) || ( arraySeq.length == 0 || rows.length == 0)){
			$('#errMsgRoucus').html(create_route_no_data_to_update).show();
			var tm = setTimeout(function(){
				$('#errMsgRoucus').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return;
		}
		var arrShortCode = new Array();
		var ids="";
		var selector=null;
		for(i=0; i<arraySeq.length; i++){
			var custCode = rows[i].shortCode;
			arrShortCode.push(custCode);
			/*if(!isNullOrEmpty(arraySeq[i]) && arraySeq[i]> rows.length){
				ids+=custCode+" , ";
				if(selector==null) selector = $('#seq_' + rows[i].customerId);
			}*/ 
		}
		/*if(ids.length>0){
			ids = ids.substring(0, ids.length-3);
			$('#errMsgRoucus').html(formatString(create_route_stt_ma_khong_lon_hon_sl_kh, ids)).show();
			selector.focus();
			return;
		}*/
		
		var checkSeq = false;
		var valueId;
		$('.seq').each(function(){
			var value = $(this).attr('value');
			if(isNaN(value)){
				if(checkSeq){
					return;
				}else{
					valueId = $(this).attr('id'); 
					$('#errMsgRoucus').html(create_route_stt_lon_hon_0).show();
					var tm = setTimeout(function(){
						$('#errMsgRoucus').html('').hide();
						clearTimeout(tm);					
					}, 5000);
					checkSeq = true;
				}
			}
		});
		if(checkSeq){
			$('#' +valueId).focus();
			return;
		}
		
		/*var flag=false;
		var idSame = "";
		for (i = 0; i < arraySeq.length - 1; i++) {
			for (j = i + 1; j < arraySeq.length; j++) {
				if (arraySeq[i] != '' && arraySeq[i] == arraySeq[j]) {
					var custCode1 = rows[i].shortCode;
					var custCode2 = rows[j].shortCode;
					idSame = formatString(create_route_stt_code_duplicate_code, custCode1, custCode2);
					selector = $('#seq_' + rows[j].customerId);
					flag = true;
					break;
				}
			}
			if(flag) break;
		}
		if (idSame.length > 0) {
			$('#errMsgRoucus').html(idSame).show();
			selector.focus();
			return;
		}*/
		var saleDate =  $('#listDate').val();
		if(saleDate == -1) {
			$('#errMsgRoucus').html(create_route_sale_day_chua_chon).show();
			$('#listDate').focus();
			return;
		}
		var params =  new Object();
		var saleDate = $('#listDateNew').val();
		params.lstSeq = arraySeq;
		params.lstShortCode = arrShortCode;
		params.saleDate = saleDate;
		params.routingId = $('#routingId').val().trim();
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			params.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		
		Utils.addOrSaveData(params, '/superviseshop/manageroute-create/order/update', null, 'errMsgRoucus', function(data){
			if(data.error!=undefined && data.error!=null && !data.error){
				$('#successMsgRoucus').html(jsp_common_save_success).change().show();
				setTimeout(function() {
					   $('#successMsgRoucus').val('').change().hide();
				}, 1500);
			}
		}, null,null,null,create_route_confirm_update_stt);
	},
	
	coppyCustomerRouting: function(index){
		var day = 0;
		var week = 0;
		var indexNew = index.substring(0,3) + SuperviseManageRouteCreate._mapCustomer.size().toString();
		var obj = new Array();
		for(var i = 0; i <= 16; i++){
			var item =  SuperviseManageRouteCreate._mapCustomer.get(index)[i];
			obj.push(item);
			if (i >= 1 && i <=7 && item != undefined && item != null && item == 1) {//t2 -> cn
				day++;
			}
		}
		obj.push($('#sysdateServer').val().trim());//17
		obj.push('');//18
		obj.push(SuperviseManageRouteCreate._ADD);//19
		obj.push(1);//20
		obj.push(1);//21
		obj.push("newcoppy");//22 - parent Coppy*/
		//W1, W2, W3, W4
		for(var i = 23; i <= 26; i++){
			var item =  SuperviseManageRouteCreate._mapCustomer.get(index)[i];
			obj.push(item);
			if (item != undefined && item != null && item == 1) {
				week++;
			}
		}
		obj.push(week * day);//27 tần suất
		//SuperviseManageRouteCreate._mapCustomerStand.put(shortCode + index, jQuery.extend(true, [], obj));
		
		/*Xu ly dua len giao dien*/
		var mapCustomerSort = new Map();//Tao Map tam
		var mapCustomerKeyNew = new Array(); 
		$('#lstRoutingCustomerDetail').html('').show().change();
		//Them danh sach khach hang hien dang co và trong Map() tam
		for(var i = 0; i< SuperviseManageRouteCreate._mapCustomer.size(); i++){
			mapCustomerSort.put(SuperviseManageRouteCreate._mapCustomer.keyArray[i], SuperviseManageRouteCreate._mapCustomer.valArray[i]);
			mapCustomerKeyNew.push(mapCustomerSort.keyArray[i]);
		}
		//Dua danh sach khach hang hien dang co ve rong
		SuperviseManageRouteCreate._mapCustomer.clear();
		SuperviseManageRouteCreate._mapCustomer = new Map();
		//Coppy dong moi
		mapCustomerSort.put(indexNew, obj);
		
		mapCustomerSort.get(index)[18] = $('#yesterdateServer').val().trim();
		mapCustomerSort.get(index)[20] = 2;//Qua khu
		mapCustomerSort.get(index)[21] = 0;//Qua khu
		mapCustomerSort.get(index)[22] = 'coppy';//Qua khu
		
		
		mapCustomerKeyNew.push(indexNew);
		//Thuat toan sap xep theo ma code
		mapCustomerKeyNew = sortArray.quickSort(mapCustomerKeyNew);
		//Gan lai danh sach vao Map() khach hang ban dau va Show() HTML
		for(var i=0; i< mapCustomerKeyNew.length; i++){
			var obj = mapCustomerSort.get(mapCustomerKeyNew[i]);
			if(obj !=undefined && obj!=null){
				SuperviseManageRouteCreate._mapCustomer.put(mapCustomerKeyNew[i], obj);
				var html = SuperviseManageRouteCreate.getHTMLCustomerRouting(mapCustomerKeyNew[i], obj);
				$('#lstRoutingCustomerDetail').append(html);			
			}
		}
		mapCustomerSort = new Map();//Dua danh sach khach hang tam ve rong
		//Xu ly defaul cho danh sach moi
		SuperviseManageRouteCreate.showOrderAndScrollBody();
		SuperviseManageRouteCreate.clearSearchBoxCustomer();
		SuperviseManageRouteCreate.showDefaulCustomerRowtingSearchInsert();
	},
	
	/**
	 * Tao moi tuyen
	 * 
	 * @author hunglm16
	 * @since October 6,2014
	 * @description Cap nhat CMS
	 * */
	addRoutingNew : function(){
		//Them moi mot tuyen
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('routingCode',jsp_common_route_code);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('routingCode',jsp_common_route_code, Utils._CODE);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('routingName',jsp_common_route_name);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('routingName',jsp_common_route_name, Utils._NAME);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.routingCode = $('#routingCode').val().trim();
		params.routingName = $('#routingName').val().trim();
		params.shopCode = $('#shopCode').val();
		if (SuperviseManageRouteCreate._shopIdIsView != undefined && SuperviseManageRouteCreate._shopIdIsView != null) {
			params.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			params.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		msg = create_route_confirm_create_route;
		$.messager.confirm(jsp_common_xacnhan, msg, function(r){
			if (r){
				Utils.saveData(params, '/superviseshop/manageroute-create/creatRoutingNew', null,  'errMsg', function(data) {
					showSuccessMsg('successMsg',data,function(){
						window.location.href = data.view;
					},1000,jsp_common_save_fail);
				}, null, null, null, function(dateError){
					if(dateError.errorType == 1){
						$('#routingCode').focus();
					}
					if(data.error != undefined && data.error !=null && data.error){
						if(errMsg != undefined && errMsg != null && errMsg.length>0){
							$('#errMsg').html(errMsg).show();
							$('#routingCode').focus();
						}else{
							$('#errMsg').html(jsp_common_save_fail).show();
							$('#routingCode').focus();
						}
					}
					if(data.error != undefined && data.error !=null && !data.error && data.view!=undefined && data.view!=null){
						window.location.href = data.view;
					}
				});
			}
		});	
		
		return false;
	},
	/**
	 * Cap nhat thong tin Tuyen
	 * 
	 * @author hunglm16
	 * @since October 8,2014
	 * */
	updateRoutingNew : function(){
		//Them moi mot tuyen
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('routingCode',jsp_common_route_code, Utils._CODE);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('routingName',jsp_common_route_name);
		}
//		if(msg.length ==0){
//			msg = Utils.getMessageOfSpecialCharactersValidate('routingName',jsp_common_route_name, Utils._NAME);
//		}
		if(msg.length > 0){
			$('#errMsgUpdateRouting').html(msg).show();
			return false;
		}
		var params = new Object();
		params.routeId = $('#routingId').val().trim();
		params.routingCode = $('#routingCode').val().trim();
		params.routingName = $('#routingName').val().trim();
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			params.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		msg = formatString(create_route_confirm_update_route, $('#routingCode').val().trim());
		$.messager.confirm(jsp_common_xacnhan, msg, function(r){
			if (r){
				Utils.saveData(params, '/superviseshop/manageroute-create/updateRoutingNew', null,  'errMsgUpdateRouting', function(data) {
					$('#successMsgUpdateRouting').html(jsp_common_save_success).show().change();
					var tm = setTimeout(function(){
						$('#successMsgUpdateRouting').html('').show().change();
						clearTimeout(tm);				 	
					}, null);
					//Load lai cay don vi Tuyen
					TreeUtils.loadRoutingTree('routingTree', null, $('#routingId').val(), function(data) {
						window.location.href= '/superviseshop/manageroute-create/editroute?routeId=' + data + '&shopCode=' + $('#shopCode').val();
					}, null, $('#shopCode').val());
					return false;
				}, null, null, null, function(dateError){
					if(dateError.errorType == 1){
						$('#routingCode').focus();
					}
					if(data.error != undefined && data.error !=null && data.error){
						if(errMsg != undefined && errMsg != null && errMsg.length>0){
							$('#errMsg').html(errMsg).show();
							$('#routingCode').focus();
						}else{
							$('#errMsgUpdateRouting').html(jsp_common_save_fail).show();
							$('#routingCode').focus();
						}
					}
					if(data.error != undefined && data.error !=null && !data.error && data.view!=undefined && data.view!=null){
						window.location.href = data.view;
					}
				});
			}
		});	
		
		return false;
	},
	
	searchRouteSetOrderNew: function(){
		$('#errMsg').html('').hide();
		var routingId = $('#routingId').val();
		var saleDate = $('#listDateNew').val();
		var shopCode = $('#shopCode').val();
		SuperviseManageRouteCreate.convertSeq();
		$('#routingGrid').datagrid('reload', {routingId: routingId,saleDate :saleDate, shopCode: shopCode});
		SuperviseManageRouteSetOrder._filterSaleDate = saleDate;
	},
	
	convertSeq: function(){
		var saleDate = $('#listDateNew').val();
		if(saleDate == 1) SuperviseManageRouteSetOrder._seqTmp = 'seq8';
		else if(saleDate == 2) SuperviseManageRouteSetOrder._seqTmp = 'seq2';
		else if(saleDate == 3) SuperviseManageRouteSetOrder._seqTmp = 'seq3';
		else if(saleDate == 4) SuperviseManageRouteSetOrder._seqTmp = 'seq4';
		else if(saleDate == 5) SuperviseManageRouteSetOrder._seqTmp = 'seq5';
		else if(saleDate == 6) SuperviseManageRouteSetOrder._seqTmp = 'seq6';
		else if(saleDate == 7) SuperviseManageRouteSetOrder._seqTmp = 'seq7';
		else SuperviseManageRouteSetOrder._seqTmp = '';
	},
	/**
	 * Autocomlete Nhan vien ban hang
	 * @author hunglm16
	 * @since MAY 15,2014
	 * */
	getDataComboboxStaffSale : function(objectId, prefix) {
		var data = new Array();		
		var parent = '';
		if (prefix != undefined && prefix != null) {
			parent = prefix;
		}
		$(parent + '#' + objectId + ' option').each(function() {
			var obj = new Object();
			obj.id = $(this).val().trim();
			obj.staffName = $(this).attr("name").trim();
			obj.staffCode = $(this).attr("code").trim();
			obj.staffCodeName = $(this).attr("code").trim() + ' - ' + $(this).attr("name").trim();
			obj.displayText = $(this).val().trim() + ' - ' + $(this).text().trim(); 
			obj.searchText = unicodeToEnglish($(this).val().trim()+ $(this).text().trim());
			obj.searchText = obj.searchText.toUpperCase();
			data.push(obj);
		});
		return data;
	},
	bindComboboxStaffSaleEasyUI:function(objectId,prefix){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		$(parent + '#' +objectId).combobox({			
			valueField : 'id',
			textField : 'staffCodeName',
			data : SuperviseManageRouteCreate.getDataComboboxStaffSale(objectId,prefix),
			//width:250,
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
			}
		});
		$('.combo-arrow').unbind('click');
	},
	downloadTemplate:function() {
		var url = "/superviseshop/manageroute-create/download-template";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manageroute-create.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manageroute-assign.js
 */
var SuperviseManageRouteAssign = {
	_visitPlanId :null ,
	hideAllTab: function(){
		$('#tabActive1').removeClass('Active');
		$('#tabActive2').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
	},
	showTab1: function(){
		$('.ErrorMsgStyle').html('').hide();
		SuperviseManageRouteAssign.hideAllTab();
		$('#tabActive1').addClass('Active');
		$('#container1').show();
		$('#customerGrid').show();
		SuperviseManageRouteAssign.resize();
	},
	showTab2: function(){
		$('.ErrorMsgStyle').html('').hide();
		SuperviseManageRouteAssign.hideAllTab();
		$('#tabActive2').addClass('Active');
		$('#container2').show();
		SuperviseManageRouteAssign.openMapCustomerRouting();
	},
	resize: function() {
		var hSidebar1=$('.ContentSection').height();
		heightContent = window.innerHeight - $('#header').height() - $('.BreadcrumbSection').height() - $('#footer').height();
		if(hSidebar1 > heightContent ) {
			heightContent = $('.ContentSection').height();
		}
		$('.ReportTreeSection').height(heightContent-95);
	},
	editStaff: function(visitPlanId,saleStaffId,fromDate,toDate,status) {
		$('.ErrorMsgStyle').html('');
		$('#errMsg').html('').hide();
		$('#btnUpdate,#btnCancel').show();
		$('#btnAdd').hide();
		SuperviseManageRouteAssign._visitPlanId = visitPlanId;
		disableCombo('saleStaffId');
		setComboValue('saleStaffId',saleStaffId);
		$('#fromDate').val(fromDate);
		$('#toDate').val(toDate);
		$('#flagFromDate').val(fromDate);
		$('#flagToDate').val(toDate);
		var yesterdate = $('#yesterdateServer').val().trim();
		if(fromDate!=undefined && fromDate!=null && Utils.compareDate(fromDate, yesterdate)){
			disableDateTimePicker('fromDate');
		}else{
			enableDateTimePicker('fromDate');
		}
		if(toDate!=undefined && toDate!=null && toDate.length > 0 && Utils.compareDate(toDate, yesterdate)){
			disableDateTimePicker('toDate');
		}else{
			enableDateTimePicker('toDate');
		}
		return false;
	},
	resetStaff: function(){
		$('#errMsg').html('').hide();
		$('#btnUpdate,#btnCancel').hide();
		$('#btnAdd').show();
		enableCombo('saleStaffId');
		setComboValue('saleStaffId',0);
		enableDateTimePicker('fromDate');
		enableDateTimePicker('toDate');
		$('#fromDate').val($('#sysdateServer').val().trim());
		$('#toDate').val("");
		$('#flagFromDate').val($('#sysdateServer').val().trim());
		$('#flagToDate').val("");
	},
	detail :function(routingId) {
		var params = new Object();
		params.routingId = routingId;
		Utils.getHtmlDataByAjax(params, '/superviseshop/manageroute-create/assign/detail',function(data) {
			$("#routingContent").html(data).show();
			SuperviseManageRouteAssign.resize();
		}, null, 'POST');
	},
	addOrUpdateStaff: function(isUpdate){
		var msg = '';
		$('#idDivErrorInsert').html('').change();
		$('.ErrorMsgStyle').html('').hide();
		var saleStaffId = $('#saleStaffId').combobox('getValue').trim();
		if(msg.length == 0 && saleStaffId == 0) {
			msg = 'Bạn chưa chọn mã NV';
			$('#saleStaffId').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate',jsp_common_from_date);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate',jsp_common_from_date);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate',jsp_common_to_date);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate',jsp_common_to_date);
		}
		
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		
		if(msg.length ==0 && !Utils.compareDate(fromDate,toDate)){
			msg = msgCommonErr3;
			$('#fromDate').focus();
		}
		if(msg.length == 0) {
			msg = Utils.compareCurrentDateGreaterEx('toDate',jsp_common_to_date);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(isUpdate) dataModel.visitPlanId = SuperviseManageRouteAssign._visitPlanId;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		dataModel.status = 1;
		dataModel.saleStaffId  = saleStaffId;
		dataModel.routingId = $('#routingId').val();
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			dataModel.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		Utils.addOrSaveData(dataModel, "/superviseshop/manageroute-create/assign/addorupdate-staff", null, 'errMsg',function(data){
			if(data.error !=undefined && data.error!=null && data.error){
				if(data.lstErr != undefined && data.lstErr !=null && data.lstErr.length >0){
					$('#idDivErrorInsert').html('').change();
					for(var i=0; i<data.lstErr.length; i++){
						if(data.lstErr[i].endDate!=undefined && data.lstErr[i].endDate!=null && data.lstErr[i].endDate.length>0){
							$('#idDivErrorInsert').append('<p id="errMsgInsert" style="background-position: left -370px; color: #FF6633;  font-size: 12px; margin: 5px 0 0 5px; padding: 2px 0 5px 29px;"> '
								+ formatString(create_route_cust_route_todate, [data.lstErr[i].customer.shortCode, data.lstErr[i].routing.routingCode, formatDate(data.lstErr[i].startDate), formatDate(data.lstErr[i].endDate)]) 
								+' </p>');
						}else{
							$('#idDivErrorInsert').append('<p id="errMsgInsert" style="background-position: left -370px; color: #FF6633;  font-size: 12px; margin: 5px 0 0 5px; padding: 2px 0 5px 29px;"> '
								+ formatString(create_route_cust_route_todate_null, [data.lstErr[i].customer.shortCode, data.lstErr[i].routing.routingCode, formatDate(data.lstErr[i].startDate)])
								+ ' </p>');
							
						}
					}
					$('#idDivErrorInsert').show().change();
				}
			}else{
				$('#staffGrid').datagrid('reload');				
				SuperviseManageRouteAssign.resetStaff();
			}			
		});
		return false;
	},
	//MAP
	_listRoute: null,
	_listMarker:null,
	_zoom:18,
	_centerLat:0,
	_centerLng:0,
	openMapCustomerRouting: function(){
		var lstRoutingCustomer = $('#customerGrid').datagrid('getData').rows;
		SuperviseManageRouteCreate._listRoute=new Array();
		for( var i = 0; i < lstRoutingCustomer.length; i++ ) { 
			var objCustomer = lstRoutingCustomer[i];
			if(objCustomer.status =='RUNNING') {
				var obj=new Object();
				if(objCustomer.monday =='GO') obj.t2 =1 ;
				else obj.t2 =0;
				if(objCustomer.tuesday =='GO') obj.t3 =1 ;
				else obj.t3 =0;
				if(objCustomer.wednesday =='GO') obj.t4 =1 ;
				else obj.t4 =0;
				if(objCustomer.thursday =='GO') obj.t5 =1 ;
				else obj.t5 =0;
				if(objCustomer.friday =='GO') obj.t6 =1 ;
				else obj.t6 =0;
				if(objCustomer.saturday =='GO') obj.t7 =1 ;
				else obj.t7 =0;
				if(objCustomer.sunday =='GO') obj.cn =1 ;
				else obj.cn =0;
				obj.customerId = objCustomer.customer.id;
				obj.customerCode = objCustomer.customer.shortCode;
				obj.customerName = objCustomer.customer.customerName;
				obj.address = objCustomer.customer.address;
				obj.lat = objCustomer.customer.lat;
				obj.lng = objCustomer.customer.lng;
				SuperviseManageRouteCreate._listRoute.push(obj);
			} 
		}
		SuperviseManageRouteCreate.fillListMarker("t2");
		SuperviseManageRouteAssign.resize();
	},
	
	changeByDateStaff : function(input, flag){
		$('.ErrorMsgStyle').html('').hide();
		var value = $(input).val();
		var fDate = $('#flagFromDate').val().trim();
		var tDate = $('#flagToDate').val().trim();
		var sysdate = $('#sysdateServer').val().trim();
		if(flag == 1){
			if(value!=null && value.length>0 && value!=null & tDate.length>0 && !Utils.compareDate(value, tDate)){
				$.messager.alert(jsp_common_thong_bao, msgCommonErr3,null,function(){
					$(input).val(fDate);
					$(input).focus();
				});
			}else if(value!=null && value.length>0 && !Utils.compareDate(sysdate, value)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr5,null,function(){
					$(input).val(fDate);
					$(input).focus();
				});
			}else if(value == null || value.length == 0){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr8,null,function(){
					$(input).val(fDate);
					$(input).focus();
				});
			}else{
				$('#flagFromDate').val(value);
			}
		} else {
			if(value!=null && value.length>0 && !Utils.compareDate(fDate, value)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr3,null,function(){
					$(input).val(tDate);
					$(input).focus();
				});
			}else if(value!=null && value.length>0 && !Utils.compareDate(sysdate, value)){//
				$.messager.alert(jsp_common_thong_bao,msgCommonErr6,null,function(){
					$(input).val(tDate);
					$(input).focus();
				});
			}else{
				$('#flagToDate').val(value);
			}
		}
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manageroute-assign.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manageroute-setorder.js
 */
var SuperviseManageRouteSetOrder = {
	_xhrSave : null,
	_listCustomer : null,
	_listCustomerMap: null,
	_seqTmp :null,
	_filterSaleDate : null,
	searchRouteSetOrder: function(){
		$('#errMsg').html('').hide();
		var routingId = $('#routingId').val();
		var saleDate = $('#listDate').val();
		SuperviseManageRouteSetOrder.convertSeq();
		$('#routingGrid').datagrid('reload', {routingId: routingId,saleDate :saleDate});
		SuperviseManageRouteSetOrder._filterSaleDate = saleDate;
	},
	convertSeq: function(){
		var saleDate = $('#listDate').val();
		if(saleDate == 1) SuperviseManageRouteSetOrder._seqTmp = 'seq8';
		else if(saleDate == 2) SuperviseManageRouteSetOrder._seqTmp = 'seq2';
		else if(saleDate == 3) SuperviseManageRouteSetOrder._seqTmp = 'seq3';
		else if(saleDate == 4) SuperviseManageRouteSetOrder._seqTmp = 'seq4';
		else if(saleDate == 5) SuperviseManageRouteSetOrder._seqTmp = 'seq5';
		else if(saleDate == 6) SuperviseManageRouteSetOrder._seqTmp = 'seq6';
		else if(saleDate == 7) SuperviseManageRouteSetOrder._seqTmp = 'seq7';
		else SuperviseManageRouteSetOrder._seqTmp = '';
	},
	detail :function(routingId) {
		var params = new Object();
		params.routingId = routingId;
		Utils.getHtmlDataByAjax(params, '/superviseshop/manageroute-create/order/detail',function(data) {
			$("#routingContent").html(data).show();
			SuperviseManageRouteSetOrder.resize();
		}, null, 'POST');
	},
	resize: function() {
		var hSidebar1=$('.ContentSection').height();
		heightContent = window.innerHeight - $('#header').height() - $('.BreadcrumbSection').height() - $('#footer').height();
		if(hSidebar1 > heightContent ) {
			heightContent = $('.ContentSection').height();
		}
		$('.ReportTreeSection').height(heightContent-95);
		return ;
	},
	hideAllTab: function(){
		$('#tabActive1').removeClass('Active');
		$('#tabActive2').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
	},
	showTab1: function(){
		SuperviseManageRouteSetOrder.hideAllTab();
		$('#tabActive1').addClass('Active');
		$('#container1').show();
		SuperviseManageRouteSetOrder.searchRouteSetOrder();
		SuperviseManageRouteSetOrder.resize();
	},
	showTab2: function(){
		SuperviseManageRouteSetOrder.hideAllTab();
		$('#tabActive2').addClass('Active');
		$('#container2').show();
		SuperviseManageRouteSetOrder.openMapCustomerRouting();
	},
	changeSelectBox: function(){
		if($('#tabActive1').hasClass('Active')) {
			SuperviseManageRouteSetOrder.searchRouteSetOrder();
		} else{
			SuperviseManageRouteSetOrder.openMapCustomerRouting();
		}
	},
//	UPDATE
	pushDataGridForMap :function(){
		SuperviseManageRouteSetOrder._listCustomerMap = new Map();
		var rows = $('#routingGrid').datagrid('getRows');
		if($.isArray(rows)) {
			for(var i = 0; i < rows.length; i++) {
				var customer = rows[i].customer;
				SuperviseManageRouteSetOrder._listCustomerMap.put(rows[i].customerId, customer);
			}
		}
	},
	getListSeq: function(){
		var selected = new Array();
		$('.seq').each(function() {
			selected.push($(this).attr('value'));
		});
		return selected;
	},
	getListRoutingCustomerId: function(){
		var selected = new Array();
		var rows = $('#routingGrid').datagrid('getRows');
		if($.isArray(rows)) {
			for(var i = 0; i < rows.length; i++) {
				var customer = rows[i].customer;
				if(customer != null) {
					selected.push(customer.id);
				}
			}
		}
		return selected;
	},
	updateListRoutingCustomer: function(){
		$('#errMsg').html('').hide();
		var arraySeq = SuperviseManageRouteSetOrder.getListSeq();
		//SuperviseManageRouteSetOrder.pushDataGridForMap();
		var rows = $('#routingGrid').datagrid('getRows');//SuperviseManageRouteSetOrder._listCustomerMap.keyArray;
		var flag = false;
		var i=0;
		
		if((arraySeq == null || rows == null) || ( arraySeq.length == 0 || rows.length == 0)){
			$('#errMsg').html(create_route_no_data_to_update).show();
			var tm = setTimeout(function(){
				$('#errMsg').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return;
		}
		var arrShortCode = new Array();
		var ids="";
		var selector=null;
		for(i=0; i<arraySeq.length; i++){
			var custCode = rows[i].shortCode;
			arrShortCode.push(custCode);
			if(isNullOrEmpty(arraySeq[i])) {
				$('#errMsg').html(formatString(create_route_cust_have_not_ttgt, custCode)).show();
				$('#seq_' +rows[i].customerId).focus();
				return;
			}
			if(arraySeq[i]> rows.length){
				ids+=custCode+" , ";
				if(selector==null) selector = $('#seq_' + rows[i].customerId);
			} 
		}
		if(ids.length>0){
			ids = ids.substring(0, ids.length-3);
			$('#errMsg').html(formatString(create_route_stt_ma_khong_lon_hon_sl_kh, ids)).show();
			selector.focus();
			return;
		}
		
		var checkSeq = false;
		var valueId;
		$('.seq').each(function(){
			var value = $(this).attr('value');
			if(isNaN(value)){
				if(checkSeq){
					return;
				}else{
					valueId = $(this).attr('id'); 
					$('#errMsg').html(create_route_stt_lon_hon_0).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);					
					}, 5000);
					checkSeq = true;
				}
			}
		});
		if(checkSeq){
			$('#' +valueId).focus();
			return;
		}
		
		var flag=false;
		var idSame = "";
		for(i=0; i<arraySeq.length-1; i++){
			for(j = i+1; j<arraySeq.length; j++){
				if(arraySeq[i] != '' && arraySeq[i]==arraySeq[j]){
					var custCode1 = rows[i].shortCode;
					var custCode2 = rows[j].shortCode;
					idSame =formatString(create_route_stt_code_duplicate_code, custCode1, custCode2);
					selector = $('#seq_' + rows[j].customerId);
					flag = true;
					break;
				}
			}
			if(flag) break;
		}
		if(idSame.length>0){
			$('#errMsg').html(idSame).show();
			selector.focus();
			return;
		}
		var saleDate =  $('#listDate').val();
		if(saleDate == -1) {
			$('#errMsg').html(create_route_sale_day_chua_chon).show();
			$('#listDate').focus();
			return;
		}
		var params =  new Object();
		var saleDate = $('#listDate').val();
		params.lstSeq = arraySeq;
		params.lstShortCode = arrShortCode;
		params.saleDate = saleDate;
		
		Utils.addOrSaveData(params, '/superviseshop/manageroute-create/order/update', null, 'errMsg', function(data){
		}, null,null,null,create_route_confirm_update_stt);
	},
//	UPDATE
//	SHOWMAP
	openMapCustomerRouting: function(){
		$('#errMsgMap').hide();
		$('#errMsg').html('').hide();
		var params = new Object();
		if(SuperviseManageRouteCreate._divMap!=undefined && SuperviseManageRouteCreate._divMap!=null && SuperviseManageRouteCreate._divMap.length > 100){
			$('#divMapRoute').html(SuperviseManageRouteCreate._divMap.toString()).show().change();
		}else{
			$('#divMapRoute').html('<div id="mapRoute" style="width:100%;height:424px;position: relative;" ></div>').show().change();			
		}
		params.routingId = $('#routingId').val();	
		params.saleDate = $('#listDate').val();
		params.shopCode = $('#shopCode').val();
		$('#divOverlay').show();
		$.ajax({
			type : 'POST',
			url : '/superviseshop/manageroute-create/order/search',
			data :($.param(params, true)),
			dataType : 'json',
			success : function(data) {
				SuperviseManageRouteSetOrder._listCustomer = new Array();
				if($.isArray(data.rows)) {
					for(var i = 0; i < data.rows.length; i++) {
//						var objCustomer = rows[i].customer;
						var obj = new Object();
						obj.customerId = data.rows[i].customerId;
						obj.shortCode = Utils.XSSEncode(data.rows[i].shortCode);
						obj.customerCode = Utils.XSSEncode(data.rows[i].shortCode);
						obj.customerName = Utils.XSSEncode(data.rows[i].customerName);
						obj.address = Utils.XSSEncode(data.rows[i].address);
						obj.lat = data.rows[i].lat;
						obj.lng = data.rows[i].lng;
						var saleDate = $('#listDate').val();
						if(saleDate == 1) obj.ttgt = data.rows[i].seq8;
						else if(saleDate == 2) obj.ttgt = data.rows[i].seq2;
						else if(saleDate == 3) obj.ttgt = data.rows[i].seq3;
						else if(saleDate == 4) obj.ttgt = data.rows[i].seq4;
						else if(saleDate == 5) obj.ttgt = data.rows[i].seq5;
						else if(saleDate == 6) obj.ttgt = data.rows[i].seq6;
						else if(saleDate == 7) obj.ttgt = data.rows[i].seq7;
						else obj.ttgt = '';
						if(obj.ttgt == null) obj.ttgt = '';
						SuperviseManageRouteSetOrder._listCustomer.push(obj);
					}
				}
				SuperviseManageRouteSetOrder.showMapCustomer(SuperviseManageRouteSetOrder._listCustomer);
				SuperviseManageRouteSetOrder.resize();
				$('#divOverlay').hide();
			}
		});
	},
	showMapCustomer:function(lstCustomer) {//tungmt
		//ViettelMap.clearOverlays();
		SuperviseManageRouteCreate._listMarker = new Array();
		for(i=0 ; i<lstCustomer.length ; i++){
			if(lstCustomer[i].lat!=null && lstCustomer[i].lat!=0 && lstCustomer[i].lat!=undefined && !isNaN(lstCustomer[i].lat) && lstCustomer[i].lng!=null && lstCustomer[i].lng!=0 && lstCustomer[i].lng!=undefined && !isNaN(lstCustomer[i].lng)){
				var flag=false;
				var kc=0;
				var listMerce=new Array();
				for(j=i+1; j<lstCustomer.length ; j++){
					kc=SuperviseManageRouteCreate.CalculateDistance(lstCustomer[i],lstCustomer[j]);					
					if(kc<100){
						flag=true;
						listMerce.push(lstCustomer[j]);
						lstCustomer.splice(j, 1);
						j--;
					}
				}
				if(flag==true){
					listMerce.push(lstCustomer[i]);//add them kh dang xet vao list
					lstCustomer[i].customerId = -lstCustomer[i].customerId;  // chuyen kh dang xet thanh kh dai dien
					lstCustomer[i].listMerce=listMerce;//gan danh sach kh trong list vao kh dai dien
					SuperviseManageRouteCreate._listMarker.push(lstCustomer[i]);
				}else{
					SuperviseManageRouteCreate._listMarker.push(lstCustomer[i]);
				}
			}
			else{
				lstCustomer.splice(i,1);
				i--;
			}
		}
		//add marker len ban do neu có
		if(SuperviseManageRouteCreate._listMarker.length>0){
			VTMapUtil.loadMapResource(function(){
				ViettelMap.loadBigMap('mapRoute',SuperviseManageRouteCreate._centerLat,SuperviseManageRouteCreate._centerLng,SuperviseManageRouteCreate._zoom,null);
				ViettelMap.addMutilMarkerStaffForSetOrder();
			});
		}
		else{
			VTMapUtil.loadMapResource(function(){
				ViettelMap.loadBigMap('mapRoute',0,0,SuperviseManageRouteCreate._zoom,null);		
			});
			$('#errMsgMap').show();
			var tm = setTimeout(function(){
				$('#errMsgMap').hide();
				clearTimeout(tm);					
			}, 5000);
		}
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manageroute-setorder.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manage-customer.js
 */
var SuperviseManageCustomer = {
	_xhrSave : null
};
/*
 * END OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manage-customer.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manage-saleplan.js
 */
var SuperviseManageSalePlan = {
	clearErrorMsg : function() {
		$('#errMsg').html('').hide();
	},
	getShopCodeByShopCodeAndName : function(input) {
		var value = $('#' + input).val().trim().split("-");
		return value[0];
	},
	getGridUrl : function(shopCode, staffSaleCode, monthPlan) {
		return "/superviseshop/manage-saleplan/getinfo?shopCode=" + shopCode + "&staffSaleCode=" + staffSaleCode + "&monthPlan=" + monthPlan;
	},
	getInfo : function() {
		SuperviseManageSalePlan.clearErrorMsg();
		msg = Utils.getMessageOfSpecialCharactersValidate('shopCodeAndName','Mã đơn vị',Utils._NAME);
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('staffSaleCodeAndName','Mã nhân viên bán hàng');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffSaleCodeAndName','Mã nhân viên bán hàng',Utils._NAME);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('monthPlan','Kế hoạch tháng');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('monthPlan','Kế hoạch tháng', Utils._DATE_MM_YYYY);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopCode = SuperviseManageSalePlan.getShopCodeByShopCodeAndName('shopCodeAndName');
		var staffSaleCode = SuperviseManageSalePlan.getShopCodeByShopCodeAndName('staffSaleCodeAndName');
		var monthPlan = $('#monthPlan').val().trim();
		var url = SuperviseManageSalePlan.getGridUrl(shopCode, staffSaleCode, monthPlan);
		$("#grid").setGridParam({
			url : url,
			page : 1,
			gridComplete: function() {
				$('#jqgh_grid_rn').html(jsp_common_numerical_order);
				updateRownumWidthForJqGrid();
				$('#shopCodeAndName').focus();
				$('#title').html('Danh sách khách hàng của NVBH ' + Utils.XSSEncode(staffSaleCode) +' trong tháng ' + monthPlan).show();
		    }
		}).trigger("reloadGrid");
		return false;
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-manage-saleplan.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-visit-result.js
 */
var VisitResult = {
	visitResultMap : null,
	clearErrorMsg : function() {
		$('#errMsg').html('').hide();
	},
	getGridUrl : function(shopCode, staffSaleCode, saleDate) {
		return "/superviseshop/visit-result/getinfo?shopCode=" + shopCode + "&staffSaleCode=" + staffSaleCode + "&saleDate=" + saleDate;
	},
	getInfo : function() {
		VisitResult.clearErrorMsg();
		msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffSaleCode','Mã nhân viên bán hàng',Utils._CODE);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('saleDate','Ngày BH');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('saleDate','Ngày BH', Utils._DATE_DD_MM_YYYY);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopCode = $('#shopCode').val().trim();
		var staffSaleCode = $('#staffSaleCode').val().trim();
		var saleDate = $('#saleDate').val().trim();	
		var url = VisitResult.getGridUrl(shopCode, staffSaleCode, saleDate);
		if(staffSaleCode != ''){
			$('#title').html('Kết quả ghé thăm của NVBH ' + Utils.XSSEncode(staffSaleCode)).show();
		} else {
			$('#title').html('Kết quả ghé thăm của NVBH ').show();
		}
		$("#grid").setGridParam({
			url : url,
			page : 1
		}).trigger("reloadGrid");
		return false;
	},
	showMap : function(staffCode) {
		var html = $('#viewMap').html();
		var shopCode = $('#shopCode').val().trim();
		var staffSaleCode = staffCode;
		var saleDate = $('#saleDate').val().trim();
		//var routeLst = new Array();
		$.fancybox(html, {
			modal : true,
			autoResize: false,	
			'title' : 'Bản đồ đầy đủ',
			'afterShow' : function() {	
				$('#viewMap').html('');
				var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
					$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top', document.documentElement.scrollTop + 10);
					$('#fancyboxMap').html($('#fancyboxMap').html() + "<div onclick='$.fancybox.close();' class='fancybox-item fancybox-close' title='Close'></div>");
					//load map
					var lat = null;
					var lng = null;
					var zoom=null;
					if(lat!='' && lng!=''){
						zoom=14;
					}
					MapUtil.loadMapResource(function(){
						Vietbando.visitResultMap = Vietbando.loadBigMapResult('bigMapContainer',lat,lng,zoom, function(point){
						});
						$.ajax({
							type : "POST",
							url : "/superviseshop/visit-result/getvisitresult",
							data : {staffSaleCode:staffSaleCode,shopCode:shopCode,saleDate:saleDate},
							dataType : "json",
							success : function(data) {
									if (data.success)
								    {
								     //add marker len ban do neu có
										if(data.visitResultLst.length>0 && data.visitResultLst[0].customerLat != null && data.visitResultLst[0].customerLng != null){
											var centerLat=data.visitResultLst[0].customerLat;
											var centerLng=data.visitResultLst[0].customerLng;
											var centerPt = new VLatLng(centerLat, centerLng);
											Vietbando.visitResultMap.setCenter(centerPt, zoom);
											Vietbando._listMarker=data.visitResultLst;
											var check = false;
											for(var i = 0; i < data.visitResultLst.length; i++) {
												var point = data.visitResultLst[i];
												if(Vietbando.isValidLatLng(point.customerLat, point.customerLng)) {
													var pt = new VLatLng(point.customerLat, point.customerLng);
													var image = null;
													var distance = null;
													if (data.visitResultLst[i].type == 4) {
														image = '/resources/images/lotrinh_kh_chuaghetham.png';
													} else if (data.visitResultLst[i].type == 1) {
														image = '/resources/images/lotrinh_kh_dangghetham.png';
													} else if (data.visitResultLst[i].type == 2) {
														image = '/resources/images/lotrinh_kh_kocodonhang.png';
													} else if (data.visitResultLst[i].type == 3)  {
														image = '/resources/images/lotrinh_kh_dc_chon.png';
													} else if (data.visitResultLst[i].type == 0) {
														image = '/resources/images/lotrinh_kh_ngoaituyen.png';
													}
													var mKOption = new VMarkerOptions(new VIcon(image, new VSize(30,30)),i+1, new VTextPosition(V_ANCHOR_BOTTOM_CENTER,new VSize(200,200)), 
															new VTextStyle(50,'bold','#ff0000','Arial'), data.visitResultLst[i].customerName, false,false);
													Vietbando._marker = new VMarker(pt, mKOption);
													Vietbando._marker.customerName = data.visitResultLst[i].customerName;
													var seqTxt= null;
													if (data.visitResultLst[i].type == 0) {
														seqTxt = '!';
													} else{
														if (data.visitResultLst[i].seq!=null) {
															seqTxt = data.visitResultLst[i].seq;
														} else {
															seqTxt = "";
														}
													}
													Vietbando._marker.seq = seqTxt;
													Vietbando._marker.address = data.visitResultLst[i].address;
													Vietbando._marker.mobiphone = data.visitResultLst[i].mobiphone;
													Vietbando._marker.phone = data.visitResultLst[i].phone;
													Vietbando.visitResultMap.addOverlay(Vietbando._marker);
													Vietbando.visitResultMap.addOverlay(new MyRouteOverlayCustomer(pt,seqTxt));
													
													if (data.visitResultLst[i].type != 0) {
														if (point.staffLat != null && point.staffLng != null) {
															if (Vietbando.isValidLatLng(point.staffLat, point.staffLng)) {
																var staffPt = new VLatLng(point.staffLat, point.staffLng);
																var staffImage = null;
																distance = Vietbando.CalculateDistance(staffPt,pt);
																var distanceTxt = distance + 'm';
																if(distance>= 1000){
																	distanceTxt = Vietbando.CalculateDistanceFormat(distance)+'km';
																}
																if (distance > data.visitResultLst[i].distanceOrder) {
																	staffImage = '/resources/images/staff-flag-red.png';
																} else {
																	staffImage = '/resources/images/staff-flag-blue.png';
																} 
																var mKOptionStaff = new VMarkerOptions(new VIcon(staffImage, new VSize(40,40)),i+1, new VTextPosition(V_ANCHOR_BOTTOM_CENTER,new VSize(200,200)), 
																		new VTextStyle(50,'bold','#ff0000','Arial'), null, false,false);
																Vietbando._marker = new VMarker(staffPt, mKOptionStaff);
																Vietbando._marker.isCustomer = false;
																Vietbando.visitResultMap.addOverlay(Vietbando._marker);
																
																Vietbando.visitResultMap.addOverlay(new MyDistanceOverlay(new VLatLng(point.staffLat, point.staffLng), distanceTxt));
															}
															if (data.visitResultLst[i].seq!=null) {
																Vietbando.visitResultMap.addOverlay(new MyRouteOverlay(new VLatLng(point.staffLat, point.staffLng), data.visitResultLst[i].seq));
															} else{
																Vietbando.visitResultMap.addOverlay(new MyRouteOverlay(new VLatLng(point.staffLat, point.staffLng), ""));
															}
														}
													}
													if ( check == false && point.lastStaffLat != null && point.lastStaffLng != null) {
														if (Vietbando.isValidLatLng(point.lastStaffLat, point.lastStaffLat)) {
															var staffLastPt = new VLatLng(point.lastStaffLat, point.lastStaffLng);
															var staffLastImage = '/resources/images/staff_location.png';
															var mKOptionStaffLast = new VMarkerOptions(new VIcon(staffLastImage, new VSize(24,24)),i+1, new VTextPosition(V_ANCHOR_BOTTOM_CENTER,new VSize(200,200)), 
																	new VTextStyle(50,'bold','#ff0000','Arial'), null, false,false);
															Vietbando._marker = new VMarker(staffLastPt, mKOptionStaffLast);
															Vietbando._marker.isCustomer = false;
															Vietbando.visitResultMap.addOverlay(Vietbando._marker);
															check = true;
														}
													}
												} 
											}
										}
								    }
							},
							error:function(XMLHttpRequest, textStatus, errorThrown) {
							}
						});
					});
					clearTimeout(tm);
				}, 500);
			},
			'afterClose' : function() {
				$('#viewMap').html(html);
			}
		});
		return false;
	}
};
var SellerPosition = {
		//sellerPositionMap : null,
		clearMsg : function() {
			$('#errMsg').html('').hide();
			$('#successMsg').html('').hide();
		},
		getGridUrl : function(shopCode,staffType,allLevel,staffCode) {
			return "/superviseshop/visit-result/searchsellerposition?shopCode=" + shopCode + "&staffType=" + staffType + "&allLevel=" + allLevel + "&staffCode=" + staffCode;
		},
		searchSellerPosition: function() {
			shopCode = $('#shopCode').val().trim();
			staffType = $('#staffType').val().trim();
			staffCode = $('#staffCode').val().trim();
			allLevel = $('#allLevel').attr('checked');
			if(allLevel=="checked"){
				allLevel = true;
			}else{
				allLevel = false;
			}
			SellerPosition.clearMsg();
			if(Vietbando.sellerPositionMap!=null){
				Vietbando.sellerPositionMap.clearOverlays();
				Vietbando._map.closeInfoWindow();
			}
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
			if(msg.length ==0){
				msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var url = SellerPosition.getGridUrl(shopCode,staffType,allLevel,staffCode);
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
			return false;
		},
		
		getSellerPositionDetail : function(staffId,staffCode,staffName,shopCode,objectType) {
			showLoading('searchLoading');
			SellerPosition.clearMsg();
			if(Vietbando.sellerPositionMap!=null){
				Vietbando.sellerPositionMap.clearOverlays();
				Vietbando._map.closeInfoWindow();
			}
			
			$('#grid tr').each(function(){
				if($(this).hasClass('ui-state-salepos')){
					$(this).removeClass('ui-state-salepos');
				}
			});
			
			$('#grid tr').each(function(){
				if($(this).attr('id')==staffId){
					$(this).addClass('ui-state-salepos');
				}
			});
			
			$.ajax({
				type : "POST",
				url : "/superviseshop/visit-result/getsellerpositiondetail",
				data : {staffId:staffId,staffCode:staffCode,objectType:objectType},
				dataType : "json",
				success : function(data) {
						if (data.success && data.mainInfo!=null && data.mainInfo.length>0){
							hideLoading('searchLoading');
							if(objectType == 7 ){//case TBHV
								var mainInfo = new TBHVMapInfo();
								var item = data.mainInfo[0];
								mainInfo.staffCode = staffCode;
								mainInfo.staffName = staffName;
								mainInfo.shopCode = shopCode;
								var firstSubItem = true;
								//lay danh sach GSNPP di cung
								mainInfo.GSNPPStr = "";
								for(var i=0;i<data.mainInfo.length;i++){
									var subItem = data.mainInfo[i];
									if(subItem.staffCode!=null && subItem.staffCode!=undefined && subItem.staffCode!=''){
										if(firstSubItem){
											mainInfo.GSNPPStr = Utils.XSSEncode(subItem.staffCode);//GSNPP
											firstSubItem = false;
										}else{
											mainInfo.GSNPPStr = Utils.XSSEncode(mainInfo.GSNPPStr + ", " + subItem.staffCode);//GSNPP
										}
									}
								}
								mainInfo.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
								mainInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
								mainInfo.lat = item.lat;
								mainInfo.lng = item.lng;
								mainInfo.imgUrl = '/resources/images/quanlyvung.png';
								
								//child info
								if(data.childInfo!=null && data.childInfo.length>0){//GSNPP
									var childInfoArr = new Array();
									for(var i=0;i<data.childInfo.length;i++){
										var item = data.childInfo[i];
										var childInfo = new GSNPPMapInfo();
										childInfo.staffCode = (item.staffCode!=null && item.staffCode!=undefined)?item.staffCode:"";
										childInfo.staffId = item.staffId;
										childInfo.staffName = (item.staffName!=null && item.staffName!=undefined)?item.staffName:"";
										childInfo.shopCode = shopCode;
										childInfo.NVBHStr = "";//get later by ajax
										childInfo.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
										childInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
										childInfo.lat = item.lat;
										childInfo.lng = item.lng;
										childInfo.imgUrl = '/resources/images/giamsat.png';
										childInfoArr.push(childInfo);
									}
								}
							}else if(objectType == 5){//case GSNPP
								var mainInfo = new GSNPPMapInfo();
								var item = data.mainInfo[0];
								mainInfo.staffCode = staffCode;
								mainInfo.staffId = staffId;
								mainInfo.staffName = staffName;
								mainInfo.shopCode = shopCode;
								//lay danh sach NVBH di cung
								var firstSubItem = true;
								mainInfo.NVBHStr = "";
								for(var i=0;i<data.mainInfo.length;i++){
									var subItem = data.mainInfo[i];
									if(subItem.staffCode!=null && subItem.staffCode!=undefined && subItem.staffCode!=''){
										if(firstSubItem){
											mainInfo.NVBHStr = subItem.staffCode;//GSNPP
											firstSubItem = false;
										}else{
											mainInfo.NVBHStr = Utils.XSSEncode(mainInfo.NVBHStr + ", " + subItem.staffCode);//GSNPP
										}
									}
								}
								mainInfo.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
								mainInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
								mainInfo.lat = item.lat;
								mainInfo.lng = item.lng;
								mainInfo.imgUrl = '/resources/images/giamsat.png';
								
								//child info
								if(data.childInfo!=null && data.childInfo.length>0){//NVBH
									var childInfoArr = new Array();
									for(var i=0;i<data.childInfo.length;i++){
										var item = data.childInfo[i];
										var childInfo = new NVBHMapInfo();
										childInfo.staffCode = (item.staffCode!=null && item.staffCode!=undefined)?item.staffCode:"";
										childInfo.staffName = (item.staffName!=null && item.staffName!=undefined)?item.staffName:"";
										childInfo.staffId = (item.staffId!=null && item.staffId!=undefined)?item.staffId:"";
										childInfo.shopCode = shopCode;
										//get later by ajax
										childInfo.revenue = "";
										childInfo.channelPoint = "";
										childInfo.revenuePoint = "";
										childInfo.lastPosTime = "";
										childInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
										childInfo.lat = item.lat;
										childInfo.lng = item.lng;
										childInfo.imgUrl = '/resources/images/nhanvien.png';
										childInfoArr.push(childInfo);
									}
								}
								
							}else if(objectType == 1){//case NVBH
								var mainInfo = new TBHVMapInfo();
								var item = data.mainInfo[0];
								mainInfo.staffCode = staffCode;
								mainInfo.staffName = staffName;
								mainInfo.staffId = (item.staffId!=null && item.staffId!=undefined)?item.staffId:"";
								mainInfo.shopCode = shopCode;
								//get later by ajax
								mainInfo.revenue = "";
								mainInfo.channelPoint = "";
								mainInfo.revenuePoint = "";
								mainInfo.lastPosTime = "";
								mainInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
								mainInfo.lat = item.lat;
								mainInfo.lng = item.lng;
								mainInfo.imgUrl = '/resources/images/nhanvien.png';
								
								//child info
								if(data.childInfo!=null && data.childInfo.length>0){//Customer
									var childInfoArr = new Array();
									for(var i=0;i<data.childInfo.length;i++){
										var item = data.childInfo[i];
										var childInfo = new NVBHMapInfo();
										childInfo.customerCode = (item.shortCode!=null && item.shortCode!=undefined)?item.shortCode:"";
										childInfo.customerName = (item.customerName!=null && item.customerName!=undefined)?item.customerName:"";
										childInfo.lat = item.customerLat;
										childInfo.lng = item.customerLng;
										childInfo.imgUrl = '/resources/images/supermarket_red.png';
										childInfoArr.push(childInfo);
									}
								}
							}
							
							//draw main info to map
							if(mainInfo!=null && mainInfo.lat!=null && mainInfo.lng!=null && Vietbando.isValidLatLng(mainInfo.lat, mainInfo.lng)) {
								var pt = new VLatLng(mainInfo.lat, mainInfo.lng);
								var mKOption = new VMarkerOptions(new VIcon(mainInfo.imgUrl,new VSize(21,24)),"", null, null, 
										mainInfo.staffName, false,false);
								Vietbando._marker = new VMarker(pt, mKOption);
								
								//set attribute for main maker
								Vietbando._marker.staffCode = staffCode;
								Vietbando._marker.staffName = mainInfo.staffName;
								Vietbando._marker.shopCode = mainInfo.shopCode;
								if(objectType==7){//TBHV
									Vietbando._marker.GSNPPStr = mainInfo.GSNPPStr;
								}else if(objectType==5){//GSNPP
									Vietbando._marker.staffId = mainInfo.staffId;
									Vietbando._marker.NVBHStr = mainInfo.NVBHStr;
								}else if(objectType==1){//NVBH
									Vietbando._marker.staffId = mainInfo.staffId;
								}
								Vietbando._marker.lastPosTime = mainInfo.lastPosTime;
								Vietbando._marker.accuracy = mainInfo.accuracy;
								Vietbando._marker.objectType = objectType;
								Vietbando.sellerPositionMap.addOverlay(Vietbando._marker);
								//mo popup cho item duoc chon
								if (objectType == 5 || objectType == 7) {//GSNPP OR TBHV
									if(objectType == 5){
										var html =String.format(Vietbando.GSNPPInfoContent,mainInfo.staffCode,mainInfo.staffName,mainInfo.shopCode,mainInfo.NVBHStr,mainInfo.lastPosTime,mainInfo.accuracy);
									}else{
										var html =String.format(Vietbando.TBHVInfoContent,Vietbando._marker.staffCode,Vietbando._marker.staffName,Vietbando._marker.shopCode,Vietbando._marker.GSNPPStr,Vietbando._marker.lastPosTime,Vietbando._marker.accuracy);
									}
									Vietbando.sellerPositionMap.openInfoWindow(pt,html);
									
									//draw child info to map
									if(childInfoArr!=null && childInfoArr.length>0){
										for(var k = 0; k < childInfoArr.length; k++) {
											var childInfo = childInfoArr[k];
											if(childInfo.lat!=null && childInfo.lng!=null && Vietbando.isValidLatLng(childInfo.lat, childInfo.lng)) {
												var pt = new VLatLng(childInfo.lat, childInfo.lng);
												var mKOption = new VMarkerOptions(new VIcon(childInfo.imgUrl,new VSize(21,24)),"", null, null, 
														childInfo.staffName, false,false);
												Vietbando._marker = new VMarker(pt, mKOption);
												
												//set attribute for child maker
												Vietbando._marker.staffCode = childInfo.staffCode;
												Vietbando._marker.staffName = childInfo.staffName;
												Vietbando._marker.shopCode = childInfo.shopCode;
												Vietbando._marker.lastPosTime = childInfo.lastPosTime;
												Vietbando._marker.accuracy = childInfo.accuracy;
												if(objectType==7){//TBHV
													Vietbando._marker.objectType = 5;
													Vietbando._marker.staffId = childInfo.staffId;
													Vietbando._marker.NVBHStr = childInfo.NVBHStr;
												}else if(objectType==5){//GSNPP
													Vietbando._marker.objectType = 1;
													Vietbando._marker.staffId = childInfo.staffId;
												}
												Vietbando.sellerPositionMap.addOverlay(Vietbando._marker);
											}
										}
									}
								}else if (objectType == 1) {//NVBH
									$.ajax({
										type : "POST",
										url : "/superviseshop/visit-result/getNVBHdetail",
										data : {staffId:mainInfo.staffId,objectType:objectType},
										dataType : "json",
										success : function(data) {
											if (data.success && data.mainInfo!=null && data.mainInfo.length>0)
										    {
												var item = data.mainInfo[0];
												mainInfo.revenue = ((item.amount!=null && item.amount!=undefined)?item.amount:"") + ((item.plan!=null && item.plan!=undefined)?("/"+item.plan):"");
												mainInfo.channelPoint = (item.pointInRouting!=null && item.pointInRouting!=undefined)?item.pointInRouting:"";
												mainInfo.revenuePoint = (item.pointAmount!=null && item.pointAmount!=undefined)?item.pointAmount:"";
												mainInfo.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
												var html =String.format(Vietbando.NVBHInfoContent,mainInfo.staffCode,mainInfo.staffName,mainInfo.shopCode,mainInfo.revenue,mainInfo.channelPoint,mainInfo.revenuePoint,mainInfo.lastPosTime,mainInfo.accuracy);
												var pt = new VLatLng(mainInfo.lat, mainInfo.lng);
												Vietbando.sellerPositionMap.openInfoWindow(pt,html);
												
												//draw child info to map (Customer)
												if(childInfoArr!=null && childInfoArr.length>0){
													for(var i = 0; i < childInfoArr.length; i++) {
														var childInfo = childInfoArr[i];
														if(childInfo.lat!=null && childInfo.lng!=null && Vietbando.isValidLatLng(childInfo.lat, childInfo.lng)) {
															var pt = new VLatLng(childInfo.lat, childInfo.lng);
															var mKOption = new VMarkerOptions(new VIcon(childInfo.imgUrl,new VSize(21,24)),"", null, null, 
																	childInfo.customerName, false,false);
															Vietbando._marker = new VMarker(pt, mKOption);
															//set attribute for child maker
															Vietbando._marker.objectType = -1;
															Vietbando._marker.staffCode = childInfo.customerCode;
															Vietbando._marker.staffName = childInfo.customerName;
															Vietbando.sellerPositionMap.addOverlay(Vietbando._marker);
														}
													}
												}
										    }
										},
										error:function(XMLHttpRequest, textStatus, errorThrown) {
										}
									});
								}
							}
					    }else if(data.success && data.mainInfo==null || data.mainInfo.length==0){
					    	hideLoading('searchLoading');
					    	$('#successMsg').html('Không có dữ liệu vị trí NVBH').show();
					    }else if(data.error){
					    	hideLoading('searchLoading');
					    	$('#errMsg').html('Lỗi trong quá trình xử lý').show();
					    }
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					hideLoading('searchLoading');
				}
			});
			return false;
		},
		loadMap : function(staffId,hasPosition) {
			$('#sellerPosMapContainer').html('');
			zoom = 14;
			var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
				MapUtil.loadMapResource(function(){
					Vietbando.loadMap('sellerPosMapContainer',null,null,zoom, function(){
						//call back function here
						var centerPt = new VLatLng(10.79659063765061, 106.66422128677368);
						Vietbando.sellerPositionMap.setCenter(centerPt, zoom);
						//get staff info
						if(hasPosition=='1'){
							$.ajax({
								type : "POST",
								url : "/superviseshop/visit-result/getstaffinfo",
								data : {staffId:staffId},
								dataType : "json",
								success : function(data) {
									if (data.success)
									{
										SellerPosition.getSellerPositionDetail(staffId,data.staffCode,data.staffName,data.shopCode,data.objectType);
									}
								},
								error:function(XMLHttpRequest, textStatus, errorThrown) {
									
								}
							});
						}
					},true);
				});
				clearTimeout(tm);
			}, 500);
			return false;
		}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.superviseshop-visit-result.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.trainingplan.js
 */
var trainingPlan = {
	mapTrainingPlan : null,
	length : 0 ,
	runner : -1,
	totalRow : 0,
	lstId : null,
	isImportSuccess : false,
	getGridUrl: function(shopId,gsnppId,dateStr){
		return "/plan/search?shopId=" + encodeChar(shopId) + "&gsnppId=" + encodeChar(gsnppId) + "&dateStr=" + dateStr;
	},
	onCheckAllChange: function(checkAll, className) {
		var rows = $('#grid').datagrid('getRows');
		if(checkAll) {
			$('.' + className).each(function() {
				this.checked = true;
				var rowIndex = $(this).val();
				var rowData = rows[rowIndex];
	    		var selectedId = rowData['id'];
	    		trainingPlan.mapTrainingPlan.put(selectedId, rowData);
			});
		} else {
			$('.'+className).each(function() {
				this.checked = false;
				var rowIndex = $(this).val();
				var rowData = rows[rowIndex];
	    		var selectedId = rowData['id'];
	    		trainingPlan.mapTrainingPlan.remove(selectedId, rowData);
			});
		}
	},
	onCheckboxChangeForCheckAll: function(isCheck,rowIndex, className, checkAllId) {
		var rows = $('#grid').datagrid('getRows');
		if(isCheck == null || isCheck == undefined) {
			return;
		} 
		if(isCheck) {
			// kiem tra xem co phai check all ko?
			var checkAll = true;
			$('.' + className).each(function() {
				if(this.checked == false) {
					checkAll = false;
				}
			});
			if(checkAll == true) {
				$('#' + checkAllId).attr('checked', 'checked');
			}
			var rowData =rows[rowIndex];
 	    	var selectedId = rowData['id'];
 	    	$('#cb_main').attr('checked',false);
 	    	trainingPlan.mapTrainingPlan.put(selectedId, rowData);
		} else {
			// bo check All
			var rowData =rows[rowIndex];
			var selectedId = rowData['id'];
 	    	trainingPlan.mapTrainingPlan.remove(selectedId);
			$('#' + checkAllId).removeAttr('checked');
		}
	},
	search : function(){
		$('#errMsg').html('').hide();		
		trainingPlan.length ++;
		var shopId = $('#shop').combotree('getValue');
		var msg="";
		if (shopId == ""){
			$('#errMsg').html('Bạn chưa chọn giá trị cho trường đơn vị.').show();
			return;
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('date', 'Tháng', false);
		}
		if (msg.length == 0){
			var date = '01/'+$('#date').val().trim();
			if(!Utils.isDate(date)){
				$('#date').focus();			
				msg = 'Tháng không tồn tại hoặc không đúng định dạng mm/yyyy.';
			}
		}
		if (msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var gsnppId=-1;
		if($('#gsnpp').val() != undefined && $('#gsnpp').val() != null && $('#gsnpp').val().length>0) gsnppId = $('#gsnpp').val().trim();
		var date = $('#date').val().trim();
		$('#dv').val(shopId);
		$('#nv').val(gsnppId);
		$('#th').val(date);
		var url = trainingPlan.getGridUrl(shopId,gsnppId,date);
		var wd = $(window).width() - 30; 
		$('#cb_main').attr('checked',false);
		trainingPlan.mapTrainingPlan = new Map();
		var checkAll = '<input type="checkbox" id="checkAll" onchange="trainingPlan.onCheckAllChange(this.checked ,\'CheckOrder\')"/>';
		 $('#grid').datagrid({  
	        url: url,  
	        selectOnCheck: false,
	        checkOnSelect : false,
	        singleSelect : true,
	        pagination : true,
	        rownumbers : true,
	        pageSize: 20,
	        pageNumber : 1,
	        scrollbarSize:0,
	        autoWidth: true,
	        autoRowHeight : true,
	        fitColumns : true,
	        scrollbarSize:0,
	        width : wd, 
	        columns:[[  
	            {field:'codeNPP',title:'Nhà phân phối',width:50,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	            {field:'nameGSNPP',title:'GSNPP',width:150,align:'left',sortable : false,resizable : false, formatter:function(value,row,index){
	            	var str = Utils.XSSEncode(row.codeGSNPP) +"-"+ Utils.XSSEncode(row.nameGSNPP);
	            	return str;
	            }},  
	            {field:'nameNVBH',title:'Nhân viên BH',width:150,align:'left',sortable : false,resizable : false, formatter:function(value,row,index){
	            	var str = Utils.XSSEncode(row.codeNVBH) +"-"+ Utils.XSSEncode(row.nameNVBH);
	            	return str;
	            }},
	            {field:'dateStr',title:'Ngày',width:60,align:'center',sortable : false,resizable : false,formatter:function(row,index){
	            		trainingPlan.totalRow ++;
	            		return Utils.XSSEncode(row);
	            	}
	            },
	            {field: 'id', title: checkAll, align:'center',sortable : false,resizable : false ,formatter:function(value,rowData,rowIndex){
	              	if(trainingPlan.compareCurrentDate(rowData.dateStr)) {
	              		return '';
	              	} else {
	              		if(trainingPlan.mapTrainingPlan.get(value) != undefined && trainingPlan.mapTrainingPlan.get(value) != null) 
	              			return '<input type="checkbox" checked=\'checked\' class="CheckOrder" value="'+rowIndex+'" onchange="trainingPlan.onCheckboxChangeForCheckAll(this.checked ,this.value,\'CheckOrder\',\'checkAll\')"/>';
	              		else 
	              			return '<input type="checkbox" class="CheckOrder" value="'+rowIndex+'" onchange="trainingPlan.onCheckboxChangeForCheckAll(this.checked ,this.value,\'CheckOrder\',\'checkAll\')"/>';
					}
	        	}},
	            {field:'P',hidden:true}
	            ]],
	        onLoadSuccess :function(data){
	        	trainingPlan.totalRow = data.total;
	        	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	        	updateRownumWidthForJqGrid('');
	        	if(data.soNPP != undefined && data.soNPP != null && data.soNPP > 0) {
	        		var str = 'Chọn toàn bộ '+ data.soNPP + ' NPP thuộc '+ Utils.XSSEncode(data.shopName);
		        	$('#strHidden').html(str);
		        	$('#hideItem').show();
	        		$('#hideItem').removeAttr('disabled');
	        	} else {
	        		$('#hideItem').hide();
	        		$('#hideItem').attr('disabled','disabled');
	        	}
	        	if(data!= undefined && data.total != undefined && data.total <=20){
	        		$('#hideItem').hide();
	        		$('#hideItem').attr('disabled','disabled');
	        	}
	        	var checkAll = true;
    	    	var runner = 0;
    			$('.CheckOrder').each(function() {
    				if(this.checked == false) {
    					checkAll = false;
    				}
    				runner ++; 
    			});
    			if(checkAll == true&& runner >0) {
    				$('#checkAll').attr('checked', 'checked');
    			} else {
    				$('#checkAll').attr('checked', false);
    			}
		    	$('.datagrid-header-check input').bind('click',function(){
		    	    if($(this).is(':checked')){
		    	    	$('#cb_main').attr('checked',false);
		    		}		    			
		    	});  
		    	$('#cb_main').unbind('click');
		    	$('#cb_main').bind('click',function(){
		    		if($(this).is(':checked')){
		    			trainingPlan.mapTrainingPlan.clear();
		    		}
		    	});
	        }
		}); 
		 $('#cb_main').change(function(){		 
			 if($(this).is(':checked')){
				 $('.CheckOrder').each(function() {
						this.checked = false;
				 });
				 $('#checkAll').prop('checked', false);
				 trainingPlan.mapTrainingPlan.clear(); 
			 }		 
		 });
	},
	importExcel : function(){
		//mo fancybox
		$('#errMsg').html('').hide();
		$('#easyuiPopup').show();
		$('#errExcelMsg').html('').hide();
		$('#resultExcelMsg').html('').hide();
		$('#fakefilepc').val('');
		ReportUtils._callBackAfterImport = function(){
			$('#cb_main').attr('checked',false);
			if (trainingPlan.isImportSuccess){
				 $('.easyui-dialog #btnClose').unbind('click');
	   			 $('.easyui-dialog #btnClose').bind('click',function(){
	   				 $('#grid').datagrid('reload');
	   				 $('#easyuiPopup').dialog('close');
	   			 });
	   		 }else{
	   			$('.easyui-dialog #btnClose').unbind('click');
	   			 $('.easyui-dialog #btnClose').bind('click',function(){
	   				 $('#easyuiPopup').dialog('close');
	   			 });
	   		 }
			$('#grid').datagrid('reload');
			ReportUtils._callBackAfterImport=null;
		};
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
	        }
		});
		
	},
	/** Tulv2 Sep 30, 2014, import file excel*/
	upload: function(){
 		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$("#errExcelMsg").html(data.message).show();
			}else{
				$('#resultExcelMsg').html("Lưu dữ liệu thành công").show();
				$('#easyuiPopup').window('close');
				trainingPlan.search();
				setTimeout(function(){
					$('#resultExcelMsg').html("").hide();
				 }, 1500);
			}
		}, "importFrm", "excelFile", null, "errExcelMsg");
	},
	exportExcel : function(){
		$('#errMsg').html('').hide();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){	
				var dataModel = new Object();		
				dataModel.shopId = $('#dv').val();
				dataModel.gsnppId = $('#nv').val();
				dataModel.dateStr = $('#th').val();
				ReportUtils.exportReport('/plan/exportExcel',dataModel,'errMsg');									
			}			
		});	
 		return false;

	},
	delPlan : function(){
		$('#errMsg').html('').hide();
		$('#successMsg').html('').hide();
		trainingPlan.lstId = new Array();
		trainingPlan.lstId = trainingPlan.mapTrainingPlan.keyArray;
	  
		var inputDate = $('#th').val().trim();
		if (!Utils.compareCurrentMonthEx(inputDate)){
			$('#errMsg').html('Không được xoá lịch huấn luyện của tháng trước tháng hiện tại.').show();
			return false;
		} 
		if (trainingPlan.totalRow == 0){
			$('#errMsg').html('Không có dữ liệu để xoá.').show();
			return false;
		}else if ( (trainingPlan.lstId ==null || trainingPlan.lstId.length==0) && $('#cb_main').is(':checked')==false){ //grid rong hoac chua chon
			$('#errMsg').html('Hãy chọn dữ liệu để xoá.').show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstDelete =  trainingPlan.lstId;
		if($('#cb_main').is(':checked')) dataModel.pressCheckbox = true;
		else dataModel.pressCheckbox = false;

		var shopId = $('#shop').combotree('getValue');
		var gsnppId = $('#gsnpp').val().trim();
		var date = $('#date').val().trim();
		dataModel.shopId = shopId;
		dataModel.gsnppId = gsnppId;
		dataModel.dateStr = date;
		
		var  msg =  'Bạn có muốn xoá không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				Utils.addOrSaveData(dataModel, '/plan/delplan' , null, 'errMsg', function(data){
					$('#cb_main').attr('checked',false);
					$('#grid').datagrid('load', {page : 1});
					trainingPlan.mapTrainingPlan.clear();
				}, null, null, true,msg,function(dataErr){
					if(dataErr.hastraining) {
						trainingPlan.mapTrainingPlan.clear();
						$('#cb_main').attr('checked',false);
						trainingPlan.mapTrainingPlanCanNotDelete = new Map();
						trainingPlan.mapTrainingPlan.clear();
						$('#errMsg').html(dataErr.errMsg).show();
						if(!dataErr.deleteAll) {
							if (dataErr.lst!=null && dataErr.lst.length > 0){
								for(var i = 0 ; i < dataErr.lst.length; ++i) {
									trainingPlan.mapTrainingPlanCanNotDelete.put(Number(dataErr.lst[i]),i);
									trainingPlan.mapTrainingPlan.put(Number(dataErr.lst[i]),i);
								}
							}
						}
						
					}
				});
			}
		});	
	},
	compareCurrentDate:function(date) {
		//date < now => true || date > now => false
		if(date.length == 0) {
			return false;
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		return trainingPlan.compareDate(date, currentDate);
	},
	compareDate: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		if(dates.compare(startDateObj,endDateObj) >= 0){
			return false;
		}
		return true;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/superviseshop/vnm.trainingplan.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

var trainingPlan = {
	mapTrainingPlan : null,
	length : 0 ,
	runner : -1,
	totalRow : 0,
	lstId : null,
	isImportSuccess : false,
	getGridUrl: function(shopId,gsnppId,dateStr){
		return "/plan/search?shopId=" + encodeChar(shopId) + "&gsnppId=" + encodeChar(gsnppId) + "&dateStr=" + dateStr;
	},
	onCheckAllChange: function(checkAll, className) {
		var rows = $('#grid').datagrid('getRows');
		if(checkAll) {
			$('.' + className).each(function() {
				this.checked = true;
				var rowIndex = $(this).val();
				var rowData = rows[rowIndex];
	    		var selectedId = rowData['id'];
	    		trainingPlan.mapTrainingPlan.put(selectedId, rowData);
			});
		} else {
			$('.'+className).each(function() {
				this.checked = false;
				var rowIndex = $(this).val();
				var rowData = rows[rowIndex];
	    		var selectedId = rowData['id'];
	    		trainingPlan.mapTrainingPlan.remove(selectedId, rowData);
			});
		}
	},
	onCheckboxChangeForCheckAll: function(isCheck,rowIndex, className, checkAllId) {
		var rows = $('#grid').datagrid('getRows');
		if(isCheck == null || isCheck == undefined) {
			return;
		} 
		if(isCheck) {
			// kiem tra xem co phai check all ko?
			var checkAll = true;
			$('.' + className).each(function() {
				if(this.checked == false) {
					checkAll = false;
				}
			});
			if(checkAll == true) {
				$('#' + checkAllId).attr('checked', 'checked');
			}
			var rowData =rows[rowIndex];
 	    	var selectedId = rowData['id'];
 	    	$('#cb_main').attr('checked',false);
 	    	trainingPlan.mapTrainingPlan.put(selectedId, rowData);
		} else {
			// bo check All
			var rowData =rows[rowIndex];
			var selectedId = rowData['id'];
 	    	trainingPlan.mapTrainingPlan.remove(selectedId);
			$('#' + checkAllId).removeAttr('checked');
		}
	},
	search : function(){
		$('#errMsg').html('').hide();		
		trainingPlan.length ++;
		var shopId = $('#shop').combotree('getValue');
		var msg="";
		if (shopId == ""){
			$('#errMsg').html('Bạn chưa chọn giá trị cho trường đơn vị.').show();
			return;
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('date', 'Tháng', false);
		}
		if (msg.length == 0){
			var date = '01/'+$('#date').val().trim();
			if(!Utils.isDate(date)){
				$('#date').focus();			
				msg = 'Tháng không tồn tại hoặc không đúng định dạng mm/yyyy.';
			}
		}
		if (msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var gsnppId=-1;
		if($('#gsnpp').val() != undefined && $('#gsnpp').val() != null && $('#gsnpp').val().length>0) gsnppId = $('#gsnpp').val().trim();
		var date = $('#date').val().trim();
		$('#dv').val(shopId);
		$('#nv').val(gsnppId);
		$('#th').val(date);
		var url = trainingPlan.getGridUrl(shopId,gsnppId,date);
		var wd = $(window).width() - 30; 
		$('#cb_main').attr('checked',false);
		trainingPlan.mapTrainingPlan = new Map();
		var checkAll = '<input type="checkbox" id="checkAll" onchange="trainingPlan.onCheckAllChange(this.checked ,\'CheckOrder\')"/>';
		 $('#grid').datagrid({  
	        url: url,  
	        selectOnCheck: false,
	        checkOnSelect : false,
	        singleSelect : true,
	        pagination : true,
	        rownumbers : true,
	        pageSize: 20,
	        pageNumber : 1,
	        scrollbarSize:0,
	        autoWidth: true,
	        autoRowHeight : true,
	        fitColumns : true,
	        scrollbarSize:0,
	        width : wd, 
	        columns:[[  
	            {field:'codeNPP',title:'Nhà phân phối',width:50,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	            {field:'nameGSNPP',title:'GSNPP',width:150,align:'left',sortable : false,resizable : false, formatter:function(value,row,index){
	            	var str = Utils.XSSEncode(row.codeGSNPP) +"-"+ Utils.XSSEncode(row.nameGSNPP);
	            	return str;
	            }},  
	            {field:'nameNVBH',title:'Nhân viên BH',width:150,align:'left',sortable : false,resizable : false, formatter:function(value,row,index){
	            	var str = Utils.XSSEncode(row.codeNVBH) +"-"+ Utils.XSSEncode(row.nameNVBH);
	            	return str;
	            }},
	            {field:'dateStr',title:'Ngày',width:60,align:'center',sortable : false,resizable : false,formatter:function(row,index){
	            		trainingPlan.totalRow ++;
	            		return Utils.XSSEncode(row);
	            	}
	            },
	            {field: 'id', title: checkAll, align:'center',sortable : false,resizable : false ,formatter:function(value,rowData,rowIndex){
	              	if(trainingPlan.compareCurrentDate(rowData.dateStr)) {
	              		return '';
	              	} else {
	              		if(trainingPlan.mapTrainingPlan.get(value) != undefined && trainingPlan.mapTrainingPlan.get(value) != null) 
	              			return '<input type="checkbox" checked=\'checked\' class="CheckOrder" value="'+rowIndex+'" onchange="trainingPlan.onCheckboxChangeForCheckAll(this.checked ,this.value,\'CheckOrder\',\'checkAll\')"/>';
	              		else 
	              			return '<input type="checkbox" class="CheckOrder" value="'+rowIndex+'" onchange="trainingPlan.onCheckboxChangeForCheckAll(this.checked ,this.value,\'CheckOrder\',\'checkAll\')"/>';
					}
	        	}},
	            {field:'P',hidden:true}
	            ]],
	        onLoadSuccess :function(data){
	        	trainingPlan.totalRow = data.total;
	        	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	        	updateRownumWidthForJqGrid('');
	        	if(data.soNPP != undefined && data.soNPP != null && data.soNPP > 0) {
	        		var str = 'Chọn toàn bộ '+ data.soNPP + ' NPP thuộc '+ Utils.XSSEncode(data.shopName);
		        	$('#strHidden').html(str);
		        	$('#hideItem').show();
	        		$('#hideItem').removeAttr('disabled');
	        	} else {
	        		$('#hideItem').hide();
	        		$('#hideItem').attr('disabled','disabled');
	        	}
	        	if(data!= undefined && data.total != undefined && data.total <=20){
	        		$('#hideItem').hide();
	        		$('#hideItem').attr('disabled','disabled');
	        	}
	        	var checkAll = true;
    	    	var runner = 0;
    			$('.CheckOrder').each(function() {
    				if(this.checked == false) {
    					checkAll = false;
    				}
    				runner ++; 
    			});
    			if(checkAll == true&& runner >0) {
    				$('#checkAll').attr('checked', 'checked');
    			} else {
    				$('#checkAll').attr('checked', false);
    			}
		    	$('.datagrid-header-check input').bind('click',function(){
		    	    if($(this).is(':checked')){
		    	    	$('#cb_main').attr('checked',false);
		    		}		    			
		    	});  
		    	$('#cb_main').unbind('click');
		    	$('#cb_main').bind('click',function(){
		    		if($(this).is(':checked')){
		    			trainingPlan.mapTrainingPlan.clear();
		    		}
		    	});
	        }
		}); 
		 $('#cb_main').change(function(){		 
			 if($(this).is(':checked')){
				 $('.CheckOrder').each(function() {
						this.checked = false;
				 });
				 $('#checkAll').prop('checked', false);
				 trainingPlan.mapTrainingPlan.clear(); 
			 }		 
		 });
	},
	importExcel : function(){
		//mo fancybox
		$('#errMsg').html('').hide();
		$('#easyuiPopup').show();
		$('#errExcelMsg').html('').hide();
		$('#resultExcelMsg').html('').hide();
		$('#fakefilepc').val('');
		ReportUtils._callBackAfterImport = function(){
			$('#cb_main').attr('checked',false);
			if (trainingPlan.isImportSuccess){
				 $('.easyui-dialog #btnClose').unbind('click');
	   			 $('.easyui-dialog #btnClose').bind('click',function(){
	   				 $('#grid').datagrid('reload');
	   				 $('#easyuiPopup').dialog('close');
	   			 });
	   		 }else{
	   			$('.easyui-dialog #btnClose').unbind('click');
	   			 $('.easyui-dialog #btnClose').bind('click',function(){
	   				 $('#easyuiPopup').dialog('close');
	   			 });
	   		 }
			$('#grid').datagrid('reload');
			ReportUtils._callBackAfterImport=null;
		};
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
	        }
		});
		
	},
	/** Tulv2 Sep 30, 2014, import file excel*/
	upload: function(){
 		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$("#errExcelMsg").html(data.message).show();
			}else{
				$('#resultExcelMsg').html("Lưu dữ liệu thành công").show();
				$('#easyuiPopup').window('close');
				trainingPlan.search();
				setTimeout(function(){
					$('#resultExcelMsg').html("").hide();
				 }, 1500);
			}
		}, "importFrm", "excelFile", null, "errExcelMsg");
	},
	exportExcel : function(){
		$('#errMsg').html('').hide();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){	
				var dataModel = new Object();		
				dataModel.shopId = $('#dv').val();
				dataModel.gsnppId = $('#nv').val();
				dataModel.dateStr = $('#th').val();
				ReportUtils.exportReport('/plan/exportExcel',dataModel,'errMsg');									
			}			
		});	
 		return false;

	},
	delPlan : function(){
		$('#errMsg').html('').hide();
		$('#successMsg').html('').hide();
		trainingPlan.lstId = new Array();
		trainingPlan.lstId = trainingPlan.mapTrainingPlan.keyArray;
	  
		var inputDate = $('#th').val().trim();
		if (!Utils.compareCurrentMonthEx(inputDate)){
			$('#errMsg').html('Không được xoá lịch huấn luyện của tháng trước tháng hiện tại.').show();
			return false;
		} 
		if (trainingPlan.totalRow == 0){
			$('#errMsg').html('Không có dữ liệu để xoá.').show();
			return false;
		}else if ( (trainingPlan.lstId ==null || trainingPlan.lstId.length==0) && $('#cb_main').is(':checked')==false){ //grid rong hoac chua chon
			$('#errMsg').html('Hãy chọn dữ liệu để xoá.').show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstDelete =  trainingPlan.lstId;
		if($('#cb_main').is(':checked')) dataModel.pressCheckbox = true;
		else dataModel.pressCheckbox = false;

		var shopId = $('#shop').combotree('getValue');
		var gsnppId = $('#gsnpp').val().trim();
		var date = $('#date').val().trim();
		dataModel.shopId = shopId;
		dataModel.gsnppId = gsnppId;
		dataModel.dateStr = date;
		
		var  msg =  'Bạn có muốn xoá không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				Utils.addOrSaveData(dataModel, '/plan/delplan' , null, 'errMsg', function(data){
					$('#cb_main').attr('checked',false);
					$('#grid').datagrid('load', {page : 1});
					trainingPlan.mapTrainingPlan.clear();
				}, null, null, true,msg,function(dataErr){
					if(dataErr.hastraining) {
						trainingPlan.mapTrainingPlan.clear();
						$('#cb_main').attr('checked',false);
						trainingPlan.mapTrainingPlanCanNotDelete = new Map();
						trainingPlan.mapTrainingPlan.clear();
						$('#errMsg').html(dataErr.errMsg).show();
						if(!dataErr.deleteAll) {
							if (dataErr.lst!=null && dataErr.lst.length > 0){
								for(var i = 0 ; i < dataErr.lst.length; ++i) {
									trainingPlan.mapTrainingPlanCanNotDelete.put(Number(dataErr.lst[i]),i);
									trainingPlan.mapTrainingPlan.put(Number(dataErr.lst[i]),i);
								}
							}
						}
						
					}
				});
			}
		});	
	},
	compareCurrentDate:function(date) {
		//date < now => true || date > now => false
		if(date.length == 0) {
			return false;
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		return trainingPlan.compareDate(date, currentDate);
	},
	compareDate: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		if(dates.compare(startDateObj,endDateObj) >= 0){
			return false;
		}
		return true;
	}
};
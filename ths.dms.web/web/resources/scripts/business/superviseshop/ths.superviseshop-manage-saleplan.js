var SuperviseManageSalePlan = {
	clearErrorMsg : function() {
		$('#errMsg').html('').hide();
	},
	getShopCodeByShopCodeAndName : function(input) {
		var value = $('#' + input).val().trim().split("-");
		return value[0];
	},
	getGridUrl : function(shopCode, staffSaleCode, monthPlan) {
		return "/superviseshop/manage-saleplan/getinfo?shopCode=" + shopCode + "&staffSaleCode=" + staffSaleCode + "&monthPlan=" + monthPlan;
	},
	getInfo : function() {
		SuperviseManageSalePlan.clearErrorMsg();
		msg = Utils.getMessageOfSpecialCharactersValidate('shopCodeAndName','Mã đơn vị',Utils._NAME);
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('staffSaleCodeAndName','Mã nhân viên bán hàng');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffSaleCodeAndName','Mã nhân viên bán hàng',Utils._NAME);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('monthPlan','Kế hoạch tháng');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('monthPlan','Kế hoạch tháng', Utils._DATE_MM_YYYY);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopCode = SuperviseManageSalePlan.getShopCodeByShopCodeAndName('shopCodeAndName');
		var staffSaleCode = SuperviseManageSalePlan.getShopCodeByShopCodeAndName('staffSaleCodeAndName');
		var monthPlan = $('#monthPlan').val().trim();
		var url = SuperviseManageSalePlan.getGridUrl(shopCode, staffSaleCode, monthPlan);
		$("#grid").setGridParam({
			url : url,
			page : 1,
			gridComplete: function() {
				$('#jqgh_grid_rn').html(jsp_common_numerical_order);
				updateRownumWidthForJqGrid();
				$('#shopCodeAndName').focus();
				$('#title').html('Danh sách khách hàng của NVBH ' + Utils.XSSEncode(staffSaleCode) +' trong tháng ' + monthPlan).show();
		    }
		}).trigger("reloadGrid");
		return false;
	}
};

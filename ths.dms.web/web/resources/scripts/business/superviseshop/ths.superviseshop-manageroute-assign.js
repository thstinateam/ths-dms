var SuperviseManageRouteAssign = {
	_visitPlanId :null ,
	hideAllTab: function(){
		$('#tabActive1').removeClass('Active');
		$('#tabActive2').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
	},
	showTab1: function(){
		$('.ErrorMsgStyle').html('').hide();
		SuperviseManageRouteAssign.hideAllTab();
		$('#tabActive1').addClass('Active');
		$('#container1').show();
		$('#customerGrid').show();
		SuperviseManageRouteAssign.resize();
	},
	showTab2: function(){
		$('.ErrorMsgStyle').html('').hide();
		SuperviseManageRouteAssign.hideAllTab();
		$('#tabActive2').addClass('Active');
		$('#container2').show();
		SuperviseManageRouteAssign.openMapCustomerRouting();
	},
	resize: function() {
		var hSidebar1=$('.ContentSection').height();
		heightContent = window.innerHeight - $('#header').height() - $('.BreadcrumbSection').height() - $('#footer').height();
		if(hSidebar1 > heightContent ) {
			heightContent = $('.ContentSection').height();
		}
		$('.ReportTreeSection').height(heightContent-95);
	},
	editStaff: function(visitPlanId,saleStaffId,fromDate,toDate,status) {
		$('.ErrorMsgStyle').html('');
		$('#errMsg').html('').hide();
		$('#btnUpdate,#btnCancel').show();
		$('#btnAdd').hide();
		SuperviseManageRouteAssign._visitPlanId = visitPlanId;
		disableCombo('saleStaffId');
		setComboValue('saleStaffId',saleStaffId);
		$('#fromDate').val(fromDate);
		$('#toDate').val(toDate);
		$('#flagFromDate').val(fromDate);
		$('#flagToDate').val(toDate);
		var yesterdate = $('#yesterdateServer').val().trim();
		if(fromDate!=undefined && fromDate!=null && Utils.compareDate(fromDate, yesterdate)){
			disableDateTimePicker('fromDate');
		}else{
			enableDateTimePicker('fromDate');
		}
		if(toDate!=undefined && toDate!=null && toDate.length > 0 && Utils.compareDate(toDate, yesterdate)){
			disableDateTimePicker('toDate');
		}else{
			enableDateTimePicker('toDate');
		}
		return false;
	},
	resetStaff: function(){
		$('#errMsg').html('').hide();
		$('#btnUpdate,#btnCancel').hide();
		$('#btnAdd').show();
		enableCombo('saleStaffId');
		setComboValue('saleStaffId',0);
		enableDateTimePicker('fromDate');
		enableDateTimePicker('toDate');
		$('#fromDate').val($('#sysdateServer').val().trim());
		$('#toDate').val("");
		$('#flagFromDate').val($('#sysdateServer').val().trim());
		$('#flagToDate').val("");
	},
	detail :function(routingId) {
		var params = new Object();
		params.routingId = routingId;
		Utils.getHtmlDataByAjax(params, '/superviseshop/manageroute-create/assign/detail',function(data) {
			$("#routingContent").html(data).show();
			SuperviseManageRouteAssign.resize();
		}, null, 'POST');
	},
	addOrUpdateStaff: function(isUpdate){
		var msg = '';
		$('#idDivErrorInsert').html('').change();
		$('.ErrorMsgStyle').html('').hide();
		var saleStaffId = $('#saleStaffId').combobox('getValue').trim();
		if(msg.length == 0 && saleStaffId == 0) {
			msg = 'Bạn chưa chọn mã NV';
			$('#saleStaffId').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate',jsp_common_from_date);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate',jsp_common_from_date);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate',jsp_common_to_date);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate',jsp_common_to_date);
		}
		
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		
		if(msg.length ==0 && !Utils.compareDate(fromDate,toDate)){
			msg = msgCommonErr3;
			$('#fromDate').focus();
		}
		if(msg.length == 0) {
			msg = Utils.compareCurrentDateGreaterEx('toDate',jsp_common_to_date);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(isUpdate) dataModel.visitPlanId = SuperviseManageRouteAssign._visitPlanId;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		dataModel.status = 1;
		dataModel.saleStaffId  = saleStaffId;
		dataModel.routingId = $('#routingId').val();
		if (SuperviseManageRouteCreate._shopIdIsView != null) {
			dataModel.shopId = SuperviseManageRouteCreate._shopIdIsView;
		}
		Utils.addOrSaveData(dataModel, "/superviseshop/manageroute-create/assign/addorupdate-staff", null, 'errMsg',function(data){
			if(data.error !=undefined && data.error!=null && data.error){
				if(data.lstErr != undefined && data.lstErr !=null && data.lstErr.length >0){
					$('#idDivErrorInsert').html('').change();
					for(var i=0; i<data.lstErr.length; i++){
						if(data.lstErr[i].endDate!=undefined && data.lstErr[i].endDate!=null && data.lstErr[i].endDate.length>0){
							$('#idDivErrorInsert').append('<p id="errMsgInsert" style="background-position: left -370px; color: #FF6633;  font-size: 12px; margin: 5px 0 0 5px; padding: 2px 0 5px 29px;"> '
								+ formatString(create_route_cust_route_todate, [data.lstErr[i].customer.shortCode, data.lstErr[i].routing.routingCode, formatDate(data.lstErr[i].startDate), formatDate(data.lstErr[i].endDate)]) 
								+' </p>');
						}else{
							$('#idDivErrorInsert').append('<p id="errMsgInsert" style="background-position: left -370px; color: #FF6633;  font-size: 12px; margin: 5px 0 0 5px; padding: 2px 0 5px 29px;"> '
								+ formatString(create_route_cust_route_todate_null, [data.lstErr[i].customer.shortCode, data.lstErr[i].routing.routingCode, formatDate(data.lstErr[i].startDate)])
								+ ' </p>');
							
						}
					}
					$('#idDivErrorInsert').show().change();
				}
			}else{
				$('#staffGrid').datagrid('reload');				
				SuperviseManageRouteAssign.resetStaff();
			}			
		});
		return false;
	},
	//MAP
	_listRoute: null,
	_listMarker:null,
	_zoom:18,
	_centerLat:0,
	_centerLng:0,
	openMapCustomerRouting: function(){
		var lstRoutingCustomer = $('#customerGrid').datagrid('getData').rows;
		SuperviseManageRouteCreate._listRoute=new Array();
		for( var i = 0; i < lstRoutingCustomer.length; i++ ) { 
			var objCustomer = lstRoutingCustomer[i];
			if(objCustomer.status =='RUNNING') {
				var obj=new Object();
				if(objCustomer.monday =='GO') obj.t2 =1 ;
				else obj.t2 =0;
				if(objCustomer.tuesday =='GO') obj.t3 =1 ;
				else obj.t3 =0;
				if(objCustomer.wednesday =='GO') obj.t4 =1 ;
				else obj.t4 =0;
				if(objCustomer.thursday =='GO') obj.t5 =1 ;
				else obj.t5 =0;
				if(objCustomer.friday =='GO') obj.t6 =1 ;
				else obj.t6 =0;
				if(objCustomer.saturday =='GO') obj.t7 =1 ;
				else obj.t7 =0;
				if(objCustomer.sunday =='GO') obj.cn =1 ;
				else obj.cn =0;
				obj.customerId = objCustomer.customer.id;
				obj.customerCode = objCustomer.customer.shortCode;
				obj.customerName = objCustomer.customer.customerName;
				obj.address = objCustomer.customer.address;
				obj.lat = objCustomer.customer.lat;
				obj.lng = objCustomer.customer.lng;
				SuperviseManageRouteCreate._listRoute.push(obj);
			} 
		}
		SuperviseManageRouteCreate.fillListMarker("t2");
		SuperviseManageRouteAssign.resize();
	},
	
	changeByDateStaff : function(input, flag){
		$('.ErrorMsgStyle').html('').hide();
		var value = $(input).val();
		var fDate = $('#flagFromDate').val().trim();
		var tDate = $('#flagToDate').val().trim();
		var sysdate = $('#sysdateServer').val().trim();
		if(flag == 1){
			if(value!=null && value.length>0 && value!=null & tDate.length>0 && !Utils.compareDate(value, tDate)){
				$.messager.alert(jsp_common_thong_bao, msgCommonErr3,null,function(){
					$(input).val(fDate);
					$(input).focus();
				});
			}else if(value!=null && value.length>0 && !Utils.compareDate(sysdate, value)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr5,null,function(){
					$(input).val(fDate);
					$(input).focus();
				});
			}else if(value == null || value.length == 0){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr8,null,function(){
					$(input).val(fDate);
					$(input).focus();
				});
			}else{
				$('#flagFromDate').val(value);
			}
		} else {
			if(value!=null && value.length>0 && !Utils.compareDate(fDate, value)){
				$.messager.alert(jsp_common_thong_bao,msgCommonErr3,null,function(){
					$(input).val(tDate);
					$(input).focus();
				});
			}else if(value!=null && value.length>0 && !Utils.compareDate(sysdate, value)){//
				$.messager.alert(jsp_common_thong_bao,msgCommonErr6,null,function(){
					$(input).val(tDate);
					$(input).focus();
				});
			}else{
				$('#flagToDate').val(value);
			}
		}
	}
};
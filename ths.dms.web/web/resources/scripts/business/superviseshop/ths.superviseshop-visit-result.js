var VisitResult = {
	visitResultMap : null,
	clearErrorMsg : function() {
		$('#errMsg').html('').hide();
	},
	getGridUrl : function(shopCode, staffSaleCode, saleDate) {
		return "/superviseshop/visit-result/getinfo?shopCode=" + shopCode + "&staffSaleCode=" + staffSaleCode + "&saleDate=" + saleDate;
	},
	getInfo : function() {
		VisitResult.clearErrorMsg();
		msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffSaleCode','Mã nhân viên bán hàng',Utils._CODE);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('saleDate','Ngày BH');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('saleDate','Ngày BH', Utils._DATE_DD_MM_YYYY);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopCode = $('#shopCode').val().trim();
		var staffSaleCode = $('#staffSaleCode').val().trim();
		var saleDate = $('#saleDate').val().trim();	
		var url = VisitResult.getGridUrl(shopCode, staffSaleCode, saleDate);
		if(staffSaleCode != ''){
			$('#title').html('Kết quả ghé thăm của NVBH ' + Utils.XSSEncode(staffSaleCode)).show();
		} else {
			$('#title').html('Kết quả ghé thăm của NVBH ').show();
		}
		$("#grid").setGridParam({
			url : url,
			page : 1
		}).trigger("reloadGrid");
		return false;
	},
	showMap : function(staffCode) {
		var html = $('#viewMap').html();
		var shopCode = $('#shopCode').val().trim();
		var staffSaleCode = staffCode;
		var saleDate = $('#saleDate').val().trim();
		//var routeLst = new Array();
		$.fancybox(html, {
			modal : true,
			autoResize: false,	
			'title' : 'Bản đồ đầy đủ',
			'afterShow' : function() {	
				$('#viewMap').html('');
				var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
					$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top', document.documentElement.scrollTop + 10);
					$('#fancyboxMap').html($('#fancyboxMap').html() + "<div onclick='$.fancybox.close();' class='fancybox-item fancybox-close' title='Close'></div>");
					//load map
					var lat = null;
					var lng = null;
					var zoom=null;
					if(lat!='' && lng!=''){
						zoom=14;
					}
					MapUtil.loadMapResource(function(){
						Vietbando.visitResultMap = Vietbando.loadBigMapResult('bigMapContainer',lat,lng,zoom, function(point){
						});
						$.ajax({
							type : "POST",
							url : "/superviseshop/visit-result/getvisitresult",
							data : {staffSaleCode:staffSaleCode,shopCode:shopCode,saleDate:saleDate},
							dataType : "json",
							success : function(data) {
									if (data.success)
								    {
								     //add marker len ban do neu có
										if(data.visitResultLst.length>0 && data.visitResultLst[0].customerLat != null && data.visitResultLst[0].customerLng != null){
											var centerLat=data.visitResultLst[0].customerLat;
											var centerLng=data.visitResultLst[0].customerLng;
											var centerPt = new VLatLng(centerLat, centerLng);
											Vietbando.visitResultMap.setCenter(centerPt, zoom);
											Vietbando._listMarker=data.visitResultLst;
											var check = false;
											for(var i = 0; i < data.visitResultLst.length; i++) {
												var point = data.visitResultLst[i];
												if(Vietbando.isValidLatLng(point.customerLat, point.customerLng)) {
													var pt = new VLatLng(point.customerLat, point.customerLng);
													var image = null;
													var distance = null;
													if (data.visitResultLst[i].type == 4) {
														image = '/resources/images/lotrinh_kh_chuaghetham.png';
													} else if (data.visitResultLst[i].type == 1) {
														image = '/resources/images/lotrinh_kh_dangghetham.png';
													} else if (data.visitResultLst[i].type == 2) {
														image = '/resources/images/lotrinh_kh_kocodonhang.png';
													} else if (data.visitResultLst[i].type == 3)  {
														image = '/resources/images/lotrinh_kh_dc_chon.png';
													} else if (data.visitResultLst[i].type == 0) {
														image = '/resources/images/lotrinh_kh_ngoaituyen.png';
													}
													var mKOption = new VMarkerOptions(new VIcon(image, new VSize(30,30)),i+1, new VTextPosition(V_ANCHOR_BOTTOM_CENTER,new VSize(200,200)), 
															new VTextStyle(50,'bold','#ff0000','Arial'), data.visitResultLst[i].customerName, false,false);
													Vietbando._marker = new VMarker(pt, mKOption);
													Vietbando._marker.customerName = data.visitResultLst[i].customerName;
													var seqTxt= null;
													if (data.visitResultLst[i].type == 0) {
														seqTxt = '!';
													} else{
														if (data.visitResultLst[i].seq!=null) {
															seqTxt = data.visitResultLst[i].seq;
														} else {
															seqTxt = "";
														}
													}
													Vietbando._marker.seq = seqTxt;
													Vietbando._marker.address = data.visitResultLst[i].address;
													Vietbando._marker.mobiphone = data.visitResultLst[i].mobiphone;
													Vietbando._marker.phone = data.visitResultLst[i].phone;
													Vietbando.visitResultMap.addOverlay(Vietbando._marker);
													Vietbando.visitResultMap.addOverlay(new MyRouteOverlayCustomer(pt,seqTxt));
													
													if (data.visitResultLst[i].type != 0) {
														if (point.staffLat != null && point.staffLng != null) {
															if (Vietbando.isValidLatLng(point.staffLat, point.staffLng)) {
																var staffPt = new VLatLng(point.staffLat, point.staffLng);
																var staffImage = null;
																distance = Vietbando.CalculateDistance(staffPt,pt);
																var distanceTxt = distance + 'm';
																if(distance>= 1000){
																	distanceTxt = Vietbando.CalculateDistanceFormat(distance)+'km';
																}
																if (distance > data.visitResultLst[i].distanceOrder) {
																	staffImage = '/resources/images/staff-flag-red.png';
																} else {
																	staffImage = '/resources/images/staff-flag-blue.png';
																} 
																var mKOptionStaff = new VMarkerOptions(new VIcon(staffImage, new VSize(40,40)),i+1, new VTextPosition(V_ANCHOR_BOTTOM_CENTER,new VSize(200,200)), 
																		new VTextStyle(50,'bold','#ff0000','Arial'), null, false,false);
																Vietbando._marker = new VMarker(staffPt, mKOptionStaff);
																Vietbando._marker.isCustomer = false;
																Vietbando.visitResultMap.addOverlay(Vietbando._marker);
																
																Vietbando.visitResultMap.addOverlay(new MyDistanceOverlay(new VLatLng(point.staffLat, point.staffLng), distanceTxt));
															}
															if (data.visitResultLst[i].seq!=null) {
																Vietbando.visitResultMap.addOverlay(new MyRouteOverlay(new VLatLng(point.staffLat, point.staffLng), data.visitResultLst[i].seq));
															} else{
																Vietbando.visitResultMap.addOverlay(new MyRouteOverlay(new VLatLng(point.staffLat, point.staffLng), ""));
															}
														}
													}
													if ( check == false && point.lastStaffLat != null && point.lastStaffLng != null) {
														if (Vietbando.isValidLatLng(point.lastStaffLat, point.lastStaffLat)) {
															var staffLastPt = new VLatLng(point.lastStaffLat, point.lastStaffLng);
															var staffLastImage = '/resources/images/staff_location.png';
															var mKOptionStaffLast = new VMarkerOptions(new VIcon(staffLastImage, new VSize(24,24)),i+1, new VTextPosition(V_ANCHOR_BOTTOM_CENTER,new VSize(200,200)), 
																	new VTextStyle(50,'bold','#ff0000','Arial'), null, false,false);
															Vietbando._marker = new VMarker(staffLastPt, mKOptionStaffLast);
															Vietbando._marker.isCustomer = false;
															Vietbando.visitResultMap.addOverlay(Vietbando._marker);
															check = true;
														}
													}
												} 
											}
										}
								    }
							},
							error:function(XMLHttpRequest, textStatus, errorThrown) {
							}
						});
					});
					clearTimeout(tm);
				}, 500);
			},
			'afterClose' : function() {
				$('#viewMap').html(html);
			}
		});
		return false;
	}
};
var SellerPosition = {
		//sellerPositionMap : null,
		clearMsg : function() {
			$('#errMsg').html('').hide();
			$('#successMsg').html('').hide();
		},
		getGridUrl : function(shopCode,staffType,allLevel,staffCode) {
			return "/superviseshop/visit-result/searchsellerposition?shopCode=" + shopCode + "&staffType=" + staffType + "&allLevel=" + allLevel + "&staffCode=" + staffCode;
		},
		searchSellerPosition: function() {
			shopCode = $('#shopCode').val().trim();
			staffType = $('#staffType').val().trim();
			staffCode = $('#staffCode').val().trim();
			allLevel = $('#allLevel').attr('checked');
			if(allLevel=="checked"){
				allLevel = true;
			}else{
				allLevel = false;
			}
			SellerPosition.clearMsg();
			if(Vietbando.sellerPositionMap!=null){
				Vietbando.sellerPositionMap.clearOverlays();
				Vietbando._map.closeInfoWindow();
			}
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
			if(msg.length ==0){
				msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var url = SellerPosition.getGridUrl(shopCode,staffType,allLevel,staffCode);
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
			return false;
		},
		
		getSellerPositionDetail : function(staffId,staffCode,staffName,shopCode,objectType) {
			showLoading('searchLoading');
			SellerPosition.clearMsg();
			if(Vietbando.sellerPositionMap!=null){
				Vietbando.sellerPositionMap.clearOverlays();
				Vietbando._map.closeInfoWindow();
			}
			
			$('#grid tr').each(function(){
				if($(this).hasClass('ui-state-salepos')){
					$(this).removeClass('ui-state-salepos');
				}
			});
			
			$('#grid tr').each(function(){
				if($(this).attr('id')==staffId){
					$(this).addClass('ui-state-salepos');
				}
			});
			
			$.ajax({
				type : "POST",
				url : "/superviseshop/visit-result/getsellerpositiondetail",
				data : {staffId:staffId,staffCode:staffCode,objectType:objectType},
				dataType : "json",
				success : function(data) {
						if (data.success && data.mainInfo!=null && data.mainInfo.length>0){
							hideLoading('searchLoading');
							if(objectType == 7 ){//case TBHV
								var mainInfo = new TBHVMapInfo();
								var item = data.mainInfo[0];
								mainInfo.staffCode = staffCode;
								mainInfo.staffName = staffName;
								mainInfo.shopCode = shopCode;
								var firstSubItem = true;
								//lay danh sach GSNPP di cung
								mainInfo.GSNPPStr = "";
								for(var i=0;i<data.mainInfo.length;i++){
									var subItem = data.mainInfo[i];
									if(subItem.staffCode!=null && subItem.staffCode!=undefined && subItem.staffCode!=''){
										if(firstSubItem){
											mainInfo.GSNPPStr = Utils.XSSEncode(subItem.staffCode);//GSNPP
											firstSubItem = false;
										}else{
											mainInfo.GSNPPStr = Utils.XSSEncode(mainInfo.GSNPPStr + ", " + subItem.staffCode);//GSNPP
										}
									}
								}
								mainInfo.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
								mainInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
								mainInfo.lat = item.lat;
								mainInfo.lng = item.lng;
								mainInfo.imgUrl = '/resources/images/quanlyvung.png';
								
								//child info
								if(data.childInfo!=null && data.childInfo.length>0){//GSNPP
									var childInfoArr = new Array();
									for(var i=0;i<data.childInfo.length;i++){
										var item = data.childInfo[i];
										var childInfo = new GSNPPMapInfo();
										childInfo.staffCode = (item.staffCode!=null && item.staffCode!=undefined)?item.staffCode:"";
										childInfo.staffId = item.staffId;
										childInfo.staffName = (item.staffName!=null && item.staffName!=undefined)?item.staffName:"";
										childInfo.shopCode = shopCode;
										childInfo.NVBHStr = "";//get later by ajax
										childInfo.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
										childInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
										childInfo.lat = item.lat;
										childInfo.lng = item.lng;
										childInfo.imgUrl = '/resources/images/giamsat.png';
										childInfoArr.push(childInfo);
									}
								}
							}else if(objectType == 5){//case GSNPP
								var mainInfo = new GSNPPMapInfo();
								var item = data.mainInfo[0];
								mainInfo.staffCode = staffCode;
								mainInfo.staffId = staffId;
								mainInfo.staffName = staffName;
								mainInfo.shopCode = shopCode;
								//lay danh sach NVBH di cung
								var firstSubItem = true;
								mainInfo.NVBHStr = "";
								for(var i=0;i<data.mainInfo.length;i++){
									var subItem = data.mainInfo[i];
									if(subItem.staffCode!=null && subItem.staffCode!=undefined && subItem.staffCode!=''){
										if(firstSubItem){
											mainInfo.NVBHStr = subItem.staffCode;//GSNPP
											firstSubItem = false;
										}else{
											mainInfo.NVBHStr = Utils.XSSEncode(mainInfo.NVBHStr + ", " + subItem.staffCode);//GSNPP
										}
									}
								}
								mainInfo.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
								mainInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
								mainInfo.lat = item.lat;
								mainInfo.lng = item.lng;
								mainInfo.imgUrl = '/resources/images/giamsat.png';
								
								//child info
								if(data.childInfo!=null && data.childInfo.length>0){//NVBH
									var childInfoArr = new Array();
									for(var i=0;i<data.childInfo.length;i++){
										var item = data.childInfo[i];
										var childInfo = new NVBHMapInfo();
										childInfo.staffCode = (item.staffCode!=null && item.staffCode!=undefined)?item.staffCode:"";
										childInfo.staffName = (item.staffName!=null && item.staffName!=undefined)?item.staffName:"";
										childInfo.staffId = (item.staffId!=null && item.staffId!=undefined)?item.staffId:"";
										childInfo.shopCode = shopCode;
										//get later by ajax
										childInfo.revenue = "";
										childInfo.channelPoint = "";
										childInfo.revenuePoint = "";
										childInfo.lastPosTime = "";
										childInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
										childInfo.lat = item.lat;
										childInfo.lng = item.lng;
										childInfo.imgUrl = '/resources/images/nhanvien.png';
										childInfoArr.push(childInfo);
									}
								}
								
							}else if(objectType == 1){//case NVBH
								var mainInfo = new TBHVMapInfo();
								var item = data.mainInfo[0];
								mainInfo.staffCode = staffCode;
								mainInfo.staffName = staffName;
								mainInfo.staffId = (item.staffId!=null && item.staffId!=undefined)?item.staffId:"";
								mainInfo.shopCode = shopCode;
								//get later by ajax
								mainInfo.revenue = "";
								mainInfo.channelPoint = "";
								mainInfo.revenuePoint = "";
								mainInfo.lastPosTime = "";
								mainInfo.accuracy = (item.accuracy!=null && item.accuracy!=undefined)?item.accuracy:"";
								mainInfo.lat = item.lat;
								mainInfo.lng = item.lng;
								mainInfo.imgUrl = '/resources/images/nhanvien.png';
								
								//child info
								if(data.childInfo!=null && data.childInfo.length>0){//Customer
									var childInfoArr = new Array();
									for(var i=0;i<data.childInfo.length;i++){
										var item = data.childInfo[i];
										var childInfo = new NVBHMapInfo();
										childInfo.customerCode = (item.shortCode!=null && item.shortCode!=undefined)?item.shortCode:"";
										childInfo.customerName = (item.customerName!=null && item.customerName!=undefined)?item.customerName:"";
										childInfo.lat = item.customerLat;
										childInfo.lng = item.customerLng;
										childInfo.imgUrl = '/resources/images/supermarket_red.png';
										childInfoArr.push(childInfo);
									}
								}
							}
							
							//draw main info to map
							if(mainInfo!=null && mainInfo.lat!=null && mainInfo.lng!=null && Vietbando.isValidLatLng(mainInfo.lat, mainInfo.lng)) {
								var pt = new VLatLng(mainInfo.lat, mainInfo.lng);
								var mKOption = new VMarkerOptions(new VIcon(mainInfo.imgUrl,new VSize(21,24)),"", null, null, 
										mainInfo.staffName, false,false);
								Vietbando._marker = new VMarker(pt, mKOption);
								
								//set attribute for main maker
								Vietbando._marker.staffCode = staffCode;
								Vietbando._marker.staffName = mainInfo.staffName;
								Vietbando._marker.shopCode = mainInfo.shopCode;
								if(objectType==7){//TBHV
									Vietbando._marker.GSNPPStr = mainInfo.GSNPPStr;
								}else if(objectType==5){//GSNPP
									Vietbando._marker.staffId = mainInfo.staffId;
									Vietbando._marker.NVBHStr = mainInfo.NVBHStr;
								}else if(objectType==1){//NVBH
									Vietbando._marker.staffId = mainInfo.staffId;
								}
								Vietbando._marker.lastPosTime = mainInfo.lastPosTime;
								Vietbando._marker.accuracy = mainInfo.accuracy;
								Vietbando._marker.objectType = objectType;
								Vietbando.sellerPositionMap.addOverlay(Vietbando._marker);
								//mo popup cho item duoc chon
								if (objectType == 5 || objectType == 7) {//GSNPP OR TBHV
									if(objectType == 5){
										var html =String.format(Vietbando.GSNPPInfoContent,mainInfo.staffCode,mainInfo.staffName,mainInfo.shopCode,mainInfo.NVBHStr,mainInfo.lastPosTime,mainInfo.accuracy);
									}else{
										var html =String.format(Vietbando.TBHVInfoContent,Vietbando._marker.staffCode,Vietbando._marker.staffName,Vietbando._marker.shopCode,Vietbando._marker.GSNPPStr,Vietbando._marker.lastPosTime,Vietbando._marker.accuracy);
									}
									Vietbando.sellerPositionMap.openInfoWindow(pt,html);
									
									//draw child info to map
									if(childInfoArr!=null && childInfoArr.length>0){
										for(var k = 0; k < childInfoArr.length; k++) {
											var childInfo = childInfoArr[k];
											if(childInfo.lat!=null && childInfo.lng!=null && Vietbando.isValidLatLng(childInfo.lat, childInfo.lng)) {
												var pt = new VLatLng(childInfo.lat, childInfo.lng);
												var mKOption = new VMarkerOptions(new VIcon(childInfo.imgUrl,new VSize(21,24)),"", null, null, 
														childInfo.staffName, false,false);
												Vietbando._marker = new VMarker(pt, mKOption);
												
												//set attribute for child maker
												Vietbando._marker.staffCode = childInfo.staffCode;
												Vietbando._marker.staffName = childInfo.staffName;
												Vietbando._marker.shopCode = childInfo.shopCode;
												Vietbando._marker.lastPosTime = childInfo.lastPosTime;
												Vietbando._marker.accuracy = childInfo.accuracy;
												if(objectType==7){//TBHV
													Vietbando._marker.objectType = 5;
													Vietbando._marker.staffId = childInfo.staffId;
													Vietbando._marker.NVBHStr = childInfo.NVBHStr;
												}else if(objectType==5){//GSNPP
													Vietbando._marker.objectType = 1;
													Vietbando._marker.staffId = childInfo.staffId;
												}
												Vietbando.sellerPositionMap.addOverlay(Vietbando._marker);
											}
										}
									}
								}else if (objectType == 1) {//NVBH
									$.ajax({
										type : "POST",
										url : "/superviseshop/visit-result/getNVBHdetail",
										data : {staffId:mainInfo.staffId,objectType:objectType},
										dataType : "json",
										success : function(data) {
											if (data.success && data.mainInfo!=null && data.mainInfo.length>0)
										    {
												var item = data.mainInfo[0];
												mainInfo.revenue = ((item.amount!=null && item.amount!=undefined)?item.amount:"") + ((item.plan!=null && item.plan!=undefined)?("/"+item.plan):"");
												mainInfo.channelPoint = (item.pointInRouting!=null && item.pointInRouting!=undefined)?item.pointInRouting:"";
												mainInfo.revenuePoint = (item.pointAmount!=null && item.pointAmount!=undefined)?item.pointAmount:"";
												mainInfo.lastPosTime = (item.strLastDatePosition!=null && item.strLastDatePosition!=undefined)?item.strLastDatePosition:"";
												var html =String.format(Vietbando.NVBHInfoContent,mainInfo.staffCode,mainInfo.staffName,mainInfo.shopCode,mainInfo.revenue,mainInfo.channelPoint,mainInfo.revenuePoint,mainInfo.lastPosTime,mainInfo.accuracy);
												var pt = new VLatLng(mainInfo.lat, mainInfo.lng);
												Vietbando.sellerPositionMap.openInfoWindow(pt,html);
												
												//draw child info to map (Customer)
												if(childInfoArr!=null && childInfoArr.length>0){
													for(var i = 0; i < childInfoArr.length; i++) {
														var childInfo = childInfoArr[i];
														if(childInfo.lat!=null && childInfo.lng!=null && Vietbando.isValidLatLng(childInfo.lat, childInfo.lng)) {
															var pt = new VLatLng(childInfo.lat, childInfo.lng);
															var mKOption = new VMarkerOptions(new VIcon(childInfo.imgUrl,new VSize(21,24)),"", null, null, 
																	childInfo.customerName, false,false);
															Vietbando._marker = new VMarker(pt, mKOption);
															//set attribute for child maker
															Vietbando._marker.objectType = -1;
															Vietbando._marker.staffCode = childInfo.customerCode;
															Vietbando._marker.staffName = childInfo.customerName;
															Vietbando.sellerPositionMap.addOverlay(Vietbando._marker);
														}
													}
												}
										    }
										},
										error:function(XMLHttpRequest, textStatus, errorThrown) {
										}
									});
								}
							}
					    }else if(data.success && data.mainInfo==null || data.mainInfo.length==0){
					    	hideLoading('searchLoading');
					    	$('#successMsg').html('Không có dữ liệu vị trí NVBH').show();
					    }else if(data.error){
					    	hideLoading('searchLoading');
					    	$('#errMsg').html('Lỗi trong quá trình xử lý').show();
					    }
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					hideLoading('searchLoading');
				}
			});
			return false;
		},
		loadMap : function(staffId,hasPosition) {
			$('#sellerPosMapContainer').html('');
			zoom = 14;
			var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
				MapUtil.loadMapResource(function(){
					Vietbando.loadMap('sellerPosMapContainer',null,null,zoom, function(){
						//call back function here
						var centerPt = new VLatLng(10.79659063765061, 106.66422128677368);
						Vietbando.sellerPositionMap.setCenter(centerPt, zoom);
						//get staff info
						if(hasPosition=='1'){
							$.ajax({
								type : "POST",
								url : "/superviseshop/visit-result/getstaffinfo",
								data : {staffId:staffId},
								dataType : "json",
								success : function(data) {
									if (data.success)
									{
										SellerPosition.getSellerPositionDetail(staffId,data.staffCode,data.staffName,data.shopCode,data.objectType);
									}
								},
								error:function(XMLHttpRequest, textStatus, errorThrown) {
									
								}
							});
						}
					},true);
				});
				clearTimeout(tm);
			}, 500);
			return false;
		}
};
var SuperviseManageRouteSetOrder = {
	_xhrSave : null,
	_listCustomer : null,
	_listCustomerMap: null,
	_seqTmp :null,
	_filterSaleDate : null,
	searchRouteSetOrder: function(){
		$('#errMsg').html('').hide();
		var routingId = $('#routingId').val();
		var saleDate = $('#listDate').val();
		SuperviseManageRouteSetOrder.convertSeq();
		$('#routingGrid').datagrid('reload', {routingId: routingId,saleDate :saleDate});
		SuperviseManageRouteSetOrder._filterSaleDate = saleDate;
	},
	convertSeq: function(){
		var saleDate = $('#listDate').val();
		if(saleDate == 1) SuperviseManageRouteSetOrder._seqTmp = 'seq8';
		else if(saleDate == 2) SuperviseManageRouteSetOrder._seqTmp = 'seq2';
		else if(saleDate == 3) SuperviseManageRouteSetOrder._seqTmp = 'seq3';
		else if(saleDate == 4) SuperviseManageRouteSetOrder._seqTmp = 'seq4';
		else if(saleDate == 5) SuperviseManageRouteSetOrder._seqTmp = 'seq5';
		else if(saleDate == 6) SuperviseManageRouteSetOrder._seqTmp = 'seq6';
		else if(saleDate == 7) SuperviseManageRouteSetOrder._seqTmp = 'seq7';
		else SuperviseManageRouteSetOrder._seqTmp = '';
	},
	detail :function(routingId) {
		var params = new Object();
		params.routingId = routingId;
		Utils.getHtmlDataByAjax(params, '/superviseshop/manageroute-create/order/detail',function(data) {
			$("#routingContent").html(data).show();
			SuperviseManageRouteSetOrder.resize();
		}, null, 'POST');
	},
	resize: function() {
		var hSidebar1=$('.ContentSection').height();
		heightContent = window.innerHeight - $('#header').height() - $('.BreadcrumbSection').height() - $('#footer').height();
		if(hSidebar1 > heightContent ) {
			heightContent = $('.ContentSection').height();
		}
		$('.ReportTreeSection').height(heightContent-95);
		return ;
	},
	hideAllTab: function(){
		$('#tabActive1').removeClass('Active');
		$('#tabActive2').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
	},
	showTab1: function(){
		SuperviseManageRouteSetOrder.hideAllTab();
		$('#tabActive1').addClass('Active');
		$('#container1').show();
		SuperviseManageRouteSetOrder.searchRouteSetOrder();
		SuperviseManageRouteSetOrder.resize();
	},
	showTab2: function(){
		SuperviseManageRouteSetOrder.hideAllTab();
		$('#tabActive2').addClass('Active');
		$('#container2').show();
		SuperviseManageRouteSetOrder.openMapCustomerRouting();
	},
	changeSelectBox: function(){
		if($('#tabActive1').hasClass('Active')) {
			SuperviseManageRouteSetOrder.searchRouteSetOrder();
		} else{
			SuperviseManageRouteSetOrder.openMapCustomerRouting();
		}
	},
//	UPDATE
	pushDataGridForMap :function(){
		SuperviseManageRouteSetOrder._listCustomerMap = new Map();
		var rows = $('#routingGrid').datagrid('getRows');
		if($.isArray(rows)) {
			for(var i = 0; i < rows.length; i++) {
				var customer = rows[i].customer;
				SuperviseManageRouteSetOrder._listCustomerMap.put(rows[i].customerId, customer);
			}
		}
	},
	getListSeq: function(){
		var selected = new Array();
		$('.seq').each(function() {
			selected.push($(this).attr('value'));
		});
		return selected;
	},
	getListRoutingCustomerId: function(){
		var selected = new Array();
		var rows = $('#routingGrid').datagrid('getRows');
		if($.isArray(rows)) {
			for(var i = 0; i < rows.length; i++) {
				var customer = rows[i].customer;
				if(customer != null) {
					selected.push(customer.id);
				}
			}
		}
		return selected;
	},
	updateListRoutingCustomer: function(){
		$('#errMsg').html('').hide();
		var arraySeq = SuperviseManageRouteSetOrder.getListSeq();
		//SuperviseManageRouteSetOrder.pushDataGridForMap();
		var rows = $('#routingGrid').datagrid('getRows');//SuperviseManageRouteSetOrder._listCustomerMap.keyArray;
		var flag = false;
		var i=0;
		
		if((arraySeq == null || rows == null) || ( arraySeq.length == 0 || rows.length == 0)){
			$('#errMsg').html(create_route_no_data_to_update).show();
			var tm = setTimeout(function(){
				$('#errMsg').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return;
		}
		var arrShortCode = new Array();
		var ids="";
		var selector=null;
		for(i=0; i<arraySeq.length; i++){
			var custCode = rows[i].shortCode;
			arrShortCode.push(custCode);
			if(isNullOrEmpty(arraySeq[i])) {
				$('#errMsg').html(formatString(create_route_cust_have_not_ttgt, custCode)).show();
				$('#seq_' +rows[i].customerId).focus();
				return;
			}
			if(arraySeq[i]> rows.length){
				ids+=custCode+" , ";
				if(selector==null) selector = $('#seq_' + rows[i].customerId);
			} 
		}
		if(ids.length>0){
			ids = ids.substring(0, ids.length-3);
			$('#errMsg').html(formatString(create_route_stt_ma_khong_lon_hon_sl_kh, ids)).show();
			selector.focus();
			return;
		}
		
		var checkSeq = false;
		var valueId;
		$('.seq').each(function(){
			var value = $(this).attr('value');
			if(isNaN(value)){
				if(checkSeq){
					return;
				}else{
					valueId = $(this).attr('id'); 
					$('#errMsg').html(create_route_stt_lon_hon_0).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);					
					}, 5000);
					checkSeq = true;
				}
			}
		});
		if(checkSeq){
			$('#' +valueId).focus();
			return;
		}
		
		var flag=false;
		var idSame = "";
		for(i=0; i<arraySeq.length-1; i++){
			for(j = i+1; j<arraySeq.length; j++){
				if(arraySeq[i] != '' && arraySeq[i]==arraySeq[j]){
					var custCode1 = rows[i].shortCode;
					var custCode2 = rows[j].shortCode;
					idSame =formatString(create_route_stt_code_duplicate_code, custCode1, custCode2);
					selector = $('#seq_' + rows[j].customerId);
					flag = true;
					break;
				}
			}
			if(flag) break;
		}
		if(idSame.length>0){
			$('#errMsg').html(idSame).show();
			selector.focus();
			return;
		}
		var saleDate =  $('#listDate').val();
		if(saleDate == -1) {
			$('#errMsg').html(create_route_sale_day_chua_chon).show();
			$('#listDate').focus();
			return;
		}
		var params =  new Object();
		var saleDate = $('#listDate').val();
		params.lstSeq = arraySeq;
		params.lstShortCode = arrShortCode;
		params.saleDate = saleDate;
		
		Utils.addOrSaveData(params, '/superviseshop/manageroute-create/order/update', null, 'errMsg', function(data){
		}, null,null,null,create_route_confirm_update_stt);
	},
//	UPDATE
//	SHOWMAP
	openMapCustomerRouting: function(){
		$('#errMsgMap').hide();
		$('#errMsg').html('').hide();
		var params = new Object();
		if(SuperviseManageRouteCreate._divMap!=undefined && SuperviseManageRouteCreate._divMap!=null && SuperviseManageRouteCreate._divMap.length > 100){
			$('#divMapRoute').html(SuperviseManageRouteCreate._divMap.toString()).show().change();
		}else{
			$('#divMapRoute').html('<div id="mapRoute" style="width:100%;height:424px;position: relative;" ></div>').show().change();			
		}
		params.routingId = $('#routingId').val();	
		params.saleDate = $('#listDate').val();
		params.shopCode = $('#shopCode').val();
		$('#divOverlay').show();
		$.ajax({
			type : 'POST',
			url : '/superviseshop/manageroute-create/order/search',
			data :($.param(params, true)),
			dataType : 'json',
			success : function(data) {
				SuperviseManageRouteSetOrder._listCustomer = new Array();
				if($.isArray(data.rows)) {
					for(var i = 0; i < data.rows.length; i++) {
//						var objCustomer = rows[i].customer;
						var obj = new Object();
						obj.customerId = data.rows[i].customerId;
						obj.shortCode = Utils.XSSEncode(data.rows[i].shortCode);
						obj.customerCode = Utils.XSSEncode(data.rows[i].shortCode);
						obj.customerName = Utils.XSSEncode(data.rows[i].customerName);
						obj.address = Utils.XSSEncode(data.rows[i].address);
						obj.lat = data.rows[i].lat;
						obj.lng = data.rows[i].lng;
						var saleDate = $('#listDate').val();
						if(saleDate == 1) obj.ttgt = data.rows[i].seq8;
						else if(saleDate == 2) obj.ttgt = data.rows[i].seq2;
						else if(saleDate == 3) obj.ttgt = data.rows[i].seq3;
						else if(saleDate == 4) obj.ttgt = data.rows[i].seq4;
						else if(saleDate == 5) obj.ttgt = data.rows[i].seq5;
						else if(saleDate == 6) obj.ttgt = data.rows[i].seq6;
						else if(saleDate == 7) obj.ttgt = data.rows[i].seq7;
						else obj.ttgt = '';
						if(obj.ttgt == null) obj.ttgt = '';
						SuperviseManageRouteSetOrder._listCustomer.push(obj);
					}
				}
				SuperviseManageRouteSetOrder.showMapCustomer(SuperviseManageRouteSetOrder._listCustomer);
				SuperviseManageRouteSetOrder.resize();
				$('#divOverlay').hide();
			}
		});
	},
	showMapCustomer:function(lstCustomer) {//tungmt
		//ViettelMap.clearOverlays();
		SuperviseManageRouteCreate._listMarker = new Array();
		for(i=0 ; i<lstCustomer.length ; i++){
			if(lstCustomer[i].lat!=null && lstCustomer[i].lat!=0 && lstCustomer[i].lat!=undefined && !isNaN(lstCustomer[i].lat) && lstCustomer[i].lng!=null && lstCustomer[i].lng!=0 && lstCustomer[i].lng!=undefined && !isNaN(lstCustomer[i].lng)){
				var flag=false;
				var kc=0;
				var listMerce=new Array();
				for(j=i+1; j<lstCustomer.length ; j++){
					kc=SuperviseManageRouteCreate.CalculateDistance(lstCustomer[i],lstCustomer[j]);					
					if(kc<100){
						flag=true;
						listMerce.push(lstCustomer[j]);
						lstCustomer.splice(j, 1);
						j--;
					}
				}
				if(flag==true){
					listMerce.push(lstCustomer[i]);//add them kh dang xet vao list
					lstCustomer[i].customerId = -lstCustomer[i].customerId;  // chuyen kh dang xet thanh kh dai dien
					lstCustomer[i].listMerce=listMerce;//gan danh sach kh trong list vao kh dai dien
					SuperviseManageRouteCreate._listMarker.push(lstCustomer[i]);
				}else{
					SuperviseManageRouteCreate._listMarker.push(lstCustomer[i]);
				}
			}
			else{
				lstCustomer.splice(i,1);
				i--;
			}
		}
		//add marker len ban do neu có
		if(SuperviseManageRouteCreate._listMarker.length>0){
			VTMapUtil.loadMapResource(function(){
				ViettelMap.loadBigMap('mapRoute',SuperviseManageRouteCreate._centerLat,SuperviseManageRouteCreate._centerLng,SuperviseManageRouteCreate._zoom,null);
				ViettelMap.addMutilMarkerStaffForSetOrder();
			});
		}
		else{
			VTMapUtil.loadMapResource(function(){
				ViettelMap.loadBigMap('mapRoute',0,0,SuperviseManageRouteCreate._zoom,null);		
			});
			$('#errMsgMap').show();
			var tm = setTimeout(function(){
				$('#errMsgMap').hide();
				clearTimeout(tm);					
			}, 5000);
		}
	}
};
var FeedBack = {
	_lstStaffInfo: [],
	_paramSearch: null,
	_INFO_FEEDBACK: 1,
	_INFO_NEED_DONE: 2,
	_mapMutilSelect: null,
	_currentSearchCallback: null,
	_mapMutilCustomer: null,
	_mapStaff: null,

	/**
	 * Lay params khi nhan buttom search
	 * @author vuongmq
	 * @param type
	 * @since 13/11/2015 
	 */
	getSearchParams: function(type) {
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsgSearch').html(msg).show();
			return false;
		}
		var params = {};
		if (type == FeedBack._INFO_FEEDBACK) {
			params.shopId = $('#shopId').val();
			params.staffCode = $('#cbxStaff').combobox('getValue');	
			params.customerCode = $('#customerCode').val();
		}
		params.type = type;
		params.typeParam = $('#typeFeedback').val();
		params.status = $('#status').val();
		params.fromDate = $('#fDate').val();
		params.toDate = $('#tDate').val();
		FeedBack._paramSearch = params;
		return params;
	},

	/**
	 * Search danh sach theo doi van de
	 * @author vuongmq
	 * @since 13/11/2015 
	 */
	searchFeedback: function() {
		hideAllMessage();
		var params = FeedBack.getSearchParams(FeedBack._INFO_FEEDBACK);
		$('#grid').datagrid('load', params);
	},

	/**
	 * hiden Checkbox Grid
	 * @author vuongmq
	 * @param rows
	 * @param type
	 * @since 17/11/2015 
	 */
	hidenCheckboxGird: function(rows, type) {
		if (rows != undefined && rows != null) {
			var i = 0;
			if (type == FeedBack._INFO_FEEDBACK) {
				$(".datagrid-cell-check input[type=checkbox]").each(function() {
					if (rows[i].result != FeedbackStatus.COMPLETE) {
						$(this).prop('disabled', true);	
						$(this).attr('hidden', true);
						$(this).attr('checked', false);
					}
					i++;
				});
			} else {
				$(".datagrid-cell-check input[type=checkbox]").each(function() {
					if (rows[i].result != FeedbackStatus.NEW) {
						$(this).prop('disabled', true);	
						$(this).attr('hidden', true);
						$(this).attr('checked', false);
					}
					i++;
				});
			}
		}
	},

	/**
	 * Init theo doi van de
	 * @author vuongmq
	 * @since 13/11/2015 
	 */
	initGridFeedback: function() {
		var params = FeedBack.getSearchParams(FeedBack._INFO_FEEDBACK);
		$('#grid').datagrid({  
		    url: '/feedback/search',
		    rownumbers: true,
		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
		    pageSize: 100,
		    pageList: [10, 50, 100, 200, 500],
		    width: $(window).width() - 70,	    		    
		    queryParams: params,
		    fitColumns: true,
		    scrollbarSize: 0,
		    columns: [[
				{field: 'id', checkbox: true, width: 50, align: 'center'},
				{field: 'staffCode', title: 'Nhân viên', width: 200, align: 'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(row.staffCode + ' - ' + row.staffName);
				}},
				{field: 'customerName', title: 'Khách hàng', width: 300, align: 'left', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
				{field: 'typeName', title: 'Loại vấn đề', width: 120, align: 'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(value);
				}},
		        {field: 'content', title: 'Nội dung', width: 310, align: 'left', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field: 'result', title: 'Trạng thái', width: 120, align: 'left', formatter: function(value, row, index) {
		        	return FeedbackStatus.parseValue(value);
		        }},
		        {field: 'remindDateStr', title: 'Ngày nhắc nhở', width: 130, align: 'center', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field: 'doneDateStr', title: 'Ngày thực hiện', width: 130, align: 'center', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field: 'item', title: 'Đính kèm', width: 80, align: 'center', formatter: function(value, row, index) {
		        	if (value != null && value > 0) {
		        		return '<a href="javascript:void(0)" onclick="return FeedBack.viewPopupInfoFeedBack(' + index + ', ' + FeedBack._INFO_FEEDBACK + ', ' + row.feedbackId + ');"><img title="Đính kèm" src="/resources/images/icon_attach.png" width="32" heigh="32"></a>';;
		        	}
		        	return '';
		        }},
		        {field:'change', title:'<a href="javaScript:void(0);" onclick="return FeedBack.addLinkFeedback();"><img title="Thêm vấn đề" src="/resources/images/icon_add.png"/></a>', width: 50, align: 'center', formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return FeedBack.viewPopupInfoFeedBack(' + index + ', ' + FeedBack._INFO_FEEDBACK + ', ' + row.feedbackId + ');"><img title="Thông tin" src="/resources/images/icon-view.png" width="16" heigh="16"></a>';
			    	return html;
			    }}, 
		    ]],
		    onCheckAll: function(r) {
		    	FeedBack.hidenCheckboxGird(r, FeedBack._INFO_FEEDBACK);
	    	},
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#searchFeedbackResult');			   	 
		   	 	$(window).resize();
		   	 	$(".datagrid-header-check input[type=checkbox]").removeAttr("checked");
		   	 	if (data != null && data.rows != null) {
			   	 	FeedBack.hidenCheckboxGird(data.rows, FeedBack._INFO_FEEDBACK);
				}
		    }		    
		});
	},

	/**
	 * Add link them van de
	 * @author vuongmq
	 * @since 13/11/2015 
	 */
	addLinkFeedback: function() {
		window.location.href = '/feedback/viewDetail';
	},

	/**
	 * View popup van de
	 * @author vuongmq
	 * @param index
	 * @param type
	 * @param feedbackId
	 * @since 13/11/2015 
	 */
	viewPopupInfoFeedBack: function(index, type, feedbackId) {
		hideAllMessage();
		var row = null;
		if (type == FeedBack._INFO_FEEDBACK) {
			row = $('#grid').datagrid('getRows')[index];
		} else {
			row = $('#gridNeedDone').datagrid('getRows')[index];
		}
		if (row != undefined && row != null) {
			var titleView = '';
			var staffInfo = '';
			if (type == FeedBack._INFO_FEEDBACK) {
				staffInfo = row.staffCode + ' - ' + row.staffName;
				titleView = 'Thông tin vấn đề: ' + VTUtilJS.XSSEncode(staffInfo);
			} else {
				staffInfo = row.createCode + ' - ' + row.createName;
				titleView = 'Thông tin vấn đề người tạo: ' + VTUtilJS.XSSEncode(staffInfo);
			}
			$('#divDialogFeedback').css('visibility', 'visible');
			$('#popupInfoFeedback').dialog({
				title: titleView,
				width: 550,
				height: 'auto',
				onOpen: function() {
					$('#popContent').val(VTUtilJS.XSSEncode(row.content));
					$('#popCreateName').html(VTUtilJS.XSSEncode(row.createName));
					$('#popRemindDate').html(VTUtilJS.XSSEncode(row.remindDateStr));
					$('#popStaffName').html(VTUtilJS.XSSEncode(row.staffName));
					$('#popDoneDate').html(VTUtilJS.XSSEncode(row.doneDateStr));
					$('#popStatus').html(VTUtilJS.XSSEncode(FeedbackStatus.parseValue(row.result)));
					$('#popTypeName').html(VTUtilJS.XSSEncode(row.typeName));
					$('#popCustomerName').html(VTUtilJS.XSSEncode(row.customerName));
					if (row.item != null && row.item > 0) {
						var params = new Object();
						params.feedbackId = feedbackId;
						var url = '/feedback/item-attach';
						Utils.getJSONDataByAjaxNotOverlay(params, url, function(data) {
							var linkUrl = '';
							if (data != null && data.rows != null && data.rows.length > 0) {
								var rows = data.rows;
								for (var i = 0, sz = rows.length; i < sz; i++) {
									var currentMil = new Date().getTime();
									var path = rows[i].url;
									var fileName = '';
									if (!isNullOrEmpty(rows[i].title)) {
										fileName = rows[i].title;
									} else {
										fileName = path.substr(path.lastIndexOf('/') + 1);
									}
									linkUrl += ', ' + '<a target="_blank" href="' + imgServerSOPath + path +'?v=' + currentMil + '">' + fileName + '</a>';
								};
								linkUrl = linkUrl.replace(', ', '');
							}
							$('#lblAttach').html('Đính kèm: ');
							$('#popAttach').html(linkUrl);
						});
					} else {
						$('#lblAttach').html('');
						$('#popAttach').html('');
					}
				},
				onClose: function() {
					$('#popContent').val('');
					$('#popCreateName').html('');
					$('#popRemindDate').html('');
					$('#popStaffName').html('');
					$('#popDoneDate').html('');
					$('#popStatus').html('');
					$('#popTypeName').html('');
					$('#popCustomerName').html('');
					$('#popAttach').html('');
				}
			});
			$('#popupInfoFeedback').dialog('open');
		}
	},

	/**
	 * Duyet theo doi van de
	 * @author vuongmq
	 * @param type
	 * @since 13/11/2015 
	 */
	approvedFeedback: function(type) {
		hideAllMessage();
		var errMsg = '';
		var data = $('#grid').datagrid('getChecked');
		if (data == null || data.length == 0) {
			errMsg = 'Vui lòng chọn vấn đề cần xử lý.';
		}
		var lstId = new Array();
		for (var i = 0, sz = data.length; i < sz; i++) {
			if (data[i] != null && data[i].result == FeedbackStatus.COMPLETE) {
				lstId.push(data[i].id);
			}
		}
		if (errMsg.length > 0) {
			$('#errMsg').html(errMsg).show();
			return false;
		}
		var params = new Object();
		params.lstId = lstId;
		params.type = type;
		var message = 'Bạn có muốn duyệt danh sách vấn đề đã chọn?';
		if (type == FeedbackStatus.NEW) {
			message = 'Bạn có muốn từ chối danh sách vấn đề đã chọn?';
		}
		Utils.addOrSaveData(params, '/feedback/update-feedback', null, 'errMsg', function(data) {
			var tm = setTimeout(function() {
				//Load lai danh sach Feedback
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				FeedBack.searchFeedback();
			 }, 1500);
		}, null, null, null, message);
	},

	/**
	 * save theo doi van de
	 * @author vuongmq
	 * @since 21/11/2015 
	 */
	saveFeedback: function() {
		hideAllMessage();
		var msg = '';
		var typeParam = $('#typeFeedback').val();
		if (isNullOrEmpty(typeParam)) {
			msg = 'Vui lòng chọn loại vấn đề.';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Ngày nhắc nhở');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('fDate', 'Ngày nhắc nhở');
		}
		var fDate = $('#fDate').val();
		if (msg.length == 0) {
			var cDate = ReportUtils.getCurrentDateString();
			if (!Utils.compareDate(cDate, fDate)) {
				msg = 'Ngày nhắc nhở không được nhỏ hơn ngày hiện tại.';
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('popContent', 'Nội dung vấn đề');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('popContent', 'Nội dung vấn đề', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			if (FeedBack._mapStaff == null || FeedBack._mapStaff.keyArray == null || FeedBack._mapStaff.keyArray.length == 0) {
				msg = 'Vui lòng chọn người thực hiện.';
			}
		}
		if (msg.length > 0) {
			$('#errMsgUpload').html(msg).show();
			return false;
		}
		var content = $('#popContent').val();
		var params = new Object();
		params.typeParam = typeParam;
		params.fromDate = fDate + ':00';
		params.content = content;
		var lstStaffId = [];
		var len = FeedBack._mapStaff.keyArray.length;
		for (var i = 0; i < len; i++) {
			var key = FeedBack._mapStaff.keyArray[i];
			var mapCustomer = FeedBack._mapStaff.get(key);
			// put vao lst;
			var lstCustomerId = [];
			var lenCustomer = mapCustomer.keyArray.length;
			for (var j = 0; j < lenCustomer; j++) {
				var keyCustomer = mapCustomer.keyArray[j];
				lstCustomerId.push(keyCustomer);
			}
			var lstId = new Object();
			lstId.id = key;
			lstId.lstCustomerId = lstCustomerId;
			lstStaffId.push(lstId);
		}
		params.lstStaffId = lstStaffId;
		var message = 'Bạn có muốn tạo vấn đề này không?';
		/*Utils.addOrSaveData(params, '/feedback/save-feedback', null, 'errMsgUpload', function(data) {
			var tm = setTimeout(function() {
				$('#successMsg').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null, message);*/
		JSONUtil.saveData2(params, '/feedback/save-feedback', message, "errMsgUpload", function() {
			var tm = setTimeout(function() {
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				window.location.href = '/feedback/info';
			 }, 1500);
		});
	},

	/**BEGIN UPLOAD IMAGE**/
	/**
	 * initUploadImageElement file feedback
	 * @author vuongmq
	 * @param elementSelector
	 * @param elementId
	 * @param clickableElement
	 * @param fileExConfig
	 * @since 25/11/2015 
	 */
	initUploadImageElement: function(elementSelector, elementId, clickableElement, fileExConfig) {
		// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
		var previewNode = document.querySelector("#template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;
		previewNode.parentNode.removeChild(previewNode);

		UploadUtil.initializeUploadElement(elementSelector, elementId, {
			url: "/feedback/save-feedback",
			paramName: function(fileOrderNumber) {
				return 'images';
			},
			autoProcessQueue: false,
			parallelUploads: 1000,
			uploadMultiple: true,
			thumbnailWidth: 50,
			thumbnailHeight: 50,
			previewTemplate: previewTemplate,
			autoQueue: false,
			previewsContainer: "#previews",
			acceptedFiles: fileExConfig,
			//maxFilesize: 20,// MB
			clickable: clickableElement,
			successmultiple: function(files, responseData) {
				Utils.updateTokenForJSON(responseData);
				UploadUtil.hideTotalProgressBar();
				UploadUtil.loadAllFileUpload();
				if (responseData != undefined && isNullOrEmpty(responseData.errMsg)) {
					var failCount = 0;
					if (responseData.fail && responseData.fail.length > 0) {
						failCount = responseData.fail.length;
						//UploadUtil.readdFailFileForUpload(responseData.fail);
					}
					$('#divOverlay').hide();
					var msg = format(feedback_upload_success, (files.length - failCount) + '/' + files.length);
					$('#successMsg').html(msg + ' ' + msgCommon1).show();
					var tm = setTimeout(function() {
						$('#successMsg').html("").hide();
						clearTimeout(tm);
						window.location.href = '/feedback/info';
					 }, 2000);
				} else {
					$('#divOverlay').hide();
					$('#errMsgUpload').html(responseData.errMsg).show();
				}
			},
		});
		
	},

	/**
	 * uploadImageFileFeedbackfile feedback
	 * @author vuongmq
	 * @since 25/11/2015 
	 */
	uploadImageFileFeedback: function() {
		hideAllMessage();
		var msg = '';
		var typeParam = $('#typeFeedback').val();
		if (isNullOrEmpty(typeParam)) {
			msg = 'Vui lòng chọn loại vấn đề.';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Ngày nhắc nhở');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('fDate', 'Ngày nhắc nhở');
		}
		var fDate = $('#fDate').val();
		if (msg.length == 0) {
			var cDate = ReportUtils.getCurrentDateString();
			if (!Utils.compareDate(cDate, fDate)) {
				msg = 'Thời gian ngày nhắc nhở không được nhỏ hơn thời gian ngày hiện tại.';
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('popContent', 'Nội dung vấn đề');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('popContent', 'Nội dung vấn đề', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			if (FeedBack._mapStaff == null || FeedBack._mapStaff.keyArray == null || FeedBack._mapStaff.keyArray.length == 0) {
				msg = 'Vui lòng chọn người thực hiện.';
			}
		}
		if (msg.length == 0) {
			var filesErr = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ERROR);
			if (filesErr.length > 0) {
				msg = 'Tồn tại file không đúng định dạng.';
			}
		}
		if (msg.length > 0) {
			$('#errMsgUpload').html(msg).show();
			return false;
		}
		var content = $('#popContent').val();
		var params = new Object();
		params.typeParam = typeParam;
		params.fromDate = fDate + ':00';
		params.content = content;
		var lstStaffId = [];
		var len = FeedBack._mapStaff.keyArray.length;
		for (var i = 0; i < len; i++) {
			var key = FeedBack._mapStaff.keyArray[i];
			var mapCustomer = FeedBack._mapStaff.get(key);
			// put vao lst;
			var lstCustomerId = [];
			var lenCustomer = mapCustomer.keyArray.length;
			for (var j = 0; j < lenCustomer; j++) {
				var keyCustomer = mapCustomer.keyArray[j];
				lstCustomerId.push(keyCustomer);
			}
			var lstId = new Object();
			lstId.id = key;
			lstId.lstCustomerId = lstCustomerId;
			lstStaffId.push(lstId);
		}
		params.lstStaffId = lstStaffId;
		var message = 'Bạn có muốn tạo vấn đề này không?';
		var files = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (files && files instanceof Array && files.length != 0) {
			$.messager.confirm(jsp_common_xacnhan, message, function(r) {
				if (r) {
					var data = JSONUtil.getSimpleObject2(params);
					UploadUtil.updateAdditionalDataForUpload(data);
					var errView = 'errMsgUpload';
					var maxTotalSize = Number($('#maxTotalSize').val());
					maxTotalSize = maxTotalSize * UploadUtil._SIZE_1024;
					UploadUtil.startUploadFile(errView, maxTotalSize, true);
				}
			});
		} else {
			JSONUtil.saveData2(params, '/feedback/save-feedback', message, "errMsgUpload", function() {
				var tm = setTimeout(function() {
					$('#successMsg').html("").hide();
					clearTimeout(tm);
					window.location.href = '/feedback/info';
				 }, 2000);
			});	
		}
	},
	/** END UPLOAD IMAGE**/

	/**
	 * Init grid danh sach nhan vien - KH
	 * @author vuongmq
	 * @since 17/11/2015 
	 */
	initGridAddStaff: function() {
		$('#gridAddStaff').datagrid({  
		    url: '',
		    rownumbers: true,
//		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
//		    pageSize: 100,
//		    pageList: [100, 200, 500],
		    width: $(window).width() - 70,	    		    
		    //queryParams: params,
		    data: [],
		    fitColumns: true,
		    scrollbarSize: 0,
		    columns: [[
				{field: 'shopCode', title: 'Đơn vị', width: 200, align: 'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(value);
				}},
				{field: 'staffCode', title: 'Người thực hiện', width: 200, align: 'left', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
				{field: 'customerName', title: 'Khách hàng', width: 300, align: 'left', formatter: function(value, row, index) {
		        	return value; // vuongmq; 20/11/2015; khong XSS cho nay, da XSS rui
		        }},
		        {field:'change', title: '<a href="javaScript:void(0);" onclick="return FeedBack.addStaffFeedback();"><img title="Thêm nhân viên" src="/resources/images/icon_add.png" width="18" heigh="18"/></a>', width: 100, align: 'center', formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return FeedBack.addCustomerFeedback(' + index +');"><img title="Thêm khách hàng" src="/resources/images/icon_add.png" width="18" heigh="18"></a>&nbsp   &nbsp';
			    		html += '<a href="javascript:void(0)" onclick="return FeedBack.deleteStaffFeedback(' + index + ');"><img title="Xóa nhân viên" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
			    	return html;
			    }}, 
		    ]],
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#feedbackResultAddStaff');
		   	 	$(window).resize(); 
		    }		    
		});
	},

	/**
	 * popup Them nhan vien
	 * @author vuongmq
	 * @param callback
	 * @since 17/11/2015 
	 */
	popupAddStaffFeedback: function(callback) {
		var title = 'Chọn nhân viên';
		FeedBack._currentSearchCallback = null;
		FeedBack._mapMutilSelect = new Map();
		var html = $('#searchStyle2EasyUIDialogDiv').html();
		$('#searchStyle2EasyUIDialog').addClass('easyui-dialog');
		$('#searchStyle2EasyUIDialog').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width: 616,
	        height: 'auto',
	        onOpen: function() {
	        	ReportUtils.loadComboTree('cbxUnitPopupStaff', 'shopIdPopupStaff', $('#curShopIdPopupStaff').val(), function(shopId) {
					FeedBack.changeShopPopup(shopId, StaffSpecType.STAFF);
				});
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function() {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				$('#cbxUnitPopupStaff').focus();
				$('#checkBoxStaff').prop('disabled', true);
				FeedBack._currentSearchCallback = callback;
				FeedBack._isOpenDialog = true;
				
				var shortText = 'Đơn vị';
				var codeText = 'Mã nhân viên';
				var nameText = 'Tên nhân viên';
				var shortField = 'shopCode';
				var codeField = 'staffCode';
				var nameField = 'staffName';
				
				Utils.bindAutoButtonEx('#searchStyle2EasyUIDialog', 'btnSearchStyle2EasyUI');
				$('#searchStyle2EasyUIGrid').show();
				var url = '/feedback/search-staff-feedback';
				var params = new Object();
				params.shopId = $('#shopIdPopupStaff').val();
				params.typeId = $('#typePopupStaff').val();
				params.staffCode = $('#searchStyle2EasyUIName').val()
				var lstId = FeedBack.getLstIdMap(FeedBack._mapStaff);
				params.lstIdStr = lstId.join(',');
				FeedBack.changeCheckStaff();
				$('#searchStyle2EasyUIGrid').datagrid({
					url: url,
					autoRowHeight: true,
					rownumbers: true, 
					checkOnSelect: true,
					pagination: true,
					rowNum: 10,
					scrollbarSize: 0,
					pageList: [10, 50, 100, 200, 500],
					queryParams: params,
					width: ($('#searchStyle2EasyUIContainerGrid').width()),
				    columns:[[
				    	{field: shortField, title: shortText, align: 'left', width: 150, sortable: false, resizable: false, formatter: function(value, row, index) {
				        	  return Utils.XSSEncode(row.shopCode + ' - ' + row.shopName);
				        }}, 
				        {field: codeField, title: codeText, align: 'left', width: 150, sortable: false, resizable: false, formatter: function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				        }},  
				        {field: nameField, title: nameText, align: 'left', width: 190, sortable: false, resizable: false, formatter: function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				        }},  
				        {field: 'id', checkbox: true, align: 'center', width: 20, sortable: false, resizable: false},
				    ]],
				    onSelect: function(rowIndex, rowData) {
				    	var selectedId = rowData['id'];
				    	FeedBack._mapMutilSelect.put(selectedId, rowData);
				    },
				    onUnselect: function(rowIndex, rowData) {
				    	var selectedId = rowData['id'];
				    	FeedBack._mapMutilSelect.remove(selectedId);
				    },
				    onSelectAll: function(rows) {
				    	if ($.isArray(rows)) {
				    		for (var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['id'];
						    	FeedBack._mapMutilSelect.put(selectedId, row);
				    		}
				    	}
				    },
				    onUnselectAll: function(rows) {
				    	if ($.isArray(rows)) {
				    		for (var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['id'];
						    	FeedBack._mapMutilSelect.remove(selectedId);
				    		}
				    	}
				    },
				    onLoadSuccess: function() {
				    	Utils.bindAutoButtonEx('.easyui-dialog', 'btnSearchStyle2EasyUI');
				    	var easyDiv = '#searchStyle2EasyUIContainerGrid ';
				    	$(easyDiv + '.datagrid-header-rownumber').html(jsp_common_numerical_order);
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv + 'input:checkbox[name=id]').each(function() {
			    			var selectedId = this.value;
			    			if (FeedBack._mapMutilSelect.get(selectedId) != null || FeedBack._mapMutilSelect.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#searchStyle2EasyUIGrid').datagrid('selectRow', runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll == false) {
			    			$(easyDiv + 'td[field=id] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
				    	tabindex = 1;
			    		$('#searchStyle2EasyUIDialog input,#searchStyle2EasyUIDialog select,#searchStyle2EasyUIDialog button').each(function() {
				    		if (this.type != 'hidden') {
					    	    $(this).attr("tabindex", '');
								tabindex++;
				    		}
						});
			    		updateRownumWidthForDataGrid('#searchStyle2EasyUIContainerGrid');
			    		$(easyDiv + '.datagrid-header-rownumber').css('width', '50px');
				    	$(easyDiv + '.datagrid-cell-rownumber').css('width', '50px');
			    		setTimeout(function() {
			    			CommonSearchEasyUI.fitEasyDialog('searchStyle2EasyUIDialog');
			    		},1000);
			    		$('#checkBoxStaff').prop('disabled', false);
				    }
				});
				$('#btnSearchStyle2EasyUI').unbind('click');
				$('#btnSearchStyle2EasyUI').bind('click', function(event) {
					hideAllMessage();
					var params = new Object();
					if ($('#checkBoxStaff').is(':checked')) {
						params.type = StaffSpecType.SUPERVISOR;
					} else {
						params.shopId = $('#shopIdPopupStaff').val();
						params.typeId = $('#typePopupStaff').val();
					}
					params.staffCode = $('#searchStyle2EasyUIName').val();
					var lstId = FeedBack.getLstIdMap(FeedBack._mapStaff);
					params.lstIdStr = lstId.join(',');
					$('#searchStyle2EasyUIGrid').datagrid('load', params);
					$('#cbxUnitPopupStaff').focus();
				});
				$('#btnSearchStyle2EasyUIUpdate').show();
				$('#btnSearchStyle2EasyUIClose').hide();
				$('#btnSearchStyle2EasyUIUpdate').unbind('click');
				$('#btnSearchStyle2EasyUIUpdate').bind('click', function(event) {
					$('#errMsgSearchStyle2EasyUI').html('').hide();
        			if (FeedBack._mapMutilSelect == null || FeedBack._mapMutilSelect.size() <= 0) {
    	        		if (codeField == 'staffCode') {
    	        			$('#errMsgSearchStyle2EasyUI').html('Bạn chưa chọn nhân viên nào. Vui lòng chọn').show();
    	        		}
    	        		return false;
    	        	}
    	        	if (FeedBack._currentSearchCallback != null) {
    	        		FeedBack._currentSearchCallback.call(this, FeedBack._mapMutilSelect.valArray);
    	    			$('#searchStyle2EasyUIDialog').dialog('close');
    	    		}
				});
	        },
	        onClose: function() {
	        	$('#searchStyle2EasyUIDialog').dialog('destroy');
	        },
	        onDestroy: function() {
	        	var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function() {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				FeedBack._currentSearchCallback = null;
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();			
	        	$('#searchStyle2EasyUIDialogDiv').html(html);
	        	FeedBack._isOpenDialog = false;
	        }
	    });
		return false;
	},

	/**
	 * popup Them khach hang
	 * @author vuongmq
	 * @param index
	 * @param callback
	 * @since 19/11/2015 
	 */
	popupAddCustomerFeedback: function(index, callback) {
		var data = $('#gridAddStaff').datagrid('getRows')[index];
		if (data == null || data.length == 0) {
			return false;
		}
		var title = 'Chọn khách hàng cho nhân viên :' + VTUtilJS.XSSEncode(data.staffCode);
		FeedBack._customerCallback = null;
		FeedBack._mapMutilCustomer = new Map();
		var html = $('#searchCustomerDiv').html();
		$('#searchCustomerDialog').addClass('easyui-dialog');
		$('#searchCustomerDialog').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width: 616,
	        height: 'auto',
	        onOpen: function() {
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function() {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				$('#staffIdPopup').val(data.id); // staff Id cua dong duoc chon
				$('#checkBoxRouting').prop('disabled', true);

				var dataModel = {shopId: data.shopId};
				Utils.initUnitCbx('cbxShop', dataModel, 230, function (rec) {
					if (rec != null && rec.id != null && rec.shopCode != null) {
						$('#shopIdPopupCustomer').val(rec.id);
						$('#shopCodePopupCustomer').val(rec.shopCode);

						var url = '/feedback/search-customer-feedback';
						var params = new Object();
						var staffId = $('#staffIdPopup').val();
						if (data.objectType == StaffSpecType.STAFF) {
							disableCombo('cbxShop');
							$('#checkBoxRouting').attr('checked', true);
							params.type = StaffSpecType.STAFF;
							params.staffId = staffId;
						} else {
							enableCombo('cbxShop');
							params.shopCode = $('#shopCodePopupCustomer').val();
						}
						params.customerCode = $('#searchCustomerName').val();
						var lstId = FeedBack.getLstIdMapCustomer(FeedBack._mapStaff, staffId);
						params.lstIdStr = lstId.join(',');
						FeedBack.gridPopupCustomer(params, url);
			    	}	
				});
				$('#cbxShop').focus();
				FeedBack._customerCallback = callback;
				FeedBack._isOpenDialogCustomer = true;
				
				Utils.bindAutoButtonEx('#searchCustomerDialog', 'btnSearchCustomer');
				FeedBack.changeCheckRouting();
				
				$('#btnSearchCustomer').unbind('click');
				$('#btnSearchCustomer').bind('click', function(event) {
					hideAllMessage();
					var params = new Object();
					var staffId = $('#staffIdPopup').val();
					if ($('#checkBoxRouting').is(':checked')) {
						params.type = StaffSpecType.STAFF;
						params.staffId = staffId;
					} else {
						params.shopCode = $('#cbxShop').combobox('getValue');
						if (isNullOrEmpty(params.shopCode)) {
							$('#errMsgSearchCustomer').html('Bạn chưa chọn đơn vị nào. Vui lòng chọn').show();
    	        			return false;
						}
					}
					params.customerCode = $('#searchCustomerName').val();
					var lstId = FeedBack.getLstIdMapCustomer(FeedBack._mapStaff, staffId);
					params.lstIdStr = lstId.join(',');
					$('#searchCustomerGrid').datagrid('load', params);
					$('#cbxShop').focus();
				});
				$('#btnCustomerUpdate').show();
				$('#btnCustomerClose').hide();
				$('#btnCustomerUpdate').unbind('click');
				$('#btnCustomerUpdate').bind('click', function(event) {
					$('#errMsgSearchCustomer').html('').hide();
        			if (FeedBack._mapMutilCustomer == null || FeedBack._mapMutilCustomer.size() <= 0) {
    	        		$('#errMsgSearchCustomer').html('Bạn chưa chọn khách hàng nào. Vui lòng chọn').show();
    	        		return false;
    	        	}
    	        	if (FeedBack._customerCallback != null) {
    	        		FeedBack._customerCallback.call(this, FeedBack._mapMutilCustomer.valArray);
    	    			$('#searchCustomerDialog').dialog('close');
    	    		}
				});
	        },
	        onClose: function() {
	        	$('#searchCustomerDialog').dialog('destroy');
	        },
	        onDestroy: function() {
	        	var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function() {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				FeedBack._customerCallback = null;
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();			
	        	$('#searchCustomerDiv').html(html);
	        	$('#staffIdPopup').val('');
	        	$('#shopCodePopupCustomer').val('');
	        	FeedBack._isOpenDialogCustomer = false;
	        }
	    });
		return false;
	},

	/**
	 * Xu ly lay lstId cua map truyen vao
	 * @author vuongmq
	 * @param map
	 * @since 21/11/2015
	 */
	getLstIdMap: function(map) {
		var lstId = [];
		if (map != null && map.keyArray != null && map.keyArray.length > 0) {
			var len = map.keyArray.length;
			for (var i = 0; i < len; i++) {
				lstId.push(map.keyArray[i]);
			}
		}
		return lstId;
	},

	/**
	 * Xu ly lay lstId cua map truyen vao, lay danh sach map customer
	 * @author vuongmq
	 * @param map
	 * @since 21/11/2015
	 */
	getLstIdMapCustomer: function(map, staffId) {
		var lstId = [];
		if (map != null && map.keyArray != null && map.keyArray.length > 0) {
			var mapCustomer = map.get(staffId);
			lstId = FeedBack.getLstIdMap(mapCustomer);
		}
		return lstId;
	},

	/**
	 * Xu ly khi thay doi shop tren popup customer
	 * @author vuongmq
	 * @param params
	 * @param url
	 * @since 19/11/2015
	 */
	gridPopupCustomer: function(params, url) {
		var shortText = 'Mã khách hàng';
		var codeText = 'Tên khách hàng';
		var nameText = 'Địa chỉ';
		var shortField = 'shortCode';
		var codeField = 'customerName';
		var nameField = 'address';

		$('#searchCustomerGrid').show();
		$('#searchCustomerGrid').datagrid({
			url: url,
			autoRowHeight: true,
			rownumbers: true, 
			checkOnSelect: true,
			pagination: true,
			rowNum: 10,
			scrollbarSize: 0,
			pageList: [10, 50, 100, 200, 500],
			queryParams: params,
			width: ($('#searchCustomerContainerGrid').width()),
		    columns:[[
		    	{field: shortField, title: shortText, align: 'left', width: 150, sortable: false, resizable: false, formatter: function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		        }}, 
		        {field: codeField, title: codeText, align: 'left', width: 150, sortable: false, resizable: false, formatter: function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		        }},  
		        {field: nameField, title: nameText, align: 'left', width: 190, sortable: false, resizable: false, formatter: function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		        }},  
		        {field: 'id', checkbox: true, align: 'center', width: 20, sortable: false, resizable: false},
		    ]],
		    onSelect: function(rowIndex, rowData) {
		    	var selectedId = rowData['id'];
		    	FeedBack._mapMutilCustomer.put(selectedId, rowData);
		    },
		    onUnselect: function(rowIndex, rowData) {
		    	var selectedId = rowData['id'];
		    	FeedBack._mapMutilCustomer.remove(selectedId);
		    },
		    onSelectAll: function(rows) {
		    	if ($.isArray(rows)) {
		    		for (var i = 0; i < rows.length; i++) {
		    			var row = rows[i];
		    			var selectedId = row['id'];
				    	FeedBack._mapMutilCustomer.put(selectedId, row);
		    		}
		    	}
		    },
		    onUnselectAll: function(rows) {
		    	if ($.isArray(rows)) {
		    		for (var i = 0; i < rows.length; i++) {
		    			var row = rows[i];
		    			var selectedId = row['id'];
				    	FeedBack._mapMutilCustomer.remove(selectedId);
		    		}
		    	}
		    },
		    onLoadSuccess: function() {
		    	Utils.bindAutoButtonEx('.easyui-dialog', 'btnSearchCustomer');
		    	var easyDiv = '#searchCustomerContainerGrid ';
		    	$(easyDiv + '.datagrid-header-rownumber').html(jsp_common_numerical_order);
		    	var exitsAll = false;
	    		var runner = 0;
	    		$(easyDiv + 'input:checkbox[name=id]').each(function() {
	    			var selectedId = this.value;
	    			if (FeedBack._mapMutilCustomer.get(selectedId) != null || FeedBack._mapMutilCustomer.get(selectedId) != undefined) {
	    				exitsAll = true;
	    				$('#searchCustomerGrid').datagrid('selectRow', runner);
	    			}
	    			runner ++; 
	    		});
	    		if (exitsAll == false) {
	    			$(easyDiv + 'td[field=id] .datagrid-header-check input:checkbox').attr('checked', false);
		    	}
		    	
		    	tabindex = 1;
	    		$('#searchCustomerDialog input,#searchCustomerDialog select,#searchCustomerDialog button').each(function() {
		    		if (this.type != 'hidden') {
			    	    $(this).attr("tabindex", '');
						tabindex++;
		    		}
				});
	    		updateRownumWidthForDataGrid('#searchCustomerContainerGrid');
	    		$(easyDiv + '.datagrid-header-rownumber').css('width', '50px');
		    	$(easyDiv + '.datagrid-cell-rownumber').css('width', '50px');
	    		setTimeout(function() {
	    			CommonSearchEasyUI.fitEasyDialog('searchCustomerDialog');
	    		},1000);
	    		$('#checkBoxRouting').prop('disabled', false);
		    }
		});
	},

	/**
	 * Xu ly khi thay doi checkbox routing khach hang lay danh sach
	 * @author vuongmq
	 * @since 19/11/2015
	 */
	changeCheckRouting: function() {
		$('#checkBoxRouting').change(function() {
			hideAllMessage();
			var params = new Object();
			var staffId = $('#staffIdPopup').val();
			if ($(this).is(':checked')) {
				disableCombo('cbxShop');
				params.type = StaffSpecType.STAFF;
				params.staffId = staffId;
			} else {
				enableCombo('cbxShop');
				params.shopCode = $('#cbxShop').combobox('getValue');
				if (isNullOrEmpty(params.shopCode)) {
					$('#errMsgSearchCustomer').html('Bạn chưa chọn đơn vị nào. Vui lòng chọn').show();
        			return false;
				}
			}
			params.customerCode = $('#searchCustomerName').val();
			var lstId = FeedBack.getLstIdMapCustomer(FeedBack._mapStaff, staffId);
			params.lstIdStr = lstId.join(',');
			$('#searchCustomerGrid').datagrid('load', params);
		});
	},

	/**
	 * Xu ly khi thay doi checkbox lay danh sach
	 * @author vuongmq
	 * @since 18/11/2015
	 */
	changeCheckStaff: function() {
		$('#checkBoxStaff').change(function() {
			hideAllMessage();
			var params = new Object();
			if ($(this).is(':checked')) {
				disableCombo('cbxUnitPopupStaff');
				disableSelectbox('typePopupStaff');
				params.type = StaffSpecType.SUPERVISOR;
			} else {
				enableCombo('cbxUnitPopupStaff');
				enableSelectbox('typePopupStaff');
				params.shopId = $('#shopIdPopupStaff').val();
				params.typeId = $('#typePopupStaff').val();
			}
			params.staffCode = $('#searchStyle2EasyUIName').val();
			var lstId = FeedBack.getLstIdMap(FeedBack._mapStaff);
			params.lstIdStr = lstId.join(',');
			$('#searchStyle2EasyUIGrid').datagrid('load', params);
		});
	},

	/**
	 * Xu ly khi thay doi don vi; lay danh sach type staff
	 * @author vuongmq
	 * @param shopId
	 * @param typeCurrentUser
	 * @since 18/11/2015
	 */
	changeShopPopup: function(shopId, typeCurrentUser) {
		if (shopId != undefined && shopId != null) {
			$.getJSON('/commons/load-staff-type?shopId=' + shopId + '&type=' + typeCurrentUser, function(data) {
				$('#typePopupStaff').html('');
				if (data != undefined && data != null && data.rows != null && data.rows.length > 0) {
					var row = data.rows;
					if (row.length > 1) { 
						$('#typePopupStaff').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode('Chọn loại nhân viên') + '</option>');
					}
					for (var i = 0, sz = row.length; i < sz; i++) {
						$('#typePopupStaff').append('<option value = "' + Utils.XSSEncode(row[i].id) + '">' + Utils.XSSEncode(row[i].name) + '</option>');  
					}
				} else {
					$('#typePopupStaff').append('<option value = "-1">' + Utils.XSSEncode('Không có loại nhân viên') + '</option>');
				}
				$('#typePopupStaff').change();
			});
		}
	},

	/**
	 * Them nhan vien
	 * @author vuongmq
	 * @since 17/11/2015 
	 */
	addStaffFeedback: function() {
		hideAllMessage();
		FeedBack.popupAddStaffFeedback(function(data) {
			if (data != null && data.length > 0) {
				console.log('length: ' + FeedBack._mapMutilSelect.keyArray.length);
				if (FeedBack._mapStaff == null ||  FeedBack._mapStaff.keyArray == null || FeedBack._mapStaff.keyArray.length == 0) {
					FeedBack._mapStaff = new Map();
				}
				var len = FeedBack._mapMutilSelect.keyArray.length;
				for (var i = 0; i < len; i++) {
					var mapCustomer = new Map();
					var key = FeedBack._mapMutilSelect.keyArray[i];
					var rows = FeedBack._mapMutilSelect.get(key);
					var newData = new Object();
					newData.id = rows.id;
					newData.objectType = rows.objectType;
					newData.shopId = rows.shopId;
					newData.shopCode = rows.shopCode + ' - ' + rows.shopName;
					newData.staffCode = rows.staffCode + ' - ' + rows.staffName;
					newData.row = rows; // them se xu ly
					var numRow = $('#gridAddStaff').datagrid('getRows').length;
					$('#gridAddStaff').datagrid('insertRow', {index: numRow, row: newData});
					FeedBack._mapStaff.put(newData.id, mapCustomer);
				}

			}
		});
	},

	/**
	 * Them Khach hang
	 * @author vuongmq
	 * @since 17/11/2015 
	 */
	addCustomerFeedback: function(index) {
		hideAllMessage();
		FeedBack.popupAddCustomerFeedback(index, function(data) {
			if (data != null && data.length > 0) {
				console.log('length: ' + FeedBack._mapMutilCustomer.keyArray.length);
				var row = $('#gridAddStaff').datagrid('getRows')[index];
				if (row != null) {
					var customerInfoOld = row.customerName;
					var customerInfo = '';
					var customerViewGrid = '';
					var mapCustomer = new Map();
					var staffId = row.id;
					if (FeedBack._mapStaff != null && FeedBack._mapStaff.keyArray != null && FeedBack._mapStaff.keyArray.length > 0) {
						mapCustomer = FeedBack._mapStaff.get(staffId);
					}
					var len = FeedBack._mapMutilCustomer.keyArray.length;
					for (var i = 0; i < len; i++) {
						var key = FeedBack._mapMutilCustomer.keyArray[i];
						var rowCustomer = FeedBack._mapMutilCustomer.get(key);
						customerViewGrid = VTUtilJS.XSSEncode(rowCustomer.shortCode + ' - ' + rowCustomer.customerName) + '&nbsp &nbsp';
						customerViewGrid += '<a href="javascript:void(0)" onclick="return FeedBack.deleteCustomerFeedback(' + index + ', ' + key + ');"><img title="Xóa khách hàng" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
						customerInfo += ', ' + customerViewGrid;
						mapCustomer.put(key, rowCustomer);
					}
					FeedBack._mapStaff.put(staffId, mapCustomer);
					if (!isNullOrEmpty(customerInfoOld)) {
						row.customerName = customerInfoOld + customerInfo;
					} else {
						row.customerName = customerInfo.replace(', ', '');	
					}
					$('#gridAddStaff').datagrid('updateRow', {index: index, row: row});
				}
			}
		});
	},

	/**
	 * Delete row nhan vien
	 * @author vuongmq
	 * @param index
	 * @since 19/11/2015 
	 */
	deleteRowGridStaff: function(index) {
		$('#gridAddStaff').datagrid('deleteRow', index);
		$('#gridAddStaff').datagrid('loadData', $('#gridAddStaff').datagrid('getRows'));

	},

	/**
	 * Delete nhan vien
	 * @author vuongmq
	 * @param index
	 * @since 17/11/2015 
	 */
	deleteStaffFeedback: function(index) {
		hideAllMessage();
		$.messager.confirm(jsp_common_xacnhan, jsp_common_msg_deltete_nv, function(r) {
			if (r) {
				var row = $('#gridAddStaff').datagrid('getRows')[index];
				if (row != null) {
					FeedBack._mapStaff.remove(row.id);
				}
				FeedBack.deleteRowGridStaff(index);
			}
		});
	},

	

	/**
	 * Xoa Khach hang
	 * @author vuongmq
	 * @param index
	 * @param id
	 * @since 17/11/2015 
	 */
	deleteCustomerFeedback: function(index, id) {
		hideAllMessage();
		$.messager.confirm(jsp_common_xacnhan, jsp_common_msg_deltete_kh, function(r) {
			if (r) {
				var row = $('#gridAddStaff').datagrid('getRows')[index];
				if (row != null) {
					var mapCustomer = FeedBack._mapStaff.get(row.id);
					if (mapCustomer != null && mapCustomer.keyArray != null && mapCustomer.keyArray.length > 0) {
						var customerViewGrid = '';
						var customerInfo = '';
						mapCustomer.remove(id); // xoa KH hien tai
						var len = mapCustomer.keyArray.length;	
						// danh sach khach hang moi sau khi xoa
						for (var i = 0; i < len; i++) {
							var key = mapCustomer.keyArray[i];
							var rowCustomer = mapCustomer.get(key);
							customerViewGrid = VTUtilJS.XSSEncode(rowCustomer.shortCode + ' - ' + rowCustomer.customerName) + '&nbsp &nbsp';
							customerViewGrid += '<a href="javascript:void(0)" onclick="return FeedBack.deleteCustomerFeedback(' + index + ', ' + key + ');"><img title="Xóa khách hàng" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
							customerInfo += ', ' + customerViewGrid;
						}
						FeedBack._mapStaff.put(row.id, mapCustomer);
						row.customerName = customerInfo.replace(', ', '');
						$('#gridAddStaff').datagrid('updateRow', {index: index, row: row});	
					}
				}
			}
		});
	},

	/**
	 * Search danh sach van de can thuc hien
	 * @author vuongmq
	 * @since 19/11/2015 
	 */
	searchNeedDoneFeedback: function() {
		hideAllMessage();
		var params = FeedBack.getSearchParams(FeedBack._INFO_NEED_DONE);
		$('#gridNeedDone').datagrid('load', params);
	},

	/**
	 * Init van de can thuc hien
	 * @author vuongmq
	 * @since 19/11/2015 
	 */
	initGridNeedDoneFeedback: function() {
		var params = FeedBack.getSearchParams(FeedBack._INFO_NEED_DONE);
		$('#gridNeedDone').datagrid({  
		    url: '/feedback/need-done/search',
		    rownumbers: true,
		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
		    pageSize: 100,
		    pageList: [10, 50, 100, 200, 500],
		    width: $(window).width() - 70,	    		    
		    queryParams: params,
		    fitColumns: true,
		    scrollbarSize: 0,
		    columns: [[
				{field: 'id', checkbox: true, width: 50, align: 'center'},
				{field: 'customerName', title: 'Khách hàng', width: 300, align: 'left', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
				{field: 'typeName', title: 'Loại vấn đề', width: 120, align: 'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(value);
				}},
		        {field: 'content', title: 'Nội dung', width: 310, align: 'left', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field: 'result', title: 'Trạng thái', width: 130, align: 'left', formatter: function(value, row, index) {
		        	return FeedbackStatus.parseValue(value);
		        }},
		        {field: 'createCode', title: 'Người tạo', width: 220, align: 'left', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(row.createCode + ' - ' + row.createName);
		        }},
		        {field: 'remindDateStr', title: 'Ngày nhắc nhở', width: 130, align: 'center', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field: 'doneDateStr', title: 'Ngày thực hiện', width: 130, align: 'center', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field: 'item', title: 'Đính kèm', width: 80, align: 'center', formatter: function(value, row, index) {
		        	if (value != null && value > 0) {
		        		return '<a href="javascript:void(0)" onclick="return FeedBack.viewPopupInfoFeedBack(' + index + ', ' + FeedBack._INFO_NEED_DONE + ', ' + row.feedbackId + ');"><img title="Đính kèm" src="/resources/images/icon_attach.png" width="32" heigh="32"></a>';;
		        	}
		        	return '';
		        }},
		        {field:'change', title: '', width: 60, align: 'center', formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return FeedBack.viewPopupInfoFeedBack(' + index + ', ' + FeedBack._INFO_NEED_DONE + ', ' + row.feedbackId + ');"><img title="Thông tin" src="/resources/images/icon-view.png" width="16" heigh="16"></a>';
			    	return html;
			    }}, 
		    ]],
		    onCheckAll: function(r) {
		    	FeedBack.hidenCheckboxGird(r, FeedBack._INFO_NEED_DONE);
	    	},
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#searchNeedDoneFeedbackResult');			   	 
		   	 	$(window).resize();
		   	 	$(".datagrid-header-check input[type=checkbox]").removeAttr("checked");
		   	 	$(window).resize(); 
		   	 	if (data != null && data.rows != null) {
			   	 	FeedBack.hidenCheckboxGird(data.rows, FeedBack._INFO_NEED_DONE);
				}
		    }		    
		});
	},

	/**
	 * Hoan thanh van de can thuc hien
	 * @author vuongmq
	 * @since 13/11/2015 
	 */
	completeFeedback: function() {
		hideAllMessage();
		var errMsg = '';
		var data = $('#gridNeedDone').datagrid('getChecked');
		if (data == null || data.length == 0) {
			errMsg = 'Vui lòng chọn vấn đề cần xử lý.';
		}
		var lstId = new Array();
		for (var i = 0, sz = data.length; i < sz; i++) {
			if (data[i] != null && data[i].result == FeedbackStatus.NEW) {
				lstId.push(data[i].id);
			}
		}
		if (errMsg.length > 0) {
			$('#errMsg').html(errMsg).show();
			return false;
		}
		var params = new Object();
		params.lstId = lstId;
		params.type = FeedbackStatus.COMPLETE; // chuyen thanh hoan thanh van de
		var message = 'Bạn có muốn hoàn thành danh sách vấn đề đã chọn?';
		Utils.addOrSaveData(params, '/feedback/update-feedback', null, 'errMsg', function(data) {
			var tm = setTimeout(function() {
				//Load lai danh sach Feedback need done
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				FeedBack.searchNeedDoneFeedback();
			 }, 1500);
		}, null, null, null, message);
	},
}
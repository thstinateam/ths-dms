var POStockIn = {
	_IMPORTING: 1,
	_IMPORTED: 2,
	_FLAG_ADD_QUANTITY_FULL: true,
	searchGridInit: function() {
		$('#dg').datagrid({  
		    url:'/po/stock-in/getinfo',		    	    
		    singleSelect:true,	     
		    width: $(window).width() - 50,
		    height: 350,
		    rownumbers : true,
		    fitColumns:true,		    
		    queryParams:{
		    	//shopId:$('#shopId').val().trim(),
		    	shopCode : $('#cbxShop').combobox('getValue'), // vuongmq; 03/08/2015; them cobobox shopcode, luc dau khong can shopCode, se lay shop root mac dinh
		    	fromDate : $('#fromDate').val().trim(),
		    	toDate : $('#toDate').val().trim(),
		    	typePo : $('#typePo').val().trim(),
		    	status :$('#status').val().trim(),
		    	saleOrderNumber: $('#saleOrderNumber').val().trim(),
		    	poAutoNumber: $('#poAutoNumber').val().trim(),
		    	invoiceNumber: $('#invoiceNumberSearch').val().trim(),
		    },
		    showFooter: true,
		    columns:[[ 
				{field: 'saleOrderNumber', title: 'Số đơn hàng', width: 130, align: 'left', formatter: function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
				{field:'poCoNumber', title:'Số ASN', width:130, align:'left', formatter:function(value,row,index){
					return Utils.XSSEncode(value);
				}},
				{field: 'poAutoNumber', title: 'Số đơn đặt hàng', width: 130, align: 'left', formatter: function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
				{field: 'invoiceNumber', title: 'Số hóa đơn', width: 100, align: 'left', formatter: function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
				{field:'type', title:'Loại đơn', width:100, align:'left', formatter:function(value,row,index){
					if (value != undefined && value != null) {
						/*var giatri = PoType.parseValue(value);
						return Utils.XSSEncode(PoTypeText.parseValue(giatri));*/
						return Utils.XSSEncode(PoTypeText.parseValue(value));
					}
					return '';
				}}, 
				
				{field:'status', title:'Trạng thái', width:120, align:'left',formatter:function(value,row,index){
					if (value != undefined && value != null) {
						/*var giatri = PoStatus.parseValue(value);
						var type = PoType.parseValue(row.type);
						return Utils.XSSEncode(PoStatusTextOfType.parseValue(giatri, type));*/
						return Utils.XSSEncode(PoStatusTextOfType.parseValue(value, row.type));
					}
					return '';
				}},			
				{field:'requestedDateStr', title:'Ngày yêu cầu giao hàng', width:90, align:'center',formatter:function(value,row,index){
					/*if (row.orderdate == null) {
						return '';
					}
					return $.datepicker.formatDate('dd/mm/yy', new Date(row.orderdate));*/
					return Utils.XSSEncode(value);
				}},
				{field: 'receivedDateStr', title:'Ngày nhập', width:90, align:'center',formatter:function(value,row,index){
					/*if (row.importDate == null) {
						return '';
					}
					return $.datepicker.formatDate('dd/mm/yy', new Date(row.importDate));*/
					return Utils.XSSEncode(value);
				}},
				{field:'poVnmDateStr', title:'Ngày tạo', width:90, align:'center',formatter:function(value,row,index){
					/*if (row.poVnmDate==null) {
						return '';
					}
					return $.datepicker.formatDate('dd/mm/yy', new Date(row.poVnmDate));*/
					return Utils.XSSEncode(value);
				}},
				{field:'amount', title:'Tổng tiền', width:120, align:'right',formatter:CommonFormatter.numberFormatter},
				{field:'discount', title:'Chiết khấu', width:120, align:'right',formatter:CommonFormatter.numberFormatter},			
				{field:'import', title:'Nhập/ trả hàng', width:90, align:'center',formatter:function(value,row,index){
					var viewTitle = '';
					if (row.type != null && row.type == 'PO_CONFIRM') {
						viewTitle = 'Nhập hàng';
					} else if (row.type != null && row.type == 'RETURNED_SALES_ORDER') {
						viewTitle = 'Trả hàng';
					}
					return '<a href="javascript:void(0);" title="'+ Utils.XSSEncode(viewTitle) +'" onClick="return POStockIn.detail('+row.id+');">'+
								'<img src="/resources/images/icon_nhaphang_active.png" width="18" height="19" /></a>';
				}}
			]],    
			onLoadSuccess :function(data){			
				$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');		   	
			   	 updateRownumWidthForJqGrid('.easyui-dialog');
		   	 	$(window).resize();    		 
			}
		});
	},
	search: function(isSave) {
		var poCoNumber = $('#poCoNumber').val().trim();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '') {
			msg = 'Từ ngày không được để trống';
			$('#fromDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if (toDate == '') {
			msg = 'Đến ngày không được để trống';
			$('#toDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if (!Utils.isDate(fromDate, '/')) {
			msg = 'Từ ngày không hợp lệ. Vui lòng nhập lại';
			$('#fromDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if (!Utils.isDate(toDate, '/')) {
			msg = 'Đến ngày không hợp lệ. Vui lòng nhập lại';
			$('#toDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if (!Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		var shopCode = $('#cbxShop').combobox('getValue'); // vuongmq; 03/08/2015; them cobobox shopcode
		var typePo = $('#typePo').val().trim();
		var status = $('#status').val().trim();
		var saleOrderNumber = $('#saleOrderNumber').val().trim();
    	var poAutoNumber = $('#poAutoNumber').val().trim();
    	var invoiceNumber = $('#invoiceNumberSearch').val().trim();
		var params = new Object();
		params.shopCode = shopCode;
		params.poCoNumber = poCoNumber;
		params.fromDate = fromDate;
		params.toDate = toDate;
		params.typePo = typePo;
		params.status = status;
		params.saleOrderNumber = saleOrderNumber;
		params.poAutoNumber = poAutoNumber;
		params.invoiceNumber = invoiceNumber;
		$('#dg').datagrid('load', params);
		$("#InputTable").html('').hide();
		return false;
	},
	discountChange:function(){
		var totalmoney=0;
		if($('#totalDetail').html().trim()!=''){
			totalmoney=Utils.returnMoneyValue($('#totalDetail').html().trim());
		}
		var discountNotVat=0;
		if($('#discountNotVat').val().trim()!=''){
			discountNotVat=Utils.returnMoneyValue($('#discountNotVat').val().trim());
		}
		var vat=Math.round((totalmoney-discountNotVat)*0.1);//Làm tròn lên
		var pay=totalmoney-discountNotVat+vat;
		$('#discountDetail').html(formatCurrencyInterger(discountNotVat));
		$('#vatDetail').html(formatCurrencyInterger(vat));
		$('#payDetail').html(formatCurrencyInterger(pay));
	},
	detail : function(poVnmId) {
		var data = new Object();
		data.poVnmId = poVnmId;
		Utils.getHtmlDataByAjax(data, "/po/stock-in/stockin", function(data) {
			$("#InputTable").html(data).show();
			window.scrollTo(0, $(document).height());
		}, null, null);
	},

	//vuongmq; 11/08/2015;  nhap/ tra hang theo so luong nhap vao
	changeWarehouse: function(poVnmDetailLotId, index) {
		var params = new Object();
		var wareHouseId = $('#wareHouse'+poVnmDetailLotId).val();
		params.wareHouseId = wareHouseId;
		var row = $('#dg_detail').datagrid('getRows')[index];
		params.productId = row.productId;
		params.shopId = row.shopId;
		Utils.getJSONDataByAjaxNotOverlay(params, "/po/stock-in/warehouse-stockTotal", function(data) {
			if (data != undefined && data != null) {
				// neu khong co ton kho thi gan lai gia tri 0
				var quantity = 0;
				if (data.stockTotal != undefined && data.stockTotal != null && data.stockTotal.quantity != undefined && data.stockTotal.quantity != null) {
					quantity = data.stockTotal.quantity;
				}
				// gan gia tri ton kho moi quantity, khi chon kho
				var rowNew = $('#dg_detail').datagrid('getRows')[index];
				rowNew.quantityStock = quantity;
				rowNew.wareHouseIdTmp = wareHouseId;
				$('#dg_detail').datagrid('updateRow',{
									index: index,
									row: rowNew
								});
				$('#dg_detail').datagrid('refreshRow',index);
				Utils.formatCurrencyFor('quantityInput' + index);
				$('#wareHouse'+poVnmDetailLotId).customStyle(); // customStyle combobox
			}
		}, null, null);
	},

	//vuongmq; 11/08/2015;  nhap/ tra hang theo so luong nhap vao,
	stockin: function(poCoNumber, idvnm, status) {
		var invoiceNumber = $('#invoiceNumber').val().trim();
		//var _quantityCheck = $('#quantity_check').val().trim();
		//var _amountCheck = $('#amount_check').val().trim();
		//var quantityCheck = Utils.returnMoneyValue(_quantityCheck);
		//var amountCheck = Utils.returnMoneyValue(_amountCheck);
		var _discountCheck = $('#discountNotVat').val().trim();
		var discountCheck = Utils.returnMoneyValue(_discountCheck);
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		msg = Utils.getMessageOfRequireCheck('invoiceNumber', 'Số hoá đơn');
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('invoiceNumber','Số hoá đơn');
		}
		/*if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('quantity_check','Số lượng kiểm soát');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('amount_check','Giá trị kiểm soát');
		}*/

		//TUNGTT
		if (msg.length == 0) {
			var payDetail = 0;
			if ($('#payDetail').html().trim() != '') {
				payDetail=Utils.returnMoneyValue($('#payDetail').html().trim());
			}
			/*var amount_check = Utils.returnMoneyValue($('#amount_check').val().trim());
			if (parseInt(amount_check) != parseInt(payDetail)){
				msg = 'Giá trị kiểm soát nhập vào khác với số tiền thanh toán của đơn hàng, bạn hãy nhập đúng';
				//$('#amount_check').val('');
				$('#amount_check').focus();
			}*/
		}
		//Vuongmq; 11/08/2015; validate gia tri cua danh sach Nhap/ tra hang
		var lstPoVNMDetail = new Array();
		var typePo = $('#typePoVNM').val();
		var typeViewErr = '';
		var dateViewErr = '';
		if (typePo == PoType.PO_CUSTOMER_SERVICE || typePo == PoType.PO_CONFIRM) {
			// nhap hang
			typeViewErr = 'nhập';
			dateViewErr = 'về';
		} else if (typePo == PoType.RETURNED_SALES_ORDER || typePo == PoType.PO_CUSTOMER_SERVICE_RETURN) {
			// tra hang
			typeViewErr = 'trả';
			dateViewErr = 'đi';
		}
		var deliveryDate = $('#deliveryDate').val();
		/*if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('deliveryDate', 'Thời gian xe ' + dateViewErr);
		}*/
		if (deliveryDate.length > 0) {
			if (msg.length == 0) {
					msg = Utils.getMessageOfInvalidFormatDateNew('deliveryDate', 'Thời gian xe ' + dateViewErr);
			}
			/*if (msg.length == 0) {
				var cDate = ReportUtils.getCurrentDateString();
				if (!Utils.compareDate(cDate, deliveryDate)) {
					msg = 'Thời gian xe ' + dateViewErr + ' không được nhỏ hơn thời gian ngày hiện tại.';
				}
			}*/
			deliveryDate = deliveryDate + ':00';
		}
		if (msg.length == 0) {
			$('.quantityInputClazz').each(function() {
				//var quantityInput = Utils.returnMoneyValue($(this).val());
				var quantityInput = 0;
				var index = $(this).attr('qIndex');
				var row = $('#dg_detail').datagrid('getRows')[index];
				var gt = $(this).val();
	 			var gtTmp = gt.split('/');
	 			if (gtTmp.length > 2 || isNaN(gtTmp[0])) {
	 				$('#errMsg').html(format('Số lượng {0} không đúng định dạng số nguyên hoặc thùng lẻ.', typeViewErr)).show();
					$(this).focus();
					return false;
	 			}
	 			if (gtTmp[1] != undefined) {
	 				if (isNaN(gtTmp[1])) {
		 				$('#errMsg').html(format('Số lượng {0} dạng thùng lẻ không đúng định dạng số nguyên.', typeViewErr)).show();
						$(this).focus();
						return false;
					} else {
	 					quantityInput = Number(Utils.returnMoneyValue(gtTmp[0].trim())) * Number(row.convfact) + Number(Utils.returnMoneyValue(gtTmp[1].trim()));
					}
	 			} else {
	 				quantityInput = Number(Utils.returnMoneyValue(gtTmp[0].trim()));
	 			}
				var dongErr = Number(index) + 1;
				if (row.lstWarehouse != null && row.lstWarehouse.length <= 0) {
					msg = 'Không có kho dòng thứ ' + dongErr + ' nên không thể ' + typeViewErr + ' hàng';
					$('#errMsg').html(msg).show();
					$(this).focus();
					return false;
				}
				var slDaNhap = row.quantity - row.quantityReceived;
				if (Number(quantityInput) > Number(slDaNhap)) {
					msg = 'Bạn đã ' + typeViewErr + ' quá số lượng dòng thứ ' + dongErr;
					$('#errMsg').html(msg).show();
					$(this).focus();
					return false;
				}
				if (typePo == PoType.RETURNED_SALES_ORDER || typePo == PoType.PO_CUSTOMER_SERVICE_RETURN) {
					var tonKho = row.quantityStock;
					if (Number(quantityInput) > Number(tonKho)) {
						msg = 'Bạn đã ' + typeViewErr + ' quá số lượng tồn kho dòng thứ ' + dongErr;
						$('#errMsg').html(msg).show();
						$(this).focus();
						return false;
					}
				}
				// add vao danh sach lstPoVNMDetail
				var productInput = new Object();
				productInput.poVnmDetailLotId = row.poVnmDetailLotId;
				productInput.wareHouseId = $('#wareHouse'+row.poVnmDetailLotId).val();
				productInput.quantity = Number(quantityInput);
				lstPoVNMDetail.push(productInput);
			});
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.poVnmId = idvnm;
		params.poCoNumber = poCoNumber;
		params.invoiceNumber = invoiceNumber;
		params.status = status;
		//params.quantity_check = quantityCheck;
		//params.amount_check = amountCheck;
		params.invoiceDate = $('#creatDate').val();
		params.deliveryDate = deliveryDate;
		params.discount_check = discountCheck;
		params.lstPovnmDetail = lstPoVNMDetail;
		var msgDialog = 'Bạn có muốn lưu thông tin này?';
		JSONUtil.saveData2(params, "/po/stock-in/stockin-action", msgDialog, 'errMsg', function(data) {
			setTimeout(function() {
				POStockIn.search(true);
			}, 3000);
		}, null, null, null, null);
		/*Utils.addOrSaveData(params, "/po/stock-in/stockin-action", null,
				'errMsg', function(data) {
					setTimeout(function() {
						POStockIn.search(true);
					}, 3000);
				}, 'loading', null, false, null);*/
		return true;
	},

	removePOConfirm : function(poCoNumber, idvnm) {
		$('#errMsg').html('').hide();
		var params = new Object();
		params.poVnmId = idvnm;
		params.poCoNumber = poCoNumber;
		Utils.addOrSaveData(params, "/po/stock-in/remove-POConfirm", null,
				'errMsg', function(data) {
			if(data.error != undefined && data.error == false){
				$('#successMsg').html('Hủy đơn hàng thành công').show();
				setTimeout(function() {
					$('#successMsg').html('').hide();
					//window.location.reload();
					POStockIn.search();
				}, 2000);
			}else{
				$('#errMsg').html(data.errMsg).show();
			}
			
				}, 'loading', null, false, null);
		return true;
	},

	/** vuongmq; 03/08/2015 ;lay ngay chot cua shopCode */
	handleSelectShopPOStockIn: function() {
		var params= {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		Utils.getJSONDataByAjaxNotOverlay(params, '/commons/view-day-lock-select-shop' , function(data) {
			if (data != null) {
				$('#strLockDate').html(data.lockDate);
				$('#fromDate').val(data.fromDate); 
				$('#toDate').val(data.toDate);
			}
		});
		
	},

	/**
	 * fill in quantity full
	 * @author vuongmq
	 * @since 29/01/2016 
	 */
	addQuantityFull: function() {
		var rows = $('#dg_detail').datagrid('getRows');
		var i = 0;
		if (POStockIn._FLAG_ADD_QUANTITY_FULL) {
			$('.quantityInputClazz').each(function() {
				var value = Number(rows[i].quantity) - Number(rows[i].quantityReceived);
				$(this).val(formatQuantityEx(value, rows[i].convfact));
				i++;
			});
			POStockIn._FLAG_ADD_QUANTITY_FULL = false;
		} else {
			$('.quantityInputClazz').each(function() {
				$(this).val('');
			});
			POStockIn._FLAG_ADD_QUANTITY_FULL = true;
		}
	},

	/**
	 * blur quantity input
	 * @author vuongmq
	 * @since 29/01/2016 
	 */
	blurQuantityInput: function(rows) {
		for (var k = 0 ; k < rows.length ; k++) {
			if (rows[k].wareHouseId != null) {
				disableSelectbox('wareHouse' + rows[k].poVnmDetailLotId);
			}
			Utils.bindFormatOnTextfield('quantityInput' + k, Utils._TF_NUMBER_CONVFACT);
			Utils.formatCurrencyFor('quantityInput' + k);
		}
		$('.quantityInputClazz').live('blur',function() {
			var typePo = $('#typePoVNM').val();
			var typeViewErr = '';
			if (typePo == PoType.PO_CONFIRM) {
				typeViewErr = 'nhập'; 
			} else {
				typeViewErr = 'trả';
			}
 			var gt = $(this).val();
 			var gtTmp = gt.split('/');
 			if (gtTmp.length > 2 || isNaN(gtTmp[0])) {
 				$('#errMsg').html(format('Số lượng {0} không đúng định dạng số nguyên hoặc thùng lẻ.', typeViewErr)).show();
				$(this).focus();
				return false;
 			}
 			var index = $(this).attr('qindex');
 			var row = $('#dg_detail').datagrid('getRows')[index];
 			if (gtTmp[1] != undefined) {
 				if (isNaN(gtTmp[1])) {
	 				$('#errMsg').html(format('Số lượng {0} dạng thùng lẻ không đúng định dạng số nguyên.', typeViewErr)).show();
					$(this).focus();
					return false;
				} else {
 					var value = Number(Utils.returnMoneyValue(gtTmp[0].trim())) * Number(row.convfact) + Number(Utils.returnMoneyValue(gtTmp[1].trim()));
 					$(this).val(formatQuantityEx(value, row.convfact));
				}
 			} else {
 				var value = Number(Utils.returnMoneyValue(gtTmp[0].trim()));
 				$(this).val(formatQuantityEx(value, row.convfact));
 			}
		});
	},
};
var POCustomerServiceManage = {
	_xhrSave : null,
	tongtien: null,
	myListHT : null,
	_mapCheckPO: new Map(),
	onPOVNMStatusChange:function(checkAll) {
		Utils.onCheckAllChange(checkAll, 'POVNMStatus');
		var flag = false;
		$("input[name=poVnmStatus]:checked").each(function(){
			flag = true;
	   	});
		if(flag){
			$('#btnExportPOConfirmReport').removeAttr('disabled').removeClass('BtnGeneralDStyle');
		}else{
			$('#btnExportPOConfirmReport').attr('disabled','disabled').addClass('BtnGeneralDStyle');
		}
	},
	onPOVNMStatusCheckBoxChange:function(checkThis) {
		Utils.onCheckboxChangeForCheckAll(checkThis, 'POVNMStatus', 'allPOVNMStatus');
		var flag = false;
		$("input[name=poVnmStatus]:checked").each(function(){
			flag = true;
	   	});
		if(flag){
			$('#btnExportPOConfirmReport').removeAttr('disabled').removeClass('BtnGeneralDStyle');
		}else{
			$('#btnExportPOConfirmReport').attr('disabled','disabled').addClass('BtnGeneralDStyle');
		}
	},

	/** Search danh sach PO DVKH*/
	search:function(){
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		if(startDate == '__/__/____'){
			$('#startDate').val('');
		}
		if(endDate == '__/__/____'){
			$('#endDate').val('');
		}
		var msg = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày tạo từ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày tạo từ');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('endDate', 'Đến ngày');
		}
		if(msg.length == 0 && !Utils.compareDate(startDate, endDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#startDate').focus();
		}
		if(msg.length > 0){
			$('#errorSearchMsg').html(msg).show();
			return false;
		}	
		var shopId = $('#shopId').val().trim();
		var warehouseId = $('#warehouse').combobox('getValue');
		var poVnmStatus = $('#poVnmStatus').val();
		var poAutoNumber = $('#poAutoNumber').val().trim();
		var poNumber = $('#poNumber').val().trim();
		//var poCoNumber = $('#poCoNumber').val().trim();
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue'); // vuongmq; 03/08/2015; them cobobox shopcode
		//params.typePo = $('#typePo').val();
		//params.shopId = shopId;//$('#shopId').val().trim();
		params.warehouseId = warehouseId;
		params.lstPoVnmStatus = poVnmStatus;//$('#poVnmStatus').val().trim();
		params.startDate = startDate;//$('#startDate').val().trim();
		params.endDate = endDate;//$('#endDate').val().trim();
		params.poAutoNumber = poAutoNumber;//$('#poAutoNumber').val().trim();		
		params.poNumber = poNumber;//$('#poNumber').val().trim();		
		//params.poCoNumber = poCoNumber;//$('#poCoNumber').val().trim();
		Utils.getHtmlDataByAjax(params, "/po/manage-po-customer-service/search", function(data){
			try {
				var _data = JSON.parse(data);
				if(_data.error && _data.errMsg != undefined) {
					$('#errorSearchMsg').html(_data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errorSearchMsg').html('').hide();
					}, 3000);
				}
			} catch (e) {
				$('#divPOCSTable').html(data);
				$("#divPOCSConfirmTable").html('').show();
				$('#scrollSectionScroll').jScrollPane();
				$('#scrollBodySectionScroll').jScrollPane();
			}
			$('#btnExportPOConfirmReport').attr('disabled','disabled').addClass('BtnGeneralDStyle');
		});
		
		return false;
	},

	/** Xu ly treo PODVKH */
	suspend:function(){
		var msg  = '';
		$('#errMsg').html('').hide();		
		var lstPOSuspendId = new Array();
		$("input[name=poVnmStatus]:checked").each(function(){
			lstPOSuspendId.push(this.value);
	   	});		
		if(lstPOSuspendId.length == 0) {
			msg = 'Bạn chưa chọn đơn hàng nào. Vui lòng chọn đơn hàng';			
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}		
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue'); // vuongmq; 03/08/2015; them cobobox shopcode
		params.lstPOSuspendId = lstPOSuspendId;	   	
		var url = '/po/manage-po-customer-service/suspend';
		
		Utils.addOrSaveData(params, url, null, 'errMsg', function(data) {
			if(data.errMsg && data.errMsg != undefined) {
				$('#errMsg').html(data.errMsg).show();				
			} else {
				setTimeout(function(){
					POCustomerServiceManage.search();
				}, 3000);
			}
		});
	},
	getDetailPOCustomerService:function(poVnmId, saleOrderNumber){
		var params = new Object();
		params.poVnmId = poVnmId;
		params.saleOrderNumber = saleOrderNumber;
		Utils.getHtmlDataByAjax(params, "/po/manage-po-customer-service/detail-cs", function(data){
			try {
				var _data = JSON.parse(data);
				if(_data.error && _data.errMsg != undefined) {
					$('#errMsg').html(_data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
					}, 3000);
				}
			} catch(e) {
				$("#divPOCSConfirmTable").html(data).show();
				
			}
		});		
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
		return false;
	},
	getDetailPOConfirm:function(poVnmId, saleOrderNumber){
		var params = new Object();
		params.poVnmId = poVnmId;
		params.saleOrderNumber = saleOrderNumber;		
		var title = 'Chi tiết ASN: ' + Utils.XSSEncode(saleOrderNumber);
		Utils.getHtmlDataByAjax(params, '/po/manage-po-customer-service/detail-po', function(data) {
			$('#loadingView').css('visibility', 'hidden');
			$('#poCSConfirmTableDetailProductPopup').html('<div id="poCSConfirmTableDetailProduct">' + data + '</div>');
			$('#poCSConfirmTableDetailProduct').dialog({
				title: title,
		        closed: false,  
		        cache: false,
		        modal: true,
		        width: 700,
		        height: 'auto',
		        onOpen: function() {
		        	$('#poCSConfirmTableDetailProduct').addClass("easyui-dialog");
		        },
		        onClose: function() {
		        	$('#poCSConfirmTableDetailProduct').dialog("destroy");
		        }
			});
		}, null, 'GET');
	},
	
	/** Export danh sach PO dich vu khach hang*/
	exportCompare:function(){			
		var msg = '';
		$('#errMsg').html('').hide();	
		var lstPOSuspendId = new Array();
		$("input[name=poVnmStatus]:checked").each(function(){
			lstPOSuspendId.push(this.value);
		});		
		if(msg.length == 0) {
			if(lstPOSuspendId.length == 0) {
				msg = 'Bạn chưa chọn đơn hàng nào. Vui lòng chọn đơn hàng';
			}
		}
		if(msg != null && msg.length > 0) {
			$('#errMsg').html(msg).show();			
			return;
		}
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue'); // vuongmq; 03/08/2015; them cobobox shopcode
		//params.shopId = $('#shopId').val().trim();		
		params.startDate = $('#startDate').val().trim();
		params.endDate = $('#endDate').val().trim();		
		params.lstPOSuspendId = lstPOSuspendId;	
		params.reportCode = $('#function_code').val();
		$.messager.confirm('Xác nhận', 'Bạn có muốn xuất báo cáo đối chiếu ?', function(r){
			if(r){
				$('#divOverlay').show();
				$.ajax({
					type : "POST",
					url : '/po/manage-po-customer-service/export',
					data :($.param(params, true)),
					dataType : "json",
					success : function(data) {
						$('#divOverlay').hide();
						if(data.error && data.errMsg != undefined) {
							$('#errMsg').html(data.errMsg).show();
						} else {
							//window.location.href = data.view;
							var filePath = ReportUtils.buildReportFilePath(data.view);
							window.location.href = filePath;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(filePath);
							},2000);
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
						$('#divOverlay').hide();
					}
				});
			}
		});
		
	},
//	DEMO MASAN
	getGridUrl: function(shopCode,poVnmStatus,poNumber,startDate,endDate){
		return "/po/manage-po-customer-service/ncc/search?shopCode="+ encodeChar(shopCode) + "&poVnmStatus=" + poVnmStatus +"&poNumber=" + encodeChar(poNumber) + "&startDate=" + encodeChar(startDate) + "&endDate=" + encodeChar(endDate);
	},
	getDetailGridUrl: function(PoDVKHId,poNumber){
		$('#errMsg').html('').hide();
		$('#detailPoDVKH').html('Chi tiết sale order:');
		$('#divDetail').show();
		$('#detailPoDVKH').html($('#detailPoDVKH').html() + " " +poNumber);
		POCustomerServiceManage.tongtien = 0;
		var url = POCustomerServiceManage.viewDetailChanged(PoDVKHId);
		$('#detailGrid').datagrid({
			url : url,
			pageList  : [10,20,30],
			rownumbers : true,
			width: $('.GridSection').width()-20,
			height:'auto',
			scrollbarSize : 0,
			columns:[[  
            	{field: 'productCode',title:'Mã hàng', width:100,align:'left',sortable : false,resizable : false,
            		formatter:function(value,rows){
            			return Utils.XSSEncode(rows.product.productCode);
                	} 
            	},
            	{field: 'productName',title:'Tên hàng', width:200,align:'left',sortable : false,resizable : false,
            		formatter:function(value,rows){
            			return Utils.XSSEncode(rows.product.productName);
                	} 
            	},
            	{field: 'price',title:'Đơn giá', width:100,align:'left',sortable : false,resizable : false,
            		formatter:PODVKHNCCFormatter.priceFormat
            	},
            	{field: 'quantity',title:'Số lượng', width:100,align:'center',sortable : false,resizable : false,
            		formatter:PODVKHNCCFormatter.quantityFormat
            	},
            	{field: 'amount',title:'Tổng tiền', width:100,align:'center',sortable : false,resizable : false,
            		formatter:PODVKHNCCFormatter.moneyFormat
            	},
            ]],
            onClickRow : function(rowIndex, rowData){
	        },
			method : 'GET',
            onLoadSuccess:function(){
            	$('#detailChangedGrid .datagrid-header-rownumber').html('STT');
    	    	updateRownumWidthForDataGrid('#detailChangedGrid');
            	 $('#tongtien').html(formatCurrency(POCustomerServiceManage.tongtien));
            	 POCustomerServiceManage.tongtien = 0;
				 $(window).scrollTop($(window).height()+100);
            	 $('#detailGrid').datagrid('resize');
            }
		});
	},
	changePODVKH : function(idPODVKH){
		var params = new Object();
		params.idPODVKH = idPODVKH;
		$.messager.confirm('Xác nhận', 'Bạn có muốn chuyển xuống NPP?', function(r){
			if(r){
				$.ajax({
					type : "POST",
					url : "/po/manage-po-customer-service/ncc/change",  //url lam nhiem vu chuyen
					data : ($.param(params, true)),
					dataType : "json",
					success : function(data) {
						if(!data.error) {
							$('#successMsg1').html('Bạn đã chuyển thành công').show();
							var tm = setTimeout(function() {
								$('#successMsg1').html('Bạn đã chuyển thành công').hide();
								clearTimeout(tm);
							}, 3000);
							POCustomerServiceManage.searchNCC();
						}else{
							var tm = setTimeout(function() {
								$('#errMsg').html(data.errMsg).show();
								clearTimeout(tm);
							}, 300);
						} 
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
					}
				});
			}
		});
		return false;
	},
	breakLine:function(){
		num = $('#errMsg').html().split('.').length;
		for(var j = 0;j<num;j++){
			if($('#errMsg').html().length > 20){
				$('#errMsg').html($('#errMsg').html().replace('.','</br>'));
			}
		}
	},
	getFullDetailPOConfirm:function(poVnmId, saleOrderNumber){
		var params = new Object();
		params.poVnmId = poVnmId;
		params.saleOrderNumber = saleOrderNumber;		
		var title = 'Chi tiết sale order: ' + Utils.XSSEncode(saleOrderNumber);
		Utils.getJSONDataByAjaxNotOverlay(params, '/po/manage-po-customer-service/detail-po-full', function(data) {
			var hiddenRow = true;
			if(data.lstData!=null && data.lstData.length>10){
				hiddenRow = false;
			}
			if(data.error !=undefined && data.error == false){
				$('#fullDettailPoVnmComfirm').dialog({
					title: title,
					closed : false,
					cache : false,
					modal : true,
					width : 800,
					height : 420,
					onOpen: function(){	
						$('#searchStyleShopGrid').datagrid({
							data: data.lstData,
							rownumbers : true,	
							fitColumns:true,
							pagination:false,
							pageNumber : 1,
							singleSelect:true,
							autoWidth: true,
							height : 275,
						    columns:[[  
						     	{field: 'lineSaleOrder', title: 'Số dòng sale order', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value,rowData,rowIndex) {
						        	return Utils.XSSEncode(value);
						        }},
						         {field: 'linePo', title: 'Số dòng đơn đặt', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value,rowData,rowIndex) {
						        	return Utils.XSSEncode(value);
						        }},
						        {field:'productCode',title:'Mã sản phẩm',align:'left', width:80,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
						        	return Utils.XSSEncode(value);
						        }},
						        {field:'productName',title:'Tên sản phẩm',align:'left', width:150, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
						        	return Utils.XSSEncode(value);
						        }},
						        /*{field:'productType',title:'Loại sản phẩm',align:'left', width:90, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
						        	if (value != undefined && value != null) {
						        		return ProductTypeVNM.parseValue(value);
						        	}
						        }},*/
						        {field : 'price', title : 'Đơn giá', align : 'right', width: 80, sortable : false, resizable : false, formatter : function(value, rowData, index){
									return formatCurrency(value);
								}},
								{field : 'quantity', title : 'Số lượng', align : 'right', width: 80, sortable : false, resizable : false, formatter : function(value, rowData, index){
									if (value != undefined && value != null) {
										value = Math.abs(value);
									}
									return formatQuantityEx(value, rowData.convfact);
								}},
								{field : 'amount', title : 'Tổng tiền', align : 'right', width: 80, sortable : false, resizable : false, formatter : function(value, rowData, index){
									if (value != undefined && value != null) {
										value = Math.abs(value);
									}
									return formatCurrency(value);
								}},
								{field : 'scrollbar', title : '', align : 'right', width: 11, sortable : false, hidden:hiddenRow, resizable : false, formatter : function(value, rowData, index){
								}}
						    ]],	    
						    onLoadSuccess :function(data){	 
						    	$('#errMsg').html('').hide();
						    	$('.datagrid-header-rownumber').html('STT');	    		    	
						    	updateRownumWidthForJqGrid('.easyui-dialog');
					    		$(window).resize();
						    }
						});
						var totalQuantity = 0;
						var totalAmount = 0;
						if (data.totalQuantity != undefined && data.totalQuantity != null) {
							totalQuantity = Math.abs(data.totalQuantity);
						}
						if (data.totalAmount != undefined && data.totalAmount != null) {
							totalAmount = Math.abs(data.totalAmount);
						}
						$('#spTotalQuantity').html(formatCurrency(totalQuantity.toString()));
						$('#spTotalAmount').html(formatCurrency(totalAmount));
					},
					buttons: [{
						 text:'Đóng',
						 handler:function(){
							 $('searchStyleShopContainerGrid').html('<table id="searchStyleShopGrid"></table>').show();
							 $('searchStyleShopContainerGrid').change();
							 $('#fullDettailPoVnmComfirm').dialog('close');
						 }
					}],
				 	onBeforeClose: function() {
			        },
			        onClose : function(){
			        	$('searchStyleShopContainerGrid').html('<table id="searchStyleShopGrid"></table>').show();
						$('searchStyleShopContainerGrid').change();
			        }
				});
			}
		}, null);
		$('html, body').animate({ scrollTop: 0 }, 1000);
	},
	
	closeDialogFullDetailPoVnmComfirm: function(){
		$('searchStyleShopContainerGrid').html('<table id="searchStyleShopGrid"></table>').hide();
		
	},
	/** vuongmq; 03/08/2015 ;lay ngay chot cua shopCode */
	handleSelectShopPOVNM: function() {
		var params= {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		Utils.getJSONDataByAjaxNotOverlay(params, '/commons/view-day-lock-select-shop' , function(data) {
			if (data != null) {
				$('#strLockDate').html(data.lockDate);
				$('#startDate').val(data.fromDate); 
				$('#endDate').val(data.toDate);
			}
		});
		
	},
	
};
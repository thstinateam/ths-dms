/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/po/vnm.manage-po-auto.js
 */
var POAutoManage = {
	_searchParams: null,
	
	onCheckAllChange:function(checkAll) {
		Utils.onCheckAllChange(checkAll, 'CheckAllPoAuto');
	},
	onChangePoAutoCheckbox: function(checkThis) {
		Utils.onCheckboxChangeForCheckAll(checkThis, 'CheckAllPoAuto', 'allPOAutoCheck');
	},
	search:function(callback){		
		$('#poAutoDetailTableDiv').html('');
		$('#ErrorMsgStyle').hide();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		if(startDate == '__/__/____'){
			$('#startDate').val('');
		}
		if(endDate == '__/__/____'){
			$('#endDate').val('');
		}
		var msg = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày tạo từ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày tạo từ');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('endDate', 'Đến ngày');
		}
		if(msg.length == 0 && !Utils.compareDate(startDate, endDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#startDate').focus();
		}
		if(msg.length > 0){
			$('#errorSearchMsg').html(msg).show();
			return false;
		}		
		var shopCodeAndName = $('#shopCode').val().trim();
		var poNumber = $('#poNumber').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		var approveStatus = $('#approveStatus').val();
		var params = new Object();
		if (poNumber != null && poNumber != undefined && poNumber.length > 0){
			params.poNumber = poNumber;
		}
		if (approveStatus != null && approveStatus != undefined && approveStatus.length > 0){
			params.approveStatus = approveStatus;
		}
		if (startDate != null && startDate != undefined && startDate.length > 0){
			params.startDate = startDate;
		}
		if (endDate != null && endDate != undefined && endDate.length > 0){
			params.endDate = endDate;
		}
		if ($('#shopCode').val().trim().length > 0) {
			params.shopCode = $('#shopCode').val().split('-')[0].trim();
		}
		POAutoManage._searchParams = params;
		Utils.getHtmlDataByAjax(params, "/po/manage-po-auto/search", function(data){
			$("#divPOAutoTable").html(data).show();
			Utils.resizeHeightAutoScrollCross('listPoAutoSearch');
			var status = $('#approveStatus').val();
			if(status == 1){
				$('#btnHuy').hide();
				$('#btnDuyet').hide();
				$('#btnXml').show();
			}else{
				$('#btnHuy').show();
				$('#btnDuyet').show();
				$('#btnXml').hide();
			}
			
			if(callback!= null && callback!= undefined){
				callback.call(this);
			}
		}, 'searchLoading', null);
		
		return false;
	},
	approvePOAuto:function(type){
		$('#ErrorMsgStyle').hide();
		var listChoosePOAutoId = new Array();
		$('input:checkbox[name=choosePOAuto]:checked').each(function() {
			listChoosePOAutoId.push(this.value);
		});
		if(listChoosePOAutoId.length == 0) {
			$('#errMsg1').html('Vui lòng chọn đơn hàng').show();
			return;
		}
		var params = null;
		if (POAutoManage._searchParams) {
			params = POAutoManage._searchParams;
		} else {
			params = {};
		}
		params.listChoosePOAutoId = listChoosePOAutoId;
		var title = '';
		if(type == 1){
			title= 'Bạn có muốn duyệt đơn hàng này?';
		}else{
			title= 'Bạn có muốn hủy đơn hàng này?';
		}
		params.type = type;
		Utils.addOrSaveData(params, '/po/manage-po-auto/approve-po', null, 'errMsg1', function(data) {
			var tm = setTimeout(function(){
				POAutoManage.search();
				clearTimeout(tm);
			}, 3000);
		}, 'ApproveAndRefuseLoading',null,null,title, function(dataErr){
			if(dataErr.errorType ==2){
				POAutoManage.search(function(){
					$('#errMsg1').html(dataErr.errMsg).show();
				});
			}
		});
	},
	loadPOAutoDetail:function(poAutoId, poNumber){
		$('#ErrorMsgStyle').hide();
		var params = new Object();
		params.poAutoId = poAutoId;
		params.poNumber = poNumber;
		Utils.getHtmlDataByAjax(params, "/po/manage-po-auto/load-po-detail", function(data){
			try {
				var _data = JSON.parse(data);
				if(_data.error && _data.errMsg != undefined) {
					$('#errorSearchMsg').html(_data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errorSearchMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				}
			} catch(e) {
				$("#poAutoDetailTableDiv").html(data).show();
				$('#listPoAutoDetail').jScrollPane();
			}
		}, null, null);
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
		return false;
	},
	exportPOAutoReport:function(poAutoId){
		$('#ErrorMsgStyle').hide();
		var params = new Object();
		var shopId = $('#shopIdId').val().trim();
		params.poAutoId = poAutoId;
		params.shopId = shopId;
		var url = '/po/manage-po-auto/export';
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				} else {
					window.location.href = data.view;
					setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                    CommonSearch.deleteFileExcelExport(data.view);
					},2000);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$('#divOverlay').hide();
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	exportXml:function(){
		$('#ErrorMsgStyle').hide();
		var listChoosePOAutoId = new Array();
		$('input:checkbox[name=choosePOAuto]:checked').each(function() {
			listChoosePOAutoId.push(this.value);
		});
		if(listChoosePOAutoId.length == 0) {
			$('#errMsg1').html('Vui lòng chọn đơn hàng').show();
			return;
		}
		$('#divOverlay').show();
		var params = null;
		if (POAutoManage._searchParams) {
			params = POAutoManage._searchParams;
		} else {
			params = {};
		}
		params.listChoosePOAutoId = listChoosePOAutoId;
		var kData = $.param(params, true);
		$.ajax({
			type : "POST",
			url : "/po/manage-po-auto/export-xml",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();

				if (!data.error) {
					window.location.href = data.path;
					setTimeout(function() { // Set timeout để đảm bảo file
						// load lên hoàn tất
						CommonSearch.deleteFileExcelExport(data.path);
					}, 2000);
				} else {
					$('#errMsg1').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/po/vnm.manage-po-auto.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/po/vnm.manage-po-customer-service.js
 */
var POCustomerServiceManage = {
	_xhrSave : null,
	tongtien: null,
	myListHT : null,
	_mapCheckPO: new Map(),
	onPOVNMStatusChange:function(checkAll) {
		Utils.onCheckAllChange(checkAll, 'POVNMStatus');
		var flag = false;
		$("input[name=poVnmStatus]:checked").each(function(){
			flag = true;
	   	});
		if(flag){
			$('#btnExportPOConfirmReport').removeAttr('disabled').removeClass('BtnGeneralDStyle');
		}else{
			$('#btnExportPOConfirmReport').attr('disabled','disabled').addClass('BtnGeneralDStyle');
		}
	},
	onPOVNMStatusCheckBoxChange:function(checkThis) {
		Utils.onCheckboxChangeForCheckAll(checkThis, 'POVNMStatus', 'allPOVNMStatus');
		var flag = false;
		$("input[name=poVnmStatus]:checked").each(function(){
			flag = true;
	   	});
		if(flag){
			$('#btnExportPOConfirmReport').removeAttr('disabled').removeClass('BtnGeneralDStyle');
		}else{
			$('#btnExportPOConfirmReport').attr('disabled','disabled').addClass('BtnGeneralDStyle');
		}
	},

	/** Search danh sach PO DVKH*/
	search:function(){
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		if(startDate == '__/__/____'){
			$('#startDate').val('');
		}
		if(endDate == '__/__/____'){
			$('#endDate').val('');
		}
		var msg = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày tạo từ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày tạo từ');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('endDate', 'Đến ngày');
		}
		if(msg.length == 0 && !Utils.compareDate(startDate, endDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#startDate').focus();
		}
		if(msg.length > 0){
			$('#errorSearchMsg').html(msg).show();
			return false;
		}	
		var shopId = $('#shopId').val().trim();
		var warehouseId = $('#warehouse').combobox('getValue');
		var poVnmStatus = $('#poVnmStatus').val();
		var poAutoNumber = $('#poAutoNumber').val().trim();
		var poNumber = $('#poNumber').val().trim();
		//var poCoNumber = $('#poCoNumber').val().trim();
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue'); // vuongmq; 03/08/2015; them cobobox shopcode
		//params.typePo = $('#typePo').val();
		//params.shopId = shopId;//$('#shopId').val().trim();
		params.warehouseId = warehouseId;
		params.lstPoVnmStatus = poVnmStatus;//$('#poVnmStatus').val().trim();
		params.startDate = startDate;//$('#startDate').val().trim();
		params.endDate = endDate;//$('#endDate').val().trim();
		params.poAutoNumber = poAutoNumber;//$('#poAutoNumber').val().trim();		
		params.poNumber = poNumber;//$('#poNumber').val().trim();		
		//params.poCoNumber = poCoNumber;//$('#poCoNumber').val().trim();
		Utils.getHtmlDataByAjax(params, "/po/manage-po-customer-service/search", function(data){
			try {
				var _data = JSON.parse(data);
				if(_data.error && _data.errMsg != undefined) {
					$('#errorSearchMsg').html(_data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errorSearchMsg').html('').hide();
					}, 3000);
				}
			} catch (e) {
				$('#divPOCSTable').html(data);
				$("#divPOCSConfirmTable").html('').show();
				$('#scrollSectionScroll').jScrollPane();
				$('#scrollBodySectionScroll').jScrollPane();
			}
			$('#btnExportPOConfirmReport').attr('disabled','disabled').addClass('BtnGeneralDStyle');
		});
		
		return false;
	},

	/** Xu ly treo PODVKH */
	suspend:function(){
		var msg  = '';
		$('#errMsg').html('').hide();		
		var lstPOSuspendId = new Array();
		$("input[name=poVnmStatus]:checked").each(function(){
			lstPOSuspendId.push(this.value);
	   	});		
		if(lstPOSuspendId.length == 0) {
			msg = 'Bạn chưa chọn đơn hàng nào. Vui lòng chọn đơn hàng';			
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}		
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue'); // vuongmq; 03/08/2015; them cobobox shopcode
		params.lstPOSuspendId = lstPOSuspendId;	   	
		var url = '/po/manage-po-customer-service/suspend';
		
		Utils.addOrSaveData(params, url, null, 'errMsg', function(data) {
			if(data.errMsg && data.errMsg != undefined) {
				$('#errMsg').html(data.errMsg).show();				
			} else {
				setTimeout(function(){
					POCustomerServiceManage.search();
				}, 3000);
			}
		});
	},
	getDetailPOCustomerService:function(poVnmId, saleOrderNumber){
		var params = new Object();
		params.poVnmId = poVnmId;
		params.saleOrderNumber = saleOrderNumber;
		Utils.getHtmlDataByAjax(params, "/po/manage-po-customer-service/detail-cs", function(data){
			try {
				var _data = JSON.parse(data);
				if(_data.error && _data.errMsg != undefined) {
					$('#errMsg').html(_data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
					}, 3000);
				}
			} catch(e) {
				$("#divPOCSConfirmTable").html(data).show();
				
			}
		});		
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
		return false;
	},
	getDetailPOConfirm:function(poVnmId, saleOrderNumber){
		var params = new Object();
		params.poVnmId = poVnmId;
		params.saleOrderNumber = saleOrderNumber;		
		var title = 'Chi tiết ASN: ' + Utils.XSSEncode(saleOrderNumber);
		Utils.getHtmlDataByAjax(params, '/po/manage-po-customer-service/detail-po', function(data) {
			$('#loadingView').css('visibility', 'hidden');
			$('#poCSConfirmTableDetailProductPopup').html('<div id="poCSConfirmTableDetailProduct">' + data + '</div>');
			$('#poCSConfirmTableDetailProduct').dialog({
				title: title,
		        closed: false,  
		        cache: false,
		        modal: true,
		        width: 700,
		        height: 'auto',
		        onOpen: function() {
		        	$('#poCSConfirmTableDetailProduct').addClass("easyui-dialog");
		        },
		        onClose: function() {
		        	$('#poCSConfirmTableDetailProduct').dialog("destroy");
		        }
			});
		}, null, 'GET');
	},
	
	/** Export danh sach PO dich vu khach hang*/
	exportCompare:function(){			
		var msg = '';
		$('#errMsg').html('').hide();	
		var lstPOSuspendId = new Array();
		$("input[name=poVnmStatus]:checked").each(function(){
			lstPOSuspendId.push(this.value);
		});		
		if(msg.length == 0) {
			if(lstPOSuspendId.length == 0) {
				msg = 'Bạn chưa chọn đơn hàng nào. Vui lòng chọn đơn hàng';
			}
		}
		if(msg != null && msg.length > 0) {
			$('#errMsg').html(msg).show();			
			return;
		}
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue'); // vuongmq; 03/08/2015; them cobobox shopcode
		//params.shopId = $('#shopId').val().trim();		
		params.startDate = $('#startDate').val().trim();
		params.endDate = $('#endDate').val().trim();		
		params.lstPOSuspendId = lstPOSuspendId;	
		params.reportCode = $('#function_code').val();
		$.messager.confirm('Xác nhận', 'Bạn có muốn xuất báo cáo đối chiếu ?', function(r){
			if(r){
				$('#divOverlay').show();
				$.ajax({
					type : "POST",
					url : '/po/manage-po-customer-service/export',
					data :($.param(params, true)),
					dataType : "json",
					success : function(data) {
						$('#divOverlay').hide();
						if(data.error && data.errMsg != undefined) {
							$('#errMsg').html(data.errMsg).show();
						} else {
							//window.location.href = data.view;
							var filePath = ReportUtils.buildReportFilePath(data.view);
							window.location.href = filePath;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(filePath);
							},2000);
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
						$('#divOverlay').hide();
					}
				});
			}
		});
		
	},
//	DEMO MASAN
	getGridUrl: function(shopCode,poVnmStatus,poNumber,startDate,endDate){
		return "/po/manage-po-customer-service/ncc/search?shopCode="+ encodeChar(shopCode) + "&poVnmStatus=" + poVnmStatus +"&poNumber=" + encodeChar(poNumber) + "&startDate=" + encodeChar(startDate) + "&endDate=" + encodeChar(endDate);
	},
	getDetailGridUrl: function(PoDVKHId,poNumber){
		$('#errMsg').html('').hide();
		$('#detailPoDVKH').html('Chi tiết sale order:');
		$('#divDetail').show();
		$('#detailPoDVKH').html($('#detailPoDVKH').html() + " " +poNumber);
		POCustomerServiceManage.tongtien = 0;
		var url = POCustomerServiceManage.viewDetailChanged(PoDVKHId);
		$('#detailGrid').datagrid({
			url : url,
			pageList  : [10,20,30],
			rownumbers : true,
			width: $('.GridSection').width()-20,
			height:'auto',
			scrollbarSize : 0,
			columns:[[  
            	{field: 'productCode',title:'Mã hàng', width:100,align:'left',sortable : false,resizable : false,
            		formatter:function(value,rows){
            			return Utils.XSSEncode(rows.product.productCode);
                	} 
            	},
            	{field: 'productName',title:'Tên hàng', width:200,align:'left',sortable : false,resizable : false,
            		formatter:function(value,rows){
            			return Utils.XSSEncode(rows.product.productName);
                	} 
            	},
            	{field: 'price',title:'Đơn giá', width:100,align:'left',sortable : false,resizable : false,
            		formatter:PODVKHNCCFormatter.priceFormat
            	},
            	{field: 'quantity',title:'Số lượng', width:100,align:'center',sortable : false,resizable : false,
            		formatter:PODVKHNCCFormatter.quantityFormat
            	},
            	{field: 'amount',title:'Tổng tiền', width:100,align:'center',sortable : false,resizable : false,
            		formatter:PODVKHNCCFormatter.moneyFormat
            	},
            ]],
            onClickRow : function(rowIndex, rowData){
	        },
			method : 'GET',
            onLoadSuccess:function(){
            	$('#detailChangedGrid .datagrid-header-rownumber').html('STT');
    	    	updateRownumWidthForDataGrid('#detailChangedGrid');
            	 $('#tongtien').html(formatCurrency(POCustomerServiceManage.tongtien));
            	 POCustomerServiceManage.tongtien = 0;
				 $(window).scrollTop($(window).height()+100);
            	 $('#detailGrid').datagrid('resize');
            }
		});
	},
	changePODVKH : function(idPODVKH){
		var params = new Object();
		params.idPODVKH = idPODVKH;
		$.messager.confirm('Xác nhận', 'Bạn có muốn chuyển xuống NPP?', function(r){
			if(r){
				$.ajax({
					type : "POST",
					url : "/po/manage-po-customer-service/ncc/change",  //url lam nhiem vu chuyen
					data : ($.param(params, true)),
					dataType : "json",
					success : function(data) {
						if(!data.error) {
							$('#successMsg1').html('Bạn đã chuyển thành công').show();
							var tm = setTimeout(function() {
								$('#successMsg1').html('Bạn đã chuyển thành công').hide();
								clearTimeout(tm);
							}, 3000);
							POCustomerServiceManage.searchNCC();
						}else{
							var tm = setTimeout(function() {
								$('#errMsg').html(data.errMsg).show();
								clearTimeout(tm);
							}, 300);
						} 
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
					}
				});
			}
		});
		return false;
	},
	breakLine:function(){
		num = $('#errMsg').html().split('.').length;
		for(var j = 0;j<num;j++){
			if($('#errMsg').html().length > 20){
				$('#errMsg').html($('#errMsg').html().replace('.','</br>'));
			}
		}
	},
	getFullDetailPOConfirm:function(poVnmId, saleOrderNumber){
		var params = new Object();
		params.poVnmId = poVnmId;
		params.saleOrderNumber = saleOrderNumber;		
		var title = 'Chi tiết sale order: ' + Utils.XSSEncode(saleOrderNumber);
		Utils.getJSONDataByAjaxNotOverlay(params, '/po/manage-po-customer-service/detail-po-full', function(data) {
			var hiddenRow = true;
			if(data.lstData!=null && data.lstData.length>10){
				hiddenRow = false;
			}
			if(data.error !=undefined && data.error == false){
				$('#fullDettailPoVnmComfirm').dialog({
					title: title,
					closed : false,
					cache : false,
					modal : true,
					width : 800,
					height : 420,
					onOpen: function(){	
						$('#searchStyleShopGrid').datagrid({
							data: data.lstData,
							rownumbers : true,	
							fitColumns:true,
							pagination:false,
							pageNumber : 1,
							singleSelect:true,
							autoWidth: true,
							height : 275,
						    columns:[[  
						     	{field: 'lineSaleOrder', title: 'Số dòng sale order', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value,rowData,rowIndex) {
						        	return Utils.XSSEncode(value);
						        }},
						         {field: 'linePo', title: 'Số dòng đơn đặt', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value,rowData,rowIndex) {
						        	return Utils.XSSEncode(value);
						        }},
						        {field:'productCode',title:'Mã sản phẩm',align:'left', width:80,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
						        	return Utils.XSSEncode(value);
						        }},
						        {field:'productName',title:'Tên sản phẩm',align:'left', width:150, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
						        	return Utils.XSSEncode(value);
						        }},
						        /*{field:'productType',title:'Loại sản phẩm',align:'left', width:90, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
						        	if (value != undefined && value != null) {
						        		return ProductTypeVNM.parseValue(value);
						        	}
						        }},*/
						        {field : 'price', title : 'Đơn giá', align : 'right', width: 80, sortable : false, resizable : false, formatter : function(value, rowData, index){
									return formatCurrency(value);
								}},
								{field : 'quantity', title : 'Số lượng', align : 'right', width: 80, sortable : false, resizable : false, formatter : function(value, rowData, index){
									if (value != undefined && value != null) {
										value = Math.abs(value);
									}
									return formatQuantityEx(value, rowData.convfact);
								}},
								{field : 'amount', title : 'Tổng tiền', align : 'right', width: 80, sortable : false, resizable : false, formatter : function(value, rowData, index){
									if (value != undefined && value != null) {
										value = Math.abs(value);
									}
									return formatCurrency(value);
								}},
								{field : 'scrollbar', title : '', align : 'right', width: 11, sortable : false, hidden:hiddenRow, resizable : false, formatter : function(value, rowData, index){
								}}
						    ]],	    
						    onLoadSuccess :function(data){	 
						    	$('#errMsg').html('').hide();
						    	$('.datagrid-header-rownumber').html('STT');	    		    	
						    	updateRownumWidthForJqGrid('.easyui-dialog');
					    		$(window).resize();
						    }
						});
						var totalQuantity = 0;
						var totalAmount = 0;
						if (data.totalQuantity != undefined && data.totalQuantity != null) {
							totalQuantity = Math.abs(data.totalQuantity);
						}
						if (data.totalAmount != undefined && data.totalAmount != null) {
							totalAmount = Math.abs(data.totalAmount);
						}
						$('#spTotalQuantity').html(formatCurrency(totalQuantity.toString()));
						$('#spTotalAmount').html(formatCurrency(totalAmount));
					},
					buttons: [{
						 text:'Đóng',
						 handler:function(){
							 $('searchStyleShopContainerGrid').html('<table id="searchStyleShopGrid"></table>').show();
							 $('searchStyleShopContainerGrid').change();
							 $('#fullDettailPoVnmComfirm').dialog('close');
						 }
					}],
				 	onBeforeClose: function() {
			        },
			        onClose : function(){
			        	$('searchStyleShopContainerGrid').html('<table id="searchStyleShopGrid"></table>').show();
						$('searchStyleShopContainerGrid').change();
			        }
				});
			}
		}, null);
		$('html, body').animate({ scrollTop: 0 }, 1000);
	},
	
	closeDialogFullDetailPoVnmComfirm: function(){
		$('searchStyleShopContainerGrid').html('<table id="searchStyleShopGrid"></table>').hide();
		
	},
	/** vuongmq; 03/08/2015 ;lay ngay chot cua shopCode */
	handleSelectShopPOVNM: function() {
		var params= {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		Utils.getJSONDataByAjaxNotOverlay(params, '/commons/view-day-lock-select-shop' , function(data) {
			if (data != null) {
				$('#strLockDate').html(data.lockDate);
				$('#startDate').val(data.fromDate); 
				$('#endDate').val(data.toDate);
			}
		});
		
	},
	
};
/*
 * END OF FILE - /web/web/resources/scripts/business/po/vnm.manage-po-customer-service.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/po/vnm.po-init.js
 */
var POInit = {
	totalStockNumber : 0,
	totalMoney : 0,
	convertPO : function() {
		$(".ErrorMsgStyle").hide();
		if($('#poDataGrid').datagrid('getRows').length == 0) {
			$('#errMsg').html('Không có mặt hàng trong đơn hàng').show();
			return;
		}
		var params = new Object();
		var checkAllProductType = true;
		params.checkAllProductType = checkAllProductType;
		Utils.addOrSaveData(params, '/po/init-po-auto/convert', null, 'errMsg', function(data) {
		}, 'convertLoading');
	},
	
	exportExcelPO : function(){
		$(".ErrorMsgStyle").hide();
		if($('#poDataGrid').datagrid('getRows').length == 0) {
			$('#errMsg').html('Không có mặt hàng trong đơn hàng').show();
			return;
		}
		var params = new Object();
		var checkAllProductType = true;
		params.checkAllProductType = checkAllProductType;
		ReportUtils.exportReport('/po/init-po-auto/export-po', params);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/po/vnm.po-init.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/po/vnm.po-invoice-adjustment.js
 */
var POInvoiceAdjustment = {
	//start Viet tam chua sua 2 ham check ben duoi	
	onCheckAllChange:function(checkAll) {
		Utils.onCheckAllChange(checkAll, 'CheckAll');
	},
	onChangePoAutoCheckbox: function(t) {
		var checkThis=$(t).is(':checked');
		Utils.onCheckboxChangeForCheckAll(checkThis, 'CheckAll', 'allCheck');
	},
	//end
	
	showAdjustmentForm : function(poConfirmId, invoiceNumber) {
		$('.ErrorMsgStyle').hide();
		$('#invoiceAdjustment').show();
		$('#poConfirmId').val(poConfirmId);
		$('#currentInvoiceNumber').val(invoiceNumber);
		$('#newInvoiceNumber').val('');
		$('#newInvoiceNumber').focus();
	},
	searchPoConfirm	: function(callback) {
		$('#invoiceAdjustment').hide();
		$('.ErrorMsgStyle').hide();
		
		var msg = Utils.getMessageOfRequireCheck('createDateFrom','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('createDateTo','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('createDateFrom', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('createDateTo', 'Đến ngày');
		}
		var createDateFrom = $('#createDateFrom').val().trim();
		var createDateTo = $('#createDateTo').val().trim();
		if(msg.length ==0 && !Utils.compareDate(createDateFrom,createDateTo)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}

		if(msg.length > 0){
			$('#errorSearchMsg').html(msg).show();
			return false;
		}
		
		var invoiceNumber = $('#invoiceNumber').val().trim();
		var poNumber = $('#poNumber').val().trim();
		var shopId = $('#shopId').val().trim();
		params = new Object();
		if(invoiceNumber.length > 0) {
			params.invoiceNumber = invoiceNumber;
		}
		if(createDateFrom.length > 0) {
			params.createDateFrom = createDateFrom;
		}
		if(createDateTo.length > 0) {
			params.createDateTo = createDateTo;
		}
		if(poNumber.length > 0) {
			params.poNumber = poNumber;
		}
		params.shopId = shopId; 
		Utils.getHtmlDataByAjax(params, '/po/invoice-adjustment/search-po', function(data) {
			var myData;
			try{
				var myData = JSON.parse(data);
			} catch(err) {
				$('#listSearchPOAdjustSearch').html(data);
				$('.MySelectBoxClass1').customStyle();
				if(callback!= null && callback!= undefined){
					callback.call(this);
				}
				return;
			}
			if(myData.error == true && myData.errMsg != undefined) {
				$('#errorSearchMsg').show();
				$('#errorSearchMsg').html(myData.errMsg);
			} else {
				$('.ScrollSection').html(data);
			}
		}, 'poSearchLoading', 'POST');
	},
	
	adjustPOConfirm	: function() {
		$('.ErrorMsgStyle').hide();
		var msg = '';
		var lstPoVnmVO = [];
		
		var mapInvoiceNumber = new Map();	
		var is_success = false;
		$('.CheckAll').each(function(){
			if($(this).is(':checked')){
				var invoiceNum = $(this).attr('invoice-num'); // vuongmq; 06/08/2015; lay la Id 
				var poVnmId = $('#poVnmId' + Utils.XSSEncode(invoiceNum)).val().trim();
				var invoiceNumber = $('#invoiceNumber' + Utils.XSSEncode(invoiceNum)).html().trim();
				var newInvoiceNumber = $('#newInvoiceNumber' + Utils.XSSEncode(invoiceNum)).val().trim();
				if(invoiceNumber.toUpperCase() == newInvoiceNumber.toUpperCase()){
					msg = 'Số hóa đơn mới không được trùng với số hóa đơn hiện tại';
					$('#newInvoiceNumber' + Utils.XSSEncode(invoiceNum)).focus();
				}
				if(msg.length > 0) {
					$('#errMsg').html(msg).show();
					return;
				}
				if(newInvoiceNumber==null || newInvoiceNumber==''){
					if(msg.length == 0){
						msg = Utils.getMessageOfRequireCheck('newInvoiceNumber' + Utils.XSSEncode(invoiceNum), 'Số hóa đơn mới');
						$('#newInvoiceNumber' + Utils.XSSEncode(invoiceNum)).focus();
					}
					if(msg.length > 0) {
						$('#errMsg').html(msg).show();
						return;
					}
				}else{
					if(msg.length == 0){
						msg = Utils.getMessageOfSpecialCharactersValidate('newInvoiceNumber'+ Utils.XSSEncode(invoiceNum) , 'Số hóa đơn mới', Utils._CODE);
						$('#newInvoiceNumber'+ Utils.XSSEncode(invoiceNum)).focus();
					}
					if(msg.length > 0) {
						$('#errMsg').html(msg).show();
						return;
					}
				}
				
				if(mapInvoiceNumber.get(newInvoiceNumber)!=null){
					is_success = true;
					return false;
				}else{
					mapInvoiceNumber.put(newInvoiceNumber,newInvoiceNumber);
				}
				if($('#checkReason').is(':checked')){
					var apParamCode = $('#apParamCode').val().trim();
				}else{
					var apParamCode = $('#apParamCode'+invoiceNum).val().trim();
				}
				lstPoVnmVO.push({poVnmId:poVnmId, invoiceNumber:newInvoiceNumber, reason:apParamCode});
			}
		});		
		var isCountChecked = $('.CheckAll:checked').length;
		if(isCountChecked <= 0){
			msg = 'Chưa check chọn cho hóa đơn cần điều chỉnh';
		}
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			return;
		}
		if(is_success){
			msg = 'Số hóa đơn mới không được phép trùng';
		}
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			return;
		}
		var params = {lstPoVnmVO:lstPoVnmVO};
		JSONUtil.saveData2(params, '/po/invoice-adjustment/adjustment-po-confirm', "Bạn có muốn cập nhật số hóa đơn?", 'errMsg', function(data) {
			POInvoiceAdjustment.searchPoConfirm(function(){
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function() {
					$('#successMsg').hide();
					clearTimeout(tm);
				}, 3000);
			});
		});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/po/vnm.po-invoice-adjustment.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/po/vnm.po-return-product.js
 */
var POReturnProduct = {
	_xhrSave : null,	
	search : function() {
		$('#errorSearchMsg').html('').hide();
		var msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Ngày tạo từ');
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0){
			var fromDate = $('#fromDate').val().trim();
			var toDate = $('#toDate').val().trim();
			if(!Utils.compareDate(fromDate, toDate)){
				msg = ' Ngày tạo từ phải nhỏ hơn hoặc bằng Đến ngày';		
				$('#fromDate').focus();
			}
		}
		if(msg.length > 0){
			$('#errorSearchMsg').html(msg).show();
			return false;
		}	
		var params = new Object();
		params.fromDate = $('#fromDate').val().trim();
		params.toDate = $('#toDate').val().trim();
		params.invoiceCode = $('#invoiceCode').val().trim();
		params.poConfirmCode = $('#poConfirmCode').val().trim();		
		Utils.getHtmlDataByAjax(params, '/po/return-product/search',function(data) {
			$("#productorderSearchTable").html(data).show();
			$("#productorderDetailTable").html('').hide();			
		}, 'loadingSearch', 'POST');
		return false;
	},
	detail : function(poConfirmCodeDetail,clearInput,callback) {
		var params = new Object();
		params.poConfirmCodeDetail = poConfirmCodeDetail;		
		Utils.getHtmlDataByAjax(params, '/po/return-product/detail',function(data) {
			$("#productorderDetailTable").html(data).show();		
			var err = $('#errorPOReturn').val();
			if (!isNullOrEmpty(err)) {
				$('#errMsgReturn').html(Utils.XSSEncode(err)).show();
			}
			if(callback!= null && callback!= undefined){
				callback.call(this);
			}	
		}, null, 'POST');
		var tabindex = -1;
		$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
			if (this.type != 'hidden') {
				$(this).attr("tabindex", tabindex);
				tabindex -=1;
			}
		});
		tabindex = 1;
		 $('input, select, button').each(function () {
    		 if (this.type != 'hidden') {
	    	     $(this).attr("tabindex", tabindex);
				 tabindex++;
    		 }
		 });
		return false;
	},
	returnProduct : function(){
		$('#errMsgReturn').hide();
		var lstPOVnmDetailLLotVoIDTemp = new Array();
		var lstPOVnmDetailLLotVoLotTemp = new Array();
		var lstPOVnmDetailLLotVoValueTemp = new Array();
		var lstSLChoPhepTemp = new Array();
		var lstStockTemp = new Array();
		var lstProduct = new Array();
		$('.Products').each(function(){
			lstProduct.push($(this).text());
		});
		$('.poVnmDetailLotVoID').each(function(){
			lstPOVnmDetailLLotVoIDTemp.push($(this).val());
		});
		$('.Lot').each(function(){
			lstPOVnmDetailLLotVoLotTemp.push($(this).text());
		});
		$('.SLChoPhep').each(function(){
			lstSLChoPhepTemp.push($(this).val());
		});
		var lstKho = new Array();
		$('.WarehouseId').each(function(){
			lstKho.push($(this).val());
		});
		var lstProductId = new Array();
		$('.ProductId').each(function(){
			lstProductId.push($(this).val());
		});
		var error = false;
		$('#vnmPoProductDetail .Count').each(function(){
			var convfact = $(this).attr('convfact');
			var quantity = getQuantity($(this).val().trim(),convfact);
			if(quantity=='' || quantity==0){
				quantity = 0;
			} else if(isNaN(quantity)) {
				$(this).focus();
				error = true;
				return;
			}
			lstPOVnmDetailLLotVoValueTemp.push(quantity);
		});
		if(error){
			$('#errMsgReturn').html("Giá trị số lượng trả phải là số nguyên dương.").show();
			return false;
		}
		
		$('#vnmPoProductDetail .Stock').each(function(){
			var convfact = $(this).attr('convfact');
			var quantity = getQuantity($(this).text(),convfact);
			if(quantity=='' || quantity==0){
				quantity = 0;
			}
			lstStockTemp.push(quantity);
		});
		
		
		var lstPOVnmDetailLLotVoID = new Array();
		var lstPOVnmDetailLLotVoLot = new Array();
		var lstPOVnmDetailLLotVoValue = new Array();
		
		var c = 0;
		for(var i=0;i<lstPOVnmDetailLLotVoIDTemp.length;i++){
			lstPOVnmDetailLLotVoID.push(lstPOVnmDetailLLotVoIDTemp[i]);
			lstPOVnmDetailLLotVoLot.push(lstPOVnmDetailLLotVoLotTemp[i]);
			lstPOVnmDetailLLotVoValue.push(lstPOVnmDetailLLotVoValueTemp[i]);
			if(lstPOVnmDetailLLotVoValueTemp[i] > lstStockTemp[i]){
				$('#errMsgReturn').html("Số lượng trả của mặt hàng vượt quá tồn kho. Vui lòng kiểm tra lại").show();
				return false;
			}
			if (Number(lstPOVnmDetailLLotVoValueTemp[i]) > 0) {
				c++;
			}
		}
		if (c <= 0) {
			$('#errMsgReturn').html("Không thể trả hàng khi số lượng trả của tất cả mặt hàng bằng 0. Vui lòng kiểm tra lại").show();
			return false;
		}
		for(var i=lstPOVnmDetailLLotVoIDTemp.length-1; i>0; i--){
			if(lstProduct[i]==lstProduct[i-1]){
				var quan = lstPOVnmDetailLLotVoValueTemp[i] + lstPOVnmDetailLLotVoValueTemp[i-1];
				var stock = lstStockTemp[i] + lstStockTemp[i-1];
				lstPOVnmDetailLLotVoValueTemp[i-1] = quan;
				lstPOVnmDetailLLotVoValueTemp.splice(i,1);
				lstStockTemp[i-1]= stock;
				lstStockTemp.splice(i,1);
				lstPOVnmDetailLLotVoIDTemp.splice(i,1);
				lstPOVnmDetailLLotVoLotTemp.splice(i,1);
				lstSLChoPhepTemp[i-1]=Number(lstSLChoPhepTemp[i]) + Number(lstSLChoPhepTemp[i-1]);
				lstSLChoPhepTemp.splice(i,1);
			}
		}
		for(var i=0; i<lstPOVnmDetailLLotVoIDTemp.length; i++){
			if(!isNaN(lstPOVnmDetailLLotVoValueTemp[i]) && Number(lstPOVnmDetailLLotVoValueTemp[i]) > 0) {
				if(lstPOVnmDetailLLotVoValueTemp[i] > lstStockTemp[i]){
					$('#errMsgReturn').html("Số lượng trả của mặt hàng vượt quá tồn kho. Vui lòng kiểm tra lại").show();
					return false;
				}
				if(lstPOVnmDetailLLotVoValueTemp[i] > lstSLChoPhepTemp[i]){
					$('#errMsgReturn').html("Số lượng trả của mặt hàng lớn hơn số lượng cho phép trả. Vui lòng kiểm tra lại").show();
					return false;
				}
			}
		}
		if(lstPOVnmDetailLLotVoID.length == 0) {
			$('#errMsgReturn').html("Số lượng nhập vào không hợp lệ. Hãy nhập lại").show();
			return false;
		}
		var params = new Object();
		params.lstPOVnmDetailLLotVoID = lstPOVnmDetailLLotVoID;
		params.lstPOVnmDetailLLotVoLot = lstPOVnmDetailLLotVoLot;
		params.lstPOVnmDetailLLotVoValue = lstPOVnmDetailLLotVoValue;
		params.lstWarehouseId = lstKho;
		params.lstProductIdKho = lstProductId;
		params.poConfirmCodeReturn = $('#poConfirmCodeReturn').val().trim();
		Utils.addOrSaveData(params, '/po/return-product/returnProduct', null,  'errMsgReturn', function(data) {
			POReturnProduct.detail($('#poConfirmCodeReturn').val().trim(),false, function(){
				POReturnProduct.enableBtnExportExcel();						
				$('#poVnmExportCode').val(data.poVnmExportCode);	
				$('#successMsgEx').html('Trả hàng thành công.').show();
				setTimeout(function(){$('#successMsgEx').hide();},3000);
			});
		}, 'loadingReturn', null, null,'Bạn có chắc chắn muốn trả hàng ?');
		return false;
	},
	enableBtnExportExcel : function(){
		$('#btnReturnProduct').addClass('BtnGeneralDStyle');
		$('#btnReturnProduct').attr('disabled',true);
		$('#btnExportExcel').removeAttr('disabled');
		$('#btnExportExcel').removeClass('BtnGeneralDStyle');
	},
	updateTotal :function(){
		var totalCount = 0;
		var totalMoney = 0; 
		var countArr = new Array();
		var convfactArr = new Array();
		$('.Count').each(function(){
			countArr.push($(this).val());
		});
		$('.Convfact').each(function(){
			convfactArr.push($(this).val());
		});
		for (var i =0 ; i< countArr.length ;++i){
			totalCount += Number(StockValidateInput.getQuantity(countArr[i], convfactArr[i]));
		}
		$('.Money').each(function(){
			totalMoney += Number(Utils.returnMoneyValue($(this).text()));
		});
		$('#totalQuantity').html(formatCurrency(totalCount));
		$('#totalAmount').html(formatCurrency(totalMoney));
	},
	changeInput: function (input,moneyId,price,convfact){
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact);
		var quantity = StockValidateInput.getQuantity(quantityTxt, convfact);
		$(input).val(quantityTxt);
		$('#'+moneyId).text(formatCurrency(quantity * price));
		POReturnProduct.updateTotal();
		return false;
	},
	exportExcel:function(){
		var poVnmExportCode = $('#poVnmExportCode').val().trim();
		var param = new Object();
		param.poVnmExportCode = poVnmExportCode;
		var msg = 'Bạn có muốn in đơn trả hàng chứ?';
		Utils.addOrSaveData(param, '/po/return-product/exportexcel', null, 'errMsgReturn', function(data){
			if(data.error && data.errMsg!= undefined){
				$('#errMsgReturn').html(Utils.XSSEncode(data.errMsg)).show();
			} else {
				window.location.href = data.view;
				setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
                    CommonSearch.deleteFileExcelExport(data.view);
				},2000);
			}
		}, 'loadingReturn', null, null, msg);
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/po/vnm.po-return-product.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/po/vnm.po-stock-in.js
 */
var POStockIn = {
	_IMPORTING: 1,
	_IMPORTED: 2,
	_FLAG_ADD_QUANTITY_FULL: true,
	searchGridInit: function() {
		$('#dg').datagrid({  
		    url:'/po/stock-in/getinfo',		    	    
		    singleSelect:true,	     
		    width: $(window).width() - 50,
		    height: 350,
		    rownumbers : true,
		    fitColumns:true,		    
		    queryParams:{
		    	//shopId:$('#shopId').val().trim(),
		    	shopCode : $('#cbxShop').combobox('getValue'), // vuongmq; 03/08/2015; them cobobox shopcode, luc dau khong can shopCode, se lay shop root mac dinh
		    	fromDate : $('#fromDate').val().trim(),
		    	toDate : $('#toDate').val().trim(),
		    	typePo : $('#typePo').val().trim(),
		    	status :$('#status').val().trim(),
		    	saleOrderNumber: $('#saleOrderNumber').val().trim(),
		    	poAutoNumber: $('#poAutoNumber').val().trim(),
		    	invoiceNumber: $('#invoiceNumberSearch').val().trim(),
		    },
		    showFooter: true,
		    columns:[[ 
				{field: 'saleOrderNumber', title: 'Số đơn hàng', width: 130, align: 'left', formatter: function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
				{field:'poCoNumber', title:'Số ASN', width:130, align:'left', formatter:function(value,row,index){
					return Utils.XSSEncode(value);
				}},
				{field: 'poAutoNumber', title: 'Số đơn đặt hàng', width: 130, align: 'left', formatter: function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
				{field: 'invoiceNumber', title: 'Số hóa đơn', width: 100, align: 'left', formatter: function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
				{field:'type', title:'Loại đơn', width:100, align:'left', formatter:function(value,row,index){
					if (value != undefined && value != null) {
						/*var giatri = PoType.parseValue(value);
						return Utils.XSSEncode(PoTypeText.parseValue(giatri));*/
						return Utils.XSSEncode(PoTypeText.parseValue(value));
					}
					return '';
				}}, 
				
				{field:'status', title:'Trạng thái', width:120, align:'left',formatter:function(value,row,index){
					if (value != undefined && value != null) {
						/*var giatri = PoStatus.parseValue(value);
						var type = PoType.parseValue(row.type);
						return Utils.XSSEncode(PoStatusTextOfType.parseValue(giatri, type));*/
						return Utils.XSSEncode(PoStatusTextOfType.parseValue(value, row.type));
					}
					return '';
				}},			
				{field:'requestedDateStr', title:'Ngày yêu cầu giao hàng', width:90, align:'center',formatter:function(value,row,index){
					/*if (row.orderdate == null) {
						return '';
					}
					return $.datepicker.formatDate('dd/mm/yy', new Date(row.orderdate));*/
					return Utils.XSSEncode(value);
				}},
				{field: 'receivedDateStr', title:'Ngày nhập', width:90, align:'center',formatter:function(value,row,index){
					/*if (row.importDate == null) {
						return '';
					}
					return $.datepicker.formatDate('dd/mm/yy', new Date(row.importDate));*/
					return Utils.XSSEncode(value);
				}},
				{field:'poVnmDateStr', title:'Ngày tạo', width:90, align:'center',formatter:function(value,row,index){
					/*if (row.poVnmDate==null) {
						return '';
					}
					return $.datepicker.formatDate('dd/mm/yy', new Date(row.poVnmDate));*/
					return Utils.XSSEncode(value);
				}},
				{field:'amount', title:'Tổng tiền', width:120, align:'right',formatter:CommonFormatter.numberFormatter},
				{field:'discount', title:'Chiết khấu', width:120, align:'right',formatter:CommonFormatter.numberFormatter},			
				{field:'import', title:'Nhập/ trả hàng', width:90, align:'center',formatter:function(value,row,index){
					var viewTitle = '';
					if (row.type != null && row.type == 'PO_CONFIRM') {
						viewTitle = 'Nhập hàng';
					} else if (row.type != null && row.type == 'RETURNED_SALES_ORDER') {
						viewTitle = 'Trả hàng';
					}
					return '<a href="javascript:void(0);" title="'+ Utils.XSSEncode(viewTitle) +'" onClick="return POStockIn.detail('+row.id+');">'+
								'<img src="/resources/images/icon_nhaphang_active.png" width="18" height="19" /></a>';
				}}
			]],    
			onLoadSuccess :function(data){			
				$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');		   	
			   	 updateRownumWidthForJqGrid('.easyui-dialog');
		   	 	$(window).resize();    		 
			}
		});
	},
	search: function(isSave) {
		var poCoNumber = $('#poCoNumber').val().trim();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '') {
			msg = 'Từ ngày không được để trống';
			$('#fromDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if (toDate == '') {
			msg = 'Đến ngày không được để trống';
			$('#toDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if (!Utils.isDate(fromDate, '/')) {
			msg = 'Từ ngày không hợp lệ. Vui lòng nhập lại';
			$('#fromDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if (!Utils.isDate(toDate, '/')) {
			msg = 'Đến ngày không hợp lệ. Vui lòng nhập lại';
			$('#toDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if (!Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		var shopCode = $('#cbxShop').combobox('getValue'); // vuongmq; 03/08/2015; them cobobox shopcode
		var typePo = $('#typePo').val().trim();
		var status = $('#status').val().trim();
		var saleOrderNumber = $('#saleOrderNumber').val().trim();
    	var poAutoNumber = $('#poAutoNumber').val().trim();
    	var invoiceNumber = $('#invoiceNumberSearch').val().trim();
		var params = new Object();
		params.shopCode = shopCode;
		params.poCoNumber = poCoNumber;
		params.fromDate = fromDate;
		params.toDate = toDate;
		params.typePo = typePo;
		params.status = status;
		params.saleOrderNumber = saleOrderNumber;
		params.poAutoNumber = poAutoNumber;
		params.invoiceNumber = invoiceNumber;
		$('#dg').datagrid('load', params);
		$("#InputTable").html('').hide();
		return false;
	},
	discountChange:function(){
		var totalmoney=0;
		if($('#totalDetail').html().trim()!=''){
			totalmoney=Utils.returnMoneyValue($('#totalDetail').html().trim());
		}
		var discountNotVat=0;
		if($('#discountNotVat').val().trim()!=''){
			discountNotVat=Utils.returnMoneyValue($('#discountNotVat').val().trim());
		}
		var vat=Math.round((totalmoney-discountNotVat)*0.1);//Làm tròn lên
		var pay=totalmoney-discountNotVat+vat;
		$('#discountDetail').html(formatCurrencyInterger(discountNotVat));
		$('#vatDetail').html(formatCurrencyInterger(vat));
		$('#payDetail').html(formatCurrencyInterger(pay));
	},
	detail : function(poVnmId) {
		var data = new Object();
		data.poVnmId = poVnmId;
		Utils.getHtmlDataByAjax(data, "/po/stock-in/stockin", function(data) {
			$("#InputTable").html(data).show();
			window.scrollTo(0, $(document).height());
		}, null, null);
	},

	//vuongmq; 11/08/2015;  nhap/ tra hang theo so luong nhap vao
	changeWarehouse: function(poVnmDetailLotId, index) {
		var params = new Object();
		var wareHouseId = $('#wareHouse'+poVnmDetailLotId).val();
		params.wareHouseId = wareHouseId;
		var row = $('#dg_detail').datagrid('getRows')[index];
		params.productId = row.productId;
		params.shopId = row.shopId;
		Utils.getJSONDataByAjaxNotOverlay(params, "/po/stock-in/warehouse-stockTotal", function(data) {
			if (data != undefined && data != null) {
				// neu khong co ton kho thi gan lai gia tri 0
				var quantity = 0;
				if (data.stockTotal != undefined && data.stockTotal != null && data.stockTotal.quantity != undefined && data.stockTotal.quantity != null) {
					quantity = data.stockTotal.quantity;
				}
				// gan gia tri ton kho moi quantity, khi chon kho
				var rowNew = $('#dg_detail').datagrid('getRows')[index];
				rowNew.quantityStock = quantity;
				rowNew.wareHouseIdTmp = wareHouseId;
				$('#dg_detail').datagrid('updateRow',{
									index: index,
									row: rowNew
								});
				$('#dg_detail').datagrid('refreshRow',index);
				Utils.formatCurrencyFor('quantityInput' + index);
				$('#wareHouse'+poVnmDetailLotId).customStyle(); // customStyle combobox
			}
		}, null, null);
	},

	//vuongmq; 11/08/2015;  nhap/ tra hang theo so luong nhap vao,
	stockin: function(poCoNumber, idvnm, status) {
		var invoiceNumber = $('#invoiceNumber').val().trim();
		//var _quantityCheck = $('#quantity_check').val().trim();
		//var _amountCheck = $('#amount_check').val().trim();
		//var quantityCheck = Utils.returnMoneyValue(_quantityCheck);
		//var amountCheck = Utils.returnMoneyValue(_amountCheck);
		var _discountCheck = $('#discountNotVat').val().trim();
		var discountCheck = Utils.returnMoneyValue(_discountCheck);
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		msg = Utils.getMessageOfRequireCheck('invoiceNumber', 'Số hoá đơn');
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('invoiceNumber','Số hoá đơn');
		}
		/*if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('quantity_check','Số lượng kiểm soát');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('amount_check','Giá trị kiểm soát');
		}*/

		//TUNGTT
		if (msg.length == 0) {
			var payDetail = 0;
			if ($('#payDetail').html().trim() != '') {
				payDetail=Utils.returnMoneyValue($('#payDetail').html().trim());
			}
			/*var amount_check = Utils.returnMoneyValue($('#amount_check').val().trim());
			if (parseInt(amount_check) != parseInt(payDetail)){
				msg = 'Giá trị kiểm soát nhập vào khác với số tiền thanh toán của đơn hàng, bạn hãy nhập đúng';
				//$('#amount_check').val('');
				$('#amount_check').focus();
			}*/
		}
		//Vuongmq; 11/08/2015; validate gia tri cua danh sach Nhap/ tra hang
		var lstPoVNMDetail = new Array();
		var typePo = $('#typePoVNM').val();
		var typeViewErr = '';
		var dateViewErr = '';
		if (typePo == PoType.PO_CUSTOMER_SERVICE || typePo == PoType.PO_CONFIRM) {
			// nhap hang
			typeViewErr = 'nhập';
			dateViewErr = 'về';
		} else if (typePo == PoType.RETURNED_SALES_ORDER || typePo == PoType.PO_CUSTOMER_SERVICE_RETURN) {
			// tra hang
			typeViewErr = 'trả';
			dateViewErr = 'đi';
		}
		var deliveryDate = $('#deliveryDate').val();
		/*if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('deliveryDate', 'Thời gian xe ' + dateViewErr);
		}*/
		if (deliveryDate.length > 0) {
			if (msg.length == 0) {
					msg = Utils.getMessageOfInvalidFormatDateNew('deliveryDate', 'Thời gian xe ' + dateViewErr);
			}
			/*if (msg.length == 0) {
				var cDate = ReportUtils.getCurrentDateString();
				if (!Utils.compareDate(cDate, deliveryDate)) {
					msg = 'Thời gian xe ' + dateViewErr + ' không được nhỏ hơn thời gian ngày hiện tại.';
				}
			}*/
			deliveryDate = deliveryDate + ':00';
		}
		if (msg.length == 0) {
			$('.quantityInputClazz').each(function() {
				//var quantityInput = Utils.returnMoneyValue($(this).val());
				var quantityInput = 0;
				var index = $(this).attr('qIndex');
				var row = $('#dg_detail').datagrid('getRows')[index];
				var gt = $(this).val();
	 			var gtTmp = gt.split('/');
	 			if (gtTmp.length > 2 || isNaN(gtTmp[0])) {
	 				$('#errMsg').html(format('Số lượng {0} không đúng định dạng số nguyên hoặc thùng lẻ.', typeViewErr)).show();
					$(this).focus();
					return false;
	 			}
	 			if (gtTmp[1] != undefined) {
	 				if (isNaN(gtTmp[1])) {
		 				$('#errMsg').html(format('Số lượng {0} dạng thùng lẻ không đúng định dạng số nguyên.', typeViewErr)).show();
						$(this).focus();
						return false;
					} else {
	 					quantityInput = Number(Utils.returnMoneyValue(gtTmp[0].trim())) * Number(row.convfact) + Number(Utils.returnMoneyValue(gtTmp[1].trim()));
					}
	 			} else {
	 				quantityInput = Number(Utils.returnMoneyValue(gtTmp[0].trim()));
	 			}
				var dongErr = Number(index) + 1;
				if (row.lstWarehouse != null && row.lstWarehouse.length <= 0) {
					msg = 'Không có kho dòng thứ ' + dongErr + ' nên không thể ' + typeViewErr + ' hàng';
					$('#errMsg').html(msg).show();
					$(this).focus();
					return false;
				}
				var slDaNhap = row.quantity - row.quantityReceived;
				if (Number(quantityInput) > Number(slDaNhap)) {
					msg = 'Bạn đã ' + typeViewErr + ' quá số lượng dòng thứ ' + dongErr;
					$('#errMsg').html(msg).show();
					$(this).focus();
					return false;
				}
				if (typePo == PoType.RETURNED_SALES_ORDER || typePo == PoType.PO_CUSTOMER_SERVICE_RETURN) {
					var tonKho = row.quantityStock;
					if (Number(quantityInput) > Number(tonKho)) {
						msg = 'Bạn đã ' + typeViewErr + ' quá số lượng tồn kho dòng thứ ' + dongErr;
						$('#errMsg').html(msg).show();
						$(this).focus();
						return false;
					}
				}
				// add vao danh sach lstPoVNMDetail
				var productInput = new Object();
				productInput.poVnmDetailLotId = row.poVnmDetailLotId;
				productInput.wareHouseId = $('#wareHouse'+row.poVnmDetailLotId).val();
				productInput.quantity = Number(quantityInput);
				lstPoVNMDetail.push(productInput);
			});
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.poVnmId = idvnm;
		params.poCoNumber = poCoNumber;
		params.invoiceNumber = invoiceNumber;
		params.status = status;
		//params.quantity_check = quantityCheck;
		//params.amount_check = amountCheck;
		params.invoiceDate = $('#creatDate').val();
		params.deliveryDate = deliveryDate;
		params.discount_check = discountCheck;
		params.lstPovnmDetail = lstPoVNMDetail;
		var msgDialog = 'Bạn có muốn lưu thông tin này?';
		JSONUtil.saveData2(params, "/po/stock-in/stockin-action", msgDialog, 'errMsg', function(data) {
			setTimeout(function() {
				POStockIn.search(true);
			}, 3000);
		}, null, null, null, null);
		/*Utils.addOrSaveData(params, "/po/stock-in/stockin-action", null,
				'errMsg', function(data) {
					setTimeout(function() {
						POStockIn.search(true);
					}, 3000);
				}, 'loading', null, false, null);*/
		return true;
	},

	removePOConfirm : function(poCoNumber, idvnm) {
		$('#errMsg').html('').hide();
		var params = new Object();
		params.poVnmId = idvnm;
		params.poCoNumber = poCoNumber;
		Utils.addOrSaveData(params, "/po/stock-in/remove-POConfirm", null,
				'errMsg', function(data) {
			if(data.error != undefined && data.error == false){
				$('#successMsg').html('Hủy đơn hàng thành công').show();
				setTimeout(function() {
					$('#successMsg').html('').hide();
					//window.location.reload();
					POStockIn.search();
				}, 2000);
			}else{
				$('#errMsg').html(data.errMsg).show();
			}
			
				}, 'loading', null, false, null);
		return true;
	},

	/** vuongmq; 03/08/2015 ;lay ngay chot cua shopCode */
	handleSelectShopPOStockIn: function() {
		var params= {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		Utils.getJSONDataByAjaxNotOverlay(params, '/commons/view-day-lock-select-shop' , function(data) {
			if (data != null) {
				$('#strLockDate').html(data.lockDate);
				$('#fromDate').val(data.fromDate); 
				$('#toDate').val(data.toDate);
			}
		});
		
	},

	/**
	 * fill in quantity full
	 * @author vuongmq
	 * @since 29/01/2016 
	 */
	addQuantityFull: function() {
		var rows = $('#dg_detail').datagrid('getRows');
		var i = 0;
		if (POStockIn._FLAG_ADD_QUANTITY_FULL) {
			$('.quantityInputClazz').each(function() {
				var value = Number(rows[i].quantity) - Number(rows[i].quantityReceived);
				$(this).val(formatQuantityEx(value, rows[i].convfact));
				i++;
			});
			POStockIn._FLAG_ADD_QUANTITY_FULL = false;
		} else {
			$('.quantityInputClazz').each(function() {
				$(this).val('');
			});
			POStockIn._FLAG_ADD_QUANTITY_FULL = true;
		}
	},

	/**
	 * blur quantity input
	 * @author vuongmq
	 * @since 29/01/2016 
	 */
	blurQuantityInput: function(rows) {
		for (var k = 0 ; k < rows.length ; k++) {
			if (rows[k].wareHouseId != null) {
				disableSelectbox('wareHouse' + rows[k].poVnmDetailLotId);
			}
			Utils.bindFormatOnTextfield('quantityInput' + k, Utils._TF_NUMBER_CONVFACT);
			Utils.formatCurrencyFor('quantityInput' + k);
		}
		$('.quantityInputClazz').live('blur',function() {
			var typePo = $('#typePoVNM').val();
			var typeViewErr = '';
			if (typePo == PoType.PO_CONFIRM) {
				typeViewErr = 'nhập'; 
			} else {
				typeViewErr = 'trả';
			}
 			var gt = $(this).val();
 			var gtTmp = gt.split('/');
 			if (gtTmp.length > 2 || isNaN(gtTmp[0])) {
 				$('#errMsg').html(format('Số lượng {0} không đúng định dạng số nguyên hoặc thùng lẻ.', typeViewErr)).show();
				$(this).focus();
				return false;
 			}
 			var index = $(this).attr('qindex');
 			var row = $('#dg_detail').datagrid('getRows')[index];
 			if (gtTmp[1] != undefined) {
 				if (isNaN(gtTmp[1])) {
	 				$('#errMsg').html(format('Số lượng {0} dạng thùng lẻ không đúng định dạng số nguyên.', typeViewErr)).show();
					$(this).focus();
					return false;
				} else {
 					var value = Number(Utils.returnMoneyValue(gtTmp[0].trim())) * Number(row.convfact) + Number(Utils.returnMoneyValue(gtTmp[1].trim()));
 					$(this).val(formatQuantityEx(value, row.convfact));
				}
 			} else {
 				var value = Number(Utils.returnMoneyValue(gtTmp[0].trim()));
 				$(this).val(formatQuantityEx(value, row.convfact));
 			}
		});
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/po/vnm.po-stock-in.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

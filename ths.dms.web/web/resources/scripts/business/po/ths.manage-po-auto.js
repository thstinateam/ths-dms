var POAutoManage = {
	_searchParams: null,
	
	onCheckAllChange:function(checkAll) {
		Utils.onCheckAllChange(checkAll, 'CheckAllPoAuto');
	},
	onChangePoAutoCheckbox: function(checkThis) {
		Utils.onCheckboxChangeForCheckAll(checkThis, 'CheckAllPoAuto', 'allPOAutoCheck');
	},
	search:function(callback){		
		$('#poAutoDetailTableDiv').html('');
		$('#ErrorMsgStyle').hide();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		if(startDate == '__/__/____'){
			$('#startDate').val('');
		}
		if(endDate == '__/__/____'){
			$('#endDate').val('');
		}
		var msg = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày tạo từ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày tạo từ');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('endDate', 'Đến ngày');
		}
		if(msg.length == 0 && !Utils.compareDate(startDate, endDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#startDate').focus();
		}
		if(msg.length > 0){
			$('#errorSearchMsg').html(msg).show();
			return false;
		}		
		var shopCodeAndName = $('#shopCode').val().trim();
		var poNumber = $('#poNumber').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		var approveStatus = $('#approveStatus').val();
		var params = new Object();
		if (poNumber != null && poNumber != undefined && poNumber.length > 0){
			params.poNumber = poNumber;
		}
		if (approveStatus != null && approveStatus != undefined && approveStatus.length > 0){
			params.approveStatus = approveStatus;
		}
		if (startDate != null && startDate != undefined && startDate.length > 0){
			params.startDate = startDate;
		}
		if (endDate != null && endDate != undefined && endDate.length > 0){
			params.endDate = endDate;
		}
		if ($('#shopCode').val().trim().length > 0) {
			params.shopCode = $('#shopCode').val().split('-')[0].trim();
		}
		POAutoManage._searchParams = params;
		Utils.getHtmlDataByAjax(params, "/po/manage-po-auto/search", function(data){
			$("#divPOAutoTable").html(data).show();
			Utils.resizeHeightAutoScrollCross('listPoAutoSearch');
			var status = $('#approveStatus').val();
			if(status == 1){
				$('#btnHuy').hide();
				$('#btnDuyet').hide();
				$('#btnXml').show();
			}else{
				$('#btnHuy').show();
				$('#btnDuyet').show();
				$('#btnXml').hide();
			}
			
			if(callback!= null && callback!= undefined){
				callback.call(this);
			}
		}, 'searchLoading', null);
		
		return false;
	},
	approvePOAuto:function(type){
		$('#ErrorMsgStyle').hide();
		var listChoosePOAutoId = new Array();
		$('input:checkbox[name=choosePOAuto]:checked').each(function() {
			listChoosePOAutoId.push(this.value);
		});
		if(listChoosePOAutoId.length == 0) {
			$('#errMsg1').html('Vui lòng chọn đơn hàng').show();
			return;
		}
		var params = null;
		if (POAutoManage._searchParams) {
			params = POAutoManage._searchParams;
		} else {
			params = {};
		}
		params.listChoosePOAutoId = listChoosePOAutoId;
		var title = '';
		if(type == 1){
			title= 'Bạn có muốn duyệt đơn hàng này?';
		}else{
			title= 'Bạn có muốn hủy đơn hàng này?';
		}
		params.type = type;
		Utils.addOrSaveData(params, '/po/manage-po-auto/approve-po', null, 'errMsg1', function(data) {
			var tm = setTimeout(function(){
				POAutoManage.search();
				clearTimeout(tm);
			}, 3000);
		}, 'ApproveAndRefuseLoading',null,null,title, function(dataErr){
			if(dataErr.errorType ==2){
				POAutoManage.search(function(){
					$('#errMsg1').html(dataErr.errMsg).show();
				});
			}
		});
	},
	loadPOAutoDetail:function(poAutoId, poNumber){
		$('#ErrorMsgStyle').hide();
		var params = new Object();
		params.poAutoId = poAutoId;
		params.poNumber = poNumber;
		Utils.getHtmlDataByAjax(params, "/po/manage-po-auto/load-po-detail", function(data){
			try {
				var _data = JSON.parse(data);
				if(_data.error && _data.errMsg != undefined) {
					$('#errorSearchMsg').html(_data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errorSearchMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				}
			} catch(e) {
				$("#poAutoDetailTableDiv").html(data).show();
				$('#listPoAutoDetail').jScrollPane();
			}
		}, null, null);
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
		return false;
	},
	exportPOAutoReport:function(poAutoId){
		$('#ErrorMsgStyle').hide();
		var params = new Object();
		var shopId = $('#shopIdId').val().trim();
		params.poAutoId = poAutoId;
		params.shopId = shopId;
		var url = '/po/manage-po-auto/export';
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				} else {
					window.location.href = data.view;
					setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                    CommonSearch.deleteFileExcelExport(data.view);
					},2000);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$('#divOverlay').hide();
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	exportXml:function(){
		$('#ErrorMsgStyle').hide();
		var listChoosePOAutoId = new Array();
		$('input:checkbox[name=choosePOAuto]:checked').each(function() {
			listChoosePOAutoId.push(this.value);
		});
		if(listChoosePOAutoId.length == 0) {
			$('#errMsg1').html('Vui lòng chọn đơn hàng').show();
			return;
		}
		$('#divOverlay').show();
		var params = null;
		if (POAutoManage._searchParams) {
			params = POAutoManage._searchParams;
		} else {
			params = {};
		}
		params.listChoosePOAutoId = listChoosePOAutoId;
		var kData = $.param(params, true);
		$.ajax({
			type : "POST",
			url : "/po/manage-po-auto/export-xml",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();

				if (!data.error) {
					window.location.href = data.path;
					setTimeout(function() { // Set timeout để đảm bảo file
						// load lên hoàn tất
						CommonSearch.deleteFileExcelExport(data.path);
					}, 2000);
				} else {
					$('#errMsg1').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
	}
};
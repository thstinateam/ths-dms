var POInvoiceAdjustment = {
	//start Viet tam chua sua 2 ham check ben duoi	
	onCheckAllChange:function(checkAll) {
		Utils.onCheckAllChange(checkAll, 'CheckAll');
	},
	onChangePoAutoCheckbox: function(t) {
		var checkThis=$(t).is(':checked');
		Utils.onCheckboxChangeForCheckAll(checkThis, 'CheckAll', 'allCheck');
	},
	//end
	
	showAdjustmentForm : function(poConfirmId, invoiceNumber) {
		$('.ErrorMsgStyle').hide();
		$('#invoiceAdjustment').show();
		$('#poConfirmId').val(poConfirmId);
		$('#currentInvoiceNumber').val(invoiceNumber);
		$('#newInvoiceNumber').val('');
		$('#newInvoiceNumber').focus();
	},
	searchPoConfirm	: function(callback) {
		$('#invoiceAdjustment').hide();
		$('.ErrorMsgStyle').hide();
		
		var msg = Utils.getMessageOfRequireCheck('createDateFrom','Từ ngày' );
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('createDateTo','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('createDateFrom', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('createDateTo', 'Đến ngày');
		}
		var createDateFrom = $('#createDateFrom').val().trim();
		var createDateTo = $('#createDateTo').val().trim();
		if(msg.length ==0 && !Utils.compareDate(createDateFrom,createDateTo)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}

		if(msg.length > 0){
			$('#errorSearchMsg').html(msg).show();
			return false;
		}
		
		var invoiceNumber = $('#invoiceNumber').val().trim();
		var poNumber = $('#poNumber').val().trim();
		var shopId = $('#shopId').val().trim();
		params = new Object();
		if(invoiceNumber.length > 0) {
			params.invoiceNumber = invoiceNumber;
		}
		if(createDateFrom.length > 0) {
			params.createDateFrom = createDateFrom;
		}
		if(createDateTo.length > 0) {
			params.createDateTo = createDateTo;
		}
		if(poNumber.length > 0) {
			params.poNumber = poNumber;
		}
		params.shopId = shopId; 
		Utils.getHtmlDataByAjax(params, '/po/invoice-adjustment/search-po', function(data) {
			var myData;
			try{
				var myData = JSON.parse(data);
			} catch(err) {
				$('#listSearchPOAdjustSearch').html(data);
				$('.MySelectBoxClass1').customStyle();
				if(callback!= null && callback!= undefined){
					callback.call(this);
				}
				return;
			}
			if(myData.error == true && myData.errMsg != undefined) {
				$('#errorSearchMsg').show();
				$('#errorSearchMsg').html(myData.errMsg);
			} else {
				$('.ScrollSection').html(data);
			}
		}, 'poSearchLoading', 'POST');
	},
	
	adjustPOConfirm	: function() {
		$('.ErrorMsgStyle').hide();
		var msg = '';
		var lstPoVnmVO = [];
		
		var mapInvoiceNumber = new Map();	
		var is_success = false;
		$('.CheckAll').each(function(){
			if($(this).is(':checked')){
				var invoiceNum = $(this).attr('invoice-num'); // vuongmq; 06/08/2015; lay la Id 
				var poVnmId = $('#poVnmId' + Utils.XSSEncode(invoiceNum)).val().trim();
				var invoiceNumber = $('#invoiceNumber' + Utils.XSSEncode(invoiceNum)).html().trim();
				var newInvoiceNumber = $('#newInvoiceNumber' + Utils.XSSEncode(invoiceNum)).val().trim();
				if(invoiceNumber.toUpperCase() == newInvoiceNumber.toUpperCase()){
					msg = 'Số hóa đơn mới không được trùng với số hóa đơn hiện tại';
					$('#newInvoiceNumber' + Utils.XSSEncode(invoiceNum)).focus();
				}
				if(msg.length > 0) {
					$('#errMsg').html(msg).show();
					return;
				}
				if(newInvoiceNumber==null || newInvoiceNumber==''){
					if(msg.length == 0){
						msg = Utils.getMessageOfRequireCheck('newInvoiceNumber' + Utils.XSSEncode(invoiceNum), 'Số hóa đơn mới');
						$('#newInvoiceNumber' + Utils.XSSEncode(invoiceNum)).focus();
					}
					if(msg.length > 0) {
						$('#errMsg').html(msg).show();
						return;
					}
				}else{
					if(msg.length == 0){
						msg = Utils.getMessageOfSpecialCharactersValidate('newInvoiceNumber'+ Utils.XSSEncode(invoiceNum) , 'Số hóa đơn mới', Utils._CODE);
						$('#newInvoiceNumber'+ Utils.XSSEncode(invoiceNum)).focus();
					}
					if(msg.length > 0) {
						$('#errMsg').html(msg).show();
						return;
					}
				}
				
				if(mapInvoiceNumber.get(newInvoiceNumber)!=null){
					is_success = true;
					return false;
				}else{
					mapInvoiceNumber.put(newInvoiceNumber,newInvoiceNumber);
				}
				if($('#checkReason').is(':checked')){
					var apParamCode = $('#apParamCode').val().trim();
				}else{
					var apParamCode = $('#apParamCode'+invoiceNum).val().trim();
				}
				lstPoVnmVO.push({poVnmId:poVnmId, invoiceNumber:newInvoiceNumber, reason:apParamCode});
			}
		});		
		var isCountChecked = $('.CheckAll:checked').length;
		if(isCountChecked <= 0){
			msg = 'Chưa check chọn cho hóa đơn cần điều chỉnh';
		}
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			return;
		}
		if(is_success){
			msg = 'Số hóa đơn mới không được phép trùng';
		}
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			return;
		}
		var params = {lstPoVnmVO:lstPoVnmVO};
		JSONUtil.saveData2(params, '/po/invoice-adjustment/adjustment-po-confirm', "Bạn có muốn cập nhật số hóa đơn?", 'errMsg', function(data) {
			POInvoiceAdjustment.searchPoConfirm(function(){
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function() {
					$('#successMsg').hide();
					clearTimeout(tm);
				}, 3000);
			});
		});
	}
};
var POReturnProduct = {
	_xhrSave : null,	
	search : function() {
		$('#errorSearchMsg').html('').hide();
		var msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Ngày tạo từ');
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length ==0){
			var fromDate = $('#fromDate').val().trim();
			var toDate = $('#toDate').val().trim();
			if(!Utils.compareDate(fromDate, toDate)){
				msg = ' Ngày tạo từ phải nhỏ hơn hoặc bằng Đến ngày';		
				$('#fromDate').focus();
			}
		}
		if(msg.length > 0){
			$('#errorSearchMsg').html(msg).show();
			return false;
		}	
		var params = new Object();
		params.fromDate = $('#fromDate').val().trim();
		params.toDate = $('#toDate').val().trim();
		params.invoiceCode = $('#invoiceCode').val().trim();
		params.poConfirmCode = $('#poConfirmCode').val().trim();		
		Utils.getHtmlDataByAjax(params, '/po/return-product/search',function(data) {
			$("#productorderSearchTable").html(data).show();
			$("#productorderDetailTable").html('').hide();			
		}, 'loadingSearch', 'POST');
		return false;
	},
	detail : function(poConfirmCodeDetail,clearInput,callback) {
		var params = new Object();
		params.poConfirmCodeDetail = poConfirmCodeDetail;		
		Utils.getHtmlDataByAjax(params, '/po/return-product/detail',function(data) {
			$("#productorderDetailTable").html(data).show();		
			var err = $('#errorPOReturn').val();
			if (!isNullOrEmpty(err)) {
				$('#errMsgReturn').html(Utils.XSSEncode(err)).show();
			}
			if(callback!= null && callback!= undefined){
				callback.call(this);
			}	
		}, null, 'POST');
		var tabindex = -1;
		$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
			if (this.type != 'hidden') {
				$(this).attr("tabindex", tabindex);
				tabindex -=1;
			}
		});
		tabindex = 1;
		 $('input, select, button').each(function () {
    		 if (this.type != 'hidden') {
	    	     $(this).attr("tabindex", tabindex);
				 tabindex++;
    		 }
		 });
		return false;
	},
	returnProduct : function(){
		$('#errMsgReturn').hide();
		var lstPOVnmDetailLLotVoIDTemp = new Array();
		var lstPOVnmDetailLLotVoLotTemp = new Array();
		var lstPOVnmDetailLLotVoValueTemp = new Array();
		var lstSLChoPhepTemp = new Array();
		var lstStockTemp = new Array();
		var lstProduct = new Array();
		$('.Products').each(function(){
			lstProduct.push($(this).text());
		});
		$('.poVnmDetailLotVoID').each(function(){
			lstPOVnmDetailLLotVoIDTemp.push($(this).val());
		});
		$('.Lot').each(function(){
			lstPOVnmDetailLLotVoLotTemp.push($(this).text());
		});
		$('.SLChoPhep').each(function(){
			lstSLChoPhepTemp.push($(this).val());
		});
		var lstKho = new Array();
		$('.WarehouseId').each(function(){
			lstKho.push($(this).val());
		});
		var lstProductId = new Array();
		$('.ProductId').each(function(){
			lstProductId.push($(this).val());
		});
		var error = false;
		$('#vnmPoProductDetail .Count').each(function(){
			var convfact = $(this).attr('convfact');
			var quantity = getQuantity($(this).val().trim(),convfact);
			if(quantity=='' || quantity==0){
				quantity = 0;
			} else if(isNaN(quantity)) {
				$(this).focus();
				error = true;
				return;
			}
			lstPOVnmDetailLLotVoValueTemp.push(quantity);
		});
		if(error){
			$('#errMsgReturn').html("Giá trị số lượng trả phải là số nguyên dương.").show();
			return false;
		}
		
		$('#vnmPoProductDetail .Stock').each(function(){
			var convfact = $(this).attr('convfact');
			var quantity = getQuantity($(this).text(),convfact);
			if(quantity=='' || quantity==0){
				quantity = 0;
			}
			lstStockTemp.push(quantity);
		});
		
		
		var lstPOVnmDetailLLotVoID = new Array();
		var lstPOVnmDetailLLotVoLot = new Array();
		var lstPOVnmDetailLLotVoValue = new Array();
		
		var c = 0;
		for(var i=0;i<lstPOVnmDetailLLotVoIDTemp.length;i++){
			lstPOVnmDetailLLotVoID.push(lstPOVnmDetailLLotVoIDTemp[i]);
			lstPOVnmDetailLLotVoLot.push(lstPOVnmDetailLLotVoLotTemp[i]);
			lstPOVnmDetailLLotVoValue.push(lstPOVnmDetailLLotVoValueTemp[i]);
			if(lstPOVnmDetailLLotVoValueTemp[i] > lstStockTemp[i]){
				$('#errMsgReturn').html("Số lượng trả của mặt hàng vượt quá tồn kho. Vui lòng kiểm tra lại").show();
				return false;
			}
			if (Number(lstPOVnmDetailLLotVoValueTemp[i]) > 0) {
				c++;
			}
		}
		if (c <= 0) {
			$('#errMsgReturn').html("Không thể trả hàng khi số lượng trả của tất cả mặt hàng bằng 0. Vui lòng kiểm tra lại").show();
			return false;
		}
		for(var i=lstPOVnmDetailLLotVoIDTemp.length-1; i>0; i--){
			if(lstProduct[i]==lstProduct[i-1]){
				var quan = lstPOVnmDetailLLotVoValueTemp[i] + lstPOVnmDetailLLotVoValueTemp[i-1];
				var stock = lstStockTemp[i] + lstStockTemp[i-1];
				lstPOVnmDetailLLotVoValueTemp[i-1] = quan;
				lstPOVnmDetailLLotVoValueTemp.splice(i,1);
				lstStockTemp[i-1]= stock;
				lstStockTemp.splice(i,1);
				lstPOVnmDetailLLotVoIDTemp.splice(i,1);
				lstPOVnmDetailLLotVoLotTemp.splice(i,1);
				lstSLChoPhepTemp[i-1]=Number(lstSLChoPhepTemp[i]) + Number(lstSLChoPhepTemp[i-1]);
				lstSLChoPhepTemp.splice(i,1);
			}
		}
		for(var i=0; i<lstPOVnmDetailLLotVoIDTemp.length; i++){
			if(!isNaN(lstPOVnmDetailLLotVoValueTemp[i]) && Number(lstPOVnmDetailLLotVoValueTemp[i]) > 0) {
				if(lstPOVnmDetailLLotVoValueTemp[i] > lstStockTemp[i]){
					$('#errMsgReturn').html("Số lượng trả của mặt hàng vượt quá tồn kho. Vui lòng kiểm tra lại").show();
					return false;
				}
				if(lstPOVnmDetailLLotVoValueTemp[i] > lstSLChoPhepTemp[i]){
					$('#errMsgReturn').html("Số lượng trả của mặt hàng lớn hơn số lượng cho phép trả. Vui lòng kiểm tra lại").show();
					return false;
				}
			}
		}
		if(lstPOVnmDetailLLotVoID.length == 0) {
			$('#errMsgReturn').html("Số lượng nhập vào không hợp lệ. Hãy nhập lại").show();
			return false;
		}
		var params = new Object();
		params.lstPOVnmDetailLLotVoID = lstPOVnmDetailLLotVoID;
		params.lstPOVnmDetailLLotVoLot = lstPOVnmDetailLLotVoLot;
		params.lstPOVnmDetailLLotVoValue = lstPOVnmDetailLLotVoValue;
		params.lstWarehouseId = lstKho;
		params.lstProductIdKho = lstProductId;
		params.poConfirmCodeReturn = $('#poConfirmCodeReturn').val().trim();
		Utils.addOrSaveData(params, '/po/return-product/returnProduct', null,  'errMsgReturn', function(data) {
			POReturnProduct.detail($('#poConfirmCodeReturn').val().trim(),false, function(){
				POReturnProduct.enableBtnExportExcel();						
				$('#poVnmExportCode').val(data.poVnmExportCode);	
				$('#successMsgEx').html('Trả hàng thành công.').show();
				setTimeout(function(){$('#successMsgEx').hide();},3000);
			});
		}, 'loadingReturn', null, null,'Bạn có chắc chắn muốn trả hàng ?');
		return false;
	},
	enableBtnExportExcel : function(){
		$('#btnReturnProduct').addClass('BtnGeneralDStyle');
		$('#btnReturnProduct').attr('disabled',true);
		$('#btnExportExcel').removeAttr('disabled');
		$('#btnExportExcel').removeClass('BtnGeneralDStyle');
	},
	updateTotal :function(){
		var totalCount = 0;
		var totalMoney = 0; 
		var countArr = new Array();
		var convfactArr = new Array();
		$('.Count').each(function(){
			countArr.push($(this).val());
		});
		$('.Convfact').each(function(){
			convfactArr.push($(this).val());
		});
		for (var i =0 ; i< countArr.length ;++i){
			totalCount += Number(StockValidateInput.getQuantity(countArr[i], convfactArr[i]));
		}
		$('.Money').each(function(){
			totalMoney += Number(Utils.returnMoneyValue($(this).text()));
		});
		$('#totalQuantity').html(formatCurrency(totalCount));
		$('#totalAmount').html(formatCurrency(totalMoney));
	},
	changeInput: function (input,moneyId,price,convfact){
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact);
		var quantity = StockValidateInput.getQuantity(quantityTxt, convfact);
		$(input).val(quantityTxt);
		$('#'+moneyId).text(formatCurrency(quantity * price));
		POReturnProduct.updateTotal();
		return false;
	},
	exportExcel:function(){
		var poVnmExportCode = $('#poVnmExportCode').val().trim();
		var param = new Object();
		param.poVnmExportCode = poVnmExportCode;
		var msg = 'Bạn có muốn in đơn trả hàng chứ?';
		Utils.addOrSaveData(param, '/po/return-product/exportexcel', null, 'errMsgReturn', function(data){
			if(data.error && data.errMsg!= undefined){
				$('#errMsgReturn').html(Utils.XSSEncode(data.errMsg)).show();
			} else {
				window.location.href = data.view;
				setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
                    CommonSearch.deleteFileExcelExport(data.view);
				},2000);
			}
		}, 'loadingReturn', null, null, msg);
		return false;
	}
};
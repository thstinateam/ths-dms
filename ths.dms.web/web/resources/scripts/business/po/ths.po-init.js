var POInit = {
	totalStockNumber : 0,
	totalMoney : 0,
	convertPO : function() {
		$(".ErrorMsgStyle").hide();
		if($('#poDataGrid').datagrid('getRows').length == 0) {
			$('#errMsg').html('Không có mặt hàng trong đơn hàng').show();
			return;
		}
		var params = new Object();
		var checkAllProductType = true;
		params.checkAllProductType = checkAllProductType;
		Utils.addOrSaveData(params, '/po/init-po-auto/convert', null, 'errMsg', function(data) {
		}, 'convertLoading');
	},
	
	exportExcelPO : function(){
		$(".ErrorMsgStyle").hide();
		if($('#poDataGrid').datagrid('getRows').length == 0) {
			$('#errMsg').html('Không có mặt hàng trong đơn hàng').show();
			return;
		}
		var params = new Object();
		var checkAllProductType = true;
		params.checkAllProductType = checkAllProductType;
		ReportUtils.exportReport('/po/init-po-auto/export-po', params);
	}
};
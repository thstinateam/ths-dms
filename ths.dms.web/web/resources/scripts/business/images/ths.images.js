
var Images = {
	maxPopup:20,
	lstCTTB : null,
	lstCTTB_temp : null,
	pagePopup:null,
	lstImages:null,
	indexCurrent:null,//id big image
	isEndImages:0,
	callBack:null,
	customerId:null,
	filterSearchAlbum: null,
	filterAlbum: null,
	filterImageOfAlbum: null,
	displayProgrameId:null,
	displayProgrameCode:null,
	isGroup: null,
	shop_type : null,
	step : 0,
	checkScrollImageOfAlbum: null,
	scrollGetImageForPopup:null,
	checkDeleteImage: null,
	isFirstLoad:true,
	
	/**
	 * Tim kiem danh sach hinh anh
	 * 
	 * @author hoanv25
	 * @since July 03, 2015
	 * */
	btnSearchClick:function(){
		var errMsg = "";
		var curFocus = '';
		$('#errMsg').html('').hide();
		$('#imageContent').html('').show();
		if(errMsg.length == 0){
			errMsg = Utils.getMessageOfRequireCheck('fromDate',image_manager_fromdate);
			curFocus = 'fromDate';
		}
		if(errMsg.length == 0){
			errMsg = Utils.getMessageOfRequireCheck('toDate',image_manager_todate);
			curFocus = 'toDate';
		}
		var fromDate = $('#fromDate').val();
		if(errMsg.length == 0){
			errMsg = Utils.getMessageOfInvalidFormatDateEx('fromDate', image_manager_fromdate);
			curFocus = 'fromDate';
		}
		var toDate = $('#toDate').val();
		if (errMsg.length == 0){
			errMsg = Utils.getMessageOfInvalidFormatDateEx('toDate', image_manager_todate);
			curFocus = 'toDate';
		}
		if(!Utils.compareDate(fromDate, toDate) && errMsg.length==0){
			errMsg = msgErr_fromdate_greater_todate;		
			curFocus = 'fromDate';
		}
		if(errMsg.length==0 && toDate!='' && fromDate!=''){
			var arrFromDate = fromDate.split('/');
			var arrToDate = toDate.split('/');
			var countMonthF=(parseInt(arrFromDate[2])*12)+parseInt(arrFromDate[1]);
			var countMonthT=(parseInt(arrToDate[2])*12)+parseInt(arrToDate[1]);
			if(countMonthT-countMonthF>2){
				errMsg =image_manager_search_error_01;
				curFocus = 'fromDate';
			}
		}
		if(errMsg.length == 0){
			errMsg = Utils.getMessageOfSpecialCharactersValidate('customerCode',image_manager_custcode,Utils._CODE);
			curFocus = 'customerCode';
		}
		if(errMsg.length == 0){
			errMsg = Utils.getMessageOfSpecialCharactersValidate('customerAddress',image_manager_nameoraddress,Utils._NAME);
			curFocus = 'customerAddress';
		}
		if(errMsg.length == 0 && $('#level').val().trim()!=''){
			var str=$('#level').val().trim().split(',');
			for(var i=0;i<str.length;i++){
				if(str[i]==''){
					errMsg=image_manager_search_error_02;
					curFocus = 'level';
				}
			}
		}
		/*if(errMsg.length == 0 && $('#monthSeq').val().trim()!=''){
			if(!/^[0-9,\-]+$/.test($('#monthSeq').val().trim())){
				errMsg='Lần chụp chỉ được nhập từ 0-9 ,kí tự "," và "-"';
				curFocus = 'monthSeq';
			}else{
				var str=$('#monthSeq').val().trim().split(',');
				for(var i=0;i<str.length;i++){
					var s=str[i].split('-');
					if(s.length>2 || (s.length>1 && (s[0]=='' || s[1]==''))){
						errMsg='Lần chụp không đúng định dạng [1,2,3-5,6]';
						curFocus = 'monthSeq';
						break;
					}
				}
			}
		}*/
		if(errMsg.length > 0){
			$('#errMsg').html(errMsg).show();
			$('#' + curFocus).focus();
			$('#imageContent').html('').show();
			return false;
		}
		Images.setParamRequest();
		$('.TabSection').show();
		$('#tab4').click();
	},
	search:function(){
		Images.displayProgrameId=null;
		var dataModel = Images.getParamRequest();
		var url='/images/searchEx';
		if($('#tab4').hasClass('Active')){
			url='/images/search';
		}
		Utils.getHtmlDataByAjax(dataModel,url,function(html){
			$('#imageContent').html(html).show(); // show Album
			if($('#errMsgDate').val().trim().length >0) {
				$('#errMsg').html($('#errMsgDate').val()).show();
				$('#fromDate').focus();
			}
			$("#title").html(image_manager_search_image).show();
		},null,null);
	},	
	setParamRequest:function(){
		Images.filterSearchAlbum = new Object();
		Images.filterAlbum = new Object();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = new Array();
			for(var i=0;i<dataShop.length;i++){
				lstShop.push(dataShop[i].shopId);
			}
			Images.filterSearchAlbum.lstShop =lstShop;
		}
		var multiStaff = $("#staff").data("kendoMultiSelect");
		var dataStaff = multiStaff.dataItems();
		if(dataStaff!=null && dataStaff.length>0){
			var lstStaff = new Array();
			for(var i=0;i<dataStaff.length;i++){
				lstStaff.push(dataStaff[i].staffId);
			}
			Images.filterSearchAlbum.lstStaffSearch =lstStaff;
		}
		Images.filterSearchAlbum.tuyen = $('#tuyen').val() ;
		Images.filterSearchAlbum.fromDate = $('#fromDate').val().trim();
		Images.filterSearchAlbum.toDate = $('#toDate').val().trim();
		Images.filterSearchAlbum.customerCode = $('#customerCode').val().trim();
		Images.filterSearchAlbum.customerNameOrAddress = $('#customerAddress').val().trim() ;
		
		/** edit by @author tientv11: Search CTTB  @update hoanv25: Search Clb*/
		var	lstCttb = new Array();
		var cttKendo = $("#cttb").data("kendoMultiSelect");
		var lstCTTId = cttKendo.value();
		if(lstCTTId!=null && lstCTTId.length>0){		
			for(var i=0;i<lstCTTId.length;i++){
				lstCttb.push(lstCTTId[i]);
			}	
		}	
		Images.filterSearchAlbum.lstCTTB = lstCttb;
		if($('#level').val().trim()!='')
			Images.filterSearchAlbum.level = $('#level').val().trim();
		/*if($('#lastSeq').is(':checked')){
			Images.filterSearchAlbum.lastSeq = true;
		}else if($('#monthSeq').val().trim()!=''){
			var monthSeq = $('#monthSeq').val().trim();
			var str=monthSeq.split(',');
			var lstMonthSeq = new Map();
			for(var i=0;i<str.length;i++){
				var s=str[i].split('-');
				if(s.length==2){
					for(var j=parseInt(s[0]),n=parseInt(s[1]);j<=n;j++){
						lstMonthSeq.put(parseInt(j));
					}
				}else{
					lstMonthSeq.put(parseInt(str[i]));
				}
			}
			var arrayMonthSeq = new Array();
			for(var i=0;i<lstMonthSeq.keyArray.length;i++){
				arrayMonthSeq.push(lstMonthSeq.keyArray[i]);
			}
			Images.filterSearchAlbum.monthSeq = arrayMonthSeq;
		}*/
	},
	getParamRequest:function(){
		var params=new Object();
		if(Images.filterSearchAlbum!=null){
			if(Images.filterSearchAlbum.lstShop!=undefined && Images.filterSearchAlbum.lstShop!=null)
				params.lstShop = Images.filterSearchAlbum.lstShop;
			if(Images.filterSearchAlbum.lstStaffSearch!=undefined && Images.filterSearchAlbum.lstStaffSearch!=null)
				params.lstStaffSearch  = Images.filterSearchAlbum.lstStaffSearch ;
			if(Images.filterSearchAlbum.tuyen!=undefined && Images.filterSearchAlbum.tuyen!=null)
				params.tuyen = Images.filterSearchAlbum.tuyen ;
			if(Images.filterSearchAlbum.fromDate!=undefined && Images.filterSearchAlbum.fromDate!=null)
				params.fromDate = Images.filterSearchAlbum.fromDate ;
			if(Images.filterSearchAlbum.toDate!=undefined && Images.filterSearchAlbum.toDate!=null)
				params.toDate = Images.filterSearchAlbum.toDate ;
			if(Images.filterSearchAlbum.customerCode!=undefined && Images.filterSearchAlbum.customerCode!=null)
				params.customerCode = Images.filterSearchAlbum.customerCode;
			if(Images.filterSearchAlbum.customerNameOrAddress!=undefined && Images.filterSearchAlbum.customerNameOrAddress!=null)
				params.customerNameOrAddress = Images.filterSearchAlbum.customerNameOrAddress ;
		}
		if(Images.customerId!=undefined && Images.customerId!=null)
			params.customerId=Images.customerId;
		if($('#tab1').hasClass('Active')){
			params.objectType = 2;
		}else if($('#tab2').hasClass('Active')){
			params.objectType = 0;
		}else if($('#tab3').hasClass('Active')){
			params.objectType = 1;
		}else {
			params.objectType = 4;
			if(Images.filterSearchAlbum!=null){
				if(Images.filterSearchAlbum.level!=undefined && Images.filterSearchAlbum.level!=null
						&& Images.filterSearchAlbum.level!='')
					params.level = Images.filterSearchAlbum.level;
				/*if(Images.filterSearchAlbum.monthSeq!=undefined && Images.filterSearchAlbum.monthSeq!=null 
						&& Images.filterSearchAlbum.monthSeq.length>0)
					params.monthSeq = Images.filterSearchAlbum.monthSeq;
				if(Images.filterSearchAlbum.lastSeq!=undefined && Images.filterSearchAlbum.lastSeq!=null)
					params.lastSeq = Images.filterSearchAlbum.lastSeq;
				*/	
				if(Images.displayProgrameId!=undefined && Images.displayProgrameId!=null){
					params.displayProgrameId=Images.displayProgrameId;
				}
				if(Images.filterSearchAlbum.lstCTTB!=undefined && Images.filterSearchAlbum.lstCTTB!=null)
					params.lstCTTB = Images.filterSearchAlbum.lstCTTB;
			}
		}
		return params;
	},
	/*cbLastSeqChange:function(){
		if($('#lastSeq').is(':checked')){
			$('#monthSeq').val('');
			$('#monthSeq').attr('disabled','disabled');
		}else $('#monthSeq').removeAttr('disabled');
	},*/
	showAlbum :function(displayProgrameId) {
		$('#errMsg').html('').hide();
		var params = Images.getParamRequest();
		params.max = Images.maxPopup;
		params.displayProgrameId = displayProgrameId;
		Utils.getHtmlDataByAjax(params, '/images/showAlbum',function(data) {
			$("#title").html(image_manager_imageofprogram).show();
			Images.filterAlbum = new Object();
			Images.filterAlbum.page = 0;
			Images.filterAlbum.isEndAlbum = false;
			
			Images.filterImageOfAlbum = new Object();
			Images.filterImageOfAlbum.page = 0;
			Images.filterImageOfAlbum.isEndImgOfAlbum = false;
			Images.isGroup = false;
			Images.displayProgrameId = displayProgrameId;
			$("#imageContent").html(data).show();
			Images.displayProgrameCode = $('#displayProgrameCodeHid').val();
			Images.showAlbumSelect(false);
		}, null, 'POST');
		return false;
	},
	exportAlbum :function(displayProgrameId) {
		$('#errMsg').html('').hide();
		var params = Images.getParamRequest();
		var typeGroup = $('#typeGroup').val();
		var id=$('#id').val();
		if(typeGroup!=undefined && typeGroup!=null && typeGroup!=''
			&& id!=undefined && id!=null && id!=''){
			params.id=id;
			params.typeGroup=typeGroup;
		}
		var msg= image_manager_search_error_03 + Images.displayProgrameCode;
		$.messager.confirm(image_manager_confirm, msg, function(r){
			if (r){
				$('#errMsg').html('').hide();
				$('#divOverlay').show();
				params.token = Utils.getToken();
				try {
					params.reportCode = $('#function_code').val();
				} catch(e) {
					// Oop! Error
				}
				var kData = $.param(params, true);
				$.ajax({
					type : 'GET',
					url : '/images/exportAlbum',
					data :(kData),
					dataType : "json",
					success : function(data) {
						$('#divOverlay').hide();
						if(data.error && data.errMsg!= undefined){
							$('#errMsg').html(image_manager_search_error_04 + escapeSpecialChar(data.errMsg)).show();
						} else{
							if(data.hasData) {
								var filePath = ReportUtils.buildReportFilePath(data.view);
								window.location.href = filePath;
							} else{
								$('#errMsg').html(image_manager_search_error_05).show();
							}
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
						$.ajax({
							type : "POST",
							url : '/check-session',
							dataType : "json",
							success : function(data, textStatus, jqXHR) {
								var linkReload = window.location.href;
								if(linkReload.indexOf('?')>0){
									if(linkReload.indexOf('newToken') < 0){
										window.location.href = window.location.href + '&newToken=1';
									} else {
										window.location.reload(true);
									}
								} else {
									window.location.href = window.location.href + '?newToken=1';
								}						
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								window.location.href = '/home';
							}
						});
					}
				});
			}
		});			
		return false;
	},
	resize : function(){
		heightContent = window.innerHeight - $('#header').height() - $('.BreadcrumbSection').height() - $('#footer').height();
		if($('.ContentSection').height() > heightContent ) {
			heightContent = $('.ContentSection').height();
		}
		$('.SidebarInSection').css({height:heightContent + 'px'});
		if($('.PhotoGroupSection').length) {
			$('.PhotoGroupSection').css({height:heightContent + 'px'});
		}
	},
	hideAllTab: function(){
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#tab4').removeClass('Active');
	},
	showTab1:function(){
		Images.hideAllTab();
		$('#tab1').addClass('Active');
		Images.showAlbumDetail();
	},
	showTab2:function(){
		Images.hideAllTab();
		$('#tab2').addClass('Active');
		Images.showAlbumDetail();
	},
	showTab3:function(){
		Images.hideAllTab();
		$('#tab3').addClass('Active');
		Images.showAlbumDetail();
	},
	showTab4:function(){
		Images.hideAllTab();
		$('#tab4').addClass('Active');
		Images.search();
	},
	showAlbumDetail :function(displayProgrameId) {
		$('#errMsg').html('').hide();
		if(displayProgrameId!=undefined && displayProgrameId!=null)
			Images.displayProgrameId = displayProgrameId;
		Images.filterImageOfAlbum = new Object();
		Images.filterImageOfAlbum.page=0;
		Images.filterImageOfAlbum.isEndImgOfAlbum = false;
		var params = Images.getParamRequest();
		params.page=0;
		params.max = Images.maxPopup;
		var url = '/images/searchEx';
		Utils.getHtmlDataByAjax(params,url ,function(data) {
			Images.isGroup = false;
			$("#imageContent").html(data).show();
			if($('#tab1').hasClass('Active')) {
				$('#titleAlbum').append(image_manager_search_error_06);
			}else if($('#tab2').hasClass('Active')){
				$('#titleAlbum').append(image_manager_search_error_07);
			}else if($('#tab3').hasClass('Active')){
				$('#titleAlbum').append(image_manager_search_error_08);
			}else{
				$('#titleAlbum').html(image_manager_search_error_09+$('#titleAlbum').html());
			}
			Images.displayProgrameCode = $('#displayProgrameCodeHid').val();
			Images.resize();
		}, null, 'POST');
		return false;
	},
	addListAlbumSelect :function(){
		var params = Images.getParamRequest();
		Images.filterAlbum.page= Images.filterAlbum.page + 1;
		params.typeGroup = $('#typeGroup').val();
		params.page = Images.filterAlbum.page;
		params.max = Images.maxPopup;
		Utils.getJSONDataByAjaxNotOverlay(params, '/images/addListAlbumSelect',function(data) {
			Images.fillAlbum(data.lstAlbum,data.typeGroup);
			Images.lazyLoadImages(".ImageAlbum", "#content");
			Images.filterAlbum.checkScrollAlbum == 1;
		}, null,'POST');
		return false;
	},
	showAlbumSelect :function(typeGroup,id) {
		$('#errMsg').html('').hide();
		Images.filterImageOfAlbum = new Object();
		Images.filterImageOfAlbum.isEndImgOfAlbum = false;
		Images.filterImageOfAlbum.page=0;
		var params = Images.getParamRequest();
		if(typeGroup!=undefined && typeGroup!=null) params.typeGroup = typeGroup;
		if(id!=undefined && id!=null) params.id = id;
		params.page = 0;
		params.max = Images.maxPopup;
		Images.filterAlbum = new Object();
		Images.filterAlbum.page = 0;
		Images.filterAlbum.isEndAlbum = false;
		Utils.getHtmlDataByAjax(params, '/images/showAlbumSelect',function(data) {
			$("#imageContent").html(data).show();
			$(".ImageOfAlbum").lazyload({
			    event: "scrollstop"
			});
		}, null,'POST');
		return false;
	},
	changeGroup:function(typeGroup,id){
		$('#errMsg').html('').hide();
		var params = Images.getParamRequest();
		if(typeGroup!=undefined && typeGroup!=null){
			params.typeGroup = typeGroup;
			$('#typeGroup').val(typeGroup);
		}
		if(id!=undefined && id!=null){
			params.id=id;
			$('#id').val(id);
		}
		Images.filterImageOfAlbum = new Object();
		Images.filterImageOfAlbum.page = 0;
		Images.filterImageOfAlbum.isEndImgOfAlbum = false;
		params.page = 0;
		params.max = Images.maxPopup;
		$('#albumContentDetail').html('');
		$('#divOverlayAddImage').show();
		Utils.getJSONDataByAjaxNotOverlay(params, '/images/addAlbumSelect',function(data) {
			if(data.title!=undefined && data.title !=''){
				$('#titleAlbum').html(image_manager_search_error_10 + data.title);
			}else $('#titleAlbum').html(image_manager_search_error_11);
			Images.fillImagesOfAlbum(data.lstImage,Images.isGroup,true);
			$(".ImageOfAlbum").lazyload({
			    event: "scrollstop"
			});
			Images.checkScrollImageOfAlbum = 1;
			$('#divOverlayAddImage').hide();
		}, null,'POST');
		return false;
	},
	addImageOfAlbumSelect: function(typeGroup,id){
		$('#errMsg').html('').hide();
		var params = Images.getParamRequest();
		if(typeGroup!=undefined && typeGroup!=null) params.typeGroup = typeGroup;
		if(id!=undefined && id!=null) params.id=id;
		Images.filterImageOfAlbum.page ++;
		params.page = Images.filterImageOfAlbum.page ;
		params.max = Images.maxPopup;
		$('#divOverlayAddImage').show();
		Utils.getJSONDataByAjaxNotOverlay(params, '/images/addAlbumSelect',function(data) {
			Images.fillImagesOfAlbum(data.lstImage,Images.isGroup,true);
			$(".ImageOfAlbum").lazyload({
			    event: "scrollstop"
			});
			Images.checkScrollImageOfAlbum = 1;
			$('#divOverlayAddImage').hide();
		}, null,'POST');
		return false;
	},
	loadImageOfAlbumAfterDelete:function(){
		$('#errMsg').html('').hide();
		var params = Images.getParamRequest();
		params.isGroup = Images.isGroup;
		params.page = 0;
		if(Images.filterImageOfAlbum.page > 0){
			params.max = Images.maxPopup * (Images.filterImageOfAlbum.page+1);
		} else {
			params.max = Images.maxPopup;
		}
		Utils.getJSONDataByAjaxNotOverlay(params, '/images/showAlbumSelect',function(data) {
			Images.fillImagesOfAlbum(data.lstImage,Images.isGroup,false);
			Images.filterImageOfAlbum.page--;
			$(".ImageOfAlbum").lazyload({
			    event: "scrollstop"
			});
		}, null,'POST');
		return false;
	},
	fillAlbum: function(lstAlbum,typeGroup){
		var listImage = '';
		if(lstAlbum!=undefined && lstAlbum!=null && lstAlbum.length>0){
			for (var i = 0; i < lstAlbum.length; i++) {
				var currentMil = new Date().getTime();
				listImage += '<li>';
				listImage += '<a href="javascript:void(0)" class="PhotoThumbGroup" onclick="Images.changeGroup(' +typeGroup+',' +Utils.XSSEncode(lstAlbum[i].customerId)+')">';
				listImage += '        <span class="BoxFrame">';
				listImage += '        	<span class="BoxMiddle">';
				listImage += '        	<img id="imagesOfAlbum_' +lstAlbum[i].id+'" width="142" height="102" class="ImageAlbum ImageFrame2"  src="/resources/images/grey.jpg" data-original="' +imgServerSOPath+lstAlbum[i].urlThum+'?v=' +currentMil+'">';
				listImage += '       	</span>';
				listImage += '        </span>';
				listImage += '</a>';
				listImage += '<p class="Text3Style"><a href="javascript:void(0)" onclick="Images.changeGroup(' +typeGroup+',' +Utils.XSSEncode(lstAlbum[i].customerId)+')">';
				listImage += Utils.XSSEncode(lstAlbum[i].customerCode)+' - ' +Utils.XSSEncode(lstAlbum[i].customerName)+' (' +lstAlbum[i].countImg+')</a></p>';
				listImage += '</li>';
			}
		} else {
			Images.filterAlbum.isEndAlbum = true;
		}
		$('#lstAlbumContent').append(listImage);
		Images.resize();
	},
	fillImagesOfAlbum: function(lstImage,isGroup,isAddImage){
		var listImage = '';
		if(lstImage!=undefined && lstImage!=null && lstImage.length>0){
			for (var i = 0; i < lstImage.length; i++) {
				listImage += '<li>';
				var currentMil = new Date().getTime();
				listImage += '<a href="javascript:void(0)" class="PhotoThumbGroup1" onclick="Images.showDialogFancy(' +lstImage[i].id+')">';
				listImage += '<span class="BoxFrame"><span class="BoxMiddle">';
				listImage += '<img  class="ImageOfAlbum ImageFrame3" src="/resources/images/grey.jpg" data-original="' +(imgServerSOPath+lstImage[i].urlThum+'?v=' +currentMil)+'" width="201" height="144" />';
				listImage += '</span></span></a>';
				listImage += '<p class="Text1Style"><a onclick="Images.showDialogFancy(' +lstImage[i].id+')" href="javascript:void(0)">' +Utils.XSSEncode(lstImage[i].shopCode)+' - ' +Utils.XSSEncode(lstImage[i].customerCode)+'-' +Utils.XSSEncode(lstImage[i].customerName)+'</a></p>';
				listImage += '<p class="Text2Style">' +lstImage[i].createDateStr+'</p>';
				listImage += '</li>';
			}
		} else {
			Images.filterImageOfAlbum.isEndImgOfAlbum = true;
		}
		if(isAddImage) {
			$('#albumContentDetail').append(listImage);
		} else {
			$('#albumContentDetail').html(listImage);
		}
		Images.resize();
	},
	
	//------------------------------------------------------------------------------------------------------------------------------------------------------
	//TUNGMT------------------------------------------------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------------------------------------
	onOffMap:function(i){
		if(i==0){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
			$('.OnFucnStyle').show();
			$('.OnFucnStyle').css('display','block');
			$('.fancybox-inner #divMapSection').removeClass('SmallMapSection');
			$('.fancybox-inner #divMapSection').addClass('LargeMapSection');
			$('.fancybox-inner #bigMap').css('width','488px');
			$('.fancybox-inner #bigMap').css('height','409px');
		}else{
			$('.OnFucnStyle').hide();
			$('.OnFucnStyle').css('display','none');
			$('.OffFucnStyle').show();
			$('.OffFucnStyle').css('display','block');
			$('.fancybox-inner #divMapSection').removeClass('LargeMapSection');
			$('.fancybox-inner #divMapSection').addClass('SmallMapSection');
			$('.fancybox-inner #bigMap').css('width','140px');
			$('.fancybox-inner #bigMap').css('height','120px');
		}
		Images.openMapOnImage();
	},
	imageBigShow : function() {
		$('.fancybox-inner #loadingImages').hide();
		$('.fancybox-inner #bigImages').fadeIn(1000);
	},
	imageLoadingShow : function() {
		$('.fancybox-inner #bigImages').hide();
		$('.fancybox-inner #loadingImages').show();
	},
	lazyLoadImages:function(object,container){
		$(object).lazyload({
//			effect : "fadeIn",
			event: "scrollstop",
			container : $(container),
			effect: "fadeIn",
	        failurelimit: 1,
	        threshold: -20
		});
		$(object).each(function() {
		    $(this).attr("src", $(this).attr("data-original"));
		    $(this).removeAttr("data-original");
		});
	},
	getImagesForPopup:function(callBack,countPage){//countPage: load hình ảnh từ trang 0->countPage
		
		var data=Images.getParamRequest();
		var typeGroup = $('#typeGroup').val();
		var id=$('#id').val();
		if(typeGroup!=undefined && typeGroup!='' && id!=undefined && id!=''){
			data.typeGroup=typeGroup;
			data.id=id;
		}
		if(countPage!=undefined && countPage!=null && countPage!=0){
			data.max=Images.maxPopup*(countPage+1);
			data.page=0;
			Images.pagePopup=countPage;
		}else{
			data.max=Images.maxPopup;
			data.page=Images.pagePopup;
		}
		if(data.page==0) scrollIntoViewPopup=1;
		if(callBack!=undefined && callBack!=null){
			Images.callBack=callBack;
		}
		$('.loadingPopup').show();
		var kData = $.param(data, true);
		$.ajax({
			type : 'POST',
			url : '/images/get-images-for-popup',
			data :(kData),
			dataType : "html",
			success : function(data) {
				Images.scrollGetImageForPopup=1;
				try {
					$('.loadingPopup').hide();
					var myData = JSON.parse(data);
					if(myData.error){
						
					}else{
						var lstImage = myData.lstImage;
						var listNew=new Map();
						if(lstImage!=undefined && lstImage!=null && lstImage.length>0){
							Images.pagePopup++;//nếu có dữ liệu thì + trang lên 1
							for(var i=0;i<lstImage.length;i++){
								var currentMil = new Date().getTime();
								lstImage[i].urlEx=lstImage[i].urlImage;
								lstImage[i].urlImage=imgServerSOPath+lstImage[i].urlImage+'?v=' +currentMil;
								lstImage[i].urlThum=imgServerSOPath+lstImage[i].urlThum+'?v=' +currentMil;
								if(lstImage[i].shopCode==undefined || lstImage[i].shopCode==null) lstImage[i].shopCode='';
								if(lstImage[i].shopName==undefined || lstImage[i].shopName==null) lstImage[i].shopName='';
								if(lstImage[i].staffCode==undefined || lstImage[i].staffCode==null) lstImage[i].staffCode='';
								if(lstImage[i].staffName==undefined || lstImage[i].staffName==null) lstImage[i].staffName='';
								if(Images.lstImages.get(lstImage[i].id)==null){
									listNew.put(lstImage[i].id,lstImage[i]);
								}
								Images.lstImages.put(lstImage[i].id,lstImage[i]);
							}
						}else{//het danh sach
							if(Images.pagePopup==0){//het hinh thi đóng fancybox
								$.fancybox.close();
							}
							Images.isEndImages=1;
							if(Images.indexCurrent==null){
								Images.indexCurrent=Images.lstImages.keyArray[Images.lstImages.keyArray.length-1];
							}
							if(Images.callBack!=undefined && Images.callBack!=null){
								Images.callBack.call();
							}
						}
						Images.fillThumbnails(listNew);
						
					}
				} catch (err) {
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				Images.scrollGetImageForPopup=1;
			}
		});
	},
	showDialogFancy:function(mediaItemId,customerId) {
		if($('.OffFucnStyle').is(':visible')){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
		}else{
			if($('.OnFucnStyle').is(':visible')){
				$('.OnFucnStyle').hide();
				$('.OnFucnStyle').css('display','none');	
			}
		}
		var nameId='popup-images';
		var title = '';
		if(Images.displayProgrameCode!=undefined && Images.displayProgrameCode!=null && Images.displayProgrameCode!=''){
			title=Images.displayProgrameCode;
			title +=' (<span id="titlePopup"></span>)';
		}else{
			title +='<span id="titlePopup"></span>';
		}
		title += '<img class="loadingPopup" src="/resources/images/loading.gif" width="16" height="16" alt="' +image_manager_search_error_12+'" class="LoadingStyle" />';
		if(customerId!=undefined && customerId!=null) {
			title += '<span id="maximizePopup" class="Maximize"><img onclick="Images.showDialogFancyFull(' +mediaItemId+', ' +customerId+')" src="/resources/images/dialog/maximize-window.gif" width="19" height="19" alt="' +image_manager_search_error_13+'" style="float: right; cursor: pointer; margin-top: -5px; padding-right: 33px;" /></span>';
		} else {
			title += '<span id="maximizePopup" class="Maximize"><img onclick="Images.showDialogFancyFull(' +mediaItemId+')" src="/resources/images/dialog/maximize-window.gif" width="19" height="19" alt="' +image_manager_search_error_13+'" style="float: right; cursor: pointer; margin-top: -5px; padding-right: 33px;" /></span>';
		}
		var html = $('#' + nameId).html();
		if(customerId!=undefined && customerId!=null)
			Images.customerId=customerId;
		if(mediaItemId!=undefined && mediaItemId!=null)
			Images.indexCurrent=mediaItemId;
		$('#' + nameId).html('');
		Images.checkDeleteImage = null;
		
		$.fancybox(html, {
			'title' : title,
			'closeBtn' : true,
			width : '1300px',
			height : '900px',
			helpers : {
				title : {
					type : 'inside'
				}
			},
			'afterShow' : function() {
				if(mediaItemId!=undefined && mediaItemId!=null)
					Images.indexCurrent=mediaItemId;
				if($('#tab4').hasClass('Active')){
					$('.fancybox-inner #result').show();
					$('.fancybox-inner #lblResult').show();
				}else{
					$('.fancybox-inner #lblResult').hide();
					$('.fancybox-inner #result').hide();
				}
				Images.pagePopup=0;
				Images.isEndImages=0;
				Images.lstImages=new Map();
				if(Images.filterImageOfAlbum.page!=undefined && Images.filterImageOfAlbum.page!=null && Images.filterImageOfAlbum.page>0)
					Images.getImagesForPopup(null,Images.filterImageOfAlbum.page);
				else
					Images.getImagesForPopup();
				Images.openMapOnImage();
				Images.scrollGetImageForPopup=1;//chặn các request giống nhau khi scroll liên tục
				$('.fancybox-inner #listImage').scroll(function(){
				   if (Images.scrollGetImageForPopup==1 && Images.isEndImages==0 && $(this).outerHeight() > ($(this).get(0).scrollHeight - $(this).scrollTop()-2)){
					   Images.scrollGetImageForPopup=0;
					   Images.getImagesForPopup();
				   }
				});
			},
			'afterClose' : function() {
				$('#' + nameId).html(html);
				Images.indexCurrent=null;
				Images.callBack=null;
				Images.customerId=null;
			},
			'beforeClose': function () {
				if(Images.checkDeleteImage != undefined && Images.checkDeleteImage != null && Images.checkDeleteImage == true) {
					Images.loadImageOfAlbumAfterDelete();
					Images.checkDeleteImage = null;
				}
			}
		});
		return false;
	},
	showDialogFancyFull:function(mediaItemId,customerId) {
		if($('.OffFucnStyle').is(':visible')){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
		}else{
			if($('.OnFucnStyle').is(':visible')){
				$('.OnFucnStyle').hide();
				$('.OnFucnStyle').css('display','none');	
			}
		}
		
		var nameId='popup-images-full';
		var title = '';
		if(Images.displayProgrameCode!=undefined && Images.displayProgrameCode!=null && Images.displayProgrameCode!=''){
			title=Images.displayProgrameCode;
			title +=' (<span id="titlePopup"></span>)';
		}else{
			title +='<span id="titlePopup"></span>';
		}
		title += '<img class="loadingPopup" src="/resources/images/loading.gif" width="16" height="16" alt="' +image_manager_search_error_12+'" class="LoadingStyle" />';
		if(customerId!=undefined && customerId!=null) {
			title += '<span id="minimizePopup" class="Minimize"><img src="/resources/images/dialog/minimize-window.gif" onclick="Images.showDialogFancy(' +mediaItemId+', ' +customerId+')" width="19" height="19" alt="' +image_manager_search_error_13+'" style="float: right; cursor: pointer; margin-top: -5px; padding-right: 33px;" /></span>';
		} else {
			title += '<span id="minimizePopup" class="Minimize"><img src="/resources/images/dialog/minimize-window.gif" onclick="Images.showDialogFancy(' +mediaItemId+')" width="19" height="19" alt="' +image_manager_search_error_13+'" style="float: right; cursor: pointer; margin-top: -5px; padding-right: 33px;" /></span>';
		}
		var html = $('#' + nameId).html();
		if(customerId!=undefined && customerId!=null)
			Images.customerId=customerId;
		if(mediaItemId!=undefined && mediaItemId!=null)
			Images.indexCurrent=mediaItemId;
		$('#' + nameId).html('');
		Images.checkDeleteImage = null;
		
		$.fancybox(html, {
			'title' : title,
			'closeBtn' : true,
			width : '1300px',
			height : '900px',
			helpers : {
				title : {
					type : 'inside'
				}
			},
			'afterShow' : function() {
				if(mediaItemId!=undefined && mediaItemId!=null)
					Images.indexCurrent=mediaItemId;
				
				if($('#tab4').hasClass('Active')){
					$('.fancybox-inner #result').show();
					$('.fancybox-inner #lblResult').show();
				}else{
					$('.fancybox-inner #lblResult').hide();
					$('.fancybox-inner #result').hide();
				}
				//Images.pagePopup++;
				Images.isEndImages=0;
				Images.lstImages=new Map();
				if(Images.filterImageOfAlbum.page!=undefined && Images.filterImageOfAlbum.page!=null && Images.filterImageOfAlbum.page>0)
					Images.getImagesForPopup(null,Images.filterImageOfAlbum.page);
				else
					Images.getImagesForPopup(null, Images.pagePopup);
				Images.openMapOnImage();
				Images.scrollGetImageForPopup=1;//chặn các request giống nhau khi scroll liên tục
				$('.fancybox-inner #listImage').scroll(function(){
				   if (Images.scrollGetImageForPopup==1 && Images.isEndImages==0 && $(this).outerHeight() > ($(this).get(0).scrollHeight - $(this).scrollTop()-2)){
					   Images.scrollGetImageForPopup=0;
					   Images.getImagesForPopup();
				   }
				});
			},
			'afterClose' : function() {
				$('#' + nameId).html(html);
				Images.indexCurrent=null;
				Images.callBack=null;
				Images.customerId=null;
			},
			'beforeClose': function () {
				if(Images.checkDeleteImage != undefined && Images.checkDeleteImage != null && Images.checkDeleteImage == true) {
					Images.loadImageOfAlbumAfterDelete();
					Images.checkDeleteImage = null;
				}
			}
		});
		return false;
	},
	fillThumbnails:function(list){
		var listImage = '';
		for (var i = 0; i < list.keyArray.length; i++) {
			var temp = list.get(list.keyArray[i]);
			if(temp!=null){
				var currentMil = new Date().getTime();
				listImage += '<li id="li' +temp.id+'">';
				listImage +='<div class="PhotoThumbnails BoxMiddle"><span class="BoxFrame"><span class="BoxMiddle"><img id="img' +temp.id+'" class="imageSelect ImageFrame2" src="/resources/images/grey.jpg"'; 
				listImage +=' data-original="' +(temp.urlThum+'?v=' +currentMil)+'" width="142" height="102" value="' +temp.id+'" /></span></span></div>';
				listImage +='<p>' + Utils.XSSEncode(temp.dmyDate + ' – ' + temp.hhmmDate) +'</p>';
				listImage += "</li>";
			}
		}
		$('.fancybox-inner #listImageUL').append(listImage);
		Images.lazyLoadImages(".fancybox-inner .imageSelect",".fancybox-inner #listImage");
		$('.imageSelect').unbind('click');
		$('.imageSelect').click(Images.imageSelect);
		if(list.keyArray.length>0){
			if((Images.indexCurrent==undefined || Images.indexCurrent==null || Images.lstImages.get(Images.indexCurrent)==null)){
				Images.indexCurrent=list.keyArray[0];
			}
			setTimeout(function(){Images.showBigImagePopup(Images.indexCurrent);},500);
		}
	},
	focusStyleThumbPopup:function(id){
		$('.fancybox-inner .FocusStyle').removeClass('FocusStyle');
		$('.fancybox-inner #li' +id).addClass('FocusStyle');
	},
	imageSelect:function() {
		var id=$(this).attr('value');
		if($('#minimizePopup').length != 0) {//dialog nho
			var func = $('#minimizePopup img').attr('onclick');
			if(func.indexOf('Full') != -1) {
				func = 'Images.showDialogFancyFull(' +id+')';
			} else {
				func = 'Images.showDialogFancy(' +id+')';
			}
			$('#minimizePopup img').attr('onclick', func);
		} else {//dialog lon
			var func = $('#maximizePopup img').attr('onclick');
			if(func.indexOf('Full') != -1) {
				func = 'Images.showDialogFancyFull(' +id+')';
			} else {
				func = 'Images.showDialogFancy(' +id+')';
			}
			$('#maximizePopup img').attr('onclick', func);
		}
		Images.showBigImagePopup(id);
	},
	showImageNextPre:function(type){//1 là next, 0 là pre
		var flag=0;
		for(var i=0;i<Images.lstImages.keyArray.length;i++){
			if(Images.lstImages.keyArray[i]==Images.indexCurrent){
				if(type==1 && i+1<Images.lstImages.keyArray.length){//next
					scrollIntoViewPopup=1;
					Images.showBigImagePopup(Images.lstImages.keyArray[i+1]);
					flag=1;
				}else if(type==0 && i>0){
					scrollIntoViewPopup=1;
					Images.showBigImagePopup(Images.lstImages.keyArray[i-1]);
				}
				break;
			}
		}
		if(type==1 && flag==0 && Images.isEndImages==0){//nếu bấm next mà trong ds hiện tại ko có thì request lấy thêm hình
			Images.indexCurrent=null;
			scrollIntoViewPopup=1;
			Images.getImagesForPopup();
		}
	},
	isScrolledIntoView:function(elem){
	    var docViewTop = $(window).scrollTop();
	    var docViewBottom = docViewTop + $(window).height();

	    var elemTop = $(elem).offset().top;
	    var elemBottom = elemTop + $(elem).height();

	    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	},
	getTypeImage:function(url){
		var type='.jpg';
		try{
		var s=url.split('.');
		if(s.length>0) type='.' +s[s.length-1];
		}catch(e){}
		return type;
	},
	showBigImagePopup:function(id){
		try {
			var temp=Images.lstImages.get(id);
			if(temp!=null){
				var titleCodeLevelSeq = '';
				if($('#tab4').hasClass('Active')){
					if(temp.displayProgrameCode!=null) titleCodeLevelSeq+=' - ' + Utils.XSSEncode(temp.displayProgrameCode);
					if(temp.levelCode!=null) titleCodeLevelSeq+=' - ' + Utils.XSSEncode(temp.levelCode);
					//if(temp.monthSeq!=null) titleCodeLevelSeq+=' ( Lần ' +temp.monthSeq+' )';
				}
				$('#titlePopup').html(Utils.XSSEncode(temp.customerCode+' - ' +temp.customerName+' - ' +temp.customerAddress+titleCodeLevelSeq));
				if(scrollIntoViewPopup!=undefined && scrollIntoViewPopup!=null && scrollIntoViewPopup==1){
					if(!Images.isScrolledIntoView('.fancybox-inner #li' +temp.id)){
						$('.fancybox-inner #li' +temp.id)[0].scrollIntoView(false);
					}
					scrollIntoViewPopup=null;
				}
				Images.indexCurrent=temp.id;
				Images.focusStyleThumbPopup(temp.id);
				Images.imageLoadingShow();
				Images.openMapOnImage();
				$('.fancybox-inner .DownloadPhotoLink').hide();
				$('.fancybox-inner #shopImage').html(Utils.XSSEncode(temp.shopCode)+' - ' +Utils.XSSEncode(temp.shopName));
				$('.fancybox-inner #staffImage').html(Utils.XSSEncode(temp.staffCode)+' - ' +Utils.XSSEncode(temp.staffName));
				$('.fancybox-inner #timeImage').html(temp.dmyDate+' - ' +temp.hhmmDate);
				$('.fancybox-inner .DownloadPhotoLink').attr('href',temp.urlImage);
				//$('.fancybox-inner .DownloadPhotoLink').attr('download',temp.customerCode+Images.getTypeImage(temp.urlImage));
				$('.fancybox-inner .DownloadPhotoLink').attr('download',temp.urlEx.substring(temp.urlEx.lastIndexOf("/")+1));
				if($('#tab4').hasClass('Active')){
					$('.fancybox-inner #result').attr('value',temp.id);
					if(temp.result!=null && temp.result==1) $('.fancybox-inner #result').attr('checked','checked');
					else $('.fancybox-inner #result').removeAttr('checked');
				}
				Images.downloadFile(temp, false);//lay base64
				$('.DeletePhotoLink').attr('value',temp.id);
				var currentMil = new Date().getTime();
				var image = temp.urlImage+'?v=' +currentMil;
				if (image != null && image != '' && image != undefined) {
					if (document.images) {
						var img = new Image();
						img.src = image;
						img.onload = function() {
							$('.fancybox-inner #bigImages').attr('src',image);
							Images.imageBigShow();
						};
					}
				}
			}
		} catch (err) {
			return;
		}
	},
	updateResult:function(){
		var id=$('.fancybox-inner #result').attr('value');
		var result = 0;
		if(id!=''){
			if($('.fancybox-inner #result').is(':checked')){
				result = 1;
			}
			var data=new Object();
			data.id=id;
			data.resultImg=result;
			Utils.addOrSaveData(data, '/images/updateResult', null, 'errMsg123', function(data) {
				if(data.error){
					if(result==1) $('.fancybox-inner #result').removeAttr('checked');
					else $('.fancybox-inner #result').attr('checked','checked');
				}else{
					var temp=Images.lstImages.get(id);
					temp.result=result;
				}
				
			}, 'loading2',null,true);
		}
	},
	downloadFile:function(temp, ow){
			var url = temp.urlEx;
			var customerInfo = Images.displayProgrameCode + " (" + Utils.XSSEncode(temp.customerCode+' - ' +temp.customerName+' - ' +temp.customerAddress) + ")";
			var shopInfo = Utils.XSSEncode(temp.shopCode + ' - ' + temp.shopName);
			var staffInfo = Utils.XSSEncode(temp.staffCode + ' - ' + temp.staffName);
			var dateInfo = temp.dmyDate + ' - ' +temp.hhmmDate;
			var data=new Object();
			data.id = temp.id;
			data.urlImage=url;
			data.customerInfo=customerInfo;
			data.shopInfo=shopInfo;
			data.staffInfo=staffInfo;
			data.dateInfo=dateInfo;
			data.overwrite=true;
			if($('#tab4').hasClass('Active') && Images.displayProgrameId!=undefined && Images.displayProgrameId!=null){
				data.displayProgrameId=Images.displayProgrameId;
			}
			$('.loadingPopup').show();			
			$('.fancybox-inner .DownloadPhotoLink').hide();
			var kData = $.param(data, true);
			$.ajax({
				type : 'GET',
				url : '/images/download/image',
				data :(kData),
				dataType : "json",
				success : function(data) {
					$('.loadingPopup').hide();
					$('.fancybox-inner .DownloadPhotoLink').show();
					if(!data.error && data.errMsg!= null && data.errMsg.length > 0){
						$('.fancybox-inner .DownloadPhotoLink').attr('href',data.errMsg);
					}else{
						$('.fancybox-inner .DownloadPhotoLink').attr('href',imgServerSOPath+url);
					}
				},
			error:function(XMLHttpRequest, textStatus, errorThrown) {$('.loadingPopup').hide();}});
	},
	openMapOnImage : function() {
		VTMapUtil.loadMapResource(function(){
			var temp = Images.lstImages.get(Images.indexCurrent);
			if(temp!=null){
				if($('.OnFucnStyle').is(':visible')){					
					$('.OffFucnStyle').hide();
					$('.OffFucnStyle').css('display','none');
					$('.OnFucnStyle').show();
					$('.OnFucnStyle').css('display','block');
					$('.fancybox-inner #divMapSection').removeClass('SmallMapSection');
					$('.fancybox-inner #divMapSection').addClass('LargeMapSection');
					$('.fancybox-inner #bigMap').css('width','488px');
					$('.fancybox-inner #bigMap').css('height','409px');
					ViettelMap.loadMapForImages('bigMap',temp.lat,temp.lng,13,1, temp.custLat, temp.custLng);
				}
				else {
					$('.OnFucnStyle').hide();
					$('.OnFucnStyle').css('display','none');
					$('.OffFucnStyle').show();
					$('.OffFucnStyle').css('display','block');
					$('.fancybox-inner #divMapSection').removeClass('LargeMapSection');
					$('.fancybox-inner #divMapSection').addClass('SmallMapSection');
					$('.fancybox-inner #bigMap').css('width','140px');
					$('.fancybox-inner #bigMap').css('height','120px');
					ViettelMap.loadMapForImages('bigMap',temp.lat,temp.lng,13,0, temp.custLat, temp.custLng);
				}
			}
		});
	},
	deleteImage:function(t){
		var id=$(t).attr('value');
		var temp = Images.lstImages.get(id);
		if(temp!=null){
			var data = new Object();
			data.id=temp.id;
			$.messager.confirm(image_manager_confirm, image_manager_search_error_14, function(r){
				if (r){
					Utils.addOrSaveData(data, '/images/delete-image', null, 'errMsgPopup', function(result) {
						if(!result.error){
							Images.checkDeleteImage = true;
							Images.calPageNumberForPopup();//tinh lai trang dang select khi delete thành công
							for(var i=0;i<Images.lstImages.keyArray.length;i++){
								if(Images.lstImages.keyArray[i]==Images.indexCurrent){
									if(i+1<Images.lstImages.keyArray.length){//next toi 1 hình
										Images.showImageNextPre(1);
									}else if(i > 0){//nếu là hình cuối trong list thì pre lại
										Images.showImageNextPre(0);
									}else{//nếu hết hình thì lấy trang khác
										Images.indexCurrent=null;
										Images.getImagesForPopup(function(){
											$.fancybox.close();
										});
									}
									break;
								}
							}
							$('#li' +temp.id).remove();
							Images.lstImages.remove(temp.id);
						}else{
							$('.fancybox-inner #errMsgPopup').html(result.errMsg).show();
							setTimeout(function(){$('.fancybox-inner #errMsgPopup').hide();},5000);
						}
					}, 'loading2',null,true,null,function(result){
						if(result.error){
							setTimeout(function(){$('.fancybox-inner #errMsgPopup').hide();},5000);
						}
					});
				}
			});
		}
	},
	rotateImage:function(t){
		var id=Images.indexCurrent;
		var temp = Images.lstImages.get(id);		
		if(temp!=null){			
			var data = new Object();
			data.id=temp.id;				
			kData = $.param(data, true);
			$('#divOverlay').show();
			$.ajax({
				type : "POST",
				url : '/images/rotate-image', 
				data :(kData),
				dataType : "json",
				success : function(result) {
					$('#divOverlay').hide();
					if(!result.error){
						var currentMil = new Date().getTime();
						var url = result.url + '?v=' + currentMil;
						var urlThumbnail = result.thumbnailUrl + '?v=' + currentMil;
						$('#img' +Images.indexCurrent).attr('data-original', urlThumbnail);
						$('#img' +Images.indexCurrent).attr('src', urlThumbnail);
						$('#imagesOfAlbum_' +Images.indexCurrent).attr('data-original', urlThumbnail);
						$('#imagesOfAlbum_' +Images.indexCurrent).attr('src', urlThumbnail);
						$('#imagesOfAlbulmDetail_' +Images.indexCurrent).attr('data-original', urlThumbnail);
						$('#imagesOfAlbulmDetail_' +Images.indexCurrent).attr('src', urlThumbnail);
						var image = url;
						if (image != null && image != '' && image != undefined) {
							if (document.images) {
								var img = new Image();
								img.src = image;
								img.onload = function() {
									$('.fancybox-inner #bigImages').attr('src',image);
									Images.imageBigShow();
								};
							}
						}						
						Images.downloadFile(temp, true);
					}else{
						$('.fancybox-inner #errMsgPopup').html(image_manager_search_error_15).show();
						setTimeout(function(){$('.fancybox-inner #errMsgPopup').hide();},5000);
					}
				}
			});
			
		}
	},
	calPageNumberForPopup:function(){
		if(Images.lstImages!=undefined && Images.lstImages!=null){
			Images.pagePopup=parseInt((Images.lstImages.keyArray.length-1)/Images.maxPopup);
		}else Images.pagePopup=0;
	},
	loadComboStaff:function(url){
		$("#staff").kendoMultiSelect({
			dataSource: {
				transport: {
	                read: {
	                    dataType: "json",
	                    url: url
	                }
	            }
			},
			filter: "contains",
	        dataTextField: "staffCode",
	        //dataValueField: "id",
	        dataValueField: "staffId",
			itemTemplate: function(data, e, s, h, q) {
				return Utils.XSSEncode(data.staffCode)+' - ' +Utils.XSSEncode(data.staffName);
			},
	        tagTemplate:  '#: data.staffCode #',
	    });

	    var staffKendo = $("#staff").data("kendoMultiSelect");
	    //staffKendo.wrapper.attr("id", "staff-wrapper");
	    staffKendo.wrapper.attr("staffId", "staff-wrapper");
	},
	loadComboCTTB:function(url){
		$("#cttb").kendoMultiSelect({
			dataSource: {
				transport: {
	                read: {
	                    dataType: "json",
	                    url: url
	                }
	            }
			},
			filter: "contains",
	        dataTextField: "displayProgrameCode",
	        dataValueField: "displayProgrameId",
			itemTemplate: function(data, e, s, h, q) {
				return '<div class="tree-vnm" node-id="' +Utils.XSSEncode(data.displayProgrameId)+'" style="width:180px"><span class="tree-vnm-text">' +Utils.XSSEncode(data.displayProgrameCode)+' - ' +Utils.XSSEncode(data.displayProgrameName) +'</span></div>';	
			},
	        tagTemplate:  '#: data.displayProgrameCode #',
	        value: [$('#displayProgrameId').val()]
	    });	 
	},
	openOrderImage:function(){
		$('#orderImagePopup').dialog({  
	        title: 'Tải hình ảnh',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height :'auto',
	        onOpen: function(){
	        	
	        }
	    });
	},
	exportAlbumEx:function(){
//		$('#errMsg').html('').hide();
//		var params = Images.getParamRequest();
		var params = new Object();
		if($('#fromDateOrder').val()!='') params.fromDate= $('#fromDateOrder').val();
		if($('#toDateOrder').val()!='') params.toDate= $('#toDateOrder').val();
		if($('#cttbOrder').val()!='') params.cttbOrder= $('#cttbOrder').val();
		var array = new Array();
		


		array.push('008_TT20201');
		array.push('04M_TT20201');
		array.push('0C6_TT20201');
		array.push('0DK_TT20201');
		array.push('036_TT20201');
		array.push('0A3_TT20201');
		array.push('00R_TT20201');
		array.push('0GU_TT20201');
		array.push('01D_TT20201');
		array.push('11G_TT20201');
		array.push('0NP_TT20201');
		array.push('038_TT20201');
		array.push('01J_TT20201');
		array.push('02R_TT20201');
		array.push('02P_DL20041');
		array.push('00T_DL20041');
		array.push('02O_DL20041');
		array.push('011_DL20041');
		array.push('13I_DL20041');
		array.push('147_HH20021');
		array.push('03L_HH20021');
		array.push('135_HH20021');
		array.push('001_HH20021');
		array.push('003_HH20021');
		array.push('0DB_HH20021');
		array.push('0K1_HH20021');
		array.push('110_HH20021');
		array.push('03R_NV20051');
		array.push('0I1_NV20051');
		array.push('030_NV20051');
		array.push('01I_NL20041');
		array.push('002_NL20041');
		array.push('01O_NL20041');
		array.push('00F_NL20041');
		array.push('07G_NL20041');
		array.push('057_NL20041');
		array.push('05T_NL20041');
		array.push('014_MH30041');
		array.push('07Y_MH30041');
		array.push('0B7_MH30041');
		array.push('03H_MH30041');
		array.push('059_MH30041');
		array.push('029_MH30041');
		array.push('024_MH30041');
		array.push('082_MH30041');
		array.push('061_MH30041');
		array.push('0XN_MH30041');
		array.push('084_MH30041');
		array.push('0MB_MH30041');
		array.push('00X_MH30041');
		array.push('00S_PT20091');
		array.push('0LG_PT20091');
		array.push('046_PT20091');
		array.push('048_PT20091');
		array.push('052_PT20091');
		array.push('0O1_PT20091');
		array.push('06Y_FM20011');
		array.push('06V_FM20011');
		array.push('0HS_FM20011');
		array.push('24N_FM20011');
		array.push('26A_FM20011');
		array.push('066_FM20011');
		array.push('054_FM20011');
		array.push('04S_FM20011');
		array.push('01H_FM20011');
		array.push('079_FM20011');
		array.push('01P_FM20011');
		array.push('03F_FM20011');
		array.push('03C_FM20011');
		array.push('876_FM20011');
		array.push('0IB_FM20011');
		array.push('00C_TH20031');
		array.push('0LW_TH20031');
		array.push('06P_TH20031');
		array.push('00I_TH20031');
		array.push('0OJ_TH20031');
		array.push('0NP_TH20031');
		array.push('0N6_TH20031');
		array.push('0NJ_PT20051');
		array.push('01J_PT20051');
		array.push('01G_PT20051');
		array.push('02H_PT20051');
		array.push('02W_PT20051');
		array.push('05U_PT20051');
		array.push('05V_PT20051');
		array.push('0BZ_PT20051');
		array.push('0TY_PT20051');
		array.push('036_PT20051');
		array.push('06O_PT20051');
		array.push('00Z_PT20051');
		array.push('00Y_PT20051');
		array.push('0G1_PT20051');
		array.push('0W5_PT20051');
		array.push('099_PT20051');
		array.push('02Y_PT20051');
		array.push('018_PT20051');
		array.push('017_DT30121');
		array.push('1D8_DT30121');
		array.push('09L_DT30121');
		array.push('09I_DT30121');
		array.push('10W_DT30121');
		array.push('05D_DT30121');
		array.push('10P_DT30121');
		array.push('100_DT30121');
		array.push('101_DT30121');
		array.push('0CG_DT30121');
		array.push('08U_DT30121');
		array.push('09X_DT30121');
		array.push('15I_DT30121');
		array.push('0R0_DT30121');
		array.push('15P_DT30121');
		array.push('19V_DT30121');
		array.push('0AU_DT30121');
		array.push('04Q_TK20011');
		array.push('0LJ_TK20011');
		array.push('0EI_TK20011');
		array.push('0QH_TK20011');
		array.push('0SC_TK20011');
		array.push('0RN_TK20011');
		array.push('02I_TK20011');
		array.push('0S6_TK20011');
		array.push('00K_TK20011');
		array.push('0H5_DH20051');
		array.push('0HJ_DH20051');
		array.push('0HX_DH20051');
		array.push('0PA_DH20051');
		array.push('13O_DH20051');
		array.push('0I3_DH20051');
		array.push('40F_DH20051');
		array.push('0NX_DH20051');
		array.push('174_DH20051');
		array.push('1AW_DH20051');
		array.push('20F_DH20051');
		array.push('15Z_DH20051');
		array.push('0GW_DH20051');
		array.push('00U_DH20051');
		array.push('017_DH20051');
		array.push('0GJ_DH20051');
		array.push('0JY_DH20051');
		array.push('18N_DH20051');
		array.push('18P_DH20051');
		array.push('18X_DH20051');
		array.push('0HT_DH20051');
		array.push('01N_DH20051');
		array.push('0IV_DH20051');
		array.push('03N_DH20051');
		array.push('0LK_DH20051');
		array.push('11X_TV20021');
		array.push('0K8_TV20021');
		array.push('11U_TV20021');
		array.push('00E_TV20021');
		array.push('02N_TV20021');
		array.push('0CR_TV20021');
		array.push('0N7_TV20021');
		array.push('0N8_TV20021');
		array.push('0DH_TB30021');
		array.push('01Z_HT30021');
		array.push('028_HT30021');
		array.push('01U_HT30021');
		array.push('06C_HT30021');
		array.push('1CS_MK20021');
		array.push('1HT_MK20021');
		array.push('1CW_MK20021');
		array.push('28D_MK20021');
		array.push('28V_MK20021');
		array.push('1KV_MK20021');
		array.push('0FV_HT20171');
		array.push('02G_HT20171');
		array.push('02H_HT20171');
		array.push('025_HT20171');
		array.push('04X_HT20171');
		array.push('1PQ_HT20171');
		array.push('1PZ_HT20171');
		array.push('01P_HT20171');
		array.push('19B_HT20171');
		array.push('00C_HT20171');
		array.push('03D_HT20171');
		array.push('02K_HT20171');
		array.push('01F_HT20171');
		array.push('06E_HT20171');
		array.push('1T9_HT20171');
		array.push('0QB_HT20171');
		array.push('01I_HT20171');
		array.push('02M_HT20171');
		array.push('2HS_VT20031');
		array.push('28S_NT30221');
		array.push('29R_NT30221');
		array.push('119_NT30031');
		array.push('021_HC30021');
		array.push('0JT_HC30021');
		array.push('06O_HC30021');
		array.push('17K_HC30021');
		array.push('0HK_HC30021');
		array.push('0F4_MK20022');
		array.push('08N_MK20022');
		array.push('0EZ_MK20022');
		array.push('0G1_MK20022');
		array.push('1DT_VT30021');
		array.push('05D_VT30021');
		array.push('1CX_VT30021');
		array.push('1FA_VT30021');
		array.push('007_VT30021');
		array.push('0K2_VT30021');
		array.push('21M_VT30021');
		array.push('0C9_VT30021');
		array.push('1Z8_VT30021');
		array.push('2EE_VT30021');
		array.push('2FG_VT30021');
		array.push('03U_VT30021');
		array.push('05C_VT30021');
		array.push('2CT_VT30021');
		array.push('2CU_VT30021');
		array.push('2CR_VT30021');
		array.push('18K_VT30021');
		array.push('23I_VT30021');
		array.push('1VL_VT30021');
		array.push('2CK_VT30021');
		array.push('23H_VT30021');
		array.push('0EY_VT30021');
		array.push('2HP_VT30021');
		array.push('2HJ_VT30021');
		array.push('1X1_VT30021');
		array.push('2I5_VT30021');
		array.push('289_VT30021');
		array.push('2HB_VT30021');
		array.push('2F0_VT30021');
		array.push('2I2_VT30021');
		array.push('090_VT30021');
		array.push('198_VT30021');
		array.push('1HX_VT30021');
		array.push('00A_VT30021');
		array.push('0K8_VT30021');
		array.push('00H_VT30021');
		array.push('01M_VT30021');
		array.push('2J4_VT30021');
		array.push('2HZ_VT30021');
		array.push('1FP_VT30021');
		array.push('03K_VT30021');
		array.push('062_VT30021');
		array.push('02D_VT30021');
		array.push('04Q_VT30021');
		array.push('0ZE_VT30021');
		array.push('030_VT30021');
		array.push('02O_VT30021');
		array.push('02N_VT30021');
		array.push('031_VT30021');
		array.push('032_VT30021');
		array.push('048_VT30021');
		array.push('1S9_VT30021');
		array.push('2AZ_VT30021');
		array.push('0ZQ_HP20141');
		array.push('0TR_HP20141');
		array.push('0WY_HP20141');
		array.push('13X_HP20141');
		array.push('28V_HP20141');
		array.push('174_HP20141');
		array.push('0RM_HP20141');
		array.push('14Z_HP20141');
		array.push('13I_HP20141');
		array.push('29E_HP20141');
		array.push('015_HP20141');
		array.push('0PB_HP20141');
		array.push('2B7_HP20141');
		array.push('1VR_HP20141');
		array.push('0BE_HP20141');
		array.push('040_HP20141');
		array.push('0AS_AL20011');
		array.push('00O_AL20011');
		array.push('13H_AL20011');
		array.push('13V_AL20011');
		array.push('077_AL20011');
		array.push('01W_AL20011');
		array.push('092_AL20011');
		array.push('11W_AL20011');
		array.push('071_AL20011');
		array.push('027_AL20011');
		array.push('0EZ_AL20011');
		array.push('021_AL20011');
		array.push('01F_AL20011');
		array.push('03L_AL20011');
		array.push('02Y_AL20011');
		array.push('02L_NV20021');
		array.push('15W_NV20021');
		array.push('02K_NV20021');
		array.push('0GV_NV20021');
		array.push('02A_NV20021');
		array.push('10I_NV20021');
		array.push('16I_NV20021');
		array.push('0C6_NV20021');
		array.push('04B_NV20021');
		array.push('00S_NV20021');
		array.push('00F_NV20021');
		array.push('04A_NV20021');
		array.push('147_NV20021');
		array.push('0F7_NV20021');
		array.push('0DQ_NV20021');
		array.push('0EJ_NV20021');
		array.push('117_NV20021');
		array.push('090_NV20021');
		array.push('17P_NV20021');
		array.push('01O_NV20021');
		array.push('0GF_NV20021');
		array.push('0JM_NV20021');
		array.push('05R_NV20021');
		array.push('018_NV20021');
		array.push('14L_NV20021');
		array.push('0LZ_CT30021');
		array.push('12K_CT30021');
		array.push('0RN_CT30021');
		array.push('061_CT30021');
		array.push('14E_CT30021');
		array.push('0G0_CT30021');
		array.push('14B_CT30021');
		array.push('1E8_CT30021');
		array.push('042_CT30021');
		array.push('05Z_CT30021');
		array.push('08J_CT30021');
		array.push('1BN_CT30021');
		array.push('17K_CT30021');
		array.push('17J_CT30021');
		array.push('0NI_CT30021');
		array.push('361_CT30021');
		array.push('0K9_CT30021');
		array.push('120_CT30021');
		array.push('037_CT30021');
		array.push('20M_CT30021');
		array.push('0KJ_CT30021');
		array.push('0EP_CT30021');
		array.push('0O4_CT30021');
		array.push('101_CT30021');
		array.push('05K_CT30021');
		array.push('01J_CT30021');
		array.push('477_CT30021');
		array.push('1C0_CT30021');
		array.push('03I_CT30021');
		array.push('0ZX_CT30021');
		array.push('36P_CT30021');
		array.push('01I_CT30021');
		array.push('00X_CT30021');
		array.push('09M_CT30021');
		array.push('0YG_CT30021');
		array.push('333_CT30021');
		array.push('09U_CT30021');
		array.push('39Z_CT30021');
		array.push('16H_CT30021');
		array.push('0ZY_CT30021');
		array.push('0OW_CT30021');
		array.push('019_CT30021');
		array.push('1BS_CT30021');
		array.push('1CI_CT30021');
		array.push('22D_LH30071');
		array.push('208_LH30071');
		array.push('04F_LH30071');
		array.push('17U_LH30071');
		array.push('24Q_LH30071');
		array.push('04I_LH30071');
		array.push('006_LL20021');
		array.push('007_LL20021');
		array.push('005_LL20021');
		array.push('0F7_LL20021');
		array.push('01G_HG20031');
		array.push('03L_HG20031');
		array.push('0KB_HG20031');
		array.push('0QY_HG20031');
		array.push('1C6_HG20031');
		array.push('30M_HG20031');
		array.push('330_HG20031');
		array.push('35S_HG20031');
		array.push('372_HG20031');
		array.push('37V_HG20031');
		array.push('37X_HG20031');
		array.push('201_TN30101');
		array.push('0AB_TN30101');
		array.push('069_TN30101');
		array.push('1CC_TT30121');
		array.push('0XW_TT30121');
		array.push('0LZ_TT30121');
		array.push('11L_TT30121');

		alert(array.length);
		params.customerOrder = array;
		params.token = Utils.getToken();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(params, true);
		$.ajax({
			type : 'POST',
			url : '/images/exportAlbumEx',
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#errMsg').html(image_manager_search_error_16 + escapeSpecialChar(data.errMsg)).show();
				} 
			}
		});
		return false;
	}
};
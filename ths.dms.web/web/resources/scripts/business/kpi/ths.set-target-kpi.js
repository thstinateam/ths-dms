var TargetKpi = {
	_MAX_CYCLE_NUM: 13,
	_lstStaffInfo: [],
	getSearchParams: function() {
		var params = {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		params.yearPeriod = $('#yearPeriod').val().trim();
		params.numPeriod = $('#numPeriod').combobox('getValue');
		return params;
	},
	search: function() {
		hideAllMessage();
		TargetKpi._lstStaffInfo = [];
		var par = TargetKpi.getSearchParams();
		Utils.getHtmlDataByAjax(par, '/kpi/set-target/search', function(data){
			$('#gridContainer').html(data);
		});
	},
	bindFormatInput: function(){
		$('#gridContainer input.kpi').each(function(index, item){
			var id = $(this).attr("id");
			Utils.bindFormatOntextfieldCurrencyFor(id, Utils._TF_NUMBER_DOT);
			$(this).bind('blur', function(){
				var kpiId = $(this).attr('kpi');
				if (!Utils.isEmpty(kpiId)) {
					var numRow = $('#gridContainer input.kpi[kpi="' + kpiId + '"]').length;
					var total = 0;
					$('#gridContainer input.kpi[kpi="' + kpiId + '"]').each(function(index, item){
						var val = $(this).val().trim().replace(/,/g, "");
						if (!Utils.isEmpty(val) && !isNaN(val)) {
							total += Number(val);
						}
					});
					if (total == 0) {
						$('#' + kpiId + '-avg').html(total);
					} else {
						$('#' + kpiId + '-avg').html(formatFloatValue(total/numRow, 2));
					}
				}
				
			});
		});
	},
	setKpi: function() {
//		$('#errExcelMsg').html('').hide();
		hideAllMessage();
		var date = new Date();
		var curYear = $('#curYearPeriod').val().trim();
		var curPeriod = $('#curNumPeriod').val().trim();
		var year = $('#yearPeriodUnder').val().trim();
		var period = $('#numPeriodUnder').combobox('getValue').trim();
		var errMsg = '';
		if (parseInt(period) < 0 || parseInt(period) > TargetKpi._MAX_CYCLE_NUM){
			errMsg = 'Chu kỳ không hợp lệ. Vui lòng chọn lại';
		}
		if (errMsg == '' && !Utils.isEmpty(curYear) && !isNaN(curYear) && parseInt(year) < parseInt(curYear)){
			errMsg = 'Năm phải lớn hơn hay bằng năm hiện tại. Vui lòng chọn lại';
		}
		if (errMsg == '' && !Utils.isEmpty(curYear) && !isNaN(curYear) && parseInt(year) == parseInt(curYear) && parseInt(period) < parseInt(curPeriod)){
			errMsg = 'Chu kỳ phải lớn hơn hay bằng chu kỳ hiện tại. Vui lòng chọn lại';
		}
		if (TargetKpi._lstStaffInfo == null || TargetKpi._lstStaffInfo.length == 0){
			errMsg = 'Danh sách nhân viên rỗng.'
		}
		if (errMsg.length > 0){
			$('#errExcelMsg').html(errMsg).show();
			return;
		}
		var lstData = [];
		var objData = {};
		for (var i = 0, n = TargetKpi._lstStaffInfo.length; i < n; i++){
			var st = TargetKpi._lstStaffInfo[i];
			var obj = {};
			var lstKpiData = [];
			objData = {};
			objData.staffId = st.staffId;
			objData.year = year;
			objData.period = period;
			$('#gridContainer input[st=' + st.staffId + ']').each(function(index, item){
				var id = $(this).attr("id");
				obj = {};
				obj.key = $(this).attr("kpi");
				obj.value = $(this).val().replace(/,/g, "");
				lstKpiData.push(obj);
			});
			objData.lstKpiData = lstKpiData;
			lstData.push(objData);
		}
		var dataModel = {};
		convertToSimpleObject(dataModel, lstData, 'lstKpiSaveInfo');
		Utils.addOrSaveData(dataModel, '/kpi/set-target/save-info', null, 'errExcelMsg', function(){
			
		});
		
	},
	downloadTemplate: function() {
		hideAllMessage();
 		var param = {};
 		try {
 			param.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
 		Utils.getJSONDataByAjax(param, '/kpi/set-target/download-template', function(data) {
 			if (data.error) {
 				$('#errExcelMsg').html(data.errMsg).show();
 			} else if (data.view != null && data.view.length > 0) {
 				var filePath = ReportUtils.buildReportFilePath(data.view);
 				window.location.href = filePath;
 			}
 			
 		}, null, null);
 	},
 	importExcel: function() {
 		var params = {};
 		$('.SuccessMsgStyle').html('').hide();
 		$('.ErrorMsgStyle').html('').hide();
 		
		Utils.importExcelUtils(function(data) {
			TargetKpi.search();
			$('#errExcelMsg').val("").change();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				$('#successMsg').html(jsp_common_save_success).show();
				var tm = setTimeout(function(){
					//Load lai danh sach quyen
					$('#successMsg').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
 	},
 	exportExcel: function() {
 		var params = TargetKpi.getSearchParams();
 		ReportUtils.exportReport('/kpi/set-target/exportExcel',params,'errMsg');
 	},
 	initCycleInfo: function() {
 		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 113);
 				Utils.bindPeriodCbx('#numPeriodUnder', jQuery.extend(true, [], data.lstCycle), data.currentNum, 113);
 			}
 		});
 	},

	/**
	 * khi change yearPeriodUnder Lay chu ky cua combobox Under(numPeriodUnder) 
	 * @author vuongmq
	 * @since 13/10/2015 
	 */
 	initCycleInfoUnder: function() {
 		var par = {};
 		par.year = $('#yearPeriodUnder').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriodUnder', data.lstCycle, data.currentNum, 113);
 			}
 		});
 	},
 	setCurrentYear: function(year) {
 		$('#yearPeriod').val(year);
 		$('#yearPeriod').change();
 		$('#yearPeriodUnder').val(year);
 		$('#yearPeriodUnder').change();
 	}
 	
}
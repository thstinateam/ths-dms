var UpdateInformationOutputWarehouse = {
	_isIndexPrint: 0,
	_mapDataGridPrdStockIssueFull: null,
	_listDataGridPrdStockIssueDg: null,
	_mapDataByIsIndexPrint: null,
	_stockIssueIdTemp: 1,
	_transType: null,
	//TODO Khoi tao Handson table
	/**
	 * Khoi tao column Grid PrdStockIssueDg
	 * 
	 * @author hunglm16
	 * @since November 10,2014
	 * */
	ColGrid : {
		isCheck:0,
		stockTransCode:1,
		stockIssueNumber:2,
		issueType:3,
		about:4,
		staffText:5,
		contractNumber:6,
		carNumber:7,
		warehouseOutPutName:8,
		warehouseInputName:9,
		stockTransDate:10,
		totalAmount:11
	},
	/**
	 * Dieu chinh lai Style cho Row và column
	 * 
	 * @author hunglm16
	 * @since November 25,2014
	 * */
	styleCellRenderer: function (instance, td, row, col, prop, value, cellProperties) {
		if(col == UpdateInformationOutputWarehouse.ColGrid.isCheck){
			Handsontable.renderers.CheckboxRenderer.apply(this, arguments);
			$(td).css({
				  "vertical-align": "middle",
				  "text-align": "center"
		    });
			$(td).attr("idx", row);
		}
	},
	/**
	 * Khoi tao doi tuong prdStockIssueDg theo Handsontable
	 * 
	 * @author hunglm16
	 * @since November 25,2014
	 * */
	initGrid: function(){
		$('#prdStockIssueDg').handsontable({
			data: [],
			colWidths: [30, 110, 110, 125, 110, 110, 125, 110, 100, 110, 110, 110],
			outsideClickDeselects: true,
			multiSelect : false,
			rowHeaders: true,
			manualColumnResize: true,
			width: $(window).width() - 45,
			height: 485,
			//minRows: 10,
			minCols: 12,
			autoWrapRow: true,
			rowHeaders: true,
			colHeaders: true,
			colHeaders: function (col) {
	           	switch (col) {
                case 0:
                    var txt = "<input type='checkbox' id='cbxCheckAll' onclick='UpdateInformationOutputWarehouse.cbxCheckAllChange(this);' ";
                    txt += UpdateInformationOutputWarehouse.isCheckedAll() ? 'checked="checked"' : '';
                    txt += ' style="margin-top: 10px; margin-top: 4px;" />';
                    return txt;
                case 1:
                    return "Số chứng từ";
                case 2:
                    return "Số phiếu";
                case 3:
                    return "Loại";
                case 4:
                    return "Về việc";
                case 5:
                    return "Người vận chuyển";
                case 6:
                    return "Hợp đồng số";
                case 7:
                    return "Phương tiện VT";
                case 8:
                    return "Xuất từ kho";
                case 9:
                    return "Nhập từ kho";
                case 10:
                    return "Ngày";
                case 11:
                	return "Thành Tiền";
	           	}
			},
			columns: [
			          {readOnly: true, data:'isCheckBox', renderer: "html"},
			          {readOnly: true, data: 'stockTransCode'},
			          {readOnly: false, data: 'stockIssueNumber'},
			          {readOnly: true, data: 'issueType'},
			          {readOnly: false, data: 'about'},
			          {readOnly: false, data: 'staffText'},
			          {readOnly: false, data: 'contractNumber'},
			          {readOnly: false, data: 'carNumber'},
			          {readOnly: false, data: 'warehouseOutPutName'},
			          {readOnly: false, data: 'warehouseInputName'},
			          {readOnly: true, data: 'stockTransDate'},
			          {readOnly: true, type: 'numeric', format: '0,0', data: 'totalAmount'},
			 ],
			 cells: function (row, col, prop) {
				 UpdateInformationOutputWarehouse.styleCellRenderer();
		     },
			 afterChange: function(changes, source) {
			 },
			 afterRender:function(){
			 },
			 beforeKeyDown: function(event){
			 }
		});
	},
	/**
	 * Xu ly su kien checkAll
	 * 
	 * @author hunglm16
	 * @since November 25,2014
	 * */
	isCheckedAll: function(){
		var data = $('#prdStockIssueDg').handsontable('getData');
		if(data != null && data.length >0){
	        for (var i = 0, ilen = data.length; i < ilen; i++) {
	            if (!data[i].isCheck) {
	                return false;
	            }
	        }
	        return true;
		}
		return false;
	},
	
	/**
	 * Ham nay dung de xu ly checkAll
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * */
	changeWhenCheckNew: function(cbx){
		var isCheck = $(cbx).prop("checked");
		var idCbx = Number($(cbx).attr("id").trim().replace(/cbxPrdStockIssueDg_/g, ''));
		if(isCheck){
			var valArray = $('#prdStockIssueDg').handsontable('getData');
			var flag = true;
			for (var i = 0; i < valArray.length; i++) {
				if (valArray[i].stockIssueId == idCbx) {
					valArray[i].isCheck = true;
					valArray[i].isCheckBox = '<input type = "checkbox" checked="checked" class="htCheckboxRendererInput" id="cbxPrdStockIssueDg_' +idCbx+'" style="margin-left: 4px; margin-top: 4px;" onchange="UpdateInformationOutputWarehouse.changeWhenCheckNew(this);" />';
				}
				if (!valArray[i].isCheck) {
					flag = false;
				};
			}
			if (flag) {
				$('#cbxCheckAll').prop('checked', true);				
			}
		} else {
			$('#cbxCheckAll').prop('checked', false);
			var valArray = $('#prdStockIssueDg').handsontable('getData');
			var flag = true;
			for (var i = 0; i < valArray.length; i++) {
				if (valArray[i].stockIssueId == idCbx) {
					valArray[i].isCheck = false;
					valArray[i].isCheckBox = '<input type = "checkbox" class="htCheckboxRendererInput" id="cbxPrdStockIssueDg_' +idCbx+'" style="margin-left: 4px; margin-top: 4px;" onchange="UpdateInformationOutputWarehouse.changeWhenCheckNew(this);" />';
					break;
				}
			}
			//$("tr td[idx="+i+"]").parent().find("td").css({background: '#E78686', color: 'black', fontWeight: 'bold'});
		}
	},
	/**
	 * Kiem tra co su kien check trong danh sach class htCheckboxRendererInput hay khong
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * @description dung thuat toan binary quicksearch
	 * */
	htCheckboxRendererInputChecked: function() {
		var htGrid = $('#prdStockIssueDg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
		   for(var i=0, sz = rows.length; i < sz; i++){
		     if(rows[i].isCheck) {
		          return true;
		     }
		   }
		}
		return false;
	},
	/**
	 * Check All cbxCheckAll Change
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * */
	cbxCheckAllChange: function (cbx) {
		if ($(cbx).prop("checked")) {
			$('input.htCheckboxRendererInput').prop("checked", true);
			var valArray = $('#prdStockIssueDg').handsontable('getData');
			for (var i = 0; i < valArray.length; i++) {
				valArray[i].isCheck = true;
				valArray[i].isCheckBox = '<input type="checkbox" checked="checked" class="htCheckboxRendererInput" id="cbxPrdStockIssueDg_' +valArray[i].stockIssueId+'" style="margin-left: 4px; margin-top: 4px;" onchange="UpdateInformationOutputWarehouse.changeWhenCheckNew(this);" />';
			}
			
		} else {
			$('input.htCheckboxRendererInput').prop("checked", false);
			var valArray = UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.keyArray;
			var valArray = $('#prdStockIssueDg').handsontable('getData');
			for (var i = 0; i < valArray.length; i++) {
				valArray[i].isCheck = false;
				valArray[i].isCheckBox = '<input type="checkbox" class="htCheckboxRendererInput" id="cbxPrdStockIssueDg_' +valArray[i].stockIssueId+'" style="margin-left: 4px; margin-top: 4px;" onchange="UpdateInformationOutputWarehouse.changeWhenCheckNew(this);" />';
			}
		}
	},
	
	/**
	 * Load danh sach Fill PrdStockIssueDg
	 * 
	 * @author hunglm16
	 * @since November 25,2014
	 * */
	loadPagePrdStockIssueDg: function() {
		UpdateInformationOutputWarehouse.initGrid();
		UpdateInformationOutputWarehouse.getDataInGridPrdStock(null);
		$('#prdStockIssueDg').on('	', 'input.checker', function (event) {
			var htGrid = $('#prdStockIssueDg').handsontable('getInstance');
			var data = htGrid.getData();
			if(data !=null){
		        var current = !$('input.checker').is(':checked'); //returns boolean
	            for (var i = 0, ilen = data.length; i < ilen; i++) {
	            	data[i].isCheck = current;
	            	UpdateInformationOutputWarehouse.changeWhenCheck(current, data[i], i);
	            }
	            htGrid.render();
			}
	    });
	},
	/**
	 * Lay danh sach cac phieu da duoc tach don theo Apparam
	 * 
	 * @author hunglm16
	 * @since November 25,2014
	 * */
	getDataInGridPrdStock: function(varParams) {
		$('.ErrorMsgStyle').html('').hide();
		var params = null;
		if(varParams == undefined || varParams == null){
			UpdateInformationOutputWarehouse._transType = $('#dllTransType').val().trim();
			params = {transType: $('#dllTransType').val().trim()};
		} else {
			params = varparams;
		}
		//$("#loadingDebit").show();
		var url = '/upd_information_output_warehouse/getListBySearchUpdInfOutputWarehs';
		Utils.getJSONDataByAjax(params, url, function (res) {
			//Xu ly cac bien toan cuc ban dau
			UpdateInformationOutputWarehouse._mapDataGridPrdStockIssueFull = new Map();
			UpdateInformationOutputWarehouse._mapDataByIsIndexPrint = new Map();
			if (UpdateInformationOutputWarehouse._isIndexPrint == undefined || UpdateInformationOutputWarehouse._isIndexPrint == null || Number(UpdateInformationOutputWarehouse._isIndexPrint) < 0) {
				UpdateInformationOutputWarehouse._isIndexPrint = 1;
			}
			--UpdateInformationOutputWarehouse._isIndexPrint;
			
			//Xu ly cho du lieu tra ve
			if (res != undefined && res.rows != undefined) {
				var data = res.rows;
		    	if(data != null && data.length > 0){
		    		//TODO Luu tru vao bien map cho du lieu thay doi
		    		var arrRowsDetai = new Array();
		    		var isIdAttrFirt = data[0].isIdAttr;
		    		UpdateInformationOutputWarehouse._stockIssueIdTemp = 0;
		    		var mapTempIndexPrint = new Map();
		    		var idValTemp = 0;
		    		for (var i = 0; i < data.length; i++) {
	    				UpdateInformationOutputWarehouse._mapDataGridPrdStockIssueFull.put(data[i].isIdDetail, data[i]);
	    				//TODO Phan loai Xu ly tach don
	    				if (data[i].isIdAttr === isIdAttrFirt) {
	    					arrRowsDetai.push(data[i]);
	    				} else if (arrRowsDetai.length > 0){
	    					//Xu ly tac don du lieu tho
	    					mapTempIndexPrint.put(idValTemp++, arrRowsDetai);
	    					idValTemp ++;
	    					//Xu ly tiep cho i tiep theo
	    					isIdAttrFirt = data[i].isIdAttr;
	    					arrRowsDetai = new Array();
	    					arrRowsDetai.push(data[i]);
	    				} else {
	    					arrRowsDetai.push(data[i]);
	    					mapTempIndexPrint.put(idValTemp++, arrRowsDetai);
		    				idValTemp ++;
		    				isIdAttrFirt = data[i].isIdAttr;
		    				arrRowsDetai = new Array();
		    				arrRowsDetai.push(data[i]);
	    				}
	    				if (i == data.length - 1) {
	    					mapTempIndexPrint.put(idValTemp++, arrRowsDetai);
	    				}
		    		}
		    		//Xu ly tach don
		    		var keyArrayTmp = mapTempIndexPrint.keyArray;
		    		for (var i = 0; i < keyArrayTmp.length; i++) {
		    			var valArrayTmp = mapTempIndexPrint.get(keyArrayTmp[i]);
		    			if (valArrayTmp.length > UpdateInformationOutputWarehouse._isIndexPrint) {
		    				var countIndex = 0;
		    				var valArrayItem = [];
		    				for (var j = 0; j < valArrayTmp.length; j ++) {
		    					if (countIndex <= UpdateInformationOutputWarehouse._isIndexPrint) {
		    						valArrayItem.push(valArrayTmp[j]);
		    						countIndex ++;
		    					} else {
		    						if (valArrayItem != null && valArrayItem.length > 0) {
		    							UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.put(UpdateInformationOutputWarehouse._stockIssueIdTemp++, valArrayItem);
		    							//Xu ly buoc tiep theo cua j
		    							valArrayItem = new Array();
		    							valArrayItem.push(valArrayTmp[j]);
		    							countIndex = 1;
		    						}
		    					}
		    					//Xu ly cho vong lap cuoi cung
								if (j == valArrayTmp.length - 1) {
									UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.put(UpdateInformationOutputWarehouse._stockIssueIdTemp++, valArrayItem);
								}
		    				}
		    			} else {
		    				UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.put(UpdateInformationOutputWarehouse._stockIssueIdTemp++, valArrayTmp);
		    			}
		    			
		    		}
		    		$("#loadingDebit").hide();
		      		$('#prdStockIssueDg').handsontable('getInstance').loadData(UpdateInformationOutputWarehouse.getListShowPrdStockIssueDg());
		    	} else {
		    		$('#prdStockIssueDg').handsontable('getInstance').loadData([]);
		    	}
		    	$('#cbxCheckAll').prop('checked', false);
		    	UpdateInformationOutputWarehouse._checkF9 = false;
			} else {
				$('#prdStockIssueDg').handsontable('getInstance').loadData([]);
			}
		}, null, null);
	},
	
	/**
	 * Xu ly tach don
	 * 
	 * @author hunglm16
	 * @since December 09,2014
	 * */
	splittingTheBill: function (arrBill, indexBill, index) {
		if (arrBill == undefined || arrBill == null || arrBill.length == 0 || indexBill == undefined 
				|| indexBill == null || isNaN(indexBill) == true || arrBill.length < index) {
			return;
		}
		var arrVal = [];
		var count = -1;
		for (var i = index; i < arrBill.length; i ++) {
			arrVal.push(arrBill[i]);
			count++;
			if (count == indexBill) {
				count = -1;
				UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.put(UpdateInformationOutputWarehouse._stockIssueIdTemp, arrVal);
				UpdateInformationOutputWarehouse._stockIssueIdTemp++;
				index = i + 1;
				UpdateInformationOutputWarehouse.splittingTheBill(arrBill, indexBill, index);
			}
		}
		return;
	},
	
	/**
	 * Lay danh sach Don da tach
	 * 
	 * @author hunglm16
	 * @since November 26,2014
	 * */
	getListShowPrdStockIssueDg: function () {
		var rows = [];
		if (UpdateInformationOutputWarehouse._mapDataByIsIndexPrint != null || UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.size() > 0) {
			var arrStockIssueId = UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.keyArray; 
			for(var i=0; i < arrStockIssueId.length; i++) {
				var valMap = UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.get(arrStockIssueId[i])[0];
				var cbxHtml = '<input type = "checkbox" class="htCheckboxRendererInput" id="cbxPrdStockIssueDg_' +arrStockIssueId[i]+'" style="margin-left: 4px; margin-top: 4px;" onchange="UpdateInformationOutputWarehouse.changeWhenCheckNew(this);" />';
				var obj = {
						isCheckBox: cbxHtml,
						stockTransCode: valMap.stockTransCode,
						stockIssueNumber: valMap.stockIssueNumber,
						issueType: valMap.issueType,
						about: valMap.about,
						staffText: valMap.staffText,
						contractNumber: valMap.contractNumber,
						carNumber: valMap.carNumber,
						warehouseOutPutName: valMap.warehouseOutPutName,
						warehouseInputName: valMap.warehouseInputName,
						stockTransDate: valMap.stockTransDate,
						totalAmount: valMap.totalAmount,
						isCheck: false,
						isIdDetail: valMap.isIdDetail,
						stockIssueId :  arrStockIssueId[i]
				};
				rows.push(obj);
			}
		}
		return rows;
	},
		
	loadInfo : function(){
		UpdateInformationOutputWarehouse.getInfo(false,false,true);
	},
	getInfo : function(checkF9,isSave, isSaveExcel) {
		if($('#shortCode').val().trim().length == 0){			
			return false;
		}	
		$('#payreceiptValue').val('');
		$('#debitPreRemain').val($('#debitPostRemain').val());
		var params = new Object();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val();
		if($('#type').val() == -1) {
			return false;
		}
		UpdateInformationOutputWarehouse.getDataInGridPrdStockIssueDg(params);
		return false;
	},
	
	/**
	 * Cap nhat thong tin phieu xuat kho kim van chuyen noi bo
	 * 
	 * @author hunglm16
	 * @since December 15,2014
	 * */
	saveOrUpdateStockIssue: function () {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		if (!$('input.htCheckboxRendererInput').is(":checked")) {
			$('#errMsg').html('Chưa có dòng nào được chọn').show();
			return;
		}
		
		var rows = $('#prdStockIssueDg').handsontable('getData');
		
		var arrStockIssue = [];
		var arrStockIssueNumber = [];
		var arrAbout = [];
		var arrStaffText = [];
		var arrContractNumber = [];
		var arrCarNumber = [];
		var arrWarehouseInputName = [];
		var arrWarehouseOutPutName = [];
		var arrIsIdAttr = [];
		
		for (var i = 0; i< rows.length; i++) {
			if (rows[i].isCheck) {
				var errMsg = '';
				var columnCell = 2;
				if(!/^[0-9a-zA-Z-_.]+$/.test(rows[i].stockIssueNumber)){
					errMsg = format(msgErr_invalid_format_code, 'Số phiếu');
					columnCell = 2;
				}
				if (errMsg.length == 0 && !/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(rows[i].about)) {
					errMsg = format(msgErr_invalid_format_name, 'Về việc');
					columnCell = 4;
				}
				if (errMsg.length == 0 && !/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(rows[i].staffText)) {
					errMsg = format(msgErr_invalid_format_name, 'Người vận chuyển');
					columnCell = 5;
				}
				if (errMsg.length == 0 && !/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(rows[i].contractNumber)) {
					errMsg = format(msgErr_invalid_format_name, 'Hợp đồng số');
					columnCell = 6;
				}
				if (errMsg.length == 0 && !/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(rows[i].carNumber)) {
					errMsg = format(msgErr_invalid_format_name, 'Phương tiện vận chuyển');
					columnCell = 7;
				}
				if (errMsg.length == 0 && !/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(rows[i].warehouseInputName)) {
					errMsg = format(msgErr_invalid_format_name, 'Nhập từ kho');
					columnCell = 8;
				}
				if (errMsg.length == 0 && !/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(rows[i].warehouseOutPutName)) {
					errMsg = format(msgErr_invalid_format_name, 'Xuất từ kho');
					columnCell = 9;
				}
				if (errMsg.length > 0) {
					$('#errMsg').html(errMsg).show();
					$('#prdStockIssueDg').handsontable('getInstance').selectCell(i, columnCell);
					return false;
				}
				
				if (rows[i].isIdAttr != undefined && rows[i].isIdAttr != null) {
					arrIsIdAttr.push(row[i].isIdAttr +'#>' +rows[i].stockIssueNumber);
				}
				if (rows[i].stockIssueNumber != undefined && rows[i].stockIssueNumber != null) {
					arrStockIssueNumberStr.push(row[i].stockIssueId +'#>' +rows[i].stockIssueNumber);
				}
				if (rows[i].stockIssueNumber != undefined && rows[i].stockIssueNumber != null) {
					arrStockIssueNumberStr.push(row[i].stockIssueId +'#>' +rows[i].stockIssueNumber);
				}
				if (rows[i].stockIssueNumber != undefined && rows[i].stockIssueNumber != null) {
					arrStockIssueNumberStr.push(row[i].stockIssueId +'#>' +rows[i].stockIssueNumber);
				}
				
				if (rows[i].stockIssueNumber != undefined && rows[i].stockIssueNumber != null) {
					arrStockIssueNumberStr.push(row[i].stockIssueId +'#>' +rows[i].stockIssueNumber);
				}
				if (rows[i].about != undefined && rows[i].about != null) {
					arrAboutStr.push(row[i].stockIssueId +'#>' +rows[i].about);
				}
				if (rows[i].staffText != undefined && rows[i].staffText != null) {
					arrStaffTextStr.push(row[i].stockIssueId +'#>' +rows[i].staffText);
				}
				if (rows[i].contractNumber != undefined && rows[i].contractNumber != null) {
					arrContractNumberStr.push(row[i].stockIssueId +'#>' +rows[i].contractNumber);
				}
				if (rows[i].carNumber != undefined && rows[i].carNumber != null) {
					arrCarNumberStr.push(row[i].stockIssueId +'#>' +rows[i].carNumber);
				}
				if (rows[i].warehouseInputName != undefined && rows[i].warehouseInputName != null) {
					arrWarehouseInputNameStr.push(row[i].stockIssueId +'#>' +rows[i].warehouseInputName);
				}
				if (rows[i].warehouseOutPutName != undefined && rows[i].warehouseOutPutName != null) {
					arrWarehouseOutPutNameStr.push(row[i].stockIssueId +'#>' +rows[i].warehouseOutPutName);
				}
				var valArrayRow = UpdateInformationOutputWarehouse._mapDataByIsIndexPrint.get(row[i].stockIssueId);
				var arrTmpInRow = [];
				for (var j = 0 ; j < valArrayRow.length; j++) {
					arrTmpInRow.push(valArrayRow[i].isIdDetail);
				}
				arrIsIdAttr.push(row[i].stockIssueId +'#>' +arrTmpInRow.join(',').trim());
            }
		}
		
		var params = {
			isIdAttrStr: arrIsIdAttr.join('#,'),
			stockIssueNumberStr:arrStockIssueNumber.join('#,'),
			about: arrAbout.join('#,'),
			staffText: arrStaffText.join('#,'),
			contractNumber: arrContractNumber.join('#,'),
			car: arrCarNumber.join('#,'),
			warehouseInputName: arrWarehouseInputName.join('#,'),
			warehouseOutPutName: arrWarehouseOutPutName.join('#,'),
			transType: UpdateInformationOutputWarehouse._transType
		};
		
		Utils.addOrSaveData(params, "/upd_information_output_warehouse/createStockIssue", null, 'errMsg', function(data) {
			UpdateInformationOutputWarehouse.loadPagePrdStockIssueDg();
			$("#successMsg").html("Lưu dữ liệu thành công").show();
			setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null);
		return false;
	}
};

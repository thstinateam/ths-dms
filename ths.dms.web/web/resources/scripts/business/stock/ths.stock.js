/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-manager.js
 */
var StockManager = {
	_arrRreeShopTocken: null,
	_mapWarehouse: null,
	_arrShopCopyCheked: null,
	_paramWarehouseSearch: null,
	_warehouseType: "",
	_shopRoot: null,
	_shopChoose: null,
	_shopChooseChange: null,
	_warehouseId: null,
	_rowIndexByTree: 0,
	_flagG:null,
	_flagDl:0,
	_activeStatus: 1,
	_stopedStatus: 0,
	///@author hunglm16; @since: August 15,2014; @description Tim kiem danh sach don vi tren Dialog
	searchListShopByCMS:function(){
		var params = {};
		params.code = $('#txtCmnShopCode').val().trim();
		params.name = $('#txtCmnShopName').val().trim();
		params.isShowAllShop = true;
		$('#common-dialog-grid-search').datagrid("load", params);
	},
	///@author hunglm16; @since: August 15,2014; @description Hien thi Dialog tim kiem danh sach don vi
	showSearchListShopByCMS:function(flag, url){
		$('#common-dialog-search-2-textbox').dialog({
			title: 'Thông tin tìm kiếm',
			width: 600, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#txtCmnShopCode').val("").show();
				$('#txtCmnShopName').val("").show();
				var params = {};
				params.code = $('#txtCmnShopCode').val();
				params.name = $('#txtCmnShopName').val();
				params.isShowAllShop = true;
				$('#common-dialog-grid-search').datagrid({
					url : url,
					autoRowHeight : true,
					rownumbers : true,
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize: 10,
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
					queryParams:params,
					fitColumns:true,
					width : ($('#common-dialog-search-2-textbox').width() - 25),
					columns : [[
						{field:'shopCode', title: stock_manage_ma_don_vi, align:'left', width: 110, sortable:false, resizable:false, formatter: function (value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:'shopName', title: stock_manage_ten_don_vi, align:'left', width: 150, sortable:false, resizable:false, formatter: function (value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
							return '<a href="javascript:void(0)" onclick="return StockManager.callSelectF9SialogShopSearchWarehouse(' +flag+', ' +row.shopId+',\'' +row.shopCode+'\',\'' +row.shopName+'\');">chọn</a>';         
						}}
					]],
					onLoadSuccess :function(){
						$('#common-dialog-search-2-textbox .datagrid-header-rownumber').html(area_tree_stt);
						var dHeight = $('#common-dialog-search-2-textbox').height();
						var wHeight = $(window).height();
						var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
					}
				});
				
			},
			onClose: function() {
				$('#txtCmnShopCode').val("").change();
				$('#txtCmnShopName').val("").change();
			}
		});
	},
	///@author hunglm16; @since: August 15,2014; @description Tao tham so tim kiem Warehouse
	createWarehouseParamaterSearch:function(){
		StockManager._paramWarehouseSearch = {};
		StockManager._paramWarehouseSearch.longG = StockManager._shopChoose.shopId;
		if ($('#txtShopCode').val() != null && $('#txtShopCode').length > 0) {
			var arrShopCode = $('#txtShopCode').val().trim().split(" - "); 
			if (arrShopCode.length > 0) {
				StockManager._paramWarehouseSearch.shopCode = arrShopCode[0];
			}
		}
		//StockManager._paramWarehouseSearch.shopCode = $('#txtShopCode').val().trim();
		StockManager._paramWarehouseSearch.code = $('#txtWarehouseCode').val().trim();
//		StockManager._paramWarehouseSearch.name = $('#txtWarehouseName').val().trim();
		StockManager._paramWarehouseSearch.status = $('#ddlStatus').val().trim();
		//StockManager._paramWarehouseSearch.textG = $('#ddlWareHouseType').val().trim();
		StockManager._paramWarehouseSearch.fromDateStr = $('#txtFromDate').val().trim();
		StockManager._paramWarehouseSearch.toDateStr = $('#txtToDate').val().trim();
		StockManager._paramWarehouseSearch.warehouseType = $('#cbWarehouseType').val();
	},
	///@author hunglm16; @since: August 15,2014; @description Tim kiem Warehouse
	searchWarehouse:function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var fDate = $('#txtFromDate').val();
		var tDate = $('#txtToDate').val();
		if(fDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('txtFromDate', stock_manage_tu_ngay);
		}
		if(msg.length == 0 && tDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('txtToDate', stock_manage_den_ngay);
		}
		if(fDate.length >0 && tDate.length >0 && !Utils.compareDate(fDate, tDate)){
				msg = stock_manage_tungay_denngay;
				$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsgStockManage').html(msg).show();
			return false;
		}
		StockManager.createWarehouseParamaterSearch();
		$('#dgWarehouse').datagrid('load', StockManager._paramWarehouseSearch);
	},
	///@author hunglm16; @since: August 15,2014; @description Goi nhan tham so gui ve luc chon Don vi tu Dialog tim kiem
	callSelectF9SialogShopSearchWarehouse: function(flag, shopId, shopCode, shopName){
		if(flag==0){
			StockManager._shopChoose = {};
			StockManager._shopChoose.shopId = shopId;
			if(shopCode!=undefined && shopCode!=null){
				StockManager._shopChoose.shopCode = shopCode;
				$('#txtShopCode').val(Utils.XSSEncode(shopCode.trim()+ " - " + shopName));
			}
			if(shopName!=undefined && shopName!=null){
				StockManager._shopChoose.shopName = shopName;
				$('#lblShopName').html(Utils.XSSEncode(shopName.trim())).change();
			}
			$('#common-dialog-search-tree-in-grid-choose-single').dialog("close");
		}else if(flag == 1){
			StockManager._shopChooseChange = {};
			StockManager._shopChooseChange.shopId = shopId;
			if(shopCode!=undefined && shopCode!=null){
				StockManager._shopChooseChange.shopCode = shopCode;
				$('#txtShopCodeDl').val(Utils.XSSEncode(shopCode.trim()+ " - " + shopName));
				$('#common-dialog-search-tree-in-grid-choose-single').dialog("close");
			}
			if(shopName!=undefined && shopName!=null){
				StockManager._shopChooseChange.shopName = shopName;
				$('#lblShopNameDl').html(shopName.trim()).change();
			}
			$('#common-dialog-search-2-textbox').dialog("close");
		}else if(flag == 2){
			StockManager._shopChooseChange = {};
			StockManager._shopChooseChange.shopId = shopId;
			if(shopCode!=undefined && shopCode!=null){
				StockManager._shopChooseChange.shopCode = shopCode;
				$('#txtShopCodeDlCp').val(shopCode.trim());
			}
			if(shopName!=undefined && shopName!=null){
				StockManager._shopChooseChange.shopName = shopName;
				$('#lblShopNameCopy').html(shopName.trim()).change();
			}
			$('#common-dialog-search-2-textbox').dialog("close");
		}
	},
	///@author hunglm16; @since: August 15,2014; @description Bat su kien thay doi shopCode
	txtShopCodeOnchange: function(txt){
		var txtShopCode = $(txt).val().trim();
		StockManager._flagG = {};
		StockManager._flagG.isTrue = false;
		StockManager._flagG.shopName = '';
		if (StockManager._arrRreeShopTocken.length > 0) {
			for (var i = 0; i < StockManager._arrRreeShopTocken.length; i++) {
				var root = StockManager._arrRreeShopTocken[i];
				StockManager.checkInofShopCodeInTreeCMS(root, txtShopCode.trim());
				if (StockManager._flagG) {
					break;
				}
			}
			if (!StockManager._flagG.isTrue) {
				$('#lblShopName').html('');
			} else {
				$('#lblShopName').html(StockManager._flagG.shopName);
			}
		} else {
			$('#lblShopName').html('');
		}
	},
	checkInofShopCodeInTreeCMS : function(node, code) {
		if (node != undefined && node != null && !StockManager._flagG.isTrue) {
			if (node.attribute != undefined && node.attribute != null && node.attribute.shopCode != undefined && node.attribute.shopCode != null && code.toUpperCase() === node.attribute.shopCode.trim().toUpperCase()) {
				StockManager._flagG.isTrue = true;
				StockManager._flagG.shopName = node.attribute.shopName;
				return;
			} else {
				var arrChidren = node.children;
				if (arrChidren != undefined && arrChidren != null && arrChidren.length > 0) {
					for (var i = 0; i < arrChidren.length; i++) {
						StockManager.checkInofShopCodeInTreeCMS(arrChidren[i], code);
					}
				}
			}
		}
	},
	
	///@author hunglm16; @since: August 15,2014; @description mo pupup them(sua) kho
	dialogChangeStockManager : function(isEvent, id) { // phuocdh2 them tham so
		$(".ErrorMsgStyle").html('').hide();
		$('input[id$="Dl"]').val("").change();
		var msg = "";
		if (isEvent == undefined || isEvent == null) {
			return false;
		}
		var titleView = "";
		StockManager._shopChooseChange = {};
		if (isEvent == 0) {
			StockManager._warehouseId = -1;
			titleView = stock_manage_them_moi_kho;
			if(StockManager._shopRoot!=null){
				$('#txtShopCodeDl').val(Utils.XSSEncode(StockManager._shopRoot.shopCode +' - ' + StockManager._shopRoot.shopName));
				$('#lblShopNameDl').html(Utils.XSSEncode(StockManager._shopRoot.shopName));
				StockManager._shopChooseChange = {};
				StockManager._shopChooseChange.shopId = StockManager._shopRoot.shopId;
				StockManager._shopChooseChange.shopCode = StockManager._shopRoot.shopCode;
				StockManager._shopChooseChange.shopName = StockManager._shopRoot.shopName;
			}
			$('#txtWarehouseCodeDl').val("").change();
			$('#txtWarehouseNameDl').val("").change();
			$('#txtDescriptionDl').val("").change();
			selectedDropdowlist('ddlStatusDl', activeType.RUNNING);
			selectedDropdowlist('cbWarehouseTypeDl', warehouseType.SALES);
			enable('txtShopCodeDl');
			enable('txtWarehouseCodeDl');
			enable('cbWarehouseTypeDl');
			$('#txtSeqDl').val("1");
		} else if (isEvent == 1) {
			StockManager._warehouseId = id;
			titleView = stock_manage_chinh_sua_kho;
			var warehouse = StockManager._mapWarehouse.get(id);
			if(warehouse!=undefined && warehouse!=null){
				StockManager._warehouseId = id;
				titleView =titleView+'<span style="color:#199700;">' +' ' + Utils.XSSEncode(warehouse.warehouseCode.trim()) +' - ' + Utils.XSSEncode(warehouse.warehouseName) + ' ' + '</span>';
				titleView = titleView.trim();
				StockManager._shopChooseChange = {};
				StockManager._shopChooseChange.shopId = warehouse.shopId;
				StockManager._shopChooseChange.shopCode = warehouse.shopCode;
				StockManager._shopChooseChange.shopName = warehouse.shopName;
				$('#txtShopCodeDl').val(Utils.XSSEncode(warehouse.shopCode + ' - ' + warehouse.shopName));
				$('#lblShopNameDl').html(Utils.XSSEncode(warehouse.shopName));
				$('#txtWarehouseCodeDl').val(Utils.XSSEncode(warehouse.warehouseCode));
				$('#txtWarehouseNameDl').val(Utils.XSSEncode(warehouse.warehouseName));
				$('#txtDescriptionDl').val(Utils.XSSEncode(warehouse.description));
				selectedDropdowlist('ddlStatusDl', warehouse.status);
				selectedDropdowlist('cbWarehouseTypeDl', warehouse.warehouseType);
				var seqStr = warehouse.seq;
				if (seqStr != undefined && seqStr != null) {
					$('#txtSeqDl').val(Utils.XSSEncode(warehouse.seq.toString()));					
				}
				disabled('txtShopCodeDl');
				disabled('txtWarehouseCodeDl');
				disabled('cbWarehouseTypeDl');
			}
		}
		
		$('#changeWarehouseDialog').dialog({
			title: titleView,
			closed: false,  
	        cache: false,  
	        modal: true,
	        width: 680,
	        onOpen : function() {
				if (isEvent == 0) {
					$('#txtWarehouseCodeDl').focus();
				} else {
					$('#txtWarehouseNameDl').focus();
				}
			},
			onClose : function() {
				StockManager._shopChooseChange = {};
			}
		});
		return false;
	},
	//@author hunglm16; @since: August 15,2014; @description Mo pupup sao chep Kho
	dialogCopyStockManager : function() {
		$(".ErrorMsgStyle").html('').hide();
		StockManager._arrShopCopyCheked = [];
		StockManager._rowIndexByTree = -1;
		StockManager._shopChooseChange = {};
		StockManager._shopChooseChange.shopId = StockManager._shopRoot.shopId;
		StockManager._shopChooseChange.shopCode = StockManager._shopRoot.shopCode;
		StockManager._shopChooseChange.shopName = StockManager._shopRoot.shopName;
		$('#txtShopCodeDlCp').val(StockManager._shopRoot.shopCode);
		$('#lblShopNameCopy').html(Utils.XSSEncode(StockManager._shopRoot.shopName)).show();
		selectedDropdowlist('ddlStatusDlCp', activeType.ALL);
		selectedDropdowlist('ddlWareHouseTypeCpDl', StockManager._warehouseType);
		$('#warehouseContainerCopyDgDiv').html('<table id="dgWarehouse"></table>').change();
		
		$('#copyWarehouseDialog').dialog({
			title: stock_manage_sao_chep_kho,
			closed: false,
	        cache: false, 
	        modal: true,
	        width: 850,
	        height: 490,
	        onOpen: function(){
	        	$('#txtShopCodeDlCp').focus();
	        	//Tao cay don vi
	        	$('#dgTreeShopCopy').treegrid({  
	    		    //url:  '/cms/searchTreeShop',
	        		data: StockManager._arrRreeShopTocken,
	    		    width: $('#shopTreeByCopyContainerGrid').width(),
	    		    height: 322,
	    		    //queryParams: {},
	    			fitColumns : true,
	    			checkOnSelect: false,
	    			selectOnCheck: false,
	    		    rownumbers : true,
	    	        idField: 'id',
	    	        treeField: 'code',
	    		    columns:[[
	    		        {field:'id',title:'',resizable:false, width:40, align:'center', fixed:true, formatter:function(value, row, index){
	    		        	StockManager._rowIndexByTree++;
	    		        	var html = '<input type="checkbox" onchange="StockManager.onchangeCbxDgTreeShopCopy(this);" name=' +row.id+' id="cbx_DgTreeShopDlCp_' +row.id+'" value="' +StockManager._rowIndexByTree+'">';
	    		        	return html;
	    		        }},
	    		        {field:'code',title:stock_manage_ma_don_vi,resizable:false, width:180, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }},
	    		        {field:'name',title:stock_manage_ten_don_vi,resizable:false, width:150, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }}
	    		    ]],
	    	        onLoadSuccess :function(data){
	    				$('.datagrid-header-rownumber').html(area_tree_stt);
	    				Utils.updateRownumWidthAndHeightForDataGrid('dgTreeShopCopy');
	    				StockManager._rowIndexByTree=-1;
	    	        }
	    		});
	        },
	        onClose:function() {
	        	selectedDropdowlist('ddlStatusDl', activeType.RUNNING);
	        }
		});
	},
	///@author hunglm16; @since: August 15,2014; @description Mo pupup sao chep Kho
	dialogSeachShopCommon: function(){
		$(".ErrorMsgStyle").html('').hide();
		$('#common-dialog-dgTreeShopGContainerGS').html('<table id="commonDialogTreeShopG"></table>').change();
		$('#common-dialog-search-tree-in-grid-choose-single').dialog({
			title: stock_manage_tim_kiem_don_vi,
			closed: false,
	        cache: false, 
	        modal: true,
	        width: 680,
	        height: 550,
	        onOpen: function(){
	        	$('#txtShopCodeDlg').val("");
	        	$('#txtShopNameDlg').val("");
	        	//Tao cay don vi
	        	$('#commonDialogTreeShopG').treegrid({  
	        		data: StockManager._arrRreeShopTocken,
	        		url : '/cms/searchTreeShop',
	    		    width: 650,
	    		    height: 325,
	    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
	    			fitColumns : true,
	    			checkOnSelect: false,
	    			selectOnCheck: false,
	    		    rownumbers : true,
	    	        idField: 'id',
	    	        treeField: 'code',
	    		    columns:[[
	    		        {field:'code',title:stock_manage_ma_don_vi,resizable:false, width:200, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }},
	    		        {field:'name',title:stock_manage_ten_don_vi,resizable:false, width:200, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }},
	    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
	    		        	return '<a href="javascript:void(0)" onclick="return StockManager.callSelectF9SialogShopSearchWarehouse(0, ' +row.id+',\'' +row.code+'\',\'' +row.name+'\');">' +stock_manage_chon+'</a>';
	    		        }}
	    		    ]],
	    	        onLoadSuccess :function(data){
	    				$('.datagrid-header-rownumber').html(area_tree_stt);
	    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
	    	        }
	    		});
	        	$('#txtShopCodeDlg').focus();
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	onchangeCbxDgTreeShopCopy:function(cbx){
		if($(cbx)[0].checked){
			var node = $('#dgTreeShopCopy').treegrid('find', parseInt($(cbx)[0].name.trim()));
			StockManager.uncheckFullParentToNode(node, node.id);
			StockManager.uncheckNodeGroup(node, node.id);
			var children = node.children;
			if(StockManager._arrShopInOrgAccessCheked!=undefined && StockManager._arrShopInOrgAccessCheked!=null && StockManager._arrShopInOrgAccessCheked.length>0){
				if(StockManager._arrShopInOrgAccessCheked.indexOf(node.id)<0){
		    		StockManager._arrShopCopyCheked.push(node.id);
		    	}
			}else{
				StockManager._arrShopCopyCheked.push(node.id);
			}
		}else{
			var node = $('#dgTreeShopCopy').treegrid('find', parseInt($(cbx)[0].name.trim()));
			StockManager._arrShopCopyCheked = jQuery.grep(StockManager._arrShopCopyCheked, function( a ) {
				return a !== node.id;
			});
			StockManager.uncheckFullParentToNode(node, node.id);
		}
	},
	///@author hunglm16; @since: August 18,2014; @description uncheckParent and uncheck Childrent
	uncheckFullParentToNode: function(node, id){
		if(node != undefined && node != null){
			StockManager._arrShopCopyCheked = jQuery.grep(StockManager._arrShopCopyCheked, function( a ) {
				return a !== node.id;
			});
			var cbx = $('#cbx_DgTreeShopDlCp_' + node.id);
			if (node.id != id) {
				$(cbx).attr('checked', false);
			}
			if (node.attribute != undefined && node.attribute != null && node.attribute.parentId != undefined && node.attribute.parentId != null) {
				var parNode = $('#dgTreeShopCopy').treegrid('find', node.attribute.parentId);
				if (parNode != undefined && parNode != null) {
					StockManager.uncheckFullParentToNode(parNode);
				} else {
					return;
				}
			}
		}else{
			return;
		}
	},
	///@author HungLM16; @since JULY 25,2014; @description Mo node dau tien va con chau cua no
	uncheckNodeGroup: function(node, id){
		if(node!=undefined && node!=null){
			if(StockManager._arrShopCopyCheked.indexOf(node.id)<0){
				StockManager._arrShopCopyCheked = jQuery.grep(StockManager._arrShopCopyCheked, function( a ) {
					return a !== node.id;
				});
	    	}
			var cbx = $('#cbx_DgTreeShopDlCp_' + node.id);
			if (node.id != id) {
				$(cbx).attr('checked', false);
			}
			var arrChidren = node.children;
			if (arrChidren != undefined && arrChidren != null && arrChidren.length > 0) {
				for (var i = 0; i < arrChidren.length; i++) {
					StockManager.uncheckNodeGroup(arrChidren[i]);
				}
			}
		}
	},
	///@author hunglm16; @since: August 15,2014; @description them moi hoac cap nhat thong tin kho
	changeWarehouseByDialog: function(){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		if (StockManager._shopChooseChange.shopId == undefined || StockManager._shopChooseChange.shopId==null || StockManager._shopChooseChange.shopId<0) {
			msg = stock_manage_khong_xac_dinh_vi_tri;
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtWarehouseCodeDl", stock_manage_ma_kho);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate("txtWarehouseCodeDl", stock_manage_ma_kho, Utils._NAME);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtWarehouseNameDl", stock_manage_ten_kho);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate("txtWarehouseNameDl", stock_manage_ten_kho, Utils._NAME);
		}
		var status = $('#ddlStatusDl').val();
		if(msg.length == 0 && (status==null || status==="" || parseInt(status)<0)){
			msg = stock_manage_khong_xac_dinh_tinh_trang;
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate("txtDescriptionDl", stock_manage_tooltip_ghi_chu, Utils._NAME);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildInteger("txtSeqDl", stock_manage_tooltip_thu_tu);
			if (msg.length > 0) {
				msg = stock_manage_thu_tu_lon_hon_mot;
			}
		}
		if (msg.length > 0) {
			$("#errMsgDialogChangeWarehouse").html(msg).show();
			return false;
		}
		var isFlag = false;
		msg ='';
		if (StockManager._warehouseId != undefined && StockManager._warehouseId != null && StockManager._warehouseId > -1) {
			var warehouse = StockManager._mapWarehouse.get(StockManager._warehouseId);
			msg = Utils.XSSEncode(stock_manage_ban_cap_nhat_kho + ' ' + warehouse.warehouseCode +' - ' + warehouse.warehouseName + ' ? ');
			isFlag = true;
		} else {
			msg = stock_manage_them_kho;
		}
		var params = {
				id: StockManager._warehouseId,
				longG: StockManager._shopChooseChange.shopId,//ShopId
				code: $("#txtWarehouseCodeDl").val().trim(),
				name: $("#txtWarehouseNameDl").val().trim(),
				status: $("#ddlStatusDl").val(),
				description: $("#txtDescriptionDl").val().trim(),
				seq: $('#txtSeqDl').val(),
				warehouseType: $('#cbWarehouseTypeDl').val(),
				isFlag: isFlag
		};
		Utils.addOrSaveData(params, "/stock-manage/addorUpdateWarehouse", null, 'errMsgDialogChangeWarehouse', function(data) {
			StockManager.searchWarehouse();
			$("#successMsgDialogChangeWarehouse").html(msgCommon1).show();
			setTimeout(function(){
				$('#changeWarehouseDialog').dialog("close");
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
		return false;
	},
	///@author hunglm16; @since: August 19,2014; @description Sao chep kho
	copyWeahouse: function(){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		if (StockManager._shopChooseChange.shopId == undefined || StockManager._shopChooseChange.shopId==null || StockManager._shopChooseChange.shopId<0) {
			msg = stock_manage_khong_xac_dinh_vi_tri;
		}
		var status = $('#ddlStatusDl').val();
		if(msg.length == 0 && (status==null || status==="" || parseInt(status)<0)){
			msg = stock_manage_khong_xac_dinh_tinh_trang;
		}
		if(msg.length == 0 && (StockManager._arrShopCopyCheked==null || StockManager._arrShopCopyCheked.length==0)){
			msg = stock_manage_chua_chon_don_vi;
		}
		if (msg.length > 0) {
			$("#errMsgCoppyWarehouseDl").html(msg).show();
			return false;
		}
		msg = stock_manage_sao_chep_kho;
		var params = {
				longG: StockManager._shopChooseChange.shopId,//ShopId
				//textG: $("#ddlWareHouseTypeCpDl").val(),
				status: $("#ddlStatusDlCp").val(),
				arrlongG: StockManager._arrShopCopyCheked
				
		};
		Utils.addOrSaveData(params, "/stock-manage/copy-Warehouse", null, 'errMsgCoppyWarehouseDl', function(data) {
			StockManager.searchWarehouse();
			$("#successCoppyWarehouseDl").html(msgCommon1).show();
			setTimeout(function(){
				$('#copyWarehouseDialog').dialog("close");
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
		return false;
	},
	///@author hunglm16; @since: August 15,2014; @description Xoa Kho
	deleteStockManager: function(id, code){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		if(code==undefined || code ==null){
			code = "";
		}
		code = code.toString().trim();
		var params = {
				id: id
		};
		var msg = stock_manage_xoa_kho+code+"?";
		Utils.addOrSaveData(params, "/stock-manage/deleteWarehouse", null, 'errMsgStockManage', function(data) {
			StockManager.searchWarehouse();
			$("#successMsgStockManage").html(msgCommon1).show();
			setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
	},
	
	///@author hunglm16; @since: August 15,2014; @description Xoa Kho
	updateStatusStockManager: function(id, code, statusStr){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		if(code==undefined || code ==null){
			code = "";
		}
		code = code.toString().trim();
		var params = {
				id: id,
				isEvent : 0
		};
		var msg = Utils.XSSEncode(stock_manage_co_muon+ '  '  +statusStr+ ' ' +stock_manage_title+ ' ' +code+ '?');
		Utils.addOrSaveData(params, "/stock-manage/updateStatusWarehouseByEnvent", null, 'errMsgStockManage', function(data) {
			StockManager.searchWarehouse();
			$("#successMsgStockManage").html(msgCommon1).show();
			setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
	}
	
};
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-manager.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-category.js
 */
var StockCategory = {
	_xhrSave : null,
	_xhrDel: null,	
	_pMap:null,
	_pCheckedMap:null,
	xhrExport: null,
	lastValue: 1,
	_pTotalCheckedMap:null,
	_status:null,
	_lstStockLock: null,
	lockStockStaff:function(){
		var dataModel = {};
		var msg = '';
		var focusId = '#saleStaff';
		var staffCode = $('#saleStaff').combobox('getValue');
		if(staffCode==undefined || staffCode == null || staffCode.trim() ===''){
		msg = 'Chưa chọn Mã NV';
		$('#saleStaff').focus();
		}
		if(msg.length == 0){
			var lstNV = $('#saleStaff').combobox('getData');
			var flag = false;
			if(lstNV!=undefined && lstNV!=null && lstNV.length>1){
				for(var i=1; i<lstNV.length; i++){
					if(lstNV[i].staffCode.toLowerCase().trim()===staffCode.toLowerCase().trim()){
						flag = true;
						break;
					}
				}
				if(!flag){
					msg = 'Mã NV không hợp lệ';
				}
			}else{
				msg = 'Không tìm thấy nhân viên nào';
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).change().show();
			return false;
		}
		
		dataModel.staffCode = staffCode;
		Utils.addOrSaveData(dataModel, "/catalog/utility/closing-day/lock-stock-staff-change", null, 'errMsg', function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg).show();
			}else {
				$('#successMsg').html('Khóa kho thành công').change().show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html('').change().show();
					window.location.reload();
				}, 700);
			}
		}, null, null);
		return false;
		
	},
	processAccordingToCycleType:function(){
		if($('#lsttype').val()=='0'){
			$('#btnStockCategorySave').show();
			disabled('addProduct');
			var message = "<font color=red>*<strong>Lưu ý:</strong></font><span style='color:#707070'> Hệ thống sẽ lấy tất cả mặt hàng trong kho để kiểm kê.</span>";
			$('#messageWarning').html(message).show();				
			StockCategory.getAllProductFromServerToGrid();
		}else{
			var message = "<font color=red>*<strong>Lưu ý:</strong></font> <span style='color:#707070'>Bạn phải chọn danh sách mặt hàng để kiểm kho.</span>";
			$('#messageWarning').html(message).show();
			enable('addProduct');
			StockCategory.getResultFromServer($('#cycleCountCode').val());
		}
	},
	getMaxActiveSockCardOfMap:function(map){
		if(map==null){
			return parseInt($('#firstNumber').val().trim())-1;
		}
		if(map.size()==0){
			return parseInt($('#firstNumber').val().trim())-1;
		}
		var max = map.valArray[0].stockCardNumber;
		for(var i=1;i<map.size();i++){
			if(map.valArray[i].isDelete == 0){
				if(max <= map.valArray[i].stockCardNumber){
					max = map.valArray[i].stockCardNumber;
				}
			}
		}
		return max;
	},
	getParams:function(){
		var shopId = $('#shopId').val().trim();
		var cycleCountCode = $('#cycleCountCode').val().trim();
		var firstNumber = $('#firstNumber').val().trim();
		var data = new Object();
		if(shopId>0){
			data.shopId = shopId;
		}
		if(cycleCountCode.length>0){
			data.cycleCountCode = cycleCountCode;
		}
		if(firstNumber>=0){
			data.firstNumber = firstNumber;
		}		
		data.startDate = $('#startDate').val().trim();
		data.cycleCountDes = $('#cycleCountDes').val().trim();
		data.cycleType = $('#lsttype').val().trim();
		return data;
	},
	openSelectProductDialog: function(){
		
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		}			
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if(!StockCategory.checkFirstNumber()){
			return false;
		}
		$('td[field=id] div input[type=checkbox]').each(function(){	
			$(this).attr('checked',false);
		});
		var listProduct = new Array();
		for(var i=0;i<StockCategory._pMap.size();i++){			
			var obj = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
			if(obj!=null && obj.id!=null){
				listProduct.push(obj.id);
			}
		}
		var listProductEx = listProduct.toString();
		CommonSearch._arrParams = null;
		$('#productDialog').css("visibility", "visible");
		var html = $('#productDialog').html();
		$('#productCode').val('');
		$('#productName').val('');
		$('#fAmnt').val('');
		$('#tAmnt').val('');
		$('#category').val('Chọn nghành hàng').change(function(){
			var params  = new Object();
			params.idParentCat = $('#category').val();	
			Utils.getHtmlDataByAjax(params,'/stock/category/getListSubCat',
					function(data) {
						try {
							var _data = JSON.parse(data);
						} catch (e) {

							$('#sub_category').html(data);
							$('#subcat .CustomStyleSelectBoxInner').html("Chọn ngành hàng con");
							$('#sub_category option[value=0]').attr('selected','selected');
						}
					}, '', 'GET');
			
			
		});
		$('#productEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        onOpen: function(){
	        	if(!$('#category').next().hasClass('CustomStyleSelectBox')){
	        		$('#category').customStyle();
	        	}
	        	if(!$('#sub_category').next().hasClass('CustomStyleSelectBox')){
	        		$('#sub_category').customStyle();
	        	}	        		        	
	        	$('#productDialog').html('');
	        	StockCategory._pCheckedMap = new Map();
	        	StockCategory._pTotalCheckedMap = new Map();
	        	
	        	$('.easyui-dialog #productGrid').datagrid({
					url : '/stock/category/search-product',
					autoRowHeight : true,
					rownumbers : true,					
					pagination:true,
					checkOnSelect : true,
					rowNum : 10,
					pageNumber:1,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false,
					queryParams:{
						listProductEx:listProductEx,						
						cycleCountCode: $("#cycleCountCode").val().trim()
					},
					pageList  : [10,20,30],
					width : ($('.easyui-dialog #productGridContainer').width()),
					columns:[[
					    {field: 'id', checkbox: true},
						{field: 'productCode',title:'Mã sản phẩm', width: 150, sortable:false,resizable:false, align: 'left' },
						{field: 'productName',title:'Tên sản phẩm',width: 320,sortable:false,resizable:false, align: 'left' },
						{field: 'cat',index:'cat', title: 'Ngành hàng', width: 100, sortable:false,resizable:false,align: 'left' },
						{field: 'subCat',index:'subCat', title: 'Ngành hàng con', width: 150, sortable:false,resizable:true,align: 'left' },
						{field: 'quantity',index:'quantity', title: 'Tồn kho',width: 100, sortable:false,resizable:false, align: 'right',formatter: CountingFormatter.quantityConvert},							    
						{field: 'productId', index:'id', hidden: true},
						{field: 'convfact', index:'convfact', hidden: true}						       
					 ]],
					 onBeforeLoad:function(param){											 
					 },
					 onLoadSuccess :function(data){   
						 	$('td[field=id] div.datagrid-header-check input[type=checkbox]').attr('checked',false);
					    	$('.datagrid-header-rownumber').html('STT');				    	
					    		
					    	var index = 0;				    
					    	$('td[field=id] div.datagrid-cell-check input[type=checkbox]').each(function(){					    		
					    		$(this).attr('index',index);
					    		var product = $('#productGrid').datagrid('getRows')[index];
					    		$(this).attr('productCode',product.productCode);
					    		if(StockCategory._pCheckedMap.get(product.productCode)!=null){
					    			$('#productGrid').datagrid('selectRow',index);
					    		}					    		
					    		++index;
					    		
					    	});
					    	$(window).resize();
					 },
					 onCheck: function(i, r) {
						var obj = new Object();
						obj.id = r.id;
						obj.code = r.productCode;
						obj.name = r.productName;
						StockCategory._pCheckedMap.put(obj.code,obj);
					 },
					 onUncheck: function(i, r) {
						 StockCategory._pCheckedMap.remove(r.productCode);
					 },
					 onCheckAll: function(rows) {
						 for (var i = 0, sz = rows.length; i < sz; i++) {
							var obj = new Object();
							obj.id = rows[i].id;
							obj.code = rows[i].productCode;
							obj.name = rows[i].productName;
							StockCategory._pCheckedMap.put(obj.code, obj);
						 }
					 },
					 onUncheckAll: function(rows) {
						 for (var i = 0, sz = rows.length; i < sz; i++) {
							StockCategory._pCheckedMap.remove(rows[i].productCode);
						 }
					 }
				});  
	        	$('#productCode').focus();
	        	Utils.bindAutoSearch();
	    		Utils.bindAutoButtonEx('.PopupContentMid','btnSearch');
		     },	        
		     onClose : function(){
		        	$('#productDialog').html(html);
		        	$('#productDialog').css("visibility", "hidden");
		        	$('#cat .CustomStyleSelectBoxInner').html('Chọn ngành hàng');
		    		$('#category option[value=0]').attr('selected','selected');		    		
		    		$('#sub_category').html('<option value="0"></option>');
		    		$('#subcat .CustomStyleSelectBoxInner').html("Chọn ngành hàng con");
		    }
		});	
	},
	searchProduct: function(){		
		var params = new Object();
		params.productCode = $('#productCode').val().trim();
		params.productName = $('#productName').val().trim();
		params.shopId = $('#shopId').val().trim();	
		params.category = $('#category').val().trim();	
		params.sub_category = $('#sub_category').val().trim();	
		params.fromAmnt = $('#fAmnt').val().trim();	
		params.toAmnt = $('#tAmnt').val().trim();
		params.cycleCountCode = $("#cycleCountCode").val().trim();
		var msg = Utils.getMessageOfSpecialCharactersValidate('productCode', 'Mã sản phẩm', Utils._CODE);
		if(msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('productName', 'Tên sản phẩm', Utils._NAME);
		}
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			return;
		}
		if (params.fromAmnt.length > 0) {
			if (!/\d$/.test(params.fromAmnt)) {
				$('#productEasyUIDialog #errMsg').html("Số lượng từ không hợp lệ!").show();
				$('#fAmnt').focus();
				return false;
			}
		}
		if (params.toAmnt.length > 0) {
			if (!/\d$/.test(params.toAmnt)) {
				$('#productEasyUIDialog #errMsg').html("Số lượng đến không hợp lệ!").show();
				$('#tAmnt').focus();
				return false;
			}
		}
		
		if (params.fromAmnt.length > 0 && params.toAmnt.length > 0) {
			var fAmnt = parseInt(params.fromAmnt);
			var tAmnt = parseInt(params.toAmnt);
			if (fAmnt > tAmnt) {
				$('#productEasyUIDialog #errMsg').html("Số lượng từ không được lớn hơn số lượng đến!").show();
				$('#tAmnt').focus();
				return false;
			}
		}		
		$("#productGrid").datagrid('load',params);		
		return false;
	},
	getProductNoPagingUrl: function(){
		var shopId = $('#shopId').val().trim();			
		var noPaging = 1;		
		var url = "/stock/category/search-product?shopId="+shopId+"&noPaging="+noPaging+"&cycleCountCode="+$("#cycleCountCode").val().trim();
		return url;
	},
	addProduct:function(id,code,name){
		var html = new Array();		
		html.push('<tr id="row_'+ id +'">');
		html.push('<td class="ColsTd1 AlignCCols OrderNumber" style="border-left: 1px solid #C6D5DB;">01</td>');
		html.push('<td class="AlignLCols">'+ Utils.XSSEncode(code) +'</td>');
		html.push('<td class="AlignLCols">'+ Utils.XSSEncode(name) +'</td>');
		html.push('<td class="EndCols AlignCCols" style="padding: 4px 5px;"><a href="javascript:void(0)" onclick="return StockCategory.delSelectedRow('+id+');">' +
				'<img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
		var num = 0;
		$('#lstProducts').append(html.join(""));
		$('.OrderNumber').each(function(){
			num++;
			$(this).html(num);
		});		
	},
	getAllProductFromServerToGrid:function(){		
		$.getJSON(StockCategory.getProductNoPagingUrl(),function(result){
			StockCategory._pMap = new Map();
			if(result.error == false){
				StockCategory._pMap = new Map();
				var list = result.list;	
				var nextStockCard = parseInt($('#firstNumber').val().trim());
				for(var i=0;i<list.length;++i){
					var obj = list[i];
					var temp  = {};
					temp.code = obj.productCode;
					temp.name = obj.productName;
					temp.isNewSelectProduct = 1;
					temp.isDelete=0;
					temp.id = obj.id;
					temp.stockCardNumber = nextStockCard++;
					StockCategory._pMap.put(temp.code,temp);
				}
			}
			StockCategory.fillTable();
		});		
		return false;
	},
	getResultFromServer:function(cycleCode){	
		$('#divOverlay').show();	
		$.getJSON(encodeURI('/stock/category/search-detail?cycleCountCode='+cycleCode),function(result){
			$('#divOverlay').hide();		
			StockCategory._pMap = new Map();
			if(!result.error){
				StockCategory._pMap = new Map();
				var list = result.list;
				for(var i=0;i<list.length;++i){
					var obj = list[i];
					var temp  = {};
					temp.id = obj.product.id;
					temp.code = obj.product.productCode;
					temp.name = obj.product.productName;
					temp.stockCardNumber = obj.stockCardNumber;
					temp.isDelete=0;
					temp.isNewSelectProduct=0;
					StockCategory._pMap.put(temp.code,temp);
				}
			}
			StockCategory.fillTable();			
		});		
		return false;
	},
	selectListProducts: function(){		
		var _gridSelection = $('#productGrid').datagrid('getSelections');
		for(var i=0;i<_gridSelection.length;++i){
			var obj = {};
			obj.id = _gridSelection[i].id;
			obj.code = _gridSelection[i].productCode;
			obj.name = _gridSelection[i].productName;
			if(StockCategory._pCheckedMap.get(obj.code)==null){
				StockCategory._pCheckedMap.put(obj.code,obj);
			}			
		}		
		var nextStockCard = 1 + StockCategory.getMaxActiveSockCardOfMap(StockCategory._pMap);
		for(var i=0;i<StockCategory._pCheckedMap.size();i++){			
			var obj = StockCategory._pMap.get(StockCategory._pCheckedMap.keyArray[i]);
			if(obj==null || obj.isDelete==1){
				var cycleCountMapProduct = {};
				var temp = StockCategory._pCheckedMap.get(StockCategory._pCheckedMap.keyArray[i]);
				cycleCountMapProduct.id = temp.id;
				cycleCountMapProduct.code = temp.code;
				cycleCountMapProduct.name = temp.name;
				cycleCountMapProduct.isDelete = 0;
				cycleCountMapProduct.stockCardNumber = nextStockCard++;
				cycleCountMapProduct.isNewSelectProduct = 1;
				StockCategory._pMap.put(StockCategory._pCheckedMap.keyArray[i],cycleCountMapProduct);
			}
		}
		if(StockCategory._pCheckedMap.size() <= 0){
			$('#addProductError').html('Vui lòng chọn sản phẩm để thêm vào phiếu').show();
			return false;
		}else{
			StockCategory.fillTable();			
		}		
		$('#firstNumber').change();
		$('.easyui-dialog').dialog('close');
		return false;
	},
	fillTable:function(){
		var html = '';	
		var stt =1;		
		var arraySortRecordForGrid =  new Array();
		for(var i=0;i<StockCategory._pMap.size();++i){
			var obj = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
			if(obj!=null && obj.isDelete == 0){
				arraySortRecordForGrid.push(obj);
			}
		}
		for(var i=0; i<arraySortRecordForGrid.length;++i){
			var obj = arraySortRecordForGrid[i];
			html+='<tr id="row_'+obj.code+'">';
			html+='<td class="AlignCCols ColsThFirst" style="border-left: 1px solid #C6D5DB;">'+ (stt++) +'</td>';
			html+='<td class="AlignRCols">'+Utils.XSSEncode(obj.stockCardNumber)+'</td>';			
			html+='<td class="AlignLCols">'+Utils.XSSEncode(obj.code)+'</td>';
			html+='<td class="AlignLCols">'+Utils.XSSEncode(obj.name)+'</td>';
			html+='<td class="AlignCCols ColsTdEnd permissionDelete" style="padding: 4px 5px;">';			
			if(StockCategory._status==0 && $('#lsttype').val()!='0'){
				html+='<a href="javascript:void(0)" onclick="return StockCategory.delSelectedRow(\''+Utils.XSSEncode(obj.code)+'\')" >';
				html+='<img src="/resources/images/icon-delete.png"	width="19" height="20" /></a>';
			}	
			html+='</td>';
			html+='</tr>';	
		}
		$('#lstProducts').html(html);		
	},	
	delSelectedRow:function(code){			
		var obj =  StockCategory._pMap.get(code);
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa sản phẩm này', function(r){
			if (r){
				if($('#lstProducts #row_' + obj.code) != null && $('#lstProducts #row_' + obj.code).html()!= null 
						&& $('#lstProducts #row_' + obj.code).html().length > 0){					
					if(obj.isNewSelectProduct==1){						
						var stockCardOfDeletedElement = obj.stockCardNumber;
						for(var i=0;i<StockCategory._pMap.size();++i){
							var itemI = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
							if(itemI!=null && itemI.isDelete == 0 && itemI.stockCardNumber > stockCardOfDeletedElement){
								itemI.stockCardNumber = itemI.stockCardNumber -1;
								StockCategory._pMap.put(StockCategory._pMap.keyArray[i],itemI);
							}
						}
						StockCategory._pMap.remove(code);
					}else{
						obj.isDelete = 1;
						StockCategory._pMap.put(code,obj);
					}	
					if($('#firstNumber').val()==obj.stockCardNumber){
						if(StockCategory._pMap!=null &&  StockCategory._pMap.size()>0){
							for(var i=0;i<StockCategory._pMap.size();++i){
								var itemI = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
								if(itemI!=null && itemI.isDelete != 1){// tuc la isDelete == 0
									var minStockCardAfterDelete = itemI.stockCardNumber;
									$('#firstNumber').val(minStockCardAfterDelete);
									break; //Lay thang dau tien thoi.
								}
							}
						}
					}
					StockCategory.fillTable();
					$('#issStockScrollBody').jScrollPane();
				}
			}
		});			
		return false;
	},
	save:function(){
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày bắt đầu');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('firstNumber','Số thẻ kiểm kê đầu tiên');
		}
		if(msg.length > 0){
			$('#errMsgInfor').html(msg).show();
			return false;
		}
		var data = StockCategory.getParams();
		if($('#lsttype').val()=='1'){
			if(StockCategory._pMap.size()==0){
				$('#errMsg').html('Vui lòng chọn sản phẩm để thêm vào phiếu').show();
				return false;
			}else{
				var lstProduct = new Array(),lstIsDelete = new Array(), lstStockCardNumber = new Array();				 
				for(var i=0;i<StockCategory._pMap.size();i++){
					var obj = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
					lstProduct.push(obj.code);
					lstIsDelete.push(obj.isDelete);
					lstStockCardNumber.push(obj.stockCardNumber);
				}
				data.lstProduct = lstProduct;
				data.lstIsDelete = lstIsDelete;
				data.lstStockCardNumber = lstStockCardNumber;
			}
		}
		if($('#lsttype').val()=='0'){
			if(StockCategory._pMap.size()==0){
				$('#errMsg').html('Không tồn tại sản phẩm trong kho').show();
				return false;
			}else{
				var lstProduct = new Array(),lstIsDelete = new Array(), lstStockCardNumber = new Array();				 
				for(var i=0;i<StockCategory._pMap.size();i++){
					var obj = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
					lstProduct.push(obj.code);
					lstIsDelete.push(obj.isDelete);
					lstStockCardNumber.push(obj.stockCardNumber);
				}
				data.lstProduct = lstProduct;
				data.lstIsDelete = lstIsDelete;
				data.lstStockCardNumber = lstStockCardNumber;
			}
		}
		var error = '';
		Utils.addOrSaveData(data, '/stock/category/addProduct', StockCategory._xhrSave, 'errMsg', function(d){
			if(!d.error){
				$("#succMsg").html('Bạn đã thêm thành công').show();				
				var tm = setTimeout(function(){
					$('#succMsg').html('').hide();
					clearTimeout(tm);					
				}, 3000);
				StockCategory.getResultFromServer($('#cycleCountCode').val().trim());
			}else{
				$('#errMsg').html(d.errMsg).show();
				error = d.errMsg;
			}
		}, 'loading2', '', null, '');
		if(error.length>0){
			$('#errMsg').html(error).show();
		}
		return false;
	},	
	showDialogPrint:function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		$('#displayChooseCycleCountMapProduct').css("visibility", "visible");
		var html = $('#displayChooseCycleCountMapProduct').html();
		$('#CycleCountMapEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 450,
	        height :170,
	        onOpen: function(){	        	
	        	Utils.bindFormatOnTextfield('fffirstNumber', Utils._TF_NUMBER);
				 Utils.bindFormatOnTextfield('lastNumber', Utils._TF_NUMBER);
				 $('.easyui-dialog #errMsg').html('').hide();
				 $('.easyui-dialog #fffirstNumber').val('');
				 $('.easyui-dialog #lastNumber').val('');
	        },
	        onClose : function(){
	        	$('#displayChooseCycleCountMapProduct').html(html);
	        	$('#displayChooseCycleCountMapProduct').css("visibility", "hidden");  	        	
	        }
		});	
	},
	printExecute:function(){	
		$('.easyui-dialog #fffirstNumber').focus();
		$('.ErrorMsgStyle').html('').hide();
		var firstNumber = $('.easyui-dialog #fffirstNumber').val().trim();
		var lastNumber = $('.easyui-dialog #lastNumber').val().trim();
		var cycleCountCode = $('#cycleCountCode').val().trim();
		var vdata = new Object();
		vdata.firstNumber = firstNumber;
		vdata.lastNumber = lastNumber;
		vdata.cycleCountCode = cycleCountCode;
		if(StockCategory.xhrExport != null){
			return false;
		}		
		var msg = '';
		if(firstNumber.length>0 && isNaN(firstNumber)){	
			msg = 'Số thẻ kiểm kho không hợp lệ. Vui lòng nhập lại';
			$('.easyui-dialog #fffirstNumber').focus();
		}
		if(msg.length==0 && lastNumber.length>0 && isNaN(lastNumber) ){
			msg = 'Số thẻ kiểm kho không hợp lệ. Vui lòng nhập lại';
			$('.easyui-dialog #lastNumber').focus();	
		}
		if (msg.length == 0 && firstNumber.length>0 && lastNumber.length>0) {
			if (parseInt(firstNumber) > parseInt(lastNumber)) {
				msg = "Số thẻ kiểm kho từ không được lớn hơn đến!";
				$('.easyui-dialog #fffirstNumber').focus();
			}
		}
		if(firstNumber == "" || lastNumber == ""){
			msg="Chưa nhập vào số thẻ";
		}
		if(msg.length>0){
			$('.easyui-dialog #errMsg').html(msg).show();
			return false;
		}
		$('.easyui-dialog #errMsg').hide();//
		$('#divOverlay').show();		
		StockCategory.xhrExport = $.ajax({
			type : "POST",
			url : "/stock/category/export-stock-category",
			data :vdata,
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				StockCategory.xhrExport = null;
				if(!data.error) {
					window.location.href = data.view;
					setTimeout(function() {
	                    CommonSearch.deleteFileExcelExport(data.view);
					},2000);
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {				
			}
		});	
		$('.easyui-dialog').dialog('close');
		return false;		
	},
	checkFirstNumber:function(){
		var msg = Utils.getMessageOfRequireCheck('firstNumber','Số thẻ kiểm kê đầu tiên');
		var fist = parseInt($('#firstNumber').val().trim());		
		if(isNaN(fist) && msg.length==0){			
			msg = 'Số thẻ kiểm kê đầu tiên không hợp lệ. Vui lòng nhập lại';			
		}
		if(msg.length>0){
			$('#errMsgInfor').html(msg).show();
			$('#firstNumber').focus();
			return false;
		}
		return true;		
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-category.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-counting-approve.js
 */
var StockCountingApprove = {
	_xhrSave : null,
	_isChange:false,
	_ccMapProductsAndResults : null,
	_ccMapProducts : null,
	_myValue : null,
	_mapProductSelect :null,
	_mapProductSelectCheck:null,
	_cycleCountId: 0,
	_cycleCountType:-1,
	clearNewProduct: function() {
		var ccMapKey = StockCountingApprove._ccMapProducts.keyArray;
		var ccMapValue = StockCountingApprove._ccMapProducts.valArray;
		for( var i = 0; i < ccMapKey.length; i++ ) {
			var ccMap =  ccMapValue[i];
			if(ccMap.id== -1){
				var productCode = ccMap.productCode;
				StockCountingApprove._ccMapProducts.remove(productCode);
				StockCountingApprove._ccMapProductsAndResults.remove(productCode);
				--i;
			}
	    }
		return false;
	},
	clearErrorMsg : function(){
		$(".ErrorMsgStyle").hide();
	},
	replaceProduct: function(){
		$('.CycleCountMapProducts').each(function(){
			var productCode = $(this).val();
			var ccMapProductId = $(this).attr('id');
			var ccMap = StockCountingApprove._ccMapProducts.get(productCode);
			if(ccMap != null){
				var checkLot = ccMap.checkLot;
				if(checkLot==0){
					var quantityTxt = StockValidateInput.formatStockQuantity(ccMap.quantity, ccMap.convfact);
					$('#td_qtyCount_' +productCode).val(quantityTxt);
				}else{
					var lstCCResult =StockCountingApprove._ccMapProductsAndResults.get(productCode);
					var quantity = 0;
					if(lstCCResult != null){
						for(var j=0; j < lstCCResult.length ;j++){
							var ccresult = lstCCResult[j];
							quantity += Number(ccresult.quantity);
						}
					}
					var quantityTxt = StockValidateInput.formatStockQuantity(quantity, ccMap.convfact);
					$('#td_qtyCountP_' +productCode).html(quantityTxt).show();
				}
			}
		});
		return false;
	},
	
	openSelectProductDialog: function(){
		$(".ErrorMsgStyle").hide();
		
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		StockCountingApprove._mapProductSelect = new Map();
		StockCountingApprove._mapProductSelectCheck = new Map();

		var html = $('#productDialog').html();
		CommonSearch._arrParams = null;
		$('#productEasyUIDialog').dialog({
			title: "Tìm kiếm sản phẩm",
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        height :600,
	        onOpen: function(){
	        	$('#productEasyUIDialog').addClass("easyui-dialog");
	        	$('#productEasyUIDialog .InputTextStyle').val('');
	    		var params = new Object();
	    		var ccMapKey = StockCountingApprove._ccMapProducts.keyArray;
	    		var ccMapValue = StockCountingApprove._ccMapProducts.valArray;
	    		var lstProduct = new Array();
	    		for( var i = 0; i < ccMapKey.length; i++ ) {
	    			var ccMap = ccMapValue[i];
	    			if(ccMap.id == -1){
	    				lstProduct.push(ccMap.productId);
	    			}
	    	    }
	    		params.lstProductString = lstProduct.join(",");
	        	$('#productEasyUIDialog #productGrid').datagrid({
					url : '/stock/approve/search-product?cycleCountCode=' +$('#cycleCountCode').val().trim()+'&shopId=' +$('#shopId').val().trim(),
					autoRowHeight : true,
					rownumbers : true,					
					pagination:true,
					rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					queryParams:params,
					pageList  : [10,20,30],
					pageNumber:1,
					width : $('#productEasyUIDialog #productGridContainerEx').width(),
				    columns:[[	
				            {field: 'productId', checkbox: true},
							{field: 'productCode',index:'productCode', title: 'Mã sản phẩm', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(v, r, i) {
								if (v) {
									return Utils.XSSEncode(v);
								}
								return "";
							}},
							{field: 'productName',index:'productName', title: 'Tên sản phẩm', width: 320, sortable:false,resizable:false, align: 'left', formatter: function(v, r, i) {
								if (v) {
									return Utils.XSSEncode(v);
								}
								return "";
							}},
							{field: 'cat',index:'cat', title: 'Ngành hàng', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(v, r, i) {
								if (v) {
									return Utils.XSSEncode(v);
								}
								return "";
							}},
							{field: 'subCat',index:'subCat', title: 'Ngành hàng con', width: 120, sortable:false,resizable:false,align: 'left', formatter: function(v, r, i) {
								if (v) {
									return Utils.XSSEncode(v);
								}
								return "";
							}},
							{field: 'availableQuantity',index:'availableQuantity', title: 'Tồn kho',width: 100, sortable:false,resizable:false, align: 'right',formatter: CountingFormatter.quantityConvert},
							{field: 'uom1', index:'uom1', hidden: true},
							{field: 'checkLot', index:'checkLot', hidden: true},
							{field: 'quantity', index:'quantity', hidden: true},
							{field: 'convfact', index:'convfact', hidden: true}
				    ]],
				    onBeforeLoad:function(param){
				    	
				    },
				    onLoadSuccess :function(data){
				    	$('.datagrid-header-rownumber').html('STT');
				    	Utils.bindAutoSearch();
				    	if (data.rows == undefined || data.rows == null) {
				    		return;
				    	}
				    	var length = 0;
				    	var r;
				    	var rows = data.rows;
				    	var obj;
				    	for (var i = 0, sz = rows.length; i < sz; i++) {
				    		r = rows[i];
				    		obj = StockCountingApprove._mapProductSelect.get(r.productCode);
				    		if (obj != undefined && obj != null) {
				    			$('td[field=productId] input[value=' + r.productId + ']').attr("checked", "checked");
				    		}
				    	}
				    	if(data.rows.length==length){
				    		$('.datagrid-header-check input').attr('checked',true);
				    	}else{
				    		$('.datagrid-header-check input').attr('checked',false);
				    	}
				    },
			    onCheck:function(i,r){
						var obj = new Object();
						obj.id = r.productId;
						obj.code = r.productCode;
						obj.name = r.productName;
						obj.uom1 = r.uom1;
						obj.checkLot = r.checkLot;
						obj.convfact = r.convfact;
						obj.quantity = r.quantity;
						var flag = true;
						$('#cycleCountMapProductsTableDetail .CycleCountMapProducts').each(function(){
							var value = $(this).attr('id');
                             if(value != null && value != undefined && value != ""){
             					if(value == r.id){
             						flag = false;
             					}
                             }  
						});
						if(flag){
							StockCountingApprove._mapProductSelect.put(obj.code, obj);
						}
						StockCountingApprove._mapProductSelectCheck.put(obj.code, obj);
					
			    },
			    onUncheck:function(i,r){
			    	StockCountingApprove._mapProductSelect.remove(r.productCode);
		    		StockCountingApprove._mapProductSelectCheck.remove(r.productCode);
			    },
			    onCheckAll:function(r){
			    	for(i=0;i<r.length;i++){
						var obj = new Object();
						obj.id = r[i].productId;
						obj.code = r[i].productCode;
						obj.name = r[i].productName;
						obj.uom1 = r[i].uom1;
						obj.checkLot = r[i].checkLot;
						obj.convfact = r[i].convfact;
						obj.quantity = r[i].quantity;
						var flag = true;
						$('#cycleCountMapProductsTableDetail .CycleCountMapProducts').each(function(){
							var value = $(this).attr('id');
                             if(value != null && value != undefined && value != ""){
             					if(value == r[i].id){
             						flag = false;
             					}
                             }  
						});
						if(flag){
							StockCountingApprove._mapProductSelect.put(obj.code, obj); 
						}
						StockCountingApprove._mapProductSelectCheck.put(obj.code, obj);
			    	}
			    },
			    onUncheckAll:function(r){
			    	for (i = 0; i < r.length; i++) {
			    		StockCountingApprove._mapProductSelect.remove(r[i].productCode);
			    		StockCountingApprove._mapProductSelectCheck.remove(r[i].productCode);
			    	}
			    }
				});
	        	$('#productEasyUIDialog #productCode').unbind('keyup');
	        	$('#productEasyUIDialog #productCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			StockCountingApprove.searchProduct();
	        		}
	        	});
	        	$('#productEasyUIDialog #productName').unbind('keyup');
	        	$('#productEasyUIDialog #productName').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			StockCountingApprove.searchProduct();
	        		}
	        	});
	        },	     
	        onClose : function() {
	        	$('#productEasyUIDialog').dialog("destroy");
	        	$('#productDialog').html(html);
	        }
	     });
	},
	searchProduct: function() {
		$(".ErrorMsgStyle").hide();
		var code = $('#productCode').val().trim();
		var name = $('#productName').val().trim();
		var msg = Utils.getMessageOfSpecialCharactersValidate('productCode', 'Mã sản phẩm', Utils._CODE);
		if(msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('productName', 'Tên sản phẩm', Utils._NAME);
		}
		if(msg.length > 0) {
			$('#addProductError').html(msg).show();
			return;
		}
		var params = {productCode: code, productName: name};
		var ccMapKey = StockCountingApprove._ccMapProducts.keyArray;
		var ccMapValue = StockCountingApprove._ccMapProducts.valArray;
		var lstProduct = new Array();
		for( var i = 0; i < ccMapKey.length; i++ ) {
			var ccMap = ccMapValue[i];
			if(ccMap.id == -1){
				lstProduct.push(ccMap.productId);
			}
	    } 
		params.lstProductString = lstProduct.join(",");
		$("#productGrid").datagrid("load", params);		
		return false;
	},	
	selectListProducts: function() {
		if (StockCountingApprove._mapProductSelectCheck.size() <= 0) {
			$('#addProductError').html('Vui lòng chọn sản phẩm để thêm vào phiếu').show();
			return false;
		} else {
			var cardNumber = 0;
			if ($('#cycleCountMapProductsTableDetail .ColsTd1').length == 0) {
				cardNumber = Number($("#lastNumber").val().trim());
				var tNo = Number($("#firstNumber").val().trim());
				if (cardNumber < tNo - 1) {
					cardNumber = tNo - 1;
				}
			} else {
				var cardNo = $('#cycleCountMapProductsTableDetail .ColsTd1').last().text();
				cardNumber = Number(cardNo.trim());
			}
			var productKeySelect = StockCountingApprove._mapProductSelect.keyArray;
			var productValueSelect = StockCountingApprove._mapProductSelect.valArray;
			for(var i=0; i < productKeySelect.length ;i++){
				var obj = productValueSelect[i];
				var ccMap = {};
				ccMap.id = -1 ;
				ccMap.checkLot = obj.checkLot;
				ccMap.convfact = obj.convfact;
				ccMap.quantity = 0;
				ccMap.productId = obj.id;
				ccMap.productCode = obj.code;
				ccMap.quantityBeforeCount = obj.quantity;
				cardNumber = cardNumber + 1;
				StockCountingApprove.addHTMLProduct(ccMap.id,obj.code,obj.name,obj.uom1,obj.checkLot,obj.id,obj.convfact, ccMap.quantityBeforeCount,cardNumber);
				StockCountingApprove._ccMapProducts.put(obj.code,ccMap);
			}
			Utils.bindFormatOnTextfieldInputCss('BindFormatOnInputCss', Utils._TF_NUMBER_CONVFACT);
			$('.easyui-dialog').dialog('close');
			$('#cycleCountMapProductsTableDetail td').css('padding','2px');
		}		
		return false;
	},
	addHTMLProduct: function(ccMapId,code,name,uom,checkLot,productId,convfact, quantity,cardNumber){
		var html = new Array();
		html.push('<tr id="tr_' + Utils.XSSEncode(code) +'">');
		html.push('<td id="cardNumber" class="ColsTd1 AlignCCols">');
		html.push(cardNumber);
		html.push('<input type="hidden" id="' +Utils.XSSEncode(productId)+'" class="CycleCountMapProducts" value="' +Utils.XSSEncode(code) + '"/>');
		html.push('<input type="hidden" id="lot" value="' +Utils.XSSEncode(checkLot) + '"/>');
		html.push('<input type="hidden" id="convfact" value="' +Utils.XSSEncode(convfact) + '"/>');
		html.push('</td>');
		html.push('<td class="ColsTd2">' +Utils.XSSEncode(code)+'</td>');
		html.push('<td class="ColsTd3">' +Utils.XSSEncode(name) +'</td>');
		html.push('<td class="ColsTd4">' +Utils.XSSEncode(uom)+'</td>');
		html.push('<td class="ColsTd5"></td>');
		html.push('<td class="ColsTd6 AlignRight"><span id="td_qtyBfCountP_' + Utils.XSSEncode(code)+'">' +StockValidateInput.formatStockQuantity(quantity, convfact)+'</span></td>');
		if(checkLot == 0){
			html.push('<td class="ColsTd7">');
			html.push('<input id="td_qtyCount_' + Utils.XSSEncode(code)+'" maxlength="9" value="" onkeypress="return NextAndPrevTextField(event,this,\'CCMapInput\')"  onchange="return StockCountingApprove.changeCCMap(this,' +ccMapId+',\'' +code+'\',' +checkLot+',' +convfact+')"  type="text" class="InputTextStyle InputText1Style AlignRightInput BindFormatOnInputCss CCMapInput" />');
			html.push('</td>');
		}else{
			html.push('<td class="ColsTd7" style="padding-right: 22px; text-align: right !important">');
			html.push('<span id="td_qtyCountP_' +Utils.XSSEncode(code)+'"></span>');
			html.push('</td>');
		}
		html.push('<td class="ColsTd8 AlignRight">');
		if(checkLot==1){
			var param = Utils.XSSEncode(ccMapId) +',' +Utils.XSSEncode(productId) +',\'' +Utils.XSSEncode(code)+'\',\'' + Utils.XSSEncode(name)+'\',' +Utils.XSSEncode(convfact);
			html.push('<a href="javascript:void(0)" onclick="StockCountingApprove.openCCResult(' + param + ')" >Nhập chi tiết</a>');
		}
		html.push('</td>');
		html.push('<td class="ColsTd9 ColsTdEnd" style="text-align: center;">');
		html.push('<a onclick="StockCountingApprove.delCCMapProduct(\'' +Utils.XSSEncode(code) +'\')" href="javascript:void(0)">');
		html.push('<img src="/resources/images/icon_delete.png" width="16" height="16" />')	;					
		html.push('</a>');
		html.push('</td>');		
		html.push('</tr>');
		var check = $('#checkListEmpty').val();
		if(check == 0) {
			$('#cycleCountMapProductsTableDetail').html('');
			$('#checkListEmpty').val(1);
		}
		$('#cycleCountMapProductsTableDetail').append(html.join(""));		
	},
	clearCCResultInput: function(){
		$('.easyui-dialog #d_lot').val('');
		$('.easyui-dialog #d_qtyCount').val('');
	},
	disableAddCCResult : function(){
		$('.easyui-dialog #d_lot').attr('disabled',true);
		$('.easyui-dialog #d_lot').val('');
		$('.easyui-dialog #d_qtyCount').attr('disabled',true);
		$('.easyui-dialog #d_qtyCount').val('');
		$('.easyui-dialog #btnSaveCCResult').attr('disabled',true);
	},
	showTotalQuantityAndScrollBodyCCResult:function(prodCode,convfact){	
		var cycleCountType = $('#h_type').val();	
		var runner = 0;
		$('.easyui-dialog .OrderNumber').each(function(){
			runner+=1;
			$(this).html(runner).show();
		});
		var totalBeforeCount = 0;
		var totalCount = 0;
		var s = StockCountingApprove._ccMapProductsAndResults;
		var temp = StockCountingApprove._ccMapProductsAndResults.get(prodCode);
		if(temp == null || temp == undefined){
			$('.easyui-dialog #totalBeforeCount').html('0/0');
			$('.easyui-dialog #totalCount').html('0/0');
			$(window).resize();
		}else{
			for(var i=0; i < StockCountingApprove._ccMapProductsAndResults.get(prodCode).length ;i++){
				var quantityBeforeCount = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].quantityBeforeCount;
				var quantity = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].quantity;
				if(quantityBeforeCount != null){
					totalBeforeCount += parseInt(quantityBeforeCount);
				}
				if(quantity != null){
					totalCount += parseInt(quantity);
				}
			}
			var value = StockValidateInput.formatStockQuantity(totalBeforeCount, convfact);
			$('.easyui-dialog #totalBeforeCount').html(value);
			value = StockValidateInput.formatStockQuantity(totalCount, convfact);
			$('.easyui-dialog #totalCount').html(value);
			$('.easyui-dialog #cycleCountResultsScroll').jScrollPane();
			Utils.bindFormatOnTextfieldInputCss('BindFormatOnInputCss', Utils._TF_NUMBER_CONVFACT);
			$(window).resize();
		}
	},
	openCCResult: function(cycleCountMapProductId,prodId,prodCode,prodName,convfact) {
		$('#productLotDialog').css("visibility", "visible");
		var html = $('#productLotDialog').html();
		$('#productLotEasyUIDialog').dialog({closed: false,cache: false,modal: true,width : 700,height :450,
			onOpen: function(){
				$('#productLotDialog').html('');
				$('.easyui-dialog #cycleCountResultsTableDetail').html('');
				$('.easyui-dialog #d_productCode').html(Utils.XSSEncode(prodCode));
				$('.easyui-dialog #d_productName').html(Utils.XSSEncode(prodName));
				var cycleCountType = parseInt($('#h_type').val().trim());
				if(cycleCountType!=0 || cycleCountMapProductId != -1){
					StockCountingApprove.disableAddCCResult();
				}else{
					$('.easyui-dialog #d_lot').attr('disabled',false);
					$('.easyui-dialog #d_qtyCount').attr('disabled',false);
					$('.easyui-dialog #btnSaveCCResult').attr('disabled',false); 
				}
				Utils.bindFormatOnTextfield('.easyui-dialog #d_lot',Utils._TF_NUMBER);
				Utils.bindFormatOnTextfield('.easyui-dialog #d_qtyCount',Utils._TF_NUMBER_CONVFACT);
				$('.easyui-dialog #d_productCode').html(Utils.XSSEncode(prodCode));
				$('.easyui-dialog #d_productName').html(Utils.XSSEncode(prodName));
				$('.easyui-dialog #h_ProductCode').val(Utils.XSSEncode(prodCode));
				$('.easyui-dialog #h_Convfact').val(Utils.XSSEncode(convfact));
				$('.easyui-dialog #btnCloseProductLot').click(function(){
					var totalCounted = $('.easyui-dialog #totalCount').text();
					var beforeCount = $('.easyui-dialog #totalBeforeCount').text();
					$('#td_qtyCountP_' +prodCode).html(Utils.XSSEncode(totalCounted));
					$('#td_qtyBfCountP_' +prodCode).html(Utils.XSSEncode(beforeCount));
					var ccMap = {};
					ccMap.id = cycleCountMapProductId;
					ccMap.checkLot = 1;
					ccMap.convfact = convfact;
					ccMap.quantity = StockValidateInput.getQuantity(totalCounted, convfact);
					ccMap.quantityBeforeCount = StockValidateInput.getQuantity(totalBeforeCount, convfact);
					ccMap.productId = prodId;
					ccMap.productCode = prodCode;
					StockCountingApprove._ccMapProducts.put(prodCode,ccMap);
					$('.easyui-dialog').dialog('close');
				});
				if (StockCountingApprove._ccMapProductsAndResults.get(prodCode) != null) {
					/**TH cycleCountMapProduct da duoc sua va duoc luu tru trong _ccMapProductsAndResults */
					if(StockCountingApprove._ccMapProductsAndResults.get(prodCode).length <= 0){
						return false;
					} else {
						for(var i=0; i < StockCountingApprove._ccMapProductsAndResults.get(prodCode).length ;i++){
							var ccResultId = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].id;
							var lot = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].lot;
							var quantityBeforeCountTmp = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].quantityBeforeCount;
							var quantityTmp = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].quantity;
							var check = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].check;
							var	quantityBeforeCount = StockValidateInput.formatStockQuantity(quantityBeforeCountTmp, convfact);
							var quantity =  StockValidateInput.formatStockQuantity(quantityTmp,convfact);
							
							var html = StockCountingApprove.getHTMLCCResult(ccResultId,lot,quantityBeforeCount,quantity,check);
							$('.easyui-dialog #cycleCountResultsTableDetail').append(html);
						}
						StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(prodCode,convfact);
					}
				} else {
					/**TH cycleCountMapProduct chua sua va chua duoc luu tru trong _ccMapProductsAndResults */
					StockCountingApprove._myValue = new Array();
					$.getJSON('/rest/catalog/getCCResult/' +prodId+'/' + cycleCountMapProductId +'/list.json', function(data){	
						if(data.length <=0){
							StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(prodCode,Number(convfact));
							return false;
						} else {
							for (var i= 0 ; i< data.length; i++) {
								var ccresult = {};
								ccresult.id = data[i].id;
								ccresult.quantity = data[i].quantityCounted;
								ccresult.lot = data[i].lot;
								ccresult.quantityBeforeCount = data[i].quantityBeforeCount;
								if(ccresult.id != null) {
									ccresult.check = 0; // 0: is product and lot old
								} else{
									ccresult.check = 1; // 1: is product and lot new
								}
								StockCountingApprove._myValue.push(ccresult);
								var quantity = StockValidateInput.formatStockQuantity(ccresult.quantity , convfact);
								var quantityBeforeCount =StockValidateInput.formatStockQuantity(ccresult.quantityBeforeCount , convfact);
								var html = StockCountingApprove.getHTMLCCResult(ccresult.id, ccresult.lot,quantityBeforeCount,quantity,ccresult.check);
								$('.easyui-dialog #cycleCountResultsTableDetail').append(html);
							}
						}
						if(StockCountingApprove._myValue.length > 0){
							StockCountingApprove._ccMapProductsAndResults.put(prodCode,StockCountingApprove._myValue);
							StockCountingApprove._myValue = null;
						}
						StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(prodCode,Number(convfact));
					});
				}
			},
			onClose : function(){
	        	$('#productLotDialog').html(html);
	        	$('#productLotDialog').css("visibility", "hidden");	 
	        	$('.easyui-dialog #errMsg2').html('').hide();
	        	$('.easyui-dialog #btnCloseProductLot').unbind('click');
	        	$('.easyui-dialog #d_lot').val('');
	        	$('.easyui-dialog #d_qtyCount').val('');
	        }
		});		
		return false;
	},
	changeDLot: function (input){
		var convfact = Number($('.easyui-dialog #h_Convfact').val());
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact);
		$(input).val(quantityTxt);
		return false;
	},
	changeCCResult: function (id, lot, input){
		$('.easyui-dialog #errMsg2').html('').hide();
		var productCode = $('.easyui-dialog #h_ProductCode').val();
		var convfact = Number($('.easyui-dialog #h_Convfact').val());
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact) ;
		var quantity =  StockValidateInput.getQuantity(quantityTxt, convfact);
		
		for (var i=0; i< StockCountingApprove._ccMapProductsAndResults.get(productCode).length; i++) {
			if (StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].id == id && StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].lot == lot)
			{
				var quantityOld  = StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].quantity;
				var quantityOldTxt = StockValidateInput.formatStockQuantity(quantityOld, convfact);
				if(quantity > 9999999) {
					$('.easyui-dialog #errMsg2').html('Số lượng đếm lô ' + Utils.XSSEncode(lot)+' không được vượt quá 9999999').show();
					$(input).val(quantityOldTxt);
					$(input).focus();
					return false;
				}
				$(input).val(quantityTxt);
				StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].quantity = quantity ;
				StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(productCode,Number(convfact));
				break;
			}
		}
		return false;
	},
	changeCCResultBf: function (id, lot, input){
		$('.easyui-dialog #errMsg2').html('').hide();
		var productCode = $('.easyui-dialog #h_ProductCode').val();
		var convfact = Number($('.easyui-dialog #h_Convfact').val());
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact) ;
		var quantity =  StockValidateInput.getQuantity(quantityTxt, convfact);
		
		for (var i=0; i< StockCountingApprove._ccMapProductsAndResults.get(productCode).length; i++) {
			if (StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].id == id && StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].lot == lot)
			{
				var quantityOld  = StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].quantity ;
				var quantityOldTxt = StockValidateInput.formatStockQuantity(quantityOld, convfact);
				if(quantity > 9999999) {
					$('.easyui-dialog #errMsg2').html('Số lượng đếm lô ' + Utils.XSSEncode(lot)+' không được vượt quá 9999999').show();
					$(input).val(quantityOldTxt);
					$(input).focus();
					return false;
				}
				$(input).val(quantityTxt);
				StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].quantityBeforeCount = quantity ;
				StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(productCode,Number(convfact));
				break;
			}
		}
		return false;
	},
	changeCCMap: function (input,ccMapId,productCode,checkLot,convfact){
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact);
		var quantity = StockValidateInput.getQuantity(quantityTxt, convfact);
		$(input).val(quantityTxt);
		var ccMap = StockCountingApprove._ccMapProducts.get(productCode);
		if(ccMap == null){
			var ccMap = {};
			ccMap.id = ccMapId ;
			ccMap.checkLot = checkLot;
			ccMap.convfact = convfact;
			ccMap.quantity = quantity;			
			StockCountingApprove._ccMapProducts.put(productCode,ccMap);
		} else{
			ccMap.quantity = quantity;
		}
		var quantityBeforeCount = StockValidateInput.getQuantity($('#td_qtyBeforeCount' +id).text().trim(),convfact);
		var difference = Number(quantityBeforeCount) - Number(quantity);
		$('#td_qtyDifference' +id).text(StockValidateInput.formatStockQuantity(difference, convfact));
		return false;
	},
	myStockInputFun: function(id,convfact){
		var quantityCounted = StockValidateInput.getQuantity($('#td_qtyCount' +id).val().trim(),convfact);
		$('#td_qtyCount' +id).val(StockValidateInput.formatStockQuantity(quantityCounted, convfact));
		if($('#td_qtyCount' +id).val().trim().length==0){
			$('#td_qtyCount' +id).val('0/0');
			$('#td_chkApprove' +id).attr('checked',false);
		}else{
			$('#td_chkApprove' +id).attr('checked',true);  
		}	
		
		var quantity = StockValidateInput.getQuantity($('#td_qtyBeforeCount' +id).text().trim(),convfact);
		var difference = Number(quantity) - Number(quantityCounted);
		$('#td_qtyDifference' +id).text(StockValidateInput.formatStockQuantity(difference, convfact));
	},
	changeCCMapBf: function (input,ccMapId,productCode,checkLot,convfact){
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact);
		var quantity = StockValidateInput.getQuantity(quantityTxt, convfact);
		$(input).val(quantityTxt);
		var ccMap = StockCountingApprove._ccMapProducts.get(productCode);
		if(ccMap == null){
			var ccMap = {};
			ccMap.id = ccMapId ;
			ccMap.checkLot = checkLot;
			ccMap.convfact = convfact;
			ccMap.quantityBeforeCount = quantity;
			StockCountingApprove._ccMapProducts.put(productCode,ccMap);
		} else {
			ccMap.quantityBeforeCount = quantity;
		}
		return false;
	},
	delCCResult:function(id,lot){
		var productCode = $('.easyui-dialog #h_ProductCode').val().trim();
		$('.easyui-dialog #errMsg2').html('').hide();
		for (var i=0; i< StockCountingApprove._ccMapProductsAndResults.get(productCode).length; i++) {
			if (StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].id == id && StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].lot == lot){
				var msg = 'Bạn có chắc chắn xóa lô ' + Utils.XSSEncode(lot) + ' khỏi đợt kiểm kê này không ?';
				$.messager.confirm('Xác nhận', msg, function(r){
					if (r){
						// Xoa row 
						$('#row_' + lot).replaceWith(''); 
						// Update lai cac gia tri trong Map va tren mang hinh
						StockCountingApprove._ccMapProductsAndResults.get(productCode).splice(i,1);
						var convfact = StockCountingApprove._ccMapProducts.get(productCode).convfact;
						StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(productCode,Number(convfact));
					}
				});
				break;
			}
		}
		return false;
	},
	addCCResult: function() {
		$('.easyui-dialog #errMsg2').hide();
		var lot = $('.easyui-dialog #d_lot').val().trim();
		var my_qtyCount = $('.easyui-dialog #d_qtyCount').val().trim();
		var productCode = $('.easyui-dialog #h_ProductCode').val().trim();
		var convfact = $('.easyui-dialog #h_Convfact').val();
		if(isNullOrEmpty(lot)){
			$('.easyui-dialog #errMsg2').html('Bạn chưa nhập giá trị Số lô. Yêu cầu nhập giá trị.').show();
			return false;
		}
		if(isNullOrEmpty(my_qtyCount)){
			$('.easyui-dialog #errMsg2').html('Bạn chưa nhập giá trị Số lượng. Yêu cầu nhập giá trị.').show();
			return false;
		}
		var checkformat = Utils.getMessageOfInvalidFormatLot('d_lot','Số lô',6);
		if(checkformat.length > 0){
			$('.easyui-dialog #errMsg2').html(checkformat).show();
			return false;
		}
		var checkpoint =false;
		if (StockCountingApprove._ccMapProductsAndResults.get(productCode) != null ) {
			for (var i=0; i < StockCountingApprove._ccMapProductsAndResults.get(productCode).length;i++) {
				// Check ton tai lo nay cua san pham
				if(StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].lot == lot ) {
					checkpoint = true;
					break;
				}
			}
			if(checkpoint) {
				$('.easyui-dialog #errMsg2').html('Số lô đã tồn tại trong danh sách').show();
				return false;
			}
		}
		var msg = 'Bạn có muốn thêm lô ' +Utils.XSSEncode(lot)+' vào đợt kiểm kê này không?';
		var quantity = StockValidateInput.getQuantity(my_qtyCount, convfact);
		if(quantity > 9999999) {
			$('.easyui-dialog #errMsg2').html('Số lượng không được vượt quá 9999999').show();
			$('.easyui-dialog #d_qtyCount').focus();
			return false;
		}
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				var quantityTxt = StockValidateInput.formatStockQuantity(quantity, convfact);
				if (StockCountingApprove._ccMapProductsAndResults.get(productCode) != null ) {
					var ccresult = {};
					ccresult.id = -1;
					ccresult.quantityBeforeCount = 0;
					ccresult.quantity = quantity;
					ccresult.lot = lot;
					ccresult.check = 2 ; // them moi lo
					StockCountingApprove._ccMapProductsAndResults.get(productCode).push(ccresult);
					// Add them row moi vao bang
					var html = StockCountingApprove.getHTMLCCResult(ccresult.id, ccresult.lot,ccresult.quantityBeforeCount, quantityTxt, ccresult.check);
					$('.easyui-dialog #cycleCountResultsTableDetail').append(html);
				} else {
					// SP moi duoc them vao chua co ccResult hoac productlot nao ca
					var ccresult = {};
					ccresult.id = -1;
					ccresult.quantityBeforeCount = 0;
					ccresult.quantity = quantity;
					ccresult.lot = lot;
					ccresult.check = 2 ; // them moi lo
					StockCountingApprove._myValue = new Array();
					StockCountingApprove._myValue.push(ccresult);
					StockCountingApprove._ccMapProductsAndResults.put(productCode,StockCountingApprove._myValue);
					StockCountingApprove._myValue = null;
					// Add them row moi vao bang
					var html = StockCountingApprove.getHTMLCCResult(ccresult.id, ccresult.lot, ccresult.quantityBeforeCount, quantityTxt, ccresult.check);
					$('.easyui-dialog #cycleCountResultsTableDetail').append(html);
				}
				StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(productCode,Number(convfact));
				StockCountingApprove.clearCCResultInput();
			}
		});
	},
	getHTMLCCResult: function(id, lot, qtyBeforeCount, qtyCount, check){
		var cycleCountType = $('#h_type').val();
		var html = new Array();
		html.push('<tr id="row_' + Utils.XSSEncode(lot) +'">');
		html.push('<td class="OrderNumber" style="border-left: 1px solid #C6D5DB;text-align: center;height:24px;"></td>'); //stt
		html.push('<td class="ColsTd2 AlignLeft">' + Utils.XSSEncode(lot) +'</td>');  //solo
		if(qtyBeforeCount==null || qtyBeforeCount == "" || qtyBeforeCount == undefined){
			qtyBeforeCount = '0/0';
		}
		html.push('<td class="ColsTd3 AlignRight">' + Utils.XSSEncode(qtyBeforeCount)  +'</td>');
		if(cycleCountType == 0) {
			html.push('<td class="ColsTd4 AlignRight"><input id="td_qtyCount_CC_' +Utils.XSSEncode(id)+'_' +Utils.XSSEncode(lot)+'" type="text" maxlength="7" size="20" class="InputTextStyle InputStyleStock BindFormatOnInputCss CCResultInput" style="float:right;" value="'
					+qtyCount+ '"onkeydown="return StockCountingApprove.inputText(event);" onkeypress="return NextAndPrevTextField(event,this,\'CCResultInput\')"  onchange="return StockCountingApprove.changeCCResult(' +Utils.XSSEncode(id)+',\'' +Utils.XSSEncode(lot)+'\',this)"/></td>');  //soluong dem
			if(check ==2){
				html.push('<td style="text-align:center;" class="ColsTd5 ColsTdEnd"><a href="javascript:void(0)" onclick="StockCountingApprove.delCCResult(' +Utils.XSSEncode(id)+',\'' +Utils.XSSEncode(lot)+'\');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
			} else{
				html.push('<td style="text-align:center;" class="ColsTd5 ColsTdEnd"></td>');
			}
		} else{
			html.push('<td class="ColsTd4 AlignRight">' + Utils.XSSEncode(qtyCount)  +'</td>');  //soluong dem
			html.push('<td style="text-align:center;" class="ColsTd5 ColsTdEnd"></td>');		
		}
		html.push('</tr>');	
		return html.join("");
	},
	delCCMapProduct :function(productCode){
		var msg = 'Bạn có chắc chắn xóa sản phẩm ' + Utils.XSSEncode(productCode) + ' khỏi đợt kiểm kê không ?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				StockCountingApprove._ccMapProducts.remove(productCode);
				StockCountingApprove._ccMapProductsAndResults.remove(productCode);
				$('#tr_' + productCode).replaceWith('');
			}
		});	
	},
	inputText:function(e){
		var tm = setTimeout(function(){
			if((e.keyCode < 47 || e.keyCode > 57)){
				return true; 
			}
			clearTimeout(tm); 
		}, 300);
	},
	approve: function(cycleCountType){
		$(".ErrorMsgStyle").hide();
		var params = new Object();
		params.cycleCountId = StockCountingApprove._cycleCountId;
		params.cycleCountType = cycleCountType;
		var msg = '';
		if(params.cycleCountType == 1){
			msg = 'Bạn có muốn thực hiện duyệt kiểm kho?';
			Utils.addOrSaveData(params, '/stock/approve/approveStockCount', null,  'errMsg', function(data) {
				StockCountingApprove.searchCycleCount();
			}, null, null, null,msg);
		} else if(params.cycleCountType == 2 ) {
			StockCountingApprove._cycleCountType = cycleCountType;
			msg = 'Bạn có muốn thực hiện hủy kiểm kho?';
			var html = $('#reasonDialog').html();
			$('#reasonDialogEasyUIDialog').dialog({
				title: msg,
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 400,
		        height: 200,
		        onOpen: function(){
		        	$('#txtReason').val('');
		        	$('#reasonDialogEasyUIDialog').addClass("easyui-dialog");
		        },	     
		        onClose : function(){
					$('#reasonDialogEasyUIDialog').dialog('destroy');
					$('#reasonDialog').html(html);
		        }
		     });
		} else if(params.cycleCountType == 3 ) {
			StockCountingApprove._cycleCountType = cycleCountType;
			msg = 'Bạn có muốn thực hiện từ chối kiểm kho?';
			var html = $('#reasonDialog').html();
			$('#reasonDialogEasyUIDialog').dialog({
				title: msg,
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 400,
		        height: 200,
		        onOpen: function(){
		        	$('#txtReason').val('');
		        	$('#reasonDialogEasyUIDialog').addClass("easyui-dialog");
		        },	     
		        onClose : function(){
					$('#reasonDialogEasyUIDialog').dialog('destroy');
					$('#reasonDialog').html(html);
		        }
		     });
		}
		
		return false;
	},
	reject: function(){
		$(".ErrorMsgStyle").hide();
		if($('#txtReason').val().length == 0){
			$('#errMsgReason').html('Bạn chưa nhập lý do.').show();
			return false;
		}
		var params = new Object();
		params.cycleCountId = StockCountingApprove._cycleCountId;
		params.cycleCountType = StockCountingApprove._cycleCountType;
		params.reason = $('#txtReason').val();
		Utils.saveData(params, '/stock/approve/approveStockCount', null,  'errMsg', function(data) {
			$('#reasonDialogEasyUIDialog').dialog('close');
			StockCountingApprove.searchCycleCount();
		}, null, null, null, null);
	},	
	exportExcel: function() {
		var params = new Object();
		params.cycleCountId = StockCountingApprove._cycleCountId;
		var msg = 'Bạn có muốn xuất báo cáo chênh lệch tồn kho của mã kiểm kê này không?';
		$('#errMsg').html('').hide();
		$('#divOverlay').show();
		ReportUtils.exportReport("/stock/approve/exportExcel", params);
		return false;
	},
	searchCycleCountOnDialog:function(){
		$('.ErrorMsgStyle').hide();
		StockCountingInput._isShowDlg = true;
		var html = $('#cycleCountDialog').html();
		$('#cycleCountEasyUIDialog').dialog({
			title: "Tìm kiếm kiểm kê",
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 850,
	        height :600,
	        onOpen: function(){
	        	StockCountingInput.loadComboboxShop("shop", '140');
	        	$('#cycleCountEasyUIDialog').addClass("easyui-dialog");
	        	$('#dgCycleCountType').addClass("MySelectBoxClass");
	        	$('#dgCycleCountType').customStyle();
	        	applyDateTimePicker('#dgCycleCountFromDate');
	        	applyDateTimePicker('#dgCycleCountToDate');
	        	$('#cycleCountEasyUIDialog .InputTextStyle:not(:disabled)').val('');
	        	$('#dgCycleCountType').val("-1").change();
	        	$('#dgCycleCountCode').parent().bind('keyup', function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			StockCountingApprove.searchCycleCountOnEsyUI();
	        		}
	        	});
	        	$('#cycleCountEasyUIDialog #grid').datagrid({
					url : '/stock/approve/search-cycle-count',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					pageNumber:1,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					queryParams:{
						shopCode:$('#shopCode').val().trim(),
						typeInventory:2
					},
					pageList  : [10,20,30],
					width : $('#cycleCountEasyUIDialog #productGridContainer').width(),
				    columns:[[				          
		              	GridUtil.getNormalField("cycleCountCode", "Mã kiểm kê", 70, "left"),
					    GridUtil.getNormalField("description", "Mô tả", 240, "left"),
					    {field: 'status', title: 'Trạng thái', width: 80,sortable:false,resizable:false, align: 'right',formatter : CountingFormatter.statusFormat2},
					    {field: 'createDate',title: 'Ngày bắt đầu kiểm kê',width: 90, sortable:false,resizable:false, align: 'center', formatter: function(v, r, i) {
					    	if (v) {
					    		return $.datepicker.formatDate('dd/mm/yy', new Date(v));
					    	}
					    	return "";
					    }},
					    {field: 'id', index:'id', hidden: true},
					    {field: 'firstNumber', index:'firstNumber', hidden: true},
					    {field: 'select', title : 'Chọn', width : 35, align : 'center', sortable : false, resizable : false,
					    	formatter: function(value, rowObject, opts) {
					    		return "<a href='javascript:void(0)' onclick=\"return StockCountingApprove.selectCycleCount(" + opts +");\">Chọn</a>";
					    	}
					   }
				    ]],
				    onBeforeLoad:function(param){
				    	
				    },
				    onLoadSuccess :function(data){
				    	GridUtil.updateDefaultRowNumWidth('#cycleCountEasyUIDialog', 'grid');
				    	$('.datagrid-header-rownumber').html('STT');
				    	Utils.bindAutoSearch();
				    }
				});  
	        },	     
	        onClose : function(){
	        	$('#cycleCountEasyUIDialog').dialog("destroy");
	        	$('#cycleCountDialog').html(html);
	        	StockCountingInput._isShowDlg = false;
	        }
	     });
	},
	selectCycleCount:function(id){
		StockCountingApprove._cycleCountId = id;
		StockCountingApprove.search();
	},
	changeShop: function() {
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.isSearch = true;
		$.ajax({
			type : "POST",
			url : '/stock/counting/changeshop',
			data : params,
			dataType : "json",
			success : function(data) {
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null){
					$('#cycleCountWareHouse').combobox({
						valueField : 'warehouseId',
						textField : 'warehouseName',
						data : data.lstWarehouseVO,
						panelWidth:206,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
				        onLoadSuccess: function(){
				        	var arr = $('#cycleCountWareHouse').combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$('#cycleCountWareHouse').combobox('select', arr[0].warehouseId);
				        	}
				        }
					});
				 }
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	},
	searchCycleCount:function(){
		$('#saveMessageError').html('');
		$("#cycleCountInfo").html('');
		var	msg = Utils.getMessageOfInvalidFormatDate('dgCycleCountFromDate', 'Từ ngày kiểm kê');
		if(msg.length > 0) {
			$('#saveMessageError').html(msg).show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		var	msg = Utils.getMessageOfInvalidFormatDate('dgCycleCountToDate', 'Đến ngày kiểm kê');
		if(msg.length > 0) {
			$('#saveMessageError').html(msg).show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		var startDate = $('#dgCycleCountFromDate').val().trim();
		var endDate = $('#dgCycleCountToDate').val().trim();
		if (msg.length == 0 && !Utils.compareDate(startDate, endDate)) {
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
			$('#dgCycleCountFromDate').focus();
			$('#saveMessageError').html(msg).show();
			return false;
		}
		var params = new Object();
		params.shopCode = $('#shopCode').val().trim();
		params.cycleCountCode = $('#dgCycleCountCode').val().trim();
		params.cycleCountStatus = $('#dgCycleCountType').val();
		params.startDate = $('#dgCycleCountFromDate').val().trim();
		params.endDate = $('#dgCycleCountToDate').val().trim();
		params.description = $('#dgCycleCountDes').val().trim();
		params.numPeriod = $('#numPeriod').combobox('getValue');
		params.yearPeriod = $('#yearPeriod').val();
		$('#grid').datagrid('load',params);
	},
	iniGridCycleCount:function(yearPeriod, numPeriod){
		var params = new Object();
		params.shopCode = $('#shopCode').val().trim();
		params.numPeriod = numPeriod;
		params.yearPeriod = yearPeriod;
		$('#grid').datagrid({
			url : '/stock/approve/search-cycle-count',
			autoRowHeight : true,
			rownumbers : true, 
			singleSelect: true,
			pagination:true,
			rowNum : 10,
			pageNumber:1,
			scrollbarSize : 0,
			fitColumns:true,
			cache: false, 
			queryParams: params,
			pageList  : [10,20,30],
			width : $('#productGridContainer').width(),
		    columns:[[
				GridUtil.getNormalField("vungshopName", "Vùng", 100, "left"),
				GridUtil.getNormalField("areaShopName", "Khu vực", 100, "left"),
              	GridUtil.getNormalField("shopName", "NPP", 100, "left"),
              	GridUtil.getNormalField("shopPhone", "Điện thoại", 70, "left"),
              	GridUtil.getNormalField("shopAddress", "Địa chỉ", 120, "left"),
			    GridUtil.getNormalField("wareHouseName", "Kho", 100, "left"),
			    GridUtil.getNormalField("cycleCountCode", "Mã kiểm kê", 70, "left"),
			    {field: 'startDate',title: 'Ngày kiểm kê', width: 70, sortable:false, resizable:false, align: 'center', formatter: function(v, r, i) {
			    	if (v) {
			    		return $.datepicker.formatDate('dd/mm/yy', new Date(v));
			    	}
			    	return "";
			    }},
			    GridUtil.getNormalField("createUser", "Người thực hiện", 70, "left"),
			    {field: 'approvedDate',title: 'Ngày duyệt', width: 70, sortable:false, resizable:false, align: 'center', formatter: function(v, r, i) {
			    	if (v) {
			    		return $.datepicker.formatDate('dd/mm/yy', new Date(v));
			    	}
			    	return "";
			    }},
			    GridUtil.getNormalField("approvedUser", "Người duyệt", 70, "left"),
			    {field: 'status', title: 'Trạng thái', width: 80, sortable:false, resizable:false, align: 'left',formatter : CountingFormatter.statusFormat},			    
			    {field: 'id', index:'id', hidden: true},
			    {field: 'select', title : '', width : 35, align : 'center', sortable : false, resizable : false,
			    	formatter:function(value, rowObject, opts){
			    		if(rowObject.status != null && rowObject.status != '5' && rowObject.status != 5){
			    			return "<span style='cursor:pointer' onclick=\"return StockCountingApprove.selectCycleCount('" + rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></span>";
			    		}
			    		
			    	}
			    }
		    ]],
		    onBeforeLoad:function(param){
		    	
		    },
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
		    	Utils.bindAutoSearch();
		    }
		});
	},
	search:function(isSave){
		$(".ErrorMsgStyle").hide();
		var param = new Object();
		param.cycleCountId = StockCountingApprove._cycleCountId;
		if(isSave){
			Utils.getHtmlDataByAjax(param, "/stock/approve/search", function(data){
				try {
					var _data = JSON.parse(data);
					if(_data.error != null && _data.errMsg != null && _data.errMsg != undefined){
						$('#errMsgSearch').html(_data.errMsg).show();
						$("#cycleCountInfo").html('').hide();
						StockCountingApprove._ccMapProductsAndResults = new Map();
						StockCountingApprove._ccMapProducts = new Map();
						StockCountingApprove.mapNewProducts = new Map();
					}
				} catch(e) {
					$("#cycleCountInfo").html(data).show();
					$('#cycleCountCode').focus();
					StockCountingApprove._ccMapProductsAndResults = new Map();
					StockCountingApprove._ccMapProducts = new Map();
					StockCountingApprove.mapNewProducts = new Map();
				}				
			}, 'loadingSearch', 'POST');
		} else {
			var ccMapKey = StockCountingApprove._ccMapProducts.keyArray;
			var ccMapValue = StockCountingApprove._ccMapProducts.valArray;
			var check = false;
			for( var i = 0; i < ccMapKey.length; i++ ) {
				var ccMap =  ccMapValue[i];
				if(ccMap.id== -1){
					check = true;
					break;
				}
		    }
			if(check == true){
				var msg = 'Bạn có có thể sẽ mất những sản phẩm được thêm mới';
				$.messager.confirm('Xác nhận', msg, function(r){
					if (r){
						Utils.getHtmlDataByAjax(param, "/stock/approve/search", function(data){
							try {
								var _data = JSON.parse(data);
								if(_data.error != null && _data.errMsg != null && _data.errMsg != undefined){
									$('#errMsgSearch').html(_data.errMsg).show();
									$("#cycleCountInfo").html('').hide();
									StockCountingApprove._ccMapProductsAndResults = new Map();
									StockCountingApprove._ccMapProducts = new Map();
									StockCountingApprove.mapNewProducts = new Map();
								}
							} catch(e) {
								$("#cycleCountInfo").html(data).show();
								StockCountingApprove.clearNewProduct();
								$('#cycleCountDes').focus();
								StockCountingApprove.replaceProduct();
							}
						}, 'loadingSearch', 'POST');
					}
				});
			} else {
				Utils.getHtmlDataByAjax(param, "/stock/approve/search", function(data){
					try {
						var _data = JSON.parse(data);
						if(_data.error != null && _data.errMsg != null && _data.errMsg != undefined){
							$('#errMsgSearch').html(_data.errMsg).show();
							$("#cycleCountInfo").html('').hide();
							StockCountingApprove._ccMapProductsAndResults = new Map();
							StockCountingApprove._ccMapProducts = new Map();
							StockCountingApprove.mapNewProducts = new Map();
						}
					} catch(e) {
						$("#cycleCountInfo").html(data).show();
						$('#cycleCountDes').focus();
						StockCountingApprove.replaceProduct();	
					}
				}, 'loadingSearch', 'POST');
			}
		}
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-counting-approve.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-counting-input.js
 */
var StockCountingInput = {
	_xhrSave : null,
	_isChange:false,
	_myListId : null,
	_myValue : null,
	_myMap : null,
	_array_old:null,
	convfact:null,
	_first_total_row:null,
	_isShowDlg: false,
	_cycleCountCode: null,
	_shopCode: null,
	/**
	 * Kiem tra san pham them moi ngoai kho kiem ke
	 * @author hunglm16
	 * @return true -> Ton tai, false -> Khong ton tai
	 * @since 14/11/2015
	 */
	checkProductUotStockCountingInput: function () {
		if ($('.isDeleteRowTmp').length > 0) {
			return true;
		}
		return false;
	},
	/**
	 * Xoa san pham them moi tam trong the kho
	 * @author hunglm16
	 * @param productId id San pham
	 * @since 12/11/2015
	 */
	removeProductInsert: function (productId, rowIndex) {
		var arrSTT = $('.isRowIndex');
		for (var stt = rowIndex - 1, size = arrSTT.length; stt < size; stt++) {
		  $(arrSTT[stt]).html(stt);
		}
		$('#tr_' + productId).remove();
	},
	getInfo : function(isShow) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê',Utils._CODE);
		}
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();
			return false;
		}
		var param = new Object();
		param.cycleCountCode = $('#cycleCountCode').val().trim();
		param.shopCode = $('#shopCode').val().trim();
		StockCountingInput._shopCode = $('#cycleCountCode').val().trim();
		StockCountingInput._cycleCountCode = $('#shopCode').val().trim();
		Utils.getHtmlDataByAjax(param, "/stock/input/getinfo", function(data){
			try {
				var _data = JSON.parse(data);
				if(_data.error != null && _data.errMsg != null && _data.errMsg != undefined){
					$('#errMsgSearch').html(_data.errMsg).show();
					$("#infoDiv").html('').hide();
					StockCountingInput.clearMap();
					StockCountingInput._myMap = new Map();
				}
			} catch(e) {
				StockCountingInput.clearMap();
				$("#infoDiv").html(data).show();			
				StockCountingInput._myMap = new Map();
				$('.ProductNotLot').each(function(){
					var textBoxId = $(this).attr('id');
					Utils.bindFormatOnTextfield(textBoxId, Utils._TF_NUMBER_CONVFACT);
				});
			}			
		}, 'loading', 'POST');
		return false;
	},
	checkIsChange:function(cycleCountMapProductId,firsttotalrow){
		var lengthOld = StockCountingInput._array_old.length;
		var lengthNew = StockCountingInput._myMap.get(cycleCountMapProductId).length;
		if(lengthOld!=lengthNew){
			return true;
		}
		var list = StockCountingInput._myMap.get(cycleCountMapProductId);
		for(var i=0;i<lengthNew;++i){
			var objNew = list[i];
			var objectOld = StockCountingInput._array_old[i];
			if(objectOld==null){
				return true;
			}
			if(objNew.quantityCounted!=objectOld.quantityCounted){
				return true;
			}
		}
		var temp = 0;
		$('.fancybox-inner .qtyCount').each(function(){
			++temp;
		});
		if(temp != firsttotalrow){
			return true;
		}
		return false;
	},
	
	viewDetail : function(cycleCountMapProductId,prodId,prodCode,prodName,convfact) {		
		StockCountingInput._array_old = new Array();	
		StockCountingInput.convfact = convfact;	
		$('#productLotDialog').css("visibility", "visible");
		var html = $('#productLotDialog').html();
		var firsttotalrow = 0;
		$('#productLotEasyUIDialog').dialog({closed: false,cache: false,modal: true,width : 700,height :450,
			onOpen: function(){
				$('.easyui-dialog #d_productCode').html(Utils.XSSEncode(prodCode));
				$('.easyui-dialog #d_productName').html(Utils.XSSEncode(prodName));
				$('.easyui-dialog #h_CCMapProductId').val(Utils.XSSEncode(cycleCountMapProductId));
				Utils.bindFormatOnTextfield('quantityCountOfLot', Utils._TF_NUMBER_CONVFACT);
				Utils.bindFormatOnTextfield('productLot', Utils._TF_NUMBER);				
				
				$.getJSON('/rest/catalog/list-product-lot/' +prodId+'/' + cycleCountMapProductId +'/list.json', function(result){
					StockCountingInput._myValue = new Array();
					var MAP_VALUE = StockCountingInput._myMap.get(cycleCountMapProductId);	
					for(var index=0;index<result.length;++index){
						var object = result[index];	
						object.deleted = 0;
						if((MAP_VALUE==null) || (MAP_VALUE!=null && MAP_VALUE.length==0)){
							StockCountingInput._myValue.push(object);
						}								
						StockCountingInput._array_old.push(object);
					}	
					if(StockCountingInput._myValue.length>0){
						StockCountingInput._myMap.put(cycleCountMapProductId,StockCountingInput._myValue);
						StockCountingInput._myValue = null;
					}
					var cc_products_lot = StockCountingInput._myMap.get(cycleCountMapProductId);
					if(cc_products_lot!=null){
						var html = new Array();						
						$('.easyui-dialog #stockCClist').html(html.join(""));
						for(var i=0;i<cc_products_lot.length;++i){
							var object = cc_products_lot[i];
							if(object.deleted==0) {										
								html.push('<tr id="row_' + Utils.XSSEncode(object.lot) +'">');
								html.push('<td class="AlignCCols OrderNumber" style="padding:0px;border-left: 1px solid #C6D5DB;" >' + (index+1) +'</td>'); 
								html.push('<td class="AlignLCols" style="padding:0px" id="ftd_lot_' + Utils.XSSEncode(object.lot) +'">' + Utils.XSSEncode(object.lot) +'</td>');  
								html.push('<td class="AlignRCols qtyBeforeCount" style="padding:0px">' + StockValidateInput.formatStockQuantity(object.quantityBeforeCount, convfact) +'</span></td>'); 
								html.push('<td class="AlignRCols" style="padding: 2px 5px;"><input maxlength="8" onchange="return StockCountingInput.showConfact(this,' +Number(convfact)+');" id="quantityCounted' +Utils.XSSEncode(object.lot)+'"  value="' +StockValidateInput.formatStockQuantity(object.quantityCounted, convfact)+'" class="qtyCount" style="width: 154px;text-align: right;" size="10" type="text"  /></td>');
								if(parseInt(object.quantityBeforeCount, 10)<=0){
									html.push('<td class="AlignCCols" style="padding:0px"><a class="DelCCResult" href="javascript:void(0)" onclick="StockCountingInput.delCCResult(\'' +Utils.XSSEncode(object.lot)+'\');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
								}else{
									html.push('<td ></td>');
								}		
								html.push('</tr>');								
							}							
						}
						$('.easyui-dialog #stockCClist').html(html.join(""));
					}	
					$('.easyui-dialog .qtyCount').each(function(){
						++firsttotalrow;
					});
					StockCountingInput.showTotalQuantityAndScrollBody();
					$('.easyui-dialog #btnCloseProductLot').unbind('click');
					$('.easyui-dialog #btnCloseProductLot').bind('click',function(){
						var newArray = StockCountingInput._myMap.get(cycleCountMapProductId);					
						var array = new Array();
						if(newArray!=null){
							for(var i=0;i<newArray.length;++i){
								var o = newArray[i];
								var object = new Object();
								object.lot = o.lot;
								object.deleted = o.deleted;
								object.quantityBeforeCount = o.quantityBeforeCount;
								var  quantityCounted = $('.easyui-dialog #quantityCounted' +o.lot).val();
								if(quantityCounted!=undefined && quantityCounted.length>0){
									object.quantityCounted = StockValidateInput.getQuantity(quantityCounted.trim(), convfact);							
								}else{
									object.quantityCounted = 0;
								}
								array.push(object);	
							}	
						}									
						StockCountingInput._myMap.put(cycleCountMapProductId,array);
						if(StockCountingInput.checkIsChange(cycleCountMapProductId,firsttotalrow)){//co thay doi
							$('#td_chkApprove' +cycleCountMapProductId).attr('checked',true);
							StockCountingInput.showTotalQuantityAndScrollBody();
							var a = $('.easyui-dialog #totalBeforeCount').html();
							var b = $('.easyui-dialog #totalCount').html();
							var countDate = $('#lstProducts #countDate' +cycleCountMapProductId).html();
							if(countDate.length>0){
								$('#lstProducts #td_qtyBeforeCount' +cycleCountMapProductId).html(StockValidateInput.formatStockQuantity(a, convfact)).show();
							}
							$('#lstProducts #td_qtyCountP' + cycleCountMapProductId).html(StockValidateInput.formatStockQuantity(b, convfact)).show();
						}else{ 					
							StockCountingInput._myValue = new Array();
							StockCountingInput._myMap.put(cycleCountMapProductId,new Array());
						}					
						$('.easyui-dialog').dialog('close');
					});		
				});
			},
			onBeforeClose:function(){
				var newArray = StockCountingInput._myMap.get(cycleCountMapProductId);					
				var array = new Array();
				if(newArray!=null){
					for(var i=0;i<newArray.length;++i){
						var o = newArray[i];
						var object = new Object();
						object.lot = o.lot;
						object.deleted = o.deleted;
						object.quantityBeforeCount = o.quantityBeforeCount;
						var  quantityCounted = $('.easyui-dialog #quantityCounted' +o.lot).val();
						if(quantityCounted!=undefined && quantityCounted.length>0){
							object.quantityCounted = StockValidateInput.getQuantity(quantityCounted.trim(), convfact);							
						}else{
							object.quantityCounted = 0;
						}
						array.push(object);	
					}	
				}									
				StockCountingInput._myMap.put(cycleCountMapProductId,array);
				if(StockCountingInput.checkIsChange(cycleCountMapProductId,firsttotalrow)){//co thay doi
					$('#td_chkApprove' +cycleCountMapProductId).attr('checked',true);
					StockCountingInput.showTotalQuantityAndScrollBody();
					var a = $('.easyui-dialog #totalBeforeCount').html();
					var b = $('.easyui-dialog #totalCount').html();
					var countDate = $('#lstProducts #countDate' +cycleCountMapProductId).html();
					if(countDate.length>0){
						$('#lstProducts #td_qtyBeforeCount' +cycleCountMapProductId).html(StockValidateInput.formatStockQuantity(a, convfact)).show();
					}
					$('#lstProducts #td_qtyCountP' + cycleCountMapProductId).html(StockValidateInput.formatStockQuantity(b, convfact)).show();
				}else{ 					
					StockCountingInput._myValue = new Array();
					StockCountingInput._myMap.put(cycleCountMapProductId,new Array());
				}					
			},
			onClose : function(){
	        	$('#productLotDialog').html(html);
	        	$('#productLotDialog').css("visibility", "hidden");	        	
	        }
		});
		
		return false;
	},
	showTotalQuantityAndScrollBody:function(){		
		var runner = 0;
		$('.easyui-dialog .OrderNumber').each(function(){
			runner+=1;
			$(this).html(runner).show();
		});
		var totalBeforeCount = 0;
		$('.easyui-dialog .qtyBeforeCount').each(function(){
			if($(this).html().trim().length>0){
				totalBeforeCount += StockValidateInput.getQuantity($(this).html().trim(),StockCountingInput.convfact);
			}			
		});
		var totalCount = 0;	
		$('.easyui-dialog .qtyCount').each(function(){			
			if($(this).val().trim().length>0){
				totalCount += StockValidateInput.getQuantity($(this).val(),StockCountingInput.convfact);				
			}
			Utils.bindFormatOnTextfield($(this).attr('id'), Utils._TF_NUMBER_CONVFACT);
		});
		totalBeforeCount = StockValidateInput.formatStockQuantity(totalBeforeCount, StockCountingInput.convfact);
		totalCount = StockValidateInput.formatStockQuantity(totalCount,StockCountingInput.convfact);
		$('.easyui-dialog #totalBeforeCount').html(totalBeforeCount);
		$('.easyui-dialog #totalCount').html(totalCount);
	},
	saveCCResult: function(){
		if(StockCountingInput._xhrSave != null){
			return false;
		}
		var lot = $('.easyui-dialog #productLot').val().trim();
		var my_qtyCount = $('.easyui-dialog #quantityCountOfLot').val().trim();
		$('.easyui-dialog #errMsg1').hide();	
		var msg = Utils.getMessageOfRequireCheck('productLot', 'Số lô');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('quantityCountOfLot', 'Số lượng');
		}	
		if(msg.length==0 && lot.length!=6){	
			msg = 'Số lô phải đủ 6 ký tự theo định dạng ddMMyy. Ví dụ: 011012';
			$('.easyui-dialog #productLot').focus();
		}
		if(msg.length==0){
			var date = new Date();
			var yy = date.getFullYear().toString().substring(0, 2);
			var dd = lot.substring(0, 2);
			var mm = lot.substring(2, 4);
			var happy_new_year = dd.toString() +"/" + mm.toString() + "/" + yy + lot.substring(4);		
			if(!Utils.isDateEx(happy_new_year, '/',Utils._DATE_DD_MM_YYYY)){
				msg = 'Số lô không đúng định dạng ddMMyy. Ví dụ: 011012';
				$('.easyui-dialog #productLot').focus();			
			}	
		}		
		if(msg.length>0){
			$('.easyui-dialog #errMsg1').html(msg).show();			
			$.fancybox.update();
			return false;
		}
		var amount = StockValidateInput.formatStockQuantity(lot, StockCountingInput.convfact);
		my_qtyCount = StockValidateInput.getQuantity(amount,StockCountingInput.convfact);	
		if(my_qtyCount.length==0){
			my_qtyCount = 0;
		}
		var cycleCountMapProductId = $('.easyui-dialog #h_CCMapProductId').val().trim();
		var cycleCountResultId = $('.easyui-dialog #h_CCResultId').val().trim();
		$('.easyui-dialog #errMsg1').hide();	
		var lot = $('.easyui-dialog #productLot').val().trim();
		var my_qtyCount = $('.easyui-dialog #quantityCountOfLot').val().trim();		
		$.messager.confirm('Xác nhận', 'Bạn có chắc chắn muốn thêm mới ?', function(r){
			if(r){
				var object = new Object();	
				object.quantityBeforeCount = 0;
				object.quantityCounted = my_qtyCount;
				object.lot = lot;
				object.deleted = 0;
				var cc_product_lots = new Array();				
				var _cc_product_lots = new Array();
				cc_product_lots = StockCountingInput._myMap.get(cycleCountMapProductId);
				if(cc_product_lots!=null && cc_product_lots.length>0){			
					for(var i=0;i<cc_product_lots.length;++i){						
						if(cc_product_lots[i].lot==object.lot && cc_product_lots[i].deleted==0){
							$('.easyui-dialog #errMsg1').html('Số lô ' + Utils.XSSEncode(object.lot) +' đã tồn tại. Vui lòng nhập giá trị khác').show();
							$.fancybox.update();
							return false;
						}
						var _object = new Object();
						_object.quantityBeforeCount = cc_product_lots[i].quantityBeforeCount;
						_object.quantityCounted = cc_product_lots[i].quantityCounted;
						_object.lot = cc_product_lots[i].lot;
						_object.deleted = cc_product_lots[i].deleted;
						_cc_product_lots.push(_object);
					}					
				}
				_cc_product_lots.push(object);
				StockCountingInput._myMap.put(cycleCountMapProductId,_cc_product_lots);		
				var html = new Array();
				html.push('<tr id="row_' + Utils.XSSEncode(object.lot) +'">');
				html.push('<td class="AlignCCols OrderNumber" style="padding:0px;border-left: 1px solid #C6D5DB;" ></td>'); 
				html.push('<td class="AlignLCols" style="padding:0px" id="ftd_lot_' + Utils.XSSEncode(object.lot) +'">' + Utils.XSSEncode(object.lot) +'</td>');  //solo
				html.push('<td class="AlignRight qtyBeforeCount" style="padding:0px">' + StockValidateInput.formatStockQuantity(object.quantityBeforeCount,StockCountingInput.convfact) +'</span></td>'); 
				html.push('<td class="AlignRCols" style="padding: 2px 5px;"><input maxlength="10" onchange="return StockCountingInput.showConfact(this,' +StockCountingInput.convfact+');" id="quantityCounted' +Utils.XSSEncode(object.lot)+'" value="' +StockValidateInput.formatStockQuantity(object.quantityCounted,StockCountingInput.convfact)+'" class="qtyCount" style="width: 154px;text-align: right;" size="10" type="text" /></td>');
				if(parseInt(object.quantityBeforeCount, 10)<=0){
					html.push('<td class="AlignCCols" style="padding:0px"><a class="DelCCResult" href="javascript:void(0)" onclick="StockCountingInput.delCCResult(\'' +Utils.XSSEncode(object.lot)+'\');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
				}else{
					html.push('<td ></td>');
				}		
				html.push('</tr>');
				$('.easyui-dialog #stockCClist').append(html.join(""));
				StockCountingInput.showTotalQuantityAndScrollBody();		
				$('.easyui-dialog #productLot').val('');		
				$('.easyui-dialog #quantityCountOfLot').val('');
				StockCountingInput._isChange = true;
			}
		});		
	},	
	delCCResult:function(lot){
		$.messager.confirm('Xác nhận', 'Bạn chắc chắn muốn xóa ?', function(r){
			if(r){
				var cycleCountMapProductId = $('.easyui-dialog #h_CCMapProductId').val().trim();
				$('.easyui-dialog #errMsg1').html('').hide();
				$('.easyui-dialog #succMsg1').hide();
				var _rows = new Array();
				var rows = StockCountingInput._myMap.get(cycleCountMapProductId);
				var rowDelete = 0;
				for(var i=0;i<rows.length;++i){
					var _object = new Object();
					if(rows[i].lot==lot){		
						_object.deleted=1;
						rowDelete = 1;
					}else{
						_object.deleted = rows[i].deleted;
					}
					if(rowDelete ==1){
						_object.quantityBeforeCount = 0;
						_object.quantityCounted = 0;
					}else{
						_object.quantityBeforeCount = rows[i].quantityBeforeCount;
						_object.quantityCounted = rows[i].quantityCounted;
					}					
					_object.lot = rows[i].lot;					
					_rows.push(_object);
				}	
				StockCountingInput._myMap.remove(cycleCountMapProductId);
				StockCountingInput._myMap.put(cycleCountMapProductId,_rows);
				$('#row_' + lot).replaceWith('');
				StockCountingInput.showTotalQuantityAndScrollBody();
				$('.easyui-dialog #productLot').val('');
				$('.easyui-dialog #productLot').removeAttr("disabled");
				$('.easyui-dialog #quantityCountOfLot').val('');
				$.fancybox.update();
			}
		});
	},
	clearMap : function() {
		StockCountingInput._myMap = new Map();
	},
	/**
	 * Cap nhat san pham vao the kho
	 * 
	 * @modify hunglm16
	 * @since 13/11/2015
	 */
	importData: function() {		
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var lstCCMapProduct = new Array();
		var lstCCResultDelete = new Array();
		$('.isFlagCurent input:checkbox[name=chkApprove]:checked').each(function() {
			var id = $(this).val();
			var checkLot = $('.isLotProductCurent #lot' +id).val();
			if (checkLot == 0) {
				var amount = $($(this).parent().parent().children()[6]).children().val().trim();
				var convfact = $($(this).parent().parent().children()[6]).children().attr('confact');
				var quantity = StockValidateInput.getQuantity(amount, convfact);
				if (quantity.length == 0) {
					quantity = 0;
				}
				if (parseInt(quantity, 10) < 0) {
					return false;
				}
				var result = '(' + id + ',' + quantity + ')';
				lstCCMapProduct.push(result);
			} else {
				var result = '(' + id + ',-1)';
				var lstCCResult = StockCountingInput._myMap.get(id);
				if (lstCCResult != null && lstCCResult.length > 0) {
					for (var j = 0; j < lstCCResult.length; j++) {
						var ccresult = lstCCResult[j];
						if (parseInt(ccresult.quantityCounted, 10) < 0) {
							return false;
						}
						result += ';(' + ccresult.lot + ',' + ccresult.quantityCounted + ')';
						if (ccresult.deleted == 1) {
							lstCCResultDelete.push(id + ';' + ccresult.lot);
						}
					}
				} else {
					result = '(' + id + ',0)';
				}
				lstCCMapProduct.push(result);
			}
		});
		//Xu ly cho cac san pham them moi
		var arrProductObj = [];
		$('.isFlagInsert input:checkbox[name=chkApprove]:checked').each(function() {
			var id = $(this).val();
			var checkLot = $('.isLotProductInsert #lot' + id).val();
			if (checkLot == 0) {
				var amount = $($(this).parent().parent().children()[6]).children().val().trim();
				var convfact = $($(this).parent().parent().children()[6]).children().attr('confact');
				var quantity = StockValidateInput.getQuantity(amount, convfact);
				if (quantity.length == 0) {
					quantity = 0;
				}
				if (parseInt(quantity, 10) < 0) {
					return false;
				}
				var result = id + ';' + quantity;
				arrProductObj.push(result);
			} else {
				var result = id + ';0';
				arrProductObj.push(result);
			}
		});
		if (lstCCMapProduct.length == 0 && arrProductObj.length == 0) {
			$('#errMsg').html('Vui lòng chọn danh sách sản phẩm trước khi lưu').show();
			return false;
		}
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.lstCCMapProduct = lstCCMapProduct;
		if(lstCCResultDelete.length > 0) {
			params.lstCCResultDelete = lstCCResultDelete;
		}
		params.cycleCountCode = $('#cycleCountCode').val().trim();
		params.productObject = arrProductObj.toString();
		$.ajax({
			type : "POST",
			url : "/stock/input/check-cccounted",
			data : ($.param(params, true)),
			dataType: "json",
			success : function(result) {
				if(result.ccmap_counted>0){
					var msg = 'Danh sách chọn có sản phẩm đã nhập dữ liệu chốt trước đó.';
					msg += '<div class="Clear"></div> <b>Đồng ý</b>: Chốt lại dữ liệu ';
					msg += '<div class="Clear"></div> <b>Hủy bỏ</b>: Không cập nhật lại số lượng kiểm kê';
					$.messager.confirm('Xác nhận', msg, function(r){
						if (r){		
							params.ischeckCountDate = true;
							Utils.saveData(params, '/stock/input/importdata',StockCountingInput._xhrSave,  'errMsg', function(data) {
								StockCountingInput.getInfo(false);	
								$("#successMsg").html("Lưu dữ liệu thành công ").show();	    			
	    						window.setTimeout('$("#successMsg").html("").hide();', 5000);	    						
							}, null, null, null, null, '1');
//						}else{
//							params.ischeckCountDate = false;
//							Utils.saveData(params, '/stock/input/importdata',StockCountingInput._xhrSave,  'errMsg', function(data) {
//								StockCountingInput.getInfo(false);	
//								$("#successMsg").html("Lưu dữ liệu thành công ").show();	    			
//	    						window.setTimeout('$("#successMsg").html("").hide();', 5000);	   								
//							}, null, null, null, null, '1');
						}
					});
				}else{
					Utils.addOrSaveData(params, "/stock/input/importdata", StockCountingInput._xhrSave, 'errMsg', function(result){
						if(!result.error){
							StockCountingInput.getInfo(false);
						}						
					}, null, null, false, 'Bạn có muốn nhập dữ liệu chốt ?');		
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						$('#loading').css('visibility','hidden');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	completeCheckStock: function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();		
		if (StockCountingInput.checkProductUotStockCountingInput()) {
			$('#errMsg').html('Không được Thêm sản phẩm mới khi Hoàn thành kiểm kho').show();
			return;
		}
		$.messager.confirm('Xác nhận', 'Bạn có muốn hoàn thành kiểm kho ?', function(r){
			if (r) {	
				var lstCCMapProduct = new Array();
				var lstCCResultDelete = new Array();
				$('input:checkbox[name=chkApprove]:checked').each(function() {
					var id = $(this).val();
					var checkLot = $('#lot' + id).val();
					if (checkLot == 0) {
						var amount = $('#td_qtyCount' + id).val().trim();
						var convfact = $('#td_qtyCount' + id).attr('confact');
						var quantity = StockValidateInput.getQuantity(amount, convfact);
						if (quantity.length == 0) {
							quantity = 0;
						}
						if (parseInt(quantity, 10) < 0) {
							return false;
						}
						var result = '(' + id + ',' + quantity + ')';
						lstCCMapProduct.push(result);
					} else {
						var result = '(' + id + ',-1)';
						var lstCCResult = StockCountingInput._myMap.get(id);
						if (lstCCResult != null && lstCCResult.length > 0) {
							for (var j = 0, sizej = lstCCResult.length; j < sizej; j++) {
								var ccresult = lstCCResult[j];
								if (parseInt(ccresult.quantityCounted, 10) < 0) {
									return false;
								}
								result += ';(' + ccresult.lot + ',' + ccresult.quantityCounted + ')';
								if (ccresult.deleted == 1) {
									lstCCResultDelete.push(id + ';' + ccresult.lot);
								}
							}
						} else {
							result = '(' + id + ',0)';
						}
						lstCCMapProduct.push(result);
					}
					
				});
				var params = new Object();
				params.shopCode = $('#shopCode').val();
				params.lstCCMapProduct = lstCCMapProduct;
				if(lstCCResultDelete.length > 0) {
					params.lstCCResultDelete = lstCCResultDelete;
				}
				params.cycleCountCode = $('#cycleCountCode').val().trim();		
				$.ajax({
					type : "POST",
					url : "/stock/input/check-cccounted",
					data : ($.param(params, true)),
					dataType: "json",
					success : function(result) {
						if (result.ccmap_counted > 0) {
							var msg = 'Danh sách chọn có sản phẩm đã nhập dữ liệu chốt trước đó.';
							msg += '<div class="Clear"></div> <b>Đồng ý</b>: Chốt lại dữ liệu ';
							msg += '<div class="Clear"></div> <b>Hủy bỏ</b>: Chỉ cập nhật lại số lượng kiểm kê';
							$.messager.confirm('Xác nhận', msg, function(r){
								if (r) {		
									params.ischeckCountDate = true;
									Utils.saveData(params, '/stock/input/completeCheckStock',StockCountingInput._xhrSave,  'errMsg', function(data) {
										StockCountingInput.getInfo(false);	
										$("#successMsg").html("Hoàn thành kiểm kho thành công").show();	    			
										window.setTimeout('$("#successMsg").html("").hide();', 5000);	    						
									}, null, null, null, null,'1');
								} else {
									params.ischeckCountDate = false;
									Utils.saveData(params, '/stock/input/completeCheckStock', StockCountingInput._xhrSave,  'errMsg', function(data) {
										StockCountingInput.getInfo(false);	
										$("#successMsg").html("Hoàn thành kiểm kho thành công").show();	    			
										window.setTimeout('$("#successMsg").html("").hide();', 5000);	   								
									}, null, null, null, null,'1');
								}
							});
						} else {
							Utils.saveData(params, "/stock/input/completeCheckStock", StockCountingInput._xhrSave, 'errMsg', function(result){
								if(!result.error){
									StockCountingInput.getInfo(false);
									$("#successMsg").html("Hoàn thành kiểm kho thành công").show();	    			
									window.setTimeout('$("#successMsg").html("").hide();', 5000);
								}						
							}, null, null, null, null, '1');		
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$.ajax({
							type : "POST",
							url : '/check-session',
							dataType : "json",
							success : function(data) {
								$('#loading').css('visibility','hidden');
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								window.location.href = '/home';
							}
						});
					}
				});
			}
		});
	},

	/**
	 * In file PDF kiem ke
	 * Khong cho hien thi: luu du lieu thanh cong
	 * @author vuongmq
	 * @since 03/09/2015 
	 */
	exportExcel: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.cycleCountCode = $('#cycleCountCode').val().trim();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var cycleType = $('#cycleCountType').val();
		var msgDialog = 'Bạn có muốn xuất phiếu kiểm kê kho này không?';
		if (cycleType == cycleCountType.COMPLETED || cycleType == cycleCountType.WAIT_APPROVED) {
			msgDialog = 'Bạn có muốn xuất báo cáo chênh lệch tồn kho của mã kiểm kê này không?';
		}
		Utils.addOrSaveData(params, '/stock/input/exportexcel', null, 'errMsg', function(data) {
			$('#errMsg').html('').hide();
			var filePath = ReportUtils.buildReportFilePath(data.path);
			window.location.href = filePath;
			setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
                CommonSearch.deleteFileExcelExport(data.path);
			}, 2000);
		}, 'loadingExportandSave', null, null, msgDialog, null, true);
		return false;
	},
	importExcel:function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		$('#isView').val(0);
		$('#cycleCountCodeImp').val($('#cycleCountCode').val());
		$('#shopCodeImp').val($('#shopCode').val());
		
		Utils.importExcelUtils(function(data){
			if (data.message != undefined && data.message != null && data.message.trim().length >0) {
				 var tm = setTimeout(function(){
	           		  $('#errExcelMsg').html(data.message.trim()).change().show();
	                   clearTimeout(tm);
	               }, 1500);
			}else{
				$('#successMsg').html(sale_plan_save_success).show();
				var tm = setTimeout(function(){
					$('#successMsg').html("").hide();
					clearTimeout(tm);
				 }, 5000);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
		return false;
	},	
 	/*viewExcel:function(){
 		$('#errExcelMsg').html('').hide();
		$('#sucExcelMsg').html('').hide();
 		$('#isView').val(1);
 		//Xu ly import 
		var options = { 
 			beforeSubmit: StockCountingInput.beforeImportExcel,   
 	 		success:      StockCountingInput.afterImportExcel,
 	 		type: "POST",
 	 		dataType: 'html'   
		}; 
 		options.data = {
			cycleCountCode:$('#cycleCountCode').val().trim(),
		 	startNumber: $('#startNumber').val().trim(),
		 	endNumber: $('#endNumber').val().trim()
		}; 
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
 		return false;
 	},*/
 	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		//showLoading();
		$('#errExcelMsg').html('').hide();
		$('#successMsg').html('').hide();
		showLoadingIcon();
		return true;
	},
 	afterImportExcel: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		//hideLoading();			
		if (statusText == 'success') {
	    	$("#responseDiv").html(responseText);
	    	
	    	var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		try {
		    		$('#tokenImport').val(newToken);
		    	} catch (e) {}
	    	}
    		if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    		$('#fakefilepc').val('');
				$('#excelFile').val('');
				return ;
	    	}
	    	else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
	    		var numFail = parseInt($('#numFail').html().trim());
	    		var fileNameFail = $('#fileNameFail').html();
	    		var mes = "";
	    		if (numFail != undefined && numFail != null && numFail > 0){
	    			mes += format(msgErr_result_import_excel_fail);
	    			mes+= ' <a href="' + fileNameFail +'">Xem chi tiết lỗi</a>';
	    			$('#errExcelMsg').html(Utils.XSSEncode(mes)).show();
	    		}else{ 
	    			$("#successMsg").html("Import dữ liệu thành công "+Number(totalRow)+" dòng").show();
	    			if($('#isView').val()=='0'){
	    				window.setTimeout('location.reload()', 3000);
	    			}
	    		}
	    	}
	    	$('#fakefilepc').val('');
			$('#excelFile').val('');
	    }	
	},
	showConfact:function(selector,convfact){
		$(selector).val(StockValidateInput.formatStockQuantity($(selector).val(), convfact));
		if($(selector).val().length==0){			
			$(selector).val('0/0');
		}
		StockCountingInput.showTotalQuantityAndScrollBody();
	},
	searchCycleCountOnEsyUI:function(){
		$('#saveMessageError').html('');
		var shopCode = $('#shop').combobox('getValue');
		if (shopCode != null && shopCode.length == 0) {
			$('#saveMessageError').html("Bạn chưa chọn đơn vị").show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		var	msg = Utils.getMessageOfInvalidFormatDate('dgCycleCountFromDate', 'Từ ngày tạo');
		if(msg.length > 0) {
			$('#saveMessageError').html(msg).show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		
		var	msg = Utils.getMessageOfInvalidFormatDate('dgCycleCountToDate', 'Đến ngày tạo');
		if(msg.length > 0) {
			$('#saveMessageError').html(msg).show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		
		var startDate = $('#dgCycleCountFromDate').val().trim();
		var endDate = $('#dgCycleCountToDate').val().trim();
		
		if(msg.length == 0 && !Utils.compareDate(startDate,endDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
			$('#dgCycleCountFromDate').focus();
			$('#saveMessageError').html(msg).show();
			return false;
		}
		var params = new Object();
		params.shopCode = shopCode;
		params.wareHouseId = $('#cycleCountWareHouse').combobox('getValue');
		params.cycleCountCode = $('.easyui-dialog #dgCycleCountCode').val().trim();
		params.cycleCountStatus = $('.easyui-dialog #dgCycleCountType').val();
		params.typeInventory = 2;
		params.startDate = $('.easyui-dialog #dgCycleCountFromDate').val().trim();
		params.endDate = $('.easyui-dialog #dgCycleCountToDate').val().trim();
		params.description = $('.easyui-dialog #dgCycleCountDes').val().trim();
		$('#grid').datagrid('load', params);
	},
	searchCycleCountOnDialog:function(){
		$('.ErrorMsgStyle').hide();
		StockCountingInput._isShowDlg = true;
		var html = $('#cycleCountDialog').html();
		$('#cycleCountEasyUIDialog').dialog({
			title: "Tìm kiếm kiểm kê",
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 850,
	        height :600,
	        onOpen: function(){
	        	StockCountingInput.loadComboboxShop("shop", '140');
	        	$('#cycleCountEasyUIDialog').addClass("easyui-dialog");
	        	$('#dgCycleCountType').addClass("MySelectBoxClass");
	        	$('#dgCycleCountType').customStyle();
	        	applyDateTimePicker('#dgCycleCountFromDate');
	        	applyDateTimePicker('#dgCycleCountToDate');
	        	$('#cycleCountEasyUIDialog .InputTextStyle:not(:disabled)').val('');
	        	$('#dgCycleCountType').val("-1").change();
	        	$('#dgCycleCountCode').parent().bind('keyup', function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			StockCountingApprove.searchCycleCountOnEsyUI();
	        		}
	        	});
	        	$('#cycleCountEasyUIDialog #grid').datagrid({
					url : '/stock/input/search-cycle-count',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					pageNumber:1,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					queryParams:{
						shopCode:$('#shopCode').val().trim(),
						typeInventory:2
					},
					pageList  : [10,20,30],
					width : $('#cycleCountEasyUIDialog #productGridContainer').width(),
				    columns:[[				          
		              	GridUtil.getNormalField("cycleCountCode", "Mã kiểm kê", 70, "left"),
					    GridUtil.getNormalField("description", "Mô tả", 200, "left"),
					    {field: 'status', title: 'Trạng thái', width: 80,sortable:false,resizable:false, align: 'left',formatter : CountingFormatter.statusFormat2},
					    {field: 'createDate',title: 'Ngày bắt đầu kiểm kê',width: 90, sortable:false,resizable:false, align: 'center', formatter: function(v, r, i) {
					    	if (v) {
					    		return $.datepicker.formatDate('dd/mm/yy', new Date(v));
					    	}
					    	return "";
					    }},
					    GridUtil.getNormalField("reason", "Lý do", 180, "left"),
					    {field: 'id', index:'id', hidden: true},
					    {field: 'firstNumber', index:'firstNumber', hidden: true},
					    {field: 'select', title : 'Chọn', width : 35, align : 'center', sortable : false, resizable : false,
					    	formatter:function(value,rowObject,opts){
					    		return "<a href='javascript:void(0)' onclick=\"return StockCountingInput.selectCycleCount("+ opts +");\">Chọn</a>";
					    	}
					   }
				    ]],
				    onBeforeLoad:function(param){
				    	
				    },
				    onLoadSuccess :function(data){
				    	GridUtil.updateDefaultRowNumWidth('#cycleCountEasyUIDialog', 'grid');
				    	$('.datagrid-header-rownumber').html('STT');
				    	Utils.bindAutoSearch();
				    }
				});  
	        },	     
	        onClose : function(){
	        	$('#cycleCountEasyUIDialog').dialog("destroy");
	        	$('#cycleCountDialog').html(html);
	        	StockCountingInput._isShowDlg = false;
	        }
	     });
	},
	selectCycleCount:function(indexGrid){
		var rows = $('#grid').datagrid('getRows')[indexGrid];
		if(rows!=null){
			$('#cycleCountCode').val(rows.cycleCountCode);
			$('.easyui-dialog').dialog('close');			
			$('#cbxShop').combobox('select', rows.shop.shopCode);
			$.getJSON('/rest/catalog/stock/cycleCount/' +rows.cycleCountCode+'/list.json', function(data){
				$('#endNumber').val(data);
				StockCountingInput.getInfo();
			});
		}
	},
	//load combobox don vi
	loadComboboxShop: function(shopIdField, panelWidth) {
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#' + shopIdField).combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: panelWidth,
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null){
			        		$('#shopCode').val(rec.shopCode);
			        		if(StockCountingInput._isShowDlg){
			        			StockCountingInput.changeShop();
			        		}
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#' + shopIdField).combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		var shopCode = $('#shopCode').val();
			        		var isHas = false;
			        		for (var i = 0; i < arr.length; i++) {
			        			if(arr[i].shopCode == shopCode) {
			        				isHas = true;
			        				break;
			        			}
			        		}
			        		if (isHas) {
			        			$('#' + shopIdField).combobox('select', shopCode);
			        		} else {
			        			$('#' + shopIdField).combobox('select', arr[0].shopCode);
			        		}
			        	}
			        }
				});
			}
		});	
	},
	changeShop: function() {
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.isSearch = true;
		
		 $.ajax({
			type : "POST",
			url : '/stock/counting/changeshop',
			data : params,
			dataType : "json",
			success : function(data) {
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null){
					$('#cycleCountWareHouse').combobox({
						valueField : 'warehouseId',
						textField : 'warehouseName',
						data : data.lstWarehouseVO,
						width:140,
						panelWidth:140,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
				        onLoadSuccess: function(){
				        	var arr = $('#cycleCountWareHouse').combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$('#cycleCountWareHouse').combobox('select', arr[0].warehouseId);
				        	}
				        }
					});
				 }
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	}
};


/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-counting-input.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-counting.js
 */
var StockCounting = {
	_xhrSave : null,
	_xhrDel:null,	
	_isSearch: true,
	search: function(){
		$('#errMsg').html('').hide();
		var msg = '';
		var shopCode =  $('#shop').combobox('getValue');
		var fDate = $('#startDate').val().trim();
		var tDate = $('#endDate').val().trim();
		var fromCreated = $('#fromCreated').val().trim();
		var toCreated = $('#toCreated').val().trim();
		if(shopCode.length == 0){
			msg = "Bạn chưa chọn Đơn vị";
		}
		if (msg.length == 0 && fDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày bắt đầu kiểm kê');
		}
		if (msg.length == 0 && tDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày kiểm kê');
		}
		if(msg.length == 0 && fDate.length >0 && tDate.length >0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Ngày bắt đầu kiểm kê không được lớn hơn Đến ngày kiểm kê.';
			$('#fromDate').focus();
		}
		if (msg.length == 0 && fromCreated.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromCreated', 'Ngày tạo');
		}
		if(msg.length == 0 && toCreated.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('toCreated', 'Đến ngày');
		}
		if(msg.length == 0 && fromCreated.length >0 && toCreated.length >0 && !Utils.compareDate(fromCreated, toCreated)){
				msg = 'Ngày tạo không được lớn hơn đến ngày.';
				$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var params = new Object();
		params.shopCode = shopCode;
		params.cycleCountCode = $('#cycleCountCode').val().trim();
		params.cycleCountStatus =  $('#cycleCountStatus').val().trim();	
		params.description = $('#description').val().trim();
		params.wareHouseId = $('#cycleCountWareHouse').combobox('getValue');
		
		params.fromCreated = $('#fromCreated').val().trim();
		params.toCreated = $('#toCreated').val().trim();
		params.startDate = $('#startDate').val().trim();
		params.endDate = $('#endDate').val().trim();	
		if (params.startDate.length > 0 && params.endDate.length > 0 && !Utils.compareDate(params.startDate, params.endDate)) {
			$('#errMsg').html('Từ ngày bắt đầu kiểm kê không được lớn hơn đến ngày kiểm kê.').show();
			$('#startDate').focus();
			return false;
		}
		if (params.fromCreated.length > 0 && params.toCreated.length > 0 && !Utils.compareDate(params.fromCreated, params.toCreated)) {
			$('#errMsg').html('Từ ngày tạo không được lớn hơn đến ngày tạo.').show();
			$('#fromCreated').focus();
			return false;
		}
		$("#grid").datagrid('load',params);
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopCode =  $('#shop').combobox('getValue');
		if(shopCode.length == 0){
			msg = "Bạn chưa chọn Đơn vị";
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('description','Mô tả');
		}
		var description = $('#description').val().trim();
		
		if(msg.length == 0 && description.length>0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả', Utils._NAME);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày bắt đầu kiểm kê');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày bắt đầu kiểm kê');
		}
		var wareHouseId = $('#cycleCountWareHouse').combobox('getValue')
		if(msg.length == 0 && wareHouseId.length == 0){
			msg = "Bạn chưa chọn kho";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopCode = shopCode;
		dataModel.cycleCountId = $('#cycleCountId').val().trim();
		dataModel.cycleCountCode = $('#cycleCountCode').val().trim();		
		dataModel.cycleCountStatus = $('#cycleCountStatus').val().trim();
		dataModel.description = $('#description').val().trim();
		dataModel.startDate = $('#startDate').val().trim();
		dataModel.wareHouseId = wareHouseId;	
		
		Utils.addOrSaveData(dataModel, "/stock/counting/save", StockCounting._xhrSave, 'errMsg', function(result){
			if(!result.error){
				var isChanged = result.isChanged==1? true:false;
				StockCounting.resetForm(isChanged);
				$("#successMsg").html('Cập nhật dữ liệu thành công').show();				
				setTimeout(function(){
					$('#successMsg').html('').hide();
					clearTimeout(this);		
					window.location.href = '/stock/counting/info';
				}, 3000);	
				$('#cycleCountCode').attr('disabled','disabled');
				$('#cycleCountId').val(result.cycleCount.id);
				$('#cycleCountStatus').attr('disabled','disabled');
				$('#cycleCountStatus').parent().addClass('BoxDisSelect');
				$('#btnUpdate').unbind('click');
				$('#btnUpdate').bind('click',StockCounting.update);
			}
		}, 'loading');		
		return false;
	},
	update: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('description','Mô tả');
		}
		var description = $('#description').val().trim();
		if(msg.length == 0 && description.length>0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả', Utils._NAME);
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày bắt đầu kiểm kê');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày bắt đầu kiểm kê');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.cycleCountId = $('#cycleCountId').val().trim();
		dataModel.cycleCountCode = $('#cycleCountCode').val().trim();		
		dataModel.cycleCountStatus = $('#cycleCountStatus').val().trim();
		dataModel.description = $('#description').val().trim();
		dataModel.startDate = $('#startDate').val().trim();	
		dataModel.wareHouseId = $('#cycleCountWareHouse').combobox('getValue');	
		Utils.addOrSaveData(dataModel, "/stock/counting/update",null,'errMsg', function(result){
			if(!result.error){
				var isChanged = result.isChanged==1? true:false;
				StockCounting.resetForm(isChanged);
				$("#successMsg").html('Cập nhật dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsg').html('').hide();
					clearTimeout(tm);					
				}, 3000);		
				$('#cycleCountStatus').attr('disabled','disabled');
				$('#cycleCountStatus').parent().addClass('BoxDisSelect');
			}
		});
		return false;
	},	
	resetForm: function(isChanged){
		if(!isChanged){
			$('#cycleChangedDiv input').attr('disabled','disabled');
			$('#cycleCountStatus').attr('disabled','disabled');
			$('#cycleCountStatus').parent().addClass('BoxDisSelect');
			$('#startDate').next().unbind('click');
			$('#cycleChangedDiv button').hide();
			$('#shop').combobox('disable');		
        	$('#shop').parent().addClass('BoxDisSelect');
        	$('#cycleCountWareHouse').combobox('disable');		
        	$('#cycleCountWareHouse').parent().addClass('BoxDisSelect');
		}
		else{
			
		}
	},
	deleteRow: function(cycleCountId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.cycleCountId = cycleCountId;		
		Utils.deleteSelectRowOnGrid(dataModel, 'phiếu kiểm kê', "/stock/counting/remove", StockCounting._xhrDel, '', '', function(result){
			if(!result.error){
				$('#successMsg').html('Xóa phiếu kiểm kê thành công').show();
				//StockCounting.resetForm();
			}
		}, 'loading2');
		return false;
	},
	changeShop: function() {
		var params = new Object();
		params.shopCode = $('#shop').combobox('getValue');
		params.isSearch = StockCounting._isSearch;
		
		 $.ajax({
			type : "POST",
			url : '/stock/counting/changeshop',
			data : params,
			dataType : "json",
			success : function(data) {
				if (data.lockDay != undefined && data.lockDay != null){
					if (StockCounting._isSearch != undefined && StockCounting._isSearch != null & StockCounting._isSearch) {
						// man hinh search
						$('.date').each(function(){
							var id = $(this).attr('id');
							setDateTimePicker(id);
							$(this).val(data.lockDay);
							$(this).css('width','173px');
						});
					} else {
						// man hinh tao moi
						if(cycleCountId != undefined && cycleCountId > 0){
							// chinh sua
							$('#startDate').val(data.lockDay);
						} else {
							// tao moi
							$('.date').each(function(){
								var id = $(this).attr('id');
								setDateTimePicker(id);
								$(this).val(data.lockDay);
								$(this).css('width','173px');
							});
						}
					}
				}
				
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null){
					$('#cycleCountWareHouse').combobox({
						valueField : 'warehouseId',
						textField : 'warehouseName',
						data : data.lstWarehouseVO,
						width:250,
						panelWidth:206,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
				        onLoadSuccess: function(){
				        	var arr = $('#cycleCountWareHouse').combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		var wareHouseId = $('#wareHouseId').val();
				        		if(wareHouseId != null && wareHouseId > 0){
				        			$('#cycleCountWareHouse').combobox('select', wareHouseId);
				        		} else {
				        			$('#cycleCountWareHouse').combobox('select', arr[0].warehouseId);
				        		}
				        	}
				        	var cycleCountId = $('#cycleCountId').val();
				        	if(cycleCountId != undefined && cycleCountId > 0){
				        		$('#shop').combobox('disable');		
				            	$('#shop').parent().addClass('BoxDisSelect');
				            	$('#cycleCountWareHouse').combobox('disable');		
				            	$('#cycleCountWareHouse').parent().addClass('BoxDisSelect');
				        	}
				        }
					});
				 }
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
//				console.log("Error");
			}
		});
	},
	initGrid: function() {
		$('#grid').datagrid({
			url : "/stock/counting/search",
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $(window).width()-50,
			autoWidth: true,
			queryParams:{
				shopCode: $('#shop').combobox('getValue'),
				cycleCountStatus:0			
			},
		    columns:[[	    
				{field:'createDate', title: 'Ngày tạo',sortable : false, resizable : false, width : 100, align : 'center',
					formatter:function(value,row,index){
						return(row.createDate == null)? '':$.datepicker.formatDate('dd/mm/yy', new Date(row.createDate));	
					}},
				{field:'startDate', title: 'Ngày bắt đầu kiểm kê',sortable : false, resizable : false, width : 100, align : 'center',
					formatter:function(value,row,index){
						return(row.startDate == null)? '':$.datepicker.formatDate('dd/mm/yy', new Date(row.startDate));	
				 }},
				{field:'wareHouseCode', title: 'Mã kho', width: 100, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field:'cycleCountCode', title: 'Mã kiểm kê', width: 100, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field:'description', title: 'Mô tả', width: 200, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field:'status', title: 'Trạng thái',width: 100, sortable:false,resizable:false, align: 'left',formatter:CountingFormatter.statusFormat},
				{field:'edit', title: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false,formatter:CountingFormatter.editFormat},
				{field:'delete', title: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false,formatter:CountingFormatter.deleteFormat},
				{field:'canUpdate',  hidden: true}		    
		    ]],	    
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');	    	
		    	 updateRownumWidthForJqGrid('.easyui-dialog');
	   		 $(window).resize();    		 
		    }
		});
	},
	//load combobox don vi
	loadComboboxShop: function() {
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#shop').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null){
			        		StockCounting.changeShop();
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shop').combobox('getData');
			        	if (arr != null && arr.length > 0){			        		
			        		if (StockCounting._isSearch != undefined && StockCounting._isSearch != null & StockCounting._isSearch) {
			        			$('#shop').combobox('select', arr[0].shopCode);
			        			StockCounting.initGrid();
			        		} else if (StockCounting._isSearch != undefined && StockCounting._isSearch != null & !StockCounting._isSearch) {
			        			var shopCode = $('#shopCode').val();
			        			if (shopCode != null && shopCode.length > 0) {
			        				$('#shop').combobox('select', $('#shopCode').val());			        				
			        			} else {
			        				$('#shop').combobox('select', arr[0].shopCode);
			        			}
			        		}
			        	}
			        }
				});
			}
		});	
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-counting.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-issued.js
 */
var StockIssued = {
	_xhrSave: null,	
	storeProduct:null,
	mapProductLot:null,
	PRODUCT_ID:null,
	CONVFACT:null,
	PRODUCT_TOTAL:null,
	request:null,
	indexEditing:0,
	lstProductExport: new Array(),
	lstProductArray:new Array(),
	lstProductMap: new Map(), /* Danh sach san pham key=productId */
	lstProductCodeMap : new Map(), /* Danh sach san pham key=productCode */
	lstProductCodeShopMap : new Map(),/* DS san pham ton kho npp*/
	gridData:new Map(),
	selectProductId:null,
	exceptProductIdStr:'',
	isAuthorize : true,
	_hasStaffCode : null,
	nameTransDialog: null,
	issuedDateDialog: null,

	openStockOutVanSalePrintDialog :function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#stockOutVanSalePrintDiv').dialog({
			title : "Phiếu xuất kho kiêm vận chuyển nội bộ",
			closed : false,
			onOpen : function() {
				if(StockIssued.nameTransDialog != null){
					$('#ipNameTransDialog').val(StockIssued.nameTransDialog);
				}
				if(StockIssued.issuedDateDialog != null){
					$('#fDateDialog').val(StockIssued.issuedDateDialog);
				}
				$('#ipContentTransDialog').val('Vận chuyển hàng đi bán lưu động');
				$('#ipNameTransDialog').focus();
			},
			onClose:function(){
				
			}
		});
		return false;
	},
	initGrid:function(){
		$('#dg').datagrid({			
			singleSelect: true,			
			fitColumns:true,		
			scrollbarSize:0,		
			width: $(window).width() - 50,
			autoWidth: true,	
			showFooter: true,
			data:StockIssued.lstProductExport,
		    columns:[[	     
		        {field:'rownum', title: 'STT', width: 40, sortable:false,resizable:true , align: 'center',formatter:StockIssuedGridFmt.rownumFmt},
			    {field:'productId', title: 'Mã sản phẩm (F9)', width: 100, sortable:false,resizable:true , align: 'left',		    	
		          	formatter:StockIssuedGridFmt.productIdFmt,	          	
		         	editor: {
		        	 type:'combobox',
		        	 options: {
		        		valueField:'productId',
		        		textField:'productCode',
		        		data:StockIssued.lstProductArray,
		        		width:200,
		        		formatter: function(row) {
		        			 return '<span style="font-weight:bold">' + Utils.XSSEncode(row.productCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.productName) + '</span>';
		        		},
		        		onSelect: function(rec) {	        			
		        			var product = StockIssued.lstProductMap.get(rec.productId);
				    		if(product!=null){
				    			StockIssued.selectProductId = rec.productId;
					    		$('.datagrid-row-editing td[field=productName] div').html(product.productName);
					    		$('.datagrid-row-editing td[field=price] div').html(formatCurrency(product.price));					    		
								$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(product.stockQuantity, product.convfact));					    		
					    		$('.ErrorMsgStyle').html('').hide();	
					    		StockIssued.disabledInputTextFiled(false);
					    		$(this).combobox('setValue',product.productId);
				    		}				    		
		        		},
		        		filter: function(q, row){
						   q = new String(q).toUpperCase();
						   var opts = $(this).combobox('options');
						   return row[opts.textField].indexOf(q)>=0;
						},
						onChange:function(newvalue,oldvalue){
					    	if(newvalue!=undefined && newvalue!=null){
					    		var product = StockIssued.lstProductCodeMap.get(new String(newvalue).toUpperCase());
						    	if(product!=null){
						    		$(this).combobox('setValue',product.productId);
						    	}
					    	}
							$('.combo-panel').each(function(){
								if(!$(this).is(':hidden')){
									$(this).parent().css('width','250px');
									$(this).css('width','248px');
								}			
							});
						}
		        	 }
		         }},
		         {field:'productName', title: 'Tên sản phẩm', width:220,sortable:false,resizable:true , align: 'left',formatter:StockIssuedGridFmt.productNameFmt},
				 {field:'stockQuantity', title: 'Tồn kho', width: 80, align: 'right', sortable:false,resizable:true,formatter: StockIssuedGridFmt.availableQuantityFmt},
				 {field:'lot',title: 'Chọn lô', width: 80, align: 'center',sortable:false,resizable:true,formatter:StockIssuedGridFmt.lotFmt},
				 {field:'quantity', title: 'Số lượng', width: 120, align: 'right',sortable:false,resizable:true,
				  	formatter:StockIssuedGridFmt.quantityFmt,
				   	editor:{type : 'text'}},
				 {field:'price', title: 'Giá bán', width: 100, align: 'right',sortable:false,resizable:true,formatter:StockIssuedGridFmt.priceFmt},
				 {field:'amount', title: 'Thành tiền', width: 100, align: 'right',sortable:false,resizable:true,formatter:StockIssuedGridFmt.amountFmt},
				 {field:'delete', title: 'Xóa', width: 30, align: 'center',sortable:false,resizable:true,formatter:StockIssuedGridFmt.delFmt}
			    ]],	    
		    onLoadSuccess :function(data){		    	
		    },
		    onAfterEdit:function(index,row){	
		    	StockIssued.indexEditing = index;
		    	var productId = row.productId;
		    	var product = StockIssued.lstProductMap.get(productId);
		    	if(product!=null){
		    		row.productCode = product.productCode;
          			row.productName = product.productName;
          			row.stockQuantity = product.stockQuantity;
          			row.grossWeight = product.grossWeight;
          			row.lot = product.checkLot;          			
          			row.price = product.price;          			
          			row.convfact = product.convfact;          			
          			row.quantity = formatQuantityEx(row.quantity,row.convfact);
          			row.quantityValue = getQuantity(row.quantity,row.convfact);
          			if(row.quantityValue==undefined || row.quantityValue==null){
          				row.quantityValue = 0;
          			}
          			if(row.quantityValue<=0){
          				row.quantity = '';
          			}          			
          			row.amount = product.price * row.quantityValue;
          			row.footer = false;
          			$('#dg').datagrid('refreshRow', index);
          			StockIssued.showTotalGrossWeightAndAmount();
		    	}
		    },
		    onSelect:function(index,row){
		    	
		    },
		    onBeforeEdit :function(rowIndex,rowData){
		    	var rows = $('#dg').datagrid('getRows');
		    	for(var i=0;i<rows.length;++i){
		    		if(i==rowIndex)
		    			continue;
		    		$('#dg').datagrid('endEdit',i);
		    	}		    	
		    	$('#dg').datagrid('selectRow', rowIndex);		    	
			},
			onDblClickRow:function(rowIndex, rowData) {
				if(!StockIssued.isAuthorize){
					return false;
				}
				$('.ErrorMsgStyle').html('').hide();
				var msg = Utils.getMessageOfRequireCheck('staffCode', 'Nhân viên', true);
				if(msg.length>0){
					$('#errMsg').html(msg).show();
					return false;
				}
				$('#dg').datagrid('beginEdit', rowIndex);	
				$('.datagrid-row-editing td[field=quantity] input').css('text-align','right');
				var productId = rowData.productId;
				var product = StockIssued.lstProductMap.get(productId);
				if(!isNullOrEmpty(rowData.productId) && product!=null){					
					StockIssued.disabledProductIdCols(rowIndex, product.productCode);
					var color =  StockIssued.addStyleRowSaleProduct(rowIndex,rowData);
					if(color==WEB_COLOR.ORANGE){
						$('#span' +rowIndex).css('color',WEB_COLOR.ORANGE_CODE);
					}else if(color==WEB_COLOR.RED){
						$('#span' +rowIndex).css('color',WEB_COLOR.RED_CODE);
					}else {
						$('#span' +rowIndex).css('color',WEB_COLOR.BLACK_CODE);
					 }					
				}else{
					$('.datagrid-row-editing td[field=productId] input').focus();
				}				
			}
		});
		StockIssued.showTotalGrossWeightAndAmount();
	},
	setEventInput:function(){	
		StockIssued.mapProductLot = new Map();	
		$('#staffCode').focus();
		disabled('btnExportExcel');	
		disabled('btnExportExcelNew');
		//applyDateTimePicker('#issuedDate');
		//$('#issuedDate').val(getCurrentDate());
		$('.MySelectBoxClass').customStyle();	
		$('#staffCode').combobox();
		var data = $('#staffCode').combobox('getData');
		for(var i=0;i<data.length;++i){
			var staff = new Object();
			staff.staffCode = data[i].value;
			staff.staffName = data[i].text;
			StaffUtils.listStaffSale.push(staff);
		}
		$('#staffCode').combobox({valueField : 'staffCode',textField : 'staffCode',panelWidth:206,data : StaffUtils.listStaffSale});
		$('#staffCode').combobox({	 
			filter: function(q, row){
				q = new String(q).toUpperCase();
				var opts = $(this).combobox('options');
				return row[opts.valueField].indexOf(q)>=0;
			},		
			onSelect:function(row){
				$('#dg').datagrid('acceptChanges');
				if(!isNullOrEmpty(row.staffCode)){
					if (StockIssued._hasStaffCode != null) {
						$.messager.confirm('Xác nhận', 'Chọn lại NVBH thì danh sách sẽ bị mất, bạn có muốn chọn lại NVBH không ?', function(r){
							if (r){
								StockIssued._hasStaffCode = row.staffCode;
								StockIssued.synchronization();
								$('#errMsg').html('').hide();
							}else{// lampv-for map product to device
								$('#staffCode').combobox('setValue', StockIssued._hasStaffCode);
							}
						});	
					} else {
						StockIssued._hasStaffCode = row.staffCode;
						StockIssued.synchronization();
						$('#errMsg').html('').hide();
					}
				}
			},
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			onChange:function(){
			}
		});			
		$('.datagrid-row-editing td[field=quantity] input').live('focus', function(e){
			$(this).attr('id','convfact_value' +StockIssued.indexEditing);			
			$(this).attr('maxlength','8');
			Utils.bindFormatOnTextfield('convfact_value' +StockIssued.indexEditing, Utils._TF_NUMBER_CONVFACT);	
			$(this).css('text-align','right');
		});
		$('.datagrid-row-editing td[field=quantity] input').live('keyup', function(e){			
			var convfact_value = $(this).val().trim();			
			if(!isNullOrEmpty(convfact_value)) {	
				var productCode = $('td[field=productId] input.validatebox-text').val().trim().toUpperCase();
				var product = StockIssued.lstProductCodeMap.get(productCode);
				if(product!=null && StockIssued.validateQuantity(convfact_value, product.convfact)){
					var quantity = getQuantity(convfact_value, product.convfact);
					var totalAmount = quantity * product.price;
					$('.datagrid-row-editing td[field=amount] div').html(formatCurrency(totalAmount));										
				}
			}			
			if(e.keyCode==keyCodes.ENTER) {		    	
		    	$('#dg').datagrid('acceptChanges');
		    	StockIssued.appendOrSelectRow();		    	
			}else {
				StockIssued.bindNextAndPrevTextField(e,'quantity');
		    }
		});		
		$('td[field=productId] input.validatebox-text').live('keypress', function(e) {
			var productCode = $(this).val().trim();
			var errType = StockIssued.validateProduct(productCode);
			if(e.keyCode == keyCode_TAB && !e.shiftKey) {		
				if(errType==ERROR.NOT_ERROR){
					var product = StockIssued.lstProductCodeMap.get(productCode);
					StockIssued.selectProductId = product.productId;
		    		$('.datagrid-row-editing td[field=productName] div').html(product.productName);
		    		$('.datagrid-row-editing td[field=price] div').html(formatCurrency(product.price));					    		
					$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(product.stockQuantity, product.convfact));					    		
		    		$('.ErrorMsgStyle').html('').hide();	
		    		StockIssued.disabledInputTextFiled(false);		    		
				}else if(productCode.length>0){					
					var msg =  '';
					if (errType == ERROR.NOT_EXISTS_SALE_CAT) {
						msg = 'không thuộc ngành hàng bán của nhân viên ';
					} else if (errType == ERROR.NOT_EXISTS) {
						msg = 'không tồn tại trong kho đơn vị ';
					}					
					$('#errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(msg) + ' .Vui lòng chọn lại').show();
					StockIssued.selectProductId = null;		
					$('.datagrid-row-editing td[field=productId] input').focus();	
					StockIssued.disabledInputTextFiled(true);
					return false;
				}				
			}else if(e.keyCode == keyCode_F9 && errType==ERROR.NOT_ERROR) {
				StockIssued.showProductEsyUIDialog();		
			}
			if(errType==ERROR.NOT_ERROR){
				StockIssued.disabledInputTextFiled(false);
			}	
			StockIssued.bindNextAndPrevTextField(e,'productId');
		});		
		$('td[field=productId] input.validatebox-text').live('blur',function(){
			var productCode = $(this).val().trim();
			var errType = StockIssued.validateProduct(productCode);
			var msg =  '';
			if(errType==ERROR.NOT_EXISTS_SALE_CAT){
				msg = 'không thuộc ngành hàng bán của nhân viên.';
			}else if(errType==ERROR.IS_EQUIPMENT){
				msg = 'thuộc ngành hàng thiết bị.';
			}else if(errType==ERROR.NOT_EXISTS){
				msg = 'không tồn tại trong kho đơn vị.';
			}
			if(msg.length>0){
				StockIssued.selectProductId = null;
				$('#saleInputErr').html('Mã sản phẩm ' + Utils.XSSEncode(msg) + ' Vui lòng chọn lại').show();
				$('.datagrid-row-editing td[field=productId] input').focus();
				StockIssued.disabledInputTextFiled(true);
				return false;
			}else{
				StockIssued.disabledInputTextFiled(false);	
			}				
		});
	},
	appendOrSelectRow:function(){
		var editIndex = $('#dg').datagrid('getRows').length;			
    	if(StockIssued.indexEditing == editIndex-1) {    		
    		var row = $('#dg').datagrid('getRows')[editIndex-1];
    		var product = StockIssued.lstProductMap.get(row.productId);
    		if(row==null || isNullOrEmpty(row.productId)){  
    			editIndex = editIndex-1;
    		}else if(!isNullOrEmpty(row.productId)){    			
    			if(product!=null){
    				if(isNullOrEmpty(row.quantity) || StockIssued.validateQuantity(row.quantity,product.convfact)){
    					$('#dg').datagrid('appendRow',new Object());
    				}   				
    			}
    		}	
    	}else{
    		var row = $('#dg').datagrid('getRows')[StockIssued.indexEditing];
    		var product = StockIssued.lstProductMap.get(row.productId);
    		editIndex = StockIssued.indexEditing +1;
    	}    	
    	StockIssued.indexEditing = editIndex;
    	$('#dg').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
    	var productCode = $('.datagrid-row-editing td[field=productId] input.validatebox-text').val();
    	if(!isNullOrEmpty(productCode)){    		
    		StockIssued.disabledProductIdCols(editIndex, productCode);
    	}else{
    		$('.datagrid-row-editing td[field=productId] input').focus();
    	}     	
	},
	bindNextAndPrevTextField:function(e,clazz){
		var editIndexRow = StockIssued.indexEditing;
		var len = $('#dg').datagrid('getRows').length;
		var index = editIndexRow;	
		var shiftKey = e.shiftKey && e.keyCode == keyCodes.TAB;	
		if(e.keyCode == keyCodes.ARROW_DOWN){	
			++index;			
		}else if(e.keyCode == keyCodes.ARROW_UP){			
			--index;							
		}else if(shiftKey){
			var product = $('#dg').datagrid('getRows')[editIndexRow];
			if(product!=null && !isNullOrEmpty(product.productId) && StockIssued.lstProductMap.get(product.productId)!=null){
				--index;	
			}else if(clazz=='productId'){
				--index;
			}
		}	
		if(index>-1 && index<len && index!=editIndexRow){			
			$('#dg').datagrid('beginEdit', index);	
			var productCode = $('.datagrid-row-editing td[field=productId] input.validatebox-text').val();
			StockIssued.indexEditing = index;
			var product = $('#dg').datagrid('getRows')[index];
			if(product!=null && !isNullOrEmpty(product.productId) && StockIssued.lstProductMap.get(product.productId)!=null){
				StockIssued.disabledProductIdCols(index, productCode);
				if(clazz=='productId'){
					if(shiftKey){
						$('.datagrid-row-editing td[field=quantity] input').focus();
					}
				}else if(clazz=='quantity'){
					$('.datagrid-row-editing td[field=quantity] input').focus();
				}		
			}else{	
				$('td[field=productId] input.validatebox-text').attr('disabled',false);
				$('td[field=productId] input.validatebox-text').focus();
			}
		}else if(shiftKey && clazz=='quantity'){
			$('td[field=productId] input.validatebox-text').focus();			
		}
	},
	disabledProductIdCols:function(editIndex,productCode){		
		$('#span' +editIndex).remove();			
		$('td[field=productId] div table tbody tr td input').css('display','none');
		$('td[field=productId] div table tbody tr td span').css('display','none');
		$('td[field=productId] div table tbody tr td').append('<span id="span' +editIndex+'"></span');
		$('#span' +editIndex).html(Utils.XSSEncode(productCode));		
		$('.datagrid-row-editing td[field=quantity] input').focus();
	},
	disabledInputTextFiled:function(flag){
		if(flag){
			disabled('btnStockOut');
			$('.datagrid-row-editing td[field=quantity] input').attr('disabled','disabled');			
		}else{
			$('.datagrid-row-editing td[field=quantity] input').attr('disabled',false);    		
			enable('btnStockOut');	
		}		
	},
	synchronization:function(byShop){
		StockIssued.lstProductExport = new Array();
		var url = '';
		if(byShop!=undefined && byShop){
			url = '/stock/issued/search-product?byShop=true';
		}else {
			var vansaleCode = $('#staffCode').combobox('getValue').trim();
			url = '/stock/issued/search-product?isF9=false&vansaleCode=' + vansaleCode;
		}		
		StockIssued.lstProductExport.push(new Object());		
		$.getJSON(url, function(data) {
			StockIssued.lstProductMap = new Map();
			StockIssued.lstProductCodeMap = new Map();
			if(data.error != undefined && data.error != null && data.error == true){
				$('#errMsg').html(data.errMsg).show();
				StockIssued.lstProductArray = null;
				StockIssued.initGrid();
				disabled('btnStockOut');
			}else{
				enable('btnStockOut');
				var len = 0;
				if(byShop!=undefined && byShop){
					var stockTotalArray = data.stock_total;
					len = data.stock_total.length;				
					for(var i = 0; i < len; i++) {
						obj = stockTotalArray[i];
						StockIssued.lstProductCodeShopMap.put(obj.product.productCode, obj); /*Danh sach san pham theo nganh hang*/
					}
					StockIssued.initGrid();
				}else{
					StockIssued.lstProductArray = data.rows;				
					len = StockIssued.lstProductArray.length;
					for(var i = 0; i < len; i++) {
						var obj = StockIssued.lstProductArray[i];
						StockIssued.lstProductMap.put(obj.productId, obj); /*Danh sach san pham theo nganh hang*/
						StockIssued.lstProductCodeMap.put(obj.productCode,obj); /*Danh sach san pham */
					}			
					StockIssued.initGrid();
					$('#dg').datagrid('beginEdit', 0);
				}
			}
		});
	},
	validateProduct:function(productCode){		
		if(isNullOrEmpty(productCode)){			
			return ERROR.NOT_ERROR;
		}
		productCode = productCode.trim().toUpperCase();		
		product = StockIssued.lstProductCodeShopMap.get(productCode);		
		if(product==null){
			return ERROR.NOT_EXISTS;
		}		
		product = StockIssued.lstProductCodeMap.get(productCode);
		if(product==null){			
			return ERROR.NOT_EXISTS_SALE_CAT;
		}		
		return ERROR.NOT_ERROR;
	},
	validateQuantity:function(value,convfact){
		if(isNullOrEmpty(value) || isNullOrEmpty(convfact)){
			return false;
		}
		var quantity = getQuantity(value,convfact);
		if(isNaN(quantity)){
			return false;
		}
		return true;
	},
	showProductEsyUIDialog:function(_dgIndex){
		$('#issSearchProductDialog').unbind('click');
		StockIssued.storeProduct = new Map();
		$('#productDialog').css("visibility", "visible");
		var html = $('#productDialog').html();		
		$('.SelProduct').each(function(){
			$(this).val('');
		 });			
		var msg = Utils.getMessageOfRequireCheck('staffCode', 'Mã nhân viên',true);	
		if(msg.length>0){
			return false;
		}		
		$('#productEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        height :550,
	        onOpen: function(){
	        	$('.easyui-dialog #errMsg').html('').hide();
	        	$('.easyui-dialog #issSearchProductDialog').css('margin-left', 285);
				$('.easyui-dialog #issSearchProductDialog').css('margin-top', 10);	
				$('.easyui-dialog #productCode').val('');
				$('.easyui-dialog #productName').val('');
				Utils.bindAutoSearch();
				$('#issSearchProductDialog').bind('click',StockIssued.searchProduct);
	        	$('.easyui-dialog #productGrid').datagrid({
					url : '/stock/issued/search-product',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					pageList  : [10,20,30],
					queryParams:{
						isF9 : true,
						vansaleCode : $('#staffCode').combobox('getValue').trim(),
						exceptProductIdStr:StockIssued.exceptProductIdStr
					},
					width : ($('.easyui-dialog #productGridContainer').width()),
				    columns:[[				          
				        {field: 'productCode',title: 'Mã sản phẩm', width: 100, sortable:false,resizable:false,align: 'left' },
					    {field: 'productName',title: 'Tên sản phẩm',  width: 380,sortable:false,resizable:false, align: 'left' },
					    {field: 'stockQuantity', title: 'Tồn kho', width: 80,sortable:false,resizable:false, align: 'right', 
					    	formatter: function(value,row,index){
					    		return formatQuantityEx(row.stockQuantity,row.convfact);
					    	}},
					    {field: 'price', title: 'Giá',width: 70, sortable:false,resizable:false, align: 'right', formatter: GridFormatterUtils.currencyCellFormatter},
					    {field: 'amt', title: 'Số lượng',width: 100, sortable:false,resizable:false,align: 'right',formatter:  function(value,row,index){
					    	return '<input index=' +index+' row-id="' +row.id+'" id="productRow_'	+ row.id
					    	+'" size="5" type="text" maxlength="8" style="text-align:right;margin: 0;width:77px;" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockIssued.amountBlured('
					    	+ index +');" onchange="StockIssued.amountChanged(' + index +'); StockIssued.formatQuantity(' +row.id+',' +row.convfact+');">';
				    	}}					    
				    ]],
				    onBeforeLoad:function(param){
				    	$('.easyui-dialog .SelProduct').each(function(){
							  var index = $(this).attr('index');
							  var obj = StockIssued.getData(index,$(this));										 
							  if(obj!=null){
								  StockIssued.storeProduct.put(obj.productId,obj);  
							  }else {
								  var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
								  var temp = StockIssued.storeProduct.get(rows.productId);
								  if(temp!=null){
									  StockIssued.storeProduct.remove(temp.productId);
								  }
							  }									 
						  });	
				    },
				    onLoadSuccess :function(data){   	 
				    	$('.datagrid-header-rownumber').html('STT');				    		
				    	Utils.bindFormatOnTextfieldInputCss('SelProduct',Utils._TF_NUMBER_CONVFACT);
						$('.easyui-dialog .SelProduct').each(function(){
						  var productId = $(this).attr('row-id');							 								  
						  var obj = StockIssued.storeProduct.get(productId);
						  if(obj!=null){
							  $(this).val(obj.quantity);
						  }else{
							  $(this).val('');
						  }
						});			
				    }
				});  
	        },	        
	        onClose : function(){
	        	$('#productDialog').html(html);
	        	$('#productDialog').css("visibility", "hidden");
	        	$('.easyui-dialog #errMsg').html('').hide();
	        	$('#dg').datagrid('reload');
	        	$('.AmountProduct');
	        	Utils.bindFormatOnTextfieldInputCss('AmountProduct', Utils._TF_NUMBER_CONVFACT);
	        }
	     });
	},	
	searchProduct: function(){			
		var code = encodeChar($('.easyui-dialog #productCode').val().trim());
//		var name = encodeChar($('.easyui-dialog #productName').val().trim());
		var name = $('.easyui-dialog #productName').val().trim();
		var vansaleCode = $('#staffCode').combobox('getValue').trim();
		$('.easyui-dialog #productGrid').datagrid('load',{productCode :code, productName: name,isF9:true,
			vansaleCode:vansaleCode,exceptProductIdStr:StockIssued.exceptProductIdStr});
		return false;
	},	
	showTotalGrossWeightAndAmount:function(){
		var totalGrossWeight = 0;
		var totalAmount = 0;
		var temp = 0;		
		$('#dg').datagrid('acceptChanges');		
		var PRODUCTS = $('#dg').datagrid('getRows');
		len = PRODUCTS.length;	
		var exceptProductIdArr = new Array();
		for(var i=0;i<len;++i){
			var product = PRODUCTS[i];
			if(product==null || isNaN(product.productId)){
				continue;
			}else if(isNullOrEmpty(product.quantity)){
				continue;
			}else if(isNullOrEmpty(product.convfact) || isNaN(product.convfact)){
				continue;
			}else if(isNullOrEmpty(product.price) || isNaN(product.price)){
				continue;
			}else if(isNullOrEmpty(product.grossWeight) || isNaN(product.grossWeight)){
				continue;
			}
			temp = getQuantity(product.quantity,product.convfact);
			totalGrossWeight += Number(temp*product.grossWeight);
			totalAmount += Number(temp * product.price);
			var color =  StockIssued.addStyleRowSaleProduct(i,product);
			if(color==WEB_COLOR.ORANGE){
				$('span.spStyle' +i).each(function(){ $(this).css('color',WEB_COLOR.ORANGE_CODE);});
			}else if(color==WEB_COLOR.RED){
				$('span.spStyle' +i).each(function(){ $(this).css('color',WEB_COLOR.RED_CODE);});
			}else {
				  $('span.spStyle' +i).each(function(){ $(this).css('color',WEB_COLOR.BLACK_CODE);});
			 }
			exceptProductIdArr.push(product.productId);
		}	
		StockIssued.exceptProductIdStr = exceptProductIdArr.toString();
		var footers = new Array();
		var ft = new Object();
		ft.productName = '<b id="footerProductName">Tổng</b>';
		ft.quantity = '<b>' + formatFloatValue(totalGrossWeight,2) + ' kg</b>';
		ft.amount = '<b>' + formatFloatValue(totalAmount,2) + ' VNĐ</b>';
		ft.footer = true;
		footers.push(ft);
		$('#dg').datagrid('reloadFooter',footers);
		$('#footerProductName').parent().css('text-align','center');
	},
	addStyleRowSaleProduct:function(i,product){
		var color = WEB_COLOR.BLACK;
		if(product==null){
			return color;
		}else{
			var stockQuantity = product.stockQuantity;
			var convfact = product.convfact;			
			if(stockQuantity==null || convfact==null){
				return color;
			}
			if(Number(stockQuantity)<0){
				return WEB_COLOR.RED;
			}
			var quantityValue = product.quantityValue;
			if(isNullOrEmpty(product.quantityValue)){
				return color;
			}			
			if(Number(stockQuantity)<Number(quantityValue)){
				return WEB_COLOR.ORANGE;
			}
		}		
		return color;
	},
	getData:function(index,selector){		
		var product = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];	
		var quantity = selector.val().trim();
		if(quantity.length==0){			
			return null;
		}
		var row = new Object();
		row.productId = product.productId;
		row.productCode = product.productCode;
		row.productName = product.productName;
		row.stockQuantity = product.stockQuantity;
		row.grossWeight = product.grossWeight;
		row.lot = product.checkLot;
		row.price = product.price;
		row.convfact = product.convfact;
		row.quantity = formatQuantityEx(quantity, row.convfact);
		row.quantityValue = getQuantity(row.quantity, row.convfact);
		if(isNaN(row.quantityValue)){
			return null;
		}		
		if (row.quantityValue <= 0) {
			row.quantity = '';
		}		
		row.amount = product.price * row.quantityValue;
		row.footer = false;
		return row;
	},	
	selectListProducts: function(){
		var mapProductSelection = new Map();
		$('.easyui-dialog #errMsg').html('').hide();
		var flag = true;
		var index = 0;
		$('.easyui-dialog .SelProduct').each(function(){
			var index = $(this).attr('index');
			if($(this).val().length>0){				
				var obj = StockIssued.getData(index,$(this));
				if(obj!=null){
					index = obj.productId;
					if(obj.quantity == '0/0' ||obj.quantity == '0'|| obj.quantity==''){										
						$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();
						flag = false;
						return false;
					}						
					if(obj.stockQuantity < obj.quantityValue){
						$('.easyui-dialog #errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(obj.productCode + ' - ' + obj.productName) + '. Vượt quá giá trị tồn kho').show();
						flag = false;						
						return false;			
					} 				
					mapProductSelection.put(obj.productId, obj);
					var storeObject = StockIssued.storeProduct.get(obj.productId);
					if(storeObject!=null){
						StockIssued.storeProduct.remove(obj.productId);
					}
				}				
			}else{
				var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
				var temp = StockIssued.storeProduct.get(rows.productId);
				if(temp!=null){
				  StockIssued.storeProduct.remove(temp.productId);
				}
			}
		});
		if(!flag){	
			 $('#productRow_' + index).focus();
			return false;
		}
		for(var i=0;i<StockIssued.storeProduct.size();i++){
			var product = StockIssued.storeProduct.get(StockIssued.storeProduct.keyArray[i]); 
			mapProductSelection.put(product.productId, product);
		}
		if(mapProductSelection.size()==0){
			$('.easyui-dialog #errMsg').html('Vui lòng chọn sản phẩm cần xuất kho.').show();
			return false;
		}	
		for(var i=0;i<mapProductSelection.size();i++){
			var data = mapProductSelection.get(mapProductSelection.keyArray[i]); 
			var len = $('#dg').datagrid('getRows').length-1;
			$('#dg').datagrid('insertRow',{index:len, row:data});
		}		
		StockIssued.showTotalGrossWeightAndAmount();
		$('.easyui-dialog').dialog('close');
	},
	duplicateProducts:function(){
		var arrDelete = new Array();
		var len = $('#dg').datagrid('getRows').length;
		var mapTestingDuplicate = new Map();
		var mapDuplicateIndex = new Map();
		for(var i=0;i<len;++i){			
			var product = $('#dg').datagrid('getRows')[i];
			var productId = product.productId;
			if(mapTestingDuplicate.get(productId)==null){
				mapTestingDuplicate.put(productId,product);		
				mapDuplicateIndex.put(productId,i);
			}else{
				var obj = new Object();
				obj.firstIndex = mapDuplicateIndex.get(productId);
				obj.lastIndex = i;
				arrDelete.push(obj);
			}			
		}
		return arrDelete;
	},
	amountChanged: function(rowId){		
		$('.easyui-dialog #errMsg').html('').hide();
		if(!StockIssued.amountBlured(rowId)){			
			$('#errCont').show();	
		}else{
			$('.easyui-dialog #errMsg').html('').hide();	
		}
	},
	amountBlured: function(index){	
		var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
		var rowId = rows.id;
		var fAmount = rows.quantity;
		var convfact = rows.convfact;
		var productCode = rows.productCode;
		var productName = rows.productName;
		var amount = $('#productRow_' + rowId).val().trim();	
		if(amount.length==0){			
			return true;
		}
		var format = formatQuantity(amount,convfact);
		if(format.length==0){
			$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();			
			return false;
		}else{
			amount = format;
			if(amount == '0/0' ||amount == '0'|| amount==''){
				$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();
				setTimeout(function() {	
					$('#productRow_' + rowId).focus();					
				}, 50);				
				return false;
			}
		}
		var fTotal = parseInt(getQuantity(fAmount, convfact));
		var total = parseInt(getQuantity(amount, convfact));		
		if(total.length>6){
			$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();	
			return false;
		}
		if(fTotal < total){
			$('.easyui-dialog #errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(productCode + ' - ' + productName) + '. Vượt quá giá trị tồn kho').show();					
			setTimeout(function() {	
				$('#productRow_' + rowId).focus();					
			}, 50);				
			return false;		
		} else {			
			return true;
		}
	},
	delSelectedRow:function(index){	
		$('#errExcelMsg').html('').hide();		
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa sản phẩm này ?', function(ret){
			if (ret == true) {
				$('#dg').datagrid('deleteRow',index);				
				var rows = $('#dg').datagrid ('getRows');
				if(rows.length == 0){
					$('#dg').datagrid('appendRow',new Object());
				}
				$('#dg').datagrid('loadData',rows);
				StockIssued.showTotalGrossWeightAndAmount();
			}
		});
	},	
	stockOutVanSale:function(){
		if(!StockIssued.isAuthorize){
			return false;
		}
		//var msg = '';		
		$('#dg').datagrid('acceptChanges');
		$('.ErrorMsgStyle').html('').hide();	
		var msg = Utils.getMessageOfRequireCheck('staffCode', 'Mã nhân viên',true);		
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('code', 'Mã phiếu');			
		}	
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã phiếu', Utils._CODE);
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('issuedDate', 'Ngày xuất');			
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('issuedDate', 'Ngày xuất');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('car', 'Xe');	
		}
		var rows = $('#dg').datagrid('getRows');
		if(rows.length==0){
			msg='Vui lòng chọn sản phẩm cần xuất kho.';
		}else{
			var row = $('#dg').datagrid('getRows')[0];
			if(isNullOrEmpty(row.productId)){
				msg='Vui lòng chọn sản phẩm cần xuất kho.';
			}			
		}		
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var isError = true;		
		var data = new Object();		
		data.vansaleCode = $('#staffCode').combobox('getValue');
		data.code = $('#code').val().trim();		
		data.carId = $('#car').val();
		data.issuedDate = $('#issuedDate').val().trim();		
		var selectedId = new Array();
		var selectedQty = new Array();
		var lstProductLot = new Array();
		
		var MAP = new Map();
		for(var i=0;i<rows.length;++i){
			var product = rows[i];
			if(isNullOrEmpty(product.productId)){
				continue;
			}
			if (product.quantityValue == 0) {
				$('#errMsg').html('Bạn chưa nhập số lượng xuất kho cho sản phẩm ' + Utils.XSSEncode(product.productCode) + '.').show();
				$('#dg').datagrid('selectRow', i);
				return false;
			}
			selectedId.push(product.productId);
			selectedQty.push(product.quantityValue);
			if(MAP.get(product.productCode)==null)
				MAP.put(product.productCode,i);
			else{
				$('#errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(product.productCode) + ' có hơn 1 dòng trong danh sách.').show();
				$('#dg').datagrid('selectRow',i);
				return false;
			}
			
			if(sys_separation_auto==0 && product.lot==1){
				var quantity = product.quantityValue;
				var list_product_lots = StockIssued.mapProductLot.get(product.productId);
				if(list_product_lots==null || list_product_lots.length<=0){
					$('#errMsg').html('Vui lòng chọn lô cần xuất cho sản phẩm ' + Utils.XSSEncode(product.productCode)).show();	
					$('#dg').datagir('selectRow',i);
					return false;
				}else{
					var result = '';
					var totalLot = 0;
					for(var j=0;j<list_product_lots.length;++j){
						var productLot = list_product_lots[j];
						if(productLot!=undefined && productLot.quantityLot>0){
							result += productLot.lot + ':' + productLot.quantityLot + ';';
							totalLot+= Number(productLot.quantityLot);
						}
					}						
					if(totalLot!=quantity){
						$('#errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(product.productCode) + ' có số lượng nhập phải bằng tổng nhập tất cả các lô.').show();	
						$('#dg').datagir('selectRow',i);
						return false;;
					}
					if(result.length>0){
						result = result.substring(0,result.length-1);						
					}
					lstProductLot.push(result);
				}
			}else{
				lstProductLot.push('');
			}			
		}
		data.lstProduct_Id = selectedId;
		data.lstProduct_Qty = selectedQty;
		data.lstProductLot = lstProductLot;		
		Utils.addOrSaveData(data, "/stock/issued/stockOutVanSale", StockIssued._xhrSave, 'errMsg', function(result){
			if(!result.error){
				StockIssued.isAuthorize = false;
				StockIssued.disabledDataOnForm();
			}else{				
				$('#errMsg').html(result.errMsg).show();				
			}
			if(result.stockTransCode!=undefined && result.stockTransCode!=null){
				$('#stockOutVanSale').val(result.stockTransCode);
			}
		}, 'loading', '', false, 'Bạn có muốn xuất kho không?');		
		
		return false;
	},
	disabledDataOnForm:function(){
		$('#btnExportExcel').unbind('click');
		$('.ErrorMsgStyle').html('').hide();	
		$('.CHOOSE_LOT').each(function(){
			var html = '<a style="color:#989898" href="javascript:void(0)">Chọn lô</a>';
			$(this).replaceWith(html);
		});		
		disabled('shopCode');
		disabled('btnStockOut');				
		enable('btnExportExcel');
		enable('btnExportExcelNew');
		disabled('issuedDate');		
		disabled('code');
		$('#car').attr('disabled','disabled');$('#car').parent().addClass('BoxDisSelect');
		$('#staffCode').combobox('disable');		
		$('#staffCode').parent().addClass('BoxDisSelect');
		$('#btnExportExcel').bind('click',StockIssued.exportStockOutVanSale);
		$('td[field=delete] a').removeAttr('onclick');		
	},
	exportStockOutVanSale:function(){		
		$('.ErrorMsgStyle').hide();
		/**
		 * @author hunglm16
		 * @since JUNE 23, 2014
		 * @description Update xuat file giong bao cao 7-8-3.Phieu xuat kho kiem van chuyen noi bo
		 * */
		var msg = Utils.getMessageOfRequireCheck('ipNameTransDialog', 'Tên lệnh điều động');		
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipNameTransDialog', 'Tên lệnh điều động', Utils._NAME);
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('fDateDialog', 'Ngày lệnh điều động');			
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('fDateDialog', 'Ngày lệnh điều động');			
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length==0&&!Utils.compareDate(day + '/' + month + '/' + year, $('#fDateDialog').val().trim())){
			msg = 'Từ ngày phải lớn hơn bằng ngày hiện tại';
			$('#fDateDialog').focus();
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ipContentTransDialog', 'Nội dung');			
		}	
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipContentTransDialog', 'Nội dung', Utils._NAME);
		}
		if(msg.length>0){
			$('#errMsg2').html(msg).show();
			return false;
		}
				
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = '';
		data.issuedDate = $('#fDateDialog').val().trim();
		data.stockTransId = $('#stockOutVanSale').val();
		data.transacContent = $('#ipContentTransDialog').val().trim();
		data.transacName = $('#ipNameTransDialog').val().trim();
		
		//Update them tham so xuat file report
		var kData = $.param(data,true);
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		$.ajax({
			type : "POST",
			url : "/stock/issued/print-pxkkvcnb",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').removeClass('Overlay');
				$('#imgOverlay').hide();
				$('#divOverlay').hide();				
				if(!data.error) {
					StockIssued.nameTransDialog = $('#ipNameTransDialog').val().trim();
					StockIssued.issuedDateDialog = $('#fDateDialog').val().trim();
					window.location.href = data.path;
				} else {
					$('#errMsg2').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').removeClass('Overlay');
				$('#divOverlay').hide();
				hideLoading(loading);
			}
		});
		return false;
		
	},
	chooseLotDialog:function(index){
		enable('btnSelectLot');
		$('.ErrorMsgStyle').html('').hide();
		var productSelection = $('#dg').datagrid('getRows')[index];
		if(productSelection==null || isNaN(productSelection.quantityValue)){
			return;
		}		
		StockIssued.PRODUCT_ID = productSelection.productId;
		StockIssued.PRODUCT_TOTAL = productSelection.quantityValue;	
		StockIssued.storeProduct = new Map();		
		$('#productLotDialog').css("visibility", "visible");
		var html = $('#productLotDialog').html();
		$('#productLotEasyUIDialog').dialog({closed: false,cache: false,modal: true,width : 600,height :470,onOpen: function(){	   
	        	StockIssued.request = false;
	        	$('#productLotDialog').html('');
				$('.easyui-dialog #productLotGrid').datagrid({
					view: bufferview,	
				    singleSelect:true,				   
				    height: 250,
				    rownumbers : true,
				    autoRowHeight:true,				   
				    fitColumns:true,				    
				    scrollbarSize:20,
					width: 550,									
				    columns:[[				          
				        {field: 'lot',title: 'Số lô', width: 80, sortable:false,resizable:false,align: 'left' },
					    {field: 'quantity',title: 'Tồn kho',  width: 80,sortable:false,resizable:false, align: 'right',
				        	formatter:function(value,row,opts){
				        		return formatQuantity(row.quantity,StockIssued.CONVFACT);
				        	}
				        },
					    {field: 'quantityLot', title: 'Số lượng', width: 80,sortable:false,resizable:false, align: 'right',
				        	formatter:function(value,row,opts){				        		
				        		var amount =  formatQuantity(row.quantityLot,StockIssued.CONVFACT);
				        		if(StockIssued.request || row.quantityLot==0){
				        			amount = "";
				        		}
				        		return '<input onkeypress="return NextAndPrevTextField(event,this,\'QUANTITY\')" type="text" class="InputTextStyle InputText1Style QUANTITY" onblur="StockIssued.onChange(this);" onchange="return StockIssued.onChange(this);" quantity="' +row.quantity+'"  id="QUANTITY' +row.lot+'" product-lot-id="' +row.id+'" value="' +amount+'" lot="' +row.lot+'" />';
				        	}
					    },					    
					    {field: 'id',hidden: true}
					   
				    ]]
				});
				$.getJSON('/stock/issued/list-product-lots?productId=' +StockIssued.PRODUCT_ID, function(result){					
					var product = result.product;
					if(product!=null){
						StockIssued.CONVFACT = product.convfact;						
						$('#fancyProductCode').html(product.productCode);
						$('#fancyProductName').html(product.productName);
						$('#fancyProductTotalQuantity').html(formatQuantityEx(StockIssued.PRODUCT_TOTAL,StockIssued.CONVFACT));
						if(result.error!=null && result.error){
							$('#fancyboxError').html('Các lô của sản phẩm ' + Utils.XSSEncode(product.productCode) +' đã hết hạn sử dụng.').show();
							disabled('btnSelectLot');
							return false;
						}
					}	
					var lots = null;					
					var list_product_lots = StockIssued.mapProductLot.get(StockIssued.PRODUCT_ID);
					if(list_product_lots!=null){
						lots = list_product_lots;
					}else{
						lots = result.lots;	
						StockIssued.request = true;
					}
					for(var i=0;i<lots.length;++i){								
						var productLot = StockIssued.createProductLot(lots[i]);	
						$('#productLotGrid').datagrid('appendRow',productLot);
					}
					StockIssued.showTotalAndScrollBodyProductLotDialog();
					$('.datagrid-header-rownumber').html('STT');
					Utils.bindFormatOnTextfieldInputCss('QUANTITY',Utils._TF_NUMBER_CONVFACT);
				});
	        },	        
	        onClose : function(){
	        	enable('btnSelectLot');
	        	$('#productLotDialog').html(html);
	        	$('#productLotDialog').css("visibility", "hidden");
	        	$('#productLotGridContainer').html('<table id="productLotGrid" class="easyui-datagrid" ></table>');
	        }
	     });			
		$('#errMsg').hide();
		return false;
	},	
	createProductLot:function(productLot,convfact){
		var newrow = {
				lot:productLot.lot,
				quantity:productLot.quantity,
				quantityLot:productLot.quantityLot,
				id:productLot.id							
		};
		return newrow;		
	},
	onChange:function(selector){
		var isError = false;
		$('.easyui-dialog .QUANTITY').each(function(){
			var selector = $(this);
			var quantityLot = getQuantity($(selector).val().trim(), StockIssued.CONVFACT);
			var quantity = $(selector).attr('quantity');
			var lot = $(selector).attr('lot');
			if(isNaN(quantityLot)){	
				isError = true;
				$('#fancyboxError').html('Số lượng của lô ' +lot+' không hợp lệ.').show();
				setTimeout(function() {$(selector).focus();}, 50);	
				return false;
			}
			if(quantity<quantityLot){	
				isError = true;
				$('#fancyboxError').html('Số lượng của lô ' +lot+' vượt quá giá trị tồn kho.').show();
				setTimeout(function() {$(selector).focus();}, 50);	
				return false;
			}
			$(selector).val(formatQuantity($(selector).val().trim(), StockIssued.CONVFACT));
			if($(selector).val().length==0){			
				$(selector).val('');
			}
		});
		if(isError){
			return false;
		}		
		StockIssued.showTotalAndScrollBodyProductLotDialog();
		return true;
	},
	acceptSeparation:function(){
		var productId = StockIssued.PRODUCT_ID;		
		var list_product_lots = new Array();
		var sum_quantity = 0;
		var validate_error = false;
		$('.easyui-dialog .QUANTITY').each(function(){		
			var quantityDisplay = $(this).val().trim();
			var lot = $(this).attr('lot');
			var convfact = StockIssued.CONVFACT;
			var quantityLot = getQuantity(quantityDisplay, StockIssued.CONVFACT);
			var quantity = $(this).attr('quantity');
			if(!isNullOrEmpty(quantityLot.toString())){
				if(isNaN(quantityLot)){
					$(this).focus();
					validate_error = true;
					$('#fancyboxError').html('Số lượng của lô ' +lot + ' không hợp lệ.').show();
					return false;
				}
				if(quantityLot>quantity){
					$(this).focus();
					validate_error = true;
					$('#fancyboxError').html('Số lượng của lô ' +lot + ' vượt quá giá trị tồn kho.').show();
					return false;
				}
				var productLot = new Object();
				productLot.lot = $(this).attr('lot');	
				productLot.id = $(this).attr('product-lot-id');
				productLot.quantityLot = quantityLot;
				productLot.quantity = quantity;							
				list_product_lots.push(productLot);	
				sum_quantity+=quantityLot;				
			}
		});
		if(validate_error){
			return false;
		}
		var totalQuantity = getQuantity(StockIssued.PRODUCT_TOTAL, StockIssued.CONVFACT);
		if(totalQuantity!=sum_quantity){
			$('#fancyboxError').html('Tổng số lượng nhập tất cả các lô phải bằng ' +formatQuantity(StockIssued.PRODUCT_TOTAL, StockIssued.CONVFACT)).show();
			return false;
		}
		StockIssued.mapProductLot.put(productId,list_product_lots);
		$('.easyui-dialog').dialog('close');
	},
	
	closeDiglogLOT:function() {
		enable('btnSelectLot');
		$('.easyui-dialog').dialog('close');
	},
	
	showTotalAndScrollBodyProductLotDialog:function(){
		var totalQty = 0;
		var totalQtyLot = 0;	
		$('.easyui-dialog .QUANTITY').each(function(){
			var totalHTML = $(this).val().trim();
			totalQtyLot += Number(getQuantity(totalHTML, StockIssued.CONVFACT));
			totalQty += Number($(this).attr('quantity'));
			$(this).css('text-align','right');
			$(this).css('width','147px');
			$(this).css('margin','0px');	
			
		});		
		$('#fancyboxError').html('').hide();		
		$('#totalQuantity').html(formatQuantity(totalQty,StockIssued.CONVFACT));
		$('#totalAvailableQuantity').html(formatQuantity(totalQtyLot,StockIssued.CONVFACT));		
	},
	exportDSBHNVBH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd3/export',dataModel);
	},
	exportDSBHNVBHSKU: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.listNVBHId = SuperviseCustomer.mapNVBH.keyArray;
		dataModel.listNVGSId = SuperviseCustomer.mapNVGS.keyArray;
		dataModel.listTBHVId = SuperviseCustomer.mapTBHV.keyArray;
		dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.lstProductId = SuperviseCustomer.mapProduct.keyArray;
		if ($('#product').val()=="") dataModel.strProductCode = "";
		else dataModel.strProductCode = $('#product').val().trim();
		dataModel.strTBHVCode = $('#staffTBHV').val().trim();
		dataModel.strNVGSCode =  $('#staffSaleCode').val().trim();
		dataModel.strNVBHCode = $('#staffSaleCode').val().trim();
		dataModel.checkKD1 = 0; 
		var kData = $.param(dataModel, true);
		$.ajax({
			type : "POST",
			url : '/report/crm/kd1/export',
			dataType : "json",
			data : (kData),
			success : function(data) {
				if(data.error == false) {
					dataModel.checkKD1 = 1;
					dataModel.listNVBHId = data.tbhv;
					dataModel.listNVGSId = data.nvgs;
					dataModel.listTBHVId = data.nvbh;
					dataModel.lstProductId = data.product;
					ReportUtils.exportReport('/report/crm/kd1/export',dataModel);
				}else{
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});	
		ReportUtils.exportReport('/report/crm/kd1/export',dataModel);
	},
	//loctt - Oct17, 2013
	formatQuantity: function(id, convfact){
		$('#productRow_' +id).val(StockValidateInput.formatStockQuantity($('#productRow_' +id).val().trim(), convfact));
	}

};
var StockIssuedGridFmt = {
		rownumFmt:function(value,row,index){
			if(!isNullOrEmpty(row.productId) && !row.footer){
				return (index+1);
			}
			return '';
		},
		productIdFmt:function(value,row,index){
			if(row.productId != null && row.productId != undefined && row.productId != ''){
				var product = StockIssued.lstProductMap.get(row.productId);
				if(product!=null && !row.footer)
					return '<span class="spStyle' +index+'" >' + Utils.XSSEncode(product.productCode) +'</span>';
				else if(row.footer){
					return value;
				}
      		} 
			return '';
		},
		productNameFmt:function(value,row,index){
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product != null && !row.footer) {
				return '<span class="spStyle' +index+'" >' + Utils.XSSEncode(product.productName) +'</span>';
			} else if(row.footer){
				return value;				
			}
			return '';
		},
		availableQuantityFmt:function(value,row,index){
			if(!isNullOrEmpty(row.productId) && !row.footer) {
	    		return '<span class="spStyle' +index+'" >' +formatQuantityEx(row.stockQuantity, row.convfact)+'</span>';
	    	}else if(row.footer){
				return value;
			}else {
	    		return '';
	    	}
		},
		lotFmt:function(value,row,index){
			if(!isNullOrEmpty(value) && row.lot==1 && sys_separation_auto=='0' && !row.footer){
    			return '<a href="javascript:void(0);" class="CHOOSE_LOT" onclick="return StockIssued.chooseLotDialog(' +index+')">Chọn lô</a>';
    		}else if(row.footer){
				return value;
			} else {
    			return '';
    		}
		},
		quantityFmt:function(value,row,index){
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product != null && row.quantityValue>0 && !row.footer) {				
				return '<span class="spStyle' +index+'" >' +formatQuantityEx(row.quantity,product.convfact)+'</span>';
			}else if(row.footer){
				return value;
			}		
			return '';
		},
		priceFmt:function(value,row,index){			
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product != null && product.price != null && product.price != undefined && !row.footer) {
				return '<span class="spStyle' +index+'" >' +formatCurrency(product.price)+'</span>';
			}else if(row.footer){
				return value;
			} else {
				return '';
			}
		},
		amountFmt:function(value,row,index){	
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product==null && !row.footer)
				return '';
			else if(row.footer){
				return value;
			}
			return '<span class="spStyle' +index+'" >' +formatFloatValue(row.amount)+'</span>';
		},
		delFmt:function(value,row,index){
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product==null)
				return '';
			return '<a tabindex="-1" href="javascript:void(0)" class="DelProduct" onclick="StockIssued.delSelectedRow(' +index+');" >'
			+'<img src="/resources/images/icon-delete.png" width="19" height="20" /></a>';
    		
		}		
};







/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-issued.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-manage-trans.js
 */
var StockManageTrans = {
	_xhrSave : null,
	stockTransCodeGlobal:null,
	fromStockCodeGlobal:null,
	toStockCodeGlobal:null,
	stockTransDateStrGlobal:null,
	stockTransTypeGlobal:null,
	nameTransDialog:null,
	issuedDateDialog:null,
	
	getShopCodeByShopCodeAndName : function(input) {
		var value = $('#'+ input).val().trim().split("-");
		return value[0];
	},
	
	openStockOutTrainsDialog :function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#stockOutVanSalePrintDiv').dialog({
			title : "Phiếu xuất kho kiêm vận chuyển nội bộ",
			closed : false,
			onOpen : function() {
				if(StockManageTrans.nameTransDialog != null){
					$('#ipNameTransDialog').val(Utils.XSSEncode(StockManageTrans.nameTransDialog));
				}
				if(StockManageTrans.issuedDateDialog != null){
					$('#fDateDialog').val(Utils.XSSEncode(StockManageTrans.issuedDateDialog));
				}
				$('#ipContentTransDialog').val('Vận chuyển hàng đi bán lưu động');
				$('#ipNameTransDialog').focus();
			},
			onClose:function(){
			}
		});
		return false;
	},
	
	exportStockOutTrans:function(){		
		$('.ErrorMsgStyle').hide();
		/**
		 * @author hunglm16
		 * @since JUNE 23, 2014
		 * @description Update xuat file giong bao cao 7-8-3.Phieu xuat kho kiem van chuyen noi bo
		 * */
		var msg = Utils.getMessageOfRequireCheck('ipNameTransDialog', 'Tên lệnh điều động');		
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipNameTransDialog', 'Tên lệnh điều động', Utils._NAME);
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('fDateDialog', 'Ngày lệnh điều động');			
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('fDateDialog', 'Ngày lệnh điều động');			
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length==0&&!Utils.compareDate(day + '/' + month + '/' + year, $('#fDateDialog').val().trim())){
			msg = 'Từ ngày phải lớn hơn bằng ngày hiện tại';
			$('#fDateDialog').focus();
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ipContentTransDialog', 'Nội dung');			
		}	
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipContentTransDialog', 'Nội dung', Utils._NAME);
		}
		if(msg.length>0){
			$('#errMsg2').html(msg).show();
			return false;
		}
				
		var data = new Object();
		data.staffCode = '';
		data.issuedDate = $('#fDateDialog').val().trim();
		data.stockTransCode = StockManageTrans.stockTransCodeGlobal;
		data.transacContent = $('#ipContentTransDialog').val().trim();
		data.transacName = $('#ipNameTransDialog').val().trim();

		//Update them tham so xuat file report
		var kData = $.param(data,true);
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		$.ajax({
			type : "POST",
			url : "/stock/manage-trans/print-pxkkvcnb",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').removeClass('Overlay');
				$('#imgOverlay').hide();
				$('#divOverlay').hide();				
				if(!data.error) {
					StockManageTrans.nameTransDialog = $('#ipNameTransDialog').val().trim();
					StockManageTrans.issuedDateDialog = $('#fDateDialog').val().trim();
					window.location.href = data.path;
				} else {
					$('#errMsg2').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').removeClass('Overlay');
				$('#divOverlay').hide();
				hideLoading(loading);
			}
		});
		return false;
		
	},
	
	search : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = "";
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('startDate','Từ ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('endDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày');
		}
	
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		
		if(msg.length ==0 && !Utils.compareDate(startDate,endDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();
			return false;
		}
		var type = $('#type').val();
		var shopId = $('#shopId').val();
		var params = {
			page: 1,
			startDate: startDate,
			endDate: endDate,
			type: type,
			shopId: shopId,
			status: $("#status").val().trim()
		};

		$('#grid').datagrid('load', params);
		$('#stockTransDetailTable').hide();
		return false;
	},
	viewDetail : function(stockTransCode, fromStockCode, toStockCode, stockTransDateStr, stockTransType, stockTransId) {
		StockManageTrans.stockTransCodeGlobal=stockTransCode;
		StockManageTrans.fromStockCodeGlobal=fromStockCode;
		StockManageTrans.toStockCodeGlobal=toStockCode;
		StockManageTrans.stockTransDateStrGlobal=stockTransDateStr;
		StockManageTrans.stockTransTypeGlobal=stockTransType;
		StockManageTrans.stockTransIdGlobal = stockTransId;
		$('#stockTransDetailTable').show();
		var params = new Object();
		params.stockTransCode = stockTransCode;
		params.stockTransId = stockTransId;
		$('#title').html('Thông tin chi tiết giao dịch kho - ' + Utils.XSSEncode(stockTransCode)).show();
		$('#productDetailGrid').html('<table id="gridDetail" class="easyui-datagrid"></table>');
		$('#gridDetail').datagrid({
			url : '/stock/manage-trans/viewdetailstock',
			method: 'GET',
			fitColumns:true,
			width: $('#productDetailGrid').width(),
			autoWidth: true,
			autoRowHeight : true,
			singleSelect: true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			pageSize:10,
			pageList  : [10,20,30],
			scrollbarSize:0,
			queryParams:params,
			columns:[[
			        {field:'productCode', title: 'Mã sản phẩm', width: 100, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
			        	  return Utils.XSSEncode(value);
			          }},
			  	    {field:'productName', title: 'Tên sản phẩm', width: 130, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
			        	  return Utils.XSSEncode(value);
			          }},
			        {field:'fromWarehouse', title: 'Kho xuất', width: 100, align: 'left', sortable:false,resizable:false,
				        	formatter: function(val, row) {
				        		if (val != undefined && val != null) {
				        			return Utils.XSSEncode(val);
				        		}
				        		return '';
				        	}
				        },
				  	{field:'toWarehouse', title: 'Kho nhập', width: 80, align: 'left',sortable:false,resizable:false,
				        	formatter: function(val, row) {
				        		if (val != undefined && val != null) {
				        			return Utils.XSSEncode(val);
				        		}
				        		return '';
				        	}
				        },
			  	    {field:'thungLe', title: 'Số lượng', sortable:false,resizable:false, align: 'right'},
			  	    /*{field:'lot', title: 'Số lô', width: 100, align: 'left', sortable:false,resizable:false},*/
			  	    {field:'price', title: 'Giá bán', width: 80, align: 'right',sortable:false,resizable:false,formatter:CommonFormatter.numberFormatter},
			  	    {field:'total', title: 'Thành tiền', width: 120, align: 'right',sortable:false,resizable:false,formatter:CommonFormatter.numberFormatter}			  	       
			]],	    
			onLoadSuccess :function(data){
				$('#btnExportExcel').unbind('click');
				 if(StockManageTrans.stockTransTypeGlobal == 'XUAT_KHO_NHAN_VIEN' || StockManageTrans.stockTransTypeGlobal == 'NHAP_KHO_NHAN_VIEN'){
					 enable('btnExportExcel');
					 $('#btnExportExcel').bind('click',function(event) {
						 StockManageTrans.printStockTrans();
					});
				 }else{
					 disabled('btnExportExcel');
				 }
				 $('.datagrid-header-rownumber').html('STT');	    	
				 updateRownumWidthForJqGrid('.easyui-dialog');
				 $(window).resize();    		
				 $('#totalWeight').html(formatCurrency(data.totalWeight));
				 $('#totalAmount').html(formatCurrency(data.totalAmount));
			}
		});		
		return false;
	},
	printStockTrans:function(){
		var data = new Object();
		//data.shopCodeTextField = StockManageTrans.getShopCodeByShopCodeAndName('shopCodeAndName');
		data.code = StockManageTrans.stockTransCodeGlobal;
		data.stockTransDate = StockManageTrans.stockTransDateStrGlobal;
		data.transType = StockManageTrans.stockTransTypeGlobal;
		data.stockTransId = StockManageTrans.stockTransIdGlobal;
		 if(data.transType == "XUAT_KHO_NHAN_VIEN"){
			 data.vansaleCode = StockManageTrans.toStockCodeGlobal;
		 }else if(data.transType ==   "NHAP_KHO_NHAN_VIEN"){
			 data.vansaleCode = StockManageTrans.fromStockCodeGlobal;	
		}else{
			data.vansaleCode = "";
		}
		try {
			data.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData;
		kData = $.param(data,true);
		$('#divOverlay').show();
		$("#imgOverlay").show();
		$.ajax({
				type : "POST",
				url : "/stock/manage-trans/printStockTrans",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						var filePath = ReportUtils.buildReportFilePath(data.path);
						window.location.href = filePath;
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
		});
		return false;
	},
	exportStockOutVanSale:function(){		
		/*$('#errMsg').hide();
		var code = $('#code').val().trim();		
		var carId = $('#car').val();*/		
		var data = new Object();
		//data.shopCodeTextField = StockManageTrans.getShopCodeByShopCodeAndName('shopCodeAndName');
		data.code = StockManageTrans.stockTransCodeGlobal;
		data.vansaleCode = StockManageTrans.codeStaffGlobal;
		data.stockTransDate = StockManageTrans.stockTransDateStrGlobal;
		data.stockTransId = StockManageTrans.stockTransIdGlobal;
		var kData = $.param(data,true);
		$('#divOverlay').show();
		$("#imgOverlay").show();
		$.ajax({
			type : "POST",
			url : "/stock/issued/exportStockOutVanSale",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					window.location.href = data.path;
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	exportStockReceivedReport:function(){
//		$('#errMsg').hide();
//		var code = $('#stockTransCode').val().trim();	
		var data = new Object();
		data.code = StockManageTrans.stockTransCodeGlobal;
		data.vansaleCode = StockManageTrans.codeStaffGlobal;
		data.stockTransDate = StockManageTrans.stockTransDateStrGlobal;
		data.stockTransId = StockManageTrans.stockTransIdGlobal;
		var kData = $.param(data,true);
		$('#divOverlay').show();
		$("#imgOverlay").show();
		$.ajax({
			type : "POST",
			url : "/stock/received/exportReport",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					window.location.href = data.path;
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-manage-trans.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-received.js
 */
var StockReceived = {
	_xhrSave : null,
	_amtMap:null,
	_storeProduct:null,
	_rowCount:null, //loctt- Oct10, 2013
	_allAmount: new Map(),//loctt - Oct16, 2013
	getInfo: function(){
		$('#errMsg').hide();
		$('#errMsg1').hide();
		$('#successMsg').hide();
		$('#stockTransCode').show();
		$('#btstock').show();
		var staffCode = $('#staffCode').combobox('getValue').trim();
		var data = new Object();
		data.staffCode = staffCode;
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		Utils.getHtmlDataByAjax(data, "/stock/received/getinfo", function(data){
			$("#productGrid").html(data).show();			
			StockReceived._amtMap = new Map();
			StockReceived._rowCount = $('#productGrid tr').length-1;//loctt - Oct10, 2013
		}, 'loading1', null);
		return false;
	},
	import: function(){
		$('#errMsg').hide();
		$('#successMsg').hide();
		$('#errMsg1').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('stockTransCode','Mã phiếu');
			if (msg.length == 0)
				{
					msg = Utils.getMessageOfSpecialCharactersValidate('stockTransCode','Mã phiếu',Utils._CODE);
				}
		}
		if(msg.length > 0){
			$('#errMsg1').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.staffCode = $('#staffCode').combobox('getValue').trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.stockTransCode = $('#stockTransCode').val().trim();		
		var url='/stock/received/import-auto';
		if(sys_separation_auto==0 || sys_separation_auto=='0'){
			/*
			 * Lay danh sach san pham duoi client
			 * CAU TRUC PRODUCT.CHECK_LOT=1: PRODUCT_CODE:{LOT/QUANTITY};
			 * CAU TRUC PRODUCT.CHECK_LOT=0; PRODUCT_CODE:QUANTITY;
			 * RESULT: PRODUCT_CODE:LOT1/QUANTITY,LOT2/QUANTITY;PRODUCT_CODE:QUANTITY;
			 */
			var lstProductVO = '';
			var MAP = new Map();
			var validate_error = false;
			$('#productGridBody .Quantity').each(function(){
				var object = new Object();
				var indexRow = $(this).attr('index-row');
				var amount = $(this).val().trim();
				var convfact = $('#Convafact' +indexRow).val().trim();
				var quantity = getQuantity(amount,convfact);
				var checkLot = $('#CheckLot' +indexRow).val().trim();
				var productCode = $('#ProductCode' +indexRow).val().trim();
				if(isNaN(quantity)){
					$('#errMsg1').html('Mã sản phẩm ' + Utils.XSSEncode(productCode) + ' có số lượng không hợp lệ.' ).show();		
					$('#Quantity' +indexRow).focus();
					validate_error=true;
					return false;
				}									
				var obj = MAP.get(productCode);
				if(obj!=null && checkLot==0){
					$('#errMsg1').html('Mã sản phẩm ' +Utils.XSSEncode(productCode) + ' tồn tại hơn 1 dòng trong bảng.' ).show();
					$('#Quantity' +indexRow).focus();
					validate_error=true;
					return false;					
				}else if(obj==null && checkLot==0){ 
					MAP.put(productCode,quantity);
					lstProductVO += productCode + ":" + quantity + ";";
				}else if(obj==null && checkLot==1){
					var str_lot = '';
					var SUB_MAP = new Map();
					$('#productGridBody .' +productCode).each(function(){
						var indexRowLot = $(this).attr('index-row');
						var lot = $('#Lot' +indexRowLot).val().trim();
						var msg = Utils.getMessageValidateLot('Lot' +indexRowLot);
						if(msg.length>0){
							$('#errMsg1').html(Utils.XSSEncode('Mã sản phẩm ' + Utils.XSSEncode(productCode) + '. ' + msg)).show();
							$('#Lot' +indexRowLot).focus();
							validate_error=true;
							return false;
						}
						var lotQuantity = $('#Quantity' +indexRowLot).val().trim();
						var convfact2 = $('#Convafact' +indexRow).val().trim();
						var newLot = SUB_MAP.get(lot);
						if(newLot!=null){
							$('#errMsg1').html('Mã sản phẩm ' + Utils.XSSEncode(productCode) + ' có số lô trùng nhau trong bảng. Một sản phẩm cùng lô phải nằm trên cùng một hàng' ).show();
							$('#Lot' +indexRowLot).focus();
							validate_error=true;	
							return false;
						}
						var product_lot = new Object();
						product_lot.lot = lot;
						product_lot.quantity = getQuantity(lotQuantity,convfact2);
						SUB_MAP.put(lot,product_lot);
					});
					var str_lot_of_product = "";
					for(var i=0;i<SUB_MAP.size();i++){
						var obj_lot = SUB_MAP.get(SUB_MAP.keyArray[i]); 
						str_lot_of_product += obj_lot.lot + "/" + obj_lot.quantity + ",";
					}
					if(str_lot_of_product.length>0){
						str_lot_of_product = str_lot_of_product.substring(0,str_lot_of_product.length-1);
					}					
					MAP.put(productCode,str_lot_of_product);
					lstProductVO += productCode + ":" + str_lot_of_product + ";";
				}		
			});
			if(lstProductVO.length>0){
				lstProductVO = lstProductVO.substring(0,lstProductVO.length-1);
			}
			if(validate_error){				
				return false;
			}
			if(MAP.size()<=0){
				$('#errMsg1').html('Không có mặt hàng nào được nhập.' ).show();
				return false;
			}
			dataModel.lstProductVO = lstProductVO;
			url = '/stock/received/import';
			$('#divOverlay').show();
			$.ajax({
				type : "POST",
				url : "/stock/received/check-product",
				data :(dataModel),
				dataType: "json",
				success : function(result) {
					$('#divOverlay').hide();
					if(result!=null && result.list_lots!=null && result.list_lots.length>0){
						var result_list_lot = result.list_lots;
						if(result_list_lot.length>0){
							var msg = 'Các sản phẩm có lô : ' + result_list_lot 
							+ '.<div class="Clear"></div> Không nằm trong danh sách lô xuất trong ngày.'
							+ '<div class="Clear"></div> Bạn có muốn nhập không?';
							$.messager.confirm('Xác nhận', msg, function(r){
								if(r){									
									Utils.saveData(dataModel, url,StockReceived._xhrSave,  'errMsg1', function(result) {
										StockReceived.callbackSuccess(result);							
									}, 'loading', null, null, null,'1');
								}
							});
							
						}
					}else{
						StockReceived.addOrSaveData(dataModel,url);
					}
				}
			});			
		}else{
			StockReceived.addOrSaveData(dataModel,url);
		}		
		return false;
	},
	addOrSaveData:function(dataModel,url){
		Utils.addOrSaveData(dataModel,url, StockReceived._xhrSave, 'errMsg', function(result){
			StockReceived.callbackSuccess(result);
			enable('btnExportExcel');
			$('.Func1Section').attr('style','width:486px;');
			$('#btnExportExcel').bind('click',function(){
				StockReceived.exportReport();
			});
		},'loading',null,null,null);
	},
	callbackSuccess:function(result){
		if(result.error){
			$('#errMsg1').html(result.errMsg).show();
			$('#btstock').hide();				
		}else{
			$('#successMsg1').show();
			$('#stockTransCode').attr("disabled", "disabled");
			$('#btstock').hide();
			$('#errMsg').hide();
			$('#errMsg1').hide();
			if(sys_separation_auto==0 || sys_separation_auto=='0'){
				$('.Quantity').each(function(){
					var objectId = $(this).attr('id');
					disabled(objectId);
				});
				$('.Lot').each(function(){
					var objectId = $(this).attr('id');
					disabled(objectId);
				});					 
				$('.DelProduct').each(function(){
					$(this).html('');
				});
				disabled('issAddProduct');
			}
		}
	},	
	searchProduct:function(){			
		var code = encodeChar($('.easyui-dialog #productCode').val().trim());
//		var name = encodeChar($('.easyui-dialog #productName').val().trim());
		var name = $('.easyui-dialog #productName').val().trim();
		var staffCode = $('#staffCode').combobox('getValue').trim();
		$('.easyui-dialog #grid').datagrid('load',{staffCode:staffCode,productCode :code, productName: name});
		return false;
	},
	openSelectProductDialog: function(){
		StockReceived._storeProduct = new Map();
		StockReceived._amtMap = new Map();
		$('#errMsg').hide();
		$('#productDialog').css("visibility", "visible");
		
		var html = $('#productDialog').html();
		$('#productEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        height :600,
	        onOpen: function(){	    
	        	$('.easyui-dialog #productCode').val('');
	    		$('.easyui-dialog #productName').val('');
	    		StockReceived._allAmount = new Map();
	        	$('.easyui-dialog #grid').datagrid({
					url : '/stock/received/search-product',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					queryParams:{
						staffCode:$('#staffCode').combobox('getValue').trim()
					},
					pageList  : [10,20,30],
					width : ($('.easyui-dialog #productGridContainer').width()),
				    columns:[[				          
				              	{field: 'productCode',index:'productCode',title: 'Mã sản phẩm', width: 70, sortable:false,resizable:false,align: 'left' },
							    {field: 'productName',index:'productName', title: 'Tên sản phẩm',  width: 200,sortable:false,resizable:false, align: 'left' },
							    {field: 'instock',index:'instock', title: 'Tồn kho', width: 50,sortable:false,resizable:false, align: 'right', formatter: IssuedStockGridFormatter.instockCellFormatterNew},
							    {field: 'price',index:'price', title: 'Giá',width: 70, sortable:false,resizable:false, align: 'right', formatter: GridFormatterUtils.currencyCellFormatter},
							    //loctt - Oct17, 2013
							    {field: 'amt',index:'amt', title: 'Số lượng',width: 60, sortable:false,resizable:false,align: 'right', formatter: function(value, row, index){		
									return '<input index=' +index+' row-id="' +row.id+'" id="productRow_' + row.id 
									+'" size="5" type="text" maxlength="8" style="text-align:right;margin: 0;width:77px;" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockIssued.amountBlured('
									+ index +');" onchange="StockReceived.formatQuantity(' +row.id+',' +row.convfact+');">';
								}},
//								    {field: 'amt',index:'amt', title: 'Số lượng',width: 60, sortable:false,resizable:false,align: 'right', formatter: IssuedStockGridFormatter.amountCellFormatter},			
							    {field: 'convfact', index:'convfact', hidden: true},
							    {field: 'id', index:'id', hidden: true},
							    {field: 'productId', index:'productId', hidden: true},							    
							    {field: 'grossWeight', index:'grossWeight', hidden: true},							    
							    {field: 'quantity', index:'quantity', hidden: true},
							    {field: 'availableQuantity', index:'availableQuantity', hidden: true},
							    {field: 'checkLot', index:'checkLot', hidden: true}	
				    ]],
				   onLoadSuccess :function(data){   	 
				    	$('.datagrid-header-rownumber').html('STT');
				    	Utils.bindAutoSearch();
				    	
				    	var rows = $('#grid').datagrid('getRows');
				    	for(var i = 0; i < rows.length; i++) {
//				    		if(StockReceived._allAmount.get(rows[i].id))
//				    			$('#grid').datagrid('checkRow', i);
				    		var obj = StockReceived._allAmount.get(rows[i].id);
				    		if (obj != null)
				    			$('#productRow_' +rows[i].id).val(obj.amount);
				    	}
				    	$('#productCode').focus();
				    	$('.datagrid-header-check input').removeAttr("checked");
				   
				    },
				    //loctt - Oct16, 2013 - begin
		            onBeforeLoad:function(node,param){
		            	var rows = $('#grid').datagrid('getRows');
		            	if(rows.length > 0){
		            		for(var i = 0; i < rows.length; i++) {
		            			var obj = StockReceived.getDataFromCurrentPage(rows[i].id, i);
		            			if(obj!=null)
					    		StockReceived._allAmount.put(rows[i].id, obj);
					    	}
		            		
		            	}
		            }
				    //loctt - Oct16, 2013 - end
				});  
	        },	     
	        onClose : function(){
	        	$('#productDialog').html(html);
	        	$('.easyui-dialog #errMsg').html('');//loctt - Oct10, 2013
	        	$('#productDialog').css("visibility", "hidden");
	        	$('#productGridContainer').html('<table id="grid" class="easyui-datagrid" ></table><div id="pager"></div>');	        }
	     });
		
		$('.easyui-dialog #productCode').focus();
		return false;
	},
	selectListProductsOnDialog:function(){
		$('.ErrorMsgStyle').html("").show().change();
		var indexRow = StockReceived.getLastNumRow();
		var flag = true;
		var productCount = 0;
		$('.IssuedStock_AmountValue').each(function(){
			var indexGrid = $(this).attr('index');			
			var productId =$(this).attr('row-id');
			if($(this).val().length>0){				
				var obj = StockReceived.getData(productId,indexRow,indexGrid);
				if(obj!=null){
					if(obj.amount == '0/0' ||obj.amount == '0'|| obj.amount==''){
						flag = false;	
						index = productId;
						$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();
						return false;
					}
					var total_accept = 0;
					var total_failed = 0;
					var total_failed = parseInt(getQuantity(obj.amount, obj.convfact));
					var total_accept = parseInt(getQuantity(obj.quantity, obj.convfact));
					if(total_accept < total_failed){
						$('.easyui-dialog #errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(obj.code + ' - ' + obj.name) + '. Vượt quá giá trị tồn kho').show();
						flag = false;	
						index = productId;
						return false;			
					} 				
					StockReceived._amtMap.put(obj.id, obj);
					++indexRow;
					var storeObject = StockReceived._storeProduct.get(obj.id);
					if(storeObject!=null){
						StockReceived._storeProduct.remove(obj.id);
					}
					productCount ++;
				}
			}
		
		});

		//loctt - Oct18, 2013 - begin
		for(var i = 0; i< StockReceived._allAmount.keyArray.length; i++){
			if(StockReceived._allAmount.keyArray[i]!= undefined){
				var obj = StockReceived._allAmount.valArray[i];
			//kiem tra xem san pham nay co trong amt map chua
				for(var j = 0; j< StockReceived._amtMap.keyArray.length; j++){
					if(StockReceived._amtMap.valArray[j].id == obj.id){
						StockReceived._amtMap.valArray[j].amount = obj.amount;
						break;
					}
				}
				if(j<StockReceived._amtMap.keyArray.length)//co gia tri trung
					continue;
				obj.indexRow = indexRow;
				if(obj!=null){
					if(obj.amount == '0/0' ||obj.amount == '0'|| obj.amount==''){
						flag = false;	
						index = productId;
						$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();
						return false;
					}
					var total_failed = parseInt(getQuantity(obj.amount, obj.convfact));
					var total_accept = parseInt(getQuantity(obj.quantity, obj.convfact));
					if(total_accept < total_failed){
						$('.easyui-dialog #errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(obj.code + ' - ' + obj.name) + '. Vượt quá giá trị tồn kho').show();
						flag = false;	
						index = productId;
						return false;			
					} 				
					StockReceived._amtMap.put(obj.id, obj);
					++indexRow;
					var storeObject = StockReceived._storeProduct.get(obj.id);
					if(storeObject!=null){
						StockReceived._storeProduct.remove(obj.id);
					}
					productCount ++;
				}
			}
		}
		//loctt - Oct18, 2013 - end
		
		//loctt -Oct9, 2013- fix 0011276 - begin
		if(productCount == 0 && $('.easyui-dialog #errMsg').html() == ''){
			$('.easyui-dialog #errMsg').html('Yêu cầu nhập số lượng').show();
			return false;
		}
		//loctt -Oct9, 2013- fix 0011276 - end
		
		if(!flag){	
			 $('#productRow_' + index).focus();
			return false;
		}
		for(var i=0;i<StockReceived._storeProduct.size();i++){
			var temp = StockReceived._storeProduct.get(StockIssued._storeProduct.keyArray[i]); 			
			StockReceived._amtMap.put(temp.id, temp);
		}
		if(StockReceived._amtMap==null || StockReceived._amtMap.size()==0){
			$('.fancybox-inner #errMsg').html('Vui lòng chọn sản phẩm cần xuất kho.').show();
			return false;
		}
		var totalWeight = 0;
		var totalMoney = 0;
		//loctt - Oct10, 2013 - fix 0011300 - begin
		if($('#productGrid').find('.NotData').text().length > 0)
			$('#productGrid .NotData').remove();
		//loctt - Oct10, 2013 - fix 0011300 -end
		for(var i=0;i<StockReceived._amtMap.size();i++){
			var obj = StockReceived._amtMap.get(StockReceived._amtMap.keyArray[i]);
			StockReceived.addProduct(obj);				
		}
		Utils.bindFormatOnTextfieldInputCss('Quantity', Utils._TF_NUMBER_CONVFACT);
		Utils.bindFormatOnTextfieldInputCss('Lot', Utils._TF_NUMBER);		
		
		StockReceived.showTotalGrossWeightAndPrice();
		$('.easyui-dialog').dialog('close');		
	},
	deleteProduct:function(rowNumber,id){	
		$.messager.confirm('Xác nhận', 'Bạn có chắc chắn muốn xóa dòng sản phẩm này ?', function(ret){
			if (ret == true) {
				if($('#ROW_' + rowNumber) != null && $('#ROW_' + rowNumber).html()!= null && $('#ROW_' + rowNumber).html().length > 0){					
					$('#ROW_' + rowNumber).replaceWith("");					
					StockReceived.showTotalGrossWeightAndPrice();
					if(id!=null && id!=undefined){
						StockReceived._amtMap.remove(id);
					}
					var i = 0;
					$('.RowNumber').each(function() {
					    ++i;
					    var chs = $(this).children();
					    $(this).html(i);
					    for (var jj = 0; jj < chs.length; jj++) {
					    	$(this).append(chs[jj].outerHTML);
					    }
					});
				}
			}
		});
	},
	getData:function(productId,indexRow,indexGrid){
		var rows = $('#grid').datagrid('getRows')[indexGrid];
		if(rows==null){
			return false;
		}		
		var amount = $('#productRow_' + productId).val().trim();
		if(amount.length==0){			
			return null;
		}				
		var convfact = rows.convfact;
		var price =  rows.price;		
		var obj = new Object();
		obj.indexRow = indexRow;
		obj.id = rows.id;	
		obj.productId = productId;
		obj.code = rows.productCode;
		obj.name = rows.productName;		
		obj.convfact = convfact;
		obj.grossWeight = rows.grossWeight;	
		obj.rowId = productId;		
		obj.display_amount = amount;
		obj.amount = formatQuantity(amount,convfact);		
		obj.price =  rows.price;
		obj.checkLot = rows.checkLot;		
		obj.totalMoney = getQuantity(amount, convfact) * price;
		obj.quantity = rows.quantity;
		obj.availableQuantity = rows.availableQuantity;
		return obj;
	},
	getDataFromCurrentPage:function(productId,indexGrid){
		var rows = $('#grid').datagrid('getRows')[indexGrid];
		if(rows==null){
			return false;
		}		
		var amount = $('#productRow_' + productId).val().trim();
		if(amount.length==0){			
			return null;
		}				
		var convfact = rows.convfact;
		var price =  rows.price;		
		var obj = new Object();
		obj.id = rows.id;	
		obj.productId = productId;
		obj.code = rows.productCode;
		obj.name = rows.productName;		
		obj.convfact = convfact;
		obj.grossWeight = rows.grossWeight;	
		obj.rowId = productId;		
		obj.display_amount = amount;
		obj.amount = formatQuantity(amount,convfact);		
		obj.price =  rows.price;
		obj.checkLot = rows.checkLot;		
		obj.totalMoney = getQuantity(amount, convfact) * price;
		obj.quantity = rows.quantity;
		obj.availableQuantity = rows.availableQuantity;
		return obj;
	},
	getLastNumRow:function(){
		var index = 0;
		$('#productGrid .RowNumber').each(function(){
			++index;
		});
		return ++index;
	},
	addProduct: function(obj){
		var html = new Array();	
		html.push('<tr id="ROW_' +obj.indexRow+'">');	
		html.push('<td style="border-left: 1px solid #C6D5DB;" class="AlignCCols RowNumber">' +obj.indexRow);
		html.push('<input type="hidden" id="ProductCode' +obj.indexRow+'" value="' +obj.code+'" />');
		html.push('<input type="hidden" id="Convafact' +obj.indexRow+'" value="' +obj.convfact+'" />');
		html.push('<input type="hidden" id="CheckLot' +obj.indexRow+'" value="' +obj.checkLot+'" />');
     	html.push('</td>');
		html.push('<td class="AlignLCols">' +Utils.XSSEncode(obj.code)+'</td>');
		html.push('<td class="AlignLCols Wordwrap">' +Utils.XSSEncode(obj.name)+'</td>');
		if (obj.checkLot == 1) {
			html.push('<td class="AlignRCols" style="padding: 4px 5px;"><input maxlength="6" index-row="' +obj.indexRow+'" id="Lot' +obj.indexRow+'" class="AlignRight Lot ' +Utils.XSSEncode(obj.code)+'" type="text" size="8" /></td>');
		}else{
			html.push('<td class="AlignRCols" style="padding: 4px 5px;"></td>');
		}
		html.push('<td style="padding: 4px 5px;" class="AlignRCols">');
		html.push('<input onblur="return StockReceived.qtyChanged(this);" onchange="return StockReceived.qtyChanged(this);" price="' +obj.price+'" gross-weight="' +obj.grossWeight+'" convfact="' +obj.convfact+'" index-row="' +obj.indexRow+'" type="text" size="8" class="AlignRight Quantity" value="' +obj.amount+'" id="Quantity' +obj.indexRow+'">');
		html.push('</td><td class="ColsTd6 AlignRight CurrencyNumber ">' +formatCurrency(obj.price)+'</td>');
		html.push('<td style="padding-right: 20px !important;" class="ColsTd7 AlignRight CurrencyNumber">' +formatCurrency(obj.totalMoney)+'</td>');
		html.push('<td class="ColsTd8 ColsTdEnd AlignCenter">');
		html.push('<a href="javascript:void(0)" onclick="return StockReceived.deleteProduct(' +obj.indexRow+',' +obj.id+');" class="DelProduct">');
		html.push('<img width="19" height="20" src="/resources/images/icon-delete.png"></a>');
		html.push('</td>');
		html.push('</tr>');
		$('#productGridBody').append(html.join(""));		
	},
	qtyChanged:function(selector){
		var convfact = $(selector).attr('convfact');
		var amount = $(selector).val().trim();		
		var amount = formatQuantity(amount,convfact);
		var quantity = getQuantity(amount, convfact);
		if(isNaN(quantity)){
			$(selector).val('0/0');
		}else{
			$(selector).val(amount);
		}		
		StockReceived.showTotalGrossWeightAndPrice();
	},
	showTotalGrossWeightAndPrice:function(){		
		var total_gross_weight = 0;
		var total_money = 0;
		$('.Quantity').each(function(){
			var amount = $(this).val().trim();
			var convfact = $(this).attr('convfact');
			var grossWeight = $(this).attr('gross-weight');
			var quantity = getQuantity(amount, convfact);
			total_gross_weight += quantity*grossWeight;
			var price = $(this).attr('price');
			total_money += price*quantity;
		});
		$('#totalWeight').html(formatFloatValue(total_gross_weight,2));
		$('#totalAmount').html(formatCurrency(total_money));
	},
	exportReport:function(){
		$('#errMsg').hide();
		var code = $('#stockTransCode').val().trim();		
		var data = new Object();
		data.code = code;
		data.vansaleCode = $('#staffCode').combobox('getValue').trim();
		//ReportUtils.exportReport('/stock/issued/exportStockOutVanSale',data);
		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/stock/received/exportReport",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					window.location.href = data.path;
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	//loctt - Oct17, 2013
	formatQuantity: function(id, convfact){
		$('#productRow_' +id).val(StockValidateInput.formatStockQuantity($('#productRow_' +id).val().trim(), convfact));
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-received.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-update.js
 */
var StockUpdate = {
	_xhrSave: null,
	_amtMap: null,
	_quantityTemp : null,
	xhrExport: null,
	storeProduct:null,
	storeProductTmp:null,
	storeStockTotalMap : null,
	storeWarehouseMap : null,
	lstProductArray:new Array(),
	lstProductUpdate: new Array(),
	lstProductMap:new Map(),/** San pham co the dieu chinh cho NPP key=productId*/
	lstProductCodeMap:new Map(),/** San pham co the dieu chinh key=productCode */
	lstProductMapMap:null,/*danh sach san pham de chon. dc load luc load trang*/
	lstProductLotMap:new Map(),
	lstProductSelectionLotMap:new Map(),
	lstWarehouse : new Array(), /** Danh sach kho cua NPP */	
	TYPE:1,	
	selectProductId:null,
	indexEditing:null,
	indexLotEditing:null,
	isAuthorize : true,
	quantityChangeLot : null,
	isValidShop: false,
	nameTransDialog: null,
	fDateDialog: null,
	contentTransDialog: null,
	isEndCode: false,
	isProcessing: false,
	setCookieStockOutVanSale: function()
	{
		StockUpdate.nameTransDialog = $('#ipNameTransDialog').val();
		StockUpdate.fDateDialog = $('#fDateDialog').val();
	},

	getCookieStockOutVanSale : function(cname)
	{
		var name = cname.trim();
		name = name.trim().toLowerCase();
		var ca = document.cookie.split('>');
		for(var i=0; i<ca.length; i++)
		{
		  var c = ca[i].trim().toLowerCase();
		  if (c.indexOf(name)==0) return ca[i].trim().substring(name.length,c.length);
		}
		return "";
	},
	
	openStockOutVanSalePrintDialog :function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#stockOutVanSalePrintDiv').dialog({
			title : stock_update_pxk_kiem_vcnb,
			closed : false,
			onOpen : function() {				
				if(StockUpdate.nameTransDialog != null){
					$('#ipNameTransDialog').val(StockUpdate.nameTransDialog);
				}
				if(StockUpdate.fDateDialog != null){
					$('#fDateDialog').val(StockUpdate.fDateDialog);
				}
				$('#ipNameTransDialog').focus();
			},
			onClose:function(){
				$('#ipContentTransDialog').val('');
				$('#customerNameDialog').val('');
				//$('#ipContentTransDialog').val('Vận chuyển hàng đi bán lưu động');
			}
		});
		return false;
	},
	
	exportStockOutUpdate:function(){		
		$('.ErrorMsgStyle').hide();
		/**
		 * @author hunglm16
		 * @since JUNE 23, 2014
		 * @description Update xuat file giong bao cao 7-8-3.Phieu xuat kho kiem van chuyen noi bo
		 * */
		var msg = Utils.getMessageOfRequireCheck('ipNameTransDialog', stock_update_ten_lenh_dieu_dong);		
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipNameTransDialog', stock_update_ten_lenh_dieu_dong , Utils._NAME);
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('fDateDialog', stock_update_ngay_lenh_dieu_dong);			
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('fDateDialog', stock_update_ngay_lenh_dieu_dong);			
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length==0&&!Utils.compareDate(day + '/' + month + '/' + year, $('#fDateDialog').val().trim())){
			msg = msgCommonErr5;
			$('#fDateDialog').focus();
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ipContentTransDialog', stock_update_noi_dung);			
		}	
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipContentTransDialog', stock_update_noi_dung, Utils._NAME);
		}
		if(msg.length>0){
			$('#errMsg2').html(msg).show();
			return false;
		}
				
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = '';
		data.issuedDate = $('#fDateDialog').val().trim();
		data.stockTransId = $('#stockTransId').val();
		data.transacContent = $('#ipContentTransDialog').val().trim();
		data.transacName = $('#ipNameTransDialog').val().trim();
		try {
			dataModel.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		//Update them tham so xuat file report
		var kData = $.param(data,true);
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		$.ajax({
			type : "POST",
			url : "/stock/update/print-pxkkvcnb",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').removeClass('Overlay');
				$('#imgOverlay').hide();
				$('#divOverlay').hide();				
				if(!data.error) {
					StockUpdate.setCookieStockOutVanSale();
					var filePath = ReportUtils.buildReportFilePath(data.path);
					window.location.href = filePath;
				} else {
					$('#errMsg2').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').removeClass('Overlay');
				$('#divOverlay').hide();
				hideLoading(loading);
				StockIssued.xhrExport = null;
			}
		});
		return false;
		
	},
	importExcel : function() {
		$('.ErrorMsgStyle').html('').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$('#errMsg').html(data.message).show();
		}, 'importFrmStock_importFrm', 'excelFile', null, 'errExcelMsg');
	},
	loadGridProduct: function() {
		$('#dg').datagrid({
	    	data : StockUpdate.lstProductUpdate,
	    	singleSelect: true,			
			fitColumns:true,		
			scrollbarSize:0,		
			width: $('#idGridDg').width() - 5,
	    	columns:[[
	    	    {field:'rownum', title: jsp_common_numerical_order, width: 35, sortable:false,resizable:true , align: 'center',
	    	    	formatter:function(value,row,index){	    	    		
	    	    		if(!isNullOrEmpty(row.productId))
	    	    			return index+1;
	    	    	}
	    	    },
				{field:'productId',title:jsp_common_product_code,width:100, resizable:false, sortable:false, align:'left', 
	    	    	formatter: function(value,row,index) {
	    	    		if(isNullOrEmpty(row.productId)) return '';
	    	    		var product = StockUpdate.lstProductMap.get(row.productId);
	    	    		if(product!=null) return Utils.XSSEncode(product.productCode);
	    	    	}, 
	    	    	editor: {
	    	    		type:'combobox',
	    	    		options:{ 
							valueField:'productId',
							textField:'productCode',
							data:StockUpdate.lstProductArray,
							width:200,
							formatter: function(row) {
								return '<span style="font-weight:bold">' + Utils.XSSEncode(row.productCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.productName) + '</span>';
						    },
						    onSelect:function(rec){				
						    	var product = StockUpdate.lstProductMap.get(rec.productId);
					    		if(product!=null){
									$('.ErrorMsgStyle').html('').hide();
									var stockQuantity = 0;
					    			StockUpdate.selectProductId = rec.productId;				    			
						    		$('.datagrid-row-editing td[field=productName] div').html(Utils.XSSEncode(product.productName));
						    		$(this).combobox('setValue',product.productId);	
									/** Lay kho mac dinh cho combobox kho */
									var ed = $('#dg').datagrid('getEditor', {index:StockUpdate.indexEditing,field:'warehouseId'});
									if(ed != null && $(ed.target).combobox('getData').length > 0){
										var warehouseId = $(ed.target).combobox('getData')[0].id;
										$(ed.target).combobox('setValue',warehouseId);
										var stockQuantity = 0;
										var availableQuantity = 0;
										var stock_total = StockUpdate.storeStockTotalMap.get(product.productCode + warehouseId);
										if(stock_total != null){
											stockQuantity = stock_total.quantity;
											availableQuantity = stock_total.availableQuantity;
										}
										$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(stockQuantity,product.convfact));
										$('.datagrid-row-editing td[field=availableQuantity] div').html(formatQuantityEx(availableQuantity,product.convfact));
									}									
					    		}
						    },
						    filter: function(q, row){
						    	q = new String(q).toUpperCase();
								var opts = $(this).combobox('options');
								return row[opts.textField].indexOf(q)>=0;
						    },
						    onChange:function(newvalue,oldvalue){
						    	if(newvalue!=undefined && newvalue!=null){
						    		var product = StockUpdate.lstProductCodeMap.get(new String(newvalue).toUpperCase());

							    	if(product!=null){
							    		StockUpdate.isEndCode = true;	
							    		$(this).combobox('setValue',product.productId);									
							    	}
							    	if(StockUpdate.isEndCode && newvalue != null){
							    		StockUpdate.isEndCode = false;
							    		var productSelection = StockUpdate.lstProductMap.get(newvalue);
							    		if(productSelection != undefined && productSelection != null){
							    			var stockQuantity = 0;
					    					StockUpdate.selectProductId = newvalue;
					    					$('.datagrid-row-editing td[field=productName] div').html(Utils.XSSEncode(productSelection.productName));
					    					$(this).combobox('setValue',productSelection.productId);	
					    					var ed = $('#dg').datagrid('getEditor', {index:StockUpdate.indexEditing,field:'warehouseId'});
					    					if(ed != null && $(ed.target).combobox('getData').length > 0){
												var warehouseId = $(ed.target).combobox('getData')[0].id;
												$(ed.target).combobox('setValue',warehouseId);
												var stockQuantity = 0;
												var availableQuantity = 0;
												var stock_total = StockUpdate.storeStockTotalMap.get(productSelection.productCode + warehouseId);
												if(stock_total != null){
													stockQuantity = stock_total.quantity;
													availableQuantity = stock_total.availableQuantity;
												}
												$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(stockQuantity,productSelection.convfact));
												$('.datagrid-row-editing td[field=availableQuantity] div').html(formatQuantityEx(availableQuantity,productSelection.convfact));
											}	
							    		}

							    	}
						    	}	
								$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});
							}
					}
				}},
				{field: 'productName', title: jsp_common_product_name, width:350, formatter:function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		          }},
				{field: 'stockQuantity', title: jsp_common_stock_quantity, width:100,align:'right',
					formatter:function(value,row,index){
						return formatQuantityEx(value,row.convfact);
					}
				},
				{field: 'availableQuantity', title: jsp_common_stock_available_quantity, width:100,align:'right',
					formatter:function(value,row,index){
						return formatQuantityEx(value,row.convfact);
					}
				},
				{field: 'warehouseId', title: jsp_common_warehouse, width:150,align:'left',
					formatter:function(value,row,index){
						return Utils.XSSEncode(row.warehouseName);;
					},
					editor: {
	    	    		type:'combobox',						
	    	    		options:{ 
							valueField:'id',
							textField:'warehouseName',
							data : StockUpdate.lstWarehouse,
							width:200,	
							formatter: function(row) {
								return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.warehouseName) + '</span>';
						    },
							filter: function(q, row){
								q = new String(q).toUpperCase().trim();
								var opts = $(this).combobox('options');
								return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
							},							
						    onSelect:function(rec){	
								var product = StockUpdate.lstProductMap.get(StockUpdate.selectProductId);
								if(product != null){
									var warehouseId = rec.id;
									var stockQuantity = 0;
									var availableQuantity = 0;
									var stock_total = StockUpdate.storeStockTotalMap.get(product.productCode + warehouseId);
									if(stock_total != null){
										stockQuantity = stock_total.quantity;
										availableQuantity = stock_total.availableQuantity;
									}
									$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(stockQuantity,product.convfact));
									$('.datagrid-row-editing td[field=availableQuantity] div').html(formatQuantityEx(availableQuantity,product.convfact));
								}								
						    },
						    onChange:function(newvalue,oldvalue){						    	
							}
						}
					}
				},
				{field:'quantity', title: jsp_common_so_luong, width:100, resizable:false, sortable:false, align:'right', 
					formatter: function(value,row,index) {
						if(isNullOrEmpty(row.productId)) return '';
	    	    		var product = StockUpdate.lstProductMap.get(row.productId);
	    	    		if(product!=null){
	    	    			return Utils.XSSEncode(value);
	    	    		}					
					},
					editor:{type : 'text'}
				},
				{field:'delete', title: jsp_common_xoa, width:30, resizable:false, sortable:false, align:'center', 
					formatter: function(value, row, index) {
						if (isNullOrEmpty(row.productId)) {
							return '';
						} 
	    	    		var product = StockUpdate.lstProductMap.get(row.productId);
	    	    		if (product != null) {
	    	    			return '<a href="javascript:void(0)" onclick="StockUpdate.delSelectedRow(' +index+');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a>';
	    	    		}				
					}
				}
	    	]],
	    	onAfterEdit:function(index,row){
	    		StockUpdate.indexEditing = index;
		    	var productId = row.productId;
		    	var product = StockUpdate.lstProductMap.get(productId);
		    	if(product!=null){
		    		row.productCode = product.productCode;
		    		row.productName = product.productName;		    		
		    		row.checkLot = product.checkLot;					
					var stockQuantity = 0;
					var availableQuantity = 0;
					if(row.warehouseId != null && row.warehouseId != ''){	
						var stock_total = StockUpdate.storeStockTotalMap.get(product.productCode + row.warehouseId);						
						if(stock_total != null){
							stockQuantity = stock_total.quantity;		
							availableQuantity = stock_total.availableQuantity;				
						}										
						row.warehouseName = StockUpdate.storeWarehouseMap.get(row.warehouseId).warehouseName;	
					}					
					row.stockQuantity = stockQuantity;
					row.availableQuantity = availableQuantity;
		    		row.convfact = product.convfact;
		    		row.quantity =  formatQuantity(row.quantity,product.convfact);
		    		row.quantityValue = getQuantity(row.quantity,product.convfact);
		    		if(isNaN(row.quantityValue)){
          				row.quantityValue = 0;
          			} 
		    		$('#dg').datagrid('refreshRow', index);
		    	}	    		
	    	},
	    	onBeforeEdit :function(rowIndex,rowData){
		    	var rows = $('#dg').datagrid('getRows');
		    	for(var i=0;i<rows.length;++i){
		    		if(i==rowIndex)
		    			continue;
		    		$('#dg').datagrid('endEdit', i);    		
		    	}	
				var ed = $('#dg').datagrid('getEditor', {index:rowIndex,field:'warehouseId'});
				if(ed != null){
					$(ed.target).combobox('loadData',$(ed.target).combobox('getData'))
				}
				
			},
			onDblClickRow:function(rowIndex, rowData) {
				if(!StockUpdate.isAuthorize){
					return false;
				}
				if(!StockUpdate.isValidShop) {
					return false;
				}
				$('.ErrorMsgStyle').html('').hide();				
//				var msg = Utils.getMessageOfRequireCheck('shopCode', jsp_common_shop_code);
//				if(msg.length>0){
//					$('#errMsg').html(msg).show();
//					return false; 
//				}	
				$('#dg').datagrid('beginEdit', rowIndex);	
				$('.datagrid-row-editing td[field=quantity] input').css('text-align','right');
				var productId = rowData.productId;
				var product = StockUpdate.lstProductMap.get(productId);
				if(!isNullOrEmpty(rowData.productId) && product!=null){					
					StockUpdate.disabledProductIdCols(rowIndex, product);
					if(product.checkLot!=1){
		    			$('.datagrid-row-editing td[field=lot] table').css('display','none');
		    		}else{
		    			$('.datagrid-row-editing td[field=lot] table').css('display','block');						    			
		    		}
				}else{
					$('.datagrid-row-editing td[field=productId] input').focus();
				}
			},
	    	onLoadSuccess: function() { 
	    		//Xu ly phan quyen va Hien thi
	    		var arrDelete =  $('#idGridDg td[field="delete"]');
	    		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
	    		  for (var i = 0, size = arrDelete.length; i < size; i++) {
	    			$(arrDelete[i]).prop("id", "gr_table_td_del_" + i);//Khai bao id danh cho phan quyen
	    			$(arrDelete[i]).addClass("cmsiscontrol");
	    		  }
	    		}
	    		Utils.functionAccessFillControl('idGridDg', function(data) {
	    			//Xu ly cac su kien lien quan den cac control phan quyen
	    			var arrTmpLength =  $('#idGridDg td[id^="gr_table_td_del_"]').length;
	    			var invisibleLenght = $('.isCMSInvisible[id^="gr_table_td_del_"]').length;
	    			if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
	    				$('#dg').datagrid("showColumn", "delete");
	    			} else {
	    				$('#dg').datagrid("hideColumn", "delete");
	    			}
	    		});
//	    		if ($('#idGridDg td[field="delete"] a').length == 0) {
//	    			$('#dg').datagrid("hideColumn", "delete");
//	    		}
	    	},
	    	onLoadError: function(){
	    	}, 
	    	onBeforeLoad: function(){
	    	}

		});	
		
	},
	synchronization:function(){
		$('.ErrorMsgStyle').html('').hide();
		var typeUpdate = $('#typeUpdate').val().trim();	
		var shopCode = $('#shopCbx').combobox('getValue').trim();
		if(shopCode.length==0){
			return false;
		}
		var params = new Object();
		params.typeUpdate = typeUpdate;
		params.shopCode = shopCode.trim();
		Utils.getJSONDataByAjax(params,'/stock/update/init-data-product',function(data){
			$('#common-dialog-search-2-textbox').dialog('close');
			if(!data.error) {
				StockUpdate.lstProductUpdate = new Array();
				StockUpdate.lstProductUpdate.push(new Object());
				
				StockUpdate.lstProductArray = new Array();
				StockUpdate.lstProductArray = data.rows;
				var len = data.rows.length;
				var products = data.rows;
				for(var i=0;i<len;++i){
					StockUpdate.lstProductCodeMap.put(products[i].productCode,products[i]);
					StockUpdate.lstProductMap.put(products[i].productId,products[i]);
				}
				StockUpdate.storeStockTotalMap = new Map();
				// Dong bo ton kho NPP
				var stock_total = data.stock_total;
				for(var i = 0, size = stock_total.length; i< size ; ++ i){
					var productCode = stock_total[i].productCode;
					var warehouseId = stock_total[i].warehouseId;
					StockUpdate.storeStockTotalMap.put(productCode + warehouseId,stock_total[i]);
				}
				StockUpdate.lstWarehouse = data.warehouses;
				StockUpdate.storeWarehouseMap = new Map();
				for(var i = 0, size = StockUpdate.lstWarehouse.length; i < size; ++i){
					var warehouse = StockUpdate.lstWarehouse[i];
					var text = Utils.XSSEncode(warehouse.warehouseCode + ' - ' + warehouse.warehouseName);
					warehouse.searchText = unicodeToEnglish(text).toUpperCase();
					StockUpdate.storeWarehouseMap.put(warehouse.id,warehouse);
				}
				$('#shopId').val(data.shop.id);
				$('#isShop').val(1);	
				$('#issuedDate').val(data.dateStr);			
					StockUpdate.isValidShop = true; // check co du lieu product
					StockUpdate.loadGridProduct();
					StockUpdate.indexEditing = 0;							
					$('#dg').datagrid('beginEdit', 0);
			} else {
				$('#shopId').val('');
				$('#errMsg').html(data.errMsg).show();
				$('#isShop').val(0);
			}
		});		
	},
	disabledProductIdCols:function(editIndex,product){
		var productCode = product.productCode;
		$('#span' +editIndex).remove();			
		$('td[field=productId] div table tbody tr td input').css('display','none');
		$('td[field=productId] div table tbody tr td span').css('display','none');
		$('td[field=productId] div table tbody tr td').append('<span style="padding-left: 4px;" id="span' +editIndex+'"></span');
		$('#span' +editIndex).html(Utils.XSSEncode(productCode));		
		$('.datagrid-row-editing td[field=quantity] input').focus();
		if(product.checkLot!=1){
			$('.datagrid-row-editing td[field=lot] table').css('display','none');
		}else{
			$('.datagrid-row-editing td[field=lot] input').css('text-align','center');
		}
	},
	setEventInput:function(){
		$('td[field=productId] input.validatebox-text').live('keypress', function(e) {
			/*if(e.keyCode == keyCode_F9 && !e.shiftKey) {
				StockUpdate.openSelectProductDialog();
			}*/
			
			var productCode = $(this).val().trim().toUpperCase();
			if(e.keyCode == keyCode_TAB && !e.shiftKey) {	
				var product = StockUpdate.lstProductCodeMap.get(productCode);
				if(product!=null){
					StockUpdate.selectProductId = product.productId;				    			
		    		$('.datagrid-row-editing td[field=productName] div').html(Utils.XSSEncode(product.productName));
		    		if(product.checkLot!=1){
		    			$('.datagrid-row-editing td[field=lot] table').css('display','none');
		    		}else{
		    			$('.datagrid-row-editing td[field=lot] table').css('display','block');						    			
		    		}
		    		$('.ErrorMsgStyle').html('').hide();
				}		
			}
		});
		$('td[field=productId] input.validatebox-text').live('blur',function(e){
			var productCode = $(this).val().trim().toUpperCase();
			if(e.keyCode == keyCode_TAB && !e.shiftKey) {	
				var product = StockUpdate.lstProductCodeMap.get(productCode);
				if(product!=null){
					StockUpdate.selectProductId = product.productId;				    			
		    		$('.datagrid-row-editing td[field=productName] div').html(Utils.XSSEncode(product.productName));
		    		if(product.checkLot!=1){
		    			$('.datagrid-row-editing td[field=lot] table').css('display','none');
		    		}else{
		    			$('.datagrid-row-editing td[field=lot] table').css('display','block');						    			
		    		}
		    		$('.ErrorMsgStyle').html('').hide();
				}		
			}
		});
		$('.datagrid-row-editing td[field=quantity] input').live('focus', function(e){
			$(this).attr('id','convfact_value' +StockUpdate.indexEditing);			
			$(this).attr('maxlength','8');
			Utils.bindFormatOnTextfield('convfact_value' +StockUpdate.indexEditing, Utils._TF_NUMBER_CONVFACT);	
			$(this).css('text-align','right');
		});
		$('.datagrid-row-editing td[field=lot] input').live('focus', function(e){
			$(this).attr('id','lot' +StockUpdate.indexEditing);			
			$(this).attr('maxlength','6');
			Utils.bindFormatOnTextfield('lot' +StockUpdate.indexEditing, Utils._TF_NUMBER);	
			$(this).css('text-align','center');
		});
		$('.datagrid-row-editing td[field=lot] input').live('blur', function(e){
			var lot = $(this).val().trim();
			var row = $('#dg').datagrid('getRows')[StockUpdate.indexEditing];
			if(row!=null && row.checkLot==1){
				var msg = Utils.getMessageValidateLot('lot' +StockUpdate.indexLotEditing);
				if(msg.length>0){
					$('#errMsg').html(msg).show();
					var stm = setTimeout(function(){						
						$(this).focus();
						clearTimeout(stm);
					},50);
					return false;
				}
			}
		});
		$('.datagrid-row-editing td[field=lot] input').live('change', function(e){
			var lot = $(this).val().trim();
			var row = $('#dg').datagrid('getRows')[StockUpdate.indexEditing];
			if(row!=null && row.checkLot==1){
				var msg = Utils.getMessageValidateLot('lot' +StockUpdate.indexEditing);
				if(msg.length>0){
					$('#errMsg').html(msg).show();
					var stm = setTimeout(function(){						
						$(this).focus();
						clearTimeout(stm);
					},50);
					return false;
				}
			}
		});
		$('.datagrid-row-editing td[field=quantity] input').live('keyup', function(e){					
			if(e.keyCode==keyCodes.ENTER) {	
				$('#dg').datagrid('acceptChanges');
		    	StockUpdate.appendOrSelectRow();		    	
			}
		});		
		$('.easyui-dialog input.QUANTITY').live('keyup', function(e){					
			if(e.keyCode==keyCodes.ENTER) {
				var index = $(this).attr('index');
				var productLot = StockUpdate.lstProductSelectionLotMap.get(index);
				if(productLot!=null){			
					productLot.quantityLot = getQuantity($(this).val().trim(),StockIssued.CONVFACT);
					StockUpdate.lstProductSelectionLotMap.put(String(index),productLot);
				}
				var dg = '.easyui-dialog #productLotGrid';
				$(dg).datagrid('acceptChanges');
				var editIndex = $(dg).datagrid('getRows').length;			
		    	if(StockUpdate.indexLotEditing == editIndex-1) {    		
		    		var row = $(dg).datagrid('getRows')[editIndex-1];
		    		var product = StockUpdate.lstProductMap.get(row.productId);
		    		if(row==null || isNullOrEmpty(row.lot)){  
		    			editIndex = editIndex-1;
		    		}else if(!isNullOrEmpty(row.lot)){    			
		    			$(dg).datagrid('appendRow',new Object());
		    			$(dg).datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
		    			$('.datagrid-row-editing td[field=lot] input').focus();
		    			StockUpdate.indexLotEditing = editIndex;
		    			StockUpdate.lstProductSelectionLotMap.put(String(editIndex),new Object());
		    		}	
		    	}	    		    	
			}
		});	
	},
	appendOrSelectRow:function(){
		var editIndex = $('#dg').datagrid('getRows').length;			
    	if(StockUpdate.indexEditing == editIndex-1) {    		
    		var row = $('#dg').datagrid('getRows')[editIndex-1];
    		var product = StockUpdate.lstProductMap.get(row.productId);
    		if(row==null || isNullOrEmpty(row.productId)){  
    			editIndex = editIndex-1;
    		}else if(!isNullOrEmpty(row.productId)){    			
    			if(product!=null){
    				if(isNullOrEmpty(row.quantity) || StockIssued.validateQuantity(row.quantity,product.convfact)){
    					$('#dg').datagrid('appendRow',new Object());
    				}    				
    			}
    		}	
    	}else{
    		var row = $('#dg').datagrid('getRows')[StockUpdate.indexEditing];
    		var product = StockUpdate.lstProductMap.get(row.productId);
    		editIndex = StockUpdate.indexEditing +1;
    	}    	
    	StockUpdate.indexEditing = editIndex;
    	$('#dg').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
    	var productCode = $('.datagrid-row-editing td[field=productId] input.validatebox-text').val();
    	if(!isNullOrEmpty(productCode)){    		
    		var pr = $('#dg').datagrid('getRows')[editIndex];
    		StockUpdate.disabledProductIdCols(editIndex, pr);
    	}else{
    		$('.datagrid-row-editing td[field=productId] input').focus();
    	}     	
	},
	openSelectProductDialog: function(){
		$('#issSearchProductDialog').unbind('click');
		StockUpdate.storeProduct = new Map();	
		$('.SelProduct').each(function(){
			$(this).val('');
		});		
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('.ErrorMsgStyle').html('').hide();
//		var msg = Utils.getMessageOfRequireCheck('shopCode', jsp_common_shop_code);
//		if(msg.length>0){
//			$('#errMsg').html(msg).show();
//			return false; 
//		}		
		$('#productDialog').css("visibility", "visible");
		var html = $('#productDialog').html(); 
		$('#productEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        height :550,
	        onOpen: function(){
	        	$('.easyui-dialog #issSearchProductDialog').css('margin-left', 285);
				$('.easyui-dialog #issSearchProductDialog').css('margin-top', 10);	
				$('#issSearchProductDialog').bind('click',StockUpdate.searchProduct);
				$('.easyui-dialog #productCode').val('');
				$('.easyui-dialog #productName').val('');
				var shopCode = $('#shopCbx').combobox('getValue');
				var typeUpdate = $('#typeUpdate').val();
				var params = new Object();
				params.shopCode = shopCode,
				params.typeUpdate =  typeUpdate;				
				Utils.bindAutoSearch();	
	        	$('.easyui-dialog #productGrid').datagrid({
					url : '/stock/update/search-product',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					pageNumber:1,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					pageList  : [10,20,30],
					queryParams:params,					
					width : ($('.easyui-dialog #productGridContainer').width()),
				    columns:[[	
						{field: 'productCode', title: jsp_common_product_code, width: 160, sortable:false,resizable:false,align: 'left', formatter:function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          } },
						{field: 'productName',title: jsp_common_product_name, width: 320,sortable:false,resizable:false, align: 'left', formatter:function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          } },
						{field: 'stockQuantity',title: jsp_common_ton_kho,width: 80, sortable:false,resizable:false, align: 'right', 
							formatter: function(value,row,index){
					    		return formatQuantityEx(row.stockQuantity,row.convfact);
					    }},
						{field: 'warehouseName',title: jsp_common_warehouse,width: 150},
						{field: 'price',title: jsp_common_gia, width: 70,sortable:false,resizable:false, align: 'right', formatter: GridFormatterUtils.currencyCellFormatter},
						{field: 'quantity',title: jsp_common_so_luong,width: 100, sortable:false,resizable:false, align: 'right', formatter: UpdateStockGridFormatter.amountCellFormatter}									        
				    ]],
				    onBeforeLoad:function(param){
				    	$('.easyui-dialog .SelProduct').each(function(){
							  var index = $(this).attr('index');
							  var obj = StockUpdate.getData(index,$(this));										 
							  if(obj!=null){
								  StockUpdate.storeProduct.put(obj.productId,obj);  
							  }else {
								  var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
								  var temp = StockUpdate.storeProduct.get(rows.productId);
								  if(temp!=null){
									  StockUpdate.storeProduct.remove(temp.productId);
								  }
							  }
						  });	
				    },
				    onLoadSuccess :function(data){  
				    	$('.easyui-dialog #productCode').focus();
				    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);				    	
				    	Utils.bindFormatOnTextfieldInputCss('SelProduct',Utils._TF_NUMBER_CONVFACT,'.easyui-dialog');
						$('.easyui-dialog .SelProduct').each(function(){
							  var productId = $(this).attr('row-id');							 								  
							  var obj = StockUpdate.storeProduct.get(productId);
							  if(obj!=null) $(this).val(obj.quantity);
							  else $(this).val('');							
						});						
				    }
				});  
	        },	        
	        onClose : function(){
	        	$('#productDialog').html(html);
	        	$('#productDialog').css("visibility", "hidden");        	
	        }
	     });	
		$('#errMsg').hide();
		return false;
	},
	getData:function(index,selector){
		var product = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];	
		var quantity = selector.val().trim();
		if(quantity.length==0){			
			return null;
		}
		var row = new Object();
		row.productId = product.productId;
		row.productCode = product.productCode;
		row.productName = product.productName;
		row.stockQuantity = product.stockQuantity;		
		row.checkLot = product.checkLot;
		row.convfact = product.convfact;
		row.quantity = formatQuantityEx(quantity, row.convfact);
		row.quantityValue = getQuantity(row.quantity, row.convfact);
		if(isNaN(row.quantityValue)){
			return null;
		}		
		if (row.quantityValue <= 0) {
			row.quantity = '';
		}		
		return row;
	},	
	searchProduct: function(){
		$('.ErrorMsgStyle').html('').hide();
		var code = $('.easyui-dialog #productCode').val().trim();
		var name = $('.easyui-dialog #productName').val().trim();
		var shopCode = $('#shopCbx').combobox('getValue').trim();		
		var params = new Object();
		params.productCode = code;
		params.productName = name;
		params.shopCode = shopCode,
		params.typeUpdate =  StockUpdate.TYPE;
		params.isKeyUpF9 = true;
		$('.easyui-dialog #productGrid').datagrid('load',params);
		return false;
	},
	selectListProducts: function(){
		$('.ErrorMsgStyle').html('').hide();
		var mapProductSelection = new Map();
		$('.easyui-dialog #errMsg').html('').hide();
		var flag = true;
		var index = 0;
		var typeUpdate = $('#typeUpdate').val().trim();
		$('.easyui-dialog .SelProduct').each(function(){
			var index = $(this).attr('index');
			if($(this).val().length>0){				
				var obj = StockUpdate.getData(index,$(this));
				if(obj!=null){
					index = obj.productId;
					if(obj.quantity == '0/0' ||obj.quantity == '0'|| obj.quantity==''){										
						$('.easyui-dialog #errMsg').html(stock_update_gia_tri_so_luong_err).show();
						flag = false;
						return false;
					}						
					if(obj.stockQuantity < obj.quantityValue && StockUpdate.TYPE==1){
						$('.easyui-dialog #errMsg').html(jsp_common_product_code + ' ' + Utils.XSSEncode(obj.productCode + ' - ' + obj.productName) + '. ' + stock_update_vuot_qua_gia_tri_ton_kho).show();
						flag = false;						
						return false;			
					} 				
					mapProductSelection.put(obj.productId, obj);
					var storeObject = StockUpdate.storeProduct.get(obj.productId);
					if(storeObject!=null){
						StockUpdate.storeProduct.remove(obj.productId);
					}
				}				
			}else{
				var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
				var temp = StockUpdate.storeProduct.get(rows.productId);
				if(temp!=null){
					StockUpdate.storeProduct.remove(temp.productId);
				}
			}
		});
		if(!flag){	
			 $('#productRow_' + index).focus();
			return false;
		}
		try{
			for(var i=0;i<StockUpdate.storeProduct.size();i++){
				var product = StockUpdate.storeProduct.get(StockUpdate.storeProduct.keyArray[i]); 
				mapProductSelection.put(product.productId, product);
			}
			if(mapProductSelection.size()==0){
				$('.easyui-dialog #errMsg').html(stock_update_chon_sp_xuat_kho).show();
				return false;
			}	
			for(var i=0;i<mapProductSelection.size();i++){
				var data = mapProductSelection.get(mapProductSelection.keyArray[i]); 
				var len = $('#dg').datagrid('getRows').length-1;
				$('#dg').datagrid('insertRow',{index:len, row:data});
			}		
			var arrDelete = StockUpdate.duplicateProducts();
			for(var i=0;i<arrDelete.length;++i){
				var obj = arrDelete[i];
				var row = $('#dg').datagrid('getRows')[obj.lastIndex];			
				$('#dg').datagrid('getRows')[obj.firstIndex].quantity = row.quantity;
				$('#dg').datagrid('getRows')[obj.firstIndex].quantityValue = row.quantityValue;			
				$('#dg').datagrid('deleteRow',obj.lastIndex);					
			}
		}
		catch(e){
			$('#dg').datagrid('loadData', $('#dg').datagrid('getRows'));
			$('.easyui-dialog').dialog('close');
		}
		$('#dg').datagrid('loadData', $('#dg').datagrid('getRows'));
		$('.easyui-dialog').dialog('close');
	},
	duplicateProducts:function(){
		var arrDelete = new Array();
		var len = $('#dg').datagrid('getRows').length;
		var mapTestingDuplicate = new Map();
		var mapDuplicateIndex = new Map();
		for(var i=0;i<len;++i){			
			var product = $('#dg').datagrid('getRows')[i];
			var productId = product.productId;
			if(product.checkLot==1){
				continue;
			}
			if(mapTestingDuplicate.get(productId)==null){
				mapTestingDuplicate.put(productId,product);		
				mapDuplicateIndex.put(productId,i);
			}else{
				var obj = new Object();
				obj.firstIndex = mapDuplicateIndex.get(productId);
				obj.lastIndex = i;
				arrDelete.push(obj);
			}			
		}
		return arrDelete;
	},
	qtyChanged:function(index){
		$('#errMsg').html('').hide();
		var msg = '';
		var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];	
		var selector =  $('#productRow_' + rows.productId);
		var amount = formatQuantity(selector.val().trim(),rows.convfact);		
		if(amount == '0/0' || amount == '0'){
			msg = stock_update_gia_tri_sl_khong_hop_le;					
			return false;
		}
		var productCode = rows.productCode;
		var productName = rows.productName;	
		var quantityInput = Number(getQuantity(amount,rows.convfact));
		var stockQuantity = rows.stockQuantity;
		if(msg.length==0 && StockUpdate.TYPE==1 && quantityInput>stockQuantity){
			msg = jsp_common_product_code + ' ' + Utils.XSSEncode(productCode + ' - ' + productName) + '. ' + stock_update_vuot_qua_gia_tri_ton_kho;	
		}	
		if(msg.length>0){
			$('.easyui-dialog #errMsg').html(msg).show();
			var tm = setTimeout(function() {	
				selector.focus();			
				clearTimeout(tm);
			}, 50);
			return false;
		}
		selector.val(amount);
	},
	
	typeUpdateChange:function(){
		var value = $('#typeUpdate').val().trim();
		var value2 = $('#typeUpdateHide').val().trim();
		if(parseInt(value) === -1) {
			$('.easyui-dialog #errMsg').html(stock_update_chon_loai_don_dieu_chinh).show();
			return;
		}
		if(parseInt(value)!=parseInt(value2)){
			$('#typeUpdateHide').val(value);
			StockUpdate.synchronization();
			StockUpdate.lstProductLotMap = new Map();
		}
		if(parseInt(value)!= 0){
			$('#code').val('DCG');
		}else{
			$('#code').val('DCT');
		}
	},
	
	delSelectedRow:function(index){	
		$.messager.confirm(jsp_common_xacnhan, stock_update_confirm_delete_product, function(r){
			if (r){
				var id = $('#dg').datagrid('getRows')[index].productId;
				$('#dg').datagrid('deleteRow',index);				
				var rows = $('#dg').datagrid ('getRows');
				$('#dg').datagrid('loadData',rows);
				//del to pupup - hunglm16
				var lst = StockUpdate.lstProductLotMap.keyArray;
				for(var i=0; i<lst.length; i++){
				    var productDelete =  lst[i].toString().trim();
				    if(id==productDelete){
				        var delKeyArr = lst[i];
				        var delValArr = StockUpdate.lstProductLotMap.valArray[i];
				        StockUpdate.lstProductLotMap.keyArray = jQuery.grep(StockUpdate.lstProductLotMap.keyArray, function(value) {
				          return value != delKeyArr;
				        });
				        StockUpdate.lstProductLotMap.valArray = jQuery.grep(StockUpdate.lstProductLotMap.valArray, function(value) {
				          return value != delValArr;
				        });
				        if(StockUpdate.lstProductLotMap.keyArray ==null || StockUpdate.lstProductLotMap.keyArray.length<1){
				        	StockUpdate.lstProductLotMap = new Map();
				        }
				        break;
				    }
				}
				if(rows.length == 0){
					$('#dg').datagrid('insertRow', {index: 0,row:{}});
					$('#dg').datagrid('beginEdit', 0);
				}
			}
		});		
	},
	updateStock:function(){
		// check dang xu ly - tranh loi double click 
		if (StockUpdate.isProcessing) {
			return false;
		}
		setTimeout(function() {
			StockUpdate.isProcessing = false;
		}, 400);
		StockUpdate.isProcessing = true;
		
		$('.ErrorMsgStyle').html('').hide();
		var msg = "";
		var isShop = $('#isShop').val().trim();
		if(isShop!=undefined && isShop.length>0 && parseInt(isShop)==0){
			msg = msgCommonShop1;
			$('#errMsg').html(msg).show();
			return false;
		}
		
		$('#dg').datagrid('acceptChanges');		
		var shopId = $('#shopId').val().trim();
		var code = $('#code').val().trim();
		var issuedDate = $('#issuedDate').val().trim();
		var staffId = $('#staffId').val().trim();		
		var shopCode = $('#shopCbx').combobox('getValue').trim();
		var typeUpdate = $('#typeUpdate').val().trim();			
//		msg = Utils.getMessageOfRequireCheck('shopCode', jsp_common_shop_code);
		/**if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('code', 'Mã phiếu');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã phiếu', Utils._CODE);
		}
		**/
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('issuedDate', jsp_common_ngay_n);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('issuedDate', jsp_common_ngay_n);
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}				
		var isError = true;
		var index = 0;
		var lstProduct = {};
		lstProduct.selectedId = new Array();
		lstProduct.selectedQty = new Array();
		lstProduct.selectedLot = new Array();		
		lstProduct.selectedObject = new Array();
		lstProduct.lstWarehouse_Id = new Array();
		
		var rows = $('#dg').datagrid('getRows');		
    	if(rows.length>=1){
    		var row = $('#dg').datagrid('getRows')[0];
    		if(isNullOrEmpty(row.productId)){
    			$('#errMsg').html(stock_update_khong_co_sp_duoc_dieu_chinh).show();
    			return false;
    		}			
		}	
    	msg = '';
    	var dem = 0;
		for(var i = 0; i < rows.length; i++) {
			var row = rows[i];			
			if(isNullOrEmpty(row.productId)) continue;			
	
			if(row.warehouseId == null || row.warehouseId == ''){
				msg = stock_update_chua_chon_kho_sp_can_nhap_xuat;
			}
			if(row.productCode!=undefined && row.productCode!=null){
				if(!isNaN(row.quantityValue) && row.quantityValue>0){
					var stockTotal = StockUpdate.storeStockTotalMap.get(row.productCode + row.warehouseId);
					if(StockUpdate.TYPE==1 && stockTotal!=null && stockTotal.availableQuantity<row.quantityValue){
						msg = formatString(stock_update_quantity_more_than_avai_quantity, Utils.XSSEncode(row.productCode));
					}				
				}else{
					msg = stock_update_quantity_error;
				}				
			}else{
				msg = stock_update_product_code_invalid;
			}	
			if(msg.length==0 && row.checkLot == 1) {
				var productLots = StockUpdate.lstProductLotMap.get(row.productId);
				if(productLots==null || productLots.length==0){
					msg = formatString(stock_update_yeu_cau_nhap_lo, Utils.XSSEncode(row.productCode));
				}else{
					var _quantityRow = 0;
					for(var t=0; t<productLots.length; t++){
						var pl = productLots[t];
						if(isNaN(pl.quantityLot) || pl.quantityLot<0)
							continue;
						if(pl.quantityLot>0){
							lstProduct.selectedId.push(row.productId);
							lstProduct.selectedQty.push(pl.quantityLot);
							lstProduct.selectedLot.push(pl.lot);
						}
						_quantityRow = _quantityRow + pl.quantityLot;
					}
					lstProduct.selectedObject.push(i);
					if(_quantityRow!=row.quantityValue){
						msg = formatString(stock_update_tong_sl_nhap_lo_err, Utils.XSSEncode(row.quantityValue));
					}
				}
			} else {
				lstProduct.selectedId.push(row.productId);
				lstProduct.selectedQty.push(row.quantityValue);								
				lstProduct.selectedLot.push('');
				lstProduct.selectedObject.push(i);
				lstProduct.lstWarehouse_Id.push(row.warehouseId);
			}
			if(msg.length==0){
				dem = 0;
				for(var j = rows.length-1; j >=0; j--) {
					if(rows[i].productId == rows[j].productId && row.warehouseId == rows[j].warehouseId){
						dem++;
					}
				}
				if(dem>1){	
					msg = format(stock_update_ma_sp_cung_kho_hon_mot_dong, Utils.XSSEncode(row.productCode));
				}
			}
			if(msg.length>0){
				$('#dg').datagrid('selectRow',i);
				$('#errMsg').html(msg).show();					
				return false;
			}
 		}		
		var params = new Object();
		//params.shopId = shopId;
		params.shopCode = $('#shopCbx').combobox('getValue').trim();
		params.code = $('#hiddenCode').val().trim();
		params.issuedDate = issuedDate;
		params.staffId = staffId;
		params.typeUpdate = parseInt(typeUpdate);			
		params.lstProduct_Id = lstProduct.selectedId;
		params.lstProduct_Qty = lstProduct.selectedQty;
		params.lstProduct_Lot = lstProduct.selectedLot;
		params.selectedObject = lstProduct.selectedObject;
		params.lstWarehouse_Id = lstProduct.lstWarehouse_Id
		$.ajax({
			type : "POST",
			url : '/stock/update/stock-check-lot',
			data :($.param(params, true)),
			dataType : "json",
			success : function(result) {
				Utils.addOrSaveData(params, '/stock/update/updateStock', StockUpdate._xhrSave, 'errorMsg', function(result){
					if(!result.error){
						StockUpdate.afterModifier();
						$('#code').val($('#hiddenCode').val().trim());
						if(result.stockTransId!=undefined && result.stockTransId!=null){
							$('#stockTransId').val(result.stockTransId);
							$('#code').val(result.transCode);
						}
					}
				}, 'loading', '', false, 'Bạn có muốn điều chỉnh không?',function(data){
					var errorMsg = data.errMsg;
					if(data.lot_id!=null && data.lot_id !=undefined && data.lot_id.length>0){							
						$('#dg').datagrid('selectRow',data.lot_id);
					}
					$('#errorMsg').html(errorMsg).show();
				});
			}			
		});		
	},
	saveData:function(data){
		Utils.saveData(data, '/stock/update/updateStock', StockUpdate._xhrSave, 'errorMsg', function(result){
			if(!result.error){
				StockUpdate.afterModifier();
				if(result.stockTransId!=undefined && result.stockTransId!=null){
					$('#stockTransId').val(result.stockTransId);
				}
			}
		}, 'loading', '', false, stock_update_confirm_adjust,function(data){
			var errorMsg = data.errMsg;
			if(data.lot_id!=null && data.lot_id !=undefined && data.lot_id.length>0){
				$('#' + data.lot_id).focus();
			}
			$('#errMsg').html(errorMsg).show();	
		});
	},
	afterModifier:function(){	
		$('#errMsg').hide();
		enable('btnExportExcel_');
		$('#btnExportExcel_').bind('click',StockUpdate.printStockModifier);
		$('#shopCbx').combobox('disable');
		$('#shopCbx').parent().addClass('BoxDisSelect');
		disabled('code');
		disabled('issuedDate');				
		disabled('btnUpdate_');
		$('#typeUpdate').attr('disabled','disabled');$('#typeUpdate').parent().addClass('BoxDisSelect');					
		$('td[field=delete] a').removeAttr('onclick');
		$('.link_lot').removeAttr('onclick');							
		$('#successMsg').html(msgCommon1).show();
		var tm = setTimeout(function(){$('#successMsg').html('').hide();clearTimeout(tm);}, 3000);
		StockUpdate.isAuthorize = false;
	},
	printStockModifier:function(){		
		var data = new Object();
		data.shopCodeTextField = $('#shopCbx').combobox('getValue').trim();	
		data.code = $('#code').val().trim();
		var type = $('#typeUpdate').val();
		if(type == 0){
			data.transType = "NHAP_KHO_DIEU_CHINH";
		}else if(type == 1){
			data.transType = "XUAT_KHO_DIEU_CHINH";
		}
		try {
			data.reportCode = $('#function_code').val();
		} catch(e) {
		}
		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/stock/update/printStockModifier",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					var filePath = ReportUtils.buildReportFilePath(data.path);
					window.open(filePath);
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
		

	},
	changeLot:function(selector){
		var LOT_OBJECT = $(selector).attr('id');
		var msg = Utils.getMessageValidateLot(LOT_OBJECT);
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			var tm = setTimeout(function() {	
				$(selector).focus();			
				clearTimeout(tm);
			}, 50);
			return false;
		}		
	},
	nextAndPrevTextField: function(e,selector, clazz) {
		if(e.keyCode == keyCodes.ARROW_DOWN || e.keyCode == keyCodes.ENTER) {
			var index = $(selector).attr('index');
			if(index == $('#gridProduct').datagrid('getRows').length - 1) {
				if(clazz == 'AmountProduct'){
					var i = $('#gridProduct').datagrid('getRows').length;
					$('#gridProduct').datagrid('insertRow', {index: i,row:{}});
					$('#gridProduct').datagrid('beginEdit', i);
					setTimeout(function() {
						$('tr[datagrid-row-index=' +i+']').find('td[field=productId] .combo-text.validatebox-text').focus();
					}, 500);
				}
			} else {
				if(clazz == 'LotProduct') {
					$('.LotProduct[index=' +(index+1)+']').focus();
				} else {
					$('.AmountProduct[index=' +(index+1)+']').focus();
				}
			}
		} else if(e.keyCode == keyCodes.ARROW_UP) {
			var index = $(selector).attr('index');
			if(index == 0) {				
			} else {
				if(clazz == 'LotProduct') {
					$('.LotProduct[index=' +(index-11)+']').focus();
				} else {
					$('.AmountProduct[index=' +(index-1)+']').focus();
				}
			}
		} else if(e.keyCode == keyCodes.ARROW_RIGHT) {
			if(clazz == 'LotProduct') {
				var index = $(selector).attr('index');
				$('.AmountProduct[index=' +index+']').focus();
			} 
		} else if(e.keyCode == keyCodes.ARROW_LEFT) {
			if(clazz == 'AmountProduct') {
				var index = $(selector).attr('index');
				$('.LotProduct[index=' +index+']').focus();
			}
		}
	},
	chooseLotDialog:function(index){
		$('.ErrorMsgStyle').html('').hide();
		var productSelection = $('#dg').datagrid('getRows')[index];
		if(productSelection==null || isNaN(productSelection.quantityValue)){
			return;
		}		
		StockIssued.PRODUCT_ID = productSelection.productId;
		StockIssued.PRODUCT_TOTAL = productSelection.quantityValue;
		StockUpdate.lstProductSelectionLotMap = new Map();
		$('#productLotDialog').css("visibility", "visible");
		var html = $('#productLotDialog').html();	
		$('#productLotEasyUIDialog').dialog({closed: false,cache: false,modal: true,width : 600,height :470,onOpen: function(){	   
	        	StockIssued.request = false;
	        	$('#productLotDialog').html('');
				$('.easyui-dialog #productLotGrid').datagrid({
					view: bufferview,	
				    singleSelect:true,				   
				    height: 250,			      
				    fitColumns:true,				    
				    scrollbarSize:20,
					width: 550,					
				    columns:[[				          
				        {field: 'rownum',title: jsp_common_numerical_order, width: 30, sortable:false,resizable:false,align: 'center',
				        	formatter:function(value,row,index){
				        		if(isNullOrEmpty(row.lot))
				        			return '';
				        		return (index+1);
				        	}
				        },
				        {field: 'lot',title: jsp_common_so_lo, width: 80, sortable:false,resizable:false,align: 'center',
				        	editor:{type : 'text'}
				        },
					    {field: 'quantity',title: jsp_common_ton_kho,  width: 80,sortable:false,resizable:false, align: 'right',
				        	formatter:function(value,row,opts){
				        		return formatQuantity(row.quantity,StockIssued.CONVFACT);
				        	}
				        },
					    {field: 'quantityLot', title: jsp_common_so_luong, width: 80,sortable:false,resizable:false, align: 'right',
				        	formatter:function(value,row,opts){				        		
				        		var amount =  formatQuantity(row.quantityLot,StockIssued.CONVFACT);		
				        		if(StockIssued.request){				        			
				        			amount = '';
				        		}
				        		return '<input index=' +opts+' style="margin: 0px;text-align: right" onkeypress="return NextAndPrevTextField(event,this,\'QUANTITY\')" type="text" class="InputTextStyle InputText1Style QUANTITY" onblur="StockUpdate.onChange(this);" onchange="return StockUpdate.onChange(this);" quantity="' +row.quantity+'" value="' +amount+'"  id="QUANTITY' +opts+'"  />';
				        	}
					    },
				        {field:'delete', title: jsp_common_xoa, width:30, resizable:false, sortable:false, align:'center', 
							formatter: function(value, row, index) {
								if(isNullOrEmpty(row.lot) || row.id!=null || row.id!=undefined) 
									return '';
								return '<a href="javascript:void(0)" onclick="StockUpdate.delSelectedLotRow(' +index+');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a>';				
							}
						}
					   
				    ]],
				    onAfterEdit:function(rowIndex,rowData){
				    	StockUpdate.indexLotEditing = rowIndex;
				    	rowData.quantity = 0;				    					
				    	rowData.id = null;	
				    	rowData.quantityLot = StockUpdate.lstProductSelectionLotMap.get(String(rowIndex)).quantityLot;
				    	$('.easyui-dialog #productLotGrid').datagrid('refreshRow', rowIndex);
				    	if(rowData.quantityLot!=null && rowData.quantityLot!= undefined){
				    		var strtId = '#QUANTITY' +rowIndex.toString();
				    		if(rowData.quantityLot>0){
						    	$(strtId).val(formatQuantity(rowData.quantityLot,StockIssued.CONVFACT)).show();
				    		}else{
				    			$(strtId).val('').show();
				    		}
				    	}
			    	},
			    	onBeforeEdit :function(rowIndex,rowData){
			    		var rows = $('.easyui-dialog #productLotGrid').datagrid('getRows');
				    	for(var i=0;i<rows.length;++i){
				    		if(i==rowIndex)
				    			continue;
				    		$('.easyui-dialog #productLotGrid').datagrid('endEdit', i);    		
				    	}		    			    	
					},
					onClickRow:function(rowIndex, rowData) {
						if(StockUpdate.TYPE==1){
							return false;
						}
						if(!isNullOrEmpty(rowData.id))
							return false;						
						$('.easyui-dialog #productLotGrid').datagrid('beginEdit', rowIndex);	
					}
				});
				var shopCode = $('#shopCbx').combobox('getValue').trim();
				$.getJSON('/stock/update/search-lot?shopCode=' +shopCode+'&productId=' +StockIssued.PRODUCT_ID, function(result){					
					var product = result.product;
					if(product!=null){
						StockIssued.CONVFACT = product.convfact;						
						$('#fancyProductCode').html(Utils.XSSEncode(product.productCode));
						$('#fancyProductName').html(Utils.XSSEncode(product.productName));
						$('#fancyProductTotalQuantity').html(formatQuantityEx(StockIssued.PRODUCT_TOTAL,StockIssued.CONVFACT));						
					}	
					var lots = null;
					var list_product_lots = null;
					if(StockUpdate.lstProductLotMap!=null && StockUpdate.lstProductLotMap!=undefined && StockUpdate.lstProductLotMap.keyArray.length>0){
						list_product_lots = StockUpdate.lstProductLotMap.get(StockIssued.PRODUCT_ID);
					}
					if(list_product_lots!=null){
						lots = list_product_lots;
					}else{
						lots = result.lots;	
						StockIssued.request = true;
					}
					for(var i=0;i<lots.length;++i){
 						//loctt - Oct14, 2013
						if(StockUpdate.TYPE==1 && lots[i].availableQuantity == 0){
							lots.splice(i,0);
							continue;	
						}
						var productLot = StockUpdate.createProductLot(lots[i]);	
						$('.easyui-dialog #productLotGrid').datagrid('appendRow',productLot);
						StockUpdate.lstProductSelectionLotMap.put(String(i),productLot);
					}
					$('.easyui-dialog #productLotGrid').datagrid('acceptChanges');
					if(StockUpdate.TYPE==0){
						$('.easyui-dialog #productLotGrid').datagrid('appendRow',new Object());
						$('.easyui-dialog #productLotGrid').datagrid('acceptChanges');
						var len = $('.easyui-dialog #productLotGrid').datagrid('getRows').length; 
						StockUpdate.lstProductSelectionLotMap.put(String(len-1),new Object());
					}
					StockUpdate.showTotalAndScrollBodyProductLotDialog();
					Utils.bindFormatOnTextfieldInputCss('QUANTITY',Utils._TF_NUMBER_CONVFACT);
				});
	        },	        
	        onClose : function(){
	        	$('#productLotDialog').html(html);
	        	$('#productLotDialog').css("visibility", "hidden");
	        	$('#productLotGridContainer').html('<table id="productLotGrid" class="easyui-datagrid" ></table>');
	        }
	     });			
		$('#errMsg').hide();
		return false;
	},	
	createProductLot:function(productLot,convfact){
		var newrow = {
				lot:productLot.lot,
				quantity:productLot.quantity,
				quantityLot:productLot.quantityLot,
				id:productLot.id							
		};
		return newrow;		
	},
	onChange:function(selector){
		$('#fancyboxError').html('').show();
		var isError = false;
		$('.easyui-dialog .QUANTITY').each(function(){
			var selector = $(this);
			var quantityLot = getQuantity($(selector).val().trim(), StockIssued.CONVFACT);
			var quantity = $(selector).attr('quantity');			
			if(isNaN(quantityLot)){	
				isError = true;
				$('#fancyboxError').html(stock_update_lot_quantity_invalid).show();
				setTimeout(function() {$(selector).focus();}, 50);	
				return false;
			}
			if(StockUpdate.TYPE==1 && quantity<quantityLot){	
				isError = true;
				$('#fancyboxError').html(stock_update_lot_quantity_more_than_stock).show();
				setTimeout(function() {$(selector).focus();}, 50);	
				return false;
			}
			$(selector).val(formatQuantity($(selector).val().trim(), StockIssued.CONVFACT));
			if($(selector).val().length==0){			
				$(selector).val('');
			}
		});
		if(isError){
			return false;
		}	
		var index = $(selector).attr('index');
		var productLot = StockUpdate.lstProductSelectionLotMap.get(String(index));
		if(productLot!=null){			
			productLot.quantityLot = getQuantity($(selector).val().trim(),StockIssued.CONVFACT);
			StockUpdate.lstProductSelectionLotMap.put(String(index),productLot);
		}
		StockUpdate.showTotalAndScrollBodyProductLotDialog();
		return true;
	},
	
	erroMsgProductLotGrid:function(stt, msg){
		$('#errMsg').html('').show();
		$('.easyui-dialog #productLotGrid').datagrid('selectRow',stt);
    	$('#fancyboxError').html(msg).show();
    	setTimeout(function(){						
    		$('#fancyboxError').html('').show();
		},4000);
		
	},
	
	acceptSeparation:function(){
		var productId = StockIssued.PRODUCT_ID;		
		var list_product_lots = new Array();
		var sum_quantity = 0;
		$('#errMsg').html('').show();
		var dg = '.easyui-dialog #productLotGrid';
		$(dg).datagrid('acceptChanges');
		var rows = $('.easyui-dialog #productLotGrid').datagrid('getRows');
		var dem = 0;
		var msg = '';
		//Chect LOT Format
		for(var f=rows.length-1; f>=0; f--){
			if(rows[f].lot == undefined || rows[f].lot.length == 0){
				break;
			}
			msg = Utils.getMessageValidateLotNew(rows[f].lot);
			if(msg !='' && msg.length >0 ){
				StockUpdate.erroMsgProductLotGrid(f, msg);
				dem = 1;
				break;
			}
	    }
		var delKeyArr = null;
		StockUpdate.lstProductLotMap.keyArray = jQuery.grep(StockUpdate.lstProductLotMap.keyArray, function(value) {
	    	return value != delKeyArr;
	    });
	    StockUpdate.lstProductLotMap.valArray = jQuery.grep(StockUpdate.lstProductLotMap.valArray, function(value) {
	    	return value != delKeyArr;
	    });
	        
		if(dem!=0){
			return false;
		}
		dem = 0;
		//Chect LOT duplicate
		for(var j=rows.length-1;j>=0;j--){
		    for(var t=0; t<rows.length; t++){
		        if(rows[j].lot == rows[t].lot){
		              dem = dem + 1;
		        }
		    }
		    if(dem>1){
		    	msg = format(stock_update_exists_lot, rows[j].lot);
		    	StockUpdate.erroMsgProductLotGrid(j, msg);
		    	break;
		    }
		    dem=0;
		}
		if(dem!=0){
			return false;
		}
		for(var i=0;i<rows.length;++i){
			var productLot = rows[i];			
			if(isNullOrEmpty(productLot.lot))
				continue;	
			var quantity = getQuantity(productLot.quantityLot,StockIssued.CONVFACT);
			if(isNaN(quantity)){
				$('.easyui-dialog .QUANTITY').focus();
				$('#fancyboxError').html(format(stock_update_lot_quantity_invalid_ex, Utils.XSSEncode(productLot.lot))).show();
				return false;
			}
			if(StockUpdate.TYPE==1 && quantity>productLot.quantity){
				$('.easyui-dialog .QUANTITY').focus();
				$('#fancyboxError').html(format(stock_update_lot_quantity_more_than_stock_ex, Utils.XSSEncode(productLot.lot))).show();
				return false;
			}				
			productLot.quantityLot = quantity;
			list_product_lots.push(productLot);	
			sum_quantity+=Number(quantity);	
		}		
		var totalQuantity = getQuantity(StockIssued.PRODUCT_TOTAL, StockIssued.CONVFACT);
		if(totalQuantity!=sum_quantity){
			$('#fancyboxError').html(format(stock_update_tong_sl_nhap_lo_err, formatQuantity(StockIssued.PRODUCT_TOTAL, StockIssued.CONVFACT))).show();
			return false;
		}
		StockUpdate.lstProductLotMap.put(productId,list_product_lots);
		StockUpdate._quantityTemp = 0;
		$('.easyui-dialog').dialog('close');
	},
	showTotalAndScrollBodyProductLotDialog:function(){
		var totalQty = 0;
		var totalQtyLot = 0;	
		$('.easyui-dialog .QUANTITY').each(function(){
			var totalHTML = $(this).val().trim();
			try{
				if(Number(getQuantity(totalHTML, StockIssued.CONVFACT))>=0){
					totalQtyLot += Number(getQuantity(totalHTML, StockIssued.CONVFACT));
				}
				if(Number($(this).attr('quantity'))>=0){
					totalQty += Number($(this).attr('quantity'));
				}
			}catch(e){}
			$(this).css('text-align','right');			
			$(this).css('margin','0px');	
			$(this).css('width','123px');			
			
		});		
		$('#fancyboxError').html('').hide();		
		$('#totalQuantity').html(formatQuantity(totalQty,StockIssued.CONVFACT));
		$('#totalAvailableQuantity').html(formatQuantity(totalQtyLot,StockIssued.CONVFACT));		
	},
	delSelectedLotRow:function(index){
		$.messager.confirm(jsp_common_xacnhan, stock_update_confirm_delete_lot, function(r){
			if (r){
				$('.easyui-dialog #productLotGrid').datagrid('deleteRow',index);				
				var rows = $('.easyui-dialog #productLotGrid').datagrid ('getRows');
				$('.easyui-dialog #productLotGrid').datagrid('loadData',rows);
			}
		});		
	},
	selectShop: function(id, code) {
		var params = new Object();
		params.shopCode = code;
		loadDateLock(params, function() {
			$('#shopId').val(id);
			StockUpdate.synchronization();
		});
	},
	downloadTemplate:function() {
		var url = "/stock/update/download-template";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-update.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-update-order.js
 */
var StockUpdateOrder = {
	_isAllowShowPrice: true,
	_isLoad: true,
	xhrSave: null,
	/**
	 * @author vuongmq
	 * @param objectId
	 * @param index
	 * @param shortCode
	 * @param customerName
	 * @description Gan Ma khach hang vao objectId(customerCode)
	 */
	callSelectF9CustomerCode: function (index, shortCode, customerName) {
		$('#customerCode').val(shortCode.trim());
		$('#common-dialog-search-2-textbox').dialog("close");
	},
	searchOrder: function(){
		$('.ErrorMsgStyle').html('').hide();
		var shopCode = $('#shopCodeCB').combobox("getValue").trim();	
		var orderType = $('#orderType').val().trim();
		var orderNumber = $('#orderNumber').val().trim();
		var orderSource = $('#orderSource').val().trim();
		var shortCode = $('#customerCode').val().trim();
		var customerName = $('#customerName').val().trim();
		var saleStaffCode = $('#saleStaffCode').combobox('getValue').trim();
		var deliveryStaffCode = $('#deliveryStaffCode').combobox('getValue').trim();
		var params = new Object();
		params.shopCode = shopCode;
		params.orderType = orderType;
		params.orderNumber = orderNumber;
		params.orderSource =  orderSource;
		/*params.fromDate =  fDate;
		params.toDate =  tDate;*/
		params.code =  shortCode;
		params.name =  customerName;
		params.saleStaffCode =  saleStaffCode;
		params.deliveryStaffCode =  deliveryStaffCode;
		
		//Thêm dk gia trị
		params.isValueOrder = $('#isValueOrder').val().trim();
		var numberOrder = $('#numberValueOrder').val().trim();
		var numberValueOrder = "";
		if ( $('#numberValueOrder').val() != undefined &&  $('#numberValueOrder').val() != null &&  $('#numberValueOrder').val() !='') {
			numberValueOrder = numberOrder.replace(/,/g, '');
		}
		params.numberValueOrder = numberValueOrder;
		//-----------------------
		
		if(StockUpdateOrder._isLoad) {
			StockUpdateOrder.initGrid(params);
		} else {
			$('#grid').datagrid('uncheckAll');	
			$('#grid').datagrid('load',params);				
		}
		return false;
	},
	approveStockOrder: function(){
		var rows = $('#grid').datagrid('getChecked');
		var msg ='';
		if(rows.length <= 0) {
			msg = 'Không có đơn hàng nào được chọn.';
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		
		var listTemp = [];
		for(var i = 0; i< rows.length; i++) {
			if(rows[i].id != undefined) {
				var oSource = rows[0].orderSource;
				if(rows[0].orderSource == null) {
					oSource = 1; // trường hop cac don GO/DP/DCT/DGC/DC là trên web
				}
				listTemp.push({ id:rows[i].id, shopId: rows[i].shopId, staffId: rows[i].staffId, orderNumber:rows[i].orderNumber, orderType:rows[i].orderType, orderSource: oSource });
			}
		}
		params.listTemp = listTemp;
		var msgDialog = 'Bạn có muốn xác nhận các đơn hàng này không?';
		/*Utils.addOrSaveData(params, '/stock/update-order-stock/approveOrder', null,  'errMsg', function(data) {
			$('#successMsg').html("Lưu dữ liệu thành công").show();
			setTimeout(function(){
				$('#btnSearch').click();
				$('.SuccessMsgStyle').html("").hide();
			}, 1500);
		}, null, null, null,msgDialog);*/
		JSONUtil.saveData2(params, "/stock/update-order-stock/approveStockOrder", msgDialog, "errMsg", function(data) {
			if(!data.error) {
				$('#btnSearch').click();
				$('#successMsg').html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html("").hide();
				}, 1500);
				if(!Utils.isEmpty(data.errCode)){
					StockUpdateOrder.showDialogStockOrderError(data);
				}
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined && data.errMsg == 'DON_HANG_DA_XU_LY'){
				$('#errMsg').html('Đã có đơn hàng đã xử lý, yêu cầu kiểm tra lại').show();
				return;
			}
			StockUpdateOrder.showDialogStockOrderError(data);
			$('#btnSearch').click();
			/*if(data.errMsg != undefined) {
				$('#errMsg').html(data.errMsg).show();
			}*/
		});
		return false;
	},
	showDialogStockOrderError: function (data) {
		var html = $('#dialogStockOrder').html();
		var title = 'CÁC ĐƠN HÀNG BỊ LỖI, KHÔNG THỂ XÁC NHẬN'; 
		var orderNumber = 'Số đơn hàng';
		var stockQuantity = 'Không đủ kho sản phẩm';

		if (data.rows != null && data.rows.length > 0) {
			$('#dialogStockOrderEasyUIDialog').dialog({
				title:'CÁC ĐƠN HÀNG BỊ LỖI, KHÔNG THỂ XÁC NHẬN',
				closed: false,
				cache: false,
				position:'right',
				modal: true,
				//width : $(window).width()-350,
				width : $(window).width()-800,
				height :350,
				onOpen: function(){							
					//var html = new Array();
					$('#dgError').datagrid({
				        singleSelect: true,
				        data: data.rows,
				        autoRowHeight: true,       
				        width: $(window).width() - 845,
				        height: 230,
				       /* frozenColumns:[[
				           {field: 'orderNumber',title:'Số đơn hàng', width:200,align:'left',sortable : false,resizable : false}
				        ]],*/
				        columns:[[
				           {field: 'orderNumber',title:'Số đơn hàng', width:150,align:'left',sortable : false,resizable : false},
				           {field: 'stockQuantity',title:'Không đủ kho sản phẩm', width:350,align:'left',sortable : false,resizable : false},
				           /*{field: 'errOrder',title:'Lỗi xác nhận đơn hàng', width:150,align:'center',sortable : false,resizable : false},*/
				        ]],
				        onLoadSuccess :function(){
				        	$('#dialogStockOrderEasyUIDialog .datagrid-header').css('height','20px');
				        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row').css('height','20px');
				        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row div.datagrid-cell').css('word-wrap','break-word').css('height','20px');
				        }
					});
				},	        
		        onClose : function(){
		        	$('#btnConfirm').unbind('click');
		        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
		        	$('#dialogStockOrder').html(html);
		        	if (!isNullOrEmpty(data.errCode)) {
		    			StockUpdateOrder.showPopupUpdateStockTotal(data.errCode);
		    		}
		        }
		     });
		} else if (!isNullOrEmpty(data.errCode)) {
			StockUpdateOrder.showPopupUpdateStockTotal(data.errCode);
		}
	},

	/**
	 * show popup xac nhan cap nhat ton kho de link qua PO auto
	 * @author trietptm
	 * @since Oct 23, 2015
	 */
	showPopupUpdateStockTotal: function(errCode) {
		var title = 'CẬP NHẬT TỒN KHO';
		var msg = "Cập nhật tồn kho làm cho tồn kho các sản phẩm <span style=\"color:red;\">" + Utils.XSSEncode(errCode) + "</span> bé hơn tồn kho cần thiết.";
		msg += "Bạn có muốn đặt hàng cho các sản phẩm này không ?";
		$('#confirmMsg').html(msg).show();
		$('#dialogStockOrderEasyUIDialog #btnConfirm').bind('click', function() {
			window.location.href = '/po/init-po-auto/info';
		}).show();

		$('#dialogStockOrderEasyUIDialog').dialog({
			title: title,
			closed: false,
			cache: false,
			position: 'right',
			modal: true,
			//width : $(window).width()-350,
			width: $(window).width()-800,
			height:250,
			onOpen: function() {							
				
			},	        
	        onClose: function() {
	        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
	        	$('#dialogStockOrder').html(html);
	        }
	     });
	},

	/**
	 * Huy don DC, DCT, DCG
	 * @author lacnv1
	 */
	cancelStockTrans: function() {
		$(".ErrorMsgStyle").hide();

		var rows = $('#grid').datagrid('getChecked');
		if (rows.length <= 0) {
			$('#errMsg').html("Không có đơn hàng nào được chọn.").show();
			return false;
		}

		var lstId = [];
		var r = null;
		for (var i = 0, sz = rows.length; i < sz; i++) {
			r = rows[i];
			if (r.orderType != "DC" && r.orderType != "DCT" && r.orderType != "DCG") {
				$("#errMsg").html("Chỉ được hủy đơn DC, DCT, DCG").show();
				return false;
			}
			if (r.id != undefined && r.id != null && Number(r.id)) {
				lstId.push(r.id);
			}
		}

		$.messager.confirm("Xác nhận", "Bạn có muốn hủy (những) đơn " + $("#orderType").val().trim() + " này không?", function(y) {
			if (y) {
				var dataModel = { lstId : lstId };
				Utils.saveData(dataModel, "/stock/update-order-stock/cancel-stock-trans", StockUpdateOrder._xhrSave, "errMsg", function(data) {
					$('#btnSearch').click();
					if (data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0) {
						StockUpdateOrder.showDialogCancelStockTransErr(data.lstErr);
						setTimeout(function () {
							$("#successMsg").hide();
						}, 200);
					}
				});
			}
		});
	},
	showDialogCancelStockTransErr: function (lstErr){
		var html = $('#dialogStockOrder').html();
		$('#dialogStockOrderEasyUIDialog').dialog({
			title:'Các đơn hàng bị lỗi, không thể hủy',
			closed: false,
			cache: false,
			modal: true,
			width : 500,
			height :350,
			onOpen: function(){							
				//var html = new Array();
				$('#dgError').datagrid({
			        singleSelect:true,
			        data: { total: lstErr.length, rows: lstErr },
			        autoRowHeight : true,       
			        width: 450,
			        height:230,
			        fitColumns: true,
			        scrollbarSize: 0,
			        columns:[[
			           {field: 'orderNumber',title:'Số đơn hàng', width:220,align:'left',sortable : false,resizable : false},
			           {field: 'approve',title:'Trạng thái không hợp lệ', width:350,align:'left',sortable : false,resizable : false},
			           {field: 'quantityErr',title:'Tồn kho không hợp lệ', width:350,align:'center',sortable : false,resizable : false}
			        ]],
			        onLoadSuccess :function(){
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row div.datagrid-cell').css('word-wrap','break-word').css('height','20px');
			        }
				});
			},	        
	        onClose : function(){
	        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
	        	$('#dialogStockOrder').html(html);
	        }
	     });
	},
	
	initUnitCbx: function(){				
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				listNVBH = data.lstNVBH;
				$('#shopCodeCB').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			            StockUpdateOrder.getStaffByShopCode();
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shopCodeCB').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
			        	}
			        	
			        }
				});
			}
		});
	},
	
	/**
	*lay danh sach nhan vien theo shop don vi
	*/
	getStaffByShopCode: function() {
		 var shopCode = $('#shopCodeCB').combobox('getValue');
		 var params = new Object();
		if(shopCode != null){
			params.shopCode = shopCode;
		}
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/stock/update-order-stock/load-staff-by-shop-code',
			data : (kData),
			dataType : "json",
			success : function(data) {
				StockUpdateOrder._isAllowShowPrice = data.isAllowShowPrice;
				SPCreateOrder._isAllowShowPrice = data.isAllowShowPrice;
				if(StockUpdateOrder._isAllowShowPrice){
					$('#valueOrderDiv').show();
					$('#isValueOrder').val(-1);
					$('#isValueOrder').change();
				} else {
					$('#valueOrderDiv').hide();
				}
				//bo sung text box gia tri DH
				var numberValueOrder = data.numberValue;
				if(numberValueOrder != '' && numberValueOrder != "") {
					SPAdjustmentTax.numberValueOrder = numberValueOrder;
					SPAdjustmentTax._textChangeBySeach.avg = numberValueOrder.replace(/,/g, '');
				}
				
				$('#shopId').val(data.shopId);

				var lstNVBHVo = data.lstNVBHVo;
				var lstNVGHVo = data.lstNVGHVo;

				if(lstNVBHVo != undefined && lstNVBHVo != null) {
					Utils.bindStaffCbx('#saleStaffCode', lstNVBHVo, null, 206, null);
				}
				
				if(lstNVGHVo != undefined && lstNVGHVo != null) {
					Utils.bindStaffCbx('#deliveryStaffCode', lstNVGHVo, null, 206, null);
				}

				if(StockUpdateOrder._isLoad){
					StockUpdateOrder.searchOrder();
					StockUpdateOrder._isLoad = false;
	        	}		

			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Error");
			}
		});
	},
	
	initGrid: function(params) {
		$('#grid').datagrid({  
		    url:'/stock/update-order-stock/search',
		    rownumbers:true,
		    pagination: true,
		    //selectOnCheck:false,
		    singleSelect:false,
		    //scrollbarSize:0,
		    autoRowHeight : true,
		    //height: 300,
		    pageNumber : 1,
		    pageSize: 100,
		    pageList  : [10,50,100,200,500],
		    width: $(window).width() - 40,	    		    
		    /* queryParams: {data : $('#date').val(), priorityCode:$('#priority').val().trim(), orderSource:$('#orderSource').val().trim()}, */
		    queryParams: params,
		    columns: [[
				{field:'id', checkbox:true},
				{field:'link', title:'', width:50, fixed:true, align:'center', sortable:false, resizable:false, formatter : function(v,r,i) {
					var orderT = '';
					var orderType = '';
					if(r.orderType != undefined && r.orderType != null){
						orderType = '&orderType=' + r.orderType;
						orderT = r.orderType;
					}
					var str = '<a href="javascript:void(0);" onclick="SPPrintOrder.showDetailInPopup(' +r.id + ',\'' + orderT + '\',\'' + r.shopCode + '\');" title="Xem chi tiết" id="aEdit"><img height="17" src="/resources/images/icon-view.png"></a>';
					return str;
				}},
				{field:'orderNumber', title:'Số đơn hàng', width:110, align:'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(row.orderNumber);
				}},
				{field:'orderDate', title:'Ngày tạo', width:135, align:'center', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(row.strOrderDate);
				}},
				{field:'customer', title:'Khách hàng', width:190, align: 'left', formatter : function(value,row,index) {
					if(row.shortCode != null || row.customerName != null) {
						return VTUtilJS.XSSEncode(row.shortCode + ' - ' + row.customerName);
					} else {
						return '';
					}
				}},
				{field:'customerAddress', title:'Địa chỉ', width:200, align: 'left', formatter : function(value,row,index) {
		        	if(row.customerAddress != null) {
		        		return VTUtilJS.XSSEncode (row.customerAddress);
		        	} else {
		        		return '';
		        	}
		        }},	
				{field:'total', title:'Tổng tiền', width:100, align:'right', formatter : function(value,row,index) {
		        	if(row.total != null) {
		        		return formatCurrencyInterger(row.total);
		        	} else {
		        		return 0;
		        	}
		        }},
		        {field:'quantity', title:'Sản lượng', width:100, align:'right', formatter : function(value,row,index) {
		        	if(row.quantity != null) {
		        		return formatCurrencyInterger(row.quantity);
		        	} else {
		        		return 0;
		        	}
		        }},
		        {field:'refOrderNumber', title:'Số đơn đặt hàng', width:110, align: 'left', formatter : function(value,row,index) {
		        	if(row.refOrderNumber != null) {
		        		return VTUtilJS.XSSEncode(row.refOrderNumber);
		        	} else {
		        		return '';
		        	}
		        }},	
		        {field:'orderType', title:'Loại đơn', width:65, align: 'left', formatter : function(value,row,index) {
		        	if(row.orderType != null) {
		        		return VTUtilJS.XSSEncode(row.orderType);
		        	} else {
		        		return '';
		        	}
		        }},	
		        {field:'staff', title:'NVBH', width:195, align:'left',formatter:function(v,r,i){
		        	if(VTUtilJS.isNullOrEmpty(r.staffCode) && VTUtilJS.isNullOrEmpty(r.staffName)) {
						return '';
					} else if(VTUtilJS.isNullOrEmpty(r.staffCode)) {
						return VTUtilJS.XSSEncode(r.staffName);
					} else if(VTUtilJS.isNullOrEmpty(r.staffName)) {
						return VTUtilJS.XSSEncode(r.staffCode);
					}
					return VTUtilJS.XSSEncode(r.staffCode+'-' +r.staffName);
		        }},
		        {field:'delivery', title:'NVGH', width:195, align:'left',formatter:function(value,row,index){
		        	if(row.deliveryCode!=null || row.deliveryName){
		        		return VTUtilJS.XSSEncode(row.deliveryCode+' - ' +row.deliveryName);
		        	}
		        	return '';
		        }}
		       	        
		    ]],
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
			   	 updateRownumWidthForJqGrid('.easyui-dialog');
			   	if (StockUpdateOrder._isAllowShowPrice) {
		    		$('#grid').datagrid('showColumn', 'total');
		    	} else {
		    		$('#grid').datagrid('hideColumn', 'total');
		    	}			   	 
			   	var orderType = $("#orderType").val().trim();
			   	 if (orderType.length == 0 || orderType == "DC" || orderType == "DCG" || orderType == "DCT") {
			   		 $("#btnCancel:hidden").show();
			   	 } else {
			   		$("#btnCancel:not(:hidden)").hide();
			   	 }
			   	 
		   	 	$(window).resize();
		   	 	$(".datagrid-header-check input[type=checkbox]").removeAttr("checked");
		    }		    
		}); 
	}
}
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-update-order.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-update-approve.js
 */
var StockUpdateApprove = {
	_isAllowShowPrice: true,
	_isLoad: true,
	xhrSave: null,
	/**
	 * @author vuongmq
	 * @param objectId
	 * @param index
	 * @param shortCode
	 * @param customerName
	 * @description Gan Ma khach hang vao objectId(customerCode)
	 */
	callSelectF9CustomerCode: function (index, shortCode, customerName) {
			$('#customerCode').val(shortCode.trim());
			$('#common-dialog-search-2-textbox').dialog("close");
	},
	searchOrder: function(){
		$('.ErrorMsgStyle').html('').hide();
		var shopId = $('#shopId').val();
		if(shopId == null || shopId.length == 0){
			$('#errMsgInfo').html('Bạn chưa chọn giá trị cho trường Đơn vị').show();
			return false;
		}
		var shopId = $('#shopId').val();
		var orderType = $('#orderType').val().trim();
		var orderNumber = $('#orderNumber').val().trim();
		var params = new Object();
		params.shopId = shopId;
		params.orderType = orderType;
		params.orderNumber = orderNumber;
		if(StockUpdateApprove._isLoad) {
			StockUpdateApprove.initGrid(params);
		} else {
			$('#grid').datagrid('uncheckAll');	
			$('#grid').datagrid('load',params);				
		}
		StockUpdateApprove._isLoad = false;
		return false;
	},

	/**
	 * Duyet dieu chinh kho
	 * @author vuongmq
	 * @since 04/09/2015 
	 */
	approveStockOrder: function(){
		var rows = $('#grid').datagrid('getChecked');
		var msg ='';
		if (rows.length <= 0) {
			msg = 'Không có đơn hàng nào được chọn.';
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		
		var listTemp = [];
		for (var i = 0; i < rows.length; i++) {
			if (rows[i].id != undefined) {
				var oSource = rows[0].orderSource;
				if (rows[0].orderSource == null) {
					oSource = 1; // trường hop cac don GO/DP/DCT/DGC/DC là trên web
				}
				listTemp.push({ id:rows[i].id, shopId: rows[i].shopId, staffId: rows[i].staffId, orderNumber:rows[i].orderNumber, orderType:rows[i].orderType, orderSource: oSource });
			}
		}
		params.listTemp = listTemp;
		var msgDialog = 'Bạn có muốn xác nhận các đơn hàng này không?';
		JSONUtil.saveData2(params, "/stock/update-approve/approveStockOrder", msgDialog, "errMsg", function(data) {
			if(!data.error) {
				$('#btnSearch').click();
				$('#successMsg').html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html("").hide();
				}, 1500);
			}
		}, "", function (data) { // callBackFail
			StockUpdateApprove.showDialogStockOrderError(data);
			$('#btnSearch').click();
		});
		return false;
	},

	/**
	 * Duyet dieu chinh kho; thong bao loi popup danh sach don hang
	 * @author vuongmq
	 * @since 04/09/2015 
	 */
	showDialogStockOrderError: function (data){
		var html = $('#dialogStockOrder').html();
		$('#dialogStockOrderEasyUIDialog').dialog({
			title: 'CÁC ĐƠN HÀNG BỊ LỖI, KHÔNG THỂ XÁC NHẬN',
			closed: false,
			cache: false,
			position: 'right',
			modal: true,
			width: 600,
			height: 350,
			onOpen: function(){							
				$('#dgError').datagrid({
			        singleSelect: true,
			        data: data.rows,
			        autoRowHeight: true,       
			        width: 550,
			        height: 230,
			        fitColumns: true,
			        scrollbarSize: 0,
			       /* frozenColumns:[[
			           {field: 'orderNumber',title:'Số đơn hàng', width:200,align:'left',sortable : false,resizable : false}
			        ]],*/
			        columns:[[
			           {field: 'orderNumber', title: 'Số đơn hàng', width: 150, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			           {field: 'stockQuantity', title: 'Không đủ kho sản phẩm', width: 350, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			           {field: 'errOrder', title:'Lỗi xác nhận đơn hàng', width: 350, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			        ]],
			        onLoadSuccess: function() {
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row div.datagrid-cell').css('word-wrap','break-word').css('height','20px');
			        }
				});
			},	        
	        onClose: function(){
	        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
	        	$('#dialogStockOrder').html(html);
	        }
	     });
	},

	/**
	 * Huy don DC, DCT, DCG
	 * @author lacnv1
	 */
	cancelStockTrans: function() {
		$(".ErrorMsgStyle").hide();

		var rows = $('#grid').datagrid('getChecked');
		if (rows.length <= 0) {
			$('#errMsg').html("Không có đơn hàng nào được chọn.").show();
			return false;
		}

		var lstId = [];
		var r = null;
		for (var i = 0, sz = rows.length; i < sz; i++) {
			r = rows[i];
			if (r.orderType != "DC" && r.orderType != "DCT" && r.orderType != "DCG") {
				$("#errMsg").html("Chỉ được hủy đơn DC, DCT, DCG").show();
				return false;
			}
			if (r.id != undefined && r.id != null && Number(r.id)) {
				lstId.push(r.id);
			}
		}

		$.messager.confirm("Xác nhận", "Bạn có muốn hủy (những) đơn " + $("#orderType").val().trim() + " này không?", function(y) {
			if (y) {
				var dataModel = { lstId : lstId };
				Utils.saveData(dataModel, "/stock/update-approve/cancel-stock-trans", StockUpdateApprove._xhrSave, "errMsg", function(data) {
					$('#btnSearch').click();
					if (data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0) {
						StockUpdateApprove.showDialogCancelStockTransErr(data.lstErr);
						setTimeout(function () {
							$("#successMsg").hide();
						}, 200);
					}
				});
			}
		});
	},
	showDialogCancelStockTransErr: function (lstErr){
		var html = $('#dialogStockOrder').html();
		$('#dialogStockOrderEasyUIDialog').dialog({
			title:'Các đơn hàng bị lỗi, không thể hủy',
			closed: false,
			cache: false,
			modal: true,
			width: 500,
			height: 350,
			onOpen: function(){							
				//var html = new Array();
				$('#dgError').datagrid({
			        singleSelect:true,
			        data: { total: lstErr.length, rows: lstErr },
			        autoRowHeight: true,       
			        width: 450,
			        height: 230,
			        fitColumns: true,
			        scrollbarSize: 0,
			        columns:[[
			           {field: 'orderNumber', title:'Số đơn hàng', width: 220, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			           {field: 'approve', title: 'Trạng thái không hợp lệ', width: 350, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			           {field: 'quantityErr', title: 'Tồn kho không hợp lệ', width: 350, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			        ]],
			        onLoadSuccess :function(){
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row div.datagrid-cell').css('word-wrap','break-word').css('height','20px');
			        }
				});
			},	        
	        onClose : function(){
	        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
	        	$('#dialogStockOrder').html(html);
	        }
	     });
	},
	
	initUnitCbx: function(){
		//load combobox don vi
		$('#shopCodeCB').combotree({
			url: '/stock/update-approve/search-unit-tree',
			lines: true,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect: function(data){
	        	if (data != null && data.id > 0) {
	        		$('#shopId').val(data.id);
	        		StockUpdateApprove.getStaffByShopCode();
	        	}
		    },
		    onLoadSuccess: function(node, data) {
				$('#shopCodeCB').combotree('setValue', data[0].id);
			}
		});
	},
	
	/**
	*lay danh sach nhan vien theo shop don vi
	*/
	getStaffByShopCode: function() {
		 var shopId = $('#shopId').val();
		 var params = new Object();
		if(shopId != null){
			params.shopId = shopId;
		}
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/stock/update-approve/load-staff-by-shop-code',
			data : (kData),
			dataType : "json",
			success : function(data) {
				StockUpdateApprove._isAllowShowPrice = data.isAllowShowPrice;
				SPCreateOrder._isAllowShowPrice = data.isAllowShowPrice;
				if(StockUpdateApprove._isAllowShowPrice){
					$('#valueOrderDiv').show();
					$('#isValueOrder').val(-1);
					$('#isValueOrder').change();
				} else {
					$('#valueOrderDiv').hide();
				}
				//bo sung text box gia tri DH
				var numberValueOrder = data.numberValue;
				if(numberValueOrder != null && numberValueOrder != '' && numberValueOrder != "") {
					SPAdjustmentTax.numberValueOrder = numberValueOrder;
					SPAdjustmentTax._textChangeBySeach.avg = numberValueOrder.replace(/,/g, '');
				}

				var lstNVBHVo = data.lstNVBHVo;
				var lstNVGHVo = data.lstNVGHVo;

				if(lstNVBHVo != undefined && lstNVBHVo != null) {
					Utils.bindStaffCbx('#saleStaffCode', lstNVBHVo);
				}
				
				if(lstNVGHVo != undefined && lstNVGHVo != null) {
					Utils.bindStaffCbx('#deliveryStaffCode', lstNVGHVo);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Error");
			}
		});
	},
	
	initGrid: function(params) {
		$('#grid').datagrid({  
		    url: '/stock/update-approve/search',
		    rownumbers:true,
		    pagination: true,
		    singleSelect:false,
		    autoRowHeight : true,
		    pageNumber : 1,
		    pageSize: 100,
		    pageList  : [10,50,100,200,500],
		    width: $(window).width() - 40,	    		    
		    queryParams: params,
		    columns: [[
				{field:'id', checkbox:true},
				{field:'link', title:'', width:50, fixed:true, align:'center', sortable:false, resizable:false, formatter : function(v,r,i) {
					var orderT = '';
					var orderType = '';
					if(r.orderType != undefined && r.orderType != null){
						orderType = '&orderType=' + r.orderType;
						orderT = r.orderType;
					}
					var str = '<a href="javascript:void(0);" onclick="SPPrintOrder.showDetailInPopup(' +r.id + ',\'' + orderT + '\',\'' + Utils.XSSEncode(r.shopCode) + '\');" title="Xem chi tiết" id="aEdit"><img height="17" src="/resources/images/icon-view.png"></a>';
					return str;
				}},
				{field:'orderNumber', title:'Số đơn hàng', width:200, align:'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(row.orderNumber);
				}},
				{field:'orderType', title:'Loại đơn', width:100, align: 'left', formatter : function(value,row,index) {
		        	if(row.orderType != null) {
		        		return VTUtilJS.XSSEncode(row.orderType);
		        	} else {
		        		return '';
		        	}
		        }},
				{field:'orderDate', title:'Ngày tạo', width:100, align:'center', formatter: function(value, row, index) {
					return formatDate(row.orderDate);
				}},
		        {field:'quantity', title:'Sản lượng', width:100, align:'right', formatter : function(value,row,index) {
		        	if(row.quantity != null) {
		        		return formatFloatValue(row.quantity);
		        	} else {
		        		return 0;
		        	}
		        }},
		        {field:'fromWarehouse', title:'Xuất từ', width:350, align:'left',formatter:function(value,row,index){
		        	if(row.fromWarehouse!=null || row.fromShop){
		        		return VTUtilJS.XSSEncode(row.fromWarehouse+' - ' +row.fromShop);
		        	}
		        	return '';
		        }},
		        {field:'toWarehouse', title:'Nhập vào', width:350, align:'left',formatter:function(value,row,index){
		        	if(row.toWarehouse!=null || row.toShop){
		        		return VTUtilJS.XSSEncode(row.toWarehouse+' - ' +row.toShop);
		        	}
		        	return '';
		        }}
		       	        
		    ]],
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
			   	 updateRownumWidthForJqGrid('.easyui-dialog');			   	 
		   	 	$(window).resize();
		   	 	$(".datagrid-header-check input[type=checkbox]").removeAttr("checked");
		    }		    
		}); 
	}
}
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-update-approve.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-transfer-internal.js
 */
var TransferInternalWarehouse = {
	_mapStockTrans: null,
	_mapStockTransDetail: null,
	_flagG: null,
	_lockDate:null,
	_productCodeFirt:null,
	_indexTmp: null,
	_lstSelectedId: null,
	_arrDelete:null,
	_arrInsert:null,
	_txtIsJoinOderNumberC: null,
	_mapproduct: null,
	_mapProduct: null,
	_lstproductavailable:[],
	_rowDgTmp: null,
	_rowsDg: null,
	_mapProductDlSearch: null,
	_mapProductDl: null,
	_flagDialog: false,
	/**
	 * Xu ly su kien change Combobox
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	eventChangeComboboxWareHouse: function (wareHouseOutputId) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		TransferInternalWarehouse._rowDgTmp = {};
		Utils.getJSONDataByAjaxNotOverlay ({wareHouseOutputId: wareHouseOutputId, shopCodeOutput: $('#shopOutput').combobox('getValue')}, '/transfer-internal-warehouse/get-List-Product-In-WareHouse', function (data){
			TransferInternalWarehouse._mapProduct = new Map();
			TransferInternalWarehouse._mapStockTransDetail = new Map();
			TransferInternalWarehouse._lstproductavailable = new Array();
			if (data != undefined && data != null && data.rows != undefined && data.rows != null && data.rows.length >0) {
				for (var i = 0; i < data.rows.length; i++) {
					TransferInternalWarehouse._mapProduct.put(data.rows[i].productId, data.rows[i]);
					TransferInternalWarehouse._lstproductavailable.push(data.rows[i]);
				}
			}
			$('#prdStockOutputDg').datagrid({
				data: TransferInternalWarehouse.getDataInPrdStockOutputDg(),
				width : $('#prdStockOpDgContGrid').width() - 15,
				fitColumns : true,
			    rownumbers : true,
			    singleSelect:true,
			    height: 275,
			    columns:[[
					{field:'productCode',title:'Mã sản phẩm', width:120,align:'left',formatter: function(value, row, index){
						return Utils.XSSEncode(value);
					}, 
					editor: {
						type:'combobox',
							options:{ 
							valueField:'productId',
						    textField:'productCode',
						    data: TransferInternalWarehouse._lstproductavailable,
						    width: 200,
						    formatter: function(row) {
						    	return '<span style="font-weight:bold">' + Utils.XSSEncode(row.productCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.productName) + '</span>';
						    },
						    onSelect:function(rec){
						    	TransferInternalWarehouse._rowDgTmp = {
					    			productId: rec.productId,
									productCode: Utils.XSSEncode(rec.productCode),
									productName: Utils.XSSEncode(rec.productName),
									convfact: rec.convfact,
									warehouseId: rec.warehouseId,
									availableQuantity: rec.availableQuantity,
									grossWeight: rec.grossWeight,
									price: rec.price,
									quantity: 0,
									amount: 0
						    	};
						    	$('#dgProductName0').html(Utils.XSSEncode(rec.productName.trim())).change();
						    	$('#dgAvlQuantity0').val(StockValidateInput.formatStockQuantity(TransferInternalWarehouse._rowDgTmp.availableQuantity, TransferInternalWarehouse._rowDgTmp.convfact));
						    	$('#dgQuantity0').val(StockValidateInput.formatStockQuantity(0, TransferInternalWarehouse._rowDgTmp.convfact));
						    	$('#dgPrice0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.price)));
						    	$('#dgAmount0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
						    	
						    	$('#dgAvlQuantity0, #dgQuantity0').bind('keyup',function(event) {
						 			if (event.keyCode == keyCodes.ENTER) {
						 				if (TransferInternalWarehouse._rowDgTmp.quantity > 0) {
							 				TransferInternalWarehouse._mapStockTransDetail.put(TransferInternalWarehouse._rowDgTmp.productId, TransferInternalWarehouse._rowDgTmp);
							 				$('.ErrorMsgStyle').html('').hide();
							 				$('.SuccessMsgStyle').html('').hide();
							 				$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
						 				} else {
						 					if (TransferInternalWarehouse._rowDgTmp.productId <=0) {
						 						$('#errMsg').html('Sản phẩm chọn không hợp lệ').show();
						 					}
						 					if (TransferInternalWarehouse._rowDgTmp.quantity < 1) {
						 						$('#errMsg').html('Số lượng phải lớn hơn 0').show();
						 					}
						 					$('#dgQuantity0').focus();
						 				}
						 			}
						 		});
						    	TransferInternalWarehouse._needNewRow = true;
						    },
						    onChange:function(pId){
						    	var flag = false;
						    	if (pId != undefined && pId != null && pId.length > 0) {
	 					    		for (var i = 0; i < TransferInternalWarehouse._lstproductavailable.length; i++) {
	 					    			var rec = TransferInternalWarehouse._lstproductavailable[i];
	 					    			if (rec.productCode == pId.toUpperCase()) {
	 					    				$('#dgQuantity0').focus();
											TransferInternalWarehouse._rowDgTmp = {
									    			productId: rec.productId,
													productCode: Utils.XSSEncode(rec.productCode),
													productName: Utils.XSSEncode(rec.productName),
													convfact: rec.convfact,
													warehouseId: rec.warehouseId,
													availableQuantity: rec.availableQuantity,
													grossWeight: rec.grossWeight,
													price: (rec.price != undefined && rec.price != null)? rec.price: 0,
													quantity: (rec.quantity != undefined && rec.quantity != null)? rec.quantity: 0,
													amount: (rec.amount != undefined && rec.amount != null)? rec.amount: 0
										    	};
											
											TransferInternalWarehouse._mapStockTransDetail.put(rec.productId, TransferInternalWarehouse._rowDgTmp);
											$('.ErrorMsgStyle').html('').hide();
											$('.SuccessMsgStyle').html('').hide();
											var tm = setTimeout(function(){
												$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
												$('#dgQuantity' +rec.productId).focus();
//												$('#dgQuantity' +rec.productId).bind('keyup',function(event){
//													if (event.keyCode == keyCodes.ENTER) {
//														$('#btnTransferWarehouse').click();
//													}
//												});
												clearTimeout(tm);
											}, 10);
											TransferInternalWarehouse._needNewRow = false;
											$(this).combobox("hidePanel");
											break;
										}
	 					    		}
	 					    	}
						    },
						    filter: function(q, row){
								q = new String(q).toUpperCase();
								return row.productCode.indexOf(q)>=0;
							},
							onHidePanel: function() {
								if (!TransferInternalWarehouse._needNewRow) {
									return;
								}
								var pId = TransferInternalWarehouse._rowDgTmp.productId;
								TransferInternalWarehouse._mapStockTransDetail.put(TransferInternalWarehouse._rowDgTmp.productId, TransferInternalWarehouse._rowDgTmp);
				 				$('.ErrorMsgStyle').html('').hide();
				 				$('.SuccessMsgStyle').html('').hide();
				 				$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
				 				setTimeout(function(){
									$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
									$('#dgQuantity' +pId).focus();
//									$('#dgQuantity' +pId).bind('keyup',function(event){
//										if (event.keyCode == keyCodes.ENTER) {
//											$('#btnTransferWarehouse').click();
//										}
//									});
								}, 50);
								TransferInternalWarehouse._needNewRow = false;
							}
						}
					}},
				    {field:'productName', title:'Tên sản phẩm', width:120, align:'left', formatter: function(value, row, index){
				    	var prdName = 'dgProductName' + row.productId;
				    	return '<label id="' +prdName+'" style="width: 100%; text-align: left;">' +Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'availableQuantity', title:'Tồn kho đáp ứng', width:80, align:'right', formatter: function(value, row, index){
				    	var idAvlQ = "dgAvlQuantity" + row.productId;
				    	var html = '<input type="text" id= "' +idAvlQ+'" onchange="" value="' +StockValidateInput.formatStockQuantity(row.availableQuantity, row.convfact)+'" style="width: 100%; text-align: right;" maxlength="18" disabled="disabled" />';
				    	return html;
				    }},
				    {field:'quantity', title:'Số lượng', width:80, align:'right', formatter: function(value, row, index){
				    	var idQuan = "dgQuantity" + row.productId;
						if (row.quantity != undefined && row.quantity != null && row.quantity >= 0) {
							return '<input type="text" id= "' +idQuan+'" onchange="TransferInternalWarehouse.changeQuanttInPrdStockOutputDg(this);" value="' +StockValidateInput.formatStockQuantity(row.quantity, row.convfact)+'" style="width: 100%; text-align: right;" maxlength="18"/>';
				    	}
				    	return '<input type="text" id= "' +idQuan+'" onchange="TransferInternalWarehouse.changeQuanttInPrdStockOutputDg(this);" value="' +StockValidateInput.formatStockQuantity(0, row.convfact)+'" style="width: 100%; text-align: right;" maxlength="18"/>';
				    }},
				    {field:'price', title:'Giá bán', width:80, align:'right', formatter: function(value, row, index){
				    	var idPrice= "dgPrice" + row.productId;
						if (value != undefined && value != null) {
							return '<label id="' +idPrice+'" style="width: 100%; text-align: left;">' + VTUtilJS.formatCurrency(Utils.formatDoubleValue(value))+'</label>';
						}
						return '<label id="' +idPrice+'" style="width: 100%; text-align: left;">' + VTUtilJS.formatCurrency(Utils.formatDoubleValue(0))+'</label>';
				    }},
				    {field:'amount', title:'Thành tiền', width:80, align:'right', formatter: function(value, row, index){
				    	var idAmount = "dgAmount" + row.productId;
				    	if (value != undefined && value != null) {
							return '<label id="' +idAmount+'" style="width: 100%; text-align: left;">' + VTUtilJS.formatCurrency(Utils.formatDoubleValue(value))+'</label>';
						}
						return '<label id="' +idAmount+'" style="width: 100%; text-align: left;">' + VTUtilJS.formatCurrency(Utils.formatDoubleValue(0))+'</label>';
				    }},
				    {field:'detelte',title:'<a onclick="return TransferInternalWarehouse.openDialogInsertProduct();" href="javaScript:void(0);"><img src="/resources/images/icon_add.png"></a>',width:30, align:'center', formatter: function(value, row, index){
				    	if (row.productId != undefined && row.productId != null && row.productId > 0) { 
				    		return '<a href="javascript:void(0)" id="dg_prdStockOutputDg_delete" onclick="return TransferInternalWarehouse.deleteRowInPrdStockOutputDg(' +row.productId+');"><img title="Xóa" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
				    	}
				    }}
				]],
				onClickRow: function(rowIndex, rowData) {
					
				},
				onAfterEdit: function(rowIndex, rowData, changes) {
					
				},
				onBeforeEdit : function(rowIndex, rowData, changes){
				},
		        onLoadSuccess :function(data){
			    	 $('.datagrid-header-rownumber').html('STT');
			    	 Utils.updateRownumWidthAndHeightForDataGrid('prdStockOutputDg');
			    	 //Xu ly cho hien thi
			    	 var arrInput = '';
			    	 TransferInternalWarehouse.getDataComboboxInPrdStockOutputDg();
			    	 var rows = $('#prdStockOutputDg').datagrid('getRows');
			    	 if(rows == null || rows.length == 0 || rows[rows.length-1].productId != null){
			    		 TransferInternalWarehouse.insertRowStockTransDetailDg();
			    	 }
			    	 var sumAmountRow = 0;
			    	 var weight = 0;
			    	 if (TransferInternalWarehouse._mapStockTransDetail != null && TransferInternalWarehouse._mapStockTransDetail.size() > 0) {
			    		 for(var i=0; i < TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++){
			    			 var productTmp = TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]);
				    		 if (productTmp.amount != undefined && productTmp.amount != null) {
				    			 sumAmountRow += productTmp.amount;
				    		 }
				    		 var weightTmp = 0;
				    		 if (productTmp.grossWeight != undefined && productTmp.grossWeight != null && productTmp.quantity != undefined && productTmp.quantity != null) {
				    			 weightTmp = productTmp.grossWeight * productTmp.quantity;
				    		 }
				    		 weight += weightTmp;
				    	 }
			    	 } else {
			    		 TransferInternalWarehouse._mapStockTransDetail = new Map();
			    	 }
			    	 $('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
			    	 $('#lblSumWeightDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(weight)));
			    	 arrInput += '#btnChangeSaleOderAIAD';
			    	 $('' +arrInput+'').bind('keyup',function(event){
			 			 if(event.keyCode == keyCodes.ENTER){
			 			 	$('#btnChangeSaleOderAIAD').click();
			 			 }
			 		});
//					$('#dgQuantity0').bind('keyup',function(event){
//						if (event.keyCode == keyCodes.ENTER) {
//							$('#btnTransferWarehouse').click();
//						}
//					});
		    	}
			});
		}, null, null);
	},
	
	/**
	 * Thay doi so luong tren Row - prdStockOutputDg
	 * 
	 * @author hunglm16
	 * @since September 2,2014
	 * */
	changeQuanttInPrdStockOutputDg: function (txt) {
		$(".ErrorMsgStyle").html('').hide();
		var productId = Number($(txt).prop('id').replace(/dgQuantity/g,'').trim());
		var productRow = TransferInternalWarehouse._mapStockTransDetail.get(productId);
		var qtt = 0;
		if (productRow != undefined && productRow != null) {
			qtt = StockValidateInput.getQuantity($(txt).val(), productRow.convfact);
			if (!Utils.isValidQuantityInput($(txt).val().trim())) {
				$('#errMsg').html(Utils.XSSEncode($(txt).val().trim()) + ' Không đúng định dạng').show();
				qtt = StockValidateInput.getQuantity(productRow.quantity, productRow.convfact);
			}
			if (qtt > productRow.availableQuantity) {
				qtt = productRow.availableQuantity;
			}
			if (qtt < 0) {
				qtt = 0;
			}
			productRow.quantity = qtt;
			productRow.amount = productRow.quantity * productRow.price;
			$('#dgQuantity' +productId).val(StockValidateInput.formatStockQuantity(productRow.quantity, productRow.convfact));
	    	$('#dgAmount' +productId).html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(productRow.amount)));
	    	var sumAmountRow = 0;
	    	var sumWeightRow = 0;
	    	for(var i=0; i < TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++){
	    		var row = TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]);
	    		sumAmountRow += row.amount;
	    		sumWeightRow += row.grossWeight * row.quantity; 
	    	}
	    	$('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
	    	$('#lblSumWeightDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumWeightRow)));
		} else {
			qtt = StockValidateInput.getQuantity($(txt).val(), TransferInternalWarehouse._rowDgTmp.convfact);
			if (!Utils.isValidQuantityInput($(txt).val().trim())) {
				$('#errMsg').html(Utils.XSSEncode($(txt).val().trim()) + ' Không đúng định dạng').show();
				qtt = StockValidateInput.getQuantity(TransferInternalWarehouse._rowDgTmp.quantity, TransferInternalWarehouse._rowDgTmp.convfact);
			}
			if (qtt > TransferInternalWarehouse._rowDgTmp.availableQuantity) {
				qtt = TransferInternalWarehouse._rowDgTmp.availableQuantity;
			}
			if (qtt < 0) {
				qtt = 0;
			}
			TransferInternalWarehouse._rowDgTmp.quantity = qtt;
			TransferInternalWarehouse._rowDgTmp.amount = TransferInternalWarehouse._rowDgTmp.quantity * TransferInternalWarehouse._rowDgTmp.price;
			$('#dgQuantity0').val(StockValidateInput.formatStockQuantity( TransferInternalWarehouse._rowDgTmp.quantity, TransferInternalWarehouse._rowDgTmp.convfact));
	    	//$('#dgAvlQuantity0').val(StockValidateInput.formatStockQuantity(TransferInternalWarehouse._rowDgTmp.availableQuantity, TransferInternalWarehouse._rowDgTmp.convfact));
	    	$('#dgAmount0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
		}
	},
	
	/**
	 * Thay doi so luong tren Row - prdStockOutputDg
	 * 
	 * @author hunglm16
	 * @since September 2,2014
	 * */
	keyPressQuanttInPrdStockOutputDg: function (e, txt) {
		var inputStr = $(txt).val();
		if ((!/^[0-9\/]+$/.test(inputStr)) || (String(inputStr).indexOf('/')!=-1 && String(inputStr).split('/').length > 2)) {
			return;
		}
		var qtt = Number($(txt).val());
		var productId = Number($(txt).prop('id').replace(/dgQuantity/g,'').trim());
		var productRow = TransferInternalWarehouse._mapStockTransDetail.get(productId);
		if (productRow != undefined && productRow != null) {
			productRow.quantity = qtt;
			productRow.amount = productRow.quantity * productRow.price;
			$('#dgQuantity' +productId).val(StockValidateInput.formatStockQuantity( productRow.quantity, productRow.convfact));
	    	$('#dgAmount' +productId).html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
	    	var sumAmountRow = 0;
	    	for(var i=0; i < TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++){
	    		sumAmountRow += TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]).amount;
	    	}
	    	$('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
		} else {
			TransferInternalWarehouse._rowDgTmp.quantity = qtt;
			TransferInternalWarehouse._rowDgTmp.amount = TransferInternalWarehouse._rowDgTmp.quantity * TransferInternalWarehouse._rowDgTmp.price;
			$('#dgQuantity0').val(StockValidateInput.formatStockQuantity( TransferInternalWarehouse._rowDgTmp.quantity, TransferInternalWarehouse._rowDgTmp.convfact));
	    	$('#dgAmount0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
		}
	},
	
	/**
	 * Lay danh sach san pham sau loai tru trong Grid
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	getDataComboboxInPrdStockOutputDg: function () {
		if (TransferInternalWarehouse._mapStockTransDetail != null && TransferInternalWarehouse._mapStockTransDetail.size() > 0) {
   		 	for (var i = 0; i < TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++) {
	   		 	for (var j = TransferInternalWarehouse._lstproductavailable.length - 1; j >= 0; j--) {
	   		 		var prdId = TransferInternalWarehouse._mapStockTransDetail.keyArray[i];
	   				if (TransferInternalWarehouse._lstproductavailable[j].productId == prdId) { 
	   					TransferInternalWarehouse._lstproductavailable.splice(j, true);
	   				}
	   			}
	    	}
   	 	}
	},
	
	
	/**
	 * Lay du lieu cho PrdStockOutputDg
	 * 
	 * @author hunglm16
	 * @since September 2,2014
	 * */
	getDataInPrdStockOutputDg : function() {
		var rows = [];
		if (TransferInternalWarehouse._mapStockTransDetail != null || TransferInternalWarehouse._mapStockTransDetail.size() > 0){
			for (var i=0; i< TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++) {
				rows.push(TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]));
			}
		}
		return rows;
	},
	
	
	/**
	 * Xoa mot dong san pham
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	deleteRowInPrdStockOutputDg: function (productId) {
		if (productId == null || productId == null || productId == 0) {
			return;
		}
		if (TransferInternalWarehouse._mapStockTransDetail.keyArray.indexOf(productId) > -1) {
			for (var i = 0; i< TransferInternalWarehouse._mapProduct.keyArray.length; i++) {
				if (TransferInternalWarehouse._mapProduct.keyArray[i] == productId) {
					TransferInternalWarehouse._lstproductavailable.push(TransferInternalWarehouse._mapProduct.get(productId));
					//Sap xep lai
		   		 	var arrSort = [];
		   		 	for (var i = 0; i < TransferInternalWarehouse._lstproductavailable.length; i++) {
		   		 		var value = TransferInternalWarehouse._lstproductavailable[i].productCode + '_' + TransferInternalWarehouse._lstproductavailable[i].productId;
		   		 		arrSort.push(value.trim());
					}
		   		 	//lenh sap xep Order by ASC
		   		 	arrSort = arrSort.sort();
		   		 	//sap xep lai mang
		   		 	var data = [];
		   		 	for (var t = 0; t < arrSort.length; t++) {
		   		 		for (var i = 0; i < TransferInternalWarehouse._lstproductavailable.length; i++) {
		   		 			if(Number(arrSort[t].split('_')[1]) == TransferInternalWarehouse._lstproductavailable[i].productId) {
		   		 				data.push(TransferInternalWarehouse._lstproductavailable[i]);
		   		 			}
		   		 		}
		   		 	}
		   		 	//gan lai mang vua sap xep 
		   		 	TransferInternalWarehouse._lstproductavailable = new Array();
		   		 	for (var t = 0; t < data.length; t++) {
		   		 		TransferInternalWarehouse._lstproductavailable.push(data[t]);		   		 		
		   		 	}
					break;
				}
			}
			TransferInternalWarehouse._mapStockTransDetail.remove(productId);
			
			$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
		} else {
			$.messager.alert('Lỗi dữ liệu','Không tìm thấy Id Sản phẩm muốn xóa.','error');
		}
	},
	/**
	 * Them moi 2 dong trong vao Datagrid Stock Trans Detail
	 * 
	 * @author hunglm16
	 * @Since October 30, 2014
	 * */
	insertRowStockTransDetailDg: function () {
		var indexMax = $('#prdStockOutputDg').datagrid('getRows').length;
		$('#prdStockOutputDg').datagrid('insertRow',{
			index: indexMax,
			row: {
				productId: 0,
				productCode: '',
				productName: '',
				convfact: 1,
				warehouseId: 0,
				availableQuantity: 0,
				grossWeight: 0,
				price: 0,
				quantity: 0,
				amount: 0
			}
		});
		TransferInternalWarehouse._rowDgTmp = {
				productId: 0,
				productCode: '',
				productName: '',
				convfact: 1,
				warehouseId: 0,
				availableQuantity: 0,
				grossWeight: 0,
				price: 0,
				quantity: 0,
				amount: 0
	    	};
		
		$('#prdStockOutputDg').datagrid('selectRow', indexMax).datagrid('beginEdit', indexMax);
		$('.combobox-f.combo-f').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				TransferInternalWarehouse.openDialogInsertProduct();
			}
		});
	},
	
	/**
	 * Dialog Tim kiem va them san pham
	 *
	 * @author hunglm16
	 * @since November 5,2014
	 * @description Xu ly su kien thay doi
	 * */
	changeQuantityinDlProductSearch : function (txt, indexRow) {
		var rows = $('#cmndl-product-grid-search').datagrid("getRows");
		var productId = $(txt).attr("productid");
		
		var rowData = null;
		if (rows.length > 0 && productId != undefined && productId != null && Number(productId) > 0) {
			for (var i = 0; i < rows.length; i++) {
				if (productId == rows[i].productId) {
					var rowData = rows[i];
					break;
				}
			}
		}
		if (rowData != null) {
			var qtt = 0;
			if (Utils.isValidQuantityInput($(txt).val().trim())) {
				qtt = StockValidateInput.getQuantity($(txt).val().trim(), rowData.convfact);
			}
			if (qtt > rowData.availableQuantity) {
				qtt = rowData.availableQuantity;
			}
			if (qtt < 0) {
				qtt = 0;
			}
			rowData.quantity = qtt;
			if (qtt > 0) {
				TransferInternalWarehouse._mapProductDl.put(productId, rowData);
			} else if (TransferInternalWarehouse._mapProductDl.keyArray.indexOf(productId) > -1){
				TransferInternalWarehouse._mapProductDl.remove(productId);
			}
			$('#cmndl-product-grid-search').datagrid('updateRow',{
				index: indexRow,
				row: rowData
			});
			$('#cmndl-product-grid-search').datagrid('selectRow', indexRow);
			var txtQtt = $('.quantitydl')[indexRow + 1];
			if (txtQtt != undefined && txtQtt != null && $(txtQtt).length > 0) {
				$(txtQtt).focus();
			} else {
				$('#btnChooseProduct').focus();
			}
		}
	},
	/**
	 * Chosse Product In Dialog
	 *
	 * @author hunglm16
	 * @since November 5,2014
	 * */
	chooseProductInDialogSearch: function () {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var arrProductId = TransferInternalWarehouse._mapProductDl.keyArray;
		if (arrProductId == null || arrProductId.length == 0) {
			$('#errMsgCmndlProduct').html('Chưa sản phẩm nào được chọn').show();
			return;
		}
		
		var lstdata = [];
		for (var i=0, size = arrProductId.length; i < size; i++) {
			var row = TransferInternalWarehouse._mapProductDl.get(arrProductId[i]);
			if (row.quantity == null || row.quantity <= 0) {
				$('#errMsgCmndlProduct').html('Sản phẩm ' + Utils.XSSEncode(row.productCode) + ' phải có số lượng > 0').show();
				$('#dgQuantityPrdDl' + row.productId).focus();
				return;
			}
			var productInsert = {
	    			productId: row.productId,
					productCode: row.productCode,
					productName: row.productName,
					convfact: row.convfact,
					warehouseId: row.warehouseId,
					availableQuantity: row.availableQuantity,
					price: row.price,
					grossWeight: row.grossWeight,
					quantity: row.quantity,
					amount: row.quantity * row.price
		    };
			lstdata.push(productInsert);
		}
		for (var i=0; i < lstdata.length; i++) {
			if (TransferInternalWarehouse._mapStockTransDetail.keyArray.indexOf(lstdata[i].productId) < 0  && TransferInternalWarehouse._mapProduct.keyArray.indexOf(lstdata[i].productId) > -1) {
				TransferInternalWarehouse._mapStockTransDetail.put(lstdata[i].productId, lstdata[i]);
				//Xoa khoi danh sach Combobox
				TransferInternalWarehouse.getDataComboboxInPrdStockOutputDg();
			}
		}
		$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
		$('#cmndl-product-search-2-textbox').dialog("close");
	},
	
	/**
	 * Dialog Tim kiem va them san pham
	 *
	 * @author hunglm16
	 * @since November 5,2014
	 * */
	searchDialogInsertProduct: function () {
		$('#cmndl-product-grid-search').datagrid("load", {
			productCode: $('#txtCmnProductCode').val().trim(),
			productName: $('#txtCmnProductName').val().trim(),
			exception : TransferInternalWarehouse._mapStockTransDetail.keyArray.join(','),
			wareHouseOutputId: $('#dllStockOutput').combobox('getValue'),
			shopCodeOutput: $('#shopOutput').combobox('getValue')
		});
	},
	
	
	/**
	 * Dialog Tim kiem va them san pham
	 *
	 * @author hunglm16
	 * @since November 5,2014
	 * */
	openDialogInsertProduct: function () {
		$('#cmndl-product-search-2-textbox').dialog({
			title: 'Tìm Kiếm Sản Phẩm',
			width: 690, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				TransferInternalWarehouse._flagDialog = true;
				TransferInternalWarehouse._mapProductDl = new Map();
				$('#cmndl-product-grid-search').datagrid({
					url: '/transfer-internal-warehouse/search-List-Product-In-WareHouse',
					queryParams: {
						productCode: $('#txtCmnProductCode').val().trim(),
						productName: $('#txtCmnProductName').val().trim(),
						exception : TransferInternalWarehouse._mapStockTransDetail.keyArray.join(','),
						wareHouseOutputId: $('#dllStockOutput').combobox('getValue'),
						shopCodeOutput: $('#shopOutput').combobox('getValue')
					},
					width : $('#cmndl-product-grid-search').width() - 15,
				    rownumbers : true,
				    singleSelect:true,
				    pagination : true,
			        rownumbers : true,
			        pageNumber : 1,
			        checkOnSelect: false,
					selectOnCheck: false,
			        scrollbarSize: 0,
			        autoWidth: true,
			        pageList: [10],
			        autoRowHeight : true,
			        fitColumns : true,
				    //height: 275,
				    columns:[[
						{field:'productCode', title:'Mã sản phẩm', align:'left', width: 90, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:'productName', title:'Tên sản phẩm', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:'availableQuantity', title:'Tồn kho', width:80, align:'right', formatter: function(value, row, index){
					    	return StockValidateInput.formatStockQuantity(row.availableQuantity, row.convfact);
					    }},
						{field:'quantity', title:'Số lượng', width:80, align:'right', formatter: function(value, row, index){
							var idQuan = "dgQuantityPrdDl" + row.productId;
							if (row.quantity != null && row.quantity > 0) {
								return '<input type="text" id= "' +idQuan+'" productid="' +row.productId+'" value="' +StockValidateInput.formatStockQuantity(row.quantity, row.convfact)+'" onchange="TransferInternalWarehouse.changeQuantityinDlProductSearch(this, ' +index+');" class="quantitydl" style="width: 100%; text-align: right;" maxlength="18"/>';
							} else {
								return '<input type="text" id= "' +idQuan+'" productid="' +row.productId+'" value="" onchange="TransferInternalWarehouse.changeQuantityinDlProductSearch(this, ' +index+');" class="quantitydl" style="width: 100%; text-align: right;" maxlength="18"/>';
							}
						}}
					]],
					onCheck: function(rowIndex, rowData) {
					},
					onUncheck: function(rowIndex, rowData) {
					},
					onCheckAll: function(rows) {
					},
					onUncheckAll: function(rows) {
					},
					onClickRow: function(rowIndex, rowData) {
						var rowCbx = $('[name=cbxProductIdCmn]')[rowIndex];
						if(rowCbx!=undefined && rowCbx!=null && !rowCbx.checked){
							$('#cmndl-product-grid-search').datagrid('unselectRow', rowIndex);
						}else{
							$('#cmndl-product-grid-search').datagrid('selectRow', rowIndex);
						}
						return true;
					},
			        onLoadSuccess: function(data){
				    	 $('.datagrid-header-rownumber').html('STT');
				    	 Utils.updateRownumWidthAndHeightForDataGrid('cmndl-product-grid-search');
				    	 $('.quantitydl').bind('keyup',function(event){
				  			if(event.keyCode == keyCodes.ENTER){
				  				TransferInternalWarehouse.chooseProductInDialogSearch();
				 			}
				 		});
			        }
				});
			},
			onClose: function() {
				$('#cmndl-product-grid-container').html('<table id="cmndl-product-grid-search"></table>').change();
				$('#txtCmnProductCode').val("").change();
				$('txtCmnProductCode').val("").change();
				TransferInternalWarehouse._flagDialog = false;
			}
		});
	},
	
	/**
	 * Tao moi phieu Dieu chuyen kho
	 * 
	 * @author hunglm16
	 * @since Septemp 2,2014
	 * */
	createStockTransDetail: function() {
		if (TransferInternalWarehouse._flagDialog) {
			//Kiem tra dialog co dang mo hay khong
			$($('.quantitydl')[0]).focus();
			return;
		}
		
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		
		var wareHouseOutputId = $('#dllStockOutput').combobox('getValue');
		var wareHouseInputId = $('#dllStockInput').combobox('getValue');
		var arrDataInsert = [];
		
		if (wareHouseOutputId == undefined || wareHouseOutputId == null || wareHouseOutputId.trim().length == 0) {
			msg = "Chưa chọn Kho xuất";
		}
		if (msg.trim().length == 0) {
			if (wareHouseInputId == undefined || wareHouseInputId == null || wareHouseInputId.trim().length == 0) {
				msg = "Chưa chọn Kho nhập";
			}
		}
		if (msg.trim().length == 0 && Number(wareHouseOutputId) == Number(wareHouseInputId)) {
			msg = "Kho nhập và kho xuất không được trùng nhau";
		}
		
		if (msg.trim().length == 0) {
			if (TransferInternalWarehouse._mapStockTransDetail.size() == 0) {
				msg = "Chưa có sản phẩm nào được chọn";
			} else {
				for (var i = 0; i< TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++) {
					var product = TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]);
					if (product.availableQuantity < product.quantity) {
						msg = "Sản phẩm " + Utils.XSSEncode(product.productCode) + " có số lượng lớn hơn tồn kho đáp ứng";
						break;
					} else if (product.quantity == undefined || product.quantity == null || product.quantity < 1) {
						msg = "Sản phẩm " + Utils.XSSEncode(product.productCode) + " số lượng phải lớn hơn 0";
						$('#dgQuantity' +product.productId).focus();
						break;
					} else if (product.productId > 0) {
						var paramsPrd = product.productId.toString() + "ESC" + product.quantity;
						arrDataInsert.push(paramsPrd);
					}
				}
			}
		}
		if (msg.trim().length == 0 && arrDataInsert.length == 0) {
			msg = "Chưa có sản phẩm nào được chọn";
		}
		if (msg.trim().length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var params = {
				shopCodeOutput: $('#shopOutput').combobox('getValue'),
				shopCodeInput: $('#shopInput').combobox('getValue'),
				wareHouseOutputId: wareHouseOutputId,
				wareHouseInputId: wareHouseInputId,
				arrStockTransDetail: arrDataInsert.join(",")
		};
		var msg = "Bạn có muốn tạo phiếu Điều chuyển kho nội bộ?";
		Utils.addOrSaveData(params, "/transfer-internal-warehouse/create-Stock-Trainsfer-Internal", null, 'errMsg', function(data) {
			if (data.error != undefined && !data.error) {
//				 $('#dllStockOutput').change();
				disabled('btnTransferWarehouse');
				$('#txtStockTransCode').val(data.stockTransCode);
				$('#txtStockTransDate').val(data.stockTransDate);
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html("").hide();
				}, 5000);				
			}
		}, null, null, null, msg);
	},
	/**
	 * khoi tao conbobox don vi
	 */
	loadShopCbx: function() {
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindShopCbx('#shopOutput', data.rows, null, null, function(data) {
					if (data != null) {
						TransferInternalWarehouse.changeShop(Utils.XSSEncode(data.shopCode), '#dllStockOutput', true);
					}					
				});
				Utils.bindShopCbx('#shopInput', jQuery.extend(true, [], data.rows), null, null, function(data){
					if (data != null) {
						TransferInternalWarehouse.changeShop(Utils.XSSEncode(data.shopCode), '#dllStockInput', false);
					}	
				});
			}
		});	
	},
	/**
	 *  su kien change shop
	 */
	changeShop: function(shopCode, selector, isShopOutput) {
		var params = new Object();
		if (isShopOutput) {
			params.shopCodeOutput = shopCode;
		} else {
			params.shopCodeInput = shopCode;
		}
		$.ajax({
			type : "POST",
			url : '/transfer-internal-warehouse/change-shop',
			data : params,
			dataType : "json",
			success : function(data) {
				if (isShopOutput) {
					$('#txtStockTransCode').val(Utils.XSSEncode(data.stockTransCode));
					$('#txtStockTransDate').val(data.stockTransDate);
				}
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null) {
					$(selector).combobox({
						valueField : 'warehouseId',
						textField : 'warehouseName',
						data : data.lstWarehouseVO,
						width: 250,
						panelWidth: 206,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseCode) + ' - ' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row) {
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
						onSelect:function(rec) {
							if (isShopOutput) {
								TransferInternalWarehouse.eventChangeComboboxWareHouse(rec.warehouseId);
							}
							enable('btnTransferWarehouse');
						},
				        onLoadSuccess: function(){
				        	var arr = $(selector).combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$(selector).combobox('select', arr[0].warehouseId);
				        	}
				        }
					});
				}
			}
		});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.stock-transfer-internal.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/stock/vnm.lockedstock.js
 */
var LockedStock = {
	_xhrSave : null,
	_xhrDel: null,	
	_pMap:null,
	_phCheckedMap:null,
	xhrExport: null,
	lastValue: 1,
	_pTotalCheckedMap:null,
	_status:null,
	_lstStockLock: null,
	_currentShopCode : '',
	_shopCodeFilter: "",
	
	lockStockStaff:function(){			
		var dataModel = new Object();
		var lstStaffId = new Array();
		var vanLockStatus = 1;
		var obj = LockedStock._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstStaffId.push(obj[i]["id"]);			
		}
		dataModel.lstStaffLock = lstStaffId;
		dataModel.vanLockStatus = vanLockStatus;
		dataModel.shopCodeFilter=LockedStock._shopCodeFilter;
		Utils.addOrSaveData(dataModel, "/catalog/utility/closing-day/lock-stock-staff-change", null, 'errMsg', function(data) {
			
			if (data.error) {				
				$('#errMsg').html(data.errMsg.replace(/\|/g, '<br>')).show();				
			}else {
				$('#successMsg').html('Khóa kho nhân viên thành công').change().show();				
				setTimeout(function(){
					$('#successMsg').html('').change().show();	
					$('#grid').datagrid('reload');
					LockedStock._phCheckedMap.clear();
				}, 700);
			  }
			
			}, null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsg').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#grid').datagrid('reload');
					LockedStock._phCheckedMap.clear();
				}
			});		
		return false;		
	},
	unlockStockStaff:function(){			
		var dataModel = new Object();
		var lstStaffId = new Array();
		var vanLockStatus = 0;
		var obj = LockedStock._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstStaffId.push(obj[i]["id"]);
		}
		dataModel.startDate = $('#startDate').val();
		dataModel.lstStaffLock = lstStaffId;
		dataModel.vanLockStatus = vanLockStatus;
		dataModel.shopCodeFilter=LockedStock._shopCodeFilter;
		Utils.addOrSaveData(dataModel, "/catalog/utility/closing-day/lock-stock-staff-change", null, 'errMsg', function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg.replace(/\|/g, '<br>')).show();
			}else {
				$('#successMsg').html('Mở khóa kho nhân viên thành công').change().show();				
				setTimeout(function(){
					$('#successMsg').html('').change().show();
					$('#grid').datagrid('reload');
					LockedStock._phCheckedMap.clear();
				}, 700);
			  }
			
			},  null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsg').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#grid').datagrid('reload');
					LockedStock._phCheckedMap.clear();
				}
			});		
		return false;		
	},	
	initUnitCbx: function(){				
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				listNVBH = data.lstNVBH;
				$('#shopCodeCB').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null && rec.shopCode != LockedStock._currentShopCode){
			        		$("#errMsg").html("").show();
			        		LockedStock.resetChangeShop(rec);
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shopCodeCB').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
			        		LockedStock._currentShopCode = Utils.XSSEncode(arr[0].shopCode);
			        	}
			        	
			        }
				});
			}
		});
	},
	resetChangeShop: function(rec){
		if (rec != undefined && rec != null){
			LockedStock._phCheckedMap = new Map();
			LockedStock._currentShopCode = rec.shopCode;
			LockedStock._shopCodeFilter = rec.shopCode;
			var params = new Object();	
			if(LockedStock._shopCodeFilter != null){
					params.shopCodeFilter = LockedStock._shopCodeFilter;
			}	
			params.type = -1;
			$('#grid').datagrid({
				url : '/catalog/utility/closing-day/search-locked-stock',
				autoRowHeight : true,
				rownumbers : true, 
				checkOnSelect :true,
				selectOnCheck: true,
				//singleSelect: true,
				pagination:true,
				rowNum : 10,
				fitColumns:true,
				pageList  : [10,20,30],
				scrollbarSize:0,
				width: $('#gridContainer').width(),
				autoWidth: true,
				queryParams:params,
				columns:[[
				    {field: 'id', index:'id', hidden: true},		
				    {field: 'staffId', checkbox: true},
					{field: 'staffCode',title:'Mã nhân viên', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
						return Utils.XSSEncode(value);
					}},
					{field: 'staffName',title:'Tên nhân viên',width: 320,sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
						return Utils.XSSEncode(value);
					}},
					{field: 'vanLock', title: 'Trạng thái', width: 100, sortable:false,resizable:false,align: 'left', formatter:UntilityStockedLock.vanLock },
					{field: 'lockDate', title:'Ngày khóa/mở khóa', width:160, align: 'left'}
				 ]],
				 onBeforeLoad:function(param){											 
				 },
				 onLoadSuccess :function(data){   
					 	$('td[field=staffId] div.datagrid-header-check input[type=checkbox]').attr('checked',false);
				    	$('.datagrid-header-rownumber').html('STT');  		    	
				    	$(window).resize();
				    	 if (data == null || data.total == 0) {
							 $("#errMsg").html("Không có nhân viên bán hàng Vansale trong ngày").show();
						 }
				    	 var n = data.rows.length;
				    	 var r = null;
				 },
				 onCheck: function(i, r) {			
					var obj = new Object();			
					obj.id = r.staffId;		
					obj.code = r.staffCode;
					obj.name = r.staffName;				
					LockedStock._phCheckedMap.put(obj.code,obj);				
				 },
				 onUncheck: function(i, r) {
					 LockedStock._phCheckedMap.remove(r.staffCode);
				 },
				 onCheckAll: function(rows) {
					 for (var i = 0, sz = rows.length; i < sz; i++) {
						var obj = new Object();
						obj.id = rows[i].staffId;		
						obj.code = rows[i].staffCode;
						obj.name = rows[i].staffName;			
						LockedStock._phCheckedMap.put(obj.code, obj);			
					 }
				 },
				  onUncheckAll: function(rows) {		
					  for (var i = 0, sz = rows.length; i < sz; i++) {
						  LockedStock._phCheckedMap.remove(rows[i].staffCode);
						 }
				 } 
			});
			
		}
	},
	getParams:function(){	
		var size = LockedStock._phCheckedMap.size();
		for(var i = 0; i < size; ++ i){
		    var staffCode = LockedStock._phCheckedMap.keyArray[i];
		    console.log(staffCode);
		}
		var shopId = $('#shopId').val().trim();
		var staffCode =$('#staffCode').val();	
		var data = new Object();
		if(shopId>0){
			data.shopId = shopId;
		}
		if(staffCode.length>0){
			data.staffCode = staffCode;
		}		
		return data;
	},
	
};
/*
 * END OF FILE - /web/web/resources/scripts/business/stock/vnm.lockedstock.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

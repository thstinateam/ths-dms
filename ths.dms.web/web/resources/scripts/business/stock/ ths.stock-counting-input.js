var StockCountingInput = {
	_xhrSave : null,
	_isChange:false,
	_myListId : null,
	_myValue : null,
	_myMap : null,
	_array_old:null,
	convfact:null,
	_first_total_row:null,
	_isShowDlg: false,
	_cycleCountCode: null,
	_shopCode: null,
	/**
	 * Kiem tra san pham them moi ngoai kho kiem ke
	 * @author hunglm16
	 * @return true -> Ton tai, false -> Khong ton tai
	 * @since 14/11/2015
	 */
	checkProductUotStockCountingInput: function () {
		if ($('.isDeleteRowTmp').length > 0) {
			return true;
		}
		return false;
	},
	/**
	 * Xoa san pham them moi tam trong the kho
	 * @author hunglm16
	 * @param productId id San pham
	 * @since 12/11/2015
	 */
	removeProductInsert: function (productId, rowIndex) {
		var arrSTT = $('.isRowIndex');
		for (var stt = rowIndex - 1, size = arrSTT.length; stt < size; stt++) {
		  $(arrSTT[stt]).html(stt);
		}
		$('#tr_' + productId).remove();
	},
	getInfo : function(isShow) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê',Utils._CODE);
		}
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();
			return false;
		}
		var param = new Object();
		param.cycleCountCode = $('#cycleCountCode').val().trim();
		param.shopCode = $('#shopCode').val().trim();
		StockCountingInput._shopCode = $('#cycleCountCode').val().trim();
		StockCountingInput._cycleCountCode = $('#shopCode').val().trim();
		Utils.getHtmlDataByAjax(param, "/stock/input/getinfo", function(data){
			try {
				var _data = JSON.parse(data);
				if(_data.error != null && _data.errMsg != null && _data.errMsg != undefined){
					$('#errMsgSearch').html(_data.errMsg).show();
					$("#infoDiv").html('').hide();
					StockCountingInput.clearMap();
					StockCountingInput._myMap = new Map();
				}
			} catch(e) {
				StockCountingInput.clearMap();
				$("#infoDiv").html(data).show();			
				StockCountingInput._myMap = new Map();
				$('.ProductNotLot').each(function(){
					var textBoxId = $(this).attr('id');
					Utils.bindFormatOnTextfield(textBoxId, Utils._TF_NUMBER_CONVFACT);
				});
			}			
		}, 'loading', 'POST');
		return false;
	},
	checkIsChange:function(cycleCountMapProductId,firsttotalrow){
		var lengthOld = StockCountingInput._array_old.length;
		var lengthNew = StockCountingInput._myMap.get(cycleCountMapProductId).length;
		if(lengthOld!=lengthNew){
			return true;
		}
		var list = StockCountingInput._myMap.get(cycleCountMapProductId);
		for(var i=0;i<lengthNew;++i){
			var objNew = list[i];
			var objectOld = StockCountingInput._array_old[i];
			if(objectOld==null){
				return true;
			}
			if(objNew.quantityCounted!=objectOld.quantityCounted){
				return true;
			}
		}
		var temp = 0;
		$('.fancybox-inner .qtyCount').each(function(){
			++temp;
		});
		if(temp != firsttotalrow){
			return true;
		}
		return false;
	},
	
	viewDetail : function(cycleCountMapProductId,prodId,prodCode,prodName,convfact) {		
		StockCountingInput._array_old = new Array();	
		StockCountingInput.convfact = convfact;	
		$('#productLotDialog').css("visibility", "visible");
		var html = $('#productLotDialog').html();
		var firsttotalrow = 0;
		$('#productLotEasyUIDialog').dialog({closed: false,cache: false,modal: true,width : 700,height :450,
			onOpen: function(){
				$('.easyui-dialog #d_productCode').html(Utils.XSSEncode(prodCode));
				$('.easyui-dialog #d_productName').html(Utils.XSSEncode(prodName));
				$('.easyui-dialog #h_CCMapProductId').val(Utils.XSSEncode(cycleCountMapProductId));
				Utils.bindFormatOnTextfield('quantityCountOfLot', Utils._TF_NUMBER_CONVFACT);
				Utils.bindFormatOnTextfield('productLot', Utils._TF_NUMBER);				
				
				$.getJSON('/rest/catalog/list-product-lot/' +prodId+'/' + cycleCountMapProductId +'/list.json', function(result){
					StockCountingInput._myValue = new Array();
					var MAP_VALUE = StockCountingInput._myMap.get(cycleCountMapProductId);	
					for(var index=0;index<result.length;++index){
						var object = result[index];	
						object.deleted = 0;
						if((MAP_VALUE==null) || (MAP_VALUE!=null && MAP_VALUE.length==0)){
							StockCountingInput._myValue.push(object);
						}								
						StockCountingInput._array_old.push(object);
					}	
					if(StockCountingInput._myValue.length>0){
						StockCountingInput._myMap.put(cycleCountMapProductId,StockCountingInput._myValue);
						StockCountingInput._myValue = null;
					}
					var cc_products_lot = StockCountingInput._myMap.get(cycleCountMapProductId);
					if(cc_products_lot!=null){
						var html = new Array();						
						$('.easyui-dialog #stockCClist').html(html.join(""));
						for(var i=0;i<cc_products_lot.length;++i){
							var object = cc_products_lot[i];
							if(object.deleted==0) {										
								html.push('<tr id="row_' + Utils.XSSEncode(object.lot) +'">');
								html.push('<td class="AlignCCols OrderNumber" style="padding:0px;border-left: 1px solid #C6D5DB;" >' + (index+1) +'</td>'); 
								html.push('<td class="AlignLCols" style="padding:0px" id="ftd_lot_' + Utils.XSSEncode(object.lot) +'">' + Utils.XSSEncode(object.lot) +'</td>');  
								html.push('<td class="AlignRCols qtyBeforeCount" style="padding:0px">' + StockValidateInput.formatStockQuantity(object.quantityBeforeCount, convfact) +'</span></td>'); 
								html.push('<td class="AlignRCols" style="padding: 2px 5px;"><input maxlength="8" onchange="return StockCountingInput.showConfact(this,' +Number(convfact)+');" id="quantityCounted' +Utils.XSSEncode(object.lot)+'"  value="' +StockValidateInput.formatStockQuantity(object.quantityCounted, convfact)+'" class="qtyCount" style="width: 154px;text-align: right;" size="10" type="text"  /></td>');
								if(parseInt(object.quantityBeforeCount, 10)<=0){
									html.push('<td class="AlignCCols" style="padding:0px"><a class="DelCCResult" href="javascript:void(0)" onclick="StockCountingInput.delCCResult(\'' +Utils.XSSEncode(object.lot)+'\');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
								}else{
									html.push('<td ></td>');
								}		
								html.push('</tr>');								
							}							
						}
						$('.easyui-dialog #stockCClist').html(html.join(""));
					}	
					$('.easyui-dialog .qtyCount').each(function(){
						++firsttotalrow;
					});
					StockCountingInput.showTotalQuantityAndScrollBody();
					$('.easyui-dialog #btnCloseProductLot').unbind('click');
					$('.easyui-dialog #btnCloseProductLot').bind('click',function(){
						var newArray = StockCountingInput._myMap.get(cycleCountMapProductId);					
						var array = new Array();
						if(newArray!=null){
							for(var i=0;i<newArray.length;++i){
								var o = newArray[i];
								var object = new Object();
								object.lot = o.lot;
								object.deleted = o.deleted;
								object.quantityBeforeCount = o.quantityBeforeCount;
								var  quantityCounted = $('.easyui-dialog #quantityCounted' +o.lot).val();
								if(quantityCounted!=undefined && quantityCounted.length>0){
									object.quantityCounted = StockValidateInput.getQuantity(quantityCounted.trim(), convfact);							
								}else{
									object.quantityCounted = 0;
								}
								array.push(object);	
							}	
						}									
						StockCountingInput._myMap.put(cycleCountMapProductId,array);
						if(StockCountingInput.checkIsChange(cycleCountMapProductId,firsttotalrow)){//co thay doi
							$('#td_chkApprove' +cycleCountMapProductId).attr('checked',true);
							StockCountingInput.showTotalQuantityAndScrollBody();
							var a = $('.easyui-dialog #totalBeforeCount').html();
							var b = $('.easyui-dialog #totalCount').html();
							var countDate = $('#lstProducts #countDate' +cycleCountMapProductId).html();
							if(countDate.length>0){
								$('#lstProducts #td_qtyBeforeCount' +cycleCountMapProductId).html(StockValidateInput.formatStockQuantity(a, convfact)).show();
							}
							$('#lstProducts #td_qtyCountP' + cycleCountMapProductId).html(StockValidateInput.formatStockQuantity(b, convfact)).show();
						}else{ 					
							StockCountingInput._myValue = new Array();
							StockCountingInput._myMap.put(cycleCountMapProductId,new Array());
						}					
						$('.easyui-dialog').dialog('close');
					});		
				});
			},
			onBeforeClose:function(){
				var newArray = StockCountingInput._myMap.get(cycleCountMapProductId);					
				var array = new Array();
				if(newArray!=null){
					for(var i=0;i<newArray.length;++i){
						var o = newArray[i];
						var object = new Object();
						object.lot = o.lot;
						object.deleted = o.deleted;
						object.quantityBeforeCount = o.quantityBeforeCount;
						var  quantityCounted = $('.easyui-dialog #quantityCounted' +o.lot).val();
						if(quantityCounted!=undefined && quantityCounted.length>0){
							object.quantityCounted = StockValidateInput.getQuantity(quantityCounted.trim(), convfact);							
						}else{
							object.quantityCounted = 0;
						}
						array.push(object);	
					}	
				}									
				StockCountingInput._myMap.put(cycleCountMapProductId,array);
				if(StockCountingInput.checkIsChange(cycleCountMapProductId,firsttotalrow)){//co thay doi
					$('#td_chkApprove' +cycleCountMapProductId).attr('checked',true);
					StockCountingInput.showTotalQuantityAndScrollBody();
					var a = $('.easyui-dialog #totalBeforeCount').html();
					var b = $('.easyui-dialog #totalCount').html();
					var countDate = $('#lstProducts #countDate' +cycleCountMapProductId).html();
					if(countDate.length>0){
						$('#lstProducts #td_qtyBeforeCount' +cycleCountMapProductId).html(StockValidateInput.formatStockQuantity(a, convfact)).show();
					}
					$('#lstProducts #td_qtyCountP' + cycleCountMapProductId).html(StockValidateInput.formatStockQuantity(b, convfact)).show();
				}else{ 					
					StockCountingInput._myValue = new Array();
					StockCountingInput._myMap.put(cycleCountMapProductId,new Array());
				}					
			},
			onClose : function(){
	        	$('#productLotDialog').html(html);
	        	$('#productLotDialog').css("visibility", "hidden");	        	
	        }
		});
		
		return false;
	},
	showTotalQuantityAndScrollBody:function(){		
		var runner = 0;
		$('.easyui-dialog .OrderNumber').each(function(){
			runner+=1;
			$(this).html(runner).show();
		});
		var totalBeforeCount = 0;
		$('.easyui-dialog .qtyBeforeCount').each(function(){
			if($(this).html().trim().length>0){
				totalBeforeCount += StockValidateInput.getQuantity($(this).html().trim(),StockCountingInput.convfact);
			}			
		});
		var totalCount = 0;	
		$('.easyui-dialog .qtyCount').each(function(){			
			if($(this).val().trim().length>0){
				totalCount += StockValidateInput.getQuantity($(this).val(),StockCountingInput.convfact);				
			}
			Utils.bindFormatOnTextfield($(this).attr('id'), Utils._TF_NUMBER_CONVFACT);
		});
		totalBeforeCount = StockValidateInput.formatStockQuantity(totalBeforeCount, StockCountingInput.convfact);
		totalCount = StockValidateInput.formatStockQuantity(totalCount,StockCountingInput.convfact);
		$('.easyui-dialog #totalBeforeCount').html(totalBeforeCount);
		$('.easyui-dialog #totalCount').html(totalCount);
	},
	saveCCResult: function(){
		if(StockCountingInput._xhrSave != null){
			return false;
		}
		var lot = $('.easyui-dialog #productLot').val().trim();
		var my_qtyCount = $('.easyui-dialog #quantityCountOfLot').val().trim();
		$('.easyui-dialog #errMsg1').hide();	
		var msg = Utils.getMessageOfRequireCheck('productLot', 'Số lô');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('quantityCountOfLot', 'Số lượng');
		}	
		if(msg.length==0 && lot.length!=6){	
			msg = 'Số lô phải đủ 6 ký tự theo định dạng ddMMyy. Ví dụ: 011012';
			$('.easyui-dialog #productLot').focus();
		}
		if(msg.length==0){
			var date = new Date();
			var yy = date.getFullYear().toString().substring(0, 2);
			var dd = lot.substring(0, 2);
			var mm = lot.substring(2, 4);
			var happy_new_year = dd.toString() +"/" + mm.toString() + "/" + yy + lot.substring(4);		
			if(!Utils.isDateEx(happy_new_year, '/',Utils._DATE_DD_MM_YYYY)){
				msg = 'Số lô không đúng định dạng ddMMyy. Ví dụ: 011012';
				$('.easyui-dialog #productLot').focus();			
			}	
		}		
		if(msg.length>0){
			$('.easyui-dialog #errMsg1').html(msg).show();			
			$.fancybox.update();
			return false;
		}
		var amount = StockValidateInput.formatStockQuantity(lot, StockCountingInput.convfact);
		my_qtyCount = StockValidateInput.getQuantity(amount,StockCountingInput.convfact);	
		if(my_qtyCount.length==0){
			my_qtyCount = 0;
		}
		var cycleCountMapProductId = $('.easyui-dialog #h_CCMapProductId').val().trim();
		var cycleCountResultId = $('.easyui-dialog #h_CCResultId').val().trim();
		$('.easyui-dialog #errMsg1').hide();	
		var lot = $('.easyui-dialog #productLot').val().trim();
		var my_qtyCount = $('.easyui-dialog #quantityCountOfLot').val().trim();		
		$.messager.confirm('Xác nhận', 'Bạn có chắc chắn muốn thêm mới ?', function(r){
			if(r){
				var object = new Object();	
				object.quantityBeforeCount = 0;
				object.quantityCounted = my_qtyCount;
				object.lot = lot;
				object.deleted = 0;
				var cc_product_lots = new Array();				
				var _cc_product_lots = new Array();
				cc_product_lots = StockCountingInput._myMap.get(cycleCountMapProductId);
				if(cc_product_lots!=null && cc_product_lots.length>0){			
					for(var i=0;i<cc_product_lots.length;++i){						
						if(cc_product_lots[i].lot==object.lot && cc_product_lots[i].deleted==0){
							$('.easyui-dialog #errMsg1').html('Số lô ' + Utils.XSSEncode(object.lot) +' đã tồn tại. Vui lòng nhập giá trị khác').show();
							$.fancybox.update();
							return false;
						}
						var _object = new Object();
						_object.quantityBeforeCount = cc_product_lots[i].quantityBeforeCount;
						_object.quantityCounted = cc_product_lots[i].quantityCounted;
						_object.lot = cc_product_lots[i].lot;
						_object.deleted = cc_product_lots[i].deleted;
						_cc_product_lots.push(_object);
					}					
				}
				_cc_product_lots.push(object);
				StockCountingInput._myMap.put(cycleCountMapProductId,_cc_product_lots);		
				var html = new Array();
				html.push('<tr id="row_' + Utils.XSSEncode(object.lot) +'">');
				html.push('<td class="AlignCCols OrderNumber" style="padding:0px;border-left: 1px solid #C6D5DB;" ></td>'); 
				html.push('<td class="AlignLCols" style="padding:0px" id="ftd_lot_' + Utils.XSSEncode(object.lot) +'">' + Utils.XSSEncode(object.lot) +'</td>');  //solo
				html.push('<td class="AlignRight qtyBeforeCount" style="padding:0px">' + StockValidateInput.formatStockQuantity(object.quantityBeforeCount,StockCountingInput.convfact) +'</span></td>'); 
				html.push('<td class="AlignRCols" style="padding: 2px 5px;"><input maxlength="10" onchange="return StockCountingInput.showConfact(this,' +StockCountingInput.convfact+');" id="quantityCounted' +Utils.XSSEncode(object.lot)+'" value="' +StockValidateInput.formatStockQuantity(object.quantityCounted,StockCountingInput.convfact)+'" class="qtyCount" style="width: 154px;text-align: right;" size="10" type="text" /></td>');
				if(parseInt(object.quantityBeforeCount, 10)<=0){
					html.push('<td class="AlignCCols" style="padding:0px"><a class="DelCCResult" href="javascript:void(0)" onclick="StockCountingInput.delCCResult(\'' +Utils.XSSEncode(object.lot)+'\');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
				}else{
					html.push('<td ></td>');
				}		
				html.push('</tr>');
				$('.easyui-dialog #stockCClist').append(html.join(""));
				StockCountingInput.showTotalQuantityAndScrollBody();		
				$('.easyui-dialog #productLot').val('');		
				$('.easyui-dialog #quantityCountOfLot').val('');
				StockCountingInput._isChange = true;
			}
		});		
	},	
	delCCResult:function(lot){
		$.messager.confirm('Xác nhận', 'Bạn chắc chắn muốn xóa ?', function(r){
			if(r){
				var cycleCountMapProductId = $('.easyui-dialog #h_CCMapProductId').val().trim();
				$('.easyui-dialog #errMsg1').html('').hide();
				$('.easyui-dialog #succMsg1').hide();
				var _rows = new Array();
				var rows = StockCountingInput._myMap.get(cycleCountMapProductId);
				var rowDelete = 0;
				for(var i=0;i<rows.length;++i){
					var _object = new Object();
					if(rows[i].lot==lot){		
						_object.deleted=1;
						rowDelete = 1;
					}else{
						_object.deleted = rows[i].deleted;
					}
					if(rowDelete ==1){
						_object.quantityBeforeCount = 0;
						_object.quantityCounted = 0;
					}else{
						_object.quantityBeforeCount = rows[i].quantityBeforeCount;
						_object.quantityCounted = rows[i].quantityCounted;
					}					
					_object.lot = rows[i].lot;					
					_rows.push(_object);
				}	
				StockCountingInput._myMap.remove(cycleCountMapProductId);
				StockCountingInput._myMap.put(cycleCountMapProductId,_rows);
				$('#row_' + lot).replaceWith('');
				StockCountingInput.showTotalQuantityAndScrollBody();
				$('.easyui-dialog #productLot').val('');
				$('.easyui-dialog #productLot').removeAttr("disabled");
				$('.easyui-dialog #quantityCountOfLot').val('');
				$.fancybox.update();
			}
		});
	},
	clearMap : function() {
		StockCountingInput._myMap = new Map();
	},
	/**
	 * Cap nhat san pham vao the kho
	 * 
	 * @modify hunglm16
	 * @since 13/11/2015
	 */
	importData: function() {		
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var lstCCMapProduct = new Array();
		var lstCCResultDelete = new Array();
		$('.isFlagCurent input:checkbox[name=chkApprove]:checked').each(function() {
			var id = $(this).val();
			var checkLot = $('.isLotProductCurent #lot' +id).val();
			if (checkLot == 0) {
				var amount = $($(this).parent().parent().children()[6]).children().val().trim();
				var convfact = $($(this).parent().parent().children()[6]).children().attr('confact');
				var quantity = StockValidateInput.getQuantity(amount, convfact);
				if (quantity.length == 0) {
					quantity = 0;
				}
				if (parseInt(quantity, 10) < 0) {
					return false;
				}
				var result = '(' + id + ',' + quantity + ')';
				lstCCMapProduct.push(result);
			} else {
				var result = '(' + id + ',-1)';
				var lstCCResult = StockCountingInput._myMap.get(id);
				if (lstCCResult != null && lstCCResult.length > 0) {
					for (var j = 0; j < lstCCResult.length; j++) {
						var ccresult = lstCCResult[j];
						if (parseInt(ccresult.quantityCounted, 10) < 0) {
							return false;
						}
						result += ';(' + ccresult.lot + ',' + ccresult.quantityCounted + ')';
						if (ccresult.deleted == 1) {
							lstCCResultDelete.push(id + ';' + ccresult.lot);
						}
					}
				} else {
					result = '(' + id + ',0)';
				}
				lstCCMapProduct.push(result);
			}
		});
		//Xu ly cho cac san pham them moi
		var arrProductObj = [];
		$('.isFlagInsert input:checkbox[name=chkApprove]:checked').each(function() {
			var id = $(this).val();
			var checkLot = $('.isLotProductInsert #lot' + id).val();
			if (checkLot == 0) {
				var amount = $($(this).parent().parent().children()[6]).children().val().trim();
				var convfact = $($(this).parent().parent().children()[6]).children().attr('confact');
				var quantity = StockValidateInput.getQuantity(amount, convfact);
				if (quantity.length == 0) {
					quantity = 0;
				}
				if (parseInt(quantity, 10) < 0) {
					return false;
				}
				var result = id + ';' + quantity;
				arrProductObj.push(result);
			} else {
				var result = id + ';0';
				arrProductObj.push(result);
			}
		});
		if (lstCCMapProduct.length == 0 && arrProductObj.length == 0) {
			$('#errMsg').html('Vui lòng chọn danh sách sản phẩm trước khi lưu').show();
			return false;
		}
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.lstCCMapProduct = lstCCMapProduct;
		if(lstCCResultDelete.length > 0) {
			params.lstCCResultDelete = lstCCResultDelete;
		}
		params.cycleCountCode = $('#cycleCountCode').val().trim();
		params.productObject = arrProductObj.toString();
		$.ajax({
			type : "POST",
			url : "/stock/input/check-cccounted",
			data : ($.param(params, true)),
			dataType: "json",
			success : function(result) {
				if(result.ccmap_counted>0){
					var msg = 'Danh sách chọn có sản phẩm đã nhập dữ liệu chốt trước đó.';
					msg += '<div class="Clear"></div> <b>Đồng ý</b>: Chốt lại dữ liệu ';
					msg += '<div class="Clear"></div> <b>Hủy bỏ</b>: Không cập nhật lại số lượng kiểm kê';
					$.messager.confirm('Xác nhận', msg, function(r){
						if (r){		
							params.ischeckCountDate = true;
							Utils.saveData(params, '/stock/input/importdata',StockCountingInput._xhrSave,  'errMsg', function(data) {
								StockCountingInput.getInfo(false);	
								$("#successMsg").html("Lưu dữ liệu thành công ").show();	    			
	    						window.setTimeout('$("#successMsg").html("").hide();', 5000);	    						
							}, null, null, null, null, '1');
//						}else{
//							params.ischeckCountDate = false;
//							Utils.saveData(params, '/stock/input/importdata',StockCountingInput._xhrSave,  'errMsg', function(data) {
//								StockCountingInput.getInfo(false);	
//								$("#successMsg").html("Lưu dữ liệu thành công ").show();	    			
//	    						window.setTimeout('$("#successMsg").html("").hide();', 5000);	   								
//							}, null, null, null, null, '1');
						}
					});
				}else{
					Utils.addOrSaveData(params, "/stock/input/importdata", StockCountingInput._xhrSave, 'errMsg', function(result){
						if(!result.error){
							StockCountingInput.getInfo(false);
						}						
					}, null, null, false, 'Bạn có muốn nhập dữ liệu chốt ?');		
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						$('#loading').css('visibility','hidden');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	completeCheckStock: function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();		
		if (StockCountingInput.checkProductUotStockCountingInput()) {
			$('#errMsg').html('Không được Thêm sản phẩm mới khi Hoàn thành kiểm kho').show();
			return;
		}
		$.messager.confirm('Xác nhận', 'Bạn có muốn hoàn thành kiểm kho ?', function(r){
			if (r) {	
				var lstCCMapProduct = new Array();
				var lstCCResultDelete = new Array();
				$('input:checkbox[name=chkApprove]:checked').each(function() {
					var id = $(this).val();
					var checkLot = $('#lot' + id).val();
					if (checkLot == 0) {
						var amount = $('#td_qtyCount' + id).val().trim();
						var convfact = $('#td_qtyCount' + id).attr('confact');
						var quantity = StockValidateInput.getQuantity(amount, convfact);
						if (quantity.length == 0) {
							quantity = 0;
						}
						if (parseInt(quantity, 10) < 0) {
							return false;
						}
						var result = '(' + id + ',' + quantity + ')';
						lstCCMapProduct.push(result);
					} else {
						var result = '(' + id + ',-1)';
						var lstCCResult = StockCountingInput._myMap.get(id);
						if (lstCCResult != null && lstCCResult.length > 0) {
							for (var j = 0, sizej = lstCCResult.length; j < sizej; j++) {
								var ccresult = lstCCResult[j];
								if (parseInt(ccresult.quantityCounted, 10) < 0) {
									return false;
								}
								result += ';(' + ccresult.lot + ',' + ccresult.quantityCounted + ')';
								if (ccresult.deleted == 1) {
									lstCCResultDelete.push(id + ';' + ccresult.lot);
								}
							}
						} else {
							result = '(' + id + ',0)';
						}
						lstCCMapProduct.push(result);
					}
					
				});
				var params = new Object();
				params.shopCode = $('#shopCode').val();
				params.lstCCMapProduct = lstCCMapProduct;
				if(lstCCResultDelete.length > 0) {
					params.lstCCResultDelete = lstCCResultDelete;
				}
				params.cycleCountCode = $('#cycleCountCode').val().trim();		
				$.ajax({
					type : "POST",
					url : "/stock/input/check-cccounted",
					data : ($.param(params, true)),
					dataType: "json",
					success : function(result) {
						if (result.ccmap_counted > 0) {
							var msg = 'Danh sách chọn có sản phẩm đã nhập dữ liệu chốt trước đó.';
							msg += '<div class="Clear"></div> <b>Đồng ý</b>: Chốt lại dữ liệu ';
							msg += '<div class="Clear"></div> <b>Hủy bỏ</b>: Chỉ cập nhật lại số lượng kiểm kê';
							$.messager.confirm('Xác nhận', msg, function(r){
								if (r) {		
									params.ischeckCountDate = true;
									Utils.saveData(params, '/stock/input/completeCheckStock',StockCountingInput._xhrSave,  'errMsg', function(data) {
										StockCountingInput.getInfo(false);	
										$("#successMsg").html("Hoàn thành kiểm kho thành công").show();	    			
										window.setTimeout('$("#successMsg").html("").hide();', 5000);	    						
									}, null, null, null, null,'1');
								} else {
									params.ischeckCountDate = false;
									Utils.saveData(params, '/stock/input/completeCheckStock', StockCountingInput._xhrSave,  'errMsg', function(data) {
										StockCountingInput.getInfo(false);	
										$("#successMsg").html("Hoàn thành kiểm kho thành công").show();	    			
										window.setTimeout('$("#successMsg").html("").hide();', 5000);	   								
									}, null, null, null, null,'1');
								}
							});
						} else {
							Utils.saveData(params, "/stock/input/completeCheckStock", StockCountingInput._xhrSave, 'errMsg', function(result){
								if(!result.error){
									StockCountingInput.getInfo(false);
									$("#successMsg").html("Hoàn thành kiểm kho thành công").show();	    			
									window.setTimeout('$("#successMsg").html("").hide();', 5000);
								}						
							}, null, null, null, null, '1');		
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$.ajax({
							type : "POST",
							url : '/check-session',
							dataType : "json",
							success : function(data) {
								$('#loading').css('visibility','hidden');
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								window.location.href = '/home';
							}
						});
					}
				});
			}
		});
	},

	/**
	 * In file PDF kiem ke
	 * Khong cho hien thi: luu du lieu thanh cong
	 * @author vuongmq
	 * @since 03/09/2015 
	 */
	exportExcel: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.cycleCountCode = $('#cycleCountCode').val().trim();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var cycleType = $('#cycleCountType').val();
		var msgDialog = 'Bạn có muốn xuất phiếu kiểm kê kho này không?';
		if (cycleType == cycleCountType.COMPLETED || cycleType == cycleCountType.WAIT_APPROVED) {
			msgDialog = 'Bạn có muốn xuất báo cáo chênh lệch tồn kho của mã kiểm kê này không?';
		}
		Utils.addOrSaveData(params, '/stock/input/exportexcel', null, 'errMsg', function(data) {
			$('#errMsg').html('').hide();
			var filePath = ReportUtils.buildReportFilePath(data.path);
			window.location.href = filePath;
			setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
                CommonSearch.deleteFileExcelExport(data.path);
			}, 2000);
		}, 'loadingExportandSave', null, null, msgDialog, null, true);
		return false;
	},
	importExcel:function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		$('#isView').val(0);
		$('#cycleCountCodeImp').val($('#cycleCountCode').val());
		$('#shopCodeImp').val($('#shopCode').val());
		
		Utils.importExcelUtils(function(data){
			if (data.message != undefined && data.message != null && data.message.trim().length >0) {
				 var tm = setTimeout(function(){
	           		  $('#errExcelMsg').html(data.message.trim()).change().show();
	                   clearTimeout(tm);
	               }, 1500);
			}else{
				$('#successMsg').html(sale_plan_save_success).show();
				var tm = setTimeout(function(){
					$('#successMsg').html("").hide();
					clearTimeout(tm);
				 }, 5000);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
		return false;
	},	
 	/*viewExcel:function(){
 		$('#errExcelMsg').html('').hide();
		$('#sucExcelMsg').html('').hide();
 		$('#isView').val(1);
 		//Xu ly import 
		var options = { 
 			beforeSubmit: StockCountingInput.beforeImportExcel,   
 	 		success:      StockCountingInput.afterImportExcel,
 	 		type: "POST",
 	 		dataType: 'html'   
		}; 
 		options.data = {
			cycleCountCode:$('#cycleCountCode').val().trim(),
		 	startNumber: $('#startNumber').val().trim(),
		 	endNumber: $('#endNumber').val().trim()
		}; 
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
 		return false;
 	},*/
 	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		//showLoading();
		$('#errExcelMsg').html('').hide();
		$('#successMsg').html('').hide();
		showLoadingIcon();
		return true;
	},
 	afterImportExcel: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		//hideLoading();			
		if (statusText == 'success') {
	    	$("#responseDiv").html(responseText);
	    	
	    	var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		try {
		    		$('#tokenImport').val(newToken);
		    	} catch (e) {}
	    	}
    		if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    		$('#fakefilepc').val('');
				$('#excelFile').val('');
				return ;
	    	}
	    	else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
	    		var numFail = parseInt($('#numFail').html().trim());
	    		var fileNameFail = $('#fileNameFail').html();
	    		var mes = "";
	    		if (numFail != undefined && numFail != null && numFail > 0){
	    			mes += format(msgErr_result_import_excel_fail);
	    			mes+= ' <a href="' + fileNameFail +'">Xem chi tiết lỗi</a>';
	    			$('#errExcelMsg').html(Utils.XSSEncode(mes)).show();
	    		}else{ 
	    			$("#successMsg").html("Import dữ liệu thành công "+Number(totalRow)+" dòng").show();
	    			if($('#isView').val()=='0'){
	    				window.setTimeout('location.reload()', 3000);
	    			}
	    		}
	    	}
	    	$('#fakefilepc').val('');
			$('#excelFile').val('');
	    }	
	},
	showConfact:function(selector,convfact){
		$(selector).val(StockValidateInput.formatStockQuantity($(selector).val(), convfact));
		if($(selector).val().length==0){			
			$(selector).val('0/0');
		}
		StockCountingInput.showTotalQuantityAndScrollBody();
	},
	searchCycleCountOnEsyUI:function(){
		$('#saveMessageError').html('');
		var shopCode = $('#shop').combobox('getValue');
		if (shopCode != null && shopCode.length == 0) {
			$('#saveMessageError').html("Bạn chưa chọn đơn vị").show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		var	msg = Utils.getMessageOfInvalidFormatDate('dgCycleCountFromDate', 'Từ ngày tạo');
		if(msg.length > 0) {
			$('#saveMessageError').html(msg).show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		
		var	msg = Utils.getMessageOfInvalidFormatDate('dgCycleCountToDate', 'Đến ngày tạo');
		if(msg.length > 0) {
			$('#saveMessageError').html(msg).show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		
		var startDate = $('#dgCycleCountFromDate').val().trim();
		var endDate = $('#dgCycleCountToDate').val().trim();
		
		if(msg.length == 0 && !Utils.compareDate(startDate,endDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
			$('#dgCycleCountFromDate').focus();
			$('#saveMessageError').html(msg).show();
			return false;
		}
		var params = new Object();
		params.shopCode = shopCode;
		params.wareHouseId = $('#cycleCountWareHouse').combobox('getValue');
		params.cycleCountCode = $('.easyui-dialog #dgCycleCountCode').val().trim();
		params.cycleCountStatus = $('.easyui-dialog #dgCycleCountType').val();
		params.typeInventory = 2;
		params.startDate = $('.easyui-dialog #dgCycleCountFromDate').val().trim();
		params.endDate = $('.easyui-dialog #dgCycleCountToDate').val().trim();
		params.description = $('.easyui-dialog #dgCycleCountDes').val().trim();
		$('#grid').datagrid('load', params);
	},
	searchCycleCountOnDialog:function(){
		$('.ErrorMsgStyle').hide();
		StockCountingInput._isShowDlg = true;
		var html = $('#cycleCountDialog').html();
		$('#cycleCountEasyUIDialog').dialog({
			title: "Tìm kiếm kiểm kê",
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 850,
	        height :600,
	        onOpen: function(){
	        	StockCountingInput.loadComboboxShop("shop", '140');
	        	$('#cycleCountEasyUIDialog').addClass("easyui-dialog");
	        	$('#dgCycleCountType').addClass("MySelectBoxClass");
	        	$('#dgCycleCountType').customStyle();
	        	applyDateTimePicker('#dgCycleCountFromDate');
	        	applyDateTimePicker('#dgCycleCountToDate');
	        	$('#cycleCountEasyUIDialog .InputTextStyle:not(:disabled)').val('');
	        	$('#dgCycleCountType').val("-1").change();
	        	$('#dgCycleCountCode').parent().bind('keyup', function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			StockCountingApprove.searchCycleCountOnEsyUI();
	        		}
	        	});
	        	$('#cycleCountEasyUIDialog #grid').datagrid({
					url : '/stock/input/search-cycle-count',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					pageNumber:1,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					queryParams:{
						shopCode:$('#shopCode').val().trim(),
						typeInventory:2
					},
					pageList  : [10,20,30],
					width : $('#cycleCountEasyUIDialog #productGridContainer').width(),
				    columns:[[				          
		              	GridUtil.getNormalField("cycleCountCode", "Mã kiểm kê", 70, "left"),
					    GridUtil.getNormalField("description", "Mô tả", 200, "left"),
					    {field: 'status', title: 'Trạng thái', width: 80,sortable:false,resizable:false, align: 'left',formatter : CountingFormatter.statusFormat2},
					    {field: 'createDate',title: 'Ngày bắt đầu kiểm kê',width: 90, sortable:false,resizable:false, align: 'center', formatter: function(v, r, i) {
					    	if (v) {
					    		return $.datepicker.formatDate('dd/mm/yy', new Date(v));
					    	}
					    	return "";
					    }},
					    GridUtil.getNormalField("reason", "Lý do", 180, "left"),
					    {field: 'id', index:'id', hidden: true},
					    {field: 'firstNumber', index:'firstNumber', hidden: true},
					    {field: 'select', title : 'Chọn', width : 35, align : 'center', sortable : false, resizable : false,
					    	formatter:function(value,rowObject,opts){
					    		return "<a href='javascript:void(0)' onclick=\"return StockCountingInput.selectCycleCount("+ opts +");\">Chọn</a>";
					    	}
					   }
				    ]],
				    onBeforeLoad:function(param){
				    	
				    },
				    onLoadSuccess :function(data){
				    	GridUtil.updateDefaultRowNumWidth('#cycleCountEasyUIDialog', 'grid');
				    	$('.datagrid-header-rownumber').html('STT');
				    	Utils.bindAutoSearch();
				    }
				});  
	        },	     
	        onClose : function(){
	        	$('#cycleCountEasyUIDialog').dialog("destroy");
	        	$('#cycleCountDialog').html(html);
	        	StockCountingInput._isShowDlg = false;
	        }
	     });
	},
	selectCycleCount:function(indexGrid){
		var rows = $('#grid').datagrid('getRows')[indexGrid];
		if(rows!=null){
			$('#cycleCountCode').val(rows.cycleCountCode);
			$('.easyui-dialog').dialog('close');			
			$('#cbxShop').combobox('select', rows.shop.shopCode);
			$.getJSON('/rest/catalog/stock/cycleCount/' +rows.cycleCountCode+'/list.json', function(data){
				$('#endNumber').val(data);
				StockCountingInput.getInfo();
			});
		}
	},
	//load combobox don vi
	loadComboboxShop: function(shopIdField, panelWidth) {
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#' + shopIdField).combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: panelWidth,
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null){
			        		$('#shopCode').val(rec.shopCode);
			        		if(StockCountingInput._isShowDlg){
			        			StockCountingInput.changeShop();
			        		}
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#' + shopIdField).combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		var shopCode = $('#shopCode').val();
			        		var isHas = false;
			        		for (var i = 0; i < arr.length; i++) {
			        			if(arr[i].shopCode == shopCode) {
			        				isHas = true;
			        				break;
			        			}
			        		}
			        		if (isHas) {
			        			$('#' + shopIdField).combobox('select', shopCode);
			        		} else {
			        			$('#' + shopIdField).combobox('select', arr[0].shopCode);
			        		}
			        	}
			        }
				});
			}
		});	
	},
	changeShop: function() {
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.isSearch = true;
		
		 $.ajax({
			type : "POST",
			url : '/stock/counting/changeshop',
			data : params,
			dataType : "json",
			success : function(data) {
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null){
					$('#cycleCountWareHouse').combobox({
						valueField : 'warehouseId',
						textField : 'warehouseName',
						data : data.lstWarehouseVO,
						width:140,
						panelWidth:140,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
				        onLoadSuccess: function(){
				        	var arr = $('#cycleCountWareHouse').combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$('#cycleCountWareHouse').combobox('select', arr[0].warehouseId);
				        	}
				        }
					});
				 }
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	}
};


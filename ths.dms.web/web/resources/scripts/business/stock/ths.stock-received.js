var StockReceived = {
	_xhrSave : null,
	_amtMap:null,
	_storeProduct:null,
	_rowCount:null, //loctt- Oct10, 2013
	_allAmount: new Map(),//loctt - Oct16, 2013
	getInfo: function(){
		$('#errMsg').hide();
		$('#errMsg1').hide();
		$('#successMsg').hide();
		$('#stockTransCode').show();
		$('#btstock').show();
		var staffCode = $('#staffCode').combobox('getValue').trim();
		var data = new Object();
		data.staffCode = staffCode;
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		Utils.getHtmlDataByAjax(data, "/stock/received/getinfo", function(data){
			$("#productGrid").html(data).show();			
			StockReceived._amtMap = new Map();
			StockReceived._rowCount = $('#productGrid tr').length-1;//loctt - Oct10, 2013
		}, 'loading1', null);
		return false;
	},
	import: function(){
		$('#errMsg').hide();
		$('#successMsg').hide();
		$('#errMsg1').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('stockTransCode','Mã phiếu');
			if (msg.length == 0)
				{
					msg = Utils.getMessageOfSpecialCharactersValidate('stockTransCode','Mã phiếu',Utils._CODE);
				}
		}
		if(msg.length > 0){
			$('#errMsg1').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.staffCode = $('#staffCode').combobox('getValue').trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.stockTransCode = $('#stockTransCode').val().trim();		
		var url='/stock/received/import-auto';
		if(sys_separation_auto==0 || sys_separation_auto=='0'){
			/*
			 * Lay danh sach san pham duoi client
			 * CAU TRUC PRODUCT.CHECK_LOT=1: PRODUCT_CODE:{LOT/QUANTITY};
			 * CAU TRUC PRODUCT.CHECK_LOT=0; PRODUCT_CODE:QUANTITY;
			 * RESULT: PRODUCT_CODE:LOT1/QUANTITY,LOT2/QUANTITY;PRODUCT_CODE:QUANTITY;
			 */
			var lstProductVO = '';
			var MAP = new Map();
			var validate_error = false;
			$('#productGridBody .Quantity').each(function(){
				var object = new Object();
				var indexRow = $(this).attr('index-row');
				var amount = $(this).val().trim();
				var convfact = $('#Convafact' +indexRow).val().trim();
				var quantity = getQuantity(amount,convfact);
				var checkLot = $('#CheckLot' +indexRow).val().trim();
				var productCode = $('#ProductCode' +indexRow).val().trim();
				if(isNaN(quantity)){
					$('#errMsg1').html('Mã sản phẩm ' + Utils.XSSEncode(productCode) + ' có số lượng không hợp lệ.' ).show();		
					$('#Quantity' +indexRow).focus();
					validate_error=true;
					return false;
				}									
				var obj = MAP.get(productCode);
				if(obj!=null && checkLot==0){
					$('#errMsg1').html('Mã sản phẩm ' +Utils.XSSEncode(productCode) + ' tồn tại hơn 1 dòng trong bảng.' ).show();
					$('#Quantity' +indexRow).focus();
					validate_error=true;
					return false;					
				}else if(obj==null && checkLot==0){ 
					MAP.put(productCode,quantity);
					lstProductVO += productCode + ":" + quantity + ";";
				}else if(obj==null && checkLot==1){
					var str_lot = '';
					var SUB_MAP = new Map();
					$('#productGridBody .' +productCode).each(function(){
						var indexRowLot = $(this).attr('index-row');
						var lot = $('#Lot' +indexRowLot).val().trim();
						var msg = Utils.getMessageValidateLot('Lot' +indexRowLot);
						if(msg.length>0){
							$('#errMsg1').html(Utils.XSSEncode('Mã sản phẩm ' + Utils.XSSEncode(productCode) + '. ' + msg)).show();
							$('#Lot' +indexRowLot).focus();
							validate_error=true;
							return false;
						}
						var lotQuantity = $('#Quantity' +indexRowLot).val().trim();
						var convfact2 = $('#Convafact' +indexRow).val().trim();
						var newLot = SUB_MAP.get(lot);
						if(newLot!=null){
							$('#errMsg1').html('Mã sản phẩm ' + Utils.XSSEncode(productCode) + ' có số lô trùng nhau trong bảng. Một sản phẩm cùng lô phải nằm trên cùng một hàng' ).show();
							$('#Lot' +indexRowLot).focus();
							validate_error=true;	
							return false;
						}
						var product_lot = new Object();
						product_lot.lot = lot;
						product_lot.quantity = getQuantity(lotQuantity,convfact2);
						SUB_MAP.put(lot,product_lot);
					});
					var str_lot_of_product = "";
					for(var i=0;i<SUB_MAP.size();i++){
						var obj_lot = SUB_MAP.get(SUB_MAP.keyArray[i]); 
						str_lot_of_product += obj_lot.lot + "/" + obj_lot.quantity + ",";
					}
					if(str_lot_of_product.length>0){
						str_lot_of_product = str_lot_of_product.substring(0,str_lot_of_product.length-1);
					}					
					MAP.put(productCode,str_lot_of_product);
					lstProductVO += productCode + ":" + str_lot_of_product + ";";
				}		
			});
			if(lstProductVO.length>0){
				lstProductVO = lstProductVO.substring(0,lstProductVO.length-1);
			}
			if(validate_error){				
				return false;
			}
			if(MAP.size()<=0){
				$('#errMsg1').html('Không có mặt hàng nào được nhập.' ).show();
				return false;
			}
			dataModel.lstProductVO = lstProductVO;
			url = '/stock/received/import';
			$('#divOverlay').show();
			$.ajax({
				type : "POST",
				url : "/stock/received/check-product",
				data :(dataModel),
				dataType: "json",
				success : function(result) {
					$('#divOverlay').hide();
					if(result!=null && result.list_lots!=null && result.list_lots.length>0){
						var result_list_lot = result.list_lots;
						if(result_list_lot.length>0){
							var msg = 'Các sản phẩm có lô : ' + result_list_lot 
							+ '.<div class="Clear"></div> Không nằm trong danh sách lô xuất trong ngày.'
							+ '<div class="Clear"></div> Bạn có muốn nhập không?';
							$.messager.confirm('Xác nhận', msg, function(r){
								if(r){									
									Utils.saveData(dataModel, url,StockReceived._xhrSave,  'errMsg1', function(result) {
										StockReceived.callbackSuccess(result);							
									}, 'loading', null, null, null,'1');
								}
							});
							
						}
					}else{
						StockReceived.addOrSaveData(dataModel,url);
					}
				}
			});			
		}else{
			StockReceived.addOrSaveData(dataModel,url);
		}		
		return false;
	},
	addOrSaveData:function(dataModel,url){
		Utils.addOrSaveData(dataModel,url, StockReceived._xhrSave, 'errMsg', function(result){
			StockReceived.callbackSuccess(result);
			enable('btnExportExcel');
			$('.Func1Section').attr('style','width:486px;');
			$('#btnExportExcel').bind('click',function(){
				StockReceived.exportReport();
			});
		},'loading',null,null,null);
	},
	callbackSuccess:function(result){
		if(result.error){
			$('#errMsg1').html(result.errMsg).show();
			$('#btstock').hide();				
		}else{
			$('#successMsg1').show();
			$('#stockTransCode').attr("disabled", "disabled");
			$('#btstock').hide();
			$('#errMsg').hide();
			$('#errMsg1').hide();
			if(sys_separation_auto==0 || sys_separation_auto=='0'){
				$('.Quantity').each(function(){
					var objectId = $(this).attr('id');
					disabled(objectId);
				});
				$('.Lot').each(function(){
					var objectId = $(this).attr('id');
					disabled(objectId);
				});					 
				$('.DelProduct').each(function(){
					$(this).html('');
				});
				disabled('issAddProduct');
			}
		}
	},	
	searchProduct:function(){			
		var code = encodeChar($('.easyui-dialog #productCode').val().trim());
//		var name = encodeChar($('.easyui-dialog #productName').val().trim());
		var name = $('.easyui-dialog #productName').val().trim();
		var staffCode = $('#staffCode').combobox('getValue').trim();
		$('.easyui-dialog #grid').datagrid('load',{staffCode:staffCode,productCode :code, productName: name});
		return false;
	},
	openSelectProductDialog: function(){
		StockReceived._storeProduct = new Map();
		StockReceived._amtMap = new Map();
		$('#errMsg').hide();
		$('#productDialog').css("visibility", "visible");
		
		var html = $('#productDialog').html();
		$('#productEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        height :600,
	        onOpen: function(){	    
	        	$('.easyui-dialog #productCode').val('');
	    		$('.easyui-dialog #productName').val('');
	    		StockReceived._allAmount = new Map();
	        	$('.easyui-dialog #grid').datagrid({
					url : '/stock/received/search-product',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					queryParams:{
						staffCode:$('#staffCode').combobox('getValue').trim()
					},
					pageList  : [10,20,30],
					width : ($('.easyui-dialog #productGridContainer').width()),
				    columns:[[				          
				              	{field: 'productCode',index:'productCode',title: 'Mã sản phẩm', width: 70, sortable:false,resizable:false,align: 'left' },
							    {field: 'productName',index:'productName', title: 'Tên sản phẩm',  width: 200,sortable:false,resizable:false, align: 'left' },
							    {field: 'instock',index:'instock', title: 'Tồn kho', width: 50,sortable:false,resizable:false, align: 'right', formatter: IssuedStockGridFormatter.instockCellFormatterNew},
							    {field: 'price',index:'price', title: 'Giá',width: 70, sortable:false,resizable:false, align: 'right', formatter: GridFormatterUtils.currencyCellFormatter},
							    //loctt - Oct17, 2013
							    {field: 'amt',index:'amt', title: 'Số lượng',width: 60, sortable:false,resizable:false,align: 'right', formatter: function(value, row, index){		
									return '<input index=' +index+' row-id="' +row.id+'" id="productRow_' + row.id 
									+'" size="5" type="text" maxlength="8" style="text-align:right;margin: 0;width:77px;" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockIssued.amountBlured('
									+ index +');" onchange="StockReceived.formatQuantity(' +row.id+',' +row.convfact+');">';
								}},
//								    {field: 'amt',index:'amt', title: 'Số lượng',width: 60, sortable:false,resizable:false,align: 'right', formatter: IssuedStockGridFormatter.amountCellFormatter},			
							    {field: 'convfact', index:'convfact', hidden: true},
							    {field: 'id', index:'id', hidden: true},
							    {field: 'productId', index:'productId', hidden: true},							    
							    {field: 'grossWeight', index:'grossWeight', hidden: true},							    
							    {field: 'quantity', index:'quantity', hidden: true},
							    {field: 'availableQuantity', index:'availableQuantity', hidden: true},
							    {field: 'checkLot', index:'checkLot', hidden: true}	
				    ]],
				   onLoadSuccess :function(data){   	 
				    	$('.datagrid-header-rownumber').html('STT');
				    	Utils.bindAutoSearch();
				    	
				    	var rows = $('#grid').datagrid('getRows');
				    	for(var i = 0; i < rows.length; i++) {
//				    		if(StockReceived._allAmount.get(rows[i].id))
//				    			$('#grid').datagrid('checkRow', i);
				    		var obj = StockReceived._allAmount.get(rows[i].id);
				    		if (obj != null)
				    			$('#productRow_' +rows[i].id).val(obj.amount);
				    	}
				    	$('#productCode').focus();
				    	$('.datagrid-header-check input').removeAttr("checked");
				   
				    },
				    //loctt - Oct16, 2013 - begin
		            onBeforeLoad:function(node,param){
		            	var rows = $('#grid').datagrid('getRows');
		            	if(rows.length > 0){
		            		for(var i = 0; i < rows.length; i++) {
		            			var obj = StockReceived.getDataFromCurrentPage(rows[i].id, i);
		            			if(obj!=null)
					    		StockReceived._allAmount.put(rows[i].id, obj);
					    	}
		            		
		            	}
		            }
				    //loctt - Oct16, 2013 - end
				});  
	        },	     
	        onClose : function(){
	        	$('#productDialog').html(html);
	        	$('.easyui-dialog #errMsg').html('');//loctt - Oct10, 2013
	        	$('#productDialog').css("visibility", "hidden");
	        	$('#productGridContainer').html('<table id="grid" class="easyui-datagrid" ></table><div id="pager"></div>');	        }
	     });
		
		$('.easyui-dialog #productCode').focus();
		return false;
	},
	selectListProductsOnDialog:function(){
		$('.ErrorMsgStyle').html("").show().change();
		var indexRow = StockReceived.getLastNumRow();
		var flag = true;
		var productCount = 0;
		$('.IssuedStock_AmountValue').each(function(){
			var indexGrid = $(this).attr('index');			
			var productId =$(this).attr('row-id');
			if($(this).val().length>0){				
				var obj = StockReceived.getData(productId,indexRow,indexGrid);
				if(obj!=null){
					if(obj.amount == '0/0' ||obj.amount == '0'|| obj.amount==''){
						flag = false;	
						index = productId;
						$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();
						return false;
					}
					var total_accept = 0;
					var total_failed = 0;
					var total_failed = parseInt(getQuantity(obj.amount, obj.convfact));
					var total_accept = parseInt(getQuantity(obj.quantity, obj.convfact));
					if(total_accept < total_failed){
						$('.easyui-dialog #errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(obj.code + ' - ' + obj.name) + '. Vượt quá giá trị tồn kho').show();
						flag = false;	
						index = productId;
						return false;			
					} 				
					StockReceived._amtMap.put(obj.id, obj);
					++indexRow;
					var storeObject = StockReceived._storeProduct.get(obj.id);
					if(storeObject!=null){
						StockReceived._storeProduct.remove(obj.id);
					}
					productCount ++;
				}
			}
		
		});

		//loctt - Oct18, 2013 - begin
		for(var i = 0; i< StockReceived._allAmount.keyArray.length; i++){
			if(StockReceived._allAmount.keyArray[i]!= undefined){
				var obj = StockReceived._allAmount.valArray[i];
			//kiem tra xem san pham nay co trong amt map chua
				for(var j = 0; j< StockReceived._amtMap.keyArray.length; j++){
					if(StockReceived._amtMap.valArray[j].id == obj.id){
						StockReceived._amtMap.valArray[j].amount = obj.amount;
						break;
					}
				}
				if(j<StockReceived._amtMap.keyArray.length)//co gia tri trung
					continue;
				obj.indexRow = indexRow;
				if(obj!=null){
					if(obj.amount == '0/0' ||obj.amount == '0'|| obj.amount==''){
						flag = false;	
						index = productId;
						$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();
						return false;
					}
					var total_failed = parseInt(getQuantity(obj.amount, obj.convfact));
					var total_accept = parseInt(getQuantity(obj.quantity, obj.convfact));
					if(total_accept < total_failed){
						$('.easyui-dialog #errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(obj.code + ' - ' + obj.name) + '. Vượt quá giá trị tồn kho').show();
						flag = false;	
						index = productId;
						return false;			
					} 				
					StockReceived._amtMap.put(obj.id, obj);
					++indexRow;
					var storeObject = StockReceived._storeProduct.get(obj.id);
					if(storeObject!=null){
						StockReceived._storeProduct.remove(obj.id);
					}
					productCount ++;
				}
			}
		}
		//loctt - Oct18, 2013 - end
		
		//loctt -Oct9, 2013- fix 0011276 - begin
		if(productCount == 0 && $('.easyui-dialog #errMsg').html() == ''){
			$('.easyui-dialog #errMsg').html('Yêu cầu nhập số lượng').show();
			return false;
		}
		//loctt -Oct9, 2013- fix 0011276 - end
		
		if(!flag){	
			 $('#productRow_' + index).focus();
			return false;
		}
		for(var i=0;i<StockReceived._storeProduct.size();i++){
			var temp = StockReceived._storeProduct.get(StockIssued._storeProduct.keyArray[i]); 			
			StockReceived._amtMap.put(temp.id, temp);
		}
		if(StockReceived._amtMap==null || StockReceived._amtMap.size()==0){
			$('.fancybox-inner #errMsg').html('Vui lòng chọn sản phẩm cần xuất kho.').show();
			return false;
		}
		var totalWeight = 0;
		var totalMoney = 0;
		//loctt - Oct10, 2013 - fix 0011300 - begin
		if($('#productGrid').find('.NotData').text().length > 0)
			$('#productGrid .NotData').remove();
		//loctt - Oct10, 2013 - fix 0011300 -end
		for(var i=0;i<StockReceived._amtMap.size();i++){
			var obj = StockReceived._amtMap.get(StockReceived._amtMap.keyArray[i]);
			StockReceived.addProduct(obj);				
		}
		Utils.bindFormatOnTextfieldInputCss('Quantity', Utils._TF_NUMBER_CONVFACT);
		Utils.bindFormatOnTextfieldInputCss('Lot', Utils._TF_NUMBER);		
		
		StockReceived.showTotalGrossWeightAndPrice();
		$('.easyui-dialog').dialog('close');		
	},
	deleteProduct:function(rowNumber,id){	
		$.messager.confirm('Xác nhận', 'Bạn có chắc chắn muốn xóa dòng sản phẩm này ?', function(ret){
			if (ret == true) {
				if($('#ROW_' + rowNumber) != null && $('#ROW_' + rowNumber).html()!= null && $('#ROW_' + rowNumber).html().length > 0){					
					$('#ROW_' + rowNumber).replaceWith("");					
					StockReceived.showTotalGrossWeightAndPrice();
					if(id!=null && id!=undefined){
						StockReceived._amtMap.remove(id);
					}
					var i = 0;
					$('.RowNumber').each(function() {
					    ++i;
					    var chs = $(this).children();
					    $(this).html(i);
					    for (var jj = 0; jj < chs.length; jj++) {
					    	$(this).append(chs[jj].outerHTML);
					    }
					});
				}
			}
		});
	},
	getData:function(productId,indexRow,indexGrid){
		var rows = $('#grid').datagrid('getRows')[indexGrid];
		if(rows==null){
			return false;
		}		
		var amount = $('#productRow_' + productId).val().trim();
		if(amount.length==0){			
			return null;
		}				
		var convfact = rows.convfact;
		var price =  rows.price;		
		var obj = new Object();
		obj.indexRow = indexRow;
		obj.id = rows.id;	
		obj.productId = productId;
		obj.code = rows.productCode;
		obj.name = rows.productName;		
		obj.convfact = convfact;
		obj.grossWeight = rows.grossWeight;	
		obj.rowId = productId;		
		obj.display_amount = amount;
		obj.amount = formatQuantity(amount,convfact);		
		obj.price =  rows.price;
		obj.checkLot = rows.checkLot;		
		obj.totalMoney = getQuantity(amount, convfact) * price;
		obj.quantity = rows.quantity;
		obj.availableQuantity = rows.availableQuantity;
		return obj;
	},
	getDataFromCurrentPage:function(productId,indexGrid){
		var rows = $('#grid').datagrid('getRows')[indexGrid];
		if(rows==null){
			return false;
		}		
		var amount = $('#productRow_' + productId).val().trim();
		if(amount.length==0){			
			return null;
		}				
		var convfact = rows.convfact;
		var price =  rows.price;		
		var obj = new Object();
		obj.id = rows.id;	
		obj.productId = productId;
		obj.code = rows.productCode;
		obj.name = rows.productName;		
		obj.convfact = convfact;
		obj.grossWeight = rows.grossWeight;	
		obj.rowId = productId;		
		obj.display_amount = amount;
		obj.amount = formatQuantity(amount,convfact);		
		obj.price =  rows.price;
		obj.checkLot = rows.checkLot;		
		obj.totalMoney = getQuantity(amount, convfact) * price;
		obj.quantity = rows.quantity;
		obj.availableQuantity = rows.availableQuantity;
		return obj;
	},
	getLastNumRow:function(){
		var index = 0;
		$('#productGrid .RowNumber').each(function(){
			++index;
		});
		return ++index;
	},
	addProduct: function(obj){
		var html = new Array();	
		html.push('<tr id="ROW_' +obj.indexRow+'">');	
		html.push('<td style="border-left: 1px solid #C6D5DB;" class="AlignCCols RowNumber">' +obj.indexRow);
		html.push('<input type="hidden" id="ProductCode' +obj.indexRow+'" value="' +obj.code+'" />');
		html.push('<input type="hidden" id="Convafact' +obj.indexRow+'" value="' +obj.convfact+'" />');
		html.push('<input type="hidden" id="CheckLot' +obj.indexRow+'" value="' +obj.checkLot+'" />');
     	html.push('</td>');
		html.push('<td class="AlignLCols">' +Utils.XSSEncode(obj.code)+'</td>');
		html.push('<td class="AlignLCols Wordwrap">' +Utils.XSSEncode(obj.name)+'</td>');
		if (obj.checkLot == 1) {
			html.push('<td class="AlignRCols" style="padding: 4px 5px;"><input maxlength="6" index-row="' +obj.indexRow+'" id="Lot' +obj.indexRow+'" class="AlignRight Lot ' +Utils.XSSEncode(obj.code)+'" type="text" size="8" /></td>');
		}else{
			html.push('<td class="AlignRCols" style="padding: 4px 5px;"></td>');
		}
		html.push('<td style="padding: 4px 5px;" class="AlignRCols">');
		html.push('<input onblur="return StockReceived.qtyChanged(this);" onchange="return StockReceived.qtyChanged(this);" price="' +obj.price+'" gross-weight="' +obj.grossWeight+'" convfact="' +obj.convfact+'" index-row="' +obj.indexRow+'" type="text" size="8" class="AlignRight Quantity" value="' +obj.amount+'" id="Quantity' +obj.indexRow+'">');
		html.push('</td><td class="ColsTd6 AlignRight CurrencyNumber ">' +formatCurrency(obj.price)+'</td>');
		html.push('<td style="padding-right: 20px !important;" class="ColsTd7 AlignRight CurrencyNumber">' +formatCurrency(obj.totalMoney)+'</td>');
		html.push('<td class="ColsTd8 ColsTdEnd AlignCenter">');
		html.push('<a href="javascript:void(0)" onclick="return StockReceived.deleteProduct(' +obj.indexRow+',' +obj.id+');" class="DelProduct">');
		html.push('<img width="19" height="20" src="/resources/images/icon-delete.png"></a>');
		html.push('</td>');
		html.push('</tr>');
		$('#productGridBody').append(html.join(""));		
	},
	qtyChanged:function(selector){
		var convfact = $(selector).attr('convfact');
		var amount = $(selector).val().trim();		
		var amount = formatQuantity(amount,convfact);
		var quantity = getQuantity(amount, convfact);
		if(isNaN(quantity)){
			$(selector).val('0/0');
		}else{
			$(selector).val(amount);
		}		
		StockReceived.showTotalGrossWeightAndPrice();
	},
	showTotalGrossWeightAndPrice:function(){		
		var total_gross_weight = 0;
		var total_money = 0;
		$('.Quantity').each(function(){
			var amount = $(this).val().trim();
			var convfact = $(this).attr('convfact');
			var grossWeight = $(this).attr('gross-weight');
			var quantity = getQuantity(amount, convfact);
			total_gross_weight += quantity*grossWeight;
			var price = $(this).attr('price');
			total_money += price*quantity;
		});
		$('#totalWeight').html(formatFloatValue(total_gross_weight,2));
		$('#totalAmount').html(formatCurrency(total_money));
	},
	exportReport:function(){
		$('#errMsg').hide();
		var code = $('#stockTransCode').val().trim();		
		var data = new Object();
		data.code = code;
		data.vansaleCode = $('#staffCode').combobox('getValue').trim();
		//ReportUtils.exportReport('/stock/issued/exportStockOutVanSale',data);
		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/stock/received/exportReport",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					window.location.href = data.path;
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	//loctt - Oct17, 2013
	formatQuantity: function(id, convfact){
		$('#productRow_' +id).val(StockValidateInput.formatStockQuantity($('#productRow_' +id).val().trim(), convfact));
	}
};
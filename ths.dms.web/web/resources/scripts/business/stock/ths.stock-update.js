var StockUpdate = {
	_xhrSave: null,
	_amtMap: null,
	_quantityTemp : null,
	xhrExport: null,
	storeProduct:null,
	storeProductTmp:null,
	storeStockTotalMap : null,
	storeWarehouseMap : null,
	lstProductArray:new Array(),
	lstProductUpdate: new Array(),
	lstProductMap:new Map(),/** San pham co the dieu chinh cho NPP key=productId*/
	lstProductCodeMap:new Map(),/** San pham co the dieu chinh key=productCode */
	lstProductMapMap:null,/*danh sach san pham de chon. dc load luc load trang*/
	lstProductLotMap:new Map(),
	lstProductSelectionLotMap:new Map(),
	lstWarehouse : new Array(), /** Danh sach kho cua NPP */	
	TYPE:1,	
	selectProductId:null,
	indexEditing:null,
	indexLotEditing:null,
	isAuthorize : true,
	quantityChangeLot : null,
	isValidShop: false,
	nameTransDialog: null,
	fDateDialog: null,
	contentTransDialog: null,
	isEndCode: false,
	isProcessing: false,
	setCookieStockOutVanSale: function()
	{
		StockUpdate.nameTransDialog = $('#ipNameTransDialog').val();
		StockUpdate.fDateDialog = $('#fDateDialog').val();
	},

	getCookieStockOutVanSale : function(cname)
	{
		var name = cname.trim();
		name = name.trim().toLowerCase();
		var ca = document.cookie.split('>');
		for(var i=0; i<ca.length; i++)
		{
		  var c = ca[i].trim().toLowerCase();
		  if (c.indexOf(name)==0) return ca[i].trim().substring(name.length,c.length);
		}
		return "";
	},
	
	openStockOutVanSalePrintDialog :function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#stockOutVanSalePrintDiv').dialog({
			title : stock_update_pxk_kiem_vcnb,
			closed : false,
			onOpen : function() {				
				if(StockUpdate.nameTransDialog != null){
					$('#ipNameTransDialog').val(StockUpdate.nameTransDialog);
				}
				if(StockUpdate.fDateDialog != null){
					$('#fDateDialog').val(StockUpdate.fDateDialog);
				}
				$('#ipNameTransDialog').focus();
			},
			onClose:function(){
				$('#ipContentTransDialog').val('');
				$('#customerNameDialog').val('');
				//$('#ipContentTransDialog').val('Vận chuyển hàng đi bán lưu động');
			}
		});
		return false;
	},
	
	exportStockOutUpdate:function(){		
		$('.ErrorMsgStyle').hide();
		/**
		 * @author hunglm16
		 * @since JUNE 23, 2014
		 * @description Update xuat file giong bao cao 7-8-3.Phieu xuat kho kiem van chuyen noi bo
		 * */
		var msg = Utils.getMessageOfRequireCheck('ipNameTransDialog', stock_update_ten_lenh_dieu_dong);		
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipNameTransDialog', stock_update_ten_lenh_dieu_dong , Utils._NAME);
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('fDateDialog', stock_update_ngay_lenh_dieu_dong);			
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('fDateDialog', stock_update_ngay_lenh_dieu_dong);			
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length==0&&!Utils.compareDate(day + '/' + month + '/' + year, $('#fDateDialog').val().trim())){
			msg = msgCommonErr5;
			$('#fDateDialog').focus();
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ipContentTransDialog', stock_update_noi_dung);			
		}	
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipContentTransDialog', stock_update_noi_dung, Utils._NAME);
		}
		if(msg.length>0){
			$('#errMsg2').html(msg).show();
			return false;
		}
				
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = '';
		data.issuedDate = $('#fDateDialog').val().trim();
		data.stockTransId = $('#stockTransId').val();
		data.transacContent = $('#ipContentTransDialog').val().trim();
		data.transacName = $('#ipNameTransDialog').val().trim();
		try {
			dataModel.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		//Update them tham so xuat file report
		var kData = $.param(data,true);
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		$.ajax({
			type : "POST",
			url : "/stock/update/print-pxkkvcnb",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').removeClass('Overlay');
				$('#imgOverlay').hide();
				$('#divOverlay').hide();				
				if(!data.error) {
					StockUpdate.setCookieStockOutVanSale();
					var filePath = ReportUtils.buildReportFilePath(data.path);
					window.location.href = filePath;
				} else {
					$('#errMsg2').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').removeClass('Overlay');
				$('#divOverlay').hide();
				hideLoading(loading);
				StockIssued.xhrExport = null;
			}
		});
		return false;
		
	},
	importExcel : function() {
		$('.ErrorMsgStyle').html('').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$('#errMsg').html(data.message).show();
		}, 'importFrmStock_importFrm', 'excelFile', null, 'errExcelMsg');
	},
	loadGridProduct: function() {
		$('#dg').datagrid({
	    	data : StockUpdate.lstProductUpdate,
	    	singleSelect: true,			
			fitColumns:true,		
			scrollbarSize:0,		
			width: $('#idGridDg').width() - 5,
	    	columns:[[
	    	    {field:'rownum', title: jsp_common_numerical_order, width: 35, sortable:false,resizable:true , align: 'center',
	    	    	formatter:function(value,row,index){	    	    		
	    	    		if(!isNullOrEmpty(row.productId))
	    	    			return index+1;
	    	    	}
	    	    },
				{field:'productId',title:jsp_common_product_code,width:100, resizable:false, sortable:false, align:'left', 
	    	    	formatter: function(value,row,index) {
	    	    		if(isNullOrEmpty(row.productId)) return '';
	    	    		var product = StockUpdate.lstProductMap.get(row.productId);
	    	    		if(product!=null) return Utils.XSSEncode(product.productCode);
	    	    	}, 
	    	    	editor: {
	    	    		type:'combobox',
	    	    		options:{ 
							valueField:'productId',
							textField:'productCode',
							data:StockUpdate.lstProductArray,
							width:200,
							formatter: function(row) {
								return '<span style="font-weight:bold">' + Utils.XSSEncode(row.productCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.productName) + '</span>';
						    },
						    onSelect:function(rec){				
						    	var product = StockUpdate.lstProductMap.get(rec.productId);
					    		if(product!=null){
									$('.ErrorMsgStyle').html('').hide();
									var stockQuantity = 0;
					    			StockUpdate.selectProductId = rec.productId;				    			
						    		$('.datagrid-row-editing td[field=productName] div').html(Utils.XSSEncode(product.productName));
						    		$(this).combobox('setValue',product.productId);	
									/** Lay kho mac dinh cho combobox kho */
									var ed = $('#dg').datagrid('getEditor', {index:StockUpdate.indexEditing,field:'warehouseId'});
									if(ed != null && $(ed.target).combobox('getData').length > 0){
										var warehouseId = $(ed.target).combobox('getData')[0].id;
										$(ed.target).combobox('setValue',warehouseId);
										var stockQuantity = 0;
										var availableQuantity = 0;
										var stock_total = StockUpdate.storeStockTotalMap.get(product.productCode + warehouseId);
										if(stock_total != null){
											stockQuantity = stock_total.quantity;
											availableQuantity = stock_total.availableQuantity;
										}
										$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(stockQuantity,product.convfact));
										$('.datagrid-row-editing td[field=availableQuantity] div').html(formatQuantityEx(availableQuantity,product.convfact));
									}									
					    		}
						    },
						    filter: function(q, row){
						    	q = new String(q).toUpperCase();
								var opts = $(this).combobox('options');
								return row[opts.textField].indexOf(q)>=0;
						    },
						    onChange:function(newvalue,oldvalue){
						    	if(newvalue!=undefined && newvalue!=null){
						    		var product = StockUpdate.lstProductCodeMap.get(new String(newvalue).toUpperCase());

							    	if(product!=null){
							    		StockUpdate.isEndCode = true;	
							    		$(this).combobox('setValue',product.productId);									
							    	}
							    	if(StockUpdate.isEndCode && newvalue != null){
							    		StockUpdate.isEndCode = false;
							    		var productSelection = StockUpdate.lstProductMap.get(newvalue);
							    		if(productSelection != undefined && productSelection != null){
							    			var stockQuantity = 0;
					    					StockUpdate.selectProductId = newvalue;
					    					$('.datagrid-row-editing td[field=productName] div').html(Utils.XSSEncode(productSelection.productName));
					    					$(this).combobox('setValue',productSelection.productId);	
					    					var ed = $('#dg').datagrid('getEditor', {index:StockUpdate.indexEditing,field:'warehouseId'});
					    					if(ed != null && $(ed.target).combobox('getData').length > 0){
												var warehouseId = $(ed.target).combobox('getData')[0].id;
												$(ed.target).combobox('setValue',warehouseId);
												var stockQuantity = 0;
												var availableQuantity = 0;
												var stock_total = StockUpdate.storeStockTotalMap.get(productSelection.productCode + warehouseId);
												if(stock_total != null){
													stockQuantity = stock_total.quantity;
													availableQuantity = stock_total.availableQuantity;
												}
												$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(stockQuantity,productSelection.convfact));
												$('.datagrid-row-editing td[field=availableQuantity] div').html(formatQuantityEx(availableQuantity,productSelection.convfact));
											}	
							    		}

							    	}
						    	}	
								$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});
							}
					}
				}},
				{field: 'productName', title: jsp_common_product_name, width:350, formatter:function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		          }},
				{field: 'stockQuantity', title: jsp_common_stock_quantity, width:100,align:'right',
					formatter:function(value,row,index){
						return formatQuantityEx(value,row.convfact);
					}
				},
				{field: 'availableQuantity', title: jsp_common_stock_available_quantity, width:100,align:'right',
					formatter:function(value,row,index){
						return formatQuantityEx(value,row.convfact);
					}
				},
				{field: 'warehouseId', title: jsp_common_warehouse, width:150,align:'left',
					formatter:function(value,row,index){
						return Utils.XSSEncode(row.warehouseName);;
					},
					editor: {
	    	    		type:'combobox',						
	    	    		options:{ 
							valueField:'id',
							textField:'warehouseName',
							data : StockUpdate.lstWarehouse,
							width:200,	
							formatter: function(row) {
								return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.warehouseName) + '</span>';
						    },
							filter: function(q, row){
								q = new String(q).toUpperCase().trim();
								var opts = $(this).combobox('options');
								return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
							},							
						    onSelect:function(rec){	
								var product = StockUpdate.lstProductMap.get(StockUpdate.selectProductId);
								if(product != null){
									var warehouseId = rec.id;
									var stockQuantity = 0;
									var availableQuantity = 0;
									var stock_total = StockUpdate.storeStockTotalMap.get(product.productCode + warehouseId);
									if(stock_total != null){
										stockQuantity = stock_total.quantity;
										availableQuantity = stock_total.availableQuantity;
									}
									$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(stockQuantity,product.convfact));
									$('.datagrid-row-editing td[field=availableQuantity] div').html(formatQuantityEx(availableQuantity,product.convfact));
								}								
						    },
						    onChange:function(newvalue,oldvalue){						    	
							}
						}
					}
				},
				{field:'quantity', title: jsp_common_so_luong, width:100, resizable:false, sortable:false, align:'right', 
					formatter: function(value,row,index) {
						if(isNullOrEmpty(row.productId)) return '';
	    	    		var product = StockUpdate.lstProductMap.get(row.productId);
	    	    		if(product!=null){
	    	    			return Utils.XSSEncode(value);
	    	    		}					
					},
					editor:{type : 'text'}
				},
				{field:'delete', title: jsp_common_xoa, width:30, resizable:false, sortable:false, align:'center', 
					formatter: function(value, row, index) {
						if (isNullOrEmpty(row.productId)) {
							return '';
						} 
	    	    		var product = StockUpdate.lstProductMap.get(row.productId);
	    	    		if (product != null) {
	    	    			return '<a href="javascript:void(0)" onclick="StockUpdate.delSelectedRow(' +index+');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a>';
	    	    		}				
					}
				}
	    	]],
	    	onAfterEdit:function(index,row){
	    		StockUpdate.indexEditing = index;
		    	var productId = row.productId;
		    	var product = StockUpdate.lstProductMap.get(productId);
		    	if(product!=null){
		    		row.productCode = product.productCode;
		    		row.productName = product.productName;		    		
		    		row.checkLot = product.checkLot;					
					var stockQuantity = 0;
					var availableQuantity = 0;
					if(row.warehouseId != null && row.warehouseId != ''){	
						var stock_total = StockUpdate.storeStockTotalMap.get(product.productCode + row.warehouseId);						
						if(stock_total != null){
							stockQuantity = stock_total.quantity;		
							availableQuantity = stock_total.availableQuantity;				
						}										
						row.warehouseName = StockUpdate.storeWarehouseMap.get(row.warehouseId).warehouseName;	
					}					
					row.stockQuantity = stockQuantity;
					row.availableQuantity = availableQuantity;
		    		row.convfact = product.convfact;
		    		row.quantity =  formatQuantity(row.quantity,product.convfact);
		    		row.quantityValue = getQuantity(row.quantity,product.convfact);
		    		if(isNaN(row.quantityValue)){
          				row.quantityValue = 0;
          			} 
		    		$('#dg').datagrid('refreshRow', index);
		    	}	    		
	    	},
	    	onBeforeEdit :function(rowIndex,rowData){
		    	var rows = $('#dg').datagrid('getRows');
		    	for(var i=0;i<rows.length;++i){
		    		if(i==rowIndex)
		    			continue;
		    		$('#dg').datagrid('endEdit', i);    		
		    	}	
				var ed = $('#dg').datagrid('getEditor', {index:rowIndex,field:'warehouseId'});
				if(ed != null){
					$(ed.target).combobox('loadData',$(ed.target).combobox('getData'))
				}
				
			},
			onDblClickRow:function(rowIndex, rowData) {
				if(!StockUpdate.isAuthorize){
					return false;
				}
				if(!StockUpdate.isValidShop) {
					return false;
				}
				$('.ErrorMsgStyle').html('').hide();				
//				var msg = Utils.getMessageOfRequireCheck('shopCode', jsp_common_shop_code);
//				if(msg.length>0){
//					$('#errMsg').html(msg).show();
//					return false; 
//				}	
				$('#dg').datagrid('beginEdit', rowIndex);	
				$('.datagrid-row-editing td[field=quantity] input').css('text-align','right');
				var productId = rowData.productId;
				var product = StockUpdate.lstProductMap.get(productId);
				if(!isNullOrEmpty(rowData.productId) && product!=null){					
					StockUpdate.disabledProductIdCols(rowIndex, product);
					if(product.checkLot!=1){
		    			$('.datagrid-row-editing td[field=lot] table').css('display','none');
		    		}else{
		    			$('.datagrid-row-editing td[field=lot] table').css('display','block');						    			
		    		}
				}else{
					$('.datagrid-row-editing td[field=productId] input').focus();
				}
			},
	    	onLoadSuccess: function() { 
	    		//Xu ly phan quyen va Hien thi
	    		var arrDelete =  $('#idGridDg td[field="delete"]');
	    		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
	    		  for (var i = 0, size = arrDelete.length; i < size; i++) {
	    			$(arrDelete[i]).prop("id", "gr_table_td_del_" + i);//Khai bao id danh cho phan quyen
	    			$(arrDelete[i]).addClass("cmsiscontrol");
	    		  }
	    		}
	    		Utils.functionAccessFillControl('idGridDg', function(data) {
	    			//Xu ly cac su kien lien quan den cac control phan quyen
	    			var arrTmpLength =  $('#idGridDg td[id^="gr_table_td_del_"]').length;
	    			var invisibleLenght = $('.isCMSInvisible[id^="gr_table_td_del_"]').length;
	    			if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
	    				$('#dg').datagrid("showColumn", "delete");
	    			} else {
	    				$('#dg').datagrid("hideColumn", "delete");
	    			}
	    		});
//	    		if ($('#idGridDg td[field="delete"] a').length == 0) {
//	    			$('#dg').datagrid("hideColumn", "delete");
//	    		}
	    	},
	    	onLoadError: function(){
	    	}, 
	    	onBeforeLoad: function(){
	    	}

		});	
		
	},
	synchronization:function(){
		$('.ErrorMsgStyle').html('').hide();
		var typeUpdate = $('#typeUpdate').val().trim();	
		var shopCode = $('#shopCbx').combobox('getValue').trim();
		if(shopCode.length==0){
			return false;
		}
		var params = new Object();
		params.typeUpdate = typeUpdate;
		params.shopCode = shopCode.trim();
		Utils.getJSONDataByAjax(params,'/stock/update/init-data-product',function(data){
			$('#common-dialog-search-2-textbox').dialog('close');
			if(!data.error) {
				StockUpdate.lstProductUpdate = new Array();
				StockUpdate.lstProductUpdate.push(new Object());
				
				StockUpdate.lstProductArray = new Array();
				StockUpdate.lstProductArray = data.rows;
				var len = data.rows.length;
				var products = data.rows;
				for(var i=0;i<len;++i){
					StockUpdate.lstProductCodeMap.put(products[i].productCode,products[i]);
					StockUpdate.lstProductMap.put(products[i].productId,products[i]);
				}
				StockUpdate.storeStockTotalMap = new Map();
				// Dong bo ton kho NPP
				var stock_total = data.stock_total;
				for(var i = 0, size = stock_total.length; i< size ; ++ i){
					var productCode = stock_total[i].productCode;
					var warehouseId = stock_total[i].warehouseId;
					StockUpdate.storeStockTotalMap.put(productCode + warehouseId,stock_total[i]);
				}
				StockUpdate.lstWarehouse = data.warehouses;
				StockUpdate.storeWarehouseMap = new Map();
				for(var i = 0, size = StockUpdate.lstWarehouse.length; i < size; ++i){
					var warehouse = StockUpdate.lstWarehouse[i];
					var text = Utils.XSSEncode(warehouse.warehouseCode + ' - ' + warehouse.warehouseName);
					warehouse.searchText = unicodeToEnglish(text).toUpperCase();
					StockUpdate.storeWarehouseMap.put(warehouse.id,warehouse);
				}
				$('#shopId').val(data.shop.id);
				$('#isShop').val(1);	
				$('#issuedDate').val(data.dateStr);			
					StockUpdate.isValidShop = true; // check co du lieu product
					StockUpdate.loadGridProduct();
					StockUpdate.indexEditing = 0;							
					$('#dg').datagrid('beginEdit', 0);
			} else {
				$('#shopId').val('');
				$('#errMsg').html(data.errMsg).show();
				$('#isShop').val(0);
			}
		});		
	},
	disabledProductIdCols:function(editIndex,product){
		var productCode = product.productCode;
		$('#span' +editIndex).remove();			
		$('td[field=productId] div table tbody tr td input').css('display','none');
		$('td[field=productId] div table tbody tr td span').css('display','none');
		$('td[field=productId] div table tbody tr td').append('<span style="padding-left: 4px;" id="span' +editIndex+'"></span');
		$('#span' +editIndex).html(Utils.XSSEncode(productCode));		
		$('.datagrid-row-editing td[field=quantity] input').focus();
		if(product.checkLot!=1){
			$('.datagrid-row-editing td[field=lot] table').css('display','none');
		}else{
			$('.datagrid-row-editing td[field=lot] input').css('text-align','center');
		}
	},
	setEventInput:function(){
		$('td[field=productId] input.validatebox-text').live('keypress', function(e) {
			/*if(e.keyCode == keyCode_F9 && !e.shiftKey) {
				StockUpdate.openSelectProductDialog();
			}*/
			
			var productCode = $(this).val().trim().toUpperCase();
			if(e.keyCode == keyCode_TAB && !e.shiftKey) {	
				var product = StockUpdate.lstProductCodeMap.get(productCode);
				if(product!=null){
					StockUpdate.selectProductId = product.productId;				    			
		    		$('.datagrid-row-editing td[field=productName] div').html(Utils.XSSEncode(product.productName));
		    		if(product.checkLot!=1){
		    			$('.datagrid-row-editing td[field=lot] table').css('display','none');
		    		}else{
		    			$('.datagrid-row-editing td[field=lot] table').css('display','block');						    			
		    		}
		    		$('.ErrorMsgStyle').html('').hide();
				}		
			}
		});
		$('td[field=productId] input.validatebox-text').live('blur',function(e){
			var productCode = $(this).val().trim().toUpperCase();
			if(e.keyCode == keyCode_TAB && !e.shiftKey) {	
				var product = StockUpdate.lstProductCodeMap.get(productCode);
				if(product!=null){
					StockUpdate.selectProductId = product.productId;				    			
		    		$('.datagrid-row-editing td[field=productName] div').html(Utils.XSSEncode(product.productName));
		    		if(product.checkLot!=1){
		    			$('.datagrid-row-editing td[field=lot] table').css('display','none');
		    		}else{
		    			$('.datagrid-row-editing td[field=lot] table').css('display','block');						    			
		    		}
		    		$('.ErrorMsgStyle').html('').hide();
				}		
			}
		});
		$('.datagrid-row-editing td[field=quantity] input').live('focus', function(e){
			$(this).attr('id','convfact_value' +StockUpdate.indexEditing);			
			$(this).attr('maxlength','8');
			Utils.bindFormatOnTextfield('convfact_value' +StockUpdate.indexEditing, Utils._TF_NUMBER_CONVFACT);	
			$(this).css('text-align','right');
		});
		$('.datagrid-row-editing td[field=lot] input').live('focus', function(e){
			$(this).attr('id','lot' +StockUpdate.indexEditing);			
			$(this).attr('maxlength','6');
			Utils.bindFormatOnTextfield('lot' +StockUpdate.indexEditing, Utils._TF_NUMBER);	
			$(this).css('text-align','center');
		});
		$('.datagrid-row-editing td[field=lot] input').live('blur', function(e){
			var lot = $(this).val().trim();
			var row = $('#dg').datagrid('getRows')[StockUpdate.indexEditing];
			if(row!=null && row.checkLot==1){
				var msg = Utils.getMessageValidateLot('lot' +StockUpdate.indexLotEditing);
				if(msg.length>0){
					$('#errMsg').html(msg).show();
					var stm = setTimeout(function(){						
						$(this).focus();
						clearTimeout(stm);
					},50);
					return false;
				}
			}
		});
		$('.datagrid-row-editing td[field=lot] input').live('change', function(e){
			var lot = $(this).val().trim();
			var row = $('#dg').datagrid('getRows')[StockUpdate.indexEditing];
			if(row!=null && row.checkLot==1){
				var msg = Utils.getMessageValidateLot('lot' +StockUpdate.indexEditing);
				if(msg.length>0){
					$('#errMsg').html(msg).show();
					var stm = setTimeout(function(){						
						$(this).focus();
						clearTimeout(stm);
					},50);
					return false;
				}
			}
		});
		$('.datagrid-row-editing td[field=quantity] input').live('keyup', function(e){					
			if(e.keyCode==keyCodes.ENTER) {	
				$('#dg').datagrid('acceptChanges');
		    	StockUpdate.appendOrSelectRow();		    	
			}
		});		
		$('.easyui-dialog input.QUANTITY').live('keyup', function(e){					
			if(e.keyCode==keyCodes.ENTER) {
				var index = $(this).attr('index');
				var productLot = StockUpdate.lstProductSelectionLotMap.get(index);
				if(productLot!=null){			
					productLot.quantityLot = getQuantity($(this).val().trim(),StockIssued.CONVFACT);
					StockUpdate.lstProductSelectionLotMap.put(String(index),productLot);
				}
				var dg = '.easyui-dialog #productLotGrid';
				$(dg).datagrid('acceptChanges');
				var editIndex = $(dg).datagrid('getRows').length;			
		    	if(StockUpdate.indexLotEditing == editIndex-1) {    		
		    		var row = $(dg).datagrid('getRows')[editIndex-1];
		    		var product = StockUpdate.lstProductMap.get(row.productId);
		    		if(row==null || isNullOrEmpty(row.lot)){  
		    			editIndex = editIndex-1;
		    		}else if(!isNullOrEmpty(row.lot)){    			
		    			$(dg).datagrid('appendRow',new Object());
		    			$(dg).datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
		    			$('.datagrid-row-editing td[field=lot] input').focus();
		    			StockUpdate.indexLotEditing = editIndex;
		    			StockUpdate.lstProductSelectionLotMap.put(String(editIndex),new Object());
		    		}	
		    	}	    		    	
			}
		});	
	},
	appendOrSelectRow:function(){
		var editIndex = $('#dg').datagrid('getRows').length;			
    	if(StockUpdate.indexEditing == editIndex-1) {    		
    		var row = $('#dg').datagrid('getRows')[editIndex-1];
    		var product = StockUpdate.lstProductMap.get(row.productId);
    		if(row==null || isNullOrEmpty(row.productId)){  
    			editIndex = editIndex-1;
    		}else if(!isNullOrEmpty(row.productId)){    			
    			if(product!=null){
    				if(isNullOrEmpty(row.quantity) || StockIssued.validateQuantity(row.quantity,product.convfact)){
    					$('#dg').datagrid('appendRow',new Object());
    				}    				
    			}
    		}	
    	}else{
    		var row = $('#dg').datagrid('getRows')[StockUpdate.indexEditing];
    		var product = StockUpdate.lstProductMap.get(row.productId);
    		editIndex = StockUpdate.indexEditing +1;
    	}    	
    	StockUpdate.indexEditing = editIndex;
    	$('#dg').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
    	var productCode = $('.datagrid-row-editing td[field=productId] input.validatebox-text').val();
    	if(!isNullOrEmpty(productCode)){    		
    		var pr = $('#dg').datagrid('getRows')[editIndex];
    		StockUpdate.disabledProductIdCols(editIndex, pr);
    	}else{
    		$('.datagrid-row-editing td[field=productId] input').focus();
    	}     	
	},
	openSelectProductDialog: function(){
		$('#issSearchProductDialog').unbind('click');
		StockUpdate.storeProduct = new Map();	
		$('.SelProduct').each(function(){
			$(this).val('');
		});		
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('.ErrorMsgStyle').html('').hide();
//		var msg = Utils.getMessageOfRequireCheck('shopCode', jsp_common_shop_code);
//		if(msg.length>0){
//			$('#errMsg').html(msg).show();
//			return false; 
//		}		
		$('#productDialog').css("visibility", "visible");
		var html = $('#productDialog').html(); 
		$('#productEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        height :550,
	        onOpen: function(){
	        	$('.easyui-dialog #issSearchProductDialog').css('margin-left', 285);
				$('.easyui-dialog #issSearchProductDialog').css('margin-top', 10);	
				$('#issSearchProductDialog').bind('click',StockUpdate.searchProduct);
				$('.easyui-dialog #productCode').val('');
				$('.easyui-dialog #productName').val('');
				var shopCode = $('#shopCbx').combobox('getValue');
				var typeUpdate = $('#typeUpdate').val();
				var params = new Object();
				params.shopCode = shopCode,
				params.typeUpdate =  typeUpdate;				
				Utils.bindAutoSearch();	
	        	$('.easyui-dialog #productGrid').datagrid({
					url : '/stock/update/search-product',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					pageNumber:1,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					pageList  : [10,20,30],
					queryParams:params,					
					width : ($('.easyui-dialog #productGridContainer').width()),
				    columns:[[	
						{field: 'productCode', title: jsp_common_product_code, width: 160, sortable:false,resizable:false,align: 'left', formatter:function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          } },
						{field: 'productName',title: jsp_common_product_name, width: 320,sortable:false,resizable:false, align: 'left', formatter:function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          } },
						{field: 'stockQuantity',title: jsp_common_ton_kho,width: 80, sortable:false,resizable:false, align: 'right', 
							formatter: function(value,row,index){
					    		return formatQuantityEx(row.stockQuantity,row.convfact);
					    }},
						{field: 'warehouseName',title: jsp_common_warehouse,width: 150},
						{field: 'price',title: jsp_common_gia, width: 70,sortable:false,resizable:false, align: 'right', formatter: GridFormatterUtils.currencyCellFormatter},
						{field: 'quantity',title: jsp_common_so_luong,width: 100, sortable:false,resizable:false, align: 'right', formatter: UpdateStockGridFormatter.amountCellFormatter}									        
				    ]],
				    onBeforeLoad:function(param){
				    	$('.easyui-dialog .SelProduct').each(function(){
							  var index = $(this).attr('index');
							  var obj = StockUpdate.getData(index,$(this));										 
							  if(obj!=null){
								  StockUpdate.storeProduct.put(obj.productId,obj);  
							  }else {
								  var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
								  var temp = StockUpdate.storeProduct.get(rows.productId);
								  if(temp!=null){
									  StockUpdate.storeProduct.remove(temp.productId);
								  }
							  }
						  });	
				    },
				    onLoadSuccess :function(data){  
				    	$('.easyui-dialog #productCode').focus();
				    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);				    	
				    	Utils.bindFormatOnTextfieldInputCss('SelProduct',Utils._TF_NUMBER_CONVFACT,'.easyui-dialog');
						$('.easyui-dialog .SelProduct').each(function(){
							  var productId = $(this).attr('row-id');							 								  
							  var obj = StockUpdate.storeProduct.get(productId);
							  if(obj!=null) $(this).val(obj.quantity);
							  else $(this).val('');							
						});						
				    }
				});  
	        },	        
	        onClose : function(){
	        	$('#productDialog').html(html);
	        	$('#productDialog').css("visibility", "hidden");        	
	        }
	     });	
		$('#errMsg').hide();
		return false;
	},
	getData:function(index,selector){
		var product = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];	
		var quantity = selector.val().trim();
		if(quantity.length==0){			
			return null;
		}
		var row = new Object();
		row.productId = product.productId;
		row.productCode = product.productCode;
		row.productName = product.productName;
		row.stockQuantity = product.stockQuantity;		
		row.checkLot = product.checkLot;
		row.convfact = product.convfact;
		row.quantity = formatQuantityEx(quantity, row.convfact);
		row.quantityValue = getQuantity(row.quantity, row.convfact);
		if(isNaN(row.quantityValue)){
			return null;
		}		
		if (row.quantityValue <= 0) {
			row.quantity = '';
		}		
		return row;
	},	
	searchProduct: function(){
		$('.ErrorMsgStyle').html('').hide();
		var code = $('.easyui-dialog #productCode').val().trim();
		var name = $('.easyui-dialog #productName').val().trim();
		var shopCode = $('#shopCbx').combobox('getValue').trim();		
		var params = new Object();
		params.productCode = code;
		params.productName = name;
		params.shopCode = shopCode,
		params.typeUpdate =  StockUpdate.TYPE;
		params.isKeyUpF9 = true;
		$('.easyui-dialog #productGrid').datagrid('load',params);
		return false;
	},
	selectListProducts: function(){
		$('.ErrorMsgStyle').html('').hide();
		var mapProductSelection = new Map();
		$('.easyui-dialog #errMsg').html('').hide();
		var flag = true;
		var index = 0;
		var typeUpdate = $('#typeUpdate').val().trim();
		$('.easyui-dialog .SelProduct').each(function(){
			var index = $(this).attr('index');
			if($(this).val().length>0){				
				var obj = StockUpdate.getData(index,$(this));
				if(obj!=null){
					index = obj.productId;
					if(obj.quantity == '0/0' ||obj.quantity == '0'|| obj.quantity==''){										
						$('.easyui-dialog #errMsg').html(stock_update_gia_tri_so_luong_err).show();
						flag = false;
						return false;
					}						
					if(obj.stockQuantity < obj.quantityValue && StockUpdate.TYPE==1){
						$('.easyui-dialog #errMsg').html(jsp_common_product_code + ' ' + Utils.XSSEncode(obj.productCode + ' - ' + obj.productName) + '. ' + stock_update_vuot_qua_gia_tri_ton_kho).show();
						flag = false;						
						return false;			
					} 				
					mapProductSelection.put(obj.productId, obj);
					var storeObject = StockUpdate.storeProduct.get(obj.productId);
					if(storeObject!=null){
						StockUpdate.storeProduct.remove(obj.productId);
					}
				}				
			}else{
				var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
				var temp = StockUpdate.storeProduct.get(rows.productId);
				if(temp!=null){
					StockUpdate.storeProduct.remove(temp.productId);
				}
			}
		});
		if(!flag){	
			 $('#productRow_' + index).focus();
			return false;
		}
		try{
			for(var i=0;i<StockUpdate.storeProduct.size();i++){
				var product = StockUpdate.storeProduct.get(StockUpdate.storeProduct.keyArray[i]); 
				mapProductSelection.put(product.productId, product);
			}
			if(mapProductSelection.size()==0){
				$('.easyui-dialog #errMsg').html(stock_update_chon_sp_xuat_kho).show();
				return false;
			}	
			for(var i=0;i<mapProductSelection.size();i++){
				var data = mapProductSelection.get(mapProductSelection.keyArray[i]); 
				var len = $('#dg').datagrid('getRows').length-1;
				$('#dg').datagrid('insertRow',{index:len, row:data});
			}		
			var arrDelete = StockUpdate.duplicateProducts();
			for(var i=0;i<arrDelete.length;++i){
				var obj = arrDelete[i];
				var row = $('#dg').datagrid('getRows')[obj.lastIndex];			
				$('#dg').datagrid('getRows')[obj.firstIndex].quantity = row.quantity;
				$('#dg').datagrid('getRows')[obj.firstIndex].quantityValue = row.quantityValue;			
				$('#dg').datagrid('deleteRow',obj.lastIndex);					
			}
		}
		catch(e){
			$('#dg').datagrid('loadData', $('#dg').datagrid('getRows'));
			$('.easyui-dialog').dialog('close');
		}
		$('#dg').datagrid('loadData', $('#dg').datagrid('getRows'));
		$('.easyui-dialog').dialog('close');
	},
	duplicateProducts:function(){
		var arrDelete = new Array();
		var len = $('#dg').datagrid('getRows').length;
		var mapTestingDuplicate = new Map();
		var mapDuplicateIndex = new Map();
		for(var i=0;i<len;++i){			
			var product = $('#dg').datagrid('getRows')[i];
			var productId = product.productId;
			if(product.checkLot==1){
				continue;
			}
			if(mapTestingDuplicate.get(productId)==null){
				mapTestingDuplicate.put(productId,product);		
				mapDuplicateIndex.put(productId,i);
			}else{
				var obj = new Object();
				obj.firstIndex = mapDuplicateIndex.get(productId);
				obj.lastIndex = i;
				arrDelete.push(obj);
			}			
		}
		return arrDelete;
	},
	qtyChanged:function(index){
		$('#errMsg').html('').hide();
		var msg = '';
		var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];	
		var selector =  $('#productRow_' + rows.productId);
		var amount = formatQuantity(selector.val().trim(),rows.convfact);		
		if(amount == '0/0' || amount == '0'){
			msg = stock_update_gia_tri_sl_khong_hop_le;					
			return false;
		}
		var productCode = rows.productCode;
		var productName = rows.productName;	
		var quantityInput = Number(getQuantity(amount,rows.convfact));
		var stockQuantity = rows.stockQuantity;
		if(msg.length==0 && StockUpdate.TYPE==1 && quantityInput>stockQuantity){
			msg = jsp_common_product_code + ' ' + Utils.XSSEncode(productCode + ' - ' + productName) + '. ' + stock_update_vuot_qua_gia_tri_ton_kho;	
		}	
		if(msg.length>0){
			$('.easyui-dialog #errMsg').html(msg).show();
			var tm = setTimeout(function() {	
				selector.focus();			
				clearTimeout(tm);
			}, 50);
			return false;
		}
		selector.val(amount);
	},
	
	typeUpdateChange:function(){
		var value = $('#typeUpdate').val().trim();
		var value2 = $('#typeUpdateHide').val().trim();
		if(parseInt(value) === -1) {
			$('.easyui-dialog #errMsg').html(stock_update_chon_loai_don_dieu_chinh).show();
			return;
		}
		if(parseInt(value)!=parseInt(value2)){
			$('#typeUpdateHide').val(value);
			StockUpdate.synchronization();
			StockUpdate.lstProductLotMap = new Map();
		}
		if(parseInt(value)!= 0){
			$('#code').val('DCG');
		}else{
			$('#code').val('DCT');
		}
	},
	
	delSelectedRow:function(index){	
		$.messager.confirm(jsp_common_xacnhan, stock_update_confirm_delete_product, function(r){
			if (r){
				var id = $('#dg').datagrid('getRows')[index].productId;
				$('#dg').datagrid('deleteRow',index);				
				var rows = $('#dg').datagrid ('getRows');
				$('#dg').datagrid('loadData',rows);
				//del to pupup - hunglm16
				var lst = StockUpdate.lstProductLotMap.keyArray;
				for(var i=0; i<lst.length; i++){
				    var productDelete =  lst[i].toString().trim();
				    if(id==productDelete){
				        var delKeyArr = lst[i];
				        var delValArr = StockUpdate.lstProductLotMap.valArray[i];
				        StockUpdate.lstProductLotMap.keyArray = jQuery.grep(StockUpdate.lstProductLotMap.keyArray, function(value) {
				          return value != delKeyArr;
				        });
				        StockUpdate.lstProductLotMap.valArray = jQuery.grep(StockUpdate.lstProductLotMap.valArray, function(value) {
				          return value != delValArr;
				        });
				        if(StockUpdate.lstProductLotMap.keyArray ==null || StockUpdate.lstProductLotMap.keyArray.length<1){
				        	StockUpdate.lstProductLotMap = new Map();
				        }
				        break;
				    }
				}
				if(rows.length == 0){
					$('#dg').datagrid('insertRow', {index: 0,row:{}});
					$('#dg').datagrid('beginEdit', 0);
				}
			}
		});		
	},
	updateStock:function(){
		// check dang xu ly - tranh loi double click 
		if (StockUpdate.isProcessing) {
			return false;
		}
		setTimeout(function() {
			StockUpdate.isProcessing = false;
		}, 400);
		StockUpdate.isProcessing = true;
		
		$('.ErrorMsgStyle').html('').hide();
		var msg = "";
		var isShop = $('#isShop').val().trim();
		if(isShop!=undefined && isShop.length>0 && parseInt(isShop)==0){
			msg = msgCommonShop1;
			$('#errMsg').html(msg).show();
			return false;
		}
		
		$('#dg').datagrid('acceptChanges');		
		var shopId = $('#shopId').val().trim();
		var code = $('#code').val().trim();
		var issuedDate = $('#issuedDate').val().trim();
		var staffId = $('#staffId').val().trim();		
		var shopCode = $('#shopCbx').combobox('getValue').trim();
		var typeUpdate = $('#typeUpdate').val().trim();			
//		msg = Utils.getMessageOfRequireCheck('shopCode', jsp_common_shop_code);
		/**if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('code', 'Mã phiếu');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã phiếu', Utils._CODE);
		}
		**/
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('issuedDate', jsp_common_ngay_n);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('issuedDate', jsp_common_ngay_n);
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}				
		var isError = true;
		var index = 0;
		var lstProduct = {};
		lstProduct.selectedId = new Array();
		lstProduct.selectedQty = new Array();
		lstProduct.selectedLot = new Array();		
		lstProduct.selectedObject = new Array();
		lstProduct.lstWarehouse_Id = new Array();
		
		var rows = $('#dg').datagrid('getRows');		
    	if(rows.length>=1){
    		var row = $('#dg').datagrid('getRows')[0];
    		if(isNullOrEmpty(row.productId)){
    			$('#errMsg').html(stock_update_khong_co_sp_duoc_dieu_chinh).show();
    			return false;
    		}			
		}	
    	msg = '';
    	var dem = 0;
		for(var i = 0; i < rows.length; i++) {
			var row = rows[i];			
			if(isNullOrEmpty(row.productId)) continue;			
	
			if(row.warehouseId == null || row.warehouseId == ''){
				msg = stock_update_chua_chon_kho_sp_can_nhap_xuat;
			}
			if(row.productCode!=undefined && row.productCode!=null){
				if(!isNaN(row.quantityValue) && row.quantityValue>0){
					var stockTotal = StockUpdate.storeStockTotalMap.get(row.productCode + row.warehouseId);
					if(StockUpdate.TYPE==1 && stockTotal!=null && stockTotal.availableQuantity<row.quantityValue){
						msg = formatString(stock_update_quantity_more_than_avai_quantity, Utils.XSSEncode(row.productCode));
					}				
				}else{
					msg = stock_update_quantity_error;
				}				
			}else{
				msg = stock_update_product_code_invalid;
			}	
			if(msg.length==0 && row.checkLot == 1) {
				var productLots = StockUpdate.lstProductLotMap.get(row.productId);
				if(productLots==null || productLots.length==0){
					msg = formatString(stock_update_yeu_cau_nhap_lo, Utils.XSSEncode(row.productCode));
				}else{
					var _quantityRow = 0;
					for(var t=0; t<productLots.length; t++){
						var pl = productLots[t];
						if(isNaN(pl.quantityLot) || pl.quantityLot<0)
							continue;
						if(pl.quantityLot>0){
							lstProduct.selectedId.push(row.productId);
							lstProduct.selectedQty.push(pl.quantityLot);
							lstProduct.selectedLot.push(pl.lot);
						}
						_quantityRow = _quantityRow + pl.quantityLot;
					}
					lstProduct.selectedObject.push(i);
					if(_quantityRow!=row.quantityValue){
						msg = formatString(stock_update_tong_sl_nhap_lo_err, Utils.XSSEncode(row.quantityValue));
					}
				}
			} else {
				lstProduct.selectedId.push(row.productId);
				lstProduct.selectedQty.push(row.quantityValue);								
				lstProduct.selectedLot.push('');
				lstProduct.selectedObject.push(i);
				lstProduct.lstWarehouse_Id.push(row.warehouseId);
			}
			if(msg.length==0){
				dem = 0;
				for(var j = rows.length-1; j >=0; j--) {
					if(rows[i].productId == rows[j].productId && row.warehouseId == rows[j].warehouseId){
						dem++;
					}
				}
				if(dem>1){	
					msg = format(stock_update_ma_sp_cung_kho_hon_mot_dong, Utils.XSSEncode(row.productCode));
				}
			}
			if(msg.length>0){
				$('#dg').datagrid('selectRow',i);
				$('#errMsg').html(msg).show();					
				return false;
			}
 		}		
		var params = new Object();
		//params.shopId = shopId;
		params.shopCode = $('#shopCbx').combobox('getValue').trim();
		params.code = $('#hiddenCode').val().trim();
		params.issuedDate = issuedDate;
		params.staffId = staffId;
		params.typeUpdate = parseInt(typeUpdate);			
		params.lstProduct_Id = lstProduct.selectedId;
		params.lstProduct_Qty = lstProduct.selectedQty;
		params.lstProduct_Lot = lstProduct.selectedLot;
		params.selectedObject = lstProduct.selectedObject;
		params.lstWarehouse_Id = lstProduct.lstWarehouse_Id
		$.ajax({
			type : "POST",
			url : '/stock/update/stock-check-lot',
			data :($.param(params, true)),
			dataType : "json",
			success : function(result) {
				Utils.addOrSaveData(params, '/stock/update/updateStock', StockUpdate._xhrSave, 'errorMsg', function(result){
					if(!result.error){
						StockUpdate.afterModifier();
						$('#code').val($('#hiddenCode').val().trim());
						if(result.stockTransId!=undefined && result.stockTransId!=null){
							$('#stockTransId').val(result.stockTransId);
							$('#code').val(result.transCode);
						}
					}
				}, 'loading', '', false, 'Bạn có muốn điều chỉnh không?',function(data){
					var errorMsg = data.errMsg;
					if(data.lot_id!=null && data.lot_id !=undefined && data.lot_id.length>0){							
						$('#dg').datagrid('selectRow',data.lot_id);
					}
					$('#errorMsg').html(errorMsg).show();
				});
			}			
		});		
	},
	saveData:function(data){
		Utils.saveData(data, '/stock/update/updateStock', StockUpdate._xhrSave, 'errorMsg', function(result){
			if(!result.error){
				StockUpdate.afterModifier();
				if(result.stockTransId!=undefined && result.stockTransId!=null){
					$('#stockTransId').val(result.stockTransId);
				}
			}
		}, 'loading', '', false, stock_update_confirm_adjust,function(data){
			var errorMsg = data.errMsg;
			if(data.lot_id!=null && data.lot_id !=undefined && data.lot_id.length>0){
				$('#' + data.lot_id).focus();
			}
			$('#errMsg').html(errorMsg).show();	
		});
	},
	afterModifier:function(){	
		$('#errMsg').hide();
		enable('btnExportExcel_');
		$('#btnExportExcel_').bind('click',StockUpdate.printStockModifier);
		$('#shopCbx').combobox('disable');
		$('#shopCbx').parent().addClass('BoxDisSelect');
		disabled('code');
		disabled('issuedDate');				
		disabled('btnUpdate_');
		$('#typeUpdate').attr('disabled','disabled');$('#typeUpdate').parent().addClass('BoxDisSelect');					
		$('td[field=delete] a').removeAttr('onclick');
		$('.link_lot').removeAttr('onclick');							
		$('#successMsg').html(msgCommon1).show();
		var tm = setTimeout(function(){$('#successMsg').html('').hide();clearTimeout(tm);}, 3000);
		StockUpdate.isAuthorize = false;
	},
	printStockModifier:function(){		
		var data = new Object();
		data.shopCodeTextField = $('#shopCbx').combobox('getValue').trim();	
		data.code = $('#code').val().trim();
		var type = $('#typeUpdate').val();
		if(type == 0){
			data.transType = "NHAP_KHO_DIEU_CHINH";
		}else if(type == 1){
			data.transType = "XUAT_KHO_DIEU_CHINH";
		}
		try {
			data.reportCode = $('#function_code').val();
		} catch(e) {
		}
		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/stock/update/printStockModifier",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					var filePath = ReportUtils.buildReportFilePath(data.path);
					window.open(filePath);
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
		

	},
	changeLot:function(selector){
		var LOT_OBJECT = $(selector).attr('id');
		var msg = Utils.getMessageValidateLot(LOT_OBJECT);
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			var tm = setTimeout(function() {	
				$(selector).focus();			
				clearTimeout(tm);
			}, 50);
			return false;
		}		
	},
	nextAndPrevTextField: function(e,selector, clazz) {
		if(e.keyCode == keyCodes.ARROW_DOWN || e.keyCode == keyCodes.ENTER) {
			var index = $(selector).attr('index');
			if(index == $('#gridProduct').datagrid('getRows').length - 1) {
				if(clazz == 'AmountProduct'){
					var i = $('#gridProduct').datagrid('getRows').length;
					$('#gridProduct').datagrid('insertRow', {index: i,row:{}});
					$('#gridProduct').datagrid('beginEdit', i);
					setTimeout(function() {
						$('tr[datagrid-row-index=' +i+']').find('td[field=productId] .combo-text.validatebox-text').focus();
					}, 500);
				}
			} else {
				if(clazz == 'LotProduct') {
					$('.LotProduct[index=' +(index+1)+']').focus();
				} else {
					$('.AmountProduct[index=' +(index+1)+']').focus();
				}
			}
		} else if(e.keyCode == keyCodes.ARROW_UP) {
			var index = $(selector).attr('index');
			if(index == 0) {				
			} else {
				if(clazz == 'LotProduct') {
					$('.LotProduct[index=' +(index-11)+']').focus();
				} else {
					$('.AmountProduct[index=' +(index-1)+']').focus();
				}
			}
		} else if(e.keyCode == keyCodes.ARROW_RIGHT) {
			if(clazz == 'LotProduct') {
				var index = $(selector).attr('index');
				$('.AmountProduct[index=' +index+']').focus();
			} 
		} else if(e.keyCode == keyCodes.ARROW_LEFT) {
			if(clazz == 'AmountProduct') {
				var index = $(selector).attr('index');
				$('.LotProduct[index=' +index+']').focus();
			}
		}
	},
	chooseLotDialog:function(index){
		$('.ErrorMsgStyle').html('').hide();
		var productSelection = $('#dg').datagrid('getRows')[index];
		if(productSelection==null || isNaN(productSelection.quantityValue)){
			return;
		}		
		StockIssued.PRODUCT_ID = productSelection.productId;
		StockIssued.PRODUCT_TOTAL = productSelection.quantityValue;
		StockUpdate.lstProductSelectionLotMap = new Map();
		$('#productLotDialog').css("visibility", "visible");
		var html = $('#productLotDialog').html();	
		$('#productLotEasyUIDialog').dialog({closed: false,cache: false,modal: true,width : 600,height :470,onOpen: function(){	   
	        	StockIssued.request = false;
	        	$('#productLotDialog').html('');
				$('.easyui-dialog #productLotGrid').datagrid({
					view: bufferview,	
				    singleSelect:true,				   
				    height: 250,			      
				    fitColumns:true,				    
				    scrollbarSize:20,
					width: 550,					
				    columns:[[				          
				        {field: 'rownum',title: jsp_common_numerical_order, width: 30, sortable:false,resizable:false,align: 'center',
				        	formatter:function(value,row,index){
				        		if(isNullOrEmpty(row.lot))
				        			return '';
				        		return (index+1);
				        	}
				        },
				        {field: 'lot',title: jsp_common_so_lo, width: 80, sortable:false,resizable:false,align: 'center',
				        	editor:{type : 'text'}
				        },
					    {field: 'quantity',title: jsp_common_ton_kho,  width: 80,sortable:false,resizable:false, align: 'right',
				        	formatter:function(value,row,opts){
				        		return formatQuantity(row.quantity,StockIssued.CONVFACT);
				        	}
				        },
					    {field: 'quantityLot', title: jsp_common_so_luong, width: 80,sortable:false,resizable:false, align: 'right',
				        	formatter:function(value,row,opts){				        		
				        		var amount =  formatQuantity(row.quantityLot,StockIssued.CONVFACT);		
				        		if(StockIssued.request){				        			
				        			amount = '';
				        		}
				        		return '<input index=' +opts+' style="margin: 0px;text-align: right" onkeypress="return NextAndPrevTextField(event,this,\'QUANTITY\')" type="text" class="InputTextStyle InputText1Style QUANTITY" onblur="StockUpdate.onChange(this);" onchange="return StockUpdate.onChange(this);" quantity="' +row.quantity+'" value="' +amount+'"  id="QUANTITY' +opts+'"  />';
				        	}
					    },
				        {field:'delete', title: jsp_common_xoa, width:30, resizable:false, sortable:false, align:'center', 
							formatter: function(value, row, index) {
								if(isNullOrEmpty(row.lot) || row.id!=null || row.id!=undefined) 
									return '';
								return '<a href="javascript:void(0)" onclick="StockUpdate.delSelectedLotRow(' +index+');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a>';				
							}
						}
					   
				    ]],
				    onAfterEdit:function(rowIndex,rowData){
				    	StockUpdate.indexLotEditing = rowIndex;
				    	rowData.quantity = 0;				    					
				    	rowData.id = null;	
				    	rowData.quantityLot = StockUpdate.lstProductSelectionLotMap.get(String(rowIndex)).quantityLot;
				    	$('.easyui-dialog #productLotGrid').datagrid('refreshRow', rowIndex);
				    	if(rowData.quantityLot!=null && rowData.quantityLot!= undefined){
				    		var strtId = '#QUANTITY' +rowIndex.toString();
				    		if(rowData.quantityLot>0){
						    	$(strtId).val(formatQuantity(rowData.quantityLot,StockIssued.CONVFACT)).show();
				    		}else{
				    			$(strtId).val('').show();
				    		}
				    	}
			    	},
			    	onBeforeEdit :function(rowIndex,rowData){
			    		var rows = $('.easyui-dialog #productLotGrid').datagrid('getRows');
				    	for(var i=0;i<rows.length;++i){
				    		if(i==rowIndex)
				    			continue;
				    		$('.easyui-dialog #productLotGrid').datagrid('endEdit', i);    		
				    	}		    			    	
					},
					onClickRow:function(rowIndex, rowData) {
						if(StockUpdate.TYPE==1){
							return false;
						}
						if(!isNullOrEmpty(rowData.id))
							return false;						
						$('.easyui-dialog #productLotGrid').datagrid('beginEdit', rowIndex);	
					}
				});
				var shopCode = $('#shopCbx').combobox('getValue').trim();
				$.getJSON('/stock/update/search-lot?shopCode=' +shopCode+'&productId=' +StockIssued.PRODUCT_ID, function(result){					
					var product = result.product;
					if(product!=null){
						StockIssued.CONVFACT = product.convfact;						
						$('#fancyProductCode').html(Utils.XSSEncode(product.productCode));
						$('#fancyProductName').html(Utils.XSSEncode(product.productName));
						$('#fancyProductTotalQuantity').html(formatQuantityEx(StockIssued.PRODUCT_TOTAL,StockIssued.CONVFACT));						
					}	
					var lots = null;
					var list_product_lots = null;
					if(StockUpdate.lstProductLotMap!=null && StockUpdate.lstProductLotMap!=undefined && StockUpdate.lstProductLotMap.keyArray.length>0){
						list_product_lots = StockUpdate.lstProductLotMap.get(StockIssued.PRODUCT_ID);
					}
					if(list_product_lots!=null){
						lots = list_product_lots;
					}else{
						lots = result.lots;	
						StockIssued.request = true;
					}
					for(var i=0;i<lots.length;++i){
 						//loctt - Oct14, 2013
						if(StockUpdate.TYPE==1 && lots[i].availableQuantity == 0){
							lots.splice(i,0);
							continue;	
						}
						var productLot = StockUpdate.createProductLot(lots[i]);	
						$('.easyui-dialog #productLotGrid').datagrid('appendRow',productLot);
						StockUpdate.lstProductSelectionLotMap.put(String(i),productLot);
					}
					$('.easyui-dialog #productLotGrid').datagrid('acceptChanges');
					if(StockUpdate.TYPE==0){
						$('.easyui-dialog #productLotGrid').datagrid('appendRow',new Object());
						$('.easyui-dialog #productLotGrid').datagrid('acceptChanges');
						var len = $('.easyui-dialog #productLotGrid').datagrid('getRows').length; 
						StockUpdate.lstProductSelectionLotMap.put(String(len-1),new Object());
					}
					StockUpdate.showTotalAndScrollBodyProductLotDialog();
					Utils.bindFormatOnTextfieldInputCss('QUANTITY',Utils._TF_NUMBER_CONVFACT);
				});
	        },	        
	        onClose : function(){
	        	$('#productLotDialog').html(html);
	        	$('#productLotDialog').css("visibility", "hidden");
	        	$('#productLotGridContainer').html('<table id="productLotGrid" class="easyui-datagrid" ></table>');
	        }
	     });			
		$('#errMsg').hide();
		return false;
	},	
	createProductLot:function(productLot,convfact){
		var newrow = {
				lot:productLot.lot,
				quantity:productLot.quantity,
				quantityLot:productLot.quantityLot,
				id:productLot.id							
		};
		return newrow;		
	},
	onChange:function(selector){
		$('#fancyboxError').html('').show();
		var isError = false;
		$('.easyui-dialog .QUANTITY').each(function(){
			var selector = $(this);
			var quantityLot = getQuantity($(selector).val().trim(), StockIssued.CONVFACT);
			var quantity = $(selector).attr('quantity');			
			if(isNaN(quantityLot)){	
				isError = true;
				$('#fancyboxError').html(stock_update_lot_quantity_invalid).show();
				setTimeout(function() {$(selector).focus();}, 50);	
				return false;
			}
			if(StockUpdate.TYPE==1 && quantity<quantityLot){	
				isError = true;
				$('#fancyboxError').html(stock_update_lot_quantity_more_than_stock).show();
				setTimeout(function() {$(selector).focus();}, 50);	
				return false;
			}
			$(selector).val(formatQuantity($(selector).val().trim(), StockIssued.CONVFACT));
			if($(selector).val().length==0){			
				$(selector).val('');
			}
		});
		if(isError){
			return false;
		}	
		var index = $(selector).attr('index');
		var productLot = StockUpdate.lstProductSelectionLotMap.get(String(index));
		if(productLot!=null){			
			productLot.quantityLot = getQuantity($(selector).val().trim(),StockIssued.CONVFACT);
			StockUpdate.lstProductSelectionLotMap.put(String(index),productLot);
		}
		StockUpdate.showTotalAndScrollBodyProductLotDialog();
		return true;
	},
	
	erroMsgProductLotGrid:function(stt, msg){
		$('#errMsg').html('').show();
		$('.easyui-dialog #productLotGrid').datagrid('selectRow',stt);
    	$('#fancyboxError').html(msg).show();
    	setTimeout(function(){						
    		$('#fancyboxError').html('').show();
		},4000);
		
	},
	
	acceptSeparation:function(){
		var productId = StockIssued.PRODUCT_ID;		
		var list_product_lots = new Array();
		var sum_quantity = 0;
		$('#errMsg').html('').show();
		var dg = '.easyui-dialog #productLotGrid';
		$(dg).datagrid('acceptChanges');
		var rows = $('.easyui-dialog #productLotGrid').datagrid('getRows');
		var dem = 0;
		var msg = '';
		//Chect LOT Format
		for(var f=rows.length-1; f>=0; f--){
			if(rows[f].lot == undefined || rows[f].lot.length == 0){
				break;
			}
			msg = Utils.getMessageValidateLotNew(rows[f].lot);
			if(msg !='' && msg.length >0 ){
				StockUpdate.erroMsgProductLotGrid(f, msg);
				dem = 1;
				break;
			}
	    }
		var delKeyArr = null;
		StockUpdate.lstProductLotMap.keyArray = jQuery.grep(StockUpdate.lstProductLotMap.keyArray, function(value) {
	    	return value != delKeyArr;
	    });
	    StockUpdate.lstProductLotMap.valArray = jQuery.grep(StockUpdate.lstProductLotMap.valArray, function(value) {
	    	return value != delKeyArr;
	    });
	        
		if(dem!=0){
			return false;
		}
		dem = 0;
		//Chect LOT duplicate
		for(var j=rows.length-1;j>=0;j--){
		    for(var t=0; t<rows.length; t++){
		        if(rows[j].lot == rows[t].lot){
		              dem = dem + 1;
		        }
		    }
		    if(dem>1){
		    	msg = format(stock_update_exists_lot, rows[j].lot);
		    	StockUpdate.erroMsgProductLotGrid(j, msg);
		    	break;
		    }
		    dem=0;
		}
		if(dem!=0){
			return false;
		}
		for(var i=0;i<rows.length;++i){
			var productLot = rows[i];			
			if(isNullOrEmpty(productLot.lot))
				continue;	
			var quantity = getQuantity(productLot.quantityLot,StockIssued.CONVFACT);
			if(isNaN(quantity)){
				$('.easyui-dialog .QUANTITY').focus();
				$('#fancyboxError').html(format(stock_update_lot_quantity_invalid_ex, Utils.XSSEncode(productLot.lot))).show();
				return false;
			}
			if(StockUpdate.TYPE==1 && quantity>productLot.quantity){
				$('.easyui-dialog .QUANTITY').focus();
				$('#fancyboxError').html(format(stock_update_lot_quantity_more_than_stock_ex, Utils.XSSEncode(productLot.lot))).show();
				return false;
			}				
			productLot.quantityLot = quantity;
			list_product_lots.push(productLot);	
			sum_quantity+=Number(quantity);	
		}		
		var totalQuantity = getQuantity(StockIssued.PRODUCT_TOTAL, StockIssued.CONVFACT);
		if(totalQuantity!=sum_quantity){
			$('#fancyboxError').html(format(stock_update_tong_sl_nhap_lo_err, formatQuantity(StockIssued.PRODUCT_TOTAL, StockIssued.CONVFACT))).show();
			return false;
		}
		StockUpdate.lstProductLotMap.put(productId,list_product_lots);
		StockUpdate._quantityTemp = 0;
		$('.easyui-dialog').dialog('close');
	},
	showTotalAndScrollBodyProductLotDialog:function(){
		var totalQty = 0;
		var totalQtyLot = 0;	
		$('.easyui-dialog .QUANTITY').each(function(){
			var totalHTML = $(this).val().trim();
			try{
				if(Number(getQuantity(totalHTML, StockIssued.CONVFACT))>=0){
					totalQtyLot += Number(getQuantity(totalHTML, StockIssued.CONVFACT));
				}
				if(Number($(this).attr('quantity'))>=0){
					totalQty += Number($(this).attr('quantity'));
				}
			}catch(e){}
			$(this).css('text-align','right');			
			$(this).css('margin','0px');	
			$(this).css('width','123px');			
			
		});		
		$('#fancyboxError').html('').hide();		
		$('#totalQuantity').html(formatQuantity(totalQty,StockIssued.CONVFACT));
		$('#totalAvailableQuantity').html(formatQuantity(totalQtyLot,StockIssued.CONVFACT));		
	},
	delSelectedLotRow:function(index){
		$.messager.confirm(jsp_common_xacnhan, stock_update_confirm_delete_lot, function(r){
			if (r){
				$('.easyui-dialog #productLotGrid').datagrid('deleteRow',index);				
				var rows = $('.easyui-dialog #productLotGrid').datagrid ('getRows');
				$('.easyui-dialog #productLotGrid').datagrid('loadData',rows);
			}
		});		
	},
	selectShop: function(id, code) {
		var params = new Object();
		params.shopCode = code;
		loadDateLock(params, function() {
			$('#shopId').val(id);
			StockUpdate.synchronization();
		});
	},
	downloadTemplate:function() {
		var url = "/stock/update/download-template";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	}
};

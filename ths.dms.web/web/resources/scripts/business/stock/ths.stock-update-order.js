var StockUpdateOrder = {
	_isAllowShowPrice: true,
	_isLoad: true,
	xhrSave: null,
	/**
	 * @author vuongmq
	 * @param objectId
	 * @param index
	 * @param shortCode
	 * @param customerName
	 * @description Gan Ma khach hang vao objectId(customerCode)
	 */
	callSelectF9CustomerCode: function (index, shortCode, customerName) {
		$('#customerCode').val(shortCode.trim());
		$('#common-dialog-search-2-textbox').dialog("close");
	},
	searchOrder: function(){
		$('.ErrorMsgStyle').html('').hide();
		var shopCode = $('#shopCodeCB').combobox("getValue").trim();	
		var orderType = $('#orderType').val().trim();
		var orderNumber = $('#orderNumber').val().trim();
		var orderSource = $('#orderSource').val().trim();
		var shortCode = $('#customerCode').val().trim();
		var customerName = $('#customerName').val().trim();
		var saleStaffCode = $('#saleStaffCode').combobox('getValue').trim();
		var deliveryStaffCode = $('#deliveryStaffCode').combobox('getValue').trim();
		var params = new Object();
		params.shopCode = shopCode;
		params.orderType = orderType;
		params.orderNumber = orderNumber;
		params.orderSource =  orderSource;
		/*params.fromDate =  fDate;
		params.toDate =  tDate;*/
		params.code =  shortCode;
		params.name =  customerName;
		params.saleStaffCode =  saleStaffCode;
		params.deliveryStaffCode =  deliveryStaffCode;
		
		//Thêm dk gia trị
		params.isValueOrder = $('#isValueOrder').val().trim();
		var numberOrder = $('#numberValueOrder').val().trim();
		var numberValueOrder = "";
		if ( $('#numberValueOrder').val() != undefined &&  $('#numberValueOrder').val() != null &&  $('#numberValueOrder').val() !='') {
			numberValueOrder = numberOrder.replace(/,/g, '');
		}
		params.numberValueOrder = numberValueOrder;
		//-----------------------
		
		if(StockUpdateOrder._isLoad) {
			StockUpdateOrder.initGrid(params);
		} else {
			$('#grid').datagrid('uncheckAll');	
			$('#grid').datagrid('load',params);				
		}
		return false;
	},
	approveStockOrder: function(){
		var rows = $('#grid').datagrid('getChecked');
		var msg ='';
		if(rows.length <= 0) {
			msg = 'Không có đơn hàng nào được chọn.';
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		
		var listTemp = [];
		for(var i = 0; i< rows.length; i++) {
			if(rows[i].id != undefined) {
				var oSource = rows[0].orderSource;
				if(rows[0].orderSource == null) {
					oSource = 1; // trường hop cac don GO/DP/DCT/DGC/DC là trên web
				}
				listTemp.push({ id:rows[i].id, shopId: rows[i].shopId, staffId: rows[i].staffId, orderNumber:rows[i].orderNumber, orderType:rows[i].orderType, orderSource: oSource });
			}
		}
		params.listTemp = listTemp;
		var msgDialog = 'Bạn có muốn xác nhận các đơn hàng này không?';
		/*Utils.addOrSaveData(params, '/stock/update-order-stock/approveOrder', null,  'errMsg', function(data) {
			$('#successMsg').html("Lưu dữ liệu thành công").show();
			setTimeout(function(){
				$('#btnSearch').click();
				$('.SuccessMsgStyle').html("").hide();
			}, 1500);
		}, null, null, null,msgDialog);*/
		JSONUtil.saveData2(params, "/stock/update-order-stock/approveStockOrder", msgDialog, "errMsg", function(data) {
			if(!data.error) {
				$('#btnSearch').click();
				$('#successMsg').html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html("").hide();
				}, 1500);
				if(!Utils.isEmpty(data.errCode)){
					StockUpdateOrder.showDialogStockOrderError(data);
				}
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined && data.errMsg == 'DON_HANG_DA_XU_LY'){
				$('#errMsg').html('Đã có đơn hàng đã xử lý, yêu cầu kiểm tra lại').show();
				return;
			}
			StockUpdateOrder.showDialogStockOrderError(data);
			$('#btnSearch').click();
			/*if(data.errMsg != undefined) {
				$('#errMsg').html(data.errMsg).show();
			}*/
		});
		return false;
	},
	showDialogStockOrderError: function (data) {
		var html = $('#dialogStockOrder').html();
		var title = 'CÁC ĐƠN HÀNG BỊ LỖI, KHÔNG THỂ XÁC NHẬN'; 
		var orderNumber = 'Số đơn hàng';
		var stockQuantity = 'Không đủ kho sản phẩm';

		if (data.rows != null && data.rows.length > 0) {
			$('#dialogStockOrderEasyUIDialog').dialog({
				title:'CÁC ĐƠN HÀNG BỊ LỖI, KHÔNG THỂ XÁC NHẬN',
				closed: false,
				cache: false,
				position:'right',
				modal: true,
				//width : $(window).width()-350,
				width : $(window).width()-800,
				height :350,
				onOpen: function(){							
					//var html = new Array();
					$('#dgError').datagrid({
				        singleSelect: true,
				        data: data.rows,
				        autoRowHeight: true,       
				        width: $(window).width() - 845,
				        height: 230,
				       /* frozenColumns:[[
				           {field: 'orderNumber',title:'Số đơn hàng', width:200,align:'left',sortable : false,resizable : false}
				        ]],*/
				        columns:[[
				           {field: 'orderNumber',title:'Số đơn hàng', width:150,align:'left',sortable : false,resizable : false},
				           {field: 'stockQuantity',title:'Không đủ kho sản phẩm', width:350,align:'left',sortable : false,resizable : false},
				           /*{field: 'errOrder',title:'Lỗi xác nhận đơn hàng', width:150,align:'center',sortable : false,resizable : false},*/
				        ]],
				        onLoadSuccess :function(){
				        	$('#dialogStockOrderEasyUIDialog .datagrid-header').css('height','20px');
				        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row').css('height','20px');
				        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row div.datagrid-cell').css('word-wrap','break-word').css('height','20px');
				        }
					});
				},	        
		        onClose : function(){
		        	$('#btnConfirm').unbind('click');
		        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
		        	$('#dialogStockOrder').html(html);
		        	if (!isNullOrEmpty(data.errCode)) {
		    			StockUpdateOrder.showPopupUpdateStockTotal(data.errCode);
		    		}
		        }
		     });
		} else if (!isNullOrEmpty(data.errCode)) {
			StockUpdateOrder.showPopupUpdateStockTotal(data.errCode);
		}
	},

	/**
	 * show popup xac nhan cap nhat ton kho de link qua PO auto
	 * @author trietptm
	 * @since Oct 23, 2015
	 */
	showPopupUpdateStockTotal: function(errCode) {
		var title = 'CẬP NHẬT TỒN KHO';
		var msg = "Cập nhật tồn kho làm cho tồn kho các sản phẩm <span style=\"color:red;\">" + Utils.XSSEncode(errCode) + "</span> bé hơn tồn kho cần thiết.";
		msg += "Bạn có muốn đặt hàng cho các sản phẩm này không ?";
		$('#confirmMsg').html(msg).show();
		$('#dialogStockOrderEasyUIDialog #btnConfirm').bind('click', function() {
			window.location.href = '/po/init-po-auto/info';
		}).show();

		$('#dialogStockOrderEasyUIDialog').dialog({
			title: title,
			closed: false,
			cache: false,
			position: 'right',
			modal: true,
			//width : $(window).width()-350,
			width: $(window).width()-800,
			height:250,
			onOpen: function() {							
				
			},	        
	        onClose: function() {
	        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
	        	$('#dialogStockOrder').html(html);
	        }
	     });
	},

	/**
	 * Huy don DC, DCT, DCG
	 * @author lacnv1
	 */
	cancelStockTrans: function() {
		$(".ErrorMsgStyle").hide();

		var rows = $('#grid').datagrid('getChecked');
		if (rows.length <= 0) {
			$('#errMsg').html("Không có đơn hàng nào được chọn.").show();
			return false;
		}

		var lstId = [];
		var r = null;
		for (var i = 0, sz = rows.length; i < sz; i++) {
			r = rows[i];
			if (r.orderType != "DC" && r.orderType != "DCT" && r.orderType != "DCG") {
				$("#errMsg").html("Chỉ được hủy đơn DC, DCT, DCG").show();
				return false;
			}
			if (r.id != undefined && r.id != null && Number(r.id)) {
				lstId.push(r.id);
			}
		}

		$.messager.confirm("Xác nhận", "Bạn có muốn hủy (những) đơn " + $("#orderType").val().trim() + " này không?", function(y) {
			if (y) {
				var dataModel = { lstId : lstId };
				Utils.saveData(dataModel, "/stock/update-order-stock/cancel-stock-trans", StockUpdateOrder._xhrSave, "errMsg", function(data) {
					$('#btnSearch').click();
					if (data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0) {
						StockUpdateOrder.showDialogCancelStockTransErr(data.lstErr);
						setTimeout(function () {
							$("#successMsg").hide();
						}, 200);
					}
				});
			}
		});
	},
	showDialogCancelStockTransErr: function (lstErr){
		var html = $('#dialogStockOrder').html();
		$('#dialogStockOrderEasyUIDialog').dialog({
			title:'Các đơn hàng bị lỗi, không thể hủy',
			closed: false,
			cache: false,
			modal: true,
			width : 500,
			height :350,
			onOpen: function(){							
				//var html = new Array();
				$('#dgError').datagrid({
			        singleSelect:true,
			        data: { total: lstErr.length, rows: lstErr },
			        autoRowHeight : true,       
			        width: 450,
			        height:230,
			        fitColumns: true,
			        scrollbarSize: 0,
			        columns:[[
			           {field: 'orderNumber',title:'Số đơn hàng', width:220,align:'left',sortable : false,resizable : false},
			           {field: 'approve',title:'Trạng thái không hợp lệ', width:350,align:'left',sortable : false,resizable : false},
			           {field: 'quantityErr',title:'Tồn kho không hợp lệ', width:350,align:'center',sortable : false,resizable : false}
			        ]],
			        onLoadSuccess :function(){
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row div.datagrid-cell').css('word-wrap','break-word').css('height','20px');
			        }
				});
			},	        
	        onClose : function(){
	        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
	        	$('#dialogStockOrder').html(html);
	        }
	     });
	},
	
	initUnitCbx: function(){				
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				listNVBH = data.lstNVBH;
				$('#shopCodeCB').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			            StockUpdateOrder.getStaffByShopCode();
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shopCodeCB').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
			        	}
			        	
			        }
				});
			}
		});
	},
	
	/**
	*lay danh sach nhan vien theo shop don vi
	*/
	getStaffByShopCode: function() {
		 var shopCode = $('#shopCodeCB').combobox('getValue');
		 var params = new Object();
		if(shopCode != null){
			params.shopCode = shopCode;
		}
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/stock/update-order-stock/load-staff-by-shop-code',
			data : (kData),
			dataType : "json",
			success : function(data) {
				StockUpdateOrder._isAllowShowPrice = data.isAllowShowPrice;
				SPCreateOrder._isAllowShowPrice = data.isAllowShowPrice;
				if(StockUpdateOrder._isAllowShowPrice){
					$('#valueOrderDiv').show();
					$('#isValueOrder').val(-1);
					$('#isValueOrder').change();
				} else {
					$('#valueOrderDiv').hide();
				}
				//bo sung text box gia tri DH
				var numberValueOrder = data.numberValue;
				if(numberValueOrder != '' && numberValueOrder != "") {
					SPAdjustmentTax.numberValueOrder = numberValueOrder;
					SPAdjustmentTax._textChangeBySeach.avg = numberValueOrder.replace(/,/g, '');
				}
				
				$('#shopId').val(data.shopId);

				var lstNVBHVo = data.lstNVBHVo;
				var lstNVGHVo = data.lstNVGHVo;

				if(lstNVBHVo != undefined && lstNVBHVo != null) {
					Utils.bindStaffCbx('#saleStaffCode', lstNVBHVo, null, 206, null);
				}
				
				if(lstNVGHVo != undefined && lstNVGHVo != null) {
					Utils.bindStaffCbx('#deliveryStaffCode', lstNVGHVo, null, 206, null);
				}

				if(StockUpdateOrder._isLoad){
					StockUpdateOrder.searchOrder();
					StockUpdateOrder._isLoad = false;
	        	}		

			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Error");
			}
		});
	},
	
	initGrid: function(params) {
		$('#grid').datagrid({  
		    url:'/stock/update-order-stock/search',
		    rownumbers:true,
		    pagination: true,
		    //selectOnCheck:false,
		    singleSelect:false,
		    //scrollbarSize:0,
		    autoRowHeight : true,
		    //height: 300,
		    pageNumber : 1,
		    pageSize: 100,
		    pageList  : [10,50,100,200,500],
		    width: $(window).width() - 40,	    		    
		    /* queryParams: {data : $('#date').val(), priorityCode:$('#priority').val().trim(), orderSource:$('#orderSource').val().trim()}, */
		    queryParams: params,
		    columns: [[
				{field:'id', checkbox:true},
				{field:'link', title:'', width:50, fixed:true, align:'center', sortable:false, resizable:false, formatter : function(v,r,i) {
					var orderT = '';
					var orderType = '';
					if(r.orderType != undefined && r.orderType != null){
						orderType = '&orderType=' + r.orderType;
						orderT = r.orderType;
					}
					var str = '<a href="javascript:void(0);" onclick="SPPrintOrder.showDetailInPopup(' +r.id + ',\'' + orderT + '\',\'' + r.shopCode + '\');" title="Xem chi tiết" id="aEdit"><img height="17" src="/resources/images/icon-view.png"></a>';
					return str;
				}},
				{field:'orderNumber', title:'Số đơn hàng', width:110, align:'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(row.orderNumber);
				}},
				{field:'orderDate', title:'Ngày tạo', width:135, align:'center', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(row.strOrderDate);
				}},
				{field:'customer', title:'Khách hàng', width:190, align: 'left', formatter : function(value,row,index) {
					if(row.shortCode != null || row.customerName != null) {
						return VTUtilJS.XSSEncode(row.shortCode + ' - ' + row.customerName);
					} else {
						return '';
					}
				}},
				{field:'customerAddress', title:'Địa chỉ', width:200, align: 'left', formatter : function(value,row,index) {
		        	if(row.customerAddress != null) {
		        		return VTUtilJS.XSSEncode (row.customerAddress);
		        	} else {
		        		return '';
		        	}
		        }},	
				{field:'total', title:'Tổng tiền', width:100, align:'right', formatter : function(value,row,index) {
		        	if(row.total != null) {
		        		return formatCurrencyInterger(row.total);
		        	} else {
		        		return 0;
		        	}
		        }},
		        {field:'quantity', title:'Sản lượng', width:100, align:'right', formatter : function(value,row,index) {
		        	if(row.quantity != null) {
		        		return formatCurrencyInterger(row.quantity);
		        	} else {
		        		return 0;
		        	}
		        }},
		        {field:'refOrderNumber', title:'Số đơn đặt hàng', width:110, align: 'left', formatter : function(value,row,index) {
		        	if(row.refOrderNumber != null) {
		        		return VTUtilJS.XSSEncode(row.refOrderNumber);
		        	} else {
		        		return '';
		        	}
		        }},	
		        {field:'orderType', title:'Loại đơn', width:65, align: 'left', formatter : function(value,row,index) {
		        	if(row.orderType != null) {
		        		return VTUtilJS.XSSEncode(row.orderType);
		        	} else {
		        		return '';
		        	}
		        }},	
		        {field:'staff', title:'NVBH', width:195, align:'left',formatter:function(v,r,i){
		        	if(VTUtilJS.isNullOrEmpty(r.staffCode) && VTUtilJS.isNullOrEmpty(r.staffName)) {
						return '';
					} else if(VTUtilJS.isNullOrEmpty(r.staffCode)) {
						return VTUtilJS.XSSEncode(r.staffName);
					} else if(VTUtilJS.isNullOrEmpty(r.staffName)) {
						return VTUtilJS.XSSEncode(r.staffCode);
					}
					return VTUtilJS.XSSEncode(r.staffCode+'-' +r.staffName);
		        }},
		        {field:'delivery', title:'NVGH', width:195, align:'left',formatter:function(value,row,index){
		        	if(row.deliveryCode!=null || row.deliveryName){
		        		return VTUtilJS.XSSEncode(row.deliveryCode+' - ' +row.deliveryName);
		        	}
		        	return '';
		        }}
		       	        
		    ]],
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
			   	 updateRownumWidthForJqGrid('.easyui-dialog');
			   	if (StockUpdateOrder._isAllowShowPrice) {
		    		$('#grid').datagrid('showColumn', 'total');
		    	} else {
		    		$('#grid').datagrid('hideColumn', 'total');
		    	}			   	 
			   	var orderType = $("#orderType").val().trim();
			   	 if (orderType.length == 0 || orderType == "DC" || orderType == "DCG" || orderType == "DCT") {
			   		 $("#btnCancel:hidden").show();
			   	 } else {
			   		$("#btnCancel:not(:hidden)").hide();
			   	 }
			   	 
		   	 	$(window).resize();
		   	 	$(".datagrid-header-check input[type=checkbox]").removeAttr("checked");
		    }		    
		}); 
	}
}
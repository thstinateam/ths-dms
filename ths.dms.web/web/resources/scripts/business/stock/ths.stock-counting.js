var StockCounting = {
	_xhrSave : null,
	_xhrDel:null,	
	_isSearch: true,
	search: function(){
		$('#errMsg').html('').hide();
		var msg = '';
		var shopCode =  $('#shop').combobox('getValue');
		var fDate = $('#startDate').val().trim();
		var tDate = $('#endDate').val().trim();
		var fromCreated = $('#fromCreated').val().trim();
		var toCreated = $('#toCreated').val().trim();
		if(shopCode.length == 0){
			msg = "Bạn chưa chọn Đơn vị";
		}
		if (msg.length == 0 && fDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày bắt đầu kiểm kê');
		}
		if (msg.length == 0 && tDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày kiểm kê');
		}
		if(msg.length == 0 && fDate.length >0 && tDate.length >0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Ngày bắt đầu kiểm kê không được lớn hơn Đến ngày kiểm kê.';
			$('#fromDate').focus();
		}
		if (msg.length == 0 && fromCreated.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromCreated', 'Ngày tạo');
		}
		if(msg.length == 0 && toCreated.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('toCreated', 'Đến ngày');
		}
		if(msg.length == 0 && fromCreated.length >0 && toCreated.length >0 && !Utils.compareDate(fromCreated, toCreated)){
				msg = 'Ngày tạo không được lớn hơn đến ngày.';
				$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var params = new Object();
		params.shopCode = shopCode;
		params.cycleCountCode = $('#cycleCountCode').val().trim();
		params.cycleCountStatus =  $('#cycleCountStatus').val().trim();	
		params.description = $('#description').val().trim();
		params.wareHouseId = $('#cycleCountWareHouse').combobox('getValue');
		
		params.fromCreated = $('#fromCreated').val().trim();
		params.toCreated = $('#toCreated').val().trim();
		params.startDate = $('#startDate').val().trim();
		params.endDate = $('#endDate').val().trim();	
		if (params.startDate.length > 0 && params.endDate.length > 0 && !Utils.compareDate(params.startDate, params.endDate)) {
			$('#errMsg').html('Từ ngày bắt đầu kiểm kê không được lớn hơn đến ngày kiểm kê.').show();
			$('#startDate').focus();
			return false;
		}
		if (params.fromCreated.length > 0 && params.toCreated.length > 0 && !Utils.compareDate(params.fromCreated, params.toCreated)) {
			$('#errMsg').html('Từ ngày tạo không được lớn hơn đến ngày tạo.').show();
			$('#fromCreated').focus();
			return false;
		}
		$("#grid").datagrid('load',params);
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopCode =  $('#shop').combobox('getValue');
		if(shopCode.length == 0){
			msg = "Bạn chưa chọn Đơn vị";
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('description','Mô tả');
		}
		var description = $('#description').val().trim();
		
		if(msg.length == 0 && description.length>0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả', Utils._NAME);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày bắt đầu kiểm kê');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày bắt đầu kiểm kê');
		}
		var wareHouseId = $('#cycleCountWareHouse').combobox('getValue')
		if(msg.length == 0 && wareHouseId.length == 0){
			msg = "Bạn chưa chọn kho";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopCode = shopCode;
		dataModel.cycleCountId = $('#cycleCountId').val().trim();
		dataModel.cycleCountCode = $('#cycleCountCode').val().trim();		
		dataModel.cycleCountStatus = $('#cycleCountStatus').val().trim();
		dataModel.description = $('#description').val().trim();
		dataModel.startDate = $('#startDate').val().trim();
		dataModel.wareHouseId = wareHouseId;	
		
		Utils.addOrSaveData(dataModel, "/stock/counting/save", StockCounting._xhrSave, 'errMsg', function(result){
			if(!result.error){
				var isChanged = result.isChanged==1? true:false;
				StockCounting.resetForm(isChanged);
				$("#successMsg").html('Cập nhật dữ liệu thành công').show();				
				setTimeout(function(){
					$('#successMsg').html('').hide();
					clearTimeout(this);		
					window.location.href = '/stock/counting/info';
				}, 3000);	
				$('#cycleCountCode').attr('disabled','disabled');
				$('#cycleCountId').val(result.cycleCount.id);
				$('#cycleCountStatus').attr('disabled','disabled');
				$('#cycleCountStatus').parent().addClass('BoxDisSelect');
				$('#btnUpdate').unbind('click');
				$('#btnUpdate').bind('click',StockCounting.update);
			}
		}, 'loading');		
		return false;
	},
	update: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('description','Mô tả');
		}
		var description = $('#description').val().trim();
		if(msg.length == 0 && description.length>0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả', Utils._NAME);
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày bắt đầu kiểm kê');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Ngày bắt đầu kiểm kê');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.cycleCountId = $('#cycleCountId').val().trim();
		dataModel.cycleCountCode = $('#cycleCountCode').val().trim();		
		dataModel.cycleCountStatus = $('#cycleCountStatus').val().trim();
		dataModel.description = $('#description').val().trim();
		dataModel.startDate = $('#startDate').val().trim();	
		dataModel.wareHouseId = $('#cycleCountWareHouse').combobox('getValue');	
		Utils.addOrSaveData(dataModel, "/stock/counting/update",null,'errMsg', function(result){
			if(!result.error){
				var isChanged = result.isChanged==1? true:false;
				StockCounting.resetForm(isChanged);
				$("#successMsg").html('Cập nhật dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsg').html('').hide();
					clearTimeout(tm);					
				}, 3000);		
				$('#cycleCountStatus').attr('disabled','disabled');
				$('#cycleCountStatus').parent().addClass('BoxDisSelect');
			}
		});
		return false;
	},	
	resetForm: function(isChanged){
		if(!isChanged){
			$('#cycleChangedDiv input').attr('disabled','disabled');
			$('#cycleCountStatus').attr('disabled','disabled');
			$('#cycleCountStatus').parent().addClass('BoxDisSelect');
			$('#startDate').next().unbind('click');
			$('#cycleChangedDiv button').hide();
			$('#shop').combobox('disable');		
        	$('#shop').parent().addClass('BoxDisSelect');
        	$('#cycleCountWareHouse').combobox('disable');		
        	$('#cycleCountWareHouse').parent().addClass('BoxDisSelect');
		}
		else{
			
		}
	},
	deleteRow: function(cycleCountId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.cycleCountId = cycleCountId;		
		Utils.deleteSelectRowOnGrid(dataModel, 'phiếu kiểm kê', "/stock/counting/remove", StockCounting._xhrDel, '', '', function(result){
			if(!result.error){
				$('#successMsg').html('Xóa phiếu kiểm kê thành công').show();
				//StockCounting.resetForm();
			}
		}, 'loading2');
		return false;
	},
	changeShop: function() {
		var params = new Object();
		params.shopCode = $('#shop').combobox('getValue');
		params.isSearch = StockCounting._isSearch;
		
		 $.ajax({
			type : "POST",
			url : '/stock/counting/changeshop',
			data : params,
			dataType : "json",
			success : function(data) {
				if (data.lockDay != undefined && data.lockDay != null){
					if (StockCounting._isSearch != undefined && StockCounting._isSearch != null & StockCounting._isSearch) {
						// man hinh search
						$('.date').each(function(){
							var id = $(this).attr('id');
							setDateTimePicker(id);
							$(this).val(data.lockDay);
							$(this).css('width','173px');
						});
					} else {
						// man hinh tao moi
						if(cycleCountId != undefined && cycleCountId > 0){
							// chinh sua
							$('#startDate').val(data.lockDay);
						} else {
							// tao moi
							$('.date').each(function(){
								var id = $(this).attr('id');
								setDateTimePicker(id);
								$(this).val(data.lockDay);
								$(this).css('width','173px');
							});
						}
					}
				}
				
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null){
					$('#cycleCountWareHouse').combobox({
						valueField : 'warehouseId',
						textField : 'warehouseName',
						data : data.lstWarehouseVO,
						width:250,
						panelWidth:206,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
				        onLoadSuccess: function(){
				        	var arr = $('#cycleCountWareHouse').combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		var wareHouseId = $('#wareHouseId').val();
				        		if(wareHouseId != null && wareHouseId > 0){
				        			$('#cycleCountWareHouse').combobox('select', wareHouseId);
				        		} else {
				        			$('#cycleCountWareHouse').combobox('select', arr[0].warehouseId);
				        		}
				        	}
				        	var cycleCountId = $('#cycleCountId').val();
				        	if(cycleCountId != undefined && cycleCountId > 0){
				        		$('#shop').combobox('disable');		
				            	$('#shop').parent().addClass('BoxDisSelect');
				            	$('#cycleCountWareHouse').combobox('disable');		
				            	$('#cycleCountWareHouse').parent().addClass('BoxDisSelect');
				        	}
				        }
					});
				 }
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
//				console.log("Error");
			}
		});
	},
	initGrid: function() {
		$('#grid').datagrid({
			url : "/stock/counting/search",
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $(window).width()-50,
			autoWidth: true,
			queryParams:{
				shopCode: $('#shop').combobox('getValue'),
				cycleCountStatus:0			
			},
		    columns:[[	    
				{field:'createDate', title: 'Ngày tạo',sortable : false, resizable : false, width : 100, align : 'center',
					formatter:function(value,row,index){
						return(row.createDate == null)? '':$.datepicker.formatDate('dd/mm/yy', new Date(row.createDate));	
					}},
				{field:'startDate', title: 'Ngày bắt đầu kiểm kê',sortable : false, resizable : false, width : 100, align : 'center',
					formatter:function(value,row,index){
						return(row.startDate == null)? '':$.datepicker.formatDate('dd/mm/yy', new Date(row.startDate));	
				 }},
				{field:'wareHouseCode', title: 'Mã kho', width: 100, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field:'cycleCountCode', title: 'Mã kiểm kê', width: 100, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field:'description', title: 'Mô tả', width: 200, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field:'status', title: 'Trạng thái',width: 100, sortable:false,resizable:false, align: 'left',formatter:CountingFormatter.statusFormat},
				{field:'edit', title: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false,formatter:CountingFormatter.editFormat},
				{field:'delete', title: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false,formatter:CountingFormatter.deleteFormat},
				{field:'canUpdate',  hidden: true}		    
		    ]],	    
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');	    	
		    	 updateRownumWidthForJqGrid('.easyui-dialog');
	   		 $(window).resize();    		 
		    }
		});
	},
	//load combobox don vi
	loadComboboxShop: function() {
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#shop').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null){
			        		StockCounting.changeShop();
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shop').combobox('getData');
			        	if (arr != null && arr.length > 0){			        		
			        		if (StockCounting._isSearch != undefined && StockCounting._isSearch != null & StockCounting._isSearch) {
			        			$('#shop').combobox('select', arr[0].shopCode);
			        			StockCounting.initGrid();
			        		} else if (StockCounting._isSearch != undefined && StockCounting._isSearch != null & !StockCounting._isSearch) {
			        			var shopCode = $('#shopCode').val();
			        			if (shopCode != null && shopCode.length > 0) {
			        				$('#shop').combobox('select', $('#shopCode').val());			        				
			        			} else {
			        				$('#shop').combobox('select', arr[0].shopCode);
			        			}
			        		}
			        	}
			        }
				});
			}
		});	
	}
};
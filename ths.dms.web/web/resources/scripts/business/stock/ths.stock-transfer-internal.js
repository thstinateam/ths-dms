var TransferInternalWarehouse = {
	_mapStockTrans: null,
	_mapStockTransDetail: null,
	_flagG: null,
	_lockDate:null,
	_productCodeFirt:null,
	_indexTmp: null,
	_lstSelectedId: null,
	_arrDelete:null,
	_arrInsert:null,
	_txtIsJoinOderNumberC: null,
	_mapproduct: null,
	_mapProduct: null,
	_lstproductavailable:[],
	_rowDgTmp: null,
	_rowsDg: null,
	_mapProductDlSearch: null,
	_mapProductDl: null,
	_flagDialog: false,
	/**
	 * Xu ly su kien change Combobox
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	eventChangeComboboxWareHouse: function (wareHouseOutputId) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		TransferInternalWarehouse._rowDgTmp = {};
		Utils.getJSONDataByAjaxNotOverlay ({wareHouseOutputId: wareHouseOutputId, shopCodeOutput: $('#shopOutput').combobox('getValue')}, '/transfer-internal-warehouse/get-List-Product-In-WareHouse', function (data){
			TransferInternalWarehouse._mapProduct = new Map();
			TransferInternalWarehouse._mapStockTransDetail = new Map();
			TransferInternalWarehouse._lstproductavailable = new Array();
			if (data != undefined && data != null && data.rows != undefined && data.rows != null && data.rows.length >0) {
				for (var i = 0; i < data.rows.length; i++) {
					TransferInternalWarehouse._mapProduct.put(data.rows[i].productId, data.rows[i]);
					TransferInternalWarehouse._lstproductavailable.push(data.rows[i]);
				}
			}
			$('#prdStockOutputDg').datagrid({
				data: TransferInternalWarehouse.getDataInPrdStockOutputDg(),
				width : $('#prdStockOpDgContGrid').width() - 15,
				fitColumns : true,
			    rownumbers : true,
			    singleSelect:true,
			    height: 275,
			    columns:[[
					{field:'productCode',title:'Mã sản phẩm', width:120,align:'left',formatter: function(value, row, index){
						return Utils.XSSEncode(value);
					}, 
					editor: {
						type:'combobox',
							options:{ 
							valueField:'productId',
						    textField:'productCode',
						    data: TransferInternalWarehouse._lstproductavailable,
						    width: 200,
						    formatter: function(row) {
						    	return '<span style="font-weight:bold">' + Utils.XSSEncode(row.productCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.productName) + '</span>';
						    },
						    onSelect:function(rec){
						    	TransferInternalWarehouse._rowDgTmp = {
					    			productId: rec.productId,
									productCode: Utils.XSSEncode(rec.productCode),
									productName: Utils.XSSEncode(rec.productName),
									convfact: rec.convfact,
									warehouseId: rec.warehouseId,
									availableQuantity: rec.availableQuantity,
									grossWeight: rec.grossWeight,
									price: rec.price,
									quantity: 0,
									amount: 0
						    	};
						    	$('#dgProductName0').html(Utils.XSSEncode(rec.productName.trim())).change();
						    	$('#dgAvlQuantity0').val(StockValidateInput.formatStockQuantity(TransferInternalWarehouse._rowDgTmp.availableQuantity, TransferInternalWarehouse._rowDgTmp.convfact));
						    	$('#dgQuantity0').val(StockValidateInput.formatStockQuantity(0, TransferInternalWarehouse._rowDgTmp.convfact));
						    	$('#dgPrice0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.price)));
						    	$('#dgAmount0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
						    	
						    	$('#dgAvlQuantity0, #dgQuantity0').bind('keyup',function(event) {
						 			if (event.keyCode == keyCodes.ENTER) {
						 				if (TransferInternalWarehouse._rowDgTmp.quantity > 0) {
							 				TransferInternalWarehouse._mapStockTransDetail.put(TransferInternalWarehouse._rowDgTmp.productId, TransferInternalWarehouse._rowDgTmp);
							 				$('.ErrorMsgStyle').html('').hide();
							 				$('.SuccessMsgStyle').html('').hide();
							 				$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
						 				} else {
						 					if (TransferInternalWarehouse._rowDgTmp.productId <=0) {
						 						$('#errMsg').html('Sản phẩm chọn không hợp lệ').show();
						 					}
						 					if (TransferInternalWarehouse._rowDgTmp.quantity < 1) {
						 						$('#errMsg').html('Số lượng phải lớn hơn 0').show();
						 					}
						 					$('#dgQuantity0').focus();
						 				}
						 			}
						 		});
						    	TransferInternalWarehouse._needNewRow = true;
						    },
						    onChange:function(pId){
						    	var flag = false;
						    	if (pId != undefined && pId != null && pId.length > 0) {
	 					    		for (var i = 0; i < TransferInternalWarehouse._lstproductavailable.length; i++) {
	 					    			var rec = TransferInternalWarehouse._lstproductavailable[i];
	 					    			if (rec.productCode == pId.toUpperCase()) {
	 					    				$('#dgQuantity0').focus();
											TransferInternalWarehouse._rowDgTmp = {
									    			productId: rec.productId,
													productCode: Utils.XSSEncode(rec.productCode),
													productName: Utils.XSSEncode(rec.productName),
													convfact: rec.convfact,
													warehouseId: rec.warehouseId,
													availableQuantity: rec.availableQuantity,
													grossWeight: rec.grossWeight,
													price: (rec.price != undefined && rec.price != null)? rec.price: 0,
													quantity: (rec.quantity != undefined && rec.quantity != null)? rec.quantity: 0,
													amount: (rec.amount != undefined && rec.amount != null)? rec.amount: 0
										    	};
											
											TransferInternalWarehouse._mapStockTransDetail.put(rec.productId, TransferInternalWarehouse._rowDgTmp);
											$('.ErrorMsgStyle').html('').hide();
											$('.SuccessMsgStyle').html('').hide();
											var tm = setTimeout(function(){
												$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
												$('#dgQuantity' +rec.productId).focus();
//												$('#dgQuantity' +rec.productId).bind('keyup',function(event){
//													if (event.keyCode == keyCodes.ENTER) {
//														$('#btnTransferWarehouse').click();
//													}
//												});
												clearTimeout(tm);
											}, 10);
											TransferInternalWarehouse._needNewRow = false;
											$(this).combobox("hidePanel");
											break;
										}
	 					    		}
	 					    	}
						    },
						    filter: function(q, row){
								q = new String(q).toUpperCase();
								return row.productCode.indexOf(q)>=0;
							},
							onHidePanel: function() {
								if (!TransferInternalWarehouse._needNewRow) {
									return;
								}
								var pId = TransferInternalWarehouse._rowDgTmp.productId;
								TransferInternalWarehouse._mapStockTransDetail.put(TransferInternalWarehouse._rowDgTmp.productId, TransferInternalWarehouse._rowDgTmp);
				 				$('.ErrorMsgStyle').html('').hide();
				 				$('.SuccessMsgStyle').html('').hide();
				 				$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
				 				setTimeout(function(){
									$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
									$('#dgQuantity' +pId).focus();
//									$('#dgQuantity' +pId).bind('keyup',function(event){
//										if (event.keyCode == keyCodes.ENTER) {
//											$('#btnTransferWarehouse').click();
//										}
//									});
								}, 50);
								TransferInternalWarehouse._needNewRow = false;
							}
						}
					}},
				    {field:'productName', title:'Tên sản phẩm', width:120, align:'left', formatter: function(value, row, index){
				    	var prdName = 'dgProductName' + row.productId;
				    	return '<label id="' +prdName+'" style="width: 100%; text-align: left;">' +Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'availableQuantity', title:'Tồn kho đáp ứng', width:80, align:'right', formatter: function(value, row, index){
				    	var idAvlQ = "dgAvlQuantity" + row.productId;
				    	var html = '<input type="text" id= "' +idAvlQ+'" onchange="" value="' +StockValidateInput.formatStockQuantity(row.availableQuantity, row.convfact)+'" style="width: 100%; text-align: right;" maxlength="18" disabled="disabled" />';
				    	return html;
				    }},
				    {field:'quantity', title:'Số lượng', width:80, align:'right', formatter: function(value, row, index){
				    	var idQuan = "dgQuantity" + row.productId;
						if (row.quantity != undefined && row.quantity != null && row.quantity >= 0) {
							return '<input type="text" id= "' +idQuan+'" onchange="TransferInternalWarehouse.changeQuanttInPrdStockOutputDg(this);" value="' +StockValidateInput.formatStockQuantity(row.quantity, row.convfact)+'" style="width: 100%; text-align: right;" maxlength="18"/>';
				    	}
				    	return '<input type="text" id= "' +idQuan+'" onchange="TransferInternalWarehouse.changeQuanttInPrdStockOutputDg(this);" value="' +StockValidateInput.formatStockQuantity(0, row.convfact)+'" style="width: 100%; text-align: right;" maxlength="18"/>';
				    }},
				    {field:'price', title:'Giá bán', width:80, align:'right', formatter: function(value, row, index){
				    	var idPrice= "dgPrice" + row.productId;
						if (value != undefined && value != null) {
							return '<label id="' +idPrice+'" style="width: 100%; text-align: left;">' + VTUtilJS.formatCurrency(Utils.formatDoubleValue(value))+'</label>';
						}
						return '<label id="' +idPrice+'" style="width: 100%; text-align: left;">' + VTUtilJS.formatCurrency(Utils.formatDoubleValue(0))+'</label>';
				    }},
				    {field:'amount', title:'Thành tiền', width:80, align:'right', formatter: function(value, row, index){
				    	var idAmount = "dgAmount" + row.productId;
				    	if (value != undefined && value != null) {
							return '<label id="' +idAmount+'" style="width: 100%; text-align: left;">' + VTUtilJS.formatCurrency(Utils.formatDoubleValue(value))+'</label>';
						}
						return '<label id="' +idAmount+'" style="width: 100%; text-align: left;">' + VTUtilJS.formatCurrency(Utils.formatDoubleValue(0))+'</label>';
				    }},
				    {field:'detelte',title:'<a onclick="return TransferInternalWarehouse.openDialogInsertProduct();" href="javaScript:void(0);"><img src="/resources/images/icon_add.png"></a>',width:30, align:'center', formatter: function(value, row, index){
				    	if (row.productId != undefined && row.productId != null && row.productId > 0) { 
				    		return '<a href="javascript:void(0)" id="dg_prdStockOutputDg_delete" onclick="return TransferInternalWarehouse.deleteRowInPrdStockOutputDg(' +row.productId+');"><img title="Xóa" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
				    	}
				    }}
				]],
				onClickRow: function(rowIndex, rowData) {
					
				},
				onAfterEdit: function(rowIndex, rowData, changes) {
					
				},
				onBeforeEdit : function(rowIndex, rowData, changes){
				},
		        onLoadSuccess :function(data){
			    	 $('.datagrid-header-rownumber').html('STT');
			    	 Utils.updateRownumWidthAndHeightForDataGrid('prdStockOutputDg');
			    	 //Xu ly cho hien thi
			    	 var arrInput = '';
			    	 TransferInternalWarehouse.getDataComboboxInPrdStockOutputDg();
			    	 var rows = $('#prdStockOutputDg').datagrid('getRows');
			    	 if(rows == null || rows.length == 0 || rows[rows.length-1].productId != null){
			    		 TransferInternalWarehouse.insertRowStockTransDetailDg();
			    	 }
			    	 var sumAmountRow = 0;
			    	 var weight = 0;
			    	 if (TransferInternalWarehouse._mapStockTransDetail != null && TransferInternalWarehouse._mapStockTransDetail.size() > 0) {
			    		 for(var i=0; i < TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++){
			    			 var productTmp = TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]);
				    		 if (productTmp.amount != undefined && productTmp.amount != null) {
				    			 sumAmountRow += productTmp.amount;
				    		 }
				    		 var weightTmp = 0;
				    		 if (productTmp.grossWeight != undefined && productTmp.grossWeight != null && productTmp.quantity != undefined && productTmp.quantity != null) {
				    			 weightTmp = productTmp.grossWeight * productTmp.quantity;
				    		 }
				    		 weight += weightTmp;
				    	 }
			    	 } else {
			    		 TransferInternalWarehouse._mapStockTransDetail = new Map();
			    	 }
			    	 $('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
			    	 $('#lblSumWeightDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(weight)));
			    	 arrInput += '#btnChangeSaleOderAIAD';
			    	 $('' +arrInput+'').bind('keyup',function(event){
			 			 if(event.keyCode == keyCodes.ENTER){
			 			 	$('#btnChangeSaleOderAIAD').click();
			 			 }
			 		});
//					$('#dgQuantity0').bind('keyup',function(event){
//						if (event.keyCode == keyCodes.ENTER) {
//							$('#btnTransferWarehouse').click();
//						}
//					});
		    	}
			});
		}, null, null);
	},
	
	/**
	 * Thay doi so luong tren Row - prdStockOutputDg
	 * 
	 * @author hunglm16
	 * @since September 2,2014
	 * */
	changeQuanttInPrdStockOutputDg: function (txt) {
		$(".ErrorMsgStyle").html('').hide();
		var productId = Number($(txt).prop('id').replace(/dgQuantity/g,'').trim());
		var productRow = TransferInternalWarehouse._mapStockTransDetail.get(productId);
		var qtt = 0;
		if (productRow != undefined && productRow != null) {
			qtt = StockValidateInput.getQuantity($(txt).val(), productRow.convfact);
			if (!Utils.isValidQuantityInput($(txt).val().trim())) {
				$('#errMsg').html(Utils.XSSEncode($(txt).val().trim()) + ' Không đúng định dạng').show();
				qtt = StockValidateInput.getQuantity(productRow.quantity, productRow.convfact);
			}
			if (qtt > productRow.availableQuantity) {
				qtt = productRow.availableQuantity;
			}
			if (qtt < 0) {
				qtt = 0;
			}
			productRow.quantity = qtt;
			productRow.amount = productRow.quantity * productRow.price;
			$('#dgQuantity' +productId).val(StockValidateInput.formatStockQuantity(productRow.quantity, productRow.convfact));
	    	$('#dgAmount' +productId).html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(productRow.amount)));
	    	var sumAmountRow = 0;
	    	var sumWeightRow = 0;
	    	for(var i=0; i < TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++){
	    		var row = TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]);
	    		sumAmountRow += row.amount;
	    		sumWeightRow += row.grossWeight * row.quantity; 
	    	}
	    	$('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
	    	$('#lblSumWeightDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumWeightRow)));
		} else {
			qtt = StockValidateInput.getQuantity($(txt).val(), TransferInternalWarehouse._rowDgTmp.convfact);
			if (!Utils.isValidQuantityInput($(txt).val().trim())) {
				$('#errMsg').html(Utils.XSSEncode($(txt).val().trim()) + ' Không đúng định dạng').show();
				qtt = StockValidateInput.getQuantity(TransferInternalWarehouse._rowDgTmp.quantity, TransferInternalWarehouse._rowDgTmp.convfact);
			}
			if (qtt > TransferInternalWarehouse._rowDgTmp.availableQuantity) {
				qtt = TransferInternalWarehouse._rowDgTmp.availableQuantity;
			}
			if (qtt < 0) {
				qtt = 0;
			}
			TransferInternalWarehouse._rowDgTmp.quantity = qtt;
			TransferInternalWarehouse._rowDgTmp.amount = TransferInternalWarehouse._rowDgTmp.quantity * TransferInternalWarehouse._rowDgTmp.price;
			$('#dgQuantity0').val(StockValidateInput.formatStockQuantity( TransferInternalWarehouse._rowDgTmp.quantity, TransferInternalWarehouse._rowDgTmp.convfact));
	    	//$('#dgAvlQuantity0').val(StockValidateInput.formatStockQuantity(TransferInternalWarehouse._rowDgTmp.availableQuantity, TransferInternalWarehouse._rowDgTmp.convfact));
	    	$('#dgAmount0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
		}
	},
	
	/**
	 * Thay doi so luong tren Row - prdStockOutputDg
	 * 
	 * @author hunglm16
	 * @since September 2,2014
	 * */
	keyPressQuanttInPrdStockOutputDg: function (e, txt) {
		var inputStr = $(txt).val();
		if ((!/^[0-9\/]+$/.test(inputStr)) || (String(inputStr).indexOf('/')!=-1 && String(inputStr).split('/').length > 2)) {
			return;
		}
		var qtt = Number($(txt).val());
		var productId = Number($(txt).prop('id').replace(/dgQuantity/g,'').trim());
		var productRow = TransferInternalWarehouse._mapStockTransDetail.get(productId);
		if (productRow != undefined && productRow != null) {
			productRow.quantity = qtt;
			productRow.amount = productRow.quantity * productRow.price;
			$('#dgQuantity' +productId).val(StockValidateInput.formatStockQuantity( productRow.quantity, productRow.convfact));
	    	$('#dgAmount' +productId).html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
	    	var sumAmountRow = 0;
	    	for(var i=0; i < TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++){
	    		sumAmountRow += TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]).amount;
	    	}
	    	$('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
		} else {
			TransferInternalWarehouse._rowDgTmp.quantity = qtt;
			TransferInternalWarehouse._rowDgTmp.amount = TransferInternalWarehouse._rowDgTmp.quantity * TransferInternalWarehouse._rowDgTmp.price;
			$('#dgQuantity0').val(StockValidateInput.formatStockQuantity( TransferInternalWarehouse._rowDgTmp.quantity, TransferInternalWarehouse._rowDgTmp.convfact));
	    	$('#dgAmount0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
		}
	},
	
	/**
	 * Lay danh sach san pham sau loai tru trong Grid
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	getDataComboboxInPrdStockOutputDg: function () {
		if (TransferInternalWarehouse._mapStockTransDetail != null && TransferInternalWarehouse._mapStockTransDetail.size() > 0) {
   		 	for (var i = 0; i < TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++) {
	   		 	for (var j = TransferInternalWarehouse._lstproductavailable.length - 1; j >= 0; j--) {
	   		 		var prdId = TransferInternalWarehouse._mapStockTransDetail.keyArray[i];
	   				if (TransferInternalWarehouse._lstproductavailable[j].productId == prdId) { 
	   					TransferInternalWarehouse._lstproductavailable.splice(j, true);
	   				}
	   			}
	    	}
   	 	}
	},
	
	
	/**
	 * Lay du lieu cho PrdStockOutputDg
	 * 
	 * @author hunglm16
	 * @since September 2,2014
	 * */
	getDataInPrdStockOutputDg : function() {
		var rows = [];
		if (TransferInternalWarehouse._mapStockTransDetail != null || TransferInternalWarehouse._mapStockTransDetail.size() > 0){
			for (var i=0; i< TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++) {
				rows.push(TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]));
			}
		}
		return rows;
	},
	
	
	/**
	 * Xoa mot dong san pham
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	deleteRowInPrdStockOutputDg: function (productId) {
		if (productId == null || productId == null || productId == 0) {
			return;
		}
		if (TransferInternalWarehouse._mapStockTransDetail.keyArray.indexOf(productId) > -1) {
			for (var i = 0; i< TransferInternalWarehouse._mapProduct.keyArray.length; i++) {
				if (TransferInternalWarehouse._mapProduct.keyArray[i] == productId) {
					TransferInternalWarehouse._lstproductavailable.push(TransferInternalWarehouse._mapProduct.get(productId));
					//Sap xep lai
		   		 	var arrSort = [];
		   		 	for (var i = 0; i < TransferInternalWarehouse._lstproductavailable.length; i++) {
		   		 		var value = TransferInternalWarehouse._lstproductavailable[i].productCode + '_' + TransferInternalWarehouse._lstproductavailable[i].productId;
		   		 		arrSort.push(value.trim());
					}
		   		 	//lenh sap xep Order by ASC
		   		 	arrSort = arrSort.sort();
		   		 	//sap xep lai mang
		   		 	var data = [];
		   		 	for (var t = 0; t < arrSort.length; t++) {
		   		 		for (var i = 0; i < TransferInternalWarehouse._lstproductavailable.length; i++) {
		   		 			if(Number(arrSort[t].split('_')[1]) == TransferInternalWarehouse._lstproductavailable[i].productId) {
		   		 				data.push(TransferInternalWarehouse._lstproductavailable[i]);
		   		 			}
		   		 		}
		   		 	}
		   		 	//gan lai mang vua sap xep 
		   		 	TransferInternalWarehouse._lstproductavailable = new Array();
		   		 	for (var t = 0; t < data.length; t++) {
		   		 		TransferInternalWarehouse._lstproductavailable.push(data[t]);		   		 		
		   		 	}
					break;
				}
			}
			TransferInternalWarehouse._mapStockTransDetail.remove(productId);
			
			$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
		} else {
			$.messager.alert('Lỗi dữ liệu','Không tìm thấy Id Sản phẩm muốn xóa.','error');
		}
	},
	/**
	 * Them moi 2 dong trong vao Datagrid Stock Trans Detail
	 * 
	 * @author hunglm16
	 * @Since October 30, 2014
	 * */
	insertRowStockTransDetailDg: function () {
		var indexMax = $('#prdStockOutputDg').datagrid('getRows').length;
		$('#prdStockOutputDg').datagrid('insertRow',{
			index: indexMax,
			row: {
				productId: 0,
				productCode: '',
				productName: '',
				convfact: 1,
				warehouseId: 0,
				availableQuantity: 0,
				grossWeight: 0,
				price: 0,
				quantity: 0,
				amount: 0
			}
		});
		TransferInternalWarehouse._rowDgTmp = {
				productId: 0,
				productCode: '',
				productName: '',
				convfact: 1,
				warehouseId: 0,
				availableQuantity: 0,
				grossWeight: 0,
				price: 0,
				quantity: 0,
				amount: 0
	    	};
		
		$('#prdStockOutputDg').datagrid('selectRow', indexMax).datagrid('beginEdit', indexMax);
		$('.combobox-f.combo-f').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				TransferInternalWarehouse.openDialogInsertProduct();
			}
		});
	},
	
	/**
	 * Dialog Tim kiem va them san pham
	 *
	 * @author hunglm16
	 * @since November 5,2014
	 * @description Xu ly su kien thay doi
	 * */
	changeQuantityinDlProductSearch : function (txt, indexRow) {
		var rows = $('#cmndl-product-grid-search').datagrid("getRows");
		var productId = $(txt).attr("productid");
		
		var rowData = null;
		if (rows.length > 0 && productId != undefined && productId != null && Number(productId) > 0) {
			for (var i = 0; i < rows.length; i++) {
				if (productId == rows[i].productId) {
					var rowData = rows[i];
					break;
				}
			}
		}
		if (rowData != null) {
			var qtt = 0;
			if (Utils.isValidQuantityInput($(txt).val().trim())) {
				qtt = StockValidateInput.getQuantity($(txt).val().trim(), rowData.convfact);
			}
			if (qtt > rowData.availableQuantity) {
				qtt = rowData.availableQuantity;
			}
			if (qtt < 0) {
				qtt = 0;
			}
			rowData.quantity = qtt;
			if (qtt > 0) {
				TransferInternalWarehouse._mapProductDl.put(productId, rowData);
			} else if (TransferInternalWarehouse._mapProductDl.keyArray.indexOf(productId) > -1){
				TransferInternalWarehouse._mapProductDl.remove(productId);
			}
			$('#cmndl-product-grid-search').datagrid('updateRow',{
				index: indexRow,
				row: rowData
			});
			$('#cmndl-product-grid-search').datagrid('selectRow', indexRow);
			var txtQtt = $('.quantitydl')[indexRow + 1];
			if (txtQtt != undefined && txtQtt != null && $(txtQtt).length > 0) {
				$(txtQtt).focus();
			} else {
				$('#btnChooseProduct').focus();
			}
		}
	},
	/**
	 * Chosse Product In Dialog
	 *
	 * @author hunglm16
	 * @since November 5,2014
	 * */
	chooseProductInDialogSearch: function () {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var arrProductId = TransferInternalWarehouse._mapProductDl.keyArray;
		if (arrProductId == null || arrProductId.length == 0) {
			$('#errMsgCmndlProduct').html('Chưa sản phẩm nào được chọn').show();
			return;
		}
		
		var lstdata = [];
		for (var i=0, size = arrProductId.length; i < size; i++) {
			var row = TransferInternalWarehouse._mapProductDl.get(arrProductId[i]);
			if (row.quantity == null || row.quantity <= 0) {
				$('#errMsgCmndlProduct').html('Sản phẩm ' + Utils.XSSEncode(row.productCode) + ' phải có số lượng > 0').show();
				$('#dgQuantityPrdDl' + row.productId).focus();
				return;
			}
			var productInsert = {
	    			productId: row.productId,
					productCode: row.productCode,
					productName: row.productName,
					convfact: row.convfact,
					warehouseId: row.warehouseId,
					availableQuantity: row.availableQuantity,
					price: row.price,
					grossWeight: row.grossWeight,
					quantity: row.quantity,
					amount: row.quantity * row.price
		    };
			lstdata.push(productInsert);
		}
		for (var i=0; i < lstdata.length; i++) {
			if (TransferInternalWarehouse._mapStockTransDetail.keyArray.indexOf(lstdata[i].productId) < 0  && TransferInternalWarehouse._mapProduct.keyArray.indexOf(lstdata[i].productId) > -1) {
				TransferInternalWarehouse._mapStockTransDetail.put(lstdata[i].productId, lstdata[i]);
				//Xoa khoi danh sach Combobox
				TransferInternalWarehouse.getDataComboboxInPrdStockOutputDg();
			}
		}
		$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
		$('#cmndl-product-search-2-textbox').dialog("close");
	},
	
	/**
	 * Dialog Tim kiem va them san pham
	 *
	 * @author hunglm16
	 * @since November 5,2014
	 * */
	searchDialogInsertProduct: function () {
		$('#cmndl-product-grid-search').datagrid("load", {
			productCode: $('#txtCmnProductCode').val().trim(),
			productName: $('#txtCmnProductName').val().trim(),
			exception : TransferInternalWarehouse._mapStockTransDetail.keyArray.join(','),
			wareHouseOutputId: $('#dllStockOutput').combobox('getValue'),
			shopCodeOutput: $('#shopOutput').combobox('getValue')
		});
	},
	
	
	/**
	 * Dialog Tim kiem va them san pham
	 *
	 * @author hunglm16
	 * @since November 5,2014
	 * */
	openDialogInsertProduct: function () {
		$('#cmndl-product-search-2-textbox').dialog({
			title: 'Tìm Kiếm Sản Phẩm',
			width: 690, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				TransferInternalWarehouse._flagDialog = true;
				TransferInternalWarehouse._mapProductDl = new Map();
				$('#cmndl-product-grid-search').datagrid({
					url: '/transfer-internal-warehouse/search-List-Product-In-WareHouse',
					queryParams: {
						productCode: $('#txtCmnProductCode').val().trim(),
						productName: $('#txtCmnProductName').val().trim(),
						exception : TransferInternalWarehouse._mapStockTransDetail.keyArray.join(','),
						wareHouseOutputId: $('#dllStockOutput').combobox('getValue'),
						shopCodeOutput: $('#shopOutput').combobox('getValue')
					},
					width : $('#cmndl-product-grid-search').width() - 15,
				    rownumbers : true,
				    singleSelect:true,
				    pagination : true,
			        rownumbers : true,
			        pageNumber : 1,
			        checkOnSelect: false,
					selectOnCheck: false,
			        scrollbarSize: 0,
			        autoWidth: true,
			        pageList: [10],
			        autoRowHeight : true,
			        fitColumns : true,
				    //height: 275,
				    columns:[[
						{field:'productCode', title:'Mã sản phẩm', align:'left', width: 90, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:'productName', title:'Tên sản phẩm', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:'availableQuantity', title:'Tồn kho', width:80, align:'right', formatter: function(value, row, index){
					    	return StockValidateInput.formatStockQuantity(row.availableQuantity, row.convfact);
					    }},
						{field:'quantity', title:'Số lượng', width:80, align:'right', formatter: function(value, row, index){
							var idQuan = "dgQuantityPrdDl" + row.productId;
							if (row.quantity != null && row.quantity > 0) {
								return '<input type="text" id= "' +idQuan+'" productid="' +row.productId+'" value="' +StockValidateInput.formatStockQuantity(row.quantity, row.convfact)+'" onchange="TransferInternalWarehouse.changeQuantityinDlProductSearch(this, ' +index+');" class="quantitydl" style="width: 100%; text-align: right;" maxlength="18"/>';
							} else {
								return '<input type="text" id= "' +idQuan+'" productid="' +row.productId+'" value="" onchange="TransferInternalWarehouse.changeQuantityinDlProductSearch(this, ' +index+');" class="quantitydl" style="width: 100%; text-align: right;" maxlength="18"/>';
							}
						}}
					]],
					onCheck: function(rowIndex, rowData) {
					},
					onUncheck: function(rowIndex, rowData) {
					},
					onCheckAll: function(rows) {
					},
					onUncheckAll: function(rows) {
					},
					onClickRow: function(rowIndex, rowData) {
						var rowCbx = $('[name=cbxProductIdCmn]')[rowIndex];
						if(rowCbx!=undefined && rowCbx!=null && !rowCbx.checked){
							$('#cmndl-product-grid-search').datagrid('unselectRow', rowIndex);
						}else{
							$('#cmndl-product-grid-search').datagrid('selectRow', rowIndex);
						}
						return true;
					},
			        onLoadSuccess: function(data){
				    	 $('.datagrid-header-rownumber').html('STT');
				    	 Utils.updateRownumWidthAndHeightForDataGrid('cmndl-product-grid-search');
				    	 $('.quantitydl').bind('keyup',function(event){
				  			if(event.keyCode == keyCodes.ENTER){
				  				TransferInternalWarehouse.chooseProductInDialogSearch();
				 			}
				 		});
			        }
				});
			},
			onClose: function() {
				$('#cmndl-product-grid-container').html('<table id="cmndl-product-grid-search"></table>').change();
				$('#txtCmnProductCode').val("").change();
				$('txtCmnProductCode').val("").change();
				TransferInternalWarehouse._flagDialog = false;
			}
		});
	},
	
	/**
	 * Tao moi phieu Dieu chuyen kho
	 * 
	 * @author hunglm16
	 * @since Septemp 2,2014
	 * */
	createStockTransDetail: function() {
		if (TransferInternalWarehouse._flagDialog) {
			//Kiem tra dialog co dang mo hay khong
			$($('.quantitydl')[0]).focus();
			return;
		}
		
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		
		var wareHouseOutputId = $('#dllStockOutput').combobox('getValue');
		var wareHouseInputId = $('#dllStockInput').combobox('getValue');
		var arrDataInsert = [];
		
		if (wareHouseOutputId == undefined || wareHouseOutputId == null || wareHouseOutputId.trim().length == 0) {
			msg = "Chưa chọn Kho xuất";
		}
		if (msg.trim().length == 0) {
			if (wareHouseInputId == undefined || wareHouseInputId == null || wareHouseInputId.trim().length == 0) {
				msg = "Chưa chọn Kho nhập";
			}
		}
		if (msg.trim().length == 0 && Number(wareHouseOutputId) == Number(wareHouseInputId)) {
			msg = "Kho nhập và kho xuất không được trùng nhau";
		}
		
		if (msg.trim().length == 0) {
			if (TransferInternalWarehouse._mapStockTransDetail.size() == 0) {
				msg = "Chưa có sản phẩm nào được chọn";
			} else {
				for (var i = 0; i< TransferInternalWarehouse._mapStockTransDetail.keyArray.length; i++) {
					var product = TransferInternalWarehouse._mapStockTransDetail.get(TransferInternalWarehouse._mapStockTransDetail.keyArray[i]);
					if (product.availableQuantity < product.quantity) {
						msg = "Sản phẩm " + Utils.XSSEncode(product.productCode) + " có số lượng lớn hơn tồn kho đáp ứng";
						break;
					} else if (product.quantity == undefined || product.quantity == null || product.quantity < 1) {
						msg = "Sản phẩm " + Utils.XSSEncode(product.productCode) + " số lượng phải lớn hơn 0";
						$('#dgQuantity' +product.productId).focus();
						break;
					} else if (product.productId > 0) {
						var paramsPrd = product.productId.toString() + "ESC" + product.quantity;
						arrDataInsert.push(paramsPrd);
					}
				}
			}
		}
		if (msg.trim().length == 0 && arrDataInsert.length == 0) {
			msg = "Chưa có sản phẩm nào được chọn";
		}
		if (msg.trim().length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var params = {
				shopCodeOutput: $('#shopOutput').combobox('getValue'),
				shopCodeInput: $('#shopInput').combobox('getValue'),
				wareHouseOutputId: wareHouseOutputId,
				wareHouseInputId: wareHouseInputId,
				arrStockTransDetail: arrDataInsert.join(",")
		};
		var msg = "Bạn có muốn tạo phiếu Điều chuyển kho nội bộ?";
		Utils.addOrSaveData(params, "/transfer-internal-warehouse/create-Stock-Trainsfer-Internal", null, 'errMsg', function(data) {
			if (data.error != undefined && !data.error) {
//				 $('#dllStockOutput').change();
				disabled('btnTransferWarehouse');
				$('#txtStockTransCode').val(data.stockTransCode);
				$('#txtStockTransDate').val(data.stockTransDate);
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html("").hide();
				}, 5000);				
			}
		}, null, null, null, msg);
	},
	/**
	 * khoi tao conbobox don vi
	 */
	loadShopCbx: function() {
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindShopCbx('#shopOutput', data.rows, null, null, function(data) {
					if (data != null) {
						TransferInternalWarehouse.changeShop(Utils.XSSEncode(data.shopCode), '#dllStockOutput', true);
					}					
				});
				Utils.bindShopCbx('#shopInput', jQuery.extend(true, [], data.rows), null, null, function(data){
					if (data != null) {
						TransferInternalWarehouse.changeShop(Utils.XSSEncode(data.shopCode), '#dllStockInput', false);
					}	
				});
			}
		});	
	},
	/**
	 *  su kien change shop
	 */
	changeShop: function(shopCode, selector, isShopOutput) {
		var params = new Object();
		if (isShopOutput) {
			params.shopCodeOutput = shopCode;
		} else {
			params.shopCodeInput = shopCode;
		}
		$.ajax({
			type : "POST",
			url : '/transfer-internal-warehouse/change-shop',
			data : params,
			dataType : "json",
			success : function(data) {
				if (isShopOutput) {
					$('#txtStockTransCode').val(Utils.XSSEncode(data.stockTransCode));
					$('#txtStockTransDate').val(data.stockTransDate);
				}
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null) {
					$(selector).combobox({
						valueField : 'warehouseId',
						textField : 'warehouseName',
						data : data.lstWarehouseVO,
						width: 250,
						panelWidth: 206,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseCode) + ' - ' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row) {
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
						onSelect:function(rec) {
							if (isShopOutput) {
								TransferInternalWarehouse.eventChangeComboboxWareHouse(rec.warehouseId);
							}
							enable('btnTransferWarehouse');
						},
				        onLoadSuccess: function(){
				        	var arr = $(selector).combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$(selector).combobox('select', arr[0].warehouseId);
				        	}
				        }
					});
				}
			}
		});
	}
};
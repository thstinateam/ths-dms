var StockCountingApprove = {
	_xhrSave : null,
	_isChange:false,
	_ccMapProductsAndResults : null,
	_ccMapProducts : null,
	_myValue : null,
	_mapProductSelect :null,
	_mapProductSelectCheck:null,
	_cycleCountId: 0,
	_cycleCountType:-1,
	clearNewProduct: function() {
		var ccMapKey = StockCountingApprove._ccMapProducts.keyArray;
		var ccMapValue = StockCountingApprove._ccMapProducts.valArray;
		for( var i = 0; i < ccMapKey.length; i++ ) {
			var ccMap =  ccMapValue[i];
			if(ccMap.id== -1){
				var productCode = ccMap.productCode;
				StockCountingApprove._ccMapProducts.remove(productCode);
				StockCountingApprove._ccMapProductsAndResults.remove(productCode);
				--i;
			}
	    }
		return false;
	},
	clearErrorMsg : function(){
		$(".ErrorMsgStyle").hide();
	},
	replaceProduct: function(){
		$('.CycleCountMapProducts').each(function(){
			var productCode = $(this).val();
			var ccMapProductId = $(this).attr('id');
			var ccMap = StockCountingApprove._ccMapProducts.get(productCode);
			if(ccMap != null){
				var checkLot = ccMap.checkLot;
				if(checkLot==0){
					var quantityTxt = StockValidateInput.formatStockQuantity(ccMap.quantity, ccMap.convfact);
					$('#td_qtyCount_' +productCode).val(quantityTxt);
				}else{
					var lstCCResult =StockCountingApprove._ccMapProductsAndResults.get(productCode);
					var quantity = 0;
					if(lstCCResult != null){
						for(var j=0; j < lstCCResult.length ;j++){
							var ccresult = lstCCResult[j];
							quantity += Number(ccresult.quantity);
						}
					}
					var quantityTxt = StockValidateInput.formatStockQuantity(quantity, ccMap.convfact);
					$('#td_qtyCountP_' +productCode).html(quantityTxt).show();
				}
			}
		});
		return false;
	},
	
	openSelectProductDialog: function(){
		$(".ErrorMsgStyle").hide();
		
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		StockCountingApprove._mapProductSelect = new Map();
		StockCountingApprove._mapProductSelectCheck = new Map();

		var html = $('#productDialog').html();
		CommonSearch._arrParams = null;
		$('#productEasyUIDialog').dialog({
			title: "Tìm kiếm sản phẩm",
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        height :600,
	        onOpen: function(){
	        	$('#productEasyUIDialog').addClass("easyui-dialog");
	        	$('#productEasyUIDialog .InputTextStyle').val('');
	    		var params = new Object();
	    		var ccMapKey = StockCountingApprove._ccMapProducts.keyArray;
	    		var ccMapValue = StockCountingApprove._ccMapProducts.valArray;
	    		var lstProduct = new Array();
	    		for( var i = 0; i < ccMapKey.length; i++ ) {
	    			var ccMap = ccMapValue[i];
	    			if(ccMap.id == -1){
	    				lstProduct.push(ccMap.productId);
	    			}
	    	    }
	    		params.lstProductString = lstProduct.join(",");
	        	$('#productEasyUIDialog #productGrid').datagrid({
					url : '/stock/approve/search-product?cycleCountCode=' +$('#cycleCountCode').val().trim()+'&shopId=' +$('#shopId').val().trim(),
					autoRowHeight : true,
					rownumbers : true,					
					pagination:true,
					rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					queryParams:params,
					pageList  : [10,20,30],
					pageNumber:1,
					width : $('#productEasyUIDialog #productGridContainerEx').width(),
				    columns:[[	
				            {field: 'productId', checkbox: true},
							{field: 'productCode',index:'productCode', title: 'Mã sản phẩm', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(v, r, i) {
								if (v) {
									return Utils.XSSEncode(v);
								}
								return "";
							}},
							{field: 'productName',index:'productName', title: 'Tên sản phẩm', width: 320, sortable:false,resizable:false, align: 'left', formatter: function(v, r, i) {
								if (v) {
									return Utils.XSSEncode(v);
								}
								return "";
							}},
							{field: 'cat',index:'cat', title: 'Ngành hàng', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(v, r, i) {
								if (v) {
									return Utils.XSSEncode(v);
								}
								return "";
							}},
							{field: 'subCat',index:'subCat', title: 'Ngành hàng con', width: 120, sortable:false,resizable:false,align: 'left', formatter: function(v, r, i) {
								if (v) {
									return Utils.XSSEncode(v);
								}
								return "";
							}},
							{field: 'availableQuantity',index:'availableQuantity', title: 'Tồn kho',width: 100, sortable:false,resizable:false, align: 'right',formatter: CountingFormatter.quantityConvert},
							{field: 'uom1', index:'uom1', hidden: true},
							{field: 'checkLot', index:'checkLot', hidden: true},
							{field: 'quantity', index:'quantity', hidden: true},
							{field: 'convfact', index:'convfact', hidden: true}
				    ]],
				    onBeforeLoad:function(param){
				    	
				    },
				    onLoadSuccess :function(data){
				    	$('.datagrid-header-rownumber').html('STT');
				    	Utils.bindAutoSearch();
				    	if (data.rows == undefined || data.rows == null) {
				    		return;
				    	}
				    	var length = 0;
				    	var r;
				    	var rows = data.rows;
				    	var obj;
				    	for (var i = 0, sz = rows.length; i < sz; i++) {
				    		r = rows[i];
				    		obj = StockCountingApprove._mapProductSelect.get(r.productCode);
				    		if (obj != undefined && obj != null) {
				    			$('td[field=productId] input[value=' + r.productId + ']').attr("checked", "checked");
				    		}
				    	}
				    	if(data.rows.length==length){
				    		$('.datagrid-header-check input').attr('checked',true);
				    	}else{
				    		$('.datagrid-header-check input').attr('checked',false);
				    	}
				    },
			    onCheck:function(i,r){
						var obj = new Object();
						obj.id = r.productId;
						obj.code = r.productCode;
						obj.name = r.productName;
						obj.uom1 = r.uom1;
						obj.checkLot = r.checkLot;
						obj.convfact = r.convfact;
						obj.quantity = r.quantity;
						var flag = true;
						$('#cycleCountMapProductsTableDetail .CycleCountMapProducts').each(function(){
							var value = $(this).attr('id');
                             if(value != null && value != undefined && value != ""){
             					if(value == r.id){
             						flag = false;
             					}
                             }  
						});
						if(flag){
							StockCountingApprove._mapProductSelect.put(obj.code, obj);
						}
						StockCountingApprove._mapProductSelectCheck.put(obj.code, obj);
					
			    },
			    onUncheck:function(i,r){
			    	StockCountingApprove._mapProductSelect.remove(r.productCode);
		    		StockCountingApprove._mapProductSelectCheck.remove(r.productCode);
			    },
			    onCheckAll:function(r){
			    	for(i=0;i<r.length;i++){
						var obj = new Object();
						obj.id = r[i].productId;
						obj.code = r[i].productCode;
						obj.name = r[i].productName;
						obj.uom1 = r[i].uom1;
						obj.checkLot = r[i].checkLot;
						obj.convfact = r[i].convfact;
						obj.quantity = r[i].quantity;
						var flag = true;
						$('#cycleCountMapProductsTableDetail .CycleCountMapProducts').each(function(){
							var value = $(this).attr('id');
                             if(value != null && value != undefined && value != ""){
             					if(value == r[i].id){
             						flag = false;
             					}
                             }  
						});
						if(flag){
							StockCountingApprove._mapProductSelect.put(obj.code, obj); 
						}
						StockCountingApprove._mapProductSelectCheck.put(obj.code, obj);
			    	}
			    },
			    onUncheckAll:function(r){
			    	for (i = 0; i < r.length; i++) {
			    		StockCountingApprove._mapProductSelect.remove(r[i].productCode);
			    		StockCountingApprove._mapProductSelectCheck.remove(r[i].productCode);
			    	}
			    }
				});
	        	$('#productEasyUIDialog #productCode').unbind('keyup');
	        	$('#productEasyUIDialog #productCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			StockCountingApprove.searchProduct();
	        		}
	        	});
	        	$('#productEasyUIDialog #productName').unbind('keyup');
	        	$('#productEasyUIDialog #productName').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			StockCountingApprove.searchProduct();
	        		}
	        	});
	        },	     
	        onClose : function() {
	        	$('#productEasyUIDialog').dialog("destroy");
	        	$('#productDialog').html(html);
	        }
	     });
	},
	searchProduct: function() {
		$(".ErrorMsgStyle").hide();
		var code = $('#productCode').val().trim();
		var name = $('#productName').val().trim();
		var msg = Utils.getMessageOfSpecialCharactersValidate('productCode', 'Mã sản phẩm', Utils._CODE);
		if(msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('productName', 'Tên sản phẩm', Utils._NAME);
		}
		if(msg.length > 0) {
			$('#addProductError').html(msg).show();
			return;
		}
		var params = {productCode: code, productName: name};
		var ccMapKey = StockCountingApprove._ccMapProducts.keyArray;
		var ccMapValue = StockCountingApprove._ccMapProducts.valArray;
		var lstProduct = new Array();
		for( var i = 0; i < ccMapKey.length; i++ ) {
			var ccMap = ccMapValue[i];
			if(ccMap.id == -1){
				lstProduct.push(ccMap.productId);
			}
	    } 
		params.lstProductString = lstProduct.join(",");
		$("#productGrid").datagrid("load", params);		
		return false;
	},	
	selectListProducts: function() {
		if (StockCountingApprove._mapProductSelectCheck.size() <= 0) {
			$('#addProductError').html('Vui lòng chọn sản phẩm để thêm vào phiếu').show();
			return false;
		} else {
			var cardNumber = 0;
			if ($('#cycleCountMapProductsTableDetail .ColsTd1').length == 0) {
				cardNumber = Number($("#lastNumber").val().trim());
				var tNo = Number($("#firstNumber").val().trim());
				if (cardNumber < tNo - 1) {
					cardNumber = tNo - 1;
				}
			} else {
				var cardNo = $('#cycleCountMapProductsTableDetail .ColsTd1').last().text();
				cardNumber = Number(cardNo.trim());
			}
			var productKeySelect = StockCountingApprove._mapProductSelect.keyArray;
			var productValueSelect = StockCountingApprove._mapProductSelect.valArray;
			for(var i=0; i < productKeySelect.length ;i++){
				var obj = productValueSelect[i];
				var ccMap = {};
				ccMap.id = -1 ;
				ccMap.checkLot = obj.checkLot;
				ccMap.convfact = obj.convfact;
				ccMap.quantity = 0;
				ccMap.productId = obj.id;
				ccMap.productCode = obj.code;
				ccMap.quantityBeforeCount = obj.quantity;
				cardNumber = cardNumber + 1;
				StockCountingApprove.addHTMLProduct(ccMap.id,obj.code,obj.name,obj.uom1,obj.checkLot,obj.id,obj.convfact, ccMap.quantityBeforeCount,cardNumber);
				StockCountingApprove._ccMapProducts.put(obj.code,ccMap);
			}
			Utils.bindFormatOnTextfieldInputCss('BindFormatOnInputCss', Utils._TF_NUMBER_CONVFACT);
			$('.easyui-dialog').dialog('close');
			$('#cycleCountMapProductsTableDetail td').css('padding','2px');
		}		
		return false;
	},
	addHTMLProduct: function(ccMapId,code,name,uom,checkLot,productId,convfact, quantity,cardNumber){
		var html = new Array();
		html.push('<tr id="tr_' + Utils.XSSEncode(code) +'">');
		html.push('<td id="cardNumber" class="ColsTd1 AlignCCols">');
		html.push(cardNumber);
		html.push('<input type="hidden" id="' +Utils.XSSEncode(productId)+'" class="CycleCountMapProducts" value="' +Utils.XSSEncode(code) + '"/>');
		html.push('<input type="hidden" id="lot" value="' +Utils.XSSEncode(checkLot) + '"/>');
		html.push('<input type="hidden" id="convfact" value="' +Utils.XSSEncode(convfact) + '"/>');
		html.push('</td>');
		html.push('<td class="ColsTd2">' +Utils.XSSEncode(code)+'</td>');
		html.push('<td class="ColsTd3">' +Utils.XSSEncode(name) +'</td>');
		html.push('<td class="ColsTd4">' +Utils.XSSEncode(uom)+'</td>');
		html.push('<td class="ColsTd5"></td>');
		html.push('<td class="ColsTd6 AlignRight"><span id="td_qtyBfCountP_' + Utils.XSSEncode(code)+'">' +StockValidateInput.formatStockQuantity(quantity, convfact)+'</span></td>');
		if(checkLot == 0){
			html.push('<td class="ColsTd7">');
			html.push('<input id="td_qtyCount_' + Utils.XSSEncode(code)+'" maxlength="9" value="" onkeypress="return NextAndPrevTextField(event,this,\'CCMapInput\')"  onchange="return StockCountingApprove.changeCCMap(this,' +ccMapId+',\'' +code+'\',' +checkLot+',' +convfact+')"  type="text" class="InputTextStyle InputText1Style AlignRightInput BindFormatOnInputCss CCMapInput" />');
			html.push('</td>');
		}else{
			html.push('<td class="ColsTd7" style="padding-right: 22px; text-align: right !important">');
			html.push('<span id="td_qtyCountP_' +Utils.XSSEncode(code)+'"></span>');
			html.push('</td>');
		}
		html.push('<td class="ColsTd8 AlignRight">');
		if(checkLot==1){
			var param = Utils.XSSEncode(ccMapId) +',' +Utils.XSSEncode(productId) +',\'' +Utils.XSSEncode(code)+'\',\'' + Utils.XSSEncode(name)+'\',' +Utils.XSSEncode(convfact);
			html.push('<a href="javascript:void(0)" onclick="StockCountingApprove.openCCResult(' + param + ')" >Nhập chi tiết</a>');
		}
		html.push('</td>');
		html.push('<td class="ColsTd9 ColsTdEnd" style="text-align: center;">');
		html.push('<a onclick="StockCountingApprove.delCCMapProduct(\'' +Utils.XSSEncode(code) +'\')" href="javascript:void(0)">');
		html.push('<img src="/resources/images/icon_delete.png" width="16" height="16" />')	;					
		html.push('</a>');
		html.push('</td>');		
		html.push('</tr>');
		var check = $('#checkListEmpty').val();
		if(check == 0) {
			$('#cycleCountMapProductsTableDetail').html('');
			$('#checkListEmpty').val(1);
		}
		$('#cycleCountMapProductsTableDetail').append(html.join(""));		
	},
	clearCCResultInput: function(){
		$('.easyui-dialog #d_lot').val('');
		$('.easyui-dialog #d_qtyCount').val('');
	},
	disableAddCCResult : function(){
		$('.easyui-dialog #d_lot').attr('disabled',true);
		$('.easyui-dialog #d_lot').val('');
		$('.easyui-dialog #d_qtyCount').attr('disabled',true);
		$('.easyui-dialog #d_qtyCount').val('');
		$('.easyui-dialog #btnSaveCCResult').attr('disabled',true);
	},
	showTotalQuantityAndScrollBodyCCResult:function(prodCode,convfact){	
		var cycleCountType = $('#h_type').val();	
		var runner = 0;
		$('.easyui-dialog .OrderNumber').each(function(){
			runner+=1;
			$(this).html(runner).show();
		});
		var totalBeforeCount = 0;
		var totalCount = 0;
		var s = StockCountingApprove._ccMapProductsAndResults;
		var temp = StockCountingApprove._ccMapProductsAndResults.get(prodCode);
		if(temp == null || temp == undefined){
			$('.easyui-dialog #totalBeforeCount').html('0/0');
			$('.easyui-dialog #totalCount').html('0/0');
			$(window).resize();
		}else{
			for(var i=0; i < StockCountingApprove._ccMapProductsAndResults.get(prodCode).length ;i++){
				var quantityBeforeCount = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].quantityBeforeCount;
				var quantity = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].quantity;
				if(quantityBeforeCount != null){
					totalBeforeCount += parseInt(quantityBeforeCount);
				}
				if(quantity != null){
					totalCount += parseInt(quantity);
				}
			}
			var value = StockValidateInput.formatStockQuantity(totalBeforeCount, convfact);
			$('.easyui-dialog #totalBeforeCount').html(value);
			value = StockValidateInput.formatStockQuantity(totalCount, convfact);
			$('.easyui-dialog #totalCount').html(value);
			$('.easyui-dialog #cycleCountResultsScroll').jScrollPane();
			Utils.bindFormatOnTextfieldInputCss('BindFormatOnInputCss', Utils._TF_NUMBER_CONVFACT);
			$(window).resize();
		}
	},
	openCCResult: function(cycleCountMapProductId,prodId,prodCode,prodName,convfact) {
		$('#productLotDialog').css("visibility", "visible");
		var html = $('#productLotDialog').html();
		$('#productLotEasyUIDialog').dialog({closed: false,cache: false,modal: true,width : 700,height :450,
			onOpen: function(){
				$('#productLotDialog').html('');
				$('.easyui-dialog #cycleCountResultsTableDetail').html('');
				$('.easyui-dialog #d_productCode').html(Utils.XSSEncode(prodCode));
				$('.easyui-dialog #d_productName').html(Utils.XSSEncode(prodName));
				var cycleCountType = parseInt($('#h_type').val().trim());
				if(cycleCountType!=0 || cycleCountMapProductId != -1){
					StockCountingApprove.disableAddCCResult();
				}else{
					$('.easyui-dialog #d_lot').attr('disabled',false);
					$('.easyui-dialog #d_qtyCount').attr('disabled',false);
					$('.easyui-dialog #btnSaveCCResult').attr('disabled',false); 
				}
				Utils.bindFormatOnTextfield('.easyui-dialog #d_lot',Utils._TF_NUMBER);
				Utils.bindFormatOnTextfield('.easyui-dialog #d_qtyCount',Utils._TF_NUMBER_CONVFACT);
				$('.easyui-dialog #d_productCode').html(Utils.XSSEncode(prodCode));
				$('.easyui-dialog #d_productName').html(Utils.XSSEncode(prodName));
				$('.easyui-dialog #h_ProductCode').val(Utils.XSSEncode(prodCode));
				$('.easyui-dialog #h_Convfact').val(Utils.XSSEncode(convfact));
				$('.easyui-dialog #btnCloseProductLot').click(function(){
					var totalCounted = $('.easyui-dialog #totalCount').text();
					var beforeCount = $('.easyui-dialog #totalBeforeCount').text();
					$('#td_qtyCountP_' +prodCode).html(Utils.XSSEncode(totalCounted));
					$('#td_qtyBfCountP_' +prodCode).html(Utils.XSSEncode(beforeCount));
					var ccMap = {};
					ccMap.id = cycleCountMapProductId;
					ccMap.checkLot = 1;
					ccMap.convfact = convfact;
					ccMap.quantity = StockValidateInput.getQuantity(totalCounted, convfact);
					ccMap.quantityBeforeCount = StockValidateInput.getQuantity(totalBeforeCount, convfact);
					ccMap.productId = prodId;
					ccMap.productCode = prodCode;
					StockCountingApprove._ccMapProducts.put(prodCode,ccMap);
					$('.easyui-dialog').dialog('close');
				});
				if (StockCountingApprove._ccMapProductsAndResults.get(prodCode) != null) {
					/**TH cycleCountMapProduct da duoc sua va duoc luu tru trong _ccMapProductsAndResults */
					if(StockCountingApprove._ccMapProductsAndResults.get(prodCode).length <= 0){
						return false;
					} else {
						for(var i=0; i < StockCountingApprove._ccMapProductsAndResults.get(prodCode).length ;i++){
							var ccResultId = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].id;
							var lot = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].lot;
							var quantityBeforeCountTmp = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].quantityBeforeCount;
							var quantityTmp = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].quantity;
							var check = StockCountingApprove._ccMapProductsAndResults.get(prodCode)[i].check;
							var	quantityBeforeCount = StockValidateInput.formatStockQuantity(quantityBeforeCountTmp, convfact);
							var quantity =  StockValidateInput.formatStockQuantity(quantityTmp,convfact);
							
							var html = StockCountingApprove.getHTMLCCResult(ccResultId,lot,quantityBeforeCount,quantity,check);
							$('.easyui-dialog #cycleCountResultsTableDetail').append(html);
						}
						StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(prodCode,convfact);
					}
				} else {
					/**TH cycleCountMapProduct chua sua va chua duoc luu tru trong _ccMapProductsAndResults */
					StockCountingApprove._myValue = new Array();
					$.getJSON('/rest/catalog/getCCResult/' +prodId+'/' + cycleCountMapProductId +'/list.json', function(data){	
						if(data.length <=0){
							StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(prodCode,Number(convfact));
							return false;
						} else {
							for (var i= 0 ; i< data.length; i++) {
								var ccresult = {};
								ccresult.id = data[i].id;
								ccresult.quantity = data[i].quantityCounted;
								ccresult.lot = data[i].lot;
								ccresult.quantityBeforeCount = data[i].quantityBeforeCount;
								if(ccresult.id != null) {
									ccresult.check = 0; // 0: is product and lot old
								} else{
									ccresult.check = 1; // 1: is product and lot new
								}
								StockCountingApprove._myValue.push(ccresult);
								var quantity = StockValidateInput.formatStockQuantity(ccresult.quantity , convfact);
								var quantityBeforeCount =StockValidateInput.formatStockQuantity(ccresult.quantityBeforeCount , convfact);
								var html = StockCountingApprove.getHTMLCCResult(ccresult.id, ccresult.lot,quantityBeforeCount,quantity,ccresult.check);
								$('.easyui-dialog #cycleCountResultsTableDetail').append(html);
							}
						}
						if(StockCountingApprove._myValue.length > 0){
							StockCountingApprove._ccMapProductsAndResults.put(prodCode,StockCountingApprove._myValue);
							StockCountingApprove._myValue = null;
						}
						StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(prodCode,Number(convfact));
					});
				}
			},
			onClose : function(){
	        	$('#productLotDialog').html(html);
	        	$('#productLotDialog').css("visibility", "hidden");	 
	        	$('.easyui-dialog #errMsg2').html('').hide();
	        	$('.easyui-dialog #btnCloseProductLot').unbind('click');
	        	$('.easyui-dialog #d_lot').val('');
	        	$('.easyui-dialog #d_qtyCount').val('');
	        }
		});		
		return false;
	},
	changeDLot: function (input){
		var convfact = Number($('.easyui-dialog #h_Convfact').val());
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact);
		$(input).val(quantityTxt);
		return false;
	},
	changeCCResult: function (id, lot, input){
		$('.easyui-dialog #errMsg2').html('').hide();
		var productCode = $('.easyui-dialog #h_ProductCode').val();
		var convfact = Number($('.easyui-dialog #h_Convfact').val());
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact) ;
		var quantity =  StockValidateInput.getQuantity(quantityTxt, convfact);
		
		for (var i=0; i< StockCountingApprove._ccMapProductsAndResults.get(productCode).length; i++) {
			if (StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].id == id && StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].lot == lot)
			{
				var quantityOld  = StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].quantity;
				var quantityOldTxt = StockValidateInput.formatStockQuantity(quantityOld, convfact);
				if(quantity > 9999999) {
					$('.easyui-dialog #errMsg2').html('Số lượng đếm lô ' + Utils.XSSEncode(lot)+' không được vượt quá 9999999').show();
					$(input).val(quantityOldTxt);
					$(input).focus();
					return false;
				}
				$(input).val(quantityTxt);
				StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].quantity = quantity ;
				StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(productCode,Number(convfact));
				break;
			}
		}
		return false;
	},
	changeCCResultBf: function (id, lot, input){
		$('.easyui-dialog #errMsg2').html('').hide();
		var productCode = $('.easyui-dialog #h_ProductCode').val();
		var convfact = Number($('.easyui-dialog #h_Convfact').val());
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact) ;
		var quantity =  StockValidateInput.getQuantity(quantityTxt, convfact);
		
		for (var i=0; i< StockCountingApprove._ccMapProductsAndResults.get(productCode).length; i++) {
			if (StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].id == id && StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].lot == lot)
			{
				var quantityOld  = StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].quantity ;
				var quantityOldTxt = StockValidateInput.formatStockQuantity(quantityOld, convfact);
				if(quantity > 9999999) {
					$('.easyui-dialog #errMsg2').html('Số lượng đếm lô ' + Utils.XSSEncode(lot)+' không được vượt quá 9999999').show();
					$(input).val(quantityOldTxt);
					$(input).focus();
					return false;
				}
				$(input).val(quantityTxt);
				StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].quantityBeforeCount = quantity ;
				StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(productCode,Number(convfact));
				break;
			}
		}
		return false;
	},
	changeCCMap: function (input,ccMapId,productCode,checkLot,convfact){
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact);
		var quantity = StockValidateInput.getQuantity(quantityTxt, convfact);
		$(input).val(quantityTxt);
		var ccMap = StockCountingApprove._ccMapProducts.get(productCode);
		if(ccMap == null){
			var ccMap = {};
			ccMap.id = ccMapId ;
			ccMap.checkLot = checkLot;
			ccMap.convfact = convfact;
			ccMap.quantity = quantity;			
			StockCountingApprove._ccMapProducts.put(productCode,ccMap);
		} else{
			ccMap.quantity = quantity;
		}
		var quantityBeforeCount = StockValidateInput.getQuantity($('#td_qtyBeforeCount' +id).text().trim(),convfact);
		var difference = Number(quantityBeforeCount) - Number(quantity);
		$('#td_qtyDifference' +id).text(StockValidateInput.formatStockQuantity(difference, convfact));
		return false;
	},
	myStockInputFun: function(id,convfact){
		var quantityCounted = StockValidateInput.getQuantity($('#td_qtyCount' +id).val().trim(),convfact);
		$('#td_qtyCount' +id).val(StockValidateInput.formatStockQuantity(quantityCounted, convfact));
		if($('#td_qtyCount' +id).val().trim().length==0){
			$('#td_qtyCount' +id).val('0/0');
			$('#td_chkApprove' +id).attr('checked',false);
		}else{
			$('#td_chkApprove' +id).attr('checked',true);  
		}	
		
		var quantity = StockValidateInput.getQuantity($('#td_qtyBeforeCount' +id).text().trim(),convfact);
		var difference = Number(quantity) - Number(quantityCounted);
		$('#td_qtyDifference' +id).text(StockValidateInput.formatStockQuantity(difference, convfact));
	},
	changeCCMapBf: function (input,ccMapId,productCode,checkLot,convfact){
		var quantityTxt = StockValidateInput.formatStockQuantity($(input).val().trim(), convfact);
		var quantity = StockValidateInput.getQuantity(quantityTxt, convfact);
		$(input).val(quantityTxt);
		var ccMap = StockCountingApprove._ccMapProducts.get(productCode);
		if(ccMap == null){
			var ccMap = {};
			ccMap.id = ccMapId ;
			ccMap.checkLot = checkLot;
			ccMap.convfact = convfact;
			ccMap.quantityBeforeCount = quantity;
			StockCountingApprove._ccMapProducts.put(productCode,ccMap);
		} else {
			ccMap.quantityBeforeCount = quantity;
		}
		return false;
	},
	delCCResult:function(id,lot){
		var productCode = $('.easyui-dialog #h_ProductCode').val().trim();
		$('.easyui-dialog #errMsg2').html('').hide();
		for (var i=0; i< StockCountingApprove._ccMapProductsAndResults.get(productCode).length; i++) {
			if (StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].id == id && StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].lot == lot){
				var msg = 'Bạn có chắc chắn xóa lô ' + Utils.XSSEncode(lot) + ' khỏi đợt kiểm kê này không ?';
				$.messager.confirm('Xác nhận', msg, function(r){
					if (r){
						// Xoa row 
						$('#row_' + lot).replaceWith(''); 
						// Update lai cac gia tri trong Map va tren mang hinh
						StockCountingApprove._ccMapProductsAndResults.get(productCode).splice(i,1);
						var convfact = StockCountingApprove._ccMapProducts.get(productCode).convfact;
						StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(productCode,Number(convfact));
					}
				});
				break;
			}
		}
		return false;
	},
	addCCResult: function() {
		$('.easyui-dialog #errMsg2').hide();
		var lot = $('.easyui-dialog #d_lot').val().trim();
		var my_qtyCount = $('.easyui-dialog #d_qtyCount').val().trim();
		var productCode = $('.easyui-dialog #h_ProductCode').val().trim();
		var convfact = $('.easyui-dialog #h_Convfact').val();
		if(isNullOrEmpty(lot)){
			$('.easyui-dialog #errMsg2').html('Bạn chưa nhập giá trị Số lô. Yêu cầu nhập giá trị.').show();
			return false;
		}
		if(isNullOrEmpty(my_qtyCount)){
			$('.easyui-dialog #errMsg2').html('Bạn chưa nhập giá trị Số lượng. Yêu cầu nhập giá trị.').show();
			return false;
		}
		var checkformat = Utils.getMessageOfInvalidFormatLot('d_lot','Số lô',6);
		if(checkformat.length > 0){
			$('.easyui-dialog #errMsg2').html(checkformat).show();
			return false;
		}
		var checkpoint =false;
		if (StockCountingApprove._ccMapProductsAndResults.get(productCode) != null ) {
			for (var i=0; i < StockCountingApprove._ccMapProductsAndResults.get(productCode).length;i++) {
				// Check ton tai lo nay cua san pham
				if(StockCountingApprove._ccMapProductsAndResults.get(productCode)[i].lot == lot ) {
					checkpoint = true;
					break;
				}
			}
			if(checkpoint) {
				$('.easyui-dialog #errMsg2').html('Số lô đã tồn tại trong danh sách').show();
				return false;
			}
		}
		var msg = 'Bạn có muốn thêm lô ' +Utils.XSSEncode(lot)+' vào đợt kiểm kê này không?';
		var quantity = StockValidateInput.getQuantity(my_qtyCount, convfact);
		if(quantity > 9999999) {
			$('.easyui-dialog #errMsg2').html('Số lượng không được vượt quá 9999999').show();
			$('.easyui-dialog #d_qtyCount').focus();
			return false;
		}
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				var quantityTxt = StockValidateInput.formatStockQuantity(quantity, convfact);
				if (StockCountingApprove._ccMapProductsAndResults.get(productCode) != null ) {
					var ccresult = {};
					ccresult.id = -1;
					ccresult.quantityBeforeCount = 0;
					ccresult.quantity = quantity;
					ccresult.lot = lot;
					ccresult.check = 2 ; // them moi lo
					StockCountingApprove._ccMapProductsAndResults.get(productCode).push(ccresult);
					// Add them row moi vao bang
					var html = StockCountingApprove.getHTMLCCResult(ccresult.id, ccresult.lot,ccresult.quantityBeforeCount, quantityTxt, ccresult.check);
					$('.easyui-dialog #cycleCountResultsTableDetail').append(html);
				} else {
					// SP moi duoc them vao chua co ccResult hoac productlot nao ca
					var ccresult = {};
					ccresult.id = -1;
					ccresult.quantityBeforeCount = 0;
					ccresult.quantity = quantity;
					ccresult.lot = lot;
					ccresult.check = 2 ; // them moi lo
					StockCountingApprove._myValue = new Array();
					StockCountingApprove._myValue.push(ccresult);
					StockCountingApprove._ccMapProductsAndResults.put(productCode,StockCountingApprove._myValue);
					StockCountingApprove._myValue = null;
					// Add them row moi vao bang
					var html = StockCountingApprove.getHTMLCCResult(ccresult.id, ccresult.lot, ccresult.quantityBeforeCount, quantityTxt, ccresult.check);
					$('.easyui-dialog #cycleCountResultsTableDetail').append(html);
				}
				StockCountingApprove.showTotalQuantityAndScrollBodyCCResult(productCode,Number(convfact));
				StockCountingApprove.clearCCResultInput();
			}
		});
	},
	getHTMLCCResult: function(id, lot, qtyBeforeCount, qtyCount, check){
		var cycleCountType = $('#h_type').val();
		var html = new Array();
		html.push('<tr id="row_' + Utils.XSSEncode(lot) +'">');
		html.push('<td class="OrderNumber" style="border-left: 1px solid #C6D5DB;text-align: center;height:24px;"></td>'); //stt
		html.push('<td class="ColsTd2 AlignLeft">' + Utils.XSSEncode(lot) +'</td>');  //solo
		if(qtyBeforeCount==null || qtyBeforeCount == "" || qtyBeforeCount == undefined){
			qtyBeforeCount = '0/0';
		}
		html.push('<td class="ColsTd3 AlignRight">' + Utils.XSSEncode(qtyBeforeCount)  +'</td>');
		if(cycleCountType == 0) {
			html.push('<td class="ColsTd4 AlignRight"><input id="td_qtyCount_CC_' +Utils.XSSEncode(id)+'_' +Utils.XSSEncode(lot)+'" type="text" maxlength="7" size="20" class="InputTextStyle InputStyleStock BindFormatOnInputCss CCResultInput" style="float:right;" value="'
					+qtyCount+ '"onkeydown="return StockCountingApprove.inputText(event);" onkeypress="return NextAndPrevTextField(event,this,\'CCResultInput\')"  onchange="return StockCountingApprove.changeCCResult(' +Utils.XSSEncode(id)+',\'' +Utils.XSSEncode(lot)+'\',this)"/></td>');  //soluong dem
			if(check ==2){
				html.push('<td style="text-align:center;" class="ColsTd5 ColsTdEnd"><a href="javascript:void(0)" onclick="StockCountingApprove.delCCResult(' +Utils.XSSEncode(id)+',\'' +Utils.XSSEncode(lot)+'\');"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
			} else{
				html.push('<td style="text-align:center;" class="ColsTd5 ColsTdEnd"></td>');
			}
		} else{
			html.push('<td class="ColsTd4 AlignRight">' + Utils.XSSEncode(qtyCount)  +'</td>');  //soluong dem
			html.push('<td style="text-align:center;" class="ColsTd5 ColsTdEnd"></td>');		
		}
		html.push('</tr>');	
		return html.join("");
	},
	delCCMapProduct :function(productCode){
		var msg = 'Bạn có chắc chắn xóa sản phẩm ' + Utils.XSSEncode(productCode) + ' khỏi đợt kiểm kê không ?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				StockCountingApprove._ccMapProducts.remove(productCode);
				StockCountingApprove._ccMapProductsAndResults.remove(productCode);
				$('#tr_' + productCode).replaceWith('');
			}
		});	
	},
	inputText:function(e){
		var tm = setTimeout(function(){
			if((e.keyCode < 47 || e.keyCode > 57)){
				return true; 
			}
			clearTimeout(tm); 
		}, 300);
	},
	approve: function(cycleCountType){
		$(".ErrorMsgStyle").hide();
		var params = new Object();
		params.cycleCountId = StockCountingApprove._cycleCountId;
		params.cycleCountType = cycleCountType;
		var msg = '';
		if(params.cycleCountType == 1){
			msg = 'Bạn có muốn thực hiện duyệt kiểm kho?';
			Utils.addOrSaveData(params, '/stock/approve/approveStockCount', null,  'errMsg', function(data) {
				StockCountingApprove.searchCycleCount();
			}, null, null, null,msg);
		} else if(params.cycleCountType == 2 ) {
			StockCountingApprove._cycleCountType = cycleCountType;
			msg = 'Bạn có muốn thực hiện hủy kiểm kho?';
			var html = $('#reasonDialog').html();
			$('#reasonDialogEasyUIDialog').dialog({
				title: msg,
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 400,
		        height: 200,
		        onOpen: function(){
		        	$('#txtReason').val('');
		        	$('#reasonDialogEasyUIDialog').addClass("easyui-dialog");
		        },	     
		        onClose : function(){
					$('#reasonDialogEasyUIDialog').dialog('destroy');
					$('#reasonDialog').html(html);
		        }
		     });
		} else if(params.cycleCountType == 3 ) {
			StockCountingApprove._cycleCountType = cycleCountType;
			msg = 'Bạn có muốn thực hiện từ chối kiểm kho?';
			var html = $('#reasonDialog').html();
			$('#reasonDialogEasyUIDialog').dialog({
				title: msg,
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 400,
		        height: 200,
		        onOpen: function(){
		        	$('#txtReason').val('');
		        	$('#reasonDialogEasyUIDialog').addClass("easyui-dialog");
		        },	     
		        onClose : function(){
					$('#reasonDialogEasyUIDialog').dialog('destroy');
					$('#reasonDialog').html(html);
		        }
		     });
		}
		
		return false;
	},
	reject: function(){
		$(".ErrorMsgStyle").hide();
		if($('#txtReason').val().length == 0){
			$('#errMsgReason').html('Bạn chưa nhập lý do.').show();
			return false;
		}
		var params = new Object();
		params.cycleCountId = StockCountingApprove._cycleCountId;
		params.cycleCountType = StockCountingApprove._cycleCountType;
		params.reason = $('#txtReason').val();
		Utils.saveData(params, '/stock/approve/approveStockCount', null,  'errMsg', function(data) {
			$('#reasonDialogEasyUIDialog').dialog('close');
			StockCountingApprove.searchCycleCount();
		}, null, null, null, null);
	},	
	exportExcel: function() {
		var params = new Object();
		params.cycleCountId = StockCountingApprove._cycleCountId;
		var msg = 'Bạn có muốn xuất báo cáo chênh lệch tồn kho của mã kiểm kê này không?';
		$('#errMsg').html('').hide();
		$('#divOverlay').show();
		ReportUtils.exportReport("/stock/approve/exportExcel", params);
		return false;
	},
	searchCycleCountOnDialog:function(){
		$('.ErrorMsgStyle').hide();
		StockCountingInput._isShowDlg = true;
		var html = $('#cycleCountDialog').html();
		$('#cycleCountEasyUIDialog').dialog({
			title: "Tìm kiếm kiểm kê",
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 850,
	        height :600,
	        onOpen: function(){
	        	StockCountingInput.loadComboboxShop("shop", '140');
	        	$('#cycleCountEasyUIDialog').addClass("easyui-dialog");
	        	$('#dgCycleCountType').addClass("MySelectBoxClass");
	        	$('#dgCycleCountType').customStyle();
	        	applyDateTimePicker('#dgCycleCountFromDate');
	        	applyDateTimePicker('#dgCycleCountToDate');
	        	$('#cycleCountEasyUIDialog .InputTextStyle:not(:disabled)').val('');
	        	$('#dgCycleCountType').val("-1").change();
	        	$('#dgCycleCountCode').parent().bind('keyup', function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			StockCountingApprove.searchCycleCountOnEsyUI();
	        		}
	        	});
	        	$('#cycleCountEasyUIDialog #grid').datagrid({
					url : '/stock/approve/search-cycle-count',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					pageNumber:1,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					queryParams:{
						shopCode:$('#shopCode').val().trim(),
						typeInventory:2
					},
					pageList  : [10,20,30],
					width : $('#cycleCountEasyUIDialog #productGridContainer').width(),
				    columns:[[				          
		              	GridUtil.getNormalField("cycleCountCode", "Mã kiểm kê", 70, "left"),
					    GridUtil.getNormalField("description", "Mô tả", 240, "left"),
					    {field: 'status', title: 'Trạng thái', width: 80,sortable:false,resizable:false, align: 'right',formatter : CountingFormatter.statusFormat2},
					    {field: 'createDate',title: 'Ngày bắt đầu kiểm kê',width: 90, sortable:false,resizable:false, align: 'center', formatter: function(v, r, i) {
					    	if (v) {
					    		return $.datepicker.formatDate('dd/mm/yy', new Date(v));
					    	}
					    	return "";
					    }},
					    {field: 'id', index:'id', hidden: true},
					    {field: 'firstNumber', index:'firstNumber', hidden: true},
					    {field: 'select', title : 'Chọn', width : 35, align : 'center', sortable : false, resizable : false,
					    	formatter: function(value, rowObject, opts) {
					    		return "<a href='javascript:void(0)' onclick=\"return StockCountingApprove.selectCycleCount(" + opts +");\">Chọn</a>";
					    	}
					   }
				    ]],
				    onBeforeLoad:function(param){
				    	
				    },
				    onLoadSuccess :function(data){
				    	GridUtil.updateDefaultRowNumWidth('#cycleCountEasyUIDialog', 'grid');
				    	$('.datagrid-header-rownumber').html('STT');
				    	Utils.bindAutoSearch();
				    }
				});  
	        },	     
	        onClose : function(){
	        	$('#cycleCountEasyUIDialog').dialog("destroy");
	        	$('#cycleCountDialog').html(html);
	        	StockCountingInput._isShowDlg = false;
	        }
	     });
	},
	selectCycleCount:function(id){
		StockCountingApprove._cycleCountId = id;
		StockCountingApprove.search();
	},
	changeShop: function() {
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.isSearch = true;
		$.ajax({
			type : "POST",
			url : '/stock/counting/changeshop',
			data : params,
			dataType : "json",
			success : function(data) {
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null){
					$('#cycleCountWareHouse').combobox({
						valueField : 'warehouseId',
						textField : 'warehouseName',
						data : data.lstWarehouseVO,
						panelWidth:206,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
				        onLoadSuccess: function(){
				        	var arr = $('#cycleCountWareHouse').combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$('#cycleCountWareHouse').combobox('select', arr[0].warehouseId);
				        	}
				        }
					});
				 }
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	},
	searchCycleCount:function(){
		$('#saveMessageError').html('');
		$("#cycleCountInfo").html('');
		var	msg = Utils.getMessageOfInvalidFormatDate('dgCycleCountFromDate', 'Từ ngày kiểm kê');
		if(msg.length > 0) {
			$('#saveMessageError').html(msg).show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		var	msg = Utils.getMessageOfInvalidFormatDate('dgCycleCountToDate', 'Đến ngày kiểm kê');
		if(msg.length > 0) {
			$('#saveMessageError').html(msg).show();
			var tm = setTimeout(function(){
				$('#saveMessageError').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		var startDate = $('#dgCycleCountFromDate').val().trim();
		var endDate = $('#dgCycleCountToDate').val().trim();
		if (msg.length == 0 && !Utils.compareDate(startDate, endDate)) {
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
			$('#dgCycleCountFromDate').focus();
			$('#saveMessageError').html(msg).show();
			return false;
		}
		var params = new Object();
		params.shopCode = $('#shopCode').val().trim();
		params.cycleCountCode = $('#dgCycleCountCode').val().trim();
		params.cycleCountStatus = $('#dgCycleCountType').val();
		params.startDate = $('#dgCycleCountFromDate').val().trim();
		params.endDate = $('#dgCycleCountToDate').val().trim();
		params.description = $('#dgCycleCountDes').val().trim();
		params.numPeriod = $('#numPeriod').combobox('getValue');
		params.yearPeriod = $('#yearPeriod').val();
		$('#grid').datagrid('load',params);
	},
	iniGridCycleCount:function(yearPeriod, numPeriod){
		var params = new Object();
		params.shopCode = $('#shopCode').val().trim();
		params.numPeriod = numPeriod;
		params.yearPeriod = yearPeriod;
		$('#grid').datagrid({
			url : '/stock/approve/search-cycle-count',
			autoRowHeight : true,
			rownumbers : true, 
			singleSelect: true,
			pagination:true,
			rowNum : 10,
			pageNumber:1,
			scrollbarSize : 0,
			fitColumns:true,
			cache: false, 
			queryParams: params,
			pageList  : [10,20,30],
			width : $('#productGridContainer').width(),
		    columns:[[
				GridUtil.getNormalField("vungshopName", "Vùng", 100, "left"),
				GridUtil.getNormalField("areaShopName", "Khu vực", 100, "left"),
              	GridUtil.getNormalField("shopName", "NPP", 100, "left"),
              	GridUtil.getNormalField("shopPhone", "Điện thoại", 70, "left"),
              	GridUtil.getNormalField("shopAddress", "Địa chỉ", 120, "left"),
			    GridUtil.getNormalField("wareHouseName", "Kho", 100, "left"),
			    GridUtil.getNormalField("cycleCountCode", "Mã kiểm kê", 70, "left"),
			    {field: 'startDate',title: 'Ngày kiểm kê', width: 70, sortable:false, resizable:false, align: 'center', formatter: function(v, r, i) {
			    	if (v) {
			    		return $.datepicker.formatDate('dd/mm/yy', new Date(v));
			    	}
			    	return "";
			    }},
			    GridUtil.getNormalField("createUser", "Người thực hiện", 70, "left"),
			    {field: 'approvedDate',title: 'Ngày duyệt', width: 70, sortable:false, resizable:false, align: 'center', formatter: function(v, r, i) {
			    	if (v) {
			    		return $.datepicker.formatDate('dd/mm/yy', new Date(v));
			    	}
			    	return "";
			    }},
			    GridUtil.getNormalField("approvedUser", "Người duyệt", 70, "left"),
			    {field: 'status', title: 'Trạng thái', width: 80, sortable:false, resizable:false, align: 'left',formatter : CountingFormatter.statusFormat},			    
			    {field: 'id', index:'id', hidden: true},
			    {field: 'select', title : '', width : 35, align : 'center', sortable : false, resizable : false,
			    	formatter:function(value, rowObject, opts){
			    		if(rowObject.status != null && rowObject.status != '5' && rowObject.status != 5){
			    			return "<span style='cursor:pointer' onclick=\"return StockCountingApprove.selectCycleCount('" + rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></span>";
			    		}
			    		
			    	}
			    }
		    ]],
		    onBeforeLoad:function(param){
		    	
		    },
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
		    	Utils.bindAutoSearch();
		    }
		});
	},
	search:function(isSave){
		$(".ErrorMsgStyle").hide();
		var param = new Object();
		param.cycleCountId = StockCountingApprove._cycleCountId;
		if(isSave){
			Utils.getHtmlDataByAjax(param, "/stock/approve/search", function(data){
				try {
					var _data = JSON.parse(data);
					if(_data.error != null && _data.errMsg != null && _data.errMsg != undefined){
						$('#errMsgSearch').html(_data.errMsg).show();
						$("#cycleCountInfo").html('').hide();
						StockCountingApprove._ccMapProductsAndResults = new Map();
						StockCountingApprove._ccMapProducts = new Map();
						StockCountingApprove.mapNewProducts = new Map();
					}
				} catch(e) {
					$("#cycleCountInfo").html(data).show();
					$('#cycleCountCode').focus();
					StockCountingApprove._ccMapProductsAndResults = new Map();
					StockCountingApprove._ccMapProducts = new Map();
					StockCountingApprove.mapNewProducts = new Map();
				}				
			}, 'loadingSearch', 'POST');
		} else {
			var ccMapKey = StockCountingApprove._ccMapProducts.keyArray;
			var ccMapValue = StockCountingApprove._ccMapProducts.valArray;
			var check = false;
			for( var i = 0; i < ccMapKey.length; i++ ) {
				var ccMap =  ccMapValue[i];
				if(ccMap.id== -1){
					check = true;
					break;
				}
		    }
			if(check == true){
				var msg = 'Bạn có có thể sẽ mất những sản phẩm được thêm mới';
				$.messager.confirm('Xác nhận', msg, function(r){
					if (r){
						Utils.getHtmlDataByAjax(param, "/stock/approve/search", function(data){
							try {
								var _data = JSON.parse(data);
								if(_data.error != null && _data.errMsg != null && _data.errMsg != undefined){
									$('#errMsgSearch').html(_data.errMsg).show();
									$("#cycleCountInfo").html('').hide();
									StockCountingApprove._ccMapProductsAndResults = new Map();
									StockCountingApprove._ccMapProducts = new Map();
									StockCountingApprove.mapNewProducts = new Map();
								}
							} catch(e) {
								$("#cycleCountInfo").html(data).show();
								StockCountingApprove.clearNewProduct();
								$('#cycleCountDes').focus();
								StockCountingApprove.replaceProduct();
							}
						}, 'loadingSearch', 'POST');
					}
				});
			} else {
				Utils.getHtmlDataByAjax(param, "/stock/approve/search", function(data){
					try {
						var _data = JSON.parse(data);
						if(_data.error != null && _data.errMsg != null && _data.errMsg != undefined){
							$('#errMsgSearch').html(_data.errMsg).show();
							$("#cycleCountInfo").html('').hide();
							StockCountingApprove._ccMapProductsAndResults = new Map();
							StockCountingApprove._ccMapProducts = new Map();
							StockCountingApprove.mapNewProducts = new Map();
						}
					} catch(e) {
						$("#cycleCountInfo").html(data).show();
						$('#cycleCountDes').focus();
						StockCountingApprove.replaceProduct();	
					}
				}, 'loadingSearch', 'POST');
			}
		}
		return false;
	}
};